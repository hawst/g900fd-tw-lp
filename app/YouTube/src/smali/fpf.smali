.class public final Lfpf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lghz;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final d:Lfph;


# instance fields
.field final a:I

.field final b:Z

.field public final c:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1832
    new-instance v0, Lfpg;

    invoke-direct {v0}, Lfpg;-><init>()V

    sput-object v0, Lfpf;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1904
    new-instance v0, Lfph;

    invoke-direct {v0}, Lfph;-><init>()V

    sput-object v0, Lfpf;->d:Lfph;

    return-void
.end method

.method public constructor <init>(IZLandroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1847
    iput p1, p0, Lfpf;->a:I

    .line 1848
    iput-boolean p2, p0, Lfpf;->b:Z

    .line 1849
    iput-object p3, p0, Lfpf;->c:Landroid/net/Uri;

    .line 1850
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 1865
    iget-boolean v0, p0, Lfpf;->b:Z

    if-eqz v0, :cond_0

    .line 1866
    iget v0, p0, Lfpf;->a:I

    mul-int/2addr v0, p1

    mul-int/lit16 v0, v0, 0x3e8

    div-int/lit8 v0, v0, 0x64

    .line 1868
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lfpf;->a:I

    goto :goto_0
.end method

.method public final synthetic a()Lgia;
    .locals 1

    .prologue
    .line 1821
    new-instance v0, Lfph;

    invoke-direct {v0, p0}, Lfph;-><init>(Lfpf;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1873
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1886
    if-nez p1, :cond_1

    .line 1895
    :cond_0
    :goto_0
    return v0

    .line 1889
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1892
    check-cast p1, Lfpf;

    .line 1893
    iget v1, p0, Lfpf;->a:I

    iget v2, p1, Lfpf;->a:I

    if-ne v1, v2, :cond_0

    .line 1894
    iget-boolean v1, p0, Lfpf;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lfpf;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1895
    iget-object v1, p0, Lfpf;->c:Landroid/net/Uri;

    iget-object v2, p1, Lfpf;->c:Landroid/net/Uri;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1878
    iget v0, p0, Lfpf;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1879
    iget-boolean v0, p0, Lfpf;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1880
    iget-object v0, p0, Lfpf;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1881
    return-void

    :cond_0
    move v0, v1

    .line 1879
    goto :goto_0
.end method
