.class public final Lbxz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lgcd;

.field public final b:Lgjm;


# direct methods
.method private constructor <init>(Lgcd;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    iput-object v0, p0, Lbxz;->a:Lgcd;

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lbxz;->b:Lgjm;

    .line 24
    return-void
.end method

.method private constructor <init>(Lgjm;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjm;

    iput-object v0, p0, Lbxz;->b:Lgjm;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lbxz;->a:Lgcd;

    .line 29
    return-void
.end method

.method public static a(Lgcd;)Lbxz;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lbxz;

    invoke-direct {v0, p0}, Lbxz;-><init>(Lgcd;)V

    return-object v0
.end method

.method public static a(Lgjm;)Lbxz;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lbxz;

    invoke-direct {v0, p0}, Lbxz;-><init>(Lgjm;)V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lbxz;->a:Lgcd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 49
    instance-of v1, p1, Lbxz;

    if-eqz v1, :cond_0

    .line 50
    check-cast p1, Lbxz;

    .line 51
    iget-object v1, p0, Lbxz;->a:Lgcd;

    iget-object v2, p1, Lbxz;->a:Lgcd;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbxz;->b:Lgjm;

    iget-object v2, p1, Lbxz;->b:Lgjm;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 53
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Lbxz;->a:Lgcd;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 60
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lbxz;->b:Lgjm;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 61
    return v0

    .line 58
    :cond_0
    iget-object v0, p0, Lbxz;->a:Lgcd;

    invoke-virtual {v0}, Lgcd;->hashCode()I

    move-result v0

    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, p0, Lbxz;->b:Lgjm;

    invoke-virtual {v1}, Lgjm;->hashCode()I

    move-result v1

    goto :goto_1
.end method
