.class public abstract Lguf;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/Set;

.field private b:Lgtk;

.field private c:I

.field private d:Ljava/util/Set;

.field private e:Ljava/util/concurrent/RunnableFuture;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lgty;)V
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 28
    new-instance v0, Lgtk;

    .line 29
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "_NOTIFICATIONS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lgtk;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lguf;->b:Lgtk;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lguf;->c:I

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lguf;->d:Ljava/util/Set;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lguf;->a:Ljava/util/Set;

    .line 39
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lgug;

    invoke-direct {v1, p0, p1, p2}, Lgug;-><init>(Lguf;Ljava/lang/String;Lgty;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Lguf;->e:Ljava/util/concurrent/RunnableFuture;

    .line 54
    return-void
.end method

.method static synthetic a(Lguf;)Ljava/util/concurrent/RunnableFuture;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lguf;->e:Ljava/util/concurrent/RunnableFuture;

    return-object v0
.end method

.method private declared-synchronized a()V
    .locals 2

    .prologue
    .line 234
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lguf;->c:I

    if-nez v0, :cond_0

    .line 235
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lguf;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 237
    :cond_0
    iget v0, p0, Lguf;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lguf;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    monitor-exit p0

    return-void

    .line 234
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lguf;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lguf;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 323
    invoke-direct {p0}, Lguf;->a()V

    .line 324
    iget-object v1, p0, Lguf;->b:Lgtk;

    new-instance v2, Lguj;

    invoke-direct {v2, p0, p1, p2}, Lguj;-><init>(Lguf;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v1, Lgtk;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    iget-object v0, v1, Lgtk;->b:Landroid/os/Handler;

    new-instance v3, Lgtm;

    invoke-direct {v3, v1, v2}, Lgtm;-><init>(Lgtk;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 335
    return-void

    .line 324
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 216
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    return-void
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lguf;->c:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 248
    iget v0, p0, Lguf;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lguf;->c:I

    .line 249
    iget v0, p0, Lguf;->c:I

    if-nez v0, :cond_0

    .line 250
    invoke-virtual {p0}, Lguf;->stopSelf()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :cond_0
    monitor-exit p0

    return-void

    .line 246
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lguf;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lguf;->b()V

    return-void
.end method

.method static synthetic b(Lguf;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x1

    iget-object v1, p0, Lguf;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgue;

    invoke-interface {v0, p1, p2}, Lgue;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lguf;->c(Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lguf;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    iget-object v0, p0, Lguf;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    invoke-direct {p0}, Lguf;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    :cond_0
    monitor-exit p0

    return-void

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()Lgtz;
    .locals 3

    .prologue
    .line 313
    :try_start_0
    iget-object v0, p0, Lguf;->e:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v0}, Ljava/util/concurrent/RunnableFuture;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtz;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 314
    :catch_0
    move-exception v0

    .line 315
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 316
    new-instance v1, Lgub;

    const-string v2, "Opening job storage was interrupted"

    invoke-direct {v1, v2, v0}, Lgub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 317
    :catch_1
    move-exception v0

    .line 318
    new-instance v1, Lgub;

    const-string v2, "Opening job storage failed"

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lgub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic c(Lguf;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lguf;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgue;

    invoke-interface {v0}, Lgue;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private declared-synchronized c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 274
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    iget-object v0, p0, Lguf;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    invoke-direct {p0}, Lguf;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    :cond_0
    monitor-exit p0

    return-void

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lguf;)Lgtz;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lguf;->c()Lgtz;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 169
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    invoke-direct {p0}, Lguf;->c()Lgtz;

    move-result-object v0

    .line 172
    invoke-virtual {v0, p1}, Lgtz;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lgtn;Lgum;)V
    .locals 2

    .prologue
    .line 82
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p0, Lguf;->a:Ljava/util/Set;

    new-instance v1, Lgun;

    invoke-direct {v1, p0, p1, p2}, Lgun;-><init>(Lguf;Lgtn;Lgum;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public final a(Ljava/lang/String;Lguc;)V
    .locals 1

    .prologue
    .line 182
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    invoke-direct {p0}, Lguf;->c()Lgtz;

    move-result-object v0

    .line 187
    monitor-enter p0

    .line 188
    :try_start_0
    invoke-virtual {v0, p1, p2}, Lgtz;->a(Ljava/lang/String;Lguc;)Ljava/lang/Object;

    move-result-object v0

    .line 189
    invoke-direct {p0, p1, v0}, Lguf;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 190
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 104
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 105
    iget-object v0, p0, Lguf;->b:Lgtk;

    iget-object v1, v0, Lgtk;->a:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Landroid/os/Handler;

    iget-object v2, v0, Lgtk;->a:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, v0, Lgtk;->b:Landroid/os/Handler;

    .line 106
    invoke-direct {p0}, Lguf;->a()V

    iget-object v0, p0, Lguf;->b:Lgtk;

    new-instance v1, Lguh;

    invoke-direct {v1, p0}, Lguh;-><init>(Lguf;)V

    invoke-virtual {v0, v1}, Lgtk;->a(Ljava/lang/Runnable;)V

    .line 107
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lguf;->b:Lgtk;

    invoke-virtual {v0}, Lgtk;->c()V

    .line 112
    iget-object v0, p0, Lguf;->b:Lgtk;

    new-instance v1, Lgui;

    invoke-direct {v1, p0}, Lgui;-><init>(Lguf;)V

    invoke-virtual {v0, v1}, Lgtk;->a(Ljava/lang/Runnable;)V

    .line 113
    iget-object v0, p0, Lguf;->b:Lgtk;

    invoke-virtual {v0}, Lgtk;->a()V

    .line 114
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 115
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x1

    return v0
.end method
