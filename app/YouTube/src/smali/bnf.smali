.class public final Lbnf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final a:Lbnf;


# instance fields
.field public final b:Lbnc;

.field public final c:Lbnh;

.field public final d:Lbnd;

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 15
    new-instance v0, Lbnf;

    sget-object v1, Lbnc;->a:Lbnc;

    sget-object v2, Lbnh;->a:Lbnh;

    sget-object v3, Lbnd;->a:Lbnd;

    move v5, v4

    move v6, v4

    move v7, v4

    move v8, v4

    invoke-direct/range {v0 .. v8}, Lbnf;-><init>(Lbnc;Lbnh;Lbnd;ZZZZZ)V

    sput-object v0, Lbnf;->a:Lbnf;

    .line 25
    new-instance v0, Lbng;

    invoke-direct {v0}, Lbng;-><init>()V

    sput-object v0, Lbnf;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x0

    .line 67
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbnc;->a(Ljava/lang/String;)Lbnc;

    move-result-object v1

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbnh;->a(Ljava/lang/String;)Lbnh;

    move-result-object v2

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbnd;->a(Ljava/lang/String;)Lbnd;

    move-result-object v3

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v4

    if-eqz v4, :cond_1

    move v4, v0

    .line 72
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v5

    if-eqz v5, :cond_2

    move v5, v0

    .line 73
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v6

    if-eqz v6, :cond_3

    move v6, v0

    .line 74
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v7

    if-eqz v7, :cond_4

    move v7, v0

    .line 75
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v9

    if-eqz v9, :cond_0

    move v8, v0

    :cond_0
    move-object v0, p0

    .line 67
    invoke-direct/range {v0 .. v8}, Lbnf;-><init>(Lbnc;Lbnh;Lbnd;ZZZZZ)V

    .line 76
    return-void

    :cond_1
    move v4, v8

    .line 71
    goto :goto_0

    :cond_2
    move v5, v8

    .line 72
    goto :goto_1

    :cond_3
    move v6, v8

    .line 73
    goto :goto_2

    :cond_4
    move v7, v8

    .line 74
    goto :goto_3
.end method

.method public constructor <init>(Lbnc;Lbnh;Lbnd;ZZZZZ)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbnc;

    iput-object v0, p0, Lbnf;->b:Lbnc;

    .line 57
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbnh;

    iput-object v0, p0, Lbnf;->c:Lbnh;

    .line 58
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbnd;

    iput-object v0, p0, Lbnf;->d:Lbnd;

    .line 59
    iput-boolean p4, p0, Lbnf;->e:Z

    .line 60
    iput-boolean p5, p0, Lbnf;->f:Z

    .line 61
    iput-boolean p6, p0, Lbnf;->g:Z

    .line 62
    iput-boolean p7, p0, Lbnf;->h:Z

    .line 63
    iput-boolean p8, p0, Lbnf;->i:Z

    .line 64
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 129
    if-ne p1, p0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return v0

    .line 133
    :cond_1
    instance-of v2, p1, Lbnf;

    if-eqz v2, :cond_3

    .line 134
    check-cast p1, Lbnf;

    .line 135
    iget-object v2, p0, Lbnf;->b:Lbnc;

    iget-object v3, p1, Lbnf;->b:Lbnc;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lbnf;->c:Lbnh;

    iget-object v3, p1, Lbnf;->c:Lbnh;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lbnf;->d:Lbnd;

    iget-object v3, p1, Lbnf;->d:Lbnd;

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lbnf;->e:Z

    iget-boolean v3, p1, Lbnf;->e:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lbnf;->f:Z

    iget-boolean v3, p1, Lbnf;->f:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lbnf;->g:Z

    iget-boolean v3, p1, Lbnf;->g:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lbnf;->h:Z

    iget-boolean v3, p1, Lbnf;->h:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lbnf;->i:Z

    iget-boolean v3, p1, Lbnf;->i:Z

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 145
    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 117
    iget-object v0, p0, Lbnf;->b:Lbnc;

    invoke-virtual {v0}, Lbnc;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lbnf;->c:Lbnh;

    invoke-virtual {v0}, Lbnh;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lbnf;->d:Lbnd;

    invoke-virtual {v0}, Lbnd;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 120
    iget-boolean v0, p0, Lbnf;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 121
    iget-boolean v0, p0, Lbnf;->f:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 122
    iget-boolean v0, p0, Lbnf;->g:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 123
    iget-boolean v0, p0, Lbnf;->h:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 124
    iget-boolean v0, p0, Lbnf;->i:Z

    if-eqz v0, :cond_4

    :goto_4
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 125
    return-void

    :cond_0
    move v0, v2

    .line 120
    goto :goto_0

    :cond_1
    move v0, v2

    .line 121
    goto :goto_1

    :cond_2
    move v0, v2

    .line 122
    goto :goto_2

    :cond_3
    move v0, v2

    .line 123
    goto :goto_3

    :cond_4
    move v1, v2

    .line 124
    goto :goto_4
.end method
