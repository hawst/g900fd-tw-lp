.class public final Ldno;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldmw;


# instance fields
.field public final a:Landroid/content/Context;

.field final b:Ljava/util/Map;

.field final c:Ljava/util/Map;

.field public d:Ldnr;

.field public e:Lfad;

.field private final f:Landroid/app/Activity;

.field private final g:Lfxe;

.field private final h:Ljava/util/List;

.field private final i:Ljava/util/Map;

.field private final j:Ljava/util/Map;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lfxe;)V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldno;->h:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldno;->i:Ljava/util/Map;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldno;->b:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldno;->j:Ljava/util/Map;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldno;->c:Ljava/util/Map;

    .line 106
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldno;->a:Landroid/content/Context;

    .line 107
    iput-object p1, p0, Ldno;->f:Landroid/app/Activity;

    .line 108
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Ldno;->g:Lfxe;

    .line 109
    return-void
.end method

.method private b(Ldnp;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 300
    iget-object v0, p0, Ldno;->b:Ljava/util/Map;

    iget-object v2, p1, Ldnp;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 301
    iget-object v0, p0, Ldno;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 302
    iget-object v0, p0, Ldno;->e:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    invoke-virtual {v0}, Ldmz;->a()Ljava/util/Map;

    move-result-object v0

    iget-object v2, p1, Ldnp;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjm;

    .line 303
    if-eqz v0, :cond_2

    .line 309
    invoke-virtual {v0}, Lgjm;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, v0, Lgjm;->g:Lgje;

    const-string v2, "metadata_updated"

    .line 310
    invoke-virtual {v0, v2, v1}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    iget-object v0, p0, Ldno;->d:Ldnr;

    invoke-interface {v0}, Ldnr;->b()V

    :cond_0
    :goto_0
    move v0, v1

    .line 321
    :goto_1
    return v0

    .line 314
    :cond_1
    iget-object v0, p0, Ldno;->e:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    invoke-virtual {v0}, Ldmz;->a()Ljava/util/Map;

    move-result-object v0

    iget-object v2, p1, Ldnp;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldno;->i:Ljava/util/Map;

    iget-object v2, p1, Ldnp;->b:Ljava/lang/String;

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldno;->e:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    iget-object v2, p1, Ldnp;->b:Ljava/lang/String;

    const/16 v3, 0x40

    invoke-virtual {v0, v2, v3}, Ldmz;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 317
    :cond_2
    invoke-virtual {p0, p1}, Ldno;->a(Ldnp;)V

    goto :goto_0

    .line 321
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 206
    const/4 v0, 0x0

    move v1, v0

    .line 207
    :goto_0
    iget-object v0, p0, Ldno;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 208
    iget-object v0, p0, Ldno;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnp;

    invoke-direct {p0, v0}, Ldno;->b(Ldnp;)Z

    move-result v0

    .line 209
    if-nez v0, :cond_1

    .line 211
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 213
    goto :goto_0

    .line 214
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Ldno;->k:Z

    if-eqz v0, :cond_0

    .line 229
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldno;->k:Z

    .line 230
    invoke-virtual {p0}, Ldno;->c()V

    .line 236
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Ldno;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 232
    invoke-direct {p0}, Ldno;->e()V

    goto :goto_0

    .line 234
    :cond_1
    iget-object v0, p0, Ldno;->d:Ldnr;

    invoke-interface {v0}, Ldnr;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;Lgcg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lgje;Z)V
    .locals 3

    .prologue
    .line 141
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    const-string v0, "filename of the video being uploaded was not provided."

    invoke-static {p2, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 143
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    new-instance v1, Ldnp;

    invoke-direct {v1}, Ldnp;-><init>()V

    .line 148
    iput-object p2, v1, Ldnp;->c:Ljava/lang/String;

    .line 149
    if-nez p10, :cond_0

    sget-object p3, Lgcg;->c:Lgcg;

    :cond_0
    iput-object p3, v1, Ldnp;->d:Lgcg;

    .line 150
    iput-object p4, v1, Ldnp;->e:Ljava/lang/String;

    .line 151
    iput-object p5, v1, Ldnp;->f:Ljava/lang/String;

    .line 152
    const/4 v0, 0x0

    iput-object v0, v1, Ldnp;->g:Ljava/lang/String;

    .line 153
    iput-object p7, v1, Ldnp;->h:Ljava/lang/String;

    .line 154
    iput-object p8, v1, Ldnp;->i:Landroid/util/Pair;

    .line 160
    iput-object p1, v1, Ldnp;->a:Landroid/net/Uri;

    .line 161
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ldnp;->b:Ljava/lang/String;

    .line 162
    iget-object v0, p0, Ldno;->c:Ljava/util/Map;

    iget-object v2, v1, Ldnp;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Ldnp;->j:Ljava/lang/String;

    .line 164
    iput-object p9, v1, Ldnp;->k:Lgje;

    .line 165
    iget-object v0, v1, Ldnp;->k:Lgje;

    const-string v2, "metadata_updated"

    invoke-virtual {v0, v2, p10}, Lgje;->a(Ljava/lang/String;Z)V

    .line 167
    iget-object v0, p0, Ldno;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    invoke-virtual {p0}, Ldno;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    invoke-direct {p0, v1}, Ldno;->b(Ldnp;)Z

    .line 171
    :cond_1
    return-void
.end method

.method a(Ldnp;)V
    .locals 10

    .prologue
    .line 334
    invoke-virtual {p0}, Ldno;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Ldno;->b:Ljava/util/Map;

    iget-object v1, p1, Ldnp;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    iget-object v0, p1, Ldnp;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Ldno;->e:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    iget-object v1, p1, Ldnp;->b:Ljava/lang/String;

    iget-object v2, p1, Ldnp;->j:Ljava/lang/String;

    iget-object v3, p1, Ldnp;->k:Lgje;

    invoke-virtual {v0, v1, v2, v3}, Ldmz;->a(Ljava/lang/String;Ljava/lang/String;Lgje;)V

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-object v0, p0, Ldno;->g:Lfxe;

    iget-object v1, p1, Ldnp;->c:Ljava/lang/String;

    iget-object v2, p1, Ldnp;->d:Lgcg;

    iget-object v3, p1, Ldnp;->e:Ljava/lang/String;

    iget-object v4, p1, Ldnp;->f:Ljava/lang/String;

    iget-object v5, p1, Ldnp;->g:Ljava/lang/String;

    iget-object v6, p1, Ldnp;->h:Ljava/lang/String;

    iget-object v7, p1, Ldnp;->i:Landroid/util/Pair;

    iget-object v8, p0, Ldno;->f:Landroid/app/Activity;

    if-eqz v8, :cond_2

    iget-object v8, p0, Ldno;->f:Landroid/app/Activity;

    new-instance v9, Ldnq;

    invoke-direct {v9, p0, p1}, Ldnq;-><init>(Ldno;Ldnp;)V

    invoke-static {v8, v9}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v8

    :goto_1
    invoke-interface/range {v0 .. v8}, Lfxe;->a(Ljava/lang/String;Lgcg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Leuc;)V

    goto :goto_0

    :cond_2
    new-instance v8, Ldnq;

    invoke-direct {v8, p0, p1}, Ldnq;-><init>(Ldno;Ldnp;)V

    goto :goto_1
.end method

.method public final a(Lgjm;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 262
    iget-object v0, p0, Ldno;->b:Ljava/util/Map;

    iget-object v2, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Ldno;->b:Ljava/util/Map;

    iget-object v2, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnp;

    .line 277
    iget-object v2, p0, Ldno;->j:Ljava/util/Map;

    iget-object v3, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    iget-object v2, p0, Ldno;->d:Ldnr;

    iget-object v0, p1, Lgjm;->g:Lgje;

    const-string v3, "metadata_updated"

    invoke-virtual {v0, v3, v1}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v2, v0}, Ldnr;->a(Z)V

    .line 280
    invoke-direct {p0}, Ldno;->e()V

    .line 282
    :cond_0
    return-void

    .line 278
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 294
    return-void
.end method

.method public final b(Lgjm;)V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 186
    invoke-virtual {p0}, Ldno;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187
    iget-object v0, p0, Ldno;->e:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    invoke-virtual {v0}, Ldmz;->a()Ljava/util/Map;

    move-result-object v0

    .line 188
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjm;

    .line 189
    iget-object v1, v0, Lgjm;->g:Lgje;

    const-string v3, "metadata_updated"

    invoke-virtual {v1, v3, v4}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 190
    iget-object v1, p0, Ldno;->e:Lfad;

    iget-object v1, v1, Lfad;->b:Landroid/os/Binder;

    check-cast v1, Ldmz;

    iget-object v0, v0, Lgjm;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ldmz;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 193
    :cond_1
    iget-object v0, p0, Ldno;->e:Lfad;

    iget-object v1, p0, Ldno;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lfad;->a(Landroid/content/Context;)V

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Ldno;->e:Lfad;

    .line 195
    iget-object v0, p0, Ldno;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 196
    iget-object v0, p0, Ldno;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 197
    iget-object v0, p0, Ldno;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 198
    iget-object v0, p0, Ldno;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 199
    iget-object v0, p0, Ldno;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 203
    :goto_1
    return-void

    .line 201
    :cond_2
    iput-boolean v4, p0, Ldno;->k:Z

    goto :goto_1
.end method

.method public final c(Lgjm;)V
    .locals 4

    .prologue
    .line 252
    iget-wide v0, p1, Lgjm;->e:J

    iget-wide v2, p1, Lgjm;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Ldno;->d:Ldnr;

    .line 255
    :cond_0
    return-void
.end method

.method public final d(Lgjm;)V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Ldno;->j:Ljava/util/Map;

    iget-object v1, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lgjm;->c:Lgjn;

    sget-object v1, Lgjn;->c:Lgjn;

    if-ne v0, v1, :cond_0

    .line 287
    iget-object v0, p0, Ldno;->j:Ljava/util/Map;

    iget-object v1, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Ldno;->e:Lfad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldno;->e:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lgjm;)V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Ldno;->i:Ljava/util/Map;

    iget-object v1, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Ldno;->i:Ljava/util/Map;

    iget-object v1, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnp;

    invoke-virtual {p0, v0}, Ldno;->a(Ldnp;)V

    .line 243
    :cond_0
    return-void
.end method
