.class public final enum Lcvj;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcvj;

.field public static final enum b:Lcvj;

.field public static final enum c:Lcvj;

.field public static final enum d:Lcvj;

.field public static final enum e:Lcvj;

.field public static final enum f:Lcvj;

.field public static final enum g:Lcvj;

.field private static final synthetic h:[Lcvj;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcvj;

    const-string v1, "NOT_DRAWABLE"

    invoke-direct {v0, v1, v3}, Lcvj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvj;->a:Lcvj;

    .line 35
    new-instance v0, Lcvj;

    const-string v1, "AD_PREFETCH"

    invoke-direct {v0, v1, v4}, Lcvj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvj;->b:Lcvj;

    .line 36
    new-instance v0, Lcvj;

    const-string v1, "AD_PREFETCH_FREQUENCY_CAPPED"

    invoke-direct {v0, v1, v5}, Lcvj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvj;->c:Lcvj;

    .line 37
    new-instance v0, Lcvj;

    const-string v1, "AD_MARKER"

    invoke-direct {v0, v1, v6}, Lcvj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvj;->d:Lcvj;

    .line 38
    new-instance v0, Lcvj;

    const-string v1, "AD_MARKER_FREQUENCY_CAPPED"

    invoke-direct {v0, v1, v7}, Lcvj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvj;->e:Lcvj;

    .line 39
    new-instance v0, Lcvj;

    const-string v1, "AD_FADEOUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcvj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvj;->f:Lcvj;

    .line 40
    new-instance v0, Lcvj;

    const-string v1, "AD_FADEOUT_FREQUENCY_CAPPED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcvj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvj;->g:Lcvj;

    .line 33
    const/4 v0, 0x7

    new-array v0, v0, [Lcvj;

    sget-object v1, Lcvj;->a:Lcvj;

    aput-object v1, v0, v3

    sget-object v1, Lcvj;->b:Lcvj;

    aput-object v1, v0, v4

    sget-object v1, Lcvj;->c:Lcvj;

    aput-object v1, v0, v5

    sget-object v1, Lcvj;->d:Lcvj;

    aput-object v1, v0, v6

    sget-object v1, Lcvj;->e:Lcvj;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcvj;->f:Lcvj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcvj;->g:Lcvj;

    aput-object v2, v0, v1

    sput-object v0, Lcvj;->h:[Lcvj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcvj;
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcvj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcvj;

    return-object v0
.end method

.method public static values()[Lcvj;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcvj;->h:[Lcvj;

    invoke-virtual {v0}, [Lcvj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcvj;

    return-object v0
.end method
