.class public final Lgml;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbt;


# instance fields
.field final a:Levi;

.field final b:Lgmt;

.field final c:Lewi;

.field final d:Lgmm;

.field final e:Lgnj;

.field public final f:Lgnp;

.field public final g:Lgmh;

.field public final h:Lgmf;

.field private final i:Lgnk;

.field private j:Lgme;

.field private final k:Lgmo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lgmt;Lewi;Ljava/util/concurrent/Executor;Lgmm;)V
    .locals 4

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v1, Lgmr;

    .line 95
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 96
    invoke-static {p2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgmn;

    invoke-direct {v3, p0}, Lgmn;-><init>(Lgml;)V

    invoke-direct {v1, v0, v2, v3}, Lgmr;-><init>(Landroid/content/Context;Ljava/lang/String;Lgmn;)V

    iput-object v1, p0, Lgml;->a:Levi;

    .line 98
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmt;

    iput-object v0, p0, Lgml;->b:Lgmt;

    .line 99
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Lgml;->c:Lewi;

    .line 100
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmm;

    iput-object v0, p0, Lgml;->d:Lgmm;

    .line 102
    new-instance v0, Lgnj;

    iget-object v1, p0, Lgml;->a:Levi;

    invoke-direct {v0, v1}, Lgnj;-><init>(Levi;)V

    iput-object v0, p0, Lgml;->e:Lgnj;

    .line 103
    new-instance v0, Lgnp;

    iget-object v1, p0, Lgml;->a:Levi;

    invoke-direct {v0, v1, p3}, Lgnp;-><init>(Levi;Lgmt;)V

    iput-object v0, p0, Lgml;->f:Lgnp;

    .line 104
    new-instance v0, Lgnk;

    iget-object v1, p0, Lgml;->a:Levi;

    invoke-direct {v0, v1}, Lgnk;-><init>(Levi;)V

    iput-object v0, p0, Lgml;->i:Lgnk;

    .line 105
    new-instance v0, Lgme;

    iget-object v1, p0, Lgml;->a:Levi;

    invoke-direct {v0, v1}, Lgme;-><init>(Levi;)V

    iput-object v0, p0, Lgml;->j:Lgme;

    .line 106
    new-instance v0, Lgmh;

    iget-object v1, p0, Lgml;->a:Levi;

    invoke-direct {v0, v1}, Lgmh;-><init>(Levi;)V

    iput-object v0, p0, Lgml;->g:Lgmh;

    .line 107
    new-instance v0, Lgmf;

    iget-object v1, p0, Lgml;->a:Levi;

    invoke-direct {v0, v1}, Lgmf;-><init>(Levi;)V

    iput-object v0, p0, Lgml;->h:Lgmf;

    .line 111
    new-instance v1, Lgmo;

    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-direct {v1, p0, v0}, Lgmo;-><init>(Lgml;Ljava/util/concurrent/Executor;)V

    iput-object v1, p0, Lgml;->k:Lgmo;

    .line 112
    return-void
.end method

.method private a(Lgcd;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 416
    iget-object v0, p1, Lgcd;->b:Ljava/lang/String;

    .line 417
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v1

    invoke-virtual {v1, v0}, Lgnm;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lgml;->f:Lgnp;

    .line 418
    invoke-virtual {v1, v0}, Lgnp;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 419
    if-eqz p1, :cond_0

    .line 420
    invoke-direct {p0, p1}, Lgml;->b(Lgcd;)V

    .line 423
    :cond_0
    invoke-direct {p0, v0}, Lgml;->n(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 426
    invoke-direct {p0, v0}, Lgml;->m(Ljava/lang/String;)V

    .line 429
    :cond_1
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 431
    :cond_2
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/util/List;ILjava/util/HashSet;)V
    .locals 10

    .prologue
    .line 396
    monitor-enter p0

    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    .line 397
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 398
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 399
    iget-object v1, v0, Lgcd;->b:Ljava/lang/String;

    .line 400
    iget-object v4, p0, Lgml;->f:Lgnp;

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "playlist_id"

    invoke-virtual {v5, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "video_id"

    invoke-virtual {v5, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "index_in_playlist"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "saved_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v4, v4, Lgnp;->a:Levi;

    invoke-interface {v4}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v6, "playlist_video"

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 401
    iget-object v4, p0, Lgml;->f:Lgnp;

    iget-object v4, v4, Lgnp;->a:Levi;

    invoke-interface {v4}, Levi;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "videos"

    const-string v6, "id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-static {v4, v5, v6, v7}, Levj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    .line 402
    iget-object v1, p0, Lgml;->f:Lgnp;

    sget-object v4, Lglv;->c:Lglv;

    invoke-virtual {v1, v0, v4, p3}, Lgnp;->a(Lgcd;Lglv;I)V

    .line 409
    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {p4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 401
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 411
    :cond_2
    monitor-exit p0

    return-void

    .line 396
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lgcd;)V
    .locals 6

    .prologue
    .line 680
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lgcd;->B:Lgch;

    sget-object v1, Lgch;->d:Lgch;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 693
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 685
    :cond_1
    :try_start_1
    iget-object v0, p1, Lgcd;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lgml;->l(Ljava/lang/String;)Z

    .line 686
    iget-object v0, p0, Lgml;->f:Lgnp;

    iget-object v1, p1, Lgcd;->b:Ljava/lang/String;

    iget-object v0, v0, Lgnp;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "videos"

    const-string v3, "id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_2

    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Delete video affected "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rows"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 680
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 686
    :cond_2
    :try_start_2
    const-string v2, "playlist_video"

    const-string v3, "playlist_id IS NULL AND video_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 687
    iget-object v0, p0, Lgml;->d:Lgmm;

    iget-object v1, p1, Lgcd;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgmm;->a(Ljava/lang/String;)V

    .line 688
    iget-object v0, p0, Lgml;->f:Lgnp;

    iget-object v1, p1, Lgcd;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgnp;->k(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 691
    iget-object v0, p0, Lgml;->d:Lgmm;

    iget-object v1, p1, Lgcd;->o:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgmm;->c(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private l(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 733
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 735
    :try_start_0
    iget-object v2, p0, Lgml;->i:Lgnk;

    iget-object v2, v2, Lgnk;->a:Levi;

    invoke-interface {v2}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "subtitles"

    const-string v4, "video_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 739
    :goto_0
    return v0

    .line 737
    :catch_0
    move-exception v0

    .line 738
    const-string v2, "Error deleting subtitle tracks"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 739
    goto :goto_0
.end method

.method private m(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 777
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 780
    :try_start_0
    iget-object v0, p0, Lgml;->e:Lgnj;

    iget-object v0, v0, Lgnj;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "streams"

    const-string v2, "video_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 785
    iget-object v0, p0, Lgml;->d:Lgmm;

    invoke-interface {v0, p1}, Lgmm;->d(Ljava/lang/String;)V

    .line 786
    :goto_0
    return-void

    .line 781
    :catch_0
    move-exception v0

    .line 782
    :try_start_1
    const-string v1, "Error deleting streams"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 785
    iget-object v0, p0, Lgml;->d:Lgmm;

    invoke-interface {v0, p1}, Lgmm;->d(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgml;->d:Lgmm;

    invoke-interface {v1, p1}, Lgmm;->d(Ljava/lang/String;)V

    throw v0
.end method

.method private n(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 818
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 819
    iget-object v0, p0, Lgml;->g:Lgmh;

    invoke-virtual {v0, p1}, Lgmh;->b(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lfoy;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 917
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 918
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 920
    :try_start_0
    iget-object v1, p0, Lgml;->g:Lgmh;

    invoke-virtual {v1, p1, p2}, Lgmh;->a(Ljava/lang/String;Ljava/lang/String;)Lfoy;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 926
    :goto_0
    return-object v0

    .line 921
    :catch_0
    move-exception v1

    .line 922
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error loading ad [originalVideoId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 924
    :catch_1
    move-exception v1

    .line 925
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error loading ad [originalVideoId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lgcd;
    .locals 1

    .prologue
    .line 155
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 156
    iget-object v0, p0, Lgml;->f:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->d(Ljava/lang/String;)Lgcd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lgni;)Lgly;
    .locals 1

    .prologue
    .line 746
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 747
    iget-object v0, p0, Lgml;->e:Lgnj;

    invoke-virtual {v0, p1, p2}, Lgnj;->a(Ljava/lang/String;Lgni;)Lgly;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lgbu;I)Z
    .locals 4

    .prologue
    .line 297
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    :try_start_1
    iget-object v0, p0, Lgml;->f:Lgnp;

    invoke-static {p1}, Lgnp;->a(Lgbu;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "preferred_stream_quality"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, v0, Lgnp;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "playlists"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 301
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgnm;->a(Lgbu;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 302
    const/4 v0, 0x1

    .line 305
    :goto_0
    monitor-exit p0

    return v0

    .line 303
    :catch_0
    move-exception v0

    .line 304
    :try_start_2
    const-string v1, "Error inserting playlist"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 305
    const/4 v0, 0x0

    goto :goto_0

    .line 297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lgbu;Ljava/util/List;Ljava/util/List;I)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 330
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    iget-object v0, p0, Lgml;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 333
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 335
    :try_start_1
    iget-object v4, p1, Lgbu;->a:Ljava/lang/String;

    .line 336
    iget-object v0, p0, Lgml;->f:Lgnp;

    invoke-virtual {v0, v4}, Lgnp;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 339
    iget-object v5, p0, Lgml;->f:Lgnp;

    invoke-virtual {v5, v4}, Lgnp;->j(Ljava/lang/String;)V

    .line 341
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    iget-object v7, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v5, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 366
    :catch_0
    move-exception v0

    .line 367
    :try_start_2
    const-string v1, "Error syncing playlist"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 368
    :try_start_3
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v0, v2

    :goto_1
    monitor-exit p0

    return v0

    .line 341
    :cond_0
    :try_start_4
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 368
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 330
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 341
    :cond_1
    :try_start_6
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 342
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 343
    invoke-direct {p0, v0, p3}, Lgml;->a(Lgcd;Ljava/util/List;)V

    goto :goto_3

    .line 347
    :cond_2
    iget-object v0, p0, Lgml;->f:Lgnp;

    invoke-static {p1}, Lgnp;->a(Lgbu;)Landroid/content/ContentValues;

    move-result-object v5

    const-string v6, "preferred_stream_quality"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, v0, Lgnp;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v6, "playlists"

    const-string v7, "id = ?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p1, Lgbu;->a:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    const-wide/16 v8, 0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x32

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Update playlist affected "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " rows"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_3
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 349
    invoke-direct {p0, v4, p2, p4, v5}, Lgml;->a(Ljava/lang/String;Ljava/util/List;ILjava/util/HashSet;)V

    .line 352
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v6

    .line 353
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 354
    invoke-virtual {v6, v0}, Lgnm;->b(Ljava/lang/String;)V

    goto :goto_4

    .line 356
    :cond_4
    invoke-virtual {v6, v4}, Lgnm;->c(Ljava/lang/String;)V

    .line 357
    invoke-virtual {v6, p1}, Lgnm;->a(Lgbu;)V

    .line 358
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 359
    iget-object v7, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 360
    invoke-virtual {v6, v0}, Lgnm;->a(Lgcd;)V

    .line 362
    :cond_5
    iget-object v7, p1, Lgbu;->a:Ljava/lang/String;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, Lgnm;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 364
    :cond_6
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 365
    :try_start_7
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move v0, v1

    goto/16 :goto_1
.end method

.method public final declared-synchronized a(Lgcd;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 311
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    :try_start_1
    iget-object v0, p0, Lgml;->f:Lgnp;

    invoke-static {p1}, Lgnp;->a(Lgcd;)Landroid/content/ContentValues;

    move-result-object v3

    const-string v4, "player_response_proto"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v4, "refresh_token"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v4, "last_refresh_timestamp"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    iget-object v0, v0, Lgnp;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v4, "videos"

    const-string v5, "id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p1, Lgcd;->b:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v4, v0

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x2f

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Update video affected "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " rows"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 318
    :catch_0
    move-exception v0

    .line 319
    :try_start_2
    const-string v1, "Error updating single video"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v2

    .line 320
    :goto_0
    monitor-exit p0

    return v0

    .line 316
    :cond_0
    :try_start_3
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lgnm;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, p1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgno;

    invoke-virtual {v0, p1}, Lgno;->a(Lgcd;)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 317
    goto :goto_0

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lgcd;I)Z
    .locals 3

    .prologue
    .line 271
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    iget-object v0, p0, Lgml;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 273
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 275
    :try_start_1
    iget-object v0, p0, Lgml;->f:Lgnp;

    sget-object v2, Lglv;->c:Lglv;

    invoke-virtual {v0, p1, v2, p2}, Lgnp;->a(Lgcd;Lglv;I)V

    .line 279
    iget-object v0, p0, Lgml;->f:Lgnp;

    iget-object v2, p1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lgnp;->i(Ljava/lang/String;)V

    .line 282
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    .line 283
    invoke-virtual {v0, p1}, Lgnm;->a(Lgcd;)V

    .line 284
    iget-object v2, p1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lgnm;->d(Ljava/lang/String;)V

    .line 285
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v0, 0x1

    .line 289
    :goto_0
    monitor-exit p0

    return v0

    .line 287
    :catch_0
    move-exception v0

    .line 288
    :try_start_3
    const-string v2, "Error inserting single video"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 289
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 271
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lgpa;)Z
    .locals 4

    .prologue
    .line 721
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 723
    :try_start_0
    iget-object v0, p0, Lgml;->i:Lgnk;

    iget-object v0, v0, Lgnk;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "subtitles"

    const/4 v2, 0x0

    invoke-static {p1}, Lgnk;->a(Lgpa;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Error inserting subtitle track"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 725
    :catch_0
    move-exception v0

    .line 726
    const-string v1, "Error inserting subtitle tracks"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 727
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 724
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 791
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 793
    :try_start_0
    iget-object v2, p0, Lgml;->e:Lgnj;

    iget-object v2, v2, Lgnj;->a:Levi;

    invoke-interface {v2}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "streams"

    const-string v4, "video_id = ? AND itag = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    new-instance v0, Landroid/database/SQLException;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x30

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Delete stream affected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rows"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 795
    :catch_0
    move-exception v0

    .line 796
    const-string v2, "Error deleting stream"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 797
    :cond_0
    return v0
.end method

.method public final a(Ljava/lang/String;IJ)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 765
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 767
    :try_start_0
    iget-object v2, p0, Lgml;->e:Lgnj;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "bytes_transferred"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v2, v2, Lgnj;->a:Levi;

    invoke-interface {v2}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v4, "streams"

    const-string v5, "video_id = ? AND itag = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    new-instance v0, Landroid/database/SQLException;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x42

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Update stream bytes_transferred affected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rows"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 769
    :catch_0
    move-exception v0

    .line 770
    const-string v2, "Error updating stream progress"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 771
    :cond_0
    return v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;J)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 576
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 577
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v2

    invoke-virtual {v2, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 578
    if-eqz v2, :cond_0

    .line 580
    :try_start_1
    iget-object v3, p0, Lgml;->f:Lgnp;

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "last_playback_timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v3, v3, Lgnp;->a:Levi;

    invoke-interface {v3}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v5, "videos"

    const-string v6, "id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-virtual {v3, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    int-to-long v4, v3

    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    new-instance v0, Landroid/database/SQLException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x47

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Update video last_playback_timestamp affected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rows"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 584
    :catch_0
    move-exception v0

    .line 586
    :try_start_2
    const-string v2, "Error updating last playback timestamp"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    move v0, v1

    .line 589
    :goto_0
    monitor-exit p0

    return v0

    .line 582
    :cond_1
    :try_start_3
    invoke-virtual {v2, p2, p3}, Lgno;->a(J)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 576
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;JJ)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 554
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 555
    cmp-long v2, p2, v4

    if-ltz v2, :cond_0

    move v2, v0

    :goto_0
    invoke-static {v2}, Lb;->b(Z)V

    .line 556
    cmp-long v2, p4, v4

    if-lez v2, :cond_1

    move v2, v0

    :goto_1
    invoke-static {v2}, Lb;->b(Z)V

    .line 557
    cmp-long v2, p2, p4

    if-gtz v2, :cond_2

    move v2, v0

    :goto_2
    invoke-static {v2}, Lb;->b(Z)V

    .line 558
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v2

    invoke-virtual {v2, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 559
    if-eqz v2, :cond_3

    .line 562
    :try_start_1
    invoke-virtual {v2, p2, p3, p4, p5}, Lgno;->a(JJ)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 569
    :goto_3
    monitor-exit p0

    return v0

    :cond_0
    move v2, v1

    .line 555
    goto :goto_0

    :cond_1
    move v2, v1

    .line 556
    goto :goto_1

    :cond_2
    move v2, v1

    .line 557
    goto :goto_2

    .line 564
    :catch_0
    move-exception v0

    .line 566
    :try_start_2
    const-string v2, "Error updating media progress"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move v0, v1

    .line 569
    goto :goto_3

    .line 554
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Lesq;)Z
    .locals 8

    .prologue
    .line 828
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 829
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 830
    iget-object v0, p0, Lgml;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 831
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 833
    :try_start_0
    iget-object v0, p0, Lgml;->f:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 834
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    .line 838
    :goto_0
    return v0

    .line 836
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgml;->j:Lgme;

    iget-object v0, v0, Lgme;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "adbreaks"

    const/4 v3, 0x0

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "adbreaks"

    iget-object v6, p2, Lesq;->a:Ljava/util/List;

    invoke-static {v6}, Lgia;->a(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object v6

    invoke-virtual {v6}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x400

    invoke-static {v6, v7}, Lfaq;->a(Ljava/lang/String;I)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v5, "original_video_id"

    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 837
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 838
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Lfqj;Z)Z
    .locals 4

    .prologue
    .line 752
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 753
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 755
    :try_start_0
    iget-object v0, p0, Lgml;->e:Lgnj;

    new-instance v1, Lglx;

    invoke-direct {v1, p2, p3}, Lglx;-><init>(Lfqj;Z)V

    invoke-static {v1}, Lgnj;->a(Lglx;)Landroid/content/ContentValues;

    move-result-object v1

    iget-object v0, v0, Lgnj;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "streams"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 756
    const/4 v0, 0x1

    .line 759
    :goto_0
    return v0

    .line 757
    :catch_0
    move-exception v0

    .line 758
    const-string v1, "Error inserting stream"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 759
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lfrl;J)Z
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 457
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 459
    if-eqz v0, :cond_1

    .line 462
    :try_start_1
    new-instance v2, Lhro;

    invoke-direct {v2}, Lhro;-><init>()V

    iget-object v1, p2, Lfrl;->a:Lhro;

    invoke-static {v1}, Lidh;->a(Lidh;)[B

    move-result-object v1

    invoke-static {v2, v1}, Lidh;->a(Lidh;[B)Lidh;

    iget-object v1, v2, Lhro;->b:Lhwn;

    if-eqz v1, :cond_0

    sget-object v3, Lhgy;->a:[Lhgy;

    iput-object v3, v1, Lhwn;->c:[Lhgy;

    sget-object v3, Lhgy;->a:[Lhgy;

    iput-object v3, v1, Lhwn;->b:[Lhgy;

    :cond_0
    sget-object v1, Lhqx;->a:[Lhqx;

    iput-object v1, v2, Lhro;->d:[Lhqx;

    new-instance v1, Lfrl;

    iget-wide v4, p2, Lfrl;->b:J

    invoke-direct {v1, v2, v4, v5}, Lfrl;-><init>(Lhro;J)V

    .line 463
    iget-object v3, p0, Lgml;->f:Lgnp;

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "player_response_proto"

    iget-object v5, v1, Lfrl;->a:Lhro;

    invoke-static {v5}, Lidh;->a(Lidh;)[B

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    invoke-virtual {v1}, Lfrl;->q()Lflg;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, v2, Lflg;->c:Ljava/lang/String;

    :goto_0
    if-eqz v2, :cond_3

    const-string v5, "refresh_token"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string v2, "saved_timestamp"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "last_refresh_timestamp"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v2, v3, Lgnp;->a:Levi;

    invoke-interface {v2}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "videos"

    const-string v5, "id = ?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-virtual {v2, v3, v4, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_4

    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x45

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Update video player_response_proto affected "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rows"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lidg; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 468
    :catch_0
    move-exception v0

    .line 470
    :try_start_2
    const-string v1, "Error updating player response for offline"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_2
    move v0, v7

    .line 476
    :goto_3
    monitor-exit p0

    return v0

    .line 463
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    :try_start_3
    const-string v2, "refresh_token"

    invoke-virtual {v4, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_3
    .catch Lidg; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 471
    :catch_1
    move-exception v0

    .line 473
    :try_start_4
    const-string v1, "Error inserting player response"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 457
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move-wide v2, p3

    move-wide v4, p3

    .line 465
    :try_start_5
    invoke-virtual/range {v0 .. v5}, Lgno;->a(Lfrl;JJ)V

    .line 466
    iget-object v0, p0, Lgml;->d:Lgmm;

    invoke-interface {v0, v1}, Lgmm;->a(Lfrl;)V
    :try_end_5
    .catch Lidg; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v6

    .line 467
    goto :goto_3
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lgjm;)Z
    .locals 4

    .prologue
    .line 510
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 511
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;

    move-result-object v1

    .line 513
    if-eqz v1, :cond_3

    .line 514
    const/4 v0, 0x0

    .line 515
    invoke-virtual {p2}, Lgjm;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 516
    sget-object v0, Lglv;->c:Lglv;

    .line 520
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 521
    invoke-virtual {p0, p1, v0}, Lgml;->a(Ljava/lang/String;Lglv;)Z

    .line 524
    :cond_1
    invoke-virtual {v1, p2}, Lgno;->a(Lgjm;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    const/4 v0, 0x1

    .line 527
    :goto_1
    monitor-exit p0

    return v0

    .line 517
    :cond_2
    :try_start_1
    iget-object v2, p2, Lgjm;->c:Lgjn;

    sget-object v3, Lgjn;->c:Lgjn;

    if-ne v2, v3, :cond_0

    .line 518
    sget-object v0, Lglv;->b:Lglv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 527
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 510
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lglv;)Z
    .locals 2

    .prologue
    .line 532
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 533
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;

    move-result-object v0

    .line 535
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgno;->b()Lglv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eq v1, p2, :cond_0

    .line 537
    :try_start_1
    iget-object v1, p0, Lgml;->f:Lgnp;

    invoke-virtual {v1, p1, p2}, Lgnp;->a(Ljava/lang/String;Lglv;)V

    .line 539
    invoke-virtual {v0, p2}, Lgno;->a(Lglv;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 540
    const/4 v0, 0x1

    .line 546
    :goto_0
    monitor-exit p0

    return v0

    .line 541
    :catch_0
    move-exception v0

    .line 543
    :try_start_2
    const-string v1, "Error updating media status"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 546
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 532
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lglv;I)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 698
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 699
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    .line 701
    invoke-virtual {v0, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;

    move-result-object v1

    if-nez v1, :cond_0

    .line 702
    invoke-virtual {p0, p1}, Lgml;->a(Ljava/lang/String;)Lgcd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 703
    if-eqz v1, :cond_0

    .line 705
    :try_start_1
    iget-object v2, p0, Lgml;->f:Lgnp;

    invoke-virtual {v2, p1, p2}, Lgnp;->a(Ljava/lang/String;Lglv;)V

    .line 706
    iget-object v2, p0, Lgml;->f:Lgnp;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "preferred_stream_quality"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v2, v2, Lgnp;->a:Levi;

    invoke-interface {v2}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v4, "videos"

    const-string v5, "id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-virtual {v2, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x48

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Update video preferred_stream_quality affected "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rows"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 710
    :catch_0
    move-exception v0

    .line 712
    :try_start_2
    const-string v1, "Error undeleting video"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 716
    :cond_0
    :goto_0
    monitor-exit p0

    return v8

    .line 708
    :cond_1
    :try_start_3
    invoke-virtual {v0, v1}, Lgnm;->a(Lgcd;)V

    .line 709
    invoke-virtual {v0, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;

    move-result-object v0

    invoke-virtual {v0, p2}, Lgno;->a(Lglv;)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 698
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lfoy;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 867
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 868
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 870
    iget-object v1, p0, Lgml;->a:Levi;

    invoke-interface {v1}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 871
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 873
    :try_start_0
    iget-object v2, p0, Lgml;->f:Lgnp;

    invoke-virtual {v2, p1}, Lgnp;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgml;->j:Lgme;

    .line 874
    invoke-virtual {v2, p1}, Lgme;->b(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 875
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 882
    :goto_0
    return v0

    .line 877
    :cond_1
    :try_start_1
    iget-object v0, p0, Lgml;->g:Lgmh;

    iget-object v0, v0, Lgmh;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "ads"

    const/4 v3, 0x0

    invoke-static {p3}, Lgmh;->a(Lfoy;)Landroid/content/ContentValues;

    move-result-object v4

    const-string v5, "original_video_id"

    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "ad_break_id"

    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 878
    iget-object v0, p3, Lfoy;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 879
    iget-object v0, p3, Lfoy;->c:Ljava/lang/String;

    iget-object v2, p0, Lgml;->h:Lgmf;

    invoke-virtual {v2, v0}, Lgmf;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lgml;->h:Lgmf;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "ad_video_id"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "playback_count"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "status"

    sget-object v4, Lglv;->c:Lglv;

    iget v4, v4, Lglv;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, v2, Lgmf;->b:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "ad_videos"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 881
    :cond_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 882
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 636
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 637
    iget-object v0, p0, Lgml;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 638
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 640
    :try_start_1
    iget-object v0, p0, Lgml;->f:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->f(Ljava/lang/String;)Lgbu;

    move-result-object v0

    .line 641
    iget-object v4, p0, Lgml;->f:Lgnp;

    invoke-virtual {v4, p1}, Lgnp;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 644
    iget-object v5, p0, Lgml;->f:Lgnp;

    iget-object v5, v5, Lgnp;->a:Levi;

    invoke-interface {v5}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "playlists"

    const-string v7, "id = ?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    int-to-long v6, v6

    const-wide/16 v8, 0x1

    cmp-long v8, v6, v8

    if-eqz v8, :cond_0

    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x32

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Delete playlist affected "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " rows"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 671
    :catch_0
    move-exception v0

    .line 672
    :try_start_2
    const-string v1, "Error deleting playlist"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 673
    :try_start_3
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v0, v2

    :goto_0
    monitor-exit p0

    return v0

    .line 644
    :cond_0
    :try_start_4
    const-string v6, "playlist_video"

    const-string v7, "playlist_id = ?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 645
    iget-object v5, p0, Lgml;->f:Lgnp;

    invoke-virtual {v5, p1}, Lgnp;->j(Ljava/lang/String;)V

    .line 646
    iget-object v5, p0, Lgml;->d:Lgmm;

    invoke-interface {v5, p1}, Lgmm;->b(Ljava/lang/String;)V

    .line 647
    iget-object v5, p0, Lgml;->f:Lgnp;

    iget-object v6, v0, Lgbu;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lgnp;->k(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 650
    iget-object v5, p0, Lgml;->d:Lgmm;

    iget-object v0, v0, Lgbu;->d:Ljava/lang/String;

    invoke-interface {v5, v0}, Lgmm;->c(Ljava/lang/String;)V

    .line 655
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 656
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 657
    invoke-direct {p0, v0, p2}, Lgml;->a(Lgcd;Ljava/util/List;)V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 673
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 636
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 661
    :cond_2
    :try_start_6
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v4

    .line 662
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 663
    invoke-virtual {v4, v0}, Lgnm;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 665
    :cond_3
    invoke-virtual {v4}, Lgnm;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 666
    iget-object v0, p0, Lgml;->d:Lgmm;

    invoke-interface {v0}, Lgmm;->a()V

    .line 668
    :cond_4
    invoke-virtual {v4, p1}, Lgnm;->c(Ljava/lang/String;)V

    .line 669
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 670
    :try_start_7
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Z)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 594
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 595
    iget-object v2, p0, Lgml;->a:Levi;

    invoke-interface {v2}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 596
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 598
    :try_start_1
    iget-object v3, p0, Lgml;->f:Lgnp;

    invoke-virtual {v3, p1}, Lgnp;->d(Ljava/lang/String;)Lgcd;

    move-result-object v3

    .line 599
    if-eqz v3, :cond_1

    .line 604
    if-nez p2, :cond_0

    iget-object v4, p0, Lgml;->f:Lgnp;

    invoke-virtual {v4, p1}, Lgnp;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 605
    iget-object v3, p0, Lgml;->f:Lgnp;

    iget-object v4, v3, Lgnp;->a:Levi;

    invoke-interface {v4}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "playlist_video"

    const-string v6, "playlist_id IS NULL AND video_id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "media_status"

    sget-object v6, Lglv;->a:Lglv;

    iget v6, v6, Lglv;->j:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "player_response_proto"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v5, "refresh_token"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v5, "saved_timestamp"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v5, "last_refresh_timestamp"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v5, "last_playback_timestamp"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    iget-object v3, v3, Lgnp;->a:Levi;

    invoke-interface {v3}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v5, "videos"

    const-string v6, "id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-virtual {v3, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    int-to-long v4, v3

    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    new-instance v0, Landroid/database/SQLException;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v6, 0x49

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Update video offline_playability_state affected "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rows"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 626
    :catch_0
    move-exception v0

    .line 627
    :try_start_2
    const-string v3, "Error deleting video"

    invoke-static {v3, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 628
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    :goto_0
    monitor-exit p0

    return v0

    .line 607
    :cond_0
    :try_start_4
    invoke-direct {p0, v3}, Lgml;->b(Lgcd;)V

    .line 614
    :cond_1
    invoke-direct {p0, p1}, Lgml;->n(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 615
    invoke-direct {p0, p1}, Lgml;->m(Ljava/lang/String;)V

    .line 619
    :cond_2
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v3

    .line 620
    invoke-virtual {v3, p1}, Lgnm;->b(Ljava/lang/String;)V

    .line 621
    invoke-virtual {v3}, Lgnm;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 622
    iget-object v3, p0, Lgml;->d:Lgmm;

    invoke-interface {v3}, Lgmm;->a()V

    .line 624
    :cond_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 625
    :try_start_5
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 594
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 628
    :catchall_1
    move-exception v0

    :try_start_6
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 183
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 184
    iget-object v0, p0, Lgml;->f:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final b()Lgnm;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lgml;->k:Lgmo;

    iget-boolean v1, v0, Lgmo;->c:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lgmo;->a()V

    :cond_0
    iget-object v0, v0, Lgmo;->b:Lgnm;

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lglv;)V
    .locals 5

    .prologue
    .line 1011
    iget-object v0, p0, Lgml;->h:Lgmf;

    invoke-virtual {v0, p1}, Lgmf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1012
    iget-object v0, p0, Lgml;->h:Lgmf;

    iget-object v0, v0, Lgmf;->b:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "UPDATE ad_videos SET status = ? WHERE ad_video_id = ?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p2, Lglv;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1014
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 941
    iget-object v0, p0, Lgml;->g:Lgmh;

    iget-object v0, v0, Lgmh;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "UPDATE ads SET vast_playback_count = vast_playback_count + 1 WHERE original_video_id = ? AND ad_break_id = ?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 942
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lfoy;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 895
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 896
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 897
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 898
    iget-object v2, p0, Lgml;->a:Levi;

    invoke-interface {v2}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 899
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 901
    :try_start_0
    iget-object v3, p0, Lgml;->f:Lgnp;

    invoke-virtual {v3, p1}, Lgnp;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lgml;->j:Lgme;

    .line 902
    invoke-virtual {v3, p1}, Lgme;->b(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 903
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v0, v1

    .line 907
    :goto_0
    return v0

    .line 905
    :cond_1
    :try_start_1
    iget-object v1, p0, Lgml;->g:Lgmh;

    iget-object v1, v1, Lgmh;->a:Levi;

    invoke-interface {v1}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v3, "ads"

    invoke-static {p3}, Lgmh;->a(Lfoy;)Landroid/content/ContentValues;

    move-result-object v4

    const-string v5, "original_video_id=? AND ad_break_id=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 906
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 907
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final c(Ljava/lang/String;)Lfrl;
    .locals 1

    .prologue
    .line 195
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 196
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {v0}, Lgno;->a()Lfrl;

    move-result-object v0

    .line 200
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Lgmb;
    .locals 1

    .prologue
    .line 205
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 206
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;

    move-result-object v0

    .line 207
    if-eqz v0, :cond_0

    .line 208
    invoke-virtual {v0}, Lgno;->e()Lgmb;

    move-result-object v0

    .line 210
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Lgbu;
    .locals 1

    .prologue
    .line 220
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 221
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, v0, Lgnm;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnn;

    .line 222
    if-eqz v0, :cond_0

    .line 223
    iget-object v0, v0, Lgnn;->a:Lgbu;

    .line 225
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 265
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 266
    iget-object v0, p0, Lgml;->i:Lgnk;

    invoke-virtual {v0, p1}, Lgnk;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lgml;->k:Lgmo;

    iget-object v1, v0, Lgmo;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lgmq;

    invoke-direct {v2, v0}, Lgmq;-><init>(Lgmo;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 143
    return-void
.end method

.method public final g()Ljava/util/List;
    .locals 1

    .prologue
    .line 215
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0}, Lgnm;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized g(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 435
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 436
    invoke-virtual {p0}, Lgml;->b()Lgnm;

    move-result-object v0

    .line 437
    invoke-virtual {v0, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;

    move-result-object v1

    .line 438
    if-eqz v1, :cond_0

    .line 439
    invoke-virtual {v0, p1}, Lgnm;->e(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 440
    invoke-virtual {v1}, Lgno;->b()Lglv;

    move-result-object v1

    sget-object v2, Lglv;->a:Lglv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v1, v2, :cond_0

    .line 442
    :try_start_1
    iget-object v1, p0, Lgml;->f:Lgnp;

    invoke-virtual {v1, p1}, Lgnp;->i(Ljava/lang/String;)V

    .line 444
    invoke-virtual {v0, p1}, Lgnm;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445
    const/4 v0, 0x1

    .line 451
    :goto_0
    monitor-exit p0

    return v0

    .line 446
    :catch_0
    move-exception v0

    .line 448
    :try_start_2
    const-string v1, "Error inserting existing video as single video"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 451
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 435
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h(Ljava/lang/String;)Lesq;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 848
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 850
    :try_start_0
    iget-object v1, p0, Lgml;->j:Lgme;

    invoke-virtual {v1, p1}, Lgme;->a(Ljava/lang/String;)Lesq;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 856
    :goto_0
    return-object v0

    .line 851
    :catch_0
    move-exception v1

    .line 852
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error loading ad breaks [originalVideoId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 854
    :catch_1
    move-exception v1

    .line 855
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error loading ad breaks [originalVideoId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 981
    iget-object v0, p0, Lgml;->h:Lgmf;

    iget-object v0, v0, Lgmf;->b:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "UPDATE ad_videos SET playback_count = playback_count + 1 WHERE ad_video_id = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 982
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 988
    iget-object v0, p0, Lgml;->h:Lgmf;

    iget-object v0, v0, Lgmf;->b:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "UPDATE ad_videos SET playback_count = 0 WHERE ad_video_id = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 989
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1095
    iget-object v0, p0, Lgml;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1096
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1098
    :try_start_0
    iget-object v0, p0, Lgml;->g:Lgmh;

    invoke-virtual {v0, p1}, Lgmh;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1099
    iget-object v3, p0, Lgml;->g:Lgmh;

    invoke-virtual {v3, v0}, Lgmh;->b(Ljava/lang/String;)I

    move-result v3

    if-gt v3, v8, :cond_0

    .line 1102
    iget-object v3, p0, Lgml;->h:Lgmf;

    iget-object v3, v3, Lgmf;->b:Levi;

    invoke-interface {v3}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "ad_videos"

    const-string v5, "ad_video_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1105
    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    iget-object v3, p0, Lgml;->f:Lgnp;

    invoke-virtual {v3, v0}, Lgnp;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1108
    invoke-direct {p0, v0}, Lgml;->m(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1115
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 1111
    :cond_1
    :try_start_1
    iget-object v0, p0, Lgml;->g:Lgmh;

    iget-object v0, v0, Lgmh;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "ads"

    const-string v3, "original_video_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1112
    iget-object v0, p0, Lgml;->j:Lgme;

    iget-object v0, v0, Lgme;->a:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "adbreaks"

    const-string v3, "original_video_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1113
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1115
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1116
    return-void
.end method
