.class public final enum Ldqe;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldqe;

.field public static final enum b:Ldqe;

.field public static final enum c:Ldqe;

.field private static final synthetic d:[Ldqe;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 187
    new-instance v0, Ldqe;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v2}, Ldqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqe;->a:Ldqe;

    new-instance v0, Ldqe;

    const-string v1, "DATA"

    invoke-direct {v0, v1, v3}, Ldqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqe;->b:Ldqe;

    new-instance v0, Ldqe;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, Ldqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqe;->c:Ldqe;

    .line 186
    const/4 v0, 0x3

    new-array v0, v0, [Ldqe;

    sget-object v1, Ldqe;->a:Ldqe;

    aput-object v1, v0, v2

    sget-object v1, Ldqe;->b:Ldqe;

    aput-object v1, v0, v3

    sget-object v1, Ldqe;->c:Ldqe;

    aput-object v1, v0, v4

    sput-object v0, Ldqe;->d:[Ldqe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldqe;
    .locals 1

    .prologue
    .line 186
    const-class v0, Ldqe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldqe;

    return-object v0
.end method

.method public static values()[Ldqe;
    .locals 1

    .prologue
    .line 186
    sget-object v0, Ldqe;->d:[Ldqe;

    invoke-virtual {v0}, [Ldqe;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldqe;

    return-object v0
.end method
