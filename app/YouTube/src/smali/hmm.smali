.class public final Lhmm;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:[Lhml;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput v1, p0, Lhmm;->a:I

    sget-object v0, Lhml;->a:[Lhml;

    iput-object v0, p0, Lhmm;->b:[Lhml;

    iput-boolean v1, p0, Lhmm;->c:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget v0, p0, Lhmm;->a:I

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iget v2, p0, Lhmm;->a:I

    invoke-static {v0, v2}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhmm;->b:[Lhml;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhmm;->b:[Lhml;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    const/4 v5, 0x2

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-boolean v1, p0, Lhmm;->c:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Lhmm;->c:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhmm;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhmm;->J:I

    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhmm;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhmm;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhmm;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhmm;->a:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhmm;->b:[Lhml;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhml;

    iget-object v3, p0, Lhmm;->b:[Lhml;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhmm;->b:[Lhml;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhmm;->b:[Lhml;

    :goto_2
    iget-object v2, p0, Lhmm;->b:[Lhml;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhmm;->b:[Lhml;

    new-instance v3, Lhml;

    invoke-direct {v3}, Lhml;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhmm;->b:[Lhml;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhmm;->b:[Lhml;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhmm;->b:[Lhml;

    new-instance v3, Lhml;

    invoke-direct {v3}, Lhml;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhmm;->b:[Lhml;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhmm;->c:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget v0, p0, Lhmm;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lhmm;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_0
    iget-object v0, p0, Lhmm;->b:[Lhml;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lhmm;->b:[Lhml;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lhmm;->c:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    iget-boolean v1, p0, Lhmm;->c:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_3
    iget-object v0, p0, Lhmm;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhmm;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhmm;

    iget v2, p0, Lhmm;->a:I

    iget v3, p1, Lhmm;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhmm;->b:[Lhml;

    iget-object v3, p1, Lhmm;->b:[Lhml;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lhmm;->c:Z

    iget-boolean v3, p1, Lhmm;->c:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhmm;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhmm;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhmm;->I:Ljava/util/List;

    iget-object v3, p1, Lhmm;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhmm;->a:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lhmm;->b:[Lhml;

    if-nez v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-boolean v0, p0, Lhmm;->c:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhmm;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Lhmm;->b:[Lhml;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhmm;->b:[Lhml;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    move v2, v1

    :goto_3
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lhmm;->b:[Lhml;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhml;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lhmm;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
