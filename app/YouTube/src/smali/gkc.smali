.class public Lgkc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lgix;


# direct methods
.method public constructor <init>(Lgix;)V
    .locals 1

    .prologue
    .line 1066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lgkc;->a:Lgix;

    .line 1068
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Exception;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1072
    instance-of v0, p1, Lfxw;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1073
    check-cast v0, Lfxw;

    .line 1074
    invoke-virtual {v0}, Lfxw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1075
    const-string v0, "operation needs a full YouTube account"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    move v0, v1

    .line 1083
    :goto_0
    return v0

    .line 1079
    :cond_0
    iget-object v0, p0, Lgkc;->a:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1080
    goto :goto_0

    .line 1082
    :cond_1
    instance-of v0, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_2

    check-cast p1, Lorg/apache/http/client/HttpResponseException;

    .line 1083
    invoke-virtual {p1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v2, 0x191

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 1063
    invoke-virtual {p0, p2}, Lgkc;->a(Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method
