.class public final Lfpl;
.super Lgia;
.source "SourceFile"


# instance fields
.field private a:Lfpi;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lgia;-><init>()V

    return-void
.end method

.method public constructor <init>(Lfpi;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lgia;-><init>()V

    .line 89
    iput-object p1, p0, Lfpl;->a:Lfpi;

    .line 90
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method protected final synthetic a(Lorg/json/JSONObject;I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 77
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lfpi;

    const-string v1, "type"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Lfpm;->e:Lfpp;

    const-string v3, "actions"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfpp;->a(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lfpu;->c:Lfpw;

    const-string v4, "events"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    invoke-virtual {v3, v4}, Lfpw;->a(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v3

    sget-object v4, Lfpq;->i:Lfpt;

    const-string v5, "app"

    invoke-virtual {v4, p1, v5}, Lfpt;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lfpq;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lfpi;-><init>(ILjava/util/List;Ljava/util/List;Lfpq;B)V

    return-object v0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 99
    const-string v0, "type"

    iget-object v1, p0, Lfpl;->a:Lfpi;

    iget v1, v1, Lfpi;->a:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 100
    const-string v0, "actions"

    iget-object v1, p0, Lfpl;->a:Lfpi;

    iget-object v1, v1, Lfpi;->b:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpl;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 101
    const-string v0, "events"

    iget-object v1, p0, Lfpl;->a:Lfpi;

    iget-object v1, v1, Lfpi;->c:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpl;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 102
    const-string v0, "app"

    iget-object v1, p0, Lfpl;->a:Lfpi;

    iget-object v1, v1, Lfpi;->d:Lfpq;

    invoke-static {p1, v0, v1}, Lfpl;->a(Lorg/json/JSONObject;Ljava/lang/String;Lghz;)V

    .line 103
    return-void
.end method
