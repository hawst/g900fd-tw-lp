.class public abstract Lidh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public J:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lidh;->J:I

    return-void
.end method

.method public static final a(Lidh;[B)Lidh;
    .locals 2

    .prologue
    .line 118
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, Lidh;->b(Lidh;[BII)Lidh;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lidh;[BII)V
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 103
    :try_start_0
    new-instance v1, Lidd;

    invoke-direct {v1, p1, v0, p3}, Lidd;-><init>([BII)V

    .line 104
    invoke-virtual {p0, v1}, Lidh;->a(Lidd;)V

    .line 105
    iget v0, v1, Lidd;->a:I

    iget v1, v1, Lidd;->b:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 109
    :cond_0
    return-void
.end method

.method public static final a(Lidh;Lidh;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 146
    if-ne p0, p1, :cond_1

    .line 147
    const/4 v0, 0x1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 149
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 152
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 155
    invoke-virtual {p0}, Lidh;->a()I

    move-result v1

    .line 156
    invoke-virtual {p1}, Lidh;->a()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 159
    new-array v2, v1, [B

    .line 160
    new-array v3, v1, [B

    .line 161
    invoke-static {p0, v2, v0, v1}, Lidh;->a(Lidh;[BII)V

    .line 162
    invoke-static {p1, v3, v0, v1}, Lidh;->a(Lidh;[BII)V

    .line 163
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public static final a(Lidh;)[B
    .locals 3

    .prologue
    .line 86
    invoke-virtual {p0}, Lidh;->a()I

    move-result v0

    new-array v0, v0, [B

    .line 87
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p0, v0, v1, v2}, Lidh;->a(Lidh;[BII)V

    .line 88
    return-object v0
.end method

.method private static b(Lidh;[BII)Lidh;
    .locals 2

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 129
    :try_start_0
    new-instance v1, Lidc;

    invoke-direct {v1, p1, v0, p3}, Lidc;-><init>([BII)V

    .line 130
    invoke-virtual {p0, v1}, Lidh;->a(Lidc;)Lidh;

    .line 131
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lidc;->a(I)V
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 132
    return-object p0

    .line 133
    :catch_0
    move-exception v0

    .line 134
    throw v0

    .line 136
    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    iput v0, p0, Lidh;->J:I

    .line 66
    return v0
.end method

.method public abstract a(Lidc;)Lidh;
.end method

.method public abstract a(Lidd;)V
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    invoke-static {p0}, La;->b(Lidh;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
