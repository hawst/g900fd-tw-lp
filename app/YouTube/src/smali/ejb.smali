.class public Lejb;
.super Ljava/lang/Object;

# interfaces
.implements Lejc;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Lu;

.field public b:Leps;

.field public c:Z


# direct methods
.method public constructor <init>(Lu;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lejb;->a:Lu;

    const/4 v0, 0x0

    iput-object v0, p0, Lejb;->b:Leps;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lejb;->c:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lejb;->b:Leps;

    invoke-virtual {v0, v1}, Leps;->a(Z)V

    iget-boolean v0, p0, Lejb;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejb;->a:Lu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejb;->a:Lu;

    :cond_0
    iput-boolean v1, p0, Lejb;->c:Z

    return-void
.end method

.method public a(Leiz;)V
    .locals 2

    iget-object v0, p0, Lejb;->b:Leps;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leps;->a(Z)V

    iget-boolean v0, p0, Lejb;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejb;->a:Lu;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Leiz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lejb;->a:Lu;

    iget-object v0, p1, Leiz;->b:Landroid/app/PendingIntent;

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lejb;->c:Z

    return-void

    :cond_1
    iget-object v0, p0, Lejb;->a:Lu;

    goto :goto_0
.end method

.method public a(Leps;)V
    .locals 0

    iput-object p1, p0, Lejb;->b:Leps;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lejb;->c:Z

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lejb;->b:Leps;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leps;->a(Z)V

    return-void
.end method
