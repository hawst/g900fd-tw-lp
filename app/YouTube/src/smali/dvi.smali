.class final Ldvi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field private final a:Ldwf;

.field private synthetic b:Ldvb;


# direct methods
.method public constructor <init>(Ldvb;Ldwf;)V
    .locals 0

    .prologue
    .line 697
    iput-object p1, p0, Ldvi;->b:Ldvb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 698
    iput-object p2, p0, Ldvi;->a:Ldwf;

    .line 699
    return-void
.end method


# virtual methods
.method public final a(Ldsp;)V
    .locals 5

    .prologue
    .line 703
    iget v0, p1, Ldsp;->c:I

    .line 704
    iget-object v1, p0, Ldvi;->b:Ldvb;

    iget-object v1, v1, Ldvb;->l:Ldyg;

    iget-object v2, p0, Ldvi;->a:Ldwf;

    iget-object v2, v2, Ldwf;->a:Ldtm;

    iget-object v2, v2, Ldtm;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "App status for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldyg;->a(Ljava/lang/String;)V

    .line 705
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 706
    iget-object v0, p0, Ldvi;->b:Ldvb;

    iget-object v1, p0, Ldvi;->a:Ldwf;

    invoke-virtual {v0, v1}, Ldvb;->a(Ldwf;)V

    .line 708
    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 694
    iget-object v0, p0, Ldvi;->a:Ldwf;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Error on retrieving app status for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Ldvi;->b:Ldvb;

    iget-object v1, p0, Ldvi;->a:Ldwf;

    invoke-virtual {v0, v1}, Ldvb;->a(Ldwf;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 694
    check-cast p2, Ldsp;

    invoke-virtual {p0, p2}, Ldvi;->a(Ldsp;)V

    return-void
.end method
