.class public Lfrj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lgcu;

.field public final b:Lggn;


# direct methods
.method public constructor <init>(Lgcu;Lggn;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcu;

    iput-object v0, p0, Lfrj;->a:Lgcu;

    .line 41
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggn;

    iput-object v0, p0, Lfrj;->b:Lggn;

    .line 42
    return-void
.end method

.method public static a(Lede;Ledm;)Lhgy;
    .locals 12

    .prologue
    const-wide/16 v10, 0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 129
    new-instance v4, Lhgy;

    invoke-direct {v4}, Lhgy;-><init>()V

    .line 130
    iget-object v0, p1, Ledm;->b:Lecw;

    iget-object v0, v0, Lecw;->a:Ljava/lang/String;

    invoke-static {v0}, Lfrk;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, v4, Lhgy;->b:I

    .line 131
    iget-object v0, p1, Ledm;->b:Lecw;

    iget-object v0, v0, Lecw;->a:Ljava/lang/String;

    invoke-static {v0}, Lfrk;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lhgy;->q:Ljava/lang/String;

    .line 132
    iget-object v0, p1, Ledm;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lhgy;->c:Ljava/lang/String;

    .line 133
    iget-object v0, p1, Ledm;->b:Lecw;

    iget-object v0, v0, Lecw;->b:Ljava/lang/String;

    iput-object v0, v4, Lhgy;->d:Ljava/lang/String;

    .line 134
    iget-object v0, p1, Ledm;->b:Lecw;

    iget v0, v0, Lecw;->e:I

    iput v0, v4, Lhgy;->e:I

    .line 135
    iget-object v0, p1, Ledm;->b:Lecw;

    iget v0, v0, Lecw;->c:I

    iput v0, v4, Lhgy;->f:I

    .line 136
    iget-object v0, p1, Ledm;->b:Lecw;

    iget v0, v0, Lecw;->d:I

    iput v0, v4, Lhgy;->g:I

    .line 138
    iget-object v0, p1, Ledk;->d:Ledj;

    .line 139
    new-instance v1, Lhtk;

    invoke-direct {v1}, Lhtk;-><init>()V

    iput-object v1, v4, Lhgy;->h:Lhtk;

    .line 140
    iget-object v1, v4, Lhgy;->h:Lhtk;

    iget-wide v6, v0, Ledj;->a:J

    iput-wide v6, v1, Lhtk;->a:J

    .line 141
    iget-object v1, v4, Lhgy;->h:Lhtk;

    iget-wide v6, v0, Ledj;->a:J

    iget-wide v8, v0, Ledj;->b:J

    add-long/2addr v6, v8

    sub-long/2addr v6, v10

    iput-wide v6, v1, Lhtk;->b:J

    .line 143
    iget-object v0, p1, Ledm;->g:Ledj;

    .line 144
    new-instance v1, Lhtk;

    invoke-direct {v1}, Lhtk;-><init>()V

    iput-object v1, v4, Lhgy;->i:Lhtk;

    .line 145
    iget-object v1, v4, Lhgy;->i:Lhtk;

    iget-wide v6, v0, Ledj;->a:J

    iput-wide v6, v1, Lhtk;->a:J

    .line 146
    iget-object v1, v4, Lhgy;->i:Lhtk;

    iget-wide v6, v0, Ledj;->a:J

    iget-wide v8, v0, Ledj;->b:J

    add-long/2addr v6, v8

    sub-long/2addr v6, v10

    iput-wide v6, v1, Lhtk;->b:J

    .line 148
    iget-wide v0, p1, Ledm;->f:J

    iput-wide v0, v4, Lhgy;->k:J

    .line 149
    iget-wide v0, p1, Ledm;->a:J

    const-wide/16 v6, -0x1

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    iget-wide v0, p1, Ledm;->a:J

    :goto_0
    iput-wide v0, v4, Lhgy;->j:J

    .line 152
    invoke-virtual {p0}, Lede;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    const/4 v0, 0x2

    iput v0, v4, Lhgy;->n:I

    .line 154
    iget-object v0, p0, Lede;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledf;

    .line 155
    instance-of v5, v0, Lgcy;

    if-eqz v5, :cond_0

    .line 156
    check-cast v0, Lgcy;

    iget-object v5, v0, Lgcy;->a:Landroid/util/SparseArray;

    move v0, v2

    .line 159
    :goto_1
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 160
    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 161
    iget-object v7, v4, Lhgy;->r:[I

    new-array v8, v3, [I

    aput v6, v8, v2

    .line 162
    invoke-static {v7, v8}, La;->a([I[I)[I

    move-result-object v6

    iput-object v6, v4, Lhgy;->r:[I

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 149
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 166
    :cond_2
    iput v2, v4, Lhgy;->n:I

    .line 169
    :cond_3
    instance-of v0, p0, Lgcx;

    if-eqz v0, :cond_5

    move-object v0, p0

    .line 170
    check-cast v0, Lgcx;

    .line 171
    iget v1, p0, Lede;->a:I

    if-ne v1, v3, :cond_4

    const-string v1, "main"

    iget-object v5, v0, Lgcx;->e:Ljava/lang/String;

    .line 172
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v2, v3

    :cond_4
    iput-boolean v2, v4, Lhgy;->p:Z

    .line 173
    iget-object v1, v0, Lgcx;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 174
    iget-object v0, v0, Lgcx;->d:Ljava/lang/String;

    iput-object v0, v4, Lhgy;->o:Ljava/lang/String;

    .line 178
    :cond_5
    return-object v4
.end method

.method public static a(Ledg;)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 94
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 95
    iget-object v0, p0, Ledg;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledi;

    iget-object v0, v0, Ledi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lede;

    .line 96
    invoke-virtual {v0}, Lede;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    iget-object v0, v0, Lede;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledf;

    .line 98
    instance-of v1, v0, Lgcy;

    if-eqz v1, :cond_1

    .line 99
    check-cast v0, Lgcy;

    iget-object v6, v0, Lgcy;->a:Landroid/util/SparseArray;

    move v1, v2

    .line 102
    :goto_0
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 103
    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 104
    new-instance v7, Lhli;

    invoke-direct {v7}, Lhli;-><init>()V

    .line 105
    iput v0, v7, Lhli;->b:I

    .line 106
    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v7, Lhli;->c:Ljava/lang/String;

    .line 107
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 112
    :cond_2
    return-object v3
.end method


# virtual methods
.method public a(Lfrf;)Lfrf;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    invoke-virtual {p1}, Lfrf;->b()Landroid/net/Uri;

    move-result-object v3

    .line 47
    if-eqz v3, :cond_2

    iget-boolean v0, p1, Lfrf;->h:Z

    if-nez v0, :cond_2

    .line 48
    iget-object v0, p1, Lfrf;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrj;->b:Lggn;

    .line 49
    invoke-virtual {v0}, Lggn;->b()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v0, v0, Lggn;->a:Landroid/content/SharedPreferences;

    const-string v4, "medialib_diagnostic_always_fetch_dash_manifest"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 52
    :cond_0
    :try_start_0
    iget-object v0, p0, Lfrj;->a:Lgcu;

    .line 53
    iget-object v4, p1, Lfrf;->e:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lgcu;->a(Landroid/net/Uri;Ljava/lang/String;)Ledg;
    :try_end_0
    .catch Lxa; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 57
    if-eqz v3, :cond_1

    iget-object v0, v3, Ledg;->a:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, v3, Ledg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    move v0, v2

    :goto_1
    if-nez v0, :cond_7

    .line 63
    :cond_2
    :goto_2
    return-object p1

    :cond_3
    move v0, v2

    .line 49
    goto :goto_0

    .line 57
    :cond_4
    iget-object v0, v3, Ledg;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledi;

    if-eqz v0, :cond_5

    iget-object v0, v0, Ledi;->a:Ljava/util/List;

    if-nez v0, :cond_6

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_1

    .line 61
    :cond_7
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v3, Ledg;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledi;

    iget-object v0, v0, Ledi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lede;

    iget-object v1, v0, Lede;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_9
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ledk;

    instance-of v6, v1, Ledm;

    if-eqz v6, :cond_9

    check-cast v1, Ledm;

    invoke-static {v0, v1}, Lfrj;->a(Lede;Ledm;)Lhgy;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_a
    invoke-static {v3}, Lfrj;->a(Ledg;)Ljava/util/List;

    move-result-object v0

    .line 60
    invoke-virtual {p1, v4, v0}, Lfrf;->a(Ljava/util/List;Ljava/util/List;)Lfrf;

    move-result-object p1

    goto :goto_2

    .line 55
    :catch_0
    move-exception v0

    goto :goto_2
.end method
