.class public final Lebc;
.super Lebf;
.source "SourceFile"


# instance fields
.field final a:Lebe;

.field private final h:Lecd;

.field private i:I

.field private j:J


# direct methods
.method public constructor <init>(Lebz;)V
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lebc;-><init>(Lebz;Ledv;Z)V

    .line 58
    return-void
.end method

.method private constructor <init>(Lebz;Ledv;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 72
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lebc;-><init>(Lebz;Ledv;ZLandroid/os/Handler;Lebe;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lebz;Ledv;ZLandroid/os/Handler;Lebe;)V
    .locals 7

    .prologue
    .line 101
    new-instance v6, Lecd;

    invoke-direct {v6}, Lecd;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lebc;-><init>(Lebz;Ledv;ZLandroid/os/Handler;Lebe;Lecd;)V

    .line 103
    return-void
.end method

.method private constructor <init>(Lebz;Ledv;ZLandroid/os/Handler;Lebe;Lecd;)V
    .locals 1

    .prologue
    .line 163
    invoke-direct/range {p0 .. p5}, Lebf;-><init>(Lebz;Ledv;ZLandroid/os/Handler;Lebj;)V

    .line 164
    iput-object p5, p0, Lebc;->a:Lebe;

    .line 165
    invoke-static {p6}, La;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecd;

    iput-object v0, p0, Lebc;->h:Lecd;

    .line 166
    const/4 v0, 0x0

    iput v0, p0, Lebc;->i:I

    .line 167
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 313
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 314
    iget-object v0, p0, Lebc;->h:Lecd;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lecd;->a(F)V

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_0
    invoke-super {p0, p1, p2}, Lebf;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected final a(J)V
    .locals 3

    .prologue
    .line 257
    invoke-super {p0, p1, p2}, Lebf;->a(J)V

    .line 259
    iget-object v0, p0, Lebc;->h:Lecd;

    invoke-virtual {v0}, Lecd;->d()V

    .line 260
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lebc;->j:J

    .line 261
    return-void
.end method

.method protected final a(JZ)V
    .locals 3

    .prologue
    .line 181
    invoke-super {p0, p1, p2, p3}, Lebf;->a(JZ)V

    .line 182
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lebc;->j:J

    .line 183
    return-void
.end method

.method protected final a(Landroid/media/MediaFormat;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 187
    iget-object v1, p0, Lebc;->h:Lecd;

    const-string v0, "channel-count"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unsupported channel count: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/4 v0, 0x4

    :goto_0
    const-string v3, "sample-rate"

    invoke-virtual {p1, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v3

    iget-object v4, v1, Lecd;->d:Landroid/media/AudioTrack;

    if-eqz v4, :cond_0

    iget v4, v1, Lecd;->e:I

    if-ne v4, v3, :cond_0

    iget v4, v1, Lecd;->f:I

    if-eq v4, v0, :cond_1

    :cond_0
    invoke-virtual {v1}, Lecd;->d()V

    invoke-static {v3, v0, v5}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v4

    iput v4, v1, Lecd;->i:I

    iput v5, v1, Lecd;->g:I

    iget v4, v1, Lecd;->c:F

    iget v5, v1, Lecd;->i:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v1, Lecd;->j:I

    iput v3, v1, Lecd;->e:I

    iput v0, v1, Lecd;->f:I

    mul-int/lit8 v0, v2, 0x2

    iput v0, v1, Lecd;->h:I

    .line 188
    :cond_1
    return-void

    .line 187
    :pswitch_2
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xfc

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x3fc

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected final a()Z
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x1

    return v0
.end method

.method protected final a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .locals 14

    .prologue
    .line 267
    if-eqz p9, :cond_1

    .line 268
    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 269
    iget-object v2, p0, Lebc;->b:Lean;

    iget v3, v2, Lean;->f:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lean;->f:I

    .line 270
    iget-object v2, p0, Lebc;->h:Lecd;

    iget v3, v2, Lecd;->o:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v3, 0x2

    iput v3, v2, Lecd;->o:I

    .line 271
    :cond_0
    const/4 v2, 0x1

    .line 308
    :goto_0
    return v2

    .line 275
    :cond_1
    iget-object v2, p0, Lebc;->h:Lecd;

    invoke-virtual {v2}, Lecd;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 277
    :try_start_0
    iget v2, p0, Lebc;->i:I

    if-eqz v2, :cond_9

    .line 278
    iget-object v2, p0, Lebc;->h:Lecd;

    iget v3, p0, Lebc;->i:I

    invoke-virtual {v2, v3}, Lecd;->a(I)I
    :try_end_0
    .catch Lech; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :goto_1
    iget v2, p0, Lecc;->g:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 289
    iget-object v2, p0, Lebc;->h:Lecd;

    invoke-virtual {v2}, Lecd;->b()V

    .line 293
    :cond_2
    iget-object v4, p0, Lebc;->h:Lecd;

    move-object/from16 v0, p7

    iget v2, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    move-object/from16 v0, p7

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    move-object/from16 v0, p7

    iget-wide v6, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const/4 v3, 0x0

    iget v8, v4, Lecd;->t:I

    if-nez v8, :cond_3

    if-eqz v5, :cond_3

    int-to-long v8, v5

    iget v10, v4, Lecd;->h:I

    int-to-long v10, v10

    div-long/2addr v8, v10

    invoke-virtual {v4, v8, v9}, Lecd;->a(J)J

    move-result-wide v8

    sub-long/2addr v6, v8

    iget-wide v8, v4, Lecd;->p:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_b

    const-wide/16 v8, 0x0

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    iput-wide v6, v4, Lecd;->p:J

    const/4 v6, 0x1

    iput v6, v4, Lecd;->o:I

    :cond_3
    :goto_2
    if-eqz v5, :cond_10

    iget v6, v4, Lecd;->t:I

    if-nez v6, :cond_6

    iput v5, v4, Lecd;->t:I

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    sget v2, Legz;->a:I

    const/16 v6, 0x15

    if-ge v2, v6, :cond_6

    iget-object v2, v4, Lecd;->r:[B

    if-eqz v2, :cond_4

    iget-object v2, v4, Lecd;->r:[B

    array-length v2, v2

    if-ge v2, v5, :cond_5

    :cond_4
    new-array v2, v5, [B

    iput-object v2, v4, Lecd;->r:[B

    :cond_5
    iget-object v2, v4, Lecd;->r:[B

    const/4 v6, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v2, v6, v5}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    iput v2, v4, Lecd;->s:I

    :cond_6
    const/4 v2, 0x0

    sget v5, Legz;->a:I

    const/16 v6, 0x15

    if-ge v5, v6, :cond_e

    iget-wide v6, v4, Lecd;->n:J

    invoke-virtual {v4}, Lecd;->f()J

    move-result-wide v8

    iget v5, v4, Lecd;->h:I

    int-to-long v10, v5

    mul-long/2addr v8, v10

    sub-long/2addr v6, v8

    long-to-int v5, v6

    iget v6, v4, Lecd;->j:I

    sub-int v5, v6, v5

    if-lez v5, :cond_7

    iget v2, v4, Lecd;->t:I

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v5, v4, Lecd;->d:Landroid/media/AudioTrack;

    iget-object v6, v4, Lecd;->r:[B

    iget v7, v4, Lecd;->s:I

    invoke-virtual {v5, v6, v7, v2}, Landroid/media/AudioTrack;->write([BII)I

    move-result v2

    if-gez v2, :cond_d

    const-string v5, "AudioTrack"

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x31

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "AudioTrack.write returned error code: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    :goto_3
    iget v5, v4, Lecd;->t:I

    sub-int/2addr v5, v2

    iput v5, v4, Lecd;->t:I

    iget-wide v6, v4, Lecd;->n:J

    int-to-long v8, v2

    add-long/2addr v6, v8

    iput-wide v6, v4, Lecd;->n:J

    iget v2, v4, Lecd;->t:I

    if-nez v2, :cond_10

    or-int/lit8 v2, v3, 0x2

    .line 297
    :goto_4
    and-int/lit8 v3, v2, 0x1

    if-eqz v3, :cond_8

    .line 298
    const-wide/high16 v4, -0x8000000000000000L

    iput-wide v4, p0, Lebc;->j:J

    .line 302
    :cond_8
    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_f

    .line 303
    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 304
    iget-object v2, p0, Lebc;->b:Lean;

    iget v3, v2, Lean;->e:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lean;->e:I

    .line 305
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 280
    :cond_9
    :try_start_1
    iget-object v2, p0, Lebc;->h:Lecd;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lecd;->a(I)I

    move-result v2

    iput v2, p0, Lebc;->i:I

    .line 281
    iget v2, p0, Lebc;->i:I
    :try_end_1
    .catch Lech; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 283
    :catch_0
    move-exception v2

    .line 284
    iget-object v3, p0, Lebc;->d:Landroid/os/Handler;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lebc;->a:Lebe;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lebc;->d:Landroid/os/Handler;

    new-instance v4, Lebd;

    invoke-direct {v4, p0, v2}, Lebd;-><init>(Lebc;Lech;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 285
    :cond_a
    new-instance v3, Leat;

    invoke-direct {v3, v2}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 293
    :cond_b
    iget-wide v8, v4, Lecd;->p:J

    iget-wide v10, v4, Lecd;->n:J

    iget v12, v4, Lecd;->h:I

    int-to-long v12, v12

    div-long/2addr v10, v12

    invoke-virtual {v4, v10, v11}, Lecd;->a(J)J

    move-result-wide v10

    add-long/2addr v8, v10

    iget v10, v4, Lecd;->o:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_c

    sub-long v10, v8, v6

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    const-wide/32 v12, 0x30d40

    cmp-long v10, v10, v12

    if-lez v10, :cond_c

    const-string v10, "AudioTrack"

    new-instance v11, Ljava/lang/StringBuilder;

    const/16 v12, 0x50

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "Discontinuity detected [expected "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", got "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x2

    iput v10, v4, Lecd;->o:I

    :cond_c
    iget v10, v4, Lecd;->o:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_3

    iget-wide v10, v4, Lecd;->p:J

    sub-long/2addr v6, v8

    add-long/2addr v6, v10

    iput-wide v6, v4, Lecd;->p:J

    const/4 v3, 0x1

    iput v3, v4, Lecd;->o:I

    const/4 v3, 0x1

    goto/16 :goto_2

    :cond_d
    iget v5, v4, Lecd;->s:I

    add-int/2addr v5, v2

    iput v5, v4, Lecd;->s:I

    goto/16 :goto_3

    :cond_e
    iget-object v2, v4, Lecd;->d:Landroid/media/AudioTrack;

    iget v5, v4, Lecd;->t:I

    const/4 v6, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v2, v0, v5, v6}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result v2

    goto/16 :goto_3

    .line 308
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_10
    move v2, v3

    goto/16 :goto_4
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 176
    invoke-static {p1}, La;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lebf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()V
    .locals 1

    .prologue
    .line 208
    invoke-super {p0}, Lebf;->b()V

    .line 209
    iget-object v0, p0, Lebc;->h:Lecd;

    invoke-virtual {v0}, Lecd;->b()V

    .line 210
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lebc;->h:Lecd;

    iget-object v1, v0, Lecd;->d:Landroid/media/AudioTrack;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lecd;->h()V

    iget-object v0, v0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    .line 215
    :cond_0
    invoke-super {p0}, Lebf;->c()V

    .line 216
    return-void
.end method

.method protected final d()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 222
    invoke-super {p0}, Lebf;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lebc;->h:Lecd;

    invoke-virtual {v2}, Lecd;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lebc;->h:Lecd;

    .line 223
    iget-wide v4, v2, Lecd;->n:J

    iget v2, v2, Lecd;->i:I

    int-to-long v2, v2

    cmp-long v2, v4, v2

    if-ltz v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected final e()Z
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lebc;->h:Lecd;

    invoke-virtual {v0}, Lecd;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    invoke-super {p0}, Lebf;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lebf;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final f()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    const-wide/high16 v2, -0x8000000000000000L

    .line 234
    iget-object v4, p0, Lebc;->h:Lecd;

    invoke-virtual {p0}, Lebc;->d()Z

    move-result v5

    invoke-virtual {v4}, Lecd;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, v4, Lecd;->p:J

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    move-wide v0, v2

    .line 235
    :cond_0
    :goto_1
    cmp-long v2, v0, v2

    if-nez v2, :cond_6

    .line 237
    iget-wide v0, p0, Lebc;->j:J

    invoke-super {p0}, Lebf;->f()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lebc;->j:J

    .line 242
    :goto_2
    iget-wide v0, p0, Lebc;->j:J

    return-wide v0

    .line 234
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, v4, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    invoke-virtual {v4}, Lecd;->e()V

    :cond_3
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    div-long/2addr v0, v8

    iget-boolean v6, v4, Lecd;->m:Z

    if-eqz v6, :cond_4

    iget-object v5, v4, Lecd;->b:Lecf;

    invoke-interface {v5}, Lecf;->a()J

    move-result-wide v6

    div-long/2addr v6, v8

    sub-long/2addr v0, v6

    iget v5, v4, Lecd;->e:I

    int-to-long v6, v5

    mul-long/2addr v0, v6

    const-wide/32 v6, 0xf4240

    div-long/2addr v0, v6

    iget-object v5, v4, Lecd;->b:Lecf;

    invoke-interface {v5}, Lecf;->b()J

    move-result-wide v6

    add-long/2addr v0, v6

    invoke-virtual {v4, v0, v1}, Lecd;->a(J)J

    move-result-wide v0

    iget-wide v4, v4, Lecd;->p:J

    add-long/2addr v0, v4

    goto :goto_1

    :cond_4
    iget v6, v4, Lecd;->k:I

    if-nez v6, :cond_5

    invoke-virtual {v4}, Lecd;->g()J

    move-result-wide v0

    iget-wide v6, v4, Lecd;->p:J

    add-long/2addr v0, v6

    :goto_3
    if-nez v5, :cond_0

    iget-wide v4, v4, Lecd;->q:J

    sub-long/2addr v0, v4

    goto :goto_1

    :cond_5
    iget-wide v6, v4, Lecd;->l:J

    add-long/2addr v0, v6

    iget-wide v6, v4, Lecd;->p:J

    add-long/2addr v0, v6

    goto :goto_3

    .line 240
    :cond_6
    iget-wide v2, p0, Lebc;->j:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lebc;->j:J

    goto :goto_2
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    iput v0, p0, Lebc;->i:I

    .line 249
    :try_start_0
    iget-object v0, p0, Lebc;->h:Lecd;

    invoke-virtual {v0}, Lecd;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    invoke-super {p0}, Lebf;->g()V

    .line 252
    return-void

    .line 251
    :catchall_0
    move-exception v0

    invoke-super {p0}, Lebf;->g()V

    throw v0
.end method
