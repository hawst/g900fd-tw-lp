.class public final Lgfz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/nio/channels/WritableByteChannel;


# instance fields
.field private final a:Ljava/util/concurrent/BlockingQueue;

.field private final b:Ljava/util/concurrent/BlockingQueue;

.field private c:Ljava/nio/ByteBuffer;

.field private d:Z

.field private synthetic e:Lgfy;


# direct methods
.method protected constructor <init>(Lgfy;)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 188
    iput-object p1, p0, Lgfz;->e:Lgfy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lgfz;->a:Ljava/util/concurrent/BlockingQueue;

    .line 196
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lgfz;->b:Ljava/util/concurrent/BlockingQueue;

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgfz;->d:Z

    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 5

    .prologue
    .line 213
    iget-object v0, p0, Lgfz;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v0

    iget-object v1, p0, Lgfz;->b:Ljava/util/concurrent/BlockingQueue;

    .line 214
    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x50

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Reading "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes, queue size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", return queue size "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 213
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 215
    const/4 v0, 0x0

    move v1, v0

    .line 216
    :goto_0
    if-lez p3, :cond_3

    .line 218
    iget-object v0, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_1

    .line 219
    if-nez v1, :cond_0

    .line 222
    :try_start_0
    iget-object v0, p0, Lgfz;->a:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lgfz;->e:Lgfy;

    invoke-static {v2}, Lgfy;->a(Lgfy;)I

    move-result v2

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v4}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    .line 223
    iget-object v0, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_1

    .line 224
    new-instance v0, Ljava/net/SocketTimeoutException;

    const-string v1, "Cronet read timeout."

    invoke-direct {v0, v1}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Interrupted."

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 231
    :cond_0
    iget-object v0, p0, Lgfz;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    .line 232
    iget-object v0, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_3

    .line 233
    :cond_1
    iget-object v0, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 240
    iget-object v2, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, p1, p2, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 241
    add-int/2addr p2, v0

    .line 242
    sub-int/2addr p3, v0

    .line 243
    add-int/2addr v0, v1

    .line 246
    iget-object v1, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-nez v1, :cond_2

    .line 247
    iget-object v1, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 248
    iget-object v1, p0, Lgfz;->b:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 249
    const/4 v1, 0x0

    iput-object v1, p0, Lgfz;->c:Ljava/nio/ByteBuffer;

    :cond_2
    move v1, v0

    .line 251
    goto :goto_0

    .line 252
    :cond_3
    return v1
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgfz;->d:Z

    .line 283
    return-void
.end method

.method public final isOpen()Z
    .locals 1

    .prologue
    .line 287
    iget-boolean v0, p0, Lgfz;->d:Z

    return v0
.end method

.method public final write(Ljava/nio/ByteBuffer;)I
    .locals 4

    .prologue
    .line 257
    invoke-virtual {p0}, Lgfz;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 260
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    .line 262
    iget-object v0, p0, Lgfz;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 263
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 266
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Allocating a buffer, size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 269
    :cond_2
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 270
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 273
    :try_start_0
    iget-object v2, p0, Lgfz;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2, v0}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    return v1

    .line 274
    :catch_0
    move-exception v0

    .line 275
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to write."

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
