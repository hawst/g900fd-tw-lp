.class public final Leek;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leee;


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:[I

.field private static final d:[I

.field private static final e:Ljava/util/Set;

.field private static final f:Ljava/util/Set;


# instance fields
.field private final g:I

.field private final h:Leem;

.field private final i:[B

.field private final j:Ljava/util/Stack;

.field private final k:Ljava/util/Stack;

.field private final l:Leep;

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Leem;

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private final w:Ljava/util/HashMap;

.field private x:Leef;

.field private y:Leen;

.field private z:Leej;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const v6, 0x6d766578

    const v5, 0x6d6f6f76

    const v4, 0x6d6f6f66

    const v3, 0x6d696e66

    const v2, 0x6d646961

    .line 52
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Leek;->a:[B

    .line 53
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Leek;->b:[B

    .line 56
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Leek;->c:[I

    .line 58
    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Leek;->d:[I

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 75
    const v1, 0x61766331

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 76
    const v1, 0x61766333

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 77
    const v1, 0x65736473

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 78
    const v1, 0x68646c72    # 4.3148E24f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 79
    const v1, 0x6d646174

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 80
    const v1, 0x6d646864

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 83
    const v1, 0x6d703461

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 84
    const v1, 0x73696478

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 85
    const v1, 0x73747364

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 86
    const v1, 0x74666474

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 87
    const v1, 0x74666864

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 88
    const v1, 0x746b6864

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 89
    const v1, 0x74726166

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 90
    const v1, 0x7472616b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 91
    const v1, 0x74726578

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 92
    const v1, 0x7472756e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 93
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 94
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 96
    const v1, 0x7374626c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 97
    const v1, 0x70737368    # 3.013775E29f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 98
    const v1, 0x7361697a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 99
    const v1, 0x75756964

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 100
    const v1, 0x73656e63

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 101
    const v1, 0x70617370

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 102
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Leek;->e:Ljava/util/Set;

    .line 108
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 109
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 110
    const v1, 0x7472616b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 111
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 112
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 113
    const v1, 0x7374626c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 114
    const v1, 0x61766343

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 115
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 116
    const v1, 0x74726166

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 117
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Leek;->f:Ljava/util/Set;

    .line 119
    return-void

    .line 52
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 53
    :array_1
    .array-data 1
        -0x5et
        0x39t
        0x4ft
        0x52t
        0x5at
        -0x65t
        0x4ft
        0x14t
        -0x5et
        0x44t
        0x6ct
        0x42t
        0x7ct
        0x64t
        -0x73t
        -0xct
    .end array-data

    .line 56
    :array_2
    .array-data 4
        0x2
        0x1
        0x2
        0x3
        0x3
        0x4
        0x4
        0x5
    .end array-data

    .line 58
    :array_3
    .array-data 4
        0x20
        0x28
        0x30
        0x38
        0x40
        0x50
        0x60
        0x70
        0x80
        0xa0
        0xc0
        0xe0
        0x100
        0x140
        0x180
        0x1c0
        0x200
        0x240
        0x280
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Leek;-><init>(I)V

    .line 150
    return-void
.end method

.method private constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput v0, p0, Leek;->g:I

    .line 158
    iput v0, p0, Leek;->m:I

    .line 159
    new-instance v0, Leem;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Leem;-><init>(I)V

    iput-object v0, p0, Leek;->h:Leem;

    .line 160
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Leek;->i:[B

    .line 161
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Leek;->j:Ljava/util/Stack;

    .line 162
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Leek;->k:Ljava/util/Stack;

    .line 163
    new-instance v0, Leep;

    invoke-direct {v0}, Leep;-><init>()V

    iput-object v0, p0, Leek;->l:Leep;

    .line 164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Leek;->w:Ljava/util/HashMap;

    .line 165
    return-void
.end method

.method private a(Lefx;ILeby;)I
    .locals 9

    .prologue
    .line 1134
    if-nez p3, :cond_0

    .line 1135
    const/16 v0, 0x20

    .line 1175
    :goto_0
    return v0

    .line 1137
    :cond_0
    iget-object v0, p0, Leek;->l:Leep;

    iget v1, p0, Leek;->t:I

    invoke-virtual {v0, v1}, Leep;->b(I)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p3, Leby;->e:J

    .line 1138
    const/4 v0, 0x0

    iput v0, p3, Leby;->d:I

    .line 1139
    iget-object v0, p0, Leek;->l:Leep;

    iget-object v0, v0, Leep;->f:[Z

    iget v1, p0, Leek;->t:I

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_1

    .line 1140
    iget v0, p3, Leby;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p3, Leby;->d:I

    .line 1141
    iget v0, p0, Leek;->t:I

    iput v0, p0, Leek;->v:I

    .line 1143
    :cond_1
    iget-object v0, p3, Leby;->b:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_2

    iget-object v0, p3, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-ge v0, p2, :cond_3

    .line 1144
    :cond_2
    invoke-virtual {p3, p2}, Leby;->a(I)Z

    .line 1146
    :cond_3
    iget-object v0, p0, Leek;->l:Leep;

    iget-boolean v0, v0, Leep;->g:Z

    if-eqz v0, :cond_d

    .line 1147
    iget-object v0, p0, Leek;->l:Leep;

    iget-object v5, v0, Leep;->j:Leem;

    iget-object v0, p0, Leek;->y:Leen;

    iget-object v0, v0, Leen;->d:[Leeo;

    iget-object v1, p0, Leek;->l:Leep;

    iget v1, v1, Leep;->a:I

    aget-object v0, v0, v1

    iget-object v6, v0, Leeo;->c:[B

    iget-boolean v7, v0, Leeo;->a:Z

    iget v1, v0, Leeo;->b:I

    iget-object v0, p0, Leek;->l:Leep;

    iget-object v0, v0, Leep;->h:[Z

    iget v2, p0, Leek;->t:I

    aget-boolean v8, v0, v2

    iget-object v0, p3, Leby;->a:Leao;

    iget-object v0, v0, Leao;->a:[B

    if-eqz v0, :cond_4

    array-length v2, v0

    const/16 v3, 0x10

    if-eq v2, v3, :cond_5

    :cond_4
    const/16 v0, 0x10

    new-array v0, v0, [B

    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v5, v0, v2, v1}, Leem;->a([BII)V

    if-eqz v8, :cond_9

    invoke-virtual {v5}, Leem;->b()I

    move-result v1

    :goto_1
    iget-object v2, p3, Leby;->a:Leao;

    iget-object v2, v2, Leao;->d:[I

    if-eqz v2, :cond_6

    array-length v3, v2

    if-ge v3, v1, :cond_7

    :cond_6
    new-array v2, v1, [I

    :cond_7
    iget-object v3, p3, Leby;->a:Leao;

    iget-object v3, v3, Leao;->e:[I

    if-eqz v3, :cond_8

    array-length v4, v3

    if-ge v4, v1, :cond_14

    :cond_8
    new-array v3, v1, [I

    move-object v4, v3

    :goto_2
    if-eqz v8, :cond_a

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v1, :cond_b

    invoke-virtual {v5}, Leem;->b()I

    move-result v8

    aput v8, v2, v3

    invoke-virtual {v5}, Leem;->f()I

    move-result v8

    aput v8, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_9
    const/4 v1, 0x1

    goto :goto_1

    :cond_a
    const/4 v3, 0x0

    const/4 v5, 0x0

    aput v5, v2, v3

    const/4 v3, 0x0

    iget-object v5, p0, Leek;->l:Leep;

    iget-object v5, v5, Leep;->c:[I

    iget v8, p0, Leek;->t:I

    aget v5, v5, v8

    aput v5, v4, v3

    :cond_b
    iget-object v8, p3, Leby;->a:Leao;

    if-eqz v7, :cond_e

    const/4 v3, 0x1

    :goto_4
    iput v1, v8, Leao;->f:I

    iput-object v2, v8, Leao;->d:[I

    iput-object v4, v8, Leao;->e:[I

    iput-object v6, v8, Leao;->b:[B

    iput-object v0, v8, Leao;->a:[B

    iput v3, v8, Leao;->c:I

    sget v0, Legz;->a:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_c

    iget-object v0, v8, Leao;->g:Landroid/media/MediaCodec$CryptoInfo;

    iget v1, v8, Leao;->f:I

    iget-object v2, v8, Leao;->d:[I

    iget-object v3, v8, Leao;->e:[I

    iget-object v4, v8, Leao;->b:[B

    iget-object v5, v8, Leao;->a:[B

    iget v6, v8, Leao;->c:I

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec$CryptoInfo;->set(I[I[I[B[BI)V

    :cond_c
    if-eqz v7, :cond_d

    iget v0, p3, Leby;->d:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p3, Leby;->d:I

    .line 1150
    :cond_d
    iget-object v4, p3, Leby;->b:Ljava/nio/ByteBuffer;

    .line 1151
    if-nez v4, :cond_f

    .line 1152
    invoke-virtual {p1, p2}, Lefx;->a(I)I

    .line 1153
    const/4 v0, 0x0

    iput v0, p3, Leby;->c:I

    .line 1173
    :goto_5
    iget v0, p0, Leek;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Leek;->t:I

    .line 1174
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Leek;->a(I)V

    .line 1175
    const/4 v0, 0x4

    goto/16 :goto_0

    .line 1147
    :cond_e
    const/4 v3, 0x0

    goto :goto_4

    .line 1155
    :cond_f
    invoke-virtual {p1, v4, p2}, Lefx;->a(Ljava/nio/ByteBuffer;I)I

    .line 1156
    iget-object v0, p0, Leek;->y:Leen;

    iget v0, v0, Leen;->a:I

    const v1, 0x76696465

    if-ne v0, v1, :cond_13

    .line 1159
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    sub-int v3, v0, p2

    move v2, v3

    .line 1161
    :goto_6
    add-int v0, v3, p2

    if-ge v2, v0, :cond_12

    .line 1162
    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1163
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v1, v0, 0xff

    const/4 v0, 0x1

    :goto_7
    const/4 v5, 0x4

    if-ge v0, v5, :cond_10

    shl-int/lit8 v1, v1, 0x8

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v1, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_10
    if-gez v1, :cond_11

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Top bit not zero: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1164
    :cond_11
    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1165
    sget-object v0, Leek;->a:[B

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1166
    add-int/lit8 v0, v1, 0x4

    add-int/2addr v0, v2

    move v2, v0

    .line 1167
    goto :goto_6

    .line 1168
    :cond_12
    add-int v0, v3, p2

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1170
    :cond_13
    iput p2, p3, Leby;->c:I

    goto :goto_5

    :cond_14
    move-object v4, v3

    goto/16 :goto_2
.end method

.method private static a(Leem;)Landroid/util/Pair;
    .locals 15

    .prologue
    .line 493
    const/16 v0, 0xc

    iput v0, p0, Leem;->b:I

    .line 494
    invoke-virtual {p0}, Leem;->d()I

    move-result v8

    .line 495
    const/4 v1, 0x0

    .line 496
    new-array v9, v8, [Leeo;

    .line 497
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v8, :cond_a

    .line 498
    iget v10, p0, Leem;->b:I

    .line 499
    invoke-virtual {p0}, Leem;->d()I

    move-result v11

    .line 500
    invoke-virtual {p0}, Leem;->d()I

    move-result v0

    .line 501
    const v2, 0x61766331

    if-eq v0, v2, :cond_0

    const v2, 0x61766333

    if-eq v0, v2, :cond_0

    const v2, 0x656e6376

    if-ne v0, v2, :cond_8

    .line 504
    :cond_0
    add-int/lit8 v0, v10, 0x8

    iput v0, p0, Leem;->b:I

    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Leem;->a(I)V

    invoke-virtual {p0}, Leem;->b()I

    move-result v2

    invoke-virtual {p0}, Leem;->b()I

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Leem;->a(I)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget v0, p0, Leem;->b:I

    move v1, v0

    :goto_1
    sub-int v0, v1, v10

    if-ge v0, v11, :cond_6

    iput v1, p0, Leem;->b:I

    iget v0, p0, Leem;->b:I

    invoke-virtual {p0}, Leem;->d()I

    move-result v12

    invoke-virtual {p0}, Leem;->d()I

    move-result v13

    const v14, 0x61766343

    if-ne v13, v14, :cond_4

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Leem;->b:I

    invoke-virtual {p0}, Leem;->a()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    const/4 v5, 0x4

    if-eq v0, v5, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Leem;->a()I

    move-result v0

    and-int/lit8 v13, v0, 0x1f

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v13, :cond_2

    invoke-static {p0}, Leek;->b(Leem;)[B

    move-result-object v14

    invoke-interface {v5, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Leem;->a()I

    move-result v13

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v13, :cond_3

    invoke-static {p0}, Leek;->b(Leem;)[B

    move-result-object v14

    invoke-interface {v5, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move-object v0, v6

    :goto_4
    add-int/2addr v1, v12

    move-object v6, v0

    goto :goto_1

    :cond_4
    const v14, 0x73696e66

    if-ne v13, v14, :cond_5

    invoke-static {p0, v0, v12}, Leek;->a(Leem;II)Leeo;

    move-result-object v0

    goto :goto_4

    :cond_5
    const v14, 0x70617370

    if-ne v13, v14, :cond_b

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Leem;->b:I

    invoke-virtual {p0}, Leem;->f()I

    move-result v0

    invoke-virtual {p0}, Leem;->f()I

    move-result v4

    int-to-float v0, v0

    int-to-float v4, v4

    div-float v4, v0, v4

    move-object v0, v6

    goto :goto_4

    :cond_6
    const-string v0, "video/avc"

    const/4 v1, -0x1

    invoke-static/range {v0 .. v5}, Lebv;->a(Ljava/lang/String;IIIFLjava/util/List;)Lebv;

    move-result-object v0

    invoke-static {v0, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    .line 505
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v1, v0

    check-cast v1, Lebv;

    .line 506
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leeo;

    aput-object v0, v9, v7

    .line 514
    :cond_7
    :goto_5
    add-int v0, v10, v11

    iput v0, p0, Leem;->b:I

    .line 497
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0

    .line 507
    :cond_8
    const v2, 0x6d703461

    if-eq v0, v2, :cond_9

    const v2, 0x656e6361

    if-eq v0, v2, :cond_9

    const v2, 0x61632d33

    if-ne v0, v2, :cond_7

    .line 510
    :cond_9
    invoke-static {p0, v0, v10, v11}, Leek;->a(Leem;III)Landroid/util/Pair;

    move-result-object v2

    .line 511
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v1, v0

    check-cast v1, Lebv;

    .line 512
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Leeo;

    aput-object v0, v9, v7

    goto :goto_5

    .line 516
    :cond_a
    invoke-static {v1, v9}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_b
    move-object v0, v6

    goto :goto_4
.end method

.method private static a(Leem;III)Landroid/util/Pair;
    .locals 12

    .prologue
    .line 554
    add-int/lit8 v0, p2, 0x8

    iput v0, p0, Leem;->b:I

    .line 555
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Leem;->a(I)V

    .line 556
    invoke-virtual {p0}, Leem;->b()I

    move-result v6

    .line 557
    invoke-virtual {p0}, Leem;->b()I

    move-result v2

    .line 558
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Leem;->a(I)V

    .line 559
    iget-object v0, p0, Leem;->a:[B

    iget v1, p0, Leem;->b:I

    const/4 v3, 0x2

    invoke-static {v0, v1, v3}, Leem;->b([BII)I

    move-result v7

    iget v0, p0, Leem;->b:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Leem;->b:I

    .line 560
    const/4 v8, -0x1

    .line 562
    const/4 v3, 0x0

    .line 563
    const/4 v10, 0x0

    .line 564
    iget v0, p0, Leem;->b:I

    move v5, v0

    .line 565
    :goto_0
    sub-int v0, v5, p2

    if-ge v0, p3, :cond_c

    .line 566
    iput v5, p0, Leem;->b:I

    .line 567
    iget v0, p0, Leem;->b:I

    .line 568
    invoke-virtual {p0}, Leem;->d()I

    move-result v9

    .line 569
    invoke-virtual {p0}, Leem;->d()I

    move-result v1

    .line 570
    const v4, 0x6d703461

    if-eq p1, v4, :cond_0

    const v4, 0x656e6361

    if-ne p1, v4, :cond_8

    .line 571
    :cond_0
    const v4, 0x65736473

    if-ne v1, v4, :cond_7

    .line 572
    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Leem;->b:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Leem;->a(I)V

    invoke-virtual {p0}, Leem;->a()I

    move-result v0

    :goto_1
    const/16 v1, 0x7f

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Leem;->a()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Leem;->a(I)V

    invoke-virtual {p0}, Leem;->a()I

    move-result v0

    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Leem;->a(I)V

    :cond_2
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Leem;->b()I

    move-result v1

    invoke-virtual {p0, v1}, Leem;->a(I)V

    :cond_3
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Leem;->a(I)V

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Leem;->a(I)V

    invoke-virtual {p0}, Leem;->a()I

    move-result v0

    :goto_2
    const/16 v1, 0x7f

    if-le v0, v1, :cond_5

    invoke-virtual {p0}, Leem;->a()I

    move-result v0

    goto :goto_2

    :cond_5
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Leem;->a(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Leem;->a(I)V

    invoke-virtual {p0}, Leem;->a()I

    move-result v1

    and-int/lit8 v0, v1, 0x7f

    :goto_3
    const/16 v3, 0x7f

    if-le v1, v3, :cond_6

    invoke-virtual {p0}, Leem;->a()I

    move-result v1

    shl-int/lit8 v0, v0, 0x8

    and-int/lit8 v3, v1, 0x7f

    or-int/2addr v0, v3

    goto :goto_3

    :cond_6
    new-array v1, v0, [B

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3, v0}, Leem;->a([BII)V

    .line 576
    invoke-static {v1}, Legr;->a([B)Landroid/util/Pair;

    move-result-object v3

    .line 577
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 578
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object v0, v10

    .line 598
    :goto_4
    add-int v3, v5, v9

    move v5, v3

    move-object v10, v0

    move-object v3, v1

    .line 599
    goto/16 :goto_0

    .line 579
    :cond_7
    const v4, 0x73696e66

    if-ne v1, v4, :cond_10

    .line 580
    invoke-static {p0, v0, v9}, Leek;->a(Leem;II)Leeo;

    move-result-object v0

    move-object v1, v3

    goto :goto_4

    .line 582
    :cond_8
    const v4, 0x61632d33

    if-ne p1, v4, :cond_b

    const v4, 0x64616333

    if-ne v1, v4, :cond_b

    .line 585
    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Leem;->b:I

    invoke-virtual {p0}, Leem;->a()I

    move-result v0

    and-int/lit16 v0, v0, 0xc0

    shr-int/lit8 v0, v0, 0x6

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    .line 586
    :goto_5
    if-eqz v0, :cond_9

    .line 587
    iget v7, v0, Leel;->b:I

    .line 588
    iget v6, v0, Leel;->a:I

    .line 589
    iget v8, v0, Leel;->c:I

    .line 593
    :cond_9
    const/4 v0, 0x0

    move-object v1, v3

    .line 594
    goto :goto_4

    .line 585
    :pswitch_0
    const v0, 0xbb80

    :goto_6
    invoke-virtual {p0}, Leem;->a()I

    move-result v4

    sget-object v1, Leek;->c:[I

    and-int/lit8 v10, v4, 0x38

    shr-int/lit8 v10, v10, 0x3

    aget v1, v1, v10

    and-int/lit8 v10, v4, 0x4

    if-eqz v10, :cond_a

    add-int/lit8 v1, v1, 0x1

    :cond_a
    sget-object v10, Leek;->d:[I

    and-int/lit8 v4, v4, 0x3

    shl-int/lit8 v4, v4, 0x3

    invoke-virtual {p0}, Leem;->a()I

    move-result v11

    shr-int/lit8 v11, v11, 0x5

    add-int/2addr v4, v11

    aget v10, v10, v4

    new-instance v4, Leel;

    invoke-direct {v4, v1, v0, v10}, Leel;-><init>(III)V

    move-object v0, v4

    goto :goto_5

    :pswitch_1
    const v0, 0xac44

    goto :goto_6

    :pswitch_2
    const/16 v0, 0x7d00

    goto :goto_6

    .line 594
    :cond_b
    const v4, 0x65632d33

    if-ne p1, v4, :cond_10

    const v4, 0x64656333

    if-ne v1, v4, :cond_10

    .line 595
    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Leem;->b:I

    const/4 v7, 0x0

    .line 596
    const/4 v0, 0x0

    move-object v1, v3

    goto :goto_4

    .line 602
    :cond_c
    const v0, 0x61632d33

    if-ne p1, v0, :cond_d

    .line 603
    const-string v1, "audio/ac3"

    .line 610
    :goto_7
    if-nez v3, :cond_f

    const/4 v9, 0x0

    :goto_8
    new-instance v0, Lebv;

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/high16 v5, -0x40800000    # -1.0f

    invoke-direct/range {v0 .. v9}, Lebv;-><init>(Ljava/lang/String;IIIFIIILjava/util/List;)V

    .line 613
    invoke-static {v0, v10}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 604
    :cond_d
    const v0, 0x65632d33

    if-ne p1, v0, :cond_e

    .line 605
    const-string v1, "audio/eac3"

    goto :goto_7

    .line 607
    :cond_e
    const-string v1, "audio/mp4a-latm"

    goto :goto_7

    .line 612
    :cond_f
    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    goto :goto_8

    :cond_10
    move-object v0, v10

    move-object v1, v3

    goto/16 :goto_4

    .line 585
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Leem;II)Leeo;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/16 v10, 0x10

    const/4 v9, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 693
    add-int/lit8 v0, p1, 0x8

    move v5, v0

    move-object v0, v4

    .line 696
    :goto_0
    sub-int v3, v5, p1

    if-ge v3, p2, :cond_6

    .line 697
    iput v5, p0, Leem;->b:I

    .line 698
    invoke-virtual {p0}, Leem;->d()I

    move-result v6

    .line 699
    invoke-virtual {p0}, Leem;->d()I

    move-result v3

    .line 700
    const v7, 0x66726d61

    if-ne v3, v7, :cond_1

    .line 701
    invoke-virtual {p0}, Leem;->d()I

    .line 709
    :cond_0
    :goto_1
    add-int v3, v5, v6

    move v5, v3

    .line 710
    goto :goto_0

    .line 702
    :cond_1
    const v7, 0x7363686d

    if-ne v3, v7, :cond_2

    .line 703
    invoke-virtual {p0, v9}, Leem;->a(I)V

    .line 704
    invoke-virtual {p0}, Leem;->d()I

    .line 705
    invoke-virtual {p0}, Leem;->d()I

    goto :goto_1

    .line 706
    :cond_2
    const v7, 0x73636869

    if-ne v3, v7, :cond_0

    .line 707
    add-int/lit8 v0, v5, 0x8

    :goto_2
    sub-int v3, v0, v5

    if-ge v3, v6, :cond_5

    iput v0, p0, Leem;->b:I

    invoke-virtual {p0}, Leem;->d()I

    move-result v3

    invoke-virtual {p0}, Leem;->d()I

    move-result v7

    const v8, 0x74656e63

    if-ne v7, v8, :cond_4

    invoke-virtual {p0, v9}, Leem;->a(I)V

    invoke-virtual {p0}, Leem;->d()I

    move-result v3

    shr-int/lit8 v0, v3, 0x8

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    and-int/lit16 v7, v3, 0xff

    new-array v8, v10, [B

    invoke-virtual {p0, v8, v2, v10}, Leem;->a([BII)V

    new-instance v3, Leeo;

    invoke-direct {v3, v0, v7, v8}, Leeo;-><init>(ZI[B)V

    move-object v0, v3

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    add-int/2addr v0, v3

    goto :goto_2

    :cond_5
    move-object v0, v4

    goto :goto_1

    .line 712
    :cond_6
    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 266
    packed-switch p1, :pswitch_data_0

    .line 274
    :cond_0
    :goto_0
    iput p1, p0, Leek;->m:I

    .line 275
    return-void

    .line 268
    :pswitch_0
    iput v1, p0, Leek;->n:I

    .line 269
    iget-object v0, p0, Leek;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iput v1, p0, Leek;->o:I

    goto :goto_0

    .line 266
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Leeh;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x10

    const/16 v4, 0x8

    .line 374
    iget-object v3, p1, Leeh;->b:Ljava/util/ArrayList;

    .line 375
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    move v2, v1

    .line 376
    :goto_0
    if-ge v2, v6, :cond_1

    .line 377
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leeg;

    .line 378
    iget v7, v0, Leeg;->a:I

    const v8, 0x70737368    # 3.013775E29f

    if-ne v7, v8, :cond_0

    .line 379
    check-cast v0, Leei;

    iget-object v0, v0, Leei;->b:Leem;

    .line 380
    const/16 v7, 0xc

    iput v7, v0, Leem;->b:I

    .line 381
    new-instance v7, Ljava/util/UUID;

    invoke-virtual {v0}, Leem;->e()J

    move-result-wide v8

    invoke-virtual {v0}, Leem;->e()J

    move-result-wide v10

    invoke-direct {v7, v8, v9, v10, v11}, Ljava/util/UUID;-><init>(JJ)V

    .line 382
    invoke-virtual {v0}, Leem;->d()I

    move-result v8

    .line 383
    new-array v9, v8, [B

    .line 384
    invoke-virtual {v0, v9, v1, v8}, Leem;->a([BII)V

    .line 385
    iget-object v0, p0, Leek;->w:Ljava/util/HashMap;

    invoke-virtual {v0, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 388
    :cond_1
    const v0, 0x6d766578

    invoke-virtual {p1, v0}, Leeh;->b(I)Leeh;

    move-result-object v0

    .line 389
    const v2, 0x74726578

    invoke-virtual {v0, v2}, Leeh;->a(I)Leei;

    move-result-object v0

    iget-object v0, v0, Leei;->b:Leem;

    iput v5, v0, Leem;->b:I

    invoke-virtual {v0}, Leem;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0}, Leem;->f()I

    move-result v3

    invoke-virtual {v0}, Leem;->f()I

    move-result v6

    invoke-virtual {v0}, Leem;->d()I

    move-result v0

    new-instance v7, Leej;

    invoke-direct {v7, v2, v3, v6, v0}, Leej;-><init>(IIII)V

    iput-object v7, p0, Leek;->z:Leej;

    .line 390
    const v0, 0x7472616b

    invoke-virtual {p1, v0}, Leeh;->b(I)Leeh;

    move-result-object v2

    const v0, 0x6d646961

    invoke-virtual {v2, v0}, Leeh;->b(I)Leeh;

    move-result-object v6

    const v0, 0x68646c72    # 4.3148E24f

    invoke-virtual {v6, v0}, Leeh;->a(I)Leei;

    move-result-object v0

    iget-object v0, v0, Leei;->b:Leem;

    iput v5, v0, Leem;->b:I

    invoke-virtual {v0}, Leem;->d()I

    move-result v3

    const v0, 0x736f756e

    if-eq v3, v0, :cond_2

    const v0, 0x76696465

    if-ne v3, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, La;->c(Z)V

    const v0, 0x746b6864

    invoke-virtual {v2, v0}, Leeh;->a(I)Leei;

    move-result-object v0

    iget-object v1, v0, Leei;->b:Leem;

    iput v4, v1, Leem;->b:I

    invoke-virtual {v1}, Leem;->d()I

    move-result v0

    invoke-static {v0}, Leek;->b(I)I

    move-result v2

    if-nez v2, :cond_4

    move v0, v4

    :goto_2
    invoke-virtual {v1, v0}, Leem;->a(I)V

    invoke-virtual {v1}, Leem;->d()I

    move-result v7

    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Leem;->a(I)V

    if-nez v2, :cond_5

    invoke-virtual {v1}, Leem;->c()J

    move-result-wide v0

    :goto_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v0, 0x6d646864

    invoke-virtual {v6, v0}, Leeh;->a(I)Leei;

    move-result-object v0

    iget-object v0, v0, Leei;->b:Leem;

    iput v4, v0, Leem;->b:I

    invoke-virtual {v0}, Leem;->d()I

    move-result v1

    invoke-static {v1}, Leek;->b(I)I

    move-result v1

    if-nez v1, :cond_6

    :goto_4
    invoke-virtual {v0, v4}, Leem;->a(I)V

    invoke-virtual {v0}, Leem;->c()J

    move-result-wide v4

    const v0, 0x6d696e66

    invoke-virtual {v6, v0}, Leeh;->b(I)Leeh;

    move-result-object v0

    const v1, 0x7374626c

    invoke-virtual {v0, v1}, Leeh;->b(I)Leeh;

    move-result-object v0

    const v1, 0x73747364

    invoke-virtual {v0, v1}, Leeh;->a(I)Leei;

    move-result-object v0

    iget-object v0, v0, Leei;->b:Leem;

    invoke-static {v0}, Leek;->a(Leem;)Landroid/util/Pair;

    move-result-object v0

    new-instance v1, Leen;

    iget-object v6, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Lebv;

    iget-object v7, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, [Leeo;

    invoke-direct/range {v1 .. v7}, Leen;-><init>(IIJLebv;[Leeo;)V

    iput-object v1, p0, Leek;->y:Leen;

    .line 391
    return-void

    :cond_3
    move v0, v1

    .line 390
    goto/16 :goto_1

    :cond_4
    move v0, v5

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Leem;->g()J

    move-result-wide v0

    goto :goto_3

    :cond_6
    move v4, v5

    goto :goto_4
.end method

.method private static a(Leem;ILeep;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 993
    add-int/lit8 v0, p1, 0x8

    iput v0, p0, Leem;->b:I

    .line 994
    invoke-virtual {p0}, Leem;->d()I

    move-result v0

    .line 995
    const v2, 0xffffff

    and-int/2addr v0, v2

    .line 997
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_0

    .line 999
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Overriding TrackEncryptionBox parameters is unsupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1002
    :cond_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1003
    :goto_0
    invoke-virtual {p0}, Leem;->f()I

    move-result v2

    .line 1004
    iget v3, p2, Leep;->b:I

    if-eq v2, v3, :cond_2

    .line 1005
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p2, Leep;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x29

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Length mismatch: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 1002
    goto :goto_0

    .line 1008
    :cond_2
    iget-object v3, p2, Leep;->h:[Z

    invoke-static {v3, v1, v2, v0}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 1009
    iget-object v0, p0, Leem;->a:[B

    array-length v0, v0

    iget v2, p0, Leem;->b:I

    sub-int/2addr v0, v2

    invoke-virtual {p2, v0}, Leep;->a(I)V

    .line 1010
    iget-object v0, p2, Leep;->j:Leem;

    iget-object v0, v0, Leem;->a:[B

    iget v2, p2, Leep;->i:I

    invoke-virtual {p0, v0, v1, v2}, Leem;->a([BII)V

    iget-object v0, p2, Leep;->j:Leem;

    iput v1, v0, Leem;->b:I

    iput-boolean v1, p2, Leep;->k:Z

    .line 1011
    return-void
.end method

.method private static b(I)I
    .locals 1

    .prologue
    .line 1222
    shr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private b(Leeh;)V
    .locals 31

    .prologue
    .line 394
    move-object/from16 v0, p0

    iget-object v2, v0, Leek;->l:Leep;

    const/4 v3, 0x0

    iput v3, v2, Leep;->b:I

    const/4 v3, 0x0

    iput-boolean v3, v2, Leep;->g:Z

    const/4 v3, 0x0

    iput-boolean v3, v2, Leep;->k:Z

    .line 395
    move-object/from16 v0, p0

    iget-object v15, v0, Leek;->y:Leen;

    move-object/from16 v0, p0

    iget-object v8, v0, Leek;->z:Leej;

    move-object/from16 v0, p0

    iget-object v0, v0, Leek;->l:Leep;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v9, v0, Leek;->g:I

    move-object/from16 v0, p0

    iget-object v0, v0, Leek;->i:[B

    move-object/from16 v17, v0

    const v2, 0x74726166

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Leeh;->b(I)Leeh;

    move-result-object v18

    const v2, 0x74666474

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Leeh;->a(I)Leei;

    move-result-object v2

    if-nez v2, :cond_6

    const-wide/16 v2, 0x0

    :goto_0
    const v4, 0x74666864

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Leeh;->a(I)Leei;

    move-result-object v4

    iget-object v10, v4, Leei;->b:Leem;

    const/16 v4, 0x8

    iput v4, v10, Leem;->b:I

    invoke-virtual {v10}, Leem;->d()I

    move-result v4

    const v5, 0xffffff

    and-int v11, v5, v4

    const/4 v4, 0x4

    invoke-virtual {v10, v4}, Leem;->a(I)V

    and-int/lit8 v4, v11, 0x1

    if-eqz v4, :cond_0

    const/16 v4, 0x8

    invoke-virtual {v10, v4}, Leem;->a(I)V

    :cond_0
    and-int/lit8 v4, v11, 0x2

    if-eqz v4, :cond_8

    invoke-virtual {v10}, Leem;->f()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move v7, v4

    :goto_1
    and-int/lit8 v4, v11, 0x8

    if-eqz v4, :cond_9

    invoke-virtual {v10}, Leem;->f()I

    move-result v4

    move v6, v4

    :goto_2
    and-int/lit8 v4, v11, 0x10

    if-eqz v4, :cond_a

    invoke-virtual {v10}, Leem;->f()I

    move-result v4

    move v5, v4

    :goto_3
    and-int/lit8 v4, v11, 0x20

    if-eqz v4, :cond_b

    invoke-virtual {v10}, Leem;->f()I

    move-result v4

    :goto_4
    new-instance v19, Leej;

    move-object/from16 v0, v19

    invoke-direct {v0, v7, v6, v5, v4}, Leej;-><init>(IIII)V

    move-object/from16 v0, v19

    iget v4, v0, Leej;->a:I

    move-object/from16 v0, v16

    iput v4, v0, Leep;->a:I

    const v4, 0x7472756e

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Leeh;->a(I)Leei;

    move-result-object v4

    iget-object v0, v4, Leei;->b:Leem;

    move-object/from16 v20, v0

    const/16 v4, 0x8

    move-object/from16 v0, v20

    iput v4, v0, Leem;->b:I

    invoke-virtual/range {v20 .. v20}, Leem;->d()I

    move-result v4

    const v5, 0xffffff

    and-int v6, v5, v4

    invoke-virtual/range {v20 .. v20}, Leem;->f()I

    move-result v21

    and-int/lit8 v4, v6, 0x1

    if-eqz v4, :cond_1

    const/4 v4, 0x4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Leem;->a(I)V

    :cond_1
    and-int/lit8 v4, v6, 0x4

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    :goto_5
    move-object/from16 v0, v19

    iget v7, v0, Leej;->d:I

    if-eqz v4, :cond_2

    invoke-virtual/range {v20 .. v20}, Leem;->f()I

    move-result v7

    :cond_2
    and-int/lit16 v5, v6, 0x100

    if-eqz v5, :cond_d

    const/4 v5, 0x1

    move v14, v5

    :goto_6
    and-int/lit16 v5, v6, 0x200

    if-eqz v5, :cond_e

    const/4 v5, 0x1

    move v13, v5

    :goto_7
    and-int/lit16 v5, v6, 0x400

    if-eqz v5, :cond_f

    const/4 v5, 0x1

    move v12, v5

    :goto_8
    and-int/lit16 v5, v6, 0x800

    if-eqz v5, :cond_10

    const/4 v5, 0x1

    :goto_9
    move/from16 v0, v21

    move-object/from16 v1, v16

    iput v0, v1, Leep;->b:I

    move-object/from16 v0, v16

    iget-object v6, v0, Leep;->c:[I

    if-eqz v6, :cond_3

    move-object/from16 v0, v16

    iget-object v6, v0, Leep;->c:[I

    array-length v6, v6

    move-object/from16 v0, v16

    iget v8, v0, Leep;->b:I

    if-ge v6, v8, :cond_4

    :cond_3
    mul-int/lit8 v6, v21, 0x7d

    div-int/lit8 v6, v6, 0x64

    new-array v8, v6, [I

    move-object/from16 v0, v16

    iput-object v8, v0, Leep;->c:[I

    new-array v8, v6, [I

    move-object/from16 v0, v16

    iput-object v8, v0, Leep;->d:[I

    new-array v8, v6, [J

    move-object/from16 v0, v16

    iput-object v8, v0, Leep;->e:[J

    new-array v8, v6, [Z

    move-object/from16 v0, v16

    iput-object v8, v0, Leep;->f:[Z

    new-array v6, v6, [Z

    move-object/from16 v0, v16

    iput-object v6, v0, Leep;->h:[Z

    :cond_4
    move-object/from16 v0, v16

    iget-object v0, v0, Leep;->c:[I

    move-object/from16 v22, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Leep;->d:[I

    move-object/from16 v23, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Leep;->e:[J

    move-object/from16 v24, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Leep;->f:[Z

    move-object/from16 v25, v0

    iget-wide v0, v15, Leen;->b:J

    move-wide/from16 v26, v0

    iget v6, v15, Leen;->a:I

    const v8, 0x76696465

    if-ne v6, v8, :cond_11

    and-int/lit8 v6, v9, 0x1

    const/4 v8, 0x1

    if-ne v6, v8, :cond_11

    const/4 v6, 0x1

    :goto_a
    const/4 v8, 0x0

    move v9, v8

    move-wide v10, v2

    :goto_b
    move/from16 v0, v21

    if-ge v9, v0, :cond_18

    if-eqz v14, :cond_12

    invoke-virtual/range {v20 .. v20}, Leem;->f()I

    move-result v2

    move v8, v2

    :goto_c
    if-eqz v13, :cond_13

    invoke-virtual/range {v20 .. v20}, Leem;->f()I

    move-result v2

    move v3, v2

    :goto_d
    if-nez v9, :cond_14

    if-eqz v4, :cond_14

    move v2, v7

    :goto_e
    if-eqz v5, :cond_16

    invoke-virtual/range {v20 .. v20}, Leem;->d()I

    move-result v28

    move/from16 v0, v28

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v28, v0

    div-long v28, v28, v26

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v28, v0

    aput v28, v23, v9

    :goto_f
    const-wide/16 v28, 0x3e8

    mul-long v28, v28, v10

    div-long v28, v28, v26

    aput-wide v28, v24, v9

    aput v3, v22, v9

    shr-int/lit8 v2, v2, 0x10

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_17

    if-eqz v6, :cond_5

    if-nez v9, :cond_17

    :cond_5
    const/4 v2, 0x1

    :goto_10
    aput-boolean v2, v25, v9

    int-to-long v2, v8

    add-long/2addr v10, v2

    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_b

    :cond_6
    const v2, 0x74666474

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Leeh;->a(I)Leei;

    move-result-object v2

    iget-object v2, v2, Leei;->b:Leem;

    const/16 v3, 0x8

    iput v3, v2, Leem;->b:I

    invoke-virtual {v2}, Leem;->d()I

    move-result v3

    invoke-static {v3}, Leek;->b(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    invoke-virtual {v2}, Leem;->g()J

    move-result-wide v2

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v2}, Leem;->c()J

    move-result-wide v2

    goto/16 :goto_0

    :cond_8
    iget v4, v8, Leej;->a:I

    move v7, v4

    goto/16 :goto_1

    :cond_9
    iget v4, v8, Leej;->b:I

    move v6, v4

    goto/16 :goto_2

    :cond_a
    iget v4, v8, Leej;->c:I

    move v5, v4

    goto/16 :goto_3

    :cond_b
    iget v4, v8, Leej;->d:I

    goto/16 :goto_4

    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_d
    const/4 v5, 0x0

    move v14, v5

    goto/16 :goto_6

    :cond_e
    const/4 v5, 0x0

    move v13, v5

    goto/16 :goto_7

    :cond_f
    const/4 v5, 0x0

    move v12, v5

    goto/16 :goto_8

    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_9

    :cond_11
    const/4 v6, 0x0

    goto/16 :goto_a

    :cond_12
    move-object/from16 v0, v19

    iget v2, v0, Leej;->b:I

    move v8, v2

    goto/16 :goto_c

    :cond_13
    move-object/from16 v0, v19

    iget v2, v0, Leej;->c:I

    move v3, v2

    goto/16 :goto_d

    :cond_14
    if-eqz v12, :cond_15

    invoke-virtual/range {v20 .. v20}, Leem;->d()I

    move-result v2

    goto/16 :goto_e

    :cond_15
    move-object/from16 v0, v19

    iget v2, v0, Leej;->d:I

    goto/16 :goto_e

    :cond_16
    const/16 v28, 0x0

    aput v28, v23, v9

    goto/16 :goto_f

    :cond_17
    const/4 v2, 0x0

    goto :goto_10

    :cond_18
    const v2, 0x7361697a

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Leeh;->a(I)Leei;

    move-result-object v2

    if-eqz v2, :cond_1e

    iget-object v3, v15, Leen;->d:[Leeo;

    move-object/from16 v0, v19

    iget v4, v0, Leej;->a:I

    aget-object v3, v3, v4

    iget-object v5, v2, Leei;->b:Leem;

    iget v6, v3, Leeo;->b:I

    const/16 v2, 0x8

    iput v2, v5, Leem;->b:I

    invoke-virtual {v5}, Leem;->d()I

    move-result v2

    const v3, 0xffffff

    and-int/2addr v2, v3

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_19

    const/16 v2, 0x8

    invoke-virtual {v5, v2}, Leem;->a(I)V

    :cond_19
    invoke-virtual {v5}, Leem;->a()I

    move-result v4

    invoke-virtual {v5}, Leem;->f()I

    move-result v7

    move-object/from16 v0, v16

    iget v2, v0, Leep;->b:I

    if-eq v7, v2, :cond_1a

    new-instance v2, Ljava/lang/IllegalStateException;

    move-object/from16 v0, v16

    iget v3, v0, Leep;->b:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x29

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Length mismatch: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1a
    const/4 v3, 0x0

    if-nez v4, :cond_1c

    move-object/from16 v0, v16

    iget-object v8, v0, Leep;->h:[Z

    const/4 v2, 0x0

    move/from16 v30, v2

    move v2, v3

    move/from16 v3, v30

    :goto_11
    if-ge v3, v7, :cond_1d

    invoke-virtual {v5}, Leem;->a()I

    move-result v9

    add-int v4, v2, v9

    if-le v9, v6, :cond_1b

    const/4 v2, 0x1

    :goto_12
    aput-boolean v2, v8, v3

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_11

    :cond_1b
    const/4 v2, 0x0

    goto :goto_12

    :cond_1c
    if-le v4, v6, :cond_21

    const/4 v2, 0x1

    :goto_13
    mul-int v3, v4, v7

    add-int/lit8 v3, v3, 0x0

    move-object/from16 v0, v16

    iget-object v4, v0, Leep;->h:[Z

    const/4 v5, 0x0

    invoke-static {v4, v5, v7, v2}, Ljava/util/Arrays;->fill([ZIIZ)V

    move v2, v3

    :cond_1d
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Leep;->a(I)V

    :cond_1e
    const v2, 0x73656e63

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Leeh;->a(I)Leei;

    move-result-object v2

    if-eqz v2, :cond_1f

    iget-object v2, v2, Leei;->b:Leem;

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-static {v2, v3, v0}, Leek;->a(Leem;ILeep;)V

    :cond_1f
    move-object/from16 v0, v18

    iget-object v2, v0, Leeh;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_14
    if-ge v3, v4, :cond_22

    move-object/from16 v0, v18

    iget-object v2, v0, Leeh;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Leeg;

    iget v5, v2, Leeg;->a:I

    const v6, 0x75756964

    if-ne v5, v6, :cond_20

    check-cast v2, Leei;

    iget-object v2, v2, Leei;->b:Leem;

    const/16 v5, 0x8

    iput v5, v2, Leem;->b:I

    const/4 v5, 0x0

    const/16 v6, 0x10

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v5, v6}, Leem;->a([BII)V

    sget-object v5, Leek;->b:[B

    move-object/from16 v0, v17

    invoke-static {v0, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-eqz v5, :cond_20

    const/16 v5, 0x10

    move-object/from16 v0, v16

    invoke-static {v2, v5, v0}, Leek;->a(Leem;ILeep;)V

    :cond_20
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_14

    :cond_21
    const/4 v2, 0x0

    goto :goto_13

    .line 396
    :cond_22
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Leek;->t:I

    .line 397
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Leek;->v:I

    .line 398
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Leek;->u:I

    .line 399
    move-object/from16 v0, p0

    iget v2, v0, Leek;->s:I

    if-eqz v2, :cond_25

    .line 400
    const/4 v2, 0x0

    :goto_15
    move-object/from16 v0, p0

    iget-object v3, v0, Leek;->l:Leep;

    iget v3, v3, Leep;->b:I

    if-ge v2, v3, :cond_24

    .line 401
    move-object/from16 v0, p0

    iget-object v3, v0, Leek;->l:Leep;

    iget-object v3, v3, Leep;->f:[Z

    aget-boolean v3, v3, v2

    if-eqz v3, :cond_23

    .line 402
    move-object/from16 v0, p0

    iget-object v3, v0, Leek;->l:Leep;

    invoke-virtual {v3, v2}, Leep;->b(I)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget v3, v0, Leek;->s:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-gtz v3, :cond_23

    .line 403
    move-object/from16 v0, p0

    iput v2, v0, Leek;->u:I

    .line 400
    :cond_23
    add-int/lit8 v2, v2, 0x1

    goto :goto_15

    .line 407
    :cond_24
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Leek;->s:I

    .line 409
    :cond_25
    return-void
.end method

.method private static b(Leem;)[B
    .locals 3

    .prologue
    .line 685
    invoke-virtual {p0}, Leem;->b()I

    move-result v0

    .line 686
    iget v1, p0, Leem;->b:I

    .line 687
    invoke-virtual {p0, v0}, Leem;->a(I)V

    .line 688
    iget-object v2, p0, Leem;->a:[B

    invoke-static {v2, v1, v0}, Legr;->a([BII)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lefx;Leby;)I
    .locals 24

    .prologue
    .line 215
    const/4 v4, 0x0

    move v14, v4

    .line 216
    :goto_0
    and-int/lit8 v4, v14, 0x27

    if-nez v4, :cond_1a

    .line 217
    :try_start_0
    move-object/from16 v0, p0

    iget v4, v0, Leek;->m:I

    packed-switch v4, :pswitch_data_0

    .line 228
    move-object/from16 v0, p0

    iget v4, v0, Leek;->t:I

    move-object/from16 v0, p0

    iget-object v5, v0, Leek;->l:Leep;

    iget v5, v5, Leep;->b:I

    if-lt v4, v5, :cond_15

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(I)V

    const/4 v4, 0x0

    :goto_1
    or-int/2addr v4, v14

    move v14, v4

    .line 229
    goto :goto_0

    .line 219
    :pswitch_0
    move-object/from16 v0, p0

    iget v4, v0, Leek;->n:I

    rsub-int/lit8 v4, v4, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Leek;->h:Leem;

    iget-object v5, v5, Leem;->a:[B

    move-object/from16 v0, p0

    iget v6, v0, Leek;->n:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v4}, Lefx;->a([BII)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    const/4 v4, 0x2

    :goto_2
    or-int/2addr v4, v14

    move v14, v4

    .line 220
    goto :goto_0

    .line 219
    :cond_0
    move-object/from16 v0, p0

    iget v5, v0, Leek;->o:I

    add-int/2addr v5, v4

    move-object/from16 v0, p0

    iput v5, v0, Leek;->o:I

    move-object/from16 v0, p0

    iget v5, v0, Leek;->n:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Leek;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Leek;->n:I

    const/16 v5, 0x8

    if-eq v4, v5, :cond_1

    const/4 v4, 0x1

    goto :goto_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->h:Leem;

    const/4 v5, 0x0

    iput v5, v4, Leem;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->h:Leem;

    invoke-virtual {v4}, Leem;->d()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Leek;->q:I

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->h:Leem;

    invoke-virtual {v4}, Leem;->d()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Leek;->p:I

    move-object/from16 v0, p0

    iget v4, v0, Leek;->p:I

    const v5, 0x6d646174

    if-ne v4, v5, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->l:Leep;

    iget-boolean v4, v4, Leep;->k:Z

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(I)V

    :goto_3
    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 233
    :catch_0
    move-exception v4

    .line 234
    new-instance v5, Lebx;

    invoke-direct {v5, v4}, Lebx;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 219
    :cond_3
    :try_start_1
    sget-object v4, Leek;->e:Ljava/util/Set;

    move-object/from16 v0, p0

    iget v5, v0, Leek;->p:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    sget-object v4, Leek;->f:Ljava/util/Set;

    move-object/from16 v0, p0

    iget v5, v0, Leek;->p:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->j:Ljava/util/Stack;

    new-instance v5, Leeh;

    move-object/from16 v0, p0

    iget v6, v0, Leek;->p:I

    invoke-direct {v5, v6}, Leeh;-><init>(I)V

    invoke-virtual {v4, v5}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->k:Ljava/util/Stack;

    move-object/from16 v0, p0

    iget v5, v0, Leek;->o:I

    move-object/from16 v0, p0

    iget v6, v0, Leek;->q:I

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    :goto_4
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_4
    new-instance v4, Leem;

    move-object/from16 v0, p0

    iget v5, v0, Leek;->q:I

    invoke-direct {v4, v5}, Leem;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Leek;->r:Leem;

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->h:Leem;

    iget-object v4, v4, Leem;->a:[B

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Leek;->r:Leem;

    iget-object v6, v6, Leem;->a:[B

    const/4 v7, 0x0

    const/16 v8, 0x8

    invoke-static {v4, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(I)V

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Leek;->r:Leem;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(I)V

    goto :goto_4

    .line 222
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->r:Leem;

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->r:Leem;

    iget-object v4, v4, Leem;->a:[B

    move-object/from16 v0, p0

    iget v5, v0, Leek;->n:I

    move-object/from16 v0, p0

    iget v6, v0, Leek;->q:I

    move-object/from16 v0, p0

    iget v7, v0, Leek;->n:I

    sub-int/2addr v6, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, Lefx;->a([BII)I

    move-result v4

    :goto_5
    const/4 v5, -0x1

    if-ne v4, v5, :cond_7

    const/4 v4, 0x2

    :goto_6
    or-int/2addr v4, v14

    move v14, v4

    .line 223
    goto/16 :goto_0

    .line 222
    :cond_6
    move-object/from16 v0, p0

    iget v4, v0, Leek;->q:I

    move-object/from16 v0, p0

    iget v5, v0, Leek;->n:I

    sub-int/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lefx;->a(I)I

    move-result v4

    goto :goto_5

    :cond_7
    move-object/from16 v0, p0

    iget v5, v0, Leek;->o:I

    add-int/2addr v5, v4

    move-object/from16 v0, p0

    iput v5, v0, Leek;->o:I

    move-object/from16 v0, p0

    iget v5, v0, Leek;->n:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Leek;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Leek;->n:I

    move-object/from16 v0, p0

    iget v5, v0, Leek;->q:I

    if-eq v4, v5, :cond_8

    const/4 v4, 0x1

    goto :goto_6

    :cond_8
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->r:Leem;

    if-eqz v4, :cond_a

    new-instance v5, Leei;

    move-object/from16 v0, p0

    iget v4, v0, Leek;->p:I

    move-object/from16 v0, p0

    iget-object v6, v0, Leek;->r:Leem;

    invoke-direct {v5, v4, v6}, Leei;-><init>(ILeem;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->j:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->j:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Leeh;

    invoke-virtual {v4, v5}, Leeh;->a(Leeg;)V

    :cond_9
    const/4 v4, 0x0

    :goto_7
    or-int/lit8 v6, v4, 0x0

    :cond_a
    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->k:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->k:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Leek;->o:I

    if-ne v4, v5, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->k:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->j:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Leeh;

    iget v5, v4, Leeh;->a:I

    const v7, 0x6d6f6f76

    if-ne v5, v7, :cond_f

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(Leeh;)V

    const/16 v4, 0x8

    :goto_9
    or-int/2addr v6, v4

    goto :goto_8

    :cond_b
    iget v4, v5, Leei;->a:I

    const v6, 0x73696478

    if-ne v4, v6, :cond_9

    iget-object v5, v5, Leei;->b:Leem;

    const/16 v4, 0x8

    iput v4, v5, Leem;->b:I

    invoke-virtual {v5}, Leem;->d()I

    move-result v4

    invoke-static {v4}, Leek;->b(I)I

    move-result v4

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Leem;->a(I)V

    invoke-virtual {v5}, Leem;->c()J

    move-result-wide v16

    if-nez v4, :cond_c

    invoke-virtual {v5}, Leem;->c()J

    move-result-wide v10

    invoke-virtual {v5}, Leem;->c()J

    move-result-wide v12

    :goto_a
    const/4 v4, 0x2

    invoke-virtual {v5, v4}, Leem;->a(I)V

    invoke-virtual {v5}, Leem;->b()I

    move-result v15

    new-array v6, v15, [I

    new-array v7, v15, [J

    new-array v8, v15, [J

    new-array v9, v15, [J

    const/4 v4, 0x0

    :goto_b
    if-ge v4, v15, :cond_e

    invoke-virtual {v5}, Leem;->d()I

    move-result v18

    const/high16 v19, -0x80000000

    and-int v19, v19, v18

    if-eqz v19, :cond_d

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Unhandled indirect reference"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_c
    invoke-virtual {v5}, Leem;->g()J

    move-result-wide v10

    invoke-virtual {v5}, Leem;->g()J

    move-result-wide v12

    goto :goto_a

    :cond_d
    invoke-virtual {v5}, Leem;->c()J

    move-result-wide v20

    const v19, 0x7fffffff

    and-int v18, v18, v19

    aput v18, v6, v4

    aput-wide v12, v7, v4

    const-wide/32 v18, 0xf4240

    mul-long v18, v18, v10

    div-long v18, v18, v16

    aput-wide v18, v9, v4

    add-long v18, v10, v20

    const-wide/32 v22, 0xf4240

    mul-long v18, v18, v22

    div-long v18, v18, v16

    aget-wide v22, v9, v4

    sub-long v18, v18, v22

    aput-wide v18, v8, v4

    add-long v10, v10, v20

    const/16 v18, 0x4

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Leem;->a(I)V

    aget v18, v6, v4

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    add-long v12, v12, v18

    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    :cond_e
    new-instance v4, Leef;

    iget-object v5, v5, Leem;->a:[B

    array-length v5, v5

    invoke-direct/range {v4 .. v9}, Leef;-><init>(I[I[J[J[J)V

    move-object/from16 v0, p0

    iput-object v4, v0, Leek;->x:Leef;

    const/16 v4, 0x10

    goto/16 :goto_7

    :cond_f
    iget v5, v4, Leeh;->a:I

    const v7, 0x6d6f6f66

    if-ne v5, v7, :cond_11

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->b(Leeh;)V

    :cond_10
    :goto_c
    const/4 v4, 0x0

    goto/16 :goto_9

    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Leek;->j:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Leek;->j:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Leeh;

    invoke-virtual {v5, v4}, Leeh;->a(Leeg;)V

    goto :goto_c

    :cond_12
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(I)V

    move v4, v6

    goto/16 :goto_6

    .line 225
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->l:Leep;

    invoke-virtual/range {p1 .. p1}, Lefx;->a()J

    move-result-wide v6

    iget v5, v4, Leep;->i:I

    int-to-long v8, v5

    cmp-long v5, v6, v8

    if-gez v5, :cond_13

    const/4 v4, 0x0

    :goto_d
    if-nez v4, :cond_14

    const/4 v4, 0x1

    :goto_e
    or-int/2addr v4, v14

    move v14, v4

    .line 226
    goto/16 :goto_0

    .line 225
    :cond_13
    iget-object v5, v4, Leep;->j:Leem;

    iget-object v5, v5, Leem;->a:[B

    const/4 v6, 0x0

    iget v7, v4, Leep;->i:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v7}, Lefx;->a([BII)I

    iget-object v5, v4, Leep;->j:Leem;

    const/4 v6, 0x0

    iput v6, v5, Leem;->b:I

    const/4 v5, 0x0

    iput-boolean v5, v4, Leep;->k:Z

    const/4 v4, 0x1

    goto :goto_d

    :cond_14
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(I)V

    const/4 v4, 0x0

    goto :goto_e

    .line 228
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->l:Leep;

    iget-object v4, v4, Leep;->c:[I

    move-object/from16 v0, p0

    iget v5, v0, Leek;->t:I

    aget v5, v4, v5

    invoke-virtual/range {p1 .. p1}, Lefx;->a()J

    move-result-wide v6

    int-to-long v8, v5

    cmp-long v4, v6, v8

    if-gez v4, :cond_16

    const/4 v4, 0x1

    goto/16 :goto_1

    :cond_16
    move-object/from16 v0, p0

    iget v4, v0, Leek;->t:I

    move-object/from16 v0, p0

    iget v6, v0, Leek;->u:I

    if-ge v4, v6, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->l:Leep;

    iget-boolean v4, v4, Leep;->g:Z

    if-eqz v4, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->l:Leep;

    iget-object v6, v4, Leep;->j:Leem;

    move-object/from16 v0, p0

    iget-object v4, v0, Leek;->y:Leen;

    iget-object v4, v4, Leen;->d:[Leeo;

    move-object/from16 v0, p0

    iget-object v7, v0, Leek;->l:Leep;

    iget v7, v7, Leep;->a:I

    aget-object v4, v4, v7

    iget v4, v4, Leeo;->b:I

    move-object/from16 v0, p0

    iget-object v7, v0, Leek;->l:Leep;

    iget-object v7, v7, Leep;->h:[Z

    move-object/from16 v0, p0

    iget v8, v0, Leek;->t:I

    aget-boolean v7, v7, v8

    invoke-virtual {v6, v4}, Leem;->a(I)V

    if-eqz v7, :cond_18

    invoke-virtual {v6}, Leem;->b()I

    move-result v4

    :goto_f
    if-eqz v7, :cond_17

    mul-int/lit8 v4, v4, 0x6

    invoke-virtual {v6, v4}, Leem;->a(I)V

    :cond_17
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lefx;->a(I)I

    move-object/from16 v0, p0

    iget v4, v0, Leek;->t:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Leek;->t:I

    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Leek;->a(I)V

    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_18
    const/4 v4, 0x1

    goto :goto_f

    :cond_19
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v5, v2}, Leek;->a(Lefx;ILeby;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    goto/16 :goto_1

    .line 232
    :cond_1a
    return v14

    .line 217
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()Leef;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Leek;->x:Leef;

    return-object v0
.end method

.method public final a(JZ)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 240
    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    long-to-int v0, v2

    iput v0, p0, Leek;->s:I

    .line 241
    if-eqz p3, :cond_3

    iget-object v0, p0, Leek;->l:Leep;

    iget-object v0, p0, Leek;->l:Leep;

    iget v0, v0, Leep;->b:I

    if-lez v0, :cond_3

    iget v0, p0, Leek;->s:I

    int-to-long v2, v0

    iget-object v0, p0, Leek;->l:Leep;

    .line 242
    invoke-virtual {v0, v1}, Leep;->b(I)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-ltz v0, :cond_3

    iget v0, p0, Leek;->s:I

    int-to-long v2, v0

    iget-object v0, p0, Leek;->l:Leep;

    iget-object v4, p0, Leek;->l:Leep;

    iget v4, v4, Leep;->b:I

    add-int/lit8 v4, v4, -0x1

    .line 243
    invoke-virtual {v0, v4}, Leep;->b(I)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gtz v0, :cond_3

    move v0, v1

    move v2, v1

    move v3, v1

    .line 246
    :goto_0
    iget-object v4, p0, Leek;->l:Leep;

    iget v4, v4, Leep;->b:I

    if-ge v0, v4, :cond_2

    .line 247
    iget-object v4, p0, Leek;->l:Leep;

    invoke-virtual {v4, v0}, Leep;->b(I)J

    move-result-wide v4

    iget v6, p0, Leek;->s:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-gtz v4, :cond_1

    .line 248
    iget-object v3, p0, Leek;->l:Leep;

    iget-object v3, v3, Leep;->f:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_0

    move v2, v0

    :cond_0
    move v3, v0

    .line 246
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_2
    iget v0, p0, Leek;->v:I

    if-ne v2, v0, :cond_3

    iget v0, p0, Leek;->t:I

    if-lt v3, v0, :cond_3

    .line 255
    iput v1, p0, Leek;->s:I

    .line 262
    :goto_1
    return v1

    .line 259
    :cond_3
    iget-object v0, p0, Leek;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 260
    iget-object v0, p0, Leek;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 261
    invoke-direct {p0, v1}, Leek;->a(I)V

    .line 262
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lebv;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Leek;->y:Leen;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Leek;->y:Leen;

    iget-object v0, v0, Leen;->c:Lebv;

    goto :goto_0
.end method

.method public final d()Ljava/util/Map;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Leek;->w:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Leek;->w:Ljava/util/HashMap;

    goto :goto_0
.end method
