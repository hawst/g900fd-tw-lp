.class public final Lgyy;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;

.field private b:Lhog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5380
    invoke-direct {p0}, Lidf;-><init>()V

    .line 5383
    iput-object v0, p0, Lgyy;->a:Lhgz;

    .line 5386
    iput-object v0, p0, Lgyy;->b:Lhog;

    .line 5380
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 5429
    const/4 v0, 0x0

    .line 5430
    iget-object v1, p0, Lgyy;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 5431
    const/4 v0, 0x1

    iget-object v1, p0, Lgyy;->a:Lhgz;

    .line 5432
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5434
    :cond_0
    iget-object v1, p0, Lgyy;->b:Lhog;

    if-eqz v1, :cond_1

    .line 5435
    const/4 v1, 0x2

    iget-object v2, p0, Lgyy;->b:Lhog;

    .line 5436
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5438
    :cond_1
    iget-object v1, p0, Lgyy;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5439
    iput v0, p0, Lgyy;->J:I

    .line 5440
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 5376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyy;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyy;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyy;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgyy;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgyy;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lgyy;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgyy;->b:Lhog;

    if-nez v0, :cond_3

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lgyy;->b:Lhog;

    :cond_3
    iget-object v0, p0, Lgyy;->b:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 5417
    iget-object v0, p0, Lgyy;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 5418
    const/4 v0, 0x1

    iget-object v1, p0, Lgyy;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 5420
    :cond_0
    iget-object v0, p0, Lgyy;->b:Lhog;

    if-eqz v0, :cond_1

    .line 5421
    const/4 v0, 0x2

    iget-object v1, p0, Lgyy;->b:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 5423
    :cond_1
    iget-object v0, p0, Lgyy;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 5425
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5398
    if-ne p1, p0, :cond_1

    .line 5403
    :cond_0
    :goto_0
    return v0

    .line 5399
    :cond_1
    instance-of v2, p1, Lgyy;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 5400
    :cond_2
    check-cast p1, Lgyy;

    .line 5401
    iget-object v2, p0, Lgyy;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyy;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgyy;->b:Lhog;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyy;->b:Lhog;

    if-nez v2, :cond_3

    .line 5402
    :goto_2
    iget-object v2, p0, Lgyy;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgyy;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 5403
    goto :goto_0

    .line 5401
    :cond_4
    iget-object v2, p0, Lgyy;->a:Lhgz;

    iget-object v3, p1, Lgyy;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyy;->b:Lhog;

    iget-object v3, p1, Lgyy;->b:Lhog;

    .line 5402
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgyy;->I:Ljava/util/List;

    iget-object v3, p1, Lgyy;->I:Ljava/util/List;

    .line 5403
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5407
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5409
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyy;->a:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 5410
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyy;->b:Lhog;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5411
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgyy;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 5412
    return v0

    .line 5409
    :cond_0
    iget-object v0, p0, Lgyy;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5410
    :cond_1
    iget-object v0, p0, Lgyy;->b:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5411
    :cond_2
    iget-object v1, p0, Lgyy;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
