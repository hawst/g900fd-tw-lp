.class public Lefm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefc;


# static fields
.field public static final a:Legt;

.field private static final d:Ljava/util/regex/Pattern;


# instance fields
.field public b:Ljava/net/HttpURLConnection;

.field public c:J

.field private final e:I

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:Legt;

.field private final i:Ljava/util/HashMap;

.field private final j:Lega;

.field private k:Lefg;

.field private l:Ljava/io/InputStream;

.field private m:Z

.field private n:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lefn;

    invoke-direct {v0}, Lefn;-><init>()V

    sput-object v0, Lefm;->a:Legt;

    .line 117
    const-string v0, "^bytes (\\d+)-(\\d+)/(\\d+)$"

    .line 118
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lefm;->d:Ljava/util/regex/Pattern;

    .line 117
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Legt;Lega;II)V
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    invoke-static {p1}, La;->m(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lefm;->g:Ljava/lang/String;

    .line 172
    iput-object p2, p0, Lefm;->h:Legt;

    .line 173
    iput-object p3, p0, Lefm;->j:Lega;

    .line 174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lefm;->i:Ljava/util/HashMap;

    .line 175
    iput p4, p0, Lefm;->e:I

    .line 176
    iput p5, p0, Lefm;->f:I

    .line 177
    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;)J
    .locals 10

    .prologue
    .line 386
    const-wide/16 v0, -0x1

    .line 387
    const-string v2, "Content-Length"

    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 388
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 390
    :try_start_0
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 395
    :cond_0
    :goto_0
    const-string v2, "Content-Range"

    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 396
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 397
    sget-object v2, Lefm;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 398
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 400
    const/4 v3, 0x2

    .line 401
    :try_start_1
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    sub-long v2, v6, v2

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    .line 402
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-gez v6, :cond_2

    move-wide v0, v2

    .line 420
    :cond_1
    :goto_1
    return-wide v0

    .line 392
    :catch_0
    move-exception v2

    const-string v2, "HttpDataSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1c

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unexpected Content-Length ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 406
    :cond_2
    cmp-long v6, v0, v2

    if-eqz v6, :cond_1

    .line 411
    :try_start_2
    const-string v6, "HttpDataSource"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1a

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Inconsistent headers ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "] ["

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "]"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-wide v0

    goto :goto_1

    .line 416
    :catch_1
    move-exception v2

    const-string v2, "HttpDataSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unexpected Content-Range ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private b(Lefg;)Ljava/net/HttpURLConnection;
    .locals 8

    .prologue
    .line 360
    new-instance v0, Ljava/net/URL;

    iget-object v1, p1, Lefg;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 361
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 362
    iget v1, p0, Lefm;->e:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 363
    iget v1, p0, Lefm;->f:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 364
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 365
    iget-object v3, p0, Lefm;->i:Ljava/util/HashMap;

    monitor-enter v3

    .line 366
    :try_start_0
    iget-object v1, p0, Lefm;->i:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 367
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 370
    const-string v1, "Accept-Encoding"

    const-string v2, "deflate"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v1, "User-Agent"

    iget-object v2, p0, Lefm;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string v2, "Range"

    iget-wide v4, p1, Lefg;->d:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x1b

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "bytes="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v4, p1, Lefg;->e:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-wide v4, p1, Lefg;->d:J

    iget-wide v6, p1, Lefg;->e:J

    add-long/2addr v4, v6

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x14

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-virtual {v0, v2, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 374
    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 324
    const/4 v0, 0x0

    iput-object v0, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    .line 326
    :cond_0
    return-void
.end method


# virtual methods
.method public a([BII)I
    .locals 6

    .prologue
    .line 277
    :try_start_0
    iget-object v0, p0, Lefm;->l:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 284
    if-lez v0, :cond_1

    .line 285
    iget-wide v2, p0, Lefm;->c:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lefm;->c:J

    .line 286
    iget-object v1, p0, Lefm;->j:Lega;

    if-eqz v1, :cond_0

    .line 287
    iget-object v1, p0, Lefm;->j:Lega;

    invoke-interface {v1, v0}, Lega;->a(I)V

    .line 296
    :cond_0
    return v0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    new-instance v1, Lefo;

    iget-object v2, p0, Lefm;->k:Lefg;

    invoke-direct {v1, v0, v2}, Lefo;-><init>(Ljava/io/IOException;Lefg;)V

    throw v1

    .line 289
    :cond_1
    iget-wide v2, p0, Lefm;->n:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lefm;->n:J

    iget-wide v4, p0, Lefm;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 292
    new-instance v0, Lefo;

    new-instance v1, Legb;

    iget-wide v2, p0, Lefm;->n:J

    iget-wide v4, p0, Lefm;->c:J

    invoke-direct {v1, v2, v3, v4, v5}, Legb;-><init>(JJ)V

    iget-object v2, p0, Lefm;->k:Lefg;

    invoke-direct {v0, v1, v2}, Lefo;-><init>(Ljava/io/IOException;Lefg;)V

    throw v0
.end method

.method public a(Lefg;)J
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 218
    iput-object p1, p0, Lefm;->k:Lefg;

    .line 219
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lefm;->c:J

    .line 221
    :try_start_0
    invoke-direct {p0, p1}, Lefm;->b(Lefg;)Ljava/net/HttpURLConnection;

    move-result-object v0

    iput-object v0, p0, Lefm;->b:Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :try_start_1
    iget-object v0, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 235
    const/16 v1, 0xc8

    if-lt v0, v1, :cond_0

    const/16 v1, 0x12b

    if-le v0, v1, :cond_3

    .line 236
    :cond_0
    iget-object v1, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v1

    .line 237
    invoke-direct {p0}, Lefm;->c()V

    .line 238
    new-instance v2, Lefq;

    invoke-direct {v2, v0, v1, p1}, Lefq;-><init>(ILjava/util/Map;Lefg;)V

    throw v2

    .line 222
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 223
    new-instance v2, Lefo;

    const-string v3, "Unable to connect to "

    iget-object v0, p1, Lefg;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0, v1, p1}, Lefo;-><init>(Ljava/lang/String;Ljava/io/IOException;Lefg;)V

    throw v2

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 232
    new-instance v2, Lefo;

    const-string v3, "Unable to connect to "

    iget-object v0, p1, Lefg;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v0, v1, p1}, Lefo;-><init>(Ljava/lang/String;Ljava/io/IOException;Lefg;)V

    throw v2

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 242
    :cond_3
    iget-object v0, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 243
    iget-object v1, p0, Lefm;->h:Legt;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lefm;->h:Legt;

    invoke-interface {v1, v0}, Legt;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 244
    invoke-direct {p0}, Lefm;->c()V

    .line 245
    new-instance v1, Lefp;

    invoke-direct {v1, v0, p1}, Lefp;-><init>(Ljava/lang/String;Lefg;)V

    throw v1

    .line 248
    :cond_4
    iget-object v0, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    invoke-static {v0}, Lefm;->a(Ljava/net/HttpURLConnection;)J

    move-result-wide v2

    .line 249
    iget-wide v0, p1, Lefg;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    move-wide v0, v2

    :goto_2
    iput-wide v0, p0, Lefm;->n:J

    .line 251
    iget-wide v0, p1, Lefg;->e:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_6

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    iget-wide v0, p1, Lefg;->e:J

    cmp-long v0, v2, v0

    if-eqz v0, :cond_6

    .line 255
    invoke-direct {p0}, Lefm;->c()V

    .line 256
    new-instance v0, Lefo;

    new-instance v1, Legb;

    iget-wide v4, p1, Lefg;->e:J

    invoke-direct {v1, v4, v5, v2, v3}, Legb;-><init>(JJ)V

    invoke-direct {v0, v1, p1}, Lefo;-><init>(Ljava/io/IOException;Lefg;)V

    throw v0

    .line 249
    :cond_5
    iget-wide v0, p1, Lefg;->e:J

    goto :goto_2

    .line 261
    :cond_6
    :try_start_2
    iget-object v0, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lefm;->l:Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 267
    const/4 v0, 0x1

    iput-boolean v0, p0, Lefm;->m:Z

    .line 268
    iget-object v0, p0, Lefm;->j:Lega;

    if-eqz v0, :cond_7

    .line 269
    iget-object v0, p0, Lefm;->j:Lega;

    invoke-interface {v0}, Lega;->b()V

    .line 272
    :cond_7
    iget-wide v0, p0, Lefm;->n:J

    return-wide v0

    .line 262
    :catch_2
    move-exception v0

    .line 263
    invoke-direct {p0}, Lefm;->c()V

    .line 264
    new-instance v1, Lefo;

    invoke-direct {v1, v0, p1}, Lefo;-><init>(Ljava/io/IOException;Lefg;)V

    throw v1
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 302
    :try_start_0
    iget-object v0, p0, Lefm;->l:Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 304
    :try_start_1
    iget-object v0, p0, Lefm;->l:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lefm;->l:Ljava/io/InputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 311
    :cond_0
    iget-boolean v0, p0, Lefm;->m:Z

    if-eqz v0, :cond_2

    .line 312
    iput-boolean v3, p0, Lefm;->m:Z

    .line 313
    iget-object v0, p0, Lefm;->j:Lega;

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lefm;->j:Lega;

    invoke-interface {v0}, Lega;->c()V

    .line 316
    :cond_1
    invoke-direct {p0}, Lefm;->c()V

    .line 319
    :cond_2
    return-void

    .line 305
    :catch_0
    move-exception v0

    .line 306
    :try_start_3
    new-instance v1, Lefo;

    iget-object v2, p0, Lefm;->k:Lefg;

    invoke-direct {v1, v0, v2}, Lefo;-><init>(Ljava/io/IOException;Lefg;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 311
    :catchall_0
    move-exception v0

    iget-boolean v1, p0, Lefm;->m:Z

    if-eqz v1, :cond_4

    .line 312
    iput-boolean v3, p0, Lefm;->m:Z

    .line 313
    iget-object v1, p0, Lefm;->j:Lega;

    if-eqz v1, :cond_3

    .line 314
    iget-object v1, p0, Lefm;->j:Lega;

    invoke-interface {v1}, Lega;->c()V

    .line 316
    :cond_3
    invoke-direct {p0}, Lefm;->c()V

    :cond_4
    throw v0
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 356
    iget-wide v0, p0, Lefm;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lefm;->n:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lefm;->n:J

    iget-wide v2, p0, Lefm;->c:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method
