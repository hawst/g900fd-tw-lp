.class public final Lgyc;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgyc;


# instance fields
.field public b:Lgxw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1568
    const/4 v0, 0x0

    new-array v0, v0, [Lgyc;

    sput-object v0, Lgyc;->a:[Lgyc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1569
    invoke-direct {p0}, Lidf;-><init>()V

    .line 1572
    const/4 v0, 0x0

    iput-object v0, p0, Lgyc;->b:Lgxw;

    .line 1569
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1609
    const/4 v0, 0x0

    .line 1610
    iget-object v1, p0, Lgyc;->b:Lgxw;

    if-eqz v1, :cond_0

    .line 1611
    const v0, 0x3c7eeec

    iget-object v1, p0, Lgyc;->b:Lgxw;

    .line 1612
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1614
    :cond_0
    iget-object v1, p0, Lgyc;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1615
    iput v0, p0, Lgyc;->J:I

    .line 1616
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 1565
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyc;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyc;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyc;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgyc;->b:Lgxw;

    if-nez v0, :cond_2

    new-instance v0, Lgxw;

    invoke-direct {v0}, Lgxw;-><init>()V

    iput-object v0, p0, Lgyc;->b:Lgxw;

    :cond_2
    iget-object v0, p0, Lgyc;->b:Lgxw;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1e3f7762 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 1600
    iget-object v0, p0, Lgyc;->b:Lgxw;

    if-eqz v0, :cond_0

    .line 1601
    const v0, 0x3c7eeec

    iget-object v1, p0, Lgyc;->b:Lgxw;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 1603
    :cond_0
    iget-object v0, p0, Lgyc;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 1605
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1583
    if-ne p1, p0, :cond_1

    .line 1587
    :cond_0
    :goto_0
    return v0

    .line 1584
    :cond_1
    instance-of v2, p1, Lgyc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1585
    :cond_2
    check-cast p1, Lgyc;

    .line 1586
    iget-object v2, p0, Lgyc;->b:Lgxw;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyc;->b:Lgxw;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgyc;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyc;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 1587
    goto :goto_0

    .line 1586
    :cond_4
    iget-object v2, p0, Lgyc;->b:Lgxw;

    iget-object v3, p1, Lgyc;->b:Lgxw;

    invoke-virtual {v2, v3}, Lgxw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyc;->I:Ljava/util/List;

    iget-object v3, p1, Lgyc;->I:Ljava/util/List;

    .line 1587
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1591
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1593
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyc;->b:Lgxw;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1594
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgyc;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1595
    return v0

    .line 1593
    :cond_0
    iget-object v0, p0, Lgyc;->b:Lgxw;

    invoke-virtual {v0}, Lgxw;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1594
    :cond_1
    iget-object v1, p0, Lgyc;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
