.class public final Lcvu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlv;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lt;

.field public c:Lfkg;

.field private final d:Lfhz;

.field private final e:Lcws;

.field private final f:Lfdw;

.field private final g:Levn;

.field private h:Ldls;

.field private i:Ljava/lang/String;

.field private j:Lfqg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Ldls;

    .line 40
    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcvu;->a:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public constructor <init>(Lfhz;Lcws;Lfdw;Lt;Levn;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcvu;->d:Lfhz;

    .line 61
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcws;

    iput-object v0, p0, Lcvu;->e:Lcws;

    .line 62
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lcvu;->f:Lfdw;

    .line 63
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    iput-object v0, p0, Lcvu;->b:Lt;

    .line 64
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcvu;->g:Levn;

    .line 65
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcvu;->h:Ldls;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcvu;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvu;->i:Ljava/lang/String;

    iget-object v1, p0, Lcvu;->h:Ldls;

    .line 145
    iget-object v1, v1, Ldls;->W:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 149
    :cond_0
    iget-object v0, p0, Lcvu;->h:Ldls;

    invoke-virtual {v0}, Ldls;->a()V

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcvu;->h:Ldls;

    .line 152
    :cond_1
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 229
    iget-object v0, p0, Lcvu;->h:Ldls;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvu;->h:Ldls;

    iput-object v1, v0, Ldls;->Y:Ldlv;

    iput-object v1, p0, Lcvu;->h:Ldls;

    .line 230
    :cond_0
    if-eqz p1, :cond_1

    .line 231
    iget-object v0, p0, Lcvu;->e:Lcws;

    invoke-virtual {v0}, Lcws;->l()V

    .line 233
    :cond_1
    return-void
.end method

.method private c(IZ)V
    .locals 5

    .prologue
    .line 188
    iget-object v0, p0, Lcvu;->c:Lfkg;

    invoke-virtual {v0}, Lfkg;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 189
    const-string v0, "info card index (%d) out of range (size of list: %d)."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 190
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcvu;->c:Lfkg;

    invoke-virtual {v3}, Lfkg;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 189
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iget-object v0, p0, Lcvu;->c:Lfkg;

    invoke-virtual {v0}, Lfkg;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkf;

    .line 194
    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lfkf;->d()Lfmu;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcvu;->f:Lfdw;

    iget-object v2, p0, Lcvu;->j:Lfqg;

    invoke-virtual {v0}, Lfkf;->d()Lfmu;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lfdw;->a(Lfqg;Lfqh;Lhcq;)V

    .line 196
    iget-object v1, p0, Lcvu;->g:Levn;

    new-instance v2, Lcyz;

    invoke-virtual {v0}, Lfkf;->d()Lfmu;

    move-result-object v0

    iget-object v0, v0, Lfmu;->a:Lhvx;

    iget-object v0, v0, Lhvx;->a:[Ljava/lang/String;

    invoke-direct {v2, v0}, Lcyz;-><init>([Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 202
    iget-object v0, p0, Lcvu;->c:Lfkg;

    invoke-virtual {v0}, Lfkg;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "unexpected info card id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v0, p0, Lcvu;->c:Lfkg;

    invoke-virtual {v0}, Lfkg;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkf;

    .line 208
    invoke-virtual {v0}, Lfkf;->b()Lfmv;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 209
    iget-object v1, p0, Lcvu;->g:Levn;

    new-instance v2, Lcyz;

    invoke-virtual {v0}, Lfkf;->b()Lfmv;

    move-result-object v3

    iget-object v3, v3, Lfmv;->a:Lhvy;

    iget-object v3, v3, Lhvy;->f:[Ljava/lang/String;

    invoke-direct {v2, v3}, Lcyz;-><init>([Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Levn;->d(Ljava/lang/Object;)V

    .line 212
    :cond_2
    iget-object v1, p0, Lcvu;->j:Lfqg;

    if-eqz v1, :cond_0

    .line 213
    invoke-virtual {v0}, Lfkf;->b()Lfmv;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 214
    iget-object v1, p0, Lcvu;->f:Lfdw;

    iget-object v2, p0, Lcvu;->j:Lfqg;

    .line 215
    invoke-virtual {v0}, Lfkf;->b()Lfmv;

    move-result-object v3

    .line 214
    invoke-virtual {v1, v2, v3, v4}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    .line 217
    :cond_3
    invoke-virtual {v0}, Lfkf;->c()Lfmt;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 218
    iget-object v1, p0, Lcvu;->f:Lfdw;

    iget-object v2, p0, Lcvu;->j:Lfqg;

    .line 219
    invoke-virtual {v0}, Lfkf;->c()Lfmt;

    move-result-object v3

    .line 218
    invoke-virtual {v1, v2, v3, v4}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    .line 221
    :cond_4
    invoke-virtual {v0}, Lfkf;->d()Lfmu;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 222
    iget-object v1, p0, Lcvu;->f:Lfdw;

    iget-object v2, p0, Lcvu;->j:Lfqg;

    .line 223
    invoke-virtual {v0}, Lfkf;->d()Lfmu;

    move-result-object v0

    .line 222
    invoke-virtual {v1, v2, v0, v4}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    goto :goto_0
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0, p2}, Lcvu;->a(Z)V

    .line 178
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcvu;->c(IZ)V

    .line 179
    return-void
.end method

.method public a(Ldls;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcvu;->h:Ldls;

    .line 170
    if-eqz p1, :cond_0

    .line 171
    iput-object p0, p1, Ldls;->Y:Ldlv;

    .line 173
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 237
    iget-object v0, p0, Lcvu;->c:Lfkg;

    invoke-virtual {v0}, Lfkg;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 238
    const-string v0, "Info Card Action: unexpected card id: %d. Action ignored"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    iget-object v0, p0, Lcvu;->c:Lfkg;

    invoke-virtual {v0}, Lfkg;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkf;

    .line 242
    invoke-virtual {v0}, Lfkf;->c()Lfmt;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 243
    iget-object v3, p0, Lcvu;->g:Levn;

    new-instance v4, Lcyz;

    invoke-virtual {v0}, Lfkf;->c()Lfmt;

    move-result-object v5

    iget-object v5, v5, Lfmt;->a:Lhvw;

    iget-object v5, v5, Lhvw;->c:[Ljava/lang/String;

    invoke-direct {v4, v5}, Lcyz;-><init>([Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Levn;->d(Ljava/lang/Object;)V

    .line 244
    iget-object v3, p0, Lcvu;->d:Lfhz;

    invoke-virtual {v0}, Lfkf;->c()Lfmt;

    move-result-object v4

    iget-object v4, v4, Lfmt;->a:Lhvw;

    iget-object v4, v4, Lhvw;->b:Lhog;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    .line 245
    invoke-virtual {v0}, Lfkf;->c()Lfmt;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v3, v0, Lfmt;->a:Lhvw;

    iget-object v3, v3, Lhvw;->b:Lhog;

    if-eqz v3, :cond_4

    iget-object v0, v0, Lfmt;->a:Lhvw;

    iget-object v0, v0, Lhvw;->b:Lhog;

    iget-object v0, v0, Lhog;->C:Licv;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcvu;->b:Lt;

    sget-object v1, Lcvu;->a:Ljava/lang/String;

    .line 247
    invoke-virtual {v0, v1}, Lt;->a(Ljava/lang/String;)Lj;

    move-result-object v0

    check-cast v0, Li;

    .line 248
    if-eqz v0, :cond_2

    .line 249
    invoke-virtual {v0}, Li;->a()V

    .line 251
    :cond_2
    invoke-direct {p0, v2}, Lcvu;->a(Z)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 245
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final b(IZ)V
    .locals 1

    .prologue
    .line 183
    invoke-direct {p0, p2}, Lcvu;->a(Z)V

    .line 184
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcvu;->c(IZ)V

    .line 185
    return-void
.end method

.method public final handleInfoCardLoadEvent(Ldag;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p1, Ldag;->a:Ljava/lang/String;

    iput-object v0, p0, Lcvu;->i:Ljava/lang/String;

    .line 99
    invoke-direct {p0}, Lcvu;->a()V

    .line 100
    iget-object v0, p1, Ldag;->b:Lfkg;

    iput-object v0, p0, Lcvu;->c:Lfkg;

    .line 101
    iget-object v0, p1, Ldag;->c:Lfqg;

    iput-object v0, p0, Lcvu;->j:Lfqg;

    .line 102
    return-void
.end method

.method public final handleInfoCardTeaserClickEvent(Ldah;)V
    .locals 5
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 120
    iget-object v0, p1, Ldah;->a:Lfkf;

    .line 121
    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {v0}, Lfkf;->a()Lfmw;

    move-result-object v1

    .line 124
    iget-object v2, p0, Lcvu;->g:Levn;

    new-instance v3, Lcyz;

    iget-object v4, v1, Lfmw;->a:Lhvz;

    iget-object v4, v4, Lhvz;->e:[Ljava/lang/String;

    invoke-direct {v3, v4}, Lcyz;-><init>([Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Levn;->d(Ljava/lang/Object;)V

    .line 126
    iget-object v2, p1, Ldah;->b:Lfqg;

    if-eqz v2, :cond_0

    .line 127
    iget-object v2, p0, Lcvu;->f:Lfdw;

    .line 128
    iget-object v3, p1, Ldah;->b:Lfqg;

    const/4 v4, 0x0

    .line 127
    invoke-virtual {v2, v3, v1, v4}, Lfdw;->a(Lfqg;Lfqh;Lhcq;)V

    .line 133
    :cond_0
    iget-object v1, p0, Lcvu;->c:Lfkg;

    if-nez v1, :cond_1

    .line 134
    const-string v0, "Failed to show info card gallery - missing infoCardCollection"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 141
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v1, p0, Lcvu;->e:Lcws;

    iget-object v1, v1, Lcws;->c:Lgeh;

    invoke-interface {v1}, Lgeh;->m()Z

    move-result v1

    .line 138
    iget-object v2, p0, Lcvu;->e:Lcws;

    invoke-virtual {v2}, Lcws;->m()V

    .line 139
    iget-object v2, p0, Lcvu;->c:Lfkg;

    invoke-virtual {v2}, Lfkg;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 140
    iget-object v2, p0, Lcvu;->i:Ljava/lang/String;

    iget-object v3, p0, Lcvu;->c:Lfkg;

    invoke-static {v2, v3, v0, v1}, Ldls;->a(Ljava/lang/String;Lfkg;IZ)Ldls;

    move-result-object v0

    iget-object v1, p0, Lcvu;->b:Lt;

    invoke-virtual {v1}, Lt;->a()Laf;

    move-result-object v1

    iget-object v2, p0, Lcvu;->b:Lt;

    sget-object v3, Lcvu;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lt;->a(Ljava/lang/String;)Lj;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v2}, Laf;->a(Lj;)Laf;

    :cond_2
    sget-object v2, Lcvu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ldls;->a(Laf;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcvu;->a(Ldls;)V

    goto :goto_0
.end method

.method public final handleInfoCardTeaserExpandEvent(Ldai;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 106
    iget-object v0, p1, Ldai;->a:Lfkf;

    invoke-virtual {v0}, Lfkf;->a()Lfmw;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcvu;->g:Levn;

    new-instance v2, Lcyz;

    iget-object v3, v0, Lfmw;->a:Lhvz;

    iget-object v3, v3, Lhvz;->c:[Ljava/lang/String;

    invoke-direct {v2, v3}, Lcyz;-><init>([Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Levn;->d(Ljava/lang/Object;)V

    .line 111
    iget-object v1, p1, Ldai;->b:Lfqg;

    if-eqz v1, :cond_0

    .line 112
    iget-object v1, p0, Lcvu;->f:Lfdw;

    .line 113
    iget-object v2, p1, Ldai;->b:Lfqg;

    const/4 v3, 0x0

    .line 112
    invoke-virtual {v1, v2, v0, v3}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    .line 115
    :cond_0
    return-void
.end method

.method public final handleVideoStageEvent(Ldac;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 82
    iget-object v1, p1, Ldac;->a:Lgol;

    sget-object v2, Lgol;->a:Lgol;

    if-ne v1, v2, :cond_1

    .line 83
    iput-object v0, p0, Lcvu;->i:Ljava/lang/String;

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v1, p1, Ldac;->a:Lgol;

    invoke-virtual {v1}, Lgol;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 87
    iget-object v1, p1, Ldac;->d:Lfoy;

    if-eqz v1, :cond_2

    iget-object v0, p1, Ldac;->d:Lfoy;

    iget-object v0, v0, Lfoy;->c:Ljava/lang/String;

    :cond_2
    iput-object v0, p0, Lcvu;->i:Ljava/lang/String;

    .line 88
    invoke-direct {p0}, Lcvu;->a()V

    goto :goto_0

    .line 89
    :cond_3
    iget-object v1, p1, Ldac;->a:Lgol;

    sget-object v2, Lgol;->b:Lgol;

    invoke-virtual {v1, v2}, Lgol;->a(Lgol;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p1, Ldac;->b:Lfrl;

    if-eqz v1, :cond_4

    .line 91
    iget-object v0, p1, Ldac;->b:Lfrl;

    iget-object v0, v0, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    iput-object v0, p0, Lcvu;->i:Ljava/lang/String;

    .line 92
    invoke-direct {p0}, Lcvu;->a()V

    goto :goto_0
.end method
