.class public final Lax;
.super Lbn;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Bundle;

.field private final b:[Lbs;

.field private c:I

.field private d:Ljava/lang/CharSequence;

.field private e:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2135
    new-instance v0, Lbo;

    invoke-direct {v0}, Lbo;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 1791
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lax;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lbs;)V

    .line 1792
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lbs;)V
    .locals 1

    .prologue
    .line 1795
    invoke-direct {p0}, Lbn;-><init>()V

    .line 1796
    iput p1, p0, Lax;->c:I

    .line 1797
    invoke-static {p2}, Lba;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lax;->d:Ljava/lang/CharSequence;

    .line 1798
    iput-object p3, p0, Lax;->e:Landroid/app/PendingIntent;

    .line 1799
    iput-object p4, p0, Lax;->a:Landroid/os/Bundle;

    .line 1800
    const/4 v0, 0x0

    iput-object v0, p0, Lax;->b:[Lbs;

    .line 1801
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 1805
    iget v0, p0, Lax;->c:I

    return v0
.end method

.method protected final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1810
    iget-object v0, p0, Lax;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected final c()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 1815
    iget-object v0, p0, Lax;->e:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1823
    iget-object v0, p0, Lax;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method public final bridge synthetic e()[Lbx;
    .locals 1

    .prologue
    .line 1772
    iget-object v0, p0, Lax;->b:[Lbs;

    return-object v0
.end method
