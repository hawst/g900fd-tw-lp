.class final Lbrn;
.super Lbrm;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0, p1, p2}, Lbrm;-><init>(Landroid/view/View;Z)V

    .line 221
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 251
    iget-object v0, p0, Lbrn;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 252
    iget-object v0, p0, Lbrn;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 253
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_2

    .line 254
    aget-object v1, v2, v0

    if-eqz v1, :cond_0

    .line 255
    aget-object v3, v2, v0

    if-eqz p1, :cond_1

    const/16 v1, 0xff

    :goto_1
    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 253
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 255
    :cond_1
    const/16 v1, 0x33

    goto :goto_1

    .line 259
    :cond_2
    return-void
.end method

.method protected final c(ILfkn;)V
    .locals 3

    .prologue
    .line 225
    invoke-super {p0, p1, p2}, Lbrm;->c(ILfkn;)V

    .line 226
    packed-switch p1, :pswitch_data_0

    .line 229
    iget-object v0, p0, Lbrn;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v1, p0, Lbrn;->a:Z

    if-eqz v1, :cond_0

    .line 231
    invoke-virtual {p2}, Lfkn;->b()Ljava/lang/CharSequence;

    move-result-object v1

    .line 229
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    :goto_1
    return-void

    .line 232
    :cond_0
    invoke-virtual {p2}, Lfkn;->a()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 235
    :pswitch_0
    iget-object v0, p0, Lbrn;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v1, p0, Lbrn;->a:Z

    if-eqz v1, :cond_3

    .line 237
    iget-object v1, p2, Lfkn;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfkn;->a:Lhlk;

    iget v1, v1, Lhlk;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p2, Lfkn;->a:Lhlk;

    iget-object v1, v1, Lhlk;->h:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkn;->c:Ljava/lang/CharSequence;

    :cond_1
    :goto_2
    iget-object v1, p2, Lfkn;->c:Ljava/lang/CharSequence;

    .line 235
    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 237
    :cond_2
    iget-object v1, p2, Lfkn;->a:Lhlk;

    iget-object v1, v1, Lhlk;->i:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkn;->c:Ljava/lang/CharSequence;

    goto :goto_2

    .line 238
    :cond_3
    invoke-virtual {p2}, Lfkn;->a()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_3

    .line 241
    :pswitch_1
    iget-object v0, p0, Lbrn;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v1, p0, Lbrn;->a:Z

    if-eqz v1, :cond_4

    .line 243
    invoke-virtual {p2}, Lfkn;->b()Ljava/lang/CharSequence;

    move-result-object v1

    .line 241
    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 244
    :cond_4
    iget-object v1, p2, Lfkn;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_5

    iget-object v1, p2, Lfkn;->a:Lhlk;

    iget v1, v1, Lhlk;->b:I

    if-nez v1, :cond_6

    iget-object v1, p2, Lfkn;->a:Lhlk;

    iget-object v1, v1, Lhlk;->d:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkn;->b:Ljava/lang/CharSequence;

    :cond_5
    :goto_5
    iget-object v1, p2, Lfkn;->b:Ljava/lang/CharSequence;

    goto :goto_4

    :cond_6
    iget-object v1, p2, Lfkn;->a:Lhlk;

    iget-object v1, v1, Lhlk;->e:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkn;->b:Ljava/lang/CharSequence;

    goto :goto_5

    .line 226
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
