.class final Lfrq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lfrp;


# direct methods
.method constructor <init>(Lfrp;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lfrq;->a:Lfrp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 57
    iget-object v1, p0, Lfrq;->a:Lfrp;

    invoke-virtual {v1}, Lfrp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v2, Lezm;

    invoke-direct {v2}, Lezm;-><init>()V

    :goto_0
    :try_start_0
    iget-object v0, v1, Lfrp;->a:Leme;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v1, Lfrp;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Leme;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, Lfrp;->c:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "gcm_registration_id"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    invoke-virtual {v1}, Lfrp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "Could not register with InnerTube because no GCM registration ID was found."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 58
    :cond_1
    :goto_1
    return-void

    .line 57
    :catch_0
    move-exception v0

    const-string v3, "Could not register with GCM: "

    invoke-static {v3, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2}, Lezm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_2
    new-instance v2, Lezm;

    invoke-direct {v2}, Lezm;-><init>()V

    iget-object v3, v1, Lfrp;->d:Lfem;

    new-instance v4, Lfen;

    iget-object v5, v3, Lfem;->b:Lfsz;

    iget-object v3, v3, Lfem;->c:Lgix;

    invoke-interface {v3}, Lgix;->d()Lgit;

    move-result-object v3

    invoke-direct {v4, v5, v3}, Lfen;-><init>(Lfsz;Lgit;)V

    iget-object v3, v4, Lfen;->a:Lhor;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, v3, Lhor;->a:[B

    iget-object v0, v1, Lfrp;->e:Lfrr;

    invoke-virtual {v0}, Lfrr;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v4, Lfen;->a:Lhor;

    iput-boolean v6, v0, Lhor;->b:Z

    :cond_3
    :goto_2
    :try_start_1
    iget-object v0, v1, Lfrp;->d:Lfem;

    iget-object v0, v0, Lfem;->e:Lfco;

    invoke-virtual {v0, v4}, Lfco;->a(Lfsp;)Lidh;

    move-result-object v0

    check-cast v0, Lhuw;
    :try_end_1
    .catch Lfdv; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v3, "Could not register for notifications with InnerTube: "

    invoke-static {v3, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2}, Lezm;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_2

    :catch_2
    move-exception v0

    const-string v3, "Could not register for notifications with InnerTube: "

    invoke-static {v3, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2}, Lezm;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_2
.end method
