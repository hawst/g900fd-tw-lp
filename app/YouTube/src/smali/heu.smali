.class public final Lheu;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhxf;

.field public b:Lhgz;

.field public c:Lhut;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41504
    invoke-direct {p0}, Lidf;-><init>()V

    .line 41507
    iput-object v0, p0, Lheu;->a:Lhxf;

    .line 41510
    iput-object v0, p0, Lheu;->b:Lhgz;

    .line 41513
    iput-object v0, p0, Lheu;->c:Lhut;

    .line 41504
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 41562
    const/4 v0, 0x0

    .line 41563
    iget-object v1, p0, Lheu;->a:Lhxf;

    if-eqz v1, :cond_0

    .line 41564
    const/4 v0, 0x1

    iget-object v1, p0, Lheu;->a:Lhxf;

    .line 41565
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 41567
    :cond_0
    iget-object v1, p0, Lheu;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 41568
    const/4 v1, 0x2

    iget-object v2, p0, Lheu;->b:Lhgz;

    .line 41569
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41571
    :cond_1
    iget-object v1, p0, Lheu;->c:Lhut;

    if-eqz v1, :cond_2

    .line 41572
    const/4 v1, 0x3

    iget-object v2, p0, Lheu;->c:Lhut;

    .line 41573
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41575
    :cond_2
    iget-object v1, p0, Lheu;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41576
    iput v0, p0, Lheu;->J:I

    .line 41577
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 41500
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lheu;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lheu;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lheu;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lheu;->a:Lhxf;

    if-nez v0, :cond_2

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lheu;->a:Lhxf;

    :cond_2
    iget-object v0, p0, Lheu;->a:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lheu;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lheu;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lheu;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lheu;->c:Lhut;

    if-nez v0, :cond_4

    new-instance v0, Lhut;

    invoke-direct {v0}, Lhut;-><init>()V

    iput-object v0, p0, Lheu;->c:Lhut;

    :cond_4
    iget-object v0, p0, Lheu;->c:Lhut;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 41547
    iget-object v0, p0, Lheu;->a:Lhxf;

    if-eqz v0, :cond_0

    .line 41548
    const/4 v0, 0x1

    iget-object v1, p0, Lheu;->a:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41550
    :cond_0
    iget-object v0, p0, Lheu;->b:Lhgz;

    if-eqz v0, :cond_1

    .line 41551
    const/4 v0, 0x2

    iget-object v1, p0, Lheu;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41553
    :cond_1
    iget-object v0, p0, Lheu;->c:Lhut;

    if-eqz v0, :cond_2

    .line 41554
    const/4 v0, 0x3

    iget-object v1, p0, Lheu;->c:Lhut;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41556
    :cond_2
    iget-object v0, p0, Lheu;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 41558
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41526
    if-ne p1, p0, :cond_1

    .line 41532
    :cond_0
    :goto_0
    return v0

    .line 41527
    :cond_1
    instance-of v2, p1, Lheu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 41528
    :cond_2
    check-cast p1, Lheu;

    .line 41529
    iget-object v2, p0, Lheu;->a:Lhxf;

    if-nez v2, :cond_4

    iget-object v2, p1, Lheu;->a:Lhxf;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lheu;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lheu;->b:Lhgz;

    if-nez v2, :cond_3

    .line 41530
    :goto_2
    iget-object v2, p0, Lheu;->c:Lhut;

    if-nez v2, :cond_6

    iget-object v2, p1, Lheu;->c:Lhut;

    if-nez v2, :cond_3

    .line 41531
    :goto_3
    iget-object v2, p0, Lheu;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lheu;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 41532
    goto :goto_0

    .line 41529
    :cond_4
    iget-object v2, p0, Lheu;->a:Lhxf;

    iget-object v3, p1, Lheu;->a:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lheu;->b:Lhgz;

    iget-object v3, p1, Lheu;->b:Lhgz;

    .line 41530
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lheu;->c:Lhut;

    iget-object v3, p1, Lheu;->c:Lhut;

    .line 41531
    invoke-virtual {v2, v3}, Lhut;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lheu;->I:Ljava/util/List;

    iget-object v3, p1, Lheu;->I:Ljava/util/List;

    .line 41532
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 41536
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 41538
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheu;->a:Lhxf;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 41539
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheu;->b:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 41540
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheu;->c:Lhut;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 41541
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lheu;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 41542
    return v0

    .line 41538
    :cond_0
    iget-object v0, p0, Lheu;->a:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_0

    .line 41539
    :cond_1
    iget-object v0, p0, Lheu;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 41540
    :cond_2
    iget-object v0, p0, Lheu;->c:Lhut;

    invoke-virtual {v0}, Lhut;->hashCode()I

    move-result v0

    goto :goto_2

    .line 41541
    :cond_3
    iget-object v1, p0, Lheu;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
