.class public final Lipe;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic i:Z


# instance fields
.field a:J

.field b:J

.field final c:I

.field final d:Lios;

.field e:Ljava/util/List;

.field public final f:Lipg;

.field final g:Lipf;

.field public final h:Liph;

.field private final j:Liph;

.field private k:Linr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lipe;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lipe;->i:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(ILios;ZZLjava/util/List;)V
    .locals 4

    .prologue
    const/high16 v2, 0x10000

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lipe;->a:J

    .line 56
    new-instance v0, Liph;

    invoke-direct {v0, p0}, Liph;-><init>(Lipe;)V

    iput-object v0, p0, Lipe;->h:Liph;

    .line 67
    new-instance v0, Liph;

    invoke-direct {v0, p0}, Liph;-><init>(Lipe;)V

    iput-object v0, p0, Lipe;->j:Liph;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lipe;->k:Linr;

    .line 78
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "connection == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    if-nez p5, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "requestHeaders == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_1
    iput p1, p0, Lipe;->c:I

    .line 81
    iput-object p2, p0, Lipe;->d:Lios;

    .line 82
    iget-object v0, p2, Lios;->f:Lioo;

    invoke-virtual {v0, v2}, Lioo;->c(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lipe;->b:J

    .line 84
    new-instance v0, Lipg;

    iget-object v1, p2, Lios;->e:Lioo;

    invoke-virtual {v1, v2}, Lioo;->c(I)I

    move-result v1

    int-to-long v2, v1

    invoke-direct {v0, p0, v2, v3}, Lipg;-><init>(Lipe;J)V

    iput-object v0, p0, Lipe;->f:Lipg;

    .line 86
    new-instance v0, Lipf;

    invoke-direct {v0, p0}, Lipf;-><init>(Lipe;)V

    iput-object v0, p0, Lipe;->g:Lipf;

    .line 87
    iget-object v0, p0, Lipe;->f:Lipg;

    invoke-static {v0, p4}, Lipg;->a(Lipg;Z)Z

    .line 88
    iget-object v0, p0, Lipe;->g:Lipf;

    invoke-static {v0, p3}, Lipf;->a(Lipf;Z)Z

    .line 89
    return-void
.end method

.method static synthetic a(Lipe;)Lios;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lipe;->d:Lios;

    return-object v0
.end method

.method static synthetic b(Lipe;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lipe;->c:I

    return v0
.end method

.method static synthetic c(Lipe;)Liph;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lipe;->h:Liph;

    return-object v0
.end method

.method static synthetic d(Lipe;)Linr;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lipe;->k:Linr;

    return-object v0
.end method

.method private d(Linr;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 238
    sget-boolean v1, Lipe;->i:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 239
    :cond_0
    monitor-enter p0

    .line 240
    :try_start_0
    iget-object v1, p0, Lipe;->k:Linr;

    if-eqz v1, :cond_1

    .line 241
    monitor-exit p0

    .line 250
    :goto_0
    return v0

    .line 243
    :cond_1
    iget-object v1, p0, Lipe;->f:Lipg;

    invoke-static {v1}, Lipg;->a(Lipg;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lipe;->g:Lipf;

    invoke-static {v1}, Lipf;->a(Lipf;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 244
    monitor-exit p0

    goto :goto_0

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 246
    :cond_2
    :try_start_1
    iput-object p1, p0, Lipe;->k:Linr;

    .line 247
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 248
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    iget-object v0, p0, Lipe;->d:Lios;

    iget v1, p0, Lipe;->c:I

    invoke-virtual {v0, v1}, Lios;->b(I)Lipe;

    .line 250
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Lipe;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lipe;->f()V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 556
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    return-void

    .line 558
    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0
.end method

.method static synthetic f(Lipe;)V
    .locals 2

    .prologue
    .line 34
    sget-boolean v0, Lipe;->i:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lipe;->f:Lipg;

    invoke-static {v0}, Lipg;->a(Lipg;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lipe;->f:Lipg;

    invoke-static {v0}, Lipg;->b(Lipg;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lipe;->g:Lipf;

    invoke-static {v0}, Lipf;->a(Lipf;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lipe;->g:Lipf;

    invoke-static {v0}, Lipf;->b(Lipf;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lipe;->a()Z

    move-result v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    sget-object v0, Linr;->h:Linr;

    invoke-virtual {p0, v0}, Lipe;->a(Linr;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    if-nez v1, :cond_2

    iget-object v0, p0, Lipe;->d:Lios;

    iget v1, p0, Lipe;->c:I

    invoke-virtual {v0, v1}, Lios;->b(I)Lipe;

    goto :goto_1
.end method

.method static synthetic g(Lipe;)Liph;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lipe;->j:Liph;

    return-object v0
.end method

.method static synthetic h(Lipe;)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lipe;->g:Lipf;

    invoke-static {v0}, Lipf;->b(Lipf;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lipe;->g:Lipf;

    invoke-static {v0}, Lipf;->a(Lipf;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream finished"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lipe;->k:Linr;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lipe;->k:Linr;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "stream was reset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method


# virtual methods
.method final a(J)V
    .locals 3

    .prologue
    .line 536
    iget-wide v0, p0, Lipe;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lipe;->b:J

    .line 537
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 538
    :cond_0
    return-void
.end method

.method public final a(Linr;)V
    .locals 2

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lipe;->d(Linr;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lipe;->d:Lios;

    iget v1, p0, Lipe;->c:I

    invoke-virtual {v0, v1, p1}, Lios;->b(ILinr;)V

    goto :goto_0
.end method

.method public final declared-synchronized a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 107
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lipe;->k:Linr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 115
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 110
    :cond_1
    :try_start_1
    iget-object v1, p0, Lipe;->f:Lipg;

    invoke-static {v1}, Lipg;->a(Lipg;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lipe;->f:Lipg;

    invoke-static {v1}, Lipg;->b(Lipg;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lipe;->g:Lipf;

    invoke-static {v1}, Lipf;->a(Lipf;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lipe;->g:Lipf;

    invoke-static {v1}, Lipf;->b(Lipf;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lipe;->e:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    .line 115
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Linr;)V
    .locals 2

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lipe;->d(Linr;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lipe;->d:Lios;

    iget v1, p0, Lipe;->c:I

    invoke-virtual {v0, v1, p1}, Lios;->a(ILinr;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 120
    iget v0, p0, Lipe;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 121
    :goto_0
    iget-object v3, p0, Lipe;->d:Lios;

    iget-boolean v3, v3, Lios;->b:Z

    if-ne v3, v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 120
    goto :goto_0

    :cond_1
    move v1, v2

    .line 121
    goto :goto_1
.end method

.method public final declared-synchronized c()Ljava/util/List;
    .locals 4

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lipe;->h:Liph;

    invoke-virtual {v0}, Liph;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 139
    :goto_0
    :try_start_1
    iget-object v0, p0, Lipe;->e:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lipe;->k:Linr;

    if-nez v0, :cond_0

    .line 140
    invoke-direct {p0}, Lipe;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lipe;->h:Liph;

    invoke-virtual {v1}, Liph;->b()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 137
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 143
    :cond_0
    :try_start_3
    iget-object v0, p0, Lipe;->h:Liph;

    invoke-virtual {v0}, Liph;->b()V

    .line 145
    iget-object v0, p0, Lipe;->e:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lipe;->e:Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit p0

    return-object v0

    .line 146
    :cond_1
    :try_start_4
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lipe;->k:Linr;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "stream was reset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
.end method

.method final declared-synchronized c(Linr;)V
    .locals 1

    .prologue
    .line 303
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lipe;->k:Linr;

    if-nez v0, :cond_0

    .line 304
    iput-object p1, p0, Lipe;->k:Linr;

    .line 305
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    :cond_0
    monitor-exit p0

    return-void

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Liqk;
    .locals 2

    .prologue
    .line 206
    monitor-enter p0

    .line 207
    :try_start_0
    iget-object v0, p0, Lipe;->e:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lipe;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "reply before requesting the sink"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    iget-object v0, p0, Lipe;->g:Lipf;

    return-object v0
.end method

.method final e()V
    .locals 2

    .prologue
    .line 290
    sget-boolean v0, Lipe;->i:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 292
    :cond_0
    monitor-enter p0

    .line 293
    :try_start_0
    iget-object v0, p0, Lipe;->f:Lipg;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lipg;->a(Lipg;Z)Z

    .line 294
    invoke-virtual {p0}, Lipe;->a()Z

    move-result v0

    .line 295
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 296
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    if-nez v0, :cond_1

    .line 298
    iget-object v0, p0, Lipe;->d:Lios;

    iget v1, p0, Lipe;->c:I

    invoke-virtual {v0, v1}, Lios;->b(I)Lipe;

    .line 300
    :cond_1
    return-void

    .line 296
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
