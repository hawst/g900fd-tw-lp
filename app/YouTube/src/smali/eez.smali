.class public final Leez;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leex;


# instance fields
.field public final a:I

.field private b:I

.field private c:I

.field private d:[[B


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->b(Z)V

    .line 36
    iput p1, p0, Leez;->a:I

    .line 37
    const/16 v0, 0x64

    new-array v0, v0, [[B

    iput-object v0, p0, Leez;->d:[[B

    .line 38
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 2

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    iget v0, p0, Leez;->b:I

    iget v1, p0, Leez;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    mul-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Leew;
    .locals 2

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    new-instance v0, Lefa;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Leez;->a(I[[B)[[B

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lefa;-><init>(Leez;[[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lefa;)V
    .locals 7

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lefa;->a:[[B

    .line 99
    iget v1, p0, Leez;->b:I

    array-length v2, v0

    sub-int/2addr v1, v2

    iput v1, p0, Leez;->b:I

    .line 101
    iget v1, p0, Leez;->c:I

    array-length v2, v0

    add-int/2addr v1, v2

    .line 102
    iget-object v2, p0, Leez;->d:[[B

    array-length v2, v2

    if-ge v2, v1, :cond_1

    .line 104
    shl-int/lit8 v2, v1, 0x1

    new-array v2, v2, [[B

    .line 105
    iget v3, p0, Leez;->c:I

    if-lez v3, :cond_0

    .line 106
    iget-object v3, p0, Leez;->d:[[B

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Leez;->c:I

    invoke-static {v3, v4, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 108
    :cond_0
    iput-object v2, p0, Leez;->d:[[B

    .line 110
    :cond_1
    const/4 v2, 0x0

    iget-object v3, p0, Leez;->d:[[B

    iget v4, p0, Leez;->c:I

    array-length v5, v0

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 111
    iput v1, p0, Leez;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(I[[B)[[B
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 70
    monitor-enter p0

    int-to-long v2, p1

    :try_start_0
    iget v0, p0, Leez;->a:I

    int-to-long v4, v0

    add-long/2addr v2, v4

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    iget v0, p0, Leez;->a:I

    int-to-long v4, v0

    div-long/2addr v2, v4

    long-to-int v3, v2

    .line 71
    if-eqz p2, :cond_0

    array-length v0, p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt v3, v0, :cond_0

    .line 89
    :goto_0
    monitor-exit p0

    return-object p2

    .line 76
    :cond_0
    :try_start_1
    new-array v0, v3, [[B

    .line 78
    if-eqz p2, :cond_1

    .line 79
    array-length v1, p2

    .line 80
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {p2, v2, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    :cond_1
    iget v2, p0, Leez;->b:I

    sub-int v4, v3, v1

    add-int/2addr v2, v4

    iput v2, p0, Leez;->b:I

    move v2, v1

    .line 84
    :goto_1
    if-ge v2, v3, :cond_3

    .line 86
    iget v1, p0, Leez;->c:I

    if-lez v1, :cond_2

    iget-object v1, p0, Leez;->d:[[B

    iget v4, p0, Leez;->c:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Leez;->c:I

    aget-object v1, v1, v4

    :goto_2
    aput-object v1, v0, v2

    .line 84
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 86
    :cond_2
    iget v1, p0, Leez;->a:I

    new-array v1, v1, [B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_3
    move-object p2, v0

    .line 89
    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 4

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iget v0, p0, Leez;->a:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Leez;->a:I

    div-int/2addr v0, v1

    .line 48
    const/4 v1, 0x0

    iget v2, p0, Leez;->b:I

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 49
    iget v1, p0, Leez;->c:I

    if-ge v0, v1, :cond_0

    .line 50
    iget-object v1, p0, Leez;->d:[[B

    iget v2, p0, Leez;->c:I

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 51
    iput v0, p0, Leez;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :cond_0
    monitor-exit p0

    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
