.class final Leqi;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Leqi;->a:Ljava/util/List;

    return-void
.end method

.method private a()[B
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Leqi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leql;

    iget v4, v0, Leql;->a:I

    invoke-static {v4}, Leqc;->b(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    iget-object v0, v0, Leql;->b:[B

    array-length v0, v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    new-array v1, v1, [B

    array-length v0, v1

    invoke-static {v1, v2, v0}, Leqc;->a([BII)Leqc;

    move-result-object v2

    iget-object v0, p0, Leqi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leql;

    iget v4, v0, Leql;->a:I

    invoke-virtual {v2, v4}, Leqc;->a(I)V

    iget-object v0, v0, Leql;->b:[B

    invoke-virtual {v2, v0}, Leqc;->b([B)V

    goto :goto_1

    :cond_1
    return-object v1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Leqi;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast p1, Leqi;

    iget-object v0, p0, Leqi;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p1, Leqi;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Leqi;->a:Ljava/util/List;

    iget-object v1, p1, Leqi;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-direct {p0}, Leqi;->a()[B

    move-result-object v0

    invoke-direct {p1}, Leqi;->a()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final hashCode()I
    .locals 2

    :try_start_0
    invoke-direct {p0}, Leqi;->a()[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
