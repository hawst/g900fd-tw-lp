.class public abstract Lgrn;
.super Lgrm;
.source "SourceFile"

# interfaces
.implements Lgsa;


# instance fields
.field final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final d:Lgrz;

.field private final e:Lgru;

.field private final f:Ljava/util/List;


# direct methods
.method public constructor <init>(Lgru;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p2, p3}, Lgrm;-><init>(Ljava/lang/String;I)V

    .line 31
    new-instance v0, Lgrz;

    invoke-direct {v0}, Lgrz;-><init>()V

    iput-object v0, p0, Lgrn;->d:Lgrz;

    .line 42
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lgrn;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 43
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lgrn;->f:Ljava/util/List;

    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgru;

    iput-object v0, p0, Lgrn;->e:Lgru;

    .line 56
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 98
    invoke-super {p0}, Lgrm;->a()V

    .line 99
    iget-object v1, p0, Lgrn;->d:Lgrz;

    monitor-enter v1

    .line 100
    :try_start_0
    iget-object v0, p0, Lgrn;->d:Lgrz;

    invoke-virtual {v0}, Lgrz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrs;

    .line 101
    invoke-virtual {v0}, Lgrs;->c()V

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 103
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgrn;->d:Lgrz;

    iget-object v0, v0, Lgrz;->a:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 104
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    iget-object v0, p0, Lgrn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 106
    return-void
.end method

.method public final a(JLandroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lgrn;->e:Lgru;

    invoke-virtual {v0, p1, p2, p3}, Lgru;->a(JLandroid/graphics/Bitmap;)Lgrs;

    move-result-object v1

    .line 113
    iget-object v2, p0, Lgrn;->d:Lgrz;

    monitor-enter v2

    .line 114
    :try_start_0
    iget-object v0, p0, Lgrn;->d:Lgrz;

    invoke-virtual {v0, v1}, Lgrz;->a(Lgrs;)Lgrs;

    .line 115
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    iget-object v0, p0, Lgrn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    .line 118
    invoke-interface {v0, p0, v1}, Lgsb;->a(Lgsa;Lgrs;)V

    goto :goto_0

    .line 115
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 120
    :cond_0
    return-void
.end method

.method public final a(Lgsb;)V
    .locals 2

    .prologue
    .line 80
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v1, p0, Lgrn;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-object v0, p0, Lgrn;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v0, p0, Lgrn;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    .line 85
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    if-eqz v0, :cond_0

    .line 87
    invoke-interface {p1, p0}, Lgsb;->b(Lgsa;)V

    .line 89
    :cond_0
    return-void

    .line 85
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 136
    iget-object v1, p0, Lgrn;->f:Ljava/util/List;

    monitor-enter v1

    .line 137
    :try_start_0
    iget-object v0, p0, Lgrn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    .line 138
    invoke-interface {v0, p1}, Lgsb;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b(JZ)Lgrs;
    .locals 3

    .prologue
    .line 67
    iget-object v1, p0, Lgrn;->d:Lgrz;

    monitor-enter v1

    .line 68
    :try_start_0
    iget-object v0, p0, Lgrn;->d:Lgrz;

    invoke-virtual {v0, p1, p2, p3}, Lgrz;->a(JZ)Lgrs;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgrs;->b()Lgrs;

    move-result-object v0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lgsb;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lgrn;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 94
    return-void
.end method

.method public final c(J)Lgrs;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lgrn;->b(JZ)Lgrs;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 125
    iget-object v1, p0, Lgrn;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 126
    :try_start_0
    iget-object v0, p0, Lgrn;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 127
    iget-object v0, p0, Lgrn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 128
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    invoke-interface {v0, p0}, Lgsb;->b(Lgsa;)V

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 132
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lgrn;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method
