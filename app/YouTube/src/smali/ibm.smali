.class public final Libm;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Libm;


# instance fields
.field private b:[I

.field private c:Lhwl;

.field private d:Ljava/lang/String;

.field private e:Lhip;

.field private f:[Lhgj;

.field private g:[Lhxp;

.field private h:[Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lgyo;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Libm;

    sput-object v0, Libm;->a:[Libm;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    sget-object v0, Lidj;->a:[I

    iput-object v0, p0, Libm;->b:[I

    iput-object v2, p0, Libm;->c:Lhwl;

    const-string v0, ""

    iput-object v0, p0, Libm;->d:Ljava/lang/String;

    iput-object v2, p0, Libm;->e:Lhip;

    sget-object v0, Lhgj;->a:[Lhgj;

    iput-object v0, p0, Libm;->f:[Lhgj;

    sget-object v0, Lhxp;->a:[Lhxp;

    iput-object v0, p0, Libm;->g:[Lhxp;

    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Libm;->h:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Libm;->i:Ljava/lang/String;

    iput-object v2, p0, Libm;->j:Lgyo;

    const-string v0, ""

    iput-object v0, p0, Libm;->k:Ljava/lang/String;

    iput v1, p0, Libm;->l:I

    iput v1, p0, Libm;->m:I

    iput v1, p0, Libm;->n:I

    iput v1, p0, Libm;->o:I

    iput v1, p0, Libm;->p:I

    iput v1, p0, Libm;->q:I

    const-string v0, ""

    iput-object v0, p0, Libm;->r:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Libm;->s:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Libm;->b:[I

    if-eqz v0, :cond_15

    iget-object v0, p0, Libm;->b:[I

    array-length v0, v0

    if-lez v0, :cond_15

    iget-object v3, p0, Libm;->b:[I

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    invoke-static {v5}, Lidd;->a(I)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v2, 0x0

    iget-object v2, p0, Libm;->b:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :goto_1
    iget-object v2, p0, Libm;->c:Lhwl;

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Libm;->c:Lhwl;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Libm;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x3

    iget-object v3, p0, Libm;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Libm;->e:Lhip;

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    iget-object v3, p0, Libm;->e:Lhip;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Libm;->f:[Lhgj;

    if-eqz v2, :cond_5

    iget-object v3, p0, Libm;->f:[Lhgj;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    if-eqz v5, :cond_4

    const/4 v6, 0x5

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    iget-object v2, p0, Libm;->g:[Lhxp;

    if-eqz v2, :cond_7

    iget-object v3, p0, Libm;->g:[Lhxp;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    if-eqz v5, :cond_6

    const/4 v6, 0x6

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    iget-object v2, p0, Libm;->h:[Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Libm;->h:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_9

    iget-object v3, p0, Libm;->h:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v1, v4, :cond_8

    aget-object v5, v3, v1

    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    add-int/2addr v0, v2

    iget-object v1, p0, Libm;->h:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Libm;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const/16 v1, 0x9

    iget-object v2, p0, Libm;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Libm;->j:Lgyo;

    if-eqz v1, :cond_b

    const/16 v1, 0xa

    iget-object v2, p0, Libm;->j:Lgyo;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Libm;->k:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const/16 v1, 0xb

    iget-object v2, p0, Libm;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Libm;->l:I

    if-eqz v1, :cond_d

    const/16 v1, 0xc

    iget v2, p0, Libm;->l:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Libm;->m:I

    if-eqz v1, :cond_e

    const/16 v1, 0xd

    iget v2, p0, Libm;->m:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Libm;->n:I

    if-eqz v1, :cond_f

    const/16 v1, 0xe

    iget v2, p0, Libm;->n:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget v1, p0, Libm;->o:I

    if-eqz v1, :cond_10

    const/16 v1, 0xf

    iget v2, p0, Libm;->o:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget v1, p0, Libm;->p:I

    if-eqz v1, :cond_11

    const/16 v1, 0x10

    iget v2, p0, Libm;->p:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget v1, p0, Libm;->q:I

    if-eqz v1, :cond_12

    const/16 v1, 0x11

    iget v2, p0, Libm;->q:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget-object v1, p0, Libm;->r:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    const/16 v1, 0x12

    iget-object v2, p0, Libm;->r:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-object v1, p0, Libm;->s:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    const/16 v1, 0x13

    iget-object v2, p0, Libm;->s:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget-object v1, p0, Libm;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Libm;->J:I

    return v0

    :cond_15
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Libm;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Libm;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Libm;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libm;->b:[I

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [I

    iget-object v3, p0, Libm;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Libm;->b:[I

    :goto_1
    iget-object v2, p0, Libm;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Libm;->b:[I

    invoke-virtual {p1}, Lidc;->d()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Libm;->b:[I

    invoke-virtual {p1}, Lidc;->d()I

    move-result v3

    aput v3, v2, v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Libm;->c:Lhwl;

    if-nez v0, :cond_3

    new-instance v0, Lhwl;

    invoke-direct {v0}, Lhwl;-><init>()V

    iput-object v0, p0, Libm;->c:Lhwl;

    :cond_3
    iget-object v0, p0, Libm;->c:Lhwl;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libm;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Libm;->e:Lhip;

    if-nez v0, :cond_4

    new-instance v0, Lhip;

    invoke-direct {v0}, Lhip;-><init>()V

    iput-object v0, p0, Libm;->e:Lhip;

    :cond_4
    iget-object v0, p0, Libm;->e:Lhip;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libm;->f:[Lhgj;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgj;

    iget-object v3, p0, Libm;->f:[Lhgj;

    if-eqz v3, :cond_5

    iget-object v3, p0, Libm;->f:[Lhgj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Libm;->f:[Lhgj;

    :goto_3
    iget-object v2, p0, Libm;->f:[Lhgj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Libm;->f:[Lhgj;

    new-instance v3, Lhgj;

    invoke-direct {v3}, Lhgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libm;->f:[Lhgj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Libm;->f:[Lhgj;

    array-length v0, v0

    goto :goto_2

    :cond_7
    iget-object v2, p0, Libm;->f:[Lhgj;

    new-instance v3, Lhgj;

    invoke-direct {v3}, Lhgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libm;->f:[Lhgj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libm;->g:[Lhxp;

    if-nez v0, :cond_9

    move v0, v1

    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lhxp;

    iget-object v3, p0, Libm;->g:[Lhxp;

    if-eqz v3, :cond_8

    iget-object v3, p0, Libm;->g:[Lhxp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Libm;->g:[Lhxp;

    :goto_5
    iget-object v2, p0, Libm;->g:[Lhxp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Libm;->g:[Lhxp;

    new-instance v3, Lhxp;

    invoke-direct {v3}, Lhxp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libm;->g:[Lhxp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_9
    iget-object v0, p0, Libm;->g:[Lhxp;

    array-length v0, v0

    goto :goto_4

    :cond_a
    iget-object v2, p0, Libm;->g:[Lhxp;

    new-instance v3, Lhxp;

    invoke-direct {v3}, Lhxp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libm;->g:[Lhxp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libm;->h:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Libm;->h:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Libm;->h:[Ljava/lang/String;

    :goto_6
    iget-object v2, p0, Libm;->h:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    iget-object v2, p0, Libm;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_b
    iget-object v2, p0, Libm;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libm;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Libm;->j:Lgyo;

    if-nez v0, :cond_c

    new-instance v0, Lgyo;

    invoke-direct {v0}, Lgyo;-><init>()V

    iput-object v0, p0, Libm;->j:Lgyo;

    :cond_c
    iget-object v0, p0, Libm;->j:Lgyo;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libm;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libm;->l:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libm;->m:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libm;->n:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libm;->o:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libm;->p:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libm;->q:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libm;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libm;->s:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x88 -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Libm;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Libm;->b:[I

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v2, p0, Libm;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Libm;->c:Lhwl;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Libm;->c:Lhwl;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v1, p0, Libm;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Libm;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v1, p0, Libm;->e:Lhip;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Libm;->e:Lhip;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v1, p0, Libm;->f:[Lhgj;

    if-eqz v1, :cond_5

    iget-object v2, p0, Libm;->f:[Lhgj;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    if-eqz v4, :cond_4

    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Libm;->g:[Lhxp;

    if-eqz v1, :cond_7

    iget-object v2, p0, Libm;->g:[Lhxp;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    if-eqz v4, :cond_6

    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    iget-object v1, p0, Libm;->h:[Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Libm;->h:[Ljava/lang/String;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    iget-object v0, p0, Libm;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const/16 v0, 0x9

    iget-object v1, p0, Libm;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_9
    iget-object v0, p0, Libm;->j:Lgyo;

    if-eqz v0, :cond_a

    const/16 v0, 0xa

    iget-object v1, p0, Libm;->j:Lgyo;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_a
    iget-object v0, p0, Libm;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const/16 v0, 0xb

    iget-object v1, p0, Libm;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_b
    iget v0, p0, Libm;->l:I

    if-eqz v0, :cond_c

    const/16 v0, 0xc

    iget v1, p0, Libm;->l:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_c
    iget v0, p0, Libm;->m:I

    if-eqz v0, :cond_d

    const/16 v0, 0xd

    iget v1, p0, Libm;->m:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_d
    iget v0, p0, Libm;->n:I

    if-eqz v0, :cond_e

    const/16 v0, 0xe

    iget v1, p0, Libm;->n:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_e
    iget v0, p0, Libm;->o:I

    if-eqz v0, :cond_f

    const/16 v0, 0xf

    iget v1, p0, Libm;->o:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_f
    iget v0, p0, Libm;->p:I

    if-eqz v0, :cond_10

    const/16 v0, 0x10

    iget v1, p0, Libm;->p:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_10
    iget v0, p0, Libm;->q:I

    if-eqz v0, :cond_11

    const/16 v0, 0x11

    iget v1, p0, Libm;->q:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_11
    iget-object v0, p0, Libm;->r:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const/16 v0, 0x12

    iget-object v1, p0, Libm;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_12
    iget-object v0, p0, Libm;->s:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    const/16 v0, 0x13

    iget-object v1, p0, Libm;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_13
    iget-object v0, p0, Libm;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Libm;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Libm;

    iget-object v2, p0, Libm;->b:[I

    iget-object v3, p1, Libm;->b:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libm;->c:Lhwl;

    if-nez v2, :cond_4

    iget-object v2, p1, Libm;->c:Lhwl;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Libm;->d:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Libm;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Libm;->e:Lhip;

    if-nez v2, :cond_6

    iget-object v2, p1, Libm;->e:Lhip;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Libm;->f:[Lhgj;

    iget-object v3, p1, Libm;->f:[Lhgj;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libm;->g:[Lhxp;

    iget-object v3, p1, Libm;->g:[Lhxp;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libm;->h:[Ljava/lang/String;

    iget-object v3, p1, Libm;->h:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libm;->i:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Libm;->i:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Libm;->j:Lgyo;

    if-nez v2, :cond_8

    iget-object v2, p1, Libm;->j:Lgyo;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Libm;->k:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Libm;->k:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_6
    iget v2, p0, Libm;->l:I

    iget v3, p1, Libm;->l:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Libm;->m:I

    iget v3, p1, Libm;->m:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Libm;->n:I

    iget v3, p1, Libm;->n:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Libm;->o:I

    iget v3, p1, Libm;->o:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Libm;->p:I

    iget v3, p1, Libm;->p:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Libm;->q:I

    iget v3, p1, Libm;->q:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Libm;->r:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Libm;->r:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Libm;->s:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Libm;->s:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_8
    iget-object v2, p0, Libm;->I:Ljava/util/List;

    if-nez v2, :cond_c

    iget-object v2, p1, Libm;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Libm;->c:Lhwl;

    iget-object v3, p1, Libm;->c:Lhwl;

    invoke-virtual {v2, v3}, Lhwl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Libm;->d:Ljava/lang/String;

    iget-object v3, p1, Libm;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Libm;->e:Lhip;

    iget-object v3, p1, Libm;->e:Lhip;

    invoke-virtual {v2, v3}, Lhip;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Libm;->i:Ljava/lang/String;

    iget-object v3, p1, Libm;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Libm;->j:Lgyo;

    iget-object v3, p1, Libm;->j:Lgyo;

    invoke-virtual {v2, v3}, Lgyo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Libm;->k:Ljava/lang/String;

    iget-object v3, p1, Libm;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Libm;->r:Ljava/lang/String;

    iget-object v3, p1, Libm;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Libm;->s:Ljava/lang/String;

    iget-object v3, p1, Libm;->s:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Libm;->I:Ljava/util/List;

    iget-object v3, p1, Libm;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    iget-object v2, p0, Libm;->b:[I

    if-nez v2, :cond_4

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Libm;->c:Lhwl;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libm;->d:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libm;->e:Lhip;

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Libm;->f:[Lhgj;

    if-nez v2, :cond_8

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    iget-object v0, p0, Libm;->g:[Lhxp;

    if-nez v0, :cond_a

    mul-int/lit8 v2, v2, 0x1f

    :cond_2
    iget-object v0, p0, Libm;->h:[Ljava/lang/String;

    if-nez v0, :cond_c

    mul-int/lit8 v2, v2, 0x1f

    :cond_3
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Libm;->i:Ljava/lang/String;

    if-nez v0, :cond_e

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libm;->j:Lgyo;

    if-nez v0, :cond_f

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libm;->k:Ljava/lang/String;

    if-nez v0, :cond_10

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libm;->l:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libm;->m:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libm;->n:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libm;->o:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libm;->p:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libm;->q:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libm;->r:Ljava/lang/String;

    if-nez v0, :cond_11

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libm;->s:Ljava/lang/String;

    if-nez v0, :cond_12

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Libm;->I:Ljava/util/List;

    if-nez v2, :cond_13

    :goto_8
    add-int/2addr v0, v1

    return v0

    :cond_4
    move v2, v0

    move v0, v1

    :goto_9
    iget-object v3, p0, Libm;->b:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Libm;->b:[I

    aget v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_5
    iget-object v0, p0, Libm;->c:Lhwl;

    invoke-virtual {v0}, Lhwl;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Libm;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Libm;->e:Lhip;

    invoke-virtual {v0}, Lhip;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_8
    move v2, v0

    move v0, v1

    :goto_a
    iget-object v3, p0, Libm;->f:[Lhgj;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Libm;->f:[Lhgj;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v1

    :goto_b
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_9
    iget-object v2, p0, Libm;->f:[Lhgj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgj;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_a
    move v0, v1

    :goto_c
    iget-object v3, p0, Libm;->g:[Lhxp;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Libm;->g:[Lhxp;

    aget-object v2, v2, v0

    if-nez v2, :cond_b

    move v2, v1

    :goto_d
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_b
    iget-object v2, p0, Libm;->g:[Lhxp;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhxp;->hashCode()I

    move-result v2

    goto :goto_d

    :cond_c
    move v0, v1

    :goto_e
    iget-object v3, p0, Libm;->h:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Libm;->h:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_d

    move v2, v1

    :goto_f
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_d
    iget-object v2, p0, Libm;->h:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_f

    :cond_e
    iget-object v0, p0, Libm;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_f
    iget-object v0, p0, Libm;->j:Lgyo;

    invoke-virtual {v0}, Lgyo;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_10
    iget-object v0, p0, Libm;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_11
    iget-object v0, p0, Libm;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_6

    :cond_12
    iget-object v0, p0, Libm;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_7

    :cond_13
    iget-object v1, p0, Libm;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_8
.end method
