.class public final enum Lbgx;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbgx;

.field public static final enum b:Lbgx;

.field public static final enum c:Lbgx;

.field public static final enum d:Lbgx;

.field public static final enum e:Lbgx;

.field public static final enum f:Lbgx;

.field public static final enum g:Lbgx;

.field private static final synthetic k:[Lbgx;


# instance fields
.field public final h:I

.field final i:I

.field public final j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 54
    new-instance v0, Lbgx;

    const-string v1, "WATCH_LATER"

    const v4, 0x7f09026a

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lbgx;-><init>(Ljava/lang/String;IIIZ)V

    sput-object v0, Lbgx;->a:Lbgx;

    .line 55
    new-instance v3, Lbgx;

    const-string v4, "FAVORITES"

    const v7, 0x7f090269

    move v6, v5

    move v8, v5

    invoke-direct/range {v3 .. v8}, Lbgx;-><init>(Ljava/lang/String;IIIZ)V

    sput-object v3, Lbgx;->b:Lbgx;

    .line 56
    new-instance v6, Lbgx;

    const-string v7, "UPLOADS"

    const v10, 0x7f090268

    move v8, v12

    move v9, v12

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lbgx;-><init>(Ljava/lang/String;IIIZ)V

    sput-object v6, Lbgx;->c:Lbgx;

    .line 57
    new-instance v6, Lbgx;

    const-string v7, "HISTORY"

    const v10, 0x7f09026b

    move v8, v13

    move v9, v13

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lbgx;-><init>(Ljava/lang/String;IIIZ)V

    sput-object v6, Lbgx;->d:Lbgx;

    .line 58
    new-instance v6, Lbgx;

    const-string v7, "POPULAR"

    const v10, 0x7f090265

    move v8, v14

    move v9, v14

    move v11, v2

    invoke-direct/range {v6 .. v11}, Lbgx;-><init>(Ljava/lang/String;IIIZ)V

    sput-object v6, Lbgx;->e:Lbgx;

    .line 59
    new-instance v6, Lbgx;

    const-string v7, "MUSIC"

    const/4 v8, 0x5

    const/4 v9, 0x5

    const v10, 0x7f09030e

    move v11, v2

    invoke-direct/range {v6 .. v11}, Lbgx;-><init>(Ljava/lang/String;IIIZ)V

    sput-object v6, Lbgx;->f:Lbgx;

    .line 60
    new-instance v6, Lbgx;

    const-string v7, "POPULAR_WW"

    const/4 v8, 0x6

    const/4 v9, 0x6

    const v10, 0x7f090265

    move v11, v2

    invoke-direct/range {v6 .. v11}, Lbgx;-><init>(Ljava/lang/String;IIIZ)V

    sput-object v6, Lbgx;->g:Lbgx;

    .line 53
    const/4 v0, 0x7

    new-array v0, v0, [Lbgx;

    sget-object v1, Lbgx;->a:Lbgx;

    aput-object v1, v0, v2

    sget-object v1, Lbgx;->b:Lbgx;

    aput-object v1, v0, v5

    sget-object v1, Lbgx;->c:Lbgx;

    aput-object v1, v0, v12

    sget-object v1, Lbgx;->d:Lbgx;

    aput-object v1, v0, v13

    sget-object v1, Lbgx;->e:Lbgx;

    aput-object v1, v0, v14

    const/4 v1, 0x5

    sget-object v2, Lbgx;->f:Lbgx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbgx;->g:Lbgx;

    aput-object v2, v0, v1

    sput-object v0, Lbgx;->k:[Lbgx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIZ)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput p3, p0, Lbgx;->h:I

    .line 68
    iput p4, p0, Lbgx;->i:I

    .line 69
    iput-boolean p5, p0, Lbgx;->j:Z

    .line 70
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbgx;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lbgx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbgx;

    return-object v0
.end method

.method public static values()[Lbgx;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lbgx;->k:[Lbgx;

    invoke-virtual {v0}, [Lbgx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbgx;

    return-object v0
.end method
