.class public Ldfl;
.super Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;
.source "SourceFile"

# interfaces
.implements Lddx;
.implements Ldec;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/16 v2, 0x7f

    const/16 v1, 0xff

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-static {v2, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Ldfl;->setBackgroundColor(I)V

    .line 30
    invoke-static {v2, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Ldfl;->setTextColor(I)V

    .line 31
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Ldfl;->setGravity(I)V

    .line 32
    const/4 v0, 0x2

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 32
    invoke-virtual {p0, v0, v1}, Ldfl;->setTextSize(IF)V

    .line 35
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ldfl;->setVisibility(I)V

    .line 36
    return-void
.end method


# virtual methods
.method public final a_(Z)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public c_(Z)V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public final f_()Landroid/view/View;
    .locals 0

    .prologue
    .line 40
    return-object p0
.end method

.method public r_()Ldeg;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 45
    new-instance v0, Ldeg;

    invoke-direct {v0, v1, v1}, Ldeg;-><init>(II)V

    return-object v0
.end method
