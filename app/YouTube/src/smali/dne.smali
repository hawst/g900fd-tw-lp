.class public final Ldne;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldnb;


# instance fields
.field final A:I

.field final B:Z

.field private final C:Landroid/os/Handler;

.field final a:Landroid/content/Context;

.field final b:Ldnn;

.field final c:Ldni;

.field final d:Ljava/util/Random;

.field final e:Landroid/os/HandlerThread;

.field final f:Ljava/lang/Object;

.field g:I

.field h:I

.field i:Z

.field final j:Ldnc;

.field final k:Ljava/util/Map;

.field final l:Ljava/util/Map;

.field final m:Ljava/util/Map;

.field final n:Ljava/util/HashSet;

.field final o:Landroid/os/PowerManager$WakeLock;

.field final p:Landroid/net/wifi/WifiManager$WifiLock;

.field final q:Lezz;

.field final r:Ldnl;

.field final s:Ldnj;

.field final t:Ldnh;

.field u:Z

.field v:Z

.field w:I

.field x:Z

.field y:Z

.field public volatile z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldni;Ldnn;Ljava/lang/String;IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p1, p0, Ldne;->a:Landroid/content/Context;

    .line 140
    iput-object p2, p0, Ldne;->c:Ldni;

    .line 141
    iput-object p3, p0, Ldne;->b:Ldnn;

    .line 142
    iput-boolean v3, p0, Ldne;->i:Z

    .line 143
    iput p5, p0, Ldne;->A:I

    .line 144
    iput-boolean p6, p0, Ldne;->B:Z

    .line 146
    new-instance v0, Ldnc;

    invoke-direct {v0, p1, p4}, Ldnc;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Ldne;->j:Ldnc;

    .line 147
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Ldne;->d:Ljava/util/Random;

    .line 148
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldne;->f:Ljava/lang/Object;

    .line 149
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Ldne;->k:Ljava/util/Map;

    .line 150
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldne;->l:Ljava/util/Map;

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldne;->m:Ljava/util/Map;

    .line 152
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldne;->n:Ljava/util/HashSet;

    .line 154
    new-instance v0, Lfam;

    new-instance v1, Ldnm;

    invoke-direct {v1}, Ldnm;-><init>()V

    invoke-direct {v0, v1}, Lfam;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Ldne;->q:Lezz;

    .line 157
    new-instance v0, Ldnj;

    invoke-direct {v0, p0}, Ldnj;-><init>(Ldne;)V

    iput-object v0, p0, Ldne;->s:Ldnj;

    .line 158
    iget-object v0, p0, Ldne;->s:Ldnj;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, v0, Ldnj;->b:Ldne;

    iget-object v2, v2, Ldne;->a:Landroid/content/Context;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 159
    new-instance v0, Ldnl;

    invoke-direct {v0, p0}, Ldnl;-><init>(Ldne;)V

    iput-object v0, p0, Ldne;->r:Ldnl;

    .line 160
    iget-object v0, p0, Ldne;->r:Ldnl;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Ldnl;->c:Ldne;

    iget-object v2, v2, Ldne;->a:Landroid/content/Context;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {v0}, Ldnl;->a()Z

    .line 161
    new-instance v0, Ldnh;

    invoke-direct {v0, p0}, Ldnh;-><init>(Ldne;)V

    iput-object v0, p0, Ldne;->t:Ldnh;

    .line 162
    iget-object v0, p0, Ldne;->t:Ldnh;

    iget-object v1, v0, Ldnh;->b:Ldne;

    iget-object v1, v1, Ldne;->a:Landroid/content/Context;

    invoke-static {v1}, Lfaq;->a(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, v0, Ldnh;->a:Z

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, v0, Ldnh;->b:Ldne;

    iget-object v2, v2, Ldne;->a:Landroid/content/Context;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 164
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 165
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Ldne;->o:Landroid/os/PowerManager$WakeLock;

    .line 166
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 167
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Ldne;->p:Landroid/net/wifi/WifiManager$WifiLock;

    .line 169
    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Ldne;->e:Landroid/os/HandlerThread;

    .line 170
    iget-object v0, p0, Ldne;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 171
    new-instance v0, Ldnf;

    iget-object v1, p0, Ldne;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ldnf;-><init>(Ldne;Landroid/os/Looper;)V

    iput-object v0, p0, Ldne;->C:Landroid/os/Handler;

    .line 177
    return-void
.end method

.method static synthetic a(Ldne;)V
    .locals 2

    .prologue
    .line 46
    iget-object v1, p0, Ldne;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Ldne;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ldne;->a(I)I

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method a(I)I
    .locals 2

    .prologue
    .line 260
    iget-object v1, p0, Ldne;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 261
    :try_start_0
    iget-object v0, p0, Ldne;->C:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 262
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldne;->i:Z

    .line 263
    iget v0, p0, Ldne;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldne;->h:I

    monitor-exit v1

    return v0

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(IIILjava/lang/Object;)I
    .locals 2

    .prologue
    .line 276
    iget-object v1, p0, Ldne;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 277
    :try_start_0
    iget-object v0, p0, Ldne;->C:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 278
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldne;->i:Z

    .line 279
    iget v0, p0, Ldne;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldne;->h:I

    monitor-exit v1

    return v0

    .line 280
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(ILjava/lang/Object;)I
    .locals 2

    .prologue
    .line 268
    iget-object v1, p0, Ldne;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 269
    :try_start_0
    iget-object v0, p0, Ldne;->C:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldne;->i:Z

    .line 271
    iget v0, p0, Ldne;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldne;->h:I

    monitor-exit v1

    return v0

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(ILjava/lang/Object;I)I
    .locals 6

    .prologue
    .line 284
    iget-object v1, p0, Ldne;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 285
    :try_start_0
    iget-object v0, p0, Ldne;->C:Landroid/os/Handler;

    iget-object v2, p0, Ldne;->C:Landroid/os/Handler;

    const/16 v3, 0xc

    invoke-virtual {v2, v3, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    int-to-long v4, p3

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 286
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldne;->i:Z

    .line 287
    iget v0, p0, Ldne;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldne;->h:I

    monitor-exit v1

    return v0

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 212
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1, p1}, Ldne;->a(IIILjava/lang/Object;)I

    move-result v0

    return v0
.end method

.method final a()V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 559
    iget-boolean v0, p0, Ldne;->x:Z

    if-nez v0, :cond_1

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 566
    :cond_1
    iget-boolean v0, p0, Ldne;->u:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldne;->r:Ldnl;

    iget-boolean v0, v0, Ldnl;->b:Z

    if-nez v0, :cond_4

    move v0, v1

    .line 567
    :goto_1
    iget-boolean v3, p0, Ldne;->v:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldne;->t:Ldnh;

    iget-boolean v3, v3, Ldnh;->a:Z

    if-nez v3, :cond_5

    move v3, v1

    .line 568
    :goto_2
    iget-object v4, p0, Ldne;->s:Ldnj;

    iget-boolean v4, v4, Ldnj;->a:Z

    if-nez v4, :cond_6

    move v4, v1

    .line 570
    :goto_3
    iget-object v5, p0, Ldne;->r:Ldnl;

    iget-boolean v5, v5, Ldnl;->a:Z

    if-nez v5, :cond_7

    const/4 v5, 0x2

    :goto_4
    or-int/lit8 v6, v5, 0x0

    .line 572
    if-eqz v4, :cond_8

    const/4 v5, 0x4

    :goto_5
    or-int/2addr v5, v6

    .line 573
    if-eqz v0, :cond_9

    const/16 v0, 0x8

    :goto_6
    or-int/2addr v5, v0

    .line 574
    if-eqz v3, :cond_a

    const/16 v0, 0x10

    :goto_7
    or-int v7, v5, v0

    .line 576
    iget-object v0, p0, Ldne;->q:Lezz;

    invoke-interface {v0}, Lezz;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v6, v2

    move v5, v2

    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnk;

    .line 577
    invoke-virtual {v0}, Ldnk;->b()Z

    move-result v9

    if-eqz v9, :cond_12

    .line 580
    iget v5, p0, Ldne;->w:I

    if-lt v6, v5, :cond_b

    const/16 v5, 0x20

    :goto_9
    or-int/2addr v5, v7

    .line 582
    if-nez v5, :cond_f

    .line 583
    iget-object v5, p0, Ldne;->l:Ljava/util/Map;

    iget-object v9, v0, Ldnk;->a:Ljava/lang/String;

    .line 584
    invoke-interface {v5, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Ldne;->n:Ljava/util/HashSet;

    iget-object v9, v0, Ldnk;->a:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    :cond_2
    move v5, v1

    .line 585
    :goto_a
    if-nez v5, :cond_3

    .line 586
    iget-object v9, v0, Ldnk;->a:Ljava/lang/String;

    iget-object v5, p0, Ldne;->l:Ljava/util/Map;

    invoke-interface {v5, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    move v5, v1

    :goto_b
    invoke-static {v5}, Lb;->c(Z)V

    iget-object v5, p0, Ldne;->b:Ldnn;

    invoke-virtual {v0}, Ldnk;->a()Lgjm;

    move-result-object v10

    invoke-interface {v5, v10, p0}, Ldnn;->a(Lgjm;Ldnb;)Ldna;

    move-result-object v5

    if-nez v5, :cond_e

    move v0, v2

    :goto_c
    if-eqz v0, :cond_13

    .line 588
    :cond_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v1

    .line 593
    goto :goto_8

    :cond_4
    move v0, v2

    .line 566
    goto/16 :goto_1

    :cond_5
    move v3, v2

    .line 567
    goto :goto_2

    :cond_6
    move v4, v2

    .line 568
    goto :goto_3

    :cond_7
    move v5, v2

    .line 570
    goto :goto_4

    :cond_8
    move v5, v2

    .line 572
    goto :goto_5

    :cond_9
    move v0, v2

    .line 573
    goto :goto_6

    :cond_a
    move v0, v2

    .line 574
    goto :goto_7

    :cond_b
    move v5, v2

    .line 580
    goto :goto_9

    :cond_c
    move v5, v2

    .line 584
    goto :goto_a

    :cond_d
    move v5, v2

    .line 586
    goto :goto_b

    :cond_e
    iget-object v10, p0, Ldne;->l:Ljava/util/Map;

    invoke-interface {v10, v9, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v9, Lgjn;->b:Lgjn;

    iput-object v9, v0, Ldnk;->c:Lgjn;

    iput v2, v0, Ldnk;->d:I

    iget-object v9, p0, Ldne;->j:Ldnc;

    invoke-virtual {v9, v0}, Ldnc;->b(Ldnk;)V

    new-instance v9, Ldng;

    invoke-direct {v9, p0, v5}, Ldng;-><init>(Ldne;Ljava/lang/Runnable;)V

    invoke-virtual {v9}, Ldng;->start()V

    iget-object v5, p0, Ldne;->c:Ldni;

    invoke-virtual {v0}, Ldnk;->a()Lgjm;

    move-result-object v0

    invoke-interface {v5, v0}, Ldni;->c(Lgjm;)V

    move v0, v1

    goto :goto_c

    .line 594
    :cond_f
    invoke-virtual {p0, v0, v5}, Ldne;->a(Ldnk;I)V

    move v0, v1

    :goto_d
    move v5, v0

    .line 597
    goto/16 :goto_8

    .line 599
    :cond_10
    iput-boolean v5, p0, Ldne;->y:Z

    .line 600
    if-eqz v5, :cond_11

    if-nez v3, :cond_11

    if-nez v4, :cond_11

    .line 601
    iget-object v0, p0, Ldne;->p:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 604
    iget-object v0, p0, Ldne;->p:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    goto/16 :goto_0

    .line 606
    :cond_11
    iget-object v0, p0, Ldne;->p:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Ldne;->p:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    :cond_12
    move v0, v5

    goto :goto_d

    :cond_13
    move v5, v1

    goto/16 :goto_8
.end method

.method a(Ldnk;I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 678
    const/4 v0, 0x0

    .line 679
    iget-object v1, p1, Ldnk;->c:Lgjn;

    sget-object v3, Lgjn;->a:Lgjn;

    if-eq v1, v3, :cond_3

    .line 680
    sget-object v0, Lgjn;->a:Lgjn;

    iput-object v0, p1, Ldnk;->c:Lgjn;

    move v1, v2

    .line 685
    :goto_0
    iget-object v3, p1, Ldnk;->a:Ljava/lang/String;

    .line 686
    iget-object v0, p0, Ldne;->l:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldna;

    .line 687
    if-eqz v0, :cond_0

    .line 688
    invoke-interface {v0, p2}, Ldna;->a(I)V

    .line 690
    :cond_0
    iget-object v0, p0, Ldne;->m:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 691
    iget-object v0, p0, Ldne;->n:Ljava/util/HashSet;

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 693
    iget v0, p1, Ldnk;->d:I

    if-eq v0, p2, :cond_2

    .line 694
    iput p2, p1, Ldnk;->d:I

    .line 698
    :goto_1
    if-eqz v2, :cond_1

    .line 699
    iget-object v0, p0, Ldne;->j:Ldnc;

    invoke-virtual {v0, p1}, Ldnc;->b(Ldnk;)V

    .line 700
    iget-object v0, p0, Ldne;->c:Ldni;

    invoke-virtual {p1}, Ldnk;->a()Lgjm;

    move-result-object v1

    invoke-interface {v0, v1}, Ldni;->c(Lgjm;)V

    .line 702
    :cond_1
    return-void

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 712
    const/16 v0, 0x8

    const/16 v1, 0x1f

    shr-long v2, p2, v1

    long-to-int v1, v2

    const-wide/32 v2, 0x7fffffff

    and-long/2addr v2, p2

    long-to-int v2, v2

    invoke-virtual {p0, v0, v1, v2, p1}, Ldne;->a(IIILjava/lang/Object;)I

    .line 713
    return-void
.end method

.method public final a(Ljava/lang/String;Ldmt;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 727
    const/16 v2, 0xb

    iget-boolean v0, p2, Ldmt;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v2, v0, v1, p1}, Ldne;->a(IIILjava/lang/Object;)I

    .line 728
    return-void

    :cond_0
    move v0, v1

    .line 727
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lgje;)V
    .locals 2

    .prologue
    .line 722
    const/16 v0, 0xa

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ldne;->a(ILjava/lang/Object;)I

    .line 723
    return-void
.end method

.method public final b(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 717
    const/16 v0, 0x9

    const/16 v1, 0x1f

    shr-long v2, p2, v1

    long-to-int v1, v2

    const-wide/32 v2, 0x7fffffff

    and-long/2addr v2, p2

    long-to-int v2, v2

    invoke-virtual {p0, v0, v1, v2, p1}, Ldne;->a(IIILjava/lang/Object;)I

    .line 718
    return-void
.end method
