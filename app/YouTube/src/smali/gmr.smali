.class final Lgmr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Levi;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Lgmn;

.field private d:Lgmx;

.field private e:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lgmn;)V
    .locals 0

    .prologue
    .line 1332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1333
    iput-object p1, p0, Lgmr;->a:Landroid/content/Context;

    .line 1334
    iput-object p2, p0, Lgmr;->b:Ljava/lang/String;

    .line 1335
    iput-object p3, p0, Lgmr;->c:Lgmn;

    .line 1336
    return-void
.end method


# virtual methods
.method public final declared-synchronized getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 4

    .prologue
    .line 1340
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgmr;->d:Lgmx;

    if-nez v0, :cond_0

    .line 1341
    iget-object v0, p0, Lgmr;->a:Landroid/content/Context;

    iget-object v1, p0, Lgmr;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1342
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1343
    new-instance v0, Lgmx;

    iget-object v1, p0, Lgmr;->a:Landroid/content/Context;

    iget-object v2, p0, Lgmr;->b:Ljava/lang/String;

    iget-object v3, p0, Lgmr;->c:Lgmn;

    invoke-direct {v0, v1, v2, v3}, Lgmx;-><init>(Landroid/content/Context;Ljava/lang/String;Lgmy;)V

    iput-object v0, p0, Lgmr;->d:Lgmx;

    .line 1344
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgmr;->e:Z

    .line 1351
    :cond_0
    :goto_0
    iget-object v0, p0, Lgmr;->d:Lgmx;

    invoke-virtual {v0}, Lgmx;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1346
    :cond_1
    :try_start_1
    new-instance v0, Lgmx;

    iget-object v1, p0, Lgmr;->a:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v3, p0, Lgmr;->c:Lgmn;

    invoke-direct {v0, v1, v2, v3}, Lgmx;-><init>(Landroid/content/Context;Ljava/lang/String;Lgmy;)V

    iput-object v0, p0, Lgmr;->d:Lgmx;

    .line 1347
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgmr;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1340
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 4

    .prologue
    .line 1356
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgmr;->e:Z

    if-nez v0, :cond_1

    .line 1357
    iget-object v0, p0, Lgmr;->d:Lgmx;

    if-eqz v0, :cond_0

    .line 1358
    iget-object v0, p0, Lgmr;->d:Lgmx;

    invoke-virtual {v0}, Lgmx;->close()V

    .line 1361
    :cond_0
    new-instance v0, Lgmx;

    iget-object v1, p0, Lgmr;->a:Landroid/content/Context;

    iget-object v2, p0, Lgmr;->b:Ljava/lang/String;

    iget-object v3, p0, Lgmr;->c:Lgmn;

    invoke-direct {v0, v1, v2, v3}, Lgmx;-><init>(Landroid/content/Context;Ljava/lang/String;Lgmy;)V

    iput-object v0, p0, Lgmr;->d:Lgmx;

    .line 1362
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgmr;->e:Z

    .line 1365
    :cond_1
    iget-object v0, p0, Lgmr;->d:Lgmx;

    invoke-virtual {v0}, Lgmx;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1356
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
