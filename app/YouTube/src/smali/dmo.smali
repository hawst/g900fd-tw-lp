.class public Ldmo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldna;


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ldnb;

.field private final e:I

.field private final f:J

.field private final g:J

.field private volatile h:Z

.field private final i:Ljava/lang/Object;

.field private final j:Z

.field private final k:Z

.field private l:J

.field private final m:Ldmq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-string v0, "^bytes (\\d+)-(\\d+)/(\\d+)$"

    .line 39
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Ldmo;->a:Ljava/util/regex/Pattern;

    .line 38
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JJLdnb;IZZLdmq;)V
    .locals 7

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ldmo;->c:Ljava/lang/String;

    .line 97
    invoke-static {p2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ldmo;->b:Ljava/lang/String;

    .line 98
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lb;->b(Z)V

    .line 99
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ldmo;->f:J

    .line 100
    const-wide/16 v2, 0x0

    cmp-long v2, p5, v2

    if-ltz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Lb;->b(Z)V

    .line 101
    iput-wide p5, p0, Ldmo;->g:J

    .line 102
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldnb;

    iput-object v2, p0, Ldmo;->d:Ldnb;

    .line 103
    const/high16 v2, 0x100000

    iput v2, p0, Ldmo;->e:I

    .line 104
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Ldmo;->i:Ljava/lang/Object;

    .line 105
    move/from16 v0, p9

    iput-boolean v0, p0, Ldmo;->j:Z

    .line 106
    move/from16 v0, p10

    iput-boolean v0, p0, Ldmo;->k:Z

    .line 107
    move-object/from16 v0, p11

    iput-object v0, p0, Ldmo;->m:Ldmq;

    .line 108
    return-void

    .line 98
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 100
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLdnb;ZZLdmq;)V
    .locals 13

    .prologue
    .line 76
    const-wide/16 v4, 0x0

    const/high16 v9, 0x100000

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide/from16 v6, p3

    move-object/from16 v8, p5

    move/from16 v10, p6

    move/from16 v11, p7

    move-object/from16 v12, p8

    invoke-direct/range {v1 .. v12}, Ldmo;-><init>(Ljava/lang/String;Ljava/lang/String;JJLdnb;IZZLdmq;)V

    .line 85
    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;)J
    .locals 10

    .prologue
    const/4 v4, 0x1

    .line 315
    const-wide/16 v0, -0x1

    .line 317
    const-string v2, "Content-Length"

    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 318
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 320
    :try_start_0
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v0

    .line 326
    :cond_0
    :goto_0
    const-string v2, "Content-Range"

    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 327
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 328
    sget-object v2, Ldmo;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 329
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 331
    const/4 v3, 0x2

    .line 332
    :try_start_1
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sub-long v2, v8, v2

    const-wide/16 v8, 0x1

    add-long/2addr v2, v8

    .line 333
    const-wide/16 v8, 0x0

    cmp-long v7, v0, v8

    if-gez v7, :cond_3

    .line 336
    const-string v5, "Using contentLength parsed from Content-Range "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-static {v4}, Lezp;->c(Ljava/lang/String;)V

    move-wide v0, v2

    .line 348
    :cond_1
    :goto_2
    return-wide v0

    .line 336
    :cond_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_2

    .line 339
    :cond_3
    cmp-long v2, v0, v2

    if-nez v2, :cond_4

    move v2, v4

    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2d

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v4, v7

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Content-Length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not match Content-Range "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lb;->d(ZLjava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    :catch_1
    move-exception v2

    goto/16 :goto_0
.end method

.method private static a(Ljava/net/URL;J)Ljava/net/HttpURLConnection;
    .locals 5

    .prologue
    const/16 v1, 0x2710

    .line 305
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 306
    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 307
    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 308
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 309
    const-string v1, "Range"

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "bytes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 311
    return-object v0
.end method

.method private static a(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 404
    if-nez p0, :cond_0

    .line 412
    :goto_0
    return-void

    .line 408
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 412
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V
    .locals 5

    .prologue
    .line 365
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldmo;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "download error ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p4}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 366
    invoke-static {p1}, Ldmo;->a(Ljava/io/InputStream;)V

    .line 367
    invoke-static {p2}, Ldmo;->a(Ljava/nio/channels/FileChannel;)V

    .line 368
    if-eqz p3, :cond_0

    .line 369
    invoke-virtual {p3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 371
    :cond_0
    iget-object v0, p0, Ldmo;->d:Ldnb;

    iget-object v1, p0, Ldmo;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p4}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    .line 372
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 415
    iget-boolean v0, p0, Ldmo;->j:Z

    if-eqz v0, :cond_0

    .line 416
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldmo;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 418
    :cond_0
    invoke-static {p1}, Lezp;->e(Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method private static a(Ljava/nio/channels/FileChannel;)V
    .locals 2

    .prologue
    .line 384
    if-nez p0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 388
    :cond_0
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/nio/channels/FileChannel;->force(Z)V

    .line 389
    invoke-virtual {p0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_0
    .catch Ljava/io/SyncFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    :try_start_1
    invoke-virtual {p0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 400
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    .line 396
    :try_start_2
    invoke-virtual {p0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 400
    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v0

    .line 396
    :try_start_3
    invoke-virtual {p0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    .line 400
    :catch_4
    move-exception v0

    goto :goto_0

    .line 395
    :catchall_0
    move-exception v0

    .line 396
    :try_start_4
    invoke-virtual {p0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 399
    :goto_1
    throw v0

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 375
    const/16 v0, 0xc8

    if-lt p0, v0, :cond_0

    const/16 v0, 0x12c

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/nio/channels/FileChannel;Ljava/nio/ByteBuffer;)I
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p1, p2}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 112
    iget-object v1, p0, Ldmo;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 113
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ldmo;->h:Z

    .line 114
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 24

    .prologue
    .line 123
    const/16 v2, 0xa

    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 125
    :try_start_0
    const-string v2, "download starting"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldmo;->a(Ljava/lang/String;)V

    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->b:Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v3, 0x0

    const-string v5, "opening output "

    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v2}, Lezp;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v10}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    :cond_0
    new-instance v5, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->b:Ljava/lang/String;

    const-string v6, "rw"

    invoke-direct {v5, v2, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v6, v0, Ldmo;->f:J

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_2

    move-object/from16 v0, p0

    iget-wide v6, v0, Ldmo;->f:J

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v8, 0x21

    invoke-direct {v2, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "writing from "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->f(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v6, v0, Ldmo;->f:J

    invoke-virtual {v5, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    move-object/from16 v0, p0

    iget-wide v8, v0, Ldmo;->f:J

    :goto_1
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v11

    :try_start_2
    move-object/from16 v0, p0

    iget-wide v6, v0, Ldmo;->g:J

    const-wide/16 v12, 0x0

    cmp-long v2, v6, v12

    if-lez v2, :cond_3

    move-object/from16 v0, p0

    iget-wide v6, v0, Ldmo;->g:J

    cmp-long v2, v8, v6

    if-nez v2, :cond_3

    invoke-static {v11}, Ldmo;->a(Ljava/nio/channels/FileChannel;)V

    const-string v2, "download already completed"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldmo;->a(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->d:Ldnb;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldmo;->b:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Ldnb;->a(Ljava/lang/String;Lgje;)V

    .line 129
    :goto_2
    return-void

    .line 125
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 126
    :catch_0
    move-exception v2

    .line 127
    move-object/from16 v0, p0

    iget-object v3, v0, Ldmo;->d:Ldnb;

    move-object/from16 v0, p0

    iget-object v4, v0, Ldmo;->b:Ljava/lang/String;

    new-instance v5, Ldmt;

    const/4 v6, 0x0

    invoke-direct {v5, v2, v6}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v3, v4, v5}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    goto :goto_2

    .line 125
    :cond_2
    :try_start_3
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v6, 0x23

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "appending from "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->f(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_4
    new-instance v6, Ldmt;

    const/4 v7, 0x1

    invoke-direct {v6, v2, v7}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v6}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V

    goto :goto_2

    :cond_3
    const-string v6, "opening input "

    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v6, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-static {v2}, Lezp;->f(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :try_start_5
    new-instance v2, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v6, v0, Ldmo;->c:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v8, v9}, Ldmo;->a(Ljava/net/URL;J)Ljava/net/HttpURLConnection;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v7

    invoke-static {v4}, Ldmo;->a(Ljava/net/HttpURLConnection;)J

    move-result-wide v12

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v14, 0x3b

    invoke-direct {v6, v14}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v14, "responseCode="

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v14, " contentLength="

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lezp;->f(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v14, v0, Ldmo;->g:J

    const-wide/16 v16, 0x0

    cmp-long v6, v14, v16

    if-nez v6, :cond_5

    const/16 v6, 0x1a0

    if-ne v7, v6, :cond_5

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    const-wide/16 v14, 0x1

    sub-long v14, v8, v14

    invoke-static {v2, v14, v15}, Ldmo;->a(Ljava/net/URL;J)Ljava/net/HttpURLConnection;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    invoke-static {v2}, Ldmo;->b(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "download already completed"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldmo;->a(Ljava/lang/String;)V

    invoke-static {v11}, Ldmo;->a(Ljava/nio/channels/FileChannel;)V

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->d:Ldnb;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldmo;->b:Ljava/lang/String;

    invoke-interface {v2, v5, v8, v9}, Ldnb;->a(Ljava/lang/String;J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->d:Ldnb;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldmo;->b:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface {v2, v5, v6}, Ldnb;->a(Ljava/lang/String;Lgje;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_2

    :catch_2
    move-exception v2

    :goto_4
    :try_start_6
    new-instance v5, Ldmt;

    const/4 v6, 0x0

    invoke-direct {v5, v2, v6}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v11, v4, v5}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V

    goto/16 :goto_2

    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_3

    :cond_5
    move-object v6, v4

    :try_start_7
    invoke-static {v7}, Ldmo;->b(I)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v4, 0x0

    new-instance v5, Ldmt;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v8, 0x17

    invoke-direct {v2, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "http status "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    div-int/lit8 v2, v7, 0x64

    const/4 v9, 0x4

    if-ne v2, v9, :cond_6

    const/16 v2, 0x198

    if-eq v7, v2, :cond_6

    const/4 v2, 0x1

    :goto_5
    invoke-direct {v5, v8, v2}, Ldmt;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11, v6, v5}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V

    goto/16 :goto_2

    :catch_3
    move-exception v2

    move-object v4, v6

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    :cond_7
    const-wide/16 v14, 0x1

    cmp-long v2, v12, v14

    if-gez v2, :cond_8

    const/4 v2, 0x0

    new-instance v4, Ldmt;

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v7, 0x23

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "content length "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    invoke-direct {v4, v5, v7}, Ldmt;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11, v6, v4}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V

    goto/16 :goto_2

    :cond_8
    const-wide/16 v14, 0x0

    cmp-long v2, v8, v14

    if-lez v2, :cond_a

    const-string v2, "Content-Range"

    invoke-virtual {v6, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v7, 0x15

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "-"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    :cond_9
    const/4 v4, 0x0

    new-instance v7, Ldmt;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, 0x28

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v15, "Content-Range "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v14, ", not "

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v14, 0x0

    invoke-direct {v7, v2, v14}, Ldmt;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11, v6, v7}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V

    :cond_a
    add-long/2addr v12, v8

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x1c

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "size is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->f(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v14, v0, Ldmo;->g:J

    const-wide/16 v16, 0x0

    cmp-long v2, v14, v16

    if-lez v2, :cond_b

    move-object/from16 v0, p0

    iget-wide v14, v0, Ldmo;->g:J

    cmp-long v2, v12, v14

    if-eqz v2, :cond_c

    const/4 v2, 0x0

    new-instance v4, Ldmt;

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v7, 0x24

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "unexpected size "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    invoke-direct {v4, v5, v7}, Ldmt;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11, v6, v4}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V

    goto/16 :goto_2

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->d:Ldnb;

    move-object/from16 v0, p0

    iget-object v4, v0, Ldmo;->b:Ljava/lang/String;

    invoke-interface {v2, v4, v12, v13}, Ldnb;->a(Ljava/lang/String;J)V

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v2, v0, Ldmo;->k:Z

    if-eqz v2, :cond_d

    invoke-virtual {v5, v12, v13}, Ljava/io/RandomAccessFile;->setLength(J)V

    :cond_d
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->m:Ldmq;

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Ldmo;->m:Ldmq;

    new-instance v2, Ldmr;

    invoke-direct {v2, v3, v4}, Ldmr;-><init>(Ljava/io/InputStream;Ldmq;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    :goto_6
    const/high16 v3, 0x10000

    :try_start_8
    new-array v14, v3, [B

    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v15

    const/4 v3, 0x0

    move-wide v4, v8

    :goto_7
    move-object/from16 v0, p0

    iget-boolean v7, v0, Ldmo;->h:Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    if-nez v7, :cond_e

    const/4 v7, -0x1

    if-eq v3, v7, :cond_e

    const/4 v3, 0x0

    const/high16 v7, 0x10000

    :try_start_9
    invoke-virtual {v2, v14, v3, v7}, Ljava/io/InputStream;->read([BII)I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    move-result v3

    :try_start_a
    move-object/from16 v0, p0

    iget-object v8, v0, Ldmo;->i:Ljava/lang/Object;

    monitor-enter v8
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    :try_start_b
    move-object/from16 v0, p0

    iget-boolean v7, v0, Ldmo;->h:Z

    if-eqz v7, :cond_f

    monitor-exit v8
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_e
    :try_start_c
    invoke-static {v2}, Ldmo;->a(Ljava/io/InputStream;)V

    invoke-static {v11}, Ldmo;->a(Ljava/nio/channels/FileChannel;)V

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Ldmo;->h:Z

    if-eqz v3, :cond_17

    const-string v2, "download canceled"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldmo;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_4
    move-exception v3

    new-instance v4, Ldmt;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11, v6, v4}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    goto/16 :goto_2

    :cond_f
    :try_start_d
    move-object/from16 v0, p0

    iget-boolean v7, v0, Ldmo;->k:Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    if-nez v7, :cond_11

    :try_start_e
    invoke-virtual {v11}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v16

    invoke-virtual {v11}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v18

    cmp-long v7, v16, v4

    if-nez v7, :cond_10

    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmo;->f:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v7, v20, v22

    if-nez v7, :cond_11

    cmp-long v7, v18, v4

    if-eqz v7, :cond_11

    :cond_10
    new-instance v3, Ldmt;

    move-object/from16 v0, p0

    iget-wide v12, v0, Ldmo;->f:J

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v9, 0x75

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Inconsistent filechannel status ["

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, Ldmt;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11, v6, v3}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :try_start_f
    monitor-exit v8

    goto/16 :goto_2

    :catchall_0
    move-exception v2

    monitor-exit v8
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :try_start_10
    throw v2
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    :catch_5
    move-exception v3

    :try_start_11
    new-instance v4, Ldmt;

    const/4 v5, 0x1

    invoke-direct {v4, v3, v5}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11, v6, v4}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V

    monitor-exit v8
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_2

    :cond_11
    if-lez v3, :cond_12

    const/4 v7, 0x0

    :try_start_12
    invoke-virtual {v15, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v15, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v15}, Ldmo;->a(Ljava/nio/channels/FileChannel;Ljava/nio/ByteBuffer;)I

    move-result v9

    if-ne v9, v3, :cond_15

    const/4 v7, 0x1

    :goto_8
    new-instance v16, Ljava/lang/StringBuilder;

    const/16 v17, 0x20

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v17, "wrote "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, " != "

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lb;->d(ZLjava/lang/Object;)V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_6
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    int-to-long v0, v3

    move-wide/from16 v16, v0

    add-long v4, v4, v16

    :cond_12
    const/4 v7, -0x1

    if-ne v3, v7, :cond_16

    const/4 v7, 0x1

    :goto_9
    :try_start_13
    move-object/from16 v0, p0

    iget-wide v0, v0, Ldmo;->l:J

    move-wide/from16 v16, v0

    sub-long v16, v4, v16

    move-object/from16 v0, p0

    iget v9, v0, Ldmo;->e:I

    int-to-long v0, v9

    move-wide/from16 v18, v0

    cmp-long v9, v16, v18

    if-gtz v9, :cond_13

    if-eqz v7, :cond_14

    :cond_13
    move-object/from16 v0, p0

    iget-object v7, v0, Ldmo;->b:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, 0x1e

    move/from16 v0, v16

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v16, "progress "

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lezp;->f(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Ldmo;->d:Ldnb;

    move-object/from16 v0, p0

    iget-object v9, v0, Ldmo;->b:Ljava/lang/String;

    invoke-interface {v7, v9, v4, v5}, Ldnb;->b(Ljava/lang/String;J)V

    move-object/from16 v0, p0

    iput-wide v4, v0, Ldmo;->l:J

    :cond_14
    monitor-exit v8

    goto/16 :goto_7

    :cond_15
    const/4 v7, 0x0

    goto/16 :goto_8

    :catch_6
    move-exception v3

    new-instance v4, Ldmt;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11, v6, v4}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V

    monitor-exit v8
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_2

    :cond_16
    const/4 v7, 0x0

    goto :goto_9

    :cond_17
    :try_start_14
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v3, v4, v12

    if-eqz v3, :cond_18

    new-instance v3, Ldmt;

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v4

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x5b

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "download completed with unexpected size "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " expecting "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, Ldmt;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11, v6, v3}, Ldmo;->a(Ljava/io/InputStream;Ljava/nio/channels/FileChannel;Ljava/net/HttpURLConnection;Ldmt;)V

    goto/16 :goto_2

    :cond_18
    const-string v2, "download completed"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldmo;->a(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Ldmo;->d:Ldnb;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldmo;->b:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Ldnb;->a(Ljava/lang/String;Lgje;)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_0

    goto/16 :goto_2

    :cond_19
    move-object v2, v3

    goto/16 :goto_6
.end method
