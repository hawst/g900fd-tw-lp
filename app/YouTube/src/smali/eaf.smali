.class public final Leaf;
.super Lida;
.source "SourceFile"


# instance fields
.field public a:Leaa;

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1841
    invoke-direct {p0}, Lida;-><init>()V

    .line 1846
    const/4 v0, 0x0

    iput-object v0, p0, Leaf;->a:Leaa;

    .line 1866
    iput-boolean v1, p0, Leaf;->b:Z

    .line 1883
    iput-boolean v1, p0, Leaf;->k:Z

    .line 1900
    iput-boolean v1, p0, Leaf;->c:Z

    .line 1917
    iput-boolean v1, p0, Leaf;->d:Z

    .line 1934
    iput-boolean v1, p0, Leaf;->e:Z

    .line 1951
    const-string v0, ""

    iput-object v0, p0, Leaf;->f:Ljava/lang/String;

    .line 1968
    iput-boolean v1, p0, Leaf;->g:Z

    .line 2027
    const/4 v0, -0x1

    iput v0, p0, Leaf;->q:I

    .line 1841
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2029
    iget v0, p0, Leaf;->q:I

    if-gez v0, :cond_0

    .line 2031
    invoke-virtual {p0}, Leaf;->b()I

    .line 2033
    :cond_0
    iget v0, p0, Leaf;->q:I

    return v0
.end method

.method public final a(Leaa;)Leaf;
    .locals 1

    .prologue
    .line 1850
    if-nez p1, :cond_0

    .line 1851
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1853
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leaf;->h:Z

    .line 1854
    iput-object p1, p0, Leaf;->a:Leaa;

    .line 1855
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Leaf;
    .locals 1

    .prologue
    .line 1955
    const/4 v0, 0x1

    iput-boolean v0, p0, Leaf;->o:Z

    .line 1956
    iput-object p1, p0, Leaf;->f:Ljava/lang/String;

    .line 1957
    return-object p0
.end method

.method public final a(Z)Leaf;
    .locals 1

    .prologue
    .line 1870
    const/4 v0, 0x1

    iput-boolean v0, p0, Leaf;->i:Z

    .line 1871
    iput-boolean p1, p0, Leaf;->b:Z

    .line 1872
    return-object p0
.end method

.method public final synthetic a(Licx;)Lida;
    .locals 1

    .prologue
    .line 1839
    :cond_0
    :goto_0
    invoke-virtual {p1}, Licx;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Licx;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Leaa;

    invoke-direct {v0}, Leaa;-><init>()V

    invoke-virtual {p1, v0}, Licx;->a(Lida;)V

    invoke-virtual {p0, v0}, Leaf;->a(Leaa;)Leaf;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Licx;->c()Z

    move-result v0

    invoke-virtual {p0, v0}, Leaf;->a(Z)Leaf;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Licx;->c()Z

    move-result v0

    invoke-virtual {p0, v0}, Leaf;->b(Z)Leaf;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Licx;->c()Z

    move-result v0

    invoke-virtual {p0, v0}, Leaf;->c(Z)Leaf;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Licx;->c()Z

    move-result v0

    invoke-virtual {p0, v0}, Leaf;->d(Z)Leaf;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Licx;->c()Z

    move-result v0

    invoke-virtual {p0, v0}, Leaf;->e(Z)Leaf;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Licx;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Leaf;->a(Ljava/lang/String;)Leaf;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Licx;->c()Z

    move-result v0

    invoke-virtual {p0, v0}, Leaf;->f(Z)Leaf;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Licy;)V
    .locals 2

    .prologue
    .line 2001
    iget-boolean v0, p0, Leaf;->h:Z

    if-eqz v0, :cond_0

    .line 2002
    const/4 v0, 0x1

    iget-object v1, p0, Leaf;->a:Leaa;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILida;)V

    .line 2004
    :cond_0
    iget-boolean v0, p0, Leaf;->i:Z

    if-eqz v0, :cond_1

    .line 2005
    const/4 v0, 0x3

    iget-boolean v1, p0, Leaf;->b:Z

    invoke-virtual {p1, v0, v1}, Licy;->a(IZ)V

    .line 2007
    :cond_1
    iget-boolean v0, p0, Leaf;->j:Z

    if-eqz v0, :cond_2

    .line 2008
    const/4 v0, 0x4

    iget-boolean v1, p0, Leaf;->k:Z

    invoke-virtual {p1, v0, v1}, Licy;->a(IZ)V

    .line 2010
    :cond_2
    iget-boolean v0, p0, Leaf;->l:Z

    if-eqz v0, :cond_3

    .line 2011
    const/4 v0, 0x5

    iget-boolean v1, p0, Leaf;->c:Z

    invoke-virtual {p1, v0, v1}, Licy;->a(IZ)V

    .line 2013
    :cond_3
    iget-boolean v0, p0, Leaf;->m:Z

    if-eqz v0, :cond_4

    .line 2014
    const/4 v0, 0x6

    iget-boolean v1, p0, Leaf;->d:Z

    invoke-virtual {p1, v0, v1}, Licy;->a(IZ)V

    .line 2016
    :cond_4
    iget-boolean v0, p0, Leaf;->n:Z

    if-eqz v0, :cond_5

    .line 2017
    const/4 v0, 0x7

    iget-boolean v1, p0, Leaf;->e:Z

    invoke-virtual {p1, v0, v1}, Licy;->a(IZ)V

    .line 2019
    :cond_5
    iget-boolean v0, p0, Leaf;->o:Z

    if-eqz v0, :cond_6

    .line 2020
    const/16 v0, 0x8

    iget-object v1, p0, Leaf;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILjava/lang/String;)V

    .line 2022
    :cond_6
    iget-boolean v0, p0, Leaf;->p:Z

    if-eqz v0, :cond_7

    .line 2023
    const/16 v0, 0x9

    iget-boolean v1, p0, Leaf;->g:Z

    invoke-virtual {p1, v0, v1}, Licy;->a(IZ)V

    .line 2025
    :cond_7
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 2037
    const/4 v0, 0x0

    .line 2038
    iget-boolean v1, p0, Leaf;->h:Z

    if-eqz v1, :cond_0

    .line 2039
    const/4 v0, 0x1

    .line 2040
    iget-object v1, p0, Leaf;->a:Leaa;

    invoke-static {v0, v1}, Licy;->b(ILida;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2042
    :cond_0
    iget-boolean v1, p0, Leaf;->i:Z

    if-eqz v1, :cond_1

    .line 2043
    const/4 v1, 0x3

    .line 2044
    iget-boolean v2, p0, Leaf;->b:Z

    invoke-static {v1}, Licy;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2046
    :cond_1
    iget-boolean v1, p0, Leaf;->j:Z

    if-eqz v1, :cond_2

    .line 2047
    const/4 v1, 0x4

    .line 2048
    iget-boolean v2, p0, Leaf;->k:Z

    invoke-static {v1}, Licy;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2050
    :cond_2
    iget-boolean v1, p0, Leaf;->l:Z

    if-eqz v1, :cond_3

    .line 2051
    const/4 v1, 0x5

    .line 2052
    iget-boolean v2, p0, Leaf;->c:Z

    invoke-static {v1}, Licy;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2054
    :cond_3
    iget-boolean v1, p0, Leaf;->m:Z

    if-eqz v1, :cond_4

    .line 2055
    const/4 v1, 0x6

    .line 2056
    iget-boolean v2, p0, Leaf;->d:Z

    invoke-static {v1}, Licy;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2058
    :cond_4
    iget-boolean v1, p0, Leaf;->n:Z

    if-eqz v1, :cond_5

    .line 2059
    const/4 v1, 0x7

    .line 2060
    iget-boolean v2, p0, Leaf;->e:Z

    invoke-static {v1}, Licy;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2062
    :cond_5
    iget-boolean v1, p0, Leaf;->o:Z

    if-eqz v1, :cond_6

    .line 2063
    const/16 v1, 0x8

    .line 2064
    iget-object v2, p0, Leaf;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Licy;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2066
    :cond_6
    iget-boolean v1, p0, Leaf;->p:Z

    if-eqz v1, :cond_7

    .line 2067
    const/16 v1, 0x9

    .line 2068
    iget-boolean v2, p0, Leaf;->g:Z

    invoke-static {v1}, Licy;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2070
    :cond_7
    iput v0, p0, Leaf;->q:I

    .line 2071
    return v0
.end method

.method public final b(Z)Leaf;
    .locals 1

    .prologue
    .line 1887
    const/4 v0, 0x1

    iput-boolean v0, p0, Leaf;->j:Z

    .line 1888
    iput-boolean p1, p0, Leaf;->k:Z

    .line 1889
    return-object p0
.end method

.method public final c(Z)Leaf;
    .locals 1

    .prologue
    .line 1904
    const/4 v0, 0x1

    iput-boolean v0, p0, Leaf;->l:Z

    .line 1905
    iput-boolean p1, p0, Leaf;->c:Z

    .line 1906
    return-object p0
.end method

.method public final d(Z)Leaf;
    .locals 1

    .prologue
    .line 1921
    const/4 v0, 0x1

    iput-boolean v0, p0, Leaf;->m:Z

    .line 1922
    iput-boolean p1, p0, Leaf;->d:Z

    .line 1923
    return-object p0
.end method

.method public final e(Z)Leaf;
    .locals 1

    .prologue
    .line 1938
    const/4 v0, 0x1

    iput-boolean v0, p0, Leaf;->n:Z

    .line 1939
    iput-boolean p1, p0, Leaf;->e:Z

    .line 1940
    return-object p0
.end method

.method public final f(Z)Leaf;
    .locals 1

    .prologue
    .line 1972
    const/4 v0, 0x1

    iput-boolean v0, p0, Leaf;->p:Z

    .line 1973
    iput-boolean p1, p0, Leaf;->g:Z

    .line 1974
    return-object p0
.end method
