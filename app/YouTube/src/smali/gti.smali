.class public final Lgti;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:F

.field private b:J

.field private synthetic c:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V
    .locals 0

    .prologue
    .line 821
    iput-object p1, p0, Lgti;->c:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 827
    iget v0, p0, Lgti;->a:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    .line 841
    :cond_0
    :goto_0
    return-void

    .line 831
    :cond_1
    iget v0, p0, Lgti;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 832
    iget-object v0, p0, Lgti;->c:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 833
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lgti;->b:J

    .line 836
    :cond_2
    iput p1, p0, Lgti;->a:F

    .line 837
    invoke-virtual {p0}, Lgti;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 838
    iget-object v0, p0, Lgti;->c:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V

    .line 839
    iget-object v0, p0, Lgti;->c:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 844
    iget v0, p0, Lgti;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 849
    invoke-virtual {p0}, Lgti;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 862
    :goto_0
    return-void

    .line 853
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 854
    iget-wide v2, p0, Lgti;->b:J

    sub-long v2, v0, v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 855
    iget-object v3, p0, Lgti;->c:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    .line 856
    invoke-static {v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->h(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)F

    move-result v3

    mul-float/2addr v2, v3

    iget v3, p0, Lgti;->a:F

    mul-float/2addr v2, v3

    .line 858
    iget-object v3, p0, Lgti;->c:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-static {v3, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;F)V

    .line 860
    iput-wide v0, p0, Lgti;->b:J

    .line 861
    iget-object v0, p0, Lgti;->c:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->postOnAnimation(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
