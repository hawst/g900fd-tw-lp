.class public Lfjp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lhew;

.field public b:Ljava/lang/CharSequence;

.field public c:Lfjo;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;


# direct methods
.method public constructor <init>(Lhew;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhew;

    iput-object v0, p0, Lfjp;->a:Lhew;

    .line 66
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 5

    .prologue
    .line 69
    iget-object v0, p0, Lfjp;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfjp;->d:Ljava/util/List;

    .line 71
    iget-object v0, p0, Lfjp;->a:Lhew;

    iget-object v1, v0, Lhew;->c:[Lher;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 72
    invoke-static {v3}, La;->a(Lher;)Lbt;

    move-result-object v3

    .line 73
    iget-object v4, p0, Lfjp;->d:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_0
    iget-object v0, p0, Lfjp;->d:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 6

    .prologue
    .line 82
    iget-object v0, p0, Lfjp;->e:Ljava/util/List;

    if-nez v0, :cond_1

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfjp;->e:Ljava/util/List;

    .line 84
    iget-object v0, p0, Lfjp;->a:Lhew;

    iget-object v1, v0, Lhew;->e:[Lhex;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 85
    iget-object v4, v3, Lhex;->b:Lhxj;

    if-eqz v4, :cond_0

    .line 86
    iget-object v4, p0, Lfjp;->e:Ljava/util/List;

    new-instance v5, Lfjk;

    iget-object v3, v3, Lhex;->b:Lhxj;

    invoke-direct {v5, v3}, Lfjk;-><init>(Lhxj;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_1
    iget-object v0, p0, Lfjp;->e:Ljava/util/List;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 119
    instance-of v1, p1, Lfjp;

    if-nez v1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lfjp;->a:Lhew;

    iget-wide v2, v1, Lhew;->b:J

    check-cast p1, Lfjp;

    iget-object v1, p1, Lfjp;->a:Lhew;

    iget-wide v4, v1, Lhew;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lfjp;->a:Lhew;

    iget-wide v0, v0, Lhew;->b:J

    long-to-int v0, v0

    return v0
.end method
