.class public final enum Lfrc;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfrc;

.field public static final enum b:Lfrc;

.field public static final enum c:Lfrc;

.field public static final enum d:Lfrc;

.field private static final synthetic e:[Lfrc;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    new-instance v0, Lfrc;

    const-string v1, "SURFACE_VIEW"

    invoke-direct {v0, v1, v2}, Lfrc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfrc;->a:Lfrc;

    .line 66
    new-instance v0, Lfrc;

    const-string v1, "SURFACE_VIEW_SECURE"

    invoke-direct {v0, v1, v3}, Lfrc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfrc;->b:Lfrc;

    .line 67
    new-instance v0, Lfrc;

    const-string v1, "SAFE_TEXTURE_VIEW"

    invoke-direct {v0, v1, v4}, Lfrc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfrc;->c:Lfrc;

    .line 68
    new-instance v0, Lfrc;

    const-string v1, "SERVER_EXPERIMENT"

    invoke-direct {v0, v1, v5}, Lfrc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfrc;->d:Lfrc;

    .line 64
    const/4 v0, 0x4

    new-array v0, v0, [Lfrc;

    sget-object v1, Lfrc;->a:Lfrc;

    aput-object v1, v0, v2

    sget-object v1, Lfrc;->b:Lfrc;

    aput-object v1, v0, v3

    sget-object v1, Lfrc;->c:Lfrc;

    aput-object v1, v0, v4

    sget-object v1, Lfrc;->d:Lfrc;

    aput-object v1, v0, v5

    sput-object v0, Lfrc;->e:[Lfrc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfrc;
    .locals 1

    .prologue
    .line 64
    const-class v0, Lfrc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfrc;

    return-object v0
.end method

.method public static values()[Lfrc;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lfrc;->e:[Lfrc;

    invoke-virtual {v0}, [Lfrc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfrc;

    return-object v0
.end method
