.class public final Leno;
.super Leny;


# instance fields
.field public a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic b:Lenl;


# direct methods
.method constructor <init>(Lenl;)V
    .locals 2

    iput-object p1, p0, Leno;->b:Lenl;

    invoke-direct {p0}, Leny;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private b(JI)V
    .locals 3

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->h(Lenl;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->h(Lenl;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejq;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, p3}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lejq;->a(Ljava/lang/Object;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private f(I)Z
    .locals 3

    invoke-static {}, Lenl;->l()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->i(Lenl;)Lejq;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->i(Lenl;)Lejq;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v2, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v2}, Lejq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Leno;->b:Lenl;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lenl;->b(Lenl;Lejq;)Lejq;

    const/4 v0, 0x1

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    invoke-virtual {p0}, Leno;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lenl;->j()Lenz;

    move-result-object v0

    const-string v1, "ICastDeviceControllerListener.onDisconnected: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Leno;->b:Lenl;

    iget-object v1, v0, Lekr;->b:Landroid/os/Handler;

    iget-object v0, v0, Lekr;->b:Landroid/os/Handler;

    const/4 v2, 0x4

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final a(J)V
    .locals 1

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Leno;->b(JI)V

    goto :goto_0
.end method

.method public final a(JI)V
    .locals 1

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Leno;->b(JI)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0, p1}, Lenl;->a(Lenl;Lcom/google/android/gms/cast/ApplicationMetadata;)Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v0, p0, Leno;->b:Lenl;

    iget-object v1, p1, Lcom/google/android/gms/cast/ApplicationMetadata;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lenl;->a(Lenl;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0, p3}, Lenl;->b(Lenl;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Lenl;->k()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->c(Lenl;)Lejq;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->c(Lenl;)Lejq;

    move-result-object v7

    new-instance v0, Lenm;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lenm;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v7, v0}, Lejq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Leno;->b:Lenl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lenl;->a(Lenl;Lejq;)Lejq;

    :cond_1
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/internal/ig;)V
    .locals 3

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lenl;->j()Lenz;

    move-result-object v0

    const-string v1, "onApplicationStatusChanged"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->e(Lenl;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lenr;

    invoke-direct {v1, p0, p1}, Lenr;-><init>(Leno;Lcom/google/android/gms/internal/ig;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/internal/il;)V
    .locals 3

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lenl;->j()Lenz;

    move-result-object v0

    const-string v1, "onDeviceStatusChanged"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->e(Lenl;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lenq;

    invoke-direct {v1, p0, p1}, Lenq;-><init>(Leno;Lcom/google/android/gms/internal/il;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lenl;->j()Lenz;

    move-result-object v0

    const-string v1, "Receive (type=text, ns=%s) %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->e(Lenl;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lens;

    invoke-direct {v1, p0, p1, p2}, Lens;-><init>(Leno;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 5

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lenl;->j()Lenz;

    move-result-object v0

    const-string v1, "IGNORING: Receive (type=binary, ns=%s) <%d bytes>"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    array-length v4, p2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Leno;->b:Lenl;

    invoke-static {v1}, Lenl;->b(Lenl;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    invoke-static {}, Lenl;->j()Lenz;

    move-result-object v0

    const-string v1, "Deprecated callback: \"onStatusreceived\""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final b(I)V
    .locals 4

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lenl;->k()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->c(Lenl;)Lejq;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->c(Lenl;)Lejq;

    move-result-object v0

    new-instance v2, Lenm;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v3, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-direct {v2, v3}, Lenm;-><init>(Lcom/google/android/gms/common/api/Status;)V

    invoke-interface {v0, v2}, Lejq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Leno;->b:Lenl;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lenl;->a(Lenl;Lejq;)Lejq;

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Leno;->f(I)Z

    goto :goto_0
.end method

.method public final d(I)V
    .locals 1

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Leno;->f(I)Z

    goto :goto_0
.end method

.method public final e(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Leno;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0, v1}, Lenl;->a(Lenl;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0, v1}, Lenl;->b(Lenl;Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0, p1}, Leno;->f(I)Z

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->d(Lenl;)Lehw;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leno;->b:Lenl;

    invoke-static {v0}, Lenl;->e(Lenl;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lenp;

    invoke-direct {v1, p0, p1}, Lenp;-><init>(Leno;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
