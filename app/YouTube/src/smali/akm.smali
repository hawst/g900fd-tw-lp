.class public final Lakm;
.super Lajm;
.source "SourceFile"


# instance fields
.field public final a:Lakw;

.field public b:Lakv;

.field public c:Landroid/view/SurfaceHolder;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lakw;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lajm;-><init>()V

    .line 43
    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakw;

    iput-object v0, p0, Lakm;->a:Lakw;

    .line 44
    const-string v0, "context cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lakm;->d:Landroid/content/Context;

    .line 45
    const-string v0, "uiHandler cannot be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lakm;->e:Landroid/os/Handler;

    .line 46
    return-void
.end method

.method static synthetic a(Lakm;Lakv;)Lakv;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lakm;->b:Lakv;

    return-object p1
.end method

.method static synthetic a(Lakm;)Lakw;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lakm;->a:Lakw;

    return-object v0
.end method

.method static synthetic a(Lakm;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lakm;->c:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic b(Lakm;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lakm;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lakm;)Lakv;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lakm;->b:Lakv;

    return-object v0
.end method

.method static synthetic d(Lakm;)Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lakm;->c:Landroid/view/SurfaceHolder;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lakm;->e:Landroid/os/Handler;

    new-instance v1, Laks;

    invoke-direct {v1, p0, p1}, Laks;-><init>(Lakm;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 142
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lakm;->e:Landroid/os/Handler;

    new-instance v1, Lakr;

    invoke-direct {v1, p0, p1, p2}, Lakr;-><init>(Lakm;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 130
    return-void
.end method

.method public final a(Lany;)V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lakm;->e:Landroid/os/Handler;

    new-instance v1, Lakn;

    invoke-direct {v1, p0, p1}, Lakn;-><init>(Lakm;Lany;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 72
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lakm;->e:Landroid/os/Handler;

    new-instance v1, Lako;

    invoke-direct {v1, p0, p1}, Lako;-><init>(Lakm;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 84
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 105
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 106
    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    .line 107
    iget-object v2, p0, Lakm;->e:Landroid/os/Handler;

    new-instance v3, Lakq;

    invoke-direct {v3, p0, v0, v1}, Lakq;-><init>(Lakm;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 116
    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    .line 117
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final b()Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 88
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 89
    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    .line 90
    iget-object v2, p0, Lakm;->e:Landroid/os/Handler;

    new-instance v3, Lakp;

    invoke-direct {v3, p0, v0, v1}, Lakp;-><init>(Lakm;Ljava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 99
    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    .line 100
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    return-object v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lakm;->e:Landroid/os/Handler;

    new-instance v1, Laku;

    invoke-direct {v1, p0, p1}, Laku;-><init>(Lakm;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 167
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lakm;->e:Landroid/os/Handler;

    new-instance v1, Lakt;

    invoke-direct {v1, p0}, Lakt;-><init>(Lakm;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 154
    return-void
.end method
