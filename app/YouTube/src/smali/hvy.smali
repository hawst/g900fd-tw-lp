.class public final Lhvy;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhxf;

.field public b:Lhgz;

.field public c:Lhgz;

.field public d:Lhjh;

.field public e:Lhji;

.field public f:[Ljava/lang/String;

.field public g:[B

.field private h:Lhgz;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Lhvy;->a:Lhxf;

    iput-object v1, p0, Lhvy;->b:Lhgz;

    iput-object v1, p0, Lhvy;->c:Lhgz;

    iput-object v1, p0, Lhvy;->d:Lhjh;

    iput-object v1, p0, Lhvy;->e:Lhji;

    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Lhvy;->f:[Ljava/lang/String;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhvy;->g:[B

    iput-object v1, p0, Lhvy;->h:Lhgz;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvy;->i:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhvy;->a:Lhxf;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    iget-object v2, p0, Lhvy;->a:Lhxf;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhvy;->b:Lhgz;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhvy;->b:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhvy;->c:Lhgz;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lhvy;->c:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhvy;->d:Lhjh;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lhvy;->d:Lhjh;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhvy;->e:Lhji;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lhvy;->e:Lhji;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhvy;->f:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lhvy;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    iget-object v3, p0, Lhvy;->f:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    add-int/2addr v0, v2

    iget-object v1, p0, Lhvy;->f:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhvy;->g:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_6

    const/16 v1, 0x8

    iget-object v2, p0, Lhvy;->g:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhvy;->h:Lhgz;

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    iget-object v2, p0, Lhvy;->h:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Lhvy;->i:Z

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    iget-boolean v2, p0, Lhvy;->i:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lhvy;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhvy;->J:I

    return v0

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhvy;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhvy;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhvy;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhvy;->a:Lhxf;

    if-nez v0, :cond_2

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhvy;->a:Lhxf;

    :cond_2
    iget-object v0, p0, Lhvy;->a:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhvy;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhvy;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lhvy;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhvy;->c:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhvy;->c:Lhgz;

    :cond_4
    iget-object v0, p0, Lhvy;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhvy;->d:Lhjh;

    if-nez v0, :cond_5

    new-instance v0, Lhjh;

    invoke-direct {v0}, Lhjh;-><init>()V

    iput-object v0, p0, Lhvy;->d:Lhjh;

    :cond_5
    iget-object v0, p0, Lhvy;->d:Lhjh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhvy;->e:Lhji;

    if-nez v0, :cond_6

    new-instance v0, Lhji;

    invoke-direct {v0}, Lhji;-><init>()V

    iput-object v0, p0, Lhvy;->e:Lhji;

    :cond_6
    iget-object v0, p0, Lhvy;->e:Lhji;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhvy;->f:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lhvy;->f:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhvy;->f:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lhvy;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lhvy;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lhvy;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhvy;->g:[B

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhvy;->h:Lhgz;

    if-nez v0, :cond_8

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhvy;->h:Lhgz;

    :cond_8
    iget-object v0, p0, Lhvy;->h:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhvy;->i:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhvy;->a:Lhxf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhvy;->a:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhvy;->b:Lhgz;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhvy;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhvy;->c:Lhgz;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhvy;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhvy;->d:Lhjh;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lhvy;->d:Lhjh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhvy;->e:Lhji;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lhvy;->e:Lhji;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Lhvy;->f:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lhvy;->f:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lhvy;->g:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0x8

    iget-object v1, p0, Lhvy;->g:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_6
    iget-object v0, p0, Lhvy;->h:Lhgz;

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Lhvy;->h:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_7
    iget-boolean v0, p0, Lhvy;->i:Z

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget-boolean v1, p0, Lhvy;->i:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_8
    iget-object v0, p0, Lhvy;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhvy;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhvy;

    iget-object v2, p0, Lhvy;->a:Lhxf;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhvy;->a:Lhxf;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhvy;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhvy;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhvy;->c:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhvy;->c:Lhgz;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhvy;->d:Lhjh;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhvy;->d:Lhjh;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhvy;->e:Lhji;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhvy;->e:Lhji;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhvy;->f:[Ljava/lang/String;

    iget-object v3, p1, Lhvy;->f:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhvy;->g:[B

    iget-object v3, p1, Lhvy;->g:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhvy;->h:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhvy;->h:Lhgz;

    if-nez v2, :cond_3

    :goto_6
    iget-boolean v2, p0, Lhvy;->i:Z

    iget-boolean v3, p1, Lhvy;->i:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhvy;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhvy;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhvy;->a:Lhxf;

    iget-object v3, p1, Lhvy;->a:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhvy;->b:Lhgz;

    iget-object v3, p1, Lhvy;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhvy;->c:Lhgz;

    iget-object v3, p1, Lhvy;->c:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhvy;->d:Lhjh;

    iget-object v3, p1, Lhvy;->d:Lhjh;

    invoke-virtual {v2, v3}, Lhjh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhvy;->e:Lhji;

    iget-object v3, p1, Lhvy;->e:Lhji;

    invoke-virtual {v2, v3}, Lhji;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhvy;->h:Lhgz;

    iget-object v3, p1, Lhvy;->h:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhvy;->I:Ljava/util/List;

    iget-object v3, p1, Lhvy;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhvy;->a:Lhxf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhvy;->b:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhvy;->c:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhvy;->d:Lhjh;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhvy;->e:Lhji;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    iget-object v2, p0, Lhvy;->f:[Ljava/lang/String;

    if-nez v2, :cond_7

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lhvy;->g:[B

    if-nez v0, :cond_9

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhvy;->h:Lhgz;

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhvy;->i:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhvy;->I:Ljava/util/List;

    if-nez v2, :cond_c

    :goto_7
    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v0, p0, Lhvy;->a:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhvy;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lhvy;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lhvy;->d:Lhjh;

    invoke-virtual {v0}, Lhjh;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lhvy;->e:Lhji;

    invoke-virtual {v0}, Lhji;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_7
    move v2, v0

    move v0, v1

    :goto_8
    iget-object v3, p0, Lhvy;->f:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhvy;->f:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_8

    move v2, v1

    :goto_9
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_8
    iget-object v2, p0, Lhvy;->f:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    move v0, v1

    :goto_a
    iget-object v3, p0, Lhvy;->g:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhvy;->g:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_a
    iget-object v0, p0, Lhvy;->h:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_b
    const/4 v0, 0x2

    goto :goto_6

    :cond_c
    iget-object v1, p0, Lhvy;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_7
.end method
