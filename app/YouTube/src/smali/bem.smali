.class public final Lbem;
.super Lbzt;
.source "SourceFile"


# static fields
.field private static final W:Ljava/util/regex/Pattern;

.field private static final X:Ljava/util/regex/Pattern;


# instance fields
.field private Y:Landroid/view/View;

.field private Z:Landroid/view/View;

.field private aA:I

.field private aB:Ljava/lang/String;

.field private aC:Lfrl;

.field private aD:Lgcd;

.field private aE:Lgbu;

.field private aF:Leyt;

.field private aG:Landroid/app/Activity;

.field private aa:Landroid/view/View;

.field private ab:Landroid/view/View;

.field private ac:Landroid/view/View;

.field private ad:Landroid/widget/TextView;

.field private ae:Landroid/view/View;

.field private af:Landroid/view/View;

.field private ag:Landroid/widget/TextView;

.field private ah:Landroid/widget/TextView;

.field private ai:Landroid/widget/TextView;

.field private aj:Landroid/widget/TextView;

.field private ak:Landroid/widget/TextView;

.field private al:Lfvi;

.field private am:Landroid/view/View;

.field private an:Ldaq;

.field private ao:Lfxe;

.field private ap:Leyp;

.field private aq:Leue;

.field private ar:Leue;

.field private as:Leue;

.field private at:Lari;

.field private au:Ldwq;

.field private av:Lcws;

.field private aw:Levn;

.field private ax:I

.field private ay:Lgoh;

.field private az:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const-string v0, "(AL|FL|PL|WL).*"

    .line 89
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbem;->W:Ljava/util/regex/Pattern;

    .line 90
    const-string v0, "RD.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbem;->X:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lbzt;-><init>()V

    .line 64
    return-void
.end method

.method private A()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v2, 0x8

    .line 399
    iget-object v0, p0, Lbem;->Y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lbem;->Z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 401
    iget-object v0, p0, Lbem;->aa:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 403
    iget-object v0, p0, Lbem;->au:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    .line 404
    invoke-virtual {v0}, Ldww;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 460
    :goto_0
    return-void

    .line 408
    :cond_0
    sget-object v1, Ldww;->b:Ldww;

    if-ne v0, v1, :cond_1

    .line 409
    invoke-direct {p0}, Lbem;->B()V

    .line 413
    :cond_1
    iget v0, p0, Lbem;->ax:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    iget v0, p0, Lbem;->ax:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 414
    :cond_2
    iget-object v0, p0, Lbem;->ah:Landroid/widget/TextView;

    iget-object v1, p0, Lbem;->aC:Lfrl;

    invoke-virtual {v1}, Lfrl;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    iget-object v0, p0, Lbem;->aD:Lgcd;

    iget-object v0, v0, Lgcd;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 416
    iget-object v0, p0, Lbem;->ai:Landroid/widget/TextView;

    iget-object v1, p0, Lbem;->aD:Lgcd;

    iget-object v1, v1, Lgcd;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 419
    :cond_3
    iget-object v0, p0, Lbem;->aD:Lgcd;

    iget-object v0, v0, Lgcd;->r:Ljava/util/Date;

    if-eqz v0, :cond_5

    .line 421
    invoke-virtual {p0}, Lbem;->j()Lo;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iget-object v1, p0, Lbem;->aD:Lgcd;

    iget-object v1, v1, Lgcd;->r:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lbem;->aj:Landroid/widget/TextView;

    const v2, 0x7f0902f7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lbem;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v0, p0, Lbem;->aj:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 429
    :goto_1
    iget-object v0, p0, Lbem;->aD:Lgcd;

    iget-object v0, v0, Lgcd;->w:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbem;->aD:Lgcd;

    iget-object v0, v0, Lgcd;->w:Ljava/lang/String;

    .line 430
    :goto_2
    iget-object v1, p0, Lbem;->ak:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446
    :cond_4
    :goto_3
    sget-object v0, Lbet;->b:[I

    iget v1, p0, Lbem;->ax:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 453
    :goto_4
    iget-object v0, p0, Lbem;->al:Lfvi;

    iget-object v1, p0, Lbem;->aC:Lfrl;

    invoke-virtual {v1}, Lfrl;->b()Lfnc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    goto/16 :goto_0

    .line 426
    :cond_5
    iget-object v0, p0, Lbem;->aj:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 429
    :cond_6
    const-string v0, ""

    goto :goto_2

    .line 432
    :cond_7
    iget v0, p0, Lbem;->ax:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_8

    .line 433
    iget-object v0, p0, Lbem;->ah:Landroid/widget/TextView;

    iget-object v1, p0, Lbem;->aE:Lgbu;

    iget-object v1, v1, Lgbu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 434
    iget-object v0, p0, Lbem;->ai:Landroid/widget/TextView;

    iget-object v1, p0, Lbem;->aE:Lgbu;

    iget-object v1, v1, Lgbu;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 435
    iget-object v0, p0, Lbem;->aj:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 436
    iget-object v0, p0, Lbem;->ak:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 438
    :cond_8
    iget v0, p0, Lbem;->ax:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 439
    iget-object v0, p0, Lbem;->ah:Landroid/widget/TextView;

    iget-object v1, p0, Lbem;->aE:Lgbu;

    iget-object v1, v1, Lgbu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    iget-object v0, p0, Lbem;->ai:Landroid/widget/TextView;

    const v1, 0x7f0902f1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 441
    iget-object v0, p0, Lbem;->aj:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 442
    iget-object v0, p0, Lbem;->ak:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 449
    :pswitch_0
    iget-object v0, p0, Lbem;->ag:Landroid/widget/TextView;

    const v1, 0x7f0902ef

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    .line 452
    :pswitch_1
    iget-object v0, p0, Lbem;->ag:Landroid/widget/TextView;

    const v1, 0x7f0902f0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    .line 446
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private B()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 463
    invoke-virtual {p0}, Lbem;->j()Lo;

    move-result-object v0

    check-cast v0, Lbhz;

    iget-object v0, v0, Lbhz;->m:Lfus;

    .line 465
    iget-object v1, p0, Lbem;->aC:Lfrl;

    invoke-virtual {v1}, Lfrl;->g()Lflo;

    move-result-object v1

    invoke-virtual {v1}, Lflo;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 466
    iget-object v1, p0, Lbem;->ab:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 467
    iget-object v1, p0, Lbem;->ae:Landroid/view/View;

    new-instance v2, Lber;

    invoke-direct {v2, p0, v0}, Lber;-><init>(Lbem;Lfus;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 515
    iget-object v0, p0, Lbem;->af:Landroid/view/View;

    new-instance v1, Lbes;

    invoke-direct {v1, p0}, Lbes;-><init>(Lbem;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 548
    :goto_0
    return-void

    .line 545
    :cond_0
    iget-object v0, p0, Lbem;->ad:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 546
    iget-object v0, p0, Lbem;->ad:Landroid/widget/TextView;

    iget-object v1, p0, Lbem;->aC:Lfrl;

    invoke-virtual {v1}, Lfrl;->g()Lflo;

    move-result-object v1

    iget-object v1, v1, Lflo;->a:Lhqn;

    iget-object v1, v1, Lhqn;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lbem;Lfrl;)Lfrl;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lbem;->aC:Lfrl;

    return-object p1
.end method

.method static synthetic a(Lbem;Lgbu;)Lgbu;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lbem;->aE:Lgbu;

    return-object p1
.end method

.method static synthetic a(Lbem;Lgcd;)Lgcd;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lbem;->aD:Lgcd;

    return-object p1
.end method

.method static synthetic a(Lbem;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lbem;->z()V

    return-void
.end method

.method private a(Ldwh;)V
    .locals 6

    .prologue
    .line 293
    iget-object v0, p0, Lbem;->au:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbem;->au:Ldwq;

    .line 294
    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    invoke-virtual {v0}, Ldwr;->b()Ljava/lang/String;

    move-result-object v0

    .line 295
    :goto_0
    iget-object v1, p0, Lbem;->aF:Leyt;

    iget-object v2, p0, Lbem;->aG:Landroid/app/Activity;

    iget v3, p1, Ldwh;->j:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Leyt;->a(Ljava/lang/String;)V

    .line 296
    return-void

    .line 294
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private a(Ldww;)V
    .locals 2

    .prologue
    .line 572
    sget-object v0, Lbet;->c:[I

    invoke-virtual {p1}, Ldww;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 584
    :goto_0
    invoke-virtual {p0}, Lbem;->a()V

    .line 589
    :cond_0
    :goto_1
    return-void

    .line 574
    :pswitch_0
    iget-object v0, p0, Lbem;->ac:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 577
    :pswitch_1
    iget-object v0, p0, Lbem;->ac:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 578
    iget-object v0, p0, Lbem;->aC:Lfrl;

    if-eqz v0, :cond_0

    .line 579
    invoke-direct {p0}, Lbem;->B()V

    goto :goto_1

    .line 583
    :pswitch_2
    iget-object v0, p0, Lbem;->au:Ldwq;

    invoke-interface {v0}, Ldwq;->j()Ldwh;

    move-result-object v0

    invoke-direct {p0, v0}, Lbem;->a(Ldwh;)V

    goto :goto_0

    .line 572
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic b(Lbem;)V
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lbet;->b:[I

    iget v1, p0, Lbem;->ax:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lbem;->aD:Lgcd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbem;->aC:Lfrl;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbem;->A()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lbem;->aE:Lgbu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbem;->aC:Lfrl;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbem;->A()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic c(Lbem;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v0, 0x0

    .line 60
    iput-object v0, p0, Lbem;->aC:Lfrl;

    iput-object v0, p0, Lbem;->aD:Lgcd;

    iput-object v0, p0, Lbem;->aE:Lgbu;

    iget-object v0, p0, Lbem;->Y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbem;->Z:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbem;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic d(Lbem;)Ldwq;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lbem;->au:Ldwq;

    return-object v0
.end method

.method static synthetic e(Lbem;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lbem;->ax:I

    return v0
.end method

.method static synthetic f(Lbem;)Lcws;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lbem;->av:Lcws;

    return-object v0
.end method

.method static synthetic g(Lbem;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lbem;->aB:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lbem;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lbem;->az:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lbem;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lbem;->aA:I

    return v0
.end method

.method private z()V
    .locals 8

    .prologue
    const/16 v2, 0x8

    const/4 v5, -0x1

    .line 299
    iget-object v0, p0, Lbem;->Y:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbem;->Z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbem;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 300
    new-instance v0, Lbeo;

    invoke-direct {v0, p0}, Lbeo;-><init>(Lbem;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbem;->aq:Leue;

    .line 313
    iget-object v0, p0, Lbem;->an:Ldaq;

    iget-object v1, p0, Lbem;->aB:Ljava/lang/String;

    iget-object v2, p0, Lbem;->ay:Lgoh;

    .line 315
    invoke-virtual {v2}, Lgoh;->e()[B

    move-result-object v2

    iget-object v3, p0, Lbem;->ay:Lgoh;

    .line 316
    iget-object v3, v3, Lgoh;->a:Leaa;

    iget-object v3, v3, Leaa;->l:Ljava/lang/String;

    const-string v4, ""

    .line 320
    invoke-virtual {p0}, Lbem;->j()Lo;

    move-result-object v6

    iget-object v7, p0, Lbem;->aq:Leue;

    invoke-static {v6, v7}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v7

    move v6, v5

    .line 313
    invoke-virtual/range {v0 .. v7}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    .line 322
    iget v0, p0, Lbem;->ax:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lbem;->ax:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 324
    :cond_0
    new-instance v0, Lbep;

    invoke-direct {v0, p0}, Lbep;-><init>(Lbem;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbem;->ar:Leue;

    .line 337
    iget-object v0, p0, Lbem;->ao:Lfxe;

    iget-object v1, p0, Lbem;->aB:Ljava/lang/String;

    invoke-virtual {p0}, Lbem;->j()Lo;

    move-result-object v2

    iget-object v3, p0, Lbem;->ar:Leue;

    invoke-static {v2, v3}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lfxe;->d(Ljava/lang/String;Leuc;)V

    .line 340
    :cond_1
    iget v0, p0, Lbem;->ax:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    iget v0, p0, Lbem;->ax:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 341
    :cond_2
    new-instance v0, Lbeq;

    invoke-direct {v0, p0}, Lbeq;-><init>(Lbem;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbem;->as:Leue;

    .line 355
    iget-object v0, p0, Lbem;->ao:Lfxe;

    iget-object v1, p0, Lbem;->az:Ljava/lang/String;

    .line 357
    invoke-virtual {p0}, Lbem;->j()Lo;

    move-result-object v2

    iget-object v3, p0, Lbem;->as:Leue;

    invoke-static {v2, v3}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v2

    .line 355
    invoke-interface {v0, v1, v2}, Lfxe;->b(Ljava/lang/String;Leuc;)V

    .line 359
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 161
    const v0, 0x7f0400ee

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 162
    const v0, 0x7f0802b6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbem;->Y:Landroid/view/View;

    .line 163
    const v0, 0x7f080260

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbem;->Z:Landroid/view/View;

    .line 164
    const v0, 0x7f0801f6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbem;->aa:Landroid/view/View;

    .line 165
    const v0, 0x7f0802bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbem;->ac:Landroid/view/View;

    .line 166
    const v0, 0x7f0802b7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbem;->ab:Landroid/view/View;

    .line 167
    const v0, 0x7f0802ba

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbem;->ad:Landroid/widget/TextView;

    .line 168
    const v0, 0x7f0800e1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbem;->ae:Landroid/view/View;

    .line 169
    const v0, 0x7f0802b8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbem;->ag:Landroid/widget/TextView;

    .line 170
    const v0, 0x7f0802b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbem;->af:Landroid/view/View;

    .line 171
    const v0, 0x7f08008b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbem;->ah:Landroid/widget/TextView;

    .line 172
    const v0, 0x7f080136

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbem;->ai:Landroid/widget/TextView;

    .line 173
    const v0, 0x7f0802bc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbem;->aj:Landroid/widget/TextView;

    .line 174
    const v0, 0x7f0800c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbem;->ak:Landroid/widget/TextView;

    .line 175
    const v0, 0x7f0800a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 176
    new-instance v2, Lfvi;

    iget-object v3, p0, Lbem;->ap:Leyp;

    new-instance v4, Leyr;

    iget-object v5, p0, Lbem;->aG:Landroid/app/Activity;

    const/4 v6, 0x7

    invoke-direct {v4, v5, v6}, Leyr;-><init>(Landroid/content/Context;I)V

    invoke-direct {v2, v3, v4, v0, v7}, Lfvi;-><init>(Leyp;Leym;Landroid/widget/ImageView;Z)V

    iput-object v2, p0, Lbem;->al:Lfvi;

    .line 181
    const v0, 0x7f0802b5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbem;->am:Landroid/view/View;

    .line 183
    return-object v1
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0, p1}, Lbzt;->a(Landroid/app/Activity;)V

    .line 137
    iput-object p1, p0, Lbem;->aG:Landroid/app/Activity;

    .line 138
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 142
    invoke-super {p0, p1}, Lbzt;->a(Landroid/os/Bundle;)V

    .line 144
    iget-object v0, p0, Lbem;->aG:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 145
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v1

    iput-object v1, p0, Lbem;->at:Lari;

    .line 146
    iget-object v1, p0, Lbem;->at:Lari;

    invoke-virtual {v1}, Lari;->f()Larh;

    move-result-object v1

    invoke-virtual {v1}, Larh;->ac()Lfko;

    move-result-object v1

    iget-boolean v1, v1, Lfko;->b:Z

    if-eqz v1, :cond_0

    .line 147
    iget-object v1, p0, Lbem;->at:Lari;

    invoke-virtual {v1}, Lari;->S()Lcws;

    move-result-object v1

    iput-object v1, p0, Lbem;->av:Lcws;

    .line 149
    :cond_0
    iget-object v1, p0, Lbem;->at:Lari;

    iget-object v1, v1, Lari;->b:Ldov;

    invoke-virtual {v1}, Ldov;->b()Ldwq;

    move-result-object v1

    iput-object v1, p0, Lbem;->au:Ldwq;

    .line 150
    iget-object v1, p0, Lbem;->at:Lari;

    iget-object v1, v1, Lari;->b:Ldov;

    invoke-virtual {v1}, Ldov;->g()Ldaq;

    move-result-object v1

    iput-object v1, p0, Lbem;->an:Ldaq;

    .line 151
    iget-object v1, p0, Lbem;->at:Lari;

    invoke-virtual {v1}, Lari;->b()Lfxe;

    move-result-object v1

    iput-object v1, p0, Lbem;->ao:Lfxe;

    .line 152
    iget-object v1, p0, Lbem;->at:Lari;

    invoke-virtual {v1}, Lari;->c()Leyp;

    move-result-object v1

    iput-object v1, p0, Lbem;->ap:Leyp;

    .line 153
    iget-object v1, p0, Lbem;->at:Lari;

    invoke-virtual {v1}, Lari;->ay()Leyt;

    move-result-object v1

    iput-object v1, p0, Lbem;->aF:Leyt;

    .line 154
    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    iput-object v0, p0, Lbem;->aw:Levn;

    .line 155
    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 188
    invoke-super {p0, p1}, Lbzt;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 190
    return-object v0
.end method

.method public final e()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x2

    .line 195
    invoke-super {p0}, Lbzt;->e()V

    .line 196
    invoke-virtual {p0}, Lbem;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "watch"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    .line 198
    iget-object v0, p0, Lbem;->au:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    .line 200
    invoke-direct {p0, v0}, Lbem;->a(Ldww;)V

    .line 201
    invoke-virtual {v0}, Ldww;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lbem;->aw:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 207
    invoke-virtual {p0}, Lbem;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "watch"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgom;

    .line 208
    iget-object v0, v0, Lgom;->a:Lgoh;

    iput-object v0, p0, Lbem;->ay:Lgoh;

    .line 209
    iget-object v0, p0, Lbem;->ay:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    iput-object v0, p0, Lbem;->az:Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lbem;->ay:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget v0, v0, Leaa;->d:I

    iput v0, p0, Lbem;->aA:I

    .line 212
    iget-object v0, p0, Lbem;->ay:Lgoh;

    iget v0, v0, Lgoh;->c:I

    .line 213
    sget-object v1, Lbet;->a:[I

    iget-object v2, p0, Lbem;->ay:Lgoh;

    iget-object v2, v2, Lgoh;->b:Lgoj;

    invoke-virtual {v2}, Lgoj;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 248
    :cond_1
    iput v6, p0, Lbem;->ax:I

    .line 251
    :goto_1
    iget v0, p0, Lbem;->ax:I

    if-ne v0, v6, :cond_7

    .line 252
    iget-object v0, p0, Lbem;->aF:Leyt;

    const v1, 0x7f09001f

    invoke-interface {v0, v1}, Leyt;->a(I)V

    .line 253
    invoke-virtual {p0}, Lbem;->a()V

    goto :goto_0

    .line 215
    :pswitch_0
    iget v0, p0, Lbem;->aA:I

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lbem;->aA:I

    .line 216
    iget-object v0, p0, Lbem;->ay:Lgoh;

    invoke-virtual {v0}, Lgoh;->d()Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lbem;->aA:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbem;->aB:Ljava/lang/String;

    .line 217
    iput v3, p0, Lbem;->ax:I

    goto :goto_1

    .line 220
    :pswitch_1
    iget-object v0, p0, Lbem;->ay:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->a:Ljava/lang/String;

    iput-object v0, p0, Lbem;->aB:Ljava/lang/String;

    .line 221
    iput v3, p0, Lbem;->ax:I

    goto :goto_1

    .line 224
    :pswitch_2
    iget-object v1, p0, Lbem;->ay:Lgoh;

    iget-object v1, v1, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->a:Ljava/lang/String;

    iput-object v1, p0, Lbem;->aB:Ljava/lang/String;

    .line 225
    const-string v1, ""

    iget-object v2, p0, Lbem;->aB:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 228
    sget-object v1, Lbem;->X:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lbem;->az:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    const/4 v0, 0x5

    iput v0, p0, Lbem;->ax:I

    goto :goto_1

    .line 232
    :cond_2
    if-ne v0, v3, :cond_3

    .line 233
    iput v3, p0, Lbem;->ax:I

    goto :goto_1

    .line 234
    :cond_3
    sget-object v1, Lbem;->W:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lbem;->az:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 237
    iget v1, p0, Lbem;->aA:I

    if-lez v1, :cond_4

    if-ne v0, v7, :cond_5

    .line 238
    :cond_4
    iput v4, p0, Lbem;->ax:I

    goto :goto_1

    .line 240
    :cond_5
    iput v7, p0, Lbem;->ax:I

    goto :goto_1

    .line 243
    :cond_6
    iput v3, p0, Lbem;->ax:I

    goto :goto_1

    .line 257
    :cond_7
    iget-object v0, p0, Lbem;->au:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->f:Ldww;

    if-ne v0, v1, :cond_8

    .line 258
    iget-object v0, p0, Lbem;->au:Ldwq;

    invoke-interface {v0}, Ldwq;->j()Ldwh;

    move-result-object v0

    invoke-direct {p0, v0}, Lbem;->a(Ldwh;)V

    .line 259
    invoke-virtual {p0}, Lbem;->a()V

    goto/16 :goto_0

    .line 264
    :cond_8
    iget v0, p0, Lbem;->ax:I

    if-ne v0, v3, :cond_c

    .line 265
    const-string v0, ""

    iput-object v0, p0, Lbem;->az:Ljava/lang/String;

    .line 266
    const/4 v0, -0x1

    iput v0, p0, Lbem;->aA:I

    .line 271
    :cond_9
    :goto_2
    iget v0, p0, Lbem;->ax:I

    if-eq v0, v4, :cond_a

    iget v0, p0, Lbem;->ax:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_b

    .line 273
    :cond_a
    invoke-virtual {p0}, Lbem;->k()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 274
    invoke-virtual {p0}, Lbem;->k()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 275
    iget-object v2, p0, Lbem;->am:Landroid/view/View;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 276
    iget-object v2, p0, Lbem;->aa:Landroid/view/View;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 279
    iget-object v2, p0, Lbem;->Z:Landroid/view/View;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 282
    :cond_b
    iget-object v0, p0, Lbem;->Z:Landroid/view/View;

    const v1, 0x7f0802b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lben;

    invoke-direct {v1, p0}, Lben;-><init>(Lbem;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    invoke-direct {p0}, Lbem;->z()V

    goto/16 :goto_0

    .line 267
    :cond_c
    iget v0, p0, Lbem;->ax:I

    if-ne v0, v4, :cond_9

    .line 268
    iput v5, p0, Lbem;->aA:I

    goto :goto_2

    .line 213
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 363
    invoke-super {p0}, Lbzt;->f()V

    .line 364
    iget-object v0, p0, Lbem;->aq:Leue;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lbem;->aq:Leue;

    iput-boolean v1, v0, Leue;->a:Z

    .line 366
    iput-object v2, p0, Lbem;->aq:Leue;

    .line 368
    :cond_0
    iget-object v0, p0, Lbem;->ar:Leue;

    if-eqz v0, :cond_1

    .line 369
    iget-object v0, p0, Lbem;->ar:Leue;

    iput-boolean v1, v0, Leue;->a:Z

    .line 370
    iput-object v2, p0, Lbem;->ar:Leue;

    .line 372
    :cond_1
    iget-object v0, p0, Lbem;->as:Leue;

    if-eqz v0, :cond_2

    .line 373
    iget-object v0, p0, Lbem;->as:Leue;

    iput-boolean v1, v0, Leue;->a:Z

    .line 374
    iput-object v2, p0, Lbem;->as:Leue;

    .line 376
    :cond_2
    iget-object v0, p0, Lbem;->aw:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 377
    return-void
.end method

.method public final onMdxStateChangedEvent(Ldwx;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 568
    iget-object v0, p1, Ldwx;->a:Ldww;

    invoke-direct {p0, v0}, Lbem;->a(Ldww;)V

    .line 569
    return-void
.end method
