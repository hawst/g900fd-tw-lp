.class public final Lxv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lxr;


# instance fields
.field private synthetic a:Z

.field private synthetic b:Lcom/android/volley/toolbox/NetworkImageView;


# direct methods
.method public constructor <init>(Lcom/android/volley/toolbox/NetworkImageView;Z)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lxv;->b:Lcom/android/volley/toolbox/NetworkImageView;

    iput-boolean p2, p0, Lxv;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lxq;Z)V
    .locals 2

    .prologue
    .line 156
    if-eqz p2, :cond_1

    iget-boolean v0, p0, Lxv;->a:Z

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lxv;->b:Lcom/android/volley/toolbox/NetworkImageView;

    new-instance v1, Lxw;

    invoke-direct {v1, p0, p1}, Lxw;-><init>(Lxv;Lxq;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/toolbox/NetworkImageView;->post(Ljava/lang/Runnable;)Z

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v0, p1, Lxq;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 167
    iget-object v0, p0, Lxv;->b:Lcom/android/volley/toolbox/NetworkImageView;

    iget-object v1, p1, Lxq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/android/volley/toolbox/NetworkImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 168
    :cond_2
    iget-object v0, p0, Lxv;->b:Lcom/android/volley/toolbox/NetworkImageView;

    invoke-static {v0}, Lcom/android/volley/toolbox/NetworkImageView;->b(Lcom/android/volley/toolbox/NetworkImageView;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lxv;->b:Lcom/android/volley/toolbox/NetworkImageView;

    iget-object v1, p0, Lxv;->b:Lcom/android/volley/toolbox/NetworkImageView;

    invoke-static {v1}, Lcom/android/volley/toolbox/NetworkImageView;->b(Lcom/android/volley/toolbox/NetworkImageView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/volley/toolbox/NetworkImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public final onErrorResponse(Lxa;)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lxv;->b:Lcom/android/volley/toolbox/NetworkImageView;

    invoke-static {v0}, Lcom/android/volley/toolbox/NetworkImageView;->a(Lcom/android/volley/toolbox/NetworkImageView;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lxv;->b:Lcom/android/volley/toolbox/NetworkImageView;

    iget-object v1, p0, Lxv;->b:Lcom/android/volley/toolbox/NetworkImageView;

    invoke-static {v1}, Lcom/android/volley/toolbox/NetworkImageView;->a(Lcom/android/volley/toolbox/NetworkImageView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/volley/toolbox/NetworkImageView;->setImageResource(I)V

    .line 148
    :cond_0
    return-void
.end method
