.class public final enum Ldbu;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldbu;

.field public static final enum b:Ldbu;

.field public static final enum c:Ldbu;

.field public static final enum d:Ldbu;

.field public static final enum e:Ldbu;

.field private static final synthetic o:[Ldbu;


# instance fields
.field public final f:Z

.field public final g:Z

.field public final h:I

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    .line 57
    new-instance v0, Ldbu;

    const-string v1, "YOUTUBE"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const v5, -0x4d33e7e2

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-direct/range {v0 .. v11}, Ldbu;-><init>(Ljava/lang/String;IZZIZZZZZZ)V

    sput-object v0, Ldbu;->a:Ldbu;

    .line 58
    new-instance v0, Ldbu;

    const-string v1, "REMOTE"

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const v5, -0x4d33e7e2

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-direct/range {v0 .. v11}, Ldbu;-><init>(Ljava/lang/String;IZZIZZZZZZ)V

    sput-object v0, Ldbu;->b:Ldbu;

    .line 59
    new-instance v0, Ldbu;

    const-string v1, "AD"

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    const v5, -0x1744d5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Ldbu;-><init>(Ljava/lang/String;IZZIZZZZZZ)V

    sput-object v0, Ldbu;->c:Ldbu;

    .line 60
    new-instance v0, Ldbu;

    const-string v1, "LIVE"

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x1

    const v5, -0x4d33e7e2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Ldbu;-><init>(Ljava/lang/String;IZZIZZZZZZ)V

    sput-object v0, Ldbu;->d:Ldbu;

    .line 61
    new-instance v0, Ldbu;

    const-string v1, "HIDDEN"

    const/4 v2, 0x4

    const/4 v3, 0x1

    const/4 v4, 0x0

    const v5, -0x4d33e7e2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Ldbu;-><init>(Ljava/lang/String;IZZIZZZZZZ)V

    sput-object v0, Ldbu;->e:Ldbu;

    .line 56
    const/4 v0, 0x5

    new-array v0, v0, [Ldbu;

    const/4 v1, 0x0

    sget-object v2, Ldbu;->a:Ldbu;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ldbu;->b:Ldbu;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Ldbu;->c:Ldbu;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Ldbu;->d:Ldbu;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Ldbu;->e:Ldbu;

    aput-object v2, v0, v1

    sput-object v0, Ldbu;->o:[Ldbu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZIZZZZZZ)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 83
    iput-boolean p3, p0, Ldbu;->f:Z

    .line 84
    iput-boolean p4, p0, Ldbu;->g:Z

    .line 85
    iput p5, p0, Ldbu;->h:I

    .line 86
    iput-boolean p6, p0, Ldbu;->i:Z

    .line 87
    iput-boolean p7, p0, Ldbu;->j:Z

    .line 88
    iput-boolean p8, p0, Ldbu;->k:Z

    .line 89
    iput-boolean p9, p0, Ldbu;->l:Z

    .line 90
    iput-boolean p10, p0, Ldbu;->m:Z

    .line 91
    iput-boolean p11, p0, Ldbu;->n:Z

    .line 92
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbu;
    .locals 1

    .prologue
    .line 56
    const-class v0, Ldbu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbu;

    return-object v0
.end method

.method public static values()[Ldbu;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Ldbu;->o:[Ldbu;

    invoke-virtual {v0}, [Ldbu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbu;

    return-object v0
.end method
