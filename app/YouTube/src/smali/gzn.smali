.class public final Lgzn;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lhiq;

.field private c:Z

.field private d:Lgzm;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8845
    invoke-direct {p0}, Lidf;-><init>()V

    .line 8848
    const-string v0, ""

    iput-object v0, p0, Lgzn;->a:Ljava/lang/String;

    .line 8851
    iput-object v1, p0, Lgzn;->b:Lhiq;

    .line 8854
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgzn;->c:Z

    .line 8857
    iput-object v1, p0, Lgzn;->d:Lgzm;

    .line 8845
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 8912
    const/4 v0, 0x0

    .line 8913
    iget-object v1, p0, Lgzn;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 8914
    const/4 v0, 0x1

    iget-object v1, p0, Lgzn;->a:Ljava/lang/String;

    .line 8915
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8917
    :cond_0
    iget-object v1, p0, Lgzn;->b:Lhiq;

    if-eqz v1, :cond_1

    .line 8918
    const/4 v1, 0x2

    iget-object v2, p0, Lgzn;->b:Lhiq;

    .line 8919
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8921
    :cond_1
    iget-boolean v1, p0, Lgzn;->c:Z

    if-eqz v1, :cond_2

    .line 8922
    const/4 v1, 0x3

    iget-boolean v2, p0, Lgzn;->c:Z

    .line 8923
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8925
    :cond_2
    iget-object v1, p0, Lgzn;->d:Lgzm;

    if-eqz v1, :cond_3

    .line 8926
    const/4 v1, 0x4

    iget-object v2, p0, Lgzn;->d:Lgzm;

    .line 8927
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8929
    :cond_3
    iget-object v1, p0, Lgzn;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8930
    iput v0, p0, Lgzn;->J:I

    .line 8931
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 8841
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzn;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzn;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzn;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgzn;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgzn;->b:Lhiq;

    if-nez v0, :cond_2

    new-instance v0, Lhiq;

    invoke-direct {v0}, Lhiq;-><init>()V

    iput-object v0, p0, Lgzn;->b:Lhiq;

    :cond_2
    iget-object v0, p0, Lgzn;->b:Lhiq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgzn;->c:Z

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lgzn;->d:Lgzm;

    if-nez v0, :cond_3

    new-instance v0, Lgzm;

    invoke-direct {v0}, Lgzm;-><init>()V

    iput-object v0, p0, Lgzn;->d:Lgzm;

    :cond_3
    iget-object v0, p0, Lgzn;->d:Lgzm;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 8894
    iget-object v0, p0, Lgzn;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8895
    const/4 v0, 0x1

    iget-object v1, p0, Lgzn;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 8897
    :cond_0
    iget-object v0, p0, Lgzn;->b:Lhiq;

    if-eqz v0, :cond_1

    .line 8898
    const/4 v0, 0x2

    iget-object v1, p0, Lgzn;->b:Lhiq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8900
    :cond_1
    iget-boolean v0, p0, Lgzn;->c:Z

    if-eqz v0, :cond_2

    .line 8901
    const/4 v0, 0x3

    iget-boolean v1, p0, Lgzn;->c:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 8903
    :cond_2
    iget-object v0, p0, Lgzn;->d:Lgzm;

    if-eqz v0, :cond_3

    .line 8904
    const/4 v0, 0x4

    iget-object v1, p0, Lgzn;->d:Lgzm;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8906
    :cond_3
    iget-object v0, p0, Lgzn;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 8908
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8871
    if-ne p1, p0, :cond_1

    .line 8878
    :cond_0
    :goto_0
    return v0

    .line 8872
    :cond_1
    instance-of v2, p1, Lgzn;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 8873
    :cond_2
    check-cast p1, Lgzn;

    .line 8874
    iget-object v2, p0, Lgzn;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzn;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzn;->b:Lhiq;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzn;->b:Lhiq;

    if-nez v2, :cond_3

    .line 8875
    :goto_2
    iget-boolean v2, p0, Lgzn;->c:Z

    iget-boolean v3, p1, Lgzn;->c:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzn;->d:Lgzm;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzn;->d:Lgzm;

    if-nez v2, :cond_3

    .line 8877
    :goto_3
    iget-object v2, p0, Lgzn;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgzn;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 8878
    goto :goto_0

    .line 8874
    :cond_4
    iget-object v2, p0, Lgzn;->a:Ljava/lang/String;

    iget-object v3, p1, Lgzn;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzn;->b:Lhiq;

    iget-object v3, p1, Lgzn;->b:Lhiq;

    .line 8875
    invoke-virtual {v2, v3}, Lhiq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgzn;->d:Lgzm;

    iget-object v3, p1, Lgzn;->d:Lgzm;

    .line 8877
    invoke-virtual {v2, v3}, Lgzm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgzn;->I:Ljava/util/List;

    iget-object v3, p1, Lgzn;->I:Ljava/util/List;

    .line 8878
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8882
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 8884
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzn;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 8885
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzn;->b:Lhiq;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 8886
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lgzn;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    add-int/2addr v0, v2

    .line 8887
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzn;->d:Lgzm;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 8888
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzn;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 8889
    return v0

    .line 8884
    :cond_0
    iget-object v0, p0, Lgzn;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8885
    :cond_1
    iget-object v0, p0, Lgzn;->b:Lhiq;

    invoke-virtual {v0}, Lhiq;->hashCode()I

    move-result v0

    goto :goto_1

    .line 8886
    :cond_2
    const/4 v0, 0x2

    goto :goto_2

    .line 8887
    :cond_3
    iget-object v0, p0, Lgzn;->d:Lgzm;

    invoke-virtual {v0}, Lgzm;->hashCode()I

    move-result v0

    goto :goto_3

    .line 8888
    :cond_4
    iget-object v1, p0, Lgzn;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
