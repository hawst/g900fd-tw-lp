.class public final Lenl;
.super Lekr;


# static fields
.field private static final A:Ljava/lang/Object;

.field private static final j:Lenz;

.field private static final z:Ljava/lang/Object;


# instance fields
.field public final c:Ljava/util/Map;

.field public d:Leno;

.field public e:Z

.field public f:Z

.field public g:D

.field public final h:Ljava/util/concurrent/atomic/AtomicLong;

.field public i:Ljava/util/Map;

.field private final k:Lcom/google/android/gms/cast/CastDevice;

.field private final l:Lehw;

.field private final m:Landroid/os/Handler;

.field private final n:J

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Z

.field private r:I

.field private s:I

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Landroid/os/Bundle;

.field private w:Lenn;

.field private x:Lejq;

.field private y:Lejq;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lenz;

    const-string v1, "CastClientImpl"

    invoke-direct {v0, v1}, Lenz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lenl;->j:Lenz;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lenl;->z:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lenl;->A:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/cast/CastDevice;JLehw;Lejt;Lejc;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p7

    move-object v4, p8

    invoke-direct/range {v0 .. v5}, Lekr;-><init>(Landroid/content/Context;Landroid/os/Looper;Lejt;Lejc;[Ljava/lang/String;)V

    iput-object p3, p0, Lenl;->k:Lcom/google/android/gms/cast/CastDevice;

    iput-object p6, p0, Lenl;->l:Lehw;

    iput-wide p4, p0, Lenl;->n:J

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lenl;->m:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lenl;->c:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lenl;->h:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lenl;->i:Ljava/util/Map;

    invoke-direct {p0}, Lenl;->m()V

    new-instance v0, Lenn;

    invoke-direct {v0, p0}, Lenn;-><init>(Lenl;)V

    iput-object v0, p0, Lenl;->w:Lenn;

    iget-object v0, p0, Lenl;->w:Lenn;

    invoke-virtual {p0, v0}, Lenl;->a(Lejc;)V

    return-void
.end method

.method static synthetic a(Lenl;Lcom/google/android/gms/cast/ApplicationMetadata;)Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 0

    return-object p1
.end method

.method static synthetic a(Lenl;Lejq;)Lejq;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lenl;->x:Lejq;

    return-object v0
.end method

.method static synthetic a(Lenl;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lenl;->t:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lenl;)V
    .locals 0

    invoke-direct {p0}, Lenl;->n()V

    return-void
.end method

.method static synthetic a(Lenl;Lcom/google/android/gms/internal/ig;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p1, Lcom/google/android/gms/internal/ig;->b:Ljava/lang/String;

    iget-object v3, p0, Lenl;->o:Ljava/lang/String;

    invoke-static {v0, v3}, La;->f(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iput-object v0, p0, Lenl;->o:Ljava/lang/String;

    move v0, v1

    :goto_0
    sget-object v3, Lenl;->j:Lenz;

    const-string v4, "hasChanged=%b, mFirstApplicationStatusUpdate=%b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lenl;->p:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lenl;->l:Lehw;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lenl;->p:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lenl;->l:Lehw;

    :cond_1
    iput-boolean v2, p0, Lenl;->p:Z

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lenl;Lcom/google/android/gms/internal/il;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p1, Lcom/google/android/gms/internal/il;->e:Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-wide v4, p1, Lcom/google/android/gms/internal/il;->b:D

    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_9

    iget-wide v6, p0, Lenl;->g:D

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_9

    iput-wide v4, p0, Lenl;->g:D

    move v0, v1

    :goto_0
    iget-boolean v3, p1, Lcom/google/android/gms/internal/il;->c:Z

    iget-boolean v4, p0, Lenl;->e:Z

    if-eq v3, v4, :cond_0

    iput-boolean v3, p0, Lenl;->e:Z

    move v0, v1

    :cond_0
    sget-object v3, Lenl;->j:Lenz;

    const-string v4, "hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lenl;->q:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lenl;->l:Lehw;

    if-eqz v3, :cond_2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lenl;->q:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lenl;->l:Lehw;

    invoke-virtual {v0}, Lehw;->a()V

    :cond_2
    iget v0, p1, Lcom/google/android/gms/internal/il;->d:I

    iget v3, p0, Lenl;->r:I

    if-eq v0, v3, :cond_8

    iput v0, p0, Lenl;->r:I

    move v0, v1

    :goto_1
    sget-object v3, Lenl;->j:Lenz;

    const-string v4, "hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lenl;->q:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lenl;->l:Lehw;

    if-eqz v3, :cond_4

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lenl;->q:Z

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lenl;->l:Lehw;

    iget v0, p0, Lenl;->r:I

    :cond_4
    iget v0, p1, Lcom/google/android/gms/internal/il;->f:I

    iget v3, p0, Lenl;->s:I

    if-eq v0, v3, :cond_7

    iput v0, p0, Lenl;->s:I

    move v0, v1

    :goto_2
    sget-object v3, Lenl;->j:Lenz;

    const-string v4, "hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lenl;->q:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lenl;->l:Lehw;

    if-eqz v1, :cond_6

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lenl;->q:Z

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lenl;->l:Lehw;

    iget v0, p0, Lenl;->s:I

    :cond_6
    iput-boolean v2, p0, Lenl;->q:Z

    return-void

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    move v0, v2

    goto/16 :goto_0
.end method

.method static synthetic b(Lenl;Lejq;)Lejq;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lenl;->y:Lejq;

    return-object v0
.end method

.method static synthetic b(Lenl;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lenl;->u:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lenl;)V
    .locals 0

    invoke-direct {p0}, Lenl;->m()V

    return-void
.end method

.method static synthetic c(Lenl;)Lejq;
    .locals 1

    iget-object v0, p0, Lenl;->x:Lejq;

    return-object v0
.end method

.method static synthetic d(Lenl;)Lehw;
    .locals 1

    iget-object v0, p0, Lenl;->l:Lehw;

    return-object v0
.end method

.method static synthetic e(Lenl;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lenl;->m:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lenl;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lenl;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic g(Lenl;)Lcom/google/android/gms/cast/CastDevice;
    .locals 1

    iget-object v0, p0, Lenl;->k:Lcom/google/android/gms/cast/CastDevice;

    return-object v0
.end method

.method static synthetic h(Lenl;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lenl;->i:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic i(Lenl;)Lejq;
    .locals 1

    iget-object v0, p0, Lenl;->y:Lejq;

    return-object v0
.end method

.method static synthetic j()Lenz;
    .locals 1

    sget-object v0, Lenl;->j:Lenz;

    return-object v0
.end method

.method static synthetic k()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lenl;->z:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic l()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lenl;->A:Ljava/lang/Object;

    return-object v0
.end method

.method private m()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, -0x1

    iput-boolean v2, p0, Lenl;->f:Z

    iput v0, p0, Lenl;->r:I

    iput v0, p0, Lenl;->s:I

    const/4 v0, 0x0

    iput-object v0, p0, Lenl;->o:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lenl;->g:D

    iput-boolean v2, p0, Lenl;->e:Z

    return-void
.end method

.method private n()V
    .locals 3

    sget-object v0, Lenl;->j:Lenz;

    const-string v1, "removing all MessageReceivedCallbacks"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lenl;->c:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lenl;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lenv;->a(Landroid/os/IBinder;)Lenu;

    move-result-object v0

    return-object v0
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 7

    const/16 v6, 0x3e9

    const/4 v0, 0x0

    const/4 v5, 0x1

    sget-object v1, Lenl;->j:Lenz;

    const-string v2, "in onPostInitHandler; statusCode=%d"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    if-ne p1, v6, :cond_2

    :cond_0
    iput-boolean v5, p0, Lenl;->f:Z

    iput-boolean v5, p0, Lenl;->p:Z

    iput-boolean v5, p0, Lenl;->q:Z

    :goto_0
    if-ne p1, v6, :cond_1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lenl;->v:Landroid/os/Bundle;

    iget-object v1, p0, Lenl;->v:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move p1, v0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lekr;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_2
    iput-boolean v0, p0, Lenl;->f:Z

    goto :goto_0
.end method

.method public a(Lejq;)V
    .locals 5

    sget-object v1, Lenl;->z:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lenl;->x:Lejq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lenl;->x:Lejq;

    new-instance v2, Lenm;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    const/16 v4, 0x7d2

    invoke-direct {v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-direct {v2, v3}, Lenm;-><init>(Lcom/google/android/gms/common/api/Status;)V

    invoke-interface {v0, v2}, Lejq;->a(Ljava/lang/Object;)V

    :cond_0
    iput-object p1, p0, Lenl;->x:Lejq;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final a(Lelk;Lekv;)V
    .locals 6

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    sget-object v0, Lenl;->j:Lenz;

    const-string v1, "getServiceFromBroker(): mLastApplicationId=%s, mLastSessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lenl;->t:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lenl;->u:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lenl;->k:Lcom/google/android/gms/cast/CastDevice;

    const-string v1, "com.google.android.gms.cast.EXTRA_CAST_DEVICE"

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "com.google.android.gms.cast.EXTRA_CAST_FLAGS"

    iget-wide v2, p0, Lenl;->n:J

    invoke-virtual {v5, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lenl;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "last_application_id"

    iget-object v1, p0, Lenl;->t:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lenl;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "last_session_id"

    iget-object v1, p0, Lenl;->u:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Leno;

    invoke-direct {v0, p0}, Leno;-><init>(Lenl;)V

    iput-object v0, p0, Lenl;->d:Leno;

    const v2, 0x5e3530

    iget-object v0, p0, Lekr;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lenl;->d:Leno;

    invoke-virtual {v0}, Leno;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lelk;->a(Lelh;ILjava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Channel namespace cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lenl;->c:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lenl;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehx;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lenl;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lenu;

    invoke-interface {v0, p1}, Lenu;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    sget-object v1, Lenl;->j:Lenz;

    const-string v2, "Error unregistering namespace (%s): %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lenz;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lejq;)V
    .locals 3

    sget-object v1, Lenl;->A:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lenl;->y:Lejq;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x7d1

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p2, v0}, Lejq;->a(Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-virtual {p0}, Lenl;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lenu;

    invoke-interface {v0, p1}, Lenu;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    :try_start_1
    iput-object p2, p0, Lenl;->y:Lejq;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lenl;->j:Lenz;

    const-string v1, "disconnect(); ServiceListener=%s, isConnected=%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lenl;->d:Leno;

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lenl;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lenl;->d:Leno;

    const/4 v1, 0x0

    iput-object v1, p0, Lenl;->d:Leno;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Leno;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lenl;->j:Lenz;

    const-string v1, "already disposed, so short-circuiting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lenl;->n()V

    :try_start_0
    invoke-virtual {p0}, Lenl;->c()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lenl;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lenl;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lenu;

    invoke-interface {v0}, Lenu;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-super {p0}, Lekr;->b()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v1, Lenl;->j:Lenz;

    const-string v2, "Error while disconnecting the controller interface: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lenz;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, Lekr;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Lekr;->b()V

    throw v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.internal.ICastDeviceController"

    return-object v0
.end method

.method public final x_()Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, Lenl;->v:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lenl;->v:Landroid/os/Bundle;

    const/4 v1, 0x0

    iput-object v1, p0, Lenl;->v:Landroid/os/Bundle;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lekr;->x_()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method
