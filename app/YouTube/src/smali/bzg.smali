.class public final Lbzg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbzj;


# instance fields
.field final synthetic a:Lbzd;

.field private final b:Landroid/view/View;

.field private final c:Z

.field private final d:Landroid/content/res/Resources;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Lcfy;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/ImageView;

.field private final j:Landroid/widget/FrameLayout;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/ImageView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/view/ViewStub;

.field private p:Landroid/widget/LinearLayout;

.field private q:I


# direct methods
.method public constructor <init>(Lbzd;Landroid/view/View;Landroid/content/res/Resources;Z)V
    .locals 3

    .prologue
    .line 101
    iput-object p1, p0, Lbzg;->a:Lbzd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lbzg;->b:Landroid/view/View;

    .line 103
    iput-boolean p4, p0, Lbzg;->c:Z

    .line 104
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lbzg;->d:Landroid/content/res/Resources;

    .line 105
    const v0, 0x7f0802a4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 106
    const v0, 0x7f08008b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzg;->e:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0802a6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzg;->f:Landroid/widget/TextView;

    .line 109
    new-instance v0, Lcfy;

    iget-object v2, p1, Lbzd;->d:Landroid/view/View$OnClickListener;

    invoke-direct {v0, p2, v2}, Lcfy;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lbzg;->g:Lcfy;

    .line 111
    const v0, 0x7f0800c5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzg;->h:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0801c4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbzg;->i:Landroid/widget/ImageView;

    .line 114
    iget-object v0, p0, Lbzg;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lbzg;->i:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 118
    :cond_0
    const v0, 0x7f08033f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lbzg;->j:Landroid/widget/FrameLayout;

    .line 119
    iget-object v0, p0, Lbzg;->j:Landroid/widget/FrameLayout;

    iget-object v2, p1, Lbzd;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const v0, 0x7f080341

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzg;->k:Landroid/widget/TextView;

    .line 121
    const v0, 0x7f080340

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbzg;->l:Landroid/widget/ImageView;

    .line 123
    const v0, 0x7f0802ab

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lbzg;->o:Landroid/view/ViewStub;

    .line 124
    const v0, 0x7f0802ac

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    .line 126
    iget-object v0, p0, Lbzg;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 127
    new-instance v0, Lbzh;

    invoke-direct {v0, p0, p1}, Lbzh;-><init>(Lbzg;Lbzd;)V

    .line 133
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lbzg;->g:Lcfy;

    .line 240
    iget-object v0, p0, Lbzg;->g:Lcfy;

    invoke-virtual {v0}, Lcfy;->e()V

    .line 244
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lbzg;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 292
    return-void
.end method

.method public final a(Lgmb;Lflh;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 250
    iget-object v0, p0, Lbzg;->g:Lcfy;

    .line 251
    if-nez p1, :cond_1

    .line 255
    if-nez p2, :cond_0

    .line 256
    iget-object v0, p0, Lbzg;->g:Lcfy;

    iget-object v0, p0, Lbzg;->g:Lcfy;

    invoke-virtual {v0}, Lcfy;->a()V

    .line 265
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-boolean v0, p2, Lflh;->a:Z

    if-nez v0, :cond_1

    .line 259
    iget-object v0, p0, Lbzg;->g:Lcfy;

    iget-object v0, p0, Lbzg;->g:Lcfy;

    invoke-virtual {v0, v2}, Lcfy;->a(Z)V

    iget-object v0, p0, Lbzg;->g:Lcfy;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Lcbf;->a(F)V

    invoke-virtual {v0}, Lcbf;->d()V

    iget-object v1, v0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->b()V

    iget-object v0, v0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(II)V

    goto :goto_0

    .line 263
    :cond_1
    iget-object v0, p0, Lbzg;->g:Lcfy;

    invoke-virtual {v0, v3}, Lcfy;->a(Z)V

    .line 264
    iget-object v0, p0, Lbzg;->g:Lcfy;

    invoke-virtual {v0, p1}, Lcfy;->a(Lgmb;)Landroid/view/View;

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lbzg;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    return-void
.end method

.method public final a(Ljava/util/List;I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 178
    iget-object v0, p0, Lbzg;->o:Landroid/view/ViewStub;

    if-nez v0, :cond_1

    .line 202
    :cond_0
    return-void

    .line 181
    :cond_1
    iget-object v0, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    .line 182
    iget-object v0, p0, Lbzg;->o:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    .line 184
    :cond_2
    iput p2, p0, Lbzg;->q:I

    .line 185
    iget-object v0, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 186
    iget-object v0, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    :cond_3
    move v2, v3

    .line 188
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 189
    iget-object v0, p0, Lbzg;->a:Lbzd;

    iget-object v0, v0, Lbzd;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04013f

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 191
    const v0, 0x7f08008b

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 192
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfks;

    invoke-virtual {v1}, Lfks;->a()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    const v0, 0x7f0801f6

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 194
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfks;

    invoke-virtual {v1}, Lfks;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget v0, p0, Lbzg;->q:I

    if-lt v2, v0, :cond_4

    .line 196
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 200
    :goto_1
    iget-object v0, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 188
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 198
    :cond_4
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 206
    iget-boolean v0, p0, Lbzg;->c:Z

    if-nez v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-object v0, p0, Lbzg;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 210
    iget-object v1, p0, Lbzg;->h:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lbzg;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 212
    iget-object v1, p0, Lbzg;->i:Landroid/widget/ImageView;

    if-eqz p1, :cond_4

    const v0, 0x7f020085

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 213
    iget-object v1, p0, Lbzg;->i:Landroid/widget/ImageView;

    iget-object v2, p0, Lbzg;->d:Landroid/content/res/Resources;

    if-eqz p1, :cond_5

    const v0, 0x7f0902aa

    .line 214
    :goto_3
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 213
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 216
    :cond_2
    iget-object v0, p0, Lbzg;->a:Lbzd;

    iget-object v1, p0, Lbzg;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lbzd;->a(Landroid/view/View;)V

    goto :goto_0

    .line 210
    :cond_3
    const/16 v0, 0x8

    goto :goto_1

    .line 212
    :cond_4
    const v0, 0x7f020083

    goto :goto_2

    .line 213
    :cond_5
    const v0, 0x7f0902a9

    goto :goto_3
.end method

.method public final a(ZZLjava/lang/CharSequence;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 269
    if-eqz p1, :cond_3

    .line 270
    iget-object v0, p0, Lbzg;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 271
    iget-object v1, p0, Lbzg;->j:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lbzg;->d:Landroid/content/res/Resources;

    if-eqz p2, :cond_0

    const v0, 0x7f090060

    .line 272
    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 271
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 273
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 274
    iget-object v0, p0, Lbzg;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lbzg;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 276
    iget-object v1, p0, Lbzg;->l:Landroid/widget/ImageView;

    if-eqz p2, :cond_1

    const v0, 0x7f02012a

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 287
    :goto_2
    return-void

    .line 271
    :cond_0
    const v0, 0x7f090061

    goto :goto_0

    .line 276
    :cond_1
    const v0, 0x7f020129

    goto :goto_1

    .line 280
    :cond_2
    iget-object v0, p0, Lbzg;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lbzg;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Lbzg;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 285
    :cond_3
    iget-object v0, p0, Lbzg;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lbzg;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lbzg;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lbzg;->a:Lbzd;

    iget-object v1, p0, Lbzg;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lbzd;->a(Landroid/view/View;)V

    .line 148
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 296
    iget-object v0, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    move v0, v1

    .line 297
    :goto_0
    iget-object v2, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 298
    iget-object v2, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 299
    iget v3, p0, Lbzg;->q:I

    if-lt v0, v3, :cond_0

    if-eqz p1, :cond_1

    .line 300
    :cond_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 297
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 302
    :cond_1
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 305
    :cond_2
    if-nez p1, :cond_3

    iget v0, p0, Lbzg;->q:I

    if-lez v0, :cond_5

    .line 306
    :cond_3
    iget-object v0, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 311
    :cond_4
    :goto_2
    iget-object v0, p0, Lbzg;->a:Lbzd;

    iget-object v1, p0, Lbzg;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lbzd;->a(Landroid/view/View;)V

    .line 312
    return-void

    .line 308
    :cond_5
    iget-object v0, p0, Lbzg;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 316
    iget-object v1, p0, Lbzg;->h:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbzg;->h:Landroid/widget/TextView;

    .line 317
    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lbzg;->f:Landroid/widget/TextView;

    invoke-static {v0, p1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 153
    iget-object v0, p0, Lbzg;->a:Lbzd;

    iget-object v1, p0, Lbzg;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lbzd;->a(Landroid/view/View;)V

    .line 154
    return-void
.end method

.method public final d(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lbzg;->m:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lbzg;->b:Landroid/view/View;

    const v1, 0x7f0802a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 160
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzg;->m:Landroid/widget/TextView;

    .line 162
    :cond_0
    iget-object v0, p0, Lbzg;->m:Landroid/widget/TextView;

    invoke-static {v0, p1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lbzg;->a:Lbzd;

    iget-object v1, p0, Lbzg;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lbzd;->a(Landroid/view/View;)V

    .line 164
    return-void
.end method

.method public final e(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lbzg;->n:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lbzg;->b:Landroid/view/View;

    const v1, 0x7f0802a9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 170
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzg;->n:Landroid/widget/TextView;

    .line 172
    :cond_0
    iget-object v0, p0, Lbzg;->n:Landroid/widget/TextView;

    invoke-static {v0, p1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lbzg;->a:Lbzd;

    iget-object v1, p0, Lbzg;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lbzd;->a(Landroid/view/View;)V

    .line 174
    return-void
.end method
