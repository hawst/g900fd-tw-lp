.class public abstract Lejn;
.super Ljava/lang/Object;

# interfaces
.implements Lejq;
.implements Leju;


# instance fields
.field private final a:Ljava/lang/Object;

.field public b:Lejo;

.field private final c:Ljava/util/concurrent/CountDownLatch;

.field private final d:Ljava/util/ArrayList;

.field private e:Lejx;

.field private volatile f:Lejw;

.field private volatile g:Z

.field private h:Z

.field private i:Z

.field private j:Lelg;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lejn;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lejn;->c:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lejn;->d:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lejn;)V
    .locals 2

    iget-object v1, p0, Lejn;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lejn;->d()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/api/Status;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lejn;->a(Lcom/google/android/gms/common/api/Status;)Lejw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lejn;->a(Lejw;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lejn;->i:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Lejw;)V
    .locals 3

    iput-object p1, p0, Lejn;->f:Lejw;

    const/4 v0, 0x0

    iput-object v0, p0, Lejn;->j:Lelg;

    iget-object v0, p0, Lejn;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, Lejn;->f:Lejw;

    invoke-interface {v0}, Lejw;->c_()Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, Lejn;->e:Lejx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejn;->b:Lejo;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lejo;->removeMessages(I)V

    iget-boolean v0, p0, Lejn;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lejn;->b:Lejo;

    iget-object v1, p0, Lejn;->e:Lejx;

    invoke-direct {p0}, Lejn;->e()Lejw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lejo;->a(Lejx;Lejw;)V

    :cond_0
    iget-object v0, p0, Lejn;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lejn;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private d()Z
    .locals 4

    iget-object v0, p0, Lejn;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Lejw;
    .locals 3

    iget-object v1, p0, Lejn;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lejn;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, Lb;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lejn;->d()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, Lb;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lejn;->f:Lejw;

    invoke-virtual {p0}, Lejn;->b()V

    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private f()Z
    .locals 2

    iget-object v1, p0, Lejn;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lejn;->h:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public abstract a(Lcom/google/android/gms/common/api/Status;)Lejw;
.end method

.method public final a()V
    .locals 2

    iget-object v1, p0, Lejn;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lejn;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lejn;->g:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lejn;->f:Lejw;

    invoke-static {v0}, La;->a(Lejw;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lejn;->e:Lejx;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lejn;->h:Z

    sget-object v0, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lejn;->a(Lcom/google/android/gms/common/api/Status;)Lejw;

    move-result-object v0

    invoke-direct {p0, v0}, Lejn;->b(Lejw;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lejw;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lejn;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lejn;->i:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lejn;->h:Z

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {p1}, La;->a(Lejw;)V

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lejn;->d()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, Lb;->a(ZLjava/lang/Object;)V

    iget-boolean v2, p0, Lejn;->g:Z

    if-nez v2, :cond_3

    :goto_2
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Lb;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lejn;->b(Lejw;)V

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final a(Lejx;)V
    .locals 3

    iget-boolean v0, p0, Lejn;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lb;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lejn;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lejn;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lejn;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lejn;->b:Lejo;

    invoke-direct {p0}, Lejn;->e()Lejw;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lejo;->a(Lejx;Lejw;)V

    :goto_2
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iput-object p1, p0, Lejn;->e:Lejx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lejw;

    invoke-virtual {p0, p1}, Lejn;->a(Lejw;)V

    return-void
.end method

.method protected b()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lejn;->g:Z

    iput-object v1, p0, Lejn;->f:Lejw;

    iput-object v1, p0, Lejn;->e:Lejx;

    return-void
.end method
