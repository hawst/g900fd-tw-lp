.class public final Lhfb;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhzh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43323
    invoke-direct {p0}, Lidf;-><init>()V

    .line 43326
    const/4 v0, 0x0

    iput-object v0, p0, Lhfb;->a:Lhzh;

    .line 43323
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 43363
    const/4 v0, 0x0

    .line 43364
    iget-object v1, p0, Lhfb;->a:Lhzh;

    if-eqz v1, :cond_0

    .line 43365
    const v0, 0x474249e

    iget-object v1, p0, Lhfb;->a:Lhzh;

    .line 43366
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 43368
    :cond_0
    iget-object v1, p0, Lhfb;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43369
    iput v0, p0, Lhfb;->J:I

    .line 43370
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 43319
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhfb;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhfb;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhfb;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhfb;->a:Lhzh;

    if-nez v0, :cond_2

    new-instance v0, Lhzh;

    invoke-direct {v0}, Lhzh;-><init>()V

    iput-object v0, p0, Lhfb;->a:Lhzh;

    :cond_2
    iget-object v0, p0, Lhfb;->a:Lhzh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x23a124f2 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 43354
    iget-object v0, p0, Lhfb;->a:Lhzh;

    if-eqz v0, :cond_0

    .line 43355
    const v0, 0x474249e

    iget-object v1, p0, Lhfb;->a:Lhzh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 43357
    :cond_0
    iget-object v0, p0, Lhfb;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 43359
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43337
    if-ne p1, p0, :cond_1

    .line 43341
    :cond_0
    :goto_0
    return v0

    .line 43338
    :cond_1
    instance-of v2, p1, Lhfb;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 43339
    :cond_2
    check-cast p1, Lhfb;

    .line 43340
    iget-object v2, p0, Lhfb;->a:Lhzh;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfb;->a:Lhzh;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhfb;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhfb;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 43341
    goto :goto_0

    .line 43340
    :cond_4
    iget-object v2, p0, Lhfb;->a:Lhzh;

    iget-object v3, p1, Lhfb;->a:Lhzh;

    invoke-virtual {v2, v3}, Lhzh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhfb;->I:Ljava/util/List;

    iget-object v3, p1, Lhfb;->I:Ljava/util/List;

    .line 43341
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 43345
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 43347
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfb;->a:Lhzh;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 43348
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhfb;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 43349
    return v0

    .line 43347
    :cond_0
    iget-object v0, p0, Lhfb;->a:Lhzh;

    invoke-virtual {v0}, Lhzh;->hashCode()I

    move-result v0

    goto :goto_0

    .line 43348
    :cond_1
    iget-object v1, p0, Lhfb;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
