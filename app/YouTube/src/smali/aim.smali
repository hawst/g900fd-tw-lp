.class public final Laim;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbt;


# instance fields
.field public a:Lanp;


# direct methods
.method public constructor <init>(Lanp;)V
    .locals 1

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    const-string v0, "service cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanp;

    iput-object v0, p0, Laim;->a:Lanp;

    .line 272
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 282
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :cond_0
    return-void

    .line 283
    :catch_0
    move-exception v0

    .line 285
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 318
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0, p1}, Lanp;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :cond_0
    return-void

    .line 319
    :catch_0
    move-exception v0

    .line 321
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lgpa;)V
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 450
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0, p1}, Lanp;->a(Lgpa;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 456
    :cond_0
    return-void

    .line 451
    :catch_0
    move-exception v0

    .line 453
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 414
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0, p1}, Lanp;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    :cond_0
    return-void

    .line 415
    :catch_0
    move-exception v0

    .line 417
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 294
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    :cond_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 297
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 354
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0, p1}, Lanp;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :cond_0
    return-void

    .line 355
    :catch_0
    move-exception v0

    .line 357
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 306
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    :cond_0
    return-void

    .line 307
    :catch_0
    move-exception v0

    .line 309
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 330
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    :cond_0
    return-void

    .line 331
    :catch_0
    move-exception v0

    .line 333
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 342
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 348
    :cond_0
    return-void

    .line 343
    :catch_0
    move-exception v0

    .line 345
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 366
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    :cond_0
    return-void

    .line 367
    :catch_0
    move-exception v0

    .line 369
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 378
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 384
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 390
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 396
    :cond_0
    return-void

    .line 391
    :catch_0
    move-exception v0

    .line 393
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 402
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    :cond_0
    return-void

    .line 403
    :catch_0
    move-exception v0

    .line 405
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 426
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->i()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 432
    :cond_0
    return-void

    .line 427
    :catch_0
    move-exception v0

    .line 429
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 438
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 444
    :cond_0
    return-void

    .line 439
    :catch_0
    move-exception v0

    .line 441
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 462
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 466
    iget-object v0, p0, Laim;->a:Lanp;

    if-eqz v0, :cond_0

    .line 468
    :try_start_0
    iget-object v0, p0, Laim;->a:Lanp;

    invoke-interface {v0}, Lanp;->k()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    :cond_0
    return-void

    .line 469
    :catch_0
    move-exception v0

    .line 471
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
