.class final Lbrs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private synthetic a:Lbro;


# direct methods
.method constructor <init>(Lbro;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lbrs;->a:Lbro;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 182
    const/4 v2, 0x4

    if-eq p2, v2, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    .line 183
    :cond_0
    iget-object v2, p0, Lbrs;->a:Lbro;

    iget-object v3, v2, Lbro;->f:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, v2, Lbro;->a:Lfbh;

    iget-object v5, v2, Lbro;->g:Ljava/lang/String;

    new-array v6, v1, [Ljava/lang/String;

    aput-object v5, v6, v0

    iget-object v0, v4, Lfbh;->k:Lfcz;

    new-instance v5, Lfbm;

    invoke-direct {v5, v4}, Lfbm;-><init>(Lfbh;)V

    new-instance v4, Lftl;

    iget-object v7, v0, Lfcz;->b:Lfsz;

    iget-object v8, v0, Lfcz;->c:Lgix;

    invoke-interface {v8}, Lgix;->d()Lgit;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lftl;-><init>(Lfsz;Lgit;)V

    iput-object v6, v4, Lftl;->a:[Ljava/lang/String;

    iput-object v9, v4, Lftl;->b:[Ljava/lang/String;

    invoke-virtual {v4, v9}, Lftl;->a(Ljava/lang/String;)Lftl;

    move-result-object v6

    invoke-virtual {v6, v3}, Lftl;->b(Ljava/lang/String;)Lftl;

    if-eqz v9, :cond_1

    iput-object v9, v4, Lftl;->c:Lfnn;

    :cond_1
    new-instance v3, Lfdc;

    invoke-direct {v3, v0}, Lfdc;-><init>(Lfcz;)V

    invoke-virtual {v3, v4, v5}, Lfdc;->b(Lfsp;Lwv;)V

    iget-object v0, v2, Lbro;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    iget-object v0, v2, Lbro;->f:Landroid/widget/EditText;

    invoke-static {v0}, Leze;->a(Landroid/view/View;)V

    :cond_2
    move v0, v1

    .line 186
    :cond_3
    return v0
.end method
