.class public final Lbra;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "http://m.youtube.com/merge_identity"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbra;->a:Landroid/net/Uri;

    .line 53
    const-string v0, "http://m.youtube.com/create_channel"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbra;->b:Landroid/net/Uri;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcuo;Lgix;Lgca;Lfxe;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const v3, 0x7f0902b8

    .line 73
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-interface {p2}, Lgix;->b()Z

    move-result v0

    invoke-static {v0}, Lb;->b(Z)V

    .line 75
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-boolean v0, p3, Lgca;->f:Z

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Leyv;

    invoke-direct {v0, p0}, Leyv;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090116

    invoke-virtual {v0, v1}, Leyv;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090117

    new-instance v2, Lbre;

    invoke-direct {v2, p4, p0}, Lbre;-><init>(Lfxe;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lbrd;

    invoke-direct {v1}, Lbrd;-><init>()V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    .line 79
    :cond_0
    iget-object v0, p3, Lgca;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    new-instance v0, Leyv;

    invoke-direct {v0, p0}, Leyv;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0902b6

    invoke-virtual {v0, v1}, Leyv;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0902b7

    new-instance v2, Lbrc;

    invoke-direct {v2, p4, p0, p1}, Lbrc;-><init>(Lfxe;Landroid/app/Activity;Lcuo;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lbrb;

    invoke-direct {v1}, Lbrb;-><init>()V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lbra;->a:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lbra;->b:Landroid/net/Uri;

    return-object v0
.end method
