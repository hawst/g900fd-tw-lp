.class public final Lgru;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgsa;


# instance fields
.field final a:Lgrz;

.field private final b:Lgrv;

.field private c:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lgrz;

    invoke-direct {v0}, Lgrz;-><init>()V

    iput-object v0, p0, Lgru;->a:Lgrz;

    .line 23
    new-instance v0, Lgrv;

    invoke-direct {v0, p0}, Lgrv;-><init>(Lgru;)V

    iput-object v0, p0, Lgru;->b:Lgrv;

    .line 151
    return-void
.end method


# virtual methods
.method public final a(JLandroid/graphics/Bitmap;)Lgrs;
    .locals 5

    .prologue
    .line 37
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    const/4 v0, 0x0

    .line 41
    iget-object v3, p0, Lgru;->a:Lgrz;

    monitor-enter v3

    .line 42
    :try_start_0
    iget-object v1, p0, Lgru;->a:Lgrz;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, p2, v2}, Lgrz;->a(JZ)Lgrs;

    move-result-object v1

    .line 44
    if-nez v1, :cond_1

    .line 45
    new-instance v1, Lgrs;

    iget-object v2, p0, Lgru;->b:Lgrv;

    invoke-direct {v1, v2, p1, p2, p3}, Lgrs;-><init>(Lgrt;JLandroid/graphics/Bitmap;)V

    .line 46
    iget-object v2, p0, Lgru;->a:Lgrz;

    invoke-virtual {v2, v1}, Lgrz;->a(Lgrs;)Lgrs;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 47
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "An existing thumbnail was already stored"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 49
    :cond_0
    :try_start_1
    iget-object v2, p0, Lgru;->c:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 50
    iget-object v0, p0, Lgru;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    .line 56
    :goto_0
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    if-eqz v1, :cond_2

    .line 59
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    invoke-interface {v0, p0, v2}, Lgsb;->a(Lgsa;Lgrs;)V

    goto :goto_1

    .line 53
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Lgrs;->b()Lgrs;

    .line 54
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 64
    :cond_2
    return-object v2

    :cond_3
    move-object v2, v1

    move-object v1, v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 141
    iget-object v1, p0, Lgru;->a:Lgrz;

    monitor-enter v1

    .line 142
    :try_start_0
    iget-object v0, p0, Lgru;->a:Lgrz;

    iget-object v0, v0, Lgrz;->a:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 143
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    iget-object v0, p0, Lgru;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lgru;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 147
    :cond_0
    return-void

    .line 143
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lgsb;)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lgru;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lgru;->c:Ljava/util/List;

    .line 129
    :cond_0
    iget-object v0, p0, Lgru;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    return-void
.end method

.method public final a(JJ)Z
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 78
    iget-object v2, p0, Lgru;->a:Lgrz;

    monitor-enter v2

    .line 79
    const/4 v1, 0x0

    .line 80
    :try_start_0
    iget-object v0, p0, Lgru;->a:Lgrz;

    invoke-virtual {v0}, Lgrz;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrs;

    .line 81
    invoke-virtual {v0}, Lgrs;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_1

    .line 83
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 85
    goto :goto_0

    .line 86
    :cond_0
    monitor-exit v2

    return v1

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final b(JZ)Lgrs;
    .locals 3

    .prologue
    .line 99
    iget-object v1, p0, Lgru;->a:Lgrz;

    monitor-enter v1

    .line 100
    :try_start_0
    iget-object v0, p0, Lgru;->a:Lgrz;

    invoke-virtual {v0, p1, p2, p3}, Lgrz;->a(JZ)Lgrs;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Lgrs;->b()Lgrs;

    .line 104
    :cond_0
    monitor-exit v1

    return-object v0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lgsb;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lgru;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lgru;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 137
    :cond_0
    return-void
.end method

.method public final c(J)Lgrs;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lgru;->b(JZ)Lgrs;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method
