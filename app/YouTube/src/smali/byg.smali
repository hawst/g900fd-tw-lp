.class public final Lbyg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/app/Activity;

.field final b:Lfhz;

.field final c:Lfdw;

.field d:Landroid/content/res/Resources;

.field private e:Lbyi;

.field private f:Lbyh;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lfhz;Lfdw;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbyg;->a:Landroid/app/Activity;

    .line 49
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lbyg;->b:Lfhz;

    .line 50
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lbyg;->c:Lfdw;

    .line 51
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbyg;->d:Landroid/content/res/Resources;

    .line 52
    return-void
.end method

.method private handleSignOutEvent(Lfcc;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lbyg;->e:Lbyi;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lbyg;->e:Lbyi;

    iget-object v1, v0, Lbyi;->a:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lbyi;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 257
    :cond_0
    iget-object v0, p0, Lbyg;->f:Lbyh;

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lbyg;->f:Lbyh;

    iget-object v1, v0, Lbyh;->a:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lbyh;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 260
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lfki;Lfrz;)V
    .locals 9

    .prologue
    const v8, 0x7f0a00df

    const/4 v7, -0x2

    const/4 v2, 0x0

    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 57
    if-nez p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    instance-of v0, p1, Lfjt;

    if-eqz v0, :cond_5

    .line 60
    iget-object v0, p0, Lbyg;->f:Lbyh;

    if-nez v0, :cond_2

    .line 61
    new-instance v0, Lbyh;

    invoke-direct {v0, p0}, Lbyh;-><init>(Lbyg;)V

    iput-object v0, p0, Lbyg;->f:Lbyh;

    .line 63
    :cond_2
    iget-object v3, p0, Lbyg;->f:Lbyh;

    check-cast p1, Lfjt;

    iget-object v0, v3, Lbyh;->c:Landroid/widget/TextView;

    iget-object v4, p1, Lfjt;->a:Lhfz;

    iget-object v4, v4, Lhfz;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lfjt;->a:Lhfz;

    iget-object v0, v0, Lhfz;->d:Lhiq;

    if-eqz v0, :cond_e

    iget-object v0, p1, Lfjt;->a:Lhfz;

    iget-object v0, v0, Lhfz;->d:Lhiq;

    iget v0, v0, Lhiq;->a:I

    invoke-static {v0}, Lbrf;->a(I)I

    move-result v0

    :goto_1
    iget-object v4, p1, Lfjt;->a:Lhfz;

    iget-object v4, v4, Lhfz;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    if-nez v0, :cond_3

    iget-object v0, v3, Lbyh;->e:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v3, Lbyh;->f:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    iget-object v0, v3, Lbyh;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iget-object v0, v3, Lbyh;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v1, v3, Lbyh;->g:Lbyg;

    iget-object v1, v1, Lbyg;->d:Landroid/content/res/Resources;

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1, v7}, Landroid/view/Window;->setLayout(II)V

    if-eqz p2, :cond_0

    iget-object v0, v3, Lbyh;->g:Lbyg;

    iget-object v0, v0, Lbyg;->c:Lfdw;

    invoke-interface {p2}, Lfrz;->B()Lfqg;

    move-result-object v1

    invoke-virtual {v0, v1, p1, v2}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    goto :goto_0

    :cond_3
    iget-object v4, v3, Lbyh;->e:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, v3, Lbyh;->f:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, v3, Lbyh;->b:Landroid/widget/TextView;

    iget-object v5, p1, Lfjt;->a:Lhfz;

    iget-object v5, v5, Lhfz;->c:Ljava/lang/String;

    invoke-static {v4, v5}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-nez v0, :cond_4

    iget-object v0, v3, Lbyh;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    iget-object v4, v3, Lbyh;->d:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, v3, Lbyh;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 64
    :cond_5
    instance-of v0, p1, Lfla;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lbyg;->e:Lbyi;

    if-nez v0, :cond_6

    .line 66
    new-instance v0, Lbyi;

    invoke-direct {v0, p0}, Lbyi;-><init>(Lbyg;)V

    iput-object v0, p0, Lbyg;->e:Lbyi;

    .line 68
    :cond_6
    iget-object v3, p0, Lbyg;->e:Lbyi;

    check-cast p1, Lfla;

    iget-object v0, v3, Lbyi;->c:Landroid/widget/TextView;

    iget-object v4, p1, Lfla;->a:Lhod;

    iget-object v4, v4, Lhod;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lfla;->a:Lhod;

    iget-object v0, v0, Lhod;->e:Lhiq;

    if-eqz v0, :cond_d

    iget-object v0, p1, Lfla;->a:Lhod;

    iget-object v0, v0, Lhod;->e:Lhiq;

    iget v0, v0, Lhiq;->a:I

    invoke-static {v0}, Lbrf;->a(I)I

    move-result v0

    :goto_3
    iget-object v4, p1, Lfla;->a:Lhod;

    iget-object v4, v4, Lhod;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    if-nez v0, :cond_7

    iget-object v0, v3, Lbyi;->g:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v3, Lbyi;->h:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    iget-object v0, p1, Lfla;->a:Lhod;

    iget-object v0, v0, Lhod;->g:Lhog;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lfla;->a:Lhod;

    iget-object v0, v0, Lhod;->g:Lhog;

    iput-object v0, v3, Lbyi;->i:Lhog;

    iget-object v0, p1, Lfla;->a:Lhod;

    iget-object v0, v0, Lhod;->f:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5
    iget-object v4, v3, Lbyi;->i:Lhog;

    if-eqz v4, :cond_b

    if-eqz v0, :cond_b

    iget-object v4, v3, Lbyi;->d:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v3, Lbyi;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, v3, Lbyi;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_6
    iget-object v0, v3, Lbyi;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iget-object v0, v3, Lbyi;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v1, v3, Lbyi;->j:Lbyg;

    iget-object v1, v1, Lbyg;->d:Landroid/content/res/Resources;

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1, v7}, Landroid/view/Window;->setLayout(II)V

    if-eqz p2, :cond_0

    iget-object v0, v3, Lbyi;->j:Lbyg;

    iget-object v0, v0, Lbyg;->c:Lfdw;

    invoke-interface {p2}, Lfrz;->B()Lfqg;

    move-result-object v1

    invoke-virtual {v0, v1, p1, v2}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    goto/16 :goto_0

    :cond_7
    iget-object v4, v3, Lbyi;->g:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, v3, Lbyi;->h:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p1, Lfla;->a:Lhod;

    iget-object v4, v4, Lhod;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, v3, Lbyi;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_7
    if-nez v0, :cond_9

    iget-object v0, v3, Lbyi;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    :cond_8
    iget-object v4, v3, Lbyi;->b:Landroid/widget/TextView;

    iget-object v5, p1, Lfla;->a:Lhod;

    iget-object v5, v5, Lhod;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v3, Lbyi;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7

    :cond_9
    iget-object v4, v3, Lbyi;->e:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, v3, Lbyi;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_a
    iget-object v0, p1, Lfla;->a:Lhod;

    iget-object v0, v0, Lhod;->b:Lhog;

    if-eqz v0, :cond_c

    iget-object v0, p1, Lfla;->a:Lhod;

    iget-object v0, v0, Lhod;->g:Lhog;

    iput-object v0, v3, Lbyi;->i:Lhog;

    iget-object v0, v3, Lbyi;->j:Lbyg;

    iget-object v0, v0, Lbyg;->d:Landroid/content/res/Resources;

    const v4, 0x7f0901d6

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_b
    iget-object v0, v3, Lbyi;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, v3, Lbyi;->f:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    :cond_c
    move-object v0, v2

    goto/16 :goto_5

    :cond_d
    move v0, v1

    goto/16 :goto_3

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method
