.class public final Lfzn;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.uploads"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.favorites"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.subscriptions"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.watchhistory"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.watchlater"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.playlists"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.recentactivity"

    aput-object v3, v1, v2

    .line 45
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lfzn;->a:Ljava/util/Set;

    .line 44
    return-void
.end method

.method static synthetic a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lfzn;->a:Ljava/util/Set;

    return-object v0
.end method

.method public static a(Lfbb;)V
    .locals 1

    .prologue
    .line 55
    invoke-static {p0}, La;->d(Lfbb;)V

    .line 56
    const-string v0, ""

    invoke-static {p0, v0}, Lfzn;->b(Lfbb;Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public static a(Lfbb;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0, p1}, La;->f(Lfbb;Ljava/lang/String;)V

    .line 51
    invoke-static {p0, p1}, Lfzn;->b(Lfbb;Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method private static b(Lfbb;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 60
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry/yt:username"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfzq;

    invoke-direct {v1}, Lfzq;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/yt:channelId"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzp;

    invoke-direct {v2}, Lfzp;-><init>()V

    .line 67
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/yt:googlePlusUserId"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzy;

    invoke-direct {v2}, Lfzy;-><init>()V

    .line 74
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/author/email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzx;

    invoke-direct {v2}, Lfzx;-><init>()V

    .line 81
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/yt:age"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzw;

    invoke-direct {v2}, Lfzw;-><init>()V

    .line 88
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/yt:gender"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzv;

    invoke-direct {v2}, Lfzv;-><init>()V

    .line 95
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/media:thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzu;

    invoke-direct {v2}, Lfzu;-><init>()V

    .line 108
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/yt:incomplete"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzt;

    invoke-direct {v2}, Lfzt;-><init>()V

    .line 115
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/yt:eligibleForChannel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzs;

    invoke-direct {v2}, Lfzs;-><init>()V

    .line 122
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/yt:statistics"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzr;

    invoke-direct {v2}, Lfzr;-><init>()V

    .line 129
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/gd:feedLink"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzo;

    invoke-direct {v2}, Lfzo;-><init>()V

    .line 138
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lfbb;->a()Lfba;

    .line 169
    return-void
.end method
