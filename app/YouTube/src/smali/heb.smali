.class public final Lheb;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lhxf;

.field public c:Lhgz;

.field public d:Lhgz;

.field public e:Lhgz;

.field public f:Lhgz;

.field public g:Lhog;

.field public h:Lhgz;

.field public i:Lhec;

.field public j:[B

.field public k:[Lhxe;

.field public l:[Lhut;

.field public m:Lhgz;

.field public n:Lhnh;

.field public o:Lhea;

.field private p:Lhgz;

.field private q:Ljava/lang/String;

.field private r:Lhxx;

.field private s:[Lhbi;

.field private t:Lhxf;

.field private u:[Lhbi;

.field private v:Z

.field private w:Lhog;

.field private x:Z

.field private y:Lhkm;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36072
    invoke-direct {p0}, Lidf;-><init>()V

    .line 36162
    const-string v0, ""

    iput-object v0, p0, Lheb;->a:Ljava/lang/String;

    .line 36165
    iput-object v1, p0, Lheb;->b:Lhxf;

    .line 36168
    iput-object v1, p0, Lheb;->c:Lhgz;

    .line 36171
    iput-object v1, p0, Lheb;->p:Lhgz;

    .line 36174
    iput-object v1, p0, Lheb;->d:Lhgz;

    .line 36177
    iput-object v1, p0, Lheb;->e:Lhgz;

    .line 36180
    iput-object v1, p0, Lheb;->f:Lhgz;

    .line 36183
    iput-object v1, p0, Lheb;->g:Lhog;

    .line 36186
    const-string v0, ""

    iput-object v0, p0, Lheb;->q:Ljava/lang/String;

    .line 36189
    iput-object v1, p0, Lheb;->h:Lhgz;

    .line 36192
    iput-object v1, p0, Lheb;->r:Lhxx;

    .line 36195
    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lheb;->s:[Lhbi;

    .line 36198
    iput-object v1, p0, Lheb;->t:Lhxf;

    .line 36201
    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lheb;->u:[Lhbi;

    .line 36204
    iput-object v1, p0, Lheb;->i:Lhec;

    .line 36207
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lheb;->j:[B

    .line 36210
    sget-object v0, Lhxe;->a:[Lhxe;

    iput-object v0, p0, Lheb;->k:[Lhxe;

    .line 36213
    iput-boolean v2, p0, Lheb;->v:Z

    .line 36216
    sget-object v0, Lhut;->a:[Lhut;

    iput-object v0, p0, Lheb;->l:[Lhut;

    .line 36219
    iput-object v1, p0, Lheb;->m:Lhgz;

    .line 36222
    iput-object v1, p0, Lheb;->w:Lhog;

    .line 36225
    iput-object v1, p0, Lheb;->n:Lhnh;

    .line 36228
    iput-object v1, p0, Lheb;->o:Lhea;

    .line 36231
    iput-boolean v2, p0, Lheb;->x:Z

    .line 36234
    iput-object v1, p0, Lheb;->y:Lhkm;

    .line 36072
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 36456
    .line 36457
    iget-object v0, p0, Lheb;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 36458
    const/4 v0, 0x1

    iget-object v2, p0, Lheb;->a:Ljava/lang/String;

    .line 36459
    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 36461
    :goto_0
    iget-object v2, p0, Lheb;->b:Lhxf;

    if-eqz v2, :cond_0

    .line 36462
    const/4 v2, 0x2

    iget-object v3, p0, Lheb;->b:Lhxf;

    .line 36463
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36465
    :cond_0
    iget-object v2, p0, Lheb;->c:Lhgz;

    if-eqz v2, :cond_1

    .line 36466
    const/4 v2, 0x3

    iget-object v3, p0, Lheb;->c:Lhgz;

    .line 36467
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36469
    :cond_1
    iget-object v2, p0, Lheb;->p:Lhgz;

    if-eqz v2, :cond_2

    .line 36470
    const/4 v2, 0x4

    iget-object v3, p0, Lheb;->p:Lhgz;

    .line 36471
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36473
    :cond_2
    iget-object v2, p0, Lheb;->d:Lhgz;

    if-eqz v2, :cond_3

    .line 36474
    const/4 v2, 0x5

    iget-object v3, p0, Lheb;->d:Lhgz;

    .line 36475
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36477
    :cond_3
    iget-object v2, p0, Lheb;->e:Lhgz;

    if-eqz v2, :cond_4

    .line 36478
    const/4 v2, 0x6

    iget-object v3, p0, Lheb;->e:Lhgz;

    .line 36479
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36481
    :cond_4
    iget-object v2, p0, Lheb;->f:Lhgz;

    if-eqz v2, :cond_5

    .line 36482
    const/4 v2, 0x7

    iget-object v3, p0, Lheb;->f:Lhgz;

    .line 36483
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36485
    :cond_5
    iget-object v2, p0, Lheb;->g:Lhog;

    if-eqz v2, :cond_6

    .line 36486
    const/16 v2, 0x8

    iget-object v3, p0, Lheb;->g:Lhog;

    .line 36487
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36489
    :cond_6
    iget-object v2, p0, Lheb;->q:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 36490
    const/16 v2, 0x9

    iget-object v3, p0, Lheb;->q:Ljava/lang/String;

    .line 36491
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36493
    :cond_7
    iget-object v2, p0, Lheb;->h:Lhgz;

    if-eqz v2, :cond_8

    .line 36494
    const/16 v2, 0xa

    iget-object v3, p0, Lheb;->h:Lhgz;

    .line 36495
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36497
    :cond_8
    iget-object v2, p0, Lheb;->r:Lhxx;

    if-eqz v2, :cond_9

    .line 36498
    const/16 v2, 0xc

    iget-object v3, p0, Lheb;->r:Lhxx;

    .line 36499
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36501
    :cond_9
    iget-object v2, p0, Lheb;->s:[Lhbi;

    if-eqz v2, :cond_b

    .line 36502
    iget-object v3, p0, Lheb;->s:[Lhbi;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_b

    aget-object v5, v3, v2

    .line 36503
    if-eqz v5, :cond_a

    .line 36504
    const/16 v6, 0xd

    .line 36505
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 36502
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 36509
    :cond_b
    iget-object v2, p0, Lheb;->t:Lhxf;

    if-eqz v2, :cond_c

    .line 36510
    const/16 v2, 0xe

    iget-object v3, p0, Lheb;->t:Lhxf;

    .line 36511
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36513
    :cond_c
    iget-object v2, p0, Lheb;->u:[Lhbi;

    if-eqz v2, :cond_e

    .line 36514
    iget-object v3, p0, Lheb;->u:[Lhbi;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_e

    aget-object v5, v3, v2

    .line 36515
    if-eqz v5, :cond_d

    .line 36516
    const/16 v6, 0xf

    .line 36517
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 36514
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 36521
    :cond_e
    iget-object v2, p0, Lheb;->i:Lhec;

    if-eqz v2, :cond_f

    .line 36522
    const/16 v2, 0x11

    iget-object v3, p0, Lheb;->i:Lhec;

    .line 36523
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 36525
    :cond_f
    iget-object v2, p0, Lheb;->j:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_10

    .line 36526
    const/16 v2, 0x12

    iget-object v3, p0, Lheb;->j:[B

    .line 36527
    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 36529
    :cond_10
    iget-object v2, p0, Lheb;->k:[Lhxe;

    if-eqz v2, :cond_12

    .line 36530
    iget-object v3, p0, Lheb;->k:[Lhxe;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_12

    aget-object v5, v3, v2

    .line 36531
    if-eqz v5, :cond_11

    .line 36532
    const/16 v6, 0x14

    .line 36533
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 36530
    :cond_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 36537
    :cond_12
    iget-boolean v2, p0, Lheb;->v:Z

    if-eqz v2, :cond_13

    .line 36538
    const/16 v2, 0x15

    iget-boolean v3, p0, Lheb;->v:Z

    .line 36539
    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 36541
    :cond_13
    iget-object v2, p0, Lheb;->l:[Lhut;

    if-eqz v2, :cond_15

    .line 36542
    iget-object v2, p0, Lheb;->l:[Lhut;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_15

    aget-object v4, v2, v1

    .line 36543
    if-eqz v4, :cond_14

    .line 36544
    const/16 v5, 0x16

    .line 36545
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 36542
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 36549
    :cond_15
    iget-object v1, p0, Lheb;->m:Lhgz;

    if-eqz v1, :cond_16

    .line 36550
    const/16 v1, 0x17

    iget-object v2, p0, Lheb;->m:Lhgz;

    .line 36551
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36553
    :cond_16
    iget-object v1, p0, Lheb;->w:Lhog;

    if-eqz v1, :cond_17

    .line 36554
    const/16 v1, 0x18

    iget-object v2, p0, Lheb;->w:Lhog;

    .line 36555
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36557
    :cond_17
    iget-object v1, p0, Lheb;->n:Lhnh;

    if-eqz v1, :cond_18

    .line 36558
    const/16 v1, 0x19

    iget-object v2, p0, Lheb;->n:Lhnh;

    .line 36559
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36561
    :cond_18
    iget-object v1, p0, Lheb;->o:Lhea;

    if-eqz v1, :cond_19

    .line 36562
    const/16 v1, 0x1a

    iget-object v2, p0, Lheb;->o:Lhea;

    .line 36563
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36565
    :cond_19
    iget-boolean v1, p0, Lheb;->x:Z

    if-eqz v1, :cond_1a

    .line 36566
    const/16 v1, 0x1b

    iget-boolean v2, p0, Lheb;->x:Z

    .line 36567
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 36569
    :cond_1a
    iget-object v1, p0, Lheb;->y:Lhkm;

    if-eqz v1, :cond_1b

    .line 36570
    const v1, 0x39d6d44

    iget-object v2, p0, Lheb;->y:Lhkm;

    .line 36571
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36573
    :cond_1b
    iget-object v1, p0, Lheb;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36574
    iput v0, p0, Lheb;->J:I

    .line 36575
    return v0

    :cond_1c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 36068
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lheb;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lheb;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lheb;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lheb;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lheb;->b:Lhxf;

    if-nez v0, :cond_2

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lheb;->b:Lhxf;

    :cond_2
    iget-object v0, p0, Lheb;->b:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lheb;->c:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lheb;->c:Lhgz;

    :cond_3
    iget-object v0, p0, Lheb;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lheb;->p:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lheb;->p:Lhgz;

    :cond_4
    iget-object v0, p0, Lheb;->p:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lheb;->d:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lheb;->d:Lhgz;

    :cond_5
    iget-object v0, p0, Lheb;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lheb;->e:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lheb;->e:Lhgz;

    :cond_6
    iget-object v0, p0, Lheb;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lheb;->f:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lheb;->f:Lhgz;

    :cond_7
    iget-object v0, p0, Lheb;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lheb;->g:Lhog;

    if-nez v0, :cond_8

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lheb;->g:Lhog;

    :cond_8
    iget-object v0, p0, Lheb;->g:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lheb;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lheb;->h:Lhgz;

    if-nez v0, :cond_9

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lheb;->h:Lhgz;

    :cond_9
    iget-object v0, p0, Lheb;->h:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lheb;->r:Lhxx;

    if-nez v0, :cond_a

    new-instance v0, Lhxx;

    invoke-direct {v0}, Lhxx;-><init>()V

    iput-object v0, p0, Lheb;->r:Lhxx;

    :cond_a
    iget-object v0, p0, Lheb;->r:Lhxx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lheb;->s:[Lhbi;

    if-nez v0, :cond_c

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lheb;->s:[Lhbi;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lheb;->s:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Lheb;->s:[Lhbi;

    :goto_2
    iget-object v2, p0, Lheb;->s:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Lheb;->s:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lheb;->s:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_c
    iget-object v0, p0, Lheb;->s:[Lhbi;

    array-length v0, v0

    goto :goto_1

    :cond_d
    iget-object v2, p0, Lheb;->s:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lheb;->s:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lheb;->t:Lhxf;

    if-nez v0, :cond_e

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lheb;->t:Lhxf;

    :cond_e
    iget-object v0, p0, Lheb;->t:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lheb;->u:[Lhbi;

    if-nez v0, :cond_10

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lheb;->u:[Lhbi;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lheb;->u:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    iput-object v2, p0, Lheb;->u:[Lhbi;

    :goto_4
    iget-object v2, p0, Lheb;->u:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    iget-object v2, p0, Lheb;->u:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lheb;->u:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_10
    iget-object v0, p0, Lheb;->u:[Lhbi;

    array-length v0, v0

    goto :goto_3

    :cond_11
    iget-object v2, p0, Lheb;->u:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lheb;->u:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lheb;->i:Lhec;

    if-nez v0, :cond_12

    new-instance v0, Lhec;

    invoke-direct {v0}, Lhec;-><init>()V

    iput-object v0, p0, Lheb;->i:Lhec;

    :cond_12
    iget-object v0, p0, Lheb;->i:Lhec;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lheb;->j:[B

    goto/16 :goto_0

    :sswitch_11
    const/16 v0, 0xa2

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lheb;->k:[Lhxe;

    if-nez v0, :cond_14

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhxe;

    iget-object v3, p0, Lheb;->k:[Lhxe;

    if-eqz v3, :cond_13

    iget-object v3, p0, Lheb;->k:[Lhxe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_13
    iput-object v2, p0, Lheb;->k:[Lhxe;

    :goto_6
    iget-object v2, p0, Lheb;->k:[Lhxe;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_15

    iget-object v2, p0, Lheb;->k:[Lhxe;

    new-instance v3, Lhxe;

    invoke-direct {v3}, Lhxe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lheb;->k:[Lhxe;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_14
    iget-object v0, p0, Lheb;->k:[Lhxe;

    array-length v0, v0

    goto :goto_5

    :cond_15
    iget-object v2, p0, Lheb;->k:[Lhxe;

    new-instance v3, Lhxe;

    invoke-direct {v3}, Lhxe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lheb;->k:[Lhxe;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lheb;->v:Z

    goto/16 :goto_0

    :sswitch_13
    const/16 v0, 0xb2

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lheb;->l:[Lhut;

    if-nez v0, :cond_17

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lhut;

    iget-object v3, p0, Lheb;->l:[Lhut;

    if-eqz v3, :cond_16

    iget-object v3, p0, Lheb;->l:[Lhut;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_16
    iput-object v2, p0, Lheb;->l:[Lhut;

    :goto_8
    iget-object v2, p0, Lheb;->l:[Lhut;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_18

    iget-object v2, p0, Lheb;->l:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lheb;->l:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_17
    iget-object v0, p0, Lheb;->l:[Lhut;

    array-length v0, v0

    goto :goto_7

    :cond_18
    iget-object v2, p0, Lheb;->l:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lheb;->l:[Lhut;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lheb;->m:Lhgz;

    if-nez v0, :cond_19

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lheb;->m:Lhgz;

    :cond_19
    iget-object v0, p0, Lheb;->m:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_15
    iget-object v0, p0, Lheb;->w:Lhog;

    if-nez v0, :cond_1a

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lheb;->w:Lhog;

    :cond_1a
    iget-object v0, p0, Lheb;->w:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Lheb;->n:Lhnh;

    if-nez v0, :cond_1b

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Lheb;->n:Lhnh;

    :cond_1b
    iget-object v0, p0, Lheb;->n:Lhnh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Lheb;->o:Lhea;

    if-nez v0, :cond_1c

    new-instance v0, Lhea;

    invoke-direct {v0}, Lhea;-><init>()V

    iput-object v0, p0, Lheb;->o:Lhea;

    :cond_1c
    iget-object v0, p0, Lheb;->o:Lhea;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lheb;->x:Z

    goto/16 :goto_0

    :sswitch_19
    iget-object v0, p0, Lheb;->y:Lhkm;

    if-nez v0, :cond_1d

    new-instance v0, Lhkm;

    invoke-direct {v0}, Lhkm;-><init>()V

    iput-object v0, p0, Lheb;->y:Lhkm;

    :cond_1d
    iget-object v0, p0, Lheb;->y:Lhkm;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x8a -> :sswitch_f
        0x92 -> :sswitch_10
        0xa2 -> :sswitch_11
        0xa8 -> :sswitch_12
        0xb2 -> :sswitch_13
        0xba -> :sswitch_14
        0xc2 -> :sswitch_15
        0xca -> :sswitch_16
        0xd2 -> :sswitch_17
        0xd8 -> :sswitch_18
        0x1ceb6a22 -> :sswitch_19
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 36359
    iget-object v1, p0, Lheb;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 36360
    const/4 v1, 0x1

    iget-object v2, p0, Lheb;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    .line 36362
    :cond_0
    iget-object v1, p0, Lheb;->b:Lhxf;

    if-eqz v1, :cond_1

    .line 36363
    const/4 v1, 0x2

    iget-object v2, p0, Lheb;->b:Lhxf;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36365
    :cond_1
    iget-object v1, p0, Lheb;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 36366
    const/4 v1, 0x3

    iget-object v2, p0, Lheb;->c:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36368
    :cond_2
    iget-object v1, p0, Lheb;->p:Lhgz;

    if-eqz v1, :cond_3

    .line 36369
    const/4 v1, 0x4

    iget-object v2, p0, Lheb;->p:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36371
    :cond_3
    iget-object v1, p0, Lheb;->d:Lhgz;

    if-eqz v1, :cond_4

    .line 36372
    const/4 v1, 0x5

    iget-object v2, p0, Lheb;->d:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36374
    :cond_4
    iget-object v1, p0, Lheb;->e:Lhgz;

    if-eqz v1, :cond_5

    .line 36375
    const/4 v1, 0x6

    iget-object v2, p0, Lheb;->e:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36377
    :cond_5
    iget-object v1, p0, Lheb;->f:Lhgz;

    if-eqz v1, :cond_6

    .line 36378
    const/4 v1, 0x7

    iget-object v2, p0, Lheb;->f:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36380
    :cond_6
    iget-object v1, p0, Lheb;->g:Lhog;

    if-eqz v1, :cond_7

    .line 36381
    const/16 v1, 0x8

    iget-object v2, p0, Lheb;->g:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36383
    :cond_7
    iget-object v1, p0, Lheb;->q:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 36384
    const/16 v1, 0x9

    iget-object v2, p0, Lheb;->q:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    .line 36386
    :cond_8
    iget-object v1, p0, Lheb;->h:Lhgz;

    if-eqz v1, :cond_9

    .line 36387
    const/16 v1, 0xa

    iget-object v2, p0, Lheb;->h:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36389
    :cond_9
    iget-object v1, p0, Lheb;->r:Lhxx;

    if-eqz v1, :cond_a

    .line 36390
    const/16 v1, 0xc

    iget-object v2, p0, Lheb;->r:Lhxx;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36392
    :cond_a
    iget-object v1, p0, Lheb;->s:[Lhbi;

    if-eqz v1, :cond_c

    .line 36393
    iget-object v2, p0, Lheb;->s:[Lhbi;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 36394
    if-eqz v4, :cond_b

    .line 36395
    const/16 v5, 0xd

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 36393
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 36399
    :cond_c
    iget-object v1, p0, Lheb;->t:Lhxf;

    if-eqz v1, :cond_d

    .line 36400
    const/16 v1, 0xe

    iget-object v2, p0, Lheb;->t:Lhxf;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36402
    :cond_d
    iget-object v1, p0, Lheb;->u:[Lhbi;

    if-eqz v1, :cond_f

    .line 36403
    iget-object v2, p0, Lheb;->u:[Lhbi;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_f

    aget-object v4, v2, v1

    .line 36404
    if-eqz v4, :cond_e

    .line 36405
    const/16 v5, 0xf

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 36403
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 36409
    :cond_f
    iget-object v1, p0, Lheb;->i:Lhec;

    if-eqz v1, :cond_10

    .line 36410
    const/16 v1, 0x11

    iget-object v2, p0, Lheb;->i:Lhec;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 36412
    :cond_10
    iget-object v1, p0, Lheb;->j:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_11

    .line 36413
    const/16 v1, 0x12

    iget-object v2, p0, Lheb;->j:[B

    invoke-virtual {p1, v1, v2}, Lidd;->a(I[B)V

    .line 36415
    :cond_11
    iget-object v1, p0, Lheb;->k:[Lhxe;

    if-eqz v1, :cond_13

    .line 36416
    iget-object v2, p0, Lheb;->k:[Lhxe;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_13

    aget-object v4, v2, v1

    .line 36417
    if-eqz v4, :cond_12

    .line 36418
    const/16 v5, 0x14

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 36416
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 36422
    :cond_13
    iget-boolean v1, p0, Lheb;->v:Z

    if-eqz v1, :cond_14

    .line 36423
    const/16 v1, 0x15

    iget-boolean v2, p0, Lheb;->v:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    .line 36425
    :cond_14
    iget-object v1, p0, Lheb;->l:[Lhut;

    if-eqz v1, :cond_16

    .line 36426
    iget-object v1, p0, Lheb;->l:[Lhut;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_16

    aget-object v3, v1, v0

    .line 36427
    if-eqz v3, :cond_15

    .line 36428
    const/16 v4, 0x16

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 36426
    :cond_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 36432
    :cond_16
    iget-object v0, p0, Lheb;->m:Lhgz;

    if-eqz v0, :cond_17

    .line 36433
    const/16 v0, 0x17

    iget-object v1, p0, Lheb;->m:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36435
    :cond_17
    iget-object v0, p0, Lheb;->w:Lhog;

    if-eqz v0, :cond_18

    .line 36436
    const/16 v0, 0x18

    iget-object v1, p0, Lheb;->w:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36438
    :cond_18
    iget-object v0, p0, Lheb;->n:Lhnh;

    if-eqz v0, :cond_19

    .line 36439
    const/16 v0, 0x19

    iget-object v1, p0, Lheb;->n:Lhnh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36441
    :cond_19
    iget-object v0, p0, Lheb;->o:Lhea;

    if-eqz v0, :cond_1a

    .line 36442
    const/16 v0, 0x1a

    iget-object v1, p0, Lheb;->o:Lhea;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36444
    :cond_1a
    iget-boolean v0, p0, Lheb;->x:Z

    if-eqz v0, :cond_1b

    .line 36445
    const/16 v0, 0x1b

    iget-boolean v1, p0, Lheb;->x:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 36447
    :cond_1b
    iget-object v0, p0, Lheb;->y:Lhkm;

    if-eqz v0, :cond_1c

    .line 36448
    const v0, 0x39d6d44

    iget-object v1, p0, Lheb;->y:Lhkm;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36450
    :cond_1c
    iget-object v0, p0, Lheb;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 36452
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36269
    if-ne p1, p0, :cond_1

    .line 36297
    :cond_0
    :goto_0
    return v0

    .line 36270
    :cond_1
    instance-of v2, p1, Lheb;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 36271
    :cond_2
    check-cast p1, Lheb;

    .line 36272
    iget-object v2, p0, Lheb;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lheb;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lheb;->b:Lhxf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lheb;->b:Lhxf;

    if-nez v2, :cond_3

    .line 36273
    :goto_2
    iget-object v2, p0, Lheb;->c:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lheb;->c:Lhgz;

    if-nez v2, :cond_3

    .line 36274
    :goto_3
    iget-object v2, p0, Lheb;->p:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lheb;->p:Lhgz;

    if-nez v2, :cond_3

    .line 36275
    :goto_4
    iget-object v2, p0, Lheb;->d:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lheb;->d:Lhgz;

    if-nez v2, :cond_3

    .line 36276
    :goto_5
    iget-object v2, p0, Lheb;->e:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lheb;->e:Lhgz;

    if-nez v2, :cond_3

    .line 36277
    :goto_6
    iget-object v2, p0, Lheb;->f:Lhgz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lheb;->f:Lhgz;

    if-nez v2, :cond_3

    .line 36278
    :goto_7
    iget-object v2, p0, Lheb;->g:Lhog;

    if-nez v2, :cond_b

    iget-object v2, p1, Lheb;->g:Lhog;

    if-nez v2, :cond_3

    .line 36279
    :goto_8
    iget-object v2, p0, Lheb;->q:Ljava/lang/String;

    if-nez v2, :cond_c

    iget-object v2, p1, Lheb;->q:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 36280
    :goto_9
    iget-object v2, p0, Lheb;->h:Lhgz;

    if-nez v2, :cond_d

    iget-object v2, p1, Lheb;->h:Lhgz;

    if-nez v2, :cond_3

    .line 36281
    :goto_a
    iget-object v2, p0, Lheb;->r:Lhxx;

    if-nez v2, :cond_e

    iget-object v2, p1, Lheb;->r:Lhxx;

    if-nez v2, :cond_3

    .line 36282
    :goto_b
    iget-object v2, p0, Lheb;->s:[Lhbi;

    iget-object v3, p1, Lheb;->s:[Lhbi;

    .line 36283
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lheb;->t:Lhxf;

    if-nez v2, :cond_f

    iget-object v2, p1, Lheb;->t:Lhxf;

    if-nez v2, :cond_3

    .line 36284
    :goto_c
    iget-object v2, p0, Lheb;->u:[Lhbi;

    iget-object v3, p1, Lheb;->u:[Lhbi;

    .line 36285
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lheb;->i:Lhec;

    if-nez v2, :cond_10

    iget-object v2, p1, Lheb;->i:Lhec;

    if-nez v2, :cond_3

    .line 36286
    :goto_d
    iget-object v2, p0, Lheb;->j:[B

    iget-object v3, p1, Lheb;->j:[B

    .line 36287
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lheb;->k:[Lhxe;

    iget-object v3, p1, Lheb;->k:[Lhxe;

    .line 36288
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lheb;->v:Z

    iget-boolean v3, p1, Lheb;->v:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lheb;->l:[Lhut;

    iget-object v3, p1, Lheb;->l:[Lhut;

    .line 36290
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lheb;->m:Lhgz;

    if-nez v2, :cond_11

    iget-object v2, p1, Lheb;->m:Lhgz;

    if-nez v2, :cond_3

    .line 36291
    :goto_e
    iget-object v2, p0, Lheb;->w:Lhog;

    if-nez v2, :cond_12

    iget-object v2, p1, Lheb;->w:Lhog;

    if-nez v2, :cond_3

    .line 36292
    :goto_f
    iget-object v2, p0, Lheb;->n:Lhnh;

    if-nez v2, :cond_13

    iget-object v2, p1, Lheb;->n:Lhnh;

    if-nez v2, :cond_3

    .line 36293
    :goto_10
    iget-object v2, p0, Lheb;->o:Lhea;

    if-nez v2, :cond_14

    iget-object v2, p1, Lheb;->o:Lhea;

    if-nez v2, :cond_3

    .line 36294
    :goto_11
    iget-boolean v2, p0, Lheb;->x:Z

    iget-boolean v3, p1, Lheb;->x:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lheb;->y:Lhkm;

    if-nez v2, :cond_15

    iget-object v2, p1, Lheb;->y:Lhkm;

    if-nez v2, :cond_3

    .line 36296
    :goto_12
    iget-object v2, p0, Lheb;->I:Ljava/util/List;

    if-nez v2, :cond_16

    iget-object v2, p1, Lheb;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 36297
    goto/16 :goto_0

    .line 36272
    :cond_4
    iget-object v2, p0, Lheb;->a:Ljava/lang/String;

    iget-object v3, p1, Lheb;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lheb;->b:Lhxf;

    iget-object v3, p1, Lheb;->b:Lhxf;

    .line 36273
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lheb;->c:Lhgz;

    iget-object v3, p1, Lheb;->c:Lhgz;

    .line 36274
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lheb;->p:Lhgz;

    iget-object v3, p1, Lheb;->p:Lhgz;

    .line 36275
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lheb;->d:Lhgz;

    iget-object v3, p1, Lheb;->d:Lhgz;

    .line 36276
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lheb;->e:Lhgz;

    iget-object v3, p1, Lheb;->e:Lhgz;

    .line 36277
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lheb;->f:Lhgz;

    iget-object v3, p1, Lheb;->f:Lhgz;

    .line 36278
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lheb;->g:Lhog;

    iget-object v3, p1, Lheb;->g:Lhog;

    .line 36279
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lheb;->q:Ljava/lang/String;

    iget-object v3, p1, Lheb;->q:Ljava/lang/String;

    .line 36280
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lheb;->h:Lhgz;

    iget-object v3, p1, Lheb;->h:Lhgz;

    .line 36281
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v2, p0, Lheb;->r:Lhxx;

    iget-object v3, p1, Lheb;->r:Lhxx;

    .line 36282
    invoke-virtual {v2, v3}, Lhxx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    .line 36283
    :cond_f
    iget-object v2, p0, Lheb;->t:Lhxf;

    iget-object v3, p1, Lheb;->t:Lhxf;

    .line 36284
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    .line 36285
    :cond_10
    iget-object v2, p0, Lheb;->i:Lhec;

    iget-object v3, p1, Lheb;->i:Lhec;

    .line 36286
    invoke-virtual {v2, v3}, Lhec;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_d

    .line 36290
    :cond_11
    iget-object v2, p0, Lheb;->m:Lhgz;

    iget-object v3, p1, Lheb;->m:Lhgz;

    .line 36291
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v2, p0, Lheb;->w:Lhog;

    iget-object v3, p1, Lheb;->w:Lhog;

    .line 36292
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_f

    :cond_13
    iget-object v2, p0, Lheb;->n:Lhnh;

    iget-object v3, p1, Lheb;->n:Lhnh;

    .line 36293
    invoke-virtual {v2, v3}, Lhnh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_10

    :cond_14
    iget-object v2, p0, Lheb;->o:Lhea;

    iget-object v3, p1, Lheb;->o:Lhea;

    .line 36294
    invoke-virtual {v2, v3}, Lhea;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_11

    :cond_15
    iget-object v2, p0, Lheb;->y:Lhkm;

    iget-object v3, p1, Lheb;->y:Lhkm;

    .line 36296
    invoke-virtual {v2, v3}, Lhkm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_12

    :cond_16
    iget-object v2, p0, Lheb;->I:Ljava/util/List;

    iget-object v3, p1, Lheb;->I:Ljava/util/List;

    .line 36297
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 36301
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 36303
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->a:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 36304
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->b:Lhxf;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 36305
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->c:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 36306
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->p:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 36307
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->d:Lhgz;

    if-nez v0, :cond_9

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 36308
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->e:Lhgz;

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 36309
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->f:Lhgz;

    if-nez v0, :cond_b

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 36310
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->g:Lhog;

    if-nez v0, :cond_c

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 36311
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->q:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 36312
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->h:Lhgz;

    if-nez v0, :cond_e

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 36313
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->r:Lhxx;

    if-nez v0, :cond_f

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 36314
    iget-object v2, p0, Lheb;->s:[Lhbi;

    if-nez v2, :cond_10

    mul-int/lit8 v2, v0, 0x1f

    .line 36320
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lheb;->t:Lhxf;

    if-nez v0, :cond_12

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 36321
    iget-object v2, p0, Lheb;->u:[Lhbi;

    if-nez v2, :cond_13

    mul-int/lit8 v2, v0, 0x1f

    .line 36327
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lheb;->i:Lhec;

    if-nez v0, :cond_15

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 36328
    iget-object v2, p0, Lheb;->j:[B

    if-nez v2, :cond_16

    mul-int/lit8 v2, v0, 0x1f

    .line 36334
    :cond_2
    iget-object v0, p0, Lheb;->k:[Lhxe;

    if-nez v0, :cond_17

    mul-int/lit8 v2, v2, 0x1f

    .line 36340
    :cond_3
    mul-int/lit8 v2, v2, 0x1f

    iget-boolean v0, p0, Lheb;->v:Z

    if-eqz v0, :cond_19

    move v0, v3

    :goto_d
    add-int/2addr v0, v2

    .line 36341
    iget-object v2, p0, Lheb;->l:[Lhut;

    if-nez v2, :cond_1a

    mul-int/lit8 v2, v0, 0x1f

    .line 36347
    :cond_4
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lheb;->m:Lhgz;

    if-nez v0, :cond_1c

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    .line 36348
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->w:Lhog;

    if-nez v0, :cond_1d

    move v0, v1

    :goto_f
    add-int/2addr v0, v2

    .line 36349
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->n:Lhnh;

    if-nez v0, :cond_1e

    move v0, v1

    :goto_10
    add-int/2addr v0, v2

    .line 36350
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->o:Lhea;

    if-nez v0, :cond_1f

    move v0, v1

    :goto_11
    add-int/2addr v0, v2

    .line 36351
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lheb;->x:Z

    if-eqz v2, :cond_20

    :goto_12
    add-int/2addr v0, v3

    .line 36352
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheb;->y:Lhkm;

    if-nez v0, :cond_21

    move v0, v1

    :goto_13
    add-int/2addr v0, v2

    .line 36353
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lheb;->I:Ljava/util/List;

    if-nez v2, :cond_22

    :goto_14
    add-int/2addr v0, v1

    .line 36354
    return v0

    .line 36303
    :cond_5
    iget-object v0, p0, Lheb;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 36304
    :cond_6
    iget-object v0, p0, Lheb;->b:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 36305
    :cond_7
    iget-object v0, p0, Lheb;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 36306
    :cond_8
    iget-object v0, p0, Lheb;->p:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 36307
    :cond_9
    iget-object v0, p0, Lheb;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 36308
    :cond_a
    iget-object v0, p0, Lheb;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 36309
    :cond_b
    iget-object v0, p0, Lheb;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 36310
    :cond_c
    iget-object v0, p0, Lheb;->g:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 36311
    :cond_d
    iget-object v0, p0, Lheb;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 36312
    :cond_e
    iget-object v0, p0, Lheb;->h:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 36313
    :cond_f
    iget-object v0, p0, Lheb;->r:Lhxx;

    invoke-virtual {v0}, Lhxx;->hashCode()I

    move-result v0

    goto/16 :goto_a

    :cond_10
    move v2, v0

    move v0, v1

    .line 36316
    :goto_15
    iget-object v5, p0, Lheb;->s:[Lhbi;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 36317
    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lheb;->s:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_11

    move v2, v1

    :goto_16
    add-int/2addr v2, v5

    .line 36316
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 36317
    :cond_11
    iget-object v2, p0, Lheb;->s:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_16

    .line 36320
    :cond_12
    iget-object v0, p0, Lheb;->t:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto/16 :goto_b

    :cond_13
    move v2, v0

    move v0, v1

    .line 36323
    :goto_17
    iget-object v5, p0, Lheb;->u:[Lhbi;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 36324
    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lheb;->u:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_14

    move v2, v1

    :goto_18
    add-int/2addr v2, v5

    .line 36323
    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    .line 36324
    :cond_14
    iget-object v2, p0, Lheb;->u:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_18

    .line 36327
    :cond_15
    iget-object v0, p0, Lheb;->i:Lhec;

    invoke-virtual {v0}, Lhec;->hashCode()I

    move-result v0

    goto/16 :goto_c

    :cond_16
    move v2, v0

    move v0, v1

    .line 36330
    :goto_19
    iget-object v5, p0, Lheb;->j:[B

    array-length v5, v5

    if-ge v0, v5, :cond_2

    .line 36331
    mul-int/lit8 v2, v2, 0x1f

    iget-object v5, p0, Lheb;->j:[B

    aget-byte v5, v5, v0

    add-int/2addr v2, v5

    .line 36330
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    :cond_17
    move v0, v1

    .line 36336
    :goto_1a
    iget-object v5, p0, Lheb;->k:[Lhxe;

    array-length v5, v5

    if-ge v0, v5, :cond_3

    .line 36337
    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lheb;->k:[Lhxe;

    aget-object v2, v2, v0

    if-nez v2, :cond_18

    move v2, v1

    :goto_1b
    add-int/2addr v2, v5

    .line 36336
    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 36337
    :cond_18
    iget-object v2, p0, Lheb;->k:[Lhxe;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhxe;->hashCode()I

    move-result v2

    goto :goto_1b

    :cond_19
    move v0, v4

    .line 36340
    goto/16 :goto_d

    :cond_1a
    move v2, v0

    move v0, v1

    .line 36343
    :goto_1c
    iget-object v5, p0, Lheb;->l:[Lhut;

    array-length v5, v5

    if-ge v0, v5, :cond_4

    .line 36344
    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lheb;->l:[Lhut;

    aget-object v2, v2, v0

    if-nez v2, :cond_1b

    move v2, v1

    :goto_1d
    add-int/2addr v2, v5

    .line 36343
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 36344
    :cond_1b
    iget-object v2, p0, Lheb;->l:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhut;->hashCode()I

    move-result v2

    goto :goto_1d

    .line 36347
    :cond_1c
    iget-object v0, p0, Lheb;->m:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 36348
    :cond_1d
    iget-object v0, p0, Lheb;->w:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_f

    .line 36349
    :cond_1e
    iget-object v0, p0, Lheb;->n:Lhnh;

    invoke-virtual {v0}, Lhnh;->hashCode()I

    move-result v0

    goto/16 :goto_10

    .line 36350
    :cond_1f
    iget-object v0, p0, Lheb;->o:Lhea;

    invoke-virtual {v0}, Lhea;->hashCode()I

    move-result v0

    goto/16 :goto_11

    :cond_20
    move v3, v4

    .line 36351
    goto/16 :goto_12

    .line 36352
    :cond_21
    iget-object v0, p0, Lheb;->y:Lhkm;

    invoke-virtual {v0}, Lhkm;->hashCode()I

    move-result v0

    goto/16 :goto_13

    .line 36353
    :cond_22
    iget-object v1, p0, Lheb;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_14
.end method
