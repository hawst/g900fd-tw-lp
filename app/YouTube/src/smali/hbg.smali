.class public final Lhbg;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhod;

.field public b:Lhfz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14951
    invoke-direct {p0}, Lidf;-><init>()V

    .line 14954
    iput-object v0, p0, Lhbg;->a:Lhod;

    .line 14957
    iput-object v0, p0, Lhbg;->b:Lhfz;

    .line 14951
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 15000
    const/4 v0, 0x0

    .line 15001
    iget-object v1, p0, Lhbg;->a:Lhod;

    if-eqz v1, :cond_0

    .line 15002
    const v0, 0x31dea0e

    iget-object v1, p0, Lhbg;->a:Lhod;

    .line 15003
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15005
    :cond_0
    iget-object v1, p0, Lhbg;->b:Lhfz;

    if-eqz v1, :cond_1

    .line 15006
    const v1, 0x32dfc43

    iget-object v2, p0, Lhbg;->b:Lhfz;

    .line 15007
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15009
    :cond_1
    iget-object v1, p0, Lhbg;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15010
    iput v0, p0, Lhbg;->J:I

    .line 15011
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 14947
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbg;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbg;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbg;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhbg;->a:Lhod;

    if-nez v0, :cond_2

    new-instance v0, Lhod;

    invoke-direct {v0}, Lhod;-><init>()V

    iput-object v0, p0, Lhbg;->a:Lhod;

    :cond_2
    iget-object v0, p0, Lhbg;->a:Lhod;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhbg;->b:Lhfz;

    if-nez v0, :cond_3

    new-instance v0, Lhfz;

    invoke-direct {v0}, Lhfz;-><init>()V

    iput-object v0, p0, Lhbg;->b:Lhfz;

    :cond_3
    iget-object v0, p0, Lhbg;->b:Lhfz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18ef5072 -> :sswitch_1
        0x196fe21a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 14988
    iget-object v0, p0, Lhbg;->a:Lhod;

    if-eqz v0, :cond_0

    .line 14989
    const v0, 0x31dea0e

    iget-object v1, p0, Lhbg;->a:Lhod;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 14991
    :cond_0
    iget-object v0, p0, Lhbg;->b:Lhfz;

    if-eqz v0, :cond_1

    .line 14992
    const v0, 0x32dfc43

    iget-object v1, p0, Lhbg;->b:Lhfz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 14994
    :cond_1
    iget-object v0, p0, Lhbg;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 14996
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 14969
    if-ne p1, p0, :cond_1

    .line 14974
    :cond_0
    :goto_0
    return v0

    .line 14970
    :cond_1
    instance-of v2, p1, Lhbg;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 14971
    :cond_2
    check-cast p1, Lhbg;

    .line 14972
    iget-object v2, p0, Lhbg;->a:Lhod;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbg;->a:Lhod;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhbg;->b:Lhfz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbg;->b:Lhfz;

    if-nez v2, :cond_3

    .line 14973
    :goto_2
    iget-object v2, p0, Lhbg;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhbg;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 14974
    goto :goto_0

    .line 14972
    :cond_4
    iget-object v2, p0, Lhbg;->a:Lhod;

    iget-object v3, p1, Lhbg;->a:Lhod;

    invoke-virtual {v2, v3}, Lhod;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbg;->b:Lhfz;

    iget-object v3, p1, Lhbg;->b:Lhfz;

    .line 14973
    invoke-virtual {v2, v3}, Lhfz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhbg;->I:Ljava/util/List;

    iget-object v3, p1, Lhbg;->I:Ljava/util/List;

    .line 14974
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 14978
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 14980
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbg;->a:Lhod;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 14981
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbg;->b:Lhfz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 14982
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbg;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 14983
    return v0

    .line 14980
    :cond_0
    iget-object v0, p0, Lhbg;->a:Lhod;

    invoke-virtual {v0}, Lhod;->hashCode()I

    move-result v0

    goto :goto_0

    .line 14981
    :cond_1
    iget-object v0, p0, Lhbg;->b:Lhfz;

    invoke-virtual {v0}, Lhfz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 14982
    :cond_2
    iget-object v1, p0, Lhbg;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
