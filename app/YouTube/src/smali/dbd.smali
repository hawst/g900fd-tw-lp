.class final Ldbd;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private synthetic a:Ldbj;


# direct methods
.method constructor <init>(Ldbc;Ldbj;)V
    .locals 0

    .prologue
    .line 108
    iput-object p2, p0, Ldbd;->a:Ldbj;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 111
    const-string v0, "com.google.android.youtube.action.controller_notification_prev"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Ldbd;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->e()V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    const-string v0, "com.google.android.youtube.action.controller_notification_play_pause"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Ldbd;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->d()V

    goto :goto_0

    .line 115
    :cond_2
    const-string v0, "com.google.android.youtube.action.controller_notification_next"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 116
    iget-object v0, p0, Ldbd;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->f()V

    goto :goto_0

    .line 117
    :cond_3
    const-string v0, "com.google.android.youtube.action.controller_notification_close"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 118
    iget-object v0, p0, Ldbd;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->g()V

    goto :goto_0

    .line 119
    :cond_4
    const-string v0, "com.google.android.youtube.action.controller_notification_replay"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Ldbd;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->h()V

    goto :goto_0
.end method
