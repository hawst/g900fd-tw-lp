.class public final Lbmy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldso;


# instance fields
.field final a:Ldbe;

.field b:Ldwq;

.field private final c:Landroid/content/res/Resources;

.field private final d:Levn;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;)V
    .locals 14

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v13

    .line 41
    iget-object v8, p1, Lckz;->a:Letc;

    .line 42
    iget-object v1, v13, Lari;->an:Landroid/content/Context;

    .line 43
    invoke-virtual {v8}, Letc;->i()Levn;

    move-result-object v0

    iput-object v0, p0, Lbmy;->d:Levn;

    .line 45
    new-instance v0, Ldbe;

    .line 47
    invoke-virtual {v8}, Letc;->p()Landroid/os/Handler;

    move-result-object v2

    .line 48
    invoke-virtual {v13}, Lari;->b()Lfxe;

    move-result-object v3

    .line 49
    invoke-virtual {v13}, Lari;->c()Leyp;

    move-result-object v4

    .line 50
    invoke-virtual {v13}, Lari;->aD()Lcst;

    move-result-object v5

    .line 51
    invoke-virtual {v13}, Lari;->O()Lgng;

    move-result-object v6

    .line 52
    invoke-virtual {v8}, Letc;->b()Lexd;

    move-result-object v7

    .line 53
    invoke-virtual {v8}, Letc;->e()Ljava/lang/String;

    move-result-object v8

    .line 54
    const-class v9, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v10, Lbnb;

    invoke-direct {v10, p0}, Lbnb;-><init>(Lbmy;)V

    const v11, 0x7f020148

    .line 57
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v12

    invoke-virtual {v12}, Lari;->K()Ldsn;

    move-result-object v12

    .line 58
    invoke-virtual {v13}, Lari;->aS()Ldbb;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, Ldbe;-><init>(Landroid/content/Context;Landroid/os/Handler;Lfxe;Leyp;Lgix;Lgng;Lexd;Ljava/lang/String;Ljava/lang/Class;Ldbj;ILdba;Ldbb;)V

    iput-object v0, p0, Lbmy;->a:Ldbe;

    .line 60
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbmy;->c:Landroid/content/res/Resources;

    .line 62
    iget-object v0, p0, Lbmy;->a:Ldbe;

    invoke-virtual {v0}, Ldbe;->b()V

    .line 65
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 66
    iget-object v0, p0, Lbmy;->a:Ldbe;

    new-instance v1, Lbmz;

    invoke-direct {v1, p0}, Lbmz;-><init>(Lbmy;)V

    invoke-virtual {v0, v1}, Ldbe;->a(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 75
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ldwq;Z)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 79
    iget-object v0, p0, Lbmy;->b:Ldwq;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lbmy;->d:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lbmy;->a:Ldbe;

    invoke-virtual {v0}, Ldbe;->b()V

    .line 83
    :cond_0
    iput-object p1, p0, Lbmy;->b:Ldwq;

    .line 84
    iget-object v0, p0, Lbmy;->b:Ldwq;

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lbmy;->d:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 86
    iget-object v0, p0, Lbmy;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_3

    .line 88
    invoke-virtual {v0}, Ldwr;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 89
    iget-object v1, p0, Lbmy;->a:Ldbe;

    invoke-virtual {v1, v4}, Ldbe;->a(Z)V

    .line 90
    iget-object v1, p0, Lbmy;->b:Ldwq;

    invoke-interface {v1}, Ldwq;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 91
    iget-object v1, p0, Lbmy;->a:Ldbe;

    iget-object v2, p0, Lbmy;->b:Ldwq;

    invoke-interface {v2}, Ldwq;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldbe;->a(Ljava/lang/String;)V

    .line 93
    :cond_1
    iget-object v1, p0, Lbmy;->a:Ldbe;

    iget-object v2, p0, Lbmy;->c:Landroid/content/res/Resources;

    const v3, 0x7f0902fb

    new-array v4, v4, [Ljava/lang/Object;

    .line 94
    invoke-virtual {v0}, Ldwr;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 93
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v1, Ldbe;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbi;

    invoke-interface {v0, v2}, Ldbi;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_2
    iget-object v0, p0, Lbmy;->a:Ldbe;

    invoke-virtual {v0, v5}, Ldbe;->a(Z)V

    .line 103
    :cond_3
    return-void
.end method

.method public final onMdxPlaybackChangedEvent(Ldwi;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 157
    iget-object v0, p1, Ldwi;->a:Ldwj;

    invoke-virtual {v0}, Ldwj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lbmy;->a:Ldbe;

    iget-object v1, p1, Ldwi;->a:Ldwj;

    iget-object v1, v1, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbe;->a(Ljava/lang/String;)V

    .line 160
    :cond_0
    iget-object v0, p0, Lbmy;->b:Ldwq;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lbmy;->a:Ldbe;

    iget-object v1, p0, Lbmy;->b:Ldwq;

    .line 162
    invoke-interface {v1}, Ldwq;->d()Z

    move-result v1

    iget-object v2, p0, Lbmy;->b:Ldwq;

    .line 163
    invoke-interface {v2}, Ldwq;->f()Z

    move-result v2

    .line 161
    invoke-virtual {v0, v1, v2}, Ldbe;->a(ZZ)V

    .line 165
    :cond_1
    return-void
.end method

.method public final onMdxVideoPlayerStateChangedEvent(Ldxa;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 107
    iget-object v1, p1, Ldxa;->a:Ldwo;

    .line 109
    sget-object v0, Lbna;->a:[I

    invoke-virtual {v1}, Ldwo;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 127
    :goto_0
    sget-object v0, Ldbl;->d:Ldbl;

    .line 128
    sget-object v2, Lbna;->a:[I

    invoke-virtual {v1}, Ldwo;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    .line 145
    :goto_1
    iget-object v1, p0, Lbmy;->a:Ldbe;

    invoke-virtual {v1, v0}, Ldbe;->a(Ldbl;)V

    .line 152
    iget-object v1, p0, Lbmy;->a:Ldbe;

    iget-object v0, p1, Ldxa;->a:Ldwo;

    sget-object v2, Ldwo;->g:Ldwo;

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Ldbe;->b(Z)V

    .line 153
    return-void

    .line 117
    :pswitch_0
    iget-object v0, p0, Lbmy;->a:Ldbe;

    invoke-virtual {v0}, Ldbe;->a()V

    goto :goto_0

    .line 122
    :pswitch_1
    iget-object v0, p0, Lbmy;->a:Ldbe;

    invoke-virtual {v0}, Ldbe;->b()V

    goto :goto_0

    .line 130
    :pswitch_2
    sget-object v0, Ldbl;->b:Ldbl;

    goto :goto_1

    .line 133
    :pswitch_3
    sget-object v0, Ldbl;->f:Ldbl;

    goto :goto_1

    .line 136
    :pswitch_4
    sget-object v0, Ldbl;->c:Ldbl;

    goto :goto_1

    .line 141
    :pswitch_5
    sget-object v0, Ldbl;->a:Ldbl;

    goto :goto_1

    .line 144
    :pswitch_6
    sget-object v0, Ldbl;->e:Ldbl;

    goto :goto_1

    .line 152
    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 128
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method
