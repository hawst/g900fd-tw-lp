.class final Lbre;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic a:Lfxe;

.field private synthetic b:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lfxe;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lbre;->a:Lfxe;

    iput-object p2, p0, Lbre;->b:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lbre;->a:Lfxe;

    invoke-interface {v0}, Lfxe;->c()V

    .line 145
    invoke-static {}, Lbra;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feature"

    const-string v2, "android"

    .line 146
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "app_version"

    iget-object v2, p0, Lbre;->b:Landroid/app/Activity;

    .line 149
    invoke-static {v2}, La;->s(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 147
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 151
    invoke-static {v0}, Ldol;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lbre;->b:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 153
    return-void
.end method
