.class public final enum Lesj;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lesj;

.field public static final enum b:Lesj;

.field public static final enum c:Lesj;

.field private static final synthetic e:[Lesj;


# instance fields
.field public d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 31
    new-instance v0, Lesj;

    const-string v1, "PRE_ROLL"

    invoke-direct {v0, v1, v4, v2}, Lesj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lesj;->a:Lesj;

    new-instance v0, Lesj;

    const-string v1, "MID_ROLL"

    invoke-direct {v0, v1, v2, v3}, Lesj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lesj;->b:Lesj;

    new-instance v0, Lesj;

    const-string v1, "POST_ROLL"

    invoke-direct {v0, v1, v3, v5}, Lesj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lesj;->c:Lesj;

    .line 30
    new-array v0, v5, [Lesj;

    sget-object v1, Lesj;->a:Lesj;

    aput-object v1, v0, v4

    sget-object v1, Lesj;->b:Lesj;

    aput-object v1, v0, v2

    sget-object v1, Lesj;->c:Lesj;

    aput-object v1, v0, v3

    sput-object v0, Lesj;->e:[Lesj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput p3, p0, Lesj;->d:I

    .line 37
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lesj;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lesj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lesj;

    return-object v0
.end method

.method public static values()[Lesj;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lesj;->e:[Lesj;

    invoke-virtual {v0}, [Lesj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lesj;

    return-object v0
.end method
