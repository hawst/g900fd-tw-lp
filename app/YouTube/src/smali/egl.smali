.class public final Legl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Legc;


# instance fields
.field final a:Ljava/io/File;

.field private final b:Legd;

.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/HashMap;

.field private final e:Ljava/util/HashMap;

.field private f:J


# direct methods
.method public constructor <init>(Ljava/io/File;Legd;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Legl;->f:J

    .line 38
    iput-object p1, p0, Legl;->a:Ljava/io/File;

    .line 39
    iput-object p2, p0, Legl;->b:Legd;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Legl;->c:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Legl;->d:Ljava/util/HashMap;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Legl;->e:Ljava/util/HashMap;

    .line 44
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    .line 45
    new-instance v1, Legm;

    invoke-direct {v1, p0, v0}, Legm;-><init>(Legl;Landroid/os/ConditionVariable;)V

    .line 53
    invoke-virtual {v1}, Legm;->start()V

    .line 54
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 55
    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    .line 278
    iget-object v0, p0, Legl;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 279
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 280
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 281
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 282
    const/4 v0, 0x1

    move v1, v0

    .line 283
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 284
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legi;

    .line 285
    iget-object v4, v0, Legi;->e:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 286
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 287
    iget-boolean v4, v0, Legi;->d:Z

    if-eqz v4, :cond_1

    .line 288
    iget-wide v4, p0, Legl;->f:J

    iget-wide v6, v0, Legi;->c:J

    sub-long/2addr v4, v6

    iput-wide v4, p0, Legl;->f:J

    .line 290
    :cond_1
    invoke-direct {p0, v0}, Legl;->f(Legi;)V

    goto :goto_1

    .line 292
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    .line 294
    goto :goto_1

    .line 295
    :cond_3
    if-eqz v1, :cond_0

    .line 296
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 299
    :cond_4
    return-void
.end method

.method private declared-synchronized d(Legi;)Legi;
    .locals 11

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Legl;->e(Legi;)Legi;

    move-result-object v10

    .line 122
    iget-boolean v2, v10, Legi;->d:Z

    if-eqz v2, :cond_1

    .line 125
    iget-object v2, p0, Legl;->d:Ljava/util/HashMap;

    iget-object v3, v10, Legi;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/TreeSet;

    move-object v9, v0

    .line 126
    invoke-virtual {v9, v10}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, La;->c(Z)V

    .line 128
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v2, v10, Legi;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    iget-object v3, v10, Legi;->a:Ljava/lang/String;

    iget-wide v4, v10, Legi;->b:J

    invoke-static/range {v2 .. v7}, Legi;->a(Ljava/io/File;Ljava/lang/String;JJ)Ljava/io/File;

    move-result-object v8

    iget-object v2, v10, Legi;->e:Ljava/io/File;

    invoke-virtual {v2, v8}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    iget-object v3, v10, Legi;->a:Ljava/lang/String;

    iget-wide v4, v10, Legi;->b:J

    invoke-static/range {v3 .. v8}, Legi;->a(Ljava/lang/String;JJLjava/io/File;)Legi;

    move-result-object v4

    .line 130
    invoke-virtual {v9, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 131
    iget-object v2, p0, Legl;->e:Ljava/util/HashMap;

    iget-object v3, v10, Legi;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v5, v3

    :goto_0
    if-ltz v5, :cond_0

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Legd;

    invoke-interface {v3, p0, v10, v4}, Legd;->a(Legc;Legi;Legi;)V

    add-int/lit8 v3, v5, -0x1

    move v5, v3

    goto :goto_0

    :cond_0
    iget-object v2, p0, Legl;->b:Legd;

    invoke-interface {v2, p0, v10, v4}, Legd;->a(Legc;Legi;Legi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v4

    .line 142
    :goto_1
    monitor-exit p0

    return-object v2

    .line 136
    :cond_1
    :try_start_1
    iget-object v2, p0, Legl;->c:Ljava/util/HashMap;

    iget-object v3, p1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 137
    iget-object v2, p0, Legl;->c:Ljava/util/HashMap;

    iget-object v3, p1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v10

    .line 138
    goto :goto_1

    .line 142
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 119
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private e(Legi;)Legi;
    .locals 10

    .prologue
    .line 194
    :goto_0
    iget-object v2, p1, Legi;->a:Ljava/lang/String;

    .line 195
    iget-wide v4, p1, Legi;->b:J

    .line 196
    iget-object v0, p0, Legl;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    .line 197
    if-nez v0, :cond_1

    .line 198
    iget-wide v0, p1, Legi;->b:J

    invoke-static {v2, v0, v1}, Legi;->b(Ljava/lang/String;J)Legi;

    move-result-object v1

    .line 215
    :cond_0
    :goto_1
    return-object v1

    .line 200
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Legi;

    .line 201
    if-eqz v1, :cond_2

    iget-wide v6, v1, Legi;->b:J

    cmp-long v3, v6, v4

    if-gtz v3, :cond_2

    iget-wide v6, v1, Legi;->b:J

    iget-wide v8, v1, Legi;->c:J

    add-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 204
    iget-object v0, v1, Legi;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    invoke-direct {p0}, Legl;->c()V

    goto :goto_0

    .line 213
    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legi;

    .line 214
    if-nez v0, :cond_3

    iget-wide v0, p1, Legi;->b:J

    invoke-static {v2, v0, v1}, Legi;->b(Ljava/lang/String;J)Legi;

    move-result-object v1

    goto :goto_1

    :cond_3
    iget-wide v4, p1, Legi;->b:J

    iget-wide v0, v0, Legi;->b:J

    iget-wide v6, p1, Legi;->b:J

    sub-long/2addr v0, v6

    .line 215
    invoke-static {v2, v4, v5, v0, v1}, Legi;->a(Ljava/lang/String;JJ)Legi;

    move-result-object v1

    goto :goto_1
.end method

.method private f(Legi;)V
    .locals 3

    .prologue
    .line 302
    iget-object v0, p0, Legl;->e:Ljava/util/HashMap;

    iget-object v1, p1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 303
    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_0

    .line 305
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Legd;

    invoke-interface {v1, p1}, Legd;->a(Legi;)V

    .line 304
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    .line 308
    :cond_0
    iget-object v0, p0, Legl;->b:Legd;

    invoke-interface {v0, p1}, Legd;->a(Legi;)V

    .line 309
    return-void
.end method

.method private g(Legi;)V
    .locals 3

    .prologue
    .line 312
    iget-object v0, p0, Legl;->e:Ljava/util/HashMap;

    iget-object v1, p1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 313
    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_0

    .line 315
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Legd;

    invoke-interface {v1, p0, p1}, Legd;->a(Legc;Legi;)V

    .line 314
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    .line 318
    :cond_0
    iget-object v0, p0, Legl;->b:Legd;

    invoke-interface {v0, p0, p1}, Legd;->a(Legc;Legi;)V

    .line 319
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;J)Legi;
    .locals 2

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2, p3}, Legi;->a(Ljava/lang/String;J)Legi;

    move-result-object v0

    .line 100
    :goto_0
    invoke-direct {p0, v0}, Legl;->d(Legi;)Legi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 101
    if-eqz v1, :cond_0

    .line 102
    monitor-exit p0

    return-object v1

    .line 108
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;JJ)Ljava/io/File;
    .locals 6

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Legl;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, La;->c(Z)V

    .line 148
    iget-object v0, p0, Legl;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    invoke-direct {p0}, Legl;->c()V

    .line 151
    iget-object v0, p0, Legl;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 153
    :cond_0
    iget-object v0, p0, Legl;->b:Legd;

    invoke-interface {v0, p0, p4, p5}, Legd;->a(Legc;J)V

    .line 154
    iget-object v0, p0, Legl;->a:Ljava/io/File;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v1, p1

    move-wide v2, p2

    invoke-static/range {v0 .. v5}, Legi;->a(Ljava/io/File;Ljava/lang/String;JJ)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Ljava/util/NavigableSet;
    .locals 2

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Legl;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/SortedSet;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/util/Set;
    .locals 2

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Legl;->d:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Legi;)V
    .locals 2

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Legl;->c:Ljava/util/HashMap;

    iget-object v1, p1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 179
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    monitor-exit p0

    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/io/File;)V
    .locals 6

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Legi;->a(Ljava/io/File;)Legi;

    move-result-object v1

    .line 160
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 161
    iget-object v0, p0, Legl;->c:Ljava/util/HashMap;

    iget-object v2, v1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, La;->c(Z)V

    .line 163
    invoke-virtual {p1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 174
    :goto_1
    monitor-exit p0

    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 167
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 168
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 169
    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 172
    :cond_2
    :try_start_2
    invoke-virtual {p0, v1}, Legl;->c(Legi;)V

    .line 173
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized b()J
    .locals 2

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Legl;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;J)Legi;
    .locals 2

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2, p3}, Legi;->a(Ljava/lang/String;J)Legi;

    move-result-object v0

    invoke-direct {p0, v0}, Legl;->d(Legi;)Legi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Legi;)V
    .locals 6

    .prologue
    .line 263
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Legl;->d:Ljava/util/HashMap;

    iget-object v1, p1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    .line 264
    iget-wide v2, p0, Legl;->f:J

    iget-wide v4, p1, Legi;->c:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Legl;->f:J

    .line 265
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, La;->c(Z)V

    .line 266
    iget-object v1, p1, Legi;->e:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 267
    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Legl;->d:Ljava/util/HashMap;

    iget-object v1, p1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    :cond_0
    invoke-direct {p0, p1}, Legl;->f(Legi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    monitor-exit p0

    return-void

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;JJ)Z
    .locals 10

    .prologue
    .line 333
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Legl;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    if-nez v0, :cond_0

    .line 335
    const/4 v0, 0x0

    .line 365
    :goto_0
    monitor-exit p0

    return v0

    .line 337
    :cond_0
    :try_start_1
    invoke-static {p1, p2, p3}, Legi;->a(Ljava/lang/String;J)Legi;

    move-result-object v1

    .line 338
    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Legi;

    .line 339
    if-eqz v1, :cond_1

    iget-wide v2, v1, Legi;->b:J

    iget-wide v4, v1, Legi;->c:J

    add-long/2addr v2, v4

    cmp-long v2, v2, p2

    if-gtz v2, :cond_2

    .line 341
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 343
    :cond_2
    add-long v4, p2, p4

    .line 344
    iget-wide v2, v1, Legi;->b:J

    iget-wide v6, v1, Legi;->c:J

    add-long/2addr v2, v6

    .line 345
    cmp-long v6, v2, v4

    if-ltz v6, :cond_3

    .line 347
    const/4 v0, 0x1

    goto :goto_0

    .line 349
    :cond_3
    const/4 v6, 0x0

    invoke-virtual {v0, v1, v6}, Ljava/util/TreeSet;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 350
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 351
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legi;

    .line 352
    iget-wide v8, v0, Legi;->b:J

    cmp-long v1, v8, v2

    if-lez v1, :cond_4

    .line 354
    const/4 v0, 0x0

    goto :goto_0

    .line 358
    :cond_4
    iget-wide v8, v0, Legi;->b:J

    iget-wide v0, v0, Legi;->c:J

    add-long/2addr v0, v8

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 359
    cmp-long v2, v0, v4

    if-ltz v2, :cond_5

    .line 361
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move-wide v2, v0

    .line 363
    goto :goto_1

    .line 365
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c(Legi;)V
    .locals 4

    .prologue
    .line 251
    iget-object v0, p0, Legl;->d:Ljava/util/HashMap;

    iget-object v1, p1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    .line 252
    if-nez v0, :cond_0

    .line 253
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 254
    iget-object v1, p0, Legl;->d:Ljava/util/HashMap;

    iget-object v2, p1, Legi;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 257
    iget-wide v0, p0, Legl;->f:J

    iget-wide v2, p1, Legi;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Legl;->f:J

    .line 258
    invoke-direct {p0, p1}, Legl;->g(Legi;)V

    .line 259
    return-void
.end method
