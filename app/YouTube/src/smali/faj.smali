.class public final Lfaj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static b:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lfaj;->a:I

    .line 23
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lfaj;->b:Ljava/util/Random;

    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 19
    sget v0, Lfaj;->a:I

    return v0
.end method

.method public static a(Ljava/lang/String;)Lfal;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 82
    .line 83
    sget v1, Lfaj;->a:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 84
    new-instance v0, Lfal;

    invoke-direct {v0}, Lfal;-><init>()V

    .line 85
    iput-object p0, v0, Lfal;->a:Ljava/lang/String;

    .line 86
    sget-object v1, Lfaj;->b:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    iput v1, v0, Lfal;->b:I

    .line 87
    iget-object v1, v0, Lfal;->a:Ljava/lang/String;

    iget v2, v0, Lfal;->b:I

    :try_start_0
    sget-object v3, Lfak;->a:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_0

    sget-object v3, Lfak;->a:Ljava/lang/reflect/Method;

    const/4 v4, 0x0

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-wide/16 v8, 0x1000

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    const/4 v1, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 87
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lfal;)V
    .locals 8

    .prologue
    .line 98
    sget v0, Lfaj;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 99
    iget-object v0, p0, Lfal;->a:Ljava/lang/String;

    iget v1, p0, Lfal;->b:I

    :try_start_0
    sget-object v2, Lfak;->b:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_0

    sget-object v2, Lfak;->b:Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-wide/16 v6, 0x1000

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    const/4 v0, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 99
    :catch_1
    move-exception v0

    goto :goto_0
.end method
