.class public final Lbfw;
.super Li;
.source "SourceFile"

# interfaces
.implements Lbmp;
.implements Lfve;


# instance fields
.field private W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private X:Letc;

.field private Y:Lari;

.field private Z:Lfhz;

.field private aa:Lgix;

.field private ab:Lfcd;

.field private ac:Leyp;

.field private ad:Lfgp;

.field private ae:Leyt;

.field private af:Lcub;

.field private ag:Lhog;

.field private ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private ai:Lcag;

.field private aj:Lbmm;

.field private ak:Lbzu;

.field private al:Landroid/app/AlertDialog;

.field private am:Lfok;

.field private an:Z

.field private ao:Landroid/view/View;

.field private ap:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private aq:Landroid/app/AlertDialog;

.field private ar:Landroid/widget/TextView;

.field private as:Landroid/widget/TextView;

.field private at:Landroid/widget/TextView;

.field private au:Landroid/widget/TextView;

.field private av:Lfvi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Li;-><init>()V

    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lbfw;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 248
    iget-object v0, p0, Lbfw;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 250
    iget-object v0, p0, Lbfw;->ag:Lhog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbfw;->ag:Lhog;

    iget-object v0, v0, Lhog;->C:Licv;

    if-nez v0, :cond_1

    .line 251
    :cond_0
    const-string v0, "Invalid navigation endpoint provided."

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 252
    invoke-virtual {p0}, Lbfw;->a()V

    .line 288
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Lbfw;->ad:Lfgp;

    new-instance v1, Lfgw;

    iget-object v2, v0, Lfgp;->b:Lfsz;

    iget-object v0, v0, Lfgp;->c:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lfgw;-><init>(Lfsz;Lgit;)V

    .line 257
    iget-object v0, p0, Lbfw;->ag:Lhog;

    iget-object v0, v0, Lhog;->C:Licv;

    invoke-virtual {v1, v0}, Lfgw;->a(Licv;)Lfgw;

    .line 258
    iget-object v0, p0, Lbfw;->ag:Lhog;

    invoke-static {v0}, Lbbp;->a(Lhog;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lfgw;->a([B)V

    .line 259
    iget-object v0, p0, Lbfw;->ad:Lfgp;

    new-instance v2, Lbga;

    invoke-direct {v2, p0}, Lbga;-><init>(Lbfw;)V

    iget-object v0, v0, Lfgp;->g:Lfgx;

    invoke-virtual {v0, v1, v2}, Lfgx;->b(Lfsp;Lwv;)V

    goto :goto_0
.end method

.method static synthetic a(Lbfw;Lfok;)Lfok;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lbfw;->am:Lfok;

    return-object p1
.end method

.method static synthetic a(Lbfw;)V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lbfw;->am:Lfok;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbfw;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbfw;->al:Landroid/app/AlertDialog;

    if-nez v0, :cond_3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lbfw;->am:Lfok;

    iget-object v2, v1, Lfok;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_1

    iget-object v2, v1, Lfok;->a:Lhfv;

    iget-object v2, v2, Lhfv;->c:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfok;->d:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, v1, Lfok;->d:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lbfw;->am:Lfok;

    iget-object v2, v1, Lfok;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_2

    iget-object v2, v1, Lfok;->a:Lhfv;

    iget-object v2, v2, Lhfv;->b:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfok;->c:Ljava/lang/CharSequence;

    :cond_2
    iget-object v1, v1, Lfok;->c:Ljava/lang/CharSequence;

    new-instance v2, Lbgb;

    invoke-direct {v2, p0}, Lbgb;-><init>(Lbfw;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbfw;->al:Landroid/app/AlertDialog;

    :cond_3
    iget-object v0, p0, Lbfw;->al:Landroid/app/AlertDialog;

    iget-object v1, p0, Lbfw;->am:Lfok;

    iget-object v2, v1, Lfok;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    iget-object v2, v1, Lfok;->a:Lhfv;

    iget-object v2, v2, Lhfv;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfok;->b:Ljava/lang/CharSequence;

    :cond_4
    iget-object v1, v1, Lfok;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbfw;->al:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method static synthetic a(Lbfw;Lfon;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lbfw;->b(Lfon;)V

    return-void
.end method

.method static synthetic a(Lbfw;Z)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbfw;->e(Z)V

    return-void
.end method

.method static synthetic b(Lbfw;)V
    .locals 6

    .prologue
    .line 62
    iget-object v0, p0, Lbfw;->ai:Lcag;

    invoke-virtual {v0}, Lcag;->a()Lfom;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lbfw;->an:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lbfw;->e(Z)V

    iget-object v1, p0, Lbfw;->aj:Lbmm;

    iget-object v2, v0, Lfom;->a:Licn;

    iget-object v2, v2, Licn;->i:Ljava/lang/String;

    iget-wide v4, v0, Lfom;->i:J

    invoke-virtual {v1}, Lbmm;->a()V

    iput-object v2, v1, Lbmm;->c:Ljava/lang/String;

    iget-object v0, v1, Lbmm;->a:Lfgp;

    invoke-virtual {v0}, Lfgp;->a()Lfgs;

    move-result-object v0

    invoke-virtual {v0, v2}, Lfgs;->b(Ljava/lang/String;)Lfgs;

    move-result-object v0

    iput-wide v4, v0, Lfgs;->a:J

    sget-object v2, Lfhy;->a:[B

    invoke-virtual {v0, v2}, Lfgs;->a([B)V

    invoke-virtual {v1, v0}, Lbmm;->a(Lfgs;)V

    goto :goto_0
.end method

.method private b(Lfon;)V
    .locals 3

    .prologue
    .line 332
    iget-object v0, p0, Lbfw;->ak:Lbzu;

    if-nez v0, :cond_0

    .line 333
    new-instance v0, Lbzu;

    iget-object v1, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lbfw;->ae:Leyt;

    invoke-direct {v0, v1, v2}, Lbzu;-><init>(Landroid/app/Activity;Leyt;)V

    iput-object v0, p0, Lbfw;->ak:Lbzu;

    .line 335
    :cond_0
    iget-object v0, p0, Lbfw;->ak:Lbzu;

    invoke-virtual {v0, p1}, Lbzu;->a(Lfon;)V

    .line 336
    return-void
.end method

.method static synthetic c(Lbfw;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lbfw;->A()V

    return-void
.end method

.method static synthetic d(Lbfw;)Leyt;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbfw;->ae:Leyt;

    return-object v0
.end method

.method static synthetic e(Lbfw;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbfw;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v0
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 291
    iput-boolean p1, p0, Lbfw;->an:Z

    .line 292
    iget-object v0, p0, Lbfw;->ai:Lcag;

    if-nez p1, :cond_0

    invoke-virtual {v0}, Lcag;->b()V

    :cond_0
    iput-boolean p1, v0, Lcag;->l:Z

    .line 293
    if-eqz p1, :cond_1

    .line 294
    iget-object v0, p0, Lbfw;->ap:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 298
    :goto_0
    return-void

    .line 296
    :cond_1
    iget-object v0, p0, Lbfw;->ap:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    goto :goto_0
.end method

.method static synthetic f(Lbfw;)Lcag;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbfw;->ai:Lcag;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    .line 123
    invoke-super {p0, p1, p2, p3}, Li;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 125
    iget-object v0, p0, Lbfw;->ag:Lhog;

    if-nez v0, :cond_1

    .line 126
    if-nez p3, :cond_0

    invoke-virtual {p0}, Lbfw;->h()Landroid/os/Bundle;

    move-result-object p3

    .line 127
    :cond_0
    const-string v0, "navigation_endpoint"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 128
    invoke-static {v0}, Lfia;->a([B)Lhog;

    move-result-object v0

    iput-object v0, p0, Lbfw;->ag:Lhog;

    .line 131
    :cond_1
    const v0, 0x7f04014a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iput-object v0, p0, Lbfw;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    .line 132
    iget-object v0, p0, Lbfw;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lfve;)V

    .line 134
    new-instance v1, Lcag;

    iget-object v2, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lbfw;->ac:Leyp;

    iget-object v0, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const-string v4, "input_method"

    .line 137
    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v4, p0, Lbfw;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const v5, 0x7f080354

    .line 138
    invoke-virtual {v4, v5}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {v1, v2, v3, v0, v4}, Lcag;-><init>(Landroid/content/Context;Leyp;Landroid/view/inputmethod/InputMethodManager;Landroid/view/View;)V

    iput-object v1, p0, Lbfw;->ai:Lcag;

    .line 140
    iget-object v0, p0, Lbfw;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const v1, 0x7f080356

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbfw;->ao:Landroid/view/View;

    .line 141
    iget-object v0, p0, Lbfw;->ao:Landroid/view/View;

    new-instance v1, Lbfx;

    invoke-direct {v1, p0}, Lbfx;-><init>(Lbfw;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lbfw;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const v1, 0x7f080261

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iput-object v0, p0, Lbfw;->ap:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    .line 149
    iget-object v0, p0, Lbfw;->ap:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v1, Lbfy;

    invoke-direct {v1, p0}, Lbfy;-><init>(Lbfw;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    new-instance v0, Lbmm;

    iget-object v1, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lbfw;->ad:Lfgp;

    iget-object v3, p0, Lbfw;->aa:Lgix;

    iget-object v4, p0, Lbfw;->ab:Lfcd;

    iget-object v5, p0, Lbfw;->X:Letc;

    .line 161
    invoke-virtual {v5}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v5

    iget-object v6, p0, Lbfw;->ae:Leyt;

    invoke-direct/range {v0 .. v6}, Lbmm;-><init>(Lbhz;Lfgp;Lgix;Lfcd;Landroid/content/SharedPreferences;Leyt;)V

    iput-object v0, p0, Lbfw;->aj:Lbmm;

    .line 163
    iget-object v0, p0, Lbfw;->aj:Lbmm;

    iput-object p0, v0, Lbmm;->d:Lbmp;

    .line 165
    iget-object v0, p0, Lbfw;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1}, Li;->a(Landroid/app/Activity;)V

    .line 98
    check-cast p1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object p1, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 99
    iget-object v0, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 100
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v1

    iput-object v1, p0, Lbfw;->Y:Lari;

    .line 101
    iget-object v0, v0, Lckz;->a:Letc;

    iput-object v0, p0, Lbfw;->X:Letc;

    .line 102
    iget-object v0, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 103
    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    invoke-static {v0}, Lbbt;->a(Lbbp;)Lfhz;

    move-result-object v0

    iput-object v0, p0, Lbfw;->Z:Lfhz;

    .line 104
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0, p1}, Li;->a(Landroid/os/Bundle;)V

    .line 110
    iget-object v0, p0, Lbfw;->Y:Lari;

    invoke-virtual {v0}, Lari;->aD()Lcst;

    move-result-object v0

    iput-object v0, p0, Lbfw;->aa:Lgix;

    .line 111
    iget-object v0, p0, Lbfw;->Y:Lari;

    invoke-virtual {v0}, Lari;->az()Lfcd;

    move-result-object v0

    iput-object v0, p0, Lbfw;->ab:Lfcd;

    .line 112
    iget-object v0, p0, Lbfw;->Y:Lari;

    invoke-virtual {v0}, Lari;->c()Leyp;

    move-result-object v0

    iput-object v0, p0, Lbfw;->ac:Leyp;

    .line 113
    iget-object v0, p0, Lbfw;->Y:Lari;

    invoke-virtual {v0}, Lari;->E()Lfgp;

    move-result-object v0

    iput-object v0, p0, Lbfw;->ad:Lfgp;

    .line 114
    iget-object v0, p0, Lbfw;->Y:Lari;

    invoke-virtual {v0}, Lari;->ay()Leyt;

    move-result-object v0

    iput-object v0, p0, Lbfw;->ae:Leyt;

    .line 115
    iget-object v0, p0, Lbfw;->Y:Lari;

    invoke-virtual {v0}, Lari;->aO()Lcub;

    move-result-object v0

    iput-object v0, p0, Lbfw;->af:Lcub;

    .line 117
    const/4 v0, 0x2

    const v1, 0x7f0d012e

    invoke-virtual {p0, v0, v1}, Lbfw;->a(II)V

    .line 118
    return-void
.end method

.method public final a(Lfgq;)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lbfw;->ai:Lcag;

    invoke-virtual {v0}, Lcag;->a()Lfom;

    move-result-object v1

    .line 208
    invoke-virtual {v1}, Lfom;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lfom;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v1}, Lfom;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v1, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->g:Lhdd;

    iget-object v0, v0, Lhdd;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->g:Lhdd;

    iget-object v0, v0, Lhdd;->a:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1, v0}, Lfgq;->d(Ljava/lang/String;)Lfgq;

    .line 210
    iget-object v0, v1, Lfom;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lfgq;->e(Ljava/lang/String;)Lfgq;

    .line 212
    :cond_0
    return-void

    .line 209
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final a(Lfnz;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 216
    invoke-direct {p0, v6}, Lbfw;->e(Z)V

    .line 217
    invoke-virtual {p1}, Lfnz;->b()Lfol;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 218
    invoke-virtual {p1}, Lfnz;->b()Lfol;

    move-result-object v0

    invoke-virtual {v0}, Lfol;->b()Lfog;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 220
    invoke-virtual {p1}, Lfnz;->b()Lfol;

    move-result-object v0

    invoke-virtual {v0}, Lfol;->b()Lfog;

    move-result-object v1

    .line 219
    iget-object v0, p0, Lbfw;->aq:Landroid/app/AlertDialog;

    if-nez v0, :cond_1

    iget-object v0, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v2, 0x7f040147

    invoke-static {v0, v2, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f08034d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v3, Lfvi;

    iget-object v4, p0, Lbfw;->ac:Leyp;

    invoke-direct {v3, v4, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v3, p0, Lbfw;->av:Lfvi;

    const v0, 0x7f08034b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbfw;->ar:Landroid/widget/TextView;

    const v0, 0x7f08034c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbfw;->as:Landroid/widget/TextView;

    const v0, 0x7f08034f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbfw;->at:Landroid/widget/TextView;

    const v0, 0x7f080350

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbfw;->au:Landroid/widget/TextView;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v3, v1, Lfog;->d:Ljava/lang/CharSequence;

    if-nez v3, :cond_0

    iget-object v3, v1, Lfog;->a:Lich;

    iget-object v3, v3, Lich;->f:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v1, Lfog;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v3, v1, Lfog;->d:Ljava/lang/CharSequence;

    new-instance v4, Lbgc;

    invoke-direct {v4, p0}, Lbgc;-><init>(Lbfw;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbfw;->aq:Landroid/app/AlertDialog;

    :cond_1
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lfog;->a()Lfnc;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbfw;->av:Lfvi;

    invoke-virtual {v1}, Lfog;->a()Lfnc;

    move-result-object v2

    invoke-virtual {v0, v2, v5}, Lfvi;->a(Lfnc;Leyo;)V

    :cond_2
    iget-object v0, p0, Lbfw;->ar:Landroid/widget/TextView;

    iget-object v2, v1, Lfog;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_3

    iget-object v2, v1, Lfog;->a:Lich;

    iget-object v2, v2, Lich;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfog;->b:Ljava/lang/CharSequence;

    :cond_3
    iget-object v2, v1, Lfog;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbfw;->as:Landroid/widget/TextView;

    iget-object v2, v1, Lfog;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    iget-object v2, v1, Lfog;->a:Lich;

    iget-object v2, v2, Lich;->b:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfog;->c:Ljava/lang/CharSequence;

    :cond_4
    iget-object v2, v1, Lfog;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbfw;->at:Landroid/widget/TextView;

    iget-object v2, v1, Lfog;->e:Ljava/lang/CharSequence;

    if-nez v2, :cond_5

    iget-object v2, v1, Lfog;->a:Lich;

    iget-object v2, v2, Lich;->d:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfog;->e:Ljava/lang/CharSequence;

    :cond_5
    iget-object v2, v1, Lfog;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbfw;->au:Landroid/widget/TextView;

    iget-object v2, v1, Lfog;->f:Ljava/lang/CharSequence;

    if-nez v2, :cond_6

    iget-object v2, v1, Lfog;->a:Lich;

    iget-object v2, v2, Lich;->e:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfog;->f:Ljava/lang/CharSequence;

    :cond_6
    iget-object v1, v1, Lfog;->f:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbfw;->aq:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 225
    :cond_7
    :goto_0
    invoke-virtual {p1}, Lfnz;->b()Lfol;

    move-result-object v0

    iget-object v1, v0, Lfol;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_8

    iget-object v1, v0, Lfol;->a:Licl;

    iget-object v1, v1, Licl;->a:Lhgz;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lfol;->a:Licl;

    iget-object v1, v1, Licl;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, v0, Lfol;->b:Ljava/lang/CharSequence;

    :cond_8
    iget-object v0, v0, Lfol;->b:Ljava/lang/CharSequence;

    .line 226
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 227
    iget-object v1, p0, Lbfw;->ae:Leyt;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Leyt;->a(Ljava/lang/String;)V

    .line 230
    :cond_9
    invoke-virtual {p0}, Lbfw;->a()V

    .line 231
    return-void

    .line 221
    :cond_a
    invoke-virtual {p1}, Lfnz;->b()Lfol;

    move-result-object v0

    invoke-virtual {v0}, Lfol;->a()Lhog;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 222
    iget-object v0, p0, Lbfw;->Z:Lfhz;

    .line 223
    invoke-virtual {p1}, Lfnz;->b()Lfol;

    move-result-object v1

    invoke-virtual {v1}, Lfol;->a()Lhog;

    move-result-object v1

    .line 222
    invoke-interface {v0, v1, v5}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lfon;)V
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbfw;->e(Z)V

    .line 236
    invoke-direct {p0, p1}, Lbfw;->b(Lfon;)V

    .line 237
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 170
    invoke-super {p0}, Li;->e()V

    .line 171
    iget-object v0, p0, Lbfw;->aa:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lbfw;->c()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 174
    iget-object v0, p0, Lbfw;->af:Lcub;

    iget-object v1, p0, Lbfw;->W:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lbfz;

    invoke-direct {v2, p0}, Lbfz;-><init>(Lbfw;)V

    invoke-virtual {v0, v1, v2}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    .line 195
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-direct {p0}, Lbfw;->A()V

    goto :goto_0
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0}, Lbfw;->A()V

    .line 203
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbfw;->e(Z)V

    .line 242
    return-void
.end method
