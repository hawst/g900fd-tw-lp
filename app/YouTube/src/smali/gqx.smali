.class final Lgqx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private a:Lye;

.field private b:Ljava/util/LinkedList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/io/Closeable;

    invoke-static {v0}, La;->d([Ljava/lang/Object;)Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lgqx;->b:Ljava/util/LinkedList;

    .line 465
    return-void
.end method

.method public varargs constructor <init>(Lye;[Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/io/Closeable;

    invoke-static {v0}, La;->d([Ljava/lang/Object;)Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lgqx;->b:Ljava/util/LinkedList;

    .line 468
    invoke-virtual {p0, p1}, Lgqx;->a(Lye;)V

    .line 469
    invoke-virtual {p0, p2}, Lgqx;->a([Ljava/io/Closeable;)V

    .line 470
    return-void
.end method


# virtual methods
.method public final a()Lye;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lgqx;->a:Lye;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    iget-object v0, p0, Lgqx;->a:Lye;

    return-object v0
.end method

.method public final a(Lye;)V
    .locals 1

    .prologue
    .line 473
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye;

    iput-object v0, p0, Lgqx;->a:Lye;

    .line 474
    return-void
.end method

.method public final varargs a([Ljava/io/Closeable;)V
    .locals 4

    .prologue
    .line 482
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 483
    iget-object v3, p0, Lgqx;->b:Ljava/util/LinkedList;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 482
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 485
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lgqx;->a:Lye;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 494
    :goto_0
    iget-object v0, p0, Lgqx;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 495
    iget-object v0, p0, Lgqx;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    .line 497
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 498
    :catch_0
    move-exception v0

    .line 499
    const-string v1, "Exception while closing resource"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 503
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lgqx;->a:Lye;

    .line 504
    return-void
.end method
