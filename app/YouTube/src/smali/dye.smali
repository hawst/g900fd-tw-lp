.class public final Ldye;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldyd;


# instance fields
.field private final a:Lgix;

.field private final b:Lfbu;

.field private final c:Levn;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Ldyg;


# direct methods
.method public constructor <init>(Lgix;Lfbu;Levn;Ljava/util/concurrent/Executor;Ldyg;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Ldye;->a:Lgix;

    .line 36
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbu;

    iput-object v0, p0, Ldye;->b:Lfbu;

    .line 37
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldye;->c:Levn;

    .line 38
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Ldye;->d:Ljava/util/concurrent/Executor;

    .line 39
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldye;->e:Ldyg;

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Leuc;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-virtual {p0}, Ldye;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Ldye;->e:Ldyg;

    const-string v1, "Can not get user auth token, not log in"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 98
    invoke-interface {p1, v2, v2}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 107
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Ldye;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Ldyf;

    invoke-direct {v1, p0, p1}, Ldyf;-><init>(Ldye;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldye;->a:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Ldye;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Ldye;->e:Ldyg;

    const-string v1, "Can not get user name, not log in"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 51
    const-string v0, ""

    .line 53
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldye;->a:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-virtual {p0}, Ldye;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Ldye;->e:Ldyg;

    const-string v2, "Can not get user auth token, not log in"

    invoke-virtual {v0, v2}, Ldyg;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 83
    :goto_0
    return-object v0

    .line 73
    :cond_0
    :try_start_0
    iget-object v0, p0, Ldye;->b:Lfbu;

    iget-object v2, p0, Ldye;->a:Lgix;

    .line 75
    invoke-interface {v2}, Lgix;->d()Lgit;

    move-result-object v2

    iget-object v2, v2, Lgit;->b:Lgiv;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lfbu;->a(Lgiv;Z)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 76
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbw;

    .line 77
    invoke-virtual {v0}, Lfbw;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lfbw;->a()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot call getAuthenticationToken on an unsuccessful fetch."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :catch_0
    move-exception v0

    :goto_1
    move-object v0, v1

    .line 83
    goto :goto_0

    .line 77
    :cond_1
    iget-object v0, v0, Lfbw;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Ldye;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldye;->a:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onSignInEvent(Lfcb;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Ldye;->c:Levn;

    sget-object v1, Ldyc;->a:Ldyc;

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 59
    return-void
.end method

.method public final onSignOutEvent(Lfcc;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Ldye;->c:Levn;

    sget-object v1, Ldyc;->a:Ldyc;

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 64
    return-void
.end method
