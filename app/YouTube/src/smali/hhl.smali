.class public final Lhhl;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhpn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61256
    invoke-direct {p0}, Lidf;-><init>()V

    .line 61259
    const/4 v0, 0x0

    iput-object v0, p0, Lhhl;->a:Lhpn;

    .line 61256
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 61296
    const/4 v0, 0x0

    .line 61297
    iget-object v1, p0, Lhhl;->a:Lhpn;

    if-eqz v1, :cond_0

    .line 61298
    const v0, 0x4be64ce

    iget-object v1, p0, Lhhl;->a:Lhpn;

    .line 61299
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 61301
    :cond_0
    iget-object v1, p0, Lhhl;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61302
    iput v0, p0, Lhhl;->J:I

    .line 61303
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 61252
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhhl;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhhl;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhhl;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhhl;->a:Lhpn;

    if-nez v0, :cond_2

    new-instance v0, Lhpn;

    invoke-direct {v0}, Lhpn;-><init>()V

    iput-object v0, p0, Lhhl;->a:Lhpn;

    :cond_2
    iget-object v0, p0, Lhhl;->a:Lhpn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x25f32672 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 61287
    iget-object v0, p0, Lhhl;->a:Lhpn;

    if-eqz v0, :cond_0

    .line 61288
    const v0, 0x4be64ce

    iget-object v1, p0, Lhhl;->a:Lhpn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 61290
    :cond_0
    iget-object v0, p0, Lhhl;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 61292
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 61270
    if-ne p1, p0, :cond_1

    .line 61274
    :cond_0
    :goto_0
    return v0

    .line 61271
    :cond_1
    instance-of v2, p1, Lhhl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 61272
    :cond_2
    check-cast p1, Lhhl;

    .line 61273
    iget-object v2, p0, Lhhl;->a:Lhpn;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhhl;->a:Lhpn;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhhl;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhhl;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 61274
    goto :goto_0

    .line 61273
    :cond_4
    iget-object v2, p0, Lhhl;->a:Lhpn;

    iget-object v3, p1, Lhhl;->a:Lhpn;

    invoke-virtual {v2, v3}, Lhpn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhhl;->I:Ljava/util/List;

    iget-object v3, p1, Lhhl;->I:Ljava/util/List;

    .line 61274
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 61278
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 61280
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhl;->a:Lhpn;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 61281
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhhl;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 61282
    return v0

    .line 61280
    :cond_0
    iget-object v0, p0, Lhhl;->a:Lhpn;

    invoke-virtual {v0}, Lhpn;->hashCode()I

    move-result v0

    goto :goto_0

    .line 61281
    :cond_1
    iget-object v1, p0, Lhhl;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
