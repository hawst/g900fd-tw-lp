.class public final enum Ldfc;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldfc;

.field public static final enum b:Ldfc;

.field public static final enum c:Ldfc;

.field public static final enum d:Ldfc;

.field public static final enum e:Ldfc;

.field private static h:[Ljava/lang/String;

.field private static i:[Ljava/lang/String;

.field private static final synthetic j:[Ldfc;


# instance fields
.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 200
    new-instance v0, Ldfc;

    const-string v1, "WHITE_ON_BLACK"

    const v2, 0x7f090153

    invoke-direct {v0, v1, v3, v2, v3}, Ldfc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldfc;->a:Ldfc;

    .line 201
    new-instance v0, Ldfc;

    const-string v1, "BLACK_ON_WHITE"

    const v2, 0x7f090154

    invoke-direct {v0, v1, v4, v2, v4}, Ldfc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldfc;->b:Ldfc;

    .line 202
    new-instance v0, Ldfc;

    const-string v1, "YELLOW_ON_BLACK"

    const v2, 0x7f090155

    invoke-direct {v0, v1, v5, v2, v5}, Ldfc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldfc;->c:Ldfc;

    .line 203
    new-instance v0, Ldfc;

    const-string v1, "YELLOW_ON_BLUE"

    const v2, 0x7f090156

    invoke-direct {v0, v1, v6, v2, v6}, Ldfc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldfc;->d:Ldfc;

    .line 204
    new-instance v0, Ldfc;

    const-string v1, "CUSTOM"

    const v2, 0x7f090157

    invoke-direct {v0, v1, v7, v2, v7}, Ldfc;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldfc;->e:Ldfc;

    .line 199
    const/4 v0, 0x5

    new-array v0, v0, [Ldfc;

    sget-object v1, Ldfc;->a:Ldfc;

    aput-object v1, v0, v3

    sget-object v1, Ldfc;->b:Ldfc;

    aput-object v1, v0, v4

    sget-object v1, Ldfc;->c:Ldfc;

    aput-object v1, v0, v5

    sget-object v1, Ldfc;->d:Ldfc;

    aput-object v1, v0, v6

    sget-object v1, Ldfc;->e:Ldfc;

    aput-object v1, v0, v7

    sput-object v0, Ldfc;->j:[Ldfc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 212
    iput p3, p0, Ldfc;->f:I

    .line 213
    iput p4, p0, Ldfc;->g:I

    .line 214
    return-void
.end method

.method static synthetic a(Ldfc;)I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Ldfc;->g:I

    return v0
.end method

.method public static a()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 230
    sget-object v0, Ldfc;->i:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 231
    invoke-static {}, Ldfc;->values()[Ldfc;

    move-result-object v1

    .line 232
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Ldfc;->i:[Ljava/lang/String;

    .line 233
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 234
    sget-object v2, Ldfc;->i:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Ldfc;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 233
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :cond_0
    sget-object v0, Ldfc;->i:[Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 219
    sget-object v0, Ldfc;->h:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 220
    invoke-static {}, Ldfc;->values()[Ldfc;

    move-result-object v1

    .line 221
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Ldfc;->h:[Ljava/lang/String;

    .line 222
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 223
    sget-object v2, Ldfc;->h:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Ldfc;->f:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 222
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_0
    sget-object v0, Ldfc;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public static b()I
    .locals 2

    .prologue
    .line 241
    invoke-static {}, Ldfc;->values()[Ldfc;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v0, v0, Ldfc;->g:I

    return v0
.end method

.method public static c()I
    .locals 2

    .prologue
    .line 245
    invoke-static {}, Ldfc;->values()[Ldfc;

    move-result-object v0

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget v0, v0, Ldfc;->g:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldfc;
    .locals 1

    .prologue
    .line 199
    const-class v0, Ldfc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldfc;

    return-object v0
.end method

.method public static values()[Ldfc;
    .locals 1

    .prologue
    .line 199
    sget-object v0, Ldfc;->j:[Ldfc;

    invoke-virtual {v0}, [Ldfc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldfc;

    return-object v0
.end method
