.class public final Lgfa;
.super Lgee;
.source "SourceFile"

# interfaces
.implements Lgec;


# instance fields
.field public a:Lgec;

.field private b:Z

.field private c:Z

.field private d:Lged;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lgee;-><init>(Landroid/content/Context;)V

    .line 47
    iput p2, p0, Lgfa;->e:I

    .line 48
    return-void
.end method

.method private c(I)Lgec;
    .locals 2

    .prologue
    .line 248
    packed-switch p1, :pswitch_data_0

    .line 258
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Requested view is not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :pswitch_1
    new-instance v0, Lgem;

    invoke-virtual {p0}, Lgfa;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgem;-><init>(Landroid/content/Context;)V

    .line 255
    :goto_0
    return-object v0

    .line 253
    :pswitch_2
    new-instance v0, Lgen;

    invoke-virtual {p0}, Lgfa;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgen;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 255
    :pswitch_3
    new-instance v0, Lgeu;

    invoke-virtual {p0}, Lgfa;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgeu;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    const-string v1, "MediaView method called before surface created"

    invoke-static {v0, v1}, Lb;->d(ZLjava/lang/Object;)V

    .line 125
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->a()I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgfa;->c:Z

    .line 156
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0, p1}, Lgec;->a(I)V

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgfa;->c:Z

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    const-string v1, "MediaView method called before surface created"

    invoke-static {v0, v1}, Lb;->d(ZLjava/lang/Object;)V

    .line 119
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0, p1, p2}, Lgec;->a(II)V

    .line 120
    return-void
.end method

.method public final a(Lged;)V
    .locals 1

    .prologue
    .line 92
    iput-object p1, p0, Lgfa;->d:Lged;

    .line 93
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgfa;->b:Z

    .line 95
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0, p1}, Lgec;->a(Lged;)V

    .line 99
    :goto_0
    return-void

    .line 97
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgfa;->b:Z

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    const-string v1, "MediaView method called before surface created"

    invoke-static {v0, v1}, Lb;->d(ZLjava/lang/Object;)V

    .line 131
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->b()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 211
    iget-object v0, p0, Lgfa;->d:Lged;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget v0, p0, Lgfa;->e:I

    if-ne p1, v0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    iget v0, p0, Lgfa;->e:I

    if-eqz v0, :cond_2

    iget v0, p0, Lgfa;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 221
    const-string v0, "Sticking to SurfaceView for subsequent playbacks"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 222
    const/4 p1, 0x3

    .line 225
    :cond_2
    iput p1, p0, Lgfa;->e:I

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x19

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "setMediaView: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lgfa;->a:Lgec;

    .line 231
    invoke-direct {p0, p1}, Lgfa;->c(I)Lgec;

    move-result-object v1

    iput-object v1, p0, Lgfa;->a:Lgec;

    .line 233
    iget-object v1, p0, Lgfa;->a:Lgec;

    iget-object v2, p0, Lgfa;->d:Lged;

    invoke-interface {v1, v2}, Lgec;->a(Lged;)V

    .line 234
    iget-object v1, p0, Lgfa;->a:Lgec;

    invoke-interface {v1}, Lgec;->j()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgfa;->addView(Landroid/view/View;)V

    .line 235
    if-eqz v0, :cond_0

    .line 236
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lgec;->a(Lged;)V

    .line 237
    invoke-interface {v0}, Lgec;->j()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgfa;->removeView(Landroid/view/View;)V

    .line 238
    invoke-interface {v0}, Lgec;->g()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->c()V

    .line 167
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgfa;->c:Z

    .line 168
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->d()V

    .line 180
    :cond_0
    return-void
.end method

.method public final e()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->e()Landroid/view/Surface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->f()Landroid/view/SurfaceHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 184
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->g()V

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lgfa;->a:Lgec;

    .line 188
    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lgfa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lgfa;->a:Lgec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->i()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Landroid/view/View;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lgfa;->a:Lgec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->j()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lgfa;->a:Lgec;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-super {p0}, Lgee;->onAttachedToWindow()V

    .line 54
    iget-object v0, p0, Lgfa;->a:Lgec;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->j()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgfa;->removeView(Landroid/view/View;)V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lgfa;->a:Lgec;

    .line 59
    :cond_0
    iget v0, p0, Lgfa;->e:I

    invoke-direct {p0, v0}, Lgfa;->c(I)Lgec;

    move-result-object v0

    iput-object v0, p0, Lgfa;->a:Lgec;

    .line 60
    iget-object v0, p0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->j()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgfa;->addView(Landroid/view/View;)V

    .line 62
    iget-boolean v0, p0, Lgfa;->b:Z

    if-eqz v0, :cond_1

    .line 63
    iput-boolean v2, p0, Lgfa;->b:Z

    .line 64
    iget-object v0, p0, Lgfa;->a:Lgec;

    iget-object v1, p0, Lgfa;->d:Lged;

    invoke-interface {v0, v1}, Lgec;->a(Lged;)V

    .line 65
    iget-boolean v0, p0, Lgfa;->c:Z

    if-eqz v0, :cond_1

    .line 66
    invoke-virtual {p0, v2}, Lgfa;->a(I)V

    .line 69
    :cond_1
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 84
    invoke-virtual {p0}, Lgfa;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 85
    invoke-virtual {p0, v3}, Lgfa;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 88
    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-virtual {p0}, Lgfa;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 74
    invoke-virtual {p0, v1}, Lgfa;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 75
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 76
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lgfa;->setMeasuredDimension(II)V

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-virtual {p0, v1, v1}, Lgfa;->setMeasuredDimension(II)V

    goto :goto_0
.end method
