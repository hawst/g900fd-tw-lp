.class public final enum Lfpa;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfpa;

.field public static final enum b:Lfpa;

.field public static final enum c:Lfpa;

.field private static final synthetic e:[Lfpa;


# instance fields
.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 460
    new-instance v0, Lfpa;

    const-string v1, "NONE"

    const-string v2, "0"

    invoke-direct {v0, v1, v3, v2}, Lfpa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfpa;->a:Lfpa;

    .line 461
    new-instance v0, Lfpa;

    const-string v1, "SKIPPABLE"

    const-string v2, "1"

    invoke-direct {v0, v1, v4, v2}, Lfpa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfpa;->b:Lfpa;

    .line 462
    new-instance v0, Lfpa;

    const-string v1, "SURVEY"

    const-string v2, "3"

    invoke-direct {v0, v1, v5, v2}, Lfpa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfpa;->c:Lfpa;

    .line 459
    const/4 v0, 0x3

    new-array v0, v0, [Lfpa;

    sget-object v1, Lfpa;->a:Lfpa;

    aput-object v1, v0, v3

    sget-object v1, Lfpa;->b:Lfpa;

    aput-object v1, v0, v4

    sget-object v1, Lfpa;->c:Lfpa;

    aput-object v1, v0, v5

    sput-object v0, Lfpa;->e:[Lfpa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 466
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 467
    iput-object p3, p0, Lfpa;->d:Ljava/lang/String;

    .line 468
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfpa;
    .locals 1

    .prologue
    .line 459
    const-class v0, Lfpa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfpa;

    return-object v0
.end method

.method public static values()[Lfpa;
    .locals 1

    .prologue
    .line 459
    sget-object v0, Lfpa;->e:[Lfpa;

    invoke-virtual {v0}, [Lfpa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfpa;

    return-object v0
.end method
