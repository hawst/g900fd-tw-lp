.class final Ldmx;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private synthetic a:Ldmu;


# direct methods
.method public constructor <init>(Ldmu;)V
    .locals 1

    .prologue
    .line 393
    iput-object p1, p0, Ldmx;->a:Ldmu;

    .line 394
    invoke-virtual {p1}, Ldmu;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 395
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 399
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 402
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    .line 403
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1, v0}, Ldmu;->a(Ldmu;Ljava/util/Map;)Ljava/util/Map;

    .line 404
    iget-object v0, p0, Ldmx;->a:Ldmu;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ldmu;->a(Ldmu;Z)Z

    .line 405
    iget-object v0, p0, Ldmx;->a:Ldmu;

    invoke-static {v0}, Ldmu;->a(Ldmu;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    .line 406
    invoke-interface {v0}, Ldmw;->a()V

    goto :goto_1

    .line 412
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lgjm;

    .line 413
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->b(Ldmu;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->a(Ldmu;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldmw;

    .line 415
    invoke-interface {v1, v0}, Ldmw;->a(Lgjm;)V

    goto :goto_2

    .line 421
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lgjm;

    .line 422
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->b(Ldmu;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->a(Ldmu;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldmw;

    .line 424
    invoke-interface {v1, v0}, Ldmw;->e(Lgjm;)V

    goto :goto_3

    .line 430
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lgjm;

    .line 431
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->b(Ldmu;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->a(Ldmu;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldmw;

    .line 433
    invoke-interface {v1, v0}, Ldmw;->b(Lgjm;)V

    goto :goto_4

    .line 439
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lgjm;

    .line 440
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->b(Ldmu;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->a(Ldmu;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldmw;

    .line 442
    invoke-interface {v1, v0}, Ldmw;->c(Lgjm;)V

    goto :goto_5

    .line 448
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lgjm;

    .line 449
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->b(Ldmu;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->a(Ldmu;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldmw;

    .line 451
    invoke-interface {v1, v0}, Ldmw;->d(Lgjm;)V

    goto :goto_6

    .line 457
    :pswitch_6
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 458
    iget-object v1, p0, Ldmx;->a:Ldmu;

    invoke-static {v1}, Ldmu;->c(Ldmu;)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 462
    iget-object v0, p0, Ldmx;->a:Ldmu;

    invoke-static {v0}, Ldmu;->a(Ldmu;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    .line 463
    invoke-interface {v0}, Ldmw;->b()V

    goto :goto_7

    .line 465
    :cond_1
    iget-object v0, p0, Ldmx;->a:Ldmu;

    invoke-virtual {v0}, Ldmu;->stopSelf()V

    goto/16 :goto_0

    .line 399
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
