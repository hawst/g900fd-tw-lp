.class public final enum Lbnd;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lbne;


# static fields
.field public static final enum a:Lbnd;

.field private static enum c:Lbnd;

.field private static enum d:Lbnd;

.field private static final synthetic f:[Lbnd;


# instance fields
.field public final b:Lffk;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12
    new-instance v0, Lbnd;

    const-string v1, "ANY"

    sget-object v2, Lffk;->a:Lffk;

    const v3, 0x7f09028d

    invoke-direct {v0, v1, v4, v2, v3}, Lbnd;-><init>(Ljava/lang/String;ILffk;I)V

    sput-object v0, Lbnd;->a:Lbnd;

    .line 13
    new-instance v0, Lbnd;

    const-string v1, "SHORT"

    sget-object v2, Lffk;->b:Lffk;

    const v3, 0x7f09028f

    invoke-direct {v0, v1, v5, v2, v3}, Lbnd;-><init>(Ljava/lang/String;ILffk;I)V

    sput-object v0, Lbnd;->c:Lbnd;

    .line 14
    new-instance v0, Lbnd;

    const-string v1, "LONG"

    sget-object v2, Lffk;->c:Lffk;

    const v3, 0x7f09028e

    invoke-direct {v0, v1, v6, v2, v3}, Lbnd;-><init>(Ljava/lang/String;ILffk;I)V

    sput-object v0, Lbnd;->d:Lbnd;

    .line 10
    const/4 v0, 0x3

    new-array v0, v0, [Lbnd;

    sget-object v1, Lbnd;->a:Lbnd;

    aput-object v1, v0, v4

    sget-object v1, Lbnd;->c:Lbnd;

    aput-object v1, v0, v5

    sget-object v1, Lbnd;->d:Lbnd;

    aput-object v1, v0, v6

    sput-object v0, Lbnd;->f:[Lbnd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILffk;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput-object p3, p0, Lbnd;->b:Lffk;

    .line 21
    iput p4, p0, Lbnd;->e:I

    .line 22
    return-void
.end method

.method public static a(I)Lbnd;
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Lbnd;->values()[Lbnd;

    move-result-object v0

    .line 47
    if-ltz p0, :cond_0

    array-length v1, v0

    if-ge p0, v1, :cond_0

    .line 48
    aget-object v0, v0, p0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lbnd;->a:Lbnd;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lbnd;
    .locals 3

    .prologue
    .line 34
    if-nez p0, :cond_0

    .line 35
    sget-object v0, Lbnd;->a:Lbnd;

    .line 41
    :goto_0
    return-object v0

    .line 38
    :cond_0
    :try_start_0
    invoke-static {p0}, Lbnd;->valueOf(Ljava/lang/String;)Lbnd;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 40
    :catch_0
    move-exception v0

    const-string v1, "Attempted to search with unsupported SearchDurationType: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 41
    sget-object v0, Lbnd;->a:Lbnd;

    goto :goto_0

    .line 40
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lbnd;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lbnd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbnd;

    return-object v0
.end method

.method public static values()[Lbnd;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lbnd;->f:[Lbnd;

    invoke-virtual {v0}, [Lbnd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbnd;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lbnd;->e:I

    return v0
.end method
