.class public final Lgpy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lfsi;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lfhz;Lfdw;Lfrz;Li;I)V
    .locals 3

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgpy;->a:Landroid/view/ViewGroup;

    .line 39
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    new-instance v0, Lfsi;

    invoke-direct {v0}, Lfsi;-><init>()V

    iput-object v0, p0, Lgpy;->b:Lfsi;

    .line 45
    iget-object v0, p0, Lgpy;->b:Lfsi;

    const-class v1, Lfmp;

    new-instance v2, Lgpx;

    invoke-direct {v2, p2, p3, p4, p5}, Lgpx;-><init>(Lfhz;Lfdw;Lfrz;Li;)V

    iget-object v0, v0, Lfsi;->c:Lfsl;

    invoke-virtual {v0, v1, v2}, Lfsl;->b(Ljava/lang/Class;Lfsk;)V

    .line 53
    iput p6, p0, Lgpy;->c:I

    .line 54
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lgpy;->b:Lfsi;

    invoke-virtual {v0}, Lfsi;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 73
    iget-object v0, p0, Lgpy;->a:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 93
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 77
    :goto_0
    iget-object v0, p0, Lgpy;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 78
    iget-object v0, p0, Lgpy;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 79
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 77
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 81
    :cond_2
    iget-object v0, p0, Lgpy;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 84
    iget-object v0, p0, Lgpy;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    move v1, v2

    move-object v0, v3

    .line 85
    :goto_1
    iget-object v5, p0, Lgpy;->b:Lfsi;

    invoke-virtual {v5}, Lfsi;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 86
    iget v5, p0, Lgpy;->c:I

    rem-int v5, v1, v5

    if-nez v5, :cond_3

    .line 87
    const v0, 0x7f040104

    iget-object v5, p0, Lgpy;->a:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 88
    iget v5, p0, Lgpy;->c:I

    int-to-float v5, v5

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    .line 89
    iget-object v5, p0, Lgpy;->a:Landroid/view/ViewGroup;

    invoke-virtual {v5, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 91
    :cond_3
    iget-object v5, p0, Lgpy;->b:Lfsi;

    invoke-virtual {v5, v1, v3, v0}, Lfsi;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 85
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 63
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 64
    iget v0, p0, Lgpy;->c:I

    if-ne v0, p1, :cond_1

    .line 69
    :goto_1
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_1
    iput p1, p0, Lgpy;->c:I

    .line 68
    invoke-direct {p0}, Lgpy;->a()V

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lgpy;->b:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 58
    iget-object v0, p0, Lgpy;->b:Lfsi;

    invoke-virtual {v0, p1}, Lfsi;->a(Ljava/util/Collection;)V

    .line 59
    invoke-direct {p0}, Lgpy;->a()V

    .line 60
    return-void
.end method
