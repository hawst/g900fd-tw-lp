.class public final Lcmx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J


# instance fields
.field private final b:Lcnq;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Lcvl;

.field private final e:Lcxl;

.field private final f:Lcnu;

.field private final g:Lcnm;

.field private final h:Lgoc;

.field private final i:J

.field private final j:Ljava/lang/String;

.field private final k:Less;

.field private final l:Ljava/util/List;

.field private final m:Lfqx;

.field private final n:Levn;

.field private volatile o:Lcxk;

.field private volatile p:I

.field private volatile q:Leud;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 51
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcmx;->a:J

    return-void
.end method

.method private constructor <init>(Lcnq;Ljava/util/concurrent/Executor;Lcvl;Lcxl;Lcnu;Lcnm;Lgoc;JLess;Lfqx;Levn;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnq;

    iput-object v0, p0, Lcmx;->b:Lcnq;

    .line 138
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcmx;->c:Ljava/util/concurrent/Executor;

    .line 139
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvl;

    iput-object v0, p0, Lcmx;->d:Lcvl;

    .line 140
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxl;

    iput-object v0, p0, Lcmx;->e:Lcxl;

    .line 141
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnu;

    iput-object v0, p0, Lcmx;->f:Lcnu;

    .line 142
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnm;

    iput-object v0, p0, Lcmx;->g:Lcnm;

    .line 143
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoc;

    iput-object v0, p0, Lcmx;->h:Lgoc;

    .line 144
    iput-wide p8, p0, Lcmx;->i:J

    .line 145
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Less;

    iput-object v0, p0, Lcmx;->k:Less;

    .line 146
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqx;

    iput-object v0, p0, Lcmx;->m:Lfqx;

    .line 147
    invoke-static {p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcmx;->n:Levn;

    .line 148
    invoke-static {p13}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcmx;->j:Ljava/lang/String;

    .line 149
    iget-object v0, p10, Less;->l:Ljava/util/List;

    invoke-static {v0}, Lcmx;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcmx;->l:Ljava/util/List;

    .line 150
    const/4 v0, 0x1

    iput v0, p0, Lcmx;->p:I

    .line 151
    sget-object v0, Lcyx;->a:Lcyx;

    invoke-virtual {p12, v0}, Levn;->d(Ljava/lang/Object;)V

    .line 155
    invoke-direct {p0}, Lcmx;->a()V

    .line 156
    return-void
.end method

.method public static a(Lcnq;Ljava/util/concurrent/Executor;Lcvl;Lcxl;Lcnu;Lcnm;Lgoc;JLesq;Lfqx;Levn;Ljava/lang/String;)Lcmx;
    .locals 17

    .prologue
    .line 102
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    move-object/from16 v0, p9

    iget-object v2, v0, Lesq;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Less;

    iget-object v4, v2, Less;->a:Lesh;

    iget-object v4, v4, Lesh;->c:Lesj;

    sget-object v5, Lesj;->b:Lesj;

    if-ne v4, v5, :cond_0

    iget-object v4, v2, Less;->a:Lesh;

    iget-object v4, v4, Lesh;->a:Lesl;

    sget-object v5, Lesl;->e:Lesl;

    if-ne v4, v5, :cond_0

    iget-object v4, v2, Less;->a:Lesh;

    iget-wide v4, v4, Lesh;->b:J

    const-wide/16 v6, 0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    move-object v12, v2

    .line 104
    :goto_0
    if-eqz v12, :cond_1

    .line 105
    iget-object v2, v12, Less;->l:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 106
    iget-object v2, v12, Less;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 107
    :cond_1
    const/4 v2, 0x0

    .line 109
    :goto_1
    return-object v2

    .line 103
    :cond_2
    const/4 v12, 0x0

    goto :goto_0

    .line 109
    :cond_3
    new-instance v2, Lcmx;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, p7

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    invoke-direct/range {v2 .. v15}, Lcmx;-><init>(Lcnq;Ljava/util/concurrent/Executor;Lcvl;Lcxl;Lcnu;Lcnm;Lgoc;JLess;Lfqx;Levn;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcmx;Leud;)Lesf;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcmx;->a(Leud;)Lesf;

    move-result-object v0

    return-object v0
.end method

.method private a(Leud;)Lesf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 445
    :try_start_0
    invoke-virtual {p1}, Leud;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesf;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 453
    if-nez v0, :cond_1

    move-object v0, v1

    .line 470
    :cond_0
    :goto_0
    return-object v0

    .line 448
    :catch_0
    move-exception v0

    .line 450
    const-string v2, "Error loading midroll ad"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 451
    goto :goto_0

    .line 456
    :cond_1
    iget-object v2, v0, Lesf;->b:Lfoy;

    .line 457
    if-nez v2, :cond_2

    move-object v0, v1

    .line 458
    goto :goto_0

    .line 460
    :cond_2
    invoke-virtual {v2}, Lfoy;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 462
    iget-object v2, p0, Lcmx;->g:Lcnm;

    .line 463
    iget-object v0, v0, Lesf;->a:Less;

    iget-object v3, p0, Lcmx;->j:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcnm;->a(Less;Ljava/lang/String;)Lcnh;

    move-result-object v0

    .line 464
    invoke-virtual {v0}, Lcnh;->c()V

    .line 465
    invoke-virtual {v0}, Lcnh;->a()V

    .line 466
    sget-object v2, Lcuv;->a:Lcuv;

    invoke-virtual {v0, v2}, Lcnh;->a(Lcuv;)V

    .line 467
    invoke-virtual {v0}, Lcnh;->b()V

    move-object v0, v1

    .line 468
    goto :goto_0

    .line 470
    :cond_3
    invoke-virtual {v2}, Lfoy;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Less;J)Less;
    .locals 10

    .prologue
    const-wide v8, 0x7ffffffffffffffeL

    const-wide/16 v2, -0x1

    .line 309
    cmp-long v0, p2, v8

    if-ltz v0, :cond_1

    move-wide v0, v2

    .line 310
    :goto_0
    new-instance v4, Lcwf;

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    const-string v2, "post"

    :goto_1
    iget v3, p0, Lcmx;->p:I

    add-int/lit8 v5, v3, 0x1

    iput v5, p0, Lcmx;->p:I

    invoke-direct {v4, v2, v3, v0, v1}, Lcwf;-><init>(Ljava/lang/String;IJ)V

    .line 314
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 315
    iget-object v0, p1, Less;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoy;

    .line 316
    iget-boolean v1, v0, Lfoy;->Y:Z

    if-eqz v1, :cond_0

    .line 318
    :try_start_0
    iget-object v1, p0, Lcmx;->h:Lgoc;

    iget-object v5, v0, Lfoy;->X:Landroid/net/Uri;

    invoke-virtual {v1, v5, v4}, Lgoc;->a(Landroid/net/Uri;Lgod;)Landroid/net/Uri;

    move-result-object v1

    .line 319
    invoke-virtual {v0}, Lfoy;->b()Lfpc;

    move-result-object v5

    iput-object v1, v5, Lfpc;->ac:Landroid/net/Uri;

    invoke-virtual {v5}, Lfpc;->a()Lfoy;
    :try_end_0
    .catch Lfax; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 324
    :cond_0
    :goto_3
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move-wide v0, p2

    .line 309
    goto :goto_0

    .line 310
    :cond_2
    const-string v2, "mid"

    goto :goto_1

    .line 320
    :catch_0
    move-exception v1

    .line 321
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x20

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Failed to substitute URI macros "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_3

    .line 326
    :cond_3
    cmp-long v0, p2, v8

    if-nez v0, :cond_4

    new-instance v0, Lesh;

    sget-object v1, Lesl;->d:Lesl;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v4, v5}, Lesh;-><init>(Lesl;J)V

    .line 329
    :goto_4
    invoke-virtual {p1}, Less;->b()Lesv;

    move-result-object v1

    .line 330
    iput-object v0, v1, Lesv;->b:Lesh;

    .line 331
    iput-object v2, v1, Lesv;->g:Ljava/util/List;

    .line 332
    invoke-virtual {v1}, Lesv;->a()Less;

    move-result-object v0

    return-object v0

    .line 326
    :cond_4
    new-instance v0, Lesh;

    sget-object v1, Lesl;->a:Lesl;

    invoke-direct {v0, v1, p2, p3}, Lesh;-><init>(Lesl;J)V

    goto :goto_4
.end method

.method static synthetic a(Lcmx;)Leud;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcmx;->q:Leud;

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 6

    .prologue
    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 165
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesh;

    .line 166
    sget-object v3, Lcnb;->a:[I

    iget-object v4, v0, Lesh;->a:Lesl;

    invoke-virtual {v4}, Lesl;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 168
    :pswitch_0
    iget-wide v4, v0, Lesh;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 171
    :pswitch_1
    const-wide v4, 0x7ffffffffffffffeL

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 177
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 181
    :goto_1
    return-object v0

    .line 180
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object v0, v1

    .line 181
    goto :goto_1

    .line 166
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a()V
    .locals 8

    .prologue
    .line 196
    iget-object v0, p0, Lcmx;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 197
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    move-object v1, v0

    .line 198
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 200
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {p0, v4, v5, v6, v7}, Lcmx;->a(JJ)V

    move-object v1, v0

    .line 202
    goto :goto_0

    .line 204
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide v2, 0x7ffffffffffffffeL

    invoke-direct {p0, v0, v1, v2, v3}, Lcmx;->a(JJ)V

    .line 205
    return-void
.end method

.method private a(JJ)V
    .locals 19

    .prologue
    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcmx;->m:Lfqx;

    iget-object v2, v2, Lfqx;->a:Lhnq;

    iget v2, v2, Lhnq;->a:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcmx;->m:Lfqx;

    .line 225
    iget-object v3, v3, Lfqx;->a:Lhnq;

    iget v3, v3, Lhnq;->b:I

    add-int/2addr v3, v2

    .line 227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcmx;->m:Lfqx;

    iget-boolean v4, v2, Lfqx;->c:Z

    if-eqz v4, :cond_0

    iget-object v2, v2, Lfqx;->a:Lhnq;

    iget v2, v2, Lhnq;->a:I

    int-to-long v4, v2

    add-long v4, v4, p1

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-ltz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_1

    .line 228
    int-to-long v4, v3

    add-long v6, p1, v4

    .line 231
    const-wide v4, 0x7ffffffffffffffeL

    cmp-long v2, p3, v4

    if-eqz v2, :cond_3

    .line 232
    int-to-long v2, v3

    add-long v14, p3, v2

    .line 234
    :goto_1
    new-instance v2, Lcng;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcmx;->i:J

    sub-long v4, v6, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcmx;->m:Lfqx;

    .line 235
    iget-object v3, v3, Lfqx;->a:Lhnq;

    iget v3, v3, Lhnq;->b:I

    int-to-long v8, v3

    sub-long/2addr v4, v8

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcng;-><init>(Lcmx;JJ)V

    .line 237
    new-instance v8, Lcnc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcmx;->m:Lfqx;

    .line 238
    iget-object v3, v3, Lfqx;->a:Lhnq;

    iget v3, v3, Lhnq;->b:I

    int-to-long v4, v3

    sub-long v10, v6, v4

    move-object/from16 v9, p0

    move-wide v12, v6

    invoke-direct/range {v8 .. v13}, Lcnc;-><init>(Lcmx;JJ)V

    .line 240
    new-instance v10, Lcnd;

    move-object/from16 v11, p0

    move-wide v12, v6

    move-object/from16 v16, v2

    move-object/from16 v17, v8

    invoke-direct/range {v10 .. v17}, Lcnd;-><init>(Lcmx;JJLcvh;Lcvh;)V

    .line 241
    move-object/from16 v0, p0

    iget-object v3, v0, Lcmx;->d:Lcvl;

    const/4 v4, 0x1

    new-array v4, v4, [Lcvh;

    const/4 v5, 0x0

    aput-object v8, v4, v5

    invoke-interface {v3, v4}, Lcvl;->a([Lcvh;)V

    .line 253
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcmx;->d:Lcvl;

    const/4 v4, 0x1

    new-array v4, v4, [Lcvh;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-interface {v3, v4}, Lcvl;->a([Lcvh;)V

    .line 254
    move-object/from16 v0, p0

    iget-object v2, v0, Lcmx;->d:Lcvl;

    const/4 v3, 0x1

    new-array v3, v3, [Lcvh;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-interface {v2, v3}, Lcvl;->a([Lcvh;)V

    .line 255
    return-void

    .line 227
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 247
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcmx;->m:Lfqx;

    iget-boolean v2, v2, Lfqx;->c:Z

    if-eqz v2, :cond_2

    const-wide v4, 0x7ffffffffffffffeL

    cmp-long v2, p3, v4

    if-eqz v2, :cond_2

    .line 248
    int-to-long v2, v3

    add-long p3, p3, v2

    move-wide/from16 v8, p3

    .line 250
    :goto_3
    new-instance v2, Lcng;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcmx;->i:J

    sub-long v4, p1, v4

    move-object/from16 v3, p0

    move-wide/from16 v6, p1

    invoke-direct/range {v2 .. v7}, Lcng;-><init>(Lcmx;JJ)V

    .line 251
    new-instance v4, Lcnd;

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object v10, v2

    invoke-direct/range {v4 .. v10}, Lcnd;-><init>(Lcmx;JJLcvh;)V

    move-object v10, v4

    goto :goto_2

    :cond_2
    move-wide/from16 v8, p3

    goto :goto_3

    :cond_3
    move-wide/from16 v14, p3

    goto/16 :goto_1
.end method

.method static synthetic a(Lcmx;Lcnc;)V
    .locals 2

    .prologue
    .line 48
    sget-object v0, Lcvj;->f:Lcvj;

    iget-object v1, p1, Lcvh;->d:Lcvj;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcmx;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcmy;

    invoke-direct {v1, p0}, Lcmy;-><init>(Lcmx;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcmx;Lcnd;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 48
    sget-object v2, Lcvj;->d:Lcvj;

    iget-object v3, p1, Lcvh;->d:Lcvj;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcmx;->f:Lcnu;

    invoke-virtual {v2}, Lcnu;->c()I

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcmx;->f:Lcnu;

    invoke-virtual {v3}, Lcnu;->c()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-wide v4, Lcmx;->a:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    :cond_0
    iget-object v2, p0, Lcmx;->d:Lcvl;

    new-array v3, v0, [Lcvh;

    aput-object p1, v3, v1

    invoke-interface {v2, v3}, Lcvl;->b([Lcvh;)V

    iget-object v2, p0, Lcmx;->d:Lcvl;

    new-array v3, v0, [Lcvh;

    iget-object v4, p1, Lcnd;->a:Lcvh;

    aput-object v4, v3, v1

    invoke-interface {v2, v3}, Lcvl;->b([Lcvh;)V

    iget-object v2, p1, Lcnd;->b:Lcvh;

    if-eqz v2, :cond_4

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcmx;->d:Lcvl;

    new-array v3, v0, [Lcvh;

    iget-object v4, p1, Lcnd;->b:Lcvh;

    aput-object v4, v3, v1

    invoke-interface {v2, v3}, Lcvl;->b([Lcvh;)V

    :cond_1
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcmx;->q:Leud;

    if-nez v2, :cond_5

    :goto_1
    if-eqz v0, :cond_2

    invoke-static {}, Leud;->a()Leud;

    move-result-object v1

    iput-object v1, p0, Lcmx;->q:Leud;

    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcmx;->b:Lcnq;

    iget-object v1, p0, Lcmx;->k:Less;

    iget-object v2, p1, Lcvv;->e:Lcvw;

    iget-wide v2, v2, Lcvw;->b:J

    invoke-direct {p0, v1, v2, v3}, Lcmx;->a(Less;J)Less;

    move-result-object v1

    iget-object v2, p0, Lcmx;->j:Ljava/lang/String;

    iget-object v3, p0, Lcmx;->q:Leud;

    invoke-virtual {v0, v1, v2, v3}, Lcnq;->a(Less;Ljava/lang/String;Leuc;)V

    iget-object v0, p0, Lcmx;->e:Lcxl;

    new-instance v1, Lcne;

    invoke-direct {v1, p0}, Lcne;-><init>(Lcmx;)V

    invoke-interface {v0, v1}, Lcxl;->a(Lcxm;)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    iget-object v0, p0, Lcmx;->q:Leud;

    invoke-virtual {v0}, Leud;->isDone()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcmx;->e:Lcxl;

    new-instance v1, Lcne;

    invoke-direct {v1, p0}, Lcne;-><init>(Lcmx;)V

    invoke-interface {v0, v1}, Lcxl;->a(Lcxm;)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcmx;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcmz;

    invoke-direct {v1, p0}, Lcmz;-><init>(Lcmx;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_2
.end method

.method static synthetic a(Lcmx;Lcng;)V
    .locals 4

    .prologue
    .line 48
    sget-object v0, Lcvj;->b:Lcvj;

    iget-object v1, p1, Lcvh;->d:Lcvj;

    if-ne v0, v1, :cond_0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcmx;->q:Leud;

    if-eqz v0, :cond_1

    monitor-exit p0

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Leud;->a()Leud;

    move-result-object v0

    iput-object v0, p0, Lcmx;->q:Leud;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcmx;->b:Lcnq;

    iget-object v1, p0, Lcmx;->k:Less;

    iget-object v2, p1, Lcvv;->f:Lcvw;

    iget-wide v2, v2, Lcvw;->b:J

    invoke-direct {p0, v1, v2, v3}, Lcmx;->a(Less;J)Less;

    move-result-object v1

    iget-object v2, p0, Lcmx;->j:Ljava/lang/String;

    iget-object v3, p0, Lcmx;->q:Leud;

    invoke-virtual {v0, v1, v2, v3}, Lcnq;->a(Less;Ljava/lang/String;Leuc;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcmx;Lcxk;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcmx;->n:Levn;

    sget-object v1, Lcyx;->a:Lcyx;

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    iput-object p1, p0, Lcmx;->o:Lcxk;

    iget-object v0, p0, Lcmx;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcna;

    invoke-direct {v1, p0, p1}, Lcna;-><init>(Lcmx;Lcxk;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcmx;Leud;)Leud;
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcmx;->q:Leud;

    return-object v0
.end method

.method static synthetic b(Lcmx;)Lfqx;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcmx;->m:Lfqx;

    return-object v0
.end method

.method static synthetic c(Lcmx;)Levn;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcmx;->n:Levn;

    return-object v0
.end method

.method static synthetic d(Lcmx;)Lcxl;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcmx;->e:Lcxl;

    return-object v0
.end method

.method static synthetic e(Lcmx;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcmx;->o:Lcxk;

    invoke-interface {v0}, Lcxk;->a()V

    return-void
.end method

.method static synthetic f(Lcmx;)V
    .locals 6

    .prologue
    .line 48
    iget-object v0, p0, Lcmx;->o:Lcxk;

    invoke-interface {v0}, Lcxk;->a()V

    iget-object v0, p0, Lcmx;->m:Lfqx;

    iget-boolean v0, v0, Lfqx;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmx;->n:Levn;

    new-instance v1, Lcyx;

    const/4 v2, 0x1

    iget-object v3, p0, Lcmx;->m:Lfqx;

    iget-object v3, v3, Lfqx;->a:Lhnq;

    iget v3, v3, Lhnq;->d:I

    int-to-long v4, v3

    invoke-direct {v1, v2, v4, v5}, Lcyx;-><init>(IJ)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
