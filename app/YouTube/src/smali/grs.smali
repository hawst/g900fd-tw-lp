.class public final Lgrs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final a:Ljava/lang/Long;

.field private final b:Lgrt;

.field private c:Landroid/graphics/Bitmap;

.field private d:I


# direct methods
.method public constructor <init>(Lgrt;JLandroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrt;

    iput-object v0, p0, Lgrs;->b:Lgrt;

    .line 48
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgrs;->a:Ljava/lang/Long;

    .line 49
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lgrs;->c:Landroid/graphics/Bitmap;

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lgrs;->d:I

    .line 51
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgrs;->c:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lgrs;
    .locals 1

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lgrs;->d:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 77
    iget v0, p0, Lgrs;->d:I

    if-lez v0, :cond_1

    .line 78
    iget v0, p0, Lgrs;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgrs;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, p0

    .line 81
    :goto_1
    monitor-exit p0

    return-object v0

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lgrs;->d:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 89
    iget v0, p0, Lgrs;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lgrs;->d:I

    .line 91
    iget v0, p0, Lgrs;->d:I

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lgrs;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lgrs;->c:Landroid/graphics/Bitmap;

    .line 94
    iget-object v0, p0, Lgrs;->b:Lgrt;

    invoke-interface {v0, p0}, Lgrt;->a(Lgrs;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :cond_0
    monitor-exit p0

    return-void

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 17
    check-cast p1, Lgrs;

    iget-object v0, p0, Lgrs;->a:Ljava/lang/Long;

    iget-object v1, p1, Lgrs;->a:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method
