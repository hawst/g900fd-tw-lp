.class public final Ldhy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ldif;

.field final b:Lcws;

.field final c:Levn;

.field private final d:Lexz;

.field private final e:Lgeh;

.field private final f:Lcyd;

.field private final g:Lezj;


# direct methods
.method public constructor <init>(Lexz;Ldif;Lgeh;Lcws;Lcyd;Levn;Lezj;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexz;

    iput-object v0, p0, Ldhy;->d:Lexz;

    .line 49
    iget-object v0, p0, Ldhy;->d:Lexz;

    new-instance v1, Ldir;

    invoke-direct {v1, p0}, Ldir;-><init>(Ldhy;)V

    invoke-virtual {v0, v1}, Lexz;->a(Lexy;)V

    .line 50
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldif;

    iput-object v0, p0, Ldhy;->a:Ldif;

    .line 51
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgeh;

    iput-object v0, p0, Ldhy;->e:Lgeh;

    .line 52
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcws;

    iput-object v0, p0, Ldhy;->b:Lcws;

    .line 53
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyd;

    iput-object v0, p0, Ldhy;->f:Lcyd;

    .line 54
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldhy;->c:Levn;

    .line 55
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Ldhy;->g:Lezj;

    .line 56
    return-void
.end method

.method private handleVideoStageEvent(Ldac;)V
    .locals 12
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Ldhy;->f:Lcyd;

    invoke-interface {v0}, Lcyd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldhy;->f:Lcyd;

    .line 145
    invoke-interface {v0}, Lcyd;->c()I

    move-result v0

    if-gtz v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v0, p0, Ldhy;->f:Lcyd;

    .line 149
    invoke-interface {v0}, Lcyd;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lgol;->c:Lgol;

    .line 151
    :goto_1
    iget-object v1, p1, Ldac;->a:Lgol;

    if-ne v1, v0, :cond_0

    .line 152
    iget-object v0, p1, Ldac;->b:Lfrl;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Ldhy;->c:Levn;

    new-instance v1, Ldik;

    invoke-direct {v1}, Ldik;-><init>()V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 155
    iget-object v0, p0, Ldhy;->c:Levn;

    new-instance v1, Ldim;

    .line 157
    iget-object v2, p1, Ldac;->b:Lfrl;

    iget-object v2, v2, Lfrl;->a:Lhro;

    invoke-static {v2}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    .line 158
    iget-object v3, p1, Ldac;->c:Ljava/lang/String;

    iget-object v4, p0, Ldhy;->f:Lcyd;

    .line 159
    invoke-interface {v4}, Lcyd;->c()I

    move-result v4

    int-to-long v4, v4

    iget-object v6, p0, Ldhy;->f:Lcyd;

    .line 160
    invoke-interface {v6}, Lcyd;->d()Z

    move-result v6

    invoke-direct/range {v1 .. v6}, Ldim;-><init>(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 155
    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 162
    iget-object v0, p1, Ldac;->b:Lfrl;

    iget-object v1, p0, Ldhy;->f:Lcyd;

    .line 163
    invoke-interface {v1}, Lcyd;->c()I

    move-result v1

    int-to-long v6, v1

    .line 161
    iget-object v1, p0, Ldhy;->e:Lgeh;

    iget-object v2, v0, Lfrl;->d:Lfrf;

    invoke-virtual {v0}, Lfrl;->i()Lfqy;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lgeh;->a(Lfrf;Lfqy;Z)[Lfqj;

    move-result-object v5

    invoke-static {v5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    array-length v1, v5

    if-lez v1, :cond_3

    iget-object v1, v0, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lfrl;->d:Lfrf;

    iget-wide v3, v0, Lfrf;->f:J

    const-wide/16 v0, 0x0

    iget-object v8, p0, Ldhy;->d:Lexz;

    iget-object v9, p0, Ldhy;->g:Lezj;

    invoke-virtual {v9}, Lezj;->a()J

    move-result-wide v10

    add-long/2addr v0, v10

    invoke-static/range {v0 .. v7}, Ldiq;->a(JLjava/lang/String;J[Lfqj;J)Lead;

    move-result-object v0

    invoke-virtual {v8, v0}, Lexz;->a(Lead;)V

    goto :goto_0

    .line 149
    :cond_2
    sget-object v0, Lgol;->f:Lgol;

    goto :goto_1

    .line 161
    :cond_3
    iget-object v0, p0, Ldhy;->c:Levn;

    new-instance v1, Ldig;

    invoke-direct {v1}, Ldig;-><init>()V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
