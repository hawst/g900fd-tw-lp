.class public final enum Lbjy;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbjy;

.field public static final enum b:Lbjy;

.field private static enum e:Lbjy;

.field private static final synthetic f:[Lbjy;


# instance fields
.field public final c:Lflj;

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Lbjy;

    const-string v1, "AMODO_ONLY"

    sget-object v2, Lflj;->a:Lflj;

    const v3, 0x7f090058

    invoke-direct {v0, v1, v4, v2, v3}, Lbjy;-><init>(Ljava/lang/String;ILflj;I)V

    sput-object v0, Lbjy;->a:Lbjy;

    .line 21
    new-instance v0, Lbjy;

    const-string v1, "SD"

    sget-object v2, Lflj;->b:Lflj;

    const v3, 0x7f0901cb

    invoke-direct {v0, v1, v5, v2, v3}, Lbjy;-><init>(Ljava/lang/String;ILflj;I)V

    sput-object v0, Lbjy;->b:Lbjy;

    .line 24
    new-instance v0, Lbjy;

    const-string v1, "HD"

    sget-object v2, Lflj;->c:Lflj;

    const v3, 0x7f0901cc

    invoke-direct {v0, v1, v6, v2, v3}, Lbjy;-><init>(Ljava/lang/String;ILflj;I)V

    sput-object v0, Lbjy;->e:Lbjy;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lbjy;

    sget-object v1, Lbjy;->a:Lbjy;

    aput-object v1, v0, v4

    sget-object v1, Lbjy;->b:Lbjy;

    aput-object v1, v0, v5

    sget-object v1, Lbjy;->e:Lbjy;

    aput-object v1, v0, v6

    sput-object v0, Lbjy;->f:[Lbjy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILflj;I)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput-object p3, p0, Lbjy;->c:Lflj;

    .line 64
    iput p4, p0, Lbjy;->d:I

    .line 65
    return-void
.end method

.method public static a(I)Lbjy;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lbjz;->b()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjy;

    return-object v0
.end method

.method public static a(Lflj;)Lbjy;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lbjz;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjy;

    return-object v0
.end method

.method static synthetic a(Lbjy;)Lflj;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lbjy;->c:Lflj;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbjy;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lbjy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbjy;

    return-object v0
.end method

.method public static values()[Lbjy;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lbjy;->f:[Lbjy;

    invoke-virtual {v0}, [Lbjy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbjy;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lbjy;->c:Lflj;

    iget v0, v0, Lflj;->d:I

    return v0
.end method
