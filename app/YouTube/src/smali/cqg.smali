.class public final Lcqg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lgku;

.field private b:Lezj;

.field private c:Lgic;


# direct methods
.method public constructor <init>(Lezj;Lcny;Lcqt;Lcsg;Lgic;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcqg;->b:Lezj;

    .line 49
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqt;

    invoke-virtual {p2, v0, p4}, Lcny;->a(Lcqt;Lcsg;)Lgko;

    move-result-object v0

    iput-object v0, p0, Lcqg;->a:Lgku;

    .line 51
    iput-object p5, p0, Lcqg;->c:Lgic;

    .line 52
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLesq;JJ)Lesq;
    .locals 15

    .prologue
    .line 161
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 162
    move-object/from16 v0, p4

    iget-object v2, v0, Lesq;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Less;

    .line 163
    invoke-virtual {v2}, Less;->b()Lesv;

    move-result-object v3

    const/4 v6, 0x0

    .line 164
    iput-object v6, v3, Lesv;->g:Ljava/util/List;

    .line 165
    iput-object p0, v3, Lesv;->f:Ljava/lang/String;

    .line 166
    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lesv;->a([B)Lesv;

    move-result-object v6

    .line 169
    iget-object v2, v2, Less;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfoy;

    .line 170
    invoke-virtual {v2}, Lfoy;->b()Lfpc;

    move-result-object v8

    .line 171
    move-wide/from16 v0, p5

    iput-wide v0, v8, Lfpc;->Z:J

    .line 172
    move-object/from16 v0, p1

    iput-object v0, v8, Lfpc;->d:Ljava/lang/String;

    .line 173
    move-object/from16 v0, p2

    iput-object v0, v8, Lfpc;->e:Ljava/lang/String;

    .line 174
    move-object/from16 v0, p3

    iput-object v0, v8, Lfpc;->f:[B

    .line 175
    iget-boolean v3, v2, Lfoy;->Y:Z

    if-nez v3, :cond_0

    iget-wide v10, v2, Lfoy;->R:J

    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-nez v3, :cond_0

    .line 178
    iget-object v3, v2, Lfoy;->aa:Lfoy;

    if-eqz v3, :cond_2

    iget-object v3, v2, Lfoy;->aa:Lfoy;

    iget-wide v10, v3, Lfoy;->R:J

    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-lez v3, :cond_2

    .line 179
    iget-object v2, v2, Lfoy;->aa:Lfoy;

    iget-wide v2, v2, Lfoy;->R:J

    .line 177
    :goto_2
    iput-wide v2, v8, Lfpc;->R:J

    .line 182
    :cond_0
    invoke-virtual {v8}, Lfpc;->a()Lfoy;

    move-result-object v2

    iget-object v3, v6, Lesv;->g:Ljava/util/List;

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v6, Lesv;->g:Ljava/util/List;

    :cond_1
    iget-object v3, v6, Lesv;->g:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 179
    :cond_2
    add-long v2, p5, p7

    goto :goto_2

    .line 184
    :cond_3
    invoke-virtual {v6}, Lesv;->a()Less;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 186
    :cond_4
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 187
    move-object/from16 v0, p4

    iget-object v2, v0, Lesq;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    move-object/from16 v0, p4

    iget-object v2, v0, Lesq;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Less;

    move-object v3, v2

    :goto_3
    new-instance v5, Lesr;

    invoke-direct {v5}, Lesr;-><init>()V

    move-object/from16 v0, p4

    iget-object v2, v0, Lesq;->a:Ljava/util/List;

    iput-object v2, v5, Lesr;->a:Ljava/util/List;

    move-object/from16 v0, p4

    iget-object v2, v0, Lesq;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    move-object/from16 v0, p4

    iget-object v2, v0, Lesq;->a:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Less;

    iget-object v2, v2, Less;->m:Lesn;

    :goto_4
    invoke-virtual {v5, v2}, Lesr;->a(Lesn;)Lesr;

    move-result-object v5

    if-eqz v3, :cond_7

    iget-object v2, v3, Less;->o:Ljava/lang/String;

    :goto_5
    iput-object v2, v5, Lesr;->b:Ljava/lang/String;

    iput-object v4, v5, Lesr;->a:Ljava/util/List;

    invoke-virtual {v5}, Lesr;->a()Lesq;

    move-result-object v2

    return-object v2

    :cond_5
    const/4 v2, 0x0

    move-object v3, v2

    goto :goto_3

    :cond_6
    sget-object v2, Lesn;->a:Lesn;

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    goto :goto_5
.end method


# virtual methods
.method public final a(Lfrl;J)Lesq;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 123
    invoke-static {}, Lb;->b()V

    .line 124
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-virtual {p1}, Lfrl;->m()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-object v0

    .line 129
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcqg;->c:Lgic;

    invoke-interface {v1, p1}, Lgic;->a_(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lesq;

    .line 130
    if-eqz v5, :cond_0

    .line 134
    iget-object v1, p1, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v1

    .line 135
    iget-object v2, p1, Lfrl;->a:Lhro;

    iget-object v2, v2, Lhro;->n:Ljava/lang/String;

    .line 136
    iget-object v3, p1, Lfrl;->a:Lhro;

    iget-object v3, v3, Lhro;->o:Ljava/lang/String;

    .line 137
    iget-object v4, p1, Lfrl;->c:[B

    iget-object v6, p0, Lcqg;->b:Lezj;

    .line 139
    invoke-virtual {v6}, Lezj;->a()J

    move-result-wide v6

    move-wide v8, p2

    .line 133
    invoke-static/range {v1 .. v9}, Lcqg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLesq;JJ)Lesq;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lfaz; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lfax; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v1

    goto :goto_0

    .line 146
    :catch_1
    move-exception v1

    goto :goto_0

    .line 143
    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/util/Map;JLfai;)Lesq;
    .locals 10

    .prologue
    .line 81
    invoke-static {}, Lb;->b()V

    .line 82
    invoke-static {}, Leud;->a()Leud;

    move-result-object v1

    .line 83
    new-instance v2, Lcqu;

    .line 84
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v2, v3, v0}, Lcqu;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 85
    iget-object v0, p0, Lcqg;->b:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v6

    .line 86
    invoke-virtual/range {p8 .. p8}, Lfai;->a()J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-gtz v0, :cond_0

    .line 88
    const/4 v0, 0x0

    .line 110
    :goto_0
    return-object v0

    .line 90
    :cond_0
    iget-object v0, p0, Lcqg;->a:Lgku;

    invoke-interface {v0, v2, v1}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 93
    :try_start_0
    invoke-virtual/range {p8 .. p8}, Lfai;->a()J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Leud;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lesq;

    .line 94
    if-nez v5, :cond_1

    .line 95
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide/from16 v8, p6

    .line 97
    invoke-static/range {v1 .. v9}, Lcqg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLesq;JJ)Lesq;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    .line 110
    :catch_1
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
