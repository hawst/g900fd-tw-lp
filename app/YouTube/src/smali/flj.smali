.class public final enum Lflj;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lflj;

.field public static final enum b:Lflj;

.field public static final enum c:Lflj;

.field private static final synthetic e:[Lflj;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    new-instance v0, Lflj;

    const-string v1, "AMODO_ONLY"

    invoke-direct {v0, v1, v3, v3}, Lflj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lflj;->a:Lflj;

    .line 77
    new-instance v0, Lflj;

    const-string v1, "SD"

    const/16 v2, 0x168

    invoke-direct {v0, v1, v4, v2}, Lflj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lflj;->b:Lflj;

    .line 78
    new-instance v0, Lflj;

    const-string v1, "HD"

    const/16 v2, 0x2d0

    invoke-direct {v0, v1, v5, v2}, Lflj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lflj;->c:Lflj;

    .line 75
    const/4 v0, 0x3

    new-array v0, v0, [Lflj;

    sget-object v1, Lflj;->a:Lflj;

    aput-object v1, v0, v3

    sget-object v1, Lflj;->b:Lflj;

    aput-object v1, v0, v4

    sget-object v1, Lflj;->c:Lflj;

    aput-object v1, v0, v5

    sput-object v0, Lflj;->e:[Lflj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 83
    iput p3, p0, Lflj;->d:I

    .line 84
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflj;
    .locals 1

    .prologue
    .line 75
    const-class v0, Lflj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflj;

    return-object v0
.end method

.method public static values()[Lflj;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lflj;->e:[Lflj;

    invoke-virtual {v0}, [Lflj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflj;

    return-object v0
.end method
