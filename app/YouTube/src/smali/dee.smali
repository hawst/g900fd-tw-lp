.class public abstract Ldee;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Ldec;


# instance fields
.field private a:Z

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldee;->a:Z

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Ldee;->b:I

    .line 109
    return-void
.end method


# virtual methods
.method public final a_(Z)V
    .locals 1

    .prologue
    .line 113
    iput-boolean p1, p0, Ldee;->a:Z

    .line 114
    if-eqz p1, :cond_0

    iget v0, p0, Ldee;->b:I

    :goto_0
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 115
    return-void

    .line 114
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final f_()Landroid/view/View;
    .locals 0

    .prologue
    .line 125
    return-object p0
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 119
    iput p1, p0, Ldee;->b:I

    .line 120
    iget-boolean v0, p0, Ldee;->a:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ldee;->b:I

    :goto_0
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 121
    return-void

    .line 120
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
