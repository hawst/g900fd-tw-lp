.class final Ltb;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lsw;)V
    .locals 1

    .prologue
    .line 604
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 605
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltb;->a:Ljava/lang/ref/WeakReference;

    .line 606
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 614
    iget-object v0, p0, Ltb;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsw;

    .line 615
    if-eqz v0, :cond_1

    .line 616
    iget v3, p1, Landroid/os/Message;->what:I

    .line 617
    iget v6, p1, Landroid/os/Message;->arg1:I

    .line 618
    iget v7, p1, Landroid/os/Message;->arg2:I

    .line 619
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 620
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v8

    .line 621
    packed-switch v3, :pswitch_data_0

    :cond_0
    move v0, v5

    :goto_0
    if-nez v0, :cond_1

    .line 622
    invoke-static {}, Lsv;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 623
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unhandled message from server: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 627
    :cond_1
    return-void

    .line 621
    :pswitch_0
    iget v1, v0, Lsw;->f:I

    if-ne v6, v1, :cond_2

    iput v5, v0, Lsw;->f:I

    iget-object v1, v0, Lsw;->h:Lsv;

    const-string v3, "Registation failed"

    invoke-static {v1, v0, v3}, Lsv;->a(Lsv;Lsw;Ljava/lang/String;)V

    :cond_2
    iget-object v1, v0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v1, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {v1, v2, v2}, Lse;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_3
    move v0, v4

    goto :goto_0

    :pswitch_1
    move v0, v4

    goto :goto_0

    :pswitch_2
    if-eqz v1, :cond_4

    instance-of v2, v1, Landroid/os/Bundle;

    if-eqz v2, :cond_0

    :cond_4
    check-cast v1, Landroid/os/Bundle;

    iget v2, v0, Lsw;->e:I

    if-nez v2, :cond_5

    iget v2, v0, Lsw;->f:I

    if-ne v6, v2, :cond_5

    if-lez v7, :cond_5

    iput v5, v0, Lsw;->f:I

    iput v7, v0, Lsw;->e:I

    iget-object v2, v0, Lsw;->h:Lsv;

    invoke-static {v1}, Lrr;->a(Landroid/os/Bundle;)Lrr;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lsv;->a(Lsv;Lsw;Lrr;)V

    iget-object v1, v0, Lsw;->h:Lsv;

    invoke-static {v1, v0}, Lsv;->a(Lsv;Lsw;)V

    move v0, v4

    goto :goto_0

    :cond_5
    move v0, v5

    goto :goto_0

    :pswitch_3
    if-eqz v1, :cond_6

    instance-of v2, v1, Landroid/os/Bundle;

    if-eqz v2, :cond_0

    :cond_6
    check-cast v1, Landroid/os/Bundle;

    iget v2, v0, Lsw;->e:I

    if-eqz v2, :cond_7

    iget-object v2, v0, Lsw;->h:Lsv;

    invoke-static {v1}, Lrr;->a(Landroid/os/Bundle;)Lrr;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lsv;->a(Lsv;Lsw;Lrr;)V

    move v0, v4

    goto :goto_0

    :cond_7
    move v0, v5

    goto :goto_0

    :pswitch_4
    if-eqz v1, :cond_8

    instance-of v2, v1, Landroid/os/Bundle;

    if-eqz v2, :cond_0

    :cond_8
    check-cast v1, Landroid/os/Bundle;

    iget-object v2, v0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse;

    if-eqz v2, :cond_9

    iget-object v0, v0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {v2, v1}, Lse;->a(Landroid/os/Bundle;)V

    move v0, v4

    goto/16 :goto_0

    :cond_9
    move v0, v5

    goto/16 :goto_0

    :pswitch_5
    if-eqz v1, :cond_a

    instance-of v3, v1, Landroid/os/Bundle;

    if-eqz v3, :cond_0

    :cond_a
    if-nez v8, :cond_b

    move-object v3, v2

    :goto_1
    check-cast v1, Landroid/os/Bundle;

    iget-object v2, v0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse;

    if-eqz v2, :cond_c

    iget-object v0, v0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {v2, v3, v1}, Lse;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    move v0, v4

    goto/16 :goto_0

    :cond_b
    const-string v2, "error"

    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto :goto_1

    :cond_c
    move v0, v5

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method
