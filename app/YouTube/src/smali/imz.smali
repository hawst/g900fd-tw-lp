.class final Limz;
.super Limu;
.source "SourceFile"

# interfaces
.implements Liql;


# instance fields
.field private b:Z

.field private synthetic c:Limt;


# direct methods
.method constructor <init>(Limt;Lu;)V
    .locals 0

    .prologue
    .line 559
    iput-object p1, p0, Limz;->c:Limt;

    .line 560
    invoke-direct {p0, p1, p2}, Limu;-><init>(Limt;Lu;)V

    .line 561
    return-void
.end method


# virtual methods
.method public final a()Liqm;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Limz;->c:Limt;

    invoke-static {v0}, Limt;->f(Limt;)Lipu;

    move-result-object v0

    invoke-interface {v0}, Lipu;->a()Liqm;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lipq;J)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 565
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_0
    iget-boolean v2, p0, Limz;->a:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 567
    :cond_1
    iget-boolean v2, p0, Limz;->b:Z

    if-eqz v2, :cond_2

    .line 576
    :goto_0
    return-wide v0

    .line 569
    :cond_2
    iget-object v2, p0, Limz;->c:Limt;

    invoke-static {v2}, Limt;->f(Limt;)Lipu;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3}, Lipu;->b(Lipq;J)J

    move-result-wide v2

    .line 570
    cmp-long v4, v2, v0

    if-nez v4, :cond_3

    .line 571
    const/4 v2, 0x1

    iput-boolean v2, p0, Limz;->b:Z

    .line 572
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Limz;->a(Z)V

    goto :goto_0

    .line 575
    :cond_3
    invoke-virtual {p0, p1, v2, v3}, Limz;->a(Lipq;J)V

    move-wide v0, v2

    .line 576
    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 584
    iget-boolean v0, p0, Limz;->a:Z

    if-eqz v0, :cond_0

    .line 590
    :goto_0
    return-void

    .line 586
    :cond_0
    iget-boolean v0, p0, Limz;->b:Z

    if-nez v0, :cond_1

    .line 587
    invoke-virtual {p0}, Limz;->A_()V

    .line 589
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Limz;->a:Z

    goto :goto_0
.end method
