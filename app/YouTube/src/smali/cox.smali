.class public final Lcox;
.super Lcnh;
.source "SourceFile"

# interfaces
.implements Leru;
.implements Leuc;


# instance fields
.field private final a:Lcnw;

.field private final b:Lfoy;

.field private final c:Less;

.field private final d:Lcuy;

.field private final e:Lgoc;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Ljava/util/List;

.field private j:I

.field private k:I

.field private l:Ljava/util/PriorityQueue;

.field private m:Lcwx;

.field private final n:Lerr;

.field private o:Levn;


# direct methods
.method constructor <init>(Levn;Lcnw;Less;Lfoy;Ljava/lang/String;IZZZLjava/util/List;ILcwx;Lerr;Lgoc;Lcuy;)V
    .locals 12

    .prologue
    .line 92
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p12

    move-object/from16 v9, p13

    move-object/from16 v10, p14

    move-object/from16 v11, p15

    invoke-direct/range {v2 .. v11}, Lcox;-><init>(Levn;Lcnw;Less;Lfoy;Ljava/lang/String;Lcwx;Lerr;Lgoc;Lcuy;)V

    .line 102
    move/from16 v0, p6

    iput v0, p0, Lcox;->j:I

    .line 103
    move/from16 v0, p7

    iput-boolean v0, p0, Lcox;->f:Z

    .line 104
    move/from16 v0, p8

    iput-boolean v0, p0, Lcox;->g:Z

    .line 105
    move/from16 v0, p9

    iput-boolean v0, p0, Lcox;->h:Z

    .line 106
    new-instance v2, Ljava/util/ArrayList;

    .line 107
    invoke-static/range {p10 .. p10}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcox;->i:Ljava/util/List;

    .line 108
    move/from16 v0, p11

    iput v0, p0, Lcox;->k:I

    .line 109
    move/from16 v0, p11

    invoke-direct {p0, v0}, Lcox;->b(I)Ljava/util/PriorityQueue;

    move-result-object v2

    iput-object v2, p0, Lcox;->l:Ljava/util/PriorityQueue;

    .line 110
    iget-object v2, p0, Lcox;->d:Lcuy;

    move/from16 v0, p11

    int-to-long v4, v0

    iput-wide v4, v2, Lcuy;->g:J

    .line 111
    return-void
.end method

.method constructor <init>(Levn;Lcnw;Less;Lfoy;Ljava/lang/String;Lcwx;Lerr;Lgoc;Lcuy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 124
    invoke-direct {p0, p1}, Lcnh;-><init>(Levn;)V

    .line 71
    iput-object v1, p0, Lcox;->m:Lcwx;

    .line 125
    iput-object p1, p0, Lcox;->o:Levn;

    .line 126
    iput-object p2, p0, Lcox;->a:Lcnw;

    .line 127
    iput-object p4, p0, Lcox;->b:Lfoy;

    .line 128
    iput-object p3, p0, Lcox;->c:Less;

    .line 129
    iput-object p7, p0, Lcox;->n:Lerr;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcox;->i:Ljava/util/List;

    .line 131
    const/4 v0, -0x1

    iput v0, p0, Lcox;->k:I

    .line 132
    iget v0, p0, Lcox;->k:I

    invoke-direct {p0, v0}, Lcox;->b(I)Ljava/util/PriorityQueue;

    move-result-object v0

    iput-object v0, p0, Lcox;->l:Ljava/util/PriorityQueue;

    .line 134
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwx;

    iput-object v0, p0, Lcox;->m:Lcwx;

    .line 135
    iput-object p9, p0, Lcox;->d:Lcuy;

    .line 136
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoc;

    iput-object v0, p0, Lcox;->e:Lgoc;

    .line 137
    iget-object v0, p0, Lcox;->d:Lcuy;

    iget-object v2, p3, Less;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, p5}, Lcuy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v2, p0, Lcox;->d:Lcuy;

    iput-object p3, v2, Lcuy;->b:Less;

    if-eqz p3, :cond_0

    iget-object v0, p3, Less;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p3, Less;->o:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    :goto_0
    iput-object v0, v2, Lcuy;->c:Ljava/util/regex/Pattern;

    .line 139
    iget-object v0, p0, Lcox;->d:Lcuy;

    iput-object p4, v0, Lcuy;->a:Lfoy;

    .line 140
    iget-object v0, p0, Lcox;->d:Lcuy;

    iget-object v1, p0, Lcox;->m:Lcwx;

    iput-object v1, v0, Lcuy;->d:Lcwx;

    .line 141
    return-void

    :cond_0
    move-object v0, v1

    .line 138
    goto :goto_0
.end method

.method static synthetic a(Lcox;)Lfoy;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcox;->b:Lfoy;

    return-object v0
.end method

.method private a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 358
    iget-object v0, p0, Lcox;->d:Lcuy;

    int-to-long v2, p1

    iput-wide v2, v0, Lcuy;->g:J

    .line 359
    iget-boolean v0, p0, Lcox;->g:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcox;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    invoke-direct {p0}, Lcox;->p()V

    .line 361
    iput-boolean v6, p0, Lcox;->g:Z

    .line 364
    :cond_0
    :goto_0
    iget-object v0, p0, Lcox;->l:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcox;->l:Ljava/util/PriorityQueue;

    .line 366
    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpf;

    iget-object v2, p0, Lcox;->b:Lfoy;

    iget v2, v2, Lfoy;->o:I

    invoke-virtual {v0, v2}, Lfpf;->a(I)I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 367
    iget-object v0, p0, Lcox;->l:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpf;

    iget-object v0, v0, Lfpf;->c:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcox;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 369
    :cond_1
    iput p1, p0, Lcox;->k:I

    .line 371
    iget-object v0, p0, Lcox;->b:Lfoy;

    iget v0, v0, Lfoy;->o:I

    mul-int/lit16 v0, v0, 0x3e8

    if-lez v0, :cond_2

    mul-int/lit8 v2, p1, 0x4

    div-int v0, v2, v0

    .line 372
    :goto_1
    iget v2, p0, Lcox;->j:I

    if-lt v0, v2, :cond_6

    move v3, v0

    .line 374
    :goto_2
    iget v2, p0, Lcox;->j:I

    if-lt v3, v2, :cond_5

    .line 375
    iget-object v2, p0, Lcox;->b:Lfoy;

    move-object v4, v2

    move v5, v1

    :goto_3
    if-eqz v4, :cond_3

    packed-switch v3, :pswitch_data_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    :goto_4
    invoke-direct {p0, v2}, Lcox;->a(Ljava/util/List;)Z

    move-result v2

    or-int/2addr v5, v2

    iget-object v2, v4, Lfoy;->aa:Lfoy;

    move-object v4, v2

    goto :goto_3

    :cond_2
    move v0, v1

    .line 371
    goto :goto_1

    .line 375
    :pswitch_0
    iget-object v2, v4, Lfoy;->v:Ljava/util/List;

    goto :goto_4

    :pswitch_1
    iget-object v2, v4, Lfoy;->w:Ljava/util/List;

    goto :goto_4

    :pswitch_2
    iget-object v2, v4, Lfoy;->x:Ljava/util/List;

    goto :goto_4

    :cond_3
    iget-object v2, p0, Lcox;->n:Lerr;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcox;->n:Lerr;

    packed-switch v3, :pswitch_data_1

    :cond_4
    :goto_5
    if-nez v5, :cond_5

    .line 376
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_2

    .line 375
    :pswitch_3
    iget-object v2, v2, Lerr;->a:Leri;

    sget-object v4, Lero;->b:Lero;

    invoke-virtual {v2, v4}, Leri;->a(Lero;)V

    goto :goto_5

    :pswitch_4
    iget-object v2, v2, Lerr;->a:Leri;

    sget-object v4, Lero;->c:Lero;

    invoke-virtual {v2, v4}, Leri;->a(Lero;)V

    goto :goto_5

    :pswitch_5
    iget-object v2, v2, Lerr;->a:Leri;

    sget-object v4, Lero;->d:Lero;

    invoke-virtual {v2, v4}, Leri;->a(Lero;)V

    goto :goto_5

    .line 379
    :cond_5
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcox;->j:I

    .line 381
    :cond_6
    iget-boolean v0, p0, Lcox;->f:Z

    if-nez v0, :cond_8

    const/16 v0, 0x7530

    if-lt p1, v0, :cond_8

    .line 382
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 383
    :goto_6
    if-eqz v0, :cond_7

    .line 384
    iget-object v1, v0, Lfoy;->B:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 385
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_6

    .line 387
    :cond_7
    iput-boolean v6, p0, Lcox;->f:Z

    .line 389
    :cond_8
    return-void

    .line 375
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 584
    sget-object v0, Lgod;->h:Lgod;

    invoke-direct {p0, p1, v0}, Lcox;->a(Landroid/net/Uri;Lgod;)V

    .line 585
    return-void
.end method

.method private a(Landroid/net/Uri;Lgod;)V
    .locals 4

    .prologue
    .line 588
    if-eqz p1, :cond_0

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    :try_start_0
    iget-object v0, p0, Lcox;->e:Lgoc;

    invoke-virtual {v0, p1, p2}, Lgoc;->a(Landroid/net/Uri;Lgod;)Landroid/net/Uri;
    :try_end_0
    .catch Lfax; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 594
    :goto_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Pinging "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 595
    iget-object v0, p0, Lcox;->a:Lcnw;

    iget-object v1, p0, Lcox;->a:Lcnw;

    const-string v2, "vastad"

    .line 596
    invoke-virtual {v1, p1, v2}, Lcnw;->a(Landroid/net/Uri;Ljava/lang/String;)Lgjt;

    move-result-object v1

    iget-object v2, p0, Lcox;->b:Lfoy;

    .line 597
    iget-boolean v2, v2, Lfoy;->ag:Z

    iput-boolean v2, v1, Lgjt;->d:Z

    iget-object v2, p0, Lcox;->b:Lfoy;

    .line 598
    iget-wide v2, v2, Lfoy;->R:J

    iput-wide v2, v1, Lgjt;->e:J

    sget-object v2, Lggu;->a:Lwu;

    .line 595
    invoke-virtual {v0, v1, v2}, Lcnw;->a(Lgjt;Lwu;)V

    .line 601
    :cond_0
    return-void

    .line 591
    :catch_0
    move-exception v0

    .line 592
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed to substitute URI macros "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 524
    iget-object v0, p0, Lcox;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529
    :cond_0
    return-void

    .line 527
    :cond_1
    iget-object v0, p0, Lcox;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    iget-object v0, p0, Lcox;->b:Lfoy;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    iget-object v0, v1, Lfoy;->O:Landroid/net/Uri;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-direct {p0, v0}, Lcox;->a(Landroid/net/Uri;)V

    iget-object v0, v1, Lfoy;->aa:Lfoy;

    move-object v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, v1, Lfoy;->O:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "label"

    invoke-virtual {v0, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Ljava/util/List;)Z
    .locals 1

    .prologue
    .line 570
    sget-object v0, Lgod;->h:Lgod;

    invoke-direct {p0, p1, v0}, Lcox;->a(Ljava/util/List;Lgod;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;Lgod;)Z
    .locals 2

    .prologue
    .line 574
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 575
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 576
    invoke-direct {p0, v0, p2}, Lcox;->a(Landroid/net/Uri;Lgod;)V

    goto :goto_0

    .line 578
    :cond_0
    const/4 v0, 0x1

    .line 580
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(I)Ljava/util/PriorityQueue;
    .locals 5

    .prologue
    .line 607
    new-instance v2, Ljava/util/PriorityQueue;

    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 608
    iget-object v0, v0, Lfoy;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-instance v1, Lcoy;

    invoke-direct {v1, p0}, Lcoy;-><init>(Lcox;)V

    invoke-direct {v2, v0, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 615
    iget-object v0, p0, Lcox;->b:Lfoy;

    move-object v1, v0

    .line 616
    :goto_0
    if-eqz v1, :cond_2

    .line 617
    iget-object v0, v1, Lfoy;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpf;

    .line 618
    iget-object v4, p0, Lcox;->b:Lfoy;

    iget v4, v4, Lfoy;->o:I

    invoke-virtual {v0, v4}, Lfpf;->a(I)I

    move-result v4

    if-le v4, p1, :cond_0

    .line 619
    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 622
    :cond_1
    iget-object v0, v1, Lfoy;->aa:Lfoy;

    move-object v1, v0

    goto :goto_0

    .line 624
    :cond_2
    return-object v2
.end method

.method private p()V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 393
    :goto_0
    if-eqz v0, :cond_0

    .line 394
    iget-object v1, v0, Lfoy;->b:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 395
    iget-object v1, v0, Lfoy;->u:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 396
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 398
    :cond_0
    return-void
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcox;->b:Lfoy;

    iget-object v0, v0, Lfoy;->r:Lfqy;

    invoke-virtual {v0}, Lfqy;->p()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcox;->c:Less;

    iget-object v0, v0, Less;->i:Ljava/util/List;

    invoke-direct {p0, v0}, Lcox;->a(Ljava/util/List;)Z

    .line 211
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 251
    iget-object v0, p0, Lcox;->n:Lerr;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcox;->n:Lerr;

    iget-object v0, v0, Lerr;->a:Leri;

    sget-object v1, Lero;->h:Lero;

    invoke-virtual {v0, v1}, Leri;->a(Lero;)V

    .line 254
    :cond_0
    new-instance v1, Lcxf;

    invoke-direct {v1, p1, p2}, Lcxf;-><init>(II)V

    .line 256
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 257
    :goto_0
    if-eqz v0, :cond_1

    .line 258
    iget-object v2, v0, Lfoy;->z:Ljava/util/List;

    invoke-direct {p0, v2, v1}, Lcox;->a(Ljava/util/List;Lgod;)Z

    .line 259
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 261
    :cond_1
    return-void
.end method

.method public final a(Lcuv;)V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcox;->c:Less;

    iget-object v0, v0, Less;->k:Ljava/util/List;

    new-instance v1, Lcux;

    invoke-direct {v1, p1}, Lcux;-><init>(Lcuv;)V

    invoke-direct {p0, v0, v1}, Lcox;->a(Ljava/util/List;Lgod;)Z

    .line 206
    return-void
.end method

.method public final a(Lcwx;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 177
    iget-object v0, p0, Lcox;->m:Lcwx;

    iget v0, v0, Lcwx;->a:I

    if-ne v0, v4, :cond_0

    move v0, v1

    .line 178
    :goto_0
    iget v3, p1, Lcwx;->a:I

    if-ne v3, v4, :cond_1

    .line 180
    :goto_1
    iput-object p1, p0, Lcox;->m:Lcwx;

    .line 181
    iget-object v2, p0, Lcox;->d:Lcuy;

    iput-object p1, v2, Lcuy;->d:Lcwx;

    .line 183
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 184
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 185
    :goto_2
    if-eqz v0, :cond_3

    .line 186
    iget-object v1, v0, Lfoy;->H:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 187
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_2

    :cond_0
    move v0, v2

    .line 177
    goto :goto_0

    :cond_1
    move v1, v2

    .line 178
    goto :goto_1

    .line 189
    :cond_2
    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    .line 190
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 191
    :goto_3
    if-eqz v0, :cond_3

    .line 192
    iget-object v1, v0, Lfoy;->I:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 193
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_3

    .line 196
    :cond_3
    return-void
.end method

.method protected final a(Lcyz;)V
    .locals 1

    .prologue
    .line 508
    iget-object v0, p1, Lcyz;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcox;->a(Ljava/util/List;)Z

    .line 509
    return-void
.end method

.method protected final a(Lczx;)V
    .locals 2

    .prologue
    .line 354
    iget-wide v0, p1, Lczx;->a:J

    long-to-int v0, v0

    invoke-direct {p0, v0}, Lcox;->a(I)V

    .line 355
    return-void
.end method

.method protected final a(Ldad;)V
    .locals 2

    .prologue
    .line 347
    iget-boolean v0, p1, Ldad;->d:Z

    if-eqz v0, :cond_0

    .line 348
    iget-wide v0, p1, Ldad;->a:J

    long-to-int v0, v0

    invoke-direct {p0, v0}, Lcox;->a(I)V

    .line 350
    :cond_0
    return-void
.end method

.method public final a(Lesm;)V
    .locals 4

    .prologue
    .line 513
    iget-object v0, p0, Lcox;->b:Lfoy;

    iget-object v0, v0, Lfoy;->aj:Lfoo;

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lcox;->b:Lfoy;

    iget-object v0, v0, Lfoy;->aj:Lfoo;

    invoke-virtual {v0}, Lfoo;->b()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfos;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lfos;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Lesm;->a(Lfos;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0, v2}, Lcox;->a(Ljava/util/List;)Z

    .line 516
    :cond_1
    return-void
.end method

.method public final a(Lfpi;I)V
    .locals 3

    .prologue
    .line 470
    iget-object v0, p0, Lcox;->b:Lfoy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcox;->b:Lfoy;

    iget-object v0, v0, Lfoy;->ai:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcox;->b:Lfoy;

    iget-object v0, v0, Lfoy;->ai:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 479
    :cond_0
    return-void

    .line 474
    :cond_1
    iget-object v0, p1, Lfpi;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpu;

    .line 475
    iget v2, v0, Lfpu;->a:I

    if-ne v2, p2, :cond_2

    .line 476
    iget-object v0, v0, Lfpu;->b:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcox;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final a(Lfpi;Lfpm;)V
    .locals 2

    .prologue
    .line 485
    iget-object v0, p0, Lcox;->b:Lfoy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcox;->b:Lfoy;

    iget-object v0, v0, Lfoy;->ai:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcox;->b:Lfoy;

    iget-object v0, v0, Lfoy;->ai:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 492
    :cond_0
    return-void

    .line 489
    :cond_1
    iget-object v0, p2, Lfpm;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 490
    invoke-direct {p0, v0}, Lcox;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method protected final a(Lggp;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 317
    new-instance v1, Lcux;

    .line 318
    invoke-static {p1}, Lcuv;->a(Lggp;)Lcuv;

    move-result-object v0

    invoke-direct {v1, v0}, Lcux;-><init>(Lcuv;)V

    .line 319
    iget v0, p0, Lcox;->j:I

    if-eq v0, v3, :cond_1

    .line 322
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 323
    :goto_0
    if-eqz v0, :cond_0

    .line 324
    iget-object v2, v0, Lfoy;->D:Ljava/util/List;

    invoke-direct {p0, v2, v1}, Lcox;->a(Ljava/util/List;Lgod;)Z

    .line 325
    iget-object v2, v0, Lfoy;->L:Ljava/util/List;

    invoke-direct {p0, v2, v1}, Lcox;->a(Ljava/util/List;Lgod;)Z

    .line 326
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 328
    :cond_0
    iput v3, p0, Lcox;->j:I

    .line 330
    :cond_1
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 44
    check-cast p1, Lgkr;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Ping failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lezp;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcox;->c:Less;

    iget-object v0, v0, Less;->j:Ljava/util/List;

    invoke-direct {p0, v0}, Lcox;->a(Ljava/util/List;)Z

    .line 216
    return-void
.end method

.method public final b(Lcuv;)V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 230
    new-instance v1, Lcux;

    invoke-direct {v1, p1}, Lcux;-><init>(Lcuv;)V

    .line 231
    :goto_0
    if-eqz v0, :cond_0

    .line 232
    iget-object v2, v0, Lfoy;->L:Ljava/util/List;

    invoke-direct {p0, v2, v1}, Lcox;->a(Ljava/util/List;Lgod;)Z

    .line 233
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 235
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcox;->b:Lfoy;

    invoke-virtual {v0}, Lfoy;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcox;->g:Z

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lcox;->b:Lfoy;

    iget-object v0, v0, Lfoy;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcox;->a(Ljava/util/List;)Z

    .line 223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcox;->g:Z

    .line 225
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 239
    iget-boolean v0, p0, Lcox;->h:Z

    if-nez v0, :cond_1

    .line 240
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 241
    :goto_0
    if-eqz v0, :cond_0

    .line 242
    iget-object v1, v0, Lfoy;->A:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 243
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 245
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcox;->h:Z

    .line 247
    :cond_1
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 266
    :goto_0
    if-eqz v0, :cond_0

    .line 267
    iget-object v1, v0, Lfoy;->J:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 268
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 270
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 284
    iget-object v0, p0, Lcox;->d:Lcuy;

    iput-boolean v2, v0, Lcuy;->f:Z

    .line 285
    iget-boolean v0, p0, Lcox;->g:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcox;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    invoke-direct {p0}, Lcox;->p()V

    .line 287
    iput-boolean v2, p0, Lcox;->g:Z

    .line 288
    iget-object v0, p0, Lcox;->n:Lerr;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcox;->n:Lerr;

    iget-object v0, v0, Lerr;->a:Leri;

    sget-object v1, Lero;->a:Lero;

    invoke-virtual {v0, v1}, Leri;->a(Lero;)V

    .line 292
    :cond_0
    iget v0, p0, Lcox;->j:I

    if-nez v0, :cond_2

    .line 293
    iput v2, p0, Lcox;->j:I

    .line 304
    :cond_1
    :goto_0
    return-void

    .line 295
    :cond_2
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 296
    :goto_1
    if-eqz v0, :cond_3

    .line 297
    iget-object v1, v0, Lfoy;->F:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 298
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_1

    .line 300
    :cond_3
    iget-object v0, p0, Lcox;->n:Lerr;

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcox;->n:Lerr;

    iget-object v0, v0, Lerr;->a:Leri;

    sget-object v1, Lero;->f:Lero;

    invoke-virtual {v0, v1}, Leri;->a(Lero;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 309
    :goto_0
    if-eqz v0, :cond_0

    .line 310
    iget-object v1, v0, Lfoy;->D:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 311
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 313
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcox;->d:Lcuy;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcuy;->f:Z

    .line 335
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 336
    :goto_0
    if-eqz v0, :cond_0

    .line 337
    iget-object v1, v0, Lfoy;->E:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 338
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 340
    :cond_0
    iget-object v0, p0, Lcox;->n:Lerr;

    if-eqz v0, :cond_1

    .line 341
    iget-object v0, p0, Lcox;->n:Lerr;

    iget-object v0, v0, Lerr;->a:Leri;

    sget-object v1, Lero;->g:Lero;

    invoke-virtual {v0, v1}, Leri;->a(Lero;)V

    .line 343
    :cond_1
    return-void
.end method

.method public final handleAdCompanionClickEvent(Lcni;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 503
    const-string v0, "clickcompanionad"

    invoke-direct {p0, v0}, Lcox;->a(Ljava/lang/String;)V

    .line 504
    return-void
.end method

.method public final i()V
    .locals 4

    .prologue
    .line 420
    iget-object v0, p0, Lcox;->d:Lcuy;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcuy;->f:Z

    .line 421
    iget-object v0, p0, Lcox;->d:Lcuy;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcox;->b:Lfoy;

    iget v2, v2, Lfoy;->o:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    iput-wide v2, v0, Lcuy;->g:J

    .line 422
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 423
    iget-boolean v1, p0, Lcox;->f:Z

    if-nez v1, :cond_1

    .line 424
    :goto_0
    if-eqz v0, :cond_0

    .line 425
    iget-object v1, v0, Lfoy;->B:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 426
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 428
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcox;->f:Z

    .line 432
    :cond_1
    :goto_1
    iget-object v0, p0, Lcox;->l:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 433
    iget-object v0, p0, Lcox;->l:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpf;

    iget-object v0, v0, Lfpf;->c:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcox;->a(Landroid/net/Uri;)V

    goto :goto_1

    .line 435
    :cond_2
    iget-object v0, p0, Lcox;->b:Lfoy;

    .line 436
    :goto_2
    if-eqz v0, :cond_3

    .line 437
    iget-object v1, v0, Lfoy;->C:Ljava/util/List;

    invoke-direct {p0, v1}, Lcox;->a(Ljava/util/List;)Z

    .line 438
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_2

    .line 440
    :cond_3
    const/4 v0, 0x5

    iput v0, p0, Lcox;->j:I

    .line 441
    iget-object v0, p0, Lcox;->n:Lerr;

    if-eqz v0, :cond_4

    .line 442
    iget-object v0, p0, Lcox;->n:Lerr;

    iget-object v0, v0, Lerr;->a:Leri;

    sget-object v1, Lero;->e:Lero;

    invoke-virtual {v0, v1}, Leri;->a(Lero;)V

    .line 444
    :cond_4
    return-void
.end method

.method public final j()Lcnj;
    .locals 8

    .prologue
    .line 448
    new-instance v0, Lcnj;

    iget v1, p0, Lcox;->j:I

    iget-boolean v2, p0, Lcox;->f:Z

    iget-boolean v3, p0, Lcox;->g:Z

    iget-boolean v4, p0, Lcox;->h:Z

    iget-object v5, p0, Lcox;->i:Ljava/util/List;

    iget v6, p0, Lcox;->k:I

    sget-object v7, Lcnl;->a:Lcnl;

    invoke-direct/range {v0 .. v7}, Lcnj;-><init>(IZZZLjava/util/List;ILcnl;)V

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 464
    const-string v0, "clickchannel"

    invoke-direct {p0, v0}, Lcox;->a(Ljava/lang/String;)V

    .line 465
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcox;->n:Lerr;

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Lcox;->n:Lerr;

    iget-object v0, v0, Lerr;->a:Leri;

    sget-object v1, Lero;->g:Lero;

    invoke-virtual {v0, v1}, Leri;->a(Lero;)V

    .line 499
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 145
    invoke-super {p0}, Lcnh;->m()V

    .line 146
    iget-object v0, p0, Lcox;->o:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 147
    iget-object v0, p0, Lcox;->d:Lcuy;

    iget-object v1, p0, Lcox;->n:Lerr;

    iput-object v1, v0, Lcuy;->e:Lerr;

    .line 149
    iget-object v0, p0, Lcox;->n:Lerr;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcox;->n:Lerr;

    iput-object p0, v0, Lerr;->b:Leru;

    .line 152
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 156
    invoke-super {p0}, Lcnh;->n()V

    .line 157
    iget-object v0, p0, Lcox;->d:Lcuy;

    iput-object v2, v0, Lcuy;->e:Lerr;

    .line 158
    iget-object v0, p0, Lcox;->n:Lerr;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcox;->n:Lerr;

    iget-object v0, v0, Lerr;->a:Leri;

    invoke-virtual {v0}, Leri;->b()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Leri;->b:Z

    .line 160
    iget-object v0, p0, Lcox;->n:Lerr;

    iput-object v2, v0, Lerr;->b:Leru;

    .line 162
    :cond_0
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcox;->b:Lfoy;

    iget-object v0, v0, Lfoy;->ak:Ljava/util/List;

    invoke-direct {p0, v0}, Lcox;->a(Ljava/util/List;)Z

    .line 521
    return-void
.end method
