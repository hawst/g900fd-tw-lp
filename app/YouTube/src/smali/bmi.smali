.class public final Lbmi;
.super Lbmf;
.source "SourceFile"


# instance fields
.field private final e:Ldnb;


# direct methods
.method public constructor <init>(Lgml;Lfet;Lcwg;Lftg;Leuk;Lezj;Lgjm;Ldnb;Ldix;ILgku;Ljava/io/File;Lggn;)V
    .locals 13

    .prologue
    .line 50
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    move/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    invoke-direct/range {v0 .. v12}, Lbmf;-><init>(Lgml;Lfet;Lcwg;Lftg;Leuk;Lezj;Lgjm;Ldix;ILgku;Ljava/io/File;Lggn;)V

    .line 62
    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnb;

    iput-object v0, p0, Lbmi;->e:Ldnb;

    .line 63
    return-void
.end method


# virtual methods
.method protected final a(J)V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lbmi;->e:Ldnb;

    iget-object v1, p0, Lbmi;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Ldnb;->a(Ljava/lang/String;J)V

    .line 80
    return-void
.end method

.method protected final a(JJ)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lbmi;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x44

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "offline ad task["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] progress "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lbmi;->a:Lgml;

    iget-object v1, p0, Lbmi;->c:Ljava/lang/String;

    sget-object v2, Lglv;->c:Lglv;

    invoke-virtual {v0, v1, v2}, Lgml;->b(Ljava/lang/String;Lglv;)V

    .line 87
    iget-object v0, p0, Lbmi;->e:Ldnb;

    iget-object v1, p0, Lbmi;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Ldnb;->b(Ljava/lang/String;J)V

    .line 88
    return-void
.end method

.method protected final a(Lfrl;)V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V
    .locals 5

    .prologue
    .line 100
    if-nez p2, :cond_0

    .line 101
    new-instance v0, Ldmt;

    iget-boolean v1, p3, Lglv;->k:Z

    invoke-direct {v0, p1, v1}, Ldmt;-><init>(Ljava/lang/String;Z)V

    .line 107
    :goto_0
    iget-boolean v1, p3, Lglv;->k:Z

    if-eqz v1, :cond_1

    .line 108
    iget-object v1, p0, Lbmi;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "offline ad task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 113
    :goto_1
    iget-object v1, p0, Lbmi;->a:Lgml;

    iget-object v2, p0, Lbmi;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, p3}, Lgml;->b(Ljava/lang/String;Lglv;)V

    .line 114
    iget-object v1, p0, Lbmi;->e:Ldnb;

    iget-object v2, p0, Lbmi;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    .line 115
    return-void

    .line 103
    :cond_0
    new-instance v0, Ldmt;

    iget-boolean v1, p3, Lglv;->k:Z

    invoke-direct {v0, p1, p2, v1}, Ldmt;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 110
    :cond_1
    iget-object v1, p0, Lbmi;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "offline ad task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected final a()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lbmi;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "offline ad task["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] success"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lbmi;->a:Lgml;

    iget-object v1, p0, Lbmi;->c:Ljava/lang/String;

    sget-object v2, Lglv;->b:Lglv;

    invoke-virtual {v0, v1, v2}, Lgml;->b(Ljava/lang/String;Lglv;)V

    .line 94
    iget-object v0, p0, Lbmi;->e:Ldnb;

    iget-object v1, p0, Lbmi;->b:Ljava/lang/String;

    new-instance v2, Lgje;

    invoke-direct {v2}, Lgje;-><init>()V

    invoke-interface {v0, v1, v2}, Ldnb;->a(Ljava/lang/String;Lgje;)V

    .line 95
    return-void
.end method
