.class public final Lgyn;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgyn;


# instance fields
.field private b:I

.field private c:Lhwl;

.field private d:Ljava/lang/String;

.field private e:Lhip;

.field private f:Lhir;

.field private g:[Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Libt;

.field private l:Licq;

.field private m:Lhpx;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3814
    const/4 v0, 0x0

    new-array v0, v0, [Lgyn;

    sput-object v0, Lgyn;->a:[Lgyn;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 3815
    invoke-direct {p0}, Lidf;-><init>()V

    .line 3818
    iput v2, p0, Lgyn;->b:I

    .line 3821
    iput-object v1, p0, Lgyn;->c:Lhwl;

    .line 3824
    const-string v0, ""

    iput-object v0, p0, Lgyn;->d:Ljava/lang/String;

    .line 3827
    iput-object v1, p0, Lgyn;->e:Lhip;

    .line 3830
    iput-object v1, p0, Lgyn;->f:Lhir;

    .line 3833
    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Lgyn;->g:[Ljava/lang/String;

    .line 3836
    const-string v0, ""

    iput-object v0, p0, Lgyn;->h:Ljava/lang/String;

    .line 3839
    iput v2, p0, Lgyn;->i:I

    .line 3842
    iput v2, p0, Lgyn;->j:I

    .line 3845
    iput-object v1, p0, Lgyn;->k:Libt;

    .line 3848
    iput-object v1, p0, Lgyn;->l:Licq;

    .line 3851
    iput-object v1, p0, Lgyn;->m:Lhpx;

    .line 3854
    const-string v0, ""

    iput-object v0, p0, Lgyn;->n:Ljava/lang/String;

    .line 3857
    const-string v0, ""

    iput-object v0, p0, Lgyn;->o:Ljava/lang/String;

    .line 3815
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3979
    .line 3980
    iget v0, p0, Lgyn;->b:I

    if-eqz v0, :cond_e

    .line 3981
    const/4 v0, 0x1

    iget v2, p0, Lgyn;->b:I

    .line 3982
    invoke-static {v0, v2}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3984
    :goto_0
    iget-object v2, p0, Lgyn;->c:Lhwl;

    if-eqz v2, :cond_0

    .line 3985
    const/4 v2, 0x2

    iget-object v3, p0, Lgyn;->c:Lhwl;

    .line 3986
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3988
    :cond_0
    iget-object v2, p0, Lgyn;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 3989
    const/4 v2, 0x3

    iget-object v3, p0, Lgyn;->d:Ljava/lang/String;

    .line 3990
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3992
    :cond_1
    iget-object v2, p0, Lgyn;->e:Lhip;

    if-eqz v2, :cond_2

    .line 3993
    const/4 v2, 0x4

    iget-object v3, p0, Lgyn;->e:Lhip;

    .line 3994
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3996
    :cond_2
    iget-object v2, p0, Lgyn;->f:Lhir;

    if-eqz v2, :cond_3

    .line 3997
    const/4 v2, 0x5

    iget-object v3, p0, Lgyn;->f:Lhir;

    .line 3998
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4000
    :cond_3
    iget-object v2, p0, Lgyn;->g:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgyn;->g:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 4002
    iget-object v3, p0, Lgyn;->g:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 4004
    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 4002
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4006
    :cond_4
    add-int/2addr v0, v2

    .line 4007
    iget-object v1, p0, Lgyn;->g:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4009
    :cond_5
    iget-object v1, p0, Lgyn;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 4010
    const/4 v1, 0x7

    iget-object v2, p0, Lgyn;->h:Ljava/lang/String;

    .line 4011
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4013
    :cond_6
    iget v1, p0, Lgyn;->i:I

    if-eqz v1, :cond_7

    .line 4014
    const/16 v1, 0x8

    iget v2, p0, Lgyn;->i:I

    .line 4015
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4017
    :cond_7
    iget v1, p0, Lgyn;->j:I

    if-eqz v1, :cond_8

    .line 4018
    const/16 v1, 0x9

    iget v2, p0, Lgyn;->j:I

    .line 4019
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4021
    :cond_8
    iget-object v1, p0, Lgyn;->k:Libt;

    if-eqz v1, :cond_9

    .line 4022
    const/16 v1, 0xa

    iget-object v2, p0, Lgyn;->k:Libt;

    .line 4023
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4025
    :cond_9
    iget-object v1, p0, Lgyn;->l:Licq;

    if-eqz v1, :cond_a

    .line 4026
    const/16 v1, 0xb

    iget-object v2, p0, Lgyn;->l:Licq;

    .line 4027
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4029
    :cond_a
    iget-object v1, p0, Lgyn;->m:Lhpx;

    if-eqz v1, :cond_b

    .line 4030
    const/16 v1, 0xc

    iget-object v2, p0, Lgyn;->m:Lhpx;

    .line 4031
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4033
    :cond_b
    iget-object v1, p0, Lgyn;->n:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 4034
    const/16 v1, 0xd

    iget-object v2, p0, Lgyn;->n:Ljava/lang/String;

    .line 4035
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4037
    :cond_c
    iget-object v1, p0, Lgyn;->o:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 4038
    const/16 v1, 0xe

    iget-object v2, p0, Lgyn;->o:Ljava/lang/String;

    .line 4039
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4041
    :cond_d
    iget-object v1, p0, Lgyn;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4042
    iput v0, p0, Lgyn;->J:I

    .line 4043
    return v0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3811
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyn;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyn;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyn;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lgyn;->b:I

    goto :goto_0

    :cond_3
    iput v3, p0, Lgyn;->b:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgyn;->c:Lhwl;

    if-nez v0, :cond_4

    new-instance v0, Lhwl;

    invoke-direct {v0}, Lhwl;-><init>()V

    iput-object v0, p0, Lgyn;->c:Lhwl;

    :cond_4
    iget-object v0, p0, Lgyn;->c:Lhwl;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgyn;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lgyn;->e:Lhip;

    if-nez v0, :cond_5

    new-instance v0, Lhip;

    invoke-direct {v0}, Lhip;-><init>()V

    iput-object v0, p0, Lgyn;->e:Lhip;

    :cond_5
    iget-object v0, p0, Lgyn;->e:Lhip;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lgyn;->f:Lhir;

    if-nez v0, :cond_6

    new-instance v0, Lhir;

    invoke-direct {v0}, Lhir;-><init>()V

    iput-object v0, p0, Lgyn;->f:Lhir;

    :cond_6
    iget-object v0, p0, Lgyn;->f:Lhir;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lgyn;->g:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lgyn;->g:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lgyn;->g:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lgyn;->g:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lgyn;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lgyn;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgyn;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lgyn;->i:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lgyn;->j:I

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lgyn;->k:Libt;

    if-nez v0, :cond_8

    new-instance v0, Libt;

    invoke-direct {v0}, Libt;-><init>()V

    iput-object v0, p0, Lgyn;->k:Libt;

    :cond_8
    iget-object v0, p0, Lgyn;->k:Libt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lgyn;->l:Licq;

    if-nez v0, :cond_9

    new-instance v0, Licq;

    invoke-direct {v0}, Licq;-><init>()V

    iput-object v0, p0, Lgyn;->l:Licq;

    :cond_9
    iget-object v0, p0, Lgyn;->l:Licq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lgyn;->m:Lhpx;

    if-nez v0, :cond_a

    new-instance v0, Lhpx;

    invoke-direct {v0}, Lhpx;-><init>()V

    iput-object v0, p0, Lgyn;->m:Lhpx;

    :cond_a
    iget-object v0, p0, Lgyn;->m:Lhpx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgyn;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgyn;->o:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 3929
    iget v0, p0, Lgyn;->b:I

    if-eqz v0, :cond_0

    .line 3930
    const/4 v0, 0x1

    iget v1, p0, Lgyn;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 3932
    :cond_0
    iget-object v0, p0, Lgyn;->c:Lhwl;

    if-eqz v0, :cond_1

    .line 3933
    const/4 v0, 0x2

    iget-object v1, p0, Lgyn;->c:Lhwl;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 3935
    :cond_1
    iget-object v0, p0, Lgyn;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3936
    const/4 v0, 0x3

    iget-object v1, p0, Lgyn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 3938
    :cond_2
    iget-object v0, p0, Lgyn;->e:Lhip;

    if-eqz v0, :cond_3

    .line 3939
    const/4 v0, 0x4

    iget-object v1, p0, Lgyn;->e:Lhip;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 3941
    :cond_3
    iget-object v0, p0, Lgyn;->f:Lhir;

    if-eqz v0, :cond_4

    .line 3942
    const/4 v0, 0x5

    iget-object v1, p0, Lgyn;->f:Lhir;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 3944
    :cond_4
    iget-object v0, p0, Lgyn;->g:[Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 3945
    iget-object v1, p0, Lgyn;->g:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 3946
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    .line 3945
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3949
    :cond_5
    iget-object v0, p0, Lgyn;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 3950
    const/4 v0, 0x7

    iget-object v1, p0, Lgyn;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 3952
    :cond_6
    iget v0, p0, Lgyn;->i:I

    if-eqz v0, :cond_7

    .line 3953
    const/16 v0, 0x8

    iget v1, p0, Lgyn;->i:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 3955
    :cond_7
    iget v0, p0, Lgyn;->j:I

    if-eqz v0, :cond_8

    .line 3956
    const/16 v0, 0x9

    iget v1, p0, Lgyn;->j:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 3958
    :cond_8
    iget-object v0, p0, Lgyn;->k:Libt;

    if-eqz v0, :cond_9

    .line 3959
    const/16 v0, 0xa

    iget-object v1, p0, Lgyn;->k:Libt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 3961
    :cond_9
    iget-object v0, p0, Lgyn;->l:Licq;

    if-eqz v0, :cond_a

    .line 3962
    const/16 v0, 0xb

    iget-object v1, p0, Lgyn;->l:Licq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 3964
    :cond_a
    iget-object v0, p0, Lgyn;->m:Lhpx;

    if-eqz v0, :cond_b

    .line 3965
    const/16 v0, 0xc

    iget-object v1, p0, Lgyn;->m:Lhpx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 3967
    :cond_b
    iget-object v0, p0, Lgyn;->n:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 3968
    const/16 v0, 0xd

    iget-object v1, p0, Lgyn;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 3970
    :cond_c
    iget-object v0, p0, Lgyn;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 3971
    const/16 v0, 0xe

    iget-object v1, p0, Lgyn;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 3973
    :cond_d
    iget-object v0, p0, Lgyn;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 3975
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3881
    if-ne p1, p0, :cond_1

    .line 3898
    :cond_0
    :goto_0
    return v0

    .line 3882
    :cond_1
    instance-of v2, p1, Lgyn;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 3883
    :cond_2
    check-cast p1, Lgyn;

    .line 3884
    iget v2, p0, Lgyn;->b:I

    iget v3, p1, Lgyn;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyn;->c:Lhwl;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyn;->c:Lhwl;

    if-nez v2, :cond_3

    .line 3885
    :goto_1
    iget-object v2, p0, Lgyn;->d:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyn;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 3886
    :goto_2
    iget-object v2, p0, Lgyn;->e:Lhip;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgyn;->e:Lhip;

    if-nez v2, :cond_3

    .line 3887
    :goto_3
    iget-object v2, p0, Lgyn;->f:Lhir;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgyn;->f:Lhir;

    if-nez v2, :cond_3

    .line 3888
    :goto_4
    iget-object v2, p0, Lgyn;->g:[Ljava/lang/String;

    iget-object v3, p1, Lgyn;->g:[Ljava/lang/String;

    .line 3889
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgyn;->h:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Lgyn;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 3890
    :goto_5
    iget v2, p0, Lgyn;->i:I

    iget v3, p1, Lgyn;->i:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgyn;->j:I

    iget v3, p1, Lgyn;->j:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyn;->k:Libt;

    if-nez v2, :cond_9

    iget-object v2, p1, Lgyn;->k:Libt;

    if-nez v2, :cond_3

    .line 3893
    :goto_6
    iget-object v2, p0, Lgyn;->l:Licq;

    if-nez v2, :cond_a

    iget-object v2, p1, Lgyn;->l:Licq;

    if-nez v2, :cond_3

    .line 3894
    :goto_7
    iget-object v2, p0, Lgyn;->m:Lhpx;

    if-nez v2, :cond_b

    iget-object v2, p1, Lgyn;->m:Lhpx;

    if-nez v2, :cond_3

    .line 3895
    :goto_8
    iget-object v2, p0, Lgyn;->n:Ljava/lang/String;

    if-nez v2, :cond_c

    iget-object v2, p1, Lgyn;->n:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 3896
    :goto_9
    iget-object v2, p0, Lgyn;->o:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Lgyn;->o:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 3897
    :goto_a
    iget-object v2, p0, Lgyn;->I:Ljava/util/List;

    if-nez v2, :cond_e

    iget-object v2, p1, Lgyn;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 3898
    goto :goto_0

    .line 3884
    :cond_4
    iget-object v2, p0, Lgyn;->c:Lhwl;

    iget-object v3, p1, Lgyn;->c:Lhwl;

    .line 3885
    invoke-virtual {v2, v3}, Lhwl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyn;->d:Ljava/lang/String;

    iget-object v3, p1, Lgyn;->d:Ljava/lang/String;

    .line 3886
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgyn;->e:Lhip;

    iget-object v3, p1, Lgyn;->e:Lhip;

    .line 3887
    invoke-virtual {v2, v3}, Lhip;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgyn;->f:Lhir;

    iget-object v3, p1, Lgyn;->f:Lhir;

    .line 3888
    invoke-virtual {v2, v3}, Lhir;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    .line 3889
    :cond_8
    iget-object v2, p0, Lgyn;->h:Ljava/lang/String;

    iget-object v3, p1, Lgyn;->h:Ljava/lang/String;

    .line 3890
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lgyn;->k:Libt;

    iget-object v3, p1, Lgyn;->k:Libt;

    .line 3893
    invoke-virtual {v2, v3}, Libt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lgyn;->l:Licq;

    iget-object v3, p1, Lgyn;->l:Licq;

    .line 3894
    invoke-virtual {v2, v3}, Licq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lgyn;->m:Lhpx;

    iget-object v3, p1, Lgyn;->m:Lhpx;

    .line 3895
    invoke-virtual {v2, v3}, Lhpx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Lgyn;->n:Ljava/lang/String;

    iget-object v3, p1, Lgyn;->n:Ljava/lang/String;

    .line 3896
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_9

    :cond_d
    iget-object v2, p0, Lgyn;->o:Ljava/lang/String;

    iget-object v3, p1, Lgyn;->o:Ljava/lang/String;

    .line 3897
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_a

    :cond_e
    iget-object v2, p0, Lgyn;->I:Ljava/util/List;

    iget-object v3, p1, Lgyn;->I:Ljava/util/List;

    .line 3898
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3902
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 3904
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgyn;->b:I

    add-int/2addr v0, v2

    .line 3905
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyn;->c:Lhwl;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 3906
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyn;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3907
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyn;->e:Lhip;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3908
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyn;->f:Lhir;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3909
    iget-object v2, p0, Lgyn;->g:[Ljava/lang/String;

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    .line 3915
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgyn;->h:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 3916
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgyn;->i:I

    add-int/2addr v0, v2

    .line 3917
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgyn;->j:I

    add-int/2addr v0, v2

    .line 3918
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyn;->k:Libt;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 3919
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyn;->l:Licq;

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 3920
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyn;->m:Lhpx;

    if-nez v0, :cond_a

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 3921
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyn;->n:Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 3922
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyn;->o:Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 3923
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgyn;->I:Ljava/util/List;

    if-nez v2, :cond_d

    :goto_a
    add-int/2addr v0, v1

    .line 3924
    return v0

    .line 3905
    :cond_1
    iget-object v0, p0, Lgyn;->c:Lhwl;

    invoke-virtual {v0}, Lhwl;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3906
    :cond_2
    iget-object v0, p0, Lgyn;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3907
    :cond_3
    iget-object v0, p0, Lgyn;->e:Lhip;

    invoke-virtual {v0}, Lhip;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3908
    :cond_4
    iget-object v0, p0, Lgyn;->f:Lhir;

    invoke-virtual {v0}, Lhir;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_5
    move v2, v0

    move v0, v1

    .line 3911
    :goto_b
    iget-object v3, p0, Lgyn;->g:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 3912
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgyn;->g:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    move v2, v1

    :goto_c
    add-int/2addr v2, v3

    .line 3911
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 3912
    :cond_6
    iget-object v2, p0, Lgyn;->g:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_c

    .line 3915
    :cond_7
    iget-object v0, p0, Lgyn;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 3918
    :cond_8
    iget-object v0, p0, Lgyn;->k:Libt;

    invoke-virtual {v0}, Libt;->hashCode()I

    move-result v0

    goto :goto_5

    .line 3919
    :cond_9
    iget-object v0, p0, Lgyn;->l:Licq;

    invoke-virtual {v0}, Licq;->hashCode()I

    move-result v0

    goto :goto_6

    .line 3920
    :cond_a
    iget-object v0, p0, Lgyn;->m:Lhpx;

    invoke-virtual {v0}, Lhpx;->hashCode()I

    move-result v0

    goto :goto_7

    .line 3921
    :cond_b
    iget-object v0, p0, Lgyn;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 3922
    :cond_c
    iget-object v0, p0, Lgyn;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    .line 3923
    :cond_d
    iget-object v1, p0, Lgyn;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_a
.end method
