.class public final Lftb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lgiq;

.field private final b:Lewi;

.field private final c:Ljava/util/List;

.field private final d:Ljava/util/List;

.field private final e:Lghm;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Lewi;


# direct methods
.method public constructor <init>(Lgiq;Lewi;Ljava/util/List;Ljava/util/List;Lghm;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 97
    new-instance v8, Lftc;

    invoke-direct {v8}, Lftc;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lftb;-><init>(Lgiq;Lewi;Ljava/util/List;Ljava/util/List;Lghm;Ljava/lang/String;Ljava/lang/String;Lewi;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Lgiq;Lewi;Ljava/util/List;Ljava/util/List;Lghm;Ljava/lang/String;Ljava/lang/String;Lewi;)V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgiq;

    iput-object v0, p0, Lftb;->a:Lgiq;

    .line 123
    iput-object p2, p0, Lftb;->b:Lewi;

    .line 124
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lftb;->c:Ljava/util/List;

    .line 125
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lftb;->d:Ljava/util/List;

    .line 126
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghm;

    iput-object v0, p0, Lftb;->e:Lghm;

    .line 127
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Lftb;->h:Lewi;

    .line 128
    iput-object p6, p0, Lftb;->f:Ljava/lang/String;

    .line 129
    iput-object p7, p0, Lftb;->g:Ljava/lang/String;

    .line 130
    return-void
.end method


# virtual methods
.method public final a(Lftd;Ljava/lang/Class;Lwv;)Lfta;
    .locals 11

    .prologue
    .line 142
    new-instance v0, Lfta;

    iget-object v4, p0, Lftb;->a:Lgiq;

    iget-object v5, p0, Lftb;->c:Ljava/util/List;

    iget-object v6, p0, Lftb;->d:Ljava/util/List;

    iget-object v7, p0, Lftb;->e:Lghm;

    iget-object v8, p0, Lftb;->f:Ljava/lang/String;

    iget-object v9, p0, Lftb;->g:Ljava/lang/String;

    iget-object v1, p0, Lftb;->h:Lewi;

    .line 152
    invoke-interface {v1}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lwr;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v10}, Lfta;-><init>(Lftd;Ljava/lang/Class;Lwv;Lgiq;Ljava/util/List;Ljava/util/List;Lghm;Ljava/lang/String;Ljava/lang/String;Lwr;)V

    .line 153
    iget-object v1, p0, Lftb;->b:Lewi;

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lftb;->b:Lewi;

    invoke-interface {v1}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lwx;

    iput-object v1, v0, Lwp;->h:Lwx;

    .line 156
    :cond_0
    invoke-interface {p1}, Lftd;->e()Z

    move-result v1

    iput-boolean v1, v0, Lwp;->e:Z

    .line 157
    return-object v0
.end method
