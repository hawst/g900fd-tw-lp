.class public final Lcnw;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Ljava/util/regex/Pattern;

.field private final c:Lgjp;

.field private final d:Lgjp;

.field private final e:Lezj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "^NO_MATCH_REGEX$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcnw;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lgjp;Lgjp;Ljava/util/regex/Pattern;Lezj;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lcnw;->c:Lgjp;

    .line 55
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lcnw;->d:Lgjp;

    .line 56
    if-nez p3, :cond_0

    sget-object p3, Lcnw;->a:Ljava/util/regex/Pattern;

    :cond_0
    iput-object p3, p0, Lcnw;->b:Ljava/util/regex/Pattern;

    .line 58
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcnw;->e:Lezj;

    .line 59
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;)Lgjt;
    .locals 3

    .prologue
    const v2, 0x37046bc

    .line 66
    iget-object v0, p0, Lcnw;->b:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnw;->c:Lgjp;

    .line 67
    invoke-static {p2, v2}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v0

    .line 70
    :goto_0
    invoke-virtual {v0, p1}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    move-result-object v0

    return-object v0

    .line 67
    :cond_0
    iget-object v0, p0, Lcnw;->d:Lgjp;

    .line 69
    invoke-static {p2, v2}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lgjt;Lwu;)V
    .locals 6

    .prologue
    .line 86
    iget-object v0, p0, Lcnw;->b:Ljava/util/regex/Pattern;

    iget-object v1, p1, Lgjt;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcnw;->c:Lgjp;

    invoke-virtual {v0, p1, p2}, Lgjp;->a(Lgjt;Lwu;)V

    .line 101
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p1, Lgjt;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".doubleclick.net"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "doubleclick.net"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 93
    iget-boolean v0, p1, Lgjt;->d:Z

    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p1, Lgjt;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ts"

    iget-object v2, p0, Lcnw;->e:Lezj;

    .line 96
    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 94
    invoke-virtual {p1, v0}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    .line 100
    :cond_2
    iget-object v0, p0, Lcnw;->d:Lgjp;

    invoke-virtual {v0, p1, p2}, Lgjp;->a(Lgjt;Lwu;)V

    goto :goto_0

    .line 92
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
