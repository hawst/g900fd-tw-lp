.class public final Lfif;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfqh;


# instance fields
.field public a:Lhul;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public b:Lgya;

.field public c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lgya;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgya;

    iput-object v0, p0, Lfif;->b:Lgya;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lfif;->a:Lhul;

    .line 35
    return-void
.end method

.method public constructor <init>(Lhul;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhul;

    iput-object v0, p0, Lfif;->a:Lhul;

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lfif;->b:Lgya;

    .line 28
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfif;->a:Lhul;

    iget-object v1, v1, Lhul;->a:[Lhun;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfif;->c:Ljava/util/List;

    .line 61
    iget-object v0, p0, Lfif;->a:Lhul;

    iget-object v1, v0, Lhul;->a:[Lhun;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 62
    iget-object v4, v3, Lhun;->h:Lgxw;

    if-eqz v4, :cond_0

    .line 63
    iget-object v3, v3, Lhun;->h:Lgxw;

    invoke-virtual {p0, v3}, Lfif;->a(Lgxw;)V

    .line 61
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    iget-object v0, p0, Lfif;->c:Ljava/util/List;

    return-object v0
.end method

.method public a(Lgxw;)V
    .locals 6

    .prologue
    .line 70
    iget-object v0, p1, Lgxw;->b:Lgxv;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p1, Lgxw;->b:Lgxv;

    iget-object v0, v0, Lgxv;->a:Lgxu;

    .line 73
    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Lfif;->c:Ljava/util/List;

    new-instance v2, Lfie;

    invoke-direct {v2, v0}, Lfie;-><init>(Lgxu;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_0
    iget-object v1, p1, Lgxw;->a:[Lgxy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 79
    iget-object v4, v3, Lgxy;->b:Lgxt;

    if-eqz v4, :cond_1

    .line 80
    iget-object v4, p0, Lfif;->c:Ljava/util/List;

    new-instance v5, Lfid;

    iget-object v3, v3, Lgxy;->b:Lgxt;

    invoke-direct {v5, v3, p0}, Lfid;-><init>(Lgxt;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_2
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfif;->b:Lgya;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfif;->a:Lhul;

    iget-object v0, v0, Lhul;->c:[B

    goto :goto_0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method
