.class public final Lhsp;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lhxf;

.field public c:Lhgz;

.field public d:Lhgz;

.field public e:Lhgz;

.field public f:Lhog;

.field public g:[Lhbi;

.field public h:Ljava/lang/String;

.field public i:Lhso;

.field public j:[B

.field private k:Lhgz;

.field private l:Ljava/lang/String;

.field private m:J

.field private n:F

.field private o:F

.field private p:[Lhut;

.field private q:Z

.field private r:Lhnh;

.field private s:Lhxf;

.field private t:Lhog;

.field private u:Z

.field private v:Lhkm;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lhsp;->a:Ljava/lang/String;

    iput-object v2, p0, Lhsp;->b:Lhxf;

    iput-object v2, p0, Lhsp;->c:Lhgz;

    iput-object v2, p0, Lhsp;->k:Lhgz;

    iput-object v2, p0, Lhsp;->d:Lhgz;

    iput-object v2, p0, Lhsp;->e:Lhgz;

    iput-object v2, p0, Lhsp;->f:Lhog;

    const-string v0, ""

    iput-object v0, p0, Lhsp;->l:Ljava/lang/String;

    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lhsp;->g:[Lhbi;

    const-string v0, ""

    iput-object v0, p0, Lhsp;->h:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhsp;->m:J

    iput v3, p0, Lhsp;->n:F

    iput v3, p0, Lhsp;->o:F

    sget-object v0, Lhut;->a:[Lhut;

    iput-object v0, p0, Lhsp;->p:[Lhut;

    iput-object v2, p0, Lhsp;->i:Lhso;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhsp;->j:[B

    iput-boolean v4, p0, Lhsp;->q:Z

    iput-object v2, p0, Lhsp;->r:Lhnh;

    iput-object v2, p0, Lhsp;->s:Lhxf;

    iput-object v2, p0, Lhsp;->t:Lhog;

    iput-boolean v4, p0, Lhsp;->u:Z

    iput-object v2, p0, Lhsp;->v:Lhkm;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 8

    const/4 v7, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lhsp;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    const/4 v0, 0x1

    iget-object v2, p0, Lhsp;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhsp;->b:Lhxf;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhsp;->b:Lhxf;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhsp;->c:Lhgz;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lhsp;->c:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhsp;->k:Lhgz;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lhsp;->k:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhsp;->d:Lhgz;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lhsp;->d:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhsp;->e:Lhgz;

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Lhsp;->e:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhsp;->f:Lhog;

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lhsp;->f:Lhog;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lhsp;->l:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const/16 v2, 0x8

    iget-object v3, p0, Lhsp;->l:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lhsp;->g:[Lhbi;

    if-eqz v2, :cond_8

    iget-object v3, p0, Lhsp;->g:[Lhbi;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_8

    aget-object v5, v3, v2

    if-eqz v5, :cond_7

    const/16 v6, 0x9

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_8
    iget-object v2, p0, Lhsp;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const/16 v2, 0xa

    iget-object v3, p0, Lhsp;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-wide v2, p0, Lhsp;->m:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    iget-wide v4, p0, Lhsp;->m:J

    invoke-static {v2, v4, v5}, Lidd;->c(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget v2, p0, Lhsp;->n:F

    cmpl-float v2, v2, v7

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    iget v3, p0, Lhsp;->n:F

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    :cond_b
    iget v2, p0, Lhsp;->o:F

    cmpl-float v2, v2, v7

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    iget v3, p0, Lhsp;->o:F

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    :cond_c
    iget-object v2, p0, Lhsp;->p:[Lhut;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lhsp;->p:[Lhut;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_e

    aget-object v4, v2, v1

    if-eqz v4, :cond_d

    const/16 v5, 0xe

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_e
    iget-object v1, p0, Lhsp;->i:Lhso;

    if-eqz v1, :cond_f

    const/16 v1, 0xf

    iget-object v2, p0, Lhsp;->i:Lhso;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Lhsp;->j:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_10

    const/16 v1, 0x11

    iget-object v2, p0, Lhsp;->j:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-boolean v1, p0, Lhsp;->q:Z

    if-eqz v1, :cond_11

    const/16 v1, 0x12

    iget-boolean v2, p0, Lhsp;->q:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_11
    iget-object v1, p0, Lhsp;->r:Lhnh;

    if-eqz v1, :cond_12

    const/16 v1, 0x13

    iget-object v2, p0, Lhsp;->r:Lhnh;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget-object v1, p0, Lhsp;->s:Lhxf;

    if-eqz v1, :cond_13

    const/16 v1, 0x14

    iget-object v2, p0, Lhsp;->s:Lhxf;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-object v1, p0, Lhsp;->t:Lhog;

    if-eqz v1, :cond_14

    const/16 v1, 0x15

    iget-object v2, p0, Lhsp;->t:Lhog;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget-boolean v1, p0, Lhsp;->u:Z

    if-eqz v1, :cond_15

    const/16 v1, 0x16

    iget-boolean v2, p0, Lhsp;->u:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_15
    iget-object v1, p0, Lhsp;->v:Lhkm;

    if-eqz v1, :cond_16

    const v1, 0x39d6d44

    iget-object v2, p0, Lhsp;->v:Lhkm;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    iget-object v1, p0, Lhsp;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhsp;->J:I

    return v0

    :cond_17
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhsp;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhsp;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhsp;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsp;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhsp;->b:Lhxf;

    if-nez v0, :cond_2

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhsp;->b:Lhxf;

    :cond_2
    iget-object v0, p0, Lhsp;->b:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhsp;->c:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsp;->c:Lhgz;

    :cond_3
    iget-object v0, p0, Lhsp;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhsp;->k:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsp;->k:Lhgz;

    :cond_4
    iget-object v0, p0, Lhsp;->k:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhsp;->d:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsp;->d:Lhgz;

    :cond_5
    iget-object v0, p0, Lhsp;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhsp;->e:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsp;->e:Lhgz;

    :cond_6
    iget-object v0, p0, Lhsp;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lhsp;->f:Lhog;

    if-nez v0, :cond_7

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhsp;->f:Lhog;

    :cond_7
    iget-object v0, p0, Lhsp;->f:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsp;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsp;->g:[Lhbi;

    if-nez v0, :cond_9

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lhsp;->g:[Lhbi;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lhsp;->g:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Lhsp;->g:[Lhbi;

    :goto_2
    iget-object v2, p0, Lhsp;->g:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lhsp;->g:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsp;->g:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lhsp;->g:[Lhbi;

    array-length v0, v0

    goto :goto_1

    :cond_a
    iget-object v2, p0, Lhsp;->g:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsp;->g:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsp;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lhsp;->m:J

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhsp;->n:F

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhsp;->o:F

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x72

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsp;->p:[Lhut;

    if-nez v0, :cond_c

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhut;

    iget-object v3, p0, Lhsp;->p:[Lhut;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lhsp;->p:[Lhut;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Lhsp;->p:[Lhut;

    :goto_4
    iget-object v2, p0, Lhsp;->p:[Lhut;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Lhsp;->p:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsp;->p:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_c
    iget-object v0, p0, Lhsp;->p:[Lhut;

    array-length v0, v0

    goto :goto_3

    :cond_d
    iget-object v2, p0, Lhsp;->p:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsp;->p:[Lhut;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lhsp;->i:Lhso;

    if-nez v0, :cond_e

    new-instance v0, Lhso;

    invoke-direct {v0}, Lhso;-><init>()V

    iput-object v0, p0, Lhsp;->i:Lhso;

    :cond_e
    iget-object v0, p0, Lhsp;->i:Lhso;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhsp;->j:[B

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhsp;->q:Z

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lhsp;->r:Lhnh;

    if-nez v0, :cond_f

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Lhsp;->r:Lhnh;

    :cond_f
    iget-object v0, p0, Lhsp;->r:Lhnh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Lhsp;->s:Lhxf;

    if-nez v0, :cond_10

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhsp;->s:Lhxf;

    :cond_10
    iget-object v0, p0, Lhsp;->s:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lhsp;->t:Lhog;

    if-nez v0, :cond_11

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhsp;->t:Lhog;

    :cond_11
    iget-object v0, p0, Lhsp;->t:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhsp;->u:Z

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Lhsp;->v:Lhkm;

    if-nez v0, :cond_12

    new-instance v0, Lhkm;

    invoke-direct {v0}, Lhkm;-><init>()V

    iput-object v0, p0, Lhsp;->v:Lhkm;

    :cond_12
    iget-object v0, p0, Lhsp;->v:Lhkm;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x65 -> :sswitch_c
        0x6d -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x8a -> :sswitch_10
        0x90 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb0 -> :sswitch_15
        0x1ceb6a22 -> :sswitch_16
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x0

    iget-object v1, p0, Lhsp;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lhsp;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lhsp;->b:Lhxf;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhsp;->b:Lhxf;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v1, p0, Lhsp;->c:Lhgz;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhsp;->c:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v1, p0, Lhsp;->k:Lhgz;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lhsp;->k:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v1, p0, Lhsp;->d:Lhgz;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lhsp;->d:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v1, p0, Lhsp;->e:Lhgz;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lhsp;->e:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v1, p0, Lhsp;->f:Lhog;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lhsp;->f:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_6
    iget-object v1, p0, Lhsp;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lhsp;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lhsp;->g:[Lhbi;

    if-eqz v1, :cond_9

    iget-object v2, p0, Lhsp;->g:[Lhbi;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    if-eqz v4, :cond_8

    const/16 v5, 0x9

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_9
    iget-object v1, p0, Lhsp;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const/16 v1, 0xa

    iget-object v2, p0, Lhsp;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_a
    iget-wide v2, p0, Lhsp;->m:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_b

    const/16 v1, 0xb

    iget-wide v2, p0, Lhsp;->m:J

    invoke-virtual {p1, v1, v2, v3}, Lidd;->a(IJ)V

    :cond_b
    iget v1, p0, Lhsp;->n:F

    cmpl-float v1, v1, v6

    if-eqz v1, :cond_c

    const/16 v1, 0xc

    iget v2, p0, Lhsp;->n:F

    invoke-virtual {p1, v1, v2}, Lidd;->a(IF)V

    :cond_c
    iget v1, p0, Lhsp;->o:F

    cmpl-float v1, v1, v6

    if-eqz v1, :cond_d

    const/16 v1, 0xd

    iget v2, p0, Lhsp;->o:F

    invoke-virtual {p1, v1, v2}, Lidd;->a(IF)V

    :cond_d
    iget-object v1, p0, Lhsp;->p:[Lhut;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lhsp;->p:[Lhut;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_f

    aget-object v3, v1, v0

    if-eqz v3, :cond_e

    const/16 v4, 0xe

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_f
    iget-object v0, p0, Lhsp;->i:Lhso;

    if-eqz v0, :cond_10

    const/16 v0, 0xf

    iget-object v1, p0, Lhsp;->i:Lhso;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_10
    iget-object v0, p0, Lhsp;->j:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_11

    const/16 v0, 0x11

    iget-object v1, p0, Lhsp;->j:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_11
    iget-boolean v0, p0, Lhsp;->q:Z

    if-eqz v0, :cond_12

    const/16 v0, 0x12

    iget-boolean v1, p0, Lhsp;->q:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_12
    iget-object v0, p0, Lhsp;->r:Lhnh;

    if-eqz v0, :cond_13

    const/16 v0, 0x13

    iget-object v1, p0, Lhsp;->r:Lhnh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_13
    iget-object v0, p0, Lhsp;->s:Lhxf;

    if-eqz v0, :cond_14

    const/16 v0, 0x14

    iget-object v1, p0, Lhsp;->s:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_14
    iget-object v0, p0, Lhsp;->t:Lhog;

    if-eqz v0, :cond_15

    const/16 v0, 0x15

    iget-object v1, p0, Lhsp;->t:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_15
    iget-boolean v0, p0, Lhsp;->u:Z

    if-eqz v0, :cond_16

    const/16 v0, 0x16

    iget-boolean v1, p0, Lhsp;->u:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_16
    iget-object v0, p0, Lhsp;->v:Lhkm;

    if-eqz v0, :cond_17

    const v0, 0x39d6d44

    iget-object v1, p0, Lhsp;->v:Lhkm;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_17
    iget-object v0, p0, Lhsp;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhsp;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhsp;

    iget-object v2, p0, Lhsp;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhsp;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhsp;->b:Lhxf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhsp;->b:Lhxf;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhsp;->c:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhsp;->c:Lhgz;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhsp;->k:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhsp;->k:Lhgz;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhsp;->d:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhsp;->d:Lhgz;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhsp;->e:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhsp;->e:Lhgz;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhsp;->f:Lhog;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhsp;->f:Lhog;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Lhsp;->l:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhsp;->l:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_8
    iget-object v2, p0, Lhsp;->g:[Lhbi;

    iget-object v3, p1, Lhsp;->g:[Lhbi;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsp;->h:Ljava/lang/String;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhsp;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_9
    iget-wide v2, p0, Lhsp;->m:J

    iget-wide v4, p1, Lhsp;->m:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lhsp;->n:F

    iget v3, p1, Lhsp;->n:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lhsp;->o:F

    iget v3, p1, Lhsp;->o:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-object v2, p0, Lhsp;->p:[Lhut;

    iget-object v3, p1, Lhsp;->p:[Lhut;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsp;->i:Lhso;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhsp;->i:Lhso;

    if-nez v2, :cond_3

    :goto_a
    iget-object v2, p0, Lhsp;->j:[B

    iget-object v3, p1, Lhsp;->j:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lhsp;->q:Z

    iget-boolean v3, p1, Lhsp;->q:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsp;->r:Lhnh;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhsp;->r:Lhnh;

    if-nez v2, :cond_3

    :goto_b
    iget-object v2, p0, Lhsp;->s:Lhxf;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhsp;->s:Lhxf;

    if-nez v2, :cond_3

    :goto_c
    iget-object v2, p0, Lhsp;->t:Lhog;

    if-nez v2, :cond_10

    iget-object v2, p1, Lhsp;->t:Lhog;

    if-nez v2, :cond_3

    :goto_d
    iget-boolean v2, p0, Lhsp;->u:Z

    iget-boolean v3, p1, Lhsp;->u:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsp;->v:Lhkm;

    if-nez v2, :cond_11

    iget-object v2, p1, Lhsp;->v:Lhkm;

    if-nez v2, :cond_3

    :goto_e
    iget-object v2, p0, Lhsp;->I:Ljava/util/List;

    if-nez v2, :cond_12

    iget-object v2, p1, Lhsp;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhsp;->a:Ljava/lang/String;

    iget-object v3, p1, Lhsp;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhsp;->b:Lhxf;

    iget-object v3, p1, Lhsp;->b:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhsp;->c:Lhgz;

    iget-object v3, p1, Lhsp;->c:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhsp;->k:Lhgz;

    iget-object v3, p1, Lhsp;->k:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhsp;->d:Lhgz;

    iget-object v3, p1, Lhsp;->d:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhsp;->e:Lhgz;

    iget-object v3, p1, Lhsp;->e:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhsp;->f:Lhog;

    iget-object v3, p1, Lhsp;->f:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhsp;->l:Ljava/lang/String;

    iget-object v3, p1, Lhsp;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhsp;->h:Ljava/lang/String;

    iget-object v3, p1, Lhsp;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhsp;->i:Lhso;

    iget-object v3, p1, Lhsp;->i:Lhso;

    invoke-virtual {v2, v3}, Lhso;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v2, p0, Lhsp;->r:Lhnh;

    iget-object v3, p1, Lhsp;->r:Lhnh;

    invoke-virtual {v2, v3}, Lhnh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v2, p0, Lhsp;->s:Lhxf;

    iget-object v3, p1, Lhsp;->s:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v2, p0, Lhsp;->t:Lhog;

    iget-object v3, p1, Lhsp;->t:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v2, p0, Lhsp;->v:Lhkm;

    iget-object v3, p1, Lhsp;->v:Lhkm;

    invoke-virtual {v2, v3}, Lhkm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v2, p0, Lhsp;->I:Ljava/util/List;

    iget-object v3, p1, Lhsp;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 10

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->b:Lhxf;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->c:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->k:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->d:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->e:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->f:Lhog;

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->l:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsp;->g:[Lhbi;

    if-nez v2, :cond_b

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsp;->h:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v6, p0, Lhsp;->m:J

    iget-wide v8, p0, Lhsp;->m:J

    const/16 v2, 0x20

    ushr-long/2addr v8, v2

    xor-long/2addr v6, v8

    long-to-int v2, v6

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhsp;->n:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhsp;->o:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lhsp;->p:[Lhut;

    if-nez v2, :cond_e

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsp;->i:Lhso;

    if-nez v0, :cond_10

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsp;->j:[B

    if-nez v2, :cond_11

    mul-int/lit8 v2, v0, 0x1f

    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-boolean v0, p0, Lhsp;->q:Z

    if-eqz v0, :cond_12

    move v0, v3

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->r:Lhnh;

    if-nez v0, :cond_13

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->s:Lhxf;

    if-nez v0, :cond_14

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->t:Lhog;

    if-nez v0, :cond_15

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lhsp;->u:Z

    if-eqz v2, :cond_16

    :goto_e
    add-int/2addr v0, v3

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsp;->v:Lhkm;

    if-nez v0, :cond_17

    move v0, v1

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhsp;->I:Ljava/util/List;

    if-nez v2, :cond_18

    :goto_10
    add-int/2addr v0, v1

    return v0

    :cond_3
    iget-object v0, p0, Lhsp;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lhsp;->b:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, Lhsp;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_6
    iget-object v0, p0, Lhsp;->k:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_7
    iget-object v0, p0, Lhsp;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_8
    iget-object v0, p0, Lhsp;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_9
    iget-object v0, p0, Lhsp;->f:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_6

    :cond_a
    iget-object v0, p0, Lhsp;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_7

    :cond_b
    move v2, v0

    move v0, v1

    :goto_11
    iget-object v5, p0, Lhsp;->g:[Lhbi;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhsp;->g:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_c

    move v2, v1

    :goto_12
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_c
    iget-object v2, p0, Lhsp;->g:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_12

    :cond_d
    iget-object v0, p0, Lhsp;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_8

    :cond_e
    move v2, v0

    move v0, v1

    :goto_13
    iget-object v5, p0, Lhsp;->p:[Lhut;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhsp;->p:[Lhut;

    aget-object v2, v2, v0

    if-nez v2, :cond_f

    move v2, v1

    :goto_14
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    :cond_f
    iget-object v2, p0, Lhsp;->p:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhut;->hashCode()I

    move-result v2

    goto :goto_14

    :cond_10
    iget-object v0, p0, Lhsp;->i:Lhso;

    invoke-virtual {v0}, Lhso;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_11
    move v2, v0

    move v0, v1

    :goto_15
    iget-object v5, p0, Lhsp;->j:[B

    array-length v5, v5

    if-ge v0, v5, :cond_2

    mul-int/lit8 v2, v2, 0x1f

    iget-object v5, p0, Lhsp;->j:[B

    aget-byte v5, v5, v0

    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    :cond_12
    move v0, v4

    goto/16 :goto_a

    :cond_13
    iget-object v0, p0, Lhsp;->r:Lhnh;

    invoke-virtual {v0}, Lhnh;->hashCode()I

    move-result v0

    goto/16 :goto_b

    :cond_14
    iget-object v0, p0, Lhsp;->s:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto/16 :goto_c

    :cond_15
    iget-object v0, p0, Lhsp;->t:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_d

    :cond_16
    move v3, v4

    goto/16 :goto_e

    :cond_17
    iget-object v0, p0, Lhsp;->v:Lhkm;

    invoke-virtual {v0}, Lhkm;->hashCode()I

    move-result v0

    goto/16 :goto_f

    :cond_18
    iget-object v1, p0, Lhsp;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_10
.end method
