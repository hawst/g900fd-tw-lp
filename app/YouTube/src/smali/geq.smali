.class public Lgeq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lexd;

.field final b:Z

.field final c:Z

.field final d:Z

.field final e:Z

.field f:I

.field g:I


# direct methods
.method public constructor <init>(Lexd;ZZZZ)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v0, p0, Lgeq;->f:I

    .line 23
    iput v0, p0, Lgeq;->g:I

    .line 32
    iput-object p1, p0, Lgeq;->a:Lexd;

    .line 33
    iput-boolean p2, p0, Lgeq;->b:Z

    .line 34
    iput-boolean p3, p0, Lgeq;->c:Z

    .line 35
    iput-boolean p4, p0, Lgeq;->d:Z

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgeq;->e:Z

    .line 37
    return-void
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lgeq;->a:Lexd;

    invoke-interface {v0}, Lexd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x90

    .line 88
    :goto_0
    return v0

    .line 84
    :cond_0
    iget-boolean v0, p0, Lgeq;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0xf0

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lgeq;->d:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lgeq;->a:Lexd;

    .line 88
    invoke-interface {v0}, Lexd;->h()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/16 v0, 0x168

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lgeq;->b:Z

    if-nez v0, :cond_4

    const/16 v0, 0x1e0

    goto :goto_0

    :cond_4
    const/16 v0, 0x2d0

    goto :goto_0
.end method

.method public a(Z)Lgeo;
    .locals 5

    .prologue
    .line 63
    iget v0, p0, Lgeq;->g:I

    if-lez v0, :cond_0

    iget v0, p0, Lgeq;->g:I

    .line 69
    :goto_0
    iget v1, p0, Lgeq;->f:I

    if-lez v1, :cond_2

    iget v1, p0, Lgeq;->f:I

    .line 74
    :goto_1
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 76
    new-instance v2, Lgeo;

    new-instance v3, Lgep;

    invoke-direct {v3, v0, v1}, Lgep;-><init>(II)V

    const/4 v0, 0x1

    iget-boolean v1, p0, Lgeq;->e:Z

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v1, v4}, Lgeo;-><init>(Lgep;ZZLjava/lang/String;)V

    return-object v2

    .line 63
    :cond_0
    if-eqz p1, :cond_1

    const v0, 0x7fffffff

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {p0}, Lgeq;->a()I

    move-result v0

    goto :goto_0

    .line 69
    :cond_2
    if-eqz p1, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    .line 73
    :cond_3
    invoke-virtual {p0}, Lgeq;->b()I

    move-result v1

    goto :goto_1
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 41
    iput p1, p0, Lgeq;->f:I

    .line 42
    iput p2, p0, Lgeq;->g:I

    .line 43
    return-void
.end method

.method b()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lgeq;->a:Lexd;

    invoke-interface {v0}, Lexd;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgeq;->d:Z

    if-nez v0, :cond_0

    const/16 v0, 0x168

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x90

    goto :goto_0
.end method
