.class public final Lgmb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lgcd;

.field public final b:Z

.field public final c:J

.field public final d:Lglz;

.field public final e:Lflo;

.field public final f:Lglv;

.field public final g:Lgjn;

.field public final h:J

.field public final i:J

.field private final j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Lgcd;ZJLglz;Lflo;Lglv;Lgjn;JJZ)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgmb;->k:Z

    .line 43
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    iput-object v0, p0, Lgmb;->a:Lgcd;

    .line 44
    iput-boolean p2, p0, Lgmb;->b:Z

    .line 45
    iput-wide p3, p0, Lgmb;->c:J

    .line 46
    iput-object p5, p0, Lgmb;->d:Lglz;

    .line 47
    iput-object p6, p0, Lgmb;->e:Lflo;

    .line 48
    iput-object p7, p0, Lgmb;->f:Lglv;

    .line 49
    iput-object p8, p0, Lgmb;->g:Lgjn;

    .line 50
    iput-wide p9, p0, Lgmb;->h:J

    .line 51
    iput-wide p11, p0, Lgmb;->i:J

    .line 52
    iput-boolean p13, p0, Lgmb;->j:Z

    .line 53
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lgmb;->a:Lgcd;

    iget-object v0, v0, Lgcd;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgmb;->a:Lgcd;

    iget-object v0, v0, Lgcd;->e:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgmb;->a:Lgcd;

    iget-object v0, v0, Lgcd;->d:Landroid/net/Uri;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    const/4 v0, 0x0

    .line 165
    invoke-virtual {p0}, Lgmb;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    iget-object v0, p0, Lgmb;->e:Lflo;

    iget-object v0, v0, Lflo;->a:Lhqn;

    iget-object v0, v0, Lhqn;->b:Ljava/lang/String;

    .line 167
    if-nez v0, :cond_0

    .line 168
    const v0, 0x7f09009a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 185
    :cond_0
    :goto_0
    return-object v0

    .line 170
    :cond_1
    invoke-virtual {p0}, Lgmb;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171
    iget-object v1, p0, Lgmb;->d:Lglz;

    invoke-virtual {v1}, Lglz;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 172
    const v0, 0x7f09009e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 173
    :cond_2
    iget-object v1, p0, Lgmb;->d:Lglz;

    iget-object v1, v1, Lglz;->b:Lflg;

    invoke-virtual {v1}, Lflg;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    iget-object v0, p0, Lgmb;->d:Lglz;

    iget-object v0, v0, Lglz;->b:Lflg;

    iget-object v0, v0, Lflg;->f:Ljava/lang/String;

    goto :goto_0

    .line 176
    :cond_3
    invoke-virtual {p0}, Lgmb;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 177
    const v0, 0x7f09009b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 178
    :cond_4
    iget-object v0, p0, Lgmb;->f:Lglv;

    sget-object v1, Lglv;->g:Lglv;

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_6

    .line 179
    const v0, 0x7f09009c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 178
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 180
    :cond_6
    iget-boolean v0, p0, Lgmb;->j:Z

    if-nez v0, :cond_7

    .line 181
    const v0, 0x7f09009d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 183
    :cond_7
    const v0, 0x7f090099

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lgmb;->f:Lglv;

    sget-object v1, Lglv;->c:Lglv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lgmb;->f:Lglv;

    sget-object v1, Lglv;->i:Lglv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lgmb;->f:Lglv;

    sget-object v1, Lglv;->b:Lglv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()I
    .locals 4

    .prologue
    .line 126
    iget-wide v0, p0, Lgmb;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 127
    iget-wide v0, p0, Lgmb;->h:J

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lgmb;->i:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lgmb;->e:Lflo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgmb;->e:Lflo;

    .line 134
    invoke-virtual {v0}, Lflo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lgmb;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgmb;->e:Lflo;

    .line 139
    invoke-virtual {v0}, Lflo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 143
    iget-object v2, p0, Lgmb;->d:Lglz;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lgmb;->d:Lglz;

    iget-object v3, v2, Lglz;->b:Lflg;

    if-nez v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    return v0

    :cond_0
    invoke-virtual {v2}, Lglz;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lglz;->b()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v0

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lgmb;->f:Lglv;

    sget-object v1, Lglv;->e:Lglv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lgmb;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    invoke-virtual {p0}, Lgmb;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 157
    invoke-virtual {p0}, Lgmb;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    invoke-virtual {p0}, Lgmb;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    invoke-virtual {p0}, Lgmb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-boolean v0, p0, Lgmb;->j:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 189
    invoke-virtual {p0}, Lgmb;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 190
    invoke-virtual {p0}, Lgmb;->h()Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    invoke-virtual {p0}, Lgmb;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lgmb;->f:Lglv;

    sget-object v2, Lglv;->h:Lglv;

    if-ne v1, v2, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lgmb;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lgmb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgmb;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
