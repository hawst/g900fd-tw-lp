.class public abstract Lcbg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field public final a:Landroid/view/View;

.field final b:Landroid/view/View;

.field final c:Lcbk;

.field final d:Lcbl;

.field final e:Lckf;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/ImageView;

.field private j:Leyp;

.field private k:Landroid/content/res/Resources;


# direct methods
.method protected constructor <init>(Landroid/view/View;Landroid/app/Activity;Leyp;Lcbk;Lcbl;Lckf;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcbg;->a:Landroid/view/View;

    .line 68
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcbg;->j:Leyp;

    .line 69
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbk;

    iput-object v0, p0, Lcbg;->c:Lcbk;

    .line 70
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbl;

    iput-object v0, p0, Lcbg;->d:Lcbl;

    .line 71
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckf;

    iput-object v0, p0, Lcbg;->e:Lckf;

    .line 72
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcbg;->k:Landroid/content/res/Resources;

    .line 73
    const v0, 0x7f080131

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbg;->f:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0801c3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbg;->g:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0801c5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbg;->h:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f0801c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcbg;->i:Landroid/widget/ImageView;

    .line 77
    const v0, 0x7f0801c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbg;->b:Landroid/view/View;

    .line 78
    return-void
.end method


# virtual methods
.method public a(Lfsg;Lfwe;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 82
    iget-object v0, p0, Lcbg;->f:Landroid/widget/TextView;

    iget-object v1, p2, Lfwe;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v1, p0, Lcbg;->g:Landroid/widget/TextView;

    invoke-virtual {p2}, Lfwe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lfwe;->f:Ljava/lang/String;

    iget v2, p2, Lfwe;->i:I

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\u2026"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcbg;->h:Landroid/widget/TextView;

    iget-object v1, p2, Lfwe;->e:Ljava/util/Date;

    iget-object v2, p0, Lcbg;->k:Landroid/content/res/Resources;

    invoke-static {v1, v2}, La;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {p2}, Lfwe;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcbg;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcbg;->b:Landroid/view/View;

    new-instance v1, Lcbh;

    invoke-direct {v1, p0, p2}, Lcbh;-><init>(Lcbg;Lfwe;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    :goto_1
    iget-object v0, p0, Lcbg;->a:Landroid/view/View;

    new-instance v1, Lcbi;

    invoke-direct {v1, p0, p2}, Lcbi;-><init>(Lcbg;Lfwe;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v0, p0, Lcbg;->i:Landroid/widget/ImageView;

    iget-object v1, p2, Lfwe;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcbg;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 109
    iget-object v0, p0, Lcbg;->i:Landroid/widget/ImageView;

    new-instance v1, Lcbj;

    invoke-direct {v1, p0, p2}, Lcbj;-><init>(Lcbg;Lfwe;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v0, p0, Lcbg;->k:Landroid/content/res/Resources;

    const v1, 0x7f0a0086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 118
    iget-object v1, p2, Lfwe;->g:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sz=50"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0xe

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "sz="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcbg;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 120
    iget-object v1, p0, Lcbg;->j:Leyp;

    iget-object v2, p0, Lcbg;->i:Landroid/widget/ImageView;

    invoke-static {v1, v0, v2}, Leyi;->a(Leyp;Landroid/net/Uri;Landroid/widget/ImageView;)V

    .line 121
    iget-object v0, p0, Lcbg;->a:Landroid/view/View;

    return-object v0

    .line 83
    :cond_0
    iget-object v0, p2, Lfwe;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcbg;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcbg;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    check-cast p2, Lfwe;

    invoke-virtual {p0, p1, p2}, Lcbg;->a(Lfsg;Lfwe;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
