.class final Lduw;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private synthetic a:Ldun;


# direct methods
.method constructor <init>(Ldun;)V
    .locals 0

    .prologue
    .line 1587
    iput-object p1, p0, Lduw;->a:Ldun;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1591
    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1592
    iget-object v0, p0, Lduw;->a:Ldun;

    invoke-virtual {v0}, Ldun;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->e:Ldww;

    if-eq v0, v1, :cond_0

    .line 1593
    iget-object v0, p0, Lduw;->a:Ldun;

    invoke-static {v0}, Ldun;->g(Ldun;)Lduy;

    move-result-object v0

    sget-object v1, Ldww;->c:Ldww;

    invoke-virtual {v0, v1}, Lduy;->a(Ldww;)V

    .line 1594
    iget-object v0, p0, Lduw;->a:Ldun;

    invoke-static {v0}, Ldun;->f(Ldun;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1599
    :cond_0
    :goto_0
    return-void

    .line 1596
    :cond_1
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1597
    iget-object v0, p0, Lduw;->a:Ldun;

    iget-object v1, v0, Ldun;->g:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, v0, Ldun;->d:Lduy;

    iget-object v1, v1, Lduy;->d:Ldww;

    sget-object v2, Ldww;->c:Ldww;

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Ldun;->f:Ldwr;

    if-nez v1, :cond_2

    iget-object v0, v0, Ldun;->e:Ldyg;

    const-string v1, "We should reconnect, but we lost the screen"

    invoke-virtual {v0, v1}, Ldyg;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, v0, Ldun;->d:Lduy;

    sget-object v2, Ldww;->a:Ldww;

    invoke-virtual {v1, v2}, Lduy;->a(Ldww;)V

    iget-object v1, v0, Ldun;->b:Landroid/content/Context;

    iget-object v2, v0, Ldun;->c:Ldur;

    sget-object v3, Ldun;->a:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, v0, Ldun;->g:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
