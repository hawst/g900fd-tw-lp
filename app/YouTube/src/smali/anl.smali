.class final Lanl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lanj;


# instance fields
.field private a:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lanl;->a:Landroid/os/IBinder;

    .line 91
    return-void
.end method


# virtual methods
.method public final a(Laiw;Lajf;Lajl;Lajo;Lait;Lain;Lajr;Laiq;Laiz;Lajc;Laji;Laju;Z)Lanm;
    .locals 5

    .prologue
    .line 102
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 103
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 106
    :try_start_0
    const-string v0, "com.google.android.apps.youtube.api.service.jar.IApiPlayerFactoryService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 107
    if-eqz p1, :cond_0

    invoke-interface {p1}, Laiw;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 108
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lajf;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 109
    if-eqz p3, :cond_2

    invoke-interface {p3}, Lajl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 110
    if-eqz p4, :cond_3

    invoke-interface {p4}, Lajo;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 111
    if-eqz p5, :cond_4

    invoke-interface {p5}, Lait;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 112
    if-eqz p6, :cond_5

    invoke-interface {p6}, Lain;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_5
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 113
    if-eqz p7, :cond_6

    invoke-interface {p7}, Lajr;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_6
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 114
    if-eqz p8, :cond_7

    invoke-interface {p8}, Laiq;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_7
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 115
    if-eqz p9, :cond_8

    invoke-interface {p9}, Laiz;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_8
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 116
    if-eqz p10, :cond_9

    invoke-interface {p10}, Lajc;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_9
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 117
    if-eqz p11, :cond_a

    invoke-interface/range {p11 .. p11}, Laji;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_a
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 118
    if-eqz p12, :cond_b

    invoke-interface/range {p12 .. p12}, Laju;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_b
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 119
    if-eqz p13, :cond_c

    const/4 v0, 0x1

    :goto_c
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 120
    iget-object v0, p0, Lanl;->a:Landroid/os/IBinder;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 121
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 122
    invoke-virtual {v2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lann;->a(Landroid/os/IBinder;)Lanm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 125
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 126
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 128
    return-object v0

    .line 107
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 108
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 109
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 110
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 111
    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    .line 112
    :cond_5
    const/4 v0, 0x0

    goto :goto_5

    .line 113
    :cond_6
    const/4 v0, 0x0

    goto :goto_6

    .line 114
    :cond_7
    const/4 v0, 0x0

    goto :goto_7

    .line 115
    :cond_8
    const/4 v0, 0x0

    goto :goto_8

    .line 116
    :cond_9
    const/4 v0, 0x0

    goto :goto_9

    .line 117
    :cond_a
    const/4 v0, 0x0

    goto :goto_a

    .line 118
    :cond_b
    const/4 v0, 0x0

    goto :goto_b

    .line 119
    :cond_c
    const/4 v0, 0x0

    goto :goto_c

    .line 125
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 126
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lanl;->a:Landroid/os/IBinder;

    return-object v0
.end method
