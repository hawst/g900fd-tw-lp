.class public Lcom/android/volley/toolbox/NetworkImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lxk;

.field private c:Lxq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/volley/toolbox/NetworkImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/volley/toolbox/NetworkImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public static synthetic a(Lcom/android/volley/toolbox/NetworkImageView;)I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public static synthetic b(Lcom/android/volley/toolbox/NetworkImageView;)I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public a(Z)V
    .locals 12

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/android/volley/toolbox/NetworkImageView;->getWidth()I

    move-result v1

    .line 105
    invoke-virtual {p0}, Lcom/android/volley/toolbox/NetworkImageView;->getHeight()I

    move-result v2

    .line 107
    invoke-virtual {p0}, Lcom/android/volley/toolbox/NetworkImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/volley/toolbox/NetworkImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v3, -0x2

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/android/volley/toolbox/NetworkImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v3, -0x2

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    .line 112
    :goto_0
    if-nez v1, :cond_2

    if-nez v2, :cond_2

    if-nez v0, :cond_2

    .line 176
    :cond_0
    :goto_1
    return-void

    .line 107
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 119
    iget-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    if-eqz v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    invoke-virtual {v0}, Lxq;->a()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    .line 123
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/volley/toolbox/NetworkImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 128
    :cond_4
    iget-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    iget-object v0, v0, Lxq;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 129
    iget-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    iget-object v0, v0, Lxq;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/volley/toolbox/NetworkImageView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    invoke-virtual {v0}, Lxq;->a()V

    .line 135
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/volley/toolbox/NetworkImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 141
    :cond_5
    iget-object v1, p0, Lcom/android/volley/toolbox/NetworkImageView;->b:Lxk;

    iget-object v3, p0, Lcom/android/volley/toolbox/NetworkImageView;->a:Ljava/lang/String;

    new-instance v6, Lxv;

    invoke-direct {v6, p0, p1}, Lxv;-><init>(Lcom/android/volley/toolbox/NetworkImageView;Z)V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v0, v2, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ImageLoader must be invoked from the main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/4 v0, 0x0

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "#W"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "#H"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v1, Lxk;->b:Lxp;

    invoke-interface {v0, v4}, Lxp;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_7

    new-instance v0, Lxq;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lxq;-><init>(Lxk;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Lxr;)V

    const/4 v1, 0x1

    invoke-interface {v6, v0, v1}, Lxr;->a(Lxq;Z)V

    .line 175
    :goto_2
    iput-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    goto/16 :goto_1

    .line 141
    :cond_7
    new-instance v0, Lxq;

    const/4 v2, 0x0

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lxq;-><init>(Lxk;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Lxr;)V

    const/4 v2, 0x1

    invoke-interface {v6, v0, v2}, Lxr;->a(Lxq;Z)V

    iget-object v2, v1, Lxk;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lxo;

    if-eqz v2, :cond_8

    iget-object v1, v2, Lxo;->c:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    new-instance v5, Lxs;

    new-instance v7, Lxl;

    invoke-direct {v7, v1, v4}, Lxl;-><init>(Lxk;Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    new-instance v11, Lxm;

    invoke-direct {v11, v1, v4}, Lxm;-><init>(Lxk;Ljava/lang/String;)V

    move-object v6, v3

    invoke-direct/range {v5 .. v11}, Lxs;-><init>(Ljava/lang/String;Lwv;IILandroid/graphics/Bitmap$Config;Lwu;)V

    iget-object v2, v1, Lxk;->a:Lws;

    invoke-virtual {v2, v5}, Lws;->a(Lwp;)Lwp;

    iget-object v2, v1, Lxk;->c:Ljava/util/HashMap;

    new-instance v3, Lxo;

    invoke-direct {v3, v1, v5, v0}, Lxo;-><init>(Lxk;Lwp;Lxq;)V

    invoke-virtual {v2, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 199
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 200
    invoke-virtual {p0}, Lcom/android/volley/toolbox/NetworkImageView;->invalidate()V

    .line 201
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 186
    iget-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    invoke-virtual {v0}, Lxq;->a()V

    .line 190
    invoke-virtual {p0, v1}, Lcom/android/volley/toolbox/NetworkImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 192
    iput-object v1, p0, Lcom/android/volley/toolbox/NetworkImageView;->c:Lxq;

    .line 194
    :cond_0
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 195
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 180
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 181
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/volley/toolbox/NetworkImageView;->a(Z)V

    .line 182
    return-void
.end method
