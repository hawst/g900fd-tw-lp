.class public Lcom/google/android/libraries/youtube/common/ui/GoneInLandscapeLinearLayout;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method private a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    .line 56
    :goto_0
    if-eqz v1, :cond_0

    const/16 v0, 0x8

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/common/ui/GoneInLandscapeLinearLayout;->setVisibility(I)V

    .line 57
    return-void

    :cond_1
    move v1, v0

    .line 55
    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/common/ui/GoneInLandscapeLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/common/ui/GoneInLandscapeLinearLayout;->a(Landroid/content/res/Configuration;)V

    .line 28
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/libraries/youtube/common/ui/GoneInLandscapeLinearLayout;->a(Landroid/content/res/Configuration;)V

    .line 34
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 50
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/common/ui/GoneInLandscapeLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/common/ui/GoneInLandscapeLinearLayout;->a(Landroid/content/res/Configuration;)V

    .line 52
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/common/ui/GoneInLandscapeLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 41
    :goto_0
    if-eqz v0, :cond_1

    .line 42
    invoke-virtual {p0, v1, v1}, Lcom/google/android/libraries/youtube/common/ui/GoneInLandscapeLinearLayout;->setMeasuredDimension(II)V

    .line 46
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 44
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_1
.end method
