.class public Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method private static varargs a(Landroid/view/View;II[I)V
    .locals 4

    .prologue
    .line 80
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 81
    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 82
    iput p2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 83
    array-length v2, p3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p3, v1

    .line 84
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 77
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 29
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 30
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 31
    div-int/lit8 v2, v0, 0x2

    .line 32
    div-int/lit8 v3, v1, 0x2

    .line 37
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildCount()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 64
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 69
    return-void

    .line 39
    :pswitch_0
    invoke-virtual {p0, v6}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    new-array v3, v7, [I

    const/16 v4, 0xd

    aput v4, v3, v6

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    goto :goto_0

    .line 43
    :pswitch_1
    invoke-virtual {p0, v6}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_0

    .line 42
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    .line 45
    invoke-virtual {p0, v7}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_1

    .line 44
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    goto :goto_0

    .line 49
    :pswitch_2
    invoke-virtual {p0, v6}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_2

    .line 48
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    .line 51
    invoke-virtual {p0, v7}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_3

    .line 50
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    .line 53
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_4

    .line 52
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    goto :goto_0

    .line 57
    :pswitch_3
    invoke-virtual {p0, v6}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_5

    .line 56
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    .line 59
    invoke-virtual {p0, v7}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_6

    .line 58
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    .line 61
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_7

    .line 60
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    .line 62
    const/4 v0, 0x3

    .line 63
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v5, [I

    fill-array-data v1, :array_8

    .line 62
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/libraries/youtube/conversation/ui/FacepileLayout;->a(Landroid/view/View;II[I)V

    goto/16 :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 43
    :array_0
    .array-data 4
        0xa
        0x9
    .end array-data

    .line 45
    :array_1
    .array-data 4
        0xc
        0xb
    .end array-data

    .line 49
    :array_2
    .array-data 4
        0xa
        0xe
    .end array-data

    .line 51
    :array_3
    .array-data 4
        0xc
        0x9
    .end array-data

    .line 53
    :array_4
    .array-data 4
        0xc
        0xb
    .end array-data

    .line 57
    :array_5
    .array-data 4
        0xa
        0x9
    .end array-data

    .line 59
    :array_6
    .array-data 4
        0xa
        0xb
    .end array-data

    .line 61
    :array_7
    .array-data 4
        0xc
        0x9
    .end array-data

    .line 63
    :array_8
    .array-data 4
        0xc
        0xb
    .end array-data
.end method
