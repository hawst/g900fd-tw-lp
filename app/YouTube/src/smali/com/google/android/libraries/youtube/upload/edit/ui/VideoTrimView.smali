.class public Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lgqr;
.implements Lgsb;
.implements Lgsz;


# instance fields
.field private A:Lgsd;

.field private B:Lgth;

.field private final C:Landroid/graphics/Rect;

.field private D:Lgsv;

.field private E:Lgqo;

.field private F:Lgrw;

.field private G:Lgsa;

.field private H:Lgsa;

.field private I:Lgsa;

.field private J:Lgsc;

.field private K:J

.field private L:J

.field private M:Lgtj;

.field private N:Z

.field private O:I

.field private P:F

.field private Q:F

.field private R:F

.field private S:J

.field private T:J

.field private U:Lgtg;

.field private V:Lgti;

.field a:Lfdw;

.field b:Lfqg;

.field private c:Z

.field private final d:F

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:F

.field private final l:I

.field private final m:I

.field private final n:J

.field private final o:Z

.field private final p:Z

.field private q:I

.field private final r:Landroid/graphics/Paint;

.field private final s:Landroid/graphics/Paint;

.field private final t:Landroid/graphics/Paint;

.field private final u:Landroid/widget/ImageView;

.field private final v:Landroid/widget/ImageView;

.field private final w:Ljava/util/List;

.field private final x:Ljava/util/List;

.field private y:F

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 182
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 123
    sget-object v0, Lgth;->a:Lgth;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    .line 127
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    .line 130
    sget-object v0, Lgsv;->a:Lgsv;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    .line 144
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->K:J

    .line 145
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-wide v0, v0, Lgsv;->c:J

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->L:J

    .line 172
    new-instance v0, Lgtg;

    invoke-direct {v0, p0}, Lgtg;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->U:Lgtg;

    .line 173
    new-instance v0, Lgti;

    invoke-direct {v0, p0}, Lgti;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->V:Lgti;

    .line 184
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->l:I

    .line 186
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->q:I

    .line 188
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 190
    sget-object v1, Lgpz;->a:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 191
    const/16 v2, 0x64

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v6, v4, v2, v3}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    iput v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d:F

    .line 192
    const v2, 0x7f0a0049

    .line 193
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->e:I

    .line 194
    const v2, 0x7f0a004a

    .line 195
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->f:I

    .line 196
    const v2, 0x7f0a004b

    .line 197
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    .line 198
    const v2, 0x7f0a004e

    .line 199
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->h:I

    .line 201
    invoke-virtual {v1, v4, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->p:Z

    .line 202
    const/4 v2, 0x2

    .line 203
    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->o:Z

    .line 204
    const/4 v2, 0x3

    const v3, 0x7f0201a2

    .line 205
    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 206
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 208
    const v1, 0x7f0b0006

    .line 209
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m:I

    .line 210
    const v1, 0x7f0b0007

    .line 211
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v4, v1

    iput-wide v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->n:J

    .line 212
    const v1, 0x7f0a004c

    .line 213
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->i:I

    .line 214
    const v1, 0x7f0a004d

    .line 215
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->j:I

    .line 216
    const v1, 0x7f0b0008

    .line 217
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->k:F

    .line 219
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->r:Landroid/graphics/Paint;

    .line 220
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->r:Landroid/graphics/Paint;

    const v3, 0x7f070077

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 222
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->s:Landroid/graphics/Paint;

    .line 223
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->s:Landroid/graphics/Paint;

    const v3, 0x7f070078

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 224
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->s:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->h:I

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 226
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->t:Landroid/graphics/Paint;

    .line 227
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->t:Landroid/graphics/Paint;

    const v3, 0x7f070079

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 228
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->t:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->h:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 230
    invoke-static {p1, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Landroid/content/Context;I)Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->u:Landroid/widget/ImageView;

    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->u:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->addView(Landroid/view/View;)V

    .line 233
    invoke-static {p1, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Landroid/content/Context;I)Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->v:Landroid/widget/ImageView;

    .line 234
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->v:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->addView(Landroid/view/View;)V

    .line 236
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->w:Ljava/util/List;

    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    .line 239
    invoke-virtual {p0, v6}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->setWillNotDraw(Z)V

    .line 240
    return-void
.end method

.method private a(J)F
    .locals 3

    .prologue
    .line 1274
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-object v0, v0, Lgsv;->e:Lgsx;

    invoke-interface {v0, p1, p2}, Lgsx;->a(J)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method

.method private static a(Landroid/view/MotionEvent;I)F
    .locals 2

    .prologue
    .line 539
    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 540
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 541
    const/high16 v0, 0x7fc00000    # NaNf

    .line 543
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    goto :goto_0
.end method

.method private a(F)J
    .locals 2

    .prologue
    .line 1270
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    sub-float v1, p1, v1

    iget-object v0, v0, Lgsv;->e:Lgsx;

    invoke-interface {v0, v1}, Lgsx;->b(F)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(JJJ)J
    .locals 4

    .prologue
    .line 1292
    sub-long v0, p2, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    sub-long v2, p4, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    :goto_0
    return-wide p2

    :cond_0
    move-wide p2, p4

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;I)Landroid/widget/ImageView;
    .locals 3

    .prologue
    .line 243
    invoke-static {p0, p1}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 244
    const v1, 0x7f0202a1

    .line 245
    invoke-static {p0, v1}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 247
    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 248
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 249
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 250
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 251
    return-object v2
.end method

.method private a(Landroid/widget/ImageView;Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 584
    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    .line 585
    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->i:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    .line 586
    invoke-virtual {p1}, Landroid/widget/ImageView;->getX()F

    move-result v3

    add-float/2addr v1, v3

    .line 587
    sub-float v3, v1, v2

    .line 588
    add-float/2addr v1, v2

    .line 591
    cmpg-float v2, v3, v0

    if-gez v2, :cond_1

    .line 592
    neg-float v0, v3

    .line 596
    :cond_0
    :goto_0
    add-float/2addr v1, v0

    .line 597
    add-float/2addr v0, v3

    .line 599
    iput v0, p2, Landroid/graphics/RectF;->left:F

    .line 600
    invoke-virtual {p1}, Landroid/widget/ImageView;->getTop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p2, Landroid/graphics/RectF;->top:F

    .line 601
    iput v1, p2, Landroid/graphics/RectF;->right:F

    .line 602
    invoke-virtual {p1}, Landroid/widget/ImageView;->getBottom()I

    move-result v0

    int-to-float v0, v0

    iput v0, p2, Landroid/graphics/RectF;->bottom:F

    .line 603
    return-void

    .line 593
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    .line 594
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;F)V
    .locals 14

    .prologue
    const-wide/16 v2, 0x0

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    invoke-virtual {v0}, Lgsv;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v7, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->y:F

    iget-object v8, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float/2addr v0, p1

    float-to-long v4, v0

    iget-boolean v0, v8, Lgsv;->d:Z

    invoke-static {v0}, Lb;->c(Z)V

    invoke-virtual {v8}, Lgsv;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    iget-object v0, v8, Lgsv;->e:Lgsx;

    instance-of v0, v0, Lgsy;

    invoke-static {v0}, Lb;->c(Z)V

    iget-object v0, v8, Lgsv;->e:Lgsx;

    check-cast v0, Lgsy;

    iget-wide v10, v0, Lgsy;->b:J

    add-long v12, v10, v4

    iget-wide v0, v0, Lgsy;->c:J

    add-long/2addr v4, v0

    cmp-long v0, v12, v2

    if-gez v0, :cond_2

    neg-long v0, v12

    :goto_1
    add-long v2, v12, v0

    add-long/2addr v4, v0

    new-instance v1, Lgsy;

    iget v6, v8, Lgsv;->b:F

    invoke-direct/range {v1 .. v6}, Lgsy;-><init>(JJF)V

    iput-object v1, v8, Lgsv;->e:Lgsx;

    iget-object v0, v8, Lgsv;->e:Lgsx;

    invoke-interface {v0, v10, v11}, Lgsx;->a(J)F

    move-result v0

    invoke-virtual {v8}, Lgsv;->b()V

    add-float/2addr v0, v7

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->y:F

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lgsv;->a(F)J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v4}, Lgsv;->a(F)J

    move-result-wide v4

    const-wide/32 v6, 0xf4240

    invoke-virtual/range {v1 .. v7}, Lgsc;->a(JJJ)V

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgth;)V

    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->P:F

    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->Q:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->R:F

    add-float/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v2, Lgtj;->a:Lgtj;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(F)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgqo;->c(J)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_2
    iget-wide v0, v8, Lgsv;->c:J

    cmp-long v0, v4, v0

    if-lez v0, :cond_4

    iget-wide v0, v8, Lgsv;->c:J

    sub-long/2addr v0, v4

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v2, Lgtj;->b:Lgtj;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(F)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgqo;->d(J)V

    goto :goto_2

    :cond_4
    move-wide v0, v2

    goto/16 :goto_1
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;Lgsj;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgsj;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;Lgth;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgth;)V

    return-void
.end method

.method private a(Lgsj;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1222
    invoke-virtual {p1, v0}, Lgsj;->a(Lgrs;)V

    .line 1223
    invoke-virtual {p1, v0}, Lgsj;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1224
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1225
    return-void
.end method

.method private a(Lgth;)V
    .locals 18

    .prologue
    .line 1132
    invoke-static/range {p1 .. p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgth;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    .line 1135
    const/4 v5, 0x0

    .line 1136
    move-object/from16 v0, p1

    iget v4, v0, Lgth;->d:I

    .line 1138
    const/4 v3, 0x0

    .line 1139
    const/4 v2, 0x0

    .line 1140
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1143
    const/4 v5, -0x1

    .line 1144
    add-int/lit8 v4, v4, 0x1

    .line 1145
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->I:Lgsa;

    move v6, v5

    move v5, v4

    move-object v4, v3

    move v3, v2

    .line 1154
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->y:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    iget v7, v7, Lgth;->b:F

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->f:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    rem-float v9, v2, v7

    move v8, v6

    .line 1156
    :goto_1
    if-ge v8, v5, :cond_6

    .line 1159
    sub-int v7, v8, v6

    .line 1160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v7, :cond_4

    .line 1161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgsj;

    .line 1169
    :goto_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    int-to-float v10, v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    iget v11, v11, Lgth;->b:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->f:I

    int-to-float v12, v12

    add-float/2addr v11, v12

    mul-float/2addr v10, v11

    add-float/2addr v7, v10

    add-float/2addr v7, v9

    .line 1170
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingTop()I

    move-result v10

    int-to-float v10, v10

    .line 1171
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    iget v11, v11, Lgth;->b:F

    add-float/2addr v11, v7

    .line 1172
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    iget v12, v12, Lgth;->c:F

    add-float/2addr v12, v10

    .line 1173
    float-to-int v13, v7

    float-to-int v10, v10

    float-to-int v14, v11

    float-to-int v12, v12

    invoke-virtual {v2, v13, v10, v14, v12}, Lgsj;->setBounds(IIII)V

    .line 1174
    sub-float v10, v11, v7

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    add-float/2addr v7, v10

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(F)J

    move-result-wide v10

    .line 1175
    iput-wide v10, v2, Lgsj;->b:J

    .line 1177
    if-eqz v4, :cond_2

    .line 1178
    invoke-interface {v4, v10, v11}, Lgsa;->c(J)Lgrs;

    move-result-object v12

    .line 1180
    const/4 v7, 0x1

    .line 1181
    iget-object v13, v2, Lgsj;->a:Lgrs;

    .line 1183
    if-eqz v13, :cond_0

    if-eqz v12, :cond_0

    .line 1184
    iget-object v13, v13, Lgrs;->a:Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 1185
    iget-object v13, v12, Lgrs;->a:Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 1187
    cmp-long v13, v16, v14

    if-lez v13, :cond_0

    .line 1188
    sub-long v16, v10, v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    .line 1189
    sub-long/2addr v10, v14

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    .line 1191
    cmp-long v7, v16, v10

    if-gez v7, :cond_5

    const/4 v7, 0x1

    .line 1195
    :cond_0
    :goto_3
    if-eqz v7, :cond_1

    .line 1196
    invoke-virtual {v2, v12}, Lgsj;->a(Lgrs;)V

    .line 1198
    :cond_1
    if-eqz v12, :cond_2

    .line 1199
    invoke-virtual {v12}, Lgrs;->c()V

    .line 1156
    :cond_2
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto/16 :goto_1

    .line 1147
    :cond_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->G:Lgsa;

    if-eqz v6, :cond_9

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->G:Lgsa;

    invoke-interface {v6}, Lgsa;->d()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1148
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->G:Lgsa;

    .line 1149
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->z:Z

    .line 1150
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->z:Z

    move v6, v5

    move v5, v4

    move-object v4, v3

    move v3, v2

    goto/16 :goto_0

    .line 1163
    :cond_4
    new-instance v2, Lgsj;

    invoke-direct {v2}, Lgsj;-><init>()V

    .line 1164
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-interface {v10, v7, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1165
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lgsj;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1166
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->w:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1191
    :cond_5
    const/4 v7, 0x0

    goto :goto_3

    .line 1205
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int v4, v5, v6

    if-le v2, v4, :cond_7

    .line 1206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    .line 1207
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgsj;

    .line 1208
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgsj;)V

    goto :goto_4

    .line 1211
    :cond_7
    if-eqz v3, :cond_8

    .line 1212
    const/4 v2, 0x0

    move v3, v2

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_8

    .line 1213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgsj;

    .line 1214
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lgsj;->a(F)V

    .line 1215
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(Lgsj;)F

    move-result v4

    .line 1216
    mul-int/lit8 v5, v3, 0x32

    invoke-virtual {v2, v4, v5}, Lgsj;->a(FI)V

    .line 1212
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 1219
    :cond_8
    return-void

    :cond_9
    move v6, v5

    move v5, v4

    move-object v4, v3

    move v3, v2

    goto/16 :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Z
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c()Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;Z)Z
    .locals 0

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->z:Z

    return p1
.end method

.method private b(Lgsj;)F
    .locals 8

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1245
    iget-boolean v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->p:Z

    if-nez v1, :cond_1

    .line 1262
    :cond_0
    :goto_0
    return v0

    .line 1250
    :cond_1
    invoke-virtual {p1}, Lgsj;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 1251
    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    invoke-direct {p0, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(F)J

    move-result-wide v4

    .line 1252
    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(F)J

    move-result-wide v6

    .line 1254
    const v1, 0x3f333333    # 0.7f

    .line 1256
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    .line 1257
    :goto_1
    if-eqz v2, :cond_2

    .line 1258
    invoke-interface {v2, v4, v5, v6, v7}, Lgsa;->a(JJ)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1256
    :cond_3
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->H:Lgsa;

    goto :goto_1
.end method

.method private b(J)J
    .locals 7

    .prologue
    .line 1278
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->o:Z

    if-nez v0, :cond_0

    .line 1288
    :goto_0
    return-wide p1

    .line 1281
    :cond_0
    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v4, v0, Lgqo;->d:J

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(JJJ)J

    move-result-wide v4

    .line 1282
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    invoke-virtual {v0, p1, p2}, Lgqo;->a(J)J

    move-result-wide v2

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(JJJ)J

    move-result-wide v4

    .line 1283
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->I:Lgsa;

    invoke-interface {v0, p1, p2}, Lgsa;->c(J)Lgrs;

    move-result-object v6

    .line 1284
    if-eqz v6, :cond_1

    .line 1285
    iget-object v0, v6, Lgrs;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(JJJ)J

    move-result-wide v4

    .line 1286
    invoke-virtual {v6}, Lgrs;->c()V

    :cond_1
    move-wide p1, v4

    .line 1288
    goto :goto_0
.end method

.method private b(I)Lgth;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1305
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 1306
    if-nez p1, :cond_1

    .line 1307
    sget-object v0, Lgth;->a:Lgth;

    .line 1320
    :goto_1
    return-object v0

    .line 1305
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1309
    :cond_1
    const v0, 0x3fe38e39

    .line 1310
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    if-eqz v2, :cond_2

    .line 1311
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    invoke-virtual {v0}, Lgqo;->a()F

    move-result v0

    .line 1314
    :cond_2
    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->e:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    .line 1315
    int-to-float v3, p1

    div-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1318
    div-int v1, p1, v2

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->f:I

    sub-int/2addr v1, v3

    int-to-float v3, v1

    .line 1320
    new-instance v1, Lgth;

    div-float v0, v3, v0

    invoke-direct {v1, v3, v0, v2}, Lgth;-><init>(FFI)V

    move-object v0, v1

    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Z
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m()Z

    move-result v0

    return v0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v1, Lgtj;->a:Lgtj;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v1, Lgtj;->b:Lgtj;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->N:Z

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 610
    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->P:F

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->Q:F

    .line 611
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v0, v0, Lgqo;->f:J

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->S:J

    .line 612
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v0, v0, Lgqo;->g:J

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->T:J

    .line 614
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v1, Lgtj;->a:Lgtj;

    if-ne v0, v1, :cond_0

    .line 615
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->u:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->R:F

    .line 621
    :goto_0
    return-void

    .line 616
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v1, Lgtj;->b:Lgtj;

    if-ne v0, v1, :cond_1

    .line 617
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->v:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->R:F

    goto :goto_0

    .line 619
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->R:F

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->e()V

    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 624
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 625
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->N:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lb;->c(Z)V

    .line 627
    iput-boolean v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->N:Z

    .line 628
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    iget-object v3, v3, Lgtj;->d:Ljava/util/Set;

    iget-object v0, v0, Lgqo;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqr;

    invoke-interface {v0, v3}, Lgqr;->a(Ljava/util/Set;)V

    goto :goto_2

    :cond_0
    move v0, v2

    .line 624
    goto :goto_0

    :cond_1
    move v0, v2

    .line 625
    goto :goto_1

    .line 629
    :cond_2
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->i()V

    new-instance v0, Lgsd;

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d:F

    invoke-direct {v0, v3, v4}, Lgsd;-><init>(Landroid/content/Context;F)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v3, v4, :cond_3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroupOverlay;->add(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    invoke-virtual {v0, v2}, Lgsd;->setAlpha(I)V

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->h()V

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    const-string v3, "alpha"

    new-array v4, v1, [I

    const/16 v5, 0xff

    aput v5, v4, v2

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m:I

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 630
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 631
    if-eqz v0, :cond_5

    .line 633
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 636
    :cond_5
    sget-object v0, Lfqi;->j:Lfqi;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a:Lfdw;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b:Lfqg;

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a:Lfdw;

    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b:Lfqg;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Lfdw;->b(Lfqg;Lfqi;Lhcq;)V

    iput-boolean v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c:Z

    .line 637
    :cond_6
    return-void
.end method

.method public static synthetic e(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 57
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v8

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c()Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-wide v0, v0, Lgsv;->c:J

    iget-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->n:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v1, Lgtj;->a:Lgtj;

    if-ne v0, v1, :cond_3

    iget-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->K:J

    :goto_1
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-object v2, v2, Lgsv;->e:Lgsx;

    invoke-interface {v2, v0, v1}, Lgsx;->b(J)F

    move-result v4

    long-to-float v2, v0

    iget-wide v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->n:J

    long-to-float v3, v6

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-long v2, v2

    long-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v4

    iget-wide v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->n:J

    long-to-float v4, v4

    mul-float/2addr v1, v4

    add-float/2addr v0, v1

    float-to-long v4, v0

    new-instance v0, Lgsc;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->F:Lgrw;

    invoke-direct {v0, v1}, Lgsc;-><init>(Lgrw;)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    const-wide/32 v6, 0xf4240

    invoke-virtual/range {v1 .. v7}, Lgsc;->a(JJJ)V

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    invoke-virtual {v0, p0}, Lgsc;->a(Lgsb;)V

    iget-object v7, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-boolean v0, v7, Lgsv;->d:Z

    if-nez v0, :cond_4

    move v0, v8

    :goto_2
    invoke-static {v0}, Lb;->c(Z)V

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_5

    move v0, v8

    :goto_3
    invoke-static {v0}, Lb;->b(Z)V

    iget-wide v0, v7, Lgsv;->c:J

    cmp-long v0, v4, v0

    if-gtz v0, :cond_6

    move v0, v8

    :goto_4
    invoke-static {v0}, Lb;->b(Z)V

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    move v9, v8

    :cond_0
    invoke-static {v9}, Lb;->b(Z)V

    new-instance v1, Lgsy;

    iget v6, v7, Lgsv;->b:F

    invoke-direct/range {v1 .. v6}, Lgsy;-><init>(JJF)V

    invoke-virtual {v7, v1, v8, v8}, Lgsv;->a(Lgsy;ZZ)V

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g()V

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d()V

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    :cond_1
    return-void

    :cond_2
    move v0, v9

    goto/16 :goto_0

    :cond_3
    iget-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->L:J

    goto :goto_1

    :cond_4
    move v0, v9

    goto :goto_2

    :cond_5
    move v0, v9

    goto :goto_3

    :cond_6
    move v0, v9

    goto :goto_4
.end method

.method public static synthetic f(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->l:I

    return v0
.end method

.method private f()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 640
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->N:Z

    if-eqz v0, :cond_2

    .line 641
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->N:Z

    invoke-static {v0}, Lb;->c(Z)V

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    iget-object v2, v0, Lgtj;->d:Ljava/util/Set;

    iget-object v0, v1, Lgqo;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqr;

    invoke-interface {v0, v1, v2}, Lgqr;->a(Lgqo;Ljava/util/Set;)V

    goto :goto_0

    :cond_0
    iput-object v9, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    iput-boolean v7, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->N:Z

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->i()V

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m()Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    iput v8, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->y:F

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-boolean v1, v0, Lgsv;->d:Z

    invoke-static {v1}, Lb;->c(Z)V

    new-instance v1, Lgsy;

    const-wide/16 v2, 0x0

    iget-wide v4, v0, Lgsv;->c:J

    iget v6, v0, Lgsv;->b:F

    invoke-direct/range {v1 .. v6}, Lgsy;-><init>(JJF)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v7, v2}, Lgsv;->a(Lgsy;ZZ)V

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g()V

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d()V

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    invoke-virtual {v0, p0}, Lgsc;->b(Lgsb;)V

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    invoke-virtual {v0}, Lgsc;->a()V

    iput-object v9, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->V:Lgti;

    invoke-virtual {v0, v8}, Lgti;->a(F)V

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0, v7}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 643
    :cond_2
    return-void
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 793
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 795
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 797
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsj;

    .line 798
    const-string v2, "alpha"

    new-array v3, v5, [I

    aput v4, v3, v4

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 799
    new-instance v3, Lgtb;

    invoke-direct {v3, p0, v0}, Lgtb;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;Lgsj;)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 805
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 808
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iput-boolean v5, v0, Lgsv;->g:Z

    .line 809
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgth;)V

    .line 810
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsj;

    .line 811
    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(Lgsj;)F

    move-result v2

    .line 812
    invoke-virtual {v0, v2}, Lgsj;->a(F)V

    .line 813
    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 814
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    .line 816
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iput-boolean v4, v0, Lgsv;->g:Z

    .line 817
    return-void

    .line 813
    nop

    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method public static synthetic g(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V
    .locals 4

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v1, Lgtj;->a:Lgtj;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v2, v1, Lgqo;->f:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lgqo;->c(J)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v1, Lgtj;->b:Lgtj;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v2, v1, Lgqo;->g:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lgqo;->d(J)V

    goto :goto_0
.end method

.method public static synthetic h(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)F
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->k:F

    return v0
.end method

.method private h()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    const-wide/16 v10, 0x3e8

    .line 931
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    if-nez v0, :cond_1

    .line 949
    :cond_0
    :goto_0
    return-void

    .line 937
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    sget-object v1, Lgtj;->a:Lgtj;

    if-ne v0, v1, :cond_5

    .line 938
    iget-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->K:J

    .line 939
    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    neg-int v0, v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 945
    :goto_1
    iget-object v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    div-long v6, v2, v10

    const-wide/32 v8, 0xea60

    div-long v8, v6, v8

    long-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v5, v8

    div-long v8, v6, v10

    long-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v1, v8

    rem-int/lit8 v8, v1, 0x3c

    rem-long/2addr v6, v10

    long-to-int v6, v6

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m()Z

    move-result v1

    if-eqz v1, :cond_6

    const v1, 0x7f09008d

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v9, v12

    const/4 v5, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v9, v5

    const/4 v5, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v5

    invoke-virtual {v7, v1, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, v4, Lgsd;->b:Ljava/lang/String;

    invoke-static {v5, v1}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    iput-object v1, v4, Lgsd;->b:Ljava/lang/String;

    iget-object v5, v4, Lgsd;->a:Landroid/graphics/Paint;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v4, Lgsd;->c:I

    invoke-virtual {v4}, Lgsd;->invalidateSelf()V

    .line 947
    :cond_2
    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(J)F

    move-result v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getX()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 948
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Lgsd;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v1}, Lgsd;->getIntrinsicHeight()I

    move-result v5

    div-int/lit8 v6, v4, 0x2

    sub-int/2addr v0, v6

    add-int/2addr v4, v0

    sub-int v5, v2, v5

    iput v12, v1, Lgsd;->d:I

    if-gez v0, :cond_3

    iput v0, v1, Lgsd;->d:I

    :cond_3
    if-le v4, v3, :cond_4

    sub-int v3, v4, v3

    iput v3, v1, Lgsd;->d:I

    :cond_4
    iget v3, v1, Lgsd;->d:I

    sub-int/2addr v0, v3

    iget v3, v1, Lgsd;->d:I

    sub-int v3, v4, v3

    invoke-virtual {v1, v0, v5, v3, v2}, Lgsd;->setBounds(IIII)V

    goto/16 :goto_0

    .line 941
    :cond_5
    iget-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->L:J

    .line 942
    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    int-to-float v0, v0

    div-float/2addr v0, v4

    goto/16 :goto_1

    .line 945
    :cond_6
    const v1, 0x7f09008c

    goto :goto_2
.end method

.method public static synthetic i(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Lgsv;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    return-object v0
.end method

.method private i()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 953
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    if-nez v0, :cond_0

    .line 973
    :goto_0
    return-void

    .line 957
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    .line 958
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 960
    const-string v2, "alpha"

    const/4 v3, 0x1

    new-array v3, v3, [I

    aput v4, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 961
    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 962
    new-instance v3, Lgtc;

    invoke-direct {v3, p0, v0, v1}, Lgtc;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;Landroid/view/ViewGroup;Lgsd;)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 971
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 972
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->A:Lgsd;

    goto :goto_0
.end method

.method public static synthetic j(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Lgsc;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 1030
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    invoke-virtual {v0}, Lgsv;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1036
    :cond_0
    return-void

    .line 1033
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsj;

    .line 1034
    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(Lgsj;)F

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lgsj;->a(FI)V

    goto :goto_0
.end method

.method public static synthetic k(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Lgsa;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->H:Lgsa;

    return-object v0
.end method

.method private k()V
    .locals 6

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 1113
    const-wide/16 v0, 0x1

    .line 1114
    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    if-eqz v3, :cond_0

    .line 1115
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v0, v0, Lgqo;->d:J

    .line 1118
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-wide v4, v3, Lgsv;->c:J

    cmp-long v3, v4, v0

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget v3, v3, Lgsv;->b:F

    float-to-int v3, v3

    if-eq v3, v2, :cond_2

    .line 1119
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    const/4 v4, 0x0

    iput-object v4, v3, Lgsv;->f:Lgsz;

    .line 1120
    new-instance v3, Lgsv;

    invoke-direct {v3, v2, v0, v1}, Lgsv;-><init>(IJ)V

    iput-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    .line 1121
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iput-object p0, v0, Lgsv;->f:Lgsz;

    .line 1123
    :cond_2
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1126
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->u:Landroid/widget/ImageView;

    iget-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->K:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(J)F

    move-result v1

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 1127
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->v:Landroid/widget/ImageView;

    iget-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->L:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(J)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 1128
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->h()V

    .line 1129
    return-void
.end method

.method public static synthetic l(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->j()V

    return-void
.end method

.method public static synthetic m(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Lgsa;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->G:Lgsa;

    return-object v0
.end method

.method private m()Z
    .locals 1

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-boolean v0, v0, Lgsv;->d:Z

    return v0
.end method

.method public static synthetic n(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->p:Z

    return v0
.end method

.method public static synthetic o(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Lgth;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1066
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsj;

    iget-wide v2, v0, Lgsj;->b:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(J)F

    move-result v2

    invoke-virtual {v0}, Lgsj;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    sub-float v2, v4, v2

    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_0

    iget v4, v3, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float v2, v4, v2

    float-to-int v2, v2

    iget v4, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v5

    add-int/2addr v5, v2

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v2, v4, v5, v3}, Lgsj;->setBounds(IIII)V

    goto :goto_0

    .line 1067
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->l()V

    .line 1068
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->invalidate()V

    .line 1069
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 332
    iput p1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->q:I

    .line 333
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->U:Lgtg;

    invoke-virtual {v0}, Lgtg;->a()V

    .line 335
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->U:Lgtg;

    int-to-long v2, p1

    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->P:F

    invoke-virtual {v0, v2, v3, v1}, Lgtg;->a(JF)V

    .line 337
    :cond_0
    return-void
.end method

.method public final a(Lgqo;Lgqq;)V
    .locals 2

    .prologue
    .line 1048
    sget-object v0, Lgtf;->b:[I

    invoke-virtual {p2}, Lgqq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1060
    :goto_0
    return-void

    .line 1050
    :pswitch_0
    iget-wide v0, p1, Lgqo;->f:J

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->K:J

    .line 1051
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->l()V

    .line 1052
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->invalidate()V

    goto :goto_0

    .line 1055
    :pswitch_1
    iget-wide v0, p1, Lgqo;->g:J

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->L:J

    .line 1056
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->l()V

    .line 1057
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->invalidate()V

    goto :goto_0

    .line 1048
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lgqo;Lgrw;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 276
    if-eqz p1, :cond_0

    .line 277
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    invoke-interface {p2}, Lgrw;->a()Lgqo;

    move-result-object v0

    invoke-virtual {p1, v0}, Lgqo;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 280
    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 283
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->F:Lgrw;

    if-ne p2, v0, :cond_2

    .line 326
    :goto_1
    return-void

    .line 280
    :cond_0
    if-nez p2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 288
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    if-eqz v0, :cond_3

    .line 289
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->f()V

    .line 290
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    invoke-virtual {v0, p0}, Lgqo;->b(Lgqr;)V

    .line 291
    iput-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->F:Lgrw;

    .line 293
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->G:Lgsa;

    invoke-interface {v0, p0}, Lgsa;->b(Lgsb;)V

    .line 294
    iput-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->G:Lgsa;

    .line 296
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->H:Lgsa;

    invoke-interface {v0, p0}, Lgsa;->b(Lgsb;)V

    .line 297
    iput-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->H:Lgsa;

    .line 299
    iput-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->I:Lgsa;

    .line 302
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->J:Lgsc;

    if-nez v0, :cond_5

    :goto_2
    invoke-static {v1}, Lb;->c(Z)V

    .line 305
    :cond_3
    iput-object p1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    .line 306
    iput-object p2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->F:Lgrw;

    .line 308
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(I)Lgth;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    if-eqz v1, :cond_4

    .line 310
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    invoke-virtual {v1, p0}, Lgqo;->a(Lgqr;)V

    .line 311
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v2, v1, Lgqo;->f:J

    iput-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->K:J

    .line 312
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v2, v1, Lgqo;->g:J

    iput-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->L:J

    .line 314
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->F:Lgrw;

    invoke-interface {v1}, Lgrw;->c()Lgsa;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->G:Lgsa;

    .line 315
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->G:Lgsa;

    invoke-interface {v1, p0}, Lgsa;->a(Lgsb;)V

    .line 317
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->F:Lgrw;

    invoke-interface {v1}, Lgrw;->d()Lgsa;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->H:Lgsa;

    .line 318
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->H:Lgsa;

    invoke-interface {v1, p0}, Lgsa;->a(Lgsb;)V

    .line 320
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->F:Lgrw;

    invoke-interface {v1}, Lgrw;->b()Lgsa;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->I:Lgsa;

    .line 323
    :cond_4
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->k()V

    .line 324
    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgth;)V

    .line 325
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->requestLayout()V

    goto :goto_1

    :cond_5
    move v1, v2

    .line 302
    goto :goto_2
.end method

.method public final a(Lgqo;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 1044
    return-void
.end method

.method public final a(Lgsa;Lgrs;)V
    .locals 1

    .prologue
    .line 990
    new-instance v0, Lgtd;

    invoke-direct {v0, p0, p1}, Lgtd;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;Lgsa;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->post(Ljava/lang/Runnable;)Z

    .line 1003
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 1026
    const-string v0, "Failed to render thumbnail"

    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1027
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 1041
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgth;)V

    .line 1074
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->j()V

    .line 1075
    return-void
.end method

.method public final b(Lgsa;)V
    .locals 1

    .prologue
    .line 1007
    new-instance v0, Lgte;

    invoke-direct {v0, p0, p1}, Lgte;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;Lgsa;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->post(Ljava/lang/Runnable;)Z

    .line 1022
    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 440
    instance-of v0, p1, Lgsj;

    if-eqz v0, :cond_0

    .line 441
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->invalidate(Landroid/graphics/Rect;)V

    .line 445
    :goto_0
    return-void

    .line 443
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 398
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 400
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 402
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingLeft()I

    move-result v0

    .line 403
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingTop()I

    move-result v1

    .line 404
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 405
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    .line 401
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 408
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 409
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 412
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingTop()I

    move-result v6

    .line 413
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingBottom()I

    move-result v1

    sub-int v7, v0, v1

    .line 416
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingLeft()I

    move-result v0

    int-to-float v1, v0

    .line 417
    iget-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->K:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(J)F

    move-result v0

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    int-to-float v2, v2

    sub-float v3, v0, v2

    .line 418
    int-to-float v2, v6

    int-to-float v4, v7

    iget-object v5, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->r:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 421
    iget-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->L:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(J)F

    move-result v0

    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    int-to-float v1, v1

    add-float/2addr v1, v0

    .line 422
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v3, v0

    .line 423
    int-to-float v2, v6

    int-to-float v4, v7

    iget-object v5, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->r:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 427
    iget-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->K:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(J)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->s:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    sub-float v1, v0, v1

    .line 428
    iget-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->L:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(J)F

    move-result v0

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->s:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    add-float v3, v0, v2

    .line 429
    int-to-float v0, v6

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->s:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    div-float/2addr v2, v8

    add-float/2addr v2, v0

    .line 430
    iget-object v5, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->s:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 431
    int-to-float v0, v7

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->t:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    div-float/2addr v2, v8

    sub-float v2, v0, v2

    .line 432
    iget-object v5, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->t:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 433
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 434
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 469
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 498
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    return v0

    .line 471
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 472
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->O:I

    .line 473
    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->O:I

    invoke-static {p1, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->P:F

    .line 475
    iget v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->P:F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->u:Landroid/widget/ImageView;

    invoke-direct {p0, v3, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Landroid/widget/ImageView;Landroid/graphics/RectF;)V

    iget v5, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->v:Landroid/widget/ImageView;

    invoke-direct {p0, v3, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Landroid/widget/ImageView;Landroid/graphics/RectF;)V

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v0, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v7, v4, v3

    if-lez v7, :cond_1

    sub-float v7, v4, v3

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    sub-float/2addr v5, v7

    sub-float/2addr v4, v7

    add-float/2addr v3, v7

    add-float/2addr v0, v7

    :cond_1
    cmpl-float v5, v6, v5

    if-ltz v5, :cond_2

    cmpg-float v5, v6, v4

    if-gtz v5, :cond_2

    sget-object v0, Lgtj;->a:Lgtj;

    :goto_2
    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    .line 476
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    if-eqz v0, :cond_0

    .line 477
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d()V

    .line 478
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->U:Lgtg;

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->q:I

    int-to-long v4, v3

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->P:F

    invoke-virtual {v0, v4, v5, v3}, Lgtg;->a(JF)V

    goto :goto_0

    .line 475
    :cond_2
    cmpl-float v5, v6, v3

    if-ltz v5, :cond_3

    cmpg-float v0, v6, v0

    if-gtz v0, :cond_3

    sget-object v0, Lgtj;->b:Lgtj;

    goto :goto_2

    :cond_3
    cmpl-float v0, v6, v4

    if-lez v0, :cond_4

    cmpg-float v0, v6, v3

    if-gez v0, :cond_4

    sget-object v0, Lgtj;->c:Lgtj;

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 486
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->O:I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 487
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->U:Lgtg;

    invoke-virtual {v0}, Lgtg;->a()V

    .line 494
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->f()V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 498
    goto/16 :goto_1

    .line 469
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 369
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingTop()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 370
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    sub-int v1, p4, p2

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 371
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    sub-int v1, p5, p3

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 372
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->k()V

    .line 375
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingTop()I

    move-result v0

    .line 376
    sub-int v1, p5, p3

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 378
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    .line 379
    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    sub-int v3, v2, v3

    .line 380
    iget-object v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->u:Landroid/widget/ImageView;

    invoke-virtual {v4, v3, v0, v2, v1}, Landroid/widget/ImageView;->layout(IIII)V

    .line 382
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    .line 383
    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    add-int/2addr v3, v2

    .line 384
    iget-object v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->v:Landroid/widget/ImageView;

    invoke-virtual {v4, v2, v0, v3, v1}, Landroid/widget/ImageView;->layout(IIII)V

    .line 387
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->l()V

    .line 390
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(I)Lgth;

    move-result-object v0

    .line 391
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->B:Lgth;

    invoke-static {v0, v1}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 392
    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgth;)V

    .line 394
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 344
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 347
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 348
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingRight()I

    move-result v2

    sub-int v2, v0, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 349
    invoke-direct {p0, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(I)Lgth;

    move-result-object v2

    .line 352
    iget v2, v2, Lgth;->c:F

    float-to-int v2, v2

    .line 354
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingTop()I

    move-result v3

    add-int/2addr v3, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    .line 357
    invoke-static {v0, p1, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->resolveSizeAndState(III)I

    move-result v0

    .line 358
    invoke-static {v3, p2, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->resolveSizeAndState(III)I

    move-result v1

    .line 356
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->setMeasuredDimension(II)V

    .line 360
    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 361
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 362
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->u:Landroid/widget/ImageView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/ImageView;->measure(II)V

    .line 363
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->v:Landroid/widget/ImageView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/ImageView;->measure(II)V

    .line 364
    return-void

    :cond_0
    move v0, v1

    .line 344
    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1091
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 1092
    check-cast p1, Landroid/os/Bundle;

    .line 1093
    const-string v0, "trimHandleInteractionAlreadyLogged"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c:Z

    .line 1095
    const-string v0, "superViewInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    .line 1097
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1098
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1081
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1082
    const-string v1, "superViewInstanceState"

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1083
    const-string v1, "trimHandleInteractionAlreadyLogged"

    iget-boolean v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1086
    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 503
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    if-nez v0, :cond_0

    .line 504
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 535
    :goto_0
    return v1

    .line 507
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->O:I

    invoke-static {p1, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 508
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 534
    :cond_1
    :goto_1
    iput v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->P:F

    goto :goto_0

    .line 510
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->N:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->Q:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->l:I

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    .line 511
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->e()V

    .line 514
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->N:Z

    if-eqz v0, :cond_1

    .line 515
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->U:Lgtg;

    iget v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->q:I

    int-to-long v4, v4

    invoke-virtual {v0, v4, v5, v3}, Lgtg;->a(JF)V

    .line 516
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Lb;->c(Z)V

    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->Q:F

    sub-float v0, v3, v0

    iget-object v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->D:Lgsv;

    iget-object v4, v4, Lgsv;->e:Lgsx;

    invoke-interface {v4, v0}, Lgsx;->d(F)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->V:Lgti;

    invoke-virtual {v6}, Lgti;->a()Z

    move-result v6

    if-nez v6, :cond_3

    sget-object v6, Lgtf;->a:[I

    iget-object v7, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->M:Lgtj;

    invoke-virtual {v7}, Lgtj;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    :cond_3
    :goto_3
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->m()Z

    move-result v4

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->R:F

    add-float/2addr v4, v0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v5, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    sub-int/2addr v0, v5

    int-to-float v0, v0

    iget-object v5, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->C:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    sub-float v0, v4, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->j:I

    int-to-float v6, v6

    cmpg-float v0, v0, v6

    if-gez v0, :cond_7

    const/high16 v0, -0x40800000    # -1.0f

    :goto_4
    iget v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->g:I

    int-to-float v6, v6

    add-float/2addr v4, v6

    sub-float v4, v5, v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->j:I

    int-to-float v4, v4

    cmpg-float v2, v2, v4

    if-gez v2, :cond_6

    const/high16 v2, 0x3f800000    # 1.0f

    :cond_4
    :goto_5
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->V:Lgti;

    invoke-virtual {v0, v2}, Lgti;->a(F)V

    goto/16 :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :pswitch_1
    iget-object v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v8, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->S:J

    add-long/2addr v4, v8

    invoke-direct {p0, v4, v5}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(J)J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, Lgqo;->c(J)V

    goto :goto_3

    :pswitch_2
    iget-object v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v8, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->T:J

    add-long/2addr v4, v8

    invoke-direct {p0, v4, v5}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(J)J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, Lgqo;->d(J)V

    goto :goto_3

    :pswitch_3
    iget-wide v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->T:J

    iget-wide v8, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->S:J

    sub-long/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    iget-wide v8, v8, Lgqo;->d:J

    sub-long/2addr v8, v6

    iget-wide v10, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->S:J

    add-long/2addr v4, v10

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(J)J

    move-result-wide v4

    iget-object v8, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    invoke-virtual {v8, v4, v5}, Lgqo;->c(J)V

    iget-object v8, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    add-long/2addr v6, v4

    invoke-virtual {v8, v6, v7}, Lgqo;->d(J)V

    iget-object v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->E:Lgqo;

    invoke-virtual {v6, v4, v5}, Lgqo;->c(J)V

    goto/16 :goto_3

    .line 521
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->O:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    if-ne v0, v2, :cond_1

    .line 522
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->U:Lgtg;

    invoke-virtual {v0}, Lgtg;->a()V

    .line 529
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->f()V

    goto/16 :goto_1

    :cond_6
    move v2, v0

    goto :goto_5

    :cond_7
    move v0, v2

    goto :goto_4

    .line 508
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 516
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 449
    instance-of v0, p1, Lgsj;

    if-eqz v0, :cond_0

    .line 450
    invoke-virtual {p0, p2, p3, p4}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 454
    :goto_0
    return-void

    .line 452
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 458
    instance-of v0, p1, Lgsj;

    if-eqz v0, :cond_0

    .line 459
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 463
    :goto_0
    return-void

    .line 461
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
