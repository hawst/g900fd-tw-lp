.class public Lcom/google/android/libraries/youtube/conversation/DraggableEditText;
.super Landroid/widget/EditText;
.source "SourceFile"


# instance fields
.field public a:Landroid/graphics/Rect;

.field public b:Z

.field private c:Landroid/graphics/PointF;

.field private d:Landroid/graphics/PointF;

.field private e:Landroid/graphics/PointF;

.field private f:Landroid/graphics/PointF;

.field private g:Landroid/graphics/PointF;

.field private h:Landroid/graphics/PointF;

.field private i:Z

.field private j:Landroid/view/View;

.field private k:Landroid/graphics/Rect;

.field private l:I

.field private m:Landroid/graphics/Canvas;

.field private n:Landroid/graphics/Bitmap;

.field private o:Landroid/graphics/Paint;

.field private p:Ljava/lang/String;

.field private q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 74
    invoke-direct {p0, p1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Landroid/content/Context;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Landroid/content/Context;)V

    .line 85
    return-void
.end method

.method private static a(FFF)F
    .locals 1

    .prologue
    .line 177
    invoke-static {p1, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;I)I
    .locals 0

    .prologue
    .line 29
    iput p1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->l:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->k:Landroid/graphics/Rect;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->p:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a()V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->getRootView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->j:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lfbo;

    invoke-direct {v1, p0}, Lfbo;-><init>(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/Anton.ttf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 99
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 100
    return-void
.end method

.method private a(Z)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 263
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    .line 264
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 265
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    div-float/2addr v3, v6

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    div-float/2addr v4, v6

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    div-float/2addr v5, v6

    add-float/2addr v0, v5

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 270
    iget v0, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setLeft(I)V

    .line 271
    iget v0, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setTop(I)V

    .line 272
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setBottom(I)V

    .line 273
    iget v0, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setRight(I)V

    .line 274
    if-eqz p1, :cond_0

    .line 275
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    .line 276
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 277
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 278
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget v3, v0, Landroid/graphics/PointF;->x:F

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    div-float v0, v3, v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 279
    iget v0, v1, Landroid/graphics/PointF;->x:F

    float-to-double v2, v0

    cmpl-double v0, v2, v8

    if-lez v0, :cond_0

    iget v0, v1, Landroid/graphics/PointF;->y:F

    float-to-double v2, v0

    cmpl-double v0, v2, v8

    if-lez v0, :cond_0

    .line 280
    iget v0, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setScaleX(F)V

    .line 281
    iget v0, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setScaleY(F)V

    .line 284
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;Z)Z
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->i:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;I)I
    .locals 0

    .prologue
    .line 29
    iput p1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->q:I

    return p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->j:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->l:I

    return v0
.end method

.method public static synthetic d(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->p:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->q:I

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 106
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    .line 107
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    .line 108
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v4, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d:Landroid/graphics/PointF;

    .line 109
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->e:Landroid/graphics/PointF;

    .line 110
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->f:Landroid/graphics/PointF;

    .line 111
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->g:Landroid/graphics/PointF;

    .line 112
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    .line 113
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->k:Landroid/graphics/Rect;

    .line 114
    iput v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->l:I

    .line 115
    iput-boolean v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->i:Z

    .line 116
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->o:Landroid/graphics/Paint;

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->o:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 118
    iput-boolean v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->b:Z

    .line 119
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 120
    new-instance v0, Lfbp;

    invoke-direct {v0, p0}, Lfbp;-><init>(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 146
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 293
    invoke-super {p0, p1}, Landroid/widget/EditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 294
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->m:Landroid/graphics/Canvas;

    if-nez v0, :cond_0

    .line 311
    :goto_0
    return-void

    .line 299
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->getCurrentTextColor()I

    move-result v6

    .line 300
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v7

    .line 301
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 302
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 303
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setStrokeMiter(F)V

    .line 304
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setTextColor(I)V

    .line 305
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 306
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->m:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->n:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v3, v2

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->n:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v4, v2

    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->o:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->m:Landroid/graphics/Canvas;

    invoke-super {p0, v0}, Landroid/widget/EditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->n:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 309
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 310
    invoke-virtual {p0, v6}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setTextColor(I)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 245
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    sub-int v1, p4, p2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 246
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    sub-int v1, p5, p3

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 247
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->n:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->n:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->n:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, v1, :cond_1

    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->n:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->n:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->m:Landroid/graphics/Canvas;

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 249
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Z)V

    .line 252
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->h:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->g:Landroid/graphics/PointF;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v4, v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 254
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->g:Landroid/graphics/PointF;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v4, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 259
    :goto_0
    invoke-super/range {p0 .. p5}, Landroid/widget/EditText;->onLayout(ZIIII)V

    .line 260
    return-void

    .line 257
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Z)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v9, 0x0

    .line 182
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->isFocusable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 225
    :goto_0
    return v0

    .line 185
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->i:Z

    if-nez v1, :cond_1

    .line 188
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 189
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setSelection(I)V

    goto :goto_0

    .line 193
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 194
    packed-switch v1, :pswitch_data_0

    .line 225
    :goto_1
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 196
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->e:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 197
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->e:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 200
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->f:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->e:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 201
    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->f:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->e:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 203
    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    div-float/2addr v1, v5

    .line 204
    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    div-float/2addr v2, v5

    sub-float v2, v6, v2

    .line 205
    iget-object v3, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    div-float/2addr v3, v5

    .line 206
    iget-object v4, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    div-float/2addr v4, v5

    sub-float v4, v6, v4

    .line 210
    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v1

    if-lez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v2

    if-ltz v5, :cond_4

    :cond_2
    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v1

    if-gtz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->f:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v9

    if-gtz v5, :cond_4

    :cond_3
    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v2

    if-ltz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->f:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v9

    if-gez v5, :cond_5

    .line 213
    :cond_4
    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v6, v5, Landroid/graphics/PointF;->x:F

    iget-object v7, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->f:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->g:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 214
    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v1, v2}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(FFF)F

    move-result v1

    iput v1, v5, Landroid/graphics/PointF;->x:F

    .line 216
    :cond_5
    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v4

    if-ltz v1, :cond_8

    :cond_6
    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v3

    if-gtz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->f:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v9

    if-gtz v1, :cond_8

    :cond_7
    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->f:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v9

    if-gez v1, :cond_9

    .line 219
    :cond_8
    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v2, v1, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->f:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget-object v6, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->g:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 220
    iget-object v1, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v2, v3, v4}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(FFF)F

    move-result v2

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 222
    :cond_9
    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Z)V

    .line 223
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->e:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 224
    iget-object v0, p0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->e:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto/16 :goto_1

    .line 194
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
