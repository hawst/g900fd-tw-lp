.class public Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;
.super Landroid/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;
.implements Leaw;
.implements Lebt;
.implements Lgqr;


# instance fields
.field public final a:Lgqh;

.field public b:Landroid/net/Uri;

.field public c:Lgqo;

.field private d:F

.field private final e:Lgso;

.field private volatile f:Z

.field private volatile g:Z

.field private h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

.field private i:Landroid/widget/ImageButton;

.field private j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

.field private k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

.field private l:Landroid/widget/ScrollView;

.field private m:I

.field private n:Lgrx;

.field private o:Leau;

.field private p:Lebp;

.field private q:J

.field private r:Ljava/util/Set;

.field private s:Lfdw;

.field private t:Lfqg;

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 90
    new-instance v0, Lgqh;

    invoke-direct {v0}, Lgqh;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a:Lgqh;

    .line 91
    new-instance v0, Lgso;

    invoke-direct {v0, p0}, Lgso;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->e:Lgso;

    .line 105
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->m:I

    .line 131
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->q:J

    .line 133
    const-class v0, Lgqq;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->r:Ljava/util/Set;

    .line 745
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;J)J
    .locals 2

    .prologue
    .line 66
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->q:J

    return-wide v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;)Leau;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    return-object v0
.end method

.method private a(Landroid/graphics/SurfaceTexture;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 640
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->p:Lebp;

    if-nez v0, :cond_1

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 644
    :cond_1
    const/4 v0, 0x0

    .line 646
    if-eqz p1, :cond_2

    .line 647
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 650
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->f:Z

    .line 651
    if-eqz p2, :cond_3

    .line 652
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->p:Lebp;

    invoke-virtual {v1, v2, v3, v0}, Leau;->b(Leav;ILjava/lang/Object;)V

    goto :goto_0

    .line 654
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->p:Lebp;

    invoke-virtual {v1, v2, v3, v0}, Leau;->a(Leav;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 66
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->r:Ljava/util/Set;

    sget-object v3, Lgqq;->a:Lgqq;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->r:Ljava/util/Set;

    sget-object v3, Lgqq;->b:Lgqq;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    iget-object v2, v2, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->c:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_4

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;Z)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Leau;->a(IZ)V

    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;)J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->q:J

    return-wide v0
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 689
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lgsm;

    invoke-direct {v1, p0}, Lgsm;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;)V

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 698
    return-void
.end method

.method public static synthetic c(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->f()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;)Lgso;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->e:Lgso;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;)Lgqh;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a:Lgqh;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 529
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    invoke-virtual {v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 530
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->setTranslationY(F)V

    .line 531
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->setTranslationY(F)V

    .line 532
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->setTranslationY(F)V

    .line 536
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getHeight()I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x3

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float v0, v3, v0

    .line 538
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->setAlpha(F)V

    .line 539
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->setAlpha(F)V

    .line 540
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 597
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a:Lgqh;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->e:Lgso;

    .line 599
    invoke-virtual {v0, v1}, Lgqh;->a(Lgqi;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->g:Z

    if-eqz v0, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    iput-boolean v6, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->g:Z

    .line 608
    new-instance v0, Leaz;

    .line 609
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->b:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v7}, Leaz;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;I)V

    .line 611
    new-instance v1, Lgsn;

    invoke-direct {v1, p0, v0}, Lgsn;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;Lebz;)V

    iput-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->p:Lebp;

    .line 612
    new-instance v1, Lebc;

    invoke-direct {v1, v0}, Lebc;-><init>(Lebz;)V

    .line 613
    new-instance v0, Lgsh;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    .line 614
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c()Lgrx;

    move-result-object v3

    iget-object v3, v3, Lgrx;->b:Lgry;

    invoke-direct {v0, v2, v3}, Lgsh;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;Lgsa;)V

    .line 617
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    const/4 v3, 0x4

    new-array v3, v3, [Lecc;

    iget-object v4, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->p:Lebp;

    aput-object v4, v3, v5

    aput-object v1, v3, v6

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    .line 620
    new-instance v4, Lgsg;

    invoke-direct {v4, v1}, Lgsg;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)V

    aput-object v4, v3, v7

    const/4 v1, 0x3

    aput-object v0, v3, v1

    .line 617
    invoke-virtual {v2, v3}, Leau;->a([Lecc;)V

    .line 623
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    iget-object v0, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->a:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    .line 624
    if-eqz v0, :cond_2

    .line 625
    invoke-direct {p0, v0, v5}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Landroid/graphics/SurfaceTexture;Z)V

    .line 628
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    iget v1, v1, Lgqo;->c:I

    iget v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->f:I

    if-eq v2, v1, :cond_0

    iput v1, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->f:I

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->a()V

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    if-eqz v0, :cond_2

    .line 709
    const/high16 v0, -0x80000000

    .line 710
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v1}, Leau;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->f:Z

    if-nez v1, :cond_1

    .line 711
    :cond_0
    const v0, 0x7fffffff

    .line 713
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a:Lgqh;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->e:Lgso;

    invoke-virtual {v1, v2, v0}, Lgqh;->a(Lgqi;I)V

    .line 715
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 405
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->g()V

    .line 406
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 383
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lgsl;

    invoke-direct {v1, p0, p1}, Lgsl;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 400
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->g()V

    .line 401
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 434
    return-void
.end method

.method public final a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 0

    .prologue
    .line 424
    return-void
.end method

.method public a(Landroid/net/Uri;Lgqo;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 545
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    invoke-virtual {v0, p0}, Lgqo;->b(Lgqr;)V

    .line 549
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    if-eqz v0, :cond_1

    .line 550
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v0}, Leau;->e()V

    .line 551
    iput-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->p:Lebp;

    .line 554
    :cond_1
    iput-object p2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    .line 555
    iput-object p1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->b:Landroid/net/Uri;

    .line 557
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c()Lgrx;

    move-result-object v0

    .line 558
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    iget-object v2, v0, Lgrx;->a:Lgqo;

    invoke-static {v2, v1}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lgrx;->g()V

    iput-object v1, v0, Lgrx;->a:Lgqo;

    invoke-virtual {v0}, Lgrx;->e()V

    .line 560
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    if-eqz v1, :cond_5

    .line 561
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    invoke-virtual {v1, p0}, Lgqo;->a(Lgqr;)V

    .line 562
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    invoke-virtual {v2}, Lgqo;->a()F

    move-result v2

    iget v3, v1, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->d:F

    cmpl-float v3, v3, v2

    if-eqz v3, :cond_3

    iput v2, v1, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->d:F

    invoke-virtual {v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->requestLayout()V

    .line 563
    :cond_3
    iget-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->q:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 564
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    invoke-virtual {v1}, Lgqo;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->q:J

    .line 567
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgqo;Lgrw;)V

    .line 568
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->setVisibility(I)V

    .line 570
    sget-object v0, Lfqi;->j:Lfqi;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Lfqi;)V

    .line 576
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->a(Lgqo;)V

    .line 578
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->f()V

    .line 579
    return-void

    .line 572
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v0, v3, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgqo;Lgrw;)V

    .line 573
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Landroid/widget/ScrollView;)V
    .locals 2

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 197
    const/4 v0, 0x0

    .line 198
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 199
    :goto_0
    if-eqz v1, :cond_0

    .line 200
    if-ne v1, p1, :cond_1

    .line 201
    const/4 v0, 0x1

    .line 206
    :cond_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 207
    iput-object p1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    .line 208
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 209
    return-void

    .line 204
    :cond_1
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Leat;)V
    .locals 1

    .prologue
    .line 410
    const-string v0, "Unable to play video"

    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 411
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->d()V

    .line 412
    return-void
.end method

.method public final a(Lebi;)V
    .locals 0

    .prologue
    .line 419
    return-void
.end method

.method public final a(Lfdw;Lfqg;)V
    .locals 2

    .prologue
    .line 217
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->s:Lfdw;

    .line 218
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqg;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->t:Lfqg;

    .line 219
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    if-eqz v0, :cond_0

    .line 220
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, v1, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a:Lfdw;

    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqg;

    iput-object v0, v1, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b:Lfqg;

    .line 222
    :cond_0
    return-void
.end method

.method public a(Lfqi;)V
    .locals 7

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->s:Lfdw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->t:Lfqg;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->u:Z

    if-nez v0, :cond_1

    .line 686
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->s:Lfdw;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->t:Lfqg;

    iget-object v2, v0, Lfdw;->a:Lfdx;

    invoke-interface {v2}, Lfdx;->S()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lfdw;->a()Lhkd;

    move-result-object v2

    new-instance v3, Lhud;

    invoke-direct {v3}, Lhud;-><init>()V

    iput-object v3, v2, Lhkd;->f:Lhud;

    iget-object v3, v1, Lfqg;->a:[B

    invoke-static {v3}, Lfdw;->a([B)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v2, Lhkd;->f:Lhud;

    iget-object v4, v1, Lfqg;->a:[B

    iput-object v4, v3, Lhud;->c:[B

    :cond_2
    iget-object v3, v2, Lhkd;->f:Lhud;

    iget-object v4, v1, Lfqg;->b:Ljava/lang/String;

    iput-object v4, v3, Lhud;->b:Ljava/lang/String;

    iget-object v3, v2, Lhkd;->f:Lhud;

    const/4 v4, 0x1

    new-array v4, v4, [Liaj;

    const/4 v5, 0x0

    invoke-static {p1}, Lfdw;->a(Lfqi;)Liaj;

    move-result-object v6

    aput-object v6, v4, v5

    iput-object v4, v3, Lhud;->a:[Liaj;

    invoke-virtual {v0, v2}, Lfdw;->a(Lhkd;)V

    iget-object v0, v0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ATTACH_CHILD:"

    iget-object v2, v1, Lfqg;->a:[B

    iget-object v1, v1, Lfqg;->b:Ljava/lang/String;

    invoke-static {v0, v2, p1, v1}, Lfdw;->a(Ljava/lang/String;[BLfqi;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lgqo;Lgqq;)V
    .locals 0

    .prologue
    .line 453
    return-void
.end method

.method public final a(Lgqo;Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->r:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 458
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->r:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 448
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 438
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->f:Z

    .line 439
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->g()V

    .line 440
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 429
    return-void
.end method

.method public c()Lgrx;
    .locals 3

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->n:Lgrx;

    if-nez v0, :cond_1

    .line 583
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 584
    const-string v0, "thumbnail_producer"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 585
    instance-of v2, v0, Lgrx;

    if-nez v2, :cond_0

    .line 586
    new-instance v0, Lgrx;

    invoke-direct {v0}, Lgrx;-><init>()V

    .line 587
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "thumbnail_producer"

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 589
    :cond_0
    check-cast v0, Lgrx;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->n:Lgrx;

    .line 590
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->n:Lgrx;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a:Lgqh;

    iget-object v2, v0, Lgrx;->c:Lgqh;

    if-eq v2, v1, :cond_1

    invoke-virtual {v0}, Lgrx;->f()V

    iput-object v1, v0, Lgrx;->c:Lgqh;

    invoke-virtual {v0}, Lgrx;->e()V

    .line 593
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->n:Lgrx;

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 672
    sget-object v0, Lfqi;->i:Lfqi;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Lfqi;)V

    .line 674
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    iget-object v0, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 675
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 242
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 244
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->d:F

    .line 246
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    if-ne p1, v0, :cond_3

    .line 465
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    if-eqz v0, :cond_2

    .line 466
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v1

    .line 467
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->getTop()I

    move-result v0

    if-eq v1, v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollX()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    invoke-virtual {v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->getTop()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 471
    const/4 v0, 0x0

    .line 472
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_0

    .line 478
    const/16 v0, 0xfa

    .line 480
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c(I)V

    .line 483
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->getTop()I

    move-result v0

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 485
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->b()V

    .line 493
    :cond_2
    :goto_0
    return-void

    .line 488
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->i:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 491
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/libraries/youtube/upload/edit/audioswap/ui/TrackSelectionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 251
    const v0, 0x7f04011f

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 252
    invoke-virtual {v1, v2}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 253
    const/high16 v0, -0x1000000

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 255
    const v0, 0x7f08030d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    .line 256
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    iput-object p0, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->g:Landroid/view/TextureView$SurfaceTextureListener;

    .line 257
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 260
    const v0, 0x7f08030e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->i:Landroid/widget/ImageButton;

    .line 261
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    const v0, 0x7f08030f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    .line 264
    const v0, 0x7f080310

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    .line 266
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 351
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 353
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->k:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgqo;Lgrw;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->a(Lgqo;)V

    .line 355
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    invoke-virtual {v0, p0}, Lgqo;->b(Lgqr;)V

    .line 358
    :cond_0
    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 501
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->e()V

    .line 503
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 337
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 339
    const-string v0, "video_uri"

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 340
    const-string v0, "editable_video"

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 342
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v0}, Leau;->b()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 343
    const-string v0, "playback_position"

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v1}, Leau;->h()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    iget-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->q:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 345
    const-string v0, "playback_position"

    iget-wide v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->q:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public onScrollChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 507
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    if-nez v0, :cond_1

    .line 524
    :cond_0
    :goto_0
    return-void

    .line 510
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->l:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    .line 511
    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->m:I

    if-eq v0, v1, :cond_2

    .line 512
    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->m:I

    .line 513
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->e()V

    .line 514
    if-nez v0, :cond_2

    .line 516
    invoke-direct {p0, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c(I)V

    .line 520
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    invoke-virtual {v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v0, v2}, Leau;->a(Z)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 291
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 293
    const/4 v0, 0x4

    invoke-static {v0, v1, v1}, La;->a(III)Leau;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    .line 294
    iput-boolean v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->g:Z

    .line 295
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v0, p0}, Leau;->a(Leaw;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->a(Leau;)V

    .line 298
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->g()V

    .line 299
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->f()V

    .line 301
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 302
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 303
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0001

    .line 305
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 306
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 303
    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    .line 307
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->h:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    float-to-int v0, v0

    iget v2, v1, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->e:I

    if-eq v2, v0, :cond_0

    iput v0, v1, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->e:I

    invoke-virtual {v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->requestLayout()V

    .line 312
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 316
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 318
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->j:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->a(Leau;)V

    .line 320
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    if-eqz v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v0}, Leau;->b()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v0}, Leau;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->q:J

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    invoke-virtual {v0}, Leau;->f()V

    .line 326
    iput-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->o:Leau;

    .line 327
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->g:Z

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a:Lgqh;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->e:Lgso;

    invoke-virtual {v0, v1}, Lgqh;->c(Lgqi;)V

    .line 332
    iput-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->p:Lebp;

    .line 333
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Landroid/graphics/SurfaceTexture;Z)V

    .line 365
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Landroid/graphics/SurfaceTexture;Z)V

    .line 373
    const/4 v0, 0x0

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 271
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->u:Z

    .line 274
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->s:Lfdw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->t:Lfqg;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->s:Lfdw;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->t:Lfqg;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Lfdw;Lfqg;)V

    .line 278
    :cond_0
    if-eqz p2, :cond_1

    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->u:Z

    .line 281
    const-string v0, "video_uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 282
    const-string v1, "editable_video"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lgqo;

    .line 283
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Landroid/net/Uri;Lgqo;)V

    .line 284
    const-string v0, "playback_position"

    const-wide/16 v2, -0x1

    .line 285
    invoke-virtual {p2, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->q:J

    .line 287
    :cond_1
    return-void
.end method
