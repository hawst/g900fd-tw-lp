.class public Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$V18CompatibilityLayer;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createWidevineDrmSessionManager18$1c66caf5(Landroid/net/Uri;Lgfl;Landroid/os/Looper;Landroid/os/Handler;Ledz;ILjava/lang/String;)Ledv;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 186
    :try_start_0
    new-instance v0, Lgfr;

    .line 187
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, p1

    move-object v3, p6

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lgfr;-><init>(Ljava/lang/String;Lgfl;Ljava/lang/String;Landroid/os/Handler;Lgft;)V

    .line 192
    new-instance v1, Lgfu;

    if-ne p5, v6, :cond_0

    move v2, v6

    :goto_0
    move-object v3, p2

    move-object v4, v0

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lgfu;-><init>(ZLandroid/os/Looper;Ledw;Landroid/os/Handler;Ledz;)V
    :try_end_0
    .catch Landroid/media/UnsupportedSchemeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :goto_1
    return-object v1

    .line 192
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    const-string v1, "CDM initialization failed."

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 200
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getWidevineSecurityLevel()I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 146
    :try_start_0
    new-instance v2, Landroid/media/MediaDrm;

    sget-object v3, Lgfu;->k:Ljava/util/UUID;

    invoke-direct {v2, v3}, Landroid/media/MediaDrm;-><init>(Ljava/util/UUID;)V

    .line 147
    const-string v3, "securityLevel"

    invoke-virtual {v2, v3}, Landroid/media/MediaDrm;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 148
    const-string v3, "L1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v0

    .line 150
    :cond_1
    const-string v3, "L2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 152
    const-string v0, "L3"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    move v0, v1

    .line 155
    goto :goto_0

    .line 158
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method
