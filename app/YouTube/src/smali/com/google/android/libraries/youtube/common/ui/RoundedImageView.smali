.class public Lcom/google/android/libraries/youtube/common/ui/RoundedImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# instance fields
.field private final a:Leyy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Leua;->b:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 28
    invoke-virtual {v5, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    .line 30
    new-instance v0, Leyy;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 32
    invoke-virtual {v5, v2, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x2

    .line 33
    invoke-virtual {v5, v3, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x3

    .line 34
    invoke-virtual {v5, v4, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    int-to-float v4, v4

    const/4 v7, 0x4

    .line 35
    invoke-virtual {v5, v7, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    int-to-float v5, v5

    invoke-direct/range {v0 .. v5}, Leyy;-><init>(Landroid/graphics/Bitmap;FFFF)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/common/ui/RoundedImageView;->a:Leyy;

    .line 36
    return-void
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 41
    iget-object v0, p0, Lcom/google/android/libraries/youtube/common/ui/RoundedImageView;->a:Leyy;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v2, v2, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, Leyy;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 42
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/youtube/common/ui/RoundedImageView;->a:Leyy;

    invoke-virtual {v0, p1}, Leyy;->a(Landroid/graphics/Bitmap;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/youtube/common/ui/RoundedImageView;->a:Leyy;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/common/ui/RoundedImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 48
    return-void
.end method
