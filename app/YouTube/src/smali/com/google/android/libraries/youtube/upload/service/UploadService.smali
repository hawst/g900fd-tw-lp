.class public Lcom/google/android/libraries/youtube/upload/service/UploadService;
.super Lguf;
.source "SourceFile"


# instance fields
.field private b:Lgvg;

.field private c:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private e:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 50
    const-string v0, "youtube_upload_service"

    new-instance v1, Lgve;

    invoke-direct {v1}, Lgve;-><init>()V

    invoke-direct {p0, v0, v1}, Lguf;-><init>(Ljava/lang/String;Lgty;)V

    .line 37
    new-instance v0, Lgvg;

    invoke-direct {v0, p0}, Lgvg;-><init>(Lcom/google/android/libraries/youtube/upload/service/UploadService;)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->b:Lgvg;

    .line 39
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->c:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 41
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 44
    new-instance v0, Landroid/os/HandlerThread;

    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "_POLLING"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->e:Landroid/os/HandlerThread;

    .line 51
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->b:Lgvg;

    return-object v0
.end method

.method public onCreate()V
    .locals 12

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->getApplication()Landroid/app/Application;

    move-result-object v0

    instance-of v0, v0, Lgvc;

    invoke-static {v0}, Lb;->c(Z)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lgvc;

    .line 58
    invoke-interface {v0}, Lgvc;->l()Lgvb;

    move-result-object v0

    .line 59
    invoke-interface {v0}, Lgvb;->g()Lfhw;

    move-result-object v2

    .line 60
    invoke-interface {v0}, Lgvb;->r()Lfgi;

    move-result-object v6

    .line 61
    invoke-interface {v0}, Lgvb;->w()Lfgj;

    move-result-object v7

    .line 62
    invoke-interface {v0}, Lgvb;->x()Lfel;

    move-result-object v8

    .line 63
    invoke-interface {v0}, Lgvb;->aV()Lgix;

    move-result-object v4

    .line 64
    invoke-interface {v0}, Lgvb;->aA()Lfbu;

    move-result-object v5

    .line 66
    new-instance v3, Lgvh;

    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v3, v0}, Lgvh;-><init>(Landroid/content/ContentResolver;)V

    .line 68
    new-instance v9, Lgum;

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->c:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v9, v0}, Lgum;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 69
    new-instance v10, Lgum;

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v10, v0}, Lgum;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 71
    new-instance v11, Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v11, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 73
    new-instance v0, Lgur;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lgur;-><init>(Lguf;Lfhw;Lgvh;Lgix;Lfbu;)V

    invoke-virtual {p0, v0, v10}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Lgtn;Lgum;)V

    .line 77
    new-instance v0, Lgts;

    invoke-direct {v0, v6}, Lgts;-><init>(Lfgi;)V

    invoke-virtual {p0, v0, v9}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Lgtn;Lgum;)V

    .line 78
    new-instance v0, Lguk;

    invoke-direct {v0, v8}, Lguk;-><init>(Lfel;)V

    invoke-virtual {p0, v0, v9}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Lgtn;Lgum;)V

    .line 79
    new-instance v0, Lgtq;

    invoke-direct {v0, v6}, Lgtq;-><init>(Lfgi;)V

    invoke-virtual {p0, v0, v9}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Lgtn;Lgum;)V

    .line 80
    new-instance v0, Lgtv;

    invoke-direct {v0}, Lgtv;-><init>()V

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lguf;->a:Ljava/util/Set;

    new-instance v2, Lgtx;

    invoke-direct {v2, p0, v0}, Lgtx;-><init>(Lguf;Lgud;)V

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 81
    new-instance v0, Lgto;

    invoke-direct {v0, v7}, Lgto;-><init>(Lfgj;)V

    invoke-virtual {p0, v0, v9}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Lgtn;Lgum;)V

    .line 82
    new-instance v0, Lguw;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v6, v11, v1}, Lguw;-><init>(Lcom/google/android/libraries/youtube/upload/service/UploadService;Lfgi;Landroid/os/Handler;Lu;)V

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lguf;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-super {p0}, Lguf;->onCreate()V

    .line 85
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lguf;->onDestroy()V

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->c:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->shutdown()V

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->shutdown()V

    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/service/UploadService;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 93
    return-void
.end method
