.class public Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field final a:Landroid/view/TextureView;

.field final b:Landroid/view/View;

.field final c:Landroid/widget/ProgressBar;

.field d:F

.field e:I

.field f:I

.field g:Landroid/view/TextureView$SurfaceTextureListener;

.field private final h:Landroid/widget/ImageView;

.field private i:Lgrs;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const v0, 0x3fe38e39

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->d:F

    .line 42
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->e:I

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->f:I

    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04012a

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 57
    const v0, 0x7f080328

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->a:Landroid/view/TextureView;

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->a:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 59
    const v0, 0x7f080329

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->h:Landroid/widget/ImageView;

    .line 60
    const v0, 0x7f08032a

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->b:Landroid/view/View;

    .line 61
    const v0, 0x7f08032c

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->c:Landroid/widget/ProgressBar;

    .line 62
    return-void
.end method


# virtual methods
.method a()V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 222
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 223
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 224
    cmpl-float v2, v0, v6

    if-eqz v2, :cond_0

    cmpl-float v2, v1, v6

    if-eqz v2, :cond_0

    .line 225
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 226
    iget v3, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->f:I

    int-to-float v3, v3

    div-float v4, v0, v7

    div-float v5, v1, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 228
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v6, v6, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 229
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 231
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float v4, v0, v4

    .line 232
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float v3, v1, v3

    div-float/2addr v0, v7

    div-float/2addr v1, v7

    .line 230
    invoke-virtual {v2, v4, v3, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 234
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->a:Landroid/view/TextureView;

    invoke-virtual {v0, v2}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    .line 236
    :cond_0
    return-void
.end method

.method public final a(Lgrs;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->i:Lgrs;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->i:Lgrs;

    invoke-virtual {v0}, Lgrs;->c()V

    .line 94
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lgrs;->b()Lgrs;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->i:Lgrs;

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->i:Lgrs;

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->i:Lgrs;

    invoke-virtual {v1}, Lgrs;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->h:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 103
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 94
    goto :goto_0

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->h:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 169
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 170
    if-eqz p1, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->a()V

    .line 173
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/high16 v3, 0x40000000    # 2.0f

    .line 155
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 156
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 157
    int-to-float v0, v1

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->d:F

    div-float/2addr v0, v2

    add-float/2addr v0, v4

    float-to-int v0, v0

    .line 158
    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->e:I

    if-le v0, v2, :cond_0

    .line 159
    iget v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->e:I

    .line 160
    int-to-float v1, v0

    iget v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->d:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v4

    float-to-int v1, v1

    .line 163
    :cond_0
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 164
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 162
    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 165
    return-void

    .line 155
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->g:Landroid/view/TextureView$SurfaceTextureListener;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->g:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V

    .line 182
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->a()V

    .line 183
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->g:Landroid/view/TextureView$SurfaceTextureListener;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->g:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z

    move-result v0

    .line 199
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->g:Landroid/view/TextureView$SurfaceTextureListener;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->g:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V

    .line 190
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->a()V

    .line 191
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->g:Landroid/view/TextureView$SurfaceTextureListener;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->g:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V

    .line 208
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->a(Lgrs;)V

    .line 209
    return-void
.end method
