.class public Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgdc;


# instance fields
.field public final a:Ljava/util/Queue;

.field public b:Z

.field public c:Z

.field public d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "m2ts_player"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->createPlayer()Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lgde;

    invoke-direct {v0}, Lgde;-><init>()V

    throw v0

    .line 52
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->c:Z

    .line 53
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->a:Ljava/util/Queue;

    .line 54
    return-void
.end method

.method public static a(F)V
    .locals 0

    .prologue
    .line 136
    invoke-static {p0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->setVolumeLevel(F)V

    .line 137
    return-void
.end method

.method public static c()J
    .locals 4

    .prologue
    .line 140
    invoke-static {}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->getPositionMillis()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private static native createPlayer()Z
.end method

.method public static native enqueueBuffer(Ljava/nio/ByteBuffer;I)V
.end method

.method public static native enqueueEos()V
.end method

.method private static native getPositionMillis()I
.end method

.method public static native getUnusedBuffer()Ljava/nio/ByteBuffer;
.end method

.method public static native setPlaybackState(Z)V
.end method

.method private static native setVolumeLevel(F)V
.end method

.method private static native shutdown()V
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 114
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 115
    invoke-static {v1}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->setPlaybackState(Z)V

    .line 116
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([B)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->c:Z

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->c:Z

    .line 130
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->setPlaybackState(Z)V

    .line 131
    invoke-static {}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->shutdown()V

    .line 133
    :cond_0
    return-void
.end method
