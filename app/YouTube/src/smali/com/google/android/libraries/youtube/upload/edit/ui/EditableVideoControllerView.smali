.class public Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Leaw;
.implements Lgqr;


# instance fields
.field private final a:Ljava/lang/Runnable;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;

.field private f:I

.field private g:I

.field private h:Leau;

.field private i:Lgqo;

.field private j:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    new-instance v0, Lgse;

    invoke-direct {v0, p0}, Lgse;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)V

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->a:Ljava/lang/Runnable;

    .line 64
    iput v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->f:I

    .line 65
    iput v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->g:I

    .line 70
    const-class v0, Lgqq;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->j:Ljava/util/Set;

    .line 80
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04006a

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 82
    const v0, 0x7f0801d4

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->b:Landroid/widget/ImageView;

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    const v0, 0x7f0801d5

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->c:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0801d6

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->d:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0801d7

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;

    iput-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e:Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e:Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 88
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->g()V

    return-void
.end method

.method private b(I)Ljava/lang/String;
    .locals 6

    .prologue
    .line 381
    div-int/lit8 v0, p1, 0x3c

    .line 382
    rem-int/lit8 v1, p1, 0x3c

    .line 383
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09008c

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e()V

    return-void
.end method

.method private c()J
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->i:Lgqo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->i:Lgqo;

    invoke-virtual {v0}, Lgqo;->b()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->f()V

    return-void
.end method

.method private d()J
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->i:Lgqo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->i:Lgqo;

    invoke-virtual {v0}, Lgqo;->c()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v0}, Leau;->g()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)Leau;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)J
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 339
    const v0, 0x7f02017b

    .line 340
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v1}, Leau;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341
    const v0, 0x7f02017a

    .line 343
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 344
    return-void
.end method

.method public static synthetic f(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->a:Ljava/lang/Runnable;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-nez v0, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->d()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 351
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->j:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 352
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e:Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;

    long-to-int v3, v0

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;->setMax(I)V

    .line 355
    :cond_2
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 356
    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->g:I

    if-eq v0, v1, :cond_0

    .line 357
    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->g:I

    .line 358
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->d:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->g:I

    invoke-direct {p0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-nez v0, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v0}, Leau;->h()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 367
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->j:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 368
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e:Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;

    long-to-int v3, v0

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;->setProgress(I)V

    .line 373
    :goto_1
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 374
    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->f:I

    if-eq v0, v1, :cond_0

    .line 375
    iput v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->f:I

    .line 376
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->c:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->f:I

    invoke-direct {p0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 370
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lgsf;

    invoke-direct {v0, p0}, Lgsf;-><init>(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->post(Ljava/lang/Runnable;)Z

    .line 169
    return-void
.end method

.method public final a(Leat;)V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method public final a(Leau;)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v0, p0}, Leau;->b(Leaw;)V

    .line 110
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    .line 112
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v0, p0}, Leau;->a(Leaw;)V

    .line 116
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e()V

    .line 117
    return-void
.end method

.method public final a(Lgqo;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->i:Lgqo;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->i:Lgqo;

    invoke-virtual {v0, p0}, Lgqo;->b(Lgqr;)V

    .line 129
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->i:Lgqo;

    .line 131
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->i:Lgqo;

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->i:Lgqo;

    invoke-virtual {v0, p0}, Lgqo;->a(Lgqr;)V

    .line 135
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->f()V

    .line 136
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->g()V

    .line 137
    return-void
.end method

.method public final a(Lgqo;Lgqq;)V
    .locals 4

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-nez v0, :cond_0

    .line 212
    :goto_0
    return-void

    .line 201
    :cond_0
    sget-object v0, Lgqq;->a:Lgqq;

    if-ne p2, v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {p1}, Lgqo;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Leau;->a(J)V

    .line 211
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->f()V

    goto :goto_0

    .line 203
    :cond_2
    sget-object v0, Lgqq;->b:Lgqq;

    if-ne p2, v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->j:Ljava/util/Set;

    sget-object v1, Lgqq;->a:Lgqq;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {p1}, Lgqo;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Leau;->a(J)V

    goto :goto_1
.end method

.method public final a(Lgqo;Ljava/util/Set;)V
    .locals 4

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->j:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 217
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e:Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 219
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-nez v0, :cond_0

    .line 234
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v0}, Leau;->h()J

    move-result-wide v0

    .line 223
    sget-object v2, Lgqq;->a:Lgqq;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 225
    invoke-virtual {p1}, Lgqo;->b()J

    move-result-wide v0

    .line 232
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v2, v0, v1}, Leau;->a(J)V

    .line 233
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leau;->a(Z)V

    goto :goto_0

    .line 226
    :cond_2
    sget-object v2, Lgqq;->b:Lgqq;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    invoke-virtual {p1}, Lgqo;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    sub-long/2addr v0, v2

    invoke-virtual {p1}, Lgqo;->b()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_1
.end method

.method public final a(Ljava/util/Set;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e:Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e:Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/SeekBarCompat;->setProgress(I)V

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v0, v1}, Leau;->a(Z)V

    .line 194
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-nez v0, :cond_0

    .line 155
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v0}, Leau;->c()Z

    move-result v0

    .line 149
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-virtual {v1}, Leau;->h()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->d()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 151
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->c()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Leau;->a(J)V

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Leau;->a(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->b:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->b()V

    .line 263
    :cond_0
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6

    .prologue
    .line 240
    if-eqz p3, :cond_0

    .line 241
    int-to-long v0, p2

    .line 242
    iget-object v2, p0, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->h:Leau;

    invoke-direct {p0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->c()J

    move-result-wide v4

    add-long/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Leau;->a(J)V

    .line 244
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 254
    return-void
.end method
