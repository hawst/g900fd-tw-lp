.class public final Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;
.super Lafh;
.source "SourceFile"

# interfaces
.implements Ladu;
.implements Laff;


# instance fields
.field private A:Z

.field private B:Z

.field private j:Lanm;

.field private final k:Lakc;

.field private final l:Lakm;

.field private final m:Lakx;

.field private final n:Lahh;

.field private final o:Lagc;

.field private final p:Lalb;

.field private final q:Lagl;

.field private final r:Lahp;

.field private final s:Lajx;

.field private final t:Lakg;

.field private final u:Lalj;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:I

.field private z:I


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lanj;Z)V
    .locals 1

    .prologue
    .line 111
    invoke-static {p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Landroid/app/Activity;)Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/content/Context;Landroid/app/Activity;Lanj;Z)V

    .line 113
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ladq;Lanj;Z)V
    .locals 17

    .prologue
    .line 175
    new-instance v3, Ldef;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Ldef;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v3}, Lafh;-><init>(Landroid/content/Context;Ladq;Ldef;)V

    .line 176
    const-string v3, "apiPlayerFactoryService cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    if-nez p4, :cond_0

    .line 180
    new-instance v3, Lafc;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lafc;-><init>(Landroid/content/Context;Ladu;)V

    .line 181
    new-instance v4, Lakm;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    move-object/from16 v0, p1

    invoke-direct {v4, v3, v0, v5}, Lakm;-><init>(Lakw;Landroid/content/Context;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lakm;

    .line 182
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lakx;

    .line 190
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->b:Ldef;

    invoke-interface {v3}, Ladt;->a()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Ldef;->a(Landroid/view/View;)V

    .line 192
    new-instance v4, Lakc;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->b:Ldef;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v4, v5, v6}, Lakc;-><init>(Lcwz;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lakc;

    .line 193
    new-instance v4, Lahh;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v4, v3, v5}, Lahh;-><init>(Ladt;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->n:Lahh;

    .line 194
    new-instance v3, Lagc;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->d:Ldbz;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lagc;-><init>(Ldbm;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->o:Lagc;

    .line 195
    new-instance v3, Lalb;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->e:Ldcr;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lalb;-><init>(Ldff;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->p:Lalb;

    .line 196
    new-instance v3, Lagl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->f:Ldge;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lagl;-><init>(Ldfu;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q:Lagl;

    .line 197
    new-instance v3, Lahp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->c:Ladr;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lahp;-><init>(Ldbr;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->r:Lahp;

    .line 198
    new-instance v3, Lajx;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->g:Ldcl;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lajx;-><init>(Lddb;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->s:Lajx;

    .line 199
    new-instance v3, Lakg;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->h:Ldcq;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lakg;-><init>(Ldeo;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->t:Lakg;

    .line 200
    new-instance v3, Lalj;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Ldcw;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lalj;-><init>(Ldfm;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->u:Lalj;

    .line 202
    new-instance v4, Lgvn;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lgvn;-><init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lakc;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lakm;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lakx;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->n:Lahh;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->o:Lagc;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->p:Lalb;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q:Lagl;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->r:Lahp;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->s:Lajx;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->t:Lakg;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->u:Lalj;

    move-object/from16 v3, p3

    move/from16 v16, p4

    invoke-interface/range {v3 .. v16}, Lanj;->a(Laiw;Lajf;Lajl;Lajo;Lait;Lain;Lajr;Laiq;Laiz;Lajc;Laji;Laju;Z)Lanm;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    .line 216
    return-void

    .line 185
    :cond_0
    new-instance v3, Lafe;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lafe;-><init>(Landroid/content/Context;Laff;)V

    .line 186
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lakm;

    .line 187
    new-instance v4, Lakx;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    move-object/from16 v0, p1

    invoke-direct {v4, v3, v0, v5}, Lakx;-><init>(Lala;Landroid/content/Context;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lakx;

    goto/16 :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lanj;Z)V
    .locals 4

    .prologue
    .line 158
    new-instance v0, Lahn;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-direct {v0, p2, v1, v2, v3}, Lahn;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/ClassLoader;Landroid/content/res/Resources$Theme;)V

    new-instance v1, Ladq;

    invoke-direct {v1, p2}, Ladq;-><init>(Landroid/app/Activity;)V

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/content/Context;Ladq;Lanj;Z)V

    .line 160
    return-void
.end method

.method public constructor <init>(Landroid/os/IBinder;Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/os/IBinder;Landroid/os/IBinder;Z)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Z)V
    .locals 3

    .prologue
    .line 145
    invoke-static {p1}, Lgxm;->a(Landroid/os/IBinder;)Lgxl;

    move-result-object v0

    invoke-static {v0}, Lgxo;->a(Lgxl;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 146
    invoke-static {p2}, Lgxm;->a(Landroid/os/IBinder;)Lgxl;

    move-result-object v1

    invoke-static {v1}, Lgxo;->a(Lgxl;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 147
    invoke-static {p3}, Lank;->a(Landroid/os/IBinder;)Lanj;

    move-result-object v2

    .line 145
    invoke-direct {p0, v0, v1, v2, p4}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/content/Context;Landroid/app/Activity;Lanj;Z)V

    .line 148
    return-void
.end method

.method public constructor <init>(Landroid/os/IBinder;Landroid/os/IBinder;Z)V
    .locals 2

    .prologue
    .line 101
    invoke-static {p1}, Lgxm;->a(Landroid/os/IBinder;)Lgxl;

    move-result-object v0

    invoke-static {v0}, Lgxo;->a(Lgxl;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 102
    invoke-static {p2}, Lank;->a(Landroid/os/IBinder;)Lanj;

    move-result-object v1

    .line 101
    invoke-direct {p0, v0, v1, p3}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/app/Activity;Lanj;Z)V

    .line 103
    return-void
.end method

.method public static synthetic A(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->O()V

    return-void
.end method

.method public static synthetic B(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic C(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->P()V

    return-void
.end method

.method public static synthetic D(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;I)I
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    return p1
.end method

.method private static a(Landroid/app/Activity;)Landroid/app/Activity;
    .locals 6

    .prologue
    .line 123
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .line 125
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 128
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Landroid/app/Activity;

    if-ne v4, v5, :cond_0

    .line 129
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 131
    :try_start_0
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not get the activity from the ActivityWrapper"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 125
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to extract the wrapped activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->S()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Lgwh;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Lgwh;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->b(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->x:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;I)I
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->z:I

    return p1
.end method

.method public static synthetic b(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->T()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:Z

    return p1
.end method

.method public static synthetic c(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->U()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;I)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->g(I)V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->v:Z

    return p1
.end method

.method public static synthetic d(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l(Z)V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->V()V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k(Z)V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m(Z)V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->W()V

    return-void
.end method

.method public static synthetic h(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->N()V

    return-void
.end method

.method public static synthetic i(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->X()V

    return-void
.end method

.method public static synthetic k(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->Y()V

    return-void
.end method

.method public static synthetic l(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->Q()V

    return-void
.end method

.method public static synthetic m(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic n(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->Z()V

    return-void
.end method

.method public static synthetic o(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->R()V

    return-void
.end method

.method public static synthetic p(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic q(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->aa()V

    return-void
.end method

.method public static synthetic r(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->R()V

    return-void
.end method

.method public static synthetic s(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic t(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->ab()V

    return-void
.end method

.method public static synthetic u(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic v(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic w(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic x(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic y(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->M()V

    return-void
.end method

.method public static synthetic z(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 345
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 346
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    return-void

    .line 347
    :catch_0
    move-exception v0

    .line 349
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->v:Z

    return v0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 382
    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:Z

    return v0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 387
    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->x:Z

    return v0
.end method

.method public final E()V
    .locals 2

    .prologue
    .line 393
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    return-void

    .line 394
    :catch_0
    move-exception v0

    .line 396
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final F()V
    .locals 2

    .prologue
    .line 403
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 407
    return-void

    .line 404
    :catch_0
    move-exception v0

    .line 406
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final G()I
    .locals 1

    .prologue
    .line 412
    iget v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    return v0
.end method

.method public final H()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->z:I

    return v0
.end method

.method public final I()V
    .locals 2

    .prologue
    .line 509
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->i()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 513
    return-void

    .line 510
    :catch_0
    move-exception v0

    .line 512
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final J()V
    .locals 2

    .prologue
    .line 477
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    return-void

    .line 478
    :catch_0
    move-exception v0

    .line 480
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final K()Z
    .locals 2

    .prologue
    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->e()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 227
    :catch_0
    move-exception v0

    .line 229
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final L()V
    .locals 2

    .prologue
    .line 307
    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->A:Z

    if-nez v0, :cond_0

    .line 308
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 317
    :goto_0
    return-void

    .line 312
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 313
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->k()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 314
    :catch_0
    move-exception v0

    .line 316
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 758
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->A:Z

    .line 759
    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    if-eqz v0, :cond_0

    .line 760
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->L()V

    .line 762
    :cond_0
    return-void
.end method

.method protected final a([B)Z
    .locals 2

    .prologue
    .line 539
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1}, Lanm;->a([B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 540
    :catch_0
    move-exception v0

    .line 542
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 766
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->A:Z

    .line 767
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 771
    const-string v0, "Cannot attach a YouTubePlayerView backed by a TextureView to a Window that is not hardware accelerated"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, La;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 774
    sget-object v0, Lgwh;->j:Lgwh;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Lgwh;)V

    .line 775
    return-void
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 236
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 237
    iput p2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    .line 238
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1, p2}, Lanm;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    return-void

    .line 239
    :catch_0
    move-exception v0

    .line 241
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 260
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 261
    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    .line 262
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1, p2, p3}, Lanm;->a(Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    return-void

    .line 263
    :catch_0
    move-exception v0

    .line 265
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/util/List;II)V
    .locals 2

    .prologue
    .line 284
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 285
    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    .line 286
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1, p2, p3}, Lanm;->a(Ljava/util/List;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 289
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 487
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1, p2}, Lanm;->a(ILandroid/view/KeyEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 492
    const/4 v0, 0x0

    return v0

    .line 488
    :catch_0
    move-exception v0

    .line 490
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 248
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 249
    iput p2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    .line 250
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1, p2}, Lanm;->b(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    return-void

    .line 251
    :catch_0
    move-exception v0

    .line 253
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 272
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 273
    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    .line 274
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1, p2, p3}, Lanm;->b(Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 277
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/util/List;II)V
    .locals 2

    .prologue
    .line 296
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 297
    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    .line 298
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1, p2, p3}, Lanm;->b(Ljava/util/List;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    return-void

    .line 299
    :catch_0
    move-exception v0

    .line 301
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 220
    invoke-super {p0}, Lafh;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 498
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1, p2}, Lanm;->b(ILandroid/view/KeyEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 503
    const/4 v0, 0x0

    return v0

    .line 499
    :catch_0
    move-exception v0

    .line 501
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e(I)V
    .locals 2

    .prologue
    .line 423
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 424
    iput p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    .line 425
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1}, Lanm;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    return-void

    .line 426
    :catch_0
    move-exception v0

    .line 428
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f(I)V
    .locals 2

    .prologue
    .line 435
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 436
    iget v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    .line 437
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1}, Lanm;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 441
    return-void

    .line 438
    :catch_0
    move-exception v0

    .line 440
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f(Z)V
    .locals 2

    .prologue
    .line 447
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1}, Lanm;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 451
    return-void

    .line 448
    :catch_0
    move-exception v0

    .line 450
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final g(Z)V
    .locals 2

    .prologue
    .line 457
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1}, Lanm;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    return-void

    .line 458
    :catch_0
    move-exception v0

    .line 460
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final h(Z)V
    .locals 2

    .prologue
    .line 467
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1}, Lanm;->d(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    return-void

    .line 468
    :catch_0
    move-exception v0

    .line 470
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final i(Z)V
    .locals 2

    .prologue
    .line 519
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1}, Lanm;->e(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    return-void

    .line 520
    :catch_0
    move-exception v0

    .line 522
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final j(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 356
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0, p1}, Lanm;->a(Z)V

    .line 357
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lakm;

    if-eqz v0, :cond_1

    .line 358
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lakm;

    iget-object v1, v0, Lakm;->a:Lakw;

    invoke-interface {v1}, Lakw;->d()V

    iget-object v1, v0, Lakm;->b:Lakv;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lakm;->b:Lakv;

    const/4 v2, 0x0

    iput-object v2, v1, Lakv;->a:Lany;

    const/4 v2, 0x0

    iput-object v2, v1, Lakv;->b:Lakw;

    const/4 v1, 0x0

    iput-object v1, v0, Lakm;->b:Lakv;

    :cond_0
    const/4 v1, 0x0

    iput-object v1, v0, Lakm;->c:Landroid/view/SurfaceHolder;

    .line 360
    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lakx;

    if-eqz v0, :cond_2

    .line 361
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lakx;

    iget-object v1, v0, Lakx;->a:Lala;

    invoke-interface {v1}, Lala;->d()V

    iget-object v1, v0, Lakx;->b:Lakz;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lakx;->b:Lakz;

    const/4 v2, 0x0

    iput-object v2, v1, Lakz;->a:Laob;

    const/4 v2, 0x0

    iput-object v2, v1, Lakz;->b:Lala;

    const/4 v1, 0x0

    iput-object v1, v0, Lakx;->b:Lakz;

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lakc;

    iget-object v1, v0, Lakc;->a:Lakf;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lakc;->a:Lakf;

    const/4 v2, 0x0

    iput-object v2, v1, Lakf;->a:Lanv;

    const/4 v1, 0x0

    iput-object v1, v0, Lakc;->a:Lakf;

    .line 364
    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->o:Lagc;

    iget-object v1, v0, Lagc;->a:Lagk;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lagc;->a:Lagk;

    const/4 v2, 0x0

    iput-object v2, v1, Lagk;->a:Land;

    const/4 v1, 0x0

    iput-object v1, v0, Lagc;->a:Lagk;

    .line 365
    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q:Lagl;

    iget-object v1, v0, Lagl;->a:Lahg;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lagl;->a:Lahg;

    const/4 v2, 0x0

    iput-object v2, v1, Lahg;->a:Lang;

    const/4 v1, 0x0

    iput-object v1, v0, Lagl;->a:Lahg;

    .line 366
    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->r:Lahp;

    iget-object v1, v0, Lahp;->a:Laim;

    if-eqz v1, :cond_6

    iget-object v1, v0, Lahp;->a:Laim;

    const/4 v2, 0x0

    iput-object v2, v1, Laim;->a:Lanp;

    const/4 v1, 0x0

    iput-object v1, v0, Lahp;->a:Laim;

    .line 367
    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->s:Lajx;

    iget-object v1, v0, Lajx;->a:Lakb;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lajx;->a:Lakb;

    const/4 v2, 0x0

    iput-object v2, v1, Lakb;->a:Lans;

    const/4 v1, 0x0

    iput-object v1, v0, Lajx;->a:Lakb;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    :cond_7
    :goto_0
    iput-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    .line 373
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final x()[B
    .locals 2

    .prologue
    .line 529
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->j()[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 530
    :catch_0
    move-exception v0

    .line 532
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final y()V
    .locals 2

    .prologue
    .line 323
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 324
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    return-void

    .line 325
    :catch_0
    move-exception v0

    .line 327
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 334
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    .line 335
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lanm;

    invoke-interface {v0}, Lanm;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    return-void

    .line 336
    :catch_0
    move-exception v0

    .line 338
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
