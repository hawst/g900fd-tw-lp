.class public final Lcom/google/android/youtube/api/StandalonePlayerActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lacu;


# static fields
.field private static a:Lcom/google/android/youtube/api/StandalonePlayerActivity;


# instance fields
.field private b:Lgom;

.field private c:Z

.field private d:Z

.field private e:Laco;

.field private f:Lalo;

.field private g:Lado;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 185
    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    :goto_0
    return-void

    .line 189
    :cond_0
    new-instance v0, Lalo;

    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->e:Laco;

    invoke-direct {v0, p0, v1, v7}, Lalo;-><init>(Landroid/app/Activity;Laco;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    invoke-virtual {v0, v7}, Lalo;->b(Z)V

    .line 192
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->b:Lgom;

    iget-object v0, v0, Lgom;->b:Leaf;

    iget-boolean v0, v0, Leaf;->b:Z

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    new-instance v1, Lgvm;

    invoke-direct {v1, p0}, Lgvm;-><init>(Lcom/google/android/youtube/api/StandalonePlayerActivity;)V

    invoke-virtual {v0, v1}, Lalo;->a(Lgwz;)V

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    invoke-virtual {v0}, Lalo;->j()Lgxl;

    move-result-object v0

    invoke-static {v0}, Lgxo;->a(Lgxl;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 206
    new-instance v0, Lado;

    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    .line 209
    iget-object v3, v1, Lalo;->j:Lacw;

    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->b:Lgom;

    .line 210
    iget-object v4, v1, Lgom;->a:Lgoh;

    iget-boolean v5, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->c:Z

    iget-boolean v6, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->d:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lado;-><init>(Landroid/app/Activity;Landroid/view/View;Lacw;Lgoh;ZZ)V

    iput-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Lado;

    .line 213
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Lado;

    invoke-virtual {v0}, Lado;->show()V

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lalo;->b(I)V

    .line 215
    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Lado;

    iget-boolean v0, v0, Lado;->b:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lalo;->d(Z)V

    .line 217
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 218
    const-string v1, "initialization_result"

    sget-object v2, Lgwg;->a:Lgwg;

    .line 219
    invoke-virtual {v2}, Lgwg;->name()Ljava/lang/String;

    move-result-object v2

    .line 218
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_2
    move v0, v7

    .line 215
    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/youtube/api/StandalonePlayerActivity;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a()V

    return-void
.end method


# virtual methods
.method public final a(Laco;)V
    .locals 4

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->e:Laco;

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->h:Ljava/lang/String;

    new-instance v1, Lgvl;

    invoke-direct {v1, p0}, Lgvl;-><init>(Lcom/google/android/youtube/api/StandalonePlayerActivity;)V

    iget-boolean v2, p1, Laco;->K:Z

    if-eqz v2, :cond_0

    iget-object v2, p1, Laco;->M:Lcub;

    iget-object v3, v2, Lcub;->j:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, v2, Lcub;->j:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    iget-object v1, v2, Lcub;->c:Lfcd;

    invoke-virtual {v1, v0}, Lfcd;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2, p0, v0}, Lcub;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Account name doesn\'t exist."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcub;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 180
    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 225
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 226
    const-string v1, "initialization_result"

    .line 227
    invoke-static {p1}, Laco;->a(Ljava/lang/Exception;)Lgwg;

    move-result-object v2

    invoke-virtual {v2}, Lgwg;->name()Ljava/lang/String;

    move-result-object v2

    .line 226
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->setResult(ILandroid/content/Intent;)V

    .line 230
    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->finish()V

    .line 231
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 67
    sget-object v1, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a:Lcom/google/android/youtube/api/StandalonePlayerActivity;

    if-eqz v1, :cond_0

    .line 68
    sget-object v1, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a:Lcom/google/android/youtube/api/StandalonePlayerActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->finish()V

    .line 70
    :cond_0
    sput-object p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a:Lcom/google/android/youtube/api/StandalonePlayerActivity;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 75
    const-string v1, "developer_key"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 76
    const-string v1, "app_version"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 77
    const-string v1, "client_library_version"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 78
    if-nez v7, :cond_1

    .line 80
    const-string v7, "1.0.0"

    .line 82
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    .line 83
    if-eqz v1, :cond_3

    move-object v5, v1

    .line 85
    :goto_0
    const-string v2, "com.google.android.music"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "google_account_name"

    .line 86
    invoke-virtual {v8, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->h:Ljava/lang/String;

    .line 88
    const-string v1, "watch"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 89
    const-string v1, "watch"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lgom;

    iput-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->b:Lgom;

    .line 94
    :goto_2
    const-string v1, "lightbox_mode"

    invoke-virtual {v8, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->c:Z

    .line 95
    const-string v1, "window_has_status_bar"

    invoke-virtual {v8, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->d:Z

    .line 97
    iget-boolean v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->c:Z

    iget-boolean v2, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->d:Z

    invoke-static {v1, v2}, Lado;->a(ZZ)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->setTheme(I)V

    .line 98
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 100
    iget-boolean v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->c:Z

    if-nez v1, :cond_2

    .line 101
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->setRequestedOrientation(I)V

    .line 104
    :cond_2
    const-string v1, "^(\\d+\\.){2}(\\d+)(\\w?)$"

    invoke-virtual {v7, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 106
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid client version"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a(Ljava/lang/Exception;)V

    .line 120
    :goto_3
    return-void

    .line 83
    :cond_3
    const-string v2, "app_package"

    .line 84
    invoke-virtual {v8, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 86
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 91
    :cond_5
    const-string v1, "video_id"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "playlist_id"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v1, "video_ids"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    const-string v1, "current_index"

    invoke-virtual {v8, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    const-string v1, "start_time_millis"

    invoke-virtual {v8, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    const-string v1, "autoplay"

    invoke-virtual {v8, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v10, :cond_6

    new-instance v1, Lgoh;

    invoke-direct {v1, v10, v11, v12}, Lgoh;-><init>(Ljava/util/List;II)V

    move-object v2, v1

    :goto_4
    if-nez v13, :cond_9

    move v1, v0

    :goto_5
    invoke-virtual {v2, v1}, Lgoh;->b(Z)V

    invoke-virtual {v2, v13}, Lgoh;->c(Z)V

    new-instance v1, Lgom;

    invoke-direct {v1, v2}, Lgom;-><init>(Lgoh;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->b:Lgom;

    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->b:Lgom;

    iput-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->b:Lgom;

    goto/16 :goto_2

    :cond_6
    if-eqz v9, :cond_7

    new-instance v1, Lgoh;

    const-string v2, ""

    invoke-direct {v1, v2, v9, v11, v12}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    move-object v2, v1

    goto :goto_4

    :cond_7
    if-eqz v2, :cond_8

    new-instance v1, Lgoh;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v9, -0x1

    invoke-direct {v1, v2, v9, v12}, Lgoh;-><init>(Ljava/util/List;II)V

    move-object v2, v1

    goto :goto_4

    :cond_8
    new-instance v1, Lgoh;

    new-instance v2, Leaa;

    invoke-direct {v2}, Leaa;-><init>()V

    invoke-direct {v1, v2}, Lgoh;-><init>(Leaa;)V

    move-object v2, v1

    goto :goto_4

    :cond_9
    move v1, v3

    goto :goto_5

    .line 110
    :cond_a
    new-instance v2, Landroid/os/Handler;

    .line 113
    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->h:Ljava/lang/String;

    .line 119
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    move v8, v0

    :goto_6
    move-object v1, p0

    move-object v3, p0

    .line 110
    invoke-static/range {v0 .. v8}, Laco;->a(ZLacu;Landroid/os/Handler;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_b
    move v8, v3

    .line 119
    goto :goto_6
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->isFinishing()Z

    move-result v1

    invoke-virtual {v0, v1}, Lalo;->a(Z)V

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->e:Laco;

    if-eqz v0, :cond_1

    .line 282
    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->e:Laco;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Laco;->a(Z)V

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Lado;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Lado;

    invoke-virtual {v0}, Lado;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 285
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Lado;

    invoke-virtual {v0}, Lado;->dismiss()V

    .line 287
    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 288
    return-void

    .line 282
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    invoke-virtual {v0, p1, p2}, Lalo;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    invoke-virtual {v0, p1, p2}, Lalo;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    invoke-virtual {v0}, Lalo;->g()V

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Lado;

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Lado;

    iget-object v1, v0, Lado;->c:Lacw;

    iget-object v0, v0, Lado;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    invoke-virtual {v1, v0}, Lacw;->d(Z)V

    .line 251
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 252
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 236
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 237
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    invoke-virtual {v0}, Lalo;->f()V

    .line 240
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 257
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 258
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    invoke-virtual {v0}, Lalo;->e()V

    .line 261
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 266
    sget-object v0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a:Lcom/google/android/youtube/api/StandalonePlayerActivity;

    if-ne v0, p0, :cond_0

    .line 267
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a:Lcom/google/android/youtube/api/StandalonePlayerActivity;

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    if-eqz v0, :cond_1

    .line 270
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Lalo;

    invoke-virtual {v0}, Lalo;->h()V

    .line 272
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 273
    return-void
.end method
