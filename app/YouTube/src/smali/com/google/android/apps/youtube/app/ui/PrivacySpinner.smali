.class public Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 31
    sput-object v0, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a:Ljava/util/Map;

    sget-object v1, Lgcg;->c:Lgcg;

    const v2, 0x7f090257

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a:Ljava/util/Map;

    sget-object v1, Lgcg;->a:Lgcg;

    const v2, 0x7f090256

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a:Ljava/util/Map;

    sget-object v1, Lgcg;->b:Lgcg;

    const v2, 0x7f090258

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->b:Landroid/view/LayoutInflater;

    .line 41
    new-instance v0, Lbur;

    invoke-direct {v0, p0}, Lbur;-><init>(Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 42
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->b:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public static synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Lgcg;)V
    .locals 1

    .prologue
    .line 45
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-virtual {p1}, Lgcg;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->setSelection(I)V

    .line 47
    return-void
.end method
