.class public Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbob;
.implements Lbxy;


# instance fields
.field public a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field public b:Landroid/content/SharedPreferences;

.field public c:Lbxu;

.field public d:Landroid/view/View;

.field public e:Landroid/view/View;

.field private f:Lcom/google/android/apps/youtube/app/ui/ClingView;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/graphics/Rect;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->j:Landroid/graphics/Rect;

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->k:Z

    .line 57
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->b:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 138
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 139
    new-instance v1, Lbzk;

    invoke-direct {v1, p0, p1}, Lbzk;-><init>(Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;Z)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 158
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->c:Lbxu;

    invoke-virtual {v0, p0}, Lbxu;->b(Lbxy;)V

    .line 162
    return-void
.end method

.method public static final a(Landroid/content/SharedPreferences;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 190
    const-string v1, "watch_while_tutorial_views_remaining"

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;Z)Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->k:Z

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 74
    const/16 v0, 0x1770

    return v0
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->j:Landroid/graphics/Rect;

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->requestLayout()V

    .line 168
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->e:Landroid/view/View;

    .line 80
    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 81
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 82
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 83
    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 98
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Lcom/google/android/apps/youtube/app/ui/ClingView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->d:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->e:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/ClingView;->a(Landroid/view/View;Landroid/view/View;)V

    .line 94
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 95
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 96
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->setVisibility(I)V

    .line 97
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a(Z)V

    .line 103
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->g:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 131
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a(Z)V

    .line 133
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 108
    const v0, 0x7f08020a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->g:Landroid/view/View;

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->g:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    const v0, 0x7f080207

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ClingView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Lcom/google/android/apps/youtube/app/ui/ClingView;

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Lcom/google/android/apps/youtube/app/ui/ClingView;

    iput-object p0, v0, Lcom/google/android/apps/youtube/app/ui/ClingView;->a:Lbob;

    .line 112
    const v0, 0x7f080344

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/widget/ImageView;

    .line 113
    const v0, 0x7f080345

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->i:Landroid/widget/ImageView;

    .line 114
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 172
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->j:Landroid/graphics/Rect;

    .line 175
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 176
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 178
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/widget/ImageView;

    .line 179
    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 178
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->i:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 182
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->j:Landroid/graphics/Rect;

    .line 183
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->i:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 185
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->i:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->i:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->i:Landroid/widget/ImageView;

    .line 186
    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 185
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 187
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 118
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Lcom/google/android/apps/youtube/app/ui/ClingView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ClingView;->a()Landroid/graphics/Rect;

    move-result-object v1

    .line 119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 121
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a(Z)V

    .line 123
    :cond_0
    const/4 v0, 0x0

    .line 125
    :cond_1
    return v0
.end method
