.class public Lcom/google/android/apps/youtube/core/LicensesActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 32
    const-string v0, "file:///android_asset/licenses.html"

    const-class v1, Lcom/google/android/apps/youtube/core/LicensesActivity;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "licensesUrl"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 46
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f04008b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/LicensesActivity;->setContentView(I)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/LicensesActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/LicensesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "licensesUrl"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    if-nez v1, :cond_0

    .line 53
    const-string v0, "LicensesActivity missing licenses URL, finishing."

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/LicensesActivity;->finish()V

    .line 69
    :goto_0
    return-void

    .line 57
    :cond_0
    const v0, 0x7f080214

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/LicensesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 58
    new-instance v2, Lcms;

    invoke-direct {v2, p0}, Lcms;-><init>(Lcom/google/android/apps/youtube/core/LicensesActivity;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 68
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method
