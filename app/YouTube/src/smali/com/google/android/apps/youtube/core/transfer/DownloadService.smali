.class public Lcom/google/android/apps/youtube/core/transfer/DownloadService;
.super Ldmu;
.source "SourceFile"


# instance fields
.field private f:Ljava/security/Key;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ldmu;-><init>()V

    .line 96
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/youtube/core/transfer/DownloadService;

    invoke-static {p0, v0}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lgjm;Ldnb;)Ldna;
    .locals 11

    .prologue
    .line 47
    new-instance v1, Ldmp;

    iget-object v2, p1, Lgjm;->b:Ljava/lang/String;

    iget-object v3, p1, Lgjm;->a:Ljava/lang/String;

    iget-wide v4, p1, Lgjm;->f:J

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 54
    iget-object v9, p0, Ldmu;->e:Ldmq;

    iget-object v10, p0, Lcom/google/android/apps/youtube/core/transfer/DownloadService;->f:Ljava/security/Key;

    move-object v6, p2

    invoke-direct/range {v1 .. v10}, Ldmp;-><init>(Ljava/lang/String;Ljava/lang/String;JLdnb;ZZLdmq;Ljava/security/Key;)V

    return-object v1
.end method

.method public final a(Lgjm;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Ldmn;

    invoke-direct {v0, p0, p1}, Ldmn;-><init>(Lcom/google/android/apps/youtube/core/transfer/DownloadService;Lgjm;)V

    return-object v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string v0, "downloads.db"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const-string v0, "download_policy"

    return-object v0
.end method

.method protected final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, "download_only_while_charging"

    return-object v0
.end method

.method protected final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "transfer_max_connections"

    return-object v0
.end method

.method protected final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string v0, "download_max_rate"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0}, Ldmu;->onCreate()V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/DownloadService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lckz;

    iget-object v0, v0, Lckz;->a:Letc;

    .line 34
    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {v0}, Letc;->k()Lfac;

    move-result-object v0

    .line 33
    invoke-static {v1, v0}, La;->a(Landroid/content/SharedPreferences;Lfac;)Ljava/security/Key;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/DownloadService;->f:Ljava/security/Key;

    .line 35
    return-void
.end method
