.class public Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;
.super Lbhz;
.source "SourceFile"


# instance fields
.field private e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private f:Ldwv;

.field private g:Lcom/google/android/apps/youtube/core/ui/PagedListView;

.field private h:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lbhz;-><init>()V

    .line 366
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->f()V

    return-void
.end method

.method public static synthetic a(Ljava/lang/String;Lo;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 44
    const v0, 0x7f0902ee

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Lo;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v4}, Leze;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public static synthetic a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2

    .prologue
    .line 44
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    invoke-virtual {v0}, Ldwr;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;)Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->h:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->f:Ldwv;

    invoke-interface {v0}, Ldwv;->b()Ljava/util/List;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    .line 151
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->h:Landroid/widget/ArrayAdapter;

    new-instance v3, Lbhr;

    invoke-direct {v3, v0}, Lbhr;-><init>(Ldwr;)V

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 154
    :cond_0
    return-void
.end method


# virtual methods
.method public handleSignOutEvent(Lfcc;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->finish()V

    .line 146
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 71
    invoke-super {p0, p1}, Lbhz;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    .line 75
    iget-object v0, v0, Lari;->b:Ldov;

    invoke-virtual {v0}, Ldov;->d()Ldwv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->f:Ldwv;

    .line 77
    const v0, 0x7f0400f1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->setContentView(I)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->d()Lkm;

    move-result-object v0

    const v1, 0x7f09023c

    invoke-virtual {v0, v1}, Lkm;->b(I)V

    .line 80
    new-instance v5, Lbha;

    invoke-direct {v5, p0}, Lbha;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;)V

    .line 100
    const v0, 0x7f0802bf

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->g:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    .line 101
    new-instance v0, Lbhq;

    const v3, 0x7f0400f0

    const v4, 0x7f0802be

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lbhq;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;Landroid/content/Context;IILandroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->h:Landroid/widget/ArrayAdapter;

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->g:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->a(Landroid/widget/ListAdapter;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->g:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    new-instance v1, Lbhc;

    invoke-direct {v1, p0}, Lbhc;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 123
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lbhz;->onPause()V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 136
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Lbhz;->onResume()V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 129
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->f()V

    .line 130
    return-void
.end method
