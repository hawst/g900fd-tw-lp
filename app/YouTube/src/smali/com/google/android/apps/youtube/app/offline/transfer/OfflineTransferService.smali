.class public Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;
.super Ldmu;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field private g:Ljava/security/Key;

.field private h:Lftg;

.field private i:Leuk;

.field private volatile j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ldmu;-><init>()V

    .line 287
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 258
    const-class v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;

    invoke-static {p0, v0}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;)Leuk;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->i:Leuk;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ldmw;)Lfad;
    .locals 1

    .prologue
    .line 262
    const-class v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;

    invoke-static {p0, v0, p1}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;Ldmw;)Lfad;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lckz;

    invoke-virtual {v0}, Lckz;->n()Lcla;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcla;->aD()Lcst;

    move-result-object v0

    .line 97
    invoke-interface {v0}, Lgix;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->d:Ldne;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    iget-object v0, v0, Lgit;->c:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Ldne;->a(ILjava/lang/Object;)I

    move-result v0

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lgjm;Ldnb;)Ldna;
    .locals 22

    .prologue
    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v11

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v12, v2, Lckz;->a:Letc;

    .line 109
    invoke-virtual {v11}, Lari;->aD()Lcst;

    move-result-object v2

    .line 110
    invoke-virtual {v11}, Lari;->a()Lfet;

    move-result-object v19

    .line 111
    new-instance v20, Lezj;

    invoke-direct/range {v20 .. v20}, Lezj;-><init>()V

    .line 112
    invoke-virtual {v11}, Lari;->O()Lgng;

    move-result-object v3

    .line 114
    invoke-interface {v2}, Lgix;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 115
    const/4 v3, 0x0

    .line 183
    :goto_0
    return-object v3

    .line 117
    :cond_0
    invoke-interface {v2}, Lgix;->d()Lgit;

    move-result-object v2

    .line 118
    iget-object v4, v2, Lgit;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lgjm;->i:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 119
    const/4 v3, 0x0

    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {v3, v2}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v2

    .line 124
    invoke-interface {v2}, Lgnd;->d()Lgml;

    move-result-object v21

    .line 125
    invoke-interface {v2}, Lgnd;->e()Lgmt;

    move-result-object v17

    .line 126
    invoke-interface {v2}, Lgnd;->f()Lgmk;

    move-result-object v5

    .line 128
    new-instance v2, Ldix;

    .line 129
    invoke-virtual {v11}, Lari;->aG()Lewi;

    move-result-object v3

    .line 135
    invoke-interface {v5}, Lgmk;->b()Legc;

    move-result-object v4

    .line 136
    invoke-interface {v5}, Lgmk;->c()Ljava/io/File;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->g:Ljava/security/Key;

    .line 138
    invoke-virtual {v11}, Lari;->aK()Lgco;

    move-result-object v7

    invoke-virtual {v7}, Lgco;->b()Lewi;

    move-result-object v7

    .line 139
    invoke-virtual {v12}, Letc;->f()Lezj;

    move-result-object v8

    .line 140
    invoke-virtual {v11}, Lari;->f()Larh;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->f:Ljava/lang/Object;

    invoke-direct/range {v2 .. v10}, Ldix;-><init>(Lewi;Legc;Ljava/io/File;Ljava/security/Key;Lewi;Lezj;Lcyc;Ljava/lang/Object;)V

    .line 142
    invoke-static/range {p1 .. p1}, La;->d(Lgjm;)I

    move-result v13

    .line 144
    invoke-virtual {v12}, Letc;->l()Lorg/apache/http/client/HttpClient;

    move-result-object v3

    .line 145
    invoke-virtual {v12}, Letc;->h()Ljava/util/concurrent/Executor;

    move-result-object v4

    .line 143
    invoke-static {v3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Lcqw;

    invoke-direct {v5}, Lcqw;-><init>()V

    new-instance v6, Lcmu;

    invoke-direct {v6}, Lcmu;-><init>()V

    new-instance v7, Lcmt;

    new-instance v8, Lgko;

    invoke-direct {v8, v3, v5, v5}, Lgko;-><init>(Lorg/apache/http/client/HttpClient;Lgib;Lghv;)V

    new-instance v5, Lgko;

    invoke-direct {v5, v3, v6, v6}, Lgko;-><init>(Lorg/apache/http/client/HttpClient;Lgib;Lghv;)V

    invoke-direct {v7, v8, v5}, Lcmt;-><init>(Lgku;Lgku;)V

    invoke-static {v4, v7}, Lgjx;->a(Ljava/util/concurrent/Executor;Lgku;)Lgjx;

    move-result-object v3

    new-instance v4, Leul;

    const/16 v5, 0x64

    invoke-direct {v4, v5}, Leul;-><init>(I)V

    const-wide/32 v6, 0x1b7740

    move-object/from16 v0, v20

    invoke-static {v4, v3, v0, v6, v7}, Lghi;->a(Leuk;Lgku;Lezj;J)Lghi;

    move-result-object v14

    .line 147
    iget-object v3, v11, Lcla;->as:Lezs;

    invoke-virtual {v3}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/io/File;

    .line 148
    invoke-static/range {p1 .. p1}, La;->a(Lgjm;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 149
    new-instance v3, Lbmk;

    .line 152
    invoke-virtual {v11}, Lari;->aR()Lcwg;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->h:Lftg;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->i:Leuk;

    .line 162
    invoke-virtual {v11}, Lari;->aK()Lgco;

    move-result-object v4

    iget-object v0, v4, Lgco;->i:Lggn;

    move-object/from16 v16, v0

    .line 164
    invoke-virtual {v11}, Lari;->I()Lgot;

    move-result-object v18

    move-object/from16 v4, v21

    move-object/from16 v5, v19

    move-object/from16 v9, v20

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object v12, v2

    invoke-direct/range {v3 .. v18}, Lbmk;-><init>(Lgml;Lfet;Lcwg;Lftg;Leuk;Lezj;Lgjm;Ldnb;Ldix;ILgku;Ljava/io/File;Lggn;Lgmt;Lgot;)V

    goto/16 :goto_0

    .line 170
    :cond_2
    new-instance v3, Lbmi;

    .line 173
    invoke-virtual {v11}, Lari;->aR()Lcwg;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->h:Lftg;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->i:Leuk;

    .line 183
    invoke-virtual {v11}, Lari;->aK()Lgco;

    move-result-object v4

    iget-object v0, v4, Lgco;->i:Lggn;

    move-object/from16 v16, v0

    move-object/from16 v4, v21

    move-object/from16 v5, v19

    move-object/from16 v9, v20

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object v12, v2

    invoke-direct/range {v3 .. v16}, Lbmi;-><init>(Lgml;Lfet;Lcwg;Lftg;Leuk;Lezj;Lgjm;Ldnb;Ldix;ILgku;Ljava/io/File;Lggn;)V

    goto/16 :goto_0
.end method

.method public final a(Lgjm;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x5

    return v0
.end method

.method public final b(Lgjm;)V
    .locals 2

    .prologue
    .line 239
    invoke-super {p0, p1}, Ldmu;->b(Lgjm;)V

    .line 240
    iget-object v0, p1, Lgjm;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->j:Ljava/lang/String;

    .line 243
    :cond_0
    return-void
.end method

.method protected final c()Ldmz;
    .locals 1

    .prologue
    .line 199
    new-instance v0, Lbmj;

    invoke-direct {v0, p0}, Lbmj;-><init>(Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;)V

    return-object v0
.end method

.method public final c(Lgjm;)V
    .locals 2

    .prologue
    .line 247
    invoke-super {p0, p1}, Ldmu;->c(Lgjm;)V

    .line 248
    iget-object v0, p1, Lgjm;->c:Lgjn;

    sget-object v1, Lgjn;->c:Lgjn;

    if-ne v0, v1, :cond_1

    .line 249
    iget-object v0, p1, Lgjm;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->j:Ljava/lang/String;

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    iget-object v0, p1, Lgjm;->c:Lgjn;

    sget-object v1, Lgjn;->b:Lgjn;

    if-ne v0, v1, :cond_0

    .line 253
    iget-object v0, p1, Lgjm;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    const-string v0, "bgol_tasks.db"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    const-string v0, "offline_policy_string"

    return-object v0
.end method

.method protected final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final i()I
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x1

    return v0
.end method

.method protected final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 74
    const-string v0, "Creating OfflineService..."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 75
    invoke-super {p0}, Ldmu;->onCreate()V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lckz;

    .line 77
    iget-object v1, v0, Lckz;->a:Letc;

    .line 79
    invoke-virtual {v1}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-virtual {v1}, Letc;->k()Lfac;

    move-result-object v1

    .line 78
    invoke-static {v2, v1}, La;->a(Landroid/content/SharedPreferences;Lfac;)Ljava/security/Key;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->g:Ljava/security/Key;

    .line 80
    invoke-virtual {v0}, Lckz;->h()Lftg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->h:Lftg;

    .line 81
    new-instance v0, Leul;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Leul;-><init>(I)V

    new-instance v1, Lezj;

    invoke-direct {v1}, Lezj;-><init>()V

    new-instance v2, Leuy;

    const-wide/32 v4, 0x36ee80

    invoke-direct {v2, v0, v1, v4, v5}, Leuy;-><init>(Leuk;Lezj;J)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->i:Leuk;

    .line 85
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 89
    const-string v0, "Destroying OfflineService..."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 90
    invoke-super {p0}, Ldmu;->onDestroy()V

    .line 91
    return-void
.end method
