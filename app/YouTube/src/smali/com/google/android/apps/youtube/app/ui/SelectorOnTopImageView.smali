.class public Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private b:Z

.field private c:Z

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    sget-object v0, Lgvk;->i:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 40
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->b:Z

    .line 41
    const/4 v1, 0x2

    .line 42
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->d:I

    .line 43
    const/4 v1, 0x3

    .line 44
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->e:I

    .line 45
    const/4 v1, 0x4

    .line 46
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->f:I

    .line 47
    const/4 v1, 0x5

    .line 48
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->g:I

    .line 49
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 50
    if-eqz v1, :cond_0

    .line 51
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 57
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    return-void

    .line 53
    :cond_0
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f010112

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 55
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-static {p1, v1}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 85
    :cond_1
    if-eqz p1, :cond_2

    .line 86
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 87
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 92
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->c:Z

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->invalidate()V

    goto :goto_0
.end method


# virtual methods
.method public drawableHotspotChanged(FF)V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->drawableHotspotChanged(FF)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 153
    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->invalidate()V

    .line 144
    :cond_0
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0}, Landroid/widget/ImageView;->jumpDrawablesToCurrentState()V

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 135
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 108
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->c:Z

    if-eqz v0, :cond_0

    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->b:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 110
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->b:Z

    if-eqz v2, :cond_3

    move v2, v1

    .line 111
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->getWidth()I

    move-result v4

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->b:Z

    if-eqz v3, :cond_4

    move v3, v1

    :goto_2
    sub-int/2addr v4, v3

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->getHeight()I

    move-result v5

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->b:Z

    if-eqz v3, :cond_5

    move v3, v1

    :goto_3
    sub-int v3, v5, v3

    .line 113
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->d:I

    add-int/2addr v0, v5

    .line 114
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->e:I

    add-int/2addr v2, v5

    .line 115
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->f:I

    sub-int/2addr v4, v5

    .line 116
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->g:I

    sub-int/2addr v3, v5

    .line 117
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v0, v2, v4, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 118
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->c:Z

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 122
    :cond_1
    return-void

    .line 109
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->getPaddingLeft()I

    move-result v0

    goto :goto_0

    .line 110
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->getPaddingTop()I

    move-result v2

    goto :goto_1

    .line 111
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->getPaddingRight()I

    move-result v3

    goto :goto_2

    .line 112
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->getPaddingBottom()I

    move-result v3

    goto :goto_3
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->c:Z

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->invalidate()V

    .line 102
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 126
    invoke-super {p0, p1}, Landroid/widget/ImageView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->a:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
