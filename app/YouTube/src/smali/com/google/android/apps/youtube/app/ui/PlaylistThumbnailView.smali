.class public Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public final b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

.field private final c:F

.field private final d:Landroid/graphics/Rect;

.field private final e:Landroid/graphics/Rect;

.field private final f:I

.field private final g:I

.field private final h:Landroid/graphics/Paint;

.field private i:Z

.field private j:Z

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    .line 60
    new-instance v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    sget-object v1, Lezd;->b:Lezd;

    invoke-direct {v0, p1, v1, v5, v5}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;-><init>(Landroid/content/Context;Lezd;IZ)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 61
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->d:Landroid/graphics/Rect;

    .line 62
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    .line 63
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->h:Landroid/graphics/Paint;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    const/16 v1, 0x51

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setGravity(I)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 68
    iput-boolean v4, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->i:Z

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 71
    const/high16 v1, 0x7f0c0000

    invoke-virtual {v0, v1, v4, v4}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->c:F

    .line 72
    sget-object v1, Lgvk;->h:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 74
    invoke-virtual {v1, v5, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->j:Z

    .line 75
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    const v3, 0x7f0a0008

    .line 77
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 75
    invoke-virtual {v1, v4, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-virtual {v2, v5, v3}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setTextSize(IF)V

    .line 78
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    const/4 v3, 0x2

    const v4, 0x106000b

    .line 80
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 78
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setTextColor(I)V

    .line 81
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 82
    if-eqz v2, :cond_0

    .line 83
    invoke-static {p1, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->k:Landroid/graphics/drawable/Drawable;

    .line 85
    :cond_0
    const/4 v2, 0x4

    const v3, 0x7f070091

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->f:I

    .line 88
    const/4 v2, 0x5

    const v3, 0x7f070092

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->g:I

    .line 91
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->addView(Landroid/view/View;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->addView(Landroid/view/View;)V

    .line 95
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->i:Z

    if-eq v0, p1, :cond_0

    .line 117
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->i:Z

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-static {v0, p1}, Leze;->a(Landroid/view/View;Z)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->invalidate()V

    .line 121
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->j:Z

    if-eq v0, p1, :cond_0

    .line 128
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->j:Z

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->requestLayout()V

    .line 131
    :cond_0
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    .prologue
    .line 226
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    .line 228
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    if-ne p2, v0, :cond_0

    .line 229
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->j:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->f:I

    .line 230
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 237
    :cond_0
    return v1

    .line 229
    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->g:I

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 201
    sub-int v4, p4, p2

    .line 202
    sub-int v5, p5, p3

    .line 203
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->j:Z

    if-eqz v0, :cond_2

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v5, v5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 208
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    const/4 v1, 0x0

    div-int/lit8 v2, v5, 0x2

    invoke-virtual {v0, v5, v1, v4, v2}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->layout(IIII)V

    .line 209
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->k:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->d:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-nez v0, :cond_3

    .line 211
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->getChildCount()I

    move-result v1

    .line 212
    const/4 v0, 0x2

    if-le v1, v0, :cond_6

    .line 213
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_6

    .line 214
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 215
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    if-eq v2, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    if-eq v2, v3, :cond_1

    .line 216
    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 213
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/widget/ImageView;->layout(IIII)V

    goto :goto_0

    .line 209
    :cond_3
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-gt v2, v3, :cond_4

    if-le v0, v1, :cond_7

    :cond_4
    int-to-float v2, v2

    int-to-float v8, v3

    div-float/2addr v2, v8

    int-to-float v0, v0

    int-to-float v8, v1

    div-float/2addr v0, v8

    cmpl-float v8, v2, v0

    if-lez v8, :cond_5

    int-to-float v0, v1

    div-float/2addr v0, v2

    float-to-int v0, v0

    move v1, v3

    :goto_3
    iget v2, v7, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    add-int/2addr v0, v3

    invoke-virtual {v6, v2, v3, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1

    :cond_5
    int-to-float v2, v3

    div-float v0, v2, v0

    float-to-int v0, v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_3

    .line 222
    :cond_6
    return-void

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 153
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-ne v1, v8, :cond_1

    .line 154
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 155
    int-to-float v1, v2

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->c:F

    div-float/2addr v1, v3

    float-to-int v1, v1

    .line 156
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 166
    :goto_0
    if-lez v2, :cond_4

    if-lez v1, :cond_4

    .line 167
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->j:Z

    if-eqz v3, :cond_3

    .line 168
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    invoke-virtual {v3, p2, p2}, Landroid/widget/ImageView;->measure(II)V

    .line 172
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v0, v2, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 173
    int-to-float v3, v1

    const v4, 0x3dcccccd    # 0.1f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->l:I

    .line 174
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->d:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    .line 176
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->l:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    .line 174
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 179
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    .line 180
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->e:Landroid/graphics/Rect;

    .line 181
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 179
    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->measure(II)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->getChildCount()I

    move-result v3

    .line 184
    const/4 v4, 0x2

    if-le v3, v4, :cond_4

    .line 185
    :goto_2
    if-ge v0, v3, :cond_4

    .line 186
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 187
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    if-eq v4, v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    if-eq v4, v5, :cond_0

    .line 188
    invoke-virtual {v4, p1, p2}, Landroid/view/View;->measure(II)V

    .line 185
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 157
    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-ne v1, v8, :cond_2

    .line 158
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 159
    int-to-float v2, v1

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->c:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 160
    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto/16 :goto_0

    :cond_2
    move v1, v0

    move v2, v0

    .line 163
    goto/16 :goto_0

    .line 170
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    invoke-virtual {v3, p1, p2}, Landroid/widget/ImageView;->measure(II)V

    goto/16 :goto_1

    .line 196
    :cond_4
    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->setMeasuredDimension(II)V

    .line 197
    return-void
.end method
