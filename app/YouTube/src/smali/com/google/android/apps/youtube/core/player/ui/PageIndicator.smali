.class public Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field public a:I

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:I

.field private f:Landroid/graphics/Rect;

.field private g:Landroid/graphics/Rect;

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:Landroid/graphics/Rect;

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->g:Landroid/graphics/Rect;

    .line 38
    sget-object v0, Lgvk;->e:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 41
    invoke-virtual {v2, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 42
    invoke-virtual {v2, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 39
    if-nez v1, :cond_0

    const v1, 0x7f020200

    invoke-static {p1, v1}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_0
    if-nez v0, :cond_1

    const v0, 0x7f020201

    invoke-static {p1, v0}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_1
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v1, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->b:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->h:I

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->i:I

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->h:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->d:I

    iput v6, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->e:I

    .line 43
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 44
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->e:I

    if-eq v0, p1, :cond_0

    .line 81
    iput p1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->e:I

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->invalidate()V

    .line 84
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 100
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:I

    if-gtz v1, :cond_0

    .line 126
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 105
    iget v2, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:I

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->h:I

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->d:I

    add-int/2addr v3, v4

    mul-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->d:I

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 107
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:Landroid/graphics/Rect;

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingLeft()I

    move-result v4

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingTop()I

    move-result v5

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getHeight()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingBottom()I

    move-result v8

    sub-int/2addr v7, v8

    .line 107
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 112
    const/16 v3, 0x31

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->g:Landroid/graphics/Rect;

    invoke-static {v3, v2, v1, v4, v5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 114
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 116
    :goto_1
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:I

    if-ge v0, v1, :cond_2

    .line 117
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->e:I

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->c:Landroid/graphics/drawable/Drawable;

    .line 118
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 119
    iget v2, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->h:I

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->i:I

    .line 120
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    .line 119
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 121
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 122
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 123
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->d:I

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->h:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    .line 125
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    .line 88
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->d:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 95
    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->resolveSize(II)I

    move-result v0

    invoke-static {v1, p2}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->resolveSize(II)I

    move-result v1

    .line 94
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->setMeasuredDimension(II)V

    .line 96
    return-void
.end method
