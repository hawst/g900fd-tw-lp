.class public final Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcwk;


# instance fields
.field public final a:Landroid/app/Activity;

.field public b:Lcwm;

.field public c:Landroid/app/Dialog;

.field public d:Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;

.field public e:Leue;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:Lgix;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/concurrent/Executor;Lgix;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->a:Landroid/app/Activity;

    .line 59
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->f:Ljava/util/concurrent/Executor;

    .line 60
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->g:Lgix;

    .line 61
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;)V
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->b:Lcwm;

    invoke-interface {v0}, Lcwm;->b()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;)V
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->b:Lcwm;

    invoke-interface {v0}, Lcwm;->c()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->e:Leue;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->e:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->d:Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;

    const-string v1, "about:blank"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;->loadUrl(Ljava/lang/String;)V

    .line 233
    return-void
.end method

.method public final a(Lfij;Lcwm;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->g:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    .line 69
    iput-object p2, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->b:Lcwm;

    .line 71
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->a:Landroid/app/Activity;

    const v2, 0x103000a

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->c:Landroid/app/Dialog;

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->c:Landroid/app/Dialog;

    const v1, 0x7f040025

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->c:Landroid/app/Dialog;

    new-instance v1, Lbcd;

    invoke-direct {v1, p0}, Lbcd;-><init>(Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->c:Landroid/app/Dialog;

    const v1, 0x7f0800c8

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    new-instance v1, Lbce;

    invoke-direct {v1, p0}, Lbce;-><init>(Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->c:Landroid/app/Dialog;

    const v1, 0x7f0800c7

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->d:Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->d:Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->d:Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->d:Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    iget-object v0, p1, Lfij;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->g:Lgix;

    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    iget-object v1, v1, Lgit;->b:Lgiv;

    invoke-virtual {v1}, Lgiv;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->d:Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;

    new-instance v3, Lbcf;

    invoke-direct {v3, p0, v0}, Lbcf;-><init>(Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog$CustomWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    new-instance v2, Lbcg;

    invoke-direct {v2, p0}, Lbcg;-><init>(Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;)V

    invoke-static {v2}, Leue;->a(Leuc;)Leue;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->e:Leue;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;->f:Ljava/util/concurrent/Executor;

    new-instance v3, Lbch;

    invoke-direct {v3, p0, v0, v1}, Lbch;-><init>(Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 84
    return-void
.end method
