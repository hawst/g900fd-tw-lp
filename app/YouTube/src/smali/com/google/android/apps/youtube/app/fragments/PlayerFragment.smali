.class public Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;
.super Lj;
.source "SourceFile"

# interfaces
.implements Landroid/view/KeyEvent$Callback;
.implements Lbgo;
.implements Lbvv;
.implements Ldcj;
.implements Ldso;
.implements Ldwn;
.implements Lfrz;


# instance fields
.field public W:Lcxb;

.field public X:Ldcd;

.field public Y:Z

.field public Z:Lbvn;

.field public a:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

.field private aA:Ldcq;

.field private aB:Ldcw;

.field private aC:Ldbz;

.field private aD:Ldfl;

.field private aE:Ldcx;

.field private aF:Lbmw;

.field private aG:Lbmx;

.field private aH:Lbms;

.field private aI:Lfrl;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private aJ:Lfqg;

.field private aK:Landroid/widget/Toast;

.field private aL:Leyp;

.field private aM:Lgot;

.field private aN:Ldsn;

.field private aO:Ldwq;

.field private aP:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private aQ:Lari;

.field private aR:Letc;

.field private aS:Ldwv;

.field private aT:Ldcd;

.field private aU:Ldwl;

.field private aV:Lcvu;

.field private aW:Ldlf;

.field private aX:Ljava/lang/String;

.field public aa:Lcws;

.field public ab:Layx;

.field public ac:Z

.field private ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private ae:Lgom;

.field private af:Leyt;

.field private ag:Ldbv;

.field private ah:Ldbo;

.field private ai:Ldfh;

.field private aj:Lddd;

.field private ak:Ldfw;

.field private al:Ldep;

.field private am:Ldfn;

.field private an:Ldfk;

.field private ao:Ldcz;

.field private ap:Lbmt;

.field private aq:Landroid/content/SharedPreferences;

.field private ar:Layy;

.field private as:Lfdw;

.field private at:Lcwg;

.field private au:Lcwj;

.field private av:Ldbz;

.field private aw:Ldcr;

.field private ax:Ldge;

.field private ay:Ldcl;

.field private az:Ldcn;

.field public b:Lbgj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lj;-><init>()V

    .line 892
    return-void
.end method

.method private F()V
    .locals 2

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcws;->d(Z)V

    .line 837
    return-void
.end method

.method private G()Lbmt;
    .locals 5

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ap:Lbmt;

    if-nez v0, :cond_0

    .line 1055
    new-instance v0, Lbmw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1}, Lbmw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aF:Lbmw;

    .line 1056
    new-instance v0, Lbmx;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1}, Lbmx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aG:Lbmx;

    .line 1060
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    iget-object v0, v0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c()I

    move-result v0

    .line 1061
    new-instance v1, Lbms;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v1, v2, v0}, Lbms;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aH:Lbms;

    .line 1062
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    const/4 v1, 0x3

    new-array v1, v1, [Ldec;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aF:Lbmw;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aG:Lbmx;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aH:Lbms;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcxb;->a([Ldec;)V

    .line 1066
    new-instance v0, Lbmt;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    .line 1067
    iget-object v1, v1, Lari;->b:Ldov;

    invoke-virtual {v1}, Ldov;->b()Ldwq;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aG:Lbmx;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aH:Lbms;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aF:Lbmw;

    invoke-direct {v0, v1, v2, v3, v4}, Lbmt;-><init>(Ldwq;Lbmx;Lbms;Lbmw;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ap:Lbmt;

    .line 1072
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ap:Lbmt;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Lcom/google/android/apps/youtube/app/WatchWhileActivity;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    return-object v0
.end method

.method private a(Ldlf;)V
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0, p1}, Lcws;->a(Ldlf;)V

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Ljava/lang/String;)V

    .line 572
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 582
    if-eqz p1, :cond_1

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iput-object p1, v0, Lbvn;->h:Ljava/lang/String;

    .line 586
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Y:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->f(Z)V

    .line 588
    :cond_1
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Lbvn;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Ldwq;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aO:Ldwq;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Leyt;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->af:Leyt;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Ldsn;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aN:Ldsn;

    return-object v0
.end method

.method private handlePaidContentTransactionCompleteEvent(Lbml;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 911
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->z()V

    .line 912
    return-void
.end method

.method private handlePlaybackServiceException(Lczb;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 875
    sget-object v0, Lbeb;->a:[I

    iget-object v1, p1, Lczb;->a:Lczc;

    invoke-virtual {v1}, Lczc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 888
    :cond_0
    :goto_0
    return-void

    .line 882
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v0, :cond_0

    .line 883
    iget-object v0, p1, Lczb;->c:Ljava/lang/String;

    .line 884
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 885
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x7f090118

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 887
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    invoke-virtual {v1, v0}, Lbvn;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 875
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private handleSequencerEndedEvent(Lczs;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 868
    iget-boolean v0, p1, Lczs;->a:Z

    if-eqz v0, :cond_0

    .line 869
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->F()V

    .line 871
    :cond_0
    return-void
.end method

.method private handleSequencerStageEvent(Lczu;)V
    .locals 7
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 851
    iget-object v0, p1, Lczu;->b:Lfrl;

    if-eqz v0, :cond_7

    .line 855
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    iget-object v2, p1, Lczu;->b:Lfrl;

    if-eq v0, v2, :cond_5

    .line 856
    iget-object v2, p1, Lczu;->b:Lfrl;

    iget-object v3, p1, Lczu;->d:Lfqg;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    iget-object v0, v0, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v2, Lfrl;->a:Lhro;

    invoke-static {v4}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aJ:Lfqg;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aN:Ldsn;

    iget-object v0, v0, Ldsn;->d:Ldwq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    invoke-virtual {v0, v2}, Lbvn;->a(Lfrl;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aX:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aX:Ljava/lang/String;

    new-instance v4, Ldtj;

    invoke-direct {v4, v0}, Ldtj;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aS:Ldwv;

    new-instance v5, Lbdy;

    invoke-direct {v5, p0}, Lbdy;-><init>(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)V

    invoke-interface {v0, v4, v5}, Ldwv;->a(Ldtj;Ldvl;)V

    iput-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aX:Ljava/lang/String;

    :cond_2
    invoke-virtual {v2}, Lfrl;->i()Lfqy;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lfqy;->b()Z

    move-result v1

    :cond_3
    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->af:Leyt;

    const v1, 0x7f090333

    invoke-interface {v0, v1}, Leyt;->a(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lfdw;

    sget-object v1, Lfqi;->a:Lfqi;

    invoke-virtual {v0, v3, v1, v6}, Lfdw;->a(Lfqg;Lfqi;Lhcq;)V

    .line 861
    :cond_5
    :goto_1
    return-void

    :cond_6
    move v0, v1

    .line 856
    goto :goto_0

    .line 858
    :cond_7
    iget-object v0, p1, Lczu;->a:Lgok;

    sget-object v1, Lgok;->f:Lgok;

    if-ne v0, v1, :cond_5

    .line 859
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->F()V

    goto :goto_1
.end method

.method private handleSignInEvent(Lfcb;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 921
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->z()V

    .line 922
    return-void
.end method

.method private handleSignOutEvent(Lfcc;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 931
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p()V

    .line 932
    return-void
.end method

.method private handleUnplayableVideoSkipped(Lczy;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 901
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aK:Landroid/widget/Toast;

    const v1, 0x7f090297

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 902
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aK:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 903
    return-void
.end method

.method private handleVideoFullscreenEvent(Ldab;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 841
    iget-boolean v0, p1, Ldab;->a:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->f(Z)V

    .line 842
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-boolean v1, p1, Ldab;->a:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    .line 843
    return-void
.end method


# virtual methods
.method public A()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 489
    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ae:Lgom;

    .line 490
    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aW:Ldlf;

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->au:Lcwj;

    iget-object v1, v0, Lcwj;->c:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcwj;->c:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcwj;->c:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->cancel()V

    :cond_0
    iput-object v2, v0, Lcwj;->c:Landroid/app/AlertDialog;

    .line 492
    :cond_1
    return-void
.end method

.method public final B()Lfqg;
    .locals 1

    .prologue
    .line 1050
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aJ:Lfqg;

    return-object v0
.end method

.method public C()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 525
    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    invoke-virtual {v0, v1}, Lbvn;->a(Lfrl;)V

    .line 529
    :cond_0
    return-void
.end method

.method public final D()V
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q()V

    .line 562
    return-void
.end method

.method public final E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 17

    .prologue
    .line 257
    const v2, 0x7f0400af

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v16

    .line 259
    const v2, 0x7f080265

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcxb;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v2}, Lari;->aU()Lert;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    iput-object v2, v3, Lert;->a:Landroid/view/View;

    .line 262
    new-instance v2, Ldcd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Ldcd;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    move-object/from16 v0, p0

    iput-object v0, v2, Ldcd;->d:Ldcj;

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 265
    iget-object v3, v3, Lavv;->l:Lbgt;

    iget-object v3, v3, Lbgt;->f:Landroid/support/v7/app/MediaRouteButton;

    .line 264
    invoke-virtual {v2, v3}, Ldcd;->a(Landroid/view/View;)V

    .line 267
    new-instance v2, Ldbz;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    .line 269
    iget-object v4, v4, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c()I

    move-result v4

    invoke-direct {v2, v3, v4}, Ldbz;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->av:Ldbz;

    .line 270
    new-instance v2, Ldcr;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Ldcr;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aw:Ldcr;

    .line 272
    new-instance v2, Lbmq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Lbmq;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ax:Ldge;

    .line 273
    new-instance v2, Ldcl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Ldcl;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ay:Ldcl;

    .line 274
    new-instance v2, Ldcn;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Ldcn;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->az:Ldcn;

    .line 275
    new-instance v2, Ldcq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Ldcq;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aA:Ldcq;

    .line 276
    new-instance v2, Ldcw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aP:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Ldcw;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Ldcw;

    .line 277
    new-instance v2, Ldcx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aP:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Ldcx;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aE:Ldcx;

    .line 279
    new-instance v2, Ldfl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Ldfl;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Ldfl;

    .line 280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Ldfl;

    const v3, 0x7f0901b1

    invoke-virtual {v2, v3}, Ldfl;->setText(I)V

    .line 282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    const/16 v3, 0xa

    new-array v3, v3, [Ldec;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Ldcw;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aE:Ldcx;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Ldfl;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aA:Ldcq;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    aput-object v5, v3, v4

    const/4 v4, 0x5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->az:Ldcn;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ax:Ldge;

    aput-object v5, v3, v4

    const/4 v4, 0x7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->av:Ldbz;

    aput-object v5, v3, v4

    const/16 v4, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aw:Ldcr;

    aput-object v5, v3, v4

    const/16 v4, 0x9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ay:Ldcl;

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcxb;->a([Ldec;)V

    .line 294
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v2}, Lari;->K()Ldsn;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aN:Ldsn;

    .line 296
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v2}, Lari;->f()Larh;

    move-result-object v2

    invoke-virtual {v2}, Larh;->ac()Lfko;

    move-result-object v2

    iget-boolean v2, v2, Lfko;->b:Z

    if-nez v2, :cond_0

    .line 298
    new-instance v2, Ldcd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Ldcd;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ldcd;->n(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, v3, Lavv;->l:Lbgt;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3, v4}, Lbgt;->a(Landroid/content/Context;)Landroid/support/v7/app/MediaRouteButton;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldcd;->a(Landroid/view/View;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aT:Ldcd;

    .line 299
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aT:Ldcd;

    move-object/from16 v0, p0

    iput-object v0, v2, Ldcd;->d:Ldcj;

    .line 302
    new-instance v2, Ldbz;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aT:Ldcd;

    .line 304
    iget-object v4, v4, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c()I

    move-result v4

    invoke-direct {v2, v3, v4}, Ldbz;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aC:Ldbz;

    .line 305
    new-instance v2, Lbvn;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    .line 307
    iget-object v4, v4, Lari;->b:Ldov;

    invoke-virtual {v4}, Ldov;->g()Ldaq;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aL:Leyp;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aM:Lgot;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aT:Ldcd;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aC:Ldbz;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 316
    iget-object v13, v11, Lbhz;->m:Lfus;

    const v11, 0x7f080266

    .line 317
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 318
    invoke-virtual {v11}, Letc;->i()Levn;

    move-result-object v15

    move-object/from16 v11, p0

    invoke-direct/range {v2 .. v15}, Lbvn;-><init>(Lcws;Ldaq;Landroid/app/Activity;Leyp;Lgot;Ldcd;Ldbz;Landroid/view/View;Lbvv;Lbvw;Lfus;Landroid/view/View;Levn;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    .line 321
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aP:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v2

    iget-object v2, v2, Lari;->b:Ldov;

    invoke-virtual {v2}, Ldov;->d()Ldwv;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aS:Ldwv;

    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const-string v3, ""

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Leze;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aK:Landroid/widget/Toast;

    .line 325
    return-object v16
.end method

.method public final a()Lcws;
    .locals 1

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 205
    invoke-super {p0, p1}, Lj;->a(Landroid/app/Activity;)V

    .line 206
    check-cast p1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 207
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 211
    invoke-super {p0, p1}, Lj;->a(Landroid/os/Bundle;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aP:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aP:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aP:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Landroid/content/SharedPreferences;

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v0}, Lari;->ay()Leyt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->af:Leyt;

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v0}, Lari;->c()Leyp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aL:Leyp;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    iget-object v0, v0, Lari;->R:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgot;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aM:Lgot;

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    iget-object v0, v0, Lari;->b:Ldov;

    invoke-virtual {v0}, Ldov;->b()Ldwq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aO:Ldwq;

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    iget-object v0, v0, Lari;->b:Ldov;

    invoke-virtual {v0}, Ldov;->k()Ldwl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aU:Ldwl;

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v0}, Lari;->Q()Layy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Layy;

    .line 224
    new-instance v0, Layx;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Layy;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 227
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    .line 228
    invoke-virtual {v4}, Lari;->c()Leyp;

    move-result-object v4

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Layx;-><init>(Landroid/app/Activity;Layy;Lbyg;Leyp;Lfrz;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v0}, Lari;->ae()Lfdw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lfdw;

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v0}, Lari;->S()Lcws;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcws;->c(Z)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v0}, Lari;->aR()Lcwg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcwg;

    .line 236
    new-instance v0, Lcwj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 240
    invoke-virtual {v4}, Letc;->h()Ljava/util/concurrent/Executor;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    .line 241
    invoke-virtual {v5}, Lari;->aD()Lcst;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/youtube/app/fragments/AgeVerificationDialog;-><init>(Landroid/app/Activity;Ljava/util/concurrent/Executor;Lgix;)V

    invoke-direct {v0, v1, v2}, Lcwj;-><init>(Landroid/app/Activity;Lcwk;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->au:Lcwj;

    .line 243
    if-eqz p1, :cond_3

    .line 244
    const-string v0, "watch_back_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbgj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lbgj;

    .line 245
    const-string v0, "playback_service_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldlf;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aW:Ldlf;

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    iget-object v0, v0, Layx;->b:Layt;

    const-string v1, "background_dialog_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_0

    invoke-static {}, Layw;->a()[I

    move-result-object v2

    array-length v2, v2

    if-ge v1, v2, :cond_1

    :cond_0
    invoke-static {}, Layw;->a()[I

    move-result-object v2

    aget v1, v2, v1

    iput v1, v0, Layt;->d:I

    :cond_1
    const-string v1, "background_failed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_2

    :try_start_0
    new-instance v2, Lhbg;

    invoke-direct {v2}, Lhbg;-><init>()V

    invoke-static {v2, v1}, Lidh;->a(Lidh;[B)Lidh;

    iput-object v2, v0, Layt;->f:Lhbg;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    const-string v1, "background_start_time"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Layt;->e:J

    .line 248
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lbgj;

    if-nez v0, :cond_4

    .line 249
    new-instance v0, Lbgj;

    invoke-direct {v0}, Lbgj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lbgj;

    .line 251
    :cond_4
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Ldwq;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 960
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-nez v0, :cond_1

    .line 987
    :cond_0
    :goto_0
    return-void

    .line 964
    :cond_1
    if-nez p1, :cond_7

    .line 965
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    invoke-virtual {v0}, Lbvn;->p()Z

    move-result v0

    .line 966
    if-eqz v0, :cond_5

    .line 967
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iget-object v1, v0, Lbvn;->f:Ldwo;

    .line 968
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iget v0, v0, Lbvn;->e:I

    .line 969
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    .line 970
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    invoke-virtual {v2}, Lbvn;->o()V

    .line 971
    if-eqz v1, :cond_4

    sget-object v2, Ldwo;->b:Ldwo;

    if-ne v1, v2, :cond_2

    const/4 v0, 0x0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v2, v0}, Lcws;->a(I)V

    sget-object v0, Ldwo;->b:Ldwo;

    if-eq v1, v0, :cond_3

    sget-object v0, Ldwo;->d:Ldwo;

    if-ne v1, v0, :cond_6

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->m()V

    .line 972
    :cond_4
    :goto_1
    if-eqz v3, :cond_5

    .line 973
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Ldep;

    invoke-virtual {v0, v3}, Ldep;->a(Lgpa;)V

    .line 977
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcwg;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->au:Lcwj;

    iput-object v1, v0, Lcwg;->d:Lcwj;

    goto :goto_0

    .line 971
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->l()V

    goto :goto_1

    .line 979
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    invoke-virtual {v0, p1}, Lbvn;->a(Ldwq;)V

    .line 982
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcwg;

    iput-object v3, v0, Lcwg;->d:Lcwj;

    .line 983
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    if-eqz v0, :cond_0

    .line 984
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    invoke-virtual {v0, v1}, Lbvn;->a(Lfrl;)V

    goto :goto_0
.end method

.method public final a(Lgom;)V
    .locals 4

    .prologue
    .line 447
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-object v1, p1, Lgom;->a:Lgoh;

    invoke-virtual {v0, v1}, Lcws;->b(Lgoh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    :goto_0
    return-void

    .line 453
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 454
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ae:Lgom;

    goto :goto_0

    .line 458
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->A()V

    .line 460
    iget-object v0, p1, Lgom;->b:Leaf;

    iget-object v0, v0, Leaf;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aX:Ljava/lang/String;

    .line 462
    iget-object v0, p1, Lgom;->a:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    .line 465
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    .line 466
    invoke-virtual {v1}, Lcws;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 467
    :cond_2
    iget-object v0, p1, Lgom;->b:Leaf;

    iget-boolean v0, v0, Leaf;->e:Z

    if-eqz v0, :cond_4

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcws;->f(Z)Ldlf;

    move-result-object v0

    .line 469
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lbgj;

    new-instance v2, Lbgl;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0}, Lbgl;-><init>(Lgom;Ldlf;)V

    invoke-virtual {v1, v2}, Lbgj;->a(Lbge;)V

    .line 477
    :cond_3
    :goto_1
    iget-object v0, p1, Lgom;->a:Lgoh;

    iget-object v1, v0, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->a:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v1, v0}, Lcws;->a(Lgoh;)V

    goto :goto_0

    .line 473
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lbgj;

    invoke-virtual {v0}, Lbgj;->b()Lbge;

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    iput-boolean p1, v0, Ldef;->c:Z

    invoke-virtual {v0, p1}, Ldef;->c(Z)V

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-object v0, v0, Lcws;->f:Lcvq;

    invoke-interface {v0, p1}, Lcvq;->c(Z)V

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iput-boolean p1, v0, Lbvn;->g:Z

    invoke-virtual {v0, p1}, Lbvn;->b(Z)V

    .line 537
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iget-boolean v0, v0, Lbvn;->d:Z

    if-eqz v0, :cond_0

    .line 1015
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iget v0, v0, Lbvn;->e:I

    .line 1017
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->g()I

    move-result v0

    goto :goto_0
.end method

.method public final c()Lgpa;
    .locals 1

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Ldep;

    iget-object v0, v0, Ldep;->d:Lgpa;

    return-object v0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 330
    invoke-super {p0, p1}, Lj;->d(Landroid/os/Bundle;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 332
    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    invoke-static {v0}, Lbbt;->a(Lbbp;)Lfhz;

    move-result-object v1

    .line 334
    new-instance v0, Lcvu;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    .line 337
    invoke-virtual {v3}, Lari;->ae()Lfdw;

    move-result-object v3

    .line 338
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->l()Lt;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 339
    invoke-virtual {v5}, Letc;->i()Levn;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcvu;-><init>(Lfhz;Lcws;Lfdw;Lt;Levn;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aV:Lcvu;

    .line 341
    new-instance v2, Ldbo;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->av:Ldbz;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 343
    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    .line 345
    invoke-virtual {v0}, Lari;->s()Lfgk;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aL:Leyp;

    move-object v8, v1

    invoke-direct/range {v2 .. v8}, Ldbo;-><init>(Ldbm;Levn;Lcws;Lfgk;Leyp;Lfhz;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Ldbo;

    .line 348
    new-instance v0, Ldbv;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 349
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 351
    invoke-virtual {v4}, Letc;->i()Levn;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    invoke-direct {v0, v2, v3, v4, v5}, Ldbv;-><init>(Landroid/content/res/Resources;Lcws;Levn;Ldbr;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ag:Ldbv;

    .line 353
    new-instance v0, Ldei;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    invoke-direct {v0, v2, v3}, Ldei;-><init>(Lcwz;Ldbr;)V

    .line 354
    new-instance v2, Ldfh;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    .line 355
    iget-object v3, v0, Lcws;->d:Lcwn;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 356
    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aw:Ldcr;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 358
    invoke-virtual {v0}, Letc;->f()Lezj;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    .line 359
    invoke-virtual {v0}, Lcws;->c()Lcxl;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    invoke-direct/range {v2 .. v8}, Ldfh;-><init>(Lcwn;Levn;Ldff;Lezj;Lcxl;Lcwz;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Ldfh;

    .line 361
    new-instance v0, Lddd;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ay:Ldcl;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-direct {v0, v2, v3}, Lddd;-><init>(Lddb;Lcws;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aj:Lddd;

    .line 364
    new-instance v2, Ldfw;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 366
    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ax:Ldge;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aL:Leyp;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    const/4 v9, 0x1

    move-object v7, v1

    invoke-direct/range {v2 .. v9}, Ldfw;-><init>(Landroid/content/Context;Levn;Ldfu;Leyp;Lfhz;Lcwz;Z)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ak:Ldfw;

    .line 372
    new-instance v0, Ldep;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aA:Ldcq;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aM:Lgot;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ag:Ldbv;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Landroid/content/SharedPreferences;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 379
    invoke-virtual {v5}, Letc;->i()Levn;

    move-result-object v7

    move v5, v10

    invoke-direct/range {v0 .. v7}, Ldep;-><init>(Ldeo;Lgot;Ldbv;Landroid/content/SharedPreferences;ZLandroid/content/Context;Levn;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Ldep;

    .line 380
    new-instance v0, Ldfn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Ldcw;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aL:Leyp;

    invoke-direct {v0, v1, v2, v10}, Ldfn;-><init>(Ldfm;Leyp;Z)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->am:Ldfn;

    .line 384
    new-instance v0, Ldbx;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->az:Ldcn;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    .line 387
    invoke-virtual {v3}, Letc;->i()Levn;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    .line 388
    invoke-virtual {v4}, Lari;->aK()Lgco;

    move-result-object v4

    iget-object v4, v4, Lgco;->c:Lefj;

    .line 389
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->k()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ag:Ldbv;

    invoke-direct/range {v0 .. v6}, Ldbx;-><init>(Lddy;Landroid/content/SharedPreferences;Levn;Lefj;Landroid/util/DisplayMetrics;Ldbv;)V

    .line 391
    new-instance v0, Ldfk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Ldfl;

    invoke-direct {v0, v1}, Ldfk;-><init>(Ldfl;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->an:Ldfk;

    .line 392
    new-instance v0, Ldcz;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aE:Ldcx;

    invoke-direct {v0, v1}, Ldcz;-><init>(Ldcx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ao:Ldcz;

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Ldbo;

    iput-object v1, v0, Ldcd;->c:Ldcf;

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aT:Ldcd;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aT:Ldcd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Ldbo;

    iput-object v1, v0, Ldcd;->c:Ldcf;

    .line 399
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->l()Lt;

    move-result-object v0

    const v1, 0x7f080258

    invoke-virtual {v0, v1}, Lt;->a(I)Lj;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    .line 401
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 413
    invoke-super {p0}, Lj;->e()V

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aU:Ldwl;

    invoke-virtual {v0, p0}, Ldwl;->a(Ldwn;)V

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b:Lbym;

    .line 418
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    iput-object v0, v1, Ldcd;->b:Ldck;

    .line 419
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aT:Ldcd;

    if-eqz v1, :cond_0

    .line 420
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aT:Ldcd;

    iput-object v0, v1, Ldcd;->b:Ldck;

    .line 422
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aV:Lcvu;

    iget-object v0, v1, Lcvu;->b:Lt;

    sget-object v2, Lcvu;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lt;->a(Ljava/lang/String;)Lj;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ldls;

    iget-object v2, v0, Ldls;->X:Lfkg;

    iput-object v2, v1, Lcvu;->c:Lfkg;

    invoke-virtual {v1, v0}, Lcvu;->a(Ldls;)V

    .line 423
    :cond_1
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 812
    invoke-super {p0, p1}, Lj;->e(Landroid/os/Bundle;)V

    .line 813
    const-string v0, "watch_back_stack"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lbgj;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 814
    const-string v0, "playback_service_state"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcws;->f(Z)Ldlf;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    iget-object v1, v0, Layx;->b:Layt;

    const-string v0, "background_dialog_type"

    iget v2, v1, Layt;->d:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, v1, Layt;->f:Lhbg;

    if-eqz v0, :cond_0

    iget-object v0, v1, Layt;->f:Lhbg;

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    :goto_0
    const-string v2, "background_failed"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v0, "background_start_time"

    iget-wide v2, v1, Layt;->e:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 816
    return-void

    .line 815
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    invoke-virtual {v0, p1}, Lcxb;->b(Z)V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v0, :cond_1

    .line 542
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-nez p1, :cond_0

    iget-boolean v0, v1, Lbvn;->g:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lbvn;->b(Z)V

    .line 544
    :cond_1
    return-void

    .line 542
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 804
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->C()V

    .line 805
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aU:Ldwl;

    invoke-virtual {v0, p0}, Ldwl;->b(Ldwn;)V

    .line 807
    invoke-super {p0}, Lj;->f()V

    .line 808
    return-void
.end method

.method public final f(Z)V
    .locals 2

    .prologue
    .line 825
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Y:Z

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Y:Z

    invoke-virtual {v0, v1}, Lcws;->d(Z)V

    .line 827
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 820
    invoke-super {p0}, Lj;->g()V

    .line 821
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Ldep;

    invoke-virtual {v0}, Ldep;->a()V

    .line 822
    return-void
.end method

.method public final g(Z)V
    .locals 1

    .prologue
    .line 831
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->f(Z)V

    .line 832
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    .line 833
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 940
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    invoke-virtual {v0, p1, p2}, Ldcd;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 955
    const/4 v0, 0x0

    return v0
.end method

.method public final onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 950
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    invoke-virtual {v0, p1, p2}, Ldcd;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final t()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 670
    invoke-super {p0}, Lj;->t()V

    .line 671
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    .line 672
    new-instance v3, Lbac;

    invoke-direct {v3}, Lbac;-><init>()V

    invoke-virtual {v0, v3}, Levn;->d(Ljava/lang/Object;)V

    .line 673
    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 674
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 675
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Ldbo;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 676
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ak:Ldfw;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 677
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ag:Ldbv;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 678
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aj:Lddd;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 679
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Ldep;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 680
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Ldfh;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 681
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->am:Ldfn;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 682
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aV:Lcvu;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 683
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->an:Ldfk;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 684
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ao:Ldcz;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 686
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v3}, Lari;->f()Larh;

    move-result-object v3

    invoke-virtual {v3}, Larh;->ac()Lfko;

    move-result-object v3

    iget-boolean v3, v3, Lfko;->b:Z

    if-eqz v3, :cond_0

    .line 687
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->G()Lbmt;

    move-result-object v3

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    .line 690
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->t()V

    .line 692
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v0}, Lari;->U()Laxk;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-interface {v0, v3}, Laxk;->a(Lo;)V

    .line 693
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v0, :cond_1

    .line 694
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aN:Ldsn;

    iget-object v0, v0, Ldsn;->d:Ldwq;

    .line 695
    if-eqz v0, :cond_4

    .line 696
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    invoke-virtual {v3, v0}, Lbvn;->a(Ldwq;)V

    .line 697
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcwg;

    iput-object v8, v0, Lcwg;->d:Lcwj;

    .line 702
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iget-boolean v0, v3, Lbvn;->c:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    const-string v4, "cannot call onResume() multiple times, need to call onPause() first"

    invoke-static {v0, v4}, Lb;->d(ZLjava/lang/Object;)V

    iput-boolean v1, v3, Lbvn;->c:Z

    iget-object v0, v3, Lbvn;->b:Ldwq;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lbvn;->a:Levn;

    invoke-virtual {v0, v3}, Levn;->a(Ljava/lang/Object;)V

    invoke-virtual {v3}, Lbvn;->q()V

    .line 706
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->u()Z

    move-result v0

    .line 708
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->hasWindowFocus()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 709
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    iget-object v4, v4, Lcxb;->a:Lgfa;

    invoke-virtual {v3, v4}, Lcws;->a(Lgec;)V

    .line 710
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Z

    .line 717
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ae:Lgom;

    if-eqz v3, :cond_7

    .line 719
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ae:Lgom;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lgom;)V

    .line 723
    :cond_2
    :goto_3
    iput-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aW:Ldlf;

    .line 725
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    iget-boolean v3, v0, Layx;->e:Z

    if-nez v3, :cond_3

    iget-object v3, v0, Layx;->b:Layt;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, v3, Layt;->e:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x7530

    cmp-long v4, v4, v6

    if-gez v4, :cond_8

    :goto_4
    if-eqz v1, :cond_3

    sget-object v1, Layv;->a:[I

    iget v4, v3, Layt;->d:I

    add-int/lit8 v4, v4, -0x1

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    :cond_3
    :goto_5
    iput-boolean v2, v0, Layx;->e:Z

    .line 726
    return-void

    .line 699
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    invoke-virtual {v0}, Lbvn;->o()V

    .line 700
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcwg;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->au:Lcwj;

    iput-object v3, v0, Lcwg;->d:Lcwj;

    goto :goto_0

    :cond_5
    move v0, v2

    .line 702
    goto :goto_1

    .line 714
    :cond_6
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Z

    goto :goto_2

    .line 720
    :cond_7
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aW:Ldlf;

    if-eqz v0, :cond_2

    .line 721
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aW:Ldlf;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Ldlf;)V

    goto :goto_3

    :cond_8
    move v1, v2

    .line 725
    goto :goto_4

    :pswitch_0
    iget-object v1, v3, Layt;->b:Layy;

    invoke-virtual {v1}, Layy;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v3, Layt;->i:Landroid/app/AlertDialog;

    if-nez v1, :cond_9

    new-instance v1, Layu;

    invoke-direct {v1, v3}, Layu;-><init>(Layt;)V

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, v3, Layt;->a:Landroid/app/Activity;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0901c0

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0901b3

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0901f2

    invoke-virtual {v4, v5, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v4, 0x7f0900a1

    invoke-virtual {v1, v4, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, v3, Layt;->i:Landroid/app/AlertDialog;

    :cond_9
    iget-object v1, v3, Layt;->i:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    iget-object v1, v3, Layt;->b:Layy;

    invoke-virtual {v1}, Layy;->c()V

    goto :goto_5

    :pswitch_1
    iget-object v1, v3, Layt;->f:Lhbg;

    if-eqz v1, :cond_3

    iget-object v1, v3, Layt;->g:Lfki;

    if-nez v1, :cond_a

    iget-object v1, v3, Layt;->f:Lhbg;

    iget-object v1, v1, Lhbg;->a:Lhod;

    if-eqz v1, :cond_b

    new-instance v1, Lfla;

    iget-object v4, v3, Layt;->f:Lhbg;

    iget-object v4, v4, Lhbg;->a:Lhod;

    invoke-direct {v1, v4, v8}, Lfla;-><init>(Lhod;Lfqh;)V

    iput-object v1, v3, Layt;->g:Lfki;

    :cond_a
    :goto_6
    iget-object v1, v3, Layt;->c:Lbyg;

    iget-object v4, v3, Layt;->g:Lfki;

    iget-object v3, v3, Layt;->h:Lfrz;

    invoke-virtual {v1, v4, v3}, Lbyg;->a(Lfki;Lfrz;)V

    goto/16 :goto_5

    :cond_b
    iget-object v1, v3, Layt;->f:Lhbg;

    iget-object v1, v1, Lhbg;->b:Lhfz;

    if-eqz v1, :cond_a

    new-instance v1, Lfjt;

    iget-object v4, v3, Layt;->f:Lhbg;

    iget-object v4, v4, Lhbg;->b:Lhfz;

    invoke-direct {v1, v4, v8}, Lfjt;-><init>(Lhfz;Lfqh;)V

    iput-object v1, v3, Layt;->g:Lfki;

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final u()V
    .locals 14

    .prologue
    const/high16 v13, 0x8000000

    const/high16 v12, 0x4000000

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 730
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcwg;

    iput-object v1, v0, Lcwg;->d:Lcwj;

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aR:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    .line 732
    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 733
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 734
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Ldbo;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 735
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ak:Ldfw;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 736
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ag:Ldbv;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 737
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aj:Lddd;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 738
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Ldep;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 739
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Ldfh;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 740
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->am:Ldfn;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 741
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aV:Lcvu;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 742
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->an:Ldfk;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 743
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ao:Ldcz;

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 745
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v4}, Lari;->f()Larh;

    move-result-object v4

    invoke-virtual {v4}, Larh;->ac()Lfko;

    move-result-object v4

    iget-boolean v4, v4, Lfko;->b:Z

    if-eqz v4, :cond_0

    .line 746
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->G()Lbmt;

    move-result-object v4

    invoke-virtual {v0, v4}, Levn;->b(Ljava/lang/Object;)V

    .line 749
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Z

    .line 750
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    if-nez v0, :cond_5

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    invoke-virtual {v0}, Layx;->a()V

    .line 751
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->isFinishing()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcws;->b(Z)V

    .line 753
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v0, :cond_3

    .line 754
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iget-boolean v1, v0, Lbvn;->c:Z

    const-string v2, "cannot call onPause() multiple times, need to call onResume() first"

    invoke-static {v1, v2}, Lb;->d(ZLjava/lang/Object;)V

    iput-boolean v3, v0, Lbvn;->c:Z

    iget-object v1, v0, Lbvn;->b:Ldwq;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lbvn;->a:Levn;

    invoke-virtual {v1, v0}, Levn;->b(Ljava/lang/Object;)V

    .line 756
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aQ:Lari;

    invoke-virtual {v0}, Lari;->U()Laxk;

    move-result-object v0

    invoke-interface {v0}, Laxk;->b()V

    .line 757
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Ldfh;

    iget-object v1, v0, Ldfh;->b:Lfoo;

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Ldfh;->c()V

    .line 759
    :cond_4
    invoke-super {p0}, Lj;->u()V

    .line 760
    return-void

    .line 750
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Layy;

    invoke-virtual {v0}, Layy;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    invoke-virtual {v0}, Lfrl;->g()Lflo;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    invoke-virtual {v0}, Lfrl;->g()Lflo;

    move-result-object v0

    iget-boolean v0, v0, Lflo;->c:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    iget-object v0, v0, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->b(Lhro;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    :goto_1
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->v()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    invoke-virtual {v0}, Layx;->a()V

    iget-object v4, v0, Layx;->a:Layy;

    invoke-virtual {v4}, Layy;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v0, Layx;->b:Layt;

    const/4 v5, 0x2

    iput v5, v4, Layt;->d:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v4, Layt;->e:J

    invoke-virtual {v4}, Layt;->a()V

    iget-object v0, v0, Layx;->c:Layr;

    invoke-virtual {v0}, Layr;->a()V

    iput-boolean v2, v0, Layr;->i:Z

    iget-object v4, v0, Layr;->h:Lba;

    if-nez v4, :cond_6

    new-instance v4, Lba;

    iget-object v5, v0, Layr;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, Lba;-><init>(Landroid/content/Context;)V

    iput-object v4, v0, Layr;->h:Lba;

    new-instance v4, Landroid/content/Intent;

    iget-object v5, v0, Layr;->a:Landroid/content/Context;

    const-class v6, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    iget-object v6, v0, Layr;->a:Landroid/content/Context;

    const-class v7, Lcom/google/android/apps/youtube/app/honeycomb/SettingsActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v5

    const-string v6, ":android:show_fragment"

    const-class v7, Lcom/google/android/apps/youtube/app/honeycomb/SettingsActivity$OfflinePrefsFragment;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    const-string v6, ":android:no_headers"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "background_settings"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    new-instance v6, Laz;

    invoke-direct {v6}, Laz;-><init>()V

    iget-object v7, v0, Layr;->b:Landroid/content/res/Resources;

    const v8, 0x7f0901b3

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Laz;->a(Ljava/lang/CharSequence;)Laz;

    move-result-object v6

    iget-object v7, v0, Layr;->h:Lba;

    iget-object v8, v0, Layr;->b:Landroid/content/res/Resources;

    const v9, 0x7f0901c0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lba;->a(Ljava/lang/CharSequence;)Lba;

    move-result-object v7

    iget-object v8, v0, Layr;->b:Landroid/content/res/Resources;

    const v9, 0x7f0901b3

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    move-result-object v7

    iget-object v8, v0, Layr;->b:Landroid/content/res/Resources;

    const v9, 0x7f0901b2

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lba;->d(Ljava/lang/CharSequence;)Lba;

    move-result-object v7

    invoke-virtual {v7, v1}, Lba;->c(Ljava/lang/CharSequence;)Lba;

    move-result-object v1

    const v7, 0x7f020196

    invoke-virtual {v1, v7}, Lba;->a(I)Lba;

    move-result-object v1

    invoke-virtual {v1, v3}, Lba;->a(Z)Lba;

    move-result-object v1

    invoke-virtual {v1, v2}, Lba;->b(Z)Lba;

    move-result-object v1

    invoke-virtual {v1, v6}, Lba;->a(Lbk;)Lba;

    move-result-object v1

    iget-object v6, v0, Layr;->b:Landroid/content/res/Resources;

    const v7, 0x7f0700b3

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v1, v6}, Lba;->b(I)Lba;

    move-result-object v1

    iget-object v6, v0, Layr;->a:Landroid/content/Context;

    invoke-static {v6, v2, v4, v13}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v1, v4}, Lba;->a(Landroid/app/PendingIntent;)Lba;

    move-result-object v1

    const v4, 0x7f02014d

    iget-object v6, v0, Layr;->b:Landroid/content/res/Resources;

    const v7, 0x7f0901b4

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Layr;->a:Landroid/content/Context;

    const/4 v8, 0x2

    invoke-static {v7, v8, v5, v13}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v1, v4, v6, v5}, Lba;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lba;

    move-result-object v1

    invoke-virtual {v1, v2}, Lba;->c(I)Lba;

    :cond_6
    iget-object v1, v0, Layr;->h:Lba;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lba;->a(J)Lba;

    iget-object v1, v0, Layr;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3ed

    iget-object v0, v0, Layr;->h:Lba;

    invoke-virtual {v0}, Lba;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    invoke-virtual {v0}, Lfrl;->g()Lflo;

    move-result-object v0

    iget-boolean v4, v0, Lflo;->c:Z

    if-nez v4, :cond_a

    iget-object v4, v0, Lflo;->a:Lhqn;

    iget-object v4, v4, Lhqn;->e:Lhbh;

    if-eqz v4, :cond_a

    iget-object v4, v0, Lflo;->a:Lhqn;

    iget-object v4, v4, Lhqn;->e:Lhbh;

    iget-object v4, v4, Lhbh;->a:Lhbf;

    if-eqz v4, :cond_a

    iget-object v0, v0, Lflo;->a:Lhqn;

    iget-object v0, v0, Lhqn;->e:Lhbh;

    iget-object v0, v0, Lhbh;->a:Lhbf;

    iget-object v0, v0, Lhbf;->b:Lhbg;

    :goto_2
    if-eqz v0, :cond_e

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    iget-object v5, v5, Lfrl;->a:Lhro;

    invoke-static {v5}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    invoke-virtual {v6}, Lfrl;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aI:Lfrl;

    invoke-virtual {v7}, Lfrl;->b()Lfnc;

    move-result-object v7

    iget-object v8, v4, Layx;->b:Layt;

    iget-object v9, v4, Layx;->d:Lfrz;

    const/4 v10, 0x3

    iput v10, v8, Layt;->d:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iput-wide v10, v8, Layt;->e:J

    iget-object v10, v8, Layt;->f:Lhbg;

    if-eq v0, v10, :cond_9

    invoke-virtual {v8}, Layt;->a()V

    iput-object v0, v8, Layt;->f:Lhbg;

    :cond_9
    iput-object v9, v8, Layt;->h:Lfrz;

    iget-object v4, v4, Layx;->c:Layr;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v5, v4, Layr;->e:Ljava/lang/String;

    invoke-virtual {v4}, Layr;->a()V

    iput-boolean v2, v4, Layr;->i:Z

    iget-object v8, v0, Lhbg;->b:Lhfz;

    if-nez v8, :cond_b

    const-string v0, "background message doesn\'t contain dismissable_dialog_renderer"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    move-object v0, v1

    goto :goto_2

    :cond_b
    iget-object v8, v4, Layr;->g:Lba;

    if-nez v8, :cond_c

    new-instance v8, Lba;

    iget-object v9, v4, Layr;->a:Landroid/content/Context;

    invoke-direct {v8, v9}, Lba;-><init>(Landroid/content/Context;)V

    iput-object v8, v4, Layr;->g:Lba;

    new-instance v8, Landroid/content/Intent;

    iget-object v9, v4, Layr;->a:Landroid/content/Context;

    const-class v10, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v8, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "background_failed"

    invoke-virtual {v8, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v8

    iget-object v9, v4, Layr;->g:Lba;

    invoke-virtual {v9, v1}, Lba;->c(Ljava/lang/CharSequence;)Lba;

    move-result-object v1

    const v9, 0x7f02014a

    invoke-virtual {v1, v9}, Lba;->a(I)Lba;

    move-result-object v1

    invoke-virtual {v1, v3}, Lba;->a(Z)Lba;

    move-result-object v1

    invoke-virtual {v1, v2}, Lba;->b(Z)Lba;

    move-result-object v1

    iget-object v9, v4, Layr;->b:Landroid/content/res/Resources;

    const v10, 0x7f0700b3

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v1, v9}, Lba;->b(I)Lba;

    move-result-object v1

    iget-object v9, v4, Layr;->a:Landroid/content/Context;

    invoke-static {v9, v3, v8, v13}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v1, v8}, Lba;->a(Landroid/app/PendingIntent;)Lba;

    move-result-object v1

    invoke-virtual {v1, v2}, Lba;->c(I)Lba;

    :cond_c
    new-instance v1, Laz;

    invoke-direct {v1}, Laz;-><init>()V

    iget-object v2, v0, Lhbg;->b:Lhfz;

    iget-object v2, v2, Lhfz;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Laz;->a(Ljava/lang/CharSequence;)Laz;

    move-result-object v1

    iget-object v2, v4, Layr;->g:Lba;

    iget-object v8, v0, Lhbg;->b:Lhfz;

    iget-object v8, v8, Lhfz;->a:Ljava/lang/String;

    invoke-virtual {v2, v8}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    move-result-object v2

    invoke-virtual {v2, v6}, Lba;->a(Ljava/lang/CharSequence;)Lba;

    move-result-object v2

    iget-object v0, v0, Lhbg;->b:Lhfz;

    iget-object v0, v0, Lhfz;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lba;->d(Ljava/lang/CharSequence;)Lba;

    move-result-object v0

    invoke-virtual {v0, v1}, Lba;->a(Lbk;)Lba;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lba;->a(J)Lba;

    iget-object v0, v4, Layr;->f:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, v4, Layr;->g:Lba;

    iget-object v1, v4, Layr;->b:Landroid/content/res/Resources;

    const v2, 0x7f020149

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lba;->a(Landroid/graphics/Bitmap;)Lba;

    :cond_d
    iget-object v0, v4, Layr;->d:Landroid/app/NotificationManager;

    const/16 v1, 0x3ed

    iget-object v2, v4, Layr;->g:Lba;

    invoke-virtual {v2}, Lba;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object v0, v4, Layr;->e:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v1, v4, Layr;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v4}, Layr;->b()I

    move-result v1

    invoke-virtual {v7, v1, v1}, Lfnc;->a(II)Lfnb;

    move-result-object v1

    iget-object v2, v4, Layr;->c:Leyp;

    iget-object v1, v1, Lfnb;->a:Landroid/net/Uri;

    new-instance v5, Lays;

    invoke-direct {v5, v4, v0}, Lays;-><init>(Layr;Ljava/lang/String;)V

    invoke-interface {v2, v1, v5}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    invoke-virtual {v0}, Layx;->a()V

    goto/16 :goto_0
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcws;->f(Z)Ldlf;

    move-result-object v0

    .line 432
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Ldlf;)V

    .line 436
    :cond_0
    return-void
.end method
