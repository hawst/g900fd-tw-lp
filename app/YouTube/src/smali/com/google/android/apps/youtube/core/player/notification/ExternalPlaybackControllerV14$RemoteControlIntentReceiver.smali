.class public Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14$RemoteControlIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 230
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 234
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Ldbj;

    if-eqz v0, :cond_0

    const-string v0, "android.intent.action.MEDIA_BUTTON"

    .line 235
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.extra.KEY_EVENT"

    .line 236
    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    const-string v0, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 239
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 240
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 242
    sparse-switch v0, :sswitch_data_0

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 244
    :sswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->b()V

    goto :goto_0

    .line 247
    :sswitch_1
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->c()V

    goto :goto_0

    .line 251
    :sswitch_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->d()V

    goto :goto_0

    .line 254
    :sswitch_3
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->f()V

    goto :goto_0

    .line 257
    :sswitch_4
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->e()V

    goto :goto_0

    .line 260
    :sswitch_5
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Ldbj;

    invoke-interface {v0}, Ldbj;->g()V

    goto :goto_0

    .line 242
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_2
        0x55 -> :sswitch_2
        0x56 -> :sswitch_5
        0x57 -> :sswitch_3
        0x58 -> :sswitch_4
        0x7e -> :sswitch_0
        0x7f -> :sswitch_1
    .end sparse-switch
.end method
