.class public Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;
.super Landroid/preference/Preference;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->a:Landroid/content/Context;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->b:Z

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->a:Landroid/content/Context;

    .line 42
    sget-object v0, Lgvk;->k:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 43
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->b:Z

    .line 44
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->a:Landroid/content/Context;

    .line 50
    sget-object v0, Lgvk;->k:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->b:Z

    .line 52
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 53
    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 57
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 71
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lckz;

    invoke-virtual {v0}, Lckz;->n()Lcla;

    move-result-object v6

    .line 73
    invoke-virtual {v6}, Lcla;->aD()Lcst;

    move-result-object v0

    .line 74
    invoke-interface {v0}, Lgix;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 75
    invoke-virtual {v6}, Lcla;->O()Lgng;

    move-result-object v1

    .line 77
    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    .line 78
    invoke-interface {v0}, Lgnd;->f()Lgmk;

    move-result-object v0

    .line 79
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->b:Z

    if-eqz v1, :cond_0

    .line 80
    invoke-interface {v0}, Lgmk;->e()Legc;

    move-result-object v0

    .line 84
    :goto_0
    if-nez v0, :cond_1

    move-wide v2, v4

    .line 85
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->b:Z

    if-eqz v0, :cond_3

    .line 86
    invoke-virtual {v6}, Lcla;->aQ()Lexn;

    move-result-object v0

    invoke-virtual {v0}, Lexn;->b()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    invoke-static {v4, v5}, Lfaq;->a(J)J

    move-result-wide v0

    move-wide v4, v0

    .line 89
    :goto_3
    const v0, 0x7f08027e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 90
    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 91
    const/high16 v1, 0x447a0000    # 1000.0f

    long-to-float v6, v2

    mul-float/2addr v1, v6

    long-to-float v6, v2

    long-to-float v7, v4

    add-float/2addr v6, v7

    div-float/2addr v1, v6

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 92
    const v0, 0x7f080280

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 93
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f0901c2

    new-array v7, v9, [Ljava/lang/Object;

    .line 94
    invoke-static {v2, v3}, La;->b(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v8

    .line 93
    invoke-virtual {v1, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    const v0, 0x7f080282

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/StorageBarPreference;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901c3

    new-array v3, v9, [Ljava/lang/Object;

    .line 97
    invoke-static {v4, v5}, La;->b(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    return-void

    .line 81
    :cond_0
    invoke-interface {v0}, Lgmk;->d()Legc;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_1
    invoke-interface {v0}, Legc;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lfaq;->a(J)J

    move-result-wide v0

    move-wide v2, v0

    goto :goto_1

    .line 86
    :cond_2
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Lexn;->c()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v4, v0

    goto/16 :goto_2

    .line 87
    :cond_3
    invoke-static {}, La;->L()J

    move-result-wide v0

    invoke-static {v0, v1}, Lfaq;->a(J)J

    move-result-wide v0

    move-wide v4, v0

    goto/16 :goto_3

    :cond_4
    move-object v0, v1

    goto/16 :goto_0
.end method
