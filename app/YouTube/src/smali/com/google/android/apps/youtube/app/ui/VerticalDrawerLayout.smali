.class public Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/util/Set;

.field private final c:Lbqx;

.field private final d:I

.field private e:Landroid/widget/Scroller;

.field private f:Z

.field private g:Z

.field private h:I

.field private i:Landroid/view/View;

.field private j:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    new-instance v0, Landroid/widget/Scroller;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->e:Landroid/widget/Scroller;

    .line 57
    new-instance v0, Lbqx;

    invoke-direct {v0, p1}, Lbqx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->c:Lbqx;

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->f:Z

    .line 60
    sget-object v0, Lgvk;->l:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 61
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->d:I

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->b:Ljava/util/Set;

    .line 63
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 247
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    .line 248
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 249
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 250
    int-to-float v2, p1

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 252
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->c()V

    .line 253
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 126
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 127
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->i:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 257
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    sub-int v3, p1, v0

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->e:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->e:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    const/16 v5, 0xc8

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->invalidate()V

    .line 264
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->j:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 244
    :goto_0
    return-void

    .line 238
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 239
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    if-ge v1, v0, :cond_1

    .line 240
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->j:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    mul-int/lit16 v2, v2, 0xff

    div-int v0, v2, v0

    rsub-int v0, v0, 0xff

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->j:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->b(I)V

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a:Z

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyl;

    .line 208
    invoke-interface {v0}, Lbyl;->b()V

    goto :goto_0

    .line 210
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 213
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->b(I)V

    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a:Z

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyl;

    .line 216
    invoke-interface {v0}, Lbyl;->a()V

    goto :goto_0

    .line 218
    :cond_0
    return-void
.end method

.method public computeScroll()V
    .locals 1

    .prologue
    .line 185
    invoke-super {p0}, Landroid/view/ViewGroup;->computeScroll()V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->e:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->e:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a(I)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->postInvalidate()V

    .line 190
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->j:Landroid/graphics/drawable/Drawable;

    .line 69
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->i:Landroid/view/View;

    .line 70
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->g:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 98
    sub-int v0, p5, p3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    .line 100
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->f:Z

    if-eqz v2, :cond_0

    .line 101
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->f:Z

    .line 102
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->i:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->c()V

    :cond_0
    move v0, v1

    .line 106
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 107
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 108
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 109
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 110
    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 111
    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 74
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getMeasuredWidth()I

    move-result v0

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getMeasuredHeight()I

    move-result v1

    .line 81
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 82
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 84
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 85
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 86
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getChildMeasureSpec(III)I

    move-result v5

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getPaddingBottom()I

    move-result v7

    add-int/2addr v6, v7

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v1, v6, v4}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getChildMeasureSpec(III)I

    move-result v4

    .line 92
    invoke-virtual {v3, v5, v4}, Landroid/view/View;->measure(II)V

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 119
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a:Z

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v0, p2, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    .line 121
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->b(I)V

    .line 123
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 137
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 139
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->g:Z

    if-nez v2, :cond_0

    .line 180
    :goto_0
    return v0

    .line 144
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->i:Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 146
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->c:Lbqx;

    invoke-virtual {v2, p1}, Lbqx;->a(Landroid/view/MotionEvent;)V

    .line 148
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v1

    .line 180
    goto :goto_0

    .line 150
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->e:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->c:Lbqx;

    invoke-virtual {v0, p1}, Lbqx;->b(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 155
    :pswitch_2
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->g:Z

    .line 156
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->c:Lbqx;

    invoke-virtual {v2, p1}, Lbqx;->d(Landroid/view/MotionEvent;)I

    move-result v2

    .line 157
    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    sub-int v2, v3, v2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a(I)V

    goto :goto_1

    .line 162
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->c:Lbqx;

    .line 163
    invoke-virtual {v2, p1, v3, v1}, Lbqx;->a(Landroid/view/MotionEvent;IZ)I

    move-result v2

    .line 164
    if-ne v2, v1, :cond_1

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a()V

    .line 171
    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->g:Z

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->c:Lbqx;

    const/4 v2, -0x1

    iput v2, v0, Lbqx;->d:I

    goto :goto_1

    .line 166
    :cond_1
    if-ne v2, v3, :cond_2

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->b()V

    goto :goto_2

    .line 169
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->h:I

    div-int/lit8 v2, v2, 0x2

    if-le v3, v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a()V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->b()V

    goto :goto_2

    .line 176
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->c:Lbqx;

    invoke-virtual {v0, p1}, Lbqx;->e(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 148
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
