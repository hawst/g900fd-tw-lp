.class public Lcom/google/android/apps/youtube/core/player/preload/PreloadVideosTransferService;
.super Ldmu;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ldmu;-><init>()V

    .line 120
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/google/android/apps/youtube/core/player/preload/PreloadVideosTransferService;

    invoke-static {p0, v0}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Lfad;
    .locals 2

    .prologue
    .line 88
    const-class v0, Lcom/google/android/apps/youtube/core/player/preload/PreloadVideosTransferService;

    new-instance v1, Ldhw;

    invoke-direct {v1}, Ldhw;-><init>()V

    invoke-static {p0, v0, v1}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;Ldmw;)Lfad;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lgjm;Ldnb;)Ldna;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/preload/PreloadVideosTransferService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcwy;

    invoke-interface {v0}, Lcwy;->o()Lezg;

    move-result-object v0

    .line 38
    invoke-interface {v0}, Lezg;->W()Ldhi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ldhi;->a(Lgjm;Ldnb;)Ldhh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lgjm;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "preload_videos_tasks.db"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "preload_videos_network_policy_string"

    return-object v0
.end method

.method protected final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "preload_videos_charging_only"

    return-object v0
.end method

.method protected final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final i()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method protected final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "Creating PreloadVideosTransferService..."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 32
    invoke-super {p0}, Ldmu;->onCreate()V

    .line 33
    return-void
.end method
