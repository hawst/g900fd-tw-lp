.class public Lcom/google/android/apps/youtube/app/notification/GcmBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 145
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 38
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lari;->aO()Lcub;

    move-result-object v1

    .line 40
    invoke-virtual {v0}, Lari;->aD()Lcst;

    move-result-object v0

    .line 43
    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    const-string v0, "youtube"

    .line 50
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 53
    :try_start_0
    new-instance v5, Lhog;

    invoke-direct {v5}, Lhog;-><init>()V

    const-string v3, "n"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-static {v5, v3}, Lidh;->a(Lidh;[B)Lidh;

    .line 55
    :cond_2
    invoke-static {v5}, Lfia;->a(Lhog;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 56
    iget-object v3, v5, Lhog;->v:Lhvv;

    if-eqz v3, :cond_3

    .line 58
    const-string v0, "Sign out notification received"

    invoke-virtual {v1, v0}, Lcub;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    const-string v1, "Could not convert base64-encoded byte stream into NavigationEndpoint proto: "

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 59
    :cond_3
    :try_start_1
    invoke-static {v0}, La;->e(Landroid/content/SharedPreferences;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const-string v0, "t"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f090327

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_4
    const-string v0, "sm"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f090328

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :cond_5
    const-string v0, "i"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/high16 v0, 0x7f030000

    invoke-static {v6, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v2, v3, v4, v0, v5}, La;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lhog;)V
    :try_end_1
    .catch Lidg; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    const-string v1, "Could not convert base64-encoded byte stream into NavigationEndpoint proto: "

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 60
    :cond_6
    :try_start_2
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v1

    invoke-virtual {v1}, Lari;->c()Leyp;

    move-result-object v7

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    new-instance v0, Lbjk;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lbjk;-><init>(Lcom/google/android/apps/youtube/app/notification/GcmBroadcastReceiver;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lhog;Landroid/content/res/Resources;)V

    invoke-interface {v7, v8, v0}, Leyp;->a(Landroid/net/Uri;Leuc;)V
    :try_end_2
    .catch Lidg; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method
