.class public Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;
.super Landroid/view/View;
.source "SourceFile"


# static fields
.field private static final j:[I


# instance fields
.field private A:I

.field private B:I

.field private C:Ljava/lang/String;

.field private D:I

.field private final E:Landroid/graphics/Rect;

.field public a:Ldfp;

.field public final b:Landroid/graphics/Paint;

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:I

.field public h:I

.field public i:Ljava/util/Map;

.field private final k:Landroid/util/DisplayMetrics;

.field private final l:Landroid/graphics/Rect;

.field private final m:Landroid/graphics/Rect;

.field private final n:Landroid/graphics/Rect;

.field private final o:Landroid/graphics/Rect;

.field private final p:Landroid/graphics/Paint;

.field private final q:Landroid/graphics/Paint;

.field private final r:Landroid/graphics/Paint;

.field private final s:Landroid/graphics/Paint;

.field private final t:Landroid/graphics/drawable/StateListDrawable;

.field private final u:I

.field private final v:I

.field private final w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, -0x101009e

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->j:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    .line 107
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->k:Landroid/util/DisplayMetrics;

    .line 110
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->d:Z

    .line 111
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->e:Z

    .line 112
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->f:Z

    .line 114
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    .line 115
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->m:Landroid/graphics/Rect;

    .line 116
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->n:Landroid/graphics/Rect;

    .line 117
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->o:Landroid/graphics/Rect;

    .line 119
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->p:Landroid/graphics/Paint;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->p:Landroid/graphics/Paint;

    const-string v1, "#B2212121"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 121
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->q:Landroid/graphics/Paint;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->q:Landroid/graphics/Paint;

    const-string v1, "#B2777777"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 123
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->b:Landroid/graphics/Paint;

    .line 124
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->s:Landroid/graphics/Paint;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->s:Landroid/graphics/Paint;

    const-string v1, "#B2FFFF00"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->k:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41400000    # 12.0f

    mul-float/2addr v0, v1

    .line 128
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->E:Landroid/graphics/Rect;

    .line 130
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    sget-object v2, Lezd;->b:Lezd;

    invoke-virtual {v2, p1}, Lezd;->a(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 132
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    const/high16 v2, 0x40c00000    # 6.0f

    const-string v3, "#50000000"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v4, v4, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    const-string v1, "0:00:00"

    const/4 v2, 0x0

    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->E:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 138
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->C:Ljava/lang/String;

    .line 140
    const v0, 0x7f02025b

    invoke-static {p1, v0}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->k:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41500000    # 13.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->u:I

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->k:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->v:I

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->k:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, -0x3db80000    # -50.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->w:I

    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->e()V

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldfp;)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 100
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfp;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a:Ldfp;

    .line 101
    return-void
.end method

.method private a(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 463
    const/4 v0, 0x3

    .line 464
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    const v2, 0x36ee80

    if-lt v1, v2, :cond_1

    .line 465
    const/4 v0, 0x5

    .line 469
    :cond_0
    :goto_0
    long-to-int v1, p1

    div-int/lit16 v1, v1, 0x3e8

    invoke-static {v1, v0}, La;->b(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 466
    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    const v2, 0xea60

    if-lt v1, v2, :cond_0

    .line 467
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private a(F)V
    .locals 4

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 448
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v0

    .line 449
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v0

    .line 450
    float-to-int v3, p1

    sub-int v0, v3, v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->y:I

    .line 451
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->y:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->y:I

    .line 452
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->g()V

    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    .line 194
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->PRESSED_ENABLED_FOCUSED_STATE_SET:[I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    goto :goto_0

    .line 192
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->ENABLED_STATE_SET:[I

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->j:[I

    goto :goto_1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->E:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->v:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->x:I

    .line 225
    return-void
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 455
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    .line 456
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 457
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->d()V

    .line 458
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->invalidate()V

    .line 459
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->m:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->n:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->o:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 160
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->g:I

    .line 162
    :goto_0
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    if-lez v1, :cond_1

    .line 163
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    .line 164
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-long v2, v1

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->h:I

    int-to-long v4, v1

    mul-long/2addr v2, v4

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    int-to-long v4, v1

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 165
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->m:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v3

    iput v1, v2, Landroid/graphics/Rect;->right:I

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    .line 168
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-long v2, v1

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->B:I

    int-to-long v4, v1

    mul-long/2addr v2, v4

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    int-to-long v4, v1

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 169
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->n:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v3

    iput v1, v2, Landroid/graphics/Rect;->right:I

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    .line 172
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-long v2, v1

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->y:I

    .line 180
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->invalidate()V

    .line 181
    return-void

    .line 160
    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->B:I

    goto :goto_0

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->m:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->n:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->y:I

    goto :goto_1
.end method

.method public final a(III)V
    .locals 5

    .prologue
    .line 208
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->B:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->h:I

    if-eq v0, p3, :cond_2

    .line 210
    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    if-eq v0, p2, :cond_1

    .line 211
    iput p2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    .line 212
    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->C:Ljava/lang/String;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->C:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->C:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->E:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 214
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->e()V

    .line 215
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->d()V

    .line 217
    :cond_1
    iput p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->B:I

    .line 218
    iput p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->h:I

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    .line 221
    :cond_2
    return-void
.end method

.method public b()I
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-gtz v0, :cond_0

    .line 279
    const/4 v0, 0x0

    .line 282
    :goto_0
    return v0

    .line 281
    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->y:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    .line 282
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 286
    const/high16 v0, 0x42200000    # 40.0f

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->k:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    .line 314
    invoke-static {}, Lezp;->a()V

    .line 315
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 319
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->f:Z

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->m:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->q:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->n:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 325
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->e:Z

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->y:I

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->z:I

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->y:I

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    .line 329
    invoke-virtual {v4}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->z:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    .line 330
    invoke-virtual {v5}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 326
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 334
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 335
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->g:I

    int-to-long v0, v0

    .line 336
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->x:I

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x7

    int-to-float v1, v1

    .line 338
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->E:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    .line 335
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->C:Ljava/lang/String;

    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->x:I

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x7

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 343
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->E:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->r:Landroid/graphics/Paint;

    .line 340
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 347
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->i:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    if-lez v0, :cond_4

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->i:Ljava/util/Map;

    sget-object v1, Ldfs;->a:Ldfs;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldfq;

    .line 349
    if-eqz v0, :cond_4

    .line 350
    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    .line 354
    iget v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    int-to-long v4, v4

    const-wide/16 v6, 0x0

    iget-wide v8, v3, Ldfq;->a:J

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 355
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-long v6, v3

    mul-long/2addr v4, v6

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->A:I

    int-to-long v6, v3

    div-long/2addr v4, v6

    const-wide/16 v6, 0x2

    sub-long/2addr v4, v6

    long-to-int v3, v4

    .line 356
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->o:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v5

    iput v3, v4, Landroid/graphics/Rect;->left:I

    .line 357
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->o:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->o:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/lit8 v4, v4, 0x4

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 358
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->o:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->s:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 352
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 335
    :cond_3
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->B:I

    int-to-long v0, v0

    goto/16 :goto_0

    .line 362
    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v4, 0x40800000    # 4.0f

    const/4 v3, 0x0

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->k:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    .line 292
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->f()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->e:Z

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c()I

    move-result v0

    .line 294
    :cond_1
    invoke-static {v3, p1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->getDefaultSize(II)I

    move-result v1

    .line 295
    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->resolveSize(II)I

    move-result v0

    .line 296
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setMeasuredDimension(II)V

    .line 298
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->f()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->e:Z

    if-nez v2, :cond_2

    .line 299
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    invoke-virtual {v2, v3, v3, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 309
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    .line 310
    return-void

    .line 301
    :cond_2
    div-int/lit8 v2, v0, 0x2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->z:I

    .line 303
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->k:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    .line 304
    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v3, v2, 0x2

    sub-int/2addr v0, v3

    .line 305
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->getPaddingLeft()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->x:I

    add-int/2addr v4, v5

    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->getPaddingRight()I

    move-result v5

    sub-int/2addr v1, v5

    iget v5, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->x:I

    sub-int/2addr v1, v5

    add-int/2addr v2, v0

    .line 305
    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 367
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 368
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v3, v2

    .line 369
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 371
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 417
    :cond_1
    :goto_0
    return v0

    .line 373
    :pswitch_0
    int-to-float v4, v3

    int-to-float v2, v2

    iget v5, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->z:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->t:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v8

    add-int/2addr v7, v8

    int-to-float v6, v6

    cmpg-float v6, v6, v4

    if-gez v6, :cond_3

    int-to-float v6, v7

    cmpg-float v4, v4, v6

    if-gez v4, :cond_3

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->z:I

    iget v6, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->u:I

    sub-int/2addr v4, v6

    int-to-float v4, v4

    cmpg-float v4, v4, v2

    if-gez v4, :cond_3

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->u:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v2, v2, v4

    if-gez v2, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_0

    .line 374
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 376
    int-to-float v1, v3

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a(F)V

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->b()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->g:I

    .line 378
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a:Ldfp;

    if-eqz v1, :cond_2

    .line 379
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a:Ldfp;

    invoke-interface {v1}, Ldfp;->a()V

    .line 381
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->d()V

    .line 382
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    .line 383
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->invalidate()V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 373
    goto :goto_1

    .line 388
    :pswitch_1
    iget-boolean v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    if-eqz v4, :cond_0

    .line 389
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->w:I

    if-ge v2, v1, :cond_4

    .line 391
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->D:I

    sub-int v1, v3, v1

    .line 392
    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->D:I

    div-int/lit8 v1, v1, 0x3

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a(F)V

    .line 398
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->b()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->g:I

    .line 399
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    .line 400
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->invalidate()V

    goto/16 :goto_0

    .line 395
    :cond_4
    iput v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->D:I

    .line 396
    int-to-float v1, v3

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a(F)V

    goto :goto_2

    .line 406
    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    if-eqz v2, :cond_0

    .line 407
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->g()V

    .line 408
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->g:I

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->B:I

    .line 409
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a:Ldfp;

    if-eqz v1, :cond_1

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a:Ldfp;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->b()I

    move-result v2

    invoke-interface {v1, v2}, Ldfp;->a(I)V

    goto/16 :goto_0

    .line 371
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setEnabled(Z)V
    .locals 0

    .prologue
    .line 198
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 199
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->d()V

    .line 200
    return-void
.end method
