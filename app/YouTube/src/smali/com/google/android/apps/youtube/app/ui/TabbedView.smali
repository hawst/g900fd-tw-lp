.class public Lcom/google/android/apps/youtube/app/ui/TabbedView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# static fields
.field private static final m:I


# instance fields
.field private A:Landroid/graphics/Rect;

.field private B:Landroid/graphics/Paint;

.field public a:Landroid/content/Context;

.field public b:Ljava/util/ArrayList;

.field public c:Landroid/widget/HorizontalScrollView;

.field public d:Landroid/widget/LinearLayout;

.field public e:Lbqk;

.field public f:I

.field public g:Ljava/util/List;

.field public h:Ljava/util/List;

.field public i:I

.field public j:Lbxm;

.field public k:I

.field public l:F

.field private n:Landroid/support/v4/view/ViewPager;

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:Landroid/content/res/ColorStateList;

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const v0, 0x7f0700ef

    sput v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->m:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 108
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Landroid/content/Context;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Landroid/content/Context;)V

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 118
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Landroid/content/Context;)V

    .line 119
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/TabbedView;F)F
    .locals 0

    .prologue
    .line 36
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->l:F

    return p1
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/TabbedView;I)I
    .locals 0

    .prologue
    .line 36
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->k:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x2

    const/16 v8, 0xa

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 122
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 126
    const/16 v2, 0x30

    invoke-static {v1, v2}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v2

    .line 128
    const v3, 0x7f0b0023

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 129
    new-instance v4, Lbqk;

    const v5, -0x19dee9

    .line 130
    invoke-static {v5, v7}, Lcaj;->a(II)Lcaj;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Lbqk;-><init>(Lbqm;I)V

    iput-object v4, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->e:Lbqk;

    .line 132
    const/4 v3, 0x4

    invoke-static {v1, v3}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->p:I

    .line 133
    const/high16 v3, 0x50000000

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->q:I

    .line 134
    const/16 v3, 0x30

    invoke-static {v1, v3}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->o:I

    .line 135
    invoke-static {v1, v6}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->r:I

    .line 136
    const/high16 v3, 0x33000000

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->s:I

    .line 137
    sget v3, Lcom/google/android/apps/youtube/app/ui/TabbedView;->m:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->t:Landroid/content/res/ColorStateList;

    .line 138
    const/16 v0, 0x14

    invoke-static {v1, v0}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:I

    .line 139
    const/16 v0, 0xe

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->u:I

    .line 141
    invoke-static {v1, v6}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->v:I

    .line 142
    const v0, -0xcccccd

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->w:I

    .line 143
    invoke-static {v1, v8}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->x:I

    .line 144
    invoke-static {v1, v8}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->y:I

    .line 146
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:Landroid/content/Context;

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    .line 148
    new-instance v0, Lbxk;

    invoke-direct {v0, p0}, Lbxk;-><init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->z:Landroid/view/View$OnClickListener;

    .line 155
    invoke-virtual {p0, v10}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setOrientation(I)V

    .line 157
    new-instance v0, Landroid/widget/HorizontalScrollView;

    invoke-direct {v0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v6}, Landroid/widget/HorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v10}, Landroid/widget/HorizontalScrollView;->setFillViewport(Z)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 167
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->e:Lbqk;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v9, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v3}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 177
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    .line 178
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:Landroid/graphics/Paint;

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 181
    new-instance v0, Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->n:Landroid/support/v4/view/ViewPager;

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->n:Landroid/support/v4/view/ViewPager;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 186
    new-instance v0, Lbxm;

    invoke-direct {v0, p0}, Lbxm;-><init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Lbxm;

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->n:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Lbxm;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lfm;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->n:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lbxl;

    invoke-direct {v1, p0}, Lbxl;-><init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lhe;)V

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/List;

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Ljava/util/List;

    .line 222
    iput v7, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    .line 223
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/TabbedView;IZ)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b(IZ)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/TabbedView;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 36
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxp;

    iget-object v0, v0, Lbxp;->a:Landroid/view/View;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(IZ)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/ui/TabbedView;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->o:I

    return v0
.end method

.method private b(IZ)V
    .locals 2

    .prologue
    .line 373
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    if-ltz v0, :cond_0

    .line 374
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxo;

    invoke-interface {v0}, Lbxo;->b()V

    goto :goto_0

    .line 377
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b(I)V

    .line 378
    if-eqz p2, :cond_1

    .line 379
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d(I)V

    .line 381
    :cond_1
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/widget/HorizontalScrollView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method private c(IZ)V
    .locals 1

    .prologue
    .line 402
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 403
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c(I)Landroid/view/View;

    move-result-object v0

    .line 404
    if-eqz v0, :cond_0

    .line 405
    invoke-virtual {v0, p2}, Landroid/view/View;->setActivated(Z)V

    .line 408
    :cond_0
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    return-object v0
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxn;

    .line 444
    invoke-interface {v0, p1}, Lbxn;->a(I)V

    goto :goto_0

    .line 446
    :cond_0
    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->n:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method


# virtual methods
.method public final a(II)Landroid/view/View;
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Ljava/lang/CharSequence;Landroid/view/View;)V

    .line 239
    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(IZ)V

    .line 370
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 354
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    if-ne p1, v0, :cond_2

    .line 359
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d(I)V

    goto :goto_0

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/widget/LinearLayout$LayoutParams;Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->v:I

    if-lez v0, :cond_0

    .line 291
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 292
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->w:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 293
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->v:I

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 296
    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->y:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 297
    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->x:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 298
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 306
    new-instance v0, Lbxp;

    invoke-direct {v0, p1, p3}, Lbxp;-><init>(Landroid/view/View;Landroid/view/View;)V

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Lbxm;

    invoke-virtual {v0}, Lbxm;->c()V

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v5, :cond_1

    .line 314
    invoke-direct {p0, v4, v4}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b(IZ)V

    .line 316
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->k:I

    .line 317
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->l:F

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->invalidate(Landroid/graphics/Rect;)V

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v5, :cond_2

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 324
    :cond_2
    return-void
.end method

.method public final a(Lbxn;)V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 428
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 247
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 248
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 249
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->t:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 251
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->u:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 252
    sget-object v1, Lezd;->c:Lezd;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lezd;->a(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 253
    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 254
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setLines(I)V

    .line 255
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 257
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 258
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f010112

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 259
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 261
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:I

    invoke-virtual {v0, v1, v5, v2, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 263
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 267
    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Landroid/view/View;Landroid/widget/LinearLayout$LayoutParams;Landroid/view/View;)V

    .line 268
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 392
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    if-ne v0, p1, :cond_0

    .line 399
    :goto_0
    return-void

    .line 396
    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c(IZ)V

    .line 397
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    .line 398
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c(IZ)V

    goto :goto_0
.end method

.method public final c(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 419
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxp;

    iget-object v0, v0, Lbxp;->a:Landroid/view/View;

    .line 423
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 7

    .prologue
    .line 466
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v6

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->k:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxp;

    iget-object v0, v0, Lbxp;->a:Landroid/view/View;

    .line 470
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 471
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    .line 472
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->l:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->k:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->k:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxp;

    iget-object v0, v0, Lbxp;->a:Landroid/view/View;

    .line 474
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int/2addr v3, v2

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->l:F

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v2, v3

    .line 475
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->l:F

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v2

    .line 477
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v3}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v3

    sub-int/2addr v2, v3

    .line 478
    add-int/2addr v1, v2

    .line 479
    add-int/2addr v0, v2

    .line 481
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->q:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 482
    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 489
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->r:I

    if-lez v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->s:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->r:I

    sub-int/2addr v0, v2

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 499
    :cond_0
    return v6

    :cond_1
    move v0, v1

    move v1, v2

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 456
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    .line 458
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    .line 459
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->p:I

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->r:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    .line 460
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    .line 461
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v4

    .line 457
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 462
    return-void
.end method
