.class public Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;
.super Lbhz;
.source "SourceFile"

# interfaces
.implements Lbii;


# instance fields
.field private e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private f:Lctk;

.field private g:Lgix;

.field private h:Lcub;

.field private i:Lfcd;

.field private j:Lbif;

.field private k:Leyt;

.field private l:Larh;

.field private o:Z

.field private p:Z

.field private q:Lfdw;

.field private r:Lfqg;

.field private s:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lbhz;-><init>()V

    .line 280
    return-void
.end method

.method private a(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3

    .prologue
    .line 211
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0902d2

    .line 212
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0902d3

    .line 213
    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0902d4

    new-instance v2, Lbhx;

    invoke-direct {v2, p0}, Lbhx;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    .line 214
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lbhw;

    invoke-direct {v1, p0}, Lbhw;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    .line 220
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 227
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->h()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)Leyt;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->k:Leyt;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->q:Lfdw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->r:Lfqg;

    sget-object v2, Lfqi;->g:Lfqi;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->j:Lbif;

    invoke-virtual {v3}, Lbif;->c()Lhcq;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfdw;->b(Lfqg;Lfqi;Lhcq;)V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)Z
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lbhz;->k()Z

    move-result v0

    return v0
.end method

.method public static synthetic e(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lbhz;->onBackPressed()V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 45
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->o:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->g:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->j:Lbif;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->i:Lfcd;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->g:Lgix;

    invoke-virtual {v2, v3}, Lfcd;->a(Lgix;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->g:Lgix;

    invoke-interface {v3}, Lgix;->d()Lgit;

    move-result-object v3

    iget-object v3, v3, Lgit;->b:Lgiv;

    invoke-virtual {v3}, Lgiv;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lbif;->d:Ljava/lang/String;

    iput-object v3, v1, Lbif;->e:Ljava/lang/String;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.google.android.youtube.intent.action.UPLOAD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v4, "data"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, v1, Lbif;->f:Landroid/graphics/Bitmap;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "no media content uri(s)"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    iget-object v0, v1, Lbif;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_1
    iput-boolean v5, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->p:Z

    :cond_1
    return-void

    :cond_2
    const-string v4, "android.intent.action.SEND"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const-string v4, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_4
    iget-object v0, v1, Lbif;->i:Lfxe;

    iget-object v3, v1, Lbif;->c:Leuc;

    invoke-interface {v0, v3}, Lfxe;->a(Leuc;)V

    iget-object v0, v1, Lbif;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v0, Lbih;

    invoke-direct {v0, v1}, Lbih;-><init>(Lbif;)V

    new-array v1, v5, [Ljava/util/List;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lbih;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method private h()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->f:Lctk;

    new-instance v1, Lbhy;

    invoke-direct {v1, p0}, Lbhy;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    invoke-virtual {v0, p0, v1}, Lctk;->a(Landroid/app/Activity;Lctr;)V

    .line 148
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->l:Larh;

    iget-object v0, v0, Lcmp;->b:Lfhw;

    invoke-virtual {v0}, Lfhw;->d()Lfng;

    move-result-object v0

    iget-boolean v0, v0, Lfng;->a:Z

    if-eqz v0, :cond_0

    .line 259
    const-string v0, "FEmy_videos"

    .line 260
    invoke-static {v0}, Lfia;->a(Ljava/lang/String;)Lhog;

    move-result-object v0

    .line 261
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 262
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 263
    const-string v2, "navigation_endpoint"

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 264
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->startActivity(Landroid/content/Intent;)V

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lbhz;->m:Lfus;

    invoke-virtual {v0}, Lfus;->d()V

    goto :goto_0
.end method


# virtual methods
.method protected final a_(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->j:Lbif;

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    .line 241
    :goto_0
    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lbhz;->a_(I)Landroid/app/Dialog;

    move-result-object v0

    :cond_0
    return-object v0

    .line 238
    :pswitch_0
    iget-object v0, v0, Lbif;->g:Lbya;

    iget-object v0, v0, Lbya;->b:Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3fd
        :pswitch_0
    .end packed-switch
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    .line 248
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->i()V

    .line 249
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    .line 254
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->i()V

    .line 255
    return-void
.end method

.method public handleSignOutEvent(Lfcc;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 277
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    .line 278
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 189
    new-instance v0, Lbhu;

    invoke-direct {v0, p0}, Lbhu;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->a(Landroid/content/DialogInterface$OnClickListener;)V

    .line 196
    const/4 v0, 0x1

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 201
    new-instance v0, Lbhv;

    invoke-direct {v0, p0}, Lbhv;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->a(Landroid/content/DialogInterface$OnClickListener;)V

    .line 208
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 65
    invoke-super {p0, p1}, Lbhz;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 67
    const v1, 0x7f04011e

    invoke-virtual {v0, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 68
    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->setContentView(Landroid/view/View;)V

    .line 69
    const v0, 0x7f0901f0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->setTitle(I)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v1

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    .line 74
    invoke-virtual {v1}, Lari;->m()Lctk;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->f:Lctk;

    .line 75
    invoke-virtual {v1}, Lari;->aD()Lcst;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->g:Lgix;

    .line 76
    invoke-virtual {v1}, Lari;->aO()Lcub;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->h:Lcub;

    .line 77
    invoke-virtual {v1}, Lari;->az()Lfcd;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->i:Lfcd;

    .line 78
    invoke-virtual {v1}, Lari;->ay()Leyt;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->k:Leyt;

    .line 79
    invoke-virtual {v1}, Lari;->f()Larh;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->l:Larh;

    .line 80
    invoke-virtual {v0}, Letc;->q()Ljava/util/concurrent/Executor;

    .line 81
    invoke-virtual {v1}, Lari;->ae()Lfdw;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->q:Lfdw;

    .line 82
    new-instance v3, Lfqg;

    invoke-virtual {v0}, Letc;->k()Lfac;

    move-result-object v0

    invoke-direct {v3, v0}, Lfqg;-><init>(Lfac;)V

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->r:Lfqg;

    .line 83
    if-nez p1, :cond_1

    move v0, v10

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->s:Z

    .line 85
    new-instance v0, Lbif;

    .line 88
    invoke-virtual {v1}, Lari;->b()Lfxe;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->k:Leyt;

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->B()Lbxu;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->q:Lfdw;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->r:Lfqg;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->l:Larh;

    move-object v1, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v9}, Lbif;-><init>(Landroid/app/Activity;Landroid/view/View;Lfxe;Lbii;Leyt;Lbxu;Lfdw;Lfqg;Larh;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->j:Lbif;

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->C()Lcal;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->j:Lbif;

    invoke-virtual {v0, v1}, Lcal;->a(Lcam;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->d()Lkm;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v10}, Lkm;->c(Z)V

    .line 99
    invoke-virtual {v0, v10}, Lkm;->d(Z)V

    .line 100
    const v1, 0x7f0200f8

    invoke-virtual {v0, v1}, Lkm;->a(I)V

    .line 101
    invoke-virtual {v0, v10}, Lkm;->a(Z)V

    .line 103
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->s:Z

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->q:Lfdw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->r:Lfqg;

    sget-object v2, Lfqi;->e:Lfqi;

    invoke-virtual {v0, v1, v2, v11}, Lfdw;->a(Lfqg;Lfqi;Lhcq;)V

    .line 108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->s:Z

    .line 110
    :cond_0
    return-void

    .line 83
    :cond_1
    const-string v0, "screen_graft_logged"

    invoke-virtual {p1, v0, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0}, Lbhz;->onPause()V

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 177
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Lbhz;->onResume()V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 171
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0, p1}, Lbhz;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 115
    const-string v0, "screen_graft_logged"

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->s:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0}, Lbhz;->onStart()V

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->o:Z

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->g:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->h:Lcub;

    new-instance v1, Lbht;

    invoke-direct {v1, p0}, Lbht;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    invoke-virtual {v0, p0, v1}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->h()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 182
    invoke-super {p0}, Lbhz;->onStop()V

    .line 183
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->o:Z

    .line 184
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->j:Lbif;

    iget-object v0, v0, Lbif;->b:Ldno;

    invoke-virtual {v0}, Ldno;->c()V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->p:Z

    .line 185
    :cond_0
    return-void
.end method
