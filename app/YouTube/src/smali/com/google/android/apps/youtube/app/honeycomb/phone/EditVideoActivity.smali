.class public Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;
.super Lbhz;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field private e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private f:Lgix;

.field private g:Lfxe;

.field private h:Leyp;

.field private i:Leyt;

.field private j:Levn;

.field private k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private l:Landroid/widget/ImageView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/EditText;

.field private q:Landroid/widget/EditText;

.field private r:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

.field private s:Landroid/widget/EditText;

.field private t:Landroid/widget/Button;

.field private u:Lgcd;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lbhz;-><init>()V

    .line 268
    return-void
.end method

.method public static a(Landroid/content/Context;Lgcd;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 66
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    const-string v1, "android.intent.action.EDIT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v1, "video"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 70
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 74
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 76
    const-string v1, "android.intent.action.EDIT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const v0, 0x7f090261

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Leze;->a(Landroid/content/Context;II)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v2, v0

    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v5, v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->r:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lgcg;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->g:Lfxe;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->u:Lgcd;

    iget-object v3, v3, Lgcd;->t:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->u:Lgcd;

    iget-object v4, v4, Lgcd;->u:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->u:Lgcd;

    iget-object v7, v7, Lgcd;->y:Ljava/util/Map;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->u:Lgcd;

    iget-object v8, v8, Lgcd;->z:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->u:Lgcd;

    iget-object v9, v9, Lgcd;->A:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->u:Lgcd;

    iget-object v10, v10, Lgcd;->h:Landroid/net/Uri;

    invoke-static {p0, p0}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v11

    invoke-interface/range {v0 .. v11}, Lfxe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgcg;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Leuc;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;Lgcd;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->a(Lgcd;)V

    return-void
.end method

.method private a(Lgcd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 172
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->u:Lgcd;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 175
    iget-object v0, p1, Lgcd;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->p:Landroid/widget/EditText;

    iget-object v1, p1, Lgcd;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 178
    :cond_0
    iget-object v0, p1, Lgcd;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->q:Landroid/widget/EditText;

    iget-object v1, p1, Lgcd;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 181
    :cond_1
    iget-object v0, p1, Lgcd;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->s:Landroid/widget/EditText;

    iget-object v1, p1, Lgcd;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :cond_2
    iget-object v0, p1, Lgcd;->x:Lgcg;

    if-eqz v0, :cond_3

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->r:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    iget-object v1, p1, Lgcd;->x:Lgcg;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a(Lgcg;)V

    .line 187
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->o:Landroid/widget/TextView;

    iget v1, p1, Lgcd;->k:I

    invoke-static {v1}, La;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 191
    iget-object v0, p1, Lgcd;->d:Landroid/net/Uri;

    if-eqz v0, :cond_4

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->h:Leyp;

    iget-object v1, p1, Lgcd;->d:Landroid/net/Uri;

    new-instance v2, Lbgs;

    invoke-direct {v2, p0}, Lbgs;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;)V

    .line 194
    invoke-static {p0, v2}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v2

    .line 192
    invoke-interface {v0, v1, v2}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->l:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;)Leyt;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->i:Leyt;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->l:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 44
    const-string v0, "Error updating video metadata"

    invoke-static {v0, p2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->i:Leyt;

    invoke-interface {v0, p2}, Leyt;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 44
    const v0, 0x7f090260

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Leze;->a(Landroid/content/Context;II)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    return-void
.end method

.method public handleSignOutEvent(Lfcc;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    .line 224
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 83
    invoke-super {p0, p1}, Lbhz;->onCreate(Landroid/os/Bundle;)V

    .line 84
    const v0, 0x7f040067

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->setContentView(I)V

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->d()Lkm;

    move-result-object v0

    const v1, 0x7f09025f

    invoke-virtual {v0, v1}, Lkm;->b(I)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v1, v1, Lckz;->a:Letc;

    .line 90
    invoke-virtual {v0}, Lari;->b()Lfxe;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->g:Lfxe;

    .line 91
    invoke-virtual {v0}, Lari;->ay()Leyt;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->i:Leyt;

    .line 92
    invoke-virtual {v0}, Lari;->aD()Lcst;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->f:Lgix;

    .line 93
    invoke-virtual {v0}, Lari;->c()Leyp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->h:Leyp;

    .line 94
    invoke-virtual {v1}, Letc;->i()Levn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->j:Levn;

    .line 96
    const v0, 0x7f0801d1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    .line 97
    const v0, 0x7f0800a4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->l:Landroid/widget/ImageView;

    .line 98
    const v0, 0x7f08012f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->o:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f080315

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->p:Landroid/widget/EditText;

    .line 101
    const v0, 0x7f080317

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->q:Landroid/widget/EditText;

    .line 102
    const v0, 0x7f080319

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->r:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    .line 103
    const v0, 0x7f08031b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->s:Landroid/widget/EditText;

    .line 105
    const v0, 0x7f0801d3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->t:Landroid/widget/Button;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->t:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->t:Landroid/widget/Button;

    const v1, 0x104000a

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->t:Landroid/widget/Button;

    new-instance v1, Lbgq;

    invoke-direct {v1, p0}, Lbgq;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 212
    invoke-super {p0}, Lbhz;->onPause()V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->j:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 214
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 202
    invoke-super {p0}, Lbhz;->onResume()V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->f:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    .line 208
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->j:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 119
    invoke-super {p0}, Lbhz;->onStart()V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->f:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    .line 143
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 125
    const-string v0, "android.intent.action.EDIT"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 126
    const-string v2, "unsupported action "

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    goto :goto_0

    .line 126
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 131
    :cond_2
    const-string v0, "video"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 132
    if-eqz v0, :cond_3

    .line 133
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->a(Lgcd;)V

    goto :goto_0

    .line 135
    :cond_3
    const-string v0, "video_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 136
    if-nez v0, :cond_4

    .line 137
    const-string v0, "video not found"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    goto :goto_0

    .line 141
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->g:Lfxe;

    new-instance v2, Lbgr;

    invoke-direct {v2, p0}, Lbgr;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;)V

    invoke-static {p0, v2}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lfxe;->e(Ljava/lang/String;Leuc;)V

    goto :goto_0
.end method
