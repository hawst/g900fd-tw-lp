.class public Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field private final A:Landroid/graphics/Rect;

.field private final B:Landroid/graphics/Rect;

.field private final C:Landroid/graphics/Rect;

.field private D:F

.field private E:I

.field private final F:Lbzo;

.field private final G:Landroid/view/animation/DecelerateInterpolator;

.field private final H:Landroid/graphics/Paint;

.field private final I:Landroid/graphics/drawable/Drawable;

.field private final J:Landroid/graphics/drawable/Drawable;

.field private final K:I

.field private L:Landroid/graphics/Rect;

.field private M:Landroid/graphics/Rect;

.field private N:Z

.field private O:Z

.field private final P:Lbzp;

.field public a:Lbzs;

.field public b:Z

.field public final c:Landroid/widget/Scroller;

.field public final d:Landroid/widget/Scroller;

.field public e:Lbzn;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Ljava/util/LinkedList;

.field private r:I

.field private s:I

.field private final t:I

.field private final u:I

.field private v:Z

.field private w:I

.field private x:I

.field private y:I

.field private final z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 242
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 243
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 244
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    .line 245
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    .line 246
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    .line 247
    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->O:Z

    .line 249
    sget-object v0, Lbzs;->a:Lbzs;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    .line 251
    new-instance v0, Landroid/widget/Scroller;

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v5}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    .line 252
    new-instance v0, Landroid/widget/Scroller;

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v5}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    .line 253
    new-instance v0, Lbzo;

    invoke-direct {v0, p0, p1}, Lbzo;-><init>(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    .line 254
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->G:Landroid/view/animation/DecelerateInterpolator;

    .line 256
    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:I

    .line 257
    const v0, 0x7f0a00cf

    .line 258
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->t:I

    .line 259
    const v0, 0x7f0a00d0

    .line 260
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:I

    .line 262
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->H:Landroid/graphics/Paint;

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->H:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    move-object v0, p1

    .line 265
    check-cast v0, Landroid/app/Activity;

    .line 266
    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lckz;

    .line 268
    invoke-virtual {v0}, Lckz;->n()Lcla;

    move-result-object v0

    invoke-virtual {v0}, Lcla;->ar()Lcyc;

    move-result-object v0

    invoke-static {v0}, La;->a(Lezk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269
    const v0, 0x7f0201e7

    invoke-static {p1, v0}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    .line 273
    :goto_0
    const v0, 0x7f0201e8

    invoke-static {p1, v0}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->J:Landroid/graphics/drawable/Drawable;

    .line 274
    const v0, 0x7f0a00cc

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:I

    .line 275
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    .line 276
    new-instance v0, Lbzp;

    invoke-direct {v0, p0}, Lbzp;-><init>(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->P:Lbzp;

    .line 278
    sget-object v0, Lgvk;->m:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 279
    invoke-virtual {v4, v3, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    .line 280
    invoke-virtual {v4, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:I

    .line 281
    const/4 v0, 0x3

    .line 282
    invoke-virtual {v4, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:I

    .line 283
    const/4 v0, 0x4

    invoke-virtual {v4, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:I

    .line 284
    const/4 v0, 0x5

    invoke-virtual {v4, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:I

    .line 286
    const/4 v0, 0x6

    const/high16 v2, 0x43700000    # 240.0f

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:I

    .line 287
    const/4 v0, 0x7

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:I

    .line 288
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 290
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    const-string v2, "playerViewId must be specified"

    invoke-static {v0, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 291
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:I

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    const-string v2, "metadataViewId must be specified"

    invoke-static {v0, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 292
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:I

    if-eqz v0, :cond_5

    :goto_4
    const-string v0, "metadataLandscapeTitleViewId must be specified"

    invoke-static {v1, v0}, Lb;->c(ZLjava/lang/Object;)V

    .line 296
    :cond_0
    return-void

    .line 271
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 284
    goto :goto_1

    :cond_3
    move v0, v3

    .line 290
    goto :goto_2

    :cond_4
    move v0, v3

    .line 291
    goto :goto_3

    :cond_5
    move v1, v3

    .line 293
    goto :goto_4
.end method

.method private static a(FII)I
    .locals 2

    .prologue
    .line 608
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 609
    sub-int v1, p2, p1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method private a(IIIZ)I
    .locals 2

    .prologue
    .line 1106
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1107
    if-ne v0, p2, :cond_0

    .line 1115
    :goto_0
    return p3

    .line 1110
    :cond_0
    int-to-float v0, v0

    int-to-float v1, p2

    div-float/2addr v0, v1

    .line 1111
    const/4 v1, 0x0

    invoke-static {v0, v1, p3}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v0

    .line 1112
    if-eqz p4, :cond_1

    .line 1113
    const/high16 v1, 0x3f400000    # 0.75f

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1115
    :cond_1
    const/16 v1, 0x32

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p3

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;)Lbzn;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    return-object v0
.end method

.method private a(II)V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v4, 0x0

    const v8, 0x3fe374bc    # 1.777f

    .line 628
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    move-result v5

    .line 629
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    move-result v6

    .line 630
    sub-int v0, p1, v5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingRight()I

    move-result v1

    sub-int v7, v0, v1

    .line 631
    sub-int v0, p2, v6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    move-result v1

    sub-int v3, v0, v1

    .line 634
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:I

    int-to-float v0, v0

    div-float/2addr v0, v8

    float-to-int v1, v0

    .line 635
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 636
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 638
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, La;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 639
    const v0, 0x3f333333    # 0.7f

    int-to-float v2, v7

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 643
    :goto_0
    int-to-float v2, v0

    div-float/2addr v2, v8

    float-to-int v2, v2

    .line 644
    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    invoke-static {v8, v5, v6, v0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/graphics/Rect;IIII)V

    move v0, v1

    move v1, v2

    .line 654
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:I

    sub-int v5, v7, v5

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:I

    sub-int/2addr v5, v6

    .line 656
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    move-result v6

    sub-int v6, p2, v6

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:I

    sub-int/2addr v6, v7

    sub-int/2addr v6, v0

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:I

    .line 654
    invoke-static {v2, v5, v6, v7, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/graphics/Rect;IIII)V

    .line 662
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v5

    div-int/lit8 v2, v2, 0x2

    .line 663
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    .line 664
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    .line 665
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    .line 666
    sub-int v2, v6, v2

    .line 667
    sub-int v5, v7, v5

    .line 669
    mul-int/lit8 v6, v2, 0x2

    if-le v5, v6, :cond_4

    .line 671
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    const/4 v5, 0x0

    iput v5, v2, Lbzo;->e:F

    .line 672
    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:I

    sub-int v2, v3, v2

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    .line 681
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_6

    .line 682
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalY()I

    move-result v0

    if-gtz v0, :cond_5

    .line 684
    invoke-virtual {p0, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Z)V

    .line 693
    :cond_0
    :goto_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    .line 695
    invoke-virtual {p0, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    .line 696
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g()V

    .line 698
    invoke-static {p1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 699
    invoke-static {p2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 697
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->measure(II)V

    .line 700
    return-void

    .line 641
    :cond_1
    const v0, 0x3f266666    # 0.65f

    int-to-float v2, v7

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto/16 :goto_0

    .line 647
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    invoke-static {v0, v5, v6, v7, v3}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/graphics/Rect;IIII)V

    move v0, v1

    move v1, v3

    goto/16 :goto_1

    .line 650
    :cond_3
    int-to-float v0, v7

    div-float/2addr v0, v8

    float-to-int v1, v0

    .line 651
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:I

    int-to-float v0, v0

    div-float/2addr v0, v8

    float-to-int v0, v0

    .line 652
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    invoke-static {v2, v5, v6, v7, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/graphics/Rect;IIII)V

    goto/16 :goto_1

    .line 677
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    int-to-double v6, v5

    int-to-double v8, v5

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float v1, v6

    iput v1, v0, Lbzo;->e:F

    .line 678
    mul-int v0, v2, v2

    mul-int v1, v5, v5

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    goto :goto_2

    .line 686
    :cond_5
    invoke-virtual {p0, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(Z)V

    goto :goto_3

    .line 688
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_8

    .line 689
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Z

    if-eqz v0, :cond_7

    sget-object v0, Lbzs;->a:Lbzs;

    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_3

    :cond_7
    sget-object v0, Lbzs;->c:Lbzs;

    goto :goto_4

    .line 690
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v1, Lbzs;->a:Lbzs;

    if-eq v0, v1, :cond_0

    .line 691
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v1, Lbzs;->c:Lbzs;

    if-ne v0, v1, :cond_9

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    :goto_5
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    goto :goto_3

    :cond_9
    move v0, v4

    goto :goto_5
.end method

.method private static a(Landroid/graphics/Rect;IIII)V
    .locals 2

    .prologue
    .line 600
    add-int v0, p1, p3

    add-int v1, p2, p4

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 601
    return-void
.end method

.method private static a(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 1298
    if-eqz p0, :cond_0

    .line 1299
    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 1301
    :cond_0
    return-void
.end method

.method private b(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 557
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 575
    :cond_0
    :goto_0
    return v0

    .line 561
    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-eq v1, p1, :cond_0

    .line 562
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    .line 565
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    .line 566
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()V

    .line 568
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g()V

    .line 569
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    if-eqz v0, :cond_2

    .line 570
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 571
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    .line 570
    invoke-interface {v0, v1}, Lbzn;->a(F)V

    .line 573
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 579
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 592
    :cond_0
    :goto_0
    return v0

    .line 583
    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    if-eq v1, p1, :cond_0

    .line 584
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    .line 585
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g()V

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    if-eqz v0, :cond_2

    .line 587
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 588
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    .line 587
    invoke-interface {v0, v1}, Lbzn;->a(F)V

    .line 590
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1167
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v1, Lbzs;->c:Lbzs;

    if-eq v0, v1, :cond_1

    .line 1185
    :cond_0
    :goto_0
    return-void

    .line 1171
    :cond_1
    const/16 v5, 0xfa

    .line 1172
    if-eqz p1, :cond_2

    .line 1173
    const/16 v5, 0xbb

    .line 1175
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d()V

    .line 1176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Z

    .line 1177
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:I

    .line 1178
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    if-gez v3, :cond_3

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:I

    neg-int v3, v3

    :goto_1
    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1184
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0

    .line 1178
    :cond_3
    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:I

    goto :goto_1
.end method

.method private e(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1188
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v1, Lbzs;->c:Lbzs;

    if-eq v0, v1, :cond_1

    .line 1204
    :cond_0
    :goto_0
    return-void

    .line 1192
    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1193
    if-nez v0, :cond_2

    .line 1196
    sget-object v0, Lbzs;->c:Lbzs;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_0

    .line 1198
    :cond_2
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->t:I

    .line 1199
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/16 v3, 0xfa

    .line 1198
    invoke-direct {p0, v0, v1, v3, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(IIIZ)I

    move-result v5

    .line 1200
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d()V

    .line 1201
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    neg-int v3, v3

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1202
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 613
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1226
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l()Lbzs;

    move-result-object v0

    .line 1227
    sget-object v1, Lbzm;->a:[I

    invoke-virtual {v0}, Lbzs;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1238
    :goto_0
    return-void

    .line 1229
    :pswitch_0
    sget-object v0, Lbzs;->a:Lbzs;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_0

    .line 1232
    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(Z)V

    goto :goto_0

    .line 1235
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Z)V

    goto :goto_0

    .line 1227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 617
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 707
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v1, Lbzs;->a:Lbzs;

    if-ne v0, v1, :cond_0

    .line 754
    :goto_0
    return-void

    .line 711
    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-gtz v0, :cond_1

    .line 713
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    .line 714
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 752
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->requestLayout()V

    .line 753
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0

    .line 715
    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    if-ge v0, v1, :cond_2

    .line 717
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    .line 719
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    .line 720
    invoke-static {v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    .line 721
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    .line 722
    invoke-static {v4, v5, v6}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v4

    .line 718
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    .line 724
    :cond_2
    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    .line 725
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 727
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    if-eqz v0, :cond_3

    .line 728
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 729
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Z

    if-eqz v1, :cond_5

    .line 731
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 732
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:I

    if-lt v0, v1, :cond_4

    .line 733
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    .line 747
    :cond_3
    :goto_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-gt v0, v1, :cond_7

    const/4 v0, 0x0

    :goto_3
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    add-int/2addr v0, v1

    .line 748
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 749
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    goto/16 :goto_1

    .line 735
    :cond_4
    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    goto :goto_2

    .line 739
    :cond_5
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->t:I

    if-lt v0, v1, :cond_6

    .line 740
    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    goto :goto_2

    .line 742
    :cond_6
    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->t:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    goto :goto_2

    .line 747
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    goto :goto_3
.end method

.method private h()V
    .locals 9

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x8

    .line 857
    .line 858
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-eqz v0, :cond_1

    .line 859
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 860
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 861
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 862
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 864
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 909
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 910
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 873
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v4, Lbzs;->a:Lbzs;

    if-eq v0, v4, :cond_7

    .line 874
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f()Z

    move-result v0

    .line 875
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e()Z

    move-result v6

    .line 876
    if-nez v0, :cond_4

    if-eqz v6, :cond_4

    move v4, v1

    .line 877
    :goto_2
    if-eqz v0, :cond_5

    if-eqz v6, :cond_5

    move v0, v1

    .line 880
    :goto_3
    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    if-ge v6, v7, :cond_c

    .line 883
    if-nez v4, :cond_b

    .line 885
    if-eqz v0, :cond_6

    move v0, v2

    .line 886
    :goto_4
    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    const v6, 0x3dcccccd    # 0.1f

    cmpl-float v4, v4, v6

    if-lez v4, :cond_a

    const v4, 0x3f8ccccd    # 1.1f

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    sub-float/2addr v4, v6

    :goto_5
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    invoke-static {v6, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/view/View;F)V

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    invoke-static {v6, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/view/View;F)V

    move v4, v2

    .line 888
    :goto_6
    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-lez v6, :cond_9

    .line 890
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->G:Landroid/view/animation/DecelerateInterpolator;

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    sub-float/2addr v5, v7

    invoke-virtual {v6, v5}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v5

    const v6, 0x3f666666    # 0.9f

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    invoke-static {v6, v5}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/view/View;F)V

    move v5, v2

    .line 893
    :goto_7
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->O:Z

    .line 895
    :goto_8
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-lez v1, :cond_2

    move v3, v2

    .line 901
    :cond_2
    :goto_9
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 902
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 903
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 904
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 906
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    move v4, v2

    .line 876
    goto :goto_2

    :cond_5
    move v0, v2

    .line 877
    goto :goto_3

    :cond_6
    move v0, v3

    .line 885
    goto :goto_4

    .line 897
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate(Landroid/graphics/Rect;)V

    .line 898
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    move v5, v3

    move v0, v3

    move v4, v3

    move v8, v3

    move v3, v2

    move v2, v8

    goto :goto_9

    .line 912
    :cond_8
    return-void

    :cond_9
    move v5, v3

    goto :goto_7

    :cond_a
    move v4, v5

    goto :goto_5

    :cond_b
    move v0, v3

    move v4, v3

    goto :goto_6

    :cond_c
    move v5, v3

    move v0, v3

    move v4, v3

    goto :goto_8
.end method

.method private i()V
    .locals 2

    .prologue
    .line 942
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 943
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:I

    .line 944
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 945
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    if-eqz v0, :cond_0

    .line 946
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    .line 949
    :cond_0
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 1091
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1092
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1094
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Z

    .line 1095
    return-void
.end method

.method private k()I
    .locals 1

    .prologue
    .line 1098
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1099
    const/16 v0, 0x190

    .line 1101
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x15e

    goto :goto_0
.end method

.method private l()Lbzs;
    .locals 2

    .prologue
    .line 1207
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-nez v0, :cond_3

    .line 1208
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Z

    if-eqz v0, :cond_0

    .line 1209
    sget-object v0, Lbzs;->a:Lbzs;

    .line 1222
    :goto_0
    return-object v0

    .line 1211
    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    if-eqz v0, :cond_2

    .line 1212
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->t:I

    if-lt v0, v1, :cond_1

    .line 1213
    sget-object v0, Lbzs;->a:Lbzs;

    goto :goto_0

    .line 1215
    :cond_1
    sget-object v0, Lbzs;->c:Lbzs;

    goto :goto_0

    .line 1218
    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    div-int/lit8 v1, v1, 0x2

    if-lt v0, v1, :cond_3

    .line 1219
    sget-object v0, Lbzs;->c:Lbzs;

    goto :goto_0

    .line 1222
    :cond_3
    sget-object v0, Lbzs;->b:Lbzs;

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 353
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:I

    if-eq v0, p1, :cond_1

    .line 354
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-le p1, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 356
    :goto_0
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:I

    .line 357
    if-eqz v0, :cond_1

    .line 358
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g()V

    .line 361
    :cond_1
    return-void

    .line 354
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lbzs;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    if-eq v0, p1, :cond_2

    .line 818
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    .line 819
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v2, Lbzs;->a:Lbzs;

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-eqz v0, :cond_0

    .line 820
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    .line 822
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 823
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v2, Lbzs;->b:Lbzs;

    if-ne v0, v2, :cond_4

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(I)Z

    .line 825
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d()V

    .line 827
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    if-eqz v0, :cond_2

    .line 828
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    invoke-interface {v0, v2}, Lbzn;->a(Lbzs;)V

    .line 832
    :cond_2
    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    .line 833
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    if-eqz v0, :cond_3

    .line 834
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    invoke-interface {v0}, Lbzn;->r()V

    .line 836
    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(I)V

    .line 837
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    .line 838
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g()V

    .line 839
    return-void

    .line 823
    :cond_4
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    goto :goto_0
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1123
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v2, Lbzs;->a:Lbzs;

    if-ne v0, v2, :cond_1

    .line 1140
    :cond_0
    :goto_0
    return-void

    .line 1127
    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    rsub-int/lit8 v4, v0, 0x0

    .line 1128
    if-nez v4, :cond_2

    .line 1130
    sget-object v0, Lbzs;->b:Lbzs;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_0

    .line 1134
    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    .line 1135
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k()I

    move-result v2

    invoke-direct {p0, v4, v0, v2, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(IIIZ)I

    move-result v5

    .line 1136
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d()V

    .line 1137
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    .line 1138
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1139
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 553
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v1, Lbzs;->a:Lbzs;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 808
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v1, Lbzs;->c:Lbzs;

    if-ne v0, v1, :cond_0

    .line 810
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d(Z)V

    .line 814
    :goto_0
    return-void

    .line 812
    :cond_0
    sget-object v0, Lbzs;->a:Lbzs;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1147
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v2, Lbzs;->a:Lbzs;

    if-ne v0, v2, :cond_1

    .line 1164
    :cond_0
    :goto_0
    return-void

    .line 1151
    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    sub-int v4, v0, v2

    .line 1152
    if-nez v4, :cond_2

    .line 1154
    sget-object v0, Lbzs;->c:Lbzs;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_0

    .line 1158
    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    .line 1159
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k()I

    move-result v2

    invoke-direct {p0, v4, v0, v2, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(IIIZ)I

    move-result v5

    .line 1160
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d()V

    .line 1161
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    .line 1162
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1163
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1304
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->O:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->N:Z

    if-eq v0, p1, :cond_2

    .line 1305
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->N:Z

    .line 1306
    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getChildCount()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    if-eq v3, v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    .line 1308
    :cond_2
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 923
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 2

    .prologue
    .line 518
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-eqz v0, :cond_1

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 523
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(I)Z

    .line 524
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 525
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-gtz v0, :cond_3

    .line 526
    sget-object v0, Lbzs;->b:Lbzs;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    .line 531
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0

    .line 527
    :cond_3
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    if-lt v0, v1, :cond_2

    .line 528
    sget-object v0, Lbzs;->c:Lbzs;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_1

    .line 532
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(I)Z

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 535
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Z

    if-eqz v0, :cond_6

    .line 536
    sget-object v0, Lbzs;->a:Lbzs;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    .line 541
    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0

    .line 538
    :cond_6
    sget-object v0, Lbzs;->c:Lbzs;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_2

    .line 542
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 544
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    if-eq v0, v1, :cond_8

    .line 545
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f(Z)V

    goto :goto_0

    .line 546
    :cond_8
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    if-eqz v0, :cond_0

    .line 547
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l()Lbzs;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 1084
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1085
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1087
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()V

    .line 1088
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    .prologue
    .line 1269
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    if-ne p2, v0, :cond_1

    .line 1270
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 1271
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v2, Lbzs;->a:Lbzs;

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-lez v1, :cond_0

    .line 1272
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->J:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1273
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 1274
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1279
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 379
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 380
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(II)V

    .line 381
    return-void
.end method

.method public onFinishInflate()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getChildCount()I

    move-result v4

    .line 301
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f()Z

    move-result v5

    .line 302
    if-eqz v5, :cond_0

    const/4 v0, 0x4

    move v3, v0

    .line 303
    :goto_0
    if-lt v4, v3, :cond_1

    move v0, v1

    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x38

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "WatchWhileLayout must have at least "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " children"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lb;->c(ZLjava/lang/Object;)V

    .line 306
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q:Ljava/util/LinkedList;

    move v0, v2

    .line 307
    :goto_2
    if-ge v0, v4, :cond_6

    .line 308
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 309
    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v6

    .line 310
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    if-nez v7, :cond_2

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    if-ne v7, v6, :cond_2

    .line 311
    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    .line 307
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 302
    :cond_0
    const/4 v0, 0x3

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 303
    goto :goto_1

    .line 312
    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    if-nez v7, :cond_3

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:I

    if-ne v7, v6, :cond_3

    .line 313
    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    goto :goto_3

    .line 314
    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    if-nez v7, :cond_4

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:I

    if-ne v7, v6, :cond_4

    .line 315
    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    goto :goto_3

    .line 316
    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:Landroid/view/View;

    if-nez v7, :cond_5

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:I

    if-ne v7, v6, :cond_5

    .line 317
    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:Landroid/view/View;

    goto :goto_3

    .line 319
    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q:Ljava/util/LinkedList;

    invoke-virtual {v6, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 323
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    if-eqz v5, :cond_7

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_9

    :goto_4
    const-string v0, "contentViews cannot be empty"

    invoke-static {v1, v0}, Lb;->c(ZLjava/lang/Object;)V

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->P:Lbzp;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->P:Lbzp;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 334
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->addView(Landroid/view/View;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->bringChildToFront(Landroid/view/View;)V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->bringChildToFront(Landroid/view/View;)V

    .line 342
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->bringChildToFront(Landroid/view/View;)V

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->bringChildToFront(Landroid/view/View;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->bringChildToFront(Landroid/view/View;)V

    .line 345
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h()V

    .line 346
    return-void

    :cond_9
    move v1, v2

    .line 328
    goto :goto_4
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v6, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 966
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    move v1, v0

    .line 1004
    :cond_1
    :goto_0
    return v1

    .line 970
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    packed-switch v3, :pswitch_data_0

    .line 1004
    :cond_3
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c()Z

    move-result v1

    goto :goto_0

    .line 974
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c()Z

    move-result v3

    if-nez v3, :cond_1

    .line 978
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    iget v4, v3, Lbzo;->d:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    if-ltz v4, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-le v5, v4, :cond_9

    iget v4, v3, Lbzo;->b:F

    iget v5, v3, Lbzo;->c:F

    invoke-virtual {v3, p1}, Lbzo;->c(Landroid/view/MotionEvent;)I

    move-result v6

    invoke-virtual {v3, p1}, Lbzo;->d(Landroid/view/MotionEvent;)I

    move-result v7

    iget-object v8, v3, Lbzo;->f:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iget-object v8, v8, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v9, Lbzs;->c:Lbzs;

    if-ne v8, v9, :cond_4

    iget-object v8, v3, Lbzo;->f:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iget v8, v8, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    iget-object v9, v3, Lbzo;->f:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iget v9, v9, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    if-ne v8, v9, :cond_4

    move v0, v1

    :cond_4
    invoke-virtual {v3, v6, v7}, Lbzo;->a(II)I

    move-result v8

    if-eqz v0, :cond_7

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v6, v3, Lbzo;->a:I

    mul-int/lit8 v6, v6, 0x2

    if-le v0, v6, :cond_6

    iget v0, v3, Lbzo;->e:F

    const/4 v6, 0x0

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_5

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v6, v3, Lbzo;->a:I

    if-ge v0, v6, :cond_6

    :cond_5
    const/4 v0, 0x3

    :goto_2
    if-eq v0, v1, :cond_3

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:I

    if-eq v2, v0, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->P:Lbzp;

    invoke-static {v2}, Lbzp;->a(Lbzp;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    goto :goto_1

    :cond_6
    iget v0, v3, Lbzo;->a:I

    mul-int/lit8 v0, v0, 0x2

    if-le v8, v0, :cond_8

    move v0, v2

    goto :goto_2

    :cond_7
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v6, v3, Lbzo;->a:I

    if-le v0, v6, :cond_8

    move v0, v2

    goto :goto_2

    :cond_8
    iput v4, v3, Lbzo;->b:F

    iput v5, v3, Lbzo;->c:F

    :goto_3
    move v0, v1

    goto :goto_2

    :cond_9
    iput v6, v3, Lbqx;->d:I

    goto :goto_3

    .line 982
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 983
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    invoke-virtual {v2, p1}, Lbzo;->b(Landroid/view/MotionEvent;)V

    .line 987
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_a
    move v1, v0

    goto/16 :goto_0

    .line 994
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i()V

    .line 995
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    iput v6, v0, Lbqx;->d:I

    goto/16 :goto_1

    .line 999
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    invoke-virtual {v0, p1}, Lbzo;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 970
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 15

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h()V

    .line 444
    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-eqz v4, :cond_1

    .line 445
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 452
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :cond_2
    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-gtz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    :goto_2
    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_7

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    :cond_3
    :goto_3
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    invoke-static {v4}, Leze;->a(F)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->J:Landroid/graphics/drawable/Drawable;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->J:Landroid/graphics/drawable/Drawable;

    invoke-static {v4}, Leze;->a(F)I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate(IIII)V

    .line 455
    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->N:Z

    if-eqz v4, :cond_a

    .line 456
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getHeight()I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f()Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v9

    invoke-static {v7, v8, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v4

    add-int/2addr v4, v6

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    sub-int v6, v4, v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    iget v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v5, v10

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    mul-int/lit8 v10, v10, 0x2

    add-int/2addr v5, v10

    invoke-static {v8, v9, v5}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v5

    add-int/2addr v5, v7

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int/2addr v4, v8

    int-to-float v4, v4

    invoke-virtual {v7, v4}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    goto/16 :goto_0

    .line 452
    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:I

    add-int/2addr v8, v9

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1

    :cond_6
    new-instance v4, Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    invoke-direct {v4, v5}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Landroid/graphics/Rect;

    goto/16 :goto_2

    :cond_7
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    const/high16 v6, 0x40000000    # 2.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3

    const/high16 v4, 0x40000000    # 2.0f

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    sub-float/2addr v4, v5

    goto/16 :goto_3

    .line 456
    :cond_8
    if-eqz v6, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    move-result v4

    sub-int v4, v5, v4

    :goto_4
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    int-to-float v4, v4

    invoke-virtual {v5, v4}, Landroid/view/View;->setTranslationY(F)V

    goto/16 :goto_0

    :cond_9
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:I

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int v6, v4, v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_4

    .line 459
    :cond_a
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    :cond_b
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 461
    sub-int v4, p4, p2

    .line 462
    sub-int v5, p5, p3

    .line 463
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    move-result v6

    .line 464
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    move-result v7

    .line 465
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e()Z

    move-result v8

    .line 466
    if-eqz v8, :cond_d

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 467
    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    iget v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    sub-int v11, v4, v11

    .line 468
    invoke-static {v9, v10, v11}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v9

    add-int/2addr v8, v9

    .line 469
    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v9, v8, v9

    .line 470
    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    sub-int v13, v5, v13

    iget-object v14, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    .line 472
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    mul-int/lit8 v14, v14, 0x2

    add-int/2addr v13, v14

    .line 471
    invoke-static {v11, v12, v13}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v11

    add-int/2addr v10, v11

    .line 473
    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    iget-object v12, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    .line 476
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v12, v8

    iget-object v13, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    .line 477
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v7

    .line 473
    invoke-virtual {v11, v8, v7, v12, v13}, Landroid/view/View;->layout(IIII)V

    .line 478
    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    .line 481
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    add-int/2addr v11, v9

    iget-object v12, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    .line 482
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v10

    .line 478
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 498
    :goto_5
    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 499
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:Landroid/view/View;

    if-eqz v4, :cond_c

    .line 500
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:Landroid/view/View;

    .line 503
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v6

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:Landroid/view/View;

    .line 504
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v7

    .line 500
    invoke-virtual {v4, v6, v7, v5, v8}, Landroid/view/View;->layout(IIII)V

    .line 506
    :cond_c
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 510
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v6

    .line 511
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v7

    .line 507
    invoke-virtual {v4, v6, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    goto :goto_6

    .line 485
    :cond_d
    if-eqz v8, :cond_e

    .line 486
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    goto :goto_5

    .line 488
    :cond_e
    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:F

    const/4 v10, 0x0

    iget v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:I

    iget-object v12, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/graphics/Rect;

    .line 489
    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v12

    add-int/2addr v11, v12

    .line 488
    invoke-static {v9, v10, v11}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v9

    add-int/2addr v8, v9

    .line 490
    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    .line 493
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v10, v6

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    .line 494
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v11, v8

    .line 490
    invoke-virtual {v9, v6, v8, v10, v11}, Landroid/view/View;->layout(IIII)V

    goto :goto_5
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 385
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 390
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 391
    if-eq v0, v7, :cond_0

    .line 392
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "WatchWhileLayout can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 394
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 395
    if-eq v0, v7, :cond_1

    .line 396
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "WatchWhileLayout can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 399
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 400
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 401
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-eqz v2, :cond_3

    .line 402
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    .line 403
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 404
    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 402
    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    .line 438
    :cond_2
    return-void

    .line 406
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 407
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 408
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 409
    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 411
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    .line 412
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/graphics/Rect;

    .line 413
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 411
    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    .line 414
    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->N:Z

    if-nez v4, :cond_2

    .line 415
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 416
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 417
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    .line 418
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 417
    invoke-virtual {v4, v0, v3}, Landroid/view/View;->measure(II)V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    .line 421
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    .line 422
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 420
    invoke-virtual {v0, v4, v1}, Landroid/view/View;->measure(II)V

    .line 429
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 433
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 434
    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    goto :goto_1

    .line 425
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Landroid/graphics/Rect;

    .line 427
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 425
    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 953
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v1, Lbzs;->b:Lbzs;

    if-ne v0, v1, :cond_1

    .line 954
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 956
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1372
    instance-of v0, p1, Lbzq;

    if-nez v0, :cond_0

    .line 1373
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1380
    :goto_0
    return-void

    .line 1377
    :cond_0
    check-cast p1, Lbzq;

    .line 1378
    invoke-virtual {p1}, Lbzq;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1379
    invoke-static {p1}, Lbzq;->a(Lbzq;)Lbzs;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1364
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1365
    new-instance v1, Lbzq;

    invoke-direct {v1, v0}, Lbzq;-><init>(Landroid/os/Parcelable;)V

    .line 1366
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l()Lbzs;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Lbzq;->a(Lbzq;Lbzs;)Lbzs;

    .line 1367
    return-object v1

    .line 1366
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 373
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 374
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(II)V

    .line 375
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1009
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    invoke-virtual {v0, p1}, Lbzo;->a(Landroid/view/MotionEvent;)V

    .line 1011
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 1057
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 1013
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d()V

    .line 1014
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    invoke-virtual {v0, p1}, Lbzo;->b(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 1018
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1020
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:I

    if-ne v0, v5, :cond_1

    .line 1021
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    invoke-virtual {v0, p1}, Lbzo;->c(Landroid/view/MotionEvent;)I

    move-result v0

    neg-int v0, v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v3, Lbzs;->c:Lbzs;

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    add-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(I)Z

    goto :goto_0

    .line 1023
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    invoke-virtual {v0, p1}, Lbzo;->c(Landroid/view/MotionEvent;)I

    move-result v3

    invoke-virtual {v0, p1}, Lbzo;->d(Landroid/view/MotionEvent;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lbzo;->a(II)I

    move-result v0

    neg-int v0, v0

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    add-int/2addr v0, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(I)Z

    goto :goto_0

    .line 1029
    :pswitch_3
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:I

    if-ne v0, v5, :cond_f

    .line 1030
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    invoke-virtual {v0, p1, v1, v1}, Lbzo;->a(Landroid/view/MotionEvent;IZ)I

    move-result v0

    .line 1031
    if-ne v0, v4, :cond_3

    move v4, v1

    :goto_1
    if-ne v0, v1, :cond_4

    move v3, v1

    :goto_2
    if-ne v0, v5, :cond_5

    move v0, v1

    :goto_3
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->t:I

    neg-int v6, v6

    if-ge v5, v6, :cond_8

    if-eqz v3, :cond_6

    move v2, v1

    move-object v0, p0

    :goto_4
    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e(Z)V

    .line 1043
    :cond_2
    :goto_5
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i()V

    .line 1044
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    iput v7, v0, Lbqx;->d:I

    goto :goto_0

    :cond_3
    move v4, v2

    .line 1031
    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    if-nez v0, :cond_7

    move v2, v1

    :cond_7
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d(Z)V

    goto :goto_5

    :cond_8
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->t:I

    if-le v5, v6, :cond_b

    if-eqz v4, :cond_9

    move v2, v1

    move-object v0, p0

    goto :goto_4

    :cond_9
    if-nez v0, :cond_a

    move v2, v1

    :cond_a
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d(Z)V

    goto :goto_5

    :cond_b
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    const/16 v6, -0x14

    if-ge v5, v6, :cond_c

    if-eqz v4, :cond_c

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d(Z)V

    goto :goto_5

    :cond_c
    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    const/16 v5, 0x14

    if-le v4, v5, :cond_d

    if-eqz v3, :cond_d

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d(Z)V

    goto :goto_5

    :cond_d
    if-nez v0, :cond_e

    move v2, v1

    move-object v0, p0

    goto :goto_4

    :cond_e
    move-object v0, p0

    goto :goto_4

    .line 1032
    :cond_f
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:I

    if-ne v0, v4, :cond_2

    .line 1033
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    invoke-virtual {v0, p1, v4, v1}, Lbzo;->a(Landroid/view/MotionEvent;IZ)I

    move-result v0

    .line 1034
    if-ne v0, v1, :cond_10

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v3, Lbzs;->b:Lbzs;

    if-ne v2, v3, :cond_10

    .line 1035
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(Z)V

    goto :goto_5

    .line 1036
    :cond_10
    if-ne v0, v4, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v2, Lbzs;->c:Lbzs;

    if-ne v0, v2, :cond_11

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:I

    if-ge v0, v2, :cond_11

    .line 1038
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Z)V

    goto :goto_5

    .line 1040
    :cond_11
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f(Z)V

    goto :goto_5

    .line 1048
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i()V

    .line 1049
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    iput v7, v0, Lbqx;->d:I

    goto/16 :goto_0

    .line 1053
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    invoke-virtual {v0, p1}, Lbzo;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 1011
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 2

    .prologue
    .line 1062
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 1063
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lbzo;

    const/4 v1, -0x1

    iput v1, v0, Lbqx;->d:I

    .line 1064
    return-void
.end method
