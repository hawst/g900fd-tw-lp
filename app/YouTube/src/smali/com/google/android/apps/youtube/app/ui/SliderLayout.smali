.class public Lcom/google/android/apps/youtube/app/ui/SliderLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field public a:Lbwq;

.field private final b:I

.field private c:Z

.field private d:I

.field private e:Z

.field private f:I

.field private g:I

.field private final h:[Landroid/view/View;

.field private final i:[I

.field private final j:[I

.field private final k:[I

.field private l:I

.field private m:Z

.field private n:I

.field private o:Landroid/widget/Scroller;

.field private p:Z

.field private q:Lbwr;

.field private r:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/high16 v5, -0x40800000    # -1.0f

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 137
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 97
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:Z

    .line 116
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    .line 120
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    .line 139
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/16 v4, 0x28

    invoke-static {v0, v4}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b:I

    .line 141
    new-instance v0, Landroid/widget/Scroller;

    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    .line 143
    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    .line 144
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    .line 145
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    .line 146
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:[I

    .line 147
    new-array v0, v3, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    .line 148
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:I

    .line 151
    sget-object v0, Lgvk;->j:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 153
    invoke-virtual {v4, v3, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 154
    if-ne v0, v1, :cond_1

    move v0, v1

    .line 155
    :goto_0
    if-eqz v0, :cond_2

    move v0, v3

    :goto_1
    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x21

    :goto_3
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f:I

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v0, :cond_5

    const/16 v0, 0x82

    :goto_4
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:I

    new-instance v0, Lbwr;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lbwr;-><init>(Lcom/google/android/apps/youtube/app/ui/SliderLayout;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    if-nez v0, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i(I)V

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->requestLayout()V

    .line 156
    :cond_0
    invoke-virtual {v4, v2, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(II)V

    .line 157
    invoke-virtual {v4, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(II)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    const/4 v3, 0x3

    invoke-virtual {v4, v3, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(I)I

    move-result v3

    aput v3, v0, v2

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    const/4 v3, 0x4

    invoke-virtual {v4, v3, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(I)I

    move-result v2

    aput v2, v0, v1

    .line 162
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 163
    return-void

    :cond_1
    move v0, v2

    .line 154
    goto :goto_0

    :cond_2
    move v0, v1

    .line 155
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    const/16 v0, 0x11

    goto :goto_3

    :cond_5
    const/16 v0, 0x42

    goto :goto_4
.end method

.method private a()I
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 503
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 508
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 509
    rsub-int/lit8 v0, v0, 0x1

    .line 511
    :cond_0
    return v0
.end method

.method private a(II)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 871
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c()I

    move-result v1

    .line 872
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    aput p2, v2, p1

    .line 873
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:[I

    rsub-int/lit8 v3, p1, 0x1

    if-gtz p2, :cond_0

    :goto_0
    aput v0, v2, v3

    .line 874
    return-void

    .line 873
    :cond_0
    sub-int/2addr v1, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 705
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Z

    if-eq v0, p1, :cond_0

    .line 706
    if-eqz p1, :cond_0

    .line 707
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(Z)V

    .line 709
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Z

    .line 713
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Z

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 714
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/SliderLayout;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    return v0
.end method

.method private b()I
    .locals 3

    .prologue
    .line 327
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c()I

    move-result v0

    .line 328
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sub-int/2addr v0, v1

    return v0
.end method

.method private static b(I)I
    .locals 2

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 210
    if-ne p0, v1, :cond_0

    .line 215
    :goto_0
    return v0

    .line 212
    :cond_0
    if-ne p0, v0, :cond_1

    .line 213
    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    move v0, v1

    .line 215
    goto :goto_0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 764
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c(I)I

    move-result v2

    if-eq v2, v3, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c(I)I

    move-result v2

    if-eq v2, v3, :cond_0

    :goto_0
    if-nez v0, :cond_1

    .line 773
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 764
    goto :goto_0

    .line 767
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:Z

    goto :goto_1
.end method

.method private c()I
    .locals 3

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 337
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    .line 338
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private c(I)I
    .locals 2

    .prologue
    .line 237
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "index must be 0 or 1"

    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    aget v0, v0, p1

    return v0

    .line 237
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 789
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()I

    .line 790
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:Lbwq;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:Lbwq;

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    .line 793
    :cond_0
    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()I

    move-result v0

    mul-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->invalidate()V

    .line 300
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 898
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:[I

    aget v0, v0, v1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 899
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v2, v2, v1

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    .line 900
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()I

    move-result v4

    if-ne v3, v4, :cond_0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 899
    :cond_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 903
    return-void

    :cond_1
    move v0, v1

    .line 898
    goto :goto_0
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:Lbwq;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:Lbwq;

    invoke-interface {v0, p1}, Lbwq;->a(I)V

    .line 345
    :cond_0
    return-void
.end method

.method private f(I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 737
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    add-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i(I)V

    .line 738
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:Z

    if-eqz v0, :cond_2

    .line 739
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()I

    move-result v2

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    sub-int/2addr v1, v2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a()I

    move-result v2

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1

    .line 741
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->requestLayout()V

    .line 743
    :cond_3
    return-void
.end method

.method private g(I)V
    .locals 0

    .prologue
    .line 781
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i(I)V

    .line 782
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->requestLayout()V

    .line 783
    return-void
.end method

.method private h(I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 811
    const/4 v0, 0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 812
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    .line 813
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    if-eqz v1, :cond_0

    .line 838
    :goto_0
    return-void

    .line 817
    :cond_0
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    sub-int v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 819
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getFocusedChild()Landroid/view/View;

    move-result-object v3

    .line 820
    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    aget-object v4, v4, v5

    if-ne v3, v4, :cond_1

    .line 821
    invoke-virtual {v3}, Landroid/view/View;->clearFocus()V

    .line 825
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()I

    move-result v3

    mul-int/2addr v0, v3

    .line 826
    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    sub-int v3, v0, v3

    .line 827
    mul-int/lit16 v5, v1, 0xc8

    .line 828
    if-nez v5, :cond_2

    .line 829
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 833
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_3

    .line 834
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 836
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 837
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->invalidate()V

    goto :goto_0
.end method

.method private i(I)V
    .locals 0

    .prologue
    .line 881
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    .line 882
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()V

    .line 883
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d()V

    .line 884
    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 2

    .prologue
    .line 245
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "index must be 0 or 1"

    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    .line 246
    if-eqz p2, :cond_1

    .line 250
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    .line 254
    :goto_1
    return-void

    .line 245
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 252
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d(I)V

    goto :goto_1
.end method

.method public final a(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 910
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "index must be 0 or 1"

    invoke-static {v0, v3}, Lb;->c(ZLjava/lang/Object;)V

    .line 911
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-ne v0, p1, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 910
    goto :goto_0

    :cond_1
    move v1, v2

    .line 911
    goto :goto_1
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    .line 545
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f:I

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-ne v0, v2, :cond_1

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-nez v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g(I)V

    .line 351
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->postInvalidate()V

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    if-eq v0, v3, :cond_0

    .line 353
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 354
    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    .line 355
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-eq v0, v1, :cond_0

    .line 356
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    .line 357
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e(I)V

    goto :goto_0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 531
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f:I

    if-ne p2, v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-ne v1, v0, :cond_0

    .line 532
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    .line 539
    :goto_0
    return v0

    .line 535
    :cond_0
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:I

    if-ne p2, v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-nez v1, :cond_1

    .line 536
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    goto :goto_0

    .line 539
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getChildCount()I

    move-result v0

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "SliderLayout must have 2 child views."

    invoke-static {v0, v3}, Lb;->c(ZLjava/lang/Object;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v1

    move v0, v2

    .line 170
    :goto_1
    if-ge v0, v4, :cond_1

    .line 171
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v3, v1}, Landroid/view/View;->setClickable(Z)V

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 167
    goto :goto_0

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    aget v0, v0, v1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->bringChildToFront(Landroid/view/View;)V

    .line 178
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()V

    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d()V

    .line 180
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v5, 0x3

    const/4 v7, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 560
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 615
    :goto_0
    return v1

    .line 564
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:Lbwq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:Lbwq;

    .line 566
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    iput v7, v0, Lbqx;->d:I

    goto :goto_0

    .line 571
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 615
    :cond_2
    :goto_1
    :pswitch_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Z

    goto :goto_0

    .line 577
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Z

    if-eqz v0, :cond_3

    move v1, v2

    .line 578
    goto :goto_0

    .line 583
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    iget v0, v4, Lbwr;->e:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_4

    if-ne v0, v5, :cond_7

    :cond_4
    move v3, v2

    :goto_2
    if-eq v0, v2, :cond_5

    if-ne v0, v5, :cond_8

    :cond_5
    move v0, v2

    :goto_3
    iget v5, v4, Lbqx;->d:I

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v5

    if-ltz v5, :cond_b

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-le v6, v5, :cond_b

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    if-eqz v3, :cond_12

    iget v3, v4, Lbqx;->c:F

    sub-float v3, v5, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v3, v3

    iget v7, v4, Lbqx;->a:I

    if-le v3, v7, :cond_9

    move v3, v2

    :goto_4
    or-int/lit8 v3, v3, 0x0

    :goto_5
    if-eqz v0, :cond_11

    iget v0, v4, Lbqx;->b:F

    sub-float v0, v6, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iget v7, v4, Lbqx;->a:I

    if-le v0, v7, :cond_a

    move v0, v2

    :goto_6
    or-int/2addr v0, v3

    :goto_7
    if-eqz v0, :cond_6

    iput v6, v4, Lbqx;->b:F

    iput v5, v4, Lbqx;->c:F

    move v1, v2

    :cond_6
    :goto_8
    if-eqz v1, :cond_2

    .line 584
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    goto :goto_1

    :cond_7
    move v3, v1

    .line 583
    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    move v3, v1

    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_6

    :cond_b
    iput v7, v4, Lbqx;->d:I

    goto :goto_8

    .line 590
    :pswitch_2
    sget-object v0, Lbwp;->a:[I

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:I

    add-int/lit8 v3, v3, -0x1

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_1

    move v0, v2

    :goto_9
    if-eqz v0, :cond_2

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    invoke-virtual {v0, p1}, Lbwr;->b(Landroid/view/MotionEvent;)V

    .line 597
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_10

    :goto_a
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    goto/16 :goto_1

    .line 590
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getLeft()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_c

    move v0, v2

    goto :goto_9

    :cond_c
    move v0, v1

    goto :goto_9

    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getTop()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_d

    move v0, v2

    goto :goto_9

    :cond_d
    move v0, v1

    goto :goto_9

    :pswitch_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getRight()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_e

    move v0, v2

    goto :goto_9

    :cond_e
    move v0, v1

    goto :goto_9

    :pswitch_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_f

    move v0, v2

    goto :goto_9

    :cond_f
    move v0, v1

    goto :goto_9

    :cond_10
    move v2, v1

    .line 597
    goto :goto_a

    .line 605
    :pswitch_7
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    .line 606
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    iput v7, v0, Lbqx;->d:I

    goto/16 :goto_1

    .line 610
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    invoke-virtual {v0, p1}, Lbwr;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    :cond_11
    move v0, v3

    goto/16 :goto_7

    :cond_12
    move v3, v1

    goto/16 :goto_5

    .line 571
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch

    .line 590
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 424
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 425
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 426
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 431
    :goto_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(Z)V

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:[I

    const/4 v1, 0x1

    aget v3, v0, v1

    .line 435
    const/4 v0, 0x2

    new-array v4, v0, [I

    .line 436
    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 437
    const/4 v0, 0x2

    new-array v6, v0, [I

    .line 438
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 439
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    aput v1, v4, v0

    .line 440
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    aput v1, v5, v0

    .line 438
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 428
    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 429
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    .line 442
    :cond_1
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 443
    sub-int v0, p5, p3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v7

    sub-int/2addr v0, v7

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v0, v7

    .line 444
    iget-boolean v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v7, :cond_2

    .line 446
    :goto_2
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v1, :cond_3

    .line 447
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v1

    .line 451
    :goto_3
    add-int/2addr v0, v1

    .line 454
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    .line 459
    const/4 v7, 0x0

    iget v8, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    sub-int v8, v1, v8

    aput v8, v6, v7

    .line 463
    :goto_4
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    const/4 v8, 0x3

    if-ne v7, v8, :cond_5

    .line 464
    const/4 v0, 0x1

    add-int/2addr v1, v2

    aput v1, v6, v0

    .line 471
    :goto_5
    const/4 v0, 0x0

    :goto_6
    const/4 v1, 0x2

    if-ge v0, v1, :cond_7

    .line 472
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v1, :cond_6

    .line 473
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    .line 474
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v2

    aget v3, v6, v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v7

    aget v8, v4, v0

    add-int/2addr v7, v8

    aget v8, v6, v0

    aget v9, v5, v0

    add-int/2addr v8, v9

    .line 473
    invoke-virtual {v1, v2, v3, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 471
    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_2
    move v0, v1

    .line 444
    goto :goto_2

    .line 449
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v1

    goto :goto_3

    .line 461
    :cond_4
    const/4 v7, 0x0

    aput v1, v6, v7

    goto :goto_4

    .line 466
    :cond_5
    const/4 v1, 0x1

    sub-int/2addr v0, v3

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    sub-int/2addr v0, v2

    aput v0, v6, v1

    goto :goto_5

    .line 476
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    aget v2, v6, v0

    .line 477
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v3

    aget v7, v6, v0

    aget v8, v4, v0

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v8

    aget v9, v5, v0

    add-int/2addr v8, v9

    .line 476
    invoke-virtual {v1, v2, v3, v7, v8}, Landroid/view/View;->layout(IIII)V

    goto :goto_7

    .line 481
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    if-eqz v0, :cond_8

    .line 482
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    .line 483
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    .line 485
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    .line 490
    :cond_8
    :goto_8
    return-void

    .line 487
    :cond_9
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g(I)V

    goto :goto_8
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 381
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 383
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 384
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:[I

    aget v4, v0, v3

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:[I

    aget v5, v0, v9

    .line 388
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingRight()I

    move-result v1

    sub-int v2, v0, v1

    .line 389
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingBottom()I

    move-result v1

    sub-int v1, v0, v1

    .line 390
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 391
    :goto_0
    new-array v6, v10, [I

    .line 394
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    aget v7, v7, v3

    if-ne v7, v10, :cond_1

    .line 399
    sub-int v7, v0, v5

    iget v8, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    sub-int/2addr v7, v8

    aput v7, v6, v3

    .line 403
    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    aget v7, v7, v9

    if-ne v7, v10, :cond_2

    .line 404
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    add-int/2addr v0, v5

    aput v0, v6, v9

    :goto_2
    move v0, v3

    .line 411
    :goto_3
    if-ge v0, v10, :cond_4

    .line 412
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v3, :cond_3

    .line 413
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    aget v5, v6, v0

    .line 414
    invoke-static {v5, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 413
    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 411
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_0
    move v0, v2

    .line 390
    goto :goto_0

    .line 401
    :cond_1
    sub-int v7, v0, v5

    aput v7, v6, v3

    goto :goto_1

    .line 406
    :cond_2
    sub-int/2addr v0, v4

    aput v0, v6, v9

    goto :goto_2

    .line 416
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v3, v3, v0

    aget v4, v6, v0

    invoke-static {v4, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 417
    invoke-static {v1, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 416
    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    goto :goto_4

    .line 420
    :cond_4
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 517
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 518
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    .line 522
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Landroid/view/View;

    aget-object v0, v1, v0

    .line 523
    if-eqz v0, :cond_1

    .line 524
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 526
    :goto_1
    return v0

    .line 520
    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    goto :goto_0

    .line 526
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 849
    check-cast p1, Lbqo;

    .line 850
    invoke-virtual {p1}, Lbqo;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 851
    iget v0, p1, Lbqo;->a:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    .line 852
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 853
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    .line 855
    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d(I)V

    .line 856
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 842
    new-instance v0, Lbqo;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lbqo;-><init>(Landroid/os/Parcelable;)V

    .line 843
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    iput v1, v0, Lbqo;->a:I

    .line 844
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 364
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 365
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 366
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    aget v1, v1, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(II)V

    .line 365
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 369
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v0, :cond_1

    if-ne p2, p4, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-nez v0, :cond_3

    if-eq p1, p3, :cond_3

    .line 372
    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:I

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d(I)V

    .line 374
    :cond_3
    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 375
    invoke-static {p2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 374
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->measure(II)V

    .line 376
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->requestLayout()V

    .line 377
    return-void

    .line 372
    :cond_4
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    invoke-virtual {v0, p1}, Lbwr;->a(Landroid/view/MotionEvent;)V

    .line 646
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 701
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 652
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 655
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    invoke-virtual {v0, p1}, Lbwr;->b(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 659
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Z

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    iget-object v1, v0, Lbwr;->f:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    iget-boolean v1, v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0, p1}, Lbwr;->d(Landroid/view/MotionEvent;)I

    move-result v0

    .line 663
    :goto_1
    if-gez v0, :cond_3

    .line 664
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    if-lez v1, :cond_0

    .line 665
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    neg-int v1, v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f(I)V

    goto :goto_0

    .line 661
    :cond_2
    invoke-virtual {v0, p1}, Lbwr;->c(Landroid/view/MotionEvent;)I

    move-result v0

    goto :goto_1

    .line 667
    :cond_3
    if-lez v0, :cond_0

    .line 668
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    sub-int/2addr v1, v2

    .line 669
    if-lez v1, :cond_0

    .line 670
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f(I)V

    goto :goto_0

    .line 677
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Z

    if-eqz v0, :cond_4

    .line 678
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    iget v1, v0, Lbwr;->e:I

    invoke-virtual {v0, p1, v1, v3}, Lbwr;->a(Landroid/view/MotionEvent;IZ)I

    move-result v0

    .line 679
    if-ne v0, v3, :cond_5

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-ne v1, v3, :cond_5

    .line 680
    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    .line 687
    :cond_4
    :goto_2
    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    .line 688
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    iput v5, v0, Lbqx;->d:I

    goto :goto_0

    .line 681
    :cond_5
    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-nez v0, :cond_6

    .line 682
    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    goto :goto_2

    .line 684
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:I

    div-int/lit8 v2, v0, 0x2

    add-int/2addr v1, v2

    div-int v0, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    goto :goto_2

    .line 692
    :pswitch_4
    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    .line 693
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    iput v5, v0, Lbqx;->d:I

    goto/16 :goto_0

    .line 697
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lbwr;

    invoke-virtual {v0, p1}, Lbwr;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 646
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 635
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 636
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Landroid/view/View;)I

    move-result v0

    .line 637
    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 638
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    .line 640
    :cond_0
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2

    .prologue
    .line 494
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Landroid/view/View;)I

    move-result v0

    .line 495
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 496
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    .line 497
    const/4 v0, 0x1

    .line 499
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
