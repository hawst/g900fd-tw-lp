.class public Lcom/google/android/apps/youtube/app/YouTubeApplication;
.super Lckz;
.source "SourceFile"

# interfaces
.implements Lfby;
.implements Lfdu;
.implements Lghd;
.implements Lgvc;


# instance fields
.field private c:Larh;

.field private d:Lexo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lckz;-><init>()V

    return-void
.end method

.method private q()Lcyc;
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c:Larh;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Larh;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Larh;-><init>(Landroid/content/ContentResolver;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c:Larh;

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c:Larh;

    return-object v0
.end method


# virtual methods
.method protected final a()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 95
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 96
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    .line 97
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.google.android.youtube.api.service.START"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v3, v2}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz v5, :cond_0

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v3, Landroid/content/pm/ServiceInfo;->processName:Ljava/lang/String;

    .line 98
    :goto_0
    if-nez v3, :cond_1

    move v0, v1

    .line 113
    :goto_1
    return v0

    .line 97
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    move v0, v1

    .line 104
    goto :goto_1

    .line 107
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 108
    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v6, v4, :cond_4

    .line 109
    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v1

    .line 113
    goto :goto_1
.end method

.method protected final b()Letc;
    .locals 2

    .prologue
    .line 140
    new-instance v0, Lare;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->q()Lcyc;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lare;-><init>(Landroid/content/Context;Lcyc;)V

    return-object v0
.end method

.method public final c()Lari;
    .locals 1

    .prologue
    .line 145
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    return-object v0
.end method

.method public final d()Ldov;
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    iget-object v0, v0, Lari;->b:Ldov;

    return-object v0
.end method

.method protected final e()V
    .locals 18

    .prologue
    .line 154
    invoke-super/range {p0 .. p0}, Lckz;->e()V

    .line 155
    move-object/from16 v0, p0

    iget-object v4, v0, Lckz;->a:Letc;

    .line 156
    invoke-super/range {p0 .. p0}, Lckz;->n()Lcla;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lari;

    .line 162
    invoke-virtual {v4}, Letc;->j()Lezn;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lezn;->a(Ljava/lang/Runnable;)V

    .line 164
    new-instance v2, Ldov;

    iget-object v3, v8, Lari;->an:Landroid/content/Context;

    iget-object v5, v8, Lari;->ap:Letc;

    iget-object v6, v8, Lari;->aq:Lcyc;

    invoke-direct {v2, v3, v5, v8, v6}, Ldov;-><init>(Landroid/content/Context;Letc;Lcla;Lcyc;)V

    iput-object v2, v8, Lari;->b:Ldov;

    iget-object v2, v8, Lari;->b:Ldov;

    iget-object v3, v2, Ldov;->b:Letc;

    invoke-virtual {v3}, Letc;->r()Ljava/lang/String;

    move-result-object v3

    iget-object v2, v2, Ldov;->b:Letc;

    invoke-virtual {v2}, Letc;->j()Lezn;

    move-result-object v2

    invoke-static {v3, v2}, La;->a(Ljava/lang/String;Lezn;)V

    .line 168
    iget-object v2, v8, Lari;->a:Lbca;

    iget-object v3, v8, Lari;->an:Landroid/content/Context;

    iget-object v5, v8, Lari;->ap:Letc;

    invoke-virtual {v2, v3, v5, v8}, Lbca;->a(Landroid/content/Context;Letc;Lari;)Ljava/util/List;

    .line 171
    invoke-virtual {v4}, Letc;->b()Lexd;

    move-result-object v7

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lckz;->a:Letc;

    invoke-virtual {v2}, Letc;->f()Lezj;

    move-result-object v6

    .line 173
    invoke-virtual {v8}, Lari;->aD()Lcst;

    move-result-object v9

    .line 177
    invoke-virtual {v4}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v10

    .line 178
    sput-object v10, Lgko;->c:Landroid/content/SharedPreferences;

    .line 181
    invoke-virtual {v4}, Letc;->i()Levn;

    move-result-object v11

    .line 182
    const-class v2, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-object/from16 v0, p0

    invoke-virtual {v11, v0, v2}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 185
    new-instance v3, Lazg;

    iget-object v2, v8, Lari;->P:Lezs;

    invoke-virtual {v2}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcqj;

    invoke-direct {v3, v2}, Lazg;-><init>(Lcqj;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazn;

    new-instance v12, Lazc;

    invoke-direct {v12}, Lazc;-><init>()V

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqi;)Lcql;

    move-result-object v2

    const-class v5, Lazx;

    invoke-interface {v2, v5}, Lcql;->a(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lazo;

    invoke-interface {v2, v5}, Lcql;->a(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lazl;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    new-instance v2, Lcyt;

    invoke-direct {v2}, Lcyt;-><init>()V

    iget-object v5, v3, Lazg;->a:Lcqj;

    const-class v12, Lazn;

    invoke-interface {v5, v12, v2}, Lcqj;->a(Ljava/lang/Class;Lcqi;)Lcql;

    move-result-object v5

    const-class v12, Lczk;

    invoke-interface {v5, v12}, Lcql;->a(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lazm;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lczo;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lczi;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lczh;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    iget-object v5, v3, Lazg;->a:Lcqj;

    const-class v12, Lbab;

    new-instance v13, Lazh;

    invoke-direct {v13, v3}, Lazh;-><init>(Lazg;)V

    invoke-interface {v5, v12, v2, v13}, Lcqj;->a(Ljava/lang/Class;Lcqi;Lewh;)Lcql;

    move-result-object v5

    const-class v12, Lczk;

    invoke-interface {v5, v12}, Lcql;->a(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lbab;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lczo;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lczi;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lczh;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    iget-object v5, v3, Lazg;->a:Lcqj;

    const-class v12, Lazp;

    new-instance v13, Lazf;

    invoke-direct {v13}, Lazf;-><init>()V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqi;)Lcql;

    move-result-object v5

    const-class v12, Lazq;

    invoke-interface {v5, v12}, Lcql;->a(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lazt;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lazr;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lazs;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v5

    const-class v12, Lazy;

    invoke-interface {v5, v12}, Lcql;->b(Ljava/lang/Class;)Lcql;

    iget-object v5, v3, Lazg;->a:Lcqj;

    const-class v12, Lczo;

    invoke-interface {v5, v12, v2}, Lcqj;->a(Ljava/lang/Class;Lcqi;)Lcql;

    move-result-object v2

    const-class v5, Lczk;

    invoke-interface {v2, v5}, Lcql;->a(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lbab;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lczo;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lczi;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lczh;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lcyu;

    new-instance v12, Lcyr;

    invoke-direct {v12}, Lcyr;-><init>()V

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqi;)Lcql;

    move-result-object v2

    const-class v5, Lczk;

    invoke-interface {v2, v5}, Lcql;->a(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lbab;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lczo;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lczi;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lczh;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Ldik;

    new-instance v12, Ldip;

    invoke-direct {v12}, Ldip;-><init>()V

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqi;)Lcql;

    move-result-object v2

    const-class v5, Ldij;

    invoke-interface {v2, v5}, Lcql;->a(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lbab;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lczo;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lczi;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    move-result-object v2

    const-class v5, Lczh;

    invoke-interface {v2, v5}, Lcql;->b(Ljava/lang/Class;)Lcql;

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazk;

    const-string v12, "app_l"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazu;

    const-string v12, "ct"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazl;

    const-string v12, "ol_i"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazm;

    const-string v12, "pl_int"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazx;

    const-string v12, "ol_i"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazv;

    const-string v12, "ct_li"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazw;

    const-string v12, "ct_li"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazt;

    const-string v12, "br_s"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazp;

    const-string v12, "br_r"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazo;

    const-string v12, "ol"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lbac;

    const-string v12, "ui_l"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lbab;

    const-string v12, "pl_int"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lbaa;

    const-string v12, "rurl_s"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazz;

    const-string v12, "rurl_r"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazq;

    const-string v12, "br_ld"

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lfrs;

    new-instance v12, Lazi;

    invoke-direct {v12, v3}, Lazi;-><init>(Lazg;)V

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v2, v3, Lazg;->a:Lcqj;

    const-class v5, Lazn;

    new-instance v12, Lazj;

    invoke-direct {v12, v3}, Lazj;-><init>(Lazg;)V

    invoke-interface {v2, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    new-instance v2, Lcyf;

    iget-object v5, v3, Lazg;->a:Lcqj;

    invoke-direct {v2, v5}, Lcyf;-><init>(Lcqj;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczj;

    const-string v13, "pl_i"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczm;

    const-string v13, "ps_s"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczl;

    const-string v13, "ps_r"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczq;

    const-string v13, "wn_s"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczp;

    const-string v13, "wn_r"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczg;

    const-string v13, "pc"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczf;

    const-string v13, "ad_vi"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczd;

    const-string v13, "ad_bl"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczn;

    const-string v13, "pl_s"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lgct;

    const-string v13, "mpl_s"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczk;

    const-string v13, "aft"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczo;

    const-string v13, "pl_int"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczi;

    const-string v13, "pl_ex"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczh;

    const-string v13, "pl_int"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczl;

    new-instance v13, Lcyi;

    invoke-direct {v13, v2}, Lcyi;-><init>(Lcyf;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lcze;

    new-instance v13, Lcyj;

    invoke-direct {v13, v2}, Lcyj;-><init>(Lcyf;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqm;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Ldac;

    new-instance v13, Lcyk;

    invoke-direct {v13, v2}, Lcyk;-><init>(Lcyf;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqm;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Ldac;

    new-instance v13, Lcyl;

    invoke-direct {v13, v2}, Lcyl;-><init>(Lcyf;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Ldam;

    new-instance v13, Lcym;

    invoke-direct {v13, v2}, Lcym;-><init>(Lcyf;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lgdq;

    new-instance v13, Lcyn;

    invoke-direct {v13, v2}, Lcyn;-><init>(Lcyf;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lczn;

    new-instance v13, Lcyo;

    invoke-direct {v13, v2}, Lcyo;-><init>(Lcyf;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v5, v2, Lcyf;->a:Lcqj;

    const-class v12, Lcyu;

    new-instance v13, Lcyp;

    invoke-direct {v13, v2}, Lcyp;-><init>(Lcyf;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    new-instance v2, Ldia;

    iget-object v5, v3, Lazg;->a:Lcqj;

    invoke-direct {v2, v5}, Ldia;-><init>(Lcqj;)V

    iget-object v5, v2, Ldia;->a:Lcqj;

    const-class v12, Ldig;

    const-string v13, "pwm_a"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Ldia;->a:Lcqj;

    const-class v12, Ldii;

    const-string v13, "pwm_c"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Ldia;->a:Lcqj;

    const-class v12, Ldij;

    const-string v13, "pwm_e"

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v5, v2, Ldia;->a:Lcqj;

    const-class v12, Ldim;

    new-instance v13, Ldib;

    invoke-direct {v13, v2}, Ldib;-><init>(Ldia;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v5, v2, Ldia;->a:Lcqj;

    const-class v12, Ldin;

    new-instance v13, Ldic;

    invoke-direct {v13, v2}, Ldic;-><init>(Ldia;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqm;)V

    iget-object v5, v2, Ldia;->a:Lcqj;

    const-class v12, Ldil;

    new-instance v13, Ldid;

    invoke-direct {v13, v2}, Ldid;-><init>(Ldia;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqm;)V

    iget-object v5, v2, Ldia;->a:Lcqj;

    const-class v12, Ldii;

    new-instance v13, Ldie;

    invoke-direct {v13, v2}, Ldie;-><init>(Ldia;)V

    invoke-interface {v5, v12, v13}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    new-instance v2, Ldhm;

    iget-object v3, v3, Lazg;->a:Lcqj;

    invoke-direct {v2, v3}, Ldhm;-><init>(Lcqj;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldgu;

    new-instance v12, Ldhl;

    invoke-direct {v12}, Ldhl;-><init>()V

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqi;)Lcql;

    move-result-object v3

    const-class v5, Ldgr;

    invoke-interface {v3, v5}, Lcql;->a(Ljava/lang/Class;)Lcql;

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldgz;

    new-instance v12, Ldhg;

    invoke-direct {v12}, Ldhg;-><init>()V

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqi;)Lcql;

    move-result-object v3

    const-class v5, Ldgy;

    invoke-interface {v3, v5}, Lcql;->a(Ljava/lang/Class;)Lcql;

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldgt;

    const-string v12, "pd_r"

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldgr;

    const-string v12, "pd_e"

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldhb;

    const-string v12, "pdt_r"

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldgx;

    const-string v12, "pdt_c"

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldgy;

    const-string v12, "pdt_e"

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldgs;

    new-instance v12, Ldhn;

    invoke-direct {v12, v2}, Ldhn;-><init>(Ldhm;)V

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldgt;

    new-instance v12, Ldho;

    invoke-direct {v12, v2}, Ldho;-><init>(Ldhm;)V

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldha;

    new-instance v12, Ldhp;

    invoke-direct {v12, v2}, Ldhp;-><init>(Ldhm;)V

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldgw;

    new-instance v12, Ldhq;

    invoke-direct {v12, v2}, Ldhq;-><init>(Ldhm;)V

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldhe;

    new-instance v12, Ldhr;

    invoke-direct {v12, v2}, Ldhr;-><init>(Ldhm;)V

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqm;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldhd;

    new-instance v12, Ldhs;

    invoke-direct {v12, v2}, Ldhs;-><init>(Ldhm;)V

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqm;)V

    iget-object v3, v2, Ldhm;->a:Lcqj;

    const-class v5, Ldhc;

    new-instance v12, Ldht;

    invoke-direct {v12, v2}, Ldht;-><init>(Ldhm;)V

    invoke-interface {v3, v5, v12}, Lcqj;->a(Ljava/lang/Class;Lcqk;)V

    .line 188
    new-instance v2, Lazn;

    invoke-direct {v2}, Lazn;-><init>()V

    invoke-virtual {v11, v2}, Levn;->c(Ljava/lang/Object;)V

    .line 190
    invoke-virtual {v8}, Lari;->S()Lcws;

    move-result-object v2

    invoke-virtual {v11, v2}, Levn;->a(Ljava/lang/Object;)V

    .line 192
    invoke-virtual {v8}, Lari;->X()Lcwq;

    move-result-object v2

    invoke-virtual {v11, v2}, Levn;->a(Ljava/lang/Object;)V

    .line 193
    invoke-virtual {v8}, Lari;->aR()Lcwg;

    move-result-object v2

    invoke-virtual {v11, v2}, Levn;->a(Ljava/lang/Object;)V

    .line 194
    invoke-virtual {v8}, Lari;->al()Ldmc;

    move-result-object v2

    invoke-virtual {v11, v2}, Levn;->a(Ljava/lang/Object;)V

    .line 196
    iget-object v2, v8, Lari;->ad:Lezs;

    invoke-virtual {v2}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldgj;

    iget-object v3, v8, Lari;->ab:Lezs;

    invoke-virtual {v3}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldgg;

    new-instance v5, Ldgm;

    iget-object v12, v2, Ldgj;->a:Levn;

    iget-object v13, v2, Ldgj;->b:Ljava/util/concurrent/Executor;

    iget-object v14, v2, Ldgj;->c:Ldaq;

    iget-object v2, v2, Ldgj;->d:Lcws;

    invoke-direct {v5, v12, v13, v14, v2}, Ldgm;-><init>(Levn;Ljava/util/concurrent/Executor;Ldaq;Lcws;)V

    invoke-static {v5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldgm;

    iput-object v2, v3, Ldgg;->a:Ldgm;

    .line 198
    invoke-virtual {v8}, Lari;->ak()Lbcc;

    move-result-object v2

    invoke-virtual {v11, v2}, Levn;->a(Ljava/lang/Object;)V

    .line 199
    iget-object v2, v8, Lari;->ak:Lezs;

    invoke-virtual {v2}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbnj;

    invoke-virtual {v11, v2}, Levn;->a(Ljava/lang/Object;)V

    .line 200
    invoke-virtual {v8}, Lari;->V()Ldhy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 201
    invoke-virtual {v8}, Lari;->V()Ldhy;

    move-result-object v2

    invoke-virtual {v11, v2}, Levn;->a(Ljava/lang/Object;)V

    .line 204
    :cond_0
    invoke-virtual {v8}, Lari;->T()Laxf;

    move-result-object v2

    invoke-interface {v2}, Laxf;->a()V

    .line 205
    invoke-virtual {v8}, Lari;->U()Laxk;

    move-result-object v2

    invoke-interface {v2}, Laxk;->a()V

    .line 209
    invoke-virtual {v8}, Lari;->K()Ldsn;

    move-result-object v2

    new-instance v3, Lbmy;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lbmy;-><init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;)V

    invoke-virtual {v2, v3}, Ldsn;->a(Ldso;)V

    .line 210
    invoke-virtual {v8}, Lari;->f()Larh;

    move-result-object v2

    invoke-virtual {v2}, Larh;->ac()Lfko;

    move-result-object v2

    iget-boolean v2, v2, Lfko;->b:Z

    if-eqz v2, :cond_1

    .line 211
    iget-object v2, v8, Lari;->b:Ldov;

    iget-object v2, v2, Ldov;->d:Lezs;

    invoke-virtual {v2}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldts;

    .line 212
    invoke-virtual {v8}, Lari;->S()Lcws;

    move-result-object v3

    .line 211
    iput-object v3, v2, Ldts;->a:Lcws;

    .line 217
    :cond_1
    invoke-virtual {v8}, Lari;->O()Lgng;

    .line 220
    invoke-virtual {v4}, Letc;->d()Lorg/apache/http/client/HttpClient;

    .line 221
    move-object/from16 v0, p0

    iget-object v2, v0, Lckz;->a:Letc;

    invoke-virtual {v2}, Letc;->b()Lexd;

    move-result-object v2

    invoke-interface {v2}, Lexd;->g()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.google.android.youtube.ManageNetworkUsageActivity"

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v5

    const/4 v12, 0x1

    if-eq v5, v12, :cond_2

    const-string v5, "Enabling network usage management"

    invoke-static {v5}, Lezp;->e(Ljava/lang/String;)V

    const/4 v5, 0x1

    const/4 v12, 0x1

    invoke-virtual {v3, v2, v5, v12}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 231
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lckz;->b:Z

    if-eqz v2, :cond_3

    .line 232
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "download_only_while_charging"

    const/4 v5, 0x1

    .line 233
    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "transfer_max_connections"

    const/4 v5, 0x3

    .line 234
    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 235
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 238
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lckz;->b:Z

    if-eqz v2, :cond_4

    invoke-super/range {p0 .. p0}, Lckz;->n()Lcla;

    move-result-object v2

    check-cast v2, Lari;

    invoke-virtual {v2}, Lari;->d()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    new-instance v3, Larc;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Larc;-><init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;[Ljava/io/File;)V

    invoke-virtual {v3}, Larc;->start()V

    .line 244
    :cond_4
    const-string v2, "1001680686"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, La;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 245
    const-string v2, "1001680686"

    const-string v3, "<Android_YT_Open_App>"

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v5}, La;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 252
    invoke-virtual {v8}, Lari;->n()Lws;

    move-result-object v2

    invoke-virtual {v2}, Lws;->a()V

    .line 253
    iget-object v2, v8, Lari;->V:Lezs;

    invoke-virtual {v2}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lws;

    invoke-virtual {v2}, Lws;->a()V

    .line 254
    iget-object v2, v8, Lari;->W:Lezs;

    invoke-virtual {v2}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lws;

    invoke-virtual {v2}, Lws;->a()V

    .line 256
    invoke-virtual {v8}, Lari;->L()Lfrp;

    move-result-object v2

    .line 257
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lckz;->b:Z

    invoke-interface {v7}, Lexd;->a()Z

    move-result v5

    .line 256
    if-nez v3, :cond_5

    iget-object v3, v2, Lfrp;->c:Landroid/content/SharedPreferences;

    const-string v12, "pending_notification_registration"

    const/4 v13, 0x0

    invoke-interface {v3, v12, v13}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    if-eqz v5, :cond_b

    invoke-virtual {v2}, Lfrp;->a()V

    iget-object v2, v2, Lfrp;->c:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "pending_notification_registration"

    const/4 v5, 0x0

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 260
    :cond_6
    :goto_0
    invoke-virtual {v4}, Letc;->g()Lexr;

    move-result-object v2

    new-instance v3, Lgnu;

    .line 261
    invoke-virtual {v8}, Lari;->p()Lgju;

    move-result-object v5

    invoke-direct {v3, v5}, Lgnu;-><init>(Lgju;)V

    .line 260
    invoke-virtual {v2, v3}, Lexr;->a(Lexq;)V

    .line 264
    invoke-virtual {v4}, Letc;->c()Lexz;

    move-result-object v5

    .line 265
    new-instance v2, Lgny;

    .line 266
    invoke-virtual {v8}, Lari;->p()Lgju;

    move-result-object v3

    invoke-direct {v2, v3, v7}, Lgny;-><init>(Lgju;Lexd;)V

    .line 265
    invoke-virtual {v5, v2}, Lexz;->a(Lexy;)V

    .line 268
    invoke-virtual {v6}, Lezj;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Lgnx;->a(J)Lead;

    move-result-object v2

    .line 267
    invoke-virtual {v5, v2}, Lexz;->a(Lead;)V

    .line 270
    new-instance v4, Lftw;

    .line 273
    invoke-virtual {v8}, Lari;->A()Lfhx;

    move-result-object v2

    invoke-direct {v4, v10, v2, v6}, Lftw;-><init>(Landroid/content/SharedPreferences;Lfhx;Lezj;)V

    .line 275
    invoke-virtual {v5, v4}, Lexz;->a(Lexy;)V

    .line 276
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 278
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->q()Lcyc;

    move-result-object v2

    invoke-interface {v2}, Lcyc;->D()Lfhw;

    move-result-object v2

    iget-object v2, v2, Lfhw;->a:Lfji;

    iget-object v7, v2, Lfji;->a:Lheg;

    if-eqz v7, :cond_7

    iget-object v7, v2, Lfji;->a:Lheg;

    iget-object v7, v7, Lheg;->b:Lhhi;

    if-eqz v7, :cond_7

    iget-object v7, v2, Lfji;->a:Lheg;

    iget-object v7, v7, Lheg;->b:Lhhi;

    iget-object v7, v7, Lhhi;->a:Lhtm;

    if-nez v7, :cond_c

    :cond_7
    const v2, 0x15180

    :cond_8
    :goto_1
    int-to-long v12, v2

    invoke-virtual {v3, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v12

    .line 277
    iget-object v2, v4, Lftw;->b:Lezj;

    invoke-virtual {v2}, Lezj;->b()J

    move-result-wide v2

    iget-object v7, v4, Lftw;->a:Landroid/content/SharedPreferences;

    invoke-static {v2, v3, v12, v13, v7}, Lftv;->a(JJLandroid/content/SharedPreferences;)J

    move-result-wide v2

    iget-object v7, v4, Lftw;->b:Lezj;

    invoke-virtual {v7}, Lezj;->b()J

    move-result-wide v14

    const-wide/16 v16, 0x32

    sub-long v16, v2, v16

    cmp-long v7, v14, v16

    if-lez v7, :cond_9

    iget-object v2, v4, Lftw;->b:Lezj;

    invoke-virtual {v2}, Lezj;->b()J

    move-result-wide v2

    const-wide/16 v14, 0x2710

    add-long/2addr v2, v14

    :cond_9
    new-instance v4, Lead;

    invoke-direct {v4}, Lead;-><init>()V

    sget-object v7, Lftv;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lead;->a(Ljava/lang/String;)Lead;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lead;->a(J)Lead;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Lead;->b(J)Lead;

    move-result-object v2

    .line 276
    invoke-virtual {v5, v2}, Lexz;->a(Lead;)V

    .line 281
    invoke-virtual {v9}, Lcst;->b()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 282
    invoke-virtual {v8}, Lari;->O()Lgng;

    move-result-object v2

    .line 286
    invoke-virtual {v2}, Lgng;->b()V

    .line 290
    :cond_a
    invoke-virtual {v8}, Lari;->l()Lght;

    move-result-object v2

    invoke-virtual {v2}, Lght;->a()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 291
    new-instance v2, Lgob;

    .line 293
    invoke-virtual {v8}, Lari;->B()Lgjp;

    move-result-object v3

    .line 294
    iget-object v4, v8, Lari;->af:Lezs;

    invoke-virtual {v4}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lgll;

    .line 297
    invoke-virtual {v8}, Lari;->l()Lght;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lgob;-><init>(Lgjp;Lgll;Lexz;Lezj;Lght;)V

    .line 298
    invoke-virtual {v5, v2}, Lexz;->a(Lexy;)V

    .line 301
    invoke-virtual {v6}, Lezj;->a()J

    move-result-wide v2

    invoke-virtual {v8}, Lari;->l()Lght;

    move-result-object v4

    .line 300
    invoke-static {v2, v3, v4}, Lgob;->a(JLght;)Lead;

    move-result-object v2

    .line 299
    iget-object v3, v5, Lexz;->e:Ljava/util/concurrent/Executor;

    new-instance v4, Leyc;

    invoke-direct {v4, v5, v2}, Leyc;-><init>(Lexz;Lead;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 306
    :goto_2
    iget-object v2, v8, Lari;->O:Lezs;

    invoke-virtual {v2}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcpn;

    invoke-interface {v2, v11}, Lcpn;->a(Levn;)V

    .line 307
    new-instance v2, Lazk;

    invoke-direct {v2}, Lazk;-><init>()V

    invoke-virtual {v11, v2}, Levn;->d(Ljava/lang/Object;)V

    .line 311
    invoke-virtual {v8}, Lari;->aG()Lewi;

    move-result-object v2

    invoke-interface {v2}, Lewi;->e_()Ljava/lang/Object;

    .line 313
    new-instance v3, Lexo;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-super/range {p0 .. p0}, Lckz;->n()Lcla;

    move-result-object v2

    check-cast v2, Lari;

    invoke-virtual {v2}, Lari;->aQ()Lexn;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lckz;->a:Letc;

    invoke-virtual {v5}, Letc;->i()Levn;

    move-result-object v5

    invoke-direct {v3, v4, v2, v5}, Lexo;-><init>(Landroid/content/Context;Lexn;Levn;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d:Lexo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d:Lexo;

    iget-object v3, v2, Lexo;->b:Lexn;

    invoke-virtual {v3}, Lexn;->b()Z

    move-result v3

    iput-boolean v3, v2, Lexo;->c:Z

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "file"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v4, v2, Lexo;->a:Landroid/content/Context;

    invoke-virtual {v4, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 315
    invoke-virtual {v8}, Lari;->W()Ldhi;

    move-result-object v2

    invoke-interface {v2}, Ldhi;->a()V

    .line 318
    invoke-virtual {v8}, Lari;->ao()Lbnm;

    move-result-object v2

    .line 319
    invoke-virtual {v11, v2}, Levn;->a(Ljava/lang/Object;)V

    .line 320
    invoke-virtual {v2}, Lbnm;->a()V

    .line 321
    return-void

    .line 256
    :cond_b
    iget-object v2, v2, Lfrp;->c:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "pending_notification_registration"

    const/4 v5, 0x1

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 278
    :cond_c
    iget-object v2, v2, Lfji;->a:Lheg;

    iget-object v2, v2, Lheg;->b:Lhhi;

    iget-object v2, v2, Lhhi;->a:Lhtm;

    iget v2, v2, Lhtm;->a:I

    if-gtz v2, :cond_8

    const v2, 0x15180

    goto/16 :goto_1

    .line 303
    :cond_d
    sget-object v2, Lgnz;->b:Ljava/lang/String;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lb;->b()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v5, Lexz;->b:Levk;

    invoke-virtual {v2}, Levk;->a()V

    :try_start_0
    invoke-virtual {v5, v3}, Lexz;->a(Ljava/util/List;)V

    iget-object v2, v5, Lexz;->b:Levk;

    invoke-virtual {v2}, Levk;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, v5, Lexz;->b:Levk;

    invoke-virtual {v2}, Levk;->b()V

    goto/16 :goto_2

    :catchall_0
    move-exception v2

    iget-object v3, v5, Lexz;->b:Levk;

    invoke-virtual {v3}, Levk;->b()V

    throw v2
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 369
    const-string v0, "YouTube"

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 380
    sget-object v2, Lfhy;->b:Ljava/util/Set;

    .line 381
    iget-object v0, p0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "country"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 382
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 383
    invoke-static {v0}, La;->A(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 384
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 420
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v1, v0

    .line 389
    const-string v0, "phone"

    .line 390
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 391
    if-eqz v0, :cond_2

    .line 392
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 395
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 396
    invoke-static {v1}, La;->A(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 397
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 398
    goto :goto_0

    .line 404
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 405
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->f()Larh;

    move-result-object v0

    invoke-virtual {v0}, Larh;->x()Ljava/lang/String;

    move-result-object v0

    .line 406
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 407
    invoke-static {v0}, La;->A(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 408
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 415
    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->A(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 416
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 420
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lftg;
    .locals 1

    .prologue
    .line 479
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->ad()Lftg;

    move-result-object v0

    return-object v0
.end method

.method public handleChannelSubscribedEvent(Lbnz;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 462
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->af()Lwc;

    move-result-object v0

    invoke-interface {v0}, Lwc;->b()V

    .line 463
    return-void
.end method

.method public handleChannelUnsubscribedEvent(Lboa;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 470
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->af()Lwc;

    move-result-object v0

    invoke-interface {v0}, Lwc;->b()V

    .line 471
    return-void
.end method

.method public handleSignInEvent(Lfcb;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 428
    const-string v0, "User signed in"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 429
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->b()Lfxe;

    move-result-object v0

    invoke-interface {v0}, Lfxe;->c()V

    .line 430
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->J()Lcok;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcok;->a(J)V

    .line 431
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->af()Lwc;

    move-result-object v0

    invoke-interface {v0}, Lwc;->b()V

    .line 432
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->L()Lfrp;

    move-result-object v0

    invoke-virtual {v0}, Lfrp;->a()V

    .line 433
    return-void
.end method

.method public handleSignOutEvent(Lfcc;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 440
    const-string v0, "User signed out"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 441
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    iget-object v0, v0, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 442
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->b()Lfxe;

    move-result-object v0

    invoke-interface {v0}, Lfxe;->c()V

    .line 443
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->af()Lwc;

    move-result-object v0

    invoke-interface {v0}, Lwc;->b()V

    .line 444
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    invoke-virtual {v0}, Lari;->L()Lfrp;

    move-result-object v0

    invoke-virtual {v0}, Lfrp;->a()V

    .line 445
    iget-object v0, p0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->h()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lard;

    invoke-direct {v1, p0}, Lard;-><init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 455
    return-void
.end method

.method public final i()Lfdt;
    .locals 1

    .prologue
    .line 484
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    return-object v0
.end method

.method public final j()Lfbx;
    .locals 1

    .prologue
    .line 500
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    return-object v0
.end method

.method public final k()Lghc;
    .locals 1

    .prologue
    .line 505
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    return-object v0
.end method

.method public final l()Lgvb;
    .locals 1

    .prologue
    .line 510
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    return-object v0
.end method

.method protected final synthetic m()Lcla;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Lari;

    iget-object v1, p0, Lckz;->a:Letc;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->q()Lcyc;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lari;-><init>(Landroid/content/Context;Letc;Lcyc;)V

    return-object v0
.end method

.method public final bridge synthetic n()Lcla;
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    return-object v0
.end method

.method public final synthetic o()Lezg;
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    return-object v0
.end method

.method public onTrimMemory(I)V
    .locals 2

    .prologue
    .line 489
    invoke-super {p0, p1}, Lckz;->onTrimMemory(I)V

    .line 490
    invoke-super {p0}, Lckz;->n()Lcla;

    move-result-object v0

    check-cast v0, Lari;

    .line 493
    const/16 v1, 0xf

    if-ne p1, v1, :cond_0

    if-eqz v0, :cond_0

    .line 494
    invoke-virtual {v0}, Lari;->af()Lwc;

    move-result-object v0

    invoke-interface {v0}, Lwc;->b()V

    .line 496
    :cond_0
    return-void
.end method
