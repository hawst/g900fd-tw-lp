.class public final Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbi;


# static fields
.field static a:Ldbj;


# instance fields
.field public final b:Ldbj;

.field public final c:Landroid/media/RemoteControlClient;

.field public d:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

.field private final e:Ldba;

.field private final f:Landroid/media/AudioManager;

.field private final g:Landroid/content/ComponentName;

.field private h:Z

.field private i:Landroid/graphics/Bitmap;

.field private j:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldbj;Ldba;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b:Ldbj;

    .line 61
    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Ldba;

    .line 62
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->f:Landroid/media/AudioManager;

    .line 63
    new-instance v0, Landroid/content/ComponentName;

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14$RemoteControlIntentReceiver;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->g:Landroid/content/ComponentName;

    .line 65
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->g:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 69
    new-instance v1, Landroid/media/RemoteControlClient;

    invoke-direct {v1, v0}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    .line 70
    sget v0, Legz;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    new-instance v1, Lday;

    invoke-direct {v1, p0}, Lday;-><init>(Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;)V

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    .line 73
    :cond_0
    return-void
.end method

.method private static a(ZZ)I
    .locals 1

    .prologue
    .line 190
    const/16 v0, 0x8

    .line 191
    if-eqz p0, :cond_0

    .line 192
    const/16 v0, 0x9

    .line 194
    :cond_0
    if-eqz p1, :cond_1

    .line 195
    or-int/lit16 v0, v0, 0x80

    .line 197
    :cond_1
    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 116
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->h:Z

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v2}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->f:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->g:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->f:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Ldba;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Ldba;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    invoke-interface {v0, v1}, Ldba;->b(Landroid/media/RemoteControlClient;)V

    .line 124
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->h:Z

    .line 126
    :cond_1
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Ldbj;

    .line 127
    return-void
.end method

.method public final a(Ldbk;)V
    .locals 9

    .prologue
    const/16 v8, 0x12

    const/16 v2, 0x9

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 102
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->h:Z

    if-nez v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->f:Landroid/media/AudioManager;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->g:Landroid/content/ComponentName;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->f:Landroid/media/AudioManager;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Ldba;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Ldba;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    invoke-interface {v0, v4}, Ldba;->a(Landroid/media/RemoteControlClient;)V

    .line 108
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->h:Z

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b:Ldbj;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Ldbj;

    .line 111
    iget-object v0, p1, Ldbk;->b:Ldbl;

    sget-object v4, Ldaz;->a:[I

    invoke-virtual {v0}, Ldbl;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_0
    sget v4, Legz;->a:I

    if-lt v4, v8, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->d:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->d:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

    invoke-interface {v5}, Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;->onGetPlaybackPosition()J

    move-result-wide v6

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v0, v6, v7, v5}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    :goto_1
    iget-boolean v0, p1, Ldbk;->c:Z

    iget-boolean v4, p1, Ldbk;->d:Z

    iget-boolean v5, p1, Ldbk;->g:Z

    sget v6, Legz;->a:I

    if-lt v6, v8, :cond_7

    invoke-static {v0, v4}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a(ZZ)I

    move-result v0

    if-eqz v5, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->d:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

    if-eqz v4, :cond_2

    or-int/lit16 v0, v0, 0x100

    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    invoke-virtual {v4, v0}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    iget-object v4, p1, Ldbk;->a:Ljava/lang/String;

    iget-object v5, p1, Ldbk;->e:Landroid/graphics/Bitmap;

    iget-wide v6, p1, Ldbk;->f:J

    if-nez v5, :cond_8

    move v0, v1

    :goto_3
    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    invoke-virtual {v8, v0}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    const/4 v8, 0x7

    invoke-virtual {v0, v8, v4}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->j:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_3
    :goto_4
    if-eqz v5, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v1, :cond_5

    :cond_4
    iput-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    invoke-virtual {v5, v1, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->j:Landroid/graphics/Bitmap;

    const/16 v1, 0x64

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v3}, Landroid/media/RemoteControlClient$MetadataEditor;->putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;

    :cond_5
    invoke-virtual {v0, v2, v6, v7}, Landroid/media/RemoteControlClient$MetadataEditor;->putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;

    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    .line 112
    return-void

    .line 111
    :pswitch_0
    const/16 v0, 0x8

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    move v0, v1

    goto :goto_0

    :pswitch_4
    move v0, v2

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    invoke-virtual {v4, v0}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    goto :goto_1

    :cond_7
    invoke-static {v0, v4}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a(ZZ)I

    move-result v0

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_3

    :cond_9
    move v1, v3

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 130
    return-void
.end method
