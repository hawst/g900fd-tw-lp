.class public Lcom/google/android/apps/youtube/core/ui/PagedListView;
.super Ldnw;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private e:Landroid/widget/LinearLayout;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;)V
    .locals 7

    .prologue
    .line 35
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/ui/PagedListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILjava/lang/String;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v4, -0x1

    .line 43
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/ui/PagedListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILjava/lang/String;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILjava/lang/String;)V
    .locals 7

    .prologue
    .line 51
    const v1, 0x7f0400a7

    const v4, 0x7f0d00fb

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Ldnw;-><init>(ILandroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V

    .line 55
    sget-object v0, Lgvk;->f:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 56
    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->f:Z

    .line 57
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/ListView;

    .line 60
    sget-object v1, Lgvk;->f:[I

    const/4 v2, 0x0

    const v3, 0x7f0d00fb

    invoke-virtual {p1, p2, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 63
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 64
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 65
    if-nez v2, :cond_4

    .line 67
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 68
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 78
    :cond_0
    :goto_0
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 79
    if-eqz v2, :cond_1

    .line 80
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelector(I)V

    .line 83
    :cond_1
    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 84
    if-ltz v2, :cond_2

    .line 85
    packed-switch v2, :pswitch_data_0

    .line 88
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 92
    :cond_2
    :goto_1
    if-gez p4, :cond_3

    .line 93
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p4

    .line 95
    :cond_3
    invoke-virtual {v0, p4}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 97
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 99
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 100
    return-void

    .line 72
    :cond_4
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 73
    if-lez v3, :cond_0

    .line 74
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    goto :goto_0

    .line 86
    :pswitch_0
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto :goto_1

    .line 87
    :pswitch_1
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto :goto_1

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected final a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/ListView;

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->e:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    .line 170
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->e:Landroid/widget/LinearLayout;

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->e:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->e:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v5}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->e:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->f:Z

    if-eqz v0, :cond_1

    .line 177
    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 179
    :cond_1
    return-void
.end method

.method public final a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0, p1}, Ldnw;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 116
    return-void
.end method

.method public final a(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0, p1}, Ldnw;->a(Landroid/widget/ListAdapter;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 110
    return-void
.end method

.method protected final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 184
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->f:Z

    if-eqz v0, :cond_0

    .line 185
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 187
    :cond_0
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/ListView;

    .line 147
    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v0

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->d:Ldoc;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/PagedListView;->d:Ldoc;

    invoke-interface {v0, p2, p3, p4}, Ldoc;->a(III)V

    .line 130
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 135
    return-void
.end method
