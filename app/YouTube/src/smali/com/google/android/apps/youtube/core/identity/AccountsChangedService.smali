.class public Lcom/google/android/apps/youtube/core/identity/AccountsChangedService;
.super Landroid/app/Service;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/AccountsChangedService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lckz;

    invoke-virtual {v0}, Lckz;->n()Lcla;

    move-result-object v0

    invoke-virtual {v0}, Lcla;->aO()Lcub;

    move-result-object v0

    .line 19
    iget-object v1, v0, Lcub;->a:Lcst;

    invoke-virtual {v1}, Lcst;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcub;->a:Lcst;

    invoke-virtual {v1}, Lcst;->d()Lgit;

    move-result-object v1

    iget-object v1, v1, Lgit;->b:Lgiv;

    invoke-virtual {v1}, Lgiv;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcub;->c:Lfcd;

    invoke-virtual {v2, v1}, Lfcd;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "Account was removed from device"

    invoke-virtual {v0, v1}, Lcub;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v1, v0, Lcub;->g:Ljava/util/concurrent/Executor;

    new-instance v2, Lcuc;

    invoke-direct {v2, v0}, Lcuc;-><init>(Lcub;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 20
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/AccountsChangedService;->stopSelf()V

    .line 21
    const/4 v0, 0x2

    return v0
.end method
