.class public Lcom/google/android/apps/youtube/app/WatchWhileActivity;
.super Lavv;
.source "SourceFile"

# interfaces
.implements Lbqw;
.implements Lbvw;
.implements Lbzn;
.implements Lcap;
.implements Ldoa;
.implements Ldok;
.implements Ldon;


# instance fields
.field private A:Lexd;

.field private B:Lckt;

.field private C:Lery;

.field private D:Ljava/util/concurrent/Executor;

.field private E:Lcyc;

.field private F:Leqt;

.field private G:Lt;

.field private H:Lbgf;

.field private I:Landroid/support/v4/widget/DrawerLayout;

.field private J:Lbcs;

.field private K:I

.field private L:Landroid/app/ProgressDialog;

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:Lbgh;

.field private Q:Lgom;

.field private R:Z

.field private S:Lara;

.field private T:Ldsn;

.field private U:Z

.field private V:Z

.field private W:Lbcp;

.field private X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

.field private Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

.field private Z:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

.field private aa:Ldnz;

.field private ab:Lcik;

.field private ac:Z

.field private ad:Z

.field private ae:Ldoj;

.field private af:Ldom;

.field private ag:Z

.field private ah:Z

.field private ai:F

.field private aj:Z

.field private ak:I

.field private al:Lbrz;

.field private am:Lbyg;

.field private an:Lbka;

.field private ao:Lbxu;

.field private ap:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

.field private aq:Lffw;

.field private ar:Ldxz;

.field public e:Lbqv;

.field public f:Lcaq;

.field public g:Lcak;

.field public h:Lcao;

.field public i:Lbbp;

.field public j:Lfun;

.field public k:Lbgh;

.field private o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private p:Lari;

.field private q:Letc;

.field private r:Levn;

.field private s:Landroid/content/res/Resources;

.field private t:Landroid/content/SharedPreferences;

.field private u:Lcub;

.field private v:Lfha;

.field private w:Lftt;

.field private x:Lcst;

.field private y:Leyt;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Lavv;-><init>()V

    .line 313
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aj:Z

    .line 2380
    return-void
.end method

.method private D()V
    .locals 1

    .prologue
    .line 956
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ad:Z

    if-nez v0, :cond_0

    .line 957
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    .line 959
    :cond_0
    return-void

    .line 957
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()Z
    .locals 2

    .prologue
    .line 962
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->s:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private F()Z
    .locals 1

    .prologue
    .line 971
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ad:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private G()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1039
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcst;

    invoke-virtual {v0}, Lcst;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1040
    iput v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K:I

    .line 1041
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    .line 1042
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    new-instance v1, Lazx;

    invoke-direct {v1}, Lazx;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 1043
    const/16 v0, 0x408

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->showDialog(I)V

    .line 1045
    new-instance v0, Laqw;

    invoke-direct {v0, p0}, Laqw;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    .line 1062
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcst;

    invoke-virtual {v1}, Lcst;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1063
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->u:Lcub;

    invoke-virtual {v1, p0, v0}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    .line 1071
    :cond_0
    :goto_0
    return-void

    .line 1068
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->u:Lcub;

    iget-object v2, v1, Lcub;->j:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, v1, Lcub;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, v1, Lcub;->c:Lfcd;

    invoke-virtual {v0}, Lfcd;->a()[Landroid/accounts/Account;

    move-result-object v0

    array-length v2, v0

    if-ne v2, v3, :cond_2

    const/4 v2, 0x0

    aget-object v0, v0, v2

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, p0, v0}, Lcub;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1, p0}, Lcub;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private H()Z
    .locals 1

    .prologue
    .line 1279
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->isTaskRoot()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->U:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private I()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1283
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/youtube/app/honeycomb/Shell$HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    .line 1284
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private J()V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 1327
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N:Z

    if-eqz v0, :cond_0

    .line 1356
    :goto_0
    return-void

    .line 1331
    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K:I

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    .line 1333
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1334
    const/16 v3, 0x408

    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->dismissDialog(I)V

    .line 1337
    :cond_1
    const/4 v3, 0x3

    iput v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K:I

    .line 1338
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    .line 1339
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()V

    .line 1342
    if-eqz v0, :cond_2

    move v1, v2

    .line 1343
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Lbgh;

    if-eqz v2, :cond_4

    .line 1344
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Lbgh;

    or-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbgh;I)V

    .line 1353
    :goto_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Lbgh;

    .line 1355
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1331
    goto :goto_1

    .line 1345
    :cond_4
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    if-nez v0, :cond_6

    .line 1346
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lbgh;

    move-result-object v0

    or-int/lit8 v1, v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbgh;I)V

    .line 1348
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    new-instance v1, Lazw;

    invoke-direct {v1}, Lazw;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_2

    .line 1350
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()V

    goto :goto_2
.end method

.method private K()Lbgh;
    .locals 1

    .prologue
    .line 1394
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->A:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1395
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M()Lbgh;

    move-result-object v0

    .line 1398
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L()Lbgh;

    move-result-object v0

    goto :goto_0
.end method

.method private static L()Lbgh;
    .locals 2

    .prologue
    .line 1406
    const-string v0, "FEwhat_to_watch"

    .line 1407
    invoke-static {v0}, Lfia;->a(Ljava/lang/String;)Lhog;

    move-result-object v0

    const/4 v1, 0x0

    .line 1406
    invoke-static {v0, v1}, La;->a(Lhog;Z)Lbgh;

    move-result-object v0

    return-object v0
.end method

.method private M()Lbgh;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1414
    .line 1415
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 1416
    invoke-virtual {v0}, Lari;->ag()Lbku;

    move-result-object v0

    .line 1418
    :try_start_0
    invoke-virtual {v0}, Lbku;->a()Lfqd;

    move-result-object v0

    .line 1419
    if-eqz v0, :cond_2

    .line 1420
    iget-object v0, v0, Lfqd;->a:Lfpx;

    .line 1421
    if-eqz v0, :cond_2

    .line 1422
    invoke-virtual {v0}, Lfpx;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lfpx;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqe;

    iget-object v0, v0, Lfqe;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpz;

    iget-object v4, v0, Lfpz;->c:Lhog;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lfpz;->c:Lhog;

    iget-object v4, v4, Lhog;->s:Lhpa;

    if-eqz v4, :cond_1

    iget-object v0, v0, Lfpz;->c:Lhog;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v1, v0

    .line 1429
    :cond_2
    :goto_1
    if-eqz v1, :cond_4

    .line 1430
    invoke-static {v1}, La;->a(Lhog;)Lbgh;

    move-result-object v0

    .line 1433
    :goto_2
    return-object v0

    :cond_3
    move-object v0, v1

    .line 1422
    goto :goto_0

    .line 1425
    :catch_0
    move-exception v0

    .line 1426
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed to get offline guide: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 1433
    :cond_4
    invoke-static {}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L()Lbgh;

    move-result-object v0

    goto :goto_2
.end method

.method private N()V
    .locals 4

    .prologue
    .line 1591
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    if-nez v0, :cond_0

    .line 1592
    new-instance v0, Lbcs;

    invoke-direct {v0}, Lbcs;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    .line 1593
    const v0, 0x7f0801e9

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    const-string v2, "GuideFragment"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(ILj;Ljava/lang/String;I)V

    .line 1595
    :cond_0
    return-void
.end method

.method private O()Lbdx;
    .locals 2

    .prologue
    .line 1598
    .line 1599
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Lt;

    move-result-object v0

    const-string v1, "PaneFragment"

    invoke-virtual {v0, v1}, Lt;->a(Ljava/lang/String;)Lj;

    move-result-object v0

    check-cast v0, Lbdx;

    .line 1600
    if-eqz v0, :cond_0

    .line 1603
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private P()Lbdx;
    .locals 2

    .prologue
    .line 1607
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lbdx;

    move-result-object v0

    .line 1608
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbdx;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1611
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Q()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1790
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v1, :cond_0

    .line 1791
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v2, Lbzs;->a:Lbzs;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1793
    :cond_0
    return v0
.end method

.method private R()V
    .locals 2

    .prologue
    .line 2133
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lbdx;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbdx;->C()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 2134
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    iget-boolean v1, v0, Lbqv;->e:Z

    if-eqz v1, :cond_1

    iget-object v0, v0, Lbqv;->c:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->b(I)V

    .line 2136
    :cond_1
    return-void

    .line 2133
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private S()V
    .locals 2

    .prologue
    .line 2139
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    iget-object v0, v0, Lbqv;->c:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->c(I)V

    .line 2140
    return-void
.end method

.method private T()V
    .locals 5

    .prologue
    .line 2149
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E:Lcyc;

    .line 2150
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2151
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 2152
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 2153
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2154
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g:Lcak;

    iget-object v1, v1, Lcak;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v3, Lcak;->a:[I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2155
    iget v1, v2, Landroid/graphics/Point;->x:I

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int/2addr v1, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2156
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    iget-object v0, v2, Lbqv;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ljn;

    iget v3, v0, Ljn;->width:I

    if-eq v3, v1, :cond_0

    iput v1, v0, Ljn;->width:I

    iget-object v1, v2, Lbqv;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2157
    :cond_0
    const v0, 0x7f0801e9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->s:Landroid/content/res/Resources;

    const v2, 0x7f0700b4

    .line 2158
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2157
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2160
    return-void
.end method

.method private U()V
    .locals 2

    .prologue
    .line 2200
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0}, Lcao;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2201
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->l()V

    .line 2203
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v0, :cond_1

    .line 2204
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setEnabled(Z)V

    .line 2207
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ao:Lbxu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 2208
    return-void
.end method

.method private V()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2211
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0}, Lcao;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2212
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->l()V

    .line 2214
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v0, :cond_1

    .line 2215
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setEnabled(Z)V

    .line 2218
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ao:Lbxu;

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 2219
    return-void
.end method

.method private W()V
    .locals 1

    .prologue
    .line 2222
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2223
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->U()V

    .line 2227
    :goto_0
    return-void

    .line 2225
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V()V

    goto :goto_0
.end method

.method private X()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2244
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ag:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    .line 2246
    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lbvn;

    iget-boolean v2, v2, Lbvn;->d:Z

    if-nez v2, :cond_2

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->j()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 2247
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa:Ldnz;

    invoke-virtual {v0, v1}, Ldnz;->c(Z)V

    .line 2249
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B()Lbxu;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 2251
    :cond_1
    return-void

    .line 2246
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Y()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2291
    invoke-static {p0}, La;->s(Landroid/content/Context;)I

    move-result v1

    .line 2293
    invoke-static {p0}, La;->m(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->t:Landroid/content/SharedPreferences;

    const-string v3, "dogfood_warning_shown_version"

    .line 2295
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 2296
    :cond_0
    if-eqz v0, :cond_2

    .line 2297
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Lt;

    move-result-object v1

    .line 2298
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W:Lbcp;

    if-nez v0, :cond_1

    .line 2300
    const-string v0, "dogfood_warning"

    .line 2302
    invoke-virtual {v1, v0}, Lt;->a(Ljava/lang/String;)Lj;

    move-result-object v0

    check-cast v0, Lbcp;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W:Lbcp;

    .line 2305
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W:Lbcp;

    if-nez v0, :cond_2

    .line 2306
    new-instance v0, Lbcp;

    invoke-direct {v0}, Lbcp;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W:Lbcp;

    .line 2307
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W:Lbcp;

    const-string v2, "dogfood_warning"

    invoke-virtual {v0, v1, v2}, Lbcp;->a(Lt;Ljava/lang/String;)V

    .line 2310
    :cond_2
    return-void
.end method

.method private Z()Ldxz;
    .locals 2

    .prologue
    .line 2313
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ar:Ldxz;

    if-nez v0, :cond_0

    .line 2314
    new-instance v0, Ldxz;

    invoke-direct {v0}, Ldxz;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ar:Ldxz;

    .line 2315
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ar:Ldxz;

    new-instance v1, Laqx;

    invoke-direct {v1, p0}, Laqx;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    iput-object v1, v0, Ldxz;->a:Ldyb;

    .line 2333
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ar:Ldxz;

    return-object v0
.end method

.method private a(Landroid/content/Intent;)I
    .locals 7

    .prologue
    const/4 v1, 0x4

    const/4 v5, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 793
    .line 794
    const-string v0, "navigation_endpoint"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 796
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "navigation_endpoint"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 795
    invoke-static {v0}, Lfia;->a([B)Lhog;

    move-result-object v0

    .line 797
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lbbp;->a(Lhog;Ljava/lang/Object;)V

    .line 798
    const/4 v1, 0x5

    .line 838
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->U:Z

    if-nez v0, :cond_0

    .line 839
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    new-instance v2, Lazl;

    invoke-direct {v2}, Lazl;-><init>()V

    invoke-virtual {v0, v2}, Levn;->d(Ljava/lang/Object;)V

    .line 842
    :cond_0
    if-eq v1, v3, :cond_1

    const/4 v0, 0x6

    if-eq v1, v0, :cond_1

    .line 843
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    new-instance v2, Lazm;

    invoke-direct {v2}, Lazm;-><init>()V

    invoke-virtual {v0, v2}, Levn;->d(Ljava/lang/Object;)V

    .line 845
    :cond_1
    iput-boolean v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V:Z

    .line 846
    return v1

    .line 799
    :cond_2
    const-string v0, "pane"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 800
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "pane"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbgh;

    .line 801
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbgh;)V

    goto :goto_0

    .line 803
    :cond_3
    const-string v0, "watch"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 804
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "watch"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgom;

    .line 805
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lgom;)V

    move v1, v3

    .line 807
    goto :goto_0

    :cond_4
    const-string v0, "alias"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 809
    const-string v0, "alias"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 810
    const-class v6, Lcom/google/android/apps/youtube/app/honeycomb/Shell$HomeActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 811
    iput-boolean v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->U:Z

    move v0, v1

    .line 822
    :goto_1
    if-ne v0, v4, :cond_5

    const-string v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 823
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(Landroid/content/Intent;)Z

    move v0, v2

    .line 826
    :cond_5
    if-ne v0, v4, :cond_7

    .line 827
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_6

    const-string v1, "playlist_uri"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v0, "playlist_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    :cond_6
    if-eqz v0, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v1}, Lari;->v()Lffg;

    move-result-object v1

    invoke-virtual {v1}, Lffg;->a()Lftk;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lftk;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    new-instance v6, Lbaa;

    invoke-direct {v6}, Lbaa;-><init>()V

    invoke-virtual {v5, v6}, Levn;->d(Ljava/lang/Object;)V

    new-instance v5, Laqv;

    invoke-direct {v5, p0, v0, p1}, Laqv;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Landroid/net/Uri;Landroid/content/Intent;)V

    invoke-virtual {v1, v2, v5}, Lffg;->a(Lftk;Lwv;)V

    move v0, v4

    :goto_2
    if-eqz v0, :cond_d

    const/4 v0, 0x6

    :cond_7
    :goto_3
    move v1, v0

    .line 829
    goto/16 :goto_0

    .line 813
    :cond_8
    const-class v1, Lcom/google/android/apps/youtube/app/honeycomb/Shell$ResultsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 814
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(Landroid/content/Intent;)Z

    move v0, v2

    goto :goto_1

    .line 815
    :cond_9
    const-class v1, Lcom/google/android/apps/youtube/app/honeycomb/Shell$MediaSearchActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 816
    invoke-static {p1}, Lgom;->b(Landroid/content/Intent;)Lgom;

    move-result-object v0

    if-nez v0, :cond_a

    move v0, v5

    :goto_4
    if-eqz v0, :cond_b

    move v0, v3

    goto :goto_1

    :cond_a
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lgom;)V

    move v0, v4

    goto :goto_4

    :cond_b
    move v0, v4

    goto :goto_1

    :cond_c
    move v0, v5

    .line 827
    goto :goto_2

    :cond_d
    move v0, v4

    goto :goto_3

    .line 829
    :cond_e
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "query"

    .line 830
    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 834
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(Landroid/content/Intent;)Z

    move v1, v2

    goto/16 :goto_0

    :cond_f
    move v1, v4

    goto/16 :goto_0

    :cond_10
    move v0, v4

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 329
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    .line 330
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.action.MAIN"

    .line 331
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    .line 332
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Levn;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    return-object v0
.end method

.method private a(ILj;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1672
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G:Lt;

    invoke-virtual {v0}, Lt;->a()Laf;

    move-result-object v0

    .line 1673
    invoke-virtual {v0, p1, p2, p3}, Laf;->b(ILj;Ljava/lang/String;)Laf;

    .line 1674
    invoke-virtual {v0, p4}, Laf;->a(I)Laf;

    .line 1675
    invoke-virtual {v0}, Laf;->a()I

    .line 1676
    return-void
.end method

.method private a(Lbdx;Lbgh;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1618
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lbgh;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v1

    .line 1620
    :goto_0
    if-eqz p3, :cond_3

    .line 1621
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    invoke-virtual {v1}, Lbgf;->b()Lbge;

    .line 1637
    :cond_0
    :goto_1
    if-eqz v0, :cond_5

    .line 1638
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G:Lt;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lbdx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lt;->a(Lj;)Lm;

    move-result-object v0

    .line 1639
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    invoke-virtual {v1, v2, v0}, Lbgf;->a(Lbgh;Lm;)V

    .line 1650
    :cond_1
    :goto_2
    const v0, 0x7f0801e8

    const-string v1, "PaneFragment"

    const/16 v2, 0x2002

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(ILj;Ljava/lang/String;I)V

    .line 1652
    return-void

    :cond_2
    move v2, v0

    .line 1618
    goto :goto_0

    .line 1631
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    .line 1632
    iget-object v3, v3, Lbgh;->b:Landroid/os/Bundle;

    const-string v4, "no_history"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v2, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    .line 1633
    invoke-virtual {v3}, Lbgh;->b()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 1640
    :cond_5
    if-eqz v2, :cond_1

    .line 1644
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    iget-object v0, v0, Lbgd;->a:Lfah;

    invoke-virtual {v0}, Lfah;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbge;

    .line 1645
    if-eqz v0, :cond_1

    iget-object v0, v0, Lbge;->a:Landroid/os/Parcelable;

    check-cast v0, Lbgh;

    invoke-virtual {v0}, Lbgh;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1646
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()Lbge;

    goto :goto_2
.end method

.method private a(Lbge;)V
    .locals 4

    .prologue
    .line 1655
    iget-object v0, p1, Lbge;->a:Landroid/os/Parcelable;

    check-cast v0, Lbgh;

    invoke-virtual {v0}, Lbgh;->a()Lbdx;

    move-result-object v1

    .line 1656
    iget-object v0, p1, Lbge;->b:Landroid/os/Parcelable;

    check-cast v0, Lm;

    .line 1657
    if-eqz v0, :cond_0

    .line 1658
    invoke-virtual {v1, v0}, Lbdx;->a(Lm;)V

    .line 1660
    :cond_0
    const v0, 0x7f0801e8

    const-string v2, "PaneFragment"

    const/16 v3, 0x1001

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(ILj;Ljava/lang/String;I)V

    .line 1662
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lbgh;)V
    .locals 0

    .prologue
    .line 171
    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Lbgh;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x408

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->dismissDialog(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->A:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Leyt;

    invoke-interface {v0, p1}, Leyt;->c(Ljava/lang/Throwable;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()V

    :cond_1
    return-void

    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Lbbp;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    return-object v0
.end method

.method private b(F)V
    .locals 4

    .prologue
    .line 1872
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->s()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 1875
    const/high16 v0, 0x40a00000    # 5.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 1876
    iget v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ak:I

    if-eq v0, v1, :cond_0

    .line 1877
    iput v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ak:I

    .line 1878
    iget v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ak:I

    rsub-int/lit8 v0, v0, 0x5

    .line 1879
    add-int/lit8 v0, v0, 0x1

    int-to-double v0, v0

    .line 1880
    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4018000000000000L    # 6.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 1881
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v0, v1, v0

    .line 1882
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v1, v0}, Lcws;->a(F)V

    .line 1884
    :cond_0
    return-void
.end method

.method private b(Lbgh;)V
    .locals 2

    .prologue
    .line 1572
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    .line 1573
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    invoke-virtual {v0}, Lbcs;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1574
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbcs;->a(Z)V

    .line 1576
    :cond_0
    return-void
.end method

.method private b(Lgom;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1680
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 1681
    iget-object v0, v0, Lckz;->a:Letc;

    .line 1682
    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    new-instance v3, Lbab;

    invoke-direct {v3}, Lbab;-><init>()V

    .line 1683
    invoke-virtual {v0, v3}, Levn;->c(Ljava/lang/Object;)V

    .line 1685
    iget-object v0, p1, Lgom;->a:Lgoh;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    iget-object v3, v3, Lari;->b:Ldov;

    invoke-virtual {v3}, Ldov;->b()Ldwq;

    move-result-object v3

    invoke-interface {v3}, Ldwq;->i()Ldww;

    move-result-object v4

    invoke-virtual {v4}, Ldww;->a()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v5}, Lari;->f()Larh;

    move-result-object v5

    invoke-virtual {v5}, Larh;->ac()Lfko;

    move-result-object v5

    iget-boolean v5, v5, Lfko;->b:Z

    if-eqz v5, :cond_1

    if-eqz v4, :cond_4

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v3}, Ldwq;->q()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v3}, Ldwq;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_4

    iget-object v0, p1, Lgom;->b:Leaf;

    iget-boolean v0, v0, Leaf;->d:Z

    if-nez v0, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c(Lgom;)V

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 1724
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 1685
    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v5

    invoke-virtual {v5}, Lari;->S()Lcws;

    move-result-object v5

    if-eqz v4, :cond_2

    iget-object v4, p1, Lgom;->b:Leaf;

    iget-boolean v4, v4, Leaf;->d:Z

    if-nez v4, :cond_2

    invoke-virtual {v5, v0}, Lcws;->b(Lgoh;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v4, v0, Lgoh;->a:Leaa;

    iget-object v4, v4, Leaa;->c:Ljava/lang/String;

    invoke-static {v4}, Ldyh;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, v0, Lgoh;->a:Leaa;

    iget-object v4, v4, Leaa;->a:Ljava/lang/String;

    iget-object v5, v0, Lgoh;->a:Leaa;

    iget-object v5, v5, Leaa;->c:Ljava/lang/String;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget v0, v0, Leaa;->d:I

    invoke-interface {v3, v4, v5, v0}, Ldwq;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c(Lgom;)V

    move v0, v1

    goto :goto_1

    .line 1689
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    .line 1695
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1696
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    sget-object v2, Lbzs;->b:Lbzs;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    .line 1697
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    .line 1699
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Y:Z

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v2, v1}, Lcws;->d(Z)V

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lgom;)V

    .line 1723
    :goto_3
    iget-object v0, p1, Lgom;->b:Leaf;

    iget-boolean v0, v0, Leaf;->g:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ah:Z

    goto :goto_2

    .line 1701
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0}, Lcao;->j()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1704
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    sget-object v1, Lbzs;->b:Lbzs;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    .line 1718
    :cond_9
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lgom;)V

    goto :goto_3

    .line 1706
    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1707
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    sget-object v3, Lbzs;->c:Lbzs;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    .line 1709
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p1, Lgom;->b:Leaf;

    iget-boolean v0, v0, Leaf;->c:Z

    if-nez v0, :cond_9

    .line 1712
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Z)V

    .line 1713
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->e(Z)V

    .line 1714
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S:Lara;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lara;->sendEmptyMessage(I)Z

    goto :goto_4
.end method

.method private b(Landroid/content/Intent;)Z
    .locals 12

    .prologue
    const/4 v9, 0x1

    .line 913
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 914
    invoke-static {v0}, Lbnc;->b(Ljava/lang/String;)Lbnc;

    move-result-object v1

    .line 915
    invoke-static {v0}, Lbnc;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 916
    const-string v0, "selected_time_filter"

    .line 917
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lfxn;

    .line 919
    new-instance v0, Lbnf;

    .line 924
    invoke-static {v8}, Lbnh;->a(Lfxn;)Lbnh;

    move-result-object v2

    sget-object v3, Lbnf;->a:Lbnf;

    .line 925
    iget-object v3, v3, Lbnf;->d:Lbnd;

    sget-object v4, Lbnf;->a:Lbnf;

    .line 926
    iget-boolean v4, v4, Lbnf;->e:Z

    sget-object v5, Lbnf;->a:Lbnf;

    .line 927
    iget-boolean v5, v5, Lbnf;->f:Z

    sget-object v6, Lbnf;->a:Lbnf;

    .line 928
    iget-boolean v6, v6, Lbnf;->g:Z

    sget-object v7, Lbnf;->a:Lbnf;

    .line 929
    iget-boolean v7, v7, Lbnf;->h:Z

    sget-object v11, Lfxn;->a:Lfxn;

    if-ne v8, v11, :cond_0

    move v8, v9

    :goto_0
    invoke-direct/range {v0 .. v8}, Lbnf;-><init>(Lbnc;Lbnh;Lbnd;ZZZZZ)V

    .line 920
    invoke-static {v10, v0}, La;->a(Ljava/lang/String;Lbnf;)Lbgh;

    move-result-object v0

    .line 919
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbgh;)V

    .line 931
    return v9

    .line 929
    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()V

    return-void
.end method

.method private c(Lgom;)V
    .locals 3

    .prologue
    .line 1768
    new-instance v0, Lbem;

    invoke-direct {v0}, Lbem;-><init>()V

    .line 1769
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1770
    const-string v2, "watch"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1771
    invoke-virtual {v0, v1}, Li;->f(Landroid/os/Bundle;)V

    .line 1772
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Lt;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Li;->a(Lt;Ljava/lang/String;)V

    .line 1773
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Lcst;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcst;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Lexd;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->A:Lexd;

    return-object v0
.end method

.method private f(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 1359
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N:Z

    if-eqz v1, :cond_0

    .line 1384
    :goto_0
    return-void

    .line 1363
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1364
    const/16 v1, 0x408

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->dismissDialog(I)V

    .line 1366
    :cond_1
    iput v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K:I

    .line 1367
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    .line 1368
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()V

    .line 1371
    if-eqz p1, :cond_2

    .line 1372
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Lbgh;

    if-eqz v1, :cond_3

    .line 1373
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Lbgh;

    or-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbgh;I)V

    .line 1374
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Lbgh;

    .line 1383
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y()V

    goto :goto_0

    .line 1371
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1375
    :cond_3
    if-nez p1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    if-nez v1, :cond_5

    .line 1376
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lbgh;

    move-result-object v1

    or-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbgh;I)V

    .line 1378
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    new-instance v1, Lazv;

    invoke-direct {v1}, Lazv;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_2

    .line 1380
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()V

    goto :goto_2
.end method

.method public static synthetic g(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Lcub;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->u:Lcub;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Lbgh;
    .locals 1

    .prologue
    .line 171
    invoke-static {}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L()Lbgh;

    move-result-object v0

    return-object v0
.end method

.method private handleAddToToastActionEvent(Lfhd;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 1226
    .line 1228
    iget-object v0, p1, Lfhd;->b:Lfle;

    iget-object v1, v0, Lfle;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    iget-object v1, v0, Lfle;->a:Lhow;

    iget-object v1, v1, Lhow;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, v0, Lfle;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, v0, Lfle;->b:Ljava/lang/CharSequence;

    const/4 v1, 0x1

    .line 1226
    invoke-static {p0, v0, v1}, Leze;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    .line 1230
    return-void
.end method

.method private handleSequencerStageEvent(Lczu;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 1192
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1193
    iget-object v0, p1, Lczu;->a:Lgok;

    const/4 v1, 0x2

    new-array v1, v1, [Lgok;

    const/4 v2, 0x0

    sget-object v3, Lgok;->d:Lgok;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lgok;->e:Lgok;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lgok;->a([Lgok;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1197
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    sget-object v1, Lbzs;->c:Lbzs;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    .line 1199
    :cond_0
    return-void
.end method

.method private handleVideoControlsVisibilityEvent(Ldaa;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 1203
    iget-boolean v0, p1, Ldaa;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ag:Z

    .line 1204
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ag:Z

    if-eqz v0, :cond_1

    .line 1205
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-eqz v0, :cond_0

    .line 1206
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa:Ldnz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldnz;->c(Z)V

    .line 1211
    :cond_0
    :goto_0
    return-void

    .line 1209
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X()V

    goto :goto_0
.end method

.method private handleYpcTipDoneEvent(Lbzx;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 1221
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->onBackPressed()V

    .line 1222
    return-void
.end method

.method public static synthetic i(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Lbdx;
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lbdx;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()V

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 2281
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S:Lara;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lara;->sendEmptyMessage(I)Z

    .line 2283
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X()V

    .line 2284
    return-void
.end method

.method protected final a(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2231
    const/16 v0, 0x408

    if-ne p1, v0, :cond_1

    .line 2232
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 2233
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    .line 2234
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    const v1, 0x7f090281

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2235
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 2236
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 2238
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:Landroid/app/ProgressDialog;

    .line 2240
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1, p2}, Lavv;->a(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2034
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-eqz v0, :cond_0

    .line 2035
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    invoke-virtual {v0}, Ldcd;->g()V

    .line 2037
    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 5

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v4, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1947
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->e(Z)V

    .line 1951
    cmpl-float v0, p1, v3

    if-lez v0, :cond_1

    .line 1953
    iget v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ai:F

    const/high16 v1, 0x40400000    # 3.0f

    sub-float/2addr v1, p1

    mul-float/2addr v0, v1

    .line 1965
    :cond_0
    :goto_0
    iput v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ai:F

    .line 1967
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(F)V

    .line 1968
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a(Z)V

    .line 1970
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ao:Lbxu;

    invoke-virtual {v0, v4}, Lbxu;->a(Z)V

    .line 1971
    return-void

    .line 1956
    :cond_1
    cmpl-float v0, p1, v1

    if-lez v0, :cond_2

    .line 1958
    const/high16 v0, 0x3e800000    # 0.25f

    const/high16 v2, 0x3f400000    # 0.75f

    sub-float/2addr v3, p1

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    .line 1961
    :goto_1
    const/4 v2, 0x0

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_0

    cmpg-float v2, p1, v1

    if-gtz v2, :cond_0

    .line 1962
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g:Lcak;

    sub-float/2addr v1, p1

    invoke-virtual {v2, v1}, Lcak;->a(F)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lbgh;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1126
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1127
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K:I

    if-eq v0, v1, :cond_0

    .line 1128
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbgh;I)V

    .line 1132
    :goto_0
    return-void

    .line 1130
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Lbgh;

    goto :goto_0
.end method

.method public a(Lbgh;I)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1505
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_2

    move v5, v1

    .line 1506
    :goto_0
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_3

    move v4, v1

    .line 1507
    :goto_1
    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_4

    move v3, v1

    .line 1508
    :goto_2
    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_5

    move v0, v1

    .line 1509
    :goto_3
    iget-object v6, p1, Lbgh;->b:Landroid/os/Bundle;

    const-string v7, "guide_entry"

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 1511
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz v5, :cond_0

    .line 1512
    if-eqz v0, :cond_6

    .line 1515
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    sget-object v8, Lbzs;->c:Lbzs;

    invoke-virtual {v7, v8}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    .line 1521
    :cond_0
    :goto_4
    if-nez v3, :cond_7

    if-eqz v6, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H()Z

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    if-eqz v7, :cond_7

    .line 1524
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I()Landroid/content/Intent;

    move-result-object v0

    .line 1525
    const-string v1, "pane"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1526
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->startActivity(Landroid/content/Intent;)V

    .line 1564
    :cond_1
    :goto_5
    return-void

    :cond_2
    move v5, v2

    .line 1505
    goto :goto_0

    :cond_3
    move v4, v2

    .line 1506
    goto :goto_1

    :cond_4
    move v3, v2

    .line 1507
    goto :goto_2

    :cond_5
    move v0, v2

    .line 1508
    goto :goto_3

    .line 1517
    :cond_6
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v7, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(Z)V

    goto :goto_4

    .line 1529
    :cond_7
    or-int/2addr v3, v6

    or-int/2addr v4, v3

    .line 1535
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    .line 1536
    invoke-virtual {v3, p1}, Lbgh;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    .line 1537
    invoke-virtual {v3}, Lbgh;->b()Z

    move-result v3

    if-nez v3, :cond_a

    move v3, v1

    .line 1538
    :goto_6
    if-nez v4, :cond_8

    if-nez v3, :cond_9

    .line 1539
    :cond_8
    invoke-virtual {p1}, Lbgh;->a()Lbdx;

    move-result-object v3

    invoke-direct {p0, v3, p1, v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbdx;Lbgh;Z)V

    .line 1540
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(Lbgh;)V

    .line 1544
    :cond_9
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R:Z

    if-eqz v3, :cond_c

    .line 1545
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0, v1}, Lbqv;->b(Z)V

    .line 1546
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R:Z

    .line 1548
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1549
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->U()V

    goto :goto_5

    :cond_a
    move v3, v2

    .line 1537
    goto :goto_6

    .line 1551
    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V()V

    goto :goto_5

    .line 1553
    :cond_c
    if-eqz v0, :cond_d

    .line 1556
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R()V

    .line 1557
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    if-eqz v0, :cond_1

    .line 1559
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    invoke-virtual {v0, v1}, Lbcs;->a(Z)V

    goto :goto_5

    .line 1561
    :cond_d
    if-eqz v5, :cond_1

    .line 1562
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    goto :goto_5
.end method

.method public final a(Lbzs;)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1889
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    .line 1891
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v2

    .line 1892
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n()Z

    move-result v3

    .line 1894
    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-nez v4, :cond_7

    .line 1895
    if-eqz v2, :cond_0

    .line 1897
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1898
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    .line 1907
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1908
    invoke-direct {p0, v6}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(F)V

    .line 1909
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Z)V

    .line 1920
    :cond_1
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-nez v4, :cond_2

    if-eqz v3, :cond_2

    .line 1922
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ah:Z

    .line 1923
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setRequestedOrientation(I)V

    .line 1926
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-nez v3, :cond_3

    if-eqz v2, :cond_9

    .line 1927
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g:Lcak;

    invoke-virtual {v2, v6}, Lcak;->a(F)V

    .line 1929
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lbdx;

    move-result-object v2

    .line 1930
    if-eqz v2, :cond_4

    .line 1931
    invoke-virtual {v2}, Lbdx;->D()V

    .line 1937
    :cond_4
    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ap:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->t:Landroid/content/SharedPreferences;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a(Landroid/content/SharedPreferences;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aj:Z

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ao:Lbxu;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ap:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    invoke-virtual {v2, v3}, Lbxu;->a(Lbxy;)V

    .line 1938
    :cond_5
    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ao:Lbxu;

    invoke-virtual {v2, v0}, Lbxu;->a(Z)V

    .line 1940
    sget-object v2, Lbzs;->c:Lbzs;

    if-eq p1, v2, :cond_6

    sget-object v2, Lbzs;->a:Lbzs;

    if-ne p1, v2, :cond_b

    :cond_6
    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aj:Z

    .line 1942
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v2

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    iput-boolean v2, v3, Lbfm;->b:Z

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 1943
    return-void

    .line 1902
    :cond_7
    if-nez v2, :cond_0

    .line 1903
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    goto :goto_0

    .line 1912
    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->f(Z)V

    iget-object v5, v4, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v5}, Lcws;->b()V

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->C()V

    iget-object v5, v4, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a()V

    iget-object v4, v4, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lbgj;

    invoke-virtual {v4}, Lbgj;->b()Lbge;

    .line 1914
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T:Ldsn;

    iget-object v4, v4, Ldsn;->d:Ldwq;

    .line 1915
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ldwq;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1916
    invoke-interface {v4}, Ldwq;->c()V

    goto/16 :goto_1

    .line 1934
    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g:Lcak;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcak;->a(F)V

    goto :goto_2

    .line 1937
    :cond_a
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ap:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    goto :goto_3

    :cond_b
    move v0, v1

    .line 1940
    goto :goto_4
.end method

.method public final a(Ldwq;Z)V
    .locals 2

    .prologue
    .line 2078
    invoke-super {p0, p1, p2}, Lavv;->a(Ldwq;Z)V

    .line 2080
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lbdx;

    move-result-object v0

    .line 2081
    if-eqz v0, :cond_0

    instance-of v1, v0, Ldso;

    if-eqz v1, :cond_0

    .line 2082
    check-cast v0, Ldso;

    invoke-interface {v0, p1, p2}, Ldso;->a(Ldwq;Z)V

    .line 2086
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Ldwq;Z)V

    .line 2089
    return-void
.end method

.method public final a(Lgom;)V
    .locals 1

    .prologue
    .line 1157
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1158
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N:Z

    if-nez v0, :cond_0

    .line 1159
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(Lgom;)V

    .line 1160
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q:Lgom;

    .line 1164
    :goto_0
    return-void

    .line 1162
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q:Lgom;

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2196
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(I)V

    .line 2197
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 2046
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ae:Ldoj;

    invoke-virtual {v0}, Ldoj;->disable()V

    .line 2047
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2048
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2050
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2051
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setRequestedOrientation(I)V

    .line 2053
    :cond_0
    return-void
.end method

.method public final b_(Z)V
    .locals 0

    .prologue
    .line 2042
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    iget-object v0, v0, Lbgd;->a:Lfah;

    invoke-virtual {v0}, Lfah;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1140
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()Lbge;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbge;)V

    .line 1148
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    .line 1149
    :goto_1
    return-void

    .line 1141
    :cond_0
    if-eqz p1, :cond_1

    .line 1142
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->finish()V

    goto :goto_1

    .line 1146
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lbgh;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbgh;I)V

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1824
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-eq v1, p1, :cond_3

    .line 1825
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    .line 1827
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->l()V

    .line 1828
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-nez v1, :cond_0

    .line 1829
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v0, v2}, Landroid/view/Window;->setFlags(II)V

    .line 1832
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v1}, Lcao;->m()V

    .line 1833
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->closeOptionsMenu()V

    .line 1835
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    iget-object v3, v1, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    iput-boolean v2, v3, Lbfm;->a:Z

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    invoke-virtual {v1, v0}, Lbxu;->a(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa:Ldnz;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    invoke-virtual {v1, v2}, Ldnz;->b(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    iget-object v3, v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v4, Lbzs;->a:Lbzs;

    if-eq v3, v4, :cond_1

    iget-boolean v3, v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    if-eq v3, v2, :cond_1

    iput-boolean v2, v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:Z

    sget-object v2, Lbzs;->b:Lbzs;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->requestLayout()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E()Z

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X()V

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ac:Z

    if-nez v1, :cond_2

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setRequestedOrientation(I)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v1, v0}, Lbqv;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ae:Ldoj;

    invoke-virtual {v0}, Ldoj;->enable()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->f(Z)V

    .line 1837
    :cond_3
    return-void

    .line 1835
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa:Ldnz;

    invoke-virtual {v1, v0}, Ldnz;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    sget-object v2, Lbzs;->c:Lbzs;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R:Z

    if-nez v2, :cond_6

    const/4 v0, 0x1

    :cond_6
    invoke-virtual {v1, v0}, Lbqv;->b(Z)V

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 2064
    if-eqz p1, :cond_0

    .line 2065
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa:Ldnz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldnz;->c(Z)V

    .line 2069
    :goto_0
    return-void

    .line 2067
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X()V

    goto :goto_0
.end method

.method public final f()Lbrz;
    .locals 3

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->al:Lbrz;

    if-nez v0, :cond_0

    .line 1001
    new-instance v0, Lbrz;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->t:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 1004
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v2

    invoke-virtual {v2}, Lari;->P()Lbjx;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lbrz;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Landroid/content/SharedPreferences;Lbjx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->al:Lbrz;

    .line 1006
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->al:Lbrz;

    return-object v0
.end method

.method public final g()Lbyg;
    .locals 3

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->am:Lbyg;

    if-nez v0, :cond_0

    .line 1011
    new-instance v0, Lbyg;

    .line 1013
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 1014
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v2

    invoke-virtual {v2}, Lari;->ae()Lfdw;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lbyg;-><init>(Landroid/app/Activity;Lfhz;Lfdw;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->am:Lbyg;

    .line 1016
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->am:Lbyg;

    return-object v0
.end method

.method public final h()Lbka;
    .locals 10

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->an:Lbka;

    if-nez v0, :cond_0

    .line 1021
    new-instance v0, Lbka;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcst;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 1024
    invoke-virtual {v1}, Lari;->O()Lgng;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->u:Lcub;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Leyt;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->A:Lexd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 1028
    invoke-virtual {v1}, Lari;->P()Lbjx;

    move-result-object v7

    .line 1029
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f()Lbrz;

    move-result-object v8

    .line 1030
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v9

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lbka;-><init>(Landroid/app/Activity;Lgix;Lgng;Lcub;Leyt;Lexd;Lbjx;Lbrz;Lbyg;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->an:Lbka;

    .line 1032
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->an:Lbka;

    return-object v0
.end method

.method public handleConnectivityChangeEvent(Lewk;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 1170
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    .line 1171
    return-void
.end method

.method public handleSignInEvent(Lfcb;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 1187
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f(Z)V

    .line 1188
    return-void
.end method

.method public handleSignOutEvent(Lfcc;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 1176
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    .line 1177
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()V

    .line 1179
    iget-boolean v0, p1, Lfcc;->a:Z

    if-eqz v0, :cond_0

    .line 1180
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G()V

    .line 1182
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1094
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lbdx;

    move-result-object v3

    .line 1095
    invoke-virtual {v3}, Lbdx;->z()Lbgh;

    move-result-object v4

    .line 1097
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    if-nez v0, :cond_2

    if-nez v4, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 1098
    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(Lbgh;)V

    .line 1100
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->l()V

    .line 1101
    invoke-virtual {v3}, Lbdx;->C()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1102
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    .line 1103
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0, v2}, Lbqv;->b(Z)V

    .line 1108
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    .line 1109
    return-void

    :cond_1
    move v0, v2

    .line 1097
    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    invoke-virtual {v0, v4}, Lbgh;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1105
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0, v1}, Lbqv;->b(Z)V

    goto :goto_1
.end method

.method protected final j()V
    .locals 1

    .prologue
    .line 1234
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M:Z

    if-eqz v0, :cond_0

    .line 1235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M:Z

    .line 1236
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->onSearchRequested()Z

    .line 1238
    :cond_0
    return-void
.end method

.method protected final j_()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 667
    invoke-super {p0}, Lavv;->j_()V

    .line 668
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N:Z

    .line 670
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcst;

    invoke-virtual {v0}, Lcst;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 672
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f(Z)V

    .line 689
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->g()Ljava/lang/String;

    move-result-object v3

    .line 690
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Ljava/lang/String;

    invoke-static {v0, v3}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 691
    :goto_1
    iput-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Ljava/lang/String;

    .line 692
    if-eqz v0, :cond_1

    .line 693
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    invoke-virtual {v0}, Lbgh;->a()Lbdx;

    move-result-object v0

    const v1, 0x7f0801e8

    const-string v3, "PaneFragment"

    invoke-direct {p0, v1, v0, v3, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(ILj;Ljava/lang/String;I)V

    .line 697
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q:Lgom;

    if-eqz v0, :cond_2

    .line 698
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q:Lgom;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lgom;)V

    .line 701
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    .line 702
    return-void

    .line 673
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcst;

    invoke-virtual {v0}, Lcst;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 674
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()V

    goto :goto_0

    .line 677
    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcst;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 678
    invoke-virtual {v0}, Lari;->q()Lfcs;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    .line 677
    invoke-virtual {v4}, Lcst;->b()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v4, Lcst;->a:Lctj;

    instance-of v0, v0, Lcsx;

    if-eqz v0, :cond_6

    iget-object v0, v4, Lcst;->a:Lctj;

    check-cast v0, Lcsx;

    invoke-virtual {v0}, Lcsx;->h()I

    move-result v7

    if-ne v7, v1, :cond_6

    invoke-virtual {v0}, Lcsx;->h()I

    move-result v7

    if-ne v7, v1, :cond_5

    iget-object v7, v0, Lcsx;->a:Landroid/content/SharedPreferences;

    const-string v8, "user_account"

    invoke-interface {v7, v8, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_5

    iget-object v0, v0, Lcsx;->a:Landroid/content/SharedPreferences;

    const-string v8, "user_identity"

    invoke-interface {v0, v8, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Lctv;

    invoke-direct {v0, v7, v3}, Lctv;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    if-eqz v0, :cond_6

    iget-object v3, v0, Lgit;->b:Lgiv;

    new-instance v7, Lcsv;

    invoke-direct {v7, v4, v0, v6}, Lcsv;-><init>(Lcst;Lctv;Levn;)V

    invoke-virtual {v5, v3, v7}, Lfcs;->a(Lgiv;Lwv;)V

    move v0, v1

    .line 683
    :goto_3
    if-nez v0, :cond_0

    .line 684
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G()V

    goto/16 :goto_0

    :cond_5
    move-object v0, v3

    .line 677
    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v2

    .line 690
    goto/16 :goto_1
.end method

.method public final k()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1242
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    iget-object v0, v0, Lbgd;->a:Lfah;

    invoke-virtual {v0}, Lfah;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1243
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    const/4 v0, 0x0

    :cond_0
    iget-object v1, v3, Lbgf;->a:Lfah;

    invoke-virtual {v1}, Lfah;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, v3, Lbgf;->a:Lfah;

    invoke-virtual {v0}, Lfah;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbge;

    iget-object v1, v0, Lbge;->a:Landroid/os/Parcelable;

    check-cast v1, Lbgh;

    if-eqz v4, :cond_3

    iget-object v5, v1, Lbgh;->a:Ljava/lang/Class;

    iget-object v6, v4, Lbgh;->a:Ljava/lang/Class;

    if-ne v5, v6, :cond_3

    iget-object v5, v1, Lbgh;->a:Ljava/lang/Class;

    const-class v6, Lbci;

    if-ne v5, v6, :cond_3

    iget-object v1, v1, Lbgh;->b:Landroid/os/Bundle;

    invoke-static {v1}, Lbgh;->a(Landroid/os/Bundle;)Lhog;

    move-result-object v1

    iget-object v5, v4, Lbgh;->b:Landroid/os/Bundle;

    invoke-static {v5}, Lbgh;->a(Landroid/os/Bundle;)Lhog;

    move-result-object v5

    invoke-static {v1, v5, v2}, Lfia;->a(Lhog;Lhog;Z)Z

    move-result v1

    :goto_0
    if-nez v1, :cond_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbge;)V

    .line 1244
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->l()V

    .line 1259
    :cond_2
    :goto_1
    const/4 v0, 0x1

    return v0

    :cond_3
    move v1, v2

    .line 1243
    goto :goto_0

    .line 1245
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1247
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I()Landroid/content/Intent;

    move-result-object v0

    .line 1248
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1249
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 1250
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-nez v0, :cond_2

    .line 1252
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    iget-object v0, v0, Lbqv;->c:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->e(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1253
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    goto :goto_1

    .line 1255
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R()V

    goto :goto_1
.end method

.method protected final l()V
    .locals 2

    .prologue
    .line 1264
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R:Z

    if-eqz v0, :cond_0

    .line 1265
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->d()V

    .line 1273
    :goto_0
    return-void

    .line 1266
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    iget-object v0, v0, Lbgd;->a:Lfah;

    invoke-virtual {v0}, Lfah;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1267
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqv;->a(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqv;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1268
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E:Lcyc;

    invoke-interface {v0}, Lcyc;->ae()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1269
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->d()V

    goto :goto_0

    .line 1271
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbqv;->a(Z)V

    goto :goto_0
.end method

.method public final m()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1776
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v1, :cond_0

    .line 1777
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v2, Lbzs;->b:Lbzs;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1779
    :cond_0
    return v0
.end method

.method public final n()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1783
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v1, :cond_0

    .line 1784
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:Lbzs;

    sget-object v2, Lbzs;->c:Lbzs;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1786
    :cond_0
    return v0
.end method

.method public final o()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1797
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v1, :cond_1

    .line 1798
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1800
    :cond_1
    return v0
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1456
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1457
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lbgj;

    iget-object v3, v3, Lbgd;->a:Lfah;

    invoke-virtual {v3}, Lfah;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lbgj;

    invoke-virtual {v3}, Lbgj;->a()Lbge;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v4}, Lcws;->b()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->A()V

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-object v0, v3, Lbge;->b:Landroid/os/Parcelable;

    check-cast v0, Ldlf;

    invoke-virtual {v4, v0}, Lcws;->a(Ldlf;)V

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 1488
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 1457
    goto :goto_0

    .line 1460
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ah:Z

    if-eqz v0, :cond_3

    .line 1461
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->finish()V

    goto :goto_1

    .line 1464
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-eqz v0, :cond_4

    .line 1465
    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    goto :goto_1

    .line 1468
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1469
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(Z)V

    goto :goto_1

    .line 1474
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0}, Lcao;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1475
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0}, Lcao;->l()V

    goto :goto_1

    .line 1479
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lbdx;

    move-result-object v0

    .line 1480
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    .line 1481
    invoke-virtual {v2}, Lbqv;->c()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1482
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    goto :goto_1

    .line 1483
    :cond_7
    if-eqz v0, :cond_8

    invoke-static {}, Lbdx;->G()Z

    .line 1485
    :cond_8
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c(Z)V

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 947
    invoke-super {p0, p1}, Lavv;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 948
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->D()V

    .line 950
    invoke-static {}, Lboi;->a()V

    .line 951
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->b()V

    .line 952
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T()V

    .line 953
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const v10, 0x7f080257

    const/4 v9, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 337
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lbhz;->a(I)Z

    .line 339
    invoke-super {p0, p1}, Lavv;->onCreate(Landroid/os/Bundle;)V

    .line 341
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q:Letc;

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->s:Landroid/content/res/Resources;

    .line 347
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Lt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G:Lt;

    .line 348
    new-instance v0, Lara;

    invoke-direct {v0, p0}, Lara;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S:Lara;

    .line 349
    iput-boolean v8, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N:Z

    .line 350
    iput v7, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K:I

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->f()Larh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E:Lcyc;

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->t:Landroid/content/SharedPreferences;

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->aO()Lcub;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->u:Lcub;

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->aD()Lcst;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcst;

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->ay()Leyt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Leyt;

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Ljava/lang/String;

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->K()Ldsn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T:Ldsn;

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q:Letc;

    invoke-virtual {v0}, Letc;->b()Lexd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->A:Lexd;

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    iget-object v0, v0, Lari;->J:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lery;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lery;

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q:Letc;

    invoke-virtual {v0}, Letc;->h()Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->D:Ljava/util/concurrent/Executor;

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->ah()Lckt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B:Lckt;

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->aj()Lffw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aq:Lffw;

    .line 365
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B()Lbxu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ao:Lbxu;

    .line 367
    new-instance v0, Lfha;

    invoke-direct {v0}, Lfha;-><init>()V

    const-class v1, Lgyv;

    new-instance v2, Lfhc;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    invoke-direct {v2, v3}, Lfhc;-><init>(Levn;)V

    invoke-virtual {v0, v1, v2}, Lfha;->a(Ljava/lang/Class;Lfgy;)V

    const-class v1, Lhil;

    new-instance v2, Lfhf;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    invoke-direct {v2, v3}, Lfhf;-><init>(Levn;)V

    invoke-virtual {v0, v1, v2}, Lfha;->a(Ljava/lang/Class;Lfgy;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Lfha;

    .line 368
    new-instance v1, Lftt;

    invoke-direct {v1}, Lftt;-><init>()V

    new-instance v2, Lbje;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    iget-object v0, v0, Lari;->x:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfeo;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Leyt;

    invoke-direct {v2, p0, v0, v3}, Lbje;-><init>(Landroid/app/Activity;Lfeo;Leyt;)V

    new-array v0, v7, [Ljava/lang/Class;

    const-class v3, Lhol;

    aput-object v3, v0, v8

    invoke-virtual {v1, v2, v0}, Lftt;->a(Lfts;[Ljava/lang/Class;)V

    new-instance v0, Lbiy;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v3}, Lari;->y()Lfdq;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Leyt;

    invoke-direct {v0, p0, v2, v3, v4}, Lbiy;-><init>(Landroid/app/Activity;Levn;Lfdq;Leyt;)V

    new-array v2, v7, [Ljava/lang/Class;

    const-class v3, Lhck;

    aput-object v3, v2, v8

    invoke-virtual {v1, v0, v2}, Lftt;->a(Lfts;[Ljava/lang/Class;)V

    new-instance v0, Lbjj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v3}, Lari;->w()Lfgj;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Leyt;

    invoke-direct {v0, p0, v2, v3, v4}, Lbjj;-><init>(Landroid/app/Activity;Levn;Lfgj;Leyt;)V

    new-array v2, v7, [Ljava/lang/Class;

    const-class v3, Lhfr;

    aput-object v3, v2, v8

    invoke-virtual {v1, v0, v2}, Lftt;->a(Lfts;[Ljava/lang/Class;)V

    new-instance v2, Lftr;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    iget-object v0, v0, Lari;->E:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdh;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Lfha;

    invoke-direct {v2, v0, v3, v4}, Lftr;-><init>(Lfdh;Levn;Lfha;)V

    new-array v0, v7, [Ljava/lang/Class;

    const-class v3, Lhgq;

    aput-object v3, v0, v8

    invoke-virtual {v1, v2, v0}, Lftt;->a(Lfts;[Ljava/lang/Class;)V

    new-instance v0, Lbjg;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B:Lckt;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    invoke-direct {v0, v2, v3}, Lbjg;-><init>(Lckt;Levn;)V

    new-array v2, v7, [Ljava/lang/Class;

    const-class v3, Lhfm;

    aput-object v3, v2, v8

    invoke-virtual {v1, v0, v2}, Lftt;->a(Lfts;[Ljava/lang/Class;)V

    new-instance v2, Lbix;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    iget-object v0, v0, Lari;->F:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdk;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Leyt;

    invoke-direct {v2, p0, v0, v3}, Lbix;-><init>(Landroid/content/Context;Lfdk;Leyt;)V

    new-array v0, v7, [Ljava/lang/Class;

    const-class v3, Lhgw;

    aput-object v3, v0, v8

    invoke-virtual {v1, v2, v0}, Lftt;->a(Lfts;[Ljava/lang/Class;)V

    new-instance v0, Lbji;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v3}, Lari;->S()Lcws;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3}, Lbji;-><init>(Landroid/app/Activity;Levn;Lcws;)V

    new-array v2, v7, [Ljava/lang/Class;

    const-class v3, Lhtt;

    aput-object v3, v2, v8

    invoke-virtual {v1, v0, v2}, Lftt;->a(Lfts;[Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->w:Lftt;

    .line 369
    new-instance v0, Lbbp;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 372
    invoke-virtual {v1}, Lari;->o()Lglm;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->w:Lftt;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 374
    invoke-virtual {v1}, Lari;->B()Lgjp;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 375
    invoke-virtual {v1}, Lari;->ap()Lgoc;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lbbp;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Levn;Lglm;Lftt;Lgjp;Lgoc;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    .line 376
    new-instance v0, Lcfd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    invoke-direct {v0, p0, v1}, Lcfd;-><init>(Landroid/app/Activity;Lfhz;)V

    .line 380
    new-instance v1, Lfun;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    invoke-direct {v1, p0, v2, v0}, Lfun;-><init>(Landroid/app/Activity;Lfhz;Lfut;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->j:Lfun;

    .line 384
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->j:Lfun;

    invoke-virtual {v0}, Lcfd;->a()Lcfa;

    move-result-object v0

    new-instance v2, Lcfb;

    invoke-direct {v2, v0, v1}, Lcfb;-><init>(Lcfa;Lfun;)V

    iput-object v2, v0, Lcfa;->b:Lfur;

    new-instance v2, Lcfc;

    invoke-direct {v2, v0, v1}, Lcfc;-><init>(Lcfa;Lfun;)V

    iput-object v2, v0, Lcfa;->a:Lfuq;

    .line 386
    if-eqz p1, :cond_3

    .line 387
    const-string v0, "back_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    iget-object v0, v0, Lbgf;->a:Lfah;

    invoke-virtual {v0}, Lfah;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbge;

    iget-object v0, v1, Lbge;->a:Landroid/os/Parcelable;

    check-cast v0, Lbgh;

    invoke-virtual {v0, v2}, Lbgh;->a(Ljava/lang/ClassLoader;)V

    iget-object v0, v1, Lbge;->b:Landroid/os/Parcelable;

    check-cast v0, Lm;

    :try_start_0
    const-class v1, Lm;

    const-string v4, "a"

    invoke-virtual {v1, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Set class loader hack failed."

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Set class loader hack failed."

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "Set class loader hack failed."

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 389
    :cond_0
    const-string v0, "current_descriptor"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbgh;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbgh;->a(Ljava/lang/ClassLoader;)V

    .line 396
    :goto_1
    invoke-static {p0}, La;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 397
    iput-boolean v7, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ac:Z

    .line 398
    iput-boolean v7, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ad:Z

    .line 399
    const v0, 0x7f040115

    .line 405
    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setContentView(I)V

    .line 407
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E:Lcyc;

    .line 411
    invoke-virtual {p0, v8}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setDefaultKeyMode(I)V

    .line 412
    new-instance v0, Lcax;

    invoke-direct {v0, p0}, Lcax;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    iget-object v1, p0, Lavv;->l:Lbgt;

    invoke-interface {v0, v1}, Lcao;->a(Lcap;)V

    .line 422
    new-instance v0, Lcar;

    invoke-direct {v0}, Lcar;-><init>()V

    const v1, 0x7f0901ed

    .line 423
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcar;->a:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->s:Landroid/content/res/Resources;

    const v2, 0x7f0700c6

    .line 424
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lcar;->c:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->s:Landroid/content/res/Resources;

    const v2, 0x7f0700c7

    .line 425
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lcar;->d:I

    const v1, 0x7f0d0139

    .line 426
    iput v1, v0, Lcar;->e:I

    const v1, 0x7f0d013a

    .line 427
    iput v1, v0, Lcar;->f:I

    .line 428
    iget-object v1, p0, Lavv;->l:Lbgt;

    invoke-virtual {v0, v1}, Lcar;->a(Lcam;)Lcar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    .line 429
    invoke-virtual {v0, v1}, Lcar;->a(Lcam;)Lcar;

    move-result-object v0

    new-instance v1, Lcay;

    .line 430
    iget-object v2, p0, Lbhz;->m:Lfus;

    invoke-direct {v1, v2}, Lcay;-><init>(Lfus;)V

    invoke-virtual {v0, v1}, Lcar;->a(Lcam;)Lcar;

    move-result-object v0

    new-instance v1, Lcaw;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 431
    invoke-virtual {v2}, Lari;->Y()Lbgm;

    move-result-object v2

    new-instance v3, Laqy;

    invoke-direct {v3, p0}, Laqy;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    invoke-direct {v1, p0, v2, v3}, Lcaw;-><init>(Landroid/app/Activity;Lbgm;Lewi;)V

    invoke-virtual {v0, v1}, Lcar;->a(Lcam;)Lcar;

    move-result-object v0

    new-instance v1, Laqz;

    invoke-direct {v1, p0}, Laqz;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    .line 432
    invoke-virtual {v0, v1}, Lcar;->a(Lcam;)Lcar;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Lcar;->a()Lcaq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    .line 435
    new-instance v1, Lcak;

    const v0, 0x7f0801e7

    .line 437
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 438
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C()Lcal;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    invoke-direct {v1, p0, v0, v2, v3}, Lcak;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Landroid/support/v7/widget/Toolbar;Lcal;Lcan;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g:Lcak;

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g:Lcak;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->s:Landroid/content/res/Resources;

    const v2, 0x7f0700d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lcak;->f:I

    .line 442
    const v0, 0x7f080255

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iput-object p0, v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:Lbzn;

    .line 445
    const v0, 0x7f0801e6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I:Landroid/support/v4/widget/DrawerLayout;

    .line 448
    new-instance v0, Lbqv;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I:Landroid/support/v4/widget/DrawerLayout;

    const v4, 0x7f090311

    const v5, 0x7f090312

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lbqv;-><init>(Lkp;Lbqw;Landroid/support/v4/widget/DrawerLayout;II)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    .line 456
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Lt;

    move-result-object v0

    const-string v1, "GuideFragment"

    invoke-virtual {v0, v1}, Lt;->a(Ljava/lang/String;)Lj;

    move-result-object v0

    check-cast v0, Lbcs;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Lbcs;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T()V

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    if-nez v0, :cond_1

    .line 461
    iput-boolean v7, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R:Z

    new-instance v0, Lbdf;

    invoke-direct {v0}, Lbdf;-><init>()V

    invoke-direct {p0, v0, v9, v7}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbdx;Lbgh;Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0, v8}, Lbqv;->b(Z)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(Lbgh;)V

    .line 466
    :cond_1
    new-instance v0, Leqt;

    const/16 v1, 0xb

    invoke-direct {v0, p0, v1, v9, v9}, Leqt;-><init>(Landroid/content/Context;ILjava/lang/String;Lu;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F:Leqt;

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F:Leqt;

    invoke-virtual {v0}, Leqt;->a()V

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G:Lt;

    invoke-virtual {v0, v10}, Lt;->a(I)Lj;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G:Lt;

    const v1, 0x7f080258

    invoke-virtual {v0, v1}, Lt;->a(I)Lj;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    .line 474
    new-instance v0, Lcik;

    .line 475
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    .line 476
    invoke-virtual {v2}, Lari;->ae()Lfdw;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    const v4, 0x7f080346

    .line 478
    invoke-virtual {p0, v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q:Letc;

    .line 479
    invoke-virtual {v5}, Letc;->b()Lexd;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q:Letc;

    .line 480
    invoke-virtual {v6}, Letc;->f()Lezj;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q:Letc;

    .line 481
    invoke-virtual {v7}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcik;-><init>(Lfhz;Lfdw;Lfrz;Landroid/view/ViewStub;Lexd;Lezj;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ab:Lcik;

    .line 482
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->al()Ldmc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ab:Lcik;

    invoke-virtual {v0, v1}, Ldmc;->addObserver(Ljava/util/Observer;)V

    .line 487
    new-instance v0, Ldnz;

    .line 488
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    .line 490
    iget-object v2, v2, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    invoke-direct {v0, v1, v9, v2, p0}, Ldnz;-><init>(Landroid/view/Window;Landroid/app/ActionBar;Ldef;Ldoa;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa:Ldnz;

    .line 492
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa:Ldnz;

    iput-boolean v8, v0, Ldnz;->a:Z

    invoke-virtual {v0}, Ldnz;->b()V

    .line 493
    new-instance v0, Ldoj;

    invoke-direct {v0, p0, p0}, Ldoj;-><init>(Landroid/app/Activity;Ldok;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ae:Ldoj;

    .line 495
    new-instance v0, Ldom;

    invoke-direct {v0, p0, p0}, Ldom;-><init>(Landroid/content/Context;Ldon;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->af:Ldom;

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-static {p0, v0}, La;->a(Landroid/app/Activity;Lbgo;)V

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->t:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a(Landroid/content/SharedPreferences;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v2, 0x7f040141

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080343

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ap:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ap:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ao:Lbxu;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->t:Landroid/content/SharedPreferences;

    invoke-virtual {p0, v10}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object p0, v1, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v3, v1, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->b:Landroid/content/SharedPreferences;

    iput-object v2, v1, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->c:Lbxu;

    iput-object v0, v1, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->d:Landroid/view/View;

    iput-object v4, v1, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->e:Landroid/view/View;

    .line 503
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E:Lcyc;

    .line 504
    const v0, 0x7f0801e8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0700b9

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 506
    return-void

    .line 392
    :cond_3
    new-instance v0, Lbgf;

    invoke-direct {v0}, Lbgf;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    goto/16 :goto_1

    .line 401
    :cond_4
    iput-boolean v8, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ac:Z

    .line 402
    iput-boolean v8, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ad:Z

    .line 403
    const v0, 0x7f0400aa

    goto/16 :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 751
    invoke-super {p0}, Lavv;->onDestroy()V

    .line 753
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F:Leqt;

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F:Leqt;

    invoke-virtual {v0}, Leqt;->b()V

    .line 757
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa:Ldnz;

    invoke-virtual {v0}, Ldnz;->a()V

    .line 758
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ae:Ldoj;

    invoke-virtual {v0}, Ldoj;->disable()V

    .line 759
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1295
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1306
    :goto_0
    return v0

    .line 1299
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lbdx;

    move-result-object v0

    .line 1300
    if-eqz v0, :cond_1

    instance-of v2, v0, Landroid/view/KeyEvent$Callback;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/view/KeyEvent$Callback;

    .line 1302
    invoke-interface {v0, p1, p2}, Landroid/view/KeyEvent$Callback;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1303
    goto :goto_0

    .line 1306
    :cond_1
    invoke-super {p0, p1, p2}, Lavv;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1311
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    .line 1312
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1323
    :goto_0
    return v0

    .line 1316
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lbdx;

    move-result-object v0

    .line 1317
    if-eqz v0, :cond_1

    instance-of v2, v0, Landroid/view/KeyEvent$Callback;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/view/KeyEvent$Callback;

    .line 1319
    invoke-interface {v0, p1, p2}, Landroid/view/KeyEvent$Callback;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1320
    goto :goto_0

    .line 1323
    :cond_1
    invoke-super {p0, p1, p2}, Lavv;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 771
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    if-eqz v0, :cond_1

    .line 772
    const-string v0, "background_failed"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 773
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    iput-boolean v2, v0, Layx;->e:Z

    .line 790
    :cond_0
    :goto_0
    return-void

    .line 778
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V:Z

    .line 779
    const-string v0, "com.google.android.youtube.action.search"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 780
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->onSearchRequested()Z

    goto :goto_0

    .line 784
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Landroid/content/Intent;)I

    move-result v0

    .line 785
    if-ne v0, v2, :cond_0

    .line 789
    invoke-super {p0, p1}, Lavv;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 706
    invoke-super {p0}, Lavv;->onPause()V

    .line 708
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N:Z

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0}, Lcao;->l()V

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0, p0}, Lcao;->b(Lcap;)V

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v1

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 714
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ae:Ldoj;

    invoke-virtual {v0}, Ldoj;->disable()V

    .line 716
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S:Lara;

    invoke-virtual {v0, v2}, Lara;->removeMessages(I)V

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S:Lara;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lara;->removeMessages(I)V

    .line 721
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ah:Z

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aq:Lffw;

    invoke-virtual {v0}, Lffw;->b()V

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 725
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    .line 726
    iget-object v0, v0, Lari;->a:Lbca;

    const-class v1, Larb;

    .line 727
    invoke-virtual {v0, v1}, Levy;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 728
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 732
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lery;

    iget-wide v2, v0, Lery;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    iput-wide v4, v0, Lery;->c:J

    .line 734
    :goto_1
    invoke-static {}, Lboi;->a()V

    .line 736
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z()Ldxz;

    move-result-object v1

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 737
    return-void

    .line 732
    :cond_1
    sget-wide v2, Lery;->b:J

    iput-wide v2, v0, Lery;->c:J

    goto :goto_1
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 565
    invoke-super {p0, p1}, Lavv;->onPostCreate(Landroid/os/Bundle;)V

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->a()V

    .line 569
    if-eqz p1, :cond_0

    .line 570
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    new-instance v1, Lazl;

    invoke-direct {v1}, Lazl;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 571
    const-string v0, "has_handled_intent"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V:Z

    .line 575
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V:Z

    if-nez v0, :cond_1

    .line 576
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 577
    if-eqz v0, :cond_1

    .line 578
    const-string v1, "com.google.android.youtube.action.search"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 579
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M:Z

    .line 585
    :cond_1
    :goto_0
    return-void

    .line 581
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 598
    invoke-super {p0}, Lavv;->onResume()V

    .line 603
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lery;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->D:Ljava/util/concurrent/Executor;

    iget-wide v2, v0, Lery;->a:J

    iput-wide v2, v0, Lery;->c:J

    new-instance v2, Lesc;

    invoke-direct {v2, v0}, Lesc;-><init>(Lery;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F:Leqt;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Leqt;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0, p0}, Lcao;->a(Lcap;)V

    .line 619
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->l()V

    .line 621
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->D()V

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ae:Ldoj;

    invoke-virtual {v0}, Ldoj;->enable()V

    .line 624
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T:Ldsn;

    iget-object v3, v0, Ldsn;->d:Ldwq;

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->b:Ldww;

    if-ne v0, v1, :cond_0

    invoke-interface {v3}, Ldwq;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lgoh;

    invoke-interface {v3}, Ldwq;->o()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3}, Ldwq;->q()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3}, Ldwq;->p()I

    move-result v3

    const/4 v4, 0x0

    sget-object v5, Lgog;->d:Lgog;

    invoke-direct/range {v0 .. v5}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;IILgog;)V

    new-instance v1, Lgom;

    invoke-direct {v1, v0}, Lgom;-><init>(Lgoh;)V

    invoke-virtual {v1, v6}, Lgom;->c(Z)V

    invoke-virtual {v1, v6}, Lgom;->b(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(Lgom;)V

    .line 626
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 627
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    .line 628
    iget-object v0, v0, Lari;->a:Lbca;

    const-class v1, Larb;

    .line 629
    invoke-virtual {v0, v1}, Levy;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 630
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 634
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 635
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v1

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 637
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aq:Lffw;

    invoke-virtual {v0}, Lffw;->c()V

    .line 639
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    if-eqz v0, :cond_2

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Layx;

    iget-object v0, v0, Layx;->c:Layr;

    invoke-virtual {v0}, Layr;->a()V

    .line 643
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Levn;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z()Ldxz;

    move-result-object v1

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 644
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 763
    invoke-super {p0, p1}, Lavv;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 764
    const-string v0, "back_stack"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Lbgf;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 765
    const-string v0, "current_descriptor"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 766
    const-string v0, "has_handled_intent"

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 767
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 1082
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0}, Lcao;->k()V

    .line 1083
    const/4 v0, 0x1

    return v0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 589
    invoke-super {p0}, Lavv;->onStart()V

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->af:Ldom;

    iget-boolean v1, v0, Ldom;->c:Z

    if-nez v1, :cond_0

    iput-boolean v3, v0, Ldom;->c:Z

    iget-object v1, v0, Ldom;->b:Landroid/content/Context;

    iget-object v2, v0, Ldom;->a:Landroid/content/IntentFilter;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->T()Laxf;

    move-result-object v0

    invoke-interface {v0, v3}, Laxf;->a(Z)V

    .line 593
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B:Lckt;

    iput-boolean v3, v0, Lckt;->c:Z

    iget-object v1, v0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0}, Lckt;->c()V

    .line 594
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 741
    invoke-super {p0}, Lavv;->onStop()V

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa:Ldnz;

    invoke-virtual {v0, v2}, Ldnz;->c(Z)V

    .line 744
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->af:Ldom;

    iget-boolean v1, v0, Ldom;->c:Z

    if-eqz v1, :cond_0

    iput-boolean v2, v0, Ldom;->c:Z

    iput-boolean v2, v0, Ldom;->d:Z

    iget-object v1, v0, Ldom;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 745
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lari;

    invoke-virtual {v0}, Lari;->T()Laxf;

    move-result-object v0

    invoke-interface {v0, v2}, Laxf;->a(Z)V

    .line 746
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B:Lckt;

    iput-boolean v2, v0, Lckt;->c:Z

    invoke-virtual {v0}, Lckt;->d()V

    .line 747
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    .prologue
    .line 1289
    invoke-super {p0, p1}, Lavv;->onWindowFocusChanged(Z)V

    .line 1290
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    if-eqz p1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->W:Lcxb;

    iget-object v2, v2, Lcxb;->a:Lgfa;

    invoke-virtual {v1, v2}, Lcws;->a(Lgec;)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Z

    .line 1291
    :cond_0
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1808
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b()V

    .line 1809
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    .line 1810
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1813
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-eqz v0, :cond_1

    .line 1814
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    .line 1815
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1816
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    sget-object v1, Lbzs;->c:Lbzs;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lbzs;)V

    .line 1821
    :cond_0
    :goto_0
    return-void

    .line 1819
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(Z)V

    goto :goto_0
.end method

.method public final r()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1975
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->e(Z)V

    .line 1976
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a(Z)V

    .line 1978
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ao:Lbxu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 1979
    return-void
.end method

.method public final s()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1998
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1999
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    .line 2016
    :cond_0
    :goto_0
    return-void

    .line 2002
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    iget v0, v0, Lbqv;->f:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    .line 2006
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2007
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->X:Ldcd;

    invoke-virtual {v0}, Ldcd;->g()V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2002
    goto :goto_1

    .line 2008
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2010
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2011
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Z)V

    goto :goto_0

    .line 2013
    :cond_5
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    goto :goto_0
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 2025
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2026
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()V

    .line 2029
    :cond_0
    return-void
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 2058
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->af:Ldom;

    iget-boolean v0, v0, Ldom;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->ae:Ldoj;

    invoke-virtual {v0}, Ldoj;->disable()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setRequestedOrientation(I)V

    .line 2059
    :cond_0
    return-void
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 2073
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    invoke-virtual {v0}, Lari;->K()Ldsn;

    move-result-object v0

    invoke-virtual {v0}, Ldsn;->b()V

    .line 2074
    return-void
.end method

.method public final w()V
    .locals 0

    .prologue
    .line 2178
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->U()V

    .line 2179
    return-void
.end method

.method public final x()V
    .locals 0

    .prologue
    .line 2183
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V()V

    .line 2184
    return-void
.end method

.method public final y()V
    .locals 3

    .prologue
    .line 2257
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lbdx;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbdx;->b()Lcan;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    .line 2258
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g:Lcak;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcak;->d:Lcan;

    if-ne v2, v0, :cond_2

    invoke-virtual {v1}, Lcak;->c()V

    invoke-virtual {v1}, Lcak;->d()V

    .line 2259
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0}, Lcao;->n()V

    .line 2260
    return-void

    .line 2257
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    goto :goto_1

    .line 2258
    :cond_2
    iput-object v0, v1, Lcak;->d:Lcan;

    invoke-virtual {v1}, Lcak;->e()Lcaj;

    move-result-object v0

    iput-object v0, v1, Lcak;->e:Lcaj;

    invoke-virtual {v1}, Lcak;->c()V

    iget-object v0, v1, Lcak;->c:Lbqk;

    iget-object v2, v1, Lcak;->e:Lcaj;

    invoke-virtual {v0, v2, v1}, Lbqk;->a(Lbqm;Lbql;)V

    invoke-virtual {v1}, Lcak;->b()V

    invoke-virtual {v1}, Lcak;->d()V

    goto :goto_2
.end method

.method public final z()V
    .locals 0

    .prologue
    .line 2274
    return-void
.end method
