.class public Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;
.super Lbqr;
.source "SourceFile"

# interfaces
.implements Lbob;


# instance fields
.field private b:Lcom/google/android/apps/youtube/app/ui/ClingView;

.field private c:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lbqr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->c:Landroid/widget/LinearLayout;

    .line 38
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 39
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 40
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 41
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->b:Lcom/google/android/apps/youtube/app/ui/ClingView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/ClingView;->a(Landroid/view/View;Landroid/view/View;)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->postInvalidate()V

    .line 46
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Lbqr;->onFinishInflate()V

    .line 30
    const v0, 0x7f0801de

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->c:Landroid/widget/LinearLayout;

    .line 31
    const v0, 0x7f080207

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ClingView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->b:Lcom/google/android/apps/youtube/app/ui/ClingView;

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->b:Lcom/google/android/apps/youtube/app/ui/ClingView;

    iput-object p0, v0, Lcom/google/android/apps/youtube/app/ui/ClingView;->a:Lbob;

    .line 33
    return-void
.end method
