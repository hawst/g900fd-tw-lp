.class public Lcom/google/android/apps/youtube/app/honeycomb/Shell$HomeActivity;
.super Lbgp;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbgp;-><init>(La;)V

    return-void
.end method


# virtual methods
.method protected final b()Ljava/lang/Class;
    .locals 3

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/Shell$HomeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 150
    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/Shell$HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "show_startup_promo"

    const/4 v2, 0x1

    .line 153
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const-class v0, Lcom/google/android/apps/youtube/app/StartupPromoActivity;

    .line 156
    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    goto :goto_0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 162
    const/high16 v0, 0x4000000

    return v0
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 145
    invoke-super {p0, p1}, Lbgp;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method
