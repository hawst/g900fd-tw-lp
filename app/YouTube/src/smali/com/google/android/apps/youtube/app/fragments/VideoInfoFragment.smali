.class public Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;
.super Lj;
.source "SourceFile"

# interfaces
.implements Lbyr;
.implements Lbzi;
.implements Lfrz;


# instance fields
.field public W:Lbxu;

.field private X:Lbfl;

.field private Y:Lari;

.field private Z:Letc;

.field public a:Lbfm;

.field private aA:Lbro;

.field private aB:Lbuk;

.field private aC:Lfrl;

.field private aD:Lflh;

.field private aE:Lfqg;

.field private aF:Lerv;

.field private aG:Lbwy;

.field private aH:Lbwx;

.field private aI:Lbwz;

.field private aJ:Lbwa;

.field private aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private ab:Landroid/content/res/Resources;

.field private ac:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

.field private ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

.field private ae:Levn;

.field private af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private ag:Landroid/widget/ListView;

.field private ah:Landroid/view/View;

.field private ai:Landroid/view/View;

.field private aj:Landroid/view/ViewStub;

.field private ak:Landroid/view/View;

.field private al:Landroid/view/View;

.field private am:Lbzd;

.field private an:Lboc;

.field private ao:Lbka;

.field private ap:Lgng;

.field private aq:Lgix;

.field private ar:Leyp;

.field private as:Lfvt;

.field private at:Lfcz;

.field private au:Lfdw;

.field private av:Lbvc;

.field private aw:Lbvc;

.field private ax:Lbvc;

.field private ay:Lcjs;

.field private az:Lfvh;

.field b:Lbym;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lj;-><init>()V

    .line 1061
    return-void
.end method

.method private A()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const v3, 0x7f08033f

    const v2, 0x7f080217

    .line 734
    .line 736
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 737
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 738
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 743
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aG:Lbwy;

    if-eqz v2, :cond_0

    .line 744
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aG:Lbwy;

    invoke-virtual {v2, v1}, Lbwy;->a(Landroid/view/View;)V

    .line 746
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aH:Lbwx;

    if-eqz v2, :cond_1

    .line 747
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aH:Lbwx;

    invoke-virtual {v2, v1}, Lbwx;->a(Landroid/view/View;)V

    .line 749
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aI:Lbwz;

    if-eqz v1, :cond_2

    .line 750
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aI:Lbwz;

    invoke-virtual {v1, v0}, Lbwz;->a(Landroid/view/View;)V

    .line 752
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 753
    return-void

    .line 739
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 740
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 741
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method private C()V
    .locals 2

    .prologue
    .line 811
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 812
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    if-eqz v0, :cond_0

    .line 813
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->A()V

    .line 814
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e(Z)V

    .line 816
    :cond_0
    return-void
.end method

.method private D()Lgnd;
    .locals 2

    .prologue
    .line 823
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aq:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 824
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ap:Lgng;

    invoke-virtual {v0}, Lgng;->c()Lgnd;

    move-result-object v0

    .line 826
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ap:Lgng;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aq:Lgix;

    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lcom/google/android/apps/youtube/app/WatchWhileActivity;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    return-object v0
.end method

.method private a(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 702
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    iget-boolean v0, v0, Lbzd;->l:Z

    if-eqz v0, :cond_0

    .line 703
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    .line 704
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Landroid/view/View;

    if-eqz v0, :cond_2

    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 705
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ak:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 706
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ak:Landroid/view/View;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 709
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 703
    goto :goto_0

    :cond_2
    move v2, v1

    .line 704
    goto :goto_1

    :cond_3
    move v1, v3

    .line 706
    goto :goto_2
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;Z)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    iput-boolean p1, v0, Lbfm;->g:Z

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;Z)V

    .line 473
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lcjs;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ay:Lcjs;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 794
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    iput p1, v0, Lbfm;->h:I

    .line 795
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b()V

    .line 796
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->C()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lboc;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->an:Lboc;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->A()V

    return-void
.end method

.method private e(Z)V
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    iput-boolean p1, v0, Lbfm;->d:Z

    .line 761
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b()V

    .line 762
    return-void
.end method

.method public static synthetic f(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    return-object v0
.end method

.method private f(Z)V
    .locals 1

    .prologue
    .line 769
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->X:Lbfl;

    .line 770
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b()V

    .line 771
    return-void
.end method

.method public static synthetic g(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lbzd;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    return-object v0
.end method

.method private handleOfflineVideoAddEvent(Lbly;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 907
    iget-object v0, p1, Lbly;->a:Lgmb;

    .line 908
    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    .line 909
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v2}, Lcws;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 910
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    invoke-virtual {v1, v0, v2}, Lbzd;->a(Lgmb;Lflh;)V

    .line 912
    :cond_0
    return-void
.end method

.method private handleOfflineVideoAddFailedEvent(Lblz;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 916
    iget-object v0, p1, Lblz;->a:Ljava/lang/String;

    .line 917
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v1}, Lcws;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 918
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    invoke-virtual {v0, v1, v2}, Lbzd;->a(Lgmb;Lflh;)V

    .line 920
    :cond_0
    return-void
.end method

.method private handleOfflineVideoCompleteEvent(Lbma;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 947
    iget-object v0, p1, Lbma;->a:Lgmb;

    .line 948
    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    .line 949
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v2}, Lcws;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 950
    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->f(Z)V

    .line 951
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    invoke-virtual {v1, v0, v2}, Lbzd;->a(Lgmb;Lflh;)V

    .line 952
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x7f090187

    invoke-static {v0, v1, v3}, Leze;->a(Landroid/content/Context;II)V

    .line 954
    :cond_0
    return-void
.end method

.method private handleOfflineVideoDeleteEvent(Lbmb;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 958
    iget-object v0, p1, Lbmb;->a:Ljava/lang/String;

    .line 959
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v1}, Lcws;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 961
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aC:Lfrl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aC:Lfrl;

    invoke-virtual {v0}, Lfrl;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 962
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p()V

    .line 968
    :cond_0
    :goto_0
    return-void

    .line 966
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    invoke-virtual {v0, v1, v2}, Lbzd;->a(Lgmb;Lflh;)V

    goto :goto_0
.end method

.method private handleOfflineVideoStatusUpdateEvent(Lbmc;)V
    .locals 5
    .annotation runtime Levv;
    .end annotation

    .prologue
    const v4, 0x7f09018a

    const/4 v3, 0x1

    .line 924
    iget-object v0, p1, Lbmc;->a:Lgmb;

    .line 925
    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    .line 926
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v2}, Lcws;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 927
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    invoke-virtual {v1, v0, v2}, Lbzd;->a(Lgmb;Lflh;)V

    .line 928
    invoke-virtual {v0}, Lgmb;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 929
    invoke-virtual {v0}, Lgmb;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 930
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v4, v3}, Leze;->a(Landroid/content/Context;II)V

    .line 943
    :cond_0
    :goto_0
    return-void

    .line 931
    :cond_1
    invoke-virtual {v0}, Lgmb;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 932
    iget-object v0, v0, Lgmb;->d:Lglz;

    .line 933
    invoke-virtual {v0}, Lglz;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 934
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v4, v3}, Leze;->a(Landroid/content/Context;II)V

    goto :goto_0

    .line 936
    :cond_2
    invoke-virtual {v0}, Lgmb;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 937
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x7f09018b

    invoke-static {v0, v1, v3}, Leze;->a(Landroid/content/Context;II)V

    goto :goto_0

    .line 939
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x7f090189

    invoke-static {v0, v1, v3}, Leze;->a(Landroid/content/Context;II)V

    goto :goto_0
.end method

.method private handlePlaybackServiceException(Lczb;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 554
    iget-object v0, p1, Lczb;->c:Ljava/lang/String;

    .line 555
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Landroid/content/res/Resources;

    const v1, 0x7f090118

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 558
    :cond_0
    sget-object v1, Lbfh;->c:[I

    iget-object v2, p1, Lczb;->a:Lczc;

    invoke-virtual {v2}, Lczc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 568
    :goto_0
    return-void

    .line 562
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v2, Lbfj;

    invoke-direct {v2, p0}, Lbfj;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lfve;)V

    .line 563
    iget-boolean v1, p1, Lczb;->b:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 566
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v2, Lbfn;

    invoke-direct {v2, p0}, Lbfn;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lfve;)V

    .line 567
    iget-boolean v1, p1, Lczb;->b:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 558
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handlePlaylistSetStatusUpdateEvent(Lbuq;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 901
    iget-boolean v0, p1, Lbuq;->a:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    iput-boolean v0, v1, Lbfm;->e:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b()V

    .line 902
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->A()V

    .line 903
    return-void
.end method

.method private handleRequestingWatchDataEvent(Lczr;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a()V

    .line 550
    return-void
.end method

.method private handleSequencerStageEvent(Lczu;)V
    .locals 25
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 516
    sget-object v1, Lbfh;->b:[I

    move-object/from16 v0, p1

    iget-object v2, v0, Lczu;->a:Lgok;

    invoke-virtual {v2}, Lgok;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 537
    :cond_0
    :goto_0
    return-void

    .line 519
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a()V

    .line 520
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ay:Lcjs;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcjs;->a(Lfwc;)V

    .line 521
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    if-eqz v1, :cond_0

    .line 522
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lbro;->a(Z)V

    .line 523
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    invoke-virtual {v1}, Lbro;->a()V

    goto :goto_0

    .line 527
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->an:Lboc;

    invoke-virtual {v1}, Lboc;->a()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b:Lbym;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lbym;->a(Lbzb;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ay:Lcjs;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcjs;->a(Lfwc;)V

    goto :goto_0

    .line 532
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aF:Lerv;

    iget-object v1, v1, Lerv;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 534
    move-object/from16 v0, p1

    iget-object v10, v0, Lczu;->c:Lfnx;

    .line 535
    move-object/from16 v0, p1

    iget-object v9, v0, Lczu;->b:Lfrl;

    .line 536
    move-object/from16 v0, p1

    iget-object v11, v0, Lczu;->d:Lfqg;

    .line 533
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aC:Lfrl;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aE:Lfqg;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->au:Lfdw;

    sget-object v2, Lfqi;->a:Lfqi;

    const/4 v3, 0x0

    invoke-virtual {v1, v11, v2, v3}, Lfdw;->a(Lfqg;Lfqi;Lhcq;)V

    iget-object v12, v10, Lfnx;->i:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->D()Lgnd;

    move-result-object v1

    invoke-interface {v1, v12}, Lgnd;->b(Ljava/lang/String;)Lgmb;

    move-result-object v13

    invoke-virtual {v9}, Lfrl;->g()Lflo;

    move-result-object v2

    invoke-virtual {v2}, Lflo;->f()Lflh;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    if-eqz v1, :cond_d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    iget-boolean v1, v1, Lflh;->a:Z

    if-eqz v1, :cond_d

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->X:Lbfl;

    iput-boolean v1, v3, Lbfl;->c:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lbxu;->a(Z)V

    const/4 v1, 0x0

    iget-object v3, v10, Lfnx;->g:Lfjm;

    if-eqz v3, :cond_f

    iget-object v1, v3, Lfjm;->a:Lhem;

    iget-object v1, v1, Lhem;->a:Lhog;

    if-eqz v1, :cond_e

    iget-object v1, v3, Lfjm;->a:Lhem;

    iget-object v1, v1, Lhem;->a:Lhog;

    iget-object v1, v1, Lhog;->D:Lheq;

    if-eqz v1, :cond_e

    iget-object v1, v3, Lfjm;->a:Lhem;

    iget-object v1, v1, Lhem;->a:Lhog;

    iget-object v1, v1, Lhog;->D:Lheq;

    iget-object v1, v1, Lheq;->a:Ljava/lang/String;

    move-object v8, v1

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    if-nez v1, :cond_1

    new-instance v1, Lbro;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->j()Lo;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->s()Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ar:Leyp;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->at:Lfcz;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v7}, Lari;->ay()Leyt;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lbro;-><init>(Landroid/app/Activity;Landroid/view/View;Leyp;Lfcz;Levn;Leyt;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    iput-object v8, v1, Lbro;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lbro;->a(Z)V

    :cond_2
    const/4 v1, 0x1

    move v7, v1

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->az:Lfvh;

    iget-object v2, v10, Lfnx;->c:Lfmi;

    invoke-virtual {v1, v2}, Lfvh;->b(Lfmi;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b:Lbym;

    invoke-virtual {v9}, Lfrl;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v10, Lfnx;->b:Lfmn;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    invoke-static {v12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lbzb;

    move-object v3, v12

    invoke-direct/range {v1 .. v6}, Lbzb;-><init>(Lbym;Ljava/lang/String;Ljava/lang/String;Lfmn;Lflh;)V

    invoke-virtual {v2, v1}, Lbym;->a(Lbzb;)V

    iget-object v1, v10, Lfnx;->m:Lfln;

    if-nez v1, :cond_3

    iget-object v1, v10, Lfnx;->a:Libi;

    iget-object v1, v1, Libi;->g:Liad;

    if-eqz v1, :cond_3

    iget-object v1, v10, Lfnx;->a:Libi;

    iget-object v1, v1, Libi;->g:Liad;

    iget-object v1, v1, Liad;->a:Lhqb;

    if-eqz v1, :cond_3

    new-instance v1, Lfln;

    iget-object v2, v10, Lfnx;->a:Libi;

    iget-object v2, v2, Libi;->g:Liad;

    iget-object v2, v2, Liad;->a:Lhqb;

    invoke-direct {v1, v2}, Lfln;-><init>(Lhqb;)V

    iput-object v1, v10, Lfnx;->m:Lfln;

    :cond_3
    iget-object v1, v10, Lfnx;->m:Lfln;

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aJ:Lbwa;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, v3, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    new-instance v4, Lbto;

    iget-object v5, v2, Lbwa;->a:Landroid/app/Activity;

    iget-object v6, v2, Lbwa;->c:Leyt;

    invoke-direct {v4, v5, v3, v6, v1}, Lbto;-><init>(Landroid/content/Context;Lfhz;Leyt;Lfln;)V

    iput-object v4, v2, Lbwa;->d:Lbto;

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aJ:Lbwa;

    iput-object v12, v1, Lbwa;->e:Ljava/lang/String;

    iget-object v2, v10, Lfnx;->d:Lfnk;

    if-eqz v2, :cond_1c

    iget-object v1, v2, Lfnk;->f:Lfkv;

    if-nez v1, :cond_6

    invoke-virtual {v2}, Lfnk;->g()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v2}, Lfnk;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbt;

    instance-of v4, v1, Lfkv;

    if-eqz v4, :cond_5

    check-cast v1, Lfkv;

    iput-object v1, v2, Lfnk;->f:Lfkv;

    :cond_6
    iget-object v5, v2, Lfnk;->f:Lfkv;

    if-eqz v5, :cond_7

    iget-boolean v1, v5, Lfkv;->c:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->X:Lbfl;

    iput-boolean v1, v2, Lbfl;->b:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lbxu;->a(Z)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    iget-object v15, v10, Lfnx;->d:Lfnk;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v17, v0

    invoke-static {v15}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v14, Lbzd;->b:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfrz;

    iput-object v1, v14, Lbzd;->k:Lfrz;

    const/4 v1, 0x1

    iput-boolean v1, v14, Lbzd;->l:Z

    const/4 v1, 0x0

    iput-boolean v1, v14, Lbzd;->i:Z

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, v15, Lfnk;->d:Ljava/lang/CharSequence;

    if-nez v3, :cond_8

    iget-object v3, v15, Lfnk;->a:Lhzs;

    iget-object v3, v3, Lhzs;->f:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v15, Lfnk;->d:Ljava/lang/CharSequence;

    :cond_8
    iget-object v3, v15, Lfnk;->d:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, v15, Lfnk;->c:Ljava/lang/CharSequence;

    if-nez v3, :cond_9

    iget-object v3, v15, Lfnk;->a:Lhzs;

    iget-object v3, v3, Lhzs;->c:Lhgz;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lfvo;->a(Lhgz;I)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v15, Lfnk;->c:Ljava/lang/CharSequence;

    :cond_9
    iget-object v3, v15, Lfnk;->c:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    invoke-static {v1}, Lfvo;->a([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v15}, Lfnk;->a()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_10

    const-string v1, ""

    :goto_4
    invoke-virtual {v15}, Lfnk;->c()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_20

    invoke-virtual {v15}, Lfnk;->b()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_20

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    invoke-virtual {v15}, Lfnk;->b()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v2}, Lfvo;->a([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    move-object v2, v1

    :goto_5
    iget-object v1, v15, Lfnk;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_a

    iget-object v1, v15, Lfnk;->a:Lhzs;

    iget-object v1, v1, Lhzs;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, v15, Lfnk;->b:Ljava/lang/CharSequence;

    :cond_a
    iget-object v0, v15, Lfnk;->b:Ljava/lang/CharSequence;

    move-object/from16 v18, v0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    if-eqz v5, :cond_1f

    const/4 v4, 0x1

    iget-boolean v3, v5, Lfkv;->c:Z

    iget-object v1, v5, Lfkv;->d:Lfki;

    if-nez v1, :cond_b

    iget-object v1, v5, Lfkv;->a:Lhnx;

    iget-object v1, v1, Lhnx;->b:Lhnw;

    if-nez v1, :cond_11

    :cond_b
    iget-object v1, v5, Lfkv;->d:Lfki;

    :goto_6
    iput-object v1, v14, Lbzd;->j:Lfki;

    iget-object v1, v5, Lfkv;->e:Ljava/lang/CharSequence;

    if-nez v1, :cond_c

    iget-object v1, v5, Lfkv;->a:Lhnx;

    iget-object v1, v1, Lhnx;->c:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, v5, Lfkv;->e:Ljava/lang/CharSequence;

    :cond_c
    iget-object v1, v5, Lfkv;->e:Ljava/lang/CharSequence;

    move-object v5, v1

    :goto_7
    const/4 v1, 0x0

    const/4 v6, 0x0

    invoke-virtual {v15}, Lfnk;->c()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_1e

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    const/4 v9, 0x1

    if-le v1, v9, :cond_14

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfna;

    invoke-virtual {v1}, Lfna;->b()Ljava/lang/CharSequence;

    move-result-object v6

    const/4 v1, 0x1

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfna;

    invoke-virtual {v1}, Lfna;->b()Ljava/lang/CharSequence;

    move-result-object v1

    move-object v8, v6

    move-object v6, v1

    :goto_8
    iget-object v1, v14, Lbzd;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_9
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbzj;

    move-object/from16 v0, v18

    invoke-interface {v1, v0}, Lbzj;->a(Ljava/lang/CharSequence;)V

    invoke-interface {v1, v12}, Lbzj;->b(Ljava/lang/CharSequence;)V

    invoke-interface {v1, v2}, Lbzj;->c(Ljava/lang/CharSequence;)V

    iget-boolean v9, v14, Lbzd;->i:Z

    invoke-interface {v1, v9}, Lbzj;->a(Z)V

    move-object/from16 v0, v16

    invoke-interface {v1, v13, v0}, Lbzj;->a(Lgmb;Lflh;)V

    invoke-interface {v1, v4, v3, v5}, Lbzj;->a(ZZLjava/lang/CharSequence;)V

    invoke-interface {v1, v8}, Lbzj;->d(Ljava/lang/CharSequence;)V

    invoke-interface {v1, v6}, Lbzj;->e(Ljava/lang/CharSequence;)V

    invoke-virtual {v15}, Lfnk;->f()Lfkt;

    move-result-object v9

    if-eqz v9, :cond_16

    invoke-virtual {v15}, Lfnk;->f()Lfkt;

    move-result-object v9

    move-object/from16 v0, v17

    iput-object v0, v9, Lfkt;->c:Lfhz;

    invoke-virtual {v15}, Lfnk;->f()Lfkt;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v9, v0, Lfkt;->b:Ljava/util/List;

    if-nez v9, :cond_15

    move-object/from16 v0, v20

    iget-object v9, v0, Lfkt;->a:Lhnl;

    iget-object v9, v9, Lhnl;->a:[Lhnn;

    if-eqz v9, :cond_15

    new-instance v9, Ljava/util/ArrayList;

    move-object/from16 v0, v20

    iget-object v0, v0, Lfkt;->a:Lhnl;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lhnl;->a:[Lhnn;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v20

    iput-object v9, v0, Lfkt;->b:Ljava/util/List;

    const/4 v9, 0x0

    :goto_a
    move-object/from16 v0, v20

    iget-object v0, v0, Lfkt;->a:Lhnl;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lhnl;->a:[Lhnn;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v9, v0, :cond_15

    move-object/from16 v0, v20

    iget-object v0, v0, Lfkt;->b:Ljava/util/List;

    move-object/from16 v21, v0

    new-instance v22, Lfks;

    move-object/from16 v0, v20

    iget-object v0, v0, Lfkt;->a:Lhnl;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lhnl;->a:[Lhnn;

    move-object/from16 v23, v0

    aget-object v23, v23, v9

    move-object/from16 v0, v23

    iget-object v0, v0, Lhnn;->b:Lhnm;

    move-object/from16 v23, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lfkt;->c:Lfhz;

    move-object/from16 v24, v0

    invoke-direct/range {v22 .. v24}, Lfks;-><init>(Lhnm;Lfhz;)V

    invoke-interface/range {v21 .. v22}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_a

    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_e
    const/4 v1, 0x0

    move-object v8, v1

    goto/16 :goto_2

    :cond_f
    invoke-virtual {v2}, Lflo;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->as:Lfvt;

    invoke-static {}, Lfvt;->a()Lfvv;

    move-result-object v2

    iput-object v12, v2, Lfvv;->a:Ljava/lang/String;

    const/4 v3, 0x2

    iput v3, v2, Lfvv;->b:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->as:Lfvt;

    new-instance v4, Lbfg;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v12}, Lbfg;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;Ljava/lang/String;)V

    invoke-virtual {v3, v2, v4}, Lfvt;->a(Lfvv;Lwv;)V

    move v7, v1

    goto/16 :goto_3

    :cond_10
    invoke-virtual {v15}, Lfnk;->a()Ljava/lang/CharSequence;

    move-result-object v1

    goto/16 :goto_4

    :cond_11
    iget-object v1, v5, Lfkv;->a:Lhnx;

    iget-object v1, v1, Lhnx;->b:Lhnw;

    iget-object v1, v1, Lhnw;->a:Lhod;

    if-eqz v1, :cond_13

    new-instance v1, Lfla;

    iget-object v6, v5, Lfkv;->a:Lhnx;

    iget-object v6, v6, Lhnx;->b:Lhnw;

    iget-object v6, v6, Lhnw;->a:Lhod;

    iget-object v8, v5, Lfkv;->b:Lfqh;

    invoke-direct {v1, v6, v8}, Lfla;-><init>(Lhod;Lfqh;)V

    iput-object v1, v5, Lfkv;->d:Lfki;

    :cond_12
    :goto_b
    iget-object v1, v5, Lfkv;->d:Lfki;

    goto/16 :goto_6

    :cond_13
    iget-object v1, v5, Lfkv;->a:Lhnx;

    iget-object v1, v1, Lhnx;->b:Lhnw;

    iget-object v1, v1, Lhnw;->b:Lhfz;

    if-eqz v1, :cond_12

    new-instance v1, Lfjt;

    iget-object v6, v5, Lfkv;->a:Lhnx;

    iget-object v6, v6, Lhnx;->b:Lhnw;

    iget-object v6, v6, Lhnw;->b:Lhfz;

    iget-object v8, v5, Lfkv;->b:Lfqh;

    invoke-direct {v1, v6, v8}, Lfjt;-><init>(Lhfz;Lfqh;)V

    iput-object v1, v5, Lfkv;->d:Lfki;

    goto :goto_b

    :cond_14
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfna;

    invoke-virtual {v1}, Lfna;->b()Ljava/lang/CharSequence;

    move-result-object v1

    move-object v8, v1

    goto/16 :goto_8

    :cond_15
    move-object/from16 v0, v20

    iget-object v9, v0, Lfkt;->b:Ljava/util/List;

    invoke-virtual {v15}, Lfnk;->f()Lfkt;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lfkt;->a:Lhnl;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lhnl;->b:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-interface {v1, v9, v0}, Lbzj;->a(Ljava/util/List;I)V

    :cond_16
    invoke-interface {v1}, Lbzj;->b()Z

    move-result v9

    invoke-interface {v1, v9}, Lbzj;->b(Z)V

    goto/16 :goto_9

    :cond_17
    iget-object v1, v14, Lbzd;->h:Lbrh;

    iget-object v2, v15, Lfnk;->e:Lfkn;

    if-nez v2, :cond_18

    iget-object v2, v15, Lfnk;->a:Lhzs;

    iget-object v2, v2, Lhzs;->g:Lhll;

    if-eqz v2, :cond_18

    iget-object v2, v15, Lfnk;->a:Lhzs;

    iget-object v2, v2, Lhzs;->g:Lhll;

    iget-object v2, v2, Lhll;->a:Lhlk;

    if-eqz v2, :cond_18

    new-instance v2, Lfkn;

    iget-object v3, v15, Lfnk;->a:Lhzs;

    iget-object v3, v3, Lhzs;->g:Lhll;

    iget-object v3, v3, Lhll;->a:Lhlk;

    invoke-direct {v2, v3}, Lfkn;-><init>(Lhlk;)V

    iput-object v2, v15, Lfnk;->e:Lfkn;

    :cond_18
    iget-object v2, v15, Lfnk;->e:Lfkn;

    invoke-virtual {v1, v2}, Lbrh;->a(Lfkn;)V

    if-eqz v13, :cond_1b

    invoke-virtual {v13}, Lgmb;->l()Z

    move-result v1

    if-eqz v1, :cond_1b

    const/4 v1, 0x1

    :goto_c
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->f(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->au:Lfdw;

    iget-object v2, v10, Lfnx;->d:Lfnk;

    const/4 v3, 0x0

    invoke-virtual {v1, v11, v2, v3}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    invoke-virtual {v1}, Lbzd;->b()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    if-eqz v1, :cond_19

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;->a()V

    :cond_19
    :goto_d
    iget-object v1, v10, Lfnx;->c:Lfmi;

    if-nez v1, :cond_1d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    :cond_1a
    :goto_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a(Landroid/content/res/Configuration;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->X:Lbfl;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lbfl;->a:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lbxu;->a(Z)V

    goto/16 :goto_0

    :cond_1b
    const/4 v1, 0x0

    goto :goto_c

    :cond_1c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    invoke-virtual {v1}, Lbzd;->c()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    if-eqz v1, :cond_19

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;->b()V

    goto :goto_d

    :cond_1d
    if-eqz v7, :cond_1a

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->C()V

    goto :goto_e

    :cond_1e
    move-object v8, v1

    goto/16 :goto_8

    :cond_1f
    move-object v5, v4

    move v4, v3

    move v3, v1

    goto/16 :goto_7

    :cond_20
    move-object v2, v1

    goto/16 :goto_5

    .line 516
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleVideoStageEvent(Ldac;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 502
    sget-object v0, Lbfh;->a:[I

    iget-object v1, p1, Ldac;->a:Lgol;

    invoke-virtual {v1}, Lgol;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 507
    :goto_0
    return-void

    .line 506
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->A()V

    goto :goto_0

    .line 502
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private z()V
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 468
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e(Z)V

    .line 469
    return-void
.end method


# virtual methods
.method public final B()Lfqg;
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aE:Lfqg;

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f0700b9

    .line 184
    const v0, 0x7f040126

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 185
    const v0, 0x7f080324

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v2, Lbfi;

    invoke-direct {v2, p0}, Lbfi;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)V

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b:Lbrv;

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const v2, 0x7f080325

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Landroid/widget/ListView;

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v0}, Lari;->f()Larh;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->setBackgroundResource(I)V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 195
    const v0, 0x7f0400e8

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Landroid/widget/ListView;

    invoke-virtual {p1, v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Landroid/view/View;

    .line 200
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->z()V

    .line 201
    return-object v1
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b:Lbym;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbym;->a(Lbzb;)V

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 478
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->X:Lbfl;

    iput-boolean v2, v0, Lbfl;->a:Z

    iput-boolean v2, v0, Lbfl;->b:Z

    iput-boolean v2, v0, Lbfl;->c:Z

    .line 479
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    invoke-virtual {v0}, Lbro;->a()V

    .line 482
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->z()V

    .line 483
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 168
    invoke-super {p0, p1}, Lj;->a(Landroid/app/Activity;)V

    move-object v0, p1

    .line 169
    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->k()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Landroid/content/res/Resources;

    .line 171
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 172
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 173
    iget-object v0, v0, Lckz;->a:Letc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Letc;

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B()Lbxu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    .line 175
    new-instance v0, Lbfm;

    invoke-direct {v0}, Lbfm;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    .line 176
    new-instance v0, Lbfl;

    invoke-direct {v0}, Lbfl;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->X:Lbfl;

    .line 177
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f090189

    const/4 v2, 0x1

    .line 838
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcws;

    invoke-virtual {v0}, Lcws;->e()Ljava/lang/String;

    move-result-object v0

    .line 839
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 892
    :cond_0
    :goto_0
    return-void

    .line 844
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aC:Lfrl;

    if-eqz v0, :cond_0

    .line 851
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v3, v2}, Leze;->a(Landroid/content/Context;II)V

    goto :goto_0

    .line 856
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->D()Lgnd;

    move-result-object v0

    invoke-interface {v0, p1}, Lgnd;->b(Ljava/lang/String;)Lgmb;

    move-result-object v0

    .line 857
    if-eqz v0, :cond_9

    .line 858
    invoke-virtual {v0}, Lgmb;->l()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lgmb;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 859
    invoke-virtual {v0}, Lgmb;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 861
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ao:Lbka;

    invoke-virtual {v0, p1}, Lbka;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 862
    :cond_4
    invoke-virtual {v0}, Lgmb;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 863
    invoke-virtual {v0}, Lgmb;->k()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 865
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ao:Lbka;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    invoke-virtual {v0, v1, p1, v2}, Lbka;->a(Ljava/lang/String;Ljava/lang/String;Lbkg;)V

    goto :goto_0

    .line 866
    :cond_5
    invoke-virtual {v0}, Lgmb;->g()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 868
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v3, v2}, Leze;->a(Landroid/content/Context;II)V

    goto :goto_0

    .line 869
    :cond_6
    invoke-virtual {v0}, Lgmb;->h()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 870
    iget-object v0, v0, Lgmb;->d:Lglz;

    .line 871
    invoke-virtual {v0}, Lglz;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 873
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f()Lbrz;

    move-result-object v0

    invoke-virtual {v0}, Lbrz;->a()V

    goto :goto_0

    .line 877
    :cond_7
    iget-object v0, v0, Lglz;->b:Lflg;

    invoke-virtual {v0}, Lflg;->b()Lfki;

    move-result-object v0

    .line 878
    if-eqz v0, :cond_0

    .line 879
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v1

    invoke-virtual {v1, v0, p0}, Lbyg;->a(Lfki;Lfrz;)V

    goto :goto_0

    .line 884
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ao:Lbka;

    invoke-virtual {v0, p1}, Lbka;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 890
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ao:Lbka;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aC:Lfrl;

    .line 891
    invoke-virtual {v1}, Lfrl;->g()Lflo;

    move-result-object v1

    invoke-virtual {v1}, Lflo;->f()Lflh;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    .line 890
    invoke-virtual {v0, p1, v1, v2, p0}, Lbka;->a(Ljava/lang/String;Lflh;Lbkg;Lfrz;)V

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    iput-boolean p1, v0, Lbfm;->c:Z

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 498
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 756
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 757
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 832
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->s()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 833
    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 28

    .prologue
    .line 206
    invoke-super/range {p0 .. p1}, Lj;->d(Landroid/os/Bundle;)V

    .line 208
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->aE()Lcuo;

    move-result-object v6

    .line 209
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->aD()Lcst;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aq:Lgix;

    .line 210
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->c()Leyp;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ar:Leyp;

    .line 211
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    iget-object v1, v1, Lari;->y:Lezs;

    invoke-virtual {v1}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfvt;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->as:Lfvt;

    .line 212
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    iget-object v1, v1, Lari;->al:Lezs;

    invoke-virtual {v1}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfcz;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->at:Lfcz;

    .line 213
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->ae()Lfdw;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->au:Lfdw;

    .line 214
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->ay()Leyt;

    move-result-object v10

    .line 215
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->b()Lfxe;

    move-result-object v3

    .line 216
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->t()Lfeb;

    move-result-object v4

    .line 217
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->u()Lfew;

    move-result-object v5

    .line 218
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Letc;

    invoke-virtual {v1}, Letc;->i()Levn;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    .line 219
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->O()Lgng;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ap:Lgng;

    .line 221
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h()Lbka;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ao:Lbka;

    .line 223
    new-instance v1, Lbwa;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v1, v2, v3, v10}, Lbwa;-><init>(Landroid/app/Activity;Lfxe;Leyt;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aJ:Lbwa;

    .line 224
    new-instance v1, Lbym;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 230
    invoke-virtual {v7}, Lari;->m()Lctk;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aq:Lgix;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 232
    invoke-virtual {v9}, Lari;->aO()Lcub;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 236
    iget-object v13, v12, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Letc;

    .line 237
    invoke-virtual {v12}, Letc;->q()Ljava/util/concurrent/Executor;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aJ:Lbwa;

    move-object/from16 v12, p0

    invoke-direct/range {v1 .. v15}, Lbym;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfxe;Lfeb;Lfew;Lcuo;Lctk;Lgix;Lcub;Leyt;Levn;Lbyr;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/Executor;Lbwa;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b:Lbym;

    .line 241
    new-instance v11, Lbrh;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aq:Lgix;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 244
    invoke-virtual {v1}, Lari;->aO()Lcub;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 248
    invoke-virtual {v1}, Lari;->o()Lglm;

    move-result-object v18

    move-object v15, v4

    move-object/from16 v16, v10

    invoke-direct/range {v11 .. v18}, Lbrh;-><init>(Landroid/app/Activity;Lgix;Lcub;Lfeb;Leyt;Levn;Lglm;)V

    .line 250
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Landroid/view/View;

    const v2, 0x7f0802b1

    .line 251
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Landroid/view/View;

    .line 252
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Landroid/view/View;

    const v2, 0x7f080342

    .line 253
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aj:Landroid/view/ViewStub;

    .line 255
    new-instance v1, Lbzd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 258
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v1, v2, v0, v4, v11}, Lbzd;-><init>(Landroid/app/Activity;Lbzi;Lbyg;Lbrh;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    .line 261
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Landroid/view/View;

    new-instance v4, Lbzg;

    iget-object v5, v1, Lbzd;->a:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/4 v7, 0x1

    invoke-direct {v4, v1, v2, v5, v7}, Lbzg;-><init>(Lbzd;Landroid/view/View;Landroid/content/res/Resources;Z)V

    iget-object v5, v1, Lbzd;->c:Ljava/util/Map;

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v1, Lbzd;->h:Lbrh;

    invoke-virtual {v4, v2}, Lbrh;->c(Landroid/view/View;)V

    iget-object v1, v1, Lbzd;->h:Lbrh;

    invoke-virtual {v1, v2}, Lbrh;->a(Landroid/view/View;)V

    .line 263
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 265
    invoke-virtual {v1}, Lari;->K()Ldsn;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 267
    iget-object v2, v2, Lari;->b:Ldov;

    invoke-virtual {v2}, Ldov;->g()Ldaq;

    move-result-object v2

    .line 264
    invoke-static {v1, v10, v2}, Lbvc;->a(Ldsn;Leyt;Ldaq;)Lbvc;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->av:Lbvc;

    .line 268
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 270
    invoke-virtual {v1}, Lari;->K()Ldsn;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 272
    iget-object v2, v2, Lari;->b:Ldov;

    invoke-virtual {v2}, Ldov;->g()Ldaq;

    move-result-object v2

    .line 269
    invoke-static {v1, v10, v2}, Lbvc;->b(Ldsn;Leyt;Ldaq;)Lbvc;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aw:Lbvc;

    .line 273
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 275
    invoke-virtual {v1}, Lari;->K()Ldsn;

    move-result-object v1

    .line 274
    new-instance v2, Lbvc;

    const/4 v4, 0x0

    new-instance v5, Lbvj;

    invoke-direct {v5}, Lbvj;-><init>()V

    const/4 v7, 0x2

    invoke-direct {v2, v1, v4, v5, v7}, Lbvc;-><init>(Ldsn;Lboi;Lbvm;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ax:Lbvc;

    .line 279
    new-instance v11, Lboc;

    .line 281
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->j()Lo;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 283
    invoke-virtual {v1}, Lari;->s()Lfgk;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ar:Leyp;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 285
    iget-object v0, v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->au:Lfdw;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Landroid/view/View;

    const v2, 0x7f0802af

    .line 288
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/view/ViewStub;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Landroid/view/View;

    const v2, 0x7f0802b0

    .line 289
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/view/ViewStub;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Landroid/view/View;

    const v2, 0x7f0802ae

    .line 290
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/view/ViewStub;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Landroid/widget/ListView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 293
    invoke-virtual {v1}, Lari;->S()Lcws;

    move-result-object v24

    move-object/from16 v18, p0

    invoke-direct/range {v11 .. v24}, Lboc;-><init>(Landroid/app/Activity;Levn;Lfgk;Leyp;Lfhz;Lfdw;Lfrz;Landroid/view/ViewStub;Landroid/view/ViewStub;Landroid/view/ViewStub;Landroid/widget/ListView;ILcws;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->an:Lboc;

    .line 295
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 297
    new-instance v11, Lcjs;

    .line 298
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->j()Lo;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 299
    invoke-virtual {v1}, Lari;->f()Larh;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->as:Lfvt;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ar:Leyp;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aq:Lgix;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 305
    invoke-virtual {v1}, Lari;->aO()Lcub;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 307
    invoke-virtual {v1}, Lari;->v()Lffg;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 308
    iget-object v0, v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v23, v0

    move-object/from16 v17, v3

    move-object/from16 v18, v6

    move-object/from16 v21, v10

    invoke-direct/range {v11 .. v23}, Lcjs;-><init>(Landroid/app/Activity;Lcyc;Landroid/widget/ListView;Lfvt;Leyp;Lfxe;Lcuo;Lgix;Lcub;Leyt;Lffg;Lfhz;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ay:Lcjs;

    .line 310
    new-instance v1, Lerv;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v2}, Lari;->C()Lgjp;

    move-result-object v2

    invoke-direct {v1, v2}, Lerv;-><init>(Lgjp;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aF:Lerv;

    .line 312
    new-instance v11, Lfuu;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 314
    invoke-virtual {v1}, Lari;->f()Larh;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 315
    iget-object v1, v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    invoke-static {v1}, Lbbt;->a(Lbbp;)Lfhz;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Letc;

    .line 316
    invoke-virtual {v1}, Letc;->i()Levn;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ar:Leyp;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aq:Lgix;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 319
    invoke-virtual {v1}, Lari;->aO()Lcub;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aF:Lerv;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 321
    invoke-virtual {v1}, Lari;->F()Lffs;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 322
    iget-object v0, v1, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->av:Lbvc;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aw:Lbvc;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->au:Lfdw;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 328
    iget-object v0, v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->j:Lfun;

    move-object/from16 v27, v0

    move-object/from16 v22, v10

    move-object/from16 v26, p0

    invoke-direct/range {v11 .. v27}, Lfuu;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lcyc;Lfhz;Levn;Leyp;Lgix;Lcub;Lerv;Lffs;Ljava/util/concurrent/atomic/AtomicReference;Leyt;Lbvc;Lbvc;Lfdw;Lfrz;Lfun;)V

    .line 330
    new-instance v1, Lcew;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lcie;

    invoke-direct {v3}, Lcie;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Letc;

    .line 333
    invoke-virtual {v4}, Letc;->i()Levn;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcew;-><init>(Landroid/content/Context;Lfsj;Levn;)V

    .line 334
    new-instance v9, Lawk;

    invoke-direct {v9, v1}, Lawk;-><init>(Lcew;)V

    .line 336
    new-instance v1, Lfuw;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 338
    iget-object v2, v2, Lbhz;->m:Lfus;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 339
    invoke-virtual {v3}, Lari;->s()Lfgk;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Letc;

    .line 340
    invoke-virtual {v4}, Letc;->i()Levn;

    move-result-object v4

    new-instance v8, Lbff;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lbff;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)V

    move-object v5, v11

    move-object/from16 v6, p0

    move-object v7, v10

    invoke-direct/range {v1 .. v8}, Lfuw;-><init>(Lfus;Lfdg;Levn;Lfuu;Lfrz;Leyt;Lful;)V

    .line 359
    new-instance v4, Lfvh;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 362
    invoke-virtual {v2}, Lari;->s()Lfgk;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Letc;

    .line 363
    invoke-virtual {v2}, Letc;->i()Levn;

    move-result-object v8

    move-object v6, v9

    move-object v9, v1

    invoke-direct/range {v4 .. v10}, Lfvh;-><init>(Landroid/widget/ListView;Lfvd;Lfdg;Levn;Lfuw;Leyt;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->az:Lfvh;

    .line 367
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->az:Lfvh;

    new-instance v2, Lbfk;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lbfk;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)V

    invoke-virtual {v1, v2}, Lfvh;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 368
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->az:Lfvh;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ay:Lcjs;

    iget-object v2, v2, Lcjs;->h:Lfsi;

    iput-object v2, v1, Lfvh;->e:Lfsf;

    .line 370
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->f()Larh;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 373
    new-instance v1, Lbuk;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 374
    invoke-virtual {v2}, Lari;->s()Lfgk;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 375
    invoke-virtual {v3}, Lari;->ay()Leyt;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v5, 0x7f0400d7

    .line 378
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->s()Landroid/view/View;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ax:Lbvc;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    .line 380
    iget-object v8, v8, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct/range {v1 .. v8}, Lbuk;-><init>(Lfdg;Leyt;Lcom/google/android/apps/youtube/app/WatchWhileActivity;ILandroid/view/View;Lbvc;Ljava/util/concurrent/atomic/AtomicReference;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aB:Lbuk;

    .line 382
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->l()Lt;

    move-result-object v2

    .line 383
    const v1, 0x7f080257

    invoke-virtual {v2, v1}, Lt;->a(I)Lj;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    .line 384
    const v1, 0x7f080307

    .line 385
    invoke-virtual {v2, v1}, Lt;->a(I)Lj;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    .line 392
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    if-eqz v1, :cond_0

    .line 393
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;->s()Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Landroid/view/View;

    .line 394
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Lari;

    invoke-virtual {v1}, Lari;->f()Larh;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 395
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lbzd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Landroid/view/View;

    new-instance v3, Lbzg;

    iget-object v4, v1, Lbzd;->a:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v1, v2, v4, v5}, Lbzg;-><init>(Lbzd;Landroid/view/View;Landroid/content/res/Resources;Z)V

    iget-object v4, v1, Lbzd;->c:Ljava/util/Map;

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v1, Lbzd;->h:Lbrh;

    invoke-virtual {v3, v2}, Lbrh;->c(Landroid/view/View;)V

    iget-object v1, v1, Lbzd;->h:Lbrh;

    invoke-virtual {v1, v2}, Lbrh;->a(Landroid/view/View;)V

    .line 397
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lbfm;->f:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lbxu;->a(Z)V

    .line 404
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b(I)V

    .line 406
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Letc;

    invoke-virtual {v1}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v1, "show_kk_badge_pre_purchase_tutorial"

    const/4 v2, 0x1

    invoke-interface {v4, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lbwy;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->X:Lbfl;

    invoke-direct/range {v1 .. v6}, Lbwy;-><init>(Landroid/app/Activity;Lbxu;Landroid/content/SharedPreferences;Lbfm;Lbfl;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aG:Lbwy;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aG:Lbwy;

    invoke-virtual {v1, v2}, Lbxu;->a(Lbxy;)V

    :cond_1
    const-string v1, "show_kk_badge_post_purchase_tutorial"

    const/4 v2, 0x1

    invoke-interface {v4, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lbwx;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->X:Lbfl;

    invoke-direct/range {v1 .. v6}, Lbwx;-><init>(Landroid/app/Activity;Lbxu;Landroid/content/SharedPreferences;Lbfm;Lbfl;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aH:Lbwx;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aH:Lbwx;

    invoke-virtual {v1, v2}, Lbxu;->a(Lbxy;)V

    :cond_2
    const-string v1, "show_offline_button_tutorial"

    const/4 v2, 0x1

    invoke-interface {v4, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lbwz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lbfm;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->X:Lbfl;

    invoke-direct/range {v1 .. v6}, Lbwz;-><init>(Landroid/app/Activity;Lbxu;Landroid/content/SharedPreferences;Lbfm;Lbfl;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aI:Lbwz;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->W:Lbxu;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aI:Lbwz;

    invoke-virtual {v1, v2}, Lbxu;->a(Lbxy;)V

    .line 407
    :cond_3
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 446
    invoke-super {p0}, Lj;->f()V

    .line 447
    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aD:Lflh;

    .line 448
    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aC:Lfrl;

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    iget-boolean v1, v0, Lbro;->h:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbro;->a:Lfbh;

    invoke-virtual {v1}, Lfbh;->a()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lbro;->h:Z

    .line 452
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 428
    invoke-super {p0, p1}, Lj;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 429
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a(Landroid/content/res/Configuration;)V

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b(I)V

    .line 431
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->A()V

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->az:Lfvh;

    invoke-virtual {v0, p1}, Lfvh;->a(Landroid/content/res/Configuration;)V

    .line 433
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 411
    invoke-super {p0}, Lj;->t()V

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 413
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a(Landroid/content/res/Configuration;)V

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->av:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aw:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ax:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ay:Lcjs;

    invoke-virtual {v0}, Lcjs;->a()V

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->an:Lboc;

    iget-object v1, v0, Lboc;->b:Lbof;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lboc;->b:Lbof;

    invoke-interface {v0}, Lbof;->f()V

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ay:Lcjs;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aB:Lbuk;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->an:Lboc;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 424
    return-void
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 437
    invoke-super {p0}, Lj;->u()V

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ay:Lcjs;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aB:Lbuk;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->an:Lboc;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 442
    return-void
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 456
    invoke-super {p0}, Lj;->v()V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->az:Lfvh;

    invoke-virtual {v0}, Lfvh;->d()V

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->av:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 459
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aw:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ax:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 461
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aA:Lbro;

    invoke-virtual {v0}, Lbro;->d()V

    .line 464
    :cond_0
    return-void
.end method
