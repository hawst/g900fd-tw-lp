.class public final Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Ldea;


# instance fields
.field private a:Landroid/widget/RelativeLayout;

.field private b:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private c:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private d:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private e:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private f:Landroid/view/View;

.field private g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private h:Landroid/view/animation/Animation;

.field private i:Landroid/view/animation/Animation;

.field private j:Landroid/view/animation/Animation;

.field private k:Landroid/view/animation/Animation;

.field private l:Ldbu;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:[Ljava/lang/String;

.field private q:I

.field private r:Landroid/app/AlertDialog$Builder;

.field private final s:Ldcp;

.field private t:Ldeb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09011e

    .line 58
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900a2

    new-instance v2, Ldco;

    invoke-direct {v2, p0}, Ldco;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;)V

    .line 59
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->r:Landroid/app/AlertDialog$Builder;

    .line 60
    new-instance v0, Ldcp;

    invoke-direct {v0, p0}, Ldcp;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->s:Ldcp;

    .line 61
    const v0, 0x7f05000a

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->h:Landroid/view/animation/Animation;

    const v0, 0x7f05000b

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->h:Landroid/view/animation/Animation;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const v0, 0x7f050013

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->j:Landroid/view/animation/Animation;

    const v0, 0x7f050014

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->k:Landroid/view/animation/Animation;

    .line 62
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;)Ldeb;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->t:Ldeb;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->l:Ldbu;

    iget-boolean v0, v0, Ldbu;->k:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setEnabled(Z)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->l:Ldbu;

    iget-boolean v3, v3, Ldbu;->k:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setEnabled(Z)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->o:Z

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setEnabled(Z)V

    .line 138
    return-void

    :cond_0
    move v0, v2

    .line 135
    goto :goto_0

    :cond_1
    move v1, v2

    .line 136
    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->clearAnimation()V

    .line 108
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->j:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->h:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 111
    return-void
.end method

.method public final a(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 122
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 124
    :cond_0
    return-void
.end method

.method public final a(Ldbu;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->l:Ldbu;

    .line 98
    return-void
.end method

.method public final a(Ldeb;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->t:Ldeb;

    .line 103
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->m:Z

    .line 167
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->f()V

    .line 168
    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->p:[Ljava/lang/String;

    .line 204
    iput p2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->q:I

    .line 205
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->k:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 117
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Z

    .line 173
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->f()V

    .line 174
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->clearAnimation()V

    .line 129
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartTime(J)V

    .line 131
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setSelected(Z)V

    .line 199
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->t:Ldeb;

    invoke-interface {v0}, Ldeb;->a()V

    .line 181
    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 212
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->f:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 214
    return-void

    :cond_0
    move v0, v2

    .line 212
    goto :goto_0

    :cond_1
    move v1, v2

    .line 213
    goto :goto_1
.end method

.method public final e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 188
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->m:Z

    .line 189
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Z

    .line 190
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->o:Z

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->p:[Ljava/lang/String;

    .line 192
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->q:I

    .line 193
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->f()V

    .line 194
    return-void
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->o:Z

    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->f()V

    .line 224
    return-void
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setSelected(Z)V

    .line 232
    return-void
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c()V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartTime(J)V

    .line 240
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->p:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->p:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->r:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->p:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->q:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->s:Ldcp;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 158
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b()V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->t:Ldeb;

    invoke-interface {v0}, Ldeb;->e()V

    .line 162
    :cond_1
    return-void

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->t:Ldeb;

    invoke-interface {v0}, Ldeb;->a()V

    goto :goto_0

    .line 150
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->d:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v0, :cond_4

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->t:Ldeb;

    invoke-interface {v0}, Ldeb;->b()V

    goto :goto_0

    .line 152
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v0, :cond_5

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->t:Ldeb;

    invoke-interface {v0}, Ldeb;->c()V

    goto :goto_0

    .line 154
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->t:Ldeb;

    invoke-interface {v0}, Ldeb;->d()V

    goto :goto_0
.end method

.method protected final onFinishInflate()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 79
    const v0, 0x7f0801b3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->a:Landroid/widget/RelativeLayout;

    .line 80
    const v0, 0x7f0801b8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v0, 0x7f0801b6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    const v0, 0x7f0801ba

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->d:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->d:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    const v0, 0x7f0801b4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const v0, 0x7f0801b5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->f:Landroid/view/View;

    .line 89
    const v0, 0x7f0801bc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->d(Z)V

    .line 92
    invoke-virtual {p0, p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-void
.end method
