.class public Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Lbrv;

.field private c:Lbrw;

.field private d:Lbrw;

.field private e:Lbrt;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 51
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->f:I

    .line 56
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    .line 57
    const v0, 0x7f040093

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b(I)V

    .line 58
    const v0, 0x7f040090

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c(I)V

    .line 59
    const v0, 0x7f040092

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d(I)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 51
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->f:I

    .line 64
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    .line 65
    invoke-direct {p0, p3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b(I)V

    .line 66
    invoke-direct {p0, p2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d(I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;III)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 51
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->f:I

    .line 75
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    .line 76
    invoke-direct {p0, p3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b(I)V

    .line 77
    invoke-direct {p0, p4}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c(I)V

    .line 78
    invoke-direct {p0, p2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d(I)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 86
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->f:I

    .line 87
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    .line 88
    sget-object v0, Lgvk;->c:[I

    .line 89
    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 90
    const v1, 0x7f040093

    .line 91
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 90
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b(I)V

    .line 94
    const v1, 0x7f040090

    .line 95
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 94
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c(I)V

    .line 98
    const v1, 0x7f040092

    .line 99
    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 98
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d(I)V

    .line 102
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 105
    invoke-virtual {p0, v4}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 106
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    return-object v0
.end method

.method private b(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 109
    new-instance v0, Lbrw;

    const/4 v2, 0x2

    move-object v1, p0

    move v3, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lbrw;-><init>(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;IIIB)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d:Lbrw;

    .line 110
    return-void
.end method

.method private c(I)V
    .locals 6

    .prologue
    .line 113
    new-instance v0, Lbrw;

    const/4 v2, 0x5

    const v4, 0x7f080225

    const/4 v5, 0x0

    move-object v1, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lbrw;-><init>(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;IIIB)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c:Lbrw;

    .line 114
    return-void
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 117
    new-instance v0, Lbrt;

    const/4 v1, 0x4

    const v2, 0x7f080227

    invoke-direct {v0, p0, v1, p1, v2}, Lbrt;-><init>(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;III)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Lbrt;

    .line 118
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 176
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 186
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->f:I

    if-eq v0, p1, :cond_3

    .line 188
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->getChildCount()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d:Lbrw;

    invoke-virtual {v0, p1}, Lbrw;->a(I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Lbrt;

    invoke-virtual {v0, p1}, Lbrt;->a(I)V

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c:Lbrw;

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c:Lbrw;

    invoke-virtual {v0, p1}, Lbrw;->a(I)V

    .line 200
    :cond_2
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->f:I

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b:Lbrv;

    if-eqz v0, :cond_3

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b:Lbrv;

    invoke-interface {v0, p1}, Lbrv;->a(I)V

    .line 205
    :cond_3
    return-void
.end method

.method public final a(Lfve;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Lbrt;

    iput-object p1, v0, Lbrt;->a:Lfve;

    .line 126
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c:Lbrw;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c:Lbrw;

    invoke-virtual {v0, p1}, Lbrw;->a(Ljava/lang/CharSequence;)V

    .line 168
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 169
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Z)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Lbrt;

    invoke-virtual {v0, p1}, Lbrt;->a(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Lbrt;

    invoke-virtual {v0, p2}, Lbrt;->a(Z)V

    .line 150
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 151
    return-void
.end method
