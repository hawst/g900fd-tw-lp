.class public Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final j:Ljava/util/EnumMap;


# instance fields
.field private a:Ldbe;

.field private b:Lcws;

.field private c:Levn;

.field private d:Lezf;

.field private e:Z

.field private f:J

.field private g:Z

.field private h:Ldbv;

.field private i:Ldbt;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 317
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Ldbs;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 318
    sput-object v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->j:Ljava/util/EnumMap;

    sget-object v1, Ldbs;->a:Ldbs;

    sget-object v2, Ldbl;->d:Ldbl;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    sget-object v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->j:Ljava/util/EnumMap;

    sget-object v1, Ldbs;->b:Ldbs;

    sget-object v2, Ldbl;->c:Ldbl;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    sget-object v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->j:Ljava/util/EnumMap;

    sget-object v1, Ldbs;->c:Ldbs;

    sget-object v2, Ldbl;->b:Ldbl;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    sget-object v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->j:Ljava/util/EnumMap;

    sget-object v1, Ldbs;->d:Ldbs;

    sget-object v2, Ldbl;->a:Ldbl;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    sget-object v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->j:Ljava/util/EnumMap;

    sget-object v1, Ldbs;->g:Ldbs;

    sget-object v2, Ldbl;->e:Ldbl;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    sget-object v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->j:Ljava/util/EnumMap;

    sget-object v1, Ldbs;->f:Ldbs;

    sget-object v2, Ldbl;->f:Ldbl;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    sget-object v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->j:Ljava/util/EnumMap;

    sget-object v1, Ldbs;->e:Ldbs;

    sget-object v2, Ldbl;->f:Ldbl;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 331
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->f:J

    return-wide v0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;J)J
    .locals 2

    .prologue
    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->f:J

    return-wide v0
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;Ldbt;)Ldbt;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->i:Ldbt;

    return-object p1
.end method

.method public static synthetic a()Ljava/util/EnumMap;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->j:Ljava/util/EnumMap;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcws;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcws;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbe;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lezf;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d:Lezf;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->i:Ldbt;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e:Z

    return v0
.end method

.method private handlePlaybackServiceException(Lczb;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    sget-object v1, Ldbl;->f:Ldbl;

    invoke-virtual {v0, v1}, Ldbe;->a(Ldbl;)V

    .line 220
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopForeground(Z)V

    .line 221
    return-void
.end method

.method private handleSequencerHasPreviousNextEvent(Lczt;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    iget-boolean v1, p1, Lczt;->a:Z

    iget-boolean v2, p1, Lczt;->b:Z

    invoke-virtual {v0, v1, v2}, Ldbe;->a(ZZ)V

    .line 226
    return-void
.end method

.method private handleVideoStageEvent(Ldac;)V
    .locals 7
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 182
    iget-object v0, p1, Ldac;->a:Lgol;

    .line 183
    new-array v1, v4, [Lgol;

    sget-object v2, Lgol;->j:Lgol;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lgol;->a([Lgol;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 184
    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopForeground(Z)V

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    new-array v1, v4, [Lgol;

    sget-object v2, Lgol;->b:Lgol;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lgol;->a([Lgol;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->g:Z

    if-eqz v1, :cond_0

    sget-object v1, Lgol;->b:Lgol;

    .line 186
    invoke-virtual {v0, v1}, Lgol;->a(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    :cond_2
    iget-object v0, p1, Ldac;->b:Lfrl;

    iget-object v0, v0, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->g:Z

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    iget-object v4, p1, Ldac;->b:Lfrl;

    invoke-static {v4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v4, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Ldbe;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v4, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ldbe;->d:Ljava/lang/String;

    iget-object v0, v1, Ldbe;->c:Ldbk;

    invoke-virtual {v0}, Ldbk;->a()V

    invoke-virtual {v1}, Ldbe;->d()V

    iget-object v0, v1, Ldbe;->a:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lfrl;->b()Lfnc;

    move-result-object v0

    iget-object v0, v0, Lfnc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v6, 0x0

    :goto_1
    iget-object v0, v4, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lfrl;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lfrl;->c()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    invoke-virtual/range {v1 .. v6}, Ldbe;->a(Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;)V

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnb;

    iget-object v6, v0, Lfnb;->a:Landroid/net/Uri;

    goto :goto_1

    :cond_4
    iget-object v0, v1, Ldbe;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ldbe;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private handleVideoTimeEvent(Ldad;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 230
    iget-wide v0, p1, Ldad;->a:J

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->f:J

    .line 231
    return-void
.end method

.method private handleYouTubePlayerStateEvent(Ldae;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 198
    iget v0, p1, Ldae;->a:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 199
    iget v0, p1, Ldae;->a:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e:Z

    .line 200
    iget v0, p1, Ldae;->a:I

    packed-switch v0, :pswitch_data_0

    .line 212
    :cond_1
    :goto_1
    :pswitch_0
    return-void

    :cond_2
    move v0, v1

    .line 199
    goto :goto_0

    .line 204
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcws;

    invoke-virtual {v0}, Lcws;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    invoke-virtual {v0}, Ldbe;->a()V

    goto :goto_1

    .line 209
    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopForeground(Z)V

    goto :goto_1

    .line 200
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 18

    .prologue
    .line 63
    invoke-super/range {p0 .. p0}, Landroid/app/Service;->onCreate()V

    .line 64
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcwy;

    .line 65
    invoke-interface {v1}, Lcwy;->o()Lezg;

    move-result-object v16

    .line 66
    invoke-interface {v1}, Lcwy;->p()Letc;

    move-result-object v17

    .line 67
    invoke-virtual/range {v17 .. v17}, Letc;->i()Levn;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Levn;

    .line 68
    new-instance v1, Ldbe;

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->getApplication()Landroid/app/Application;

    move-result-object v2

    .line 70
    invoke-virtual/range {v17 .. v17}, Letc;->p()Landroid/os/Handler;

    move-result-object v3

    .line 71
    invoke-interface/range {v16 .. v16}, Lezg;->b()Lfxe;

    move-result-object v4

    .line 72
    invoke-interface/range {v16 .. v16}, Lezg;->c()Leyp;

    move-result-object v5

    .line 73
    invoke-interface/range {v16 .. v16}, Lezg;->aD()Lcst;

    move-result-object v6

    .line 74
    invoke-interface/range {v16 .. v16}, Lezg;->O()Lgng;

    move-result-object v7

    .line 75
    invoke-virtual/range {v17 .. v17}, Letc;->b()Lexd;

    move-result-object v8

    .line 76
    invoke-virtual/range {v17 .. v17}, Letc;->e()Ljava/lang/String;

    move-result-object v9

    .line 77
    invoke-interface/range {v16 .. v16}, Lezg;->j()Ljava/lang/Class;

    move-result-object v10

    new-instance v11, Lcvg;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcvg;-><init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)V

    const v12, 0x7f020196

    const/4 v13, 0x0

    .line 81
    invoke-interface/range {v16 .. v16}, Lezg;->aS()Ldbb;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct/range {v1 .. v15}, Ldbe;-><init>(Landroid/content/Context;Landroid/os/Handler;Lfxe;Leyp;Lgix;Lgng;Lexd;Ljava/lang/String;Ljava/lang/Class;Ldbj;ILdba;Ldbb;B)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    .line 83
    invoke-interface/range {v16 .. v16}, Lezg;->S()Lcws;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcws;

    .line 84
    invoke-interface/range {v16 .. v16}, Lezg;->aN()Lezf;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d:Lezf;

    .line 85
    new-instance v1, Ldbv;

    .line 86
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcws;

    .line 88
    invoke-virtual/range {v17 .. v17}, Letc;->i()Levn;

    move-result-object v4

    new-instance v5, Lcvf;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcvf;-><init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)V

    invoke-direct {v1, v2, v3, v4, v5}, Ldbv;-><init>(Landroid/content/res/Resources;Lcws;Levn;Ldbr;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->h:Ldbv;

    .line 91
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 92
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    new-instance v2, Lcve;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcve;-><init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)V

    invoke-virtual {v1, v2}, Ldbe;->a(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 100
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->h:Ldbv;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcws;

    invoke-virtual {v0}, Lcws;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcws;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcws;->a(Z)V

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    invoke-virtual {v0}, Ldbe;->b()V

    .line 159
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    .line 160
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->i:Ldbt;

    .line 161
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->h:Ldbv;

    .line 162
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 163
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 113
    const-string v0, "background_mode"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d:Lezf;

    invoke-virtual {v1}, Lezf;->a()V

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->g:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Levn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->h:Ldbv;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcws;

    invoke-virtual {v0}, Lcws;->t()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    invoke-virtual {v0}, Ldbe;->a()V

    .line 117
    :cond_0
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Ldbe;

    invoke-virtual {v0}, Ldbe;->b()V

    goto :goto_0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcws;

    invoke-virtual {v0}, Lcws;->b()V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopSelf()V

    .line 127
    return-void
.end method
