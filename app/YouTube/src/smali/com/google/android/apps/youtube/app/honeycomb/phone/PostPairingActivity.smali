.class public Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;
.super Lavv;
.source "SourceFile"


# static fields
.field private static final e:Ljava/util/Map;


# instance fields
.field private f:Lgix;

.field private g:Lfxe;

.field private h:Leyp;

.field private i:Lexd;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/TextView;

.field private o:Landroid/widget/ImageView;

.field private p:Lawb;

.field private q:Ldsn;

.field private r:Ldwq;

.field private s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private t:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->e:Ljava/util/Map;

    .line 76
    invoke-static {}, Lbgx;->values()[Lbgx;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 77
    sget-object v4, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->e:Ljava/util/Map;

    iget v5, v3, Lbgx;->h:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lavv;-><init>()V

    .line 261
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Ldwq;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->r:Ldwq;

    return-object v0
.end method

.method private a(Lbgx;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0xf

    .line 202
    new-instance v0, Lbgy;

    invoke-direct {v0, p0, p1}, Lbgy;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;Lbgx;)V

    .line 203
    invoke-static {p0, v0}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v5

    .line 204
    sget-object v0, Lbgw;->a:[I

    invoke-virtual {p1}, Lbgx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 234
    :goto_0
    return-void

    .line 206
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->g:Lfxe;

    invoke-interface {v0, v3, v5}, Lfxe;->a(ILeuc;)V

    goto :goto_0

    .line 209
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->g:Lfxe;

    invoke-interface {v0, v3, v5}, Lfxe;->d(ILeuc;)V

    goto :goto_0

    .line 212
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->g:Lfxe;

    invoke-interface {v0, v3, v5}, Lfxe;->b(ILeuc;)V

    goto :goto_0

    .line 215
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->g:Lfxe;

    invoke-interface {v0, v3, v5}, Lfxe;->c(ILeuc;)V

    goto :goto_0

    .line 218
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->g()Ljava/lang/String;

    move-result-object v3

    .line 219
    sget-object v0, Lfxj;->b:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v3, v2

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->g:Lfxe;

    sget-object v1, Lfxm;->a:Lfxm;

    sget-object v4, Lfxn;->b:Lfxn;

    invoke-interface/range {v0 .. v5}, Lfxe;->a(Lfxm;Ljava/lang/String;Ljava/lang/String;Lfxn;Leuc;)V

    goto :goto_0

    .line 228
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->g:Lfxe;

    sget-object v1, Lfxm;->a:Lfxm;

    const-string v2, "Music"

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 229
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->g()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lfxn;->b:Lfxn;

    .line 228
    invoke-interface/range {v0 .. v5}, Lfxe;->a(Lfxm;Ljava/lang/String;Ljava/lang/String;Lfxn;Leuc;)V

    goto :goto_0

    .line 232
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->g:Lfxe;

    sget-object v1, Lfxm;->a:Lfxm;

    sget-object v4, Lfxn;->b:Lfxn;

    move-object v3, v2

    invoke-interface/range {v0 .. v5}, Lfxe;->a(Lfxm;Ljava/lang/String;Ljava/lang/String;Lfxn;Leuc;)V

    goto :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;Lbgx;)V
    .locals 2

    .prologue
    .line 46
    iget v0, p1, Lbgx;->h:I

    invoke-static {}, Lbgx;->values()[Lbgx;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "We run out of feeds! How?"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->e:Ljava/util/Map;

    iget v1, p1, Lbgx;->h:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgx;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->f:Lgix;

    invoke-interface {v1}, Lgix;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lbgx;->j:Z

    if-eqz v1, :cond_1

    sget-object v0, Lbgx;->e:Lbgx;

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lbgx;)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Lawb;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lawb;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->o:Landroid/widget/ImageView;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->t:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public handleSignOutEvent(Lfcc;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->finish()V

    .line 194
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->k()Z

    .line 199
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 96
    invoke-super {p0, p1}, Lavv;->onCreate(Landroid/os/Bundle;)V

    .line 97
    const v0, 0x7f0400b6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->setContentView(I)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->d()Lkm;

    move-result-object v0

    const v1, 0x7f0902f2

    invoke-virtual {v0, v1}, Lkm;->b(I)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v1, v1, Lckz;->a:Letc;

    .line 103
    invoke-virtual {v0}, Lari;->c()Leyp;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->h:Leyp;

    .line 104
    invoke-virtual {v0}, Lari;->b()Lfxe;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->g:Lfxe;

    .line 105
    invoke-virtual {v0}, Lari;->aD()Lcst;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->f:Lgix;

    .line 106
    invoke-virtual {v1}, Letc;->b()Lexd;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->i:Lexd;

    .line 107
    invoke-virtual {v0}, Lari;->K()Ldsn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->q:Ldsn;

    .line 109
    const v0, 0x7f080268

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->j:Landroid/view/View;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->j:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->k:Landroid/widget/TextView;

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->j:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->o:Landroid/widget/ImageView;

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->j:Landroid/view/View;

    const v1, 0x7f08027c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->t:Landroid/view/View;

    .line 114
    new-instance v0, Lbgv;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->h:Leyp;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->i:Lexd;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lbgv;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;Landroid/content/Context;Leyp;Lexd;Laws;)V

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->o:Landroid/widget/ImageView;

    .line 139
    invoke-virtual {v0, v1, v5}, Lawr;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lawm;

    move-result-object v0

    check-cast v0, Lawb;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lawb;

    .line 140
    return-void
.end method

.method public onMdxStateChangedEvent(Ldwx;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 256
    sget-object v0, Ldww;->b:Ldww;

    iget-object v1, p1, Ldwx;->a:Ldww;

    if-eq v0, v1, :cond_0

    .line 257
    iget-object v0, p0, Lbhz;->m:Lfus;

    invoke-virtual {v0}, Lfus;->c()V

    .line 259
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Lavv;->onPause()V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 184
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 176
    invoke-super {p0}, Lavv;->onResume()V

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 178
    return-void
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 144
    invoke-super {p0}, Lavv;->onStart()V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->q:Ldsn;

    iget-object v0, v0, Ldsn;->d:Ldwq;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->r:Ldwq;

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->r:Ldwq;

    if-nez v0, :cond_0

    .line 148
    const-string v0, "Ooops! We should be connected a route but that\'s not the case!"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->finish()V

    .line 166
    :goto_0
    return-void

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->f:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 154
    sget-object v0, Lbgx;->a:Lbgx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lbgx;)V

    .line 159
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->r:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_1

    .line 161
    const v0, 0x7f080278

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 162
    const v1, 0x7f0902ec

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->r:Ldwq;

    .line 163
    invoke-interface {v4}, Ldwq;->n()Ldwr;

    move-result-object v4

    invoke-virtual {v4}, Ldwr;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 162
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 156
    :cond_2
    sget-object v0, Lbgx;->e:Lbgx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lbgx;)V

    goto :goto_1
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 171
    invoke-super {p0}, Lavv;->onStop()V

    .line 172
    return-void
.end method
