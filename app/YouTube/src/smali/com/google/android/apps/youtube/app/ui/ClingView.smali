.class public Lcom/google/android/apps/youtube/app/ui/ClingView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field a:Lbob;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private final d:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    sget-object v0, Lgvk;->b:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 32
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 33
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 35
    if-eqz v1, :cond_0

    .line 36
    invoke-static {p1, v1}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->d:Landroid/graphics/drawable/Drawable;

    .line 37
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()[I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    if-nez v0, :cond_0

    move-object v0, v3

    .line 113
    :goto_0
    return-object v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 105
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->b:Landroid/view/View;

    if-eq v0, v4, :cond_1

    if-eqz v0, :cond_1

    .line 106
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    add-int/2addr v2, v4

    .line 107
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    add-int/2addr v1, v4

    .line 108
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_1

    .line 110
    :cond_1
    if-nez v0, :cond_2

    move-object v0, v3

    .line 111
    goto :goto_0

    .line 113
    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v3, 0x0

    aput v2, v0, v3

    const/4 v2, 0x1

    aput v1, v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/graphics/Rect;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 117
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ClingView;->b()[I

    move-result-object v1

    .line 119
    if-eqz v1, :cond_0

    .line 120
    new-instance v0, Landroid/graphics/Rect;

    aget v2, v1, v4

    aget v3, v1, v6

    aget v4, v1, v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    .line 121
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    aget v1, v1, v6

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    .line 122
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v1, v5

    invoke-direct {v0, v2, v3, v4, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 127
    :goto_0
    return-object v0

    .line 125
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 53
    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    .line 54
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->b:Landroid/view/View;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/ClingView;->postInvalidate()V

    .line 56
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 68
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 69
    const/high16 v0, -0x28000000

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ClingView;->b()[I

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    move-object v6, v0

    .line 75
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 80
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v4, v0

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 81
    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 82
    iget v0, v6, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 83
    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v1, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 85
    :cond_3
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 86
    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v0

    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    .line 87
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p1

    .line 86
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 90
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->a:Lbob;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->a:Lbob;

    invoke-interface {v0, v6}, Lbob;->a(Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 74
    :cond_5
    const/4 v2, 0x2

    new-array v3, v2, [I

    aget v2, v0, v6

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    aput v2, v3, v6

    aget v0, v0, v7

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    aput v0, v3, v7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    :goto_3
    aget v4, v3, v6

    div-int/lit8 v6, v0, 0x2

    sub-int/2addr v4, v6

    aget v3, v3, v7

    div-int/lit8 v6, v2, 0x2

    sub-int/2addr v3, v6

    add-int v6, v4, v0

    add-int/2addr v2, v3

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v3, v6, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v6, v0

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ClingView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    goto :goto_3
.end method
