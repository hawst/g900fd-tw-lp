.class public Lcom/google/android/apps/youtube/core/transfer/UploadService;
.super Ldmu;
.source "SourceFile"


# instance fields
.field private f:Ljava/util/concurrent/Executor;

.field private g:Lorg/apache/http/client/HttpClient;

.field private h:Lfxe;

.field private i:Lgix;

.field private j:Lgad;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ldmu;-><init>()V

    .line 104
    return-void
.end method

.method public static a(Landroid/content/Context;Ldmw;)Lfad;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/youtube/core/transfer/UploadService;

    invoke-static {p0, v0, p1}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;Ldmw;)Lfad;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lgjm;Ldnb;)Ldna;
    .locals 9

    .prologue
    .line 46
    new-instance v0, Ldns;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->f:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->g:Lorg/apache/http/client/HttpClient;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->h:Lfxe;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->i:Lgix;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->j:Lgad;

    move-object v1, p0

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Ldns;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfxe;Lgix;Lgad;Lgjm;Ldnb;)V

    return-object v0
.end method

.method public final a(Lgjm;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const-string v0, "uploads.db"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, "upload_policy"

    return-object v0
.end method

.method protected final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 64
    invoke-super {p0}, Ldmu;->onCreate()V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lckz;

    .line 66
    invoke-virtual {v0}, Lckz;->n()Lcla;

    move-result-object v1

    .line 67
    iget-object v2, v0, Lckz;->a:Letc;

    .line 68
    invoke-virtual {v2}, Letc;->h()Ljava/util/concurrent/Executor;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->f:Ljava/util/concurrent/Executor;

    .line 69
    invoke-virtual {v2}, Letc;->l()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->g:Lorg/apache/http/client/HttpClient;

    .line 70
    invoke-virtual {v1}, Lcla;->aD()Lcst;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->i:Lgix;

    .line 71
    invoke-virtual {v1}, Lcla;->ax()Lfbc;

    move-result-object v1

    .line 72
    new-instance v2, Lgad;

    invoke-direct {v2, v1}, Lgad;-><init>(Lfbc;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->j:Lgad;

    .line 73
    invoke-virtual {v0}, Lckz;->n()Lcla;

    move-result-object v0

    invoke-virtual {v0}, Lcla;->b()Lfxe;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->h:Lfxe;

    .line 74
    return-void
.end method
