.class public Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;
.super Lemh;
.source "SourceFile"


# instance fields
.field private a:Lari;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lemh;-><init>()V

    return-void
.end method

.method private a()I
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a:Lari;

    invoke-virtual {v0}, Lari;->M()Lffp;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a:Lari;

    .line 106
    invoke-virtual {v1}, Lari;->ag()Lbku;

    move-result-object v1

    .line 107
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a:Lari;

    invoke-virtual {v2}, Lari;->ao()Lbnm;

    move-result-object v2

    .line 111
    :try_start_0
    invoke-virtual {v0}, Lffp;->a()Lffr;

    move-result-object v3

    iget-object v0, v0, Lffp;->e:Lffq;

    invoke-virtual {v0, v3}, Lffq;->c(Lfsp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmm;

    .line 112
    invoke-virtual {v1, v0}, Lbku;->a(Lfmm;)V

    .line 116
    invoke-virtual {v2}, Lbnm;->a()V
    :try_end_0
    .catch Lfdv; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    const/4 v0, 0x0

    .line 120
    :goto_0
    return v0

    .line 118
    :catch_0
    move-exception v0

    .line 119
    const-string v1, "Failed to fetch settings"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 120
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(I)I
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 137
    if-nez p0, :cond_1

    .line 138
    const/4 v0, 0x0

    .line 143
    :cond_0
    :goto_0
    return v0

    .line 140
    :cond_1
    if-eq p0, v0, :cond_0

    .line 143
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Lgnd;
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a:Lari;

    invoke-virtual {v0}, Lari;->aD()Lcst;

    move-result-object v0

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a:Lari;

    invoke-virtual {v1}, Lari;->O()Lgng;

    move-result-object v1

    .line 128
    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    .line 129
    iget-object v2, v0, Lgit;->c:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 130
    const/4 v0, 0x0

    .line 133
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1, v0}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 156
    if-gez v0, :cond_0

    .line 157
    const-string v0, ""

    .line 159
    :goto_0
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    .line 40
    :try_start_0
    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    move-object v2, p1

    .line 42
    :goto_0
    const-string v3, "offline_r"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 43
    invoke-static {p1}, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->b(Ljava/lang/String;)Lgnd;

    move-result-object v1

    if-nez v1, :cond_2

    .line 57
    :cond_0
    :goto_1
    return v0

    .line 40
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 43
    :cond_2
    invoke-interface {v1}, Lgnd;->j()Lgmv;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lgmv;->a()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a(I)I

    move-result v0

    goto :goto_1

    .line 44
    :cond_3
    const-string v3, "offline_c"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 45
    invoke-static {p1}, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->b(Ljava/lang/String;)Lgnd;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lgnd;->j()Lgmv;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lgmv;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a(I)I

    move-result v0

    goto :goto_1

    .line 46
    :cond_4
    const-string v3, "preload_videos"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 47
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a:Lari;

    invoke-virtual {v2}, Lari;->W()Ldhi;

    move-result-object v2

    invoke-interface {v2}, Ldhi;->b()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1

    .line 48
    :cond_6
    const-string v1, "settings_fetch"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 53
    :catch_0
    move-exception v1

    .line 54
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error occurred processing task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 51
    :cond_7
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown task tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; aborting..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Lemh;->onCreate()V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/task/YouTubeNetworkTaskService;->a:Lari;

    .line 35
    return-void
.end method
