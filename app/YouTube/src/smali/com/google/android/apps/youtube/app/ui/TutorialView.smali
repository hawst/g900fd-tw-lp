.class public Lcom/google/android/apps/youtube/app/ui/TutorialView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbob;


# instance fields
.field public a:Lbxt;

.field private b:Lcom/google/android/apps/youtube/app/ui/ClingView;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/LinearLayout;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->f:I

    .line 39
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->b()V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a:Lbxt;

    invoke-interface {v0}, Lbxt;->a()V

    .line 108
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 74
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 82
    :goto_0
    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setVisibility(I)V

    .line 79
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 80
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 81
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 141
    const/4 v0, 0x1

    const-string v1, "Only  RelativeLayout.ALIGN_BOTTOM or RelativeLayout.ALIGN_RIGHT are supported at the moment"

    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    .line 145
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->f:I

    .line 146
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->e:Landroid/widget/LinearLayout;

    .line 118
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 119
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->f:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 121
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 122
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->e:Landroid/widget/LinearLayout;

    .line 124
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 136
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    return-void

    .line 125
    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->f:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 126
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 128
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 129
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 130
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->d:Landroid/widget/TextView;

    .line 132
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 133
    const/16 v2, 0x31

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 134
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->b:Lcom/google/android/apps/youtube/app/ui/ClingView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/ClingView;->a(Landroid/view/View;Landroid/view/View;)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->postInvalidate()V

    .line 103
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 91
    :goto_0
    return-void

    .line 90
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->c:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 96
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->c()V

    .line 98
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 52
    const v0, 0x7f08020a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->c:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->c:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    const v0, 0x7f080207

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ClingView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->b:Lcom/google/android/apps/youtube/app/ui/ClingView;

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->b:Lcom/google/android/apps/youtube/app/ui/ClingView;

    iput-object p0, v0, Lcom/google/android/apps/youtube/app/ui/ClingView;->a:Lbob;

    .line 56
    const v0, 0x7f0800c1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->d:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f08030a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->e:Landroid/widget/LinearLayout;

    .line 58
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->b:Lcom/google/android/apps/youtube/app/ui/ClingView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ClingView;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 63
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->c()V

    .line 67
    :cond_0
    const/4 v0, 0x0

    .line 69
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
