.class public final Lcom/google/android/gms/googlehelp/GoogleHelp;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public b:Ljava/lang/String;

.field public c:Landroid/accounts/Account;

.field public d:Landroid/os/Bundle;

.field public e:Z

.field public f:Z

.field public g:Ljava/util/List;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Landroid/os/Bundle;

.field public k:Landroid/graphics/Bitmap;

.field public l:[B

.field public m:I

.field public n:I

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Landroid/net/Uri;

.field public s:Ljava/util/List;

.field public t:I

.field public u:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lemo;

    invoke-direct {v0}, Lemo;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIIZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->a:I

    iput-object p2, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->c:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->d:Landroid/os/Bundle;

    iput-boolean p5, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->e:Z

    iput-boolean p6, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->f:Z

    iput-object p7, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->g:Ljava/util/List;

    iput-object p8, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->h:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->i:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->j:Landroid/os/Bundle;

    iput-object p11, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->k:Landroid/graphics/Bitmap;

    iput-object p12, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->l:[B

    iput p13, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->m:I

    iput p14, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->n:I

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->o:Z

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->p:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->q:Ljava/lang/String;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->r:Landroid/net/Uri;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->s:Ljava/util/List;

    move/from16 v0, p20

    iput v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->t:I

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->u:Ljava/util/List;

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;Z)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 11

    const/high16 v10, 0x40000

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x3c

    invoke-virtual {p1, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v3, v2

    if-lez v3, :cond_0

    array-length v3, v2

    if-gt v3, v10, :cond_0

    iput-object v2, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->l:[B

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->m:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->n:I

    move-object v0, v1

    move-object v1, p0

    :goto_1
    iput-object v0, v1, Lcom/google/android/gms/googlehelp/GoogleHelp;->k:Landroid/graphics/Bitmap;

    return-object p0

    :cond_0
    if-eqz v2, :cond_1

    array-length v2, v2

    if-le v2, v10, :cond_1

    if-nez p2, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-double v2, v2

    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v4, v2

    double-to-int v4, v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-double v6, v5

    div-double v2, v6, v2

    double-to-int v2, v2

    invoke-static {p1, v4, v2, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    move p2, v0

    goto :goto_0

    :cond_1
    const-string v2, "GOOGLEHELP_GoogleHelp"

    const-string v3, "The bytes of the compressed jpeg is too large."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->l:[B

    if-nez p1, :cond_2

    move-object v0, v1

    move-object v1, p0

    goto :goto_1

    :cond_2
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-double v2, v2

    const-wide/high16 v4, 0x4110000000000000L    # 262144.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v4, v2

    double-to-int v4, v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-double v6, v5

    div-double v2, v6, v2

    double-to-int v2, v2

    invoke-static {v1, v4, v2, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, p0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 22

    new-instance v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    const/16 v20, 0x0

    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v2, p0

    invoke-direct/range {v0 .. v21}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIIZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.googlehelp.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->c:Landroid/accounts/Account;

    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->k:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->l:[B

    :goto_0
    return-object p0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/graphics/Bitmap;Z)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->r:Landroid/net/Uri;

    return-object p0
.end method

.method public final a(Landroid/os/Bundle;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->j:Landroid/os/Bundle;

    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lemo;->a(Lcom/google/android/gms/googlehelp/GoogleHelp;Landroid/os/Parcel;I)V

    return-void
.end method
