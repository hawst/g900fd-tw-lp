.class public final Lcom/google/zxing/client/android/CaptureActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Lifu;

.field public c:Lifk;

.field public d:Lcom/google/zxing/client/android/ViewfinderView;

.field public e:Lifo;

.field private f:Landroid/widget/TextView;

.field private g:Z

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/google/zxing/client/android/CaptureActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/client/android/CaptureActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 276
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 277
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 278
    const v1, 0x7f090001

    invoke-virtual {p0, v1}, Lcom/google/zxing/client/android/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 279
    const v1, 0x104000a

    new-instance v2, Lifn;

    invoke-direct {v2, p0}, Lifn;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 280
    new-instance v1, Lifn;

    invoke-direct {v1, p0}, Lifn;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 281
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 282
    return-void
.end method

.method public static a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lifb;Lifb;)V
    .locals 6

    .prologue
    .line 253
    iget v1, p2, Lifb;->a:F

    iget v2, p2, Lifb;->b:F

    iget v3, p3, Lifb;->a:F

    iget v4, p3, Lifb;->b:F

    move-object v0, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 254
    return-void
.end method

.method private a(Landroid/view/SurfaceHolder;)V
    .locals 9

    .prologue
    .line 258
    :try_start_0
    iget-object v3, p0, Lcom/google/zxing/client/android/CaptureActivity;->b:Lifu;

    iget-object v0, v3, Lifu;->c:Landroid/hardware/Camera;

    if-nez v0, :cond_2

    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 264
    :catch_0
    move-exception v0

    .line 265
    sget-object v1, Lcom/google/zxing/client/android/CaptureActivity;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 266
    invoke-direct {p0}, Lcom/google/zxing/client/android/CaptureActivity;->a()V

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    :try_start_1
    iput-object v0, v3, Lifu;->c:Landroid/hardware/Camera;

    :cond_2
    move-object v2, v0

    invoke-virtual {v2, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    iget-boolean v0, v3, Lifu;->f:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, v3, Lifu;->f:Z

    iget-object v4, v3, Lifu;->b:Lift;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    iget-object v0, v4, Lift;->a:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    if-ge v0, v1, :cond_7

    :goto_1
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    iput-object v6, v4, Lift;->b:Landroid/graphics/Point;

    iget-object v0, v4, Lift;->b:Landroid/graphics/Point;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x13

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Screen resolution: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v4, Lift;->b:Landroid/graphics/Point;

    const/4 v1, 0x0

    invoke-static {v5, v0, v1}, Lift;->a(Landroid/hardware/Camera$Parameters;Landroid/graphics/Point;Z)Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, v4, Lift;->c:Landroid/graphics/Point;

    iget-object v0, v4, Lift;->c:Landroid/graphics/Point;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x13

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Camera resolution: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v3, Lifu;->i:I

    if-lez v0, :cond_3

    iget v0, v3, Lifu;->j:I

    if-lez v0, :cond_3

    iget v0, v3, Lifu;->i:I

    iget v1, v3, Lifu;->j:I

    invoke-virtual {v3, v0, v1}, Lifu;->a(II)V

    const/4 v0, 0x0

    iput v0, v3, Lifu;->i:I

    const/4 v0, 0x0

    iput v0, v3, Lifu;->j:I

    :cond_3
    iget-object v0, v3, Lifu;->b:Lift;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    if-nez v1, :cond_4

    const-string v0, "CameraConfiguration"

    const-string v1, "Device error: no camera parameters are available. Proceeding without configuration."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iget-object v0, v3, Lifu;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    const/4 v0, 0x0

    iput-boolean v0, v3, Lifu;->h:Z

    .line 261
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->c:Lifk;

    if-nez v0, :cond_0

    .line 262
    new-instance v0, Lifk;

    iget-object v1, p0, Lcom/google/zxing/client/android/CaptureActivity;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/zxing/client/android/CaptureActivity;->b:Lifu;

    invoke-direct {v0, p0, v1, v2}, Lifk;-><init>(Lcom/google/zxing/client/android/CaptureActivity;Ljava/lang/String;Lifu;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->c:Lifk;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 267
    :catch_1
    move-exception v0

    .line 270
    sget-object v1, Lcom/google/zxing/client/android/CaptureActivity;->a:Ljava/lang/String;

    const-string v2, "Unexpected error initializing camera"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 271
    invoke-direct {p0}, Lcom/google/zxing/client/android/CaptureActivity;->a()V

    goto/16 :goto_0

    .line 258
    :cond_4
    :try_start_2
    iget-object v4, v0, Lift;->a:Landroid/content/Context;

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "off"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lift;->a(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v1, v4}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "auto"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "macro"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lift;->a(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v1, v4}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    :cond_6
    iget-object v4, v0, Lift;->c:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v0, v0, Lift;->c:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v4, v0}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    invoke-virtual {v2, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :cond_7
    move v8, v1

    move v1, v0

    move v0, v8

    goto/16 :goto_1
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 81
    invoke-virtual {p0}, Lcom/google/zxing/client/android/CaptureActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 82
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 83
    const v0, 0x7f040030

    invoke-virtual {p0, v0}, Lcom/google/zxing/client/android/CaptureActivity;->setContentView(I)V

    .line 85
    const v0, 0x7f08010b

    invoke-virtual {p0, v0}, Lcom/google/zxing/client/android/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->f:Landroid/widget/TextView;

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->c:Lifk;

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->g:Z

    .line 88
    new-instance v0, Lifo;

    invoke-direct {v0, p0}, Lifo;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->e:Lifo;

    .line 89
    return-void
.end method

.method protected final onDestroy()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->e:Lifo;

    invoke-virtual {v0}, Lifo;->b()V

    iget-object v0, v0, Lifo;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 159
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 160
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 164
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 165
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/zxing/client/android/CaptureActivity;->setResult(I)V

    .line 166
    invoke-virtual {p0}, Lcom/google/zxing/client/android/CaptureActivity;->finish()V

    .line 172
    :cond_0
    :goto_0
    return v0

    .line 168
    :cond_1
    const/16 v1, 0x50

    if-eq p1, v1, :cond_0

    const/16 v1, 0x1b

    if-eq p1, v1, :cond_0

    .line 172
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 142
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->c:Lifk;

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->c:Lifk;

    const/4 v1, 0x3

    iput v1, v0, Lifk;->b:I

    iget-object v1, v0, Lifk;->c:Lifu;

    iget-object v2, v1, Lifu;->c:Landroid/hardware/Camera;

    if-eqz v2, :cond_0

    iget-boolean v2, v1, Lifu;->g:Z

    if-eqz v2, :cond_0

    iget-object v2, v1, Lifu;->c:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v2, v1, Lifu;->k:Lifv;

    invoke-virtual {v2, v4, v3}, Lifv;->a(Landroid/os/Handler;I)V

    iget-object v2, v1, Lifu;->l:Lifs;

    invoke-virtual {v2, v4, v3}, Lifs;->a(Landroid/os/Handler;I)V

    iput-boolean v3, v1, Lifu;->g:Z

    :cond_0
    iget-object v1, v0, Lifk;->a:Lifm;

    invoke-virtual {v1}, Lifm;->a()Landroid/os/Handler;

    move-result-object v1

    const v2, 0x7f080005

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    :try_start_0
    iget-object v1, v0, Lifk;->a:Lifm;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Lifm;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const v1, 0x7f080003

    invoke-virtual {v0, v1}, Lifk;->removeMessages(I)V

    const v1, 0x7f080002

    invoke-virtual {v0, v1}, Lifk;->removeMessages(I)V

    .line 144
    iput-object v4, p0, Lcom/google/zxing/client/android/CaptureActivity;->c:Lifk;

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->e:Lifo;

    invoke-virtual {v0}, Lifo;->b()V

    iget-object v1, v0, Lifo;->b:Landroid/app/Activity;

    iget-object v0, v0, Lifo;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 147
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->b:Lifu;

    iget-object v1, v0, Lifu;->c:Landroid/hardware/Camera;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lifu;->c:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    iput-object v4, v0, Lifu;->c:Landroid/hardware/Camera;

    iput-object v4, v0, Lifu;->d:Landroid/graphics/Rect;

    iput-object v4, v0, Lifu;->e:Landroid/graphics/Rect;

    .line 148
    :cond_2
    iget-boolean v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->g:Z

    if-nez v0, :cond_3

    .line 149
    const v0, 0x7f080109

    invoke-virtual {p0, v0}, Lcom/google/zxing/client/android/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    .line 150
    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 151
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 153
    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 154
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 93
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 100
    new-instance v0, Lifu;

    invoke-virtual {p0}, Lcom/google/zxing/client/android/CaptureActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1}, Lifu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->b:Lifu;

    .line 101
    const v0, 0x7f08010a

    invoke-virtual {p0, v0}, Lcom/google/zxing/client/android/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/client/android/ViewfinderView;

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    .line 102
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    iget-object v1, p0, Lcom/google/zxing/client/android/CaptureActivity;->b:Lifu;

    iput-object v1, v0, Lcom/google/zxing/client/android/ViewfinderView;->a:Lifu;

    .line 104
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->f:Landroid/widget/TextView;

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    invoke-virtual {v0, v3}, Lcom/google/zxing/client/android/ViewfinderView;->setVisibility(I)V

    .line 106
    const v0, 0x7f080109

    invoke-virtual {p0, v0}, Lcom/google/zxing/client/android/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    .line 107
    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 108
    iget-boolean v1, p0, Lcom/google/zxing/client/android/CaptureActivity;->g:Z

    if-eqz v1, :cond_2

    .line 112
    invoke-direct {p0, v0}, Lcom/google/zxing/client/android/CaptureActivity;->a(Landroid/view/SurfaceHolder;)V

    .line 119
    :goto_0
    invoke-virtual {p0}, Lcom/google/zxing/client/android/CaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 120
    if-nez v1, :cond_3

    const/4 v0, 0x0

    .line 121
    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    .line 122
    :cond_0
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 123
    const-string v2, "com.google.zxing.client.android.YOUTUBE_SCAN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    const-string v0, "SCAN_WIDTH"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SCAN_HEIGHT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    const-string v0, "SCAN_WIDTH"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 126
    const-string v2, "SCAN_HEIGHT"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 127
    if-lez v0, :cond_1

    if-lez v2, :cond_1

    .line 128
    iget-object v3, p0, Lcom/google/zxing/client/android/CaptureActivity;->b:Lifu;

    invoke-virtual {v3, v0, v2}, Lifu;->a(II)V

    .line 132
    :cond_1
    const-string v0, "CHARACTER_SET"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->h:Ljava/lang/String;

    .line 137
    :goto_2
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->e:Lifo;

    iget-object v1, v0, Lifo;->b:Landroid/app/Activity;

    iget-object v2, v0, Lifo;->c:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {v0}, Lifo;->a()V

    .line 138
    return-void

    .line 115
    :cond_2
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 116
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    goto :goto_0

    .line 120
    :cond_3
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 134
    :cond_4
    invoke-virtual {p0}, Lcom/google/zxing/client/android/CaptureActivity;->finish()V

    goto :goto_2
.end method

.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 176
    if-nez p1, :cond_0

    .line 177
    sget-object v0, Lcom/google/zxing/client/android/CaptureActivity;->a:Ljava/lang/String;

    const-string v1, "*** WARNING *** surfaceCreated() gave us a null surface!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :cond_0
    iget-boolean v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->g:Z

    if-nez v0, :cond_1

    .line 180
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->g:Z

    .line 181
    invoke-direct {p0, p1}, Lcom/google/zxing/client/android/CaptureActivity;->a(Landroid/view/SurfaceHolder;)V

    .line 183
    :cond_1
    return-void
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/client/android/CaptureActivity;->g:Z

    .line 187
    return-void
.end method
