.class public Ldls;
.super Li;
.source "SourceFile"

# interfaces
.implements Ldma;


# instance fields
.field public W:Ljava/lang/String;

.field public X:Lfkg;

.field public Y:Ldlv;

.field private Z:I

.field private aa:Z

.field private ab:Lxk;

.field private ac:Landroid/content/res/Resources;

.field private ad:Landroid/support/v4/view/ViewPager;

.field private ae:Ldlw;

.field private af:I

.field private ag:I

.field private ah:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Li;-><init>()V

    .line 328
    return-void
.end method

.method public static a(Ljava/lang/String;Lfkg;IZ)Ldls;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 95
    const-string v1, "videoId"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v1, "infoCardCollection"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 97
    const-string v1, "selectedCardIndex"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    const-string v1, "videoWasPlaying"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 100
    new-instance v1, Ldls;

    invoke-direct {v1}, Ldls;-><init>()V

    .line 101
    invoke-virtual {v1, v0}, Ldls;->f(Landroid/os/Bundle;)V

    .line 102
    iput-boolean p3, v1, Ldls;->aa:Z

    .line 103
    return-object v1
.end method

.method static synthetic a(Ldls;)Ldlv;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ldls;->Y:Ldlv;

    return-object v0
.end method

.method static synthetic b(Ldls;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic c(Ldls;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Ldls;->aa:Z

    return v0
.end method

.method static synthetic d(Ldls;)Lxk;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ldls;->ab:Lxk;

    return-object v0
.end method

.method private e(Z)V
    .locals 3

    .prologue
    .line 199
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p0}, Ldls;->j()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    if-eqz p1, :cond_0

    iget v0, p0, Ldls;->af:I

    iget v2, p0, Ldls;->ah:I

    add-int/2addr v0, v2

    :goto_0
    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v0, 0x2

    add-int/2addr v0, v1

    iget-object v1, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    neg-int v0, v0

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->c(I)V

    .line 200
    if-eqz p1, :cond_1

    iget v0, p0, Ldls;->af:I

    :goto_1
    iget-object v1, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 203
    iget-object v0, p0, Ldls;->ae:Ldlw;

    invoke-virtual {v0, p1}, Ldlw;->a(Z)V

    .line 205
    invoke-virtual {p0}, Ldls;->s()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 206
    invoke-virtual {p0}, Ldls;->s()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 207
    return-void

    .line 199
    :cond_0
    iget v0, p0, Ldls;->af:I

    goto :goto_0

    .line 200
    :cond_1
    iget v0, p0, Ldls;->af:I

    iget v1, p0, Ldls;->ag:I

    add-int/2addr v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 136
    invoke-super {p0, p1, p2, p3}, Li;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 138
    invoke-virtual {p0}, Ldls;->j()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Ldls;->ac:Landroid/content/res/Resources;

    .line 140
    iget-object v0, p0, Ldls;->ac:Landroid/content/res/Resources;

    const v1, 0x7f0a0078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ldls;->af:I

    .line 141
    iget-object v0, p0, Ldls;->ac:Landroid/content/res/Resources;

    const v1, 0x7f0a0079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ldls;->ag:I

    .line 143
    iget-object v0, p0, Ldls;->ac:Landroid/content/res/Resources;

    const v1, 0x7f0a007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ldls;->ah:I

    .line 146
    const v0, 0x7f04007d

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 148
    const v0, 0x7f0800c8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 150
    const v0, 0x7f0801fa

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;

    .line 151
    iget-object v1, p0, Ldls;->X:Lfkg;

    invoke-virtual {v1}, Lfkg;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v4, v0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:I

    if-eq v4, v1, :cond_0

    iput v1, v0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->requestLayout()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->invalidate()V

    .line 152
    :cond_0
    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a(I)V

    .line 154
    const v1, 0x7f0801f9

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    .line 155
    iget-object v1, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Landroid/support/v4/view/ViewPager;->b(I)V

    .line 156
    iget-object v1, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    new-instance v4, Ldlx;

    invoke-direct {v4, p0, v0}, Ldlx;-><init>(Ldls;Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;)V

    invoke-virtual {v1, v4}, Landroid/support/v4/view/ViewPager;->a(Lhe;)V

    .line 157
    new-instance v0, Ldlw;

    iget-object v1, p0, Ldls;->X:Lfkg;

    invoke-virtual {v1}, Lfkg;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ldlw;-><init>(Ldls;Ljava/util/List;)V

    iput-object v0, p0, Ldls;->ae:Ldlw;

    .line 158
    iget-object v0, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Ldls;->ae:Ldlw;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lfm;)V

    .line 159
    iget-object v0, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Ldls;->Z:I

    invoke-virtual {v0, v1, v5}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 161
    new-instance v0, Ldlt;

    invoke-direct {v0, p0}, Ldlt;-><init>(Ldls;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    const v0, 0x7f0801f8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 172
    new-instance v1, Ldlu;

    invoke-direct {v1, p0}, Ldlu;-><init>(Ldls;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    return-object v2
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0, p1}, Li;->a(Landroid/os/Bundle;)V

    .line 124
    const/4 v0, 0x2

    const v1, 0x7f0d0111

    invoke-virtual {p0, v0, v1}, Ldls;->a(II)V

    .line 125
    invoke-virtual {p0}, Ldls;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "videoId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldls;->W:Ljava/lang/String;

    .line 126
    invoke-virtual {p0}, Ldls;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "infoCardCollection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lfkg;

    iput-object v0, p0, Ldls;->X:Lfkg;

    .line 127
    invoke-virtual {p0}, Ldls;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selectedCardIndex"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldls;->Z:I

    .line 128
    invoke-virtual {p0}, Ldls;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "videoWasPlaying"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldls;->aa:Z

    .line 129
    invoke-virtual {p0}, Ldls;->j()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcwy;

    .line 130
    invoke-interface {v0}, Lcwy;->o()Lezg;

    move-result-object v0

    invoke-interface {v0}, Lezg;->aT()Lxk;

    move-result-object v0

    iput-object v0, p0, Ldls;->ab:Lxk;

    .line 131
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Ldls;->Y:Ldlv;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Ldls;->Y:Ldlv;

    invoke-interface {v0, p1}, Ldlv;->b(I)V

    .line 119
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 187
    invoke-super {p0}, Li;->e()V

    .line 188
    iget-object v0, p0, Ldls;->ac:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Ldls;->e(Z)V

    .line 189
    return-void

    .line 188
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 237
    invoke-super {p0, p1}, Li;->onCancel(Landroid/content/DialogInterface;)V

    .line 238
    iget-object v0, p0, Ldls;->Y:Ldlv;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Ldls;->Y:Ldlv;

    iget-object v1, p0, Ldls;->ad:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v1

    iget-boolean v2, p0, Ldls;->aa:Z

    invoke-interface {v0, v1, v2}, Ldlv;->b(IZ)V

    .line 241
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 193
    invoke-virtual {p0}, Ldls;->s()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 194
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Ldls;->e(Z)V

    .line 196
    :cond_0
    return-void

    .line 194
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
