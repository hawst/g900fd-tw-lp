.class public Lfkl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhkv;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field private final d:Lfqh;

.field private e:Ljava/util/List;


# direct methods
.method public constructor <init>(Lhkv;Lfqh;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkv;

    iput-object v0, p0, Lfkl;->a:Lhkv;

    .line 33
    iput-object p2, p0, Lfkl;->d:Lfqh;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 7

    .prologue
    .line 37
    iget-object v0, p0, Lfkl;->e:Ljava/util/List;

    if-nez v0, :cond_5

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfkl;->a:Lhkv;

    iget-object v1, v1, Lhkv;->a:[Lhkx;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfkl;->e:Ljava/util/List;

    .line 39
    iget-object v0, p0, Lfkl;->a:Lhkv;

    iget-object v1, v0, Lhkv;->a:[Lhkx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 40
    iget-object v4, v3, Lhkx;->d:Lhds;

    if-eqz v4, :cond_0

    .line 41
    iget-object v4, p0, Lfkl;->e:Ljava/util/List;

    new-instance v5, Lfja;

    iget-object v6, v3, Lhkx;->d:Lhds;

    invoke-direct {v5, v6, p0}, Lfja;-><init>(Lhds;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_0
    iget-object v4, v3, Lhkx;->c:Lhdv;

    if-eqz v4, :cond_1

    .line 44
    iget-object v4, p0, Lfkl;->e:Ljava/util/List;

    new-instance v5, Lfjc;

    iget-object v6, v3, Lhkx;->c:Lhdv;

    invoke-direct {v5, v6, p0}, Lfjc;-><init>(Lhdv;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    :cond_1
    iget-object v4, v3, Lhkx;->b:Lheb;

    if-eqz v4, :cond_2

    .line 47
    iget-object v4, p0, Lfkl;->e:Ljava/util/List;

    new-instance v5, Lfjf;

    iget-object v6, v3, Lhkx;->b:Lheb;

    invoke-direct {v5, v6, p0}, Lfjf;-><init>(Lheb;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_2
    iget-object v4, v3, Lhkx;->f:Lhdx;

    if-eqz v4, :cond_3

    .line 50
    iget-object v4, p0, Lfkl;->e:Ljava/util/List;

    new-instance v5, Lfjd;

    iget-object v6, v3, Lhkx;->f:Lhdx;

    invoke-direct {v5, v6, p0}, Lfjd;-><init>(Lhdx;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_3
    iget-object v4, v3, Lhkx;->e:Lhdz;

    if-eqz v4, :cond_4

    .line 53
    iget-object v4, p0, Lfkl;->e:Ljava/util/List;

    new-instance v5, Lfje;

    iget-object v3, v3, Lhkx;->e:Lhdz;

    invoke-direct {v5, v3, p0}, Lfje;-><init>(Lhdz;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_5
    iget-object v0, p0, Lfkl;->e:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 3

    .prologue
    .line 93
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 94
    invoke-virtual {p0}, Lfkl;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 95
    instance-of v2, v0, Lfat;

    if-eqz v2, :cond_0

    .line 96
    check-cast v0, Lfat;

    invoke-interface {v0, p1}, Lfat;->a(Lfau;)V

    goto :goto_0

    .line 99
    :cond_1
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lfkl;->a:Lhkv;

    iget-object v0, v0, Lhkv;->e:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfkl;->d:Lfqh;

    return-object v0
.end method
