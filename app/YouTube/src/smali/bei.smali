.class final Lbei;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Ljava/lang/String;

.field volatile b:Z

.field final synthetic c:Lbec;


# direct methods
.method public constructor <init>(Lbec;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lbei;->c:Lbec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    if-nez p2, :cond_0

    const-string p2, ""

    :cond_0
    iput-object p2, p0, Lbei;->a:Ljava/lang/String;

    .line 226
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 236
    :try_start_0
    iget-object v0, p0, Lbei;->c:Lbec;

    iget-object v0, v0, Lbec;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 237
    iget-object v1, p0, Lbei;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 238
    iget-object v1, p0, Lbei;->c:Lbec;

    .line 239
    invoke-static {v1}, Lbec;->g(Lbec;)Ldmj;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldmj;->a(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    .line 241
    iget-boolean v1, p0, Lbei;->b:Z

    if-eqz v1, :cond_0

    .line 258
    :goto_0
    return-void

    .line 245
    :cond_0
    iget-object v1, p0, Lbei;->c:Lbec;

    iget-object v1, v1, Lbec;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lbej;

    invoke-direct {v2, p0, v0}, Lbej;-><init>(Lbei;Ljava/util/Collection;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 254
    :catch_0
    move-exception v0

    .line 255
    const-string v1, "Error fetching search suggestions"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
