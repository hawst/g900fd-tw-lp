.class public Lorg/chromium/net/RemoteAndroidKeyStore;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/chromium/net/AndroidKeyStore;


# static fields
.field private static synthetic b:Z


# instance fields
.field private final a:Lorg/chromium/net/IRemoteAndroidKeyStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lorg/chromium/net/RemoteAndroidKeyStore;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/net/RemoteAndroidKeyStore;->b:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDSAKeyParamQ(Lorg/chromium/net/AndroidPrivateKey;)[B
    .locals 2

    .prologue
    .line 59
    check-cast p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;

    .line 61
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/RemoteAndroidKeyStore;->a:Lorg/chromium/net/IRemoteAndroidKeyStore;

    iget v1, p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;->a:I

    invoke-interface {v0, v1}, Lorg/chromium/net/IRemoteAndroidKeyStore;->c(I)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 65
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getECKeyOrder(Lorg/chromium/net/AndroidPrivateKey;)[B
    .locals 2

    .prologue
    .line 71
    check-cast p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;

    .line 73
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/RemoteAndroidKeyStore;->a:Lorg/chromium/net/IRemoteAndroidKeyStore;

    iget v1, p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;->a:I

    invoke-interface {v0, v1}, Lorg/chromium/net/IRemoteAndroidKeyStore;->d(I)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 77
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOpenSSLEngineForPrivateKey(Lorg/chromium/net/AndroidPrivateKey;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122
    sget-boolean v0, Lorg/chromium/net/RemoteAndroidKeyStore;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 123
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getOpenSSLHandleForPrivateKey(Lorg/chromium/net/AndroidPrivateKey;)J
    .locals 2

    .prologue
    .line 115
    sget-boolean v0, Lorg/chromium/net/RemoteAndroidKeyStore;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 116
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyEncodedBytes(Lorg/chromium/net/AndroidPrivateKey;)[B
    .locals 1

    .prologue
    .line 108
    sget-boolean v0, Lorg/chromium/net/RemoteAndroidKeyStore;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 109
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPrivateKeyType(Lorg/chromium/net/AndroidPrivateKey;)I
    .locals 2

    .prologue
    .line 95
    check-cast p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;

    .line 97
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/RemoteAndroidKeyStore;->a:Lorg/chromium/net/IRemoteAndroidKeyStore;

    iget v1, p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;->a:I

    invoke-interface {v0, v1}, Lorg/chromium/net/IRemoteAndroidKeyStore;->e(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 101
    :goto_0
    return v0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 101
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRSAKeyModulus(Lorg/chromium/net/AndroidPrivateKey;)[B
    .locals 2

    .prologue
    .line 47
    check-cast p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;

    .line 49
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/RemoteAndroidKeyStore;->a:Lorg/chromium/net/IRemoteAndroidKeyStore;

    iget v1, p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;->a:I

    invoke-interface {v0, v1}, Lorg/chromium/net/IRemoteAndroidKeyStore;->a(I)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 53
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public rawSignDigestWithPrivateKey(Lorg/chromium/net/AndroidPrivateKey;[B)[B
    .locals 2

    .prologue
    .line 83
    check-cast p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;

    .line 85
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/RemoteAndroidKeyStore;->a:Lorg/chromium/net/IRemoteAndroidKeyStore;

    iget v1, p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;->a:I

    invoke-interface {v0, v1, p2}, Lorg/chromium/net/IRemoteAndroidKeyStore;->a(I[B)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 89
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseKey(Lorg/chromium/net/AndroidPrivateKey;)V
    .locals 2

    .prologue
    .line 138
    check-cast p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;

    .line 140
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/RemoteAndroidKeyStore;->a:Lorg/chromium/net/IRemoteAndroidKeyStore;

    iget v1, p1, Lorg/chromium/net/RemoteAndroidKeyStore$RemotePrivateKey;->a:I

    invoke-interface {v0, v1}, Lorg/chromium/net/IRemoteAndroidKeyStore;->f(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_0
    return-void

    .line 142
    :catch_0
    move-exception v0

    .line 143
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
