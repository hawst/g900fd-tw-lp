.class public abstract Lorg/chromium/net/HttpUrlRequestFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lorg/chromium/net/HttpUrlRequestFactoryConfig;)Lorg/chromium/net/HttpUrlRequestFactory;
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x0

    .line 27
    iget-object v1, p1, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a:Lorg/json/JSONObject;

    const-string v2, "ENABLE_LEGACY_MODE"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 28
    invoke-static {p0, p1}, Lorg/chromium/net/HttpUrlRequestFactory;->b(Landroid/content/Context;Lorg/chromium/net/HttpUrlRequestFactoryConfig;)Lorg/chromium/net/HttpUrlRequestFactory;

    move-result-object v0

    .line 30
    :cond_0
    if-nez v0, :cond_1

    .line 32
    new-instance v0, Lorg/chromium/net/HttpUrlConnectionUrlRequestFactory;

    invoke-direct {v0, p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequestFactory;-><init>(Landroid/content/Context;)V

    .line 34
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Using network stack: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/chromium/net/HttpUrlRequestFactory;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    return-object v0
.end method

.method private static b(Landroid/content/Context;Lorg/chromium/net/HttpUrlRequestFactoryConfig;)Lorg/chromium/net/HttpUrlRequestFactory;
    .locals 5

    .prologue
    .line 78
    const/4 v1, 0x0

    .line 80
    :try_start_0
    const-class v0, Lorg/chromium/net/HttpUrlRequestFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v2, "org.chromium.net.ChromiumUrlRequestFactory"

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v2, Lorg/chromium/net/HttpUrlRequestFactory;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 84
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 87
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/net/HttpUrlRequestFactory;

    .line 89
    invoke-virtual {v0}, Lorg/chromium/net/HttpUrlRequestFactory;->a()Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    :goto_0
    return-object v0

    .line 99
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 94
    :catch_1
    move-exception v0

    .line 95
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot instantiate: org.chromium.net.ChromiumUrlRequestFactory"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;
.end method

.method public abstract a(Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;
.end method

.method public abstract a()Z
.end method

.method public abstract b()Ljava/lang/String;
.end method
