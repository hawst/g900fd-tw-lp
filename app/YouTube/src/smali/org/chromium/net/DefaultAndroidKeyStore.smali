.class public Lorg/chromium/net/DefaultAndroidKeyStore;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/chromium/net/AndroidKeyStore;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method private static a(Lorg/chromium/net/AndroidPrivateKey;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 146
    check-cast p0, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    iget-object v1, p0, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->a:Ljava/security/PrivateKey;

    .line 148
    if-nez v1, :cond_0

    .line 149
    const-string v1, "AndroidKeyStoreInProcessImpl"

    const-string v2, "key == null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :goto_0
    return-object v0

    .line 152
    :cond_0
    instance-of v2, v1, Ljava/security/interfaces/RSAPrivateKey;

    if-nez v2, :cond_1

    .line 153
    const-string v1, "AndroidKeyStoreInProcessImpl"

    const-string v2, "does not implement RSAPrivateKey"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 160
    :cond_1
    :try_start_0
    const-string v2, "org.apache.harmony.xnet.provider.jsse.OpenSSLRSAPrivateKey"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 169
    invoke-virtual {v2, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 173
    const-string v2, "AndroidKeyStoreInProcessImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Private key is not an OpenSSLRSAPrivateKey instance, its class name is:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 162
    :catch_0
    move-exception v1

    .line 166
    const-string v2, "AndroidKeyStoreInProcessImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot find system OpenSSLRSAPrivateKey class: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 183
    :cond_2
    :try_start_1
    const-string v3, "getOpenSSLKey"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 184
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 185
    const/4 v3, 0x0

    :try_start_2
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 189
    const/4 v3, 0x0

    :try_start_3
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 191
    if-nez v1, :cond_3

    .line 193
    const-string v1, "AndroidKeyStoreInProcessImpl"

    const-string v2, "getOpenSSLKey() returned null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 197
    :catch_1
    move-exception v1

    .line 198
    const-string v2, "AndroidKeyStoreInProcessImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception while trying to retrieve system EVP_PKEY handle: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 189
    :catchall_0
    move-exception v1

    const/4 v3, 0x0

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    move-object v0, v1

    .line 196
    goto/16 :goto_0
.end method


# virtual methods
.method public getDSAKeyParamQ(Lorg/chromium/net/AndroidPrivateKey;)[B
    .locals 2

    .prologue
    .line 66
    check-cast p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    iget-object v0, p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->a:Ljava/security/PrivateKey;

    .line 67
    instance-of v1, v0, Ljava/security/interfaces/DSAKey;

    if-eqz v1, :cond_0

    .line 68
    check-cast v0, Ljava/security/interfaces/DSAKey;

    invoke-interface {v0}, Ljava/security/interfaces/DSAKey;->getParams()Ljava/security/interfaces/DSAParams;

    move-result-object v0

    .line 69
    invoke-interface {v0}, Ljava/security/interfaces/DSAParams;->getQ()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    .line 71
    :cond_0
    const-string v0, "AndroidKeyStoreInProcessImpl"

    const-string v1, "Not a DSAKey instance!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getECKeyOrder(Lorg/chromium/net/AndroidPrivateKey;)[B
    .locals 2

    .prologue
    .line 77
    check-cast p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    iget-object v0, p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->a:Ljava/security/PrivateKey;

    .line 78
    instance-of v1, v0, Ljava/security/interfaces/ECKey;

    if-eqz v1, :cond_0

    .line 79
    check-cast v0, Ljava/security/interfaces/ECKey;

    invoke-interface {v0}, Ljava/security/interfaces/ECKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getOrder()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    .line 82
    :cond_0
    const-string v0, "AndroidKeyStoreInProcessImpl"

    const-string v1, "Not an ECKey instance!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOpenSSLEngineForPrivateKey(Lorg/chromium/net/AndroidPrivateKey;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 249
    :try_start_0
    const-string v1, "org.apache.harmony.xnet.provider.jsse.OpenSSLEngine"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 259
    invoke-static {p1}, Lorg/chromium/net/DefaultAndroidKeyStore;->a(Lorg/chromium/net/AndroidPrivateKey;)Ljava/lang/Object;

    move-result-object v1

    .line 260
    if-nez v1, :cond_0

    .line 295
    :goto_0
    return-object v0

    .line 251
    :catch_0
    move-exception v1

    .line 255
    const-string v2, "AndroidKeyStoreInProcessImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot find system OpenSSLEngine class: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 268
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getEngine"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    .line 274
    const/4 v4, 0x1

    :try_start_2
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 275
    const/4 v4, 0x0

    :try_start_3
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 279
    const/4 v4, 0x0

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 281
    if-nez v1, :cond_1

    .line 283
    const-string v3, "AndroidKeyStoreInProcessImpl"

    const-string v4, "getEngine() returned null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 287
    const-string v2, "AndroidKeyStoreInProcessImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Engine is not an OpenSSLEngine instance, its class name is:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 293
    :catch_1
    move-exception v1

    .line 294
    const-string v2, "AndroidKeyStoreInProcessImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception while trying to retrieve OpenSSLEngine object: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 269
    :catch_2
    move-exception v1

    .line 271
    :try_start_5
    const-string v2, "AndroidKeyStoreInProcessImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No getEngine() method on OpenSSLKey member:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 279
    :catchall_0
    move-exception v1

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    throw v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :cond_2
    move-object v0, v1

    .line 291
    goto/16 :goto_0
.end method

.method public getOpenSSLHandleForPrivateKey(Lorg/chromium/net/AndroidPrivateKey;)J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 205
    invoke-static {p1}, Lorg/chromium/net/DefaultAndroidKeyStore;->a(Lorg/chromium/net/AndroidPrivateKey;)Ljava/lang/Object;

    move-result-object v0

    .line 206
    if-nez v0, :cond_1

    move-wide v0, v2

    .line 240
    :cond_0
    :goto_0
    return-wide v0

    .line 219
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v4, "getPkeyContext"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 225
    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {v4, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 226
    const/4 v1, 0x0

    :try_start_2
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    .line 230
    const/4 v5, 0x0

    :try_start_3
    invoke-virtual {v4, v5}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 232
    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 234
    const-string v4, "AndroidKeyStoreInProcessImpl"

    const-string v5, "getPkeyContext() returned null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    .line 239
    const-string v1, "AndroidKeyStoreInProcessImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception while trying to retrieve system EVP_PKEY handle: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    .line 240
    goto :goto_0

    .line 220
    :catch_1
    move-exception v0

    .line 222
    :try_start_4
    const-string v1, "AndroidKeyStoreInProcessImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No getPkeyContext() method on OpenSSLKey member:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    .line 223
    goto :goto_0

    .line 230
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
.end method

.method public getPrivateKeyEncodedBytes(Lorg/chromium/net/AndroidPrivateKey;)[B
    .locals 1

    .prologue
    .line 88
    check-cast p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    iget-object v0, p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->a:Ljava/security/PrivateKey;

    .line 89
    invoke-interface {v0}, Ljava/security/PrivateKey;->getEncoded()[B

    move-result-object v0

    return-object v0
.end method

.method public getPrivateKeyType(Lorg/chromium/net/AndroidPrivateKey;)I
    .locals 2

    .prologue
    .line 134
    check-cast p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    iget-object v0, p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->a:Ljava/security/PrivateKey;

    .line 135
    instance-of v1, v0, Ljava/security/interfaces/RSAPrivateKey;

    if-eqz v1, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 142
    :goto_0
    return v0

    .line 137
    :cond_0
    instance-of v1, v0, Ljava/security/interfaces/DSAPrivateKey;

    if-eqz v1, :cond_1

    .line 138
    const/4 v0, 0x1

    goto :goto_0

    .line 139
    :cond_1
    instance-of v0, v0, Ljava/security/interfaces/ECPrivateKey;

    if-eqz v0, :cond_2

    .line 140
    const/4 v0, 0x2

    goto :goto_0

    .line 142
    :cond_2
    const/16 v0, 0xff

    goto :goto_0
.end method

.method public getRSAKeyModulus(Lorg/chromium/net/AndroidPrivateKey;)[B
    .locals 2

    .prologue
    .line 56
    check-cast p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    iget-object v0, p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->a:Ljava/security/PrivateKey;

    .line 57
    instance-of v1, v0, Ljava/security/interfaces/RSAKey;

    if-eqz v1, :cond_0

    .line 58
    check-cast v0, Ljava/security/interfaces/RSAKey;

    invoke-interface {v0}, Ljava/security/interfaces/RSAKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    .line 60
    :cond_0
    const-string v0, "AndroidKeyStoreInProcessImpl"

    const-string v1, "Not a RSAKey instance!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public rawSignDigestWithPrivateKey(Lorg/chromium/net/AndroidPrivateKey;[B)[B
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 95
    check-cast p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;

    iget-object v2, p1, Lorg/chromium/net/DefaultAndroidKeyStore$DefaultAndroidPrivateKey;->a:Ljava/security/PrivateKey;

    .line 101
    :try_start_0
    instance-of v0, v2, Ljava/security/interfaces/RSAPrivateKey;

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "NONEwithRSA"

    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 115
    :goto_0
    if-nez v0, :cond_3

    .line 116
    const-string v0, "AndroidKeyStoreInProcessImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported private key algorithm: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :goto_1
    return-object v1

    .line 106
    :cond_0
    :try_start_1
    instance-of v0, v2, Ljava/security/interfaces/DSAPrivateKey;

    if-eqz v0, :cond_1

    .line 107
    const-string v0, "NONEwithDSA"

    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    goto :goto_0

    .line 108
    :cond_1
    instance-of v0, v2, Ljava/security/interfaces/ECPrivateKey;

    if-eqz v0, :cond_2

    .line 109
    const-string v0, "NONEwithECDSA"

    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 122
    :cond_3
    :try_start_2
    invoke-virtual {v0, v2}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    .line 123
    invoke-virtual {v0, p2}, Ljava/security/Signature;->update([B)V

    .line 124
    invoke-virtual {v0}, Ljava/security/Signature;->sign()[B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    goto :goto_1

    .line 125
    :catch_1
    move-exception v0

    .line 126
    const-string v3, "AndroidKeyStoreInProcessImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception while signing message with "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " private key: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public releaseKey(Lorg/chromium/net/AndroidPrivateKey;)V
    .locals 0

    .prologue
    .line 302
    return-void
.end method
