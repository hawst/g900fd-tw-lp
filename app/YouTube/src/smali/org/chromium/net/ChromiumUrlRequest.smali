.class public Lorg/chromium/net/ChromiumUrlRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/chromium/net/HttpUrlRequest;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# instance fields
.field private a:J

.field private final b:Lorg/chromium/net/ChromiumUrlRequestContext;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/util/Map;

.field private final f:Ljava/nio/channels/WritableByteChannel;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:[B

.field private j:Ljava/nio/channels/ReadableByteChannel;

.field private k:Z

.field private volatile l:Z

.field private volatile m:Z

.field private volatile n:Z

.field private volatile o:Z

.field private p:Z

.field private q:Ljava/lang/String;

.field private final r:Lorg/chromium/net/HttpUrlRequestListener;

.field private s:Z

.field private t:J

.field private u:Z

.field private v:Z

.field private w:J

.field private final x:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lorg/chromium/net/ChromiumUrlRequestContext;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V
    .locals 4

    .prologue
    const/4 v0, 0x3

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:Ljava/lang/Object;

    .line 86
    if-nez p1, :cond_0

    .line 87
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    if-nez p2, :cond_1

    .line 90
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "URL is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_1
    iput-object p1, p0, Lorg/chromium/net/ChromiumUrlRequest;->b:Lorg/chromium/net/ChromiumUrlRequestContext;

    .line 93
    iput-object p2, p0, Lorg/chromium/net/ChromiumUrlRequest;->c:Ljava/lang/String;

    .line 94
    packed-switch p3, :pswitch_data_0

    :goto_0
    :pswitch_0
    iput v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->d:I

    .line 95
    iput-object p4, p0, Lorg/chromium/net/ChromiumUrlRequest;->e:Ljava/util/Map;

    .line 96
    iput-object p5, p0, Lorg/chromium/net/ChromiumUrlRequest;->f:Ljava/nio/channels/WritableByteChannel;

    .line 97
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->b:Lorg/chromium/net/ChromiumUrlRequestContext;

    iget-wide v0, v0, Lorg/chromium/net/ChromiumUrlRequestContext;->a:J

    iget-object v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->c:Ljava/lang/String;

    iget v3, p0, Lorg/chromium/net/ChromiumUrlRequest;->d:I

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/chromium/net/ChromiumUrlRequest;->nativeCreateRequestAdapter(JLjava/lang/String;I)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    .line 101
    iput-object p6, p0, Lorg/chromium/net/ChromiumUrlRequest;->r:Lorg/chromium/net/HttpUrlRequestListener;

    .line 102
    return-void

    .line 94
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public constructor <init>(Lorg/chromium/net/ChromiumUrlRequestContext;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)V
    .locals 7

    .prologue
    .line 68
    new-instance v5, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-direct {v5}, Lorg/chromium/net/ChunkedWritableByteChannel;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/chromium/net/ChromiumUrlRequest;-><init>(Lorg/chromium/net/ChromiumUrlRequestContext;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->s:Z

    .line 71
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 481
    new-instance v0, Ljava/io/IOException;

    const-string v1, "CalledByNative method has thrown an exception"

    invoke-direct {v0, v1, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 483
    const-string v0, "ChromiumNetwork"

    const-string v1, "Exception in CalledByNative method"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 486
    :try_start_0
    invoke-virtual {p0}, Lorg/chromium/net/ChromiumUrlRequest;->e()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 491
    :goto_0
    return-void

    .line 487
    :catch_0
    move-exception v0

    .line 488
    const-string v1, "ChromiumNetwork"

    const-string v2, "Exception trying to cancel request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private finish()V
    .locals 4
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 585
    :try_start_0
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 586
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->o:Z

    .line 588
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->n:Z

    if-eqz v0, :cond_0

    .line 589
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611
    :goto_0
    return-void

    .line 592
    :cond_0
    :try_start_2
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->f:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 600
    :goto_1
    :try_start_3
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->r:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->b(Lorg/chromium/net/HttpUrlRequest;)V

    .line 604
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/net/ChromiumUrlRequest;->nativeDestroyRequestAdapter(J)V

    .line 605
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    .line 606
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->n:Z

    .line 607
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 610
    :catch_0
    move-exception v0

    .line 609
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Exception in finish"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 597
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private h()V
    .locals 2

    .prologue
    .line 451
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->n:Z

    if-eqz v0, :cond_0

    .line 452
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Accessing recycled request"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 457
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->l:Z

    if-eqz v0, :cond_0

    .line 458
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Request already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 460
    :cond_0
    return-void
.end method

.method private native nativeAddHeader(JLjava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeAppendChunk(JLjava/nio/ByteBuffer;IZ)V
.end method

.method private native nativeCancel(J)V
.end method

.method private native nativeCreateRequestAdapter(JLjava/lang/String;I)J
.end method

.method private native nativeDestroyRequestAdapter(J)V
.end method

.method private native nativeEnableChunkedUpload(JLjava/lang/String;)V
.end method

.method private native nativeGetAllHeaders(JLorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;)V
.end method

.method private native nativeGetContentLength(J)J
.end method

.method private native nativeGetContentType(J)Ljava/lang/String;
.end method

.method private native nativeGetErrorCode(J)I
.end method

.method private native nativeGetErrorString(J)Ljava/lang/String;
.end method

.method private native nativeGetHeader(JLjava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeGetHttpStatusCode(J)I
.end method

.method private native nativeGetNegotiatedProtocol(J)Ljava/lang/String;
.end method

.method private native nativeSetMethod(JLjava/lang/String;)V
.end method

.method private native nativeSetUploadChannel(JLjava/lang/String;J)V
.end method

.method private native nativeSetUploadData(JLjava/lang/String;[B)V
.end method

.method private native nativeStart(J)V
.end method

.method private onAppendResponseHeader(Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 621
    :try_start_0
    invoke-virtual {p1, p2}, Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 622
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1, p2, v0}, Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    :cond_0
    invoke-virtual {p1, p2}, Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 628
    :goto_0
    return-void

    .line 625
    :catch_0
    move-exception v0

    .line 626
    invoke-direct {p0, v0}, Lorg/chromium/net/ChromiumUrlRequest;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private onBytesRead(Ljava/nio/ByteBuffer;)V
    .locals 10
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v8, 0x0

    .line 546
    :try_start_0
    iget-boolean v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->u:Z

    if-eqz v2, :cond_1

    .line 576
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    .line 551
    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    int-to-long v6, v2

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    .line 552
    iget-boolean v3, p0, Lorg/chromium/net/ChromiumUrlRequest;->v:Z

    if-eqz v3, :cond_2

    .line 553
    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    cmp-long v3, v4, v8

    if-lez v3, :cond_0

    .line 556
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/chromium/net/ChromiumUrlRequest;->v:Z

    .line 557
    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    int-to-long v6, v2

    sub-long/2addr v4, v6

    sub-long v4, v8, v4

    long-to-int v3, v4

    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 561
    :cond_2
    cmp-long v3, v8, v8

    if-eqz v3, :cond_4

    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    cmp-long v3, v4, v8

    if-lez v3, :cond_4

    .line 563
    :goto_1
    if-eqz v0, :cond_3

    .line 564
    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J

    long-to-int v1, v4

    sub-int v1, v2, v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 567
    :cond_3
    :goto_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 568
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->f:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v1, p1}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 573
    :catch_0
    move-exception v0

    .line 574
    invoke-direct {p0, v0}, Lorg/chromium/net/ChromiumUrlRequest;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 561
    goto :goto_1

    .line 570
    :cond_5
    if-eqz v0, :cond_0

    .line 571
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->u:Z

    invoke-virtual {p0}, Lorg/chromium/net/ChromiumUrlRequest;->e()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private onResponseStarted()V
    .locals 8
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    const-wide/16 v4, 0x0

    .line 499
    :try_start_0
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetContentType(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->q:Ljava/lang/String;

    .line 500
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetContentLength(J)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:J

    .line 501
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->p:Z

    .line 503
    cmp-long v0, v4, v4

    if-lez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:J

    .line 506
    :cond_0
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->s:Z

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->u:Z

    if-nez v0, :cond_3

    .line 512
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->f:Ljava/nio/channels/WritableByteChannel;

    check-cast v0, Lorg/chromium/net/ChunkedWritableByteChannel;

    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:J

    long-to-int v1, v2

    iget-object v2, v0, Lorg/chromium/net/ChunkedWritableByteChannel;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lorg/chromium/net/ChunkedWritableByteChannel;->b:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 531
    :catch_0
    move-exception v0

    .line 532
    invoke-direct {p0, v0}, Lorg/chromium/net/ChromiumUrlRequest;->a(Ljava/lang/Exception;)V

    .line 534
    :goto_0
    return-void

    .line 512
    :cond_2
    :try_start_1
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, v0, Lorg/chromium/net/ChunkedWritableByteChannel;->b:Ljava/nio/ByteBuffer;

    .line 516
    :cond_3
    cmp-long v0, v4, v4

    if-eqz v0, :cond_5

    .line 520
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetHttpStatusCode(J)I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_6

    .line 522
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_4

    .line 523
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:J

    iput-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:J

    .line 525
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->v:Z

    .line 530
    :cond_5
    :goto_1
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->r:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->a(Lorg/chromium/net/HttpUrlRequest;)V

    goto :goto_0

    .line 527
    :cond_6
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->w:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private readFromUploadChannel(Ljava/nio/ByteBuffer;)I
    .locals 1
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 640
    const/4 v0, -0x1

    return v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->t:J

    return-wide v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 302
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->i()V

    .line 303
    iput-object p1, p0, Lorg/chromium/net/ChromiumUrlRequest;->h:Ljava/lang/String;

    .line 304
    return-void
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 216
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:Ljava/lang/Object;

    monitor-enter v1

    .line 217
    :try_start_0
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->i()V

    .line 218
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "contentType is required"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 219
    :cond_0
    :try_start_1
    iput-object p1, p0, Lorg/chromium/net/ChromiumUrlRequest;->g:Ljava/lang/String;

    .line 220
    iput-object p2, p0, Lorg/chromium/net/ChromiumUrlRequest;->i:[B

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->j:Ljava/nio/channels/ReadableByteChannel;

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->k:Z

    .line 223
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 132
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetHttpStatusCode(J)I

    move-result v0

    .line 138
    const/16 v1, 0xce

    if-ne v0, v1, :cond_0

    .line 139
    const/16 v0, 0xc8

    .line 141
    :cond_0
    return v0
.end method

.method public final c()[B
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->f:Ljava/nio/channels/WritableByteChannel;

    check-cast v0, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-virtual {v0}, Lorg/chromium/net/ChunkedWritableByteChannel;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 312
    iget-object v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:Ljava/lang/Object;

    monitor-enter v2

    .line 313
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->m:Z

    if-eqz v0, :cond_0

    .line 314
    monitor-exit v2

    .line 356
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->i()V

    .line 318
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->h()V

    .line 320
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->l:Z

    .line 322
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->e:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 323
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 324
    iget-wide v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v4, v5, v1, v0}, Lorg/chromium/net/ChromiumUrlRequest;->nativeAddHeader(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 356
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 329
    :cond_1
    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->i:[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->i:[B

    array-length v0, v0

    if-lez v0, :cond_2

    .line 338
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    iget-object v3, p0, Lorg/chromium/net/ChromiumUrlRequest;->g:Ljava/lang/String;

    iget-object v4, p0, Lorg/chromium/net/ChromiumUrlRequest;->i:[B

    invoke-direct {p0, v0, v1, v3, v4}, Lorg/chromium/net/ChromiumUrlRequest;->nativeSetUploadData(JLjava/lang/String;[B)V

    .line 340
    :cond_2
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 352
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    iget-object v3, p0, Lorg/chromium/net/ChromiumUrlRequest;->h:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lorg/chromium/net/ChromiumUrlRequest;->nativeSetMethod(JLjava/lang/String;)V

    .line 355
    :cond_3
    iget-wide v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/ChromiumUrlRequest;->nativeStart(J)V

    .line 356
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 361
    iget-object v1, p0, Lorg/chromium/net/ChromiumUrlRequest;->x:Ljava/lang/Object;

    monitor-enter v1

    .line 362
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->m:Z

    if-eqz v0, :cond_0

    .line 363
    monitor-exit v1

    .line 371
    :goto_0
    return-void

    .line 366
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->m:Z

    .line 368
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->n:Z

    if-nez v0, :cond_1

    .line 369
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/net/ChromiumUrlRequest;->nativeCancel(J)V

    .line 371
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/util/Map;
    .locals 4

    .prologue
    .line 409
    invoke-direct {p0}, Lorg/chromium/net/ChromiumUrlRequest;->h()V

    .line 410
    iget-boolean v0, p0, Lorg/chromium/net/ChromiumUrlRequest;->p:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Response headers not available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 411
    :cond_0
    new-instance v0, Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;

    invoke-direct {v0, p0}, Lorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;-><init>(Lorg/chromium/net/ChromiumUrlRequest;)V

    .line 412
    iget-wide v2, p0, Lorg/chromium/net/ChromiumUrlRequest;->a:J

    invoke-direct {p0, v2, v3, v0}, Lorg/chromium/net/ChromiumUrlRequest;->nativeGetAllHeaders(JLorg/chromium/net/ChromiumUrlRequest$ResponseHeadersMap;)V

    .line 413
    return-object v0
.end method
