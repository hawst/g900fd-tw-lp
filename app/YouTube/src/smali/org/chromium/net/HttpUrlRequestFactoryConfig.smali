.class public Lorg/chromium/net/HttpUrlRequestFactoryConfig;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/32 v2, 0x19000

    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a:Lorg/json/JSONObject;

    .line 21
    const-string v0, "ENABLE_LEGACY_MODE"

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    .line 22
    const-string v0, "ENABLE_QUIC"

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    .line 23
    const/4 v0, 0x1

    const-string v1, "ENABLE_SPDY"

    invoke-direct {p0, v1, v0}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    .line 24
    sget-object v0, Lorg/chromium/net/HttpUrlRequestFactoryConfig$HttpCache;->b:Lorg/chromium/net/HttpUrlRequestFactoryConfig$HttpCache;

    sget-object v1, Lorg/chromium/net/HttpUrlRequestFactoryConfig$1;->a:[I

    invoke-virtual {v0}, Lorg/chromium/net/HttpUrlRequestFactoryConfig$HttpCache;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 25
    :goto_0
    return-void

    .line 24
    :pswitch_0
    const-string v0, "HTTP_CACHE"

    const-string v1, "HTTP_CACHE_DISABLED"

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    goto :goto_0

    :pswitch_1
    const-string v0, "HTTP_CACHE_MAX_SIZE"

    invoke-direct {p0, v0, v2, v3}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;J)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    const-string v0, "HTTP_CACHE"

    const-string v1, "HTTP_CACHE_DISK"

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    goto :goto_0

    :pswitch_2
    const-string v0, "HTTP_CACHE_MAX_SIZE"

    invoke-direct {p0, v0, v2, v3}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;J)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    const-string v0, "HTTP_CACHE"

    const-string v1, "HTTP_CACHE_MEMORY"

    invoke-direct {p0, v0, v1}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;J)Lorg/chromium/net/HttpUrlRequestFactoryConfig;
    .locals 2

    .prologue
    .line 164
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/net/HttpUrlRequestFactoryConfig;
    .locals 1

    .prologue
    .line 177
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;
    .locals 1

    .prologue
    .line 151
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;II)Lorg/chromium/net/HttpUrlRequestFactoryConfig;
    .locals 3

    .prologue
    .line 114
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal QUIC Hint Host: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a:Lorg/json/JSONObject;

    const-string v1, "QUIC_HINTS"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 121
    if-nez v0, :cond_1

    .line 122
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 123
    iget-object v1, p0, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a:Lorg/json/JSONObject;

    const-string v2, "QUIC_HINTS"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 126
    :cond_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 127
    const-string v2, "QUIC_HINT_HOST"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 128
    const-string v2, "QUIC_HINT_PORT"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 129
    const-string v2, "QUIC_HINT_ALT_PORT"

    invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 130
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;
    .locals 1

    .prologue
    .line 57
    const-string v0, "ENABLE_QUIC"

    invoke-direct {p0, v0, p1}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;
    .locals 1

    .prologue
    .line 64
    const-string v0, "ENABLE_SPDY"

    invoke-direct {p0, v0, p1}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
