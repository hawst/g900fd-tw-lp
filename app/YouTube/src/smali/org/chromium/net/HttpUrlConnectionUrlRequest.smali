.class Lorg/chromium/net/HttpUrlConnectionUrlRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/chromium/net/HttpUrlRequest;


# static fields
.field private static t:Ljava/util/concurrent/ExecutorService;

.field private static final u:Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/nio/channels/WritableByteChannel;

.field private final e:Lorg/chromium/net/HttpUrlRequestListener;

.field private f:Ljava/io/IOException;

.field private g:Ljava/net/HttpURLConnection;

.field private h:I

.field private i:Z

.field private j:J

.field private k:Ljava/lang/String;

.field private l:[B

.field private m:Ljava/nio/channels/ReadableByteChannel;

.field private n:Ljava/lang/String;

.field private o:I

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Ljava/io/InputStream;

.field private final s:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->u:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)V
    .locals 6

    .prologue
    .line 99
    new-instance v4, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-direct {v4}, Lorg/chromium/net/ChunkedWritableByteChannel;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V

    .line 101
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    if-nez p1, :cond_0

    .line 107
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    if-nez p2, :cond_1

    .line 110
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "URL is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_1
    iput-object p1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->a:Landroid/content/Context;

    .line 113
    iput-object p2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->b:Ljava/lang/String;

    .line 114
    iput-object p3, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->c:Ljava/util/Map;

    .line 115
    iput-object p4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    .line 116
    iput-object p5, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    .line 117
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->s:Ljava/lang/Object;

    .line 118
    return-void
.end method

.method static synthetic a(Lorg/chromium/net/HttpUrlConnectionUrlRequest;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, -0x1

    const/4 v3, 0x1

    const-wide/16 v8, 0x0

    .line 34
    :try_start_0
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->s:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->p:Z

    if-eqz v0, :cond_1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->b(Lorg/chromium/net/HttpUrlRequest;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->q:Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v0, :cond_2

    :try_start_4
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/net/ProtocolException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_2
    :try_start_5
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const v1, 0x15f90

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->c:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v5, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    move v1, v2

    :goto_2
    :try_start_6
    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    if-nez v1, :cond_0

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->b(Lorg/chromium/net/HttpUrlRequest;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception v0

    :goto_3
    if-nez v2, :cond_3

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v1, p0}, Lorg/chromium/net/HttpUrlRequestListener;->b(Lorg/chromium/net/HttpUrlRequest;)V

    :cond_3
    throw v0

    :catch_1
    move-exception v0

    :try_start_9
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_4
    cmp-long v0, v8, v8

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const-string v1, "Range"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "bytes="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const-string v1, "User-Agent"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const-string v1, "User-Agent"

    iget-object v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->a:Landroid/content/Context;

    invoke-static {v4}, Lorg/chromium/net/UserAgent;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->l:[B

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_7
    const/4 v0, 0x0

    :try_start_a
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-object v0

    :goto_4
    :try_start_b
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    iput v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:I

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->n:Ljava/lang/String;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v1

    iput v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:I

    cmp-long v1, v8, v8

    if-lez v1, :cond_8

    iget v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:I

    :cond_8
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v1, p0}, Lorg/chromium/net/HttpUrlRequestListener;->a(Lorg/chromium/net/HttpUrlRequest;)V

    iget v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:I

    div-int/lit8 v1, v1, 0x64

    const/4 v4, 0x2

    if-eq v1, v4, :cond_d

    move v1, v3

    :goto_5
    if-eqz v1, :cond_9

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    :cond_9
    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/io/InputStream;

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/io/InputStream;

    if-eqz v0, :cond_a

    const-string v0, "gzip"

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/io/InputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/io/InputStream;

    const/4 v0, -0x1

    iput v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:I

    :cond_a
    cmp-long v0, v8, v8

    if-eqz v0, :cond_c

    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_e

    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:I

    if-eq v0, v10, :cond_b

    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:I

    int-to-long v0, v0

    long-to-int v0, v0

    iput v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:I

    :cond_b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:Z

    :cond_c
    :goto_6
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/io/InputStream;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    if-eqz v0, :cond_f

    :try_start_c
    invoke-static {}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lorg/chromium/net/HttpUrlConnectionUrlRequest$3;

    invoke-direct {v1, p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest$3;-><init>(Lorg/chromium/net/HttpUrlConnectionUrlRequest;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :goto_7
    if-nez v3, :cond_0

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->b(Lorg/chromium/net/HttpUrlRequest;)V

    goto/16 :goto_0

    :cond_d
    move v1, v2

    goto :goto_5

    :cond_e
    const-wide/16 v0, 0x0

    :try_start_d
    iput-wide v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j:J
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto :goto_6

    :catchall_2
    move-exception v0

    move v2, v3

    goto/16 :goto_3

    :catchall_3
    move-exception v0

    move v2, v1

    goto/16 :goto_3

    :catch_2
    move-exception v0

    move v1, v3

    goto/16 :goto_2

    :catch_3
    move-exception v1

    goto/16 :goto_4

    :cond_f
    move v3, v2

    goto :goto_7
.end method

.method static synthetic b(Lorg/chromium/net/HttpUrlConnectionUrlRequest;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 34
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/io/InputStream;

    if-eqz v0, :cond_2

    const/16 v0, 0x2000

    new-array v3, v0, [B

    :cond_0
    :goto_0
    invoke-direct {p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->r:Ljava/io/InputStream;

    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j:J

    iget-boolean v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:Z

    if-eqz v1, :cond_6

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j:J

    cmp-long v1, v4, v8

    if-lez v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->i:Z

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j:J

    int-to-long v6, v0

    sub-long/2addr v4, v6

    sub-long v4, v8, v4

    long-to-int v1, v4

    sub-int/2addr v0, v1

    :goto_1
    cmp-long v4, v8, v8

    if-eqz v4, :cond_4

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j:J

    cmp-long v4, v4, v8

    if-lez v4, :cond_4

    iget-wide v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->j:J

    long-to-int v2, v4

    sub-int/2addr v0, v2

    if-lez v0, :cond_1

    iget-object v2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-static {v3, v1, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    :cond_1
    invoke-virtual {p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    :goto_3
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->e:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-interface {v0, p0}, Lorg/chromium/net/HttpUrlRequestListener;->b(Lorg/chromium/net/HttpUrlRequest;)V

    return-void

    :cond_4
    :try_start_3
    iget-object v4, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-static {v3, v1, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_5
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_4
    :try_start_6
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    if-nez v1, :cond_3

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    goto :goto_3

    :catch_2
    move-exception v0

    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    if-nez v1, :cond_3

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_7
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_7
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_4

    :goto_5
    :try_start_8
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v1}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_5
    :goto_6
    throw v0

    :catch_3
    move-exception v1

    iget-object v2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    if-nez v2, :cond_5

    iput-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->f:Ljava/io/IOException;

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v0

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_1
.end method

.method private static h()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .prologue
    .line 121
    sget-object v1, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->u:Ljava/lang/Object;

    monitor-enter v1

    .line 122
    :try_start_0
    sget-object v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->t:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest$1;

    invoke-direct {v0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest$1;-><init>()V

    .line 137
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->t:Ljava/util/concurrent/ExecutorService;

    .line 139
    :cond_0
    sget-object v0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->t:Ljava/util/concurrent/ExecutorService;

    monitor-exit v1

    return-object v0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 309
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 310
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const-string v2, "Content-Type"

    iget-object v3, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->k:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->l:[B

    if-eqz v0, :cond_3

    .line 317
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    iget-object v2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->l:[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 318
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 319
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->l:[B

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    :cond_1
    if-eqz v1, :cond_2

    .line 333
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 336
    :cond_2
    return-void

    .line 321
    :cond_3
    :try_start_1
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 322
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 323
    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 324
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 325
    :goto_0
    const/4 v3, 0x0

    invoke-interface {v3, v2}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v3

    if-lez v3, :cond_1

    .line 326
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 327
    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    invoke-virtual {v1, v0, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 328
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 332
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 333
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_4
    throw v0
.end method

.method private j()Z
    .locals 2

    .prologue
    .line 415
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->s:Ljava/lang/Object;

    monitor-enter v1

    .line 416
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->p:Z

    monitor-exit v1

    return v0

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 472
    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184
    .line 185
    iput-object p1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->q:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 161
    .line 162
    iput-object p1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->k:Ljava/lang/String;

    .line 163
    iput-object p2, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->l:[B

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->m:Ljava/nio/channels/ReadableByteChannel;

    .line 165
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 427
    iget v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->o:I

    .line 434
    const/16 v1, 0xce

    if-ne v0, v1, :cond_0

    .line 435
    const/16 v0, 0xc8

    .line 437
    :cond_0
    return v0
.end method

.method public final c()[B
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->d:Ljava/nio/channels/WritableByteChannel;

    check-cast v0, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-virtual {v0}, Lorg/chromium/net/ChunkedWritableByteChannel;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 190
    invoke-static {}, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->h()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lorg/chromium/net/HttpUrlConnectionUrlRequest$2;

    invoke-direct {v1, p0}, Lorg/chromium/net/HttpUrlConnectionUrlRequest$2;-><init>(Lorg/chromium/net/HttpUrlConnectionUrlRequest;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 196
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 404
    iget-object v1, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->s:Ljava/lang/Object;

    monitor-enter v1

    .line 405
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->p:Z

    if-eqz v0, :cond_0

    .line 406
    monitor-exit v1

    .line 410
    :goto_0
    return-void

    .line 409
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->p:Z

    .line 410
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/util/Map;
    .locals 2

    .prologue
    .line 497
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    if-nez v0, :cond_0

    .line 498
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Response headers not available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_0
    iget-object v0, p0, Lorg/chromium/net/HttpUrlConnectionUrlRequest;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
