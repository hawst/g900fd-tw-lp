.class Lorg/chromium/base/SystemMessageHandler;
.super Landroid/os/Handler;
.source "SourceFile"


# static fields
.field private static synthetic h:Z


# instance fields
.field private a:J

.field private b:J

.field private c:Landroid/os/MessageQueue;

.field private d:Ljava/lang/reflect/Field;

.field private e:Ljava/lang/reflect/Field;

.field private f:Z

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lorg/chromium/base/SystemMessageHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/base/SystemMessageHandler;->h:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(J)V
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 37
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 25
    iput-wide v0, p0, Lorg/chromium/base/SystemMessageHandler;->a:J

    .line 26
    iput-wide v0, p0, Lorg/chromium/base/SystemMessageHandler;->b:J

    .line 38
    iput-wide p1, p0, Lorg/chromium/base/SystemMessageHandler;->a:J

    .line 39
    sget-boolean v0, Lorg/chromium/base/SystemMessageHandler;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/base/SystemMessageHandler;->c:Landroid/os/MessageQueue;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    const-class v0, Landroid/os/Looper;

    const-string v1, "getQueue"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {p0}, Lorg/chromium/base/SystemMessageHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/MessageQueue;

    iput-object v0, p0, Lorg/chromium/base/SystemMessageHandler;->c:Landroid/os/MessageQueue;

    iget-object v0, p0, Lorg/chromium/base/SystemMessageHandler;->c:Landroid/os/MessageQueue;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "mMessages"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/base/SystemMessageHandler;->d:Ljava/lang/reflect/Field;

    iget-object v0, p0, Lorg/chromium/base/SystemMessageHandler;->d:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const-class v0, Landroid/os/Message;

    const-string v1, "target"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/base/SystemMessageHandler;->e:Ljava/lang/reflect/Field;

    iget-object v0, p0, Lorg/chromium/base/SystemMessageHandler;->e:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/chromium/base/SystemMessageHandler;->g:J
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "SystemMessageHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load method: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->b()V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v1, "SystemMessageHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load field: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->b()V

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_3
    const-string v1, "SystemMessageHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed invocation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->b()V

    goto :goto_0

    :catch_3
    move-exception v0

    :try_start_4
    const-string v1, "SystemMessageHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal access to reflected invocation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->b()V

    goto :goto_0

    :catch_4
    move-exception v0

    :try_start_5
    const-string v1, "SystemMessageHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal argument to reflected invocation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->b()V

    goto :goto_0

    :catch_5
    move-exception v0

    :try_start_6
    const-string v1, "SystemMessageHandler"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->b()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->b()V

    throw v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 151
    :try_start_0
    invoke-virtual {p2, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    const-string v1, "SystemMessageHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed field access: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->b()V

    .line 156
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 80
    iget-object v0, p0, Lorg/chromium/base/SystemMessageHandler;->c:Landroid/os/MessageQueue;

    if-nez v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-static {}, Lorg/chromium/base/TraceEvent;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 84
    iput-boolean v1, p0, Lorg/chromium/base/SystemMessageHandler;->f:Z

    goto :goto_0

    .line 87
    :cond_1
    iget-object v0, p0, Lorg/chromium/base/SystemMessageHandler;->c:Landroid/os/MessageQueue;

    iget-object v2, p0, Lorg/chromium/base/SystemMessageHandler;->d:Ljava/lang/reflect/Field;

    invoke-direct {p0, v0, v2}, Lorg/chromium/base/SystemMessageHandler;->a(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 88
    if-eqz v0, :cond_2

    iget-object v2, p0, Lorg/chromium/base/SystemMessageHandler;->e:Ljava/lang/reflect/Field;

    invoke-direct {p0, v0, v2}, Lorg/chromium/base/SystemMessageHandler;->a(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lorg/chromium/base/SystemMessageHandler;->a(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 140
    iget-boolean v0, p0, Lorg/chromium/base/SystemMessageHandler;->f:Z

    if-ne p1, v0, :cond_0

    .line 147
    :goto_0
    return-void

    .line 141
    :cond_0
    iput-boolean p1, p0, Lorg/chromium/base/SystemMessageHandler;->f:Z

    .line 142
    iget-boolean v0, p0, Lorg/chromium/base/SystemMessageHandler;->f:Z

    if-eqz v0, :cond_1

    .line 143
    const-string v0, "SyncBarrier"

    iget-wide v2, p0, Lorg/chromium/base/SystemMessageHandler;->g:J

    invoke-static {v0, v2, v3}, Lorg/chromium/base/TraceEvent;->a(Ljava/lang/String;J)V

    goto :goto_0

    .line 145
    :cond_1
    const-string v0, "SyncBarrier"

    iget-wide v2, p0, Lorg/chromium/base/SystemMessageHandler;->g:J

    invoke-static {v0, v2, v3}, Lorg/chromium/base/TraceEvent;->b(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 132
    const-string v0, "SystemMessageHandler"

    const-string v1, "Unexpected error with sync barrier detection, disabling."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iput-object v2, p0, Lorg/chromium/base/SystemMessageHandler;->c:Landroid/os/MessageQueue;

    .line 134
    iput-object v2, p0, Lorg/chromium/base/SystemMessageHandler;->d:Ljava/lang/reflect/Field;

    .line 135
    iput-object v2, p0, Lorg/chromium/base/SystemMessageHandler;->e:Ljava/lang/reflect/Field;

    .line 136
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/chromium/base/SystemMessageHandler;->a(Z)V

    .line 137
    return-void
.end method

.method private static create(J)Lorg/chromium/base/SystemMessageHandler;
    .locals 2
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 161
    new-instance v0, Lorg/chromium/base/SystemMessageHandler;

    invoke-direct {v0, p0, p1}, Lorg/chromium/base/SystemMessageHandler;-><init>(J)V

    return-object v0
.end method

.method private native nativeDoRunLoopOnce(JJ)V
.end method

.method private removeAllPendingMessages()V
    .locals 1
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->a()V

    .line 75
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/base/SystemMessageHandler;->removeMessages(I)V

    .line 76
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/chromium/base/SystemMessageHandler;->removeMessages(I)V

    .line 77
    return-void
.end method

.method private scheduleDelayedWork(JJ)V
    .locals 5
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 62
    iget-wide v0, p0, Lorg/chromium/base/SystemMessageHandler;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0, v4}, Lorg/chromium/base/SystemMessageHandler;->removeMessages(I)V

    .line 65
    :cond_0
    iput-wide p1, p0, Lorg/chromium/base/SystemMessageHandler;->b:J

    .line 66
    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->a()V

    .line 67
    iget-boolean v0, p0, Lorg/chromium/base/SystemMessageHandler;->f:Z

    if-eqz v0, :cond_1

    const-string v0, "SystemMessageHandler:delayedWorkBlocked"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->a(Ljava/lang/String;)V

    .line 68
    :cond_1
    invoke-virtual {p0, v4, p3, p4}, Lorg/chromium/base/SystemMessageHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 69
    return-void
.end method

.method private scheduleWork()V
    .locals 1
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->a()V

    .line 55
    iget-boolean v0, p0, Lorg/chromium/base/SystemMessageHandler;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "SystemMessageHandler:immediateWorkBlocked"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->a(Ljava/lang/String;)V

    .line 56
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/base/SystemMessageHandler;->sendEmptyMessage(I)Z

    .line 57
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/chromium/base/SystemMessageHandler;->a()V

    .line 45
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/base/SystemMessageHandler;->b:J

    .line 48
    :cond_0
    iget-wide v0, p0, Lorg/chromium/base/SystemMessageHandler;->a:J

    iget-wide v2, p0, Lorg/chromium/base/SystemMessageHandler;->b:J

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/chromium/base/SystemMessageHandler;->nativeDoRunLoopOnce(JJ)V

    .line 49
    return-void
.end method
