.class public Lorg/chromium/base/PowerMonitor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field private static final c:Ljava/lang/Runnable;


# instance fields
.field private a:Z

.field private final b:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lorg/chromium/base/PowerMonitor$1;

    invoke-direct {v0}, Lorg/chromium/base/PowerMonitor$1;-><init>()V

    sput-object v0, Lorg/chromium/base/PowerMonitor;->c:Ljava/lang/Runnable;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lorg/chromium/base/PowerMonitor;->b:Landroid/os/Handler;

    .line 64
    return-void
.end method

.method static synthetic a()V
    .locals 0

    .prologue
    .line 19
    invoke-static {}, Lorg/chromium/base/PowerMonitor;->nativeOnMainActivitySuspended()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method private static isBatteryPower()Z
    .locals 1
    .annotation build Lorg/chromium/base/CalledByNative;
    .end annotation

    .prologue
    .line 92
    const/4 v0, 0x0

    iget-boolean v0, v0, Lorg/chromium/base/PowerMonitor;->a:Z

    return v0
.end method

.method private static native nativeOnBatteryChargingChanged()V
.end method

.method private static native nativeOnMainActivityResumed()V
.end method

.method private static native nativeOnMainActivitySuspended()V
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 81
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 83
    iget-object v0, p0, Lorg/chromium/base/PowerMonitor;->b:Landroid/os/Handler;

    sget-object v1, Lorg/chromium/base/PowerMonitor;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 84
    invoke-static {}, Lorg/chromium/base/PowerMonitor;->nativeOnMainActivityResumed()V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 86
    iget-object v0, p0, Lorg/chromium/base/PowerMonitor;->b:Landroid/os/Handler;

    sget-object v1, Lorg/chromium/base/PowerMonitor;->c:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
