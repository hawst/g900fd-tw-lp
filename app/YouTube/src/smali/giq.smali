.class public Lgiq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lws;

.field final b:Landroid/content/SharedPreferences;

.field final c:Lghm;

.field d:Lgim;

.field final e:Ljava/lang/String;

.field final f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lws;Landroid/content/SharedPreferences;Lghm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    iput-object v0, p0, Lgiq;->a:Lws;

    .line 56
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lgiq;->b:Landroid/content/SharedPreferences;

    .line 57
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghm;

    iput-object v0, p0, Lgiq;->c:Lghm;

    .line 58
    invoke-static {p4}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgiq;->e:Ljava/lang/String;

    .line 59
    invoke-static {p5}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgiq;->f:Ljava/lang/String;

    .line 61
    invoke-static {p6}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 62
    const-string v0, "%s_%s"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "apiary_device_id"

    aput-object v2, v1, v3

    aput-object p6, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgiq;->g:Ljava/lang/String;

    .line 63
    const-string v0, "%s_%s"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "apiary_device_key"

    aput-object v2, v1, v3

    aput-object p6, v1, v4

    .line 64
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgiq;->h:Ljava/lang/String;

    .line 65
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lgiq;->d:Lgim;

    .line 216
    iget-object v0, p0, Lgiq;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lgiq;->g:Ljava/lang/String;

    .line 217
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lgiq;->h:Ljava/lang/String;

    .line 218
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 219
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 220
    return-void
.end method

.method a(Lgim;)V
    .locals 5

    .prologue
    .line 88
    iget-object v0, p0, Lgiq;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lgiq;->g:Ljava/lang/String;

    .line 89
    iget-object v2, p1, Lgim;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lgiq;->h:Ljava/lang/String;

    new-instance v2, Ljava/lang/String;

    .line 91
    iget-object v3, p1, Lgim;->b:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    .line 90
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 92
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 93
    return-void
.end method

.method public a(Ljava/util/Map;Ljava/lang/String;[B)V
    .locals 6

    .prologue
    .line 204
    invoke-virtual {p0}, Lgiq;->c()Lgim;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_0

    .line 209
    const-string v1, "device_id=%s,data=%s,content=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v0, Lgim;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, La;->b([BI)[B

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v0, v4, v5}, Lgim;->a([BI)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x14

    invoke-virtual {v0, p3, v4}, Lgim;->a([BI)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "X-Goog-Device-Auth"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    :cond_0
    return-void
.end method

.method b()Lgim;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 72
    iget-object v1, p0, Lgiq;->b:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lgiq;->g:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    iget-object v2, p0, Lgiq;->b:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgiq;->h:Ljava/lang/String;

    .line 74
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 76
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 77
    const/4 v0, 0x0

    invoke-static {v2, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 78
    new-instance v0, Lgim;

    invoke-direct {v0, v1, v2}, Lgim;-><init>(Ljava/lang/String;[B)V

    .line 80
    :cond_0
    return-object v0
.end method

.method declared-synchronized c()Lgim;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 111
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lb;->b()V

    .line 114
    iget-object v0, p0, Lgiq;->c:Lghm;

    invoke-virtual {v0}, Lghm;->g()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 186
    :goto_0
    monitor-exit p0

    return-object v0

    .line 119
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgiq;->d:Lgim;

    if-eqz v0, :cond_1

    .line 120
    const-string v0, "Returned cached device auth."

    invoke-static {v0}, Lezp;->f(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lgiq;->d:Lgim;

    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {p0}, Lgiq;->b()Lgim;

    move-result-object v0

    iput-object v0, p0, Lgiq;->d:Lgim;

    .line 126
    iget-object v0, p0, Lgiq;->d:Lgim;

    if-eqz v0, :cond_2

    .line 127
    const-string v0, "Returned device auth from shared preferences."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lgiq;->d:Lgim;

    goto :goto_0

    .line 133
    :cond_2
    new-instance v3, Lezm;

    invoke-direct {v3}, Lezm;-><init>()V

    .line 135
    iget-object v0, p0, Lgiq;->c:Lghm;

    invoke-virtual {v0}, Lghm;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v2, p0, Lgiq;->c:Lghm;

    .line 136
    invoke-virtual {v2}, Lghm;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "key"

    iget-object v4, p0, Lgiq;->e:Ljava/lang/String;

    .line 137
    invoke-virtual {v0, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "rawDeviceId"

    iget-object v4, p0, Lgiq;->f:Ljava/lang/String;

    .line 138
    invoke-virtual {v0, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 141
    invoke-static {}, Lgky;->a()Lgky;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 145
    :try_start_2
    new-instance v5, Lgip;

    iget-object v2, p0, Lgiq;->c:Lghm;

    .line 148
    invoke-virtual {v2}, Lghm;->g()[B

    move-result-object v2

    const/4 v6, 0x4

    invoke-direct {v5, v0, v4, v2, v6}, Lgip;-><init>(Landroid/net/Uri;Lwv;[BI)V
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 161
    :cond_3
    :try_start_3
    iget-object v0, p0, Lgiq;->a:Lws;

    invoke-virtual {v0, v5}, Lws;->a(Lwp;)Lwp;

    .line 164
    const-wide/16 v6, 0xf

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v0}, Lgky;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgim;

    iput-object v0, p0, Lgiq;->d:Lgim;

    .line 165
    iget-object v0, p0, Lgiq;->d:Lgim;

    invoke-virtual {p0, v0}, Lgiq;->a(Lgim;)V

    .line 166
    const-string v0, "Successfully completed device registration."

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lgiq;->d:Lgim;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 156
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 170
    :catch_2
    move-exception v0

    move-object v2, v0

    .line 172
    :try_start_4
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lxa;

    if-eqz v0, :cond_4

    .line 173
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lxa;

    .line 174
    iget-object v6, v0, Lxa;->a:Lwm;

    if-eqz v6, :cond_4

    iget-object v6, v0, Lxa;->a:Lwm;

    iget-object v6, v6, Lwm;->b:[B

    if-eqz v6, :cond_4

    .line 175
    const-string v6, "Server returned: "

    new-instance v7, Ljava/lang/String;

    iget-object v0, v0, Lxa;->a:Lwm;

    iget-object v0, v0, Lwm;->b:[B

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 182
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x27

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Could not do device auth handshake: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " - "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 181
    invoke-static {v2}, Lezp;->b(Ljava/lang/String;)V

    .line 183
    invoke-virtual {v3}, Lezm;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 185
    iget-wide v2, v3, Lezm;->a:J

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x36

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Giving up device auth after "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tries"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 186
    goto/16 :goto_0

    .line 175
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 178
    :catch_3
    move-exception v0

    goto :goto_2

    .line 168
    :catch_4
    move-exception v0

    goto :goto_2
.end method
