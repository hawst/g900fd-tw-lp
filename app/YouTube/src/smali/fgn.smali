.class public final Lfgn;
.super Lfsp;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Z

.field public d:Ljava/util/List;

.field private final j:Lfth;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method constructor <init>(Lfsz;Lgit;Lfth;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 191
    iget-object v0, p2, Lgit;->b:Lgiv;

    sget-object v1, Lfsq;->c:Lfsq;

    invoke-direct {p0, p1, v0, v1}, Lfsp;-><init>(Lfsz;Lgiv;Lfsq;)V

    .line 179
    const-string v0, ""

    iput-object v0, p0, Lfgn;->k:Ljava/lang/String;

    .line 180
    const-string v0, ""

    iput-object v0, p0, Lfgn;->a:Ljava/lang/String;

    .line 181
    iput v2, p0, Lfgn;->b:I

    .line 182
    const-string v0, ""

    iput-object v0, p0, Lfgn;->l:Ljava/lang/String;

    .line 183
    const-string v0, ""

    iput-object v0, p0, Lfgn;->m:Ljava/lang/String;

    .line 184
    iput-boolean v2, p0, Lfgn;->c:Z

    .line 185
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfgn;->d:Ljava/util/List;

    .line 192
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfth;

    iput-object v0, p0, Lfgn;->j:Lfth;

    .line 193
    sget-object v0, Lfhy;->a:[B

    invoke-virtual {p0, v0}, Lfgn;->a([B)V

    .line 194
    return-void
.end method


# virtual methods
.method public final a(Lfjk;)Lfgn;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p1, Lfjk;->a:Ljava/lang/String;

    invoke-static {v0}, Lfgn;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgn;->m:Ljava/lang/String;

    .line 226
    iget-object v0, p1, Lfjk;->b:[B

    invoke-virtual {p0, v0}, Lfgn;->a([B)V

    .line 227
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lfgn;
    .locals 1

    .prologue
    .line 201
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfgn;->k:Ljava/lang/String;

    .line 202
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    const-string v0, "next"

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lfgn;
    .locals 1

    .prologue
    .line 220
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfgn;->l:Ljava/lang/String;

    .line 221
    return-object p0
.end method

.method protected final b()V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lfgn;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfgn;->k:Ljava/lang/String;

    .line 243
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfgn;->m:Ljava/lang/String;

    .line 244
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 245
    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 246
    return-void

    .line 244
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic c()Lidh;
    .locals 4

    .prologue
    .line 172
    new-instance v2, Libh;

    invoke-direct {v2}, Libh;-><init>()V

    iget-object v0, p0, Lfgn;->a:Ljava/lang/String;

    iput-object v0, v2, Libh;->c:Ljava/lang/String;

    iget v0, p0, Lfgn;->b:I

    if-ltz v0, :cond_0

    iget v0, p0, Lfgn;->b:I

    iput v0, v2, Libh;->e:I

    :cond_0
    iget-object v0, p0, Lfgn;->l:Ljava/lang/String;

    iput-object v0, v2, Libh;->d:Ljava/lang/String;

    iget-object v0, p0, Lfgn;->k:Ljava/lang/String;

    iput-object v0, v2, Libh;->b:Ljava/lang/String;

    iget-object v0, p0, Lfgn;->m:Ljava/lang/String;

    iput-object v0, v2, Libh;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lfgn;->i()Lhjx;

    move-result-object v0

    iput-object v0, v2, Libh;->a:Lhjx;

    iget-boolean v0, p0, Lfgn;->c:Z

    iput-boolean v0, v2, Libh;->g:Z

    iget-object v0, p0, Lfgn;->j:Lfth;

    invoke-interface {v0}, Lfth;->a()Lhrn;

    move-result-object v0

    iput-object v0, v2, Libh;->h:Lhrn;

    iget-object v0, p0, Lfgn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, v2, Libh;->i:[I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lfgn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v3, v2, Libh;->i:[I

    iget-object v0, p0, Lfgn;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 280
    invoke-virtual {p0}, Lfgn;->j()Lggq;

    move-result-object v0

    .line 281
    const-string v1, "videoId"

    iget-object v2, p0, Lfgn;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 282
    const-string v1, "playlistId"

    iget-object v2, p0, Lfgn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 283
    const-string v1, "playlistIndex"

    iget v2, p0, Lfgn;->b:I

    invoke-static {v2}, Lfgn;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;I)Lggq;

    .line 284
    const-string v1, "params"

    iget-object v2, p0, Lfgn;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 285
    const-string v1, "continuation"

    iget-object v2, p0, Lfgn;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 286
    const-string v1, "isAdPlayback"

    iget-boolean v2, p0, Lfgn;->c:Z

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Z)Lggq;

    .line 287
    invoke-virtual {v0}, Lggq;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
