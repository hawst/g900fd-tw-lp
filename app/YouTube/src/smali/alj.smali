.class public final Lalj;
.super Lajv;
.source "SourceFile"


# instance fields
.field private final a:Ldfm;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ldfm;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lajv;-><init>()V

    .line 21
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfm;

    iput-object v0, p0, Lalj;->a:Ldfm;

    .line 22
    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lalj;->b:Landroid/os/Handler;

    .line 23
    return-void
.end method

.method static synthetic a(Lalj;)Ldfm;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lalj;->a:Ldfm;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lalj;->b:Landroid/os/Handler;

    new-instance v1, Lalk;

    invoke-direct {v1, p0}, Lalk;-><init>(Lalj;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 33
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lalj;->b:Landroid/os/Handler;

    new-instance v1, Laln;

    invoke-direct {v1, p0, p1}, Laln;-><init>(Lalj;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 63
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lalj;->b:Landroid/os/Handler;

    new-instance v1, Lall;

    invoke-direct {v1, p0}, Lall;-><init>(Lalj;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 43
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lalj;->b:Landroid/os/Handler;

    new-instance v1, Lalm;

    invoke-direct {v1, p0}, Lalm;-><init>(Lalj;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 53
    return-void
.end method
