.class public final Lfqj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lhgy;

.field public final b:Ljava/lang/String;

.field public final c:J

.field public d:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 302
    new-instance v0, Lfqk;

    invoke-direct {v0}, Lfqk;-><init>()V

    sput-object v0, Lfqj;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lhgy;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lfqj;->a:Lhgy;

    .line 53
    iput-object p2, p0, Lfqj;->b:Ljava/lang/String;

    .line 54
    iput-wide p3, p0, Lfqj;->c:J

    .line 58
    iget-object v0, p1, Lhgy;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfqj;->d:Landroid/net/Uri;

    .line 59
    if-eqz p2, :cond_0

    .line 60
    iget-object v0, p0, Lfqj;->d:Landroid/net/Uri;

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v0

    const-string v1, "dnc"

    const-string v2, "1"

    .line 61
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    .line 62
    iget-object v0, v0, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfqj;->d:Landroid/net/Uri;

    .line 64
    :cond_0
    return-void
.end method

.method static synthetic a(Lfqj;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lfqj;->d:Landroid/net/Uri;

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;J)Lfqj;
    .locals 2

    .prologue
    .line 254
    new-instance v0, Lhgy;

    invoke-direct {v0}, Lhgy;-><init>()V

    .line 255
    const/16 v1, 0x5d

    iput v1, v0, Lhgy;->b:I

    .line 256
    const-string v1, "application/x-mpegURL"

    iput-object v1, v0, Lhgy;->d:Ljava/lang/String;

    .line 257
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhgy;->c:Ljava/lang/String;

    .line 258
    new-instance v1, Lfqj;

    invoke-direct {v1, v0, p1, p2, p3}, Lfqj;-><init>(Lhgy;Ljava/lang/String;J)V

    return-object v1
.end method

.method static synthetic b(Lfqj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lfqj;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lfqj;)J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lfqj;->c:J

    return-wide v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 82
    if-nez p1, :cond_0

    iget-object v0, p0, Lfqj;->d:Landroid/net/Uri;

    .line 84
    :goto_0
    return-object v0

    .line 82
    :cond_0
    iget-object v0, p0, Lfqj;->d:Landroid/net/Uri;

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v0

    const-string v1, "cpn"

    .line 83
    invoke-virtual {v0, v1, p1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    .line 84
    iget-object v0, v0, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Lhgy;
    .locals 2

    .prologue
    .line 69
    :try_start_0
    new-instance v0, Lhgy;

    invoke-direct {v0}, Lhgy;-><init>()V

    .line 70
    iget-object v1, p0, Lfqj;->a:Lhgy;

    invoke-static {v1}, Lidh;->a(Lidh;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lidh;->a(Lidh;[B)Lidh;

    move-result-object v0

    check-cast v0, Lhgy;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 71
    :catch_0
    move-exception v0

    .line 73
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()I
    .locals 8

    .prologue
    const/16 v3, 0x1e0

    const/16 v2, 0x168

    const/16 v1, 0xf0

    const/16 v0, 0x90

    const/4 v4, -0x1

    .line 155
    iget-object v5, p0, Lfqj;->a:Lhgy;

    iget-object v5, v5, Lhgy;->d:Ljava/lang/String;

    invoke-static {v5}, Lb;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 156
    iget-object v5, p0, Lfqj;->a:Lhgy;

    iget v5, v5, Lhgy;->f:I

    .line 157
    iget-object v6, p0, Lfqj;->a:Lhgy;

    iget v6, v6, Lhgy;->g:I

    .line 158
    const/16 v7, 0xf00

    if-gt v5, v7, :cond_0

    const/16 v7, 0x870

    if-le v6, v7, :cond_2

    :cond_0
    move v0, v4

    .line 181
    :cond_1
    :goto_0
    return v0

    .line 160
    :cond_2
    const/16 v7, 0xa00

    if-gt v5, v7, :cond_3

    const/16 v7, 0x5a0

    if-le v6, v7, :cond_4

    .line 161
    :cond_3
    const/16 v0, 0x870

    goto :goto_0

    .line 162
    :cond_4
    const/16 v7, 0x780

    if-gt v5, v7, :cond_5

    const/16 v7, 0x438

    if-le v6, v7, :cond_6

    .line 163
    :cond_5
    const/16 v0, 0x5a0

    goto :goto_0

    .line 164
    :cond_6
    const/16 v7, 0x500

    if-gt v5, v7, :cond_7

    const/16 v7, 0x2d0

    if-le v6, v7, :cond_8

    .line 165
    :cond_7
    const/16 v0, 0x438

    goto :goto_0

    .line 166
    :cond_8
    const/16 v7, 0x356

    if-gt v5, v7, :cond_9

    if-le v6, v3, :cond_a

    .line 167
    :cond_9
    const/16 v0, 0x2d0

    goto :goto_0

    .line 168
    :cond_a
    const/16 v7, 0x280

    if-gt v5, v7, :cond_b

    if-le v6, v2, :cond_c

    :cond_b
    move v0, v3

    .line 169
    goto :goto_0

    .line 170
    :cond_c
    const/16 v3, 0x1aa

    if-gt v5, v3, :cond_d

    if-le v6, v1, :cond_e

    :cond_d
    move v0, v2

    .line 171
    goto :goto_0

    .line 172
    :cond_e
    const/16 v2, 0x100

    if-gt v5, v2, :cond_f

    if-le v6, v0, :cond_10

    :cond_f
    move v0, v1

    .line 173
    goto :goto_0

    .line 174
    :cond_10
    if-gtz v5, :cond_1

    if-gtz v6, :cond_1

    :cond_11
    move v0, v4

    .line 181
    goto :goto_0

    .line 177
    :cond_12
    iget-object v0, p0, Lfqj;->a:Lhgy;

    iget-object v0, v0, Lhgy;->d:Ljava/lang/String;

    invoke-static {v0}, Lb;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-static {}, Lfqm;->k()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lfqj;->a:Lhgy;

    iget v1, v1, Lhgy;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 179
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ledm;
    .locals 28

    .prologue
    .line 227
    new-instance v2, Lfrk;

    .line 228
    move-object/from16 v0, p0

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget v3, v3, Lhgy;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lfqj;->a:Lhgy;

    iget-object v4, v4, Lhgy;->q:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lfqj;->a:Lhgy;

    iget-object v5, v5, Lhgy;->d:Ljava/lang/String;

    .line 231
    move-object/from16 v0, p0

    iget-object v6, v0, Lfqj;->a:Lhgy;

    iget v6, v6, Lhgy;->f:I

    .line 232
    move-object/from16 v0, p0

    iget-object v7, v0, Lfqj;->a:Lhgy;

    iget v7, v7, Lhgy;->g:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lfqj;->a:Lhgy;

    iget v10, v10, Lhgy;->e:I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lfqj;->a:Lhgy;

    iget-object v12, v12, Lhgy;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lfqj;->a:Lhgy;

    iget-boolean v13, v13, Lhgy;->p:Z

    invoke-direct/range {v2 .. v13}, Lfrk;-><init>(ILjava/lang/String;Ljava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Z)V

    .line 239
    move-object/from16 v0, p0

    iget-wide v0, v0, Lfqj;->c:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfqj;->b:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget-wide v0, v3, Lhgy;->j:J

    move-wide/from16 v22, v0

    .line 245
    invoke-virtual/range {p0 .. p1}, Lfqj;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget-object v3, v3, Lhgy;->h:Lhtk;

    iget-wide v6, v3, Lhtk;->a:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget-object v3, v3, Lhgy;->h:Lhtk;

    iget-wide v8, v3, Lhtk;->b:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget-object v3, v3, Lhgy;->i:Lhtk;

    iget-wide v13, v3, Lhtk;->a:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget-object v3, v3, Lhgy;->i:Lhtk;

    iget-wide v0, v3, Lhtk;->b:J

    move-wide/from16 v24, v0

    .line 250
    move-object/from16 v0, p0

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget-wide v0, v3, Lhgy;->k:J

    move-wide/from16 v18, v0

    .line 239
    new-instance v3, Ledj;

    const/4 v5, 0x0

    sub-long/2addr v8, v6

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    invoke-direct/range {v3 .. v9}, Ledj;-><init>(Landroid/net/Uri;Ljava/lang/String;JJ)V

    new-instance v6, Leds;

    const-wide/16 v8, 0x1

    const-wide/16 v10, 0x0

    sub-long v24, v24, v13

    const-wide/16 v26, 0x1

    add-long v15, v24, v26

    move-object v7, v3

    move-object v12, v4

    invoke-direct/range {v6 .. v16}, Leds;-><init>(Ledj;JJLandroid/net/Uri;JJ)V

    new-instance v8, Ledm;

    const-wide/16 v9, 0x0

    move-wide/from16 v11, v20

    move-object/from16 v13, v17

    move-wide/from16 v14, v22

    move-object/from16 v16, v2

    move-object/from16 v17, v6

    invoke-direct/range {v8 .. v19}, Ledm;-><init>(JJLjava/lang/String;JLecw;Leds;J)V

    return-object v8
.end method

.method public final c()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 200
    iget-object v1, p0, Lfqj;->a:Lhgy;

    iget-object v2, v1, Lhgy;->r:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 201
    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 202
    const/4 v0, 0x1

    .line 205
    :cond_0
    return v0

    .line 200
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lfqj;->a:Lhgy;

    iget-object v0, v0, Lhgy;->d:Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lfqj;->a:Lhgy;

    iget-object v0, v0, Lhgy;->d:Ljava/lang/String;

    invoke-static {v0}, Lb;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 276
    if-ne p0, p1, :cond_1

    .line 282
    :cond_0
    :goto_0
    return v0

    .line 277
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 278
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 279
    :cond_3
    check-cast p1, Lfqj;

    .line 280
    iget-wide v2, p0, Lfqj;->c:J

    iget-wide v4, p1, Lfqj;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lfqj;->b:Ljava/lang/String;

    iget-object v3, p1, Lfqj;->b:Ljava/lang/String;

    .line 281
    invoke-static {v2, v3}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lfqj;->a:Lhgy;

    iget-object v3, p1, Lfqj;->a:Lhgy;

    .line 282
    invoke-static {v2, v3}, Lidh;->a(Lidh;Lidh;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 263
    iget-wide v2, p0, Lfqj;->c:J

    iget-wide v4, p0, Lfqj;->c:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit8 v0, v0, 0x1f

    .line 266
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lfqj;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 267
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lfqj;->a:Lhgy;

    if-nez v2, :cond_1

    .line 270
    :goto_1
    add-int/2addr v0, v1

    .line 271
    return v0

    .line 266
    :cond_0
    iget-object v0, p0, Lfqj;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 267
    :cond_1
    iget-object v1, p0, Lfqj;->a:Lhgy;

    .line 270
    invoke-static {v1}, Lidh;->a(Lidh;)[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 287
    const-string v0, "v:{%s} t:{%d} i:{%d} s:{%dx%d} m:{%s} e:{%s} u:{%s}"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfqj;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lfqj;->c:J

    .line 289
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 290
    iget-object v3, p0, Lfqj;->a:Lhgy;

    iget v3, v3, Lhgy;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 291
    iget-object v3, p0, Lfqj;->a:Lhgy;

    iget v3, v3, Lhgy;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lfqj;->a:Lhgy;

    iget v3, v3, Lhgy;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    .line 292
    iget-object v3, p0, Lfqj;->a:Lhgy;

    iget-object v3, v3, Lhgy;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lfqj;->a:Lhgy;

    iget-object v3, v3, Lhgy;->r:[I

    .line 293
    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lfqj;->d:Landroid/net/Uri;

    .line 294
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 287
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lfqj;->a:Lhgy;

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lidh;)V

    .line 327
    iget-object v0, p0, Lfqj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 328
    iget-wide v0, p0, Lfqj;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 329
    return-void
.end method
