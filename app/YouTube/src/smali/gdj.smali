.class public final Lgdj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;
.implements Leaw;
.implements Lgeh;


# static fields
.field private static final b:Lgdm;

.field private static c:Ljava/lang/Boolean;


# instance fields
.field private A:Lecc;

.field private B:Z

.field private C:Lfrf;

.field private D:I

.field private E:I

.field private F:F

.field private G:Z

.field private H:Ljava/lang/String;

.field private I:Lfqy;

.field private J:Ledz;

.field private K:Z

.field private L:Lfrb;

.field private M:Lfrc;

.field private N:Z

.field private O:Z

.field final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final d:Lgdm;

.field private final e:Lexd;

.field private final f:Lezi;

.field private final g:Lewi;

.field private final h:Legq;

.field private final i:Leey;

.field private final j:Lgfl;

.field private final k:Lgei;

.field private final l:Lgdp;

.field private final m:Lebt;

.field private final n:Lebe;

.field private final o:Lges;

.field private final p:Lgdl;

.field private final q:Landroid/os/Handler;

.field private final r:Legc;

.field private final s:Lewi;

.field private final t:Lewi;

.field private final u:Lggn;

.field private v:Lgec;

.field private w:Leau;

.field private x:Lggh;

.field private y:Lggf;

.field private z:Lecc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lgdm;

    invoke-direct {v0}, Lgdm;-><init>()V

    sput-object v0, Lgdj;->b:Lgdm;

    return-void
.end method

.method public constructor <init>(Lexd;Lezi;Lewi;Lezj;Legq;Leey;Lges;Lgfl;Lggn;Legc;Lewi;Lewi;)V
    .locals 14

    .prologue
    .line 170
    sget-object v13, Lgdj;->b:Lgdm;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lgdj;-><init>(Lexd;Lezi;Lewi;Lezj;Legq;Leey;Lges;Lgfl;Lggn;Legc;Lewi;Lewi;Lgdm;)V

    .line 184
    return-void
.end method

.method private constructor <init>(Lexd;Lezi;Lewi;Lezj;Legq;Leey;Lges;Lgfl;Lggn;Legc;Lewi;Lewi;Lgdm;)V
    .locals 3

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lgdj;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 200
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lgdj;->e:Lexd;

    .line 201
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezi;

    iput-object v0, p0, Lgdj;->f:Lezi;

    .line 202
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Lgdj;->g:Lewi;

    .line 203
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legq;

    iput-object v0, p0, Lgdj;->h:Legq;

    .line 204
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leey;

    iput-object v0, p0, Lgdj;->i:Leey;

    .line 205
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lges;

    iput-object v0, p0, Lgdj;->o:Lges;

    .line 206
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfl;

    iput-object v0, p0, Lgdj;->j:Lgfl;

    .line 207
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggn;

    iput-object v0, p0, Lgdj;->u:Lggn;

    .line 208
    iput-object p10, p0, Lgdj;->r:Legc;

    .line 209
    iput-object p11, p0, Lgdj;->s:Lewi;

    .line 211
    invoke-static {p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Lgdj;->t:Lewi;

    .line 212
    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdm;

    iput-object v0, p0, Lgdj;->d:Lgdm;

    .line 213
    new-instance v0, Lgdp;

    invoke-direct {v0, p0}, Lgdp;-><init>(Lgdj;)V

    iput-object v0, p0, Lgdj;->l:Lgdp;

    .line 214
    new-instance v0, Lgdo;

    invoke-direct {v0, p0}, Lgdo;-><init>(Lgdj;)V

    iput-object v0, p0, Lgdj;->m:Lebt;

    .line 215
    new-instance v0, Lgdn;

    invoke-direct {v0, p0}, Lgdn;-><init>(Lgdj;)V

    iput-object v0, p0, Lgdj;->n:Lebe;

    .line 216
    new-instance v1, Lget;

    new-instance v2, Lgdf;

    .line 217
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    invoke-direct {v2, v0}, Lgdf;-><init>(Lezj;)V

    invoke-direct {v1, v2}, Lget;-><init>(Lgei;)V

    iput-object v1, p0, Lgdj;->k:Lgei;

    .line 218
    new-instance v0, Lgdl;

    invoke-direct {v0, p0}, Lgdl;-><init>(Lgdj;)V

    iput-object v0, p0, Lgdj;->p:Lgdl;

    .line 219
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lgdj;->q:Landroid/os/Handler;

    .line 220
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lgdj;->F:F

    .line 221
    new-instance v0, Ledz;

    invoke-direct {v0, p0}, Ledz;-><init>(Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;)V

    iput-object v0, p0, Lgdj;->J:Ledz;

    .line 222
    return-void
.end method

.method static synthetic a(Lgdj;I)I
    .locals 0

    .prologue
    .line 87
    iput p1, p0, Lgdj;->D:I

    return p1
.end method

.method static synthetic a(Lgdj;)Lfrf;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lgdj;->C:Lfrf;

    return-object v0
.end method

.method private a(Lfrf;Lfqy;Lfrb;ZZ)Lger;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 249
    iget-boolean v0, p1, Lfrf;->h:Z

    if-eqz v0, :cond_0

    .line 251
    const-string v0, "Live video not supported by adaptive DASH player."

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 252
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 255
    :cond_0
    invoke-static {}, La;->K()I

    move-result v2

    invoke-static {}, Lgdj;->x()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p2, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->t:I

    :goto_0
    if-lt v2, v0, :cond_6

    iget-object v0, p0, Lgdj;->f:Lezi;

    invoke-virtual {v0}, Lezi;->a()F

    move-result v3

    iget-object v0, p2, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->u:F

    :goto_1
    cmpl-float v0, v3, v0

    if-ltz v0, :cond_6

    invoke-static {}, Lfqm;->f()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lgdj;->a(Lfrf;Ljava/util/Set;)[Lfre;

    move-result-object v0

    invoke-static {}, Lfqm;->g()Ljava/util/Set;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lgdj;->a(Lfrf;Ljava/util/Set;)[Lfre;

    move-result-object v3

    array-length v4, v0

    if-nez v4, :cond_3

    move v0, v7

    :goto_2
    if-eqz v0, :cond_7

    .line 256
    invoke-static {}, Lfqm;->g()Ljava/util/Set;

    move-result-object v3

    .line 257
    :goto_3
    iget-boolean v0, p1, Lfrf;->j:Z

    if-eqz v0, :cond_8

    if-nez p5, :cond_8

    move v8, v7

    .line 258
    :goto_4
    iget-object v0, p0, Lgdj;->o:Lges;

    .line 260
    iget-object v2, p1, Lfrf;->c:Ljava/util/List;

    .line 262
    invoke-static {}, Lfqm;->k()Ljava/util/Set;

    move-result-object v4

    .line 263
    invoke-virtual {p2, p3}, Lfqy;->b(Lfrb;)Z

    move-result v5

    if-nez p4, :cond_9

    move v6, v7

    :goto_5
    move-object v1, p2

    .line 258
    invoke-interface/range {v0 .. v8}, Lges;->a(Lfqy;Ljava/util/Collection;Ljava/util/Set;Ljava/util/Set;ZZZZ)Lger;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    .line 255
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    array-length v4, v3

    if-eqz v4, :cond_6

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v0, v0, v4

    iget v0, v0, Lfre;->a:I

    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lfre;->a:I

    if-gt v0, v3, :cond_6

    invoke-virtual {p2}, Lfqy;->t()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v4

    if-gez v4, :cond_4

    move v0, v7

    goto :goto_2

    :cond_4
    invoke-virtual {v0, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    if-lt v0, v3, :cond_5

    move v0, v7

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_2

    .line 256
    :cond_7
    invoke-static {}, Lfqm;->f()Ljava/util/Set;

    move-result-object v3

    goto :goto_3

    :cond_8
    move v8, v1

    .line 257
    goto :goto_4

    :cond_9
    move v6, v1

    .line 263
    goto :goto_5
.end method

.method public static a(Leat;I)Lggp;
    .locals 6

    .prologue
    .line 1049
    invoke-virtual {p0}, Leat;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 1050
    invoke-virtual {p0}, Leat;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 1051
    instance-of v2, v0, Lefq;

    if-eqz v2, :cond_1

    .line 1052
    check-cast v0, Lefq;

    iget v0, v0, Lefq;->a:I

    .line 1053
    const/16 v1, 0x193

    if-ne v0, v1, :cond_0

    .line 1054
    new-instance v0, Lggp;

    const-string v1, "staleconfig"

    invoke-direct {v0, v1, p1}, Lggp;-><init>(Ljava/lang/String;I)V

    .line 1099
    :goto_0
    return-object v0

    .line 1057
    :cond_0
    new-instance v1, Lggp;

    const-string v2, "net.badstatus"

    .line 1058
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, p1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    invoke-virtual {v1}, Lggp;->b()Lggp;

    move-result-object v0

    goto :goto_0

    .line 1059
    :cond_1
    instance-of v2, v0, Lefp;

    if-eqz v2, :cond_2

    .line 1060
    new-instance v1, Lggp;

    const-string v2, "fmt.unparseable"

    check-cast v0, Lefp;

    iget-object v0, v0, Lefp;->a:Ljava/lang/String;

    invoke-direct {v1, v2, p1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    .line 1064
    :cond_2
    instance-of v2, v0, Lefo;

    if-eqz v2, :cond_3

    .line 1065
    new-instance v1, Lggp;

    const-string v2, "net.closed"

    invoke-direct {v1, v2, p1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1068
    invoke-virtual {v1}, Lggp;->b()Lggp;

    move-result-object v0

    goto :goto_0

    .line 1069
    :cond_3
    instance-of v2, v0, Lebx;

    if-eqz v2, :cond_4

    .line 1070
    new-instance v1, Lggp;

    const-string v2, "fmt.unparseable"

    invoke-direct {v1, v2, p1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    .line 1074
    :cond_4
    instance-of v2, v0, Landroid/media/MediaCodec$CryptoException;

    if-eqz v2, :cond_5

    .line 1075
    check-cast v0, Landroid/media/MediaCodec$CryptoException;

    .line 1076
    new-instance v1, Lggp;

    const-string v2, "drm.keyerror"

    .line 1079
    invoke-virtual {v0}, Landroid/media/MediaCodec$CryptoException;->getErrorCode()I

    move-result v3

    invoke-virtual {v0}, Landroid/media/MediaCodec$CryptoException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, p1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    .line 1080
    :cond_5
    instance-of v2, v0, Lebi;

    if-eqz v2, :cond_6

    .line 1081
    new-instance v1, Lggp;

    const-string v2, "android.exo.decoderinit"

    check-cast v0, Lebi;

    iget-object v0, v0, Lebi;->a:Ljava/lang/String;

    invoke-direct {v1, v2, p1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1084
    invoke-virtual {v1}, Lggp;->b()Lggp;

    move-result-object v0

    goto/16 :goto_0

    .line 1085
    :cond_6
    instance-of v2, v0, Lech;

    if-eqz v2, :cond_7

    .line 1086
    new-instance v1, Lggp;

    const-string v2, "android.exo.audioinit"

    check-cast v0, Lech;

    iget v0, v0, Lech;->a:I

    .line 1089
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, p1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    invoke-virtual {v1}, Lggp;->b()Lggp;

    move-result-object v0

    goto/16 :goto_0

    .line 1090
    :cond_7
    instance-of v2, v0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_8

    .line 1091
    new-instance v1, Lggp;

    const-string v2, "android.exo"

    invoke-direct {v1, v2, p1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1092
    invoke-virtual {v1}, Lggp;->b()Lggp;

    move-result-object v0

    goto/16 :goto_0

    .line 1093
    :cond_8
    if-eqz v0, :cond_9

    .line 1094
    new-instance v1, Lggp;

    const-string v2, "android.exo"

    invoke-direct {v1, v2, p1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 1099
    :cond_9
    new-instance v0, Lggp;

    const-string v2, "android.exo"

    invoke-direct {v0, v2, p1, v1}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private a(Lger;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 538
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 539
    iget-object v0, p0, Lgdj;->I:Lfqy;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lb;->c(Z)V

    .line 540
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    iget-object v0, p0, Lgdj;->I:Lfqy;

    iget-object v3, p0, Lgdj;->L:Lfrb;

    invoke-virtual {v0, v3}, Lfqy;->b(Lfrb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 542
    iget-object v0, p1, Lger;->a:[Lfqj;

    array-length v0, v0

    if-le v0, v1, :cond_0

    .line 543
    iget-object v0, p1, Lger;->f:Lgep;

    .line 544
    iget-object v3, p0, Lgdj;->x:Lggh;

    iget-object v4, p0, Lgdj;->w:Leau;

    .line 546
    iget v5, v0, Lgep;->b:I

    .line 547
    iget v0, v0, Lgep;->a:I

    .line 544
    new-instance v6, Lggj;

    invoke-direct {v6, v5, v0}, Lggj;-><init>(II)V

    invoke-virtual {v4, v3, v1, v6}, Leau;->a(Leav;ILjava/lang/Object;)V

    .line 549
    :cond_0
    iget-object v0, p1, Lger;->b:[Lfqj;

    array-length v0, v0

    if-le v0, v1, :cond_1

    .line 550
    iget-object v0, p0, Lgdj;->y:Lggf;

    iget-object v1, p0, Lgdj;->w:Leau;

    iget-object v3, p1, Lger;->g:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Leau;->a(Leav;ILjava/lang/Object;)V

    .line 553
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 538
    goto :goto_0

    :cond_3
    move v0, v2

    .line 539
    goto :goto_1
.end method

.method private a(Lggp;)V
    .locals 1

    .prologue
    .line 454
    invoke-static {p1}, La;->a(Lggp;)V

    .line 455
    iget-object v0, p0, Lgdj;->k:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Lggp;)V

    .line 456
    invoke-direct {p0}, Lgdj;->p()V

    .line 457
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 450
    new-instance v0, Lggp;

    invoke-virtual {p0}, Lgdj;->i()I

    move-result v1

    invoke-direct {v0, p1, v1, p2}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    invoke-direct {p0, v0}, Lgdj;->a(Lggp;)V

    .line 451
    return-void
.end method

.method private a([Ledm;[Ledm;ILjava/lang/String;)Z
    .locals 18

    .prologue
    .line 474
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->w:Leau;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->I:Lfqy;

    .line 475
    iget-object v3, v2, Lfqy;->a:Lhre;

    iget-object v3, v3, Lhre;->b:Lhgg;

    if-eqz v3, :cond_7

    iget-object v2, v2, Lfqy;->a:Lhre;

    iget-object v2, v2, Lhre;->b:Lhgg;

    iget-boolean v2, v2, Lhgg;->r:Z

    :goto_0
    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->C:Lfrf;

    .line 476
    iget-boolean v2, v2, Lfrf;->j:Z

    if-eqz v2, :cond_0

    .line 478
    invoke-direct/range {p0 .. p0}, Lgdj;->r()V

    .line 481
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->w:Leau;

    if-nez v2, :cond_8

    .line 483
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->d:Lgdm;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lgdj;->I:Lfqy;

    .line 484
    invoke-virtual {v4}, Lfqy;->j()I

    move-result v4

    const/16 v5, 0x1388

    .line 483
    invoke-virtual {v2, v3, v4, v5}, Lgdm;->a(III)Leau;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lgdj;->w:Leau;

    .line 485
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->w:Leau;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Leau;->a(Leaw;)V

    .line 492
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->C:Lfrf;

    iget-boolean v2, v2, Lfrf;->j:Z

    if-eqz v2, :cond_b

    .line 494
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->J:Ledz;

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->C:Lfrf;

    .line 495
    iget-boolean v4, v3, Lfrf;->j:Z

    invoke-static {v4}, Lb;->c(Z)V

    iget-object v3, v3, Lfrf;->d:Lhwn;

    iget-object v4, v3, Lhwn;->f:[Lhli;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_a

    aget-object v6, v4, v3

    iget v7, v6, Lhli;->b:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_9

    iget-object v3, v6, Lhli;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lgdj;->j:Lgfl;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgdj;->w:Leau;

    .line 497
    invoke-virtual {v5}, Leau;->a()Landroid/os/Looper;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lgdj;->q:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lgdj;->H:Ljava/lang/String;

    .line 494
    invoke-virtual/range {v2 .. v7}, Ledz;->a(Landroid/net/Uri;Lgfl;Landroid/os/Looper;Landroid/os/Handler;Ljava/lang/String;)Ledv;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    move-object v15, v2

    .line 508
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->w:Leau;

    move/from16 v0, p3

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Leau;->a(J)V

    .line 509
    new-instance v2, Leaq;

    new-instance v3, Leez;

    move-object/from16 v0, p0

    iget-object v4, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v4}, Lfqy;->g()I

    move-result v4

    shl-int/lit8 v4, v4, 0xa

    invoke-direct {v3, v4}, Leez;-><init>(I)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v6}, Lfqy;->c()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v7}, Lfqy;->d()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v8}, Lfqy;->e()F

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v9}, Lfqy;->f()F

    move-result v9

    invoke-direct/range {v2 .. v9}, Leaq;-><init>(Leex;Landroid/os/Handler;Lu;IIFF)V

    if-eqz p1, :cond_c

    move-object/from16 v0, p1

    array-length v3, v0

    if-lez v3, :cond_c

    const/4 v3, 0x0

    aget-object v3, p1, v3

    if-eqz v3, :cond_c

    const/4 v3, 0x1

    move v13, v3

    :goto_5
    if-eqz v13, :cond_d

    const/4 v3, 0x2

    :goto_6
    new-array v0, v3, [Lecc;

    move-object/from16 v16, v0

    new-instance v3, Lggf;

    move-object/from16 v0, p4

    invoke-direct {v3, v0}, Lggf;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lgdj;->y:Lggf;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->g:Lewi;

    invoke-interface {v3}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lefc;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->s:Lewi;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->s:Lewi;

    invoke-interface {v3}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v7, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->r:Legc;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->r:Legc;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v3, Lggc;

    new-instance v5, Ledb;

    invoke-direct {v5}, Ledb;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lgdj;->I:Lfqy;

    iget-object v8, v6, Lfqy;->a:Lhre;

    iget-object v8, v8, Lhre;->b:Lhgg;

    if-eqz v8, :cond_e

    iget-object v6, v6, Lfqy;->a:Lhre;

    iget-object v6, v6, Lhre;->b:Lhgg;

    iget v6, v6, Lhgg;->o:I

    :goto_7
    if-eqz v6, :cond_f

    :goto_8
    move-object/from16 v0, p0

    iget-object v8, v0, Lgdj;->h:Legq;

    move-object/from16 v0, p0

    iget-object v9, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v9}, Lfqy;->k()Z

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x1

    new-array v11, v11, [Ledm;

    const/4 v12, 0x0

    const/16 v17, 0x0

    aget-object v17, p2, v17

    aput-object v17, v11, v12

    invoke-direct/range {v3 .. v11}, Lggc;-><init>(Lefc;Lecy;ILjava/util/List;Legq;ZZ[Ledm;)V

    new-instance v4, Lecl;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgdj;->I:Lfqy;

    iget-object v6, v5, Lfqy;->a:Lhre;

    iget-object v6, v6, Lhre;->b:Lhgg;

    if-eqz v6, :cond_10

    iget-object v5, v5, Lfqy;->a:Lhre;

    iget-object v5, v5, Lhre;->b:Lhgg;

    iget v5, v5, Lhgg;->C:I

    :goto_9
    if-eqz v5, :cond_11

    :goto_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v6}, Lfqy;->g()I

    move-result v6

    mul-int/2addr v5, v6

    shl-int/lit8 v7, v5, 0xa

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lgdj;->q:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lgdj;->p:Lgdl;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgdj;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v5, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v5}, Lfqy;->s()I

    move-result v12

    move-object v5, v3

    move-object v6, v2

    invoke-direct/range {v4 .. v12}, Lecl;-><init>(Lecv;Leba;IZLandroid/os/Handler;Lecu;II)V

    new-instance v3, Lebc;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lgdj;->q:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v8, v0, Lgdj;->n:Lebe;

    move-object v5, v15

    invoke-direct/range {v3 .. v8}, Lebc;-><init>(Lebz;Ledv;ZLandroid/os/Handler;Lebe;)V

    aput-object v3, v16, v14

    if-eqz v13, :cond_5

    const/4 v3, 0x0

    aget-object v3, p2, v3

    iget-object v3, v3, Ledm;->b:Lecw;

    iget v13, v3, Lecw;->e:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->u:Lggn;

    invoke-virtual {v3}, Lggn;->b()Z

    move-result v4

    if-eqz v4, :cond_12

    iget-object v3, v3, Lggn;->a:Landroid/content/SharedPreferences;

    const-string v4, "medialib_diagnostic_cycling_format_evaluator_enabled"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_12

    const/4 v3, 0x1

    :goto_b
    if-eqz v3, :cond_13

    new-instance v3, Lgga;

    invoke-direct {v3}, Lgga;-><init>()V

    :goto_c
    move-object/from16 v0, p0

    iput-object v3, v0, Lgdj;->x:Lggh;

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v3}, Lfqy;->r()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->g:Lewi;

    invoke-interface {v3}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lefc;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->s:Lewi;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->s:Lewi;

    invoke-interface {v3}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v7, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->r:Legc;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->r:Legc;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    new-instance v3, Lggc;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgdj;->x:Lggh;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgdj;->I:Lfqy;

    iget-object v8, v6, Lfqy;->a:Lhre;

    iget-object v8, v8, Lhre;->b:Lhgg;

    if-eqz v8, :cond_20

    iget-object v6, v6, Lfqy;->a:Lhre;

    iget-object v6, v6, Lhre;->b:Lhgg;

    iget v6, v6, Lhgg;->p:I

    :goto_d
    if-eqz v6, :cond_21

    :goto_e
    move-object/from16 v0, p0

    iget-object v8, v0, Lgdj;->h:Legq;

    move-object/from16 v0, p0

    iget-object v9, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v9}, Lfqy;->k()Z

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v10}, Lfqy;->r()Z

    move-result v10

    move-object/from16 v11, p1

    invoke-direct/range {v3 .. v11}, Lggc;-><init>(Lefc;Lecy;ILjava/util/List;Legq;ZZ[Ledm;)V

    new-instance v4, Lecl;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgdj;->I:Lfqy;

    iget-object v6, v5, Lfqy;->a:Lhre;

    iget-object v6, v6, Lhre;->b:Lhgg;

    if-eqz v6, :cond_22

    iget-object v5, v5, Lfqy;->a:Lhre;

    iget-object v5, v5, Lhre;->b:Lhgg;

    iget v5, v5, Lhgg;->B:I

    :goto_f
    if-eqz v5, :cond_23

    :goto_10
    move-object/from16 v0, p0

    iget-object v6, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v6}, Lfqy;->g()I

    move-result v6

    mul-int/2addr v5, v6

    shl-int/lit8 v7, v5, 0xa

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lgdj;->q:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lgdj;->p:Lgdl;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgdj;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v5, v0, Lgdj;->I:Lfqy;

    invoke-virtual {v5}, Lfqy;->s()I

    move-result v12

    move-object v5, v3

    move-object v6, v2

    invoke-direct/range {v4 .. v12}, Lecl;-><init>(Lecv;Leba;IZLandroid/os/Handler;Lecu;II)V

    new-instance v2, Lgdk;

    const/4 v6, 0x1

    const/4 v7, 0x1

    const-wide/16 v8, 0x1388

    new-instance v10, Leca;

    move-object/from16 v0, p0

    iget-object v3, v0, Lgdj;->t:Lewi;

    invoke-interface {v3}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v5, 0x1

    invoke-direct {v10, v3, v5}, Leca;-><init>(FZ)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lgdj;->q:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v12, v0, Lgdj;->m:Lebt;

    const/4 v13, 0x1

    move-object/from16 v3, p0

    move-object v5, v15

    invoke-direct/range {v2 .. v14}, Lgdk;-><init>(Lgdj;Lebz;Ledv;ZIJLebu;Landroid/os/Handler;Lebt;IZ)V

    aput-object v2, v16, v17

    .line 514
    :cond_5
    const/4 v2, 0x0

    aget-object v2, v16, v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lgdj;->A:Lecc;

    .line 515
    move-object/from16 v0, v16

    array-length v2, v0

    const/4 v3, 0x1

    if-le v2, v3, :cond_24

    .line 516
    const/4 v2, 0x1

    aget-object v2, v16, v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lgdj;->z:Lecc;

    .line 520
    :goto_11
    move-object/from16 v0, p0

    iget v2, v0, Lgdj;->F:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lgdj;->a(F)V

    .line 521
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lgdj;->b(Z)V

    .line 523
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->w:Leau;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Leau;->a([Lecc;)V

    .line 524
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lgdj;->K:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->z:Lecc;

    if-eqz v2, :cond_6

    .line 525
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->w:Leau;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Leau;->a(IZ)V

    .line 527
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->k:Lgei;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lgei;->a(I)V

    .line 528
    const/4 v2, 0x1

    :goto_12
    return v2

    .line 475
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 487
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lgdj;->w:Leau;

    invoke-virtual {v2}, Leau;->e()V

    .line 488
    invoke-direct/range {p0 .. p0}, Lgdj;->s()V

    goto/16 :goto_1

    .line 495
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 501
    :catch_0
    move-exception v2

    const-string v2, "drm.missingapi"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lgdj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 502
    const/4 v2, 0x0

    goto :goto_12

    .line 505
    :cond_b
    const/4 v2, 0x0

    move-object v15, v2

    goto/16 :goto_4

    .line 509
    :cond_c
    const/4 v3, 0x0

    move v13, v3

    goto/16 :goto_5

    :cond_d
    const/4 v3, 0x1

    goto/16 :goto_6

    :cond_e
    const/4 v6, 0x0

    goto/16 :goto_7

    :cond_f
    const/4 v6, 0x1

    goto/16 :goto_8

    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_9

    :cond_11
    const/16 v5, 0x64

    goto/16 :goto_a

    :cond_12
    const/4 v3, 0x0

    goto/16 :goto_b

    :cond_13
    new-instance v3, Lggg;

    move-object/from16 v0, p0

    iget-object v4, v0, Lgdj;->e:Lexd;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgdj;->i:Leey;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgdj;->I:Lfqy;

    iget-object v7, v6, Lfqy;->a:Lhre;

    iget-object v7, v7, Lhre;->b:Lhgg;

    if-eqz v7, :cond_14

    iget-object v6, v6, Lfqy;->a:Lhre;

    iget-object v6, v6, Lhre;->b:Lhgg;

    iget v6, v6, Lhgg;->c:I

    mul-int/lit8 v6, v6, 0x8

    :goto_13
    if-eqz v6, :cond_15

    :goto_14
    move-object/from16 v0, p0

    iget-object v7, v0, Lgdj;->I:Lfqy;

    iget-object v8, v7, Lfqy;->a:Lhre;

    iget-object v8, v8, Lhre;->b:Lhgg;

    if-eqz v8, :cond_16

    iget-object v7, v7, Lfqy;->a:Lhre;

    iget-object v7, v7, Lhre;->b:Lhgg;

    iget v7, v7, Lhgg;->d:I

    :goto_15
    if-eqz v7, :cond_17

    :goto_16
    move-object/from16 v0, p0

    iget-object v8, v0, Lgdj;->I:Lfqy;

    iget-object v9, v8, Lfqy;->a:Lhre;

    iget-object v9, v9, Lhre;->b:Lhgg;

    if-eqz v9, :cond_18

    iget-object v8, v8, Lfqy;->a:Lhre;

    iget-object v8, v8, Lhre;->b:Lhgg;

    iget v8, v8, Lhgg;->e:I

    :goto_17
    if-eqz v8, :cond_19

    :goto_18
    move-object/from16 v0, p0

    iget-object v9, v0, Lgdj;->I:Lfqy;

    iget-object v10, v9, Lfqy;->a:Lhre;

    iget-object v10, v10, Lhre;->b:Lhgg;

    if-eqz v10, :cond_1a

    iget-object v9, v9, Lfqy;->a:Lhre;

    iget-object v9, v9, Lhre;->b:Lhgg;

    iget v9, v9, Lhgg;->f:I

    :goto_19
    if-eqz v9, :cond_1b

    :goto_1a
    move-object/from16 v0, p0

    iget-object v10, v0, Lgdj;->I:Lfqy;

    iget-object v11, v10, Lfqy;->a:Lhre;

    iget-object v11, v11, Lhre;->b:Lhgg;

    if-eqz v11, :cond_1c

    iget-object v10, v10, Lfqy;->a:Lhre;

    iget-object v10, v10, Lhre;->b:Lhgg;

    iget v10, v10, Lhgg;->k:F

    :goto_1b
    const/4 v11, 0x0

    cmpl-float v11, v10, v11

    if-eqz v11, :cond_1d

    :goto_1c
    const/16 v11, 0x90

    move-object/from16 v0, p0

    iget-object v12, v0, Lgdj;->C:Lfrf;

    iget-boolean v12, v12, Lfrf;->j:Z

    if-eqz v12, :cond_1e

    const/16 v12, 0x1e0

    :goto_1d
    move-object/from16 v0, p0

    iget-object v14, v0, Lgdj;->I:Lfqy;

    iget-object v0, v14, Lfqy;->a:Lhre;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lhre;->b:Lhgg;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1f

    iget-object v14, v14, Lfqy;->a:Lhre;

    iget-object v14, v14, Lhre;->b:Lhgg;

    iget-boolean v14, v14, Lhgg;->s:Z

    :goto_1e
    invoke-direct/range {v3 .. v14}, Lggg;-><init>(Lexd;Leey;IIIIFIIIZ)V

    goto/16 :goto_c

    :cond_14
    const/4 v6, 0x0

    goto :goto_13

    :cond_15
    const v6, 0xc3500

    goto :goto_14

    :cond_16
    const/4 v7, 0x0

    goto :goto_15

    :cond_17
    const/16 v7, 0x2710

    goto :goto_16

    :cond_18
    const/4 v8, 0x0

    goto :goto_17

    :cond_19
    const/16 v8, 0x61a8

    goto :goto_18

    :cond_1a
    const/4 v9, 0x0

    goto :goto_19

    :cond_1b
    const/16 v9, 0x61a8

    goto :goto_1a

    :cond_1c
    const/4 v10, 0x0

    goto :goto_1b

    :cond_1d
    const/high16 v10, 0x3f400000    # 0.75f

    goto :goto_1c

    :cond_1e
    const v12, 0x7fffffff

    goto :goto_1d

    :cond_1f
    const/4 v14, 0x0

    goto :goto_1e

    :cond_20
    const/4 v6, 0x0

    goto/16 :goto_d

    :cond_21
    const/4 v6, 0x1

    goto/16 :goto_e

    :cond_22
    const/4 v5, 0x0

    goto/16 :goto_f

    :cond_23
    const/16 v5, 0x104

    goto/16 :goto_10

    .line 518
    :cond_24
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lgdj;->z:Lecc;

    goto/16 :goto_11
.end method

.method private static a([Lfqj;Ljava/lang/String;I)[Ledm;
    .locals 4

    .prologue
    .line 792
    array-length v0, p0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 793
    new-array v2, v1, [Ledm;

    .line 794
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 795
    aget-object v3, p0, v0

    invoke-virtual {v3, p1}, Lfqj;->b(Ljava/lang/String;)Ledm;

    move-result-object v3

    aput-object v3, v2, v0

    .line 794
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 797
    :cond_0
    return-object v2
.end method

.method private a(Lfrf;Ljava/util/Set;)[Lfre;
    .locals 9

    .prologue
    .line 764
    :try_start_0
    iget-object v0, p0, Lgdj;->o:Lges;

    new-instance v1, Lfqy;

    invoke-direct {v1}, Lfqy;-><init>()V

    .line 766
    iget-object v2, p1, Lfrf;->c:Ljava/util/List;

    .line 768
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p2

    .line 764
    invoke-interface/range {v0 .. v8}, Lges;->a(Lfqy;Ljava/util/Collection;Ljava/util/Set;Ljava/util/Set;ZZZZ)Lger;

    move-result-object v0

    .line 773
    iget-object v0, v0, Lger;->d:[Lfre;
    :try_end_0
    .catch Lgej; {:try_start_0 .. :try_end_0} :catch_0

    .line 775
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lges;->b:[Lfre;

    goto :goto_0
.end method

.method static synthetic b(Lgdj;I)I
    .locals 0

    .prologue
    .line 87
    iput p1, p0, Lgdj;->E:I

    return p1
.end method

.method private b(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 908
    iget-object v1, p0, Lgdj;->w:Leau;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgdj;->z:Lecc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgdj;->v:Lgec;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgdj;->v:Lgec;

    .line 911
    invoke-interface {v1}, Lgec;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lgdj;->B:Z

    if-nez v1, :cond_0

    .line 913
    iget-object v1, p0, Lgdj;->x:Lggh;

    iget-object v2, p0, Lgdj;->w:Leau;

    iget-object v3, p0, Lgdj;->v:Lgec;

    .line 915
    invoke-interface {v3}, Lgec;->a()I

    move-result v3

    iget-object v4, p0, Lgdj;->v:Lgec;

    .line 916
    invoke-interface {v4}, Lgec;->b()I

    move-result v4

    .line 913
    invoke-virtual {v1, v2, v3, v4}, Lggh;->a(Leau;II)V

    .line 917
    if-eqz p1, :cond_2

    .line 918
    iget-object v1, p0, Lgdj;->w:Leau;

    iget-object v2, p0, Lgdj;->z:Lecc;

    iget-object v3, p0, Lgdj;->v:Lgec;

    .line 921
    invoke-interface {v3}, Lgec;->e()Landroid/view/Surface;

    move-result-object v3

    .line 918
    invoke-virtual {v1, v2, v0, v3}, Leau;->b(Leav;ILjava/lang/Object;)V

    .line 928
    :goto_0
    iget-object v1, p0, Lgdj;->w:Leau;

    invoke-virtual {v1, v0, v0}, Leau;->a(IZ)V

    .line 929
    iput-boolean v0, p0, Lgdj;->B:Z

    .line 931
    :cond_0
    iget-object v1, p0, Lgdj;->k:Lgei;

    iget-object v2, p0, Lgdj;->v:Lgec;

    if-eqz v2, :cond_1

    iget-object v0, p0, Lgdj;->v:Lgec;

    .line 932
    invoke-interface {v0}, Lgec;->i()I

    move-result v0

    .line 931
    :cond_1
    invoke-interface {v1, v0}, Lgei;->d(I)V

    .line 933
    invoke-direct {p0}, Lgdj;->u()V

    .line 934
    return-void

    .line 923
    :cond_2
    iget-object v1, p0, Lgdj;->w:Leau;

    iget-object v2, p0, Lgdj;->z:Lecc;

    iget-object v3, p0, Lgdj;->v:Lgec;

    .line 926
    invoke-interface {v3}, Lgec;->e()Landroid/view/Surface;

    move-result-object v3

    .line 923
    invoke-virtual {v1, v2, v0, v3}, Leau;->a(Leav;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic b(Lgdj;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lgdj;->K:Z

    return v0
.end method

.method static synthetic c(Lgdj;)Lgei;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lgdj;->k:Lgei;

    return-object v0
.end method

.method static synthetic d(Lgdj;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lgdj;->B:Z

    return v0
.end method

.method static synthetic e(Lgdj;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgdj;->b(Z)V

    return-void
.end method

.method static synthetic f(Lgdj;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgdj;->b(Z)V

    return-void
.end method

.method static synthetic g(Lgdj;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lgdj;->D:I

    return v0
.end method

.method static synthetic h(Lgdj;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lgdj;->E:I

    return v0
.end method

.method static synthetic i(Lgdj;)Lgec;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lgdj;->v:Lgec;

    return-object v0
.end method

.method static synthetic j(Lgdj;)Lggh;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lgdj;->x:Lggh;

    return-object v0
.end method

.method static synthetic k(Lgdj;)Leau;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lgdj;->w:Leau;

    return-object v0
.end method

.method static synthetic l(Lgdj;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lgdj;->w()V

    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 460
    iput-object v1, p0, Lgdj;->z:Lecc;

    .line 461
    iput-object v1, p0, Lgdj;->A:Lecc;

    .line 462
    iput-object v1, p0, Lgdj;->C:Lfrf;

    .line 463
    iget-object v0, p0, Lgdj;->p:Lgdl;

    iput-object v1, v0, Lgdl;->a:Lger;

    iput-object v1, v0, Lgdl;->b:Lfqj;

    iput-object v1, v0, Lgdl;->c:Lfqj;

    .line 464
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 812
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    .line 813
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgdj;->b(Z)V

    .line 814
    iget-object v0, p0, Lgdj;->k:Lgei;

    new-instance v1, Lgct;

    invoke-direct {v1}, Lgct;-><init>()V

    invoke-interface {v0, v1}, Lgei;->a(Lgcs;)V

    .line 815
    iget-object v0, p0, Lgdj;->w:Leau;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leau;->a(Z)V

    .line 817
    :cond_0
    return-void
.end method

.method private r()V
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    .line 873
    invoke-direct {p0}, Lgdj;->s()V

    .line 874
    iget-object v0, p0, Lgdj;->w:Leau;

    invoke-virtual {v0}, Leau;->f()V

    .line 875
    const/4 v0, 0x0

    iput-object v0, p0, Lgdj;->w:Leau;

    .line 877
    :cond_0
    return-void
.end method

.method private s()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 880
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->z:Lecc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->v:Lgec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->v:Lgec;

    .line 883
    invoke-interface {v0}, Lgec;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgdj;->B:Z

    if-eqz v0, :cond_0

    .line 885
    iget-object v0, p0, Lgdj;->w:Leau;

    iget-object v1, p0, Lgdj;->z:Lecc;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Leau;->a(Leav;ILjava/lang/Object;)V

    .line 888
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgdj;->B:Z

    .line 889
    invoke-direct {p0}, Lgdj;->u()V

    .line 890
    iget-object v0, p0, Lgdj;->k:Lgei;

    invoke-interface {v0, v3}, Lgei;->d(I)V

    .line 891
    return-void
.end method

.method private t()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 947
    iget-object v2, p0, Lgdj;->w:Leau;

    invoke-virtual {v2}, Leau;->b()I

    move-result v3

    .line 948
    iget-object v2, p0, Lgdj;->w:Leau;

    invoke-virtual {v2}, Leau;->c()Z

    move-result v4

    .line 949
    if-ne v3, v6, :cond_3

    move v2, v0

    .line 950
    :goto_0
    const/4 v5, 0x4

    if-ne v3, v5, :cond_4

    if-eqz v4, :cond_4

    iget-object v3, p0, Lgdj;->w:Leau;

    .line 952
    invoke-virtual {v3}, Leau;->d()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 954
    :goto_1
    iget-boolean v1, p0, Lgdj;->O:Z

    if-eq v1, v2, :cond_0

    .line 955
    iget-object v3, p0, Lgdj;->k:Lgei;

    if-eqz v2, :cond_5

    const/4 v1, 0x5

    :goto_2
    invoke-interface {v3, v1}, Lgei;->a(I)V

    .line 957
    iput-boolean v2, p0, Lgdj;->O:Z

    .line 959
    :cond_0
    iget-boolean v1, p0, Lgdj;->N:Z

    if-eq v1, v0, :cond_2

    .line 960
    if-eqz v0, :cond_6

    .line 961
    iget-object v1, p0, Lgdj;->k:Lgei;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lgei;->a(I)V

    .line 965
    :cond_1
    :goto_3
    iput-boolean v0, p0, Lgdj;->N:Z

    .line 967
    :cond_2
    invoke-direct {p0}, Lgdj;->u()V

    .line 968
    return-void

    :cond_3
    move v2, v1

    .line 949
    goto :goto_0

    :cond_4
    move v0, v1

    .line 952
    goto :goto_1

    .line 955
    :cond_5
    const/4 v1, 0x6

    goto :goto_2

    .line 962
    :cond_6
    if-nez v4, :cond_1

    .line 963
    iget-object v1, p0, Lgdj;->k:Lgei;

    invoke-interface {v1, v6}, Lgei;->a(I)V

    goto :goto_3
.end method

.method private u()V
    .locals 3

    .prologue
    .line 971
    iget-object v0, p0, Lgdj;->v:Lgec;

    if-eqz v0, :cond_0

    .line 972
    iget-boolean v0, p0, Lgdj;->B:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lgdj;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 973
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "stayAwake: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->d(Ljava/lang/String;)V

    .line 974
    iget-object v1, p0, Lgdj;->v:Lgec;

    invoke-interface {v1, v0}, Lgec;->a(Z)V

    .line 976
    :cond_0
    return-void

    .line 972
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1139
    iget-object v0, p0, Lgdj;->C:Lfrf;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgdj;->C:Lfrf;

    .line 1140
    iget-boolean v0, v0, Lfrf;->j:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 1141
    :goto_0
    iget-object v3, p0, Lgdj;->I:Lfqy;

    if-eqz v3, :cond_5

    iget-object v4, p0, Lgdj;->I:Lfqy;

    iget-object v3, p0, Lgdj;->M:Lfrc;

    .line 1142
    if-nez v3, :cond_0

    sget-object v3, Lfrc;->d:Lfrc;

    :cond_0
    sget-object v5, Lfra;->b:[I

    invoke-virtual {v3}, Lfrc;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_0

    iget-object v3, v4, Lfqy;->a:Lhre;

    iget-object v3, v3, Lhre;->b:Lhgg;

    if-eqz v3, :cond_4

    iget-object v3, v4, Lfqy;->a:Lhre;

    iget-object v3, v3, Lhre;->b:Lhgg;

    iget-boolean v3, v3, Lhgg;->A:Z

    .line 1143
    :goto_1
    iget-object v4, p0, Lgdj;->I:Lfqy;

    if-eqz v4, :cond_8

    iget-object v5, p0, Lgdj;->I:Lfqy;

    iget-object v4, p0, Lgdj;->M:Lfrc;

    .line 1144
    if-nez v4, :cond_1

    sget-object v4, Lfrc;->d:Lfrc;

    :cond_1
    sget-object v6, Lfra;->b:[I

    invoke-virtual {v4}, Lfrc;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_1

    if-eqz v0, :cond_7

    iget-object v0, v5, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_2

    iget-object v0, v5, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget-boolean v0, v0, Lhgg;->A:Z

    if-nez v0, :cond_7

    .line 1146
    :cond_2
    :goto_2
    :pswitch_0
    if-eqz v3, :cond_9

    .line 1147
    const/4 v0, 0x2

    .line 1151
    :goto_3
    return v0

    :cond_3
    move v0, v2

    .line 1140
    goto :goto_0

    :pswitch_1
    move v3, v2

    .line 1142
    goto :goto_1

    :pswitch_2
    move v3, v1

    goto :goto_1

    :cond_4
    move v3, v1

    goto :goto_1

    :cond_5
    if-nez v0, :cond_6

    move v3, v1

    goto :goto_1

    :cond_6
    move v3, v2

    goto :goto_1

    :pswitch_3
    move v1, v2

    .line 1144
    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_2

    :cond_8
    move v1, v0

    goto :goto_2

    .line 1148
    :cond_9
    if-eqz v1, :cond_a

    .line 1149
    const/4 v0, 0x4

    goto :goto_3

    .line 1151
    :cond_a
    const/4 v0, 0x3

    goto :goto_3

    .line 1142
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1144
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private w()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1170
    iget-object v0, p0, Lgdj;->v:Lgec;

    if-eqz v0, :cond_0

    .line 1171
    iget-object v0, p0, Lgdj;->v:Lgec;

    invoke-interface {v0, v1}, Lgec;->a(Lged;)V

    .line 1172
    invoke-direct {p0}, Lgdj;->s()V

    .line 1173
    iput-object v1, p0, Lgdj;->v:Lgec;

    .line 1175
    :cond_0
    return-void
.end method

.method private static x()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1264
    sget-object v0, Lgdj;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 1265
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lgdj;->c:Ljava/lang/Boolean;

    .line 1266
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_1

    new-instance v0, Landroid/media/MediaCodecList;

    invoke-direct {v0, v1}, Landroid/media/MediaCodecList;-><init>(I)V

    invoke-virtual {v0}, Landroid/media/MediaCodecList;->getCodecInfos()[Landroid/media/MediaCodecInfo;

    move-result-object v0

    :goto_0
    array-length v2, v0

    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 1267
    invoke-virtual {v3}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "vp9.decode"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1268
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lgdj;->c:Ljava/lang/Boolean;

    .line 1273
    :cond_0
    sget-object v0, Lgdj;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 1266
    :cond_1
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v0

    new-array v2, v0, [Landroid/media/MediaCodecInfo;

    move v0, v1

    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_2

    invoke-static {v0}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move-object v0, v2

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1105
    invoke-direct {p0}, Lgdj;->t()V

    .line 1106
    return-void
.end method

.method public final a(F)V
    .locals 4

    .prologue
    .line 995
    iput p1, p0, Lgdj;->F:F

    .line 996
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->A:Lecc;

    if-eqz v0, :cond_0

    .line 997
    iget-object v0, p0, Lgdj;->w:Leau;

    iget-object v1, p0, Lgdj;->A:Lecc;

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Leau;->a(Leav;ILjava/lang/Object;)V

    .line 999
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v1, 0x5

    const/4 v2, 0x0

    .line 1018
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    if-ne p1, v1, :cond_1

    .line 1019
    :cond_0
    iput-boolean v2, p0, Lgdj;->G:Z

    .line 1021
    :cond_1
    if-ne p1, v1, :cond_2

    .line 1022
    iget-object v0, p0, Lgdj;->k:Lgei;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lgei;->a(I)V

    .line 1023
    iput-boolean v2, p0, Lgdj;->N:Z

    iput-boolean v2, p0, Lgdj;->O:Z

    invoke-direct {p0}, Lgdj;->u()V

    .line 1027
    :goto_0
    return-void

    .line 1025
    :cond_2
    invoke-direct {p0}, Lgdj;->t()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lgdj;->k:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Landroid/os/Handler;)V

    .line 227
    return-void
.end method

.method public final a(Leat;)V
    .locals 2

    .prologue
    .line 1032
    invoke-virtual {p1}, Leat;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lgfq;

    if-eqz v0, :cond_0

    .line 1037
    :goto_0
    return-void

    .line 1035
    :cond_0
    iget-object v0, p0, Lgdj;->k:Lgei;

    .line 1036
    invoke-virtual {p0}, Lgdj;->i()I

    move-result v1

    invoke-static {p1, v1}, Lgdj;->a(Leat;I)Lggp;

    move-result-object v1

    .line 1035
    invoke-interface {v0, v1}, Lgei;->a(Lggp;)V

    goto :goto_0
.end method

.method public final a(Lfrf;ILjava/lang/String;Lfqy;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 275
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgdj;->C:Lfrf;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgdj;->C:Lfrf;

    .line 280
    iget-object v0, v0, Lfrf;->e:Ljava/lang/String;

    iget-object v1, p1, Lfrf;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    invoke-virtual {p0, p2}, Lgdj;->b(I)V

    .line 283
    iget-object v0, p0, Lgdj;->v:Lgec;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lgdj;->v:Lgec;

    invoke-interface {v0, v7}, Lgec;->a(I)V

    .line 287
    :cond_0
    invoke-virtual {p0}, Lgdj;->m()Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    invoke-direct {p0}, Lgdj;->q()V

    .line 352
    :cond_1
    :goto_0
    return-void

    .line 293
    :cond_2
    invoke-direct {p0}, Lgdj;->p()V

    .line 294
    iput-object p1, p0, Lgdj;->C:Lfrf;

    .line 295
    iput-object p3, p0, Lgdj;->H:Ljava/lang/String;

    .line 296
    iput-object p4, p0, Lgdj;->I:Lfqy;

    .line 297
    iget-object v0, p0, Lgdj;->u:Lggn;

    invoke-virtual {v0}, Lggn;->a()Lfrb;

    move-result-object v0

    iput-object v0, p0, Lgdj;->L:Lfrb;

    .line 298
    iget-object v1, p0, Lgdj;->u:Lggn;

    const-string v2, "media_view_activation_type"

    const-class v3, Lfrc;

    sget-object v4, Lfrc;->d:Lfrc;

    iget-boolean v0, v1, Lggn;->b:Z

    if-nez v0, :cond_6

    move v0, v6

    :goto_1
    invoke-virtual {v1, v2, v3, v4, v0}, Lggn;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;Z)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfrc;

    iput-object v0, p0, Lgdj;->M:Lfrc;

    .line 299
    iget-object v0, p0, Lgdj;->J:Ledz;

    iget-object v1, p0, Lgdj;->I:Lfqy;

    invoke-virtual {v0, v1}, Ledz;->a(Lfqy;)V

    .line 300
    iget-object v0, p0, Lgdj;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 301
    iput v7, p0, Lgdj;->D:I

    .line 302
    iput v7, p0, Lgdj;->E:I

    .line 303
    iput-boolean v7, p0, Lgdj;->G:Z

    .line 304
    iget-object v0, p0, Lgdj;->v:Lgec;

    if-eqz v0, :cond_3

    .line 305
    iget-object v0, p0, Lgdj;->v:Lgec;

    invoke-interface {v0}, Lgec;->c()V

    .line 309
    :cond_3
    iget-boolean v0, p0, Lgdj;->K:Z

    iget-object v1, p0, Lgdj;->C:Lfrf;

    invoke-virtual {v1}, Lfrf;->c()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lgdj;->K:Z

    .line 313
    :try_start_0
    iget-object v1, p0, Lgdj;->C:Lfrf;

    iget-object v2, p0, Lgdj;->I:Lfqy;

    iget-object v3, p0, Lgdj;->L:Lfrb;

    iget-boolean v4, p0, Lgdj;->K:Z

    iget-object v0, p0, Lgdj;->J:Ledz;

    .line 318
    invoke-virtual {v0}, Ledz;->c()Z

    move-result v5

    move-object v0, p0

    .line 313
    invoke-direct/range {v0 .. v5}, Lgdj;->a(Lfrf;Lfqy;Lfrb;ZZ)Lger;
    :try_end_0
    .catch Lgej; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 323
    iget-object v1, p0, Lgdj;->p:Lgdl;

    iput-object v0, v1, Lgdl;->a:Lger;

    .line 326
    :try_start_1
    iget-object v1, p0, Lgdj;->v:Lgec;

    if-eqz v1, :cond_4

    .line 327
    iget-object v1, p0, Lgdj;->v:Lgec;

    invoke-direct {p0}, Lgdj;->v()I

    move-result v2

    invoke-interface {v1, v2}, Lgec;->b(I)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 334
    :cond_4
    iget-object v1, v0, Lger;->a:[Lfqj;

    .line 335
    iget-object v2, v0, Lger;->b:[Lfqj;

    .line 336
    iget-object v3, p0, Lgdj;->k:Lgei;

    invoke-interface {v3, v7}, Lgei;->a(I)V

    .line 337
    iget-object v3, p0, Lgdj;->I:Lfqy;

    iget-object v4, p0, Lgdj;->L:Lfrb;

    .line 338
    invoke-virtual {v3, v4}, Lfqy;->b(Lfrb;)Z

    move-result v3

    if-eqz v3, :cond_5

    const v6, 0x7fffffff

    .line 339
    :cond_5
    iget-object v3, p0, Lgdj;->H:Ljava/lang/String;

    .line 340
    invoke-static {v1, v3, v6}, Lgdj;->a([Lfqj;Ljava/lang/String;I)[Ledm;

    move-result-object v1

    .line 341
    iget-object v3, p0, Lgdj;->H:Ljava/lang/String;

    .line 342
    invoke-static {v2, v3, v6}, Lgdj;->a([Lfqj;Ljava/lang/String;I)[Ledm;

    move-result-object v2

    .line 348
    iget-object v3, v0, Lger;->g:Ljava/lang/String;

    .line 344
    invoke-direct {p0, v1, v2, p2, v3}, Lgdj;->a([Ledm;[Ledm;ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 349
    invoke-direct {p0, v0}, Lgdj;->a(Lger;)V

    .line 350
    invoke-direct {p0}, Lgdj;->q()V

    goto/16 :goto_0

    :cond_6
    move v0, v7

    .line 298
    goto/16 :goto_1

    .line 319
    :catch_0
    move-exception v0

    .line 320
    const-string v1, "fmt.noneavailable"

    invoke-direct {p0, v1, v0}, Lgdj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 329
    :catch_1
    move-exception v0

    .line 330
    const-string v1, "android.exo"

    invoke-direct {p0, v1, v0}, Lgdj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method public final a(Lgec;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1110
    iput-boolean v1, p0, Lgdj;->K:Z

    .line 1112
    invoke-direct {p0}, Lgdj;->w()V

    .line 1113
    iput-object p1, p0, Lgdj;->v:Lgec;

    .line 1114
    iget-object v0, p0, Lgdj;->l:Lgdp;

    invoke-interface {p1, v0}, Lgec;->a(Lged;)V

    .line 1116
    if-eqz p1, :cond_0

    .line 1117
    :try_start_0
    invoke-direct {p0}, Lgdj;->v()I

    move-result v0

    invoke-interface {p1, v0}, Lgec;->b(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1135
    :cond_0
    invoke-direct {p0, v1}, Lgdj;->b(Z)V

    .line 1136
    :goto_0
    return-void

    .line 1119
    :catch_0
    move-exception v0

    .line 1120
    invoke-virtual {p0}, Lgdj;->h()V

    .line 1121
    iget-object v1, p0, Lgdj;->k:Lgei;

    new-instance v2, Lggp;

    const-string v3, "android.exo"

    .line 1123
    invoke-virtual {p0}, Lgdj;->i()I

    move-result v4

    invoke-direct {v2, v3, v4, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1121
    invoke-interface {v1, v2}, Lgei;->a(Lggp;)V

    goto :goto_0

    .line 1126
    :catch_1
    move-exception v0

    .line 1128
    invoke-virtual {p0}, Lgdj;->h()V

    .line 1129
    iget-object v1, p0, Lgdj;->k:Lgei;

    new-instance v2, Lggp;

    const-string v3, "android.exo"

    .line 1131
    invoke-virtual {p0}, Lgdj;->i()I

    move-result v4

    invoke-direct {v2, v3, v4, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1129
    invoke-interface {v1, v2}, Lgei;->a(Lggp;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 864
    invoke-direct {p0}, Lgdj;->r()V

    .line 865
    if-eqz p1, :cond_0

    iget-object v0, p0, Lgdj;->v:Lgec;

    if-eqz v0, :cond_0

    .line 866
    iget-object v0, p0, Lgdj;->v:Lgec;

    invoke-interface {v0}, Lgec;->g()V

    .line 867
    const/4 v0, 0x0

    iput-object v0, p0, Lgdj;->v:Lgec;

    .line 869
    :cond_0
    return-void
.end method

.method public final a(Lfrf;Lfqy;Z)[Lfqj;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 357
    iget-object v0, p0, Lgdj;->u:Lggn;

    invoke-virtual {v0}, Lggn;->a()Lfrb;

    move-result-object v0

    .line 358
    invoke-virtual {p2, v0}, Lfqy;->b(Lfrb;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    sget-object v0, Lges;->a:[Lfqj;

    .line 372
    :goto_0
    return-object v0

    .line 366
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lgdj;->b(Lfrf;Lfqy;Z)Lger;
    :try_end_0
    .catch Lgej; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 370
    iget-object v1, v0, Lger;->a:[Lfqj;

    .line 371
    iget-object v2, v0, Lger;->b:[Lfqj;

    .line 372
    if-nez p3, :cond_1

    array-length v0, v1

    if-lez v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [Lfqj;

    aget-object v2, v2, v3

    aput-object v2, v0, v3

    aget-object v1, v1, v3

    aput-object v1, v0, v4

    goto :goto_0

    .line 368
    :catch_0
    move-exception v0

    sget-object v0, Lges;->a:[Lfqj;

    goto :goto_0

    .line 372
    :cond_1
    new-array v0, v4, [Lfqj;

    aget-object v1, v2, v3

    aput-object v1, v0, v3

    goto :goto_0
.end method

.method public final b(Lfrf;Lfqy;Z)Lger;
    .locals 6

    .prologue
    .line 381
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    iget-object v0, p0, Lgdj;->u:Lggn;

    .line 385
    invoke-virtual {v0}, Lggn;->a()Lfrb;

    move-result-object v3

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    .line 382
    invoke-direct/range {v0 .. v5}, Lgdj;->a(Lfrf;Lfqy;Lfrb;ZZ)Lger;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 392
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->C:Lfrf;

    if-nez v0, :cond_1

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    iget-object v0, p0, Lgdj;->z:Lecc;

    if-eqz v0, :cond_0

    .line 402
    :try_start_0
    iget-object v1, p0, Lgdj;->C:Lfrf;

    iget-object v2, p0, Lgdj;->I:Lfqy;

    iget-object v3, p0, Lgdj;->L:Lfrb;

    iget-boolean v4, p0, Lgdj;->K:Z

    iget-object v0, p0, Lgdj;->J:Ledz;

    .line 407
    invoke-virtual {v0}, Ledz;->c()Z

    move-result v5

    move-object v0, p0

    .line 402
    invoke-direct/range {v0 .. v5}, Lgdj;->a(Lfrf;Lfqy;Lfrb;ZZ)Lger;
    :try_end_0
    .catch Lgej; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 412
    iget-object v1, p0, Lgdj;->I:Lfqy;

    iget-object v2, p0, Lgdj;->L:Lfrb;

    invoke-virtual {v1, v2}, Lfqy;->b(Lfrb;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 413
    invoke-direct {p0, v0}, Lgdj;->a(Lger;)V

    goto :goto_0

    .line 417
    :cond_2
    iget-object v1, p0, Lgdj;->p:Lgdl;

    iput-object v0, v1, Lgdl;->a:Lger;

    .line 418
    iget-object v1, v0, Lger;->a:[Lfqj;

    aget-object v1, v1, v6

    .line 419
    iget-object v2, v0, Lger;->b:[Lfqj;

    aget-object v2, v2, v6

    .line 420
    iget-object v3, p0, Lgdj;->p:Lgdl;

    iget-object v3, v3, Lgdl;->b:Lfqj;

    invoke-virtual {v1, v3}, Lfqj;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 424
    new-array v3, v7, [Ledm;

    iget-object v4, p0, Lgdj;->H:Ljava/lang/String;

    .line 425
    invoke-virtual {v1, v4}, Lfqj;->b(Ljava/lang/String;)Ledm;

    move-result-object v1

    aput-object v1, v3, v6

    .line 426
    new-array v1, v7, [Ledm;

    iget-object v4, p0, Lgdj;->H:Ljava/lang/String;

    .line 427
    invoke-virtual {v2, v4}, Lfqj;->b(Ljava/lang/String;)Ledm;

    move-result-object v2

    aput-object v2, v1, v6

    .line 429
    iget-object v2, p0, Lgdj;->k:Lgei;

    invoke-interface {v2}, Lgei;->b()V

    .line 433
    invoke-virtual {p0}, Lgdj;->i()I

    move-result v2

    .line 434
    iget-object v0, v0, Lger;->g:Ljava/lang/String;

    .line 430
    invoke-direct {p0, v3, v1, v2, v0}, Lgdj;->a([Ledm;[Ledm;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    invoke-direct {p0}, Lgdj;->q()V

    goto :goto_0

    .line 409
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 828
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lgdj;->k:Lgei;

    invoke-interface {v0, p1}, Lgei;->b(I)V

    .line 830
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdj;->G:Z

    .line 831
    iget-object v0, p0, Lgdj;->w:Leau;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Leau;->a(J)V

    .line 832
    iget-object v0, p0, Lgdj;->k:Lgei;

    invoke-interface {v0, p1}, Lgei;->c(I)V

    .line 834
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lgdj;->k:Lgei;

    invoke-interface {v0, p1}, Lgei;->b(Landroid/os/Handler;)V

    .line 232
    return-void
.end method

.method public final c()Lfqj;
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lgdj;->p:Lgdl;

    iget-object v0, v0, Lgdl;->b:Lfqj;

    return-object v0
.end method

.method public final d()Lfqj;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lgdj;->p:Lgdl;

    iget-object v0, v0, Lgdl;->c:Lfqj;

    return-object v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 805
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->w:Leau;

    invoke-virtual {v0}, Leau;->b()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lgdj;->G:Z

    if-nez v0, :cond_0

    .line 806
    iget-object v0, p0, Lgdj;->w:Leau;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Leau;->a(J)V

    .line 808
    :cond_0
    invoke-direct {p0}, Lgdj;->q()V

    .line 809
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 821
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    .line 822
    iget-object v0, p0, Lgdj;->w:Leau;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leau;->a(Z)V

    .line 824
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 842
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    .line 843
    iget-object v0, p0, Lgdj;->w:Leau;

    invoke-virtual {v0}, Leau;->e()V

    .line 844
    invoke-direct {p0}, Lgdj;->p()V

    .line 845
    iget-object v0, p0, Lgdj;->k:Lgei;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lgei;->a(I)V

    .line 847
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 854
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    .line 855
    iget-object v0, p0, Lgdj;->w:Leau;

    invoke-virtual {v0}, Leau;->e()V

    .line 856
    invoke-direct {p0}, Lgdj;->r()V

    .line 857
    invoke-direct {p0}, Lgdj;->p()V

    .line 858
    iget-object v0, p0, Lgdj;->k:Lgei;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lgei;->a(I)V

    .line 860
    :cond_0
    return-void
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 980
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->w:Leau;

    invoke-virtual {v0}, Leau;->h()J

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 985
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->w:Leau;

    invoke-virtual {v0}, Leau;->g()J

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 990
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->w:Leau;

    invoke-virtual {v0}, Leau;->i()J

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1003
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgdj;->O:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1009
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lgdj;->N:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgdj;->O:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgdj;->w:Leau;

    .line 1011
    invoke-virtual {v0}, Leau;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 1179
    iget-object v0, p0, Lgdj;->v:Lgec;

    if-eqz v0, :cond_0

    .line 1180
    iget-object v0, p0, Lgdj;->v:Lgec;

    invoke-interface {v0}, Lgec;->c()V

    .line 1182
    :cond_0
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1157
    iput-boolean v2, p0, Lgdj;->K:Z

    .line 1160
    iget-object v0, p0, Lgdj;->w:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->z:Lecc;

    if-eqz v0, :cond_0

    .line 1161
    iget-object v0, p0, Lgdj;->w:Leau;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Leau;->a(IZ)V

    .line 1163
    :cond_0
    invoke-direct {p0}, Lgdj;->w()V

    .line 1164
    return-void
.end method

.method public final onDrmError(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 1186
    invoke-virtual {p0}, Lgdj;->g()V

    .line 1187
    instance-of v0, p1, Lgfq;

    if-eqz v0, :cond_4

    .line 1188
    check-cast p1, Lgfq;

    .line 1191
    invoke-virtual {p0}, Lgdj;->i()I

    move-result v1

    .line 1189
    iget-object v2, p1, Lgfq;->b:Lgfp;

    if-eqz v2, :cond_0

    new-instance v0, Lggp;

    const-string v3, "drm.auth"

    iget v2, v2, Lgfp;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v3, v1, v2}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1188
    :goto_0
    invoke-direct {p0, v0}, Lgdj;->a(Lggp;)V

    .line 1195
    :goto_1
    return-void

    .line 1189
    :cond_0
    invoke-virtual {p1}, Lgfq;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lgfq;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lxa;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lgfq;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lxa;

    iget-object v2, v0, Lxa;->a:Lwm;

    if-eqz v2, :cond_1

    new-instance v2, Lggp;

    const-string v3, "drm.net.badstatus"

    iget-object v0, v0, Lxa;->a:Lwm;

    iget v0, v0, Lwm;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v3, v1, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    invoke-virtual {v2}, Lggp;->b()Lggp;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lgfq;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lwz;

    if-eqz v0, :cond_2

    new-instance v0, Lggp;

    const-string v2, "drm.net.timeout"

    invoke-direct {v0, v2, v1}, Lggp;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Lggp;->b()Lggp;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lgfq;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lwn;

    if-eqz v0, :cond_3

    new-instance v0, Lggp;

    const-string v2, "drm.net.connect"

    invoke-direct {v0, v2, v1}, Lggp;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Lggp;->b()Lggp;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v0, Lggp;

    const-string v2, "drm"

    invoke-direct {v0, v2, v1, p1}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    goto :goto_0

    .line 1193
    :cond_4
    const-string v0, "drm"

    invoke-direct {p0, v0, p1}, Lgdj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final onHdEntitlementReceived()V
    .locals 2

    .prologue
    .line 1239
    iget-object v0, p0, Lgdj;->C:Lfrf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdj;->C:Lfrf;

    .line 1240
    iget-boolean v0, v0, Lfrf;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1239
    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 1242
    iget-object v0, p0, Lgdj;->C:Lfrf;

    if-nez v0, :cond_2

    .line 1250
    :goto_1
    return-void

    .line 1240
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1246
    :cond_2
    invoke-virtual {p0}, Lgdj;->b()V

    .line 1249
    iget-object v0, p0, Lgdj;->p:Lgdl;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lgdl;->a(I)V

    goto :goto_1
.end method
