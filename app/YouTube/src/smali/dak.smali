.class public final enum Ldak;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldak;

.field public static final enum b:Ldak;

.field private static final synthetic c:[Ldak;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Ldak;

    const-string v1, "NAVIGATION"

    invoke-direct {v0, v1, v2}, Ldak;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldak;->a:Ldak;

    .line 17
    new-instance v0, Ldak;

    const-string v1, "PLAYER_CONTROL"

    invoke-direct {v0, v1, v3}, Ldak;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldak;->b:Ldak;

    .line 9
    const/4 v0, 0x2

    new-array v0, v0, [Ldak;

    sget-object v1, Ldak;->a:Ldak;

    aput-object v1, v0, v2

    sget-object v1, Ldak;->b:Ldak;

    aput-object v1, v0, v3

    sput-object v0, Ldak;->c:[Ldak;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldak;
    .locals 1

    .prologue
    .line 9
    const-class v0, Ldak;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldak;

    return-object v0
.end method

.method public static values()[Ldak;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Ldak;->c:[Ldak;

    invoke-virtual {v0}, [Ldak;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldak;

    return-object v0
.end method
