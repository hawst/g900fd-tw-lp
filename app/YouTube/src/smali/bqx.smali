.class public Lbqx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public b:F

.field public c:F

.field public d:I

.field private final e:I

.field private final f:I

.field private g:Landroid/view/VelocityTracker;

.field private h:F

.field private i:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 68
    const/16 v0, 0xc8

    invoke-direct {p0, p1, v0}, Lbqx;-><init>(Landroid/content/Context;I)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lbqx;->d:I

    .line 72
    const/16 v0, 0xc8

    if-lt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "snapVelocity cannot be less than 200"

    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    .line 74
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    iput v1, p0, Lbqx;->a:I

    .line 76
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lbqx;->e:I

    .line 77
    iput p2, p0, Lbqx;->f:I

    .line 78
    return-void

    .line 72
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;IZ)I
    .locals 5

    .prologue
    const/4 v2, 0x3

    .line 156
    iget v0, p0, Lbqx;->d:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 157
    if-gez v0, :cond_0

    move v0, v2

    .line 191
    :goto_0
    return v0

    .line 161
    :cond_0
    iget-object v1, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    iget v4, p0, Lbqx;->e:I

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 164
    sget-object v1, Lbqy;->a:[I

    add-int/lit8 v3, p2, -0x1

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 175
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot assess fling for ANY orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :pswitch_0
    iget v1, p0, Lbqx;->i:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    sub-float v0, v1, v0

    float-to-int v1, v0

    .line 168
    iget-object v0, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    iget v3, p0, Lbqx;->d:I

    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v0

    float-to-int v0, v0

    .line 178
    :goto_1
    iget-object v3, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v3, 0x0

    iput-object v3, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    .line 184
    :cond_1
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v3, 0x14

    if-le v1, v3, :cond_2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v3, p0, Lbqx;->f:I

    if-gt v1, v3, :cond_3

    :cond_2
    move v0, v2

    .line 185
    goto :goto_0

    .line 171
    :pswitch_1
    iget v1, p0, Lbqx;->h:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    sub-float v0, v1, v0

    float-to-int v1, v0

    .line 172
    iget-object v0, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    iget v3, p0, Lbqx;->d:I

    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    float-to-int v0, v0

    .line 173
    goto :goto_1

    .line 186
    :cond_3
    if-lez v0, :cond_4

    .line 188
    const/4 v0, 0x1

    goto :goto_0

    .line 191
    :cond_4
    const/4 v0, 0x2

    goto :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 82
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    .line 84
    :cond_0
    iget-object v0, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 85
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lbqx;->b:F

    .line 90
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lbqx;->c:F

    .line 91
    iget v0, p0, Lbqx;->b:F

    iput v0, p0, Lbqx;->h:F

    .line 92
    iget v0, p0, Lbqx;->c:F

    iput v0, p0, Lbqx;->i:F

    .line 93
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lbqx;->d:I

    .line 94
    return-void
.end method

.method public final c(Landroid/view/MotionEvent;)I
    .locals 2

    .prologue
    .line 97
    iget v0, p0, Lbqx;->d:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 98
    if-gez v0, :cond_0

    .line 99
    const/4 v0, 0x0

    .line 104
    :goto_0
    return v0

    .line 101
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 102
    iget v0, p0, Lbqx;->b:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 103
    iput v1, p0, Lbqx;->b:F

    goto :goto_0
.end method

.method public final d(Landroid/view/MotionEvent;)I
    .locals 2

    .prologue
    .line 108
    iget v0, p0, Lbqx;->d:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 109
    if-gez v0, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 115
    :goto_0
    return v0

    .line 112
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    .line 113
    iget v0, p0, Lbqx;->c:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 114
    iput v1, p0, Lbqx;->c:F

    goto :goto_0
.end method

.method public final e(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 196
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    shr-int/lit8 v0, v0, 0x8

    and-int/lit16 v0, v0, 0xff

    .line 198
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v2, p0, Lbqx;->d:I

    if-ne v1, v2, :cond_0

    .line 202
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 203
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lbqx;->b:F

    .line 204
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iput v1, p0, Lbqx;->c:F

    .line 205
    iget v1, p0, Lbqx;->b:F

    iput v1, p0, Lbqx;->h:F

    .line 206
    iget v1, p0, Lbqx;->c:F

    iput v1, p0, Lbqx;->i:F

    .line 207
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lbqx;->d:I

    .line 208
    iget-object v0, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lbqx;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 212
    :cond_0
    return-void

    .line 202
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
