.class public final Leae;
.super Lida;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/util/List;

.field public d:Z

.field public e:I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 848
    invoke-direct {p0}, Lida;-><init>()V

    .line 853
    const-string v0, ""

    iput-object v0, p0, Leae;->b:Ljava/lang/String;

    .line 870
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Leae;->c:Ljava/util/List;

    .line 897
    const/4 v0, 0x0

    iput v0, p0, Leae;->e:I

    .line 936
    const/4 v0, -0x1

    iput v0, p0, Leae;->f:I

    .line 848
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 938
    iget v0, p0, Leae;->f:I

    if-gez v0, :cond_0

    .line 940
    invoke-virtual {p0}, Leae;->b()I

    .line 942
    :cond_0
    iget v0, p0, Leae;->f:I

    return v0
.end method

.method public final a(I)Leae;
    .locals 2

    .prologue
    .line 883
    iget-object v0, p0, Leae;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 884
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Leae;->c:Ljava/util/List;

    .line 886
    :cond_0
    iget-object v0, p0, Leae;->c:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 887
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Leae;
    .locals 1

    .prologue
    .line 857
    const/4 v0, 0x1

    iput-boolean v0, p0, Leae;->a:Z

    .line 858
    iput-object p1, p0, Leae;->b:Ljava/lang/String;

    .line 859
    return-object p0
.end method

.method public final synthetic a(Licx;)Lida;
    .locals 1

    .prologue
    .line 846
    :cond_0
    :goto_0
    invoke-virtual {p1}, Licx;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Licx;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Licx;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Leae;->a(Ljava/lang/String;)Leae;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Licx;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Leae;->a(I)Leae;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Licx;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Leae;->b(I)Leae;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Licy;)V
    .locals 3

    .prologue
    .line 925
    iget-boolean v0, p0, Leae;->a:Z

    if-eqz v0, :cond_0

    .line 926
    const/4 v0, 0x1

    iget-object v1, p0, Leae;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILjava/lang/String;)V

    .line 928
    :cond_0
    iget-object v0, p0, Leae;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 929
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Licy;->a(II)V

    goto :goto_0

    .line 931
    :cond_1
    iget-boolean v0, p0, Leae;->d:Z

    if-eqz v0, :cond_2

    .line 932
    const/4 v0, 0x3

    iget v1, p0, Leae;->e:I

    invoke-virtual {p1, v0, v1}, Licy;->a(II)V

    .line 934
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 946
    .line 947
    iget-boolean v0, p0, Leae;->a:Z

    if-eqz v0, :cond_2

    .line 948
    const/4 v0, 0x1

    .line 949
    iget-object v1, p0, Leae;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Licy;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 953
    :goto_0
    iget-object v0, p0, Leae;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 955
    invoke-static {v0}, Licy;->a(I)I

    move-result v0

    add-int/2addr v2, v0

    .line 956
    goto :goto_1

    .line 957
    :cond_0
    add-int v0, v1, v2

    .line 958
    iget-object v1, p0, Leae;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 960
    iget-boolean v1, p0, Leae;->d:Z

    if-eqz v1, :cond_1

    .line 961
    const/4 v1, 0x3

    .line 962
    iget v2, p0, Leae;->e:I

    invoke-static {v1, v2}, Licy;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 964
    :cond_1
    iput v0, p0, Leae;->f:I

    .line 965
    return v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public final b(I)Leae;
    .locals 1

    .prologue
    .line 901
    const/4 v0, 0x1

    iput-boolean v0, p0, Leae;->d:Z

    .line 902
    iput p1, p0, Leae;->e:I

    .line 903
    return-object p0
.end method
