.class public final Lezm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:J

.field private final b:J

.field private final c:J

.field private final d:Ljava/util/Random;

.field private e:J


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    .line 36
    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x7530

    const-wide/16 v6, 0x5

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lezm;-><init>(JJJ)V

    .line 37
    return-void
.end method

.method private constructor <init>(JJJ)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x5

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lezm;->d:Ljava/util/Random;

    .line 26
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lezm;->e:J

    .line 27
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lezm;->b:J

    .line 48
    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lezm;->c:J

    .line 49
    const-wide/16 v0, 0x1

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 50
    iput-wide v2, p0, Lezm;->a:J

    .line 51
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 64
    iget-wide v0, p0, Lezm;->e:J

    iget-wide v4, p0, Lezm;->a:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    move v0, v2

    .line 78
    :goto_0
    return v0

    .line 67
    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget-object v3, p0, Lezm;->d:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextDouble()D

    move-result-wide v4

    add-double/2addr v0, v4

    .line 68
    iget-wide v4, p0, Lezm;->b:J

    long-to-double v4, v4

    mul-double/2addr v0, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p0, Lezm;->e:J

    long-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v0, v4

    double-to-long v0, v0

    .line 69
    iget-wide v4, p0, Lezm;->c:J

    cmp-long v3, v0, v4

    if-lez v3, :cond_1

    iget-wide v0, p0, Lezm;->c:J

    .line 71
    :cond_1
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Sleeping thread for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lezp;->d(Ljava/lang/String;)V

    .line 72
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 73
    iget-wide v0, p0, Lezm;->e:J

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    iput-wide v0, p0, Lezm;->e:J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    const/4 v0, 0x1

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    const-string v1, "Thread interrupted"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    .line 78
    goto :goto_0
.end method
