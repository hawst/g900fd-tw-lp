.class public final Ldbo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbn;
.implements Ldcf;


# instance fields
.field final a:Ldbm;

.field final b:Leyp;

.field final c:Landroid/os/Handler;

.field d:Lfoy;

.field e:Lhog;

.field f:Leue;

.field private final g:Levn;

.field private final h:Lcws;

.field private final i:Lfgk;

.field private final j:Lfhz;

.field private final k:Lezj;

.field private final l:Lcwn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private m:Ldbq;

.field private n:Z

.field private o:Z


# direct methods
.method public constructor <init>(Ldbm;Levn;Lcws;Lfgk;Leyp;Lfhz;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbm;

    iput-object v0, p0, Ldbo;->a:Ldbm;

    .line 75
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldbo;->g:Levn;

    .line 76
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcws;

    iput-object v0, p0, Ldbo;->h:Lcws;

    .line 77
    iput-object p4, p0, Ldbo;->i:Lfgk;

    .line 78
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Ldbo;->b:Leyp;

    .line 79
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Ldbo;->j:Lfhz;

    .line 80
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ldbo;->c:Landroid/os/Handler;

    .line 81
    new-instance v0, Lezj;

    invoke-direct {v0}, Lezj;-><init>()V

    iput-object v0, p0, Ldbo;->k:Lezj;

    .line 82
    iget-object v0, p3, Lcws;->d:Lcwn;

    iput-object v0, p0, Ldbo;->l:Lcwn;

    .line 83
    invoke-interface {p1, p0}, Ldbm;->a(Ldbn;)V

    .line 84
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 122
    iget-object v0, p0, Ldbo;->f:Leue;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Ldbo;->f:Leue;

    iput-boolean v2, v0, Leue;->a:Z

    .line 124
    iput-object v1, p0, Ldbo;->f:Leue;

    .line 126
    :cond_0
    iget-object v0, p0, Ldbo;->m:Ldbq;

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Ldbo;->m:Ldbq;

    iput-boolean v2, v0, Ldbq;->a:Z

    .line 128
    iput-object v1, p0, Ldbo;->m:Ldbq;

    .line 130
    :cond_1
    iput-object v1, p0, Ldbo;->e:Lhog;

    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbo;->o:Z

    .line 132
    iget-object v0, p0, Ldbo;->a:Ldbm;

    invoke-interface {v0}, Ldbm;->a()V

    .line 133
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Ldbo;->d:Lfoy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbo;->d:Lfoy;

    iget-object v0, v0, Lfoy;->t:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Ldbo;->l:Lcwn;

    invoke-virtual {v0}, Lcwn;->f()V

    .line 233
    iget-object v0, p0, Ldbo;->j:Lfhz;

    iget-object v1, p0, Ldbo;->d:Lfoy;

    .line 234
    iget-object v1, v1, Lfoy;->t:Landroid/net/Uri;

    invoke-static {v1}, Lfia;->a(Landroid/net/Uri;)Lhog;

    move-result-object v1

    const/4 v2, 0x0

    .line 233
    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    .line 236
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Ldbo;->d:Lfoy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbo;->d:Lfoy;

    invoke-virtual {v0}, Lfoy;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Ldbo;->l:Lcwn;

    invoke-virtual {v0, p1, p2}, Lcwn;->a(II)V

    .line 249
    iget-object v0, p0, Ldbo;->h:Lcws;

    iget-boolean v1, v0, Lcws;->j:Z

    if-nez v1, :cond_0

    iget-object v0, v0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->n()V

    .line 251
    :cond_0
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Ldbo;->g:Levn;

    new-instance v1, Ldaf;

    invoke-direct {v1, p1}, Ldaf;-><init>(Z)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 213
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Ldbo;->d:Lfoy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbo;->d:Lfoy;

    invoke-virtual {v0}, Lfoy;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Ldbo;->l:Lcwn;

    invoke-virtual {v0}, Lcwn;->e()V

    .line 243
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 255
    iget-object v0, p0, Ldbo;->e:Lhog;

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Ldbo;->l:Lcwn;

    iget-object v1, v0, Lcwn;->c:Lcnh;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcwn;->B:Z

    if-nez v1, :cond_0

    iget-object v0, v0, Lcwn;->c:Lcnh;

    invoke-virtual {v0}, Lcnh;->k()V

    .line 257
    :cond_0
    iget-object v0, p0, Ldbo;->j:Lfhz;

    iget-object v1, p0, Ldbo;->e:Lhog;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    .line 259
    :cond_1
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 269
    invoke-virtual {p0}, Ldbo;->a()V

    .line 270
    return-void
.end method

.method public final handleVideoStageEvent(Ldac;)V
    .locals 7
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 137
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v4, Lgol;->f:Lgol;

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ldbo;->n:Z

    .line 138
    iget-object v0, p1, Ldac;->a:Lgol;

    invoke-virtual {v0}, Lgol;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p1, Ldac;->d:Lfoy;

    if-eqz v0, :cond_8

    iget-object v0, p1, Ldac;->d:Lfoy;

    iget-object v0, v0, Lfoy;->aj:Lfoo;

    if-nez v0, :cond_8

    .line 139
    iget-object v0, p0, Ldbo;->d:Lfoy;

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p1, Ldac;->d:Lfoy;

    iput-object v0, p0, Ldbo;->d:Lfoy;

    .line 142
    invoke-direct {p0}, Ldbo;->e()V

    iget-object v0, p0, Ldbo;->d:Lfoy;

    iget-object v0, v0, Lfoy;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Ldbo;->d:Lfoy;

    iget-object v0, v0, Lfoy;->t:Landroid/net/Uri;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Ldbo;->a(Z)V

    .line 144
    :cond_0
    :goto_2
    iget-boolean v0, p0, Ldbo;->n:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldbo;->d:Lfoy;

    iget-object v4, p0, Ldbo;->k:Lezj;

    invoke-virtual {v0, v4}, Lfoy;->b(Lezj;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 146
    iget-boolean v0, p0, Ldbo;->o:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Ldbo;->d:Lfoy;

    invoke-virtual {v0}, Lfoy;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Ldbo;->d:Lfoy;

    iget v0, v0, Lfoy;->o:I

    const/4 v4, 0x7

    if-le v0, v4, :cond_7

    move v0, v1

    :goto_3
    iget-object v4, p0, Ldbo;->a:Ldbm;

    iget-object v5, p0, Ldbo;->d:Lfoy;

    iget-object v6, v5, Lfoy;->W:Lfkg;

    if-nez v6, :cond_1

    invoke-virtual {v5}, Lfoy;->c()Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v5, p0, Ldbo;->d:Lfoy;

    iget-object v5, v5, Lfoy;->t:Landroid/net/Uri;

    if-eqz v5, :cond_2

    move v2, v1

    :cond_2
    iget-object v5, p0, Ldbo;->d:Lfoy;

    iget-object v5, v5, Lfoy;->d:Ljava/lang/String;

    invoke-interface {v4, v3, v0, v2, v5}, Ldbm;->a(Ljava/lang/String;ZZLjava/lang/String;)V

    iput-boolean v1, p0, Ldbo;->o:Z

    .line 152
    :cond_3
    :goto_4
    return-void

    :cond_4
    move v0, v2

    .line 137
    goto :goto_0

    :cond_5
    move v0, v2

    .line 142
    goto :goto_1

    :cond_6
    iget-object v0, p0, Ldbo;->i:Lfgk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbo;->i:Lfgk;

    invoke-virtual {v0}, Lfgk;->a()Lfgn;

    move-result-object v0

    iget-object v4, p0, Ldbo;->d:Lfoy;

    iget-object v4, v4, Lfoy;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lfgn;->a(Ljava/lang/String;)Lfgn;

    iput-boolean v1, v0, Lfgn;->c:Z

    new-instance v4, Ldbq;

    iget-object v5, p0, Ldbo;->d:Lfoy;

    iget-object v5, v5, Lfoy;->c:Ljava/lang/String;

    invoke-direct {v4, p0, v5}, Ldbq;-><init>(Ldbo;Ljava/lang/String;)V

    iput-object v4, p0, Ldbo;->m:Ldbq;

    iget-object v4, p0, Ldbo;->i:Lfgk;

    iget-object v5, p0, Ldbo;->m:Ldbq;

    invoke-virtual {v4, v0, v5}, Lfgk;->a(Lfgn;Lwv;)V

    goto :goto_2

    :cond_7
    move v0, v2

    .line 146
    goto :goto_3

    .line 149
    :cond_8
    iput-object v3, p0, Ldbo;->d:Lfoy;

    .line 150
    invoke-direct {p0}, Ldbo;->e()V

    goto :goto_4
.end method

.method public final handleVideoTimeEvent(Ldad;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 156
    iget-boolean v0, p0, Ldbo;->n:Z

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Ldbo;->a:Ldbm;

    iget-wide v2, p1, Ldad;->a:J

    long-to-int v1, v2

    iget-wide v2, p1, Ldad;->b:J

    long-to-int v2, v2

    invoke-interface {v0, v1, v2}, Ldbm;->a(II)V

    .line 159
    :cond_0
    return-void
.end method
