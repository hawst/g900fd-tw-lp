.class public Ldsp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ldth;

.field public final c:I

.field public final d:Z

.field private final e:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    new-instance v0, Ldsq;

    invoke-direct {v0}, Ldsq;-><init>()V

    sput-object v0, Ldsp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 133
    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Ldsp;-><init>(ILandroid/net/Uri;Ljava/lang/String;Ldth;Z)V

    .line 134
    return-void
.end method

.method private constructor <init>(ILandroid/net/Uri;Ljava/lang/String;Ldth;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput p1, p0, Ldsp;->c:I

    .line 118
    iput-object v0, p0, Ldsp;->e:Landroid/net/Uri;

    .line 119
    iput-object v0, p0, Ldsp;->a:Ljava/lang/String;

    .line 120
    iput-object v0, p0, Ldsp;->b:Ldth;

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldsp;->d:Z

    .line 122
    return-void
.end method

.method public constructor <init>(Ldsr;)V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iget v0, p1, Ldsr;->d:I

    iput v0, p0, Ldsp;->c:I

    .line 126
    iget-object v0, p1, Ldsr;->a:Landroid/net/Uri;

    iput-object v0, p0, Ldsp;->e:Landroid/net/Uri;

    .line 127
    iget-object v0, p1, Ldsr;->b:Ljava/lang/String;

    iput-object v0, p0, Ldsp;->a:Ljava/lang/String;

    .line 128
    iget-object v0, p1, Ldsr;->c:Ldth;

    iput-object v0, p0, Ldsp;->b:Ldth;

    .line 129
    iget-boolean v0, p1, Ldsr;->e:Z

    iput-boolean v0, p0, Ldsp;->d:Z

    .line 130
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 180
    if-ne p0, p1, :cond_1

    .line 217
    :cond_0
    :goto_0
    return v0

    .line 183
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 184
    goto :goto_0

    .line 186
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 187
    goto :goto_0

    .line 189
    :cond_3
    check-cast p1, Ldsp;

    .line 190
    iget-object v2, p0, Ldsp;->e:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 191
    iget-object v2, p1, Ldsp;->e:Landroid/net/Uri;

    if-eqz v2, :cond_5

    move v0, v1

    .line 192
    goto :goto_0

    .line 194
    :cond_4
    iget-object v2, p0, Ldsp;->e:Landroid/net/Uri;

    iget-object v3, p1, Ldsp;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 195
    goto :goto_0

    .line 197
    :cond_5
    iget-object v2, p0, Ldsp;->a:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 198
    iget-object v2, p1, Ldsp;->a:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 199
    goto :goto_0

    .line 201
    :cond_6
    iget-object v2, p0, Ldsp;->a:Ljava/lang/String;

    iget-object v3, p1, Ldsp;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 202
    goto :goto_0

    .line 204
    :cond_7
    iget-object v2, p0, Ldsp;->b:Ldth;

    if-nez v2, :cond_8

    .line 205
    iget-object v2, p1, Ldsp;->b:Ldth;

    if-eqz v2, :cond_9

    move v0, v1

    .line 206
    goto :goto_0

    .line 208
    :cond_8
    iget-object v2, p0, Ldsp;->b:Ldth;

    iget-object v3, p1, Ldsp;->b:Ldth;

    invoke-virtual {v2, v3}, Ldth;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 209
    goto :goto_0

    .line 211
    :cond_9
    iget v2, p0, Ldsp;->c:I

    iget v3, p1, Ldsp;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 212
    goto :goto_0

    .line 214
    :cond_a
    iget-boolean v2, p0, Ldsp;->d:Z

    iget-boolean v3, p1, Ldsp;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 215
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 168
    iget-object v0, p0, Ldsp;->e:Landroid/net/Uri;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 171
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldsp;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 172
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldsp;->b:Ldth;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 173
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Ldsp;->c:I

    add-int/2addr v0, v2

    .line 174
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Ldsp;->d:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 175
    return v0

    .line 168
    :cond_1
    iget-object v0, p0, Ldsp;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_0

    .line 171
    :cond_2
    iget-object v0, p0, Ldsp;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 172
    :cond_3
    iget-object v0, p0, Ldsp;->b:Ldth;

    invoke-virtual {v0}, Ldth;->hashCode()I

    move-result v0

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 256
    iget-object v0, p0, Ldsp;->a:Ljava/lang/String;

    iget-object v1, p0, Ldsp;->b:Ldth;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Ldsp;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3e

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "AppStatus [runningPathSegment="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", screenId="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Ldsp;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 268
    iget-object v0, p0, Ldsp;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 269
    iget-object v0, p0, Ldsp;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Ldsp;->b:Ldth;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 271
    iget-boolean v0, p0, Ldsp;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 272
    return-void

    .line 271
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
