.class final Liod;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lins;


# instance fields
.field private final a:Lipu;

.field private final b:Liob;

.field private final c:Z

.field private d:Liny;


# direct methods
.method constructor <init>(Lipu;IZ)V
    .locals 3

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Liod;->a:Lipu;

    .line 97
    iput-boolean p3, p0, Liod;->c:Z

    .line 98
    new-instance v0, Liob;

    iget-object v1, p0, Liod;->a:Lipu;

    invoke-direct {v0, v1}, Liob;-><init>(Lipu;)V

    iput-object v0, p0, Liod;->b:Liob;

    .line 99
    new-instance v0, Liny;

    const/16 v1, 0x1000

    iget-object v2, p0, Liod;->b:Liob;

    invoke-direct {v0, v1, v2}, Liny;-><init>(ILiql;)V

    iput-object v0, p0, Liod;->d:Liny;

    .line 100
    return-void
.end method

.method private a(ISBI)Ljava/util/List;
    .locals 8

    .prologue
    const/16 v7, 0x80

    const/16 v6, 0x40

    const/4 v5, -0x1

    .line 205
    iget-object v0, p0, Liod;->b:Liob;

    iget-object v1, p0, Liod;->b:Liob;

    iput p1, v1, Liob;->d:I

    iput p1, v0, Liob;->a:I

    .line 206
    iget-object v0, p0, Liod;->b:Liob;

    iput-short p2, v0, Liob;->e:S

    .line 207
    iget-object v0, p0, Liod;->b:Liob;

    iput-byte p3, v0, Liob;->b:B

    .line 208
    iget-object v0, p0, Liod;->b:Liob;

    iput p4, v0, Liob;->c:I

    .line 212
    iget-object v0, p0, Liod;->d:Liny;

    :goto_0
    iget-object v1, v0, Liny;->b:Lipu;

    invoke-interface {v1}, Lipu;->e()Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, v0, Liny;->b:Lipu;

    invoke-interface {v1}, Lipu;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ne v1, v7, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "index == 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit16 v2, v1, 0x80

    if-ne v2, v7, :cond_4

    const/16 v2, 0x7f

    invoke-virtual {v0, v1, v2}, Liny;->a(II)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Liny;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Linx;->a()[Linv;

    move-result-object v2

    aget-object v1, v2, v1

    iget-object v2, v0, Liny;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {}, Linx;->a()[Linv;

    move-result-object v2

    array-length v2, v2

    sub-int v2, v1, v2

    invoke-virtual {v0, v2}, Liny;->a(I)I

    move-result v2

    if-ltz v2, :cond_2

    iget-object v3, v0, Liny;->e:[Linv;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_3

    :cond_2
    new-instance v0, Ljava/io/IOException;

    add-int/lit8 v1, v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Header index too large "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v1, v0, Liny;->a:Ljava/util/List;

    iget-object v3, v0, Liny;->e:[Linv;

    aget-object v2, v3, v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    if-ne v1, v6, :cond_5

    invoke-virtual {v0}, Liny;->b()Lipv;

    move-result-object v1

    invoke-static {v1}, Linx;->a(Lipv;)Lipv;

    move-result-object v1

    invoke-virtual {v0}, Liny;->b()Lipv;

    move-result-object v2

    new-instance v3, Linv;

    invoke-direct {v3, v1, v2}, Linv;-><init>(Lipv;Lipv;)V

    invoke-virtual {v0, v5, v3}, Liny;->a(ILinv;)V

    goto/16 :goto_0

    :cond_5
    and-int/lit8 v2, v1, 0x40

    if-ne v2, v6, :cond_6

    const/16 v2, 0x3f

    invoke-virtual {v0, v1, v2}, Liny;->a(II)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Liny;->b(I)Lipv;

    move-result-object v1

    invoke-virtual {v0}, Liny;->b()Lipv;

    move-result-object v2

    new-instance v3, Linv;

    invoke-direct {v3, v1, v2}, Linv;-><init>(Lipv;Lipv;)V

    invoke-virtual {v0, v5, v3}, Liny;->a(ILinv;)V

    goto/16 :goto_0

    :cond_6
    and-int/lit8 v2, v1, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_9

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v2}, Liny;->a(II)I

    move-result v1

    iput v1, v0, Liny;->d:I

    iget v1, v0, Liny;->d:I

    if-ltz v1, :cond_7

    iget v1, v0, Liny;->d:I

    iget v2, v0, Liny;->c:I

    if-le v1, v2, :cond_8

    :cond_7
    new-instance v1, Ljava/io/IOException;

    iget v0, v0, Liny;->d:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid header table byte count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    invoke-virtual {v0}, Liny;->a()V

    goto/16 :goto_0

    :cond_9
    const/16 v2, 0x10

    if-eq v1, v2, :cond_a

    if-nez v1, :cond_b

    :cond_a
    invoke-virtual {v0}, Liny;->b()Lipv;

    move-result-object v1

    invoke-static {v1}, Linx;->a(Lipv;)Lipv;

    move-result-object v1

    invoke-virtual {v0}, Liny;->b()Lipv;

    move-result-object v2

    iget-object v3, v0, Liny;->a:Ljava/util/List;

    new-instance v4, Linv;

    invoke-direct {v4, v1, v2}, Linv;-><init>(Lipv;Lipv;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_b
    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Liny;->a(II)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Liny;->b(I)Lipv;

    move-result-object v1

    invoke-virtual {v0}, Liny;->b()Lipv;

    move-result-object v2

    iget-object v3, v0, Liny;->a:Ljava/util/List;

    new-instance v4, Linv;

    invoke-direct {v4, v1, v2}, Linv;-><init>(Lipv;Lipv;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 213
    :cond_c
    iget-object v0, p0, Liod;->d:Liny;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Liny;->a:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, v0, Liny;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-object v1
.end method

.method private a(Lint;I)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Liod;->a:Lipu;

    invoke-interface {v0}, Lipu;->i()I

    .line 241
    iget-object v0, p0, Liod;->a:Lipu;

    invoke-interface {v0}, Lipu;->g()B

    .line 244
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 103
    iget-boolean v0, p0, Liod;->c:Z

    if-eqz v0, :cond_1

    .line 109
    :cond_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Liod;->a:Lipu;

    invoke-static {}, Lioa;->a()Lipv;

    move-result-object v1

    iget-object v1, v1, Lipv;->b:[B

    array-length v1, v1

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, Lipu;->c(J)Lipv;

    move-result-object v0

    .line 105
    invoke-static {}, Lioa;->b()Ljava/util/logging/Logger;

    move-result-object v1

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lioa;->b()Ljava/util/logging/Logger;

    move-result-object v1

    const-string v2, "<< CONNECTION %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lipv;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 106
    :cond_2
    invoke-static {}, Lioa;->a()Lipv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 107
    const-string v1, "Expected a connection header but was %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lipv;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public final a(Lint;)Z
    .locals 11

    .prologue
    const/16 v10, 0x4000

    const/16 v9, 0x8

    const/4 v4, 0x4

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 113
    :try_start_0
    iget-object v0, p0, Liod;->a:Lipu;

    const-wide/16 v2, 0x9

    invoke-interface {v0, v2, v3}, Lipu;->a(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    iget-object v0, p0, Liod;->a:Lipu;

    invoke-static {v0}, Lioa;->a(Lipu;)I

    move-result v0

    .line 131
    if-ltz v0, :cond_0

    if-le v0, v10, :cond_2

    .line 132
    :cond_0
    const-string v2, "FRAME_SIZE_ERROR: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 115
    :catch_0
    move-exception v0

    move v6, v1

    .line 180
    :cond_1
    :goto_0
    return v6

    .line 134
    :cond_2
    iget-object v2, p0, Liod;->a:Lipu;

    invoke-interface {v2}, Lipu;->g()B

    move-result v2

    int-to-byte v2, v2

    .line 135
    iget-object v3, p0, Liod;->a:Lipu;

    invoke-interface {v3}, Lipu;->g()B

    move-result v3

    int-to-byte v5, v3

    .line 136
    iget-object v3, p0, Liod;->a:Lipu;

    invoke-interface {v3}, Lipu;->i()I

    move-result v3

    const v7, 0x7fffffff

    and-int/2addr v3, v7

    .line 137
    invoke-static {}, Lioa;->b()Ljava/util/logging/Logger;

    move-result-object v7

    sget-object v8, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v7, v8}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-static {}, Lioa;->b()Ljava/util/logging/Logger;

    move-result-object v7

    invoke-static {v6, v3, v0, v2, v5}, Lioc;->a(ZIIBB)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 139
    :cond_3
    packed-switch v2, :pswitch_data_0

    .line 178
    iget-object v1, p0, Liod;->a:Lipu;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, Lipu;->f(J)V

    goto :goto_0

    .line 141
    :pswitch_0
    and-int/lit8 v2, v5, 0x1

    if-eqz v2, :cond_4

    move v4, v6

    :goto_1
    and-int/lit8 v2, v5, 0x20

    if-eqz v2, :cond_5

    move v2, v6

    :goto_2
    if-eqz v2, :cond_6

    const-string v0, "PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_4
    move v4, v1

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_2

    :cond_6
    and-int/lit8 v2, v5, 0x8

    if-eqz v2, :cond_7

    iget-object v1, p0, Liod;->a:Lipu;

    invoke-interface {v1}, Lipu;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-short v1, v1

    :cond_7
    invoke-static {v0, v5, v1}, Lioa;->a(IBS)I

    move-result v0

    iget-object v2, p0, Liod;->a:Lipu;

    invoke-interface {p1, v4, v3, v2, v0}, Lint;->a(ZILipu;I)V

    iget-object v0, p0, Liod;->a:Lipu;

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, Lipu;->f(J)V

    goto :goto_0

    .line 145
    :pswitch_1
    if-nez v3, :cond_8

    const-string v0, "PROTOCOL_ERROR: TYPE_HEADERS streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_8
    and-int/lit8 v2, v5, 0x1

    if-eqz v2, :cond_a

    move v2, v6

    :goto_3
    and-int/lit8 v4, v5, 0x8

    if-eqz v4, :cond_b

    iget-object v4, p0, Liod;->a:Lipu;

    invoke-interface {v4}, Lipu;->g()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    :goto_4
    and-int/lit8 v7, v5, 0x20

    if-eqz v7, :cond_9

    invoke-direct {p0, p1, v3}, Liod;->a(Lint;I)V

    add-int/lit8 v0, v0, -0x5

    :cond_9
    invoke-static {v0, v5, v4}, Lioa;->a(IBS)I

    move-result v0

    invoke-direct {p0, v0, v4, v5, v3}, Liod;->a(ISBI)Ljava/util/List;

    move-result-object v4

    sget-object v5, Linw;->d:Linw;

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lint;->a(ZZILjava/util/List;Linw;)V

    goto/16 :goto_0

    :cond_a
    move v2, v1

    goto :goto_3

    :cond_b
    move v4, v1

    goto :goto_4

    .line 149
    :pswitch_2
    const/4 v2, 0x5

    if-eq v0, v2, :cond_c

    const-string v2, "TYPE_PRIORITY length: %d != 5"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_c
    if-nez v3, :cond_d

    const-string v0, "TYPE_PRIORITY streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_d
    invoke-direct {p0, p1, v3}, Liod;->a(Lint;I)V

    goto/16 :goto_0

    .line 153
    :pswitch_3
    if-eq v0, v4, :cond_e

    const-string v2, "TYPE_RST_STREAM length: %d != 4"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_e
    if-nez v3, :cond_f

    const-string v0, "TYPE_RST_STREAM streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_f
    iget-object v0, p0, Liod;->a:Lipu;

    invoke-interface {v0}, Lipu;->i()I

    move-result v0

    invoke-static {v0}, Linr;->b(I)Linr;

    move-result-object v2

    if-nez v2, :cond_10

    const-string v2, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_10
    invoke-interface {p1, v3, v2}, Lint;->a(ILinr;)V

    goto/16 :goto_0

    .line 157
    :pswitch_4
    if-eqz v3, :cond_11

    const-string v0, "TYPE_SETTINGS streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_11
    and-int/lit8 v2, v5, 0x1

    if-eqz v2, :cond_12

    if-eqz v0, :cond_1

    const-string v0, "FRAME_SIZE_ERROR ack frame should be empty!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_12
    rem-int/lit8 v2, v0, 0x6

    if-eqz v2, :cond_13

    const-string v2, "TYPE_SETTINGS length %% 6 != 0: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_13
    new-instance v5, Lioo;

    invoke-direct {v5}, Lioo;-><init>()V

    move v3, v1

    :goto_5
    if-ge v3, v0, :cond_16

    iget-object v2, p0, Liod;->a:Lipu;

    invoke-interface {v2}, Lipu;->h()S

    move-result v2

    iget-object v7, p0, Liod;->a:Lipu;

    invoke-interface {v7}, Lipu;->i()I

    move-result v7

    packed-switch v2, :pswitch_data_1

    const-string v0, "PROTOCOL_ERROR invalid settings id: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v0, v3}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :pswitch_5
    if-eqz v7, :cond_14

    if-eq v7, v6, :cond_14

    const-string v0, "PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :pswitch_6
    move v2, v4

    :cond_14
    :pswitch_7
    invoke-virtual {v5, v2, v1, v7}, Lioo;->a(III)Lioo;

    add-int/lit8 v2, v3, 0x6

    move v3, v2

    goto :goto_5

    :pswitch_8
    const/4 v2, 0x7

    if-gez v7, :cond_14

    const-string v0, "PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :pswitch_9
    if-lt v7, v10, :cond_15

    const v8, 0xffffff

    if-le v7, v8, :cond_14

    :cond_15
    const-string v0, "PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_16
    invoke-interface {p1, v1, v5}, Lint;->a(ZLioo;)V

    invoke-virtual {v5}, Lioo;->a()I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v0, p0, Liod;->d:Liny;

    invoke-virtual {v5}, Lioo;->a()I

    move-result v1

    iput v1, v0, Liny;->c:I

    iget v1, v0, Liny;->c:I

    iput v1, v0, Liny;->d:I

    invoke-virtual {v0}, Liny;->a()V

    goto/16 :goto_0

    .line 161
    :pswitch_a
    if-nez v3, :cond_17

    const-string v0, "PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_17
    and-int/lit8 v2, v5, 0x8

    if-eqz v2, :cond_18

    iget-object v1, p0, Liod;->a:Lipu;

    invoke-interface {v1}, Lipu;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-short v1, v1

    :cond_18
    iget-object v2, p0, Liod;->a:Lipu;

    invoke-interface {v2}, Lipu;->i()I

    move-result v2

    const v4, 0x7fffffff

    and-int/2addr v2, v4

    add-int/lit8 v0, v0, -0x4

    invoke-static {v0, v5, v1}, Lioa;->a(IBS)I

    move-result v0

    invoke-direct {p0, v0, v1, v5, v3}, Liod;->a(ISBI)Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Lint;->a(ILjava/util/List;)V

    goto/16 :goto_0

    .line 165
    :pswitch_b
    if-eq v0, v9, :cond_19

    const-string v2, "TYPE_PING length != 8: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_19
    if-eqz v3, :cond_1a

    const-string v0, "TYPE_PING streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1a
    iget-object v0, p0, Liod;->a:Lipu;

    invoke-interface {v0}, Lipu;->i()I

    move-result v0

    iget-object v2, p0, Liod;->a:Lipu;

    invoke-interface {v2}, Lipu;->i()I

    move-result v2

    and-int/lit8 v3, v5, 0x1

    if-eqz v3, :cond_1b

    move v1, v6

    :cond_1b
    invoke-interface {p1, v1, v0, v2}, Lint;->a(ZII)V

    goto/16 :goto_0

    .line 169
    :pswitch_c
    if-ge v0, v9, :cond_1c

    const-string v2, "TYPE_GOAWAY length < 8: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1c
    if-eqz v3, :cond_1d

    const-string v0, "TYPE_GOAWAY streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1d
    iget-object v2, p0, Liod;->a:Lipu;

    invoke-interface {v2}, Lipu;->i()I

    move-result v2

    iget-object v3, p0, Liod;->a:Lipu;

    invoke-interface {v3}, Lipu;->i()I

    move-result v3

    add-int/lit8 v4, v0, -0x8

    invoke-static {v3}, Linr;->b(I)Linr;

    move-result-object v0

    if-nez v0, :cond_1e

    const-string v0, "TYPE_GOAWAY unexpected error code: %d"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1e
    sget-object v0, Lipv;->a:Lipv;

    if-lez v4, :cond_1f

    iget-object v0, p0, Liod;->a:Lipu;

    int-to-long v4, v4

    invoke-interface {v0, v4, v5}, Lipu;->c(J)Lipv;

    move-result-object v0

    :cond_1f
    invoke-interface {p1, v2, v0}, Lint;->a(ILipv;)V

    goto/16 :goto_0

    .line 173
    :pswitch_d
    if-eq v0, v4, :cond_20

    const-string v2, "TYPE_WINDOW_UPDATE length !=4: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_20
    iget-object v0, p0, Liod;->a:Lipu;

    invoke-interface {v0}, Lipu;->i()I

    move-result v0

    int-to-long v4, v0

    const-wide/32 v8, 0x7fffffff

    and-long/2addr v4, v8

    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-nez v0, :cond_21

    const-string v0, "windowSizeIncrement was 0"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_21
    invoke-interface {p1, v3, v4, v5}, Lint;->a(IJ)V

    goto/16 :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 157
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_7
    .end packed-switch
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Liod;->a:Lipu;

    invoke-interface {v0}, Lipu;->close()V

    .line 360
    return-void
.end method
