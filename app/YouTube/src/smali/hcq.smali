.class public final Lhcq;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lhcu;

.field private b:Lhct;

.field private c:Lhcr;

.field private d:Lhcs;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22618
    invoke-direct {p0}, Lidf;-><init>()V

    .line 22991
    sget-object v0, Lhcu;->a:[Lhcu;

    iput-object v0, p0, Lhcq;->a:[Lhcu;

    .line 22994
    iput-object v1, p0, Lhcq;->b:Lhct;

    .line 22997
    iput-object v1, p0, Lhcq;->c:Lhcr;

    .line 23000
    iput-object v1, p0, Lhcq;->d:Lhcs;

    .line 22618
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 23064
    .line 23065
    iget-object v1, p0, Lhcq;->a:[Lhcu;

    if-eqz v1, :cond_1

    .line 23066
    iget-object v2, p0, Lhcq;->a:[Lhcu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 23067
    if-eqz v4, :cond_0

    .line 23068
    const/4 v5, 0x1

    .line 23069
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 23066
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 23073
    :cond_1
    iget-object v1, p0, Lhcq;->b:Lhct;

    if-eqz v1, :cond_2

    .line 23074
    const/4 v1, 0x2

    iget-object v2, p0, Lhcq;->b:Lhct;

    .line 23075
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23077
    :cond_2
    iget-object v1, p0, Lhcq;->c:Lhcr;

    if-eqz v1, :cond_3

    .line 23078
    const/4 v1, 0x3

    iget-object v2, p0, Lhcq;->c:Lhcr;

    .line 23079
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23081
    :cond_3
    iget-object v1, p0, Lhcq;->d:Lhcs;

    if-eqz v1, :cond_4

    .line 23082
    const/4 v1, 0x4

    iget-object v2, p0, Lhcq;->d:Lhcs;

    .line 23083
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23085
    :cond_4
    iget-object v1, p0, Lhcq;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23086
    iput v0, p0, Lhcq;->J:I

    .line 23087
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 22614
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhcq;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhcq;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhcq;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhcq;->a:[Lhcu;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhcu;

    iget-object v3, p0, Lhcq;->a:[Lhcu;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhcq;->a:[Lhcu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhcq;->a:[Lhcu;

    :goto_2
    iget-object v2, p0, Lhcq;->a:[Lhcu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhcq;->a:[Lhcu;

    new-instance v3, Lhcu;

    invoke-direct {v3}, Lhcu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcq;->a:[Lhcu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhcq;->a:[Lhcu;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhcq;->a:[Lhcu;

    new-instance v3, Lhcu;

    invoke-direct {v3}, Lhcu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcq;->a:[Lhcu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhcq;->b:Lhct;

    if-nez v0, :cond_5

    new-instance v0, Lhct;

    invoke-direct {v0}, Lhct;-><init>()V

    iput-object v0, p0, Lhcq;->b:Lhct;

    :cond_5
    iget-object v0, p0, Lhcq;->b:Lhct;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhcq;->c:Lhcr;

    if-nez v0, :cond_6

    new-instance v0, Lhcr;

    invoke-direct {v0}, Lhcr;-><init>()V

    iput-object v0, p0, Lhcq;->c:Lhcr;

    :cond_6
    iget-object v0, p0, Lhcq;->c:Lhcr;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lhcq;->d:Lhcs;

    if-nez v0, :cond_7

    new-instance v0, Lhcs;

    invoke-direct {v0}, Lhcs;-><init>()V

    iput-object v0, p0, Lhcq;->d:Lhcs;

    :cond_7
    iget-object v0, p0, Lhcq;->d:Lhcs;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 23042
    iget-object v0, p0, Lhcq;->a:[Lhcu;

    if-eqz v0, :cond_1

    .line 23043
    iget-object v1, p0, Lhcq;->a:[Lhcu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 23044
    if-eqz v3, :cond_0

    .line 23045
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 23043
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 23049
    :cond_1
    iget-object v0, p0, Lhcq;->b:Lhct;

    if-eqz v0, :cond_2

    .line 23050
    const/4 v0, 0x2

    iget-object v1, p0, Lhcq;->b:Lhct;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 23052
    :cond_2
    iget-object v0, p0, Lhcq;->c:Lhcr;

    if-eqz v0, :cond_3

    .line 23053
    const/4 v0, 0x3

    iget-object v1, p0, Lhcq;->c:Lhcr;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 23055
    :cond_3
    iget-object v0, p0, Lhcq;->d:Lhcs;

    if-eqz v0, :cond_4

    .line 23056
    const/4 v0, 0x4

    iget-object v1, p0, Lhcq;->d:Lhcs;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 23058
    :cond_4
    iget-object v0, p0, Lhcq;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 23060
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 23014
    if-ne p1, p0, :cond_1

    .line 23021
    :cond_0
    :goto_0
    return v0

    .line 23015
    :cond_1
    instance-of v2, p1, Lhcq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 23016
    :cond_2
    check-cast p1, Lhcq;

    .line 23017
    iget-object v2, p0, Lhcq;->a:[Lhcu;

    iget-object v3, p1, Lhcq;->a:[Lhcu;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhcq;->b:Lhct;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhcq;->b:Lhct;

    if-nez v2, :cond_3

    .line 23018
    :goto_1
    iget-object v2, p0, Lhcq;->c:Lhcr;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhcq;->c:Lhcr;

    if-nez v2, :cond_3

    .line 23019
    :goto_2
    iget-object v2, p0, Lhcq;->d:Lhcs;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhcq;->d:Lhcs;

    if-nez v2, :cond_3

    .line 23020
    :goto_3
    iget-object v2, p0, Lhcq;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhcq;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 23021
    goto :goto_0

    .line 23017
    :cond_4
    iget-object v2, p0, Lhcq;->b:Lhct;

    iget-object v3, p1, Lhcq;->b:Lhct;

    .line 23018
    invoke-virtual {v2, v3}, Lhct;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhcq;->c:Lhcr;

    iget-object v3, p1, Lhcq;->c:Lhcr;

    .line 23019
    invoke-virtual {v2, v3}, Lhcr;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhcq;->d:Lhcs;

    iget-object v3, p1, Lhcq;->d:Lhcs;

    .line 23020
    invoke-virtual {v2, v3}, Lhcs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhcq;->I:Ljava/util/List;

    iget-object v3, p1, Lhcq;->I:Ljava/util/List;

    .line 23021
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 23025
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 23027
    iget-object v2, p0, Lhcq;->a:[Lhcu;

    if-nez v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    .line 23033
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhcq;->b:Lhct;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 23034
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcq;->c:Lhcr;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 23035
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcq;->d:Lhcs;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 23036
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhcq;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_3
    add-int/2addr v0, v1

    .line 23037
    return v0

    :cond_1
    move v2, v0

    move v0, v1

    .line 23029
    :goto_4
    iget-object v3, p0, Lhcq;->a:[Lhcu;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 23030
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhcq;->a:[Lhcu;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    .line 23029
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 23030
    :cond_2
    iget-object v2, p0, Lhcq;->a:[Lhcu;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhcu;->hashCode()I

    move-result v2

    goto :goto_5

    .line 23033
    :cond_3
    iget-object v0, p0, Lhcq;->b:Lhct;

    invoke-virtual {v0}, Lhct;->hashCode()I

    move-result v0

    goto :goto_0

    .line 23034
    :cond_4
    iget-object v0, p0, Lhcq;->c:Lhcr;

    invoke-virtual {v0}, Lhcr;->hashCode()I

    move-result v0

    goto :goto_1

    .line 23035
    :cond_5
    iget-object v0, p0, Lhcq;->d:Lhcs;

    invoke-virtual {v0}, Lhcs;->hashCode()I

    move-result v0

    goto :goto_2

    .line 23036
    :cond_6
    iget-object v1, p0, Lhcq;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
