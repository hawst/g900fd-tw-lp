.class public final enum Lfxh;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfxh;

.field private static enum c:Lfxh;

.field private static final synthetic d:[Lfxh;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lfxh;

    const-string v1, "V_2"

    const-string v2, "2"

    invoke-direct {v0, v1, v3, v2}, Lfxh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfxh;->c:Lfxh;

    .line 23
    new-instance v0, Lfxh;

    const-string v1, "V_2_1"

    const-string v2, "2.1"

    invoke-direct {v0, v1, v4, v2}, Lfxh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfxh;->a:Lfxh;

    .line 20
    const/4 v0, 0x2

    new-array v0, v0, [Lfxh;

    sget-object v1, Lfxh;->c:Lfxh;

    aput-object v1, v0, v3

    sget-object v1, Lfxh;->a:Lfxh;

    aput-object v1, v0, v4

    sput-object v0, Lfxh;->d:[Lfxh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput-object p3, p0, Lfxh;->b:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public static a(Ljava/lang/String;)Lfxh;
    .locals 1

    .prologue
    .line 37
    const-string v0, "2.1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    sget-object v0, Lfxh;->a:Lfxh;

    .line 40
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lfxh;->c:Lfxh;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lfxh;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lfxh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfxh;

    return-object v0
.end method

.method public static values()[Lfxh;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lfxh;->d:[Lfxh;

    invoke-virtual {v0}, [Lfxh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfxh;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lfxh;->b:Ljava/lang/String;

    return-object v0
.end method
