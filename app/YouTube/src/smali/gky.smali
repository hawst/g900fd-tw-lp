.class public Lgky;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Future;
.implements Lwv;


# instance fields
.field private final a:Lxz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lxz;

    invoke-direct {v0}, Lxz;-><init>()V

    iput-object v0, p0, Lgky;->a:Lxz;

    .line 33
    return-void
.end method

.method public static a()Lgky;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lgky;

    invoke-direct {v0}, Lgky;-><init>()V

    return-object v0
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgky;->a:Lxz;

    invoke-virtual {v0, p1}, Lxz;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lb;->b()V

    .line 69
    iget-object v0, p0, Lgky;->a:Lxz;

    invoke-virtual {v0}, Lxz;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lb;->b()V

    .line 76
    iget-object v0, p0, Lgky;->a:Lxz;

    invoke-virtual {v0, p1, p2, p3}, Lxz;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lgky;->a:Lxz;

    invoke-virtual {v0}, Lxz;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lgky;->a:Lxz;

    invoke-virtual {v0}, Lxz;->isDone()Z

    move-result v0

    return v0
.end method

.method public onErrorResponse(Lxa;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lgky;->a:Lxz;

    invoke-virtual {v0, p1}, Lxz;->onErrorResponse(Lxa;)V

    .line 44
    return-void
.end method

.method public onResponse(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lgky;->a:Lxz;

    invoke-virtual {v0, p1}, Lxz;->onResponse(Ljava/lang/Object;)V

    .line 49
    return-void
.end method
