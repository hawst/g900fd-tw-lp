.class public final Lhaw;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:[Lhza;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12895
    invoke-direct {p0}, Lidf;-><init>()V

    .line 12898
    sget-object v0, Lhza;->a:[Lhza;

    iput-object v0, p0, Lhaw;->a:[Lhza;

    .line 12895
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 12944
    .line 12945
    iget-object v1, p0, Lhaw;->a:[Lhza;

    if-eqz v1, :cond_1

    .line 12946
    iget-object v2, p0, Lhaw;->a:[Lhza;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 12947
    if-eqz v4, :cond_0

    .line 12948
    const/4 v5, 0x1

    .line 12949
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 12946
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 12953
    :cond_1
    iget-object v1, p0, Lhaw;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12954
    iput v0, p0, Lhaw;->J:I

    .line 12955
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12891
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhaw;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhaw;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhaw;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhaw;->a:[Lhza;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhza;

    iget-object v3, p0, Lhaw;->a:[Lhza;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhaw;->a:[Lhza;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhaw;->a:[Lhza;

    :goto_2
    iget-object v2, p0, Lhaw;->a:[Lhza;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhaw;->a:[Lhza;

    new-instance v3, Lhza;

    invoke-direct {v3}, Lhza;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhaw;->a:[Lhza;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhaw;->a:[Lhza;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhaw;->a:[Lhza;

    new-instance v3, Lhza;

    invoke-direct {v3}, Lhza;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhaw;->a:[Lhza;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 12931
    iget-object v0, p0, Lhaw;->a:[Lhza;

    if-eqz v0, :cond_1

    .line 12932
    iget-object v1, p0, Lhaw;->a:[Lhza;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 12933
    if-eqz v3, :cond_0

    .line 12934
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 12932
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12938
    :cond_1
    iget-object v0, p0, Lhaw;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 12940
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12909
    if-ne p1, p0, :cond_1

    .line 12913
    :cond_0
    :goto_0
    return v0

    .line 12910
    :cond_1
    instance-of v2, p1, Lhaw;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 12911
    :cond_2
    check-cast p1, Lhaw;

    .line 12912
    iget-object v2, p0, Lhaw;->a:[Lhza;

    iget-object v3, p1, Lhaw;->a:[Lhza;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhaw;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhaw;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 12913
    goto :goto_0

    .line 12912
    :cond_4
    iget-object v2, p0, Lhaw;->I:Ljava/util/List;

    iget-object v3, p1, Lhaw;->I:Ljava/util/List;

    .line 12913
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12917
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 12919
    iget-object v2, p0, Lhaw;->a:[Lhza;

    if-nez v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    .line 12925
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhaw;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_0
    add-int/2addr v0, v1

    .line 12926
    return v0

    :cond_1
    move v2, v0

    move v0, v1

    .line 12921
    :goto_1
    iget-object v3, p0, Lhaw;->a:[Lhza;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 12922
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhaw;->a:[Lhza;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    move v2, v1

    :goto_2
    add-int/2addr v2, v3

    .line 12921
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 12922
    :cond_2
    iget-object v2, p0, Lhaw;->a:[Lhza;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhza;->hashCode()I

    move-result v2

    goto :goto_2

    .line 12925
    :cond_3
    iget-object v1, p0, Lhaw;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_0
.end method
