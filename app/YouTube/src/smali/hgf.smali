.class public final Lhgf;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:J

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 49120
    invoke-direct {p0}, Lidf;-><init>()V

    .line 49123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhgf;->a:Z

    .line 49126
    iput-wide v2, p0, Lhgf;->b:J

    .line 49129
    iput-wide v2, p0, Lhgf;->c:J

    .line 49120
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 49178
    const/4 v0, 0x0

    .line 49179
    iget-boolean v1, p0, Lhgf;->a:Z

    if-eqz v1, :cond_0

    .line 49180
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhgf;->a:Z

    .line 49181
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 49183
    :cond_0
    iget-wide v2, p0, Lhgf;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 49184
    const/4 v1, 0x2

    iget-wide v2, p0, Lhgf;->b:J

    .line 49185
    invoke-static {v1, v2, v3}, Lidd;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 49187
    :cond_1
    iget-wide v2, p0, Lhgf;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 49188
    const/4 v1, 0x3

    iget-wide v2, p0, Lhgf;->c:J

    .line 49189
    invoke-static {v1, v2, v3}, Lidd;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 49191
    :cond_2
    iget-object v1, p0, Lhgf;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49192
    iput v0, p0, Lhgf;->J:I

    .line 49193
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 49116
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhgf;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhgf;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhgf;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhgf;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lhgf;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lhgf;->c:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 49163
    iget-boolean v0, p0, Lhgf;->a:Z

    if-eqz v0, :cond_0

    .line 49164
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhgf;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 49166
    :cond_0
    iget-wide v0, p0, Lhgf;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 49167
    const/4 v0, 0x2

    iget-wide v2, p0, Lhgf;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(IJ)V

    .line 49169
    :cond_1
    iget-wide v0, p0, Lhgf;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 49170
    const/4 v0, 0x3

    iget-wide v2, p0, Lhgf;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(IJ)V

    .line 49172
    :cond_2
    iget-object v0, p0, Lhgf;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 49174
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 49142
    if-ne p1, p0, :cond_1

    .line 49148
    :cond_0
    :goto_0
    return v0

    .line 49143
    :cond_1
    instance-of v2, p1, Lhgf;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 49144
    :cond_2
    check-cast p1, Lhgf;

    .line 49145
    iget-boolean v2, p0, Lhgf;->a:Z

    iget-boolean v3, p1, Lhgf;->a:Z

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lhgf;->b:J

    iget-wide v4, p1, Lhgf;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lhgf;->c:J

    iget-wide v4, p1, Lhgf;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhgf;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgf;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 49148
    goto :goto_0

    .line 49145
    :cond_4
    iget-object v2, p0, Lhgf;->I:Ljava/util/List;

    iget-object v3, p1, Lhgf;->I:Ljava/util/List;

    .line 49148
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 49152
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 49154
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lhgf;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 49155
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhgf;->b:J

    iget-wide v4, p0, Lhgf;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 49156
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhgf;->c:J

    iget-wide v4, p0, Lhgf;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 49157
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhgf;->I:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 49158
    return v0

    .line 49154
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 49157
    :cond_1
    iget-object v0, p0, Lhgf;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_1
.end method
