.class public final enum Like;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static enum a:Like;

.field private static enum b:Like;

.field private static enum c:Like;

.field private static enum d:Like;

.field private static final e:[Like;

.field private static final synthetic f:[Like;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Like;

    const-string v1, "L"

    invoke-direct {v0, v1, v2, v3}, Like;-><init>(Ljava/lang/String;II)V

    sput-object v0, Like;->a:Like;

    .line 30
    new-instance v0, Like;

    const-string v1, "M"

    invoke-direct {v0, v1, v3, v2}, Like;-><init>(Ljava/lang/String;II)V

    sput-object v0, Like;->b:Like;

    .line 32
    new-instance v0, Like;

    const-string v1, "Q"

    invoke-direct {v0, v1, v4, v5}, Like;-><init>(Ljava/lang/String;II)V

    sput-object v0, Like;->c:Like;

    .line 34
    new-instance v0, Like;

    const-string v1, "H"

    invoke-direct {v0, v1, v5, v4}, Like;-><init>(Ljava/lang/String;II)V

    sput-object v0, Like;->d:Like;

    .line 25
    new-array v0, v6, [Like;

    sget-object v1, Like;->a:Like;

    aput-object v1, v0, v2

    sget-object v1, Like;->b:Like;

    aput-object v1, v0, v3

    sget-object v1, Like;->c:Like;

    aput-object v1, v0, v4

    sget-object v1, Like;->d:Like;

    aput-object v1, v0, v5

    sput-object v0, Like;->f:[Like;

    .line 36
    new-array v0, v6, [Like;

    sget-object v1, Like;->b:Like;

    aput-object v1, v0, v2

    sget-object v1, Like;->a:Like;

    aput-object v1, v0, v3

    sget-object v1, Like;->d:Like;

    aput-object v1, v0, v4

    sget-object v1, Like;->c:Like;

    aput-object v1, v0, v5

    sput-object v0, Like;->e:[Like;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    return-void
.end method

.method public static a(I)Like;
    .locals 1

    .prologue
    .line 53
    if-ltz p0, :cond_0

    sget-object v0, Like;->e:[Like;

    const/4 v0, 0x4

    if-lt p0, v0, :cond_1

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 56
    :cond_1
    sget-object v0, Like;->e:[Like;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Like;
    .locals 1

    .prologue
    .line 25
    const-class v0, Like;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Like;

    return-object v0
.end method

.method public static values()[Like;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Like;->f:[Like;

    invoke-virtual {v0}, [Like;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Like;

    return-object v0
.end method
