.class final Ldqi;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private synthetic a:Ljava/util/concurrent/CountDownLatch;

.field private synthetic b:Ldqg;


# direct methods
.method constructor <init>(Ldqg;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Ldqi;->b:Ldqg;

    iput-object p3, p0, Ldqi;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 318
    :try_start_0
    iget-object v0, p0, Ldqi;->b:Ldqg;

    invoke-static {v0}, Ldqg;->c(Ldqg;)Ldpo;

    move-result-object v0

    new-instance v1, Ldpu;

    invoke-direct {v1}, Ldpu;-><init>()V

    invoke-virtual {v1}, Ldpu;->a()Ldpt;

    move-result-object v1

    invoke-interface {v0, v1}, Ldpo;->a(Ldpt;)Ldqq;

    move-result-object v0

    .line 319
    iget-object v1, p0, Ldqi;->b:Ldqg;

    invoke-interface {v0}, Ldqq;->b()Z

    move-result v0

    invoke-static {v1, v0}, Ldqg;->d(Ldqg;Z)Z

    .line 320
    iget-object v0, p0, Ldqi;->b:Ldqg;

    iget-object v1, p0, Ldqi;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0, v1}, Ldqg;->a(Ldqg;Ljava/util/concurrent/CountDownLatch;)V
    :try_end_0
    .catch Ldzg; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 346
    :goto_0
    return-void

    .line 323
    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    const-string v0, "IP Address of the phone is: "

    iget-object v1, p0, Ldqi;->b:Ldqg;

    invoke-static {v1}, Ldqg;->i(Ldqg;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 324
    :goto_1
    const-string v0, "java.net.preferIPv4Stack"

    const-string v1, "true"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 325
    const-string v0, "java.net.preferIPv6Addresses"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 326
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "This device suffers from issue 9431 on code.google.com - setting java.net.preferIPv6Addresses to false, java.net.preferIPv4Stack to true"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    iget-object v0, p0, Ldqi;->b:Ldqg;

    invoke-static {v0}, Ldqg;->c(Ldqg;)Ldpo;

    move-result-object v0

    new-instance v1, Ldpu;

    invoke-direct {v1}, Ldpu;-><init>()V

    invoke-virtual {v1}, Ldpu;->a()Ldpt;

    move-result-object v1

    invoke-interface {v0, v1}, Ldpo;->a(Ldpt;)Ldqq;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 332
    :try_start_2
    iget-object v0, p0, Ldqi;->b:Ldqg;

    iget-object v1, p0, Ldqi;->b:Ldqg;

    invoke-static {v1}, Ldqg;->e(Ldqg;)Ldqq;

    move-result-object v1

    invoke-interface {v1}, Ldqq;->b()Z

    move-result v1

    invoke-static {v0, v1}, Ldqg;->d(Ldqg;Z)Z

    .line 333
    iget-object v0, p0, Ldqi;->b:Ldqg;

    iget-object v1, p0, Ldqi;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0, v1}, Ldqg;->a(Ldqg;Ljava/util/concurrent/CountDownLatch;)V
    :try_end_2
    .catch Ldzg; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 339
    :goto_2
    :try_start_3
    invoke-static {}, Ldqg;->c()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 341
    :catch_1
    move-exception v0

    .line 342
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error testing for buffered proxy. Will assume the worst (buffered proxy)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 344
    iget-object v0, p0, Ldqi;->b:Ldqg;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ldqg;->d(Ldqg;Z)Z

    goto :goto_0

    .line 323
    :cond_0
    :try_start_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 335
    :catch_2
    move-exception v0

    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Tough luck, still can\'t connect. The remote control will not work"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    sget-object v0, Ldta;->b:Ldta;

    invoke-virtual {v0}, Ldta;->a()Landroid/content/Intent;

    move-result-object v0

    .line 337
    iget-object v1, p0, Ldqi;->b:Ldqg;

    invoke-static {v1}, Ldqg;->j(Ldqg;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2
.end method
