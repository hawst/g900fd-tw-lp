.class public final Lbgt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcam;
.implements Lcap;


# instance fields
.field public a:Z

.field public final b:Lrz;

.field public final c:Lsb;

.field public final d:Lbgu;

.field public e:Landroid/support/v7/app/MediaRouteButton;

.field public f:Landroid/support/v7/app/MediaRouteButton;

.field private g:Landroid/view/MenuItem;

.field private h:Z

.field private final i:Lmc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lrz;Ldrw;Ldsl;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbgt;->h:Z

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbgt;->a:Z

    .line 53
    invoke-static {p1}, Lsb;->a(Landroid/content/Context;)Lsb;

    move-result-object v0

    iput-object v0, p0, Lbgt;->c:Lsb;

    .line 54
    iput-object p2, p0, Lbgt;->b:Lrz;

    .line 55
    new-instance v0, Lbgu;

    invoke-direct {v0, p0}, Lbgu;-><init>(Lbgt;)V

    iput-object v0, p0, Lbgt;->d:Lbgu;

    .line 56
    new-instance v0, Ldse;

    invoke-direct {v0, p3, p4}, Ldse;-><init>(Ldrw;Ldsl;)V

    iput-object v0, p0, Lbgt;->i:Lmc;

    .line 57
    invoke-virtual {p0, p1}, Lbgt;->a(Landroid/content/Context;)Landroid/support/v7/app/MediaRouteButton;

    move-result-object v0

    iput-object v0, p0, Lbgt;->f:Landroid/support/v7/app/MediaRouteButton;

    .line 58
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbgt;->h:Z

    .line 112
    invoke-virtual {p0}, Lbgt;->b()V

    .line 113
    return-void
.end method

.method public final a(Landroid/content/Context;)Landroid/support/v7/app/MediaRouteButton;
    .locals 3

    .prologue
    .line 61
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400ae

    const/4 v2, 0x0

    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteButton;

    .line 63
    iget-object v1, p0, Lbgt;->b:Lrz;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->a(Lrz;)V

    .line 64
    iget-object v1, p0, Lbgt;->i:Lmc;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->a(Lmc;)V

    .line 65
    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lbgt;->c:Lsb;

    iget-object v1, p0, Lbgt;->d:Lbgu;

    invoke-virtual {v0, v1}, Lsb;->a(Lsc;)V

    .line 127
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 2

    .prologue
    .line 80
    iput-object p1, p0, Lbgt;->g:Landroid/view/MenuItem;

    .line 81
    iget-object v0, p0, Lbgt;->g:Landroid/view/MenuItem;

    invoke-static {v0}, Lez;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteButton;

    iput-object v0, p0, Lbgt;->e:Landroid/support/v7/app/MediaRouteButton;

    .line 82
    iget-object v0, p0, Lbgt;->e:Landroid/support/v7/app/MediaRouteButton;

    iget-object v1, p0, Lbgt;->b:Lrz;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->a(Lrz;)V

    .line 83
    iget-object v0, p0, Lbgt;->e:Landroid/support/v7/app/MediaRouteButton;

    iget-object v1, p0, Lbgt;->i:Lmc;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->a(Lmc;)V

    .line 84
    invoke-virtual {p0}, Lbgt;->b()V

    .line 85
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 138
    iget-object v2, p0, Lbgt;->g:Landroid/view/MenuItem;

    if-nez v2, :cond_0

    .line 152
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v2, p0, Lbgt;->c:Lsb;

    iget-object v2, p0, Lbgt;->b:Lrz;

    invoke-static {v2, v0}, Lsb;->a(Lrz;I)Z

    move-result v2

    .line 145
    iget-boolean v3, p0, Lbgt;->h:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lbgt;->a:Z

    if-nez v3, :cond_3

    :cond_1
    if-eqz v2, :cond_3

    .line 147
    :goto_1
    iget-object v2, p0, Lbgt;->g:Landroid/view/MenuItem;

    if-eqz v2, :cond_2

    .line 148
    iget-object v2, p0, Lbgt;->g:Landroid/view/MenuItem;

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 149
    iget-object v2, p0, Lbgt;->g:Landroid/view/MenuItem;

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 151
    :cond_2
    iget-object v2, p0, Lbgt;->f:Landroid/support/v7/app/MediaRouteButton;

    if-eqz v0, :cond_4

    :goto_2
    invoke-virtual {v2, v1}, Landroid/support/v7/app/MediaRouteButton;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 145
    goto :goto_1

    .line 151
    :cond_4
    const/16 v1, 0x8

    goto :goto_2
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 70
    const v0, 0x7f08036b

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 75
    const v0, 0x7f110004

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    return v0
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbgt;->h:Z

    .line 106
    invoke-virtual {p0}, Lbgt;->b()V

    .line 107
    return-void
.end method
