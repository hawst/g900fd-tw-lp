.class public Lbci;
.super Lbdx;
.source "SourceFile"

# interfaces
.implements Lcji;
.implements Lcjj;
.implements Lfrz;


# instance fields
.field private X:Lari;

.field private Y:Letc;

.field private Z:Lfcv;

.field public a:J

.field private aa:Leyt;

.field private ab:Lezj;

.field private ac:Lerv;

.field private ad:Lfdw;

.field private ae:Landroid/content/SharedPreferences;

.field private af:Lazd;

.field private ag:Lbcl;

.field private ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

.field private aj:Lfuu;

.field private ak:Lcks;

.field private al:Lbvc;

.field private am:Lbvc;

.field private an:Lfuw;

.field private ao:Lcjh;

.field private ap:Ljava/lang/CharSequence;

.field private aq:Lfqg;

.field private ar:Lhog;

.field private as:Lfmz;

.field private at:Ljava/lang/String;

.field private au:Lbrx;

.field private av:Levn;

.field private aw:Z

.field private ax:I

.field private ay:I

.field private az:Lbcm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lbdx;-><init>()V

    .line 741
    return-void
.end method

.method private H()Lhog;
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lbci;->ak:Lcks;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lbci;->ak:Lcks;

    invoke-virtual {v0}, Lcks;->b()Lfmz;

    move-result-object v0

    .line 340
    if-eqz v0, :cond_0

    .line 341
    iget-object v1, v0, Lfmz;->a:Lhxa;

    iget-object v1, v1, Lhxa;->a:Lhog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbci;->as:Lfmz;

    if-eq v1, v0, :cond_0

    .line 343
    iget-object v0, v0, Lfmz;->a:Lhxa;

    iget-object v0, v0, Lhxa;->a:Lhog;

    .line 346
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbci;->ar:Lhog;

    goto :goto_0
.end method

.method private I()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 350
    iput-object v3, p0, Lbci;->W:Lcan;

    .line 353
    invoke-virtual {p0}, Lbci;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    .line 355
    iget-object v0, p0, Lbci;->ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget v1, p0, Lbci;->ax:I

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->e:Lbqk;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lcaj;->a(II)Lcaj;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lbqk;->a(Lbqm;Lbql;)V

    .line 357
    :cond_0
    return-void
.end method

.method private J()V
    .locals 3

    .prologue
    .line 721
    invoke-virtual {p0}, Lbci;->k()Landroid/content/res/Resources;

    move-result-object v0

    .line 722
    const v1, 0x7f0700d4

    .line 723
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const v2, 0x7f0700d5

    .line 724
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 722
    invoke-direct {p0, v1, v0}, Lbci;->a(II)V

    .line 725
    return-void
.end method

.method static synthetic a(Lbci;Ljava/lang/Object;)Landroid/view/View;
    .locals 18

    .prologue
    .line 94
    const/4 v1, 0x0

    move-object/from16 v0, p1

    instance-of v2, v0, Lfiu;

    if-eqz v2, :cond_1

    new-instance v1, Lcbz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbci;->X:Lari;

    invoke-virtual {v3}, Lari;->F()Lffs;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbci;->X:Lari;

    invoke-virtual {v4}, Lari;->aD()Lcst;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lbci;->X:Lari;

    invoke-virtual {v5}, Lari;->aO()Lcub;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lbci;->X:Lari;

    invoke-virtual {v6}, Lari;->c()Leyp;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v7, v7, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbci;->Y:Letc;

    invoke-virtual {v8}, Letc;->i()Levn;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lbci;->X:Lari;

    iget-object v9, v9, Lari;->aj:Lezs;

    invoke-virtual {v9}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lesy;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbci;->aa:Leyt;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbci;->X:Lari;

    invoke-virtual {v11}, Lari;->f()Larh;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lbci;->ad:Lfdw;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbci;->az:Lbcm;

    move-object/from16 v13, p0

    invoke-direct/range {v1 .. v14}, Lcbz;-><init>(Lbhz;Lffs;Lgix;Lcub;Leyp;Lfhz;Levn;Lesy;Leyt;Lcyc;Lfdw;Lfrz;Lcca;)V

    check-cast p1, Lfiu;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcbz;->a(Lfiu;)Landroid/view/View;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lfiu;->a()Lfnc;

    move-result-object v2

    invoke-virtual {v2}, Lfnc;->a()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct/range {p0 .. p0}, Lbci;->J()V

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    move-object/from16 v0, p1

    instance-of v2, v0, Lflv;

    if-eqz v2, :cond_3

    new-instance v1, Lbjq;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbci;->X:Lari;

    invoke-virtual {v3}, Lari;->aD()Lcst;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbci;->X:Lari;

    invoke-virtual {v4}, Lari;->O()Lgng;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lbci;->X:Lari;

    invoke-virtual {v5}, Lari;->aO()Lcub;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lbci;->X:Lari;

    invoke-virtual {v6}, Lari;->ay()Leyt;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lbci;->Y:Letc;

    invoke-virtual {v7}, Letc;->b()Lexd;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lbci;->X:Lari;

    invoke-virtual {v8}, Lari;->P()Lbjx;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f()Lbrz;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v10

    move-object/from16 v11, p0

    invoke-direct/range {v1 .. v11}, Lbjq;-><init>(Landroid/app/Activity;Lgix;Lgng;Lcub;Leyt;Lexd;Lbjx;Lbrz;Lbyg;Lfrz;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->X:Lari;

    invoke-virtual {v2}, Lari;->f()Larh;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lcgo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v4, 0x7f0400d4

    move-object/from16 v0, p0

    iget-object v5, v0, Lbci;->X:Lari;

    invoke-virtual {v5}, Lari;->aD()Lcst;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lbci;->X:Lari;

    invoke-virtual {v6}, Lari;->aO()Lcub;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lbci;->X:Lari;

    invoke-virtual {v7}, Lari;->t()Lfeb;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lbci;->X:Lari;

    invoke-virtual {v8}, Lari;->ay()Leyt;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lbci;->Y:Letc;

    invoke-virtual {v9}, Letc;->i()Levn;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lbci;->X:Lari;

    invoke-virtual {v10}, Lari;->b()Lfxe;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lbci;->X:Lari;

    invoke-virtual {v11}, Lari;->c()Leyp;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v12, v12, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbci;->X:Lari;

    invoke-virtual {v13}, Lari;->O()Lgng;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lbci;->X:Lari;

    invoke-virtual {v14}, Lari;->aD()Lcst;

    move-result-object v14

    invoke-interface {v14}, Lgix;->b()Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-interface {v14}, Lgix;->d()Lgit;

    move-result-object v14

    invoke-virtual {v13, v14}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v13

    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lbci;->X:Lari;

    invoke-virtual {v14}, Lari;->o()Lglm;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lbci;->ad:Lfdw;

    move-object/from16 v16, v0

    move-object v14, v1

    move-object/from16 v17, p0

    invoke-direct/range {v2 .. v17}, Lcgo;-><init>(Lbhz;ILgix;Lcub;Lfeb;Leyt;Levn;Lfxe;Leyp;Lfhz;Lgnd;Lbjq;Lglm;Lfdw;Lfrz;)V

    check-cast p1, Lflv;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcgo;->a(Lflv;)Landroid/view/View;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lbci;->k()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    const v4, 0x7f0700c1

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lbci;->a(II)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v13}, Lgng;->c()Lgnd;

    move-result-object v13

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    invoke-interface {v2}, Lcan;->c()I

    move-result v3

    invoke-interface {v2}, Lcan;->d()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lbci;->a(II)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lbci;Lfis;)Lcjh;
    .locals 9

    .prologue
    .line 94
    invoke-virtual {p1}, Lfis;->b()Ljava/lang/Object;

    move-result-object v7

    instance-of v0, v7, Lfiu;

    if-eqz v0, :cond_0

    new-instance v1, Lcjk;

    iget-object v2, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lbci;->av:Levn;

    move-object v0, v7

    check-cast v0, Lfiu;

    invoke-direct {v1, v2, v3, v0}, Lcjk;-><init>(Landroid/app/Activity;Levn;Lfiu;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, v7, Lflv;

    if-eqz v0, :cond_1

    new-instance v0, Lcjn;

    iget-object v1, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lbci;->X:Lari;

    invoke-virtual {v2}, Lari;->f()Larh;

    move-result-object v2

    iget-object v3, p0, Lbci;->Y:Letc;

    invoke-virtual {v3}, Letc;->i()Levn;

    move-result-object v3

    iget-object v4, p0, Lbci;->X:Lari;

    invoke-virtual {v4}, Lari;->b()Lfxe;

    move-result-object v4

    iget-object v5, p0, Lbci;->X:Lari;

    iget-object v5, v5, Lari;->w:Lezs;

    invoke-virtual {v5}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lffb;

    iget-object v6, p0, Lbci;->X:Lari;

    iget-object v6, v6, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    check-cast v7, Lflv;

    iget-object v8, p0, Lbci;->aa:Leyt;

    invoke-direct/range {v0 .. v8}, Lcjn;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lcyc;Levn;Lfxe;Lffb;Ljava/util/concurrent/atomic/AtomicReference;Lflv;Leyt;)V

    goto :goto_0

    :cond_1
    instance-of v0, v7, Lfjw;

    if-eqz v0, :cond_2

    new-instance v0, Lcjl;

    iget-object v1, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    iget-object v2, p0, Lbci;->Y:Letc;

    invoke-virtual {v2}, Letc;->i()Levn;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcjl;-><init>(Lfhz;Levn;Lfis;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lbci;)Lcks;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->ak:Lcks;

    return-object v0
.end method

.method static synthetic a(Lbci;Lfqg;)Lfqg;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lbci;->aq:Lfqg;

    return-object p1
.end method

.method static synthetic a(Lbci;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lbci;->ap:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic a(Lbci;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lbci;->at:Ljava/lang/String;

    return-object p1
.end method

.method private static a(Lhog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 698
    if-eqz p0, :cond_0

    iget-object v0, p0, Lhog;->c:Lhbm;

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lhog;->c:Lhbm;

    iget-object v0, v0, Lhbm;->a:Ljava/lang/String;

    .line 702
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(II)V
    .locals 0

    .prologue
    .line 706
    iput p1, p0, Lbci;->ax:I

    .line 707
    iput p2, p0, Lbci;->ay:I

    .line 708
    invoke-direct {p0}, Lbci;->I()V

    .line 709
    return-void
.end method

.method static synthetic a(Lbci;Lcjh;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->ao:Lcjh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbci;->ao:Lcjh;

    invoke-virtual {v0}, Lcjh;->b()V

    :cond_0
    iput-object p1, p0, Lbci;->ao:Lcjh;

    if-eqz p1, :cond_1

    iput-object p0, p1, Lcjh;->a:Lcjj;

    iput-object p0, p1, Lcjh;->b:Lcji;

    invoke-virtual {p1}, Lcjh;->a()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lbci;Ljava/util/List;Landroid/view/View;)V
    .locals 13

    .prologue
    .line 94
    if-eqz p2, :cond_3

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lbci;->ak:Lcks;

    invoke-virtual {v1}, Lcks;->a()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v2, v0

    :cond_0
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lfmz;

    invoke-virtual {v9}, Lfmz;->c()Lfmi;

    move-result-object v12

    if-eqz v12, :cond_0

    new-instance v0, Lcew;

    iget-object v1, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lcie;

    invoke-direct {v3}, Lcie;-><init>()V

    iget-object v4, p0, Lbci;->Y:Letc;

    invoke-virtual {v4}, Letc;->i()Levn;

    move-result-object v4

    invoke-direct {v0, v1, v3, v4}, Lcew;-><init>(Landroid/content/Context;Lfsj;Levn;)V

    new-instance v3, Lawk;

    invoke-direct {v3, v0}, Lawk;-><init>(Lcew;)V

    iget-object v0, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400f9

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    if-eqz v2, :cond_4

    const/4 v10, 0x0

    invoke-virtual {v1, p2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    :goto_2
    new-instance v0, Lfvh;

    new-instance v2, Lfsc;

    invoke-direct {v2}, Lfsc;-><init>()V

    iget-object v4, p0, Lbci;->Z:Lfcv;

    iget-object v5, p0, Lbci;->Y:Letc;

    invoke-virtual {v5}, Letc;->i()Levn;

    move-result-object v5

    iget-object v6, p0, Lbci;->an:Lfuw;

    iget-object v7, p0, Lbci;->aa:Leyt;

    iget-object v8, p0, Lbci;->aj:Lfuu;

    invoke-virtual {v8}, Lfuu;->b()Lfsl;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lfvh;-><init>(Landroid/widget/ListView;Lfsc;Lfvd;Lfdg;Levn;Lfuw;Leyt;Lfsl;)V

    invoke-virtual {v0, v12}, Lfvh;->a(Lfmi;)V

    iget-object v1, p0, Lbci;->ak:Lcks;

    iget-object v2, v1, Lcks;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v1, Lcks;->b:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lfvh;->c:Landroid/widget/ListView;

    invoke-virtual {v9}, Lfmz;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v1, Lcks;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v3, v9, Lfmz;->a:Lhxa;

    iget-object v3, v3, Lhxa;->e:Lhiq;

    iget v3, v3, Lhiq;->a:I

    invoke-static {v3}, Lbrf;->a(I)I

    move-result v3

    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    iget-object v5, v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    const v6, 0x7f010112

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v3, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget v3, v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:I

    const/4 v5, 0x0

    iget v6, v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:I

    const/4 v7, 0x0

    invoke-virtual {v4, v3, v5, v6, v7}, Landroid/widget/ImageView;->setPadding(IIII)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x1

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v3, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v4, v3, v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Landroid/view/View;Landroid/widget/LinearLayout$LayoutParams;Landroid/view/View;)V

    :goto_3
    iget-object v0, v9, Lfmz;->a:Lhxa;

    iget-boolean v0, v0, Lhxa;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcks;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v1, v1, Lcks;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(IZ)V

    :cond_1
    iget-object v0, p0, Lbci;->au:Lbrx;

    if-nez v0, :cond_2

    iget-object v0, p0, Lbci;->ae:Landroid/content/SharedPreferences;

    const-string v1, "show_music_what_to_watch_tutorial"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v9, Lfmz;->a:Lhxa;

    iget-object v0, v0, Lhxa;->a:Lhog;

    invoke-static {v0}, Lbci;->a(Lhog;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FEmusic"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbci;->ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v1, p0, Lbci;->ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lbrx;

    iget-object v2, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B()Lbxu;

    move-result-object v3

    iget-object v4, p0, Lbci;->ae:Landroid/content/SharedPreferences;

    invoke-direct {v1, v2, v3, v4, v0}, Lbrx;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lbxu;Landroid/content/SharedPreferences;Landroid/view/View;)V

    iput-object v1, p0, Lbci;->au:Lbrx;

    iget-object v0, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B()Lbxu;

    move-result-object v0

    iget-object v1, p0, Lbci;->au:Lbrx;

    invoke-virtual {v0, v1}, Lbxu;->a(Lbxy;)V

    :cond_2
    move v2, v10

    goto/16 :goto_1

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    const v4, 0x7f0400d3

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    move v10, v2

    goto/16 :goto_2

    :cond_5
    iget-object v2, v1, Lcks;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v3, v9, Lfmz;->a:Lhxa;

    iget-object v3, v3, Lhxa;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Ljava/lang/CharSequence;Landroid/view/View;)V

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lbci;->ak:Lcks;

    invoke-virtual {v0}, Lcks;->b()Lfmz;

    move-result-object v0

    iput-object v0, p0, Lbci;->as:Lfmz;

    return-void
.end method

.method static synthetic a(Lbci;Lmi;)V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lbci;->k()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {p1, v0}, La;->a(Lmi;I)I

    move-result v0

    invoke-static {v0}, La;->a(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lbci;->a(II)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 393
    iget-object v0, p0, Lbci;->ar:Lhog;

    invoke-static {v0}, Lbbp;->a(Lhog;)[B

    move-result-object v0

    .line 394
    iget-object v1, p0, Lbci;->Z:Lfcv;

    invoke-virtual {v1}, Lfcv;->a()Lfcy;

    move-result-object v1

    .line 396
    invoke-virtual {v1, v0}, Lfcy;->a([B)V

    .line 397
    iget-object v0, p0, Lbci;->ar:Lhog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbci;->ar:Lhog;

    iget-object v0, v0, Lhog;->c:Lhbm;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lbci;->ar:Lhog;

    invoke-static {v0}, Lbci;->a(Lhog;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfcy;->a(Ljava/lang/String;)Lfcy;

    .line 399
    iget-object v0, p0, Lbci;->ar:Lhog;

    iget-object v0, v0, Lhog;->c:Lhbm;

    iget-object v0, v0, Lhbm;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lfcy;->b(Ljava/lang/String;)Lfcy;

    .line 400
    if-eqz p1, :cond_0

    .line 401
    sget-object v0, Lfsq;->b:Lfsq;

    invoke-virtual {v1, v0}, Lfcy;->a(Lfsq;)V

    .line 405
    :cond_0
    iget-object v0, p0, Lbci;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    iget-object v0, p0, Lbci;->au:Lbrx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B()Lbxu;

    move-result-object v0

    iget-object v2, p0, Lbci;->au:Lbrx;

    invoke-virtual {v0, v2}, Lbxu;->b(Lbxy;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbci;->au:Lbrx;

    .line 407
    :cond_1
    iget-object v0, p0, Lbci;->Y:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    .line 408
    new-instance v2, Lazt;

    invoke-direct {v2}, Lazt;-><init>()V

    invoke-virtual {v0, v2}, Levn;->d(Ljava/lang/Object;)V

    .line 410
    iget-object v0, p0, Lbci;->Z:Lfcv;

    iget-object v2, p0, Lbci;->ag:Lbcl;

    invoke-virtual {v0, v1, v2}, Lfcv;->a(Lfcy;Lwv;)V

    .line 411
    return-void
.end method

.method static synthetic a(Lbci;Z)Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbci;->aw:Z

    return v0
.end method

.method static synthetic b(Lbci;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lbci;->I()V

    return-void
.end method

.method static synthetic b(Lbci;Z)V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbci;->a(Z)V

    return-void
.end method

.method static synthetic c(Lbci;)Letc;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->Y:Letc;

    return-object v0
.end method

.method static synthetic d(Lbci;)Lhog;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->ar:Lhog;

    return-object v0
.end method

.method static synthetic e(Lbci;)Lfqg;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->aq:Lfqg;

    return-object v0
.end method

.method static synthetic f(Lbci;)Lfdw;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->ad:Lfdw;

    return-object v0
.end method

.method static synthetic g(Lbci;)Levn;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->av:Levn;

    return-object v0
.end method

.method static synthetic h(Lbci;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v0
.end method

.method private handlePaidContentTransactionCompleteEvent(Lbml;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 689
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbci;->a(Z)V

    .line 690
    return-void
.end method

.method static synthetic i(Lbci;)Lezj;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->ab:Lezj;

    return-object v0
.end method

.method static synthetic j(Lbci;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 94
    iget-object v1, p0, Lbci;->af:Lazd;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbci;->af:Lazd;

    invoke-virtual {v1}, Lazd;->a()V

    iput-object v0, p0, Lbci;->af:Lazd;

    :cond_0
    iget-object v1, p0, Lbci;->ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbci;->ak:Lcks;

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lbci;->ak:Lcks;

    iget-object v2, v1, Lcks;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget v2, v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    iget-object v3, v1, Lcks;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    if-gez v2, :cond_4

    :cond_3
    :goto_1
    if-eqz v0, :cond_1

    new-instance v1, Lazd;

    iget-object v2, p0, Lbci;->Y:Letc;

    invoke-virtual {v2}, Letc;->i()Levn;

    move-result-object v2

    iget-object v3, p0, Lbci;->ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-direct {v1, v2, v3, v0}, Lazd;-><init>(Levn;Lcom/google/android/apps/youtube/app/ui/TabbedView;Lfvh;)V

    iput-object v1, p0, Lbci;->af:Lazd;

    iget-object v0, p0, Lbci;->af:Lazd;

    iget-object v1, v0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-object v1, v0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lazd;->b:Lfvh;

    invoke-virtual {v1, v0}, Lfvh;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-static {v0}, Leyi;->a(Leyo;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lazd;->c:Z

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, v0, Lazd;->d:Ljava/util/HashSet;

    goto :goto_0

    :cond_4
    iget-object v0, v1, Lcks;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvh;

    goto :goto_1
.end method

.method static synthetic k(Lbci;)Leyt;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbci;->aa:Leyt;

    return-object v0
.end method

.method static synthetic l(Lbci;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lbci;->J()V

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 3

    .prologue
    .line 680
    iget-object v0, p0, Lbci;->ak:Lcks;

    iget-object v1, v0, Lcks;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget v1, v1, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    iget-object v2, v0, Lcks;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    if-gez v1, :cond_1

    .line 681
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    iget-object v0, v0, Lcks;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvh;

    invoke-virtual {v0}, Lfvh;->h()V

    goto :goto_0
.end method

.method public final B()Lfqg;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lbci;->aq:Lfqg;

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 19

    .prologue
    .line 172
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lbci;->aq:Lfqg;

    .line 173
    const v2, 0x7f04002d

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lbci;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v3, Lbco;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lbco;-><init>(Lbci;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lfve;)V

    .line 179
    new-instance v2, Lfuu;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbci;->X:Lari;

    .line 181
    invoke-virtual {v4}, Lari;->f()Larh;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 182
    iget-object v5, v5, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbci;->Y:Letc;

    .line 183
    invoke-virtual {v6}, Letc;->i()Levn;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lbci;->X:Lari;

    .line 184
    invoke-virtual {v7}, Lari;->c()Leyp;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lbci;->X:Lari;

    .line 185
    invoke-virtual {v8}, Lari;->aD()Lcst;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lbci;->X:Lari;

    .line 186
    invoke-virtual {v9}, Lari;->aO()Lcub;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lbci;->ac:Lerv;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbci;->X:Lari;

    .line 188
    invoke-virtual {v11}, Lari;->F()Lffs;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lbci;->X:Lari;

    .line 189
    iget-object v12, v12, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbci;->aa:Leyt;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbci;->al:Lbvc;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbci;->am:Lbvc;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbci;->ad:Lfdw;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v17, v0

    .line 195
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->j:Lfun;

    move-object/from16 v18, v0

    move-object/from16 v17, p0

    invoke-direct/range {v2 .. v18}, Lfuu;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lcyc;Lfhz;Levn;Leyp;Lgix;Lcub;Lerv;Lffs;Ljava/util/concurrent/atomic/AtomicReference;Leyt;Lbvc;Lbvc;Lfdw;Lfrz;Lfun;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lbci;->aj:Lfuu;

    .line 196
    new-instance v2, Lbwo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 198
    iget-object v4, v4, Lbhz;->m:Lfus;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbci;->X:Lari;

    .line 199
    invoke-virtual {v5}, Lari;->z()Lfcv;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lbci;->Y:Letc;

    .line 200
    invoke-virtual {v6}, Letc;->i()Levn;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lbci;->aj:Lfuu;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbci;->aa:Leyt;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbci;->X:Lari;

    .line 204
    invoke-virtual {v8}, Lari;->aD()Lcst;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v8, v0, Lbci;->X:Lari;

    .line 205
    invoke-virtual {v8}, Lari;->ah()Lckt;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v8, v0, Lbci;->X:Lari;

    .line 206
    invoke-virtual {v8}, Lari;->aj()Lffw;

    move-result-object v12

    new-instance v13, Lbcj;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lbcj;-><init>(Lbci;)V

    move-object/from16 v8, p0

    invoke-direct/range {v2 .. v13}, Lbwo;-><init>(Landroid/app/Activity;Lfus;Lfdg;Levn;Lfuu;Lfrz;Leyt;Lgix;Lckt;Lffw;Lful;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lbci;->an:Lfuw;

    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g:Lcak;

    .line 215
    iget-object v3, v2, Lcak;->e:Lcaj;

    iget v3, v3, Lcaj;->c:I

    move-object/from16 v0, p0

    iput v3, v0, Lbci;->ax:I

    .line 216
    iget-object v2, v2, Lcak;->e:Lcaj;

    iget v2, v2, Lcaj;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lbci;->ay:I

    .line 217
    if-eqz p3, :cond_0

    .line 218
    const-string v2, "action_bar_color"

    move-object/from16 v0, p0

    iget v3, v0, Lbci;->ax:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lbci;->ax:I

    .line 219
    const-string v2, "status_bar_color"

    move-object/from16 v0, p0

    iget v3, v0, Lbci;->ay:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lbci;->ay:I

    .line 222
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const v3, 0x7f0800fe

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lbci;->ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    move-object/from16 v0, p0

    iget v3, v0, Lbci;->ax:I

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;->e:Lbqk;

    const/4 v4, -0x1

    invoke-static {v3, v4}, Lcaj;->a(II)Lcaj;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbqk;->a(Lbqm;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    new-instance v3, Lbck;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lbck;-><init>(Lbci;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Lbxn;)V

    .line 237
    new-instance v2, Lcks;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbci;->ai:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-direct {v2, v3}, Lcks;-><init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lbci;->ak:Lcks;

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->ak:Lcks;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lbci;->a(Lezl;)V

    .line 240
    if-eqz p3, :cond_3

    .line 241
    :goto_0
    const-string v2, "navigation_endpoint"

    .line 242
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    .line 241
    invoke-static {v2}, Lfia;->a([B)Lhog;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbci;->ar:Lhog;

    .line 243
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbci;->aw:Z

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->ar:Lhog;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->ar:Lhog;

    iget-object v2, v2, Lhog;->c:Lhbm;

    if-nez v2, :cond_2

    .line 246
    :cond_1
    const-string v2, "Browse Fragment was given a navigation endpoint without browse data."

    invoke-static {v2}, Lezp;->b(Ljava/lang/String;)V

    .line 249
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lbci;->ah:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v2

    .line 240
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lbci;->h()Landroid/os/Bundle;

    move-result-object p3

    goto :goto_0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lbci;->ap:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 140
    invoke-super {p0, p1}, Lbdx;->a(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0}, Lbci;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    iput-object v0, p0, Lbci;->X:Lari;

    .line 143
    invoke-virtual {p0}, Lbci;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    iget-object v0, v0, Lckz;->a:Letc;

    iput-object v0, p0, Lbci;->Y:Letc;

    .line 144
    iget-object v0, p0, Lbci;->X:Lari;

    invoke-virtual {v0}, Lari;->ay()Leyt;

    move-result-object v0

    iput-object v0, p0, Lbci;->aa:Leyt;

    .line 145
    iget-object v0, p0, Lbci;->X:Lari;

    invoke-virtual {v0}, Lari;->z()Lfcv;

    move-result-object v0

    iput-object v0, p0, Lbci;->Z:Lfcv;

    .line 146
    iget-object v0, p0, Lbci;->X:Lari;

    invoke-virtual {v0}, Lari;->ae()Lfdw;

    move-result-object v0

    iput-object v0, p0, Lbci;->ad:Lfdw;

    .line 147
    iget-object v0, p0, Lbci;->Y:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lbci;->ae:Landroid/content/SharedPreferences;

    .line 148
    iget-object v0, p0, Lbci;->Y:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    iput-object v0, p0, Lbci;->av:Levn;

    .line 149
    iget-object v0, p0, Lbci;->Y:Letc;

    invoke-virtual {v0}, Letc;->f()Lezj;

    move-result-object v0

    iput-object v0, p0, Lbci;->ab:Lezj;

    .line 152
    new-instance v0, Lerv;

    iget-object v1, p0, Lbci;->X:Lari;

    invoke-virtual {v1}, Lari;->C()Lgjp;

    move-result-object v1

    invoke-direct {v0, v1}, Lerv;-><init>(Lgjp;)V

    iput-object v0, p0, Lbci;->ac:Lerv;

    .line 154
    iget-object v0, p0, Lbci;->X:Lari;

    .line 156
    invoke-virtual {v0}, Lari;->K()Ldsn;

    move-result-object v0

    iget-object v1, p0, Lbci;->aa:Leyt;

    iget-object v2, p0, Lbci;->X:Lari;

    .line 158
    iget-object v2, v2, Lari;->b:Ldov;

    invoke-virtual {v2}, Ldov;->g()Ldaq;

    move-result-object v2

    .line 155
    invoke-static {v0, v1, v2}, Lbvc;->a(Ldsn;Leyt;Ldaq;)Lbvc;

    move-result-object v0

    iput-object v0, p0, Lbci;->al:Lbvc;

    .line 159
    iget-object v0, p0, Lbci;->X:Lari;

    .line 161
    invoke-virtual {v0}, Lari;->K()Ldsn;

    move-result-object v0

    iget-object v1, p0, Lbci;->aa:Leyt;

    iget-object v2, p0, Lbci;->X:Lari;

    .line 163
    iget-object v2, v2, Lari;->b:Ldov;

    invoke-virtual {v2}, Ldov;->g()Ldaq;

    move-result-object v2

    .line 160
    invoke-static {v0, v1, v2}, Lbvc;->b(Ldsn;Leyt;Ldaq;)Lbvc;

    move-result-object v0

    iput-object v0, p0, Lbci;->am:Lbvc;

    .line 165
    new-instance v0, Lbcl;

    invoke-direct {v0, p0}, Lbcl;-><init>(Lbci;)V

    iput-object v0, p0, Lbci;->ag:Lbcl;

    .line 166
    new-instance v0, Lbcm;

    invoke-direct {v0, p0}, Lbcm;-><init>(Lbci;)V

    iput-object v0, p0, Lbci;->az:Lbcm;

    .line 167
    return-void
.end method

.method public final b()Lcan;
    .locals 5

    .prologue
    .line 366
    iget-object v0, p0, Lbci;->W:Lcan;

    if-nez v0, :cond_2

    .line 367
    iget-object v0, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    invoke-virtual {v0}, Lcaq;->h()Lcar;

    move-result-object v1

    .line 368
    iget-object v0, p0, Lbci;->ap:Ljava/lang/CharSequence;

    iput-object v0, v1, Lcar;->a:Ljava/lang/CharSequence;

    iget v0, p0, Lbci;->ax:I

    .line 369
    iput v0, v1, Lcar;->c:I

    iget v0, p0, Lbci;->ay:I

    .line 370
    iput v0, v1, Lcar;->d:I

    iget-object v0, p0, Lbci;->ao:Lcjh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbci;->ao:Lcjh;

    .line 372
    invoke-virtual {v0}, Lcjh;->c()Ljava/util/Collection;

    move-result-object v0

    .line 371
    :goto_0
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcam;

    iget-object v3, v1, Lcar;->g:Ljava/util/LinkedHashMap;

    invoke-interface {v0}, Lcam;->g()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 373
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 374
    :cond_1
    invoke-virtual {v1}, Lcar;->a()Lcaq;

    move-result-object v0

    iput-object v0, p0, Lbci;->W:Lcan;

    .line 376
    :cond_2
    iget-object v0, p0, Lbci;->W:Lcan;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lbci;->at:Ljava/lang/String;

    return-object v0
.end method

.method public final e()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 254
    invoke-super {p0}, Lbdx;->e()V

    .line 255
    iget-boolean v0, p0, Lbci;->aw:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbci;->ab:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lbci;->a:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 256
    invoke-direct {p0, v1}, Lbci;->a(Z)V

    .line 258
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 255
    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 315
    invoke-super {p0, p1}, Lbdx;->e(Landroid/os/Bundle;)V

    .line 316
    const-string v0, "navigation_endpoint"

    .line 318
    invoke-direct {p0}, Lbci;->H()Lhog;

    move-result-object v1

    invoke-static {v1}, Lidh;->a(Lidh;)[B

    move-result-object v1

    .line 316
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 319
    const-string v0, "action_bar_color"

    iget v1, p0, Lbci;->ax:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 320
    const-string v0, "status_bar_color"

    iget v1, p0, Lbci;->ay:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 321
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 266
    invoke-super {p0}, Lbdx;->f()V

    .line 268
    invoke-direct {p0}, Lbci;->H()Lhog;

    move-result-object v0

    iput-object v0, p0, Lbci;->ar:Lhog;

    .line 269
    return-void
.end method

.method public final n_()V
    .locals 1

    .prologue
    .line 675
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbci;->a(Z)V

    .line 676
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 325
    invoke-super {p0, p1}, Lbdx;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 326
    iget-object v0, p0, Lbci;->ak:Lcks;

    iget-object v0, v0, Lcks;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvh;

    invoke-virtual {v0, p1}, Lfvh;->a(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 327
    :cond_0
    return-void
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 273
    invoke-super {p0}, Lbdx;->t()V

    .line 274
    iget-object v0, p0, Lbci;->av:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 275
    iget-object v0, p0, Lbci;->ao:Lcjh;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lbci;->ao:Lcjh;

    invoke-virtual {v0}, Lcjh;->a()V

    .line 279
    :cond_0
    iget-object v0, p0, Lbci;->al:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    .line 280
    iget-object v0, p0, Lbci;->am:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    .line 281
    return-void
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 285
    invoke-super {p0}, Lbdx;->u()V

    .line 286
    iget-object v0, p0, Lbci;->av:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 287
    iget-object v0, p0, Lbci;->ao:Lcjh;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lbci;->ao:Lcjh;

    invoke-virtual {v0}, Lcjh;->b()V

    .line 290
    :cond_0
    iget-object v0, p0, Lbci;->af:Lazd;

    if-eqz v0, :cond_1

    .line 291
    iget-object v0, p0, Lbci;->af:Lazd;

    invoke-virtual {v0}, Lazd;->a()V

    .line 292
    const/4 v0, 0x0

    iput-object v0, p0, Lbci;->af:Lazd;

    .line 295
    :cond_1
    iget-object v0, p0, Lbci;->az:Lbcm;

    invoke-virtual {v0}, Lbcm;->b()V

    .line 296
    return-void
.end method

.method public final v()V
    .locals 2

    .prologue
    .line 300
    invoke-super {p0}, Lbdx;->v()V

    .line 301
    iget-object v0, p0, Lbci;->al:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 302
    iget-object v0, p0, Lbci;->am:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 304
    iget-object v0, p0, Lbci;->au:Lbrx;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lbci;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B()Lbxu;

    move-result-object v0

    iget-object v1, p0, Lbci;->au:Lbrx;

    invoke-virtual {v0, v1}, Lbxu;->b(Lbxy;)V

    .line 308
    :cond_0
    iget-object v0, p0, Lbci;->ag:Lbcl;

    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lbci;->ag:Lbcl;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lbcl;->a:Z

    .line 311
    :cond_1
    return-void
.end method
