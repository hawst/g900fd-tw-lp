.class public final Lhgy;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhgy;


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I

.field public g:I

.field public h:Lhtk;

.field public i:Lhtk;

.field public j:J

.field public k:J

.field public l:Z

.field public m:Z

.field public n:I

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:Ljava/lang/String;

.field public r:[I

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:I

.field private v:I

.field private w:I

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53556
    const/4 v0, 0x0

    new-array v0, v0, [Lhgy;

    sput-object v0, Lhgy;->a:[Lhgy;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 53557
    invoke-direct {p0}, Lidf;-><init>()V

    .line 53572
    iput v1, p0, Lhgy;->b:I

    .line 53575
    const-string v0, ""

    iput-object v0, p0, Lhgy;->c:Ljava/lang/String;

    .line 53578
    const-string v0, ""

    iput-object v0, p0, Lhgy;->s:Ljava/lang/String;

    .line 53581
    const-string v0, ""

    iput-object v0, p0, Lhgy;->t:Ljava/lang/String;

    .line 53584
    const-string v0, ""

    iput-object v0, p0, Lhgy;->d:Ljava/lang/String;

    .line 53587
    iput v1, p0, Lhgy;->e:I

    .line 53590
    iput v1, p0, Lhgy;->f:I

    .line 53593
    iput v1, p0, Lhgy;->g:I

    .line 53596
    iput-object v2, p0, Lhgy;->h:Lhtk;

    .line 53599
    iput-object v2, p0, Lhgy;->i:Lhtk;

    .line 53602
    iput-wide v4, p0, Lhgy;->j:J

    .line 53605
    iput-wide v4, p0, Lhgy;->k:J

    .line 53608
    iput v1, p0, Lhgy;->u:I

    .line 53611
    iput v1, p0, Lhgy;->v:I

    .line 53614
    iput v1, p0, Lhgy;->w:I

    .line 53617
    const-string v0, ""

    iput-object v0, p0, Lhgy;->x:Ljava/lang/String;

    .line 53620
    iput-boolean v1, p0, Lhgy;->l:Z

    .line 53623
    iput-boolean v1, p0, Lhgy;->m:Z

    .line 53626
    iput v1, p0, Lhgy;->n:I

    .line 53629
    const-string v0, ""

    iput-object v0, p0, Lhgy;->y:Ljava/lang/String;

    .line 53632
    const-string v0, ""

    iput-object v0, p0, Lhgy;->o:Ljava/lang/String;

    .line 53635
    iput-boolean v1, p0, Lhgy;->p:Z

    .line 53638
    const-string v0, ""

    iput-object v0, p0, Lhgy;->q:Ljava/lang/String;

    .line 53641
    sget-object v0, Lidj;->a:[I

    iput-object v0, p0, Lhgy;->r:[I

    .line 53644
    iput v1, p0, Lhgy;->z:I

    .line 53647
    const-string v0, ""

    iput-object v0, p0, Lhgy;->A:Ljava/lang/String;

    .line 53650
    iput v1, p0, Lhgy;->B:I

    .line 53557
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 53850
    .line 53851
    iget v0, p0, Lhgy;->b:I

    if-eqz v0, :cond_1b

    .line 53852
    const/4 v0, 0x1

    iget v2, p0, Lhgy;->b:I

    .line 53853
    invoke-static {v0, v2}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 53855
    :goto_0
    iget-object v2, p0, Lhgy;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 53856
    const/4 v2, 0x2

    iget-object v3, p0, Lhgy;->c:Ljava/lang/String;

    .line 53857
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53859
    :cond_0
    iget-object v2, p0, Lhgy;->s:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 53860
    const/4 v2, 0x3

    iget-object v3, p0, Lhgy;->s:Ljava/lang/String;

    .line 53861
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53863
    :cond_1
    iget-object v2, p0, Lhgy;->t:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 53864
    const/4 v2, 0x4

    iget-object v3, p0, Lhgy;->t:Ljava/lang/String;

    .line 53865
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53867
    :cond_2
    iget-object v2, p0, Lhgy;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 53868
    const/4 v2, 0x5

    iget-object v3, p0, Lhgy;->d:Ljava/lang/String;

    .line 53869
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53871
    :cond_3
    iget v2, p0, Lhgy;->e:I

    if-eqz v2, :cond_4

    .line 53872
    const/4 v2, 0x6

    iget v3, p0, Lhgy;->e:I

    .line 53873
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 53875
    :cond_4
    iget v2, p0, Lhgy;->f:I

    if-eqz v2, :cond_5

    .line 53876
    const/4 v2, 0x7

    iget v3, p0, Lhgy;->f:I

    .line 53877
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 53879
    :cond_5
    iget v2, p0, Lhgy;->g:I

    if-eqz v2, :cond_6

    .line 53880
    const/16 v2, 0x8

    iget v3, p0, Lhgy;->g:I

    .line 53881
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 53883
    :cond_6
    iget-object v2, p0, Lhgy;->h:Lhtk;

    if-eqz v2, :cond_7

    .line 53884
    const/16 v2, 0x9

    iget-object v3, p0, Lhgy;->h:Lhtk;

    .line 53885
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53887
    :cond_7
    iget-object v2, p0, Lhgy;->i:Lhtk;

    if-eqz v2, :cond_8

    .line 53888
    const/16 v2, 0xa

    iget-object v3, p0, Lhgy;->i:Lhtk;

    .line 53889
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53891
    :cond_8
    iget-wide v2, p0, Lhgy;->j:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_9

    .line 53892
    const/16 v2, 0xb

    iget-wide v4, p0, Lhgy;->j:J

    .line 53893
    invoke-static {v2, v4, v5}, Lidd;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 53895
    :cond_9
    iget-wide v2, p0, Lhgy;->k:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_a

    .line 53896
    const/16 v2, 0xc

    iget-wide v4, p0, Lhgy;->k:J

    .line 53897
    invoke-static {v2, v4, v5}, Lidd;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 53899
    :cond_a
    iget v2, p0, Lhgy;->u:I

    if-eqz v2, :cond_b

    .line 53900
    const/16 v2, 0xd

    iget v3, p0, Lhgy;->u:I

    .line 53901
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 53903
    :cond_b
    iget v2, p0, Lhgy;->v:I

    if-eqz v2, :cond_c

    .line 53904
    const/16 v2, 0xe

    iget v3, p0, Lhgy;->v:I

    .line 53905
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 53907
    :cond_c
    iget v2, p0, Lhgy;->w:I

    if-eqz v2, :cond_d

    .line 53908
    const/16 v2, 0xf

    iget v3, p0, Lhgy;->w:I

    .line 53909
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 53911
    :cond_d
    iget-object v2, p0, Lhgy;->x:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 53912
    const/16 v2, 0x10

    iget-object v3, p0, Lhgy;->x:Ljava/lang/String;

    .line 53913
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53915
    :cond_e
    iget-boolean v2, p0, Lhgy;->l:Z

    if-eqz v2, :cond_f

    .line 53916
    const/16 v2, 0x11

    iget-boolean v3, p0, Lhgy;->l:Z

    .line 53917
    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 53919
    :cond_f
    iget-boolean v2, p0, Lhgy;->m:Z

    if-eqz v2, :cond_10

    .line 53920
    const/16 v2, 0x12

    iget-boolean v3, p0, Lhgy;->m:Z

    .line 53921
    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 53923
    :cond_10
    iget v2, p0, Lhgy;->n:I

    if-eqz v2, :cond_11

    .line 53924
    const/16 v2, 0x13

    iget v3, p0, Lhgy;->n:I

    .line 53925
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 53927
    :cond_11
    iget-object v2, p0, Lhgy;->y:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 53928
    const/16 v2, 0x14

    iget-object v3, p0, Lhgy;->y:Ljava/lang/String;

    .line 53929
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53931
    :cond_12
    iget-object v2, p0, Lhgy;->o:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 53932
    const/16 v2, 0x15

    iget-object v3, p0, Lhgy;->o:Ljava/lang/String;

    .line 53933
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53935
    :cond_13
    iget-boolean v2, p0, Lhgy;->p:Z

    if-eqz v2, :cond_14

    .line 53936
    const/16 v2, 0x16

    iget-boolean v3, p0, Lhgy;->p:Z

    .line 53937
    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 53939
    :cond_14
    iget-object v2, p0, Lhgy;->q:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 53940
    const/16 v2, 0x17

    iget-object v3, p0, Lhgy;->q:Ljava/lang/String;

    .line 53941
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53943
    :cond_15
    iget-object v2, p0, Lhgy;->r:[I

    if-eqz v2, :cond_17

    iget-object v2, p0, Lhgy;->r:[I

    array-length v2, v2

    if-lez v2, :cond_17

    .line 53945
    iget-object v3, p0, Lhgy;->r:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_16

    aget v5, v3, v1

    .line 53947
    invoke-static {v5}, Lidd;->a(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 53945
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 53949
    :cond_16
    add-int/2addr v0, v2

    .line 53950
    iget-object v1, p0, Lhgy;->r:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 53952
    :cond_17
    iget v1, p0, Lhgy;->z:I

    if-eqz v1, :cond_18

    .line 53953
    const/16 v1, 0x19

    iget v2, p0, Lhgy;->z:I

    .line 53954
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 53956
    :cond_18
    iget-object v1, p0, Lhgy;->A:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 53957
    const/16 v1, 0x1a

    iget-object v2, p0, Lhgy;->A:Ljava/lang/String;

    .line 53958
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53960
    :cond_19
    iget v1, p0, Lhgy;->B:I

    if-eqz v1, :cond_1a

    .line 53961
    const/16 v1, 0x1b

    iget v2, p0, Lhgy;->B:I

    .line 53962
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 53964
    :cond_1a
    iget-object v1, p0, Lhgy;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53965
    iput v0, p0, Lhgy;->J:I

    .line 53966
    return v0

    :cond_1b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53553
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhgy;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhgy;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhgy;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhgy;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgy;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgy;->s:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgy;->t:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgy;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhgy;->e:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhgy;->f:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhgy;->g:I

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lhgy;->h:Lhtk;

    if-nez v0, :cond_2

    new-instance v0, Lhtk;

    invoke-direct {v0}, Lhtk;-><init>()V

    iput-object v0, p0, Lhgy;->h:Lhtk;

    :cond_2
    iget-object v0, p0, Lhgy;->h:Lhtk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lhgy;->i:Lhtk;

    if-nez v0, :cond_3

    new-instance v0, Lhtk;

    invoke-direct {v0}, Lhtk;-><init>()V

    iput-object v0, p0, Lhgy;->i:Lhtk;

    :cond_3
    iget-object v0, p0, Lhgy;->i:Lhtk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhgy;->j:J

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhgy;->k:J

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhgy;->u:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhgy;->v:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhgy;->w:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgy;->x:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhgy;->l:Z

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhgy;->m:Z

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-ne v0, v5, :cond_5

    :cond_4
    iput v0, p0, Lhgy;->n:I

    goto/16 :goto_0

    :cond_5
    iput v3, p0, Lhgy;->n:I

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgy;->y:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgy;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhgy;->p:Z

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgy;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_18
    const/16 v0, 0xc0

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhgy;->r:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Lhgy;->r:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhgy;->r:[I

    :goto_1
    iget-object v1, p0, Lhgy;->r:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lhgy;->r:[I

    invoke-virtual {p1}, Lidc;->d()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lhgy;->r:[I

    invoke-virtual {p1}, Lidc;->d()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhgy;->z:I

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgy;->A:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_7

    if-eq v0, v4, :cond_7

    if-ne v0, v5, :cond_8

    :cond_7
    iput v0, p0, Lhgy;->B:I

    goto/16 :goto_0

    :cond_8
    iput v3, p0, Lhgy;->B:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb0 -> :sswitch_16
        0xba -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xd8 -> :sswitch_1b
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 53761
    iget v0, p0, Lhgy;->b:I

    if-eqz v0, :cond_0

    .line 53762
    const/4 v0, 0x1

    iget v1, p0, Lhgy;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53764
    :cond_0
    iget-object v0, p0, Lhgy;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 53765
    const/4 v0, 0x2

    iget-object v1, p0, Lhgy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53767
    :cond_1
    iget-object v0, p0, Lhgy;->s:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 53768
    const/4 v0, 0x3

    iget-object v1, p0, Lhgy;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53770
    :cond_2
    iget-object v0, p0, Lhgy;->t:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 53771
    const/4 v0, 0x4

    iget-object v1, p0, Lhgy;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53773
    :cond_3
    iget-object v0, p0, Lhgy;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 53774
    const/4 v0, 0x5

    iget-object v1, p0, Lhgy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53776
    :cond_4
    iget v0, p0, Lhgy;->e:I

    if-eqz v0, :cond_5

    .line 53777
    const/4 v0, 0x6

    iget v1, p0, Lhgy;->e:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53779
    :cond_5
    iget v0, p0, Lhgy;->f:I

    if-eqz v0, :cond_6

    .line 53780
    const/4 v0, 0x7

    iget v1, p0, Lhgy;->f:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53782
    :cond_6
    iget v0, p0, Lhgy;->g:I

    if-eqz v0, :cond_7

    .line 53783
    const/16 v0, 0x8

    iget v1, p0, Lhgy;->g:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53785
    :cond_7
    iget-object v0, p0, Lhgy;->h:Lhtk;

    if-eqz v0, :cond_8

    .line 53786
    const/16 v0, 0x9

    iget-object v1, p0, Lhgy;->h:Lhtk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 53788
    :cond_8
    iget-object v0, p0, Lhgy;->i:Lhtk;

    if-eqz v0, :cond_9

    .line 53789
    const/16 v0, 0xa

    iget-object v1, p0, Lhgy;->i:Lhtk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 53791
    :cond_9
    iget-wide v0, p0, Lhgy;->j:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_a

    .line 53792
    const/16 v0, 0xb

    iget-wide v2, p0, Lhgy;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 53794
    :cond_a
    iget-wide v0, p0, Lhgy;->k:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_b

    .line 53795
    const/16 v0, 0xc

    iget-wide v2, p0, Lhgy;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 53797
    :cond_b
    iget v0, p0, Lhgy;->u:I

    if-eqz v0, :cond_c

    .line 53798
    const/16 v0, 0xd

    iget v1, p0, Lhgy;->u:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53800
    :cond_c
    iget v0, p0, Lhgy;->v:I

    if-eqz v0, :cond_d

    .line 53801
    const/16 v0, 0xe

    iget v1, p0, Lhgy;->v:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53803
    :cond_d
    iget v0, p0, Lhgy;->w:I

    if-eqz v0, :cond_e

    .line 53804
    const/16 v0, 0xf

    iget v1, p0, Lhgy;->w:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53806
    :cond_e
    iget-object v0, p0, Lhgy;->x:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 53807
    const/16 v0, 0x10

    iget-object v1, p0, Lhgy;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53809
    :cond_f
    iget-boolean v0, p0, Lhgy;->l:Z

    if-eqz v0, :cond_10

    .line 53810
    const/16 v0, 0x11

    iget-boolean v1, p0, Lhgy;->l:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 53812
    :cond_10
    iget-boolean v0, p0, Lhgy;->m:Z

    if-eqz v0, :cond_11

    .line 53813
    const/16 v0, 0x12

    iget-boolean v1, p0, Lhgy;->m:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 53815
    :cond_11
    iget v0, p0, Lhgy;->n:I

    if-eqz v0, :cond_12

    .line 53816
    const/16 v0, 0x13

    iget v1, p0, Lhgy;->n:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53818
    :cond_12
    iget-object v0, p0, Lhgy;->y:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 53819
    const/16 v0, 0x14

    iget-object v1, p0, Lhgy;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53821
    :cond_13
    iget-object v0, p0, Lhgy;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 53822
    const/16 v0, 0x15

    iget-object v1, p0, Lhgy;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53824
    :cond_14
    iget-boolean v0, p0, Lhgy;->p:Z

    if-eqz v0, :cond_15

    .line 53825
    const/16 v0, 0x16

    iget-boolean v1, p0, Lhgy;->p:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 53827
    :cond_15
    iget-object v0, p0, Lhgy;->q:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 53828
    const/16 v0, 0x17

    iget-object v1, p0, Lhgy;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53830
    :cond_16
    iget-object v0, p0, Lhgy;->r:[I

    if-eqz v0, :cond_17

    iget-object v0, p0, Lhgy;->r:[I

    array-length v0, v0

    if-lez v0, :cond_17

    .line 53831
    iget-object v1, p0, Lhgy;->r:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_17

    aget v3, v1, v0

    .line 53832
    const/16 v4, 0x18

    invoke-virtual {p1, v4, v3}, Lidd;->a(II)V

    .line 53831
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53835
    :cond_17
    iget v0, p0, Lhgy;->z:I

    if-eqz v0, :cond_18

    .line 53836
    const/16 v0, 0x19

    iget v1, p0, Lhgy;->z:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53838
    :cond_18
    iget-object v0, p0, Lhgy;->A:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 53839
    const/16 v0, 0x1a

    iget-object v1, p0, Lhgy;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53841
    :cond_19
    iget v0, p0, Lhgy;->B:I

    if-eqz v0, :cond_1a

    .line 53842
    const/16 v0, 0x1b

    iget v1, p0, Lhgy;->B:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 53844
    :cond_1a
    iget-object v0, p0, Lhgy;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 53846
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53687
    if-ne p1, p0, :cond_1

    .line 53717
    :cond_0
    :goto_0
    return v0

    .line 53688
    :cond_1
    instance-of v2, p1, Lhgy;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 53689
    :cond_2
    check-cast p1, Lhgy;

    .line 53690
    iget v2, p0, Lhgy;->b:I

    iget v3, p1, Lhgy;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhgy;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgy;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53691
    :goto_1
    iget-object v2, p0, Lhgy;->s:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhgy;->s:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53692
    :goto_2
    iget-object v2, p0, Lhgy;->t:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhgy;->t:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53693
    :goto_3
    iget-object v2, p0, Lhgy;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhgy;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53694
    :goto_4
    iget v2, p0, Lhgy;->e:I

    iget v3, p1, Lhgy;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhgy;->f:I

    iget v3, p1, Lhgy;->f:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhgy;->g:I

    iget v3, p1, Lhgy;->g:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhgy;->h:Lhtk;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhgy;->h:Lhtk;

    if-nez v2, :cond_3

    .line 53698
    :goto_5
    iget-object v2, p0, Lhgy;->i:Lhtk;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhgy;->i:Lhtk;

    if-nez v2, :cond_3

    .line 53699
    :goto_6
    iget-wide v2, p0, Lhgy;->j:J

    iget-wide v4, p1, Lhgy;->j:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lhgy;->k:J

    iget-wide v4, p1, Lhgy;->k:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lhgy;->u:I

    iget v3, p1, Lhgy;->u:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhgy;->v:I

    iget v3, p1, Lhgy;->v:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhgy;->w:I

    iget v3, p1, Lhgy;->w:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhgy;->x:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhgy;->x:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53705
    :goto_7
    iget-boolean v2, p0, Lhgy;->l:Z

    iget-boolean v3, p1, Lhgy;->l:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhgy;->m:Z

    iget-boolean v3, p1, Lhgy;->m:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhgy;->n:I

    iget v3, p1, Lhgy;->n:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhgy;->y:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhgy;->y:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53709
    :goto_8
    iget-object v2, p0, Lhgy;->o:Ljava/lang/String;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhgy;->o:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53710
    :goto_9
    iget-boolean v2, p0, Lhgy;->p:Z

    iget-boolean v3, p1, Lhgy;->p:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhgy;->q:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhgy;->q:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53712
    :goto_a
    iget-object v2, p0, Lhgy;->r:[I

    iget-object v3, p1, Lhgy;->r:[I

    .line 53713
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lhgy;->z:I

    iget v3, p1, Lhgy;->z:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhgy;->A:Ljava/lang/String;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhgy;->A:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53715
    :goto_b
    iget v2, p0, Lhgy;->B:I

    iget v3, p1, Lhgy;->B:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhgy;->I:Ljava/util/List;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhgy;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 53717
    goto/16 :goto_0

    .line 53690
    :cond_4
    iget-object v2, p0, Lhgy;->c:Ljava/lang/String;

    iget-object v3, p1, Lhgy;->c:Ljava/lang/String;

    .line 53691
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhgy;->s:Ljava/lang/String;

    iget-object v3, p1, Lhgy;->s:Ljava/lang/String;

    .line 53692
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhgy;->t:Ljava/lang/String;

    iget-object v3, p1, Lhgy;->t:Ljava/lang/String;

    .line 53693
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhgy;->d:Ljava/lang/String;

    iget-object v3, p1, Lhgy;->d:Ljava/lang/String;

    .line 53694
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhgy;->h:Lhtk;

    iget-object v3, p1, Lhgy;->h:Lhtk;

    .line 53698
    invoke-virtual {v2, v3}, Lhtk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhgy;->i:Lhtk;

    iget-object v3, p1, Lhgy;->i:Lhtk;

    .line 53699
    invoke-virtual {v2, v3}, Lhtk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhgy;->x:Ljava/lang/String;

    iget-object v3, p1, Lhgy;->x:Ljava/lang/String;

    .line 53705
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhgy;->y:Ljava/lang/String;

    iget-object v3, p1, Lhgy;->y:Ljava/lang/String;

    .line 53709
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhgy;->o:Ljava/lang/String;

    iget-object v3, p1, Lhgy;->o:Ljava/lang/String;

    .line 53710
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhgy;->q:Ljava/lang/String;

    iget-object v3, p1, Lhgy;->q:Ljava/lang/String;

    .line 53712
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    .line 53713
    :cond_e
    iget-object v2, p0, Lhgy;->A:Ljava/lang/String;

    iget-object v3, p1, Lhgy;->A:Ljava/lang/String;

    .line 53715
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v2, p0, Lhgy;->I:Ljava/util/List;

    iget-object v3, p1, Lhgy;->I:Ljava/util/List;

    .line 53717
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 53721
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 53723
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhgy;->b:I

    add-int/2addr v0, v4

    .line 53724
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgy;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 53725
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgy;->s:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 53726
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgy;->t:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 53727
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgy;->d:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 53728
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhgy;->e:I

    add-int/2addr v0, v4

    .line 53729
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhgy;->f:I

    add-int/2addr v0, v4

    .line 53730
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhgy;->g:I

    add-int/2addr v0, v4

    .line 53731
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgy;->h:Lhtk;

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    .line 53732
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgy;->i:Lhtk;

    if-nez v0, :cond_6

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    .line 53733
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lhgy;->j:J

    iget-wide v6, p0, Lhgy;->j:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 53734
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lhgy;->k:J

    iget-wide v6, p0, Lhgy;->k:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 53735
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhgy;->u:I

    add-int/2addr v0, v4

    .line 53736
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhgy;->v:I

    add-int/2addr v0, v4

    .line 53737
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhgy;->w:I

    add-int/2addr v0, v4

    .line 53738
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgy;->x:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_6
    add-int/2addr v0, v4

    .line 53739
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhgy;->l:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    .line 53740
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhgy;->m:Z

    if-eqz v0, :cond_9

    move v0, v2

    :goto_8
    add-int/2addr v0, v4

    .line 53741
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhgy;->n:I

    add-int/2addr v0, v4

    .line 53742
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgy;->y:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_9
    add-int/2addr v0, v4

    .line 53743
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgy;->o:Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    :goto_a
    add-int/2addr v0, v4

    .line 53744
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lhgy;->p:Z

    if-eqz v4, :cond_c

    :goto_b
    add-int/2addr v0, v2

    .line 53745
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgy;->q:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 53746
    iget-object v2, p0, Lhgy;->r:[I

    if-nez v2, :cond_e

    mul-int/lit8 v2, v0, 0x1f

    .line 53752
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget v2, p0, Lhgy;->z:I

    add-int/2addr v0, v2

    .line 53753
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgy;->A:Ljava/lang/String;

    if-nez v0, :cond_f

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 53754
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhgy;->B:I

    add-int/2addr v0, v2

    .line 53755
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhgy;->I:Ljava/util/List;

    if-nez v2, :cond_10

    :goto_e
    add-int/2addr v0, v1

    .line 53756
    return v0

    .line 53724
    :cond_1
    iget-object v0, p0, Lhgy;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 53725
    :cond_2
    iget-object v0, p0, Lhgy;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 53726
    :cond_3
    iget-object v0, p0, Lhgy;->t:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 53727
    :cond_4
    iget-object v0, p0, Lhgy;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 53731
    :cond_5
    iget-object v0, p0, Lhgy;->h:Lhtk;

    invoke-virtual {v0}, Lhtk;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 53732
    :cond_6
    iget-object v0, p0, Lhgy;->i:Lhtk;

    invoke-virtual {v0}, Lhtk;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 53738
    :cond_7
    iget-object v0, p0, Lhgy;->x:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_6

    :cond_8
    move v0, v3

    .line 53739
    goto/16 :goto_7

    :cond_9
    move v0, v3

    .line 53740
    goto/16 :goto_8

    .line 53742
    :cond_a
    iget-object v0, p0, Lhgy;->y:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    .line 53743
    :cond_b
    iget-object v0, p0, Lhgy;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_a

    :cond_c
    move v2, v3

    .line 53744
    goto :goto_b

    .line 53745
    :cond_d
    iget-object v0, p0, Lhgy;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_c

    :cond_e
    move v2, v0

    move v0, v1

    .line 53748
    :goto_f
    iget-object v3, p0, Lhgy;->r:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 53749
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhgy;->r:[I

    aget v3, v3, v0

    add-int/2addr v2, v3

    .line 53748
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 53753
    :cond_f
    iget-object v0, p0, Lhgy;->A:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_d

    .line 53755
    :cond_10
    iget-object v1, p0, Lhgy;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_e
.end method
