.class public Lfqy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lhre;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 391
    new-instance v0, Lfqz;

    invoke-direct {v0}, Lfqz;-><init>()V

    sput-object v0, Lfqy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lhre;

    invoke-direct {v0}, Lhre;-><init>()V

    invoke-direct {p0, v0}, Lfqy;-><init>(Lhre;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Lhre;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhre;

    iput-object v0, p0, Lfqy;->a:Lhre;

    .line 79
    return-void
.end method

.method public static o()Z
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final a()Lfqx;
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->g:Lhnq;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lfqx;

    invoke-direct {v0}, Lfqx;-><init>()V

    .line 85
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lfqx;

    iget-object v1, p0, Lfqy;->a:Lhre;

    iget-object v1, v1, Lhre;->g:Lhnq;

    invoke-direct {v0, v1}, Lfqx;-><init>(Lhnq;)V

    goto :goto_0
.end method

.method public final a(Lfrb;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 89
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_1

    .line 100
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 92
    :cond_1
    sget-object v2, Lfra;->a:[I

    invoke-virtual {p1}, Lfrb;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 100
    iget-object v2, p0, Lfqy;->a:Lhre;

    iget-object v2, v2, Lhre;->b:Lhgg;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfqy;->a:Lhre;

    iget-object v2, v2, Lhre;->b:Lhgg;

    iget-boolean v2, v2, Lhgg;->a:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 97
    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->a:Lhav;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->a:Lhav;

    iget-boolean v0, v0, Lhav;->b:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lfrb;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 123
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_1

    .line 134
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 126
    :cond_1
    sget-object v2, Lfra;->a:[I

    invoke-virtual {p1}, Lfrb;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 134
    iget-object v2, p0, Lfqy;->a:Lhre;

    iget-object v2, v2, Lhre;->b:Lhgg;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfqy;->a:Lhre;

    iget-object v2, v2, Lhre;->b:Lhgg;

    iget-boolean v2, v2, Lhgg;->b:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 131
    goto :goto_0

    .line 126
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->g:I

    .line 204
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    .line 202
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 204
    :cond_1
    const/16 v0, 0x3a98

    goto :goto_1
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->h:I

    .line 210
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    .line 208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 210
    :cond_1
    const/16 v0, 0x7530

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x0

    return v0
.end method

.method public final e()F
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 220
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->i:F

    .line 222
    :goto_0
    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 220
    goto :goto_0

    .line 222
    :cond_1
    const v0, 0x3e4ccccd    # 0.2f

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 434
    instance-of v0, p1, Lfqy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    check-cast p1, Lfqy;

    iget-object v1, p1, Lfqy;->a:Lhre;

    .line 435
    invoke-static {v0, v1}, Lidh;->a(Lidh;Lidh;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()F
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 226
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->j:F

    .line 228
    :goto_0
    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 226
    goto :goto_0

    .line 228
    :cond_1
    const v0, 0x3f4ccccd    # 0.8f

    goto :goto_1
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->l:I

    .line 234
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    .line 232
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 234
    :cond_1
    const/16 v0, 0x32

    goto :goto_1
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->m:I

    .line 252
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    .line 250
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 252
    :cond_1
    const/16 v0, 0x1f40

    goto :goto_1
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->n:I

    .line 258
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    .line 256
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :cond_1
    const/16 v0, 0x1f40

    goto :goto_1
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->q:I

    .line 278
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    .line 276
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 278
    :cond_1
    const/16 v0, 0x1f4

    goto :goto_1
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget-boolean v0, v0, Lhgg;->v:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->c:Lhqv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->c:Lhqv;

    iget v0, v0, Lhqv;->a:F

    float-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->e:Lhoh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->e:Lhoh;

    iget-boolean v0, v0, Lhoh;->a:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->e:Lhoh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->e:Lhoh;

    iget-boolean v0, v0, Lhoh;->b:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->i:Lgyu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->i:Lgyu;

    iget-boolean v0, v0, Lgyu;->a:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->h:Lhlj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->h:Lhlj;

    iget-boolean v0, v0, Lhlj;->a:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget-boolean v0, v0, Lhgg;->D:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget v0, v0, Lhgg;->E:I

    .line 372
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    .line 370
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 372
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final t()Landroid/util/SparseIntArray;
    .locals 6

    .prologue
    .line 381
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    .line 382
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget-object v2, v0, Lhgg;->F:[Lhfc;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 385
    iget v5, v4, Lhfc;->b:I

    iget v4, v4, Lhfc;->c:I

    invoke-virtual {v1, v5, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 384
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 388
    :cond_0
    return-object v1
.end method

.method public final u()Lhre;
    .locals 2

    .prologue
    .line 422
    new-instance v0, Lhre;

    invoke-direct {v0}, Lhre;-><init>()V

    .line 424
    :try_start_0
    iget-object v1, p0, Lfqy;->a:Lhre;

    invoke-static {v1}, Lidh;->a(Lidh;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lidh;->a(Lidh;[B)Lidh;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 428
    :goto_0
    return-object v0

    .line 426
    :catch_0
    move-exception v0

    new-instance v0, Lhre;

    invoke-direct {v0}, Lhre;-><init>()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 413
    invoke-virtual {p0}, Lfqy;->u()Lhre;

    move-result-object v0

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lidh;)V

    .line 414
    return-void
.end method
