.class public final Lbif;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcam;
.implements Ldnr;


# static fields
.field private static final j:[Ljava/lang/String;


# instance fields
.field private final A:Landroid/widget/EditText;

.field private final B:Landroid/widget/EditText;

.field private final C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Lgcg;

.field private H:Z

.field private I:I

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Landroid/view/MenuItem;

.field public final a:Landroid/app/Activity;

.field public final b:Ldno;

.field public final c:Leuc;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Landroid/graphics/Bitmap;

.field public final g:Lbya;

.field public final h:Ljava/util/List;

.field public final i:Lfxe;

.field private final k:Landroid/content/ContentResolver;

.field private final l:Landroid/content/SharedPreferences;

.field private final m:Lbii;

.field private final n:Leyt;

.field private final o:Lbxu;

.field private final p:Lfdw;

.field private final q:Lfqg;

.field private final r:Z

.field private s:Ljava/lang/String;

.field private final t:Landroid/widget/ScrollView;

.field private final u:Landroid/widget/ImageView;

.field private final v:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

.field private final w:Landroid/widget/CheckBox;

.field private final x:Landroid/view/View;

.field private final y:Landroid/widget/TextView;

.field private final z:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 95
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "longitude"

    aput-object v2, v0, v1

    sput-object v0, Lbif;->j:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lfxe;Lbii;Leyt;Lbxu;Lfdw;Lfqg;Larh;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput-object p3, p0, Lbif;->i:Lfxe;

    .line 169
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbif;->a:Landroid/app/Activity;

    .line 170
    iput-object p4, p0, Lbif;->m:Lbii;

    .line 171
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbif;->n:Leyt;

    .line 172
    iput-object p6, p0, Lbif;->o:Lbxu;

    .line 174
    iput-object p7, p0, Lbif;->p:Lfdw;

    .line 175
    iput-object p8, p0, Lbif;->q:Lfqg;

    .line 177
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lbif;->k:Landroid/content/ContentResolver;

    .line 178
    const-string v0, "youtube"

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lbif;->l:Landroid/content/SharedPreferences;

    .line 180
    iget-object v0, p0, Lbif;->l:Landroid/content/SharedPreferences;

    const-string v2, "upload_privacy"

    sget-object v3, Lgcg;->c:Lgcg;

    .line 181
    invoke-virtual {v3}, Lgcg;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    invoke-static {v0}, Lgcg;->valueOf(Ljava/lang/String;)Lgcg;

    move-result-object v0

    iput-object v0, p0, Lbif;->G:Lgcg;

    .line 183
    iget-object v0, p9, Lcmp;->b:Lfhw;

    invoke-virtual {v0}, Lfhw;->d()Lfng;

    move-result-object v2

    .line 184
    iget-object v0, p0, Lbif;->l:Landroid/content/SharedPreferences;

    const-string v3, "enable_upload_video_editing"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-boolean v0, v2, Lfng;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lbif;->r:Z

    .line 187
    const v0, 0x7f08012f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbif;->y:Landroid/widget/TextView;

    .line 189
    const v0, 0x7f0800a4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbif;->u:Landroid/widget/ImageView;

    .line 190
    const v0, 0x7f08030b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lbif;->t:Landroid/widget/ScrollView;

    .line 191
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 192
    const v0, 0x7f08030c

    .line 193
    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    iput-object v0, p0, Lbif;->C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    .line 194
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lbif;->C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 195
    iget-object v0, p0, Lbif;->C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    iget-object v1, p0, Lbif;->t:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Landroid/widget/ScrollView;)V

    .line 196
    iget-object v0, p0, Lbif;->C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    iget v1, v2, Lfng;->c:I

    iget-object v2, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a:Lgqh;

    iget-object v0, v0, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a:Lgqh;

    iget v0, v0, Lgqh;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v2, v0}, Lgqh;->a(I)V

    .line 197
    iget-object v0, p0, Lbif;->C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    invoke-virtual {v0, p7, p8}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Lfdw;Lfqg;)V

    .line 199
    const v0, 0x7f080315

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbif;->z:Landroid/widget/EditText;

    .line 200
    const v0, 0x7f080317

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbif;->A:Landroid/widget/EditText;

    .line 201
    const v0, 0x7f08031b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbif;->B:Landroid/widget/EditText;

    .line 202
    const v0, 0x7f080319

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    iput-object v0, p0, Lbif;->v:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    .line 204
    iget-object v0, p0, Lbif;->v:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    iget-object v1, p0, Lbif;->G:Lgcg;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a(Lgcg;)V

    .line 205
    const v0, 0x7f08031d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbif;->w:Landroid/widget/CheckBox;

    .line 206
    const v0, 0x7f08031c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbif;->x:Landroid/view/View;

    .line 208
    new-instance v0, Lbya;

    new-instance v1, Lbig;

    invoke-direct {v1, p0}, Lbig;-><init>(Lbif;)V

    invoke-direct {v0, p1, v1}, Lbya;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    iput-object v0, p0, Lbif;->g:Lbya;

    .line 216
    new-instance v0, Lbij;

    invoke-direct {v0, p0}, Lbij;-><init>(Lbif;)V

    invoke-static {p1, v0}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    iput-object v0, p0, Lbif;->c:Leuc;

    .line 218
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbif;->h:Ljava/util/List;

    .line 219
    new-instance v0, Ldno;

    invoke-direct {v0, p1, p3}, Ldno;-><init>(Landroid/app/Activity;Lfxe;)V

    iput-object v0, p0, Lbif;->b:Ldno;

    .line 220
    return-void

    :cond_1
    move v0, v1

    .line 185
    goto/16 :goto_0
.end method

.method private a(Landroid/net/Uri;)Lbik;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 461
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    :try_start_0
    iget-object v0, p0, Lbif;->k:Landroid/content/ContentResolver;

    sget-object v2, Lbif;->j:[Ljava/lang/String;

    const-string v3, "mime_type LIKE \'video/%\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    move-object v1, v0

    .line 471
    :goto_0
    if-nez v1, :cond_0

    move-object v0, v6

    .line 515
    :goto_1
    return-object v0

    .line 465
    :catch_0
    move-exception v0

    .line 466
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2e

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Illegal argument when resolving content URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    move-object v1, v6

    .line 469
    goto :goto_0

    .line 467
    :catch_1
    move-exception v0

    .line 468
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error resolving content from URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    move-object v1, v6

    goto/16 :goto_0

    .line 476
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 477
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto/16 :goto_1

    .line 479
    :cond_1
    :try_start_2
    new-instance v0, Lbik;

    invoke-direct {v0}, Lbik;-><init>()V

    .line 480
    const-string v2, "_id"

    invoke-static {v1, v2}, Lbif;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lbik;->a:Ljava/lang/Long;

    .line 481
    const-string v2, "mime_type"

    invoke-static {v1, v2}, Lbif;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbik;->c:Ljava/lang/String;

    .line 482
    const-string v2, "duration"

    invoke-static {v1, v2}, Lbif;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lbik;->d:Ljava/lang/Long;

    .line 483
    const-string v2, "latitude"

    invoke-static {v1, v2}, Lbif;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbik;->e:Ljava/lang/String;

    .line 484
    const-string v2, "longitude"

    invoke-static {v1, v2}, Lbif;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbik;->f:Ljava/lang/String;

    .line 485
    iget-object v2, p0, Lbif;->f:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 486
    iget-object v2, p0, Lbif;->f:Landroid/graphics/Bitmap;

    iput-object v2, v0, Lbik;->b:Landroid/graphics/Bitmap;

    .line 507
    :cond_2
    :goto_2
    iput-object p1, v0, Lbik;->h:Landroid/net/Uri;

    .line 508
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbik;->i:Ljava/lang/String;

    .line 509
    iget-object v2, v0, Lbik;->c:Ljava/lang/String;

    const-string v3, "video/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 510
    iget-object v0, v0, Lbik;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "invalid file type ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 511
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto/16 :goto_1

    .line 491
    :cond_3
    :try_start_3
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_4

    .line 492
    iget-object v2, p0, Lbif;->a:Landroid/app/Activity;

    invoke-static {v2, p1}, Landroid/provider/DocumentsContract;->isDocumentUri(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 493
    iget-object v2, p0, Lbif;->k:Landroid/content/ContentResolver;

    new-instance v3, Landroid/graphics/Point;

    const/16 v4, 0x60

    const/16 v5, 0x60

    invoke-direct {v3, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    const/4 v4, 0x0

    invoke-static {v2, p1, v3, v4}, Landroid/provider/DocumentsContract;->getDocumentThumbnail(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/graphics/Point;Landroid/os/CancellationSignal;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v0, Lbik;->b:Landroid/graphics/Bitmap;

    .line 502
    :cond_4
    iget-object v2, v0, Lbik;->b:Landroid/graphics/Bitmap;

    if-nez v2, :cond_2

    .line 503
    iget-object v2, p0, Lbif;->k:Landroid/content/ContentResolver;

    .line 504
    iget-object v3, v0, Lbik;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v3, 0x3

    const/4 v7, 0x0

    .line 503
    invoke-static {v2, v4, v5, v3, v7}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v0, Lbik;->b:Landroid/graphics/Bitmap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 515
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1
.end method

.method static synthetic a(Lbif;Landroid/net/Uri;)Lbik;
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lbif;->a(Landroid/net/Uri;)Lbik;

    move-result-object v0

    return-object v0
.end method

.method private a(Lbik;)Lgje;
    .locals 7

    .prologue
    .line 388
    new-instance v0, Lgje;

    invoke-direct {v0}, Lgje;-><init>()V

    .line 389
    iget-object v1, p0, Lbif;->s:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 390
    const-string v1, "username"

    iget-object v2, p0, Lbif;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_0
    iget-object v1, p1, Lbik;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 393
    const-string v1, "upload_title"

    iget-object v2, p1, Lbik;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_1
    iget-object v1, p1, Lbik;->b:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 396
    iget-object v1, p1, Lbik;->b:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lfaq;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v1

    .line 397
    const-string v2, "upload_file_thumbnail"

    invoke-virtual {v0, v2, v1}, Lgje;->a(Ljava/lang/String;[B)V

    .line 399
    :cond_2
    iget-object v1, p1, Lbik;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 400
    const-string v1, "upload_file_duration"

    iget-object v2, p1, Lbik;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lgje;->a(Ljava/lang/String;J)V

    .line 402
    :cond_3
    const-string v1, "upload_start_time_millis"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgje;->a(Ljava/lang/String;J)V

    .line 403
    const-string v1, "authAccount"

    iget-object v2, p0, Lbif;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const-string v1, "tracking_account_id"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lgje;->a(Ljava/lang/String;Z)V

    .line 405
    iget-object v1, p0, Lbif;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 406
    const-string v1, "account_id"

    iget-object v2, p0, Lbif;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_4
    const-string v1, "upload_description"

    iget-object v2, p0, Lbif;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const-string v1, "upload_keywords"

    iget-object v2, p0, Lbif;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v1, "upload_privacy"

    iget-object v2, p0, Lbif;->G:Lgcg;

    invoke-virtual {v2}, Lgcg;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    iget-boolean v1, p0, Lbif;->H:Z

    if-eqz v1, :cond_5

    .line 412
    invoke-static {p1}, Lbif;->b(Lbik;)Landroid/util/Pair;

    move-result-object v1

    .line 414
    if-eqz v1, :cond_5

    .line 415
    const-string v2, "upload_location"

    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :cond_5
    return-object v0
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 520
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 521
    if-gez v0, :cond_0

    .line 522
    const/4 v0, 0x0

    .line 524
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lbif;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lbif;->s:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lbif;)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->g:Lbya;

    iget-boolean v0, v0, Lbya;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbif;->b:Ldno;

    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p0, v0, Ldno;->d:Ldnr;

    invoke-virtual {v0}, Ldno;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ldno;->a()V

    :goto_0
    iget-object v0, p0, Lbif;->o:Lbxu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    return-void

    :cond_0
    iget-object v1, v0, Ldno;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->a(Landroid/content/Context;Ldmw;)Lfad;

    move-result-object v1

    iput-object v1, v0, Ldno;->e:Lfad;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbif;->a:Landroid/app/Activity;

    const/16 v1, 0x3fd

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method static synthetic a(Lbif;Z)Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbif;->L:Z

    return v0
.end method

.method private static b(Lbik;)Landroid/util/Pair;
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lbik;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 434
    iget-object v0, p0, Lbik;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 435
    :cond_0
    const/4 v0, 0x0

    .line 437
    :goto_0
    return-object v0

    .line 438
    :cond_1
    iget-object v0, p0, Lbik;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    .line 439
    iget-object v1, p0, Lbik;->f:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    .line 437
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 528
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 529
    if-gez v0, :cond_0

    .line 530
    const/4 v0, 0x0

    .line 532
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lbif;)Ljava/util/List;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lbif;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 600
    iget-object v0, p0, Lbif;->l:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "upload_privacy"

    iget-object v2, p0, Lbif;->G:Lgcg;

    .line 601
    invoke-virtual {v2}, Lgcg;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 602
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 603
    iget-object v0, p0, Lbif;->m:Lbii;

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lbif;->m:Lbii;

    invoke-interface {v0}, Lbii;->f()V

    .line 606
    :cond_0
    return-void
.end method

.method static synthetic d(Lbif;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lbif;->r:Z

    return v0
.end method

.method static synthetic e(Lbif;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->u:Landroid/widget/ImageView;

    return-object v0
.end method

.method private e()V
    .locals 18

    .prologue
    .line 609
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lbif;->I:I

    .line 610
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->z:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbif;->D:Ljava/lang/String;

    .line 611
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->A:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbif;->E:Ljava/lang/String;

    .line 612
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->B:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbif;->F:Ljava/lang/String;

    .line 613
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->v:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgcg;

    move-object/from16 v0, p0

    iput-object v2, v0, Lbif;->G:Lgcg;

    .line 614
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->w:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbif;->H:Z

    .line 616
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->D:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 617
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbif;->J:Z

    if-eqz v2, :cond_0

    .line 618
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->a:Landroid/app/Activity;

    const v3, 0x7f090261

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Leze;->a(Landroid/content/Context;II)V

    .line 619
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->M:Landroid/view/MenuItem;

    if-eqz v2, :cond_0

    .line 620
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->M:Landroid/view/MenuItem;

    const v3, 0x7f0901f0

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 670
    :cond_0
    :goto_0
    return-void

    .line 626
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    iget-object v2, v2, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbif;->J:Z

    if-eqz v2, :cond_0

    .line 631
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbif;->J:Z

    if-eqz v2, :cond_3

    .line 632
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->p:Lfdw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbif;->q:Lfqg;

    sget-object v4, Lfqi;->f:Lfqi;

    .line 635
    invoke-virtual/range {p0 .. p0}, Lbif;->c()Lhcq;

    move-result-object v5

    .line 632
    invoke-virtual {v2, v3, v4, v5}, Lfdw;->b(Lfqg;Lfqi;Lhcq;)V

    .line 638
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 639
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->h:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lbik;

    .line 640
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->D:Ljava/lang/String;

    iput-object v2, v11, Lbik;->g:Ljava/lang/String;

    .line 641
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->b:Ldno;

    .line 642
    iget-object v3, v11, Lbik;->h:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbif;->C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    iget-object v4, v4, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v5, "trimStartUs=%d,trimEndUs=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v4, Lgqo;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, v4, Lgqo;->g:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 643
    :cond_4
    iget-object v4, v11, Lbik;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbif;->G:Lgcg;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbif;->D:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbif;->E:Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lbif;->F:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lbif;->H:Z

    if-eqz v10, :cond_7

    .line 649
    invoke-static {v11}, Lbif;->b(Lbik;)Landroid/util/Pair;

    move-result-object v10

    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lbif;->C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    .line 650
    iget-object v12, v12, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lbif;->a(Lbik;)Lgje;

    move-result-object v11

    if-eqz v12, :cond_6

    iget-wide v14, v12, Lgqo;->f:J

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-nez v13, :cond_5

    iget-wide v14, v12, Lgqo;->g:J

    iget-wide v0, v12, Lgqo;->d:J

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-eqz v13, :cond_6

    :cond_5
    const-string v13, "upload_trim_start_us"

    iget-wide v14, v12, Lgqo;->f:J

    invoke-virtual {v11, v13, v14, v15}, Lgje;->a(Ljava/lang/String;J)V

    const-string v13, "upload_trim_end_us"

    iget-wide v14, v12, Lgqo;->g:J

    invoke-virtual {v11, v13, v14, v15}, Lgje;->a(Ljava/lang/String;J)V

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lbif;->J:Z

    .line 641
    invoke-virtual/range {v2 .. v12}, Ldno;->a(Landroid/net/Uri;Ljava/lang/String;Lgcg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lgje;Z)V

    goto/16 :goto_0

    .line 649
    :cond_7
    const/4 v10, 0x0

    goto :goto_1

    .line 653
    :cond_8
    const/4 v2, 0x1

    .line 654
    move-object/from16 v0, p0

    iget-object v3, v0, Lbif;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move v3, v2

    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lbik;

    .line 655
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->D:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xd

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v11, Lbik;->g:Ljava/lang/String;

    .line 656
    add-int/lit8 v13, v3, 0x1

    .line 657
    move-object/from16 v0, p0

    iget-object v2, v0, Lbif;->b:Ldno;

    .line 658
    iget-object v3, v11, Lbik;->h:Landroid/net/Uri;

    .line 659
    iget-object v4, v11, Lbik;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbif;->G:Lgcg;

    .line 661
    iget-object v6, v11, Lbik;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbif;->E:Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lbif;->F:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lbif;->H:Z

    if-eqz v10, :cond_9

    .line 665
    invoke-static {v11}, Lbif;->b(Lbik;)Landroid/util/Pair;

    move-result-object v10

    .line 666
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lbif;->a(Lbik;)Lgje;

    move-result-object v11

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lbif;->J:Z

    .line 657
    invoke-virtual/range {v2 .. v12}, Ldno;->a(Landroid/net/Uri;Ljava/lang/String;Lgcg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lgje;Z)V

    move v3, v13

    .line 668
    goto :goto_2

    .line 665
    :cond_9
    const/4 v10, 0x0

    goto :goto_3
.end method

.method static synthetic f(Lbif;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->y:Landroid/widget/TextView;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 705
    iget-boolean v0, p0, Lbif;->K:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbif;->L:Z

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lbif;->M:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lbif;->M:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 710
    :cond_0
    return-void
.end method

.method static synthetic g(Lbif;)Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->C:Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    return-object v0
.end method

.method static synthetic h(Lbif;)Lbxu;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->o:Lbxu;

    return-object v0
.end method

.method static synthetic i(Lbif;)Landroid/view/View;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->x:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lbif;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lbif;->f()V

    return-void
.end method

.method static synthetic k(Lbif;)Leyt;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->n:Leyt;

    return-object v0
.end method

.method static synthetic l(Lbif;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbif;->l:Landroid/content/SharedPreferences;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 551
    invoke-direct {p0}, Lbif;->e()V

    .line 552
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbif;->K:Z

    .line 553
    invoke-direct {p0}, Lbif;->f()V

    .line 554
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 363
    iput-object p1, p0, Lbif;->M:Landroid/view/MenuItem;

    .line 364
    iget-object v0, p0, Lbif;->M:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 365
    invoke-direct {p0}, Lbif;->f()V

    .line 367
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 579
    const-string v0, "Error requesting location for upload"

    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 580
    iget v0, p0, Lbif;->I:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbif;->I:I

    .line 581
    iget v0, p0, Lbif;->I:I

    if-nez v0, :cond_0

    .line 582
    iget-object v0, p0, Lbif;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 583
    iget-object v0, p0, Lbif;->n:Leyt;

    invoke-interface {v0, p1}, Leyt;->c(Ljava/lang/Throwable;)V

    .line 584
    iget-object v0, p0, Lbif;->M:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lbif;->M:Landroid/view/MenuItem;

    const v1, 0x7f0901f0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 586
    iget-object v0, p0, Lbif;->M:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 592
    :cond_0
    :goto_0
    return-void

    .line 589
    :cond_1
    invoke-direct {p0}, Lbif;->d()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 558
    iget v1, p0, Lbif;->I:I

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    sub-int v0, v1, v0

    iput v0, p0, Lbif;->I:I

    .line 559
    iget v0, p0, Lbif;->I:I

    if-nez v0, :cond_0

    .line 560
    invoke-direct {p0}, Lbif;->d()V

    .line 562
    :cond_0
    return-void

    .line 558
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 566
    iget v0, p0, Lbif;->I:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbif;->I:I

    .line 567
    iget-object v0, p0, Lbif;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 568
    iget-object v0, p0, Lbif;->a:Landroid/app/Activity;

    const v1, 0x7f090253

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    .line 569
    iget-object v0, p0, Lbif;->m:Lbii;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Lbif;->m:Lbii;

    invoke-interface {v0}, Lbii;->g()V

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 572
    :cond_1
    iget v0, p0, Lbif;->I:I

    if-nez v0, :cond_0

    .line 573
    invoke-direct {p0}, Lbif;->d()V

    goto :goto_0
.end method

.method public final c()Lhcq;
    .locals 8

    .prologue
    .line 678
    new-instance v2, Lhcq;

    invoke-direct {v2}, Lhcq;-><init>()V

    .line 679
    iget-object v0, p0, Lbif;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lhcu;

    iput-object v0, v2, Lhcq;->a:[Lhcu;

    .line 680
    const/4 v0, 0x0

    .line 681
    iget-object v1, p0, Lbif;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbik;

    .line 682
    iget-object v4, v2, Lhcq;->a:[Lhcu;

    new-instance v5, Lhcu;

    invoke-direct {v5}, Lhcu;-><init>()V

    aput-object v5, v4, v1

    .line 683
    iget-object v4, v2, Lhcq;->a:[Lhcu;

    aget-object v4, v4, v1

    iget-object v5, v0, Lbik;->i:Ljava/lang/String;

    iput-object v5, v4, Lhcu;->c:Ljava/lang/String;

    .line 684
    iget-object v4, p0, Lbif;->b:Ldno;

    iget-object v0, v0, Lbik;->h:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ldno;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v4, Ldno;->e:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    invoke-virtual {v0}, Ldmz;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjm;

    .line 685
    :goto_1
    if-eqz v0, :cond_0

    .line 686
    iget-object v4, v2, Lhcq;->a:[Lhcu;

    aget-object v4, v4, v1

    iget-wide v6, v0, Lgjm;->e:J

    iput-wide v6, v4, Lhcu;->b:J

    .line 689
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 690
    goto :goto_0

    .line 684
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 691
    :cond_2
    return-object v2
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 353
    const v0, 0x7f08036f

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 358
    const v0, 0x7f110005

    return v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 371
    iput-boolean v2, p0, Lbif;->J:Z

    .line 372
    iget-object v0, p0, Lbif;->M:Landroid/view/MenuItem;

    const v1, 0x7f090252

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 373
    invoke-direct {p0}, Lbif;->e()V

    .line 374
    return v2
.end method
