.class public final Lfrl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lfqh;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lhro;

.field public final b:J

.field public final c:[B

.field public final d:Lfrf;

.field private e:Lflo;

.field private f:Lflp;

.field private g:Lfqy;

.field private h:Lfkg;

.field private i:Lfrn;

.field private j:Lflg;

.field private k:Lflr;

.field private l:Lfoy;

.field private m:Lgyr;

.field private n:Liak;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 666
    new-instance v0, Lfrm;

    invoke-direct {v0}, Lfrm;-><init>()V

    sput-object v0, Lfrl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lhro;J)V
    .locals 6

    .prologue
    .line 83
    sget-object v4, Lfhy;->a:[B

    new-instance v0, Lfri;

    const/4 v1, 0x0

    new-array v1, v1, [Lfrj;

    invoke-direct {v0, v1}, Lfri;-><init>([Lfrj;)V

    .line 87
    invoke-static {v0, p1, p2, p3}, Lfrl;->a(Lfri;Lhro;J)Lfrf;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    .line 83
    invoke-direct/range {v0 .. v5}, Lfrl;-><init>(Lhro;J[BLfrf;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Lhro;J[BLfrf;)V
    .locals 2

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhro;

    iput-object v0, p0, Lfrl;->a:Lhro;

    .line 110
    iput-wide p2, p0, Lfrl;->b:J

    .line 111
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lfrl;->c:[B

    .line 112
    iput-object p5, p0, Lfrl;->d:Lfrf;

    .line 113
    return-void
.end method

.method private static a([Lhgy;)Landroid/util/SparseArray;
    .locals 4

    .prologue
    .line 598
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 600
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 601
    aget-object v2, p0, v0

    .line 602
    iget v3, v2, Lhgy;->b:I

    invoke-virtual {v1, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 600
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 604
    :cond_0
    return-object v1
.end method

.method public static final a(Lfri;Lhro;J)Lfrf;
    .locals 10

    .prologue
    .line 235
    iget-object v0, p1, Lhro;->b:Lhwn;

    if-nez v0, :cond_0

    .line 236
    const/4 v0, 0x0

    .line 240
    :goto_0
    return-object v0

    .line 238
    :cond_0
    iget-object v0, p1, Lhro;->g:Lhzc;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lhro;->g:Lhzc;

    iget-wide v0, v0, Lhzc;->c:J

    .line 240
    :goto_1
    iget-object v2, p1, Lhro;->b:Lhwn;

    .line 242
    invoke-static {p1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v0

    .line 245
    invoke-static {p1}, Lfrl;->b(Lhro;)Z

    move-result v8

    move-object v1, p0

    move-wide v6, p2

    .line 240
    invoke-virtual/range {v1 .. v8}, Lfri;->a(Lhwn;Ljava/lang/String;JJZ)Lfrf;

    move-result-object v0

    goto :goto_0

    .line 238
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method public static a([BJ)Lfrl;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 653
    if-nez p0, :cond_0

    .line 662
    :goto_0
    return-object v0

    .line 658
    :cond_0
    :try_start_0
    new-instance v2, Lhro;

    invoke-direct {v2}, Lhro;-><init>()V

    .line 659
    invoke-static {v2, p0}, Lidh;->a(Lidh;[B)Lidh;

    .line 660
    new-instance v1, Lfrl;

    const-wide/16 v4, 0x0

    invoke-direct {v1, v2, v4, v5}, Lfrl;-><init>(Lhro;J)V
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 662
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lhro;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lhro;->g:Lhzc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhro;->g:Lhzc;

    iget-object v0, v0, Lhzc;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Landroid/util/SparseArray;)[Lhgy;
    .locals 3

    .prologue
    .line 609
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 610
    new-array v2, v0, [Lhgy;

    .line 611
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 612
    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgy;

    aput-object v0, v2, v1

    .line 611
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 614
    :cond_0
    return-object v2
.end method

.method public static b(Lhro;)Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lhro;->g:Lhzc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhro;->g:Lhzc;

    iget-boolean v0, v0, Lhzc;->d:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfqj;Lfqj;JJ)Lfrl;
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 549
    new-instance v1, Lhro;

    invoke-direct {v1}, Lhro;-><init>()V

    .line 550
    iget-object v0, p0, Lfrl;->a:Lhro;

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lidh;->a(Lidh;[B)Lidh;

    .line 552
    iget-object v0, v1, Lhro;->b:Lhwn;

    .line 553
    if-eqz v0, :cond_2

    .line 554
    cmp-long v2, p5, v4

    if-lez v2, :cond_3

    .line 555
    iput-wide p5, v0, Lhwn;->a:J

    .line 560
    :goto_0
    iget-object v2, v0, Lhwn;->c:[Lhgy;

    .line 561
    invoke-static {v2}, Lfrl;->a([Lhgy;)Landroid/util/SparseArray;

    move-result-object v2

    .line 563
    if-eqz p1, :cond_0

    .line 565
    invoke-static {}, Lfqm;->h()Ljava/util/Set;

    move-result-object v3

    iget-object v4, p1, Lfqj;->a:Lhgy;

    iget v4, v4, Lhgy;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 567
    iget-object v3, p1, Lfqj;->a:Lhgy;

    iget v3, v3, Lhgy;->b:I

    .line 568
    invoke-virtual {p1}, Lfqj;->a()Lhgy;

    move-result-object v4

    .line 566
    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 579
    :cond_0
    :goto_1
    if-eqz p2, :cond_1

    .line 581
    iget-object v3, p2, Lfqj;->a:Lhgy;

    iget v3, v3, Lhgy;->b:I

    invoke-virtual {p2}, Lfqj;->a()Lhgy;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 583
    :cond_1
    invoke-static {v2}, Lfrl;->a(Landroid/util/SparseArray;)[Lhgy;

    move-result-object v2

    iput-object v2, v0, Lhwn;->c:[Lhgy;

    .line 586
    :cond_2
    new-instance v0, Lfrl;

    iget-object v4, p0, Lfrl;->c:[B

    new-instance v2, Lfri;

    const/4 v3, 0x0

    new-array v3, v3, [Lfrj;

    invoke-direct {v2, v3}, Lfri;-><init>([Lfrj;)V

    .line 590
    invoke-static {v2, v1, p3, p4}, Lfrl;->a(Lfri;Lhro;J)Lfrf;

    move-result-object v5

    move-wide v2, p3

    invoke-direct/range {v0 .. v5}, Lfrl;-><init>(Lhro;J[BLfrf;)V

    return-object v0

    .line 557
    :cond_3
    iput-wide v4, v0, Lhwn;->a:J

    goto :goto_0

    .line 570
    :cond_4
    iget-object v3, v0, Lhwn;->b:[Lhgy;

    .line 571
    invoke-static {v3}, Lfrl;->a([Lhgy;)Landroid/util/SparseArray;

    move-result-object v3

    .line 573
    iget-object v4, p1, Lfqj;->a:Lhgy;

    iget v4, v4, Lhgy;->b:I

    .line 574
    invoke-virtual {p1}, Lfqj;->a()Lhgy;

    move-result-object v5

    .line 572
    invoke-virtual {v3, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 575
    invoke-static {v3}, Lfrl;->a(Landroid/util/SparseArray;)[Lhgy;

    move-result-object v3

    iput-object v3, v0, Lhwn;->b:[Lhgy;

    goto :goto_1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->g:Lhzc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->g:Lhzc;

    iget-object v0, v0, Lhzc;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final b()Lfnc;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->g:Lhzc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->g:Lhzc;

    iget-object v0, v0, Lhzc;->e:Lhxf;

    .line 145
    :goto_0
    new-instance v1, Lfnc;

    invoke-direct {v1, v0}, Lfnc;-><init>(Lhxf;)V

    return-object v1

    .line 141
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->g:Lhzc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->g:Lhzc;

    iget-wide v0, v0, Lhzc;->c:J

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->l:[B

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 694
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 714
    const/4 v0, 0x0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 480
    if-ne p1, p0, :cond_1

    .line 489
    :cond_0
    :goto_0
    return v0

    .line 483
    :cond_1
    instance-of v2, p1, Lfrl;

    if-nez v2, :cond_2

    move v0, v1

    .line 484
    goto :goto_0

    .line 486
    :cond_2
    check-cast p1, Lfrl;

    .line 488
    iget-object v2, p0, Lfrl;->a:Lhro;

    invoke-static {v2}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lfrl;->a:Lhro;

    invoke-static {v3}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 489
    invoke-virtual {p0}, Lfrl;->g()Lflo;

    move-result-object v2

    invoke-virtual {p1}, Lfrl;->g()Lflo;

    move-result-object v3

    invoke-static {v2, v3}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lfrl;->q()Lflg;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lflo;
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lfrl;->e:Lflo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->a:Lhqn;

    if-eqz v0, :cond_0

    .line 192
    new-instance v0, Lflo;

    iget-object v1, p0, Lfrl;->a:Lhro;

    iget-object v1, v1, Lhro;->a:Lhqn;

    invoke-direct {v0, v1}, Lflo;-><init>(Lhqn;)V

    iput-object v0, p0, Lfrl;->e:Lflo;

    .line 194
    :cond_0
    iget-object v0, p0, Lfrl;->e:Lflo;

    return-object v0
.end method

.method public final h()Lflp;
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lfrl;->f:Lflp;

    if-nez v0, :cond_0

    .line 203
    new-instance v0, Lflp;

    iget-object v1, p0, Lfrl;->a:Lhro;

    iget-object v1, v1, Lhro;->e:Lhqw;

    invoke-direct {v0, v1}, Lflp;-><init>(Lhqw;)V

    iput-object v0, p0, Lfrl;->f:Lflp;

    .line 205
    :cond_0
    iget-object v0, p0, Lfrl;->f:Lflp;

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x13

    .line 473
    mul-int/lit8 v1, v0, 0x13

    .line 474
    invoke-virtual {p0}, Lfrl;->g()Lflo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 475
    return v0

    .line 474
    :cond_0
    invoke-virtual {p0}, Lfrl;->g()Lflo;

    move-result-object v0

    invoke-virtual {v0}, Lflo;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final i()Lfqy;
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lfrl;->g:Lfqy;

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->j:Lhre;

    if-eqz v0, :cond_1

    new-instance v0, Lfqy;

    iget-object v1, p0, Lfrl;->a:Lhro;

    iget-object v1, v1, Lhro;->j:Lhre;

    invoke-direct {v0, v1}, Lfqy;-><init>(Lhre;)V

    :goto_0
    iput-object v0, p0, Lfrl;->g:Lfqy;

    .line 218
    :cond_0
    iget-object v0, p0, Lfrl;->g:Lfqy;

    return-object v0

    .line 214
    :cond_1
    new-instance v0, Lfqy;

    new-instance v1, Lhre;

    invoke-direct {v1}, Lhre;-><init>()V

    invoke-direct {v0, v1}, Lfqy;-><init>(Lhre;)V

    goto :goto_0
.end method

.method public final j()Lhqz;
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->h:[Lhal;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 254
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->h:[Lhal;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Lhal;->b:Lhqz;

    .line 256
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Lfkg;
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lfrl;->h:Lfkg;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->k:Lhju;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->k:Lhju;

    iget-object v0, v0, Lhju;->a:Lhjj;

    if-eqz v0, :cond_0

    .line 310
    new-instance v0, Lfkg;

    iget-object v1, p0, Lfrl;->a:Lhro;

    iget-object v1, v1, Lhro;->k:Lhju;

    iget-object v1, v1, Lhju;->a:Lhjj;

    invoke-direct {v0, v1}, Lfkg;-><init>(Lhjj;)V

    iput-object v0, p0, Lfrl;->h:Lfkg;

    .line 313
    :cond_0
    iget-object v0, p0, Lfrl;->h:Lfkg;

    return-object v0
.end method

.method public final l()Lfrn;
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lfrl;->i:Lfrn;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->p:Lhzz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->p:Lhzz;

    iget-object v0, v0, Lhzz;->a:Lhzy;

    if-eqz v0, :cond_0

    .line 321
    new-instance v0, Lfrn;

    iget-object v1, p0, Lfrl;->a:Lhro;

    iget-object v1, v1, Lhro;->p:Lhzz;

    iget-object v1, v1, Lhzz;->a:Lhzy;

    invoke-direct {v0, v1, p0}, Lfrn;-><init>(Lhzy;Lfqh;)V

    iput-object v0, p0, Lfrl;->i:Lfrn;

    .line 324
    :cond_0
    iget-object v0, p0, Lfrl;->i:Lfrn;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 331
    invoke-virtual {p0}, Lfrl;->n()Liak;

    move-result-object v0

    .line 332
    if-eqz v0, :cond_0

    iget-object v0, v0, Liak;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Liak;
    .locals 5

    .prologue
    .line 356
    iget-object v0, p0, Lfrl;->n:Liak;

    if-nez v0, :cond_0

    .line 358
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v1, v0, Lhro;->d:[Lhqx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 359
    iget-object v4, v3, Lhqx;->b:Liak;

    if-eqz v4, :cond_1

    .line 360
    iget-object v0, v3, Lhqx;->b:Liak;

    iput-object v0, p0, Lfrl;->n:Liak;

    .line 365
    :cond_0
    iget-object v0, p0, Lfrl;->n:Liak;

    return-object v0

    .line 358
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final o()Lfoy;
    .locals 6

    .prologue
    .line 375
    iget-object v0, p0, Lfrl;->l:Lfoy;

    if-nez v0, :cond_1

    .line 376
    const/4 v0, 0x0

    .line 377
    iget-object v1, p0, Lfrl;->a:Lhro;

    iget-object v2, v1, Lhro;->d:[Lhqx;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 378
    iget-object v5, v4, Lhqx;->d:Lhys;

    if-eqz v5, :cond_2

    .line 379
    iget-object v0, v4, Lhqx;->d:Lhys;

    iget-object v0, v0, Lhys;->a:Lhyr;

    .line 383
    :cond_0
    if-eqz v0, :cond_1

    .line 384
    invoke-static {v0}, La;->a(Lhyr;)Lfoy;

    move-result-object v0

    iput-object v0, p0, Lfrl;->l:Lfoy;

    .line 385
    iget-object v0, p0, Lfrl;->l:Lfoy;

    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p0, Lfrl;->l:Lfoy;

    invoke-virtual {v0}, Lfoy;->b()Lfpc;

    move-result-object v0

    .line 390
    invoke-virtual {p0}, Lfrl;->c()I

    move-result v1

    iput v1, v0, Lfpc;->q:I

    .line 391
    iget-object v1, p0, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lfpc;->i:Ljava/lang/String;

    .line 392
    invoke-virtual {p0}, Lfrl;->i()Lfqy;

    move-result-object v1

    iput-object v1, v0, Lfpc;->t:Lfqy;

    .line 393
    invoke-virtual {v0}, Lfpc;->a()Lfoy;

    move-result-object v0

    iput-object v0, p0, Lfrl;->l:Lfoy;

    .line 397
    :cond_1
    iget-object v0, p0, Lfrl;->l:Lfoy;

    return-object v0

    .line 377
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final p()Lflr;
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lfrl;->k:Lflr;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->m:Lhat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->m:Lhat;

    iget-object v0, v0, Lhat;->a:Lhrb;

    if-eqz v0, :cond_0

    .line 404
    new-instance v0, Lflr;

    iget-object v1, p0, Lfrl;->a:Lhro;

    iget-object v1, v1, Lhro;->m:Lhat;

    iget-object v1, v1, Lhat;->a:Lhrb;

    invoke-direct {v0, v1}, Lflr;-><init>(Lhrb;)V

    iput-object v0, p0, Lfrl;->k:Lflr;

    .line 407
    :cond_0
    iget-object v0, p0, Lfrl;->k:Lflr;

    return-object v0
.end method

.method public final q()Lflg;
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lfrl;->j:Lflg;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->i:Lhpl;

    if-eqz v0, :cond_0

    .line 444
    new-instance v0, Lflg;

    iget-object v1, p0, Lfrl;->a:Lhro;

    iget-object v1, v1, Lhro;->i:Lhpl;

    invoke-direct {v0, v1}, Lflg;-><init>(Lhpl;)V

    iput-object v0, p0, Lfrl;->j:Lflg;

    .line 446
    :cond_0
    iget-object v0, p0, Lfrl;->j:Lflg;

    return-object v0
.end method

.method public final r()Lhrd;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->f:Lhby;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->f:Lhby;

    iget-object v0, v0, Lhby;->a:Lhrd;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Lgyr;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lfrl;->m:Lgyr;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->j:Lhre;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->j:Lhre;

    iget-object v0, v0, Lhre;->d:Lgyr;

    iput-object v0, p0, Lfrl;->m:Lgyr;

    .line 458
    :cond_0
    iget-object v0, p0, Lfrl;->m:Lgyr;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 699
    iget-object v0, p0, Lfrl;->a:Lhro;

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lidh;)V

    .line 700
    iget-wide v0, p0, Lfrl;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 701
    iget-object v0, p0, Lfrl;->c:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 702
    iget-object v0, p0, Lfrl;->c:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 703
    iget-object v0, p0, Lfrl;->d:Lfrf;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 704
    return-void
.end method
