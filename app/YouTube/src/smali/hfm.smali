.class public final Lhfm;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46237
    invoke-direct {p0}, Lidf;-><init>()V

    .line 46240
    const-string v0, ""

    iput-object v0, p0, Lhfm;->a:Ljava/lang/String;

    .line 46243
    const-string v0, ""

    iput-object v0, p0, Lhfm;->b:Ljava/lang/String;

    .line 46237
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 46286
    const/4 v0, 0x0

    .line 46287
    iget-object v1, p0, Lhfm;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 46288
    const/4 v0, 0x1

    iget-object v1, p0, Lhfm;->a:Ljava/lang/String;

    .line 46289
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 46291
    :cond_0
    iget-object v1, p0, Lhfm;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 46292
    const/4 v1, 0x2

    iget-object v2, p0, Lhfm;->b:Ljava/lang/String;

    .line 46293
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46295
    :cond_1
    iget-object v1, p0, Lhfm;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46296
    iput v0, p0, Lhfm;->J:I

    .line 46297
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 46233
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhfm;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhfm;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhfm;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfm;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfm;->b:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 46274
    iget-object v0, p0, Lhfm;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46275
    const/4 v0, 0x1

    iget-object v1, p0, Lhfm;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 46277
    :cond_0
    iget-object v0, p0, Lhfm;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 46278
    const/4 v0, 0x2

    iget-object v1, p0, Lhfm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 46280
    :cond_1
    iget-object v0, p0, Lhfm;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 46282
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46255
    if-ne p1, p0, :cond_1

    .line 46260
    :cond_0
    :goto_0
    return v0

    .line 46256
    :cond_1
    instance-of v2, p1, Lhfm;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 46257
    :cond_2
    check-cast p1, Lhfm;

    .line 46258
    iget-object v2, p0, Lhfm;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfm;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhfm;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhfm;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 46259
    :goto_2
    iget-object v2, p0, Lhfm;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhfm;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 46260
    goto :goto_0

    .line 46258
    :cond_4
    iget-object v2, p0, Lhfm;->a:Ljava/lang/String;

    iget-object v3, p1, Lhfm;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhfm;->b:Ljava/lang/String;

    iget-object v3, p1, Lhfm;->b:Ljava/lang/String;

    .line 46259
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhfm;->I:Ljava/util/List;

    iget-object v3, p1, Lhfm;->I:Ljava/util/List;

    .line 46260
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 46264
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 46266
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfm;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 46267
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfm;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 46268
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhfm;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 46269
    return v0

    .line 46266
    :cond_0
    iget-object v0, p0, Lhfm;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 46267
    :cond_1
    iget-object v0, p0, Lhfm;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 46268
    :cond_2
    iget-object v1, p0, Lhfm;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
