.class public final Lgcu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Lgcw;

.field private final c:Lws;


# direct methods
.method public constructor <init>(Lws;)V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0}, Lgcu;-><init>(Lws;Ljava/util/Map;)V

    .line 57
    return-void
.end method

.method private constructor <init>(Lws;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    iput-object v0, p0, Lgcu;->c:Lws;

    .line 63
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lgcu;->a:Ljava/util/Map;

    .line 64
    new-instance v0, Lgcw;

    invoke-direct {v0}, Lgcw;-><init>()V

    iput-object v0, p0, Lgcu;->b:Lgcw;

    .line 65
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;)Ledg;
    .locals 3

    .prologue
    .line 78
    invoke-static {}, Lb;->b()V

    .line 79
    invoke-static {}, Lgky;->a()Lgky;

    move-result-object v0

    .line 80
    new-instance v1, Lgcv;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0, p2}, Lgcv;-><init>(Lgcu;Ljava/lang/String;Lwv;Ljava/lang/String;)V

    const/4 v2, 0x0

    iput-boolean v2, v1, Lwp;->e:Z

    iget-object v2, p0, Lgcu;->c:Lws;

    invoke-virtual {v2, v1}, Lws;->a(Lwp;)Lwp;

    .line 82
    :try_start_0
    invoke-virtual {v0}, Lgky;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledg;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    new-instance v1, Lxa;

    invoke-direct {v1, v0}, Lxa;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 85
    :catch_1
    move-exception v0

    .line 86
    new-instance v1, Lxa;

    invoke-direct {v1, v0}, Lxa;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
