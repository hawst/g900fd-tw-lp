.class public final Lbzu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Leyt;

.field private final b:Landroid/app/Activity;

.field private c:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Leyt;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbzu;->b:Landroid/app/Activity;

    .line 27
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbzu;->a:Leyt;

    .line 28
    return-void
.end method

.method public static b(Lfon;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 54
    const-string v1, " "

    iget-object v0, p0, Lfon;->b:[Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfon;->a:Lico;

    iget-object v0, v0, Lico;->a:[Lhgz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfon;->a:Lico;

    iget-object v0, v0, Lico;->a:[Lhgz;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lfon;->b:[Ljava/lang/CharSequence;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lfon;->a:Lico;

    iget-object v2, v2, Lico;->a:[Lhgz;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lfon;->b:[Ljava/lang/CharSequence;

    iget-object v3, p0, Lfon;->a:Lico;

    iget-object v3, v3, Lico;->a:[Lhgz;

    aget-object v3, v3, v0

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lfon;->b:[Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Lfvo;->a(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lfon;)V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lbzu;->c:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Leyv;

    iget-object v1, p0, Lbzu;->b:Landroid/app/Activity;

    invoke-direct {v0, v1}, Leyv;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900a1

    const/4 v2, 0x0

    .line 36
    invoke-virtual {v0, v1, v2}, Leyv;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbzu;->c:Landroid/app/AlertDialog;

    .line 40
    :cond_0
    invoke-static {p1}, Lbzu;->b(Lfon;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 41
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42
    iget-object v0, p0, Lbzu;->b:Landroid/app/Activity;

    const v1, 0x7f09001f

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 45
    :cond_1
    iget-object v1, p0, Lbzu;->c:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 46
    iget-object v0, p0, Lbzu;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 47
    return-void
.end method
