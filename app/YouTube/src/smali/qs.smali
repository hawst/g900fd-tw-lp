.class public final Lqs;
.super Landroid/widget/CheckBox;
.source "SourceFile"


# static fields
.field private static final a:[I


# instance fields
.field private final b:Lqw;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010107

    aput v2, v0, v1

    sput-object v0, Lqs;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const v0, 0x101006c

    invoke-direct {p0, p1, p2, v0}, Lqs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const v2, 0x101006c

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1, p2, v2}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    sget-object v0, Lqs;->a:[I

    invoke-static {p1, p2, v0, v2, v1}, Lrb;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lrb;

    move-result-object v0

    .line 49
    invoke-virtual {v0, v1}, Lrb;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lqs;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 50
    iget-object v1, v0, Lrb;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 52
    invoke-virtual {v0}, Lrb;->a()Lqw;

    move-result-object v0

    iput-object v0, p0, Lqs;->b:Lqw;

    .line 53
    return-void
.end method


# virtual methods
.method public final setButtonDrawable(I)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lqs;->b:Lqw;

    invoke-virtual {v0, p1}, Lqw;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lqs;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 58
    return-void
.end method
