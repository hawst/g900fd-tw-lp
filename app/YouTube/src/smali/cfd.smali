.class public final Lcfd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfut;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lfhz;

.field private c:Lcfa;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lfhz;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcfd;->a:Landroid/app/Activity;

    .line 27
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcfd;->b:Lfhz;

    .line 28
    return-void
.end method


# virtual methods
.method public a()Lcfa;
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcfd;->c:Lcfa;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcfa;

    iget-object v1, p0, Lcfd;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcfd;->b:Lfhz;

    invoke-direct {v0, v1, v2}, Lcfa;-><init>(Landroid/content/Context;Lfhz;)V

    iput-object v0, p0, Lcfd;->c:Lcfa;

    .line 53
    :cond_0
    iget-object v0, p0, Lcfd;->c:Lcfa;

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Lfsi;)V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lfkq;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const-class v0, Lfkq;

    invoke-virtual {p0}, Lcfd;->a()Lcfa;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 36
    :cond_0
    return-void
.end method
