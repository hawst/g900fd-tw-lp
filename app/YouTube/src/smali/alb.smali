.class public final Lalb;
.super Lajs;
.source "SourceFile"


# instance fields
.field private final a:Ldff;

.field private final b:Landroid/os/Handler;

.field private c:Lali;


# direct methods
.method public constructor <init>(Ldff;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lajs;-><init>()V

    .line 27
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldff;

    iput-object v0, p0, Lalb;->a:Ldff;

    .line 28
    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lalb;->b:Landroid/os/Handler;

    .line 29
    return-void
.end method

.method static synthetic a(Lalb;)Lali;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lalb;->c:Lali;

    return-object v0
.end method

.method static synthetic a(Lalb;Lali;)Lali;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lalb;->c:Lali;

    return-object p1
.end method

.method static synthetic b(Lalb;)Ldff;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lalb;->a:Ldff;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lalb;->b:Landroid/os/Handler;

    new-instance v1, Lald;

    invoke-direct {v1, p0}, Lald;-><init>(Lalb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 57
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lalb;->b:Landroid/os/Handler;

    new-instance v1, Lalg;

    invoke-direct {v1, p0, p1}, Lalg;-><init>(Lalb;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 88
    return-void
.end method

.method public final a(Laoe;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lalb;->b:Landroid/os/Handler;

    new-instance v1, Lalc;

    invoke-direct {v1, p0, p1}, Lalc;-><init>(Lalb;Laoe;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 47
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lalb;->b:Landroid/os/Handler;

    new-instance v1, Lalf;

    invoke-direct {v1, p0, p1, p2, p3}, Lalf;-><init>(Lalb;Ljava/lang/String;Ljava/util/List;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 78
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lalb;->b:Landroid/os/Handler;

    new-instance v1, Lale;

    invoke-direct {v1, p0, p1}, Lale;-><init>(Lalb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 67
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lalb;->b:Landroid/os/Handler;

    new-instance v1, Lalh;

    invoke-direct {v1, p0}, Lalh;-><init>(Lalb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 98
    return-void
.end method
