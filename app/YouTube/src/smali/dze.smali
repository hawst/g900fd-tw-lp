.class public final Ldze;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldyn;
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ldza;


# direct methods
.method private constructor <init>(Ldza;)V
    .locals 2

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    if-nez p1, :cond_0

    .line 199
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "dispatcher cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_0
    iput-object p1, p0, Ldze;->a:Ldza;

    .line 202
    return-void
.end method

.method public static a(Ldza;J)Ldze;
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 184
    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x30

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "delayMs cannot be negative: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    new-instance v0, Ldze;

    invoke-direct {v0, p0}, Ldze;-><init>(Ldza;)V

    .line 189
    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 190
    invoke-virtual {p0, v0}, Ldza;->execute(Ljava/lang/Runnable;)V

    .line 194
    :goto_0
    return-object v0

    .line 192
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v1, v0}, Ldza;->a(JLjava/lang/Object;Ldyn;)Ldym;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldym;)V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Ldze;->a:Ldza;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldza;->b:Z

    .line 210
    return-void
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Ldze;->a:Ldza;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldza;->b:Z

    .line 206
    return-void
.end method
