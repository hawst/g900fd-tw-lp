.class public final Lafe;
.super Ladp;
.source "SourceFile"

# interfaces
.implements Ladt;
.implements Lala;


# instance fields
.field private final a:Laff;

.field private b:Landroid/view/TextureView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Laff;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Ladp;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laff;

    iput-object v0, p0, Lafe;->a:Laff;

    .line 38
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 99
    return-object p0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lafe;->a:Laff;

    invoke-interface {v0}, Laff;->a()V

    .line 88
    return-void
.end method

.method public final a(Landroid/view/TextureView;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lafe;->b:Landroid/view/TextureView;

    .line 74
    invoke-virtual {p0, p1}, Lafe;->addView(Landroid/view/View;)V

    .line 75
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lafe;->b:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lafe;->b:Landroid/view/TextureView;

    invoke-virtual {p0, v0}, Lafe;->removeView(Landroid/view/View;)V

    .line 106
    iget-object v0, p0, Lafe;->b:Landroid/view/TextureView;

    invoke-virtual {p0, v0}, Lafe;->addView(Landroid/view/View;)V

    .line 108
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lafe;->b:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lafe;->b:Landroid/view/TextureView;

    invoke-virtual {p0, v0}, Lafe;->removeView(Landroid/view/View;)V

    .line 82
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lafe;->b:Landroid/view/TextureView;

    .line 83
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lafe;->a:Laff;

    invoke-interface {v0}, Laff;->b()V

    .line 93
    return-void
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Ladp;->onAttachedToWindow()V

    .line 64
    invoke-virtual {p0}, Lafe;->isHardwareAccelerated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lafe;->a:Laff;

    invoke-interface {v0}, Laff;->c()V

    .line 67
    :cond_0
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 54
    iget-object v0, p0, Lafe;->b:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lafe;->b:Landroid/view/TextureView;

    iget-object v1, p0, Lafe;->b:Landroid/view/TextureView;

    .line 56
    invoke-virtual {v1}, Landroid/view/TextureView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lafe;->b:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getMeasuredHeight()I

    move-result v2

    .line 55
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/TextureView;->layout(IIII)V

    .line 58
    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 42
    invoke-super {p0, p1, p2}, Ladp;->onMeasure(II)V

    .line 44
    invoke-virtual {p0}, Lafe;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 45
    invoke-virtual {p0}, Lafe;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 47
    iget-object v2, p0, Lafe;->b:Landroid/view/TextureView;

    if-eqz v2, :cond_0

    .line 48
    iget-object v2, p0, Lafe;->b:Landroid/view/TextureView;

    invoke-virtual {v2, v0, v1}, Landroid/view/TextureView;->measure(II)V

    .line 50
    :cond_0
    return-void
.end method
