.class public abstract Lfsb;
.super Lfsa;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lfsh;


# instance fields
.field private final a:Lfhz;

.field private b:Lfsj;

.field private c:Lhog;


# direct methods
.method public constructor <init>(Lfhz;Lfdw;Lfrz;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p2, p3}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 33
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lfsb;->a:Lfhz;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lfsb;->b:Lfsj;

    .line 35
    return-void
.end method

.method public constructor <init>(Lfhz;Lfsj;Lfdw;Lfrz;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p3, p4}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 42
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lfsb;->b:Lfsj;

    .line 43
    iget-object v0, p0, Lfsb;->b:Lfsj;

    invoke-interface {v0, p0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 44
    return-void
.end method


# virtual methods
.method public a(Lfsg;Lflb;)Landroid/view/View;
    .locals 2

    .prologue
    .line 76
    move-object v0, p2

    check-cast v0, Lfqh;

    invoke-super {p0, p1, v0}, Lfsa;->a(Lfsg;Lfqh;)Landroid/view/View;

    .line 77
    invoke-interface {p2}, Lflb;->b()Lhog;

    move-result-object v0

    iput-object v0, p0, Lfsb;->c:Lhog;

    .line 79
    iget-object v0, p0, Lfsb;->b:Lfsj;

    if-eqz v0, :cond_0

    .line 81
    iget-object v1, p0, Lfsb;->b:Lfsj;

    iget-object v0, p0, Lfsb;->c:Lhog;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lfsj;->a(Z)V

    .line 84
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 81
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 21
    check-cast p2, Lflb;

    invoke-virtual {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 21
    check-cast p2, Lflb;

    invoke-virtual {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lfsb;->c:Lhog;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lfsb;->a:Lfhz;

    iget-object v1, p0, Lfsb;->c:Lhog;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    .line 67
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 71
    invoke-virtual {p0}, Lfsb;->b()V

    .line 72
    return-void
.end method
