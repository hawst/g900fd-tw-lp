.class public final enum Ldgq;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldgq;

.field private static final synthetic c:[Ldgq;


# instance fields
.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 151
    new-instance v0, Ldgq;

    const-string v1, "PLAYER_RESPONSE_FAILED"

    invoke-direct {v0, v1, v2, v3}, Ldgq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldgq;->a:Ldgq;

    .line 150
    new-array v0, v3, [Ldgq;

    sget-object v1, Ldgq;->a:Ldgq;

    aput-object v1, v0, v2

    sput-object v0, Ldgq;->c:[Ldgq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 156
    const/4 v0, 0x1

    iput v0, p0, Ldgq;->b:I

    .line 157
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldgq;
    .locals 1

    .prologue
    .line 150
    const-class v0, Ldgq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldgq;

    return-object v0
.end method

.method public static values()[Ldgq;
    .locals 1

    .prologue
    .line 150
    sget-object v0, Ldgq;->c:[Ldgq;

    invoke-virtual {v0}, [Ldgq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldgq;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Ldgq;->b:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
