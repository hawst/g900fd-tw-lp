.class public final Lefk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefc;


# instance fields
.field private final a:Lega;

.field private b:Ljava/io/RandomAccessFile;

.field private c:J

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lefk;-><init>(Lega;)V

    .line 37
    return-void
.end method

.method private constructor <init>(Lega;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lefk;->a:Lega;

    .line 46
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    .line 69
    iget-wide v0, p0, Lefk;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 70
    const/4 v0, -0x1

    .line 86
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    :try_start_0
    iget-object v0, p0, Lefk;->b:Ljava/io/RandomAccessFile;

    iget-wide v2, p0, Lefk;->c:J

    int-to-long v4, p3

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, p1, p2, v1}, Ljava/io/RandomAccessFile;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 79
    if-lez v0, :cond_0

    .line 80
    iget-wide v2, p0, Lefk;->c:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lefk;->c:J

    .line 81
    iget-object v1, p0, Lefk;->a:Lega;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lefk;->a:Lega;

    invoke-interface {v1, v0}, Lega;->a(I)V

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    new-instance v1, Lefl;

    invoke-direct {v1, v0}, Lefl;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a(Lefg;)J
    .locals 4

    .prologue
    .line 51
    :try_start_0
    new-instance v0, Ljava/io/RandomAccessFile;

    iget-object v1, p1, Lefg;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "r"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lefk;->b:Ljava/io/RandomAccessFile;

    .line 52
    iget-object v0, p0, Lefk;->b:Ljava/io/RandomAccessFile;

    iget-wide v2, p1, Lefg;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 53
    iget-wide v0, p1, Lefg;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lefk;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    iget-wide v2, p1, Lefg;->d:J

    sub-long/2addr v0, v2

    :goto_0
    iput-wide v0, p0, Lefk;->c:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lefk;->d:Z

    .line 60
    iget-object v0, p0, Lefk;->a:Lega;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lefk;->a:Lega;

    invoke-interface {v0}, Lega;->b()V

    .line 64
    :cond_0
    iget-wide v0, p0, Lefk;->c:J

    return-wide v0

    .line 53
    :cond_1
    :try_start_1
    iget-wide v0, p1, Lefg;->e:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    new-instance v1, Lefl;

    invoke-direct {v1, v0}, Lefl;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 92
    iget-object v0, p0, Lefk;->b:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_0

    .line 94
    :try_start_0
    iget-object v0, p0, Lefk;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    iput-object v3, p0, Lefk;->b:Ljava/io/RandomAccessFile;

    .line 100
    iget-boolean v0, p0, Lefk;->d:Z

    if-eqz v0, :cond_0

    .line 101
    iput-boolean v2, p0, Lefk;->d:Z

    .line 102
    iget-object v0, p0, Lefk;->a:Lega;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lefk;->a:Lega;

    invoke-interface {v0}, Lega;->c()V

    .line 108
    :cond_0
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    :try_start_1
    new-instance v1, Lefl;

    invoke-direct {v1, v0}, Lefl;-><init>(Ljava/io/IOException;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    :catchall_0
    move-exception v0

    iput-object v3, p0, Lefk;->b:Ljava/io/RandomAccessFile;

    .line 100
    iget-boolean v1, p0, Lefk;->d:Z

    if-eqz v1, :cond_1

    .line 101
    iput-boolean v2, p0, Lefk;->d:Z

    .line 102
    iget-object v1, p0, Lefk;->a:Lega;

    if-eqz v1, :cond_1

    .line 103
    iget-object v1, p0, Lefk;->a:Lega;

    invoke-interface {v1}, Lega;->c()V

    :cond_1
    throw v0
.end method
