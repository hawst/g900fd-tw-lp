.class public final Laps;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder;


# instance fields
.field a:Lajl;

.field private final b:Landroid/os/Handler;

.field private final c:Lapt;

.field private final d:Ljava/util/List;

.field private e:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lajl;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Laps;->b:Landroid/os/Handler;

    .line 50
    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajl;

    iput-object v0, p0, Laps;->a:Lajl;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laps;->d:Ljava/util/List;

    .line 54
    new-instance v0, Lapw;

    invoke-direct {v0, p0}, Lapw;-><init>(Laps;)V

    iput-object v0, p0, Laps;->c:Lapt;

    .line 57
    :try_start_0
    iget-object v0, p0, Laps;->c:Lapt;

    invoke-interface {p2, v0}, Lajl;->a(Lany;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Laps;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Laps;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Laps;Landroid/view/Surface;)Landroid/view/Surface;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Laps;->e:Landroid/view/Surface;

    return-object p1
.end method

.method static synthetic a(Laps;III)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Laps;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/view/SurfaceHolder$Callback;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Laps;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Laps;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder$Callback;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Laps;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Laps;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder$Callback;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final addCallback(Landroid/view/SurfaceHolder$Callback;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Laps;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Laps;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_0
    return-void
.end method

.method public final getSurface()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Laps;->e:Landroid/view/Surface;

    return-object v0
.end method

.method public final getSurfaceFrame()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Laps;->a:Lajl;

    if-eqz v0, :cond_0

    .line 118
    :try_start_0
    iget-object v0, p0, Laps;->a:Lajl;

    invoke-interface {v0}, Lajl;->b()Landroid/graphics/Rect;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    goto :goto_0
.end method

.method public final isCreating()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Laps;->a:Lajl;

    if-eqz v0, :cond_0

    .line 106
    :try_start_0
    iget-object v0, p0, Laps;->a:Lajl;

    invoke-interface {v0}, Lajl;->a()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 111
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lockCanvas()Landroid/graphics/Canvas;
    .locals 2

    .prologue
    .line 185
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported call to lockCanvas"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported call to lockCanvas"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final removeCallback(Landroid/view/SurfaceHolder$Callback;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Laps;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method

.method public final setFixedSize(II)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Laps;->a:Lajl;

    if-eqz v0, :cond_0

    .line 142
    :try_start_0
    iget-object v0, p0, Laps;->a:Lajl;

    invoke-interface {v0, p1, p2}, Lajl;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFormat(I)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Laps;->a:Lajl;

    if-eqz v0, :cond_0

    .line 153
    :try_start_0
    iget-object v0, p0, Laps;->a:Lajl;

    invoke-interface {v0, p1}, Lajl;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setKeepScreenOn(Z)V
    .locals 1

    .prologue
    .line 128
    const-string v0, "setKeepScreenOn should be through player Surface, not SurfaceHolder"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Laps;->a:Lajl;

    if-eqz v0, :cond_0

    .line 131
    :try_start_0
    iget-object v0, p0, Laps;->a:Lajl;

    invoke-interface {v0, p1}, Lajl;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setSizeFromLayout()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Laps;->a:Lajl;

    if-eqz v0, :cond_0

    .line 164
    :try_start_0
    iget-object v0, p0, Laps;->a:Lajl;

    invoke-interface {v0}, Lajl;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setType(I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Laps;->a:Lajl;

    if-eqz v0, :cond_0

    .line 176
    :try_start_0
    iget-object v0, p0, Laps;->a:Lajl;

    invoke-interface {v0, p1}, Lajl;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 195
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported call to unlockCanvasAndPost"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
