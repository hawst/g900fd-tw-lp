.class public final Lhej;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:J

.field public d:I

.field public e:I

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:Lgzq;

.field public k:Lhgx;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:J

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:Lhkl;

.field private t:Z

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 39360
    invoke-direct {p0}, Lidf;-><init>()V

    .line 39363
    const-string v0, ""

    iput-object v0, p0, Lhej;->l:Ljava/lang/String;

    .line 39366
    const-string v0, ""

    iput-object v0, p0, Lhej;->a:Ljava/lang/String;

    .line 39369
    iput v1, p0, Lhej;->b:I

    .line 39372
    iput-wide v4, p0, Lhej;->c:J

    .line 39375
    iput v1, p0, Lhej;->d:I

    .line 39378
    iput v1, p0, Lhej;->e:I

    .line 39381
    iput v1, p0, Lhej;->f:I

    .line 39384
    iput-boolean v1, p0, Lhej;->m:Z

    .line 39387
    iput-boolean v1, p0, Lhej;->g:Z

    .line 39390
    iput-boolean v1, p0, Lhej;->h:Z

    .line 39393
    const-string v0, ""

    iput-object v0, p0, Lhej;->i:Ljava/lang/String;

    .line 39396
    iput-wide v4, p0, Lhej;->n:J

    .line 39399
    const-string v0, ""

    iput-object v0, p0, Lhej;->o:Ljava/lang/String;

    .line 39402
    const-string v0, ""

    iput-object v0, p0, Lhej;->p:Ljava/lang/String;

    .line 39405
    const-string v0, ""

    iput-object v0, p0, Lhej;->q:Ljava/lang/String;

    .line 39408
    iput v1, p0, Lhej;->r:I

    .line 39411
    iput-object v2, p0, Lhej;->j:Lgzq;

    .line 39414
    iput-object v2, p0, Lhej;->s:Lhkl;

    .line 39417
    iput-boolean v1, p0, Lhej;->t:Z

    .line 39420
    iput-boolean v1, p0, Lhej;->u:Z

    .line 39423
    iput-object v2, p0, Lhej;->k:Lhgx;

    .line 39360
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 39580
    const/4 v0, 0x0

    .line 39581
    iget-object v1, p0, Lhej;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 39582
    const/4 v0, 0x1

    iget-object v1, p0, Lhej;->l:Ljava/lang/String;

    .line 39583
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 39585
    :cond_0
    iget-object v1, p0, Lhej;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 39586
    const/4 v1, 0x3

    iget-object v2, p0, Lhej;->a:Ljava/lang/String;

    .line 39587
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39589
    :cond_1
    iget v1, p0, Lhej;->b:I

    if-eqz v1, :cond_2

    .line 39590
    const/4 v1, 0x4

    iget v2, p0, Lhej;->b:I

    .line 39591
    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39593
    :cond_2
    iget-wide v2, p0, Lhej;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 39594
    const/4 v1, 0x5

    iget-wide v2, p0, Lhej;->c:J

    .line 39595
    invoke-static {v1, v2, v3}, Lidd;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 39597
    :cond_3
    iget v1, p0, Lhej;->d:I

    if-eqz v1, :cond_4

    .line 39598
    const/4 v1, 0x6

    iget v2, p0, Lhej;->d:I

    .line 39599
    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39601
    :cond_4
    iget v1, p0, Lhej;->e:I

    if-eqz v1, :cond_5

    .line 39602
    const/4 v1, 0x7

    iget v2, p0, Lhej;->e:I

    .line 39603
    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39605
    :cond_5
    iget v1, p0, Lhej;->f:I

    if-eqz v1, :cond_6

    .line 39606
    const/16 v1, 0x8

    iget v2, p0, Lhej;->f:I

    .line 39607
    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39609
    :cond_6
    iget-boolean v1, p0, Lhej;->m:Z

    if-eqz v1, :cond_7

    .line 39610
    const/16 v1, 0x9

    iget-boolean v2, p0, Lhej;->m:Z

    .line 39611
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 39613
    :cond_7
    iget-boolean v1, p0, Lhej;->g:Z

    if-eqz v1, :cond_8

    .line 39614
    const/16 v1, 0xa

    iget-boolean v2, p0, Lhej;->g:Z

    .line 39615
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 39617
    :cond_8
    iget-boolean v1, p0, Lhej;->h:Z

    if-eqz v1, :cond_9

    .line 39618
    const/16 v1, 0xb

    iget-boolean v2, p0, Lhej;->h:Z

    .line 39619
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 39621
    :cond_9
    iget-object v1, p0, Lhej;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 39622
    const/16 v1, 0xc

    iget-object v2, p0, Lhej;->i:Ljava/lang/String;

    .line 39623
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39625
    :cond_a
    iget-wide v2, p0, Lhej;->n:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_b

    .line 39626
    const/16 v1, 0xd

    iget-wide v2, p0, Lhej;->n:J

    .line 39627
    invoke-static {v1, v2, v3}, Lidd;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 39629
    :cond_b
    iget-object v1, p0, Lhej;->o:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 39630
    const/16 v1, 0xf

    iget-object v2, p0, Lhej;->o:Ljava/lang/String;

    .line 39631
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39633
    :cond_c
    iget-object v1, p0, Lhej;->p:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 39634
    const/16 v1, 0x10

    iget-object v2, p0, Lhej;->p:Ljava/lang/String;

    .line 39635
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39637
    :cond_d
    iget-object v1, p0, Lhej;->q:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 39638
    const/16 v1, 0x13

    iget-object v2, p0, Lhej;->q:Ljava/lang/String;

    .line 39639
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39641
    :cond_e
    iget v1, p0, Lhej;->r:I

    if-eqz v1, :cond_f

    .line 39642
    const/16 v1, 0x14

    iget v2, p0, Lhej;->r:I

    .line 39643
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39645
    :cond_f
    iget-object v1, p0, Lhej;->j:Lgzq;

    if-eqz v1, :cond_10

    .line 39646
    const/16 v1, 0x15

    iget-object v2, p0, Lhej;->j:Lgzq;

    .line 39647
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39649
    :cond_10
    iget-object v1, p0, Lhej;->s:Lhkl;

    if-eqz v1, :cond_11

    .line 39650
    const/16 v1, 0x16

    iget-object v2, p0, Lhej;->s:Lhkl;

    .line 39651
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39653
    :cond_11
    iget-boolean v1, p0, Lhej;->t:Z

    if-eqz v1, :cond_12

    .line 39654
    const/16 v1, 0x17

    iget-boolean v2, p0, Lhej;->t:Z

    .line 39655
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 39657
    :cond_12
    iget-boolean v1, p0, Lhej;->u:Z

    if-eqz v1, :cond_13

    .line 39658
    const/16 v1, 0x18

    iget-boolean v2, p0, Lhej;->u:Z

    .line 39659
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 39661
    :cond_13
    iget-object v1, p0, Lhej;->k:Lhgx;

    if-eqz v1, :cond_14

    .line 39662
    const/16 v1, 0x19

    iget-object v2, p0, Lhej;->k:Lhgx;

    .line 39663
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39665
    :cond_14
    iget-object v1, p0, Lhej;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39666
    iput v0, p0, Lhej;->J:I

    .line 39667
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 39356
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhej;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhej;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhej;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhej;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhej;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhej;->b:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhej;->c:J

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhej;->d:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhej;->e:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhej;->f:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhej;->m:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhej;->g:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhej;->h:Z

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhej;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lhej;->n:J

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhej;->o:Ljava/lang/String;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhej;->p:Ljava/lang/String;

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhej;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lhej;->r:I

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lhej;->r:I

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lhej;->j:Lgzq;

    if-nez v0, :cond_4

    new-instance v0, Lgzq;

    invoke-direct {v0}, Lgzq;-><init>()V

    iput-object v0, p0, Lhej;->j:Lgzq;

    :cond_4
    iget-object v0, p0, Lhej;->j:Lgzq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lhej;->s:Lhkl;

    if-nez v0, :cond_5

    new-instance v0, Lhkl;

    invoke-direct {v0}, Lhkl;-><init>()V

    iput-object v0, p0, Lhej;->s:Lhkl;

    :cond_5
    iget-object v0, p0, Lhej;->s:Lhkl;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhej;->t:Z

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhej;->u:Z

    goto/16 :goto_0

    :sswitch_15
    iget-object v0, p0, Lhej;->k:Lhgx;

    if-nez v0, :cond_6

    new-instance v0, Lhgx;

    invoke-direct {v0}, Lhgx;-><init>()V

    iput-object v0, p0, Lhej;->k:Lhgx;

    :cond_6
    iget-object v0, p0, Lhej;->k:Lhgx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x7a -> :sswitch_d
        0x82 -> :sswitch_e
        0x9a -> :sswitch_f
        0xa0 -> :sswitch_10
        0xaa -> :sswitch_11
        0xb2 -> :sswitch_12
        0xb8 -> :sswitch_13
        0xc0 -> :sswitch_14
        0xca -> :sswitch_15
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 39511
    iget-object v0, p0, Lhej;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39512
    const/4 v0, 0x1

    iget-object v1, p0, Lhej;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 39514
    :cond_0
    iget-object v0, p0, Lhej;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 39515
    const/4 v0, 0x3

    iget-object v1, p0, Lhej;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 39517
    :cond_1
    iget v0, p0, Lhej;->b:I

    if-eqz v0, :cond_2

    .line 39518
    const/4 v0, 0x4

    iget v1, p0, Lhej;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    .line 39520
    :cond_2
    iget-wide v0, p0, Lhej;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 39521
    const/4 v0, 0x5

    iget-wide v2, p0, Lhej;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 39523
    :cond_3
    iget v0, p0, Lhej;->d:I

    if-eqz v0, :cond_4

    .line 39524
    const/4 v0, 0x6

    iget v1, p0, Lhej;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    .line 39526
    :cond_4
    iget v0, p0, Lhej;->e:I

    if-eqz v0, :cond_5

    .line 39527
    const/4 v0, 0x7

    iget v1, p0, Lhej;->e:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    .line 39529
    :cond_5
    iget v0, p0, Lhej;->f:I

    if-eqz v0, :cond_6

    .line 39530
    const/16 v0, 0x8

    iget v1, p0, Lhej;->f:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    .line 39532
    :cond_6
    iget-boolean v0, p0, Lhej;->m:Z

    if-eqz v0, :cond_7

    .line 39533
    const/16 v0, 0x9

    iget-boolean v1, p0, Lhej;->m:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 39535
    :cond_7
    iget-boolean v0, p0, Lhej;->g:Z

    if-eqz v0, :cond_8

    .line 39536
    const/16 v0, 0xa

    iget-boolean v1, p0, Lhej;->g:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 39538
    :cond_8
    iget-boolean v0, p0, Lhej;->h:Z

    if-eqz v0, :cond_9

    .line 39539
    const/16 v0, 0xb

    iget-boolean v1, p0, Lhej;->h:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 39541
    :cond_9
    iget-object v0, p0, Lhej;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 39542
    const/16 v0, 0xc

    iget-object v1, p0, Lhej;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 39544
    :cond_a
    iget-wide v0, p0, Lhej;->n:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_b

    .line 39545
    const/16 v0, 0xd

    iget-wide v2, p0, Lhej;->n:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(IJ)V

    .line 39547
    :cond_b
    iget-object v0, p0, Lhej;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 39548
    const/16 v0, 0xf

    iget-object v1, p0, Lhej;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 39550
    :cond_c
    iget-object v0, p0, Lhej;->p:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 39551
    const/16 v0, 0x10

    iget-object v1, p0, Lhej;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 39553
    :cond_d
    iget-object v0, p0, Lhej;->q:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 39554
    const/16 v0, 0x13

    iget-object v1, p0, Lhej;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 39556
    :cond_e
    iget v0, p0, Lhej;->r:I

    if-eqz v0, :cond_f

    .line 39557
    const/16 v0, 0x14

    iget v1, p0, Lhej;->r:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 39559
    :cond_f
    iget-object v0, p0, Lhej;->j:Lgzq;

    if-eqz v0, :cond_10

    .line 39560
    const/16 v0, 0x15

    iget-object v1, p0, Lhej;->j:Lgzq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 39562
    :cond_10
    iget-object v0, p0, Lhej;->s:Lhkl;

    if-eqz v0, :cond_11

    .line 39563
    const/16 v0, 0x16

    iget-object v1, p0, Lhej;->s:Lhkl;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 39565
    :cond_11
    iget-boolean v0, p0, Lhej;->t:Z

    if-eqz v0, :cond_12

    .line 39566
    const/16 v0, 0x17

    iget-boolean v1, p0, Lhej;->t:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 39568
    :cond_12
    iget-boolean v0, p0, Lhej;->u:Z

    if-eqz v0, :cond_13

    .line 39569
    const/16 v0, 0x18

    iget-boolean v1, p0, Lhej;->u:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 39571
    :cond_13
    iget-object v0, p0, Lhej;->k:Lhgx;

    if-eqz v0, :cond_14

    .line 39572
    const/16 v0, 0x19

    iget-object v1, p0, Lhej;->k:Lhgx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 39574
    :cond_14
    iget-object v0, p0, Lhej;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 39576
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 39454
    if-ne p1, p0, :cond_1

    .line 39478
    :cond_0
    :goto_0
    return v0

    .line 39455
    :cond_1
    instance-of v2, p1, Lhej;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 39456
    :cond_2
    check-cast p1, Lhej;

    .line 39457
    iget-object v2, p0, Lhej;->l:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhej;->l:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhej;->a:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhej;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 39458
    :goto_2
    iget v2, p0, Lhej;->b:I

    iget v3, p1, Lhej;->b:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lhej;->c:J

    iget-wide v4, p1, Lhej;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lhej;->d:I

    iget v3, p1, Lhej;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhej;->e:I

    iget v3, p1, Lhej;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhej;->f:I

    iget v3, p1, Lhej;->f:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhej;->m:Z

    iget-boolean v3, p1, Lhej;->m:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhej;->g:Z

    iget-boolean v3, p1, Lhej;->g:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhej;->h:Z

    iget-boolean v3, p1, Lhej;->h:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhej;->i:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhej;->i:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 39467
    :goto_3
    iget-wide v2, p0, Lhej;->n:J

    iget-wide v4, p1, Lhej;->n:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhej;->o:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhej;->o:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 39469
    :goto_4
    iget-object v2, p0, Lhej;->p:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhej;->p:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 39470
    :goto_5
    iget-object v2, p0, Lhej;->q:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhej;->q:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 39471
    :goto_6
    iget v2, p0, Lhej;->r:I

    iget v3, p1, Lhej;->r:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhej;->j:Lgzq;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhej;->j:Lgzq;

    if-nez v2, :cond_3

    .line 39473
    :goto_7
    iget-object v2, p0, Lhej;->s:Lhkl;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhej;->s:Lhkl;

    if-nez v2, :cond_3

    .line 39474
    :goto_8
    iget-boolean v2, p0, Lhej;->t:Z

    iget-boolean v3, p1, Lhej;->t:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhej;->u:Z

    iget-boolean v3, p1, Lhej;->u:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhej;->k:Lhgx;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhej;->k:Lhgx;

    if-nez v2, :cond_3

    .line 39477
    :goto_9
    iget-object v2, p0, Lhej;->I:Ljava/util/List;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhej;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 39478
    goto/16 :goto_0

    .line 39457
    :cond_4
    iget-object v2, p0, Lhej;->l:Ljava/lang/String;

    iget-object v3, p1, Lhej;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhej;->a:Ljava/lang/String;

    iget-object v3, p1, Lhej;->a:Ljava/lang/String;

    .line 39458
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhej;->i:Ljava/lang/String;

    iget-object v3, p1, Lhej;->i:Ljava/lang/String;

    .line 39467
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhej;->o:Ljava/lang/String;

    iget-object v3, p1, Lhej;->o:Ljava/lang/String;

    .line 39469
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhej;->p:Ljava/lang/String;

    iget-object v3, p1, Lhej;->p:Ljava/lang/String;

    .line 39470
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhej;->q:Ljava/lang/String;

    iget-object v3, p1, Lhej;->q:Ljava/lang/String;

    .line 39471
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhej;->j:Lgzq;

    iget-object v3, p1, Lhej;->j:Lgzq;

    .line 39473
    invoke-virtual {v2, v3}, Lgzq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lhej;->s:Lhkl;

    iget-object v3, p1, Lhej;->s:Lhkl;

    .line 39474
    invoke-virtual {v2, v3}, Lhkl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Lhej;->k:Lhgx;

    iget-object v3, p1, Lhej;->k:Lhgx;

    .line 39477
    invoke-virtual {v2, v3}, Lhgx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_9

    :cond_d
    iget-object v2, p0, Lhej;->I:Ljava/util/List;

    iget-object v3, p1, Lhej;->I:Ljava/util/List;

    .line 39478
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 39482
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 39484
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhej;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 39485
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhej;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 39486
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhej;->b:I

    add-int/2addr v0, v4

    .line 39487
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lhej;->c:J

    iget-wide v6, p0, Lhej;->c:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 39488
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhej;->d:I

    add-int/2addr v0, v4

    .line 39489
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhej;->e:I

    add-int/2addr v0, v4

    .line 39490
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhej;->f:I

    add-int/2addr v0, v4

    .line 39491
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhej;->m:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v4

    .line 39492
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhej;->g:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v4

    .line 39493
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhej;->h:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    .line 39494
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhej;->i:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    .line 39495
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lhej;->n:J

    iget-wide v6, p0, Lhej;->n:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 39496
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhej;->o:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v4

    .line 39497
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhej;->p:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v4

    .line 39498
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhej;->q:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v4

    .line 39499
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhej;->r:I

    add-int/2addr v0, v4

    .line 39500
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhej;->j:Lgzq;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v4

    .line 39501
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhej;->s:Lhkl;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v4

    .line 39502
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhej;->t:Z

    if-eqz v0, :cond_b

    move v0, v2

    :goto_b
    add-int/2addr v0, v4

    .line 39503
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lhej;->u:Z

    if-eqz v4, :cond_c

    :goto_c
    add-int/2addr v0, v2

    .line 39504
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhej;->k:Lhgx;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 39505
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhej;->I:Ljava/util/List;

    if-nez v2, :cond_e

    :goto_e
    add-int/2addr v0, v1

    .line 39506
    return v0

    .line 39484
    :cond_0
    iget-object v0, p0, Lhej;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 39485
    :cond_1
    iget-object v0, p0, Lhej;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_2
    move v0, v3

    .line 39491
    goto :goto_2

    :cond_3
    move v0, v3

    .line 39492
    goto :goto_3

    :cond_4
    move v0, v3

    .line 39493
    goto :goto_4

    .line 39494
    :cond_5
    iget-object v0, p0, Lhej;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 39496
    :cond_6
    iget-object v0, p0, Lhej;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 39497
    :cond_7
    iget-object v0, p0, Lhej;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_7

    .line 39498
    :cond_8
    iget-object v0, p0, Lhej;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 39500
    :cond_9
    iget-object v0, p0, Lhej;->j:Lgzq;

    invoke-virtual {v0}, Lgzq;->hashCode()I

    move-result v0

    goto :goto_9

    .line 39501
    :cond_a
    iget-object v0, p0, Lhej;->s:Lhkl;

    invoke-virtual {v0}, Lhkl;->hashCode()I

    move-result v0

    goto :goto_a

    :cond_b
    move v0, v3

    .line 39502
    goto :goto_b

    :cond_c
    move v2, v3

    .line 39503
    goto :goto_c

    .line 39504
    :cond_d
    iget-object v0, p0, Lhej;->k:Lhgx;

    invoke-virtual {v0}, Lhgx;->hashCode()I

    move-result v0

    goto :goto_d

    .line 39505
    :cond_e
    iget-object v1, p0, Lhej;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_e
.end method
