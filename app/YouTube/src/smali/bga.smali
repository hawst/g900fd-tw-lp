.class final Lbga;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field private synthetic a:Lbfw;


# direct methods
.method constructor <init>(Lbfw;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lbga;->a:Lbfw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 3

    .prologue
    .line 263
    iget-object v0, p0, Lbga;->a:Lbfw;

    invoke-static {v0}, Lbfw;->e(Lbfw;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lbga;->a:Lbfw;

    invoke-static {v1}, Lbfw;->d(Lbfw;)Leyt;

    move-result-object v1

    invoke-interface {v1, p1}, Leyt;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;Z)V

    .line 264
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 259
    check-cast p1, Lfoc;

    invoke-virtual {p1}, Lfoc;->a()Lfon;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbga;->a:Lbfw;

    invoke-virtual {p1}, Lfoc;->a()Lfon;

    move-result-object v1

    invoke-static {v0, v1}, Lbfw;->a(Lbfw;Lfon;)V

    iget-object v0, p0, Lbga;->a:Lbfw;

    invoke-virtual {v0}, Lbfw;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lfoc;->b:Lfom;

    if-nez v0, :cond_1

    iget-object v0, p1, Lfoc;->a:Licb;

    iget-object v0, v0, Licb;->a:Lhxn;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lfoc;->a:Licb;

    iget-object v0, v0, Licb;->a:Lhxn;

    iget-object v0, v0, Lhxn;->a:Licn;

    if-eqz v0, :cond_1

    new-instance v0, Lfom;

    iget-object v1, p1, Lfoc;->a:Licb;

    iget-object v1, v1, Licb;->a:Lhxn;

    iget-object v1, v1, Lhxn;->a:Licn;

    invoke-direct {v0, v1}, Lfom;-><init>(Licn;)V

    iput-object v0, p1, Lfoc;->b:Lfom;

    :cond_1
    iget-object v1, p1, Lfoc;->b:Lfom;

    if-nez v1, :cond_2

    const-string v0, "TipResponse missing screen data."

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lbga;->a:Lbfw;

    invoke-virtual {v0}, Lbfw;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbga;->a:Lbfw;

    invoke-static {v0}, Lbfw;->f(Lbfw;)Lcag;

    move-result-object v0

    iput-object v1, v0, Lcag;->k:Lfom;

    iget-object v2, v0, Lcag;->j:Ljava/text/NumberFormat;

    iget-object v3, v1, Lfom;->g:Ljava/util/Currency;

    if-nez v3, :cond_3

    iget-object v3, v1, Lfom;->a:Licn;

    iget-object v3, v3, Licn;->e:Lhxm;

    if-eqz v3, :cond_3

    iget-object v3, v1, Lfom;->a:Licn;

    iget-object v3, v3, Licn;->e:Lhxm;

    iget-object v3, v3, Lhxm;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v3

    iput-object v3, v1, Lfom;->g:Ljava/util/Currency;

    :cond_3
    iget-object v3, v1, Lfom;->g:Ljava/util/Currency;

    invoke-virtual {v2, v3}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    iget-object v2, v0, Lcag;->c:Landroid/widget/TextView;

    iget-object v3, v1, Lfom;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_4

    iget-object v3, v1, Lfom;->a:Licn;

    iget-object v3, v3, Licn;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v1, Lfom;->b:Ljava/lang/CharSequence;

    :cond_4
    iget-object v3, v1, Lfom;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcag;->e:Landroid/widget/TextView;

    iget-object v3, v1, Lfom;->c:Ljava/lang/CharSequence;

    if-nez v3, :cond_5

    iget-object v3, v1, Lfom;->a:Licn;

    iget-object v3, v3, Licn;->f:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v1, Lfom;->c:Ljava/lang/CharSequence;

    :cond_5
    iget-object v3, v1, Lfom;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcag;->c()V

    iget-object v2, v0, Lcag;->g:Lfvi;

    iget-object v3, v1, Lfom;->d:Lfnc;

    if-nez v3, :cond_6

    new-instance v3, Lfnc;

    iget-object v4, v1, Lfom;->a:Licn;

    iget-object v4, v4, Licn;->b:Lhxf;

    invoke-direct {v3, v4}, Lfnc;-><init>(Lhxf;)V

    iput-object v3, v1, Lfom;->d:Lfnc;

    :cond_6
    iget-object v3, v1, Lfom;->d:Lfnc;

    invoke-virtual {v2, v3, v5}, Lfvi;->a(Lfnc;Leyo;)V

    iget-object v2, v0, Lcag;->h:Lfvi;

    iget-object v3, v1, Lfom;->e:Lfnc;

    if-nez v3, :cond_7

    new-instance v3, Lfnc;

    iget-object v4, v1, Lfom;->a:Licn;

    iget-object v4, v4, Licn;->d:Lhxf;

    invoke-direct {v3, v4}, Lfnc;-><init>(Lhxf;)V

    iput-object v3, v1, Lfom;->e:Lfnc;

    :cond_7
    iget-object v3, v1, Lfom;->e:Lfnc;

    invoke-virtual {v2, v3, v5}, Lfvi;->a(Lfnc;Leyo;)V

    iget-object v2, v0, Lcag;->i:Lfvi;

    invoke-virtual {v1}, Lfom;->a()Lfnc;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lfvi;->a(Lfnc;Leyo;)V

    iget-object v2, v0, Lcag;->m:Lbzy;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    iput-object v0, v2, Lbzy;->l:Lfom;

    invoke-virtual {v1}, Lfom;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, v2, Lbzy;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v2, Lbzy;->d:Landroid/widget/TextView;

    iget-object v3, v1, Lfom;->k:Ljava/lang/CharSequence;

    if-nez v3, :cond_8

    invoke-virtual {v1}, Lfom;->f()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, v1, Lfom;->a:Licn;

    iget-object v3, v3, Licn;->g:Lhdd;

    iget-object v3, v3, Lhdd;->b:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v1, Lfom;->k:Ljava/lang/CharSequence;

    :cond_8
    iget-object v3, v1, Lfom;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v2, Lbzy;->f:Lfvi;

    invoke-virtual {v1}, Lfom;->a()Lfnc;

    move-result-object v3

    invoke-virtual {v0, v3, v5}, Lfvi;->a(Lfnc;Leyo;)V

    iget-object v0, v2, Lbzy;->g:Landroid/widget/TextView;

    iget-object v3, v1, Lfom;->j:Ljava/lang/CharSequence;

    if-nez v3, :cond_9

    invoke-virtual {v1}, Lfom;->f()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, v1, Lfom;->a:Licn;

    iget-object v3, v3, Licn;->g:Lhdd;

    iget-object v3, v3, Lhdd;->d:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v1, Lfom;->j:Ljava/lang/CharSequence;

    :cond_9
    iget-object v3, v1, Lfom;->j:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v2, Lbzy;->i:Landroid/widget/TextView;

    iget-object v3, v1, Lfom;->m:Ljava/lang/CharSequence;

    if-nez v3, :cond_a

    invoke-virtual {v1}, Lfom;->f()Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, v1, Lfom;->a:Licn;

    iget-object v3, v3, Licn;->g:Lhdd;

    iget-object v3, v3, Lhdd;->e:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v1, Lfom;->m:Ljava/lang/CharSequence;

    :cond_a
    iget-object v3, v1, Lfom;->m:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v2, Lbzy;->j:Landroid/widget/TextView;

    iget-object v3, v1, Lfom;->n:Ljava/lang/CharSequence;

    if-nez v3, :cond_b

    invoke-virtual {v1}, Lfom;->f()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, v1, Lfom;->a:Licn;

    iget-object v3, v3, Licn;->g:Lhdd;

    iget-object v3, v3, Lhdd;->h:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v1, Lfom;->n:Ljava/lang/CharSequence;

    :cond_b
    iget-object v3, v1, Lfom;->n:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v2, Lbzy;->k:Landroid/widget/TextView;

    iget-object v2, v1, Lfom;->l:Ljava/lang/CharSequence;

    if-nez v2, :cond_c

    invoke-virtual {v1}, Lfom;->f()Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, v1, Lfom;->a:Licn;

    iget-object v2, v2, Licn;->g:Lhdd;

    iget-object v2, v2, Lhdd;->c:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfom;->l:Ljava/lang/CharSequence;

    :cond_c
    iget-object v2, v1, Lfom;->l:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lbga;->a:Lbfw;

    iget-object v2, v1, Lfom;->f:Lfok;

    if-nez v2, :cond_d

    iget-object v2, v1, Lfom;->a:Licn;

    iget-object v2, v2, Licn;->h:Lhfv;

    if-eqz v2, :cond_d

    new-instance v2, Lfok;

    iget-object v3, v1, Lfom;->a:Licn;

    iget-object v3, v3, Licn;->h:Lhfv;

    invoke-direct {v2, v3}, Lfok;-><init>(Lhfv;)V

    iput-object v2, v1, Lfom;->f:Lfok;

    :cond_d
    iget-object v1, v1, Lfom;->f:Lfok;

    invoke-static {v0, v1}, Lbfw;->a(Lbfw;Lfok;)Lfok;

    iget-object v0, p0, Lbga;->a:Lbfw;

    invoke-static {v0}, Lbfw;->e(Lbfw;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    iget-object v0, p0, Lbga;->a:Lbfw;

    invoke-static {v0, v6}, Lbfw;->a(Lbfw;Z)V

    goto/16 :goto_0

    :cond_e
    iget-object v0, v2, Lbzy;->c:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
