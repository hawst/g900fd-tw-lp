.class public final Linp;
.super Ljava/net/HttpURLConnection;
.source "SourceFile"


# instance fields
.field final a:Lilw;

.field public b:Linc;

.field c:Lilr;

.field private d:Lilt;

.field private e:J

.field private f:I

.field private g:Ljava/io/IOException;

.field private h:Lils;

.field private i:Limg;


# direct methods
.method public constructor <init>(Ljava/net/URL;Lilw;)V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0, p1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    .line 74
    new-instance v0, Lilt;

    invoke-direct {v0}, Lilt;-><init>()V

    iput-object v0, p0, Linp;->d:Lilt;

    .line 77
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Linp;->e:J

    .line 98
    iput-object p2, p0, Linp;->a:Lilw;

    .line 99
    return-void
.end method

.method private a()Lils;
    .locals 5

    .prologue
    .line 139
    iget-object v0, p0, Linp;->h:Lils;

    if-nez v0, :cond_0

    .line 140
    invoke-direct {p0}, Linp;->c()Linc;

    move-result-object v0

    invoke-virtual {v0}, Linc;->e()Limd;

    move-result-object v0

    .line 141
    iget-object v1, v0, Limd;->f:Lils;

    .line 143
    invoke-virtual {v1}, Lils;->a()Lilt;

    move-result-object v1

    invoke-static {}, Limj;->a()Limj;

    invoke-static {}, Limj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "-Response-Source"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Limd;->h:Limd;

    if-nez v3, :cond_2

    iget-object v3, v0, Limd;->i:Limd;

    if-nez v3, :cond_1

    const-string v0, "NONE"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lilt;->a(Ljava/lang/String;Ljava/lang/String;)Lilt;

    move-result-object v0

    invoke-virtual {v0}, Lilt;->a()Lils;

    move-result-object v0

    iput-object v0, p0, Linp;->h:Lils;

    .line 147
    :cond_0
    iget-object v0, p0, Linp;->h:Lils;

    return-object v0

    .line 143
    :cond_1
    iget v0, v0, Limd;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "CACHE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v3, v0, Limd;->i:Limd;

    if-nez v3, :cond_3

    iget v0, v0, Limd;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x13

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "NETWORK "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, v0, Limd;->h:Limd;

    iget v0, v0, Limd;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "CONDITIONAL_CACHE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Liln;Lini;Limd;)Linc;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 317
    new-instance v0, Limc;

    invoke-direct {v0}, Limc;-><init>()V

    invoke-virtual {p0}, Linp;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v0, v2}, Limc;->a(Ljava/net/URL;)Limc;

    move-result-object v0

    invoke-virtual {v0, p1, v5}, Limc;->a(Ljava/lang/String;Lt;)Limc;

    move-result-object v2

    .line 320
    iget-object v0, p0, Linp;->d:Lilt;

    invoke-virtual {v0}, Lilt;->a()Lils;

    move-result-object v4

    move v0, v1

    .line 321
    :goto_0
    iget-object v3, v4, Lils;->a:[Ljava/lang/String;

    array-length v3, v3

    div-int/lit8 v3, v3, 0x2

    if-ge v0, v3, :cond_0

    .line 322
    invoke-virtual {v4, v0}, Lils;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v0}, Lils;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Limc;->b(Ljava/lang/String;Ljava/lang/String;)Limc;

    .line 321
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 326
    :cond_0
    invoke-static {p1}, Line;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328
    iget-wide v6, p0, Linp;->e:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    .line 329
    const-string v0, "Content-Length"

    iget-wide v6, p0, Linp;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    .line 337
    :goto_1
    const-string v0, "Content-Type"

    invoke-virtual {v4, v0}, Lils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 338
    const-string v0, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded"

    invoke-virtual {v2, v0, v3}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    :cond_1
    move v3, v1

    .line 342
    const-string v0, "User-Agent"

    invoke-virtual {v4, v0}, Lils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 343
    const-string v1, "User-Agent"

    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    :goto_2
    invoke-virtual {v2, v1, v0}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    .line 346
    :cond_2
    invoke-virtual {v2}, Limc;->a()Limb;

    move-result-object v2

    .line 349
    iget-object v1, p0, Linp;->a:Lilw;

    .line 350
    sget-object v0, Limh;->a:Limh;

    invoke-virtual {v0, v1}, Limh;->a(Lilw;)Lu;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Linp;->getUseCaches()Z

    move-result v0

    if-nez v0, :cond_3

    .line 351
    iget-object v0, p0, Linp;->a:Lilw;

    invoke-virtual {v0}, Lilw;->b()Lilw;

    move-result-object v1

    iput-object v5, v1, Lilw;->g:Lcw;

    iput-object v5, v1, Lilw;->f:Lu;

    .line 354
    :cond_3
    new-instance v0, Linc;

    move-object v4, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Linc;-><init>(Lilw;Limb;ZLiln;Linj;Lini;Limd;)V

    return-object v0

    .line 330
    :cond_4
    iget v0, p0, Linp;->chunkLength:I

    if-lez v0, :cond_5

    .line 331
    const-string v0, "Transfer-Encoding"

    const-string v3, "chunked"

    invoke-virtual {v2, v0, v3}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    goto :goto_1

    .line 333
    :cond_5
    const/4 v1, 0x1

    goto :goto_1

    .line 343
    :cond_6
    const-string v4, "Java"

    const-string v0, "java.version"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 540
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 541
    if-eqz p2, :cond_0

    .line 542
    iget-object v0, p0, Linp;->a:Lilw;

    iget-object v0, v0, Lilw;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 544
    :cond_0
    const-string v0, ","

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 546
    :try_start_0
    invoke-static {v4}, Lima;->a(Ljava/lang/String;)Lima;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 547
    :catch_0
    move-exception v0

    .line 548
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 551
    :cond_1
    iget-object v0, p0, Linp;->a:Lilw;

    invoke-static {v1}, Limo;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Lima;->b:Lima;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "protocols doesn\'t contain http/1.1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "protocols must not contain null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {v1}, Limo;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lilw;->c:Ljava/util/List;

    .line 552
    return-void
.end method

.method private a(Z)Z
    .locals 20

    .prologue
    .line 425
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Linp;->b:Linc;

    iget-object v2, v11, Linc;->u:Limr;

    if-nez v2, :cond_28

    iget-object v2, v11, Linc;->g:Linn;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 435
    :catch_0
    move-exception v2

    .line 436
    move-object/from16 v0, p0

    iget-object v3, v0, Linp;->b:Linc;

    invoke-virtual {v3, v2}, Linc;->a(Ljava/io/IOException;)Linc;

    move-result-object v3

    .line 437
    if-eqz v3, :cond_39

    .line 438
    move-object/from16 v0, p0

    iput-object v3, v0, Linp;->b:Linc;

    .line 439
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 425
    :cond_0
    :try_start_1
    iget-object v2, v11, Linc;->k:Limb;

    invoke-virtual {v2}, Limb;->c()Limc;

    move-result-object v3

    const-string v4, "Host"

    invoke-virtual {v2, v4}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v4, "Host"

    invoke-virtual {v2}, Limb;->a()Ljava/net/URL;

    move-result-object v5

    invoke-static {v5}, Linc;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    :cond_1
    iget-object v4, v11, Linc;->c:Liln;

    if-eqz v4, :cond_2

    iget-object v4, v11, Linc;->c:Liln;

    iget-object v4, v4, Liln;->g:Lima;

    sget-object v5, Lima;->a:Lima;

    if-eq v4, v5, :cond_3

    :cond_2
    const-string v4, "Connection"

    invoke-virtual {v2, v4}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v4, "Connection"

    const-string v5, "Keep-Alive"

    invoke-virtual {v3, v4, v5}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    :cond_3
    const-string v4, "Accept-Encoding"

    invoke-virtual {v2, v4}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    iput-boolean v4, v11, Linc;->i:Z

    const-string v4, "Accept-Encoding"

    const-string v5, "gzip"

    invoke-virtual {v3, v4, v5}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    :cond_4
    iget-object v4, v11, Linc;->b:Lilw;

    iget-object v4, v4, Lilw;->e:Ljava/net/CookieHandler;

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Limc;->a()Limb;

    move-result-object v5

    iget-object v5, v5, Limb;->c:Lils;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ling;->a(Lils;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v2}, Limb;->b()Ljava/net/URI;

    move-result-object v6

    invoke-virtual {v4, v6, v5}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    invoke-static {v3, v4}, Ling;->a(Limc;Ljava/util/Map;)V

    :cond_5
    const-string v4, "User-Agent"

    invoke-virtual {v2, v4}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_6

    const-string v2, "User-Agent"

    const-string v4, "okhttp/2.0.1-SNAPSHOT"

    invoke-virtual {v3, v2, v4}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    :cond_6
    invoke-virtual {v3}, Limc;->a()Limb;

    move-result-object v12

    sget-object v2, Limh;->a:Limh;

    iget-object v3, v11, Linc;->b:Lilw;

    invoke-virtual {v2, v3}, Limh;->a(Lilw;)Lu;

    move-result-object v9

    if-eqz v9, :cond_a

    invoke-interface {v9}, Lu;->h()Limd;

    move-result-object v2

    move-object v8, v2

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v10, Lims;

    invoke-direct {v10, v2, v3, v12, v8}, Lims;-><init>(JLimb;Limd;)V

    iget-object v2, v10, Lims;->c:Limd;

    if-nez v2, :cond_b

    new-instance v2, Limr;

    iget-object v3, v10, Lims;->b:Limb;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Limr;-><init>(Limb;Limd;)V

    :goto_2
    iget-object v3, v2, Limr;->a:Limb;

    if-eqz v3, :cond_7

    iget-object v3, v10, Lims;->b:Limb;

    invoke-virtual {v3}, Limb;->d()Lill;

    move-result-object v3

    iget-boolean v3, v3, Lill;->i:Z

    if-eqz v3, :cond_7

    new-instance v2, Limr;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Limr;-><init>(Limb;Limd;)V

    :cond_7
    iput-object v2, v11, Linc;->u:Limr;

    iget-object v2, v11, Linc;->u:Limr;

    iget-object v2, v2, Limr;->a:Limb;

    iput-object v2, v11, Linc;->l:Limb;

    iget-object v2, v11, Linc;->u:Limr;

    iget-object v2, v2, Limr;->b:Limd;

    iput-object v2, v11, Linc;->m:Limd;

    if-eqz v9, :cond_8

    iget-object v2, v11, Linc;->u:Limr;

    :cond_8
    if-eqz v8, :cond_9

    iget-object v2, v11, Linc;->m:Limd;

    if-nez v2, :cond_9

    iget-object v2, v8, Limd;->g:Limf;

    invoke-static {v2}, Limo;->a(Ljava/io/Closeable;)V

    :cond_9
    iget-object v2, v11, Linc;->l:Limb;

    if-eqz v2, :cond_29

    iget-object v2, v11, Linc;->c:Liln;

    if-nez v2, :cond_27

    iget-object v13, v11, Linc;->l:Limb;

    iget-object v2, v11, Linc;->c:Liln;

    if-eqz v2, :cond_22

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :cond_a
    const/4 v2, 0x0

    move-object v8, v2

    goto :goto_1

    :cond_b
    iget-object v2, v10, Lims;->b:Limb;

    invoke-virtual {v2}, Limb;->e()Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, v10, Lims;->c:Limd;

    iget-object v2, v2, Limd;->e:Lilr;

    if-nez v2, :cond_c

    new-instance v2, Limr;

    iget-object v3, v10, Lims;->b:Limb;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Limr;-><init>(Limb;Limd;)V

    goto :goto_2

    :cond_c
    iget-object v2, v10, Lims;->c:Limd;

    iget-object v3, v10, Lims;->b:Limb;

    invoke-static {v2, v3}, Limr;->a(Limd;Limb;)Z

    move-result v2

    if-nez v2, :cond_d

    new-instance v2, Limr;

    iget-object v3, v10, Lims;->b:Limb;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Limr;-><init>(Limb;Limd;)V

    goto :goto_2

    :cond_d
    iget-object v2, v10, Lims;->b:Limb;

    invoke-virtual {v2}, Limb;->d()Lill;

    move-result-object v13

    iget-boolean v2, v13, Lill;->a:Z

    if-nez v2, :cond_e

    iget-object v2, v10, Lims;->b:Limb;

    invoke-static {v2}, Lims;->a(Limb;)Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_e
    new-instance v2, Limr;

    iget-object v3, v10, Lims;->b:Limb;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Limr;-><init>(Limb;Limd;)V

    goto/16 :goto_2

    :cond_f
    iget-object v2, v10, Lims;->d:Ljava/util/Date;

    if-eqz v2, :cond_15

    const-wide/16 v2, 0x0

    iget-wide v4, v10, Lims;->j:J

    iget-object v6, v10, Lims;->d:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    :goto_3
    iget v4, v10, Lims;->l:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_10

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v5, v10, Lims;->l:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    :cond_10
    iget-wide v4, v10, Lims;->j:J

    iget-wide v6, v10, Lims;->i:J

    sub-long/2addr v4, v6

    iget-wide v6, v10, Lims;->a:J

    iget-wide v14, v10, Lims;->j:J

    sub-long/2addr v6, v14

    add-long/2addr v2, v4

    add-long v14, v2, v6

    iget-object v2, v10, Lims;->c:Limd;

    invoke-virtual {v2}, Limd;->c()Lill;

    move-result-object v2

    iget v3, v2, Lill;->c:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_16

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v2, v2, Lill;->c:I

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    :cond_11
    :goto_4
    iget v4, v13, Lill;->c:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3b

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v5, v13, Lill;->c:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    move-wide v6, v2

    :goto_5
    const-wide/16 v2, 0x0

    iget v4, v13, Lill;->h:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3a

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v3, v13, Lill;->h:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    move-wide v4, v2

    :goto_6
    const-wide/16 v2, 0x0

    iget-object v0, v10, Lims;->c:Limd;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Limd;->c()Lill;

    move-result-object v16

    move-object/from16 v0, v16

    iget-boolean v0, v0, Lill;->f:Z

    move/from16 v17, v0

    if-nez v17, :cond_12

    iget v0, v13, Lill;->g:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_12

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v3, v13, Lill;->g:I

    int-to-long v0, v3

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    :cond_12
    move-object/from16 v0, v16

    iget-boolean v13, v0, Lill;->a:Z

    if-nez v13, :cond_1d

    add-long v16, v14, v4

    add-long/2addr v2, v6

    cmp-long v2, v16, v2

    if-gez v2, :cond_1d

    iget-object v2, v10, Lims;->c:Limd;

    invoke-virtual {v2}, Limd;->a()Lime;

    move-result-object v3

    add-long/2addr v4, v14

    cmp-long v2, v4, v6

    if-ltz v2, :cond_13

    const-string v2, "Warning"

    const-string v4, "110 HttpURLConnection \"Response is stale\""

    invoke-virtual {v3, v2, v4}, Lime;->b(Ljava/lang/String;Ljava/lang/String;)Lime;

    :cond_13
    const-wide/32 v4, 0x5265c00

    cmp-long v2, v14, v4

    if-lez v2, :cond_14

    iget-object v2, v10, Lims;->c:Limd;

    invoke-virtual {v2}, Limd;->c()Lill;

    move-result-object v2

    iget v2, v2, Lill;->c:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1c

    iget-object v2, v10, Lims;->h:Ljava/util/Date;

    if-nez v2, :cond_1c

    const/4 v2, 0x1

    :goto_7
    if-eqz v2, :cond_14

    const-string v2, "Warning"

    const-string v4, "113 HttpURLConnection \"Heuristic expiration\""

    invoke-virtual {v3, v2, v4}, Lime;->b(Ljava/lang/String;Ljava/lang/String;)Lime;

    :cond_14
    new-instance v2, Limr;

    const/4 v4, 0x0

    invoke-virtual {v3}, Lime;->a()Limd;

    move-result-object v3

    invoke-direct {v2, v4, v3}, Limr;-><init>(Limb;Limd;)V

    goto/16 :goto_2

    :cond_15
    const-wide/16 v2, 0x0

    goto/16 :goto_3

    :cond_16
    iget-object v2, v10, Lims;->h:Ljava/util/Date;

    if-eqz v2, :cond_18

    iget-object v2, v10, Lims;->d:Ljava/util/Date;

    if-eqz v2, :cond_17

    iget-object v2, v10, Lims;->d:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    :goto_8
    iget-object v4, v10, Lims;->h:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_11

    const-wide/16 v2, 0x0

    goto/16 :goto_4

    :cond_17
    iget-wide v2, v10, Lims;->j:J

    goto :goto_8

    :cond_18
    iget-object v2, v10, Lims;->f:Ljava/util/Date;

    if-eqz v2, :cond_1b

    iget-object v2, v10, Lims;->c:Limd;

    iget-object v2, v2, Limd;->a:Limb;

    invoke-virtual {v2}, Limb;->a()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1b

    iget-object v2, v10, Lims;->d:Ljava/util/Date;

    if-eqz v2, :cond_19

    iget-object v2, v10, Lims;->d:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    :goto_9
    iget-object v4, v10, Lims;->f:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1a

    const-wide/16 v4, 0xa

    div-long/2addr v2, v4

    goto/16 :goto_4

    :cond_19
    iget-wide v2, v10, Lims;->i:J

    goto :goto_9

    :cond_1a
    const-wide/16 v2, 0x0

    goto/16 :goto_4

    :cond_1b
    const-wide/16 v2, 0x0

    goto/16 :goto_4

    :cond_1c
    const/4 v2, 0x0

    goto :goto_7

    :cond_1d
    iget-object v2, v10, Lims;->b:Limb;

    invoke-virtual {v2}, Limb;->c()Limc;

    move-result-object v2

    iget-object v3, v10, Lims;->f:Ljava/util/Date;

    if-eqz v3, :cond_20

    const-string v3, "If-Modified-Since"

    iget-object v4, v10, Lims;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    :cond_1e
    :goto_a
    iget-object v3, v10, Lims;->k:Ljava/lang/String;

    if-eqz v3, :cond_1f

    const-string v3, "If-None-Match"

    iget-object v4, v10, Lims;->k:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    :cond_1f
    invoke-virtual {v2}, Limc;->a()Limb;

    move-result-object v3

    invoke-static {v3}, Lims;->a(Limb;)Z

    move-result v2

    if-eqz v2, :cond_21

    new-instance v2, Limr;

    iget-object v4, v10, Lims;->c:Limd;

    invoke-direct {v2, v3, v4}, Limr;-><init>(Limb;Limd;)V

    goto/16 :goto_2

    :cond_20
    iget-object v3, v10, Lims;->d:Ljava/util/Date;

    if-eqz v3, :cond_1e

    const-string v3, "If-Modified-Since"

    iget-object v4, v10, Lims;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    goto :goto_a

    :cond_21
    new-instance v2, Limr;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Limr;-><init>(Limb;Limd;)V

    goto/16 :goto_2

    :cond_22
    iget-object v2, v11, Linc;->d:Linj;

    if-nez v2, :cond_26

    iget-object v14, v11, Linc;->b:Lilw;

    invoke-virtual {v13}, Limb;->a()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_23

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_24

    :cond_23
    new-instance v2, Ljava/net/UnknownHostException;

    invoke-virtual {v13}, Limb;->a()Ljava/net/URL;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_24
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v13}, Limb;->e()Z

    move-result v2

    if-eqz v2, :cond_25

    iget-object v6, v14, Lilw;->i:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v7, v14, Lilw;->j:Ljavax/net/ssl/HostnameVerifier;

    :cond_25
    new-instance v2, Lilj;

    invoke-virtual {v13}, Limb;->a()Ljava/net/URL;

    move-result-object v4

    invoke-static {v4}, Limo;->a(Ljava/net/URL;)I

    move-result v4

    iget-object v5, v14, Lilw;->h:Ljavax/net/SocketFactory;

    iget-object v8, v14, Lilw;->k:Lilk;

    iget-object v9, v14, Lilw;->b:Ljava/net/Proxy;

    iget-object v10, v14, Lilw;->c:Ljava/util/List;

    invoke-direct/range {v2 .. v10}, Lilj;-><init>(Ljava/lang/String;ILjavax/net/SocketFactory;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lilk;Ljava/net/Proxy;Ljava/util/List;)V

    new-instance v3, Linj;

    invoke-virtual {v13}, Limb;->b()Ljava/net/URI;

    move-result-object v4

    invoke-direct {v3, v2, v4, v14, v13}, Linj;-><init>(Lilj;Ljava/net/URI;Lilw;Limb;)V

    iput-object v3, v11, Linc;->d:Linj;

    :cond_26
    iget-object v2, v11, Linc;->d:Linj;

    invoke-virtual {v2}, Linj;->a()Liln;

    move-result-object v3

    sget-object v4, Limh;->a:Limh;

    iget-object v5, v2, Linj;->c:Lilw;

    iget-object v2, v2, Linj;->f:Limb;

    invoke-virtual {v4, v5, v3, v11, v2}, Limh;->a(Lilw;Liln;Linc;Limb;)V

    iput-object v3, v11, Linc;->c:Liln;

    iget-object v2, v11, Linc;->c:Liln;

    iget-object v2, v2, Liln;->b:Limg;

    iput-object v2, v11, Linc;->e:Limg;

    :cond_27
    sget-object v2, Limh;->a:Limh;

    iget-object v3, v11, Linc;->c:Liln;

    invoke-virtual {v2, v3, v11}, Limh;->a(Liln;Linc;)Linn;

    move-result-object v2

    iput-object v2, v11, Linc;->g:Linn;

    invoke-virtual {v11}, Linc;->b()Z

    move-result v2

    if-eqz v2, :cond_28

    iget-object v2, v11, Linc;->p:Liqk;

    if-nez v2, :cond_28

    iget-object v2, v11, Linc;->g:Linn;

    invoke-interface {v2, v12}, Linn;->a(Limb;)Liqk;

    move-result-object v2

    iput-object v2, v11, Linc;->p:Liqk;

    .line 426
    :cond_28
    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, Linp;->b:Linc;

    iget-object v2, v2, Linc;->e:Limg;

    move-object/from16 v0, p0

    iput-object v2, v0, Linp;->i:Limg;

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Linp;->b:Linc;

    iget-object v2, v2, Linc;->c:Liln;

    if-eqz v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v2, v0, Linp;->b:Linc;

    iget-object v2, v2, Linc;->c:Liln;

    iget-object v2, v2, Liln;->i:Lilr;

    :goto_c
    move-object/from16 v0, p0

    iput-object v2, v0, Linp;->c:Lilr;

    .line 430
    if-eqz p1, :cond_32

    .line 431
    move-object/from16 v0, p0

    iget-object v3, v0, Linp;->b:Linc;

    iget-object v2, v3, Linc;->o:Limd;

    if-nez v2, :cond_32

    iget-object v2, v3, Linc;->l:Limb;

    if-nez v2, :cond_2d

    iget-object v2, v3, Linc;->m:Limd;

    if-nez v2, :cond_2d

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "call sendRequest() first!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 425
    :cond_29
    iget-object v2, v11, Linc;->c:Liln;

    if-eqz v2, :cond_2a

    sget-object v2, Limh;->a:Limh;

    iget-object v3, v11, Linc;->b:Lilw;

    iget-object v3, v3, Lilw;->l:Lilo;

    iget-object v4, v11, Linc;->c:Liln;

    invoke-virtual {v2, v3, v4}, Limh;->a(Lilo;Liln;)V

    const/4 v2, 0x0

    iput-object v2, v11, Linc;->c:Liln;

    :cond_2a
    iget-object v2, v11, Linc;->m:Limd;

    if-eqz v2, :cond_2b

    iget-object v2, v11, Linc;->m:Limd;

    invoke-virtual {v2}, Limd;->a()Lime;

    move-result-object v2

    iget-object v3, v11, Linc;->k:Limb;

    iput-object v3, v2, Lime;->a:Limb;

    iget-object v3, v11, Linc;->f:Limd;

    invoke-static {v3}, Linc;->a(Limd;)Limd;

    move-result-object v3

    invoke-virtual {v2, v3}, Lime;->c(Limd;)Lime;

    move-result-object v2

    iget-object v3, v11, Linc;->m:Limd;

    invoke-static {v3}, Linc;->a(Limd;)Limd;

    move-result-object v3

    invoke-virtual {v2, v3}, Lime;->b(Limd;)Lime;

    move-result-object v2

    invoke-virtual {v2}, Lime;->a()Limd;

    move-result-object v2

    iput-object v2, v11, Linc;->o:Limd;

    :goto_d
    iget-object v2, v11, Linc;->o:Limd;

    iget-object v2, v2, Limd;->g:Limf;

    if-eqz v2, :cond_28

    iget-object v2, v11, Linc;->o:Limd;

    iget-object v2, v2, Limd;->g:Limf;

    invoke-virtual {v2}, Limf;->a()Lipu;

    move-result-object v2

    invoke-virtual {v11, v2}, Linc;->a(Liql;)V

    goto/16 :goto_b

    :cond_2b
    new-instance v2, Lime;

    invoke-direct {v2}, Lime;-><init>()V

    iget-object v3, v11, Linc;->k:Limb;

    iput-object v3, v2, Lime;->a:Limb;

    iget-object v3, v11, Linc;->f:Limd;

    invoke-static {v3}, Linc;->a(Limd;)Limd;

    move-result-object v3

    invoke-virtual {v2, v3}, Lime;->c(Limd;)Lime;

    move-result-object v2

    sget-object v3, Lima;->b:Lima;

    iput-object v3, v2, Lime;->b:Lima;

    const/16 v3, 0x1f8

    iput v3, v2, Lime;->c:I

    const-string v3, "Unsatisfiable Request (only-if-cached)"

    iput-object v3, v2, Lime;->d:Ljava/lang/String;

    sget-object v3, Linc;->a:Limf;

    iput-object v3, v2, Lime;->g:Limf;

    invoke-virtual {v2}, Lime;->a()Limd;

    move-result-object v2

    iput-object v2, v11, Linc;->o:Limd;

    goto :goto_d

    .line 427
    :cond_2c
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 431
    :cond_2d
    iget-object v2, v3, Linc;->l:Limb;

    if-eqz v2, :cond_32

    iget-object v2, v3, Linc;->q:Lipt;

    if-eqz v2, :cond_2e

    iget-object v2, v3, Linc;->q:Lipt;

    invoke-interface {v2}, Lipt;->c()Lipq;

    move-result-object v2

    iget-wide v4, v2, Lipq;->b:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_2e

    iget-object v2, v3, Linc;->q:Lipt;

    invoke-interface {v2}, Lipt;->b()V

    :cond_2e
    iget-wide v4, v3, Linc;->h:J

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_30

    iget-object v2, v3, Linc;->l:Limb;

    invoke-static {v2}, Ling;->a(Limb;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_2f

    iget-object v2, v3, Linc;->p:Liqk;

    instance-of v2, v2, Lini;

    if-eqz v2, :cond_2f

    iget-object v2, v3, Linc;->p:Liqk;

    check-cast v2, Lini;

    iget-object v2, v2, Lini;->a:Lipq;

    iget-wide v4, v2, Lipq;->b:J

    iget-object v2, v3, Linc;->l:Limb;

    invoke-virtual {v2}, Limb;->c()Limc;

    move-result-object v2

    const-string v6, "Content-Length"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v6, v4}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    move-result-object v2

    invoke-virtual {v2}, Limc;->a()Limb;

    move-result-object v2

    iput-object v2, v3, Linc;->l:Limb;

    :cond_2f
    iget-object v2, v3, Linc;->g:Linn;

    iget-object v4, v3, Linc;->l:Limb;

    invoke-interface {v2, v4}, Linn;->b(Limb;)V

    :cond_30
    iget-object v2, v3, Linc;->p:Liqk;

    if-eqz v2, :cond_31

    iget-object v2, v3, Linc;->q:Lipt;

    if-eqz v2, :cond_33

    iget-object v2, v3, Linc;->q:Lipt;

    invoke-interface {v2}, Lipt;->close()V

    :goto_e
    iget-object v2, v3, Linc;->p:Liqk;

    instance-of v2, v2, Lini;

    if-eqz v2, :cond_31

    invoke-static {}, Limo;->a()Lini;

    move-result-object v2

    iget-object v4, v3, Linc;->p:Liqk;

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_31

    iget-object v4, v3, Linc;->g:Linn;

    iget-object v2, v3, Linc;->p:Liqk;

    check-cast v2, Lini;

    invoke-interface {v4, v2}, Linn;->a(Lini;)V

    :cond_31
    iget-object v2, v3, Linc;->g:Linn;

    invoke-interface {v2}, Linn;->a()V

    iget-object v2, v3, Linc;->g:Linn;

    invoke-interface {v2}, Linn;->b()Lime;

    move-result-object v2

    iget-object v4, v3, Linc;->l:Limb;

    iput-object v4, v2, Lime;->a:Limb;

    iget-object v4, v3, Linc;->c:Liln;

    iget-object v4, v4, Liln;->i:Lilr;

    iput-object v4, v2, Lime;->e:Lilr;

    sget-object v4, Ling;->a:Ljava/lang/String;

    iget-wide v6, v3, Linc;->h:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lime;->a(Ljava/lang/String;Ljava/lang/String;)Lime;

    move-result-object v2

    sget-object v4, Ling;->b:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lime;->a(Ljava/lang/String;Ljava/lang/String;)Lime;

    move-result-object v2

    invoke-virtual {v2}, Lime;->a()Limd;

    move-result-object v2

    iput-object v2, v3, Linc;->n:Limd;

    sget-object v2, Limh;->a:Limh;

    iget-object v4, v3, Linc;->c:Liln;

    iget-object v5, v3, Linc;->n:Limd;

    iget-object v5, v5, Limd;->b:Lima;

    invoke-virtual {v2, v4, v5}, Limh;->a(Liln;Lima;)V

    iget-object v2, v3, Linc;->n:Limd;

    iget-object v2, v2, Limd;->f:Lils;

    invoke-virtual {v3, v2}, Linc;->a(Lils;)V

    iget-object v2, v3, Linc;->m:Limd;

    if-eqz v2, :cond_37

    iget-object v2, v3, Linc;->m:Limd;

    iget-object v4, v3, Linc;->n:Limd;

    iget v5, v4, Limd;->c:I

    const/16 v6, 0x130

    if-ne v5, v6, :cond_34

    const/4 v2, 0x1

    :goto_f
    if-eqz v2, :cond_36

    iget-object v2, v3, Linc;->m:Limd;

    invoke-virtual {v2}, Limd;->a()Lime;

    move-result-object v2

    iget-object v4, v3, Linc;->k:Limb;

    iput-object v4, v2, Lime;->a:Limb;

    iget-object v4, v3, Linc;->f:Limd;

    invoke-static {v4}, Linc;->a(Limd;)Limd;

    move-result-object v4

    invoke-virtual {v2, v4}, Lime;->c(Limd;)Lime;

    move-result-object v2

    iget-object v4, v3, Linc;->m:Limd;

    iget-object v4, v4, Limd;->f:Lils;

    iget-object v5, v3, Linc;->n:Limd;

    iget-object v5, v5, Limd;->f:Lils;

    invoke-static {v4, v5}, Linc;->a(Lils;Lils;)Lils;

    move-result-object v4

    invoke-virtual {v2, v4}, Lime;->a(Lils;)Lime;

    move-result-object v2

    iget-object v4, v3, Linc;->m:Limd;

    invoke-static {v4}, Linc;->a(Limd;)Limd;

    move-result-object v4

    invoke-virtual {v2, v4}, Lime;->b(Limd;)Lime;

    move-result-object v2

    iget-object v4, v3, Linc;->n:Limd;

    invoke-static {v4}, Linc;->a(Limd;)Limd;

    move-result-object v4

    invoke-virtual {v2, v4}, Lime;->a(Limd;)Lime;

    move-result-object v2

    invoke-virtual {v2}, Lime;->a()Limd;

    move-result-object v2

    iput-object v2, v3, Linc;->o:Limd;

    iget-object v2, v3, Linc;->g:Linn;

    invoke-interface {v2}, Linn;->e()V

    invoke-virtual {v3}, Linc;->h()V

    sget-object v2, Limh;->a:Limh;

    iget-object v4, v3, Linc;->b:Lilw;

    invoke-virtual {v2, v4}, Limh;->a(Lilw;)Lu;

    iget-object v2, v3, Linc;->m:Limd;

    iget-object v2, v3, Linc;->o:Limd;

    invoke-static {v2}, Linc;->a(Limd;)Limd;

    iget-object v2, v3, Linc;->m:Limd;

    iget-object v2, v2, Limd;->g:Limf;

    if-eqz v2, :cond_32

    iget-object v2, v3, Linc;->m:Limd;

    iget-object v2, v2, Limd;->g:Limf;

    invoke-virtual {v2}, Limf;->a()Lipu;

    move-result-object v2

    invoke-virtual {v3, v2}, Linc;->a(Liql;)V

    .line 434
    :cond_32
    :goto_10
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 431
    :cond_33
    iget-object v2, v3, Linc;->p:Liqk;

    invoke-interface {v2}, Liqk;->close()V

    goto/16 :goto_e

    :cond_34
    iget-object v2, v2, Limd;->f:Lils;

    const-string v5, "Last-Modified"

    invoke-virtual {v2, v5}, Lils;->b(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_35

    iget-object v4, v4, Limd;->f:Lils;

    const-string v5, "Last-Modified"

    invoke-virtual {v4, v5}, Lils;->b(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    if-eqz v4, :cond_35

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-gez v2, :cond_35

    const/4 v2, 0x1

    goto/16 :goto_f

    :cond_35
    const/4 v2, 0x0

    goto/16 :goto_f

    :cond_36
    iget-object v2, v3, Linc;->m:Limd;

    iget-object v2, v2, Limd;->g:Limf;

    invoke-static {v2}, Limo;->a(Ljava/io/Closeable;)V

    :cond_37
    iget-object v2, v3, Linc;->n:Limd;

    invoke-virtual {v2}, Limd;->a()Lime;

    move-result-object v2

    iget-object v4, v3, Linc;->k:Limb;

    iput-object v4, v2, Lime;->a:Limb;

    iget-object v4, v3, Linc;->f:Limd;

    invoke-static {v4}, Linc;->a(Limd;)Limd;

    move-result-object v4

    invoke-virtual {v2, v4}, Lime;->c(Limd;)Lime;

    move-result-object v2

    iget-object v4, v3, Linc;->m:Limd;

    invoke-static {v4}, Linc;->a(Limd;)Limd;

    move-result-object v4

    invoke-virtual {v2, v4}, Lime;->b(Limd;)Lime;

    move-result-object v2

    iget-object v4, v3, Linc;->n:Limd;

    invoke-static {v4}, Linc;->a(Limd;)Limd;

    move-result-object v4

    invoke-virtual {v2, v4}, Lime;->a(Limd;)Lime;

    move-result-object v2

    invoke-virtual {v2}, Lime;->a()Limd;

    move-result-object v2

    iput-object v2, v3, Linc;->o:Limd;

    invoke-virtual {v3}, Linc;->j()Z

    move-result v2

    if-nez v2, :cond_38

    iget-object v2, v3, Linc;->g:Linn;

    iget-object v4, v3, Linc;->t:Lu;

    invoke-interface {v2, v4}, Linn;->a(Lu;)Liql;

    move-result-object v2

    iput-object v2, v3, Linc;->r:Liql;

    iget-object v2, v3, Linc;->r:Liql;

    invoke-static {v2}, Liqa;->a(Liql;)Lipu;

    move-result-object v2

    iput-object v2, v3, Linc;->s:Lipu;

    goto :goto_10

    :cond_38
    invoke-virtual {v3}, Linc;->g()V

    iget-object v2, v3, Linc;->g:Linn;

    iget-object v4, v3, Linc;->t:Lu;

    invoke-interface {v2, v4}, Linn;->a(Lu;)Liql;

    move-result-object v2

    invoke-virtual {v3, v2}, Linc;->a(Liql;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_10

    .line 443
    :cond_39
    move-object/from16 v0, p0

    iput-object v2, v0, Linp;->g:Ljava/io/IOException;

    .line 444
    throw v2

    :cond_3a
    move-wide v4, v2

    goto/16 :goto_6

    :cond_3b
    move-wide v6, v2

    goto/16 :goto_5
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 289
    iget-object v1, p0, Linp;->g:Ljava/io/IOException;

    if-eqz v1, :cond_0

    .line 290
    iget-object v0, p0, Linp;->g:Ljava/io/IOException;

    throw v0

    .line 291
    :cond_0
    iget-object v1, p0, Linp;->b:Linc;

    if-eqz v1, :cond_1

    .line 312
    :goto_0
    return-void

    .line 295
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Linp;->connected:Z

    .line 297
    :try_start_0
    iget-boolean v1, p0, Linp;->doOutput:Z

    if-eqz v1, :cond_2

    .line 298
    iget-object v1, p0, Linp;->method:Ljava/lang/String;

    const-string v2, "GET"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 300
    const-string v1, "POST"

    iput-object v1, p0, Linp;->method:Ljava/lang/String;

    .line 307
    :cond_2
    iget-boolean v1, p0, Linp;->doOutput:Z

    if-eqz v1, :cond_3

    iget-wide v2, p0, Linp;->e:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    invoke-static {}, Limo;->a()Lini;

    move-result-object v0

    .line 308
    :cond_3
    iget-object v1, p0, Linp;->method:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v0, v3}, Linp;->a(Ljava/lang/String;Liln;Lini;Limd;)Linc;

    move-result-object v0

    iput-object v0, p0, Linp;->b:Linc;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 309
    :catch_0
    move-exception v0

    .line 310
    iput-object v0, p0, Linp;->g:Ljava/io/IOException;

    .line 311
    throw v0

    .line 301
    :cond_4
    :try_start_1
    iget-object v1, p0, Linp;->method:Ljava/lang/String;

    invoke-static {v1}, Line;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 303
    new-instance v0, Ljava/net/ProtocolException;

    iget-object v1, p0, Linp;->method:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " does not support writing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private c()Linc;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 369
    invoke-direct {p0}, Linp;->b()V

    .line 371
    iget-object v0, p0, Linp;->b:Linc;

    invoke-virtual {v0}, Linc;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 372
    iget-object v0, p0, Linp;->b:Linc;

    .line 385
    :goto_0
    return-object v0

    .line 408
    :cond_0
    iget-object v5, p0, Linp;->b:Linc;

    invoke-virtual {v3}, Limb;->a()Ljava/net/URL;

    move-result-object v6

    invoke-virtual {v5, v6}, Linc;->b(Ljava/net/URL;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 409
    iget-object v5, p0, Linp;->b:Linc;

    invoke-virtual {v5}, Linc;->h()V

    .line 412
    :cond_1
    iget-object v5, p0, Linp;->b:Linc;

    invoke-virtual {v5}, Linc;->i()Liln;

    move-result-object v5

    .line 413
    iget-object v3, v3, Limb;->b:Ljava/lang/String;

    check-cast v0, Lini;

    invoke-direct {p0, v3, v5, v0, v4}, Linp;->a(Ljava/lang/String;Liln;Lini;Limd;)Linc;

    move-result-object v0

    iput-object v0, p0, Linp;->b:Linc;

    .line 376
    :cond_2
    invoke-direct {p0, v2}, Linp;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    iget-object v0, p0, Linp;->b:Linc;

    invoke-virtual {v0}, Linc;->e()Limd;

    move-result-object v4

    .line 381
    iget-object v3, p0, Linp;->b:Linc;

    iget-object v0, v3, Linc;->o:Limd;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    iget-object v0, v3, Linc;->e:Limg;

    if-eqz v0, :cond_5

    iget-object v0, v3, Linc;->e:Limg;

    iget-object v0, v0, Limg;->b:Ljava/net/Proxy;

    :goto_1
    iget-object v5, v3, Linc;->o:Limd;

    iget v5, v5, Limd;->c:I

    sparse-switch v5, :sswitch_data_0

    :cond_4
    move-object v3, v1

    .line 383
    :goto_2
    if-nez v3, :cond_c

    .line 384
    iget-object v0, p0, Linp;->b:Linc;

    invoke-virtual {v0}, Linc;->h()V

    .line 385
    iget-object v0, p0, Linp;->b:Linc;

    goto :goto_0

    .line 381
    :cond_5
    iget-object v0, v3, Linc;->b:Lilw;

    iget-object v0, v0, Lilw;->b:Ljava/net/Proxy;

    goto :goto_1

    :sswitch_0
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v5

    sget-object v6, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v5, v6, :cond_6

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :sswitch_1
    iget-object v5, v3, Linc;->b:Lilw;

    iget-object v5, v5, Lilw;->k:Lilk;

    iget-object v3, v3, Linc;->o:Limd;

    invoke-static {v5, v3, v0}, Ling;->a(Lilk;Limd;Ljava/net/Proxy;)Limb;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    :sswitch_2
    iget-object v0, v3, Linc;->k:Limb;

    iget-object v0, v0, Limb;->b:Ljava/lang/String;

    const-string v5, "GET"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, v3, Linc;->k:Limb;

    iget-object v0, v0, Limb;->b:Ljava/lang/String;

    const-string v5, "HEAD"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_7
    :sswitch_3
    iget-object v0, v3, Linc;->b:Lilw;

    iget-boolean v0, v0, Lilw;->o:Z

    if-eqz v0, :cond_4

    iget-object v0, v3, Linc;->o:Limd;

    const-string v5, "Location"

    invoke-virtual {v0, v5}, Limd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v5, Ljava/net/URL;

    iget-object v6, v3, Linc;->k:Limb;

    invoke-virtual {v6}, Limb;->a()Ljava/net/URL;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v6, "https"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v5}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v6, "http"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_8
    invoke-virtual {v5}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iget-object v6, v3, Linc;->k:Limb;

    invoke-virtual {v6}, Limb;->a()Ljava/net/URL;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, v3, Linc;->b:Lilw;

    iget-boolean v0, v0, Lilw;->n:Z

    if-eqz v0, :cond_4

    :cond_9
    iget-object v0, v3, Linc;->k:Limb;

    invoke-virtual {v0}, Limb;->c()Limc;

    move-result-object v0

    iget-object v6, v3, Linc;->k:Limb;

    iget-object v6, v6, Limb;->b:Ljava/lang/String;

    invoke-static {v6}, Line;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    const-string v6, "GET"

    invoke-virtual {v0, v6, v1}, Limc;->a(Ljava/lang/String;Lt;)Limc;

    const-string v6, "Transfer-Encoding"

    invoke-virtual {v0, v6}, Limc;->a(Ljava/lang/String;)Limc;

    const-string v6, "Content-Length"

    invoke-virtual {v0, v6}, Limc;->a(Ljava/lang/String;)Limc;

    const-string v6, "Content-Type"

    invoke-virtual {v0, v6}, Limc;->a(Ljava/lang/String;)Limc;

    :cond_a
    invoke-virtual {v3, v5}, Linc;->b(Ljava/net/URL;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "Authorization"

    invoke-virtual {v0, v3}, Limc;->a(Ljava/lang/String;)Limc;

    :cond_b
    invoke-virtual {v0, v5}, Limc;->a(Ljava/net/URL;)Limc;

    move-result-object v0

    invoke-virtual {v0}, Limc;->a()Limb;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_2

    .line 388
    :cond_c
    iget v0, v4, Limd;->c:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_d

    iget v0, p0, Linp;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Linp;->f:I

    const/16 v5, 0x14

    if-le v0, v5, :cond_d

    .line 389
    new-instance v0, Ljava/net/ProtocolException;

    iget v1, p0, Linp;->f:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Too many redirects: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    move v0, v2

    .line 388
    goto :goto_3

    .line 393
    :cond_d
    invoke-virtual {v3}, Limb;->a()Ljava/net/URL;

    move-result-object v0

    iput-object v0, p0, Linp;->url:Ljava/net/URL;

    .line 394
    iget-object v0, v3, Limb;->c:Lils;

    invoke-virtual {v0}, Lils;->a()Lilt;

    move-result-object v0

    iput-object v0, p0, Linp;->d:Lilt;

    .line 399
    iget-object v0, p0, Linp;->b:Linc;

    invoke-virtual {v0}, Linc;->c()Liqk;

    move-result-object v0

    .line 400
    iget-object v5, v3, Limb;->b:Ljava/lang/String;

    iget-object v6, p0, Linp;->method:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    move-object v0, v1

    .line 404
    :cond_e
    if-eqz v0, :cond_0

    instance-of v5, v0, Lini;

    if-nez v5, :cond_0

    .line 405
    new-instance v0, Ljava/net/HttpRetryException;

    const-string v1, "Cannot retry streamed HTTP body"

    iget v2, p0, Linp;->responseCode:I

    invoke-direct {v0, v1, v2}, Ljava/net/HttpRetryException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 381
    nop

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_3
        0x12d -> :sswitch_3
        0x12e -> :sswitch_3
        0x12f -> :sswitch_3
        0x133 -> :sswitch_2
        0x191 -> :sswitch_1
        0x197 -> :sswitch_0
    .end sparse-switch

    .line 388
    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 509
    iget-boolean v0, p0, Linp;->connected:Z

    if-eqz v0, :cond_0

    .line 510
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 512
    :cond_0
    if-nez p1, :cond_1

    .line 513
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 515
    :cond_1
    if-nez p2, :cond_2

    .line 521
    invoke-static {}, Limj;->a()Limj;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Ignoring header "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " because its value was null."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Limj;->a(Ljava/lang/String;)V

    .line 531
    :goto_0
    return-void

    .line 526
    :cond_2
    const-string v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "X-Android-Protocols"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 527
    :cond_3
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Linp;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 529
    :cond_4
    iget-object v0, p0, Linp;->d:Lilt;

    invoke-virtual {v0, p1, p2}, Lilt;->a(Ljava/lang/String;Ljava/lang/String;)Lilt;

    goto :goto_0
.end method

.method public final connect()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Linp;->b()V

    .line 105
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Linp;->a(Z)Z

    move-result v0

    .line 106
    if-eqz v0, :cond_0

    .line 107
    return-void
.end method

.method public final disconnect()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Linp;->b:Linc;

    if-nez v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v0, p0, Linp;->b:Linc;

    iget-object v1, v0, Linc;->g:Linn;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, v0, Linc;->g:Linn;

    invoke-interface {v1, v0}, Linn;->a(Linc;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final getConnectTimeout()I
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Linp;->a:Lilw;

    iget v0, v0, Lilw;->p:I

    return v0
.end method

.method public final getErrorStream()Ljava/io/InputStream;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 128
    :try_start_0
    invoke-direct {p0}, Linp;->c()Linc;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Linc;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Linc;->e()Limd;

    move-result-object v2

    iget v2, v2, Limd;->c:I

    const/16 v3, 0x190

    if-lt v2, v3, :cond_0

    .line 130
    invoke-virtual {v1}, Linc;->f()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 134
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getHeaderField(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    :try_start_0
    invoke-direct {p0}, Linp;->a()Lils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lils;->b(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    if-nez p1, :cond_0

    :try_start_0
    invoke-direct {p0}, Linp;->c()Linc;

    move-result-object v0

    invoke-virtual {v0}, Linc;->e()Limd;

    move-result-object v0

    invoke-static {v0}, Linm;->a(Limd;)Linm;

    move-result-object v0

    invoke-virtual {v0}, Linm;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    :goto_0
    return-object v0

    .line 182
    :cond_0
    invoke-direct {p0}, Linp;->a()Lils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lils;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    :try_start_0
    invoke-direct {p0}, Linp;->a()Lils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lils;->a(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 194
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderFields()Ljava/util/Map;
    .locals 2

    .prologue
    .line 200
    :try_start_0
    invoke-direct {p0}, Linp;->a()Lils;

    move-result-object v0

    invoke-direct {p0}, Linp;->c()Linc;

    move-result-object v1

    invoke-virtual {v1}, Linc;->e()Limd;

    move-result-object v1

    invoke-static {v1}, Linm;->a(Limd;)Linm;

    move-result-object v1

    invoke-virtual {v1}, Linm;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ling;->a(Lils;Ljava/lang/String;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 203
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 217
    iget-boolean v0, p0, Linp;->doInput:Z

    if-nez v0, :cond_0

    .line 218
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "This protocol does not support input"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_0
    invoke-direct {p0}, Linp;->c()Linc;

    move-result-object v0

    .line 227
    invoke-virtual {p0}, Linp;->getResponseCode()I

    move-result v1

    const/16 v2, 0x190

    if-lt v1, v2, :cond_1

    .line 228
    new-instance v0, Ljava/io/FileNotFoundException;

    iget-object v1, p0, Linp;->url:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_1
    invoke-virtual {v0}, Linc;->f()Ljava/io/InputStream;

    move-result-object v0

    .line 232
    if-nez v0, :cond_2

    .line 233
    new-instance v0, Ljava/net/ProtocolException;

    invoke-virtual {p0}, Linp;->getResponseCode()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No response body exists; responseCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_2
    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 239
    invoke-virtual {p0}, Linp;->connect()V

    .line 241
    iget-object v1, p0, Linp;->b:Linc;

    iget-object v0, v1, Linc;->q:Lipt;

    if-eqz v0, :cond_0

    .line 242
    :goto_0
    if-nez v0, :cond_3

    .line 243
    new-instance v1, Ljava/net/ProtocolException;

    const-string v2, "method does not support a request body: "

    iget-object v0, p0, Linp;->method:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 241
    :cond_0
    invoke-virtual {v1}, Linc;->c()Liqk;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Liqa;->a(Liqk;)Lipt;

    move-result-object v0

    iput-object v0, v1, Linc;->q:Lipt;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 243
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 244
    :cond_3
    iget-object v1, p0, Linp;->b:Linc;

    invoke-virtual {v1}, Linc;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 245
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "cannot write request body after response has been read"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_4
    invoke-interface {v0}, Lipt;->d()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public final getPermission()Ljava/security/Permission;
    .locals 5

    .prologue
    .line 252
    invoke-virtual {p0}, Linp;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 253
    invoke-virtual {p0}, Linp;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Limo;->a(Ljava/net/URL;)I

    move-result v0

    .line 254
    invoke-virtual {p0}, Linp;->usingProxy()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255
    iget-object v0, p0, Linp;->a:Lilw;

    iget-object v0, v0, Lilw;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    .line 256
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    .line 257
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    .line 259
    :cond_0
    new-instance v2, Ljava/net/SocketPermission;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xc

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "connect, resolve"

    invoke-direct {v2, v0, v1}, Ljava/net/SocketPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public final getReadTimeout()I
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Linp;->a:Lilw;

    iget v0, v0, Lilw;->q:I

    return v0
.end method

.method public final getRequestProperties()Ljava/util/Map;
    .locals 2

    .prologue
    .line 208
    iget-boolean v0, p0, Linp;->connected:Z

    if-eqz v0, :cond_0

    .line 209
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot access request header fields after connection is set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    iget-object v0, p0, Linp;->d:Lilt;

    invoke-virtual {v0}, Lilt;->a()Lils;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ling;->a(Lils;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 263
    if-nez p1, :cond_0

    move-object v0, v1

    .line 264
    :goto_0
    return-object v0

    :cond_0
    iget-object v3, p0, Linp;->d:Lilt;

    iget-object v0, v3, Lilt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    move v2, v0

    :goto_1
    if-ltz v2, :cond_2

    iget-object v0, v3, Lilt;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v3, Lilt;->a:Ljava/util/List;

    add-int/lit8 v1, v2, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v2, -0x2

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final getResponseCode()I
    .locals 1

    .prologue
    .line 471
    invoke-direct {p0}, Linp;->c()Linc;

    move-result-object v0

    invoke-virtual {v0}, Linc;->e()Limd;

    move-result-object v0

    iget v0, v0, Limd;->c:I

    return v0
.end method

.method public final getResponseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 467
    invoke-direct {p0}, Linp;->c()Linc;

    move-result-object v0

    invoke-virtual {v0}, Linc;->e()Limd;

    move-result-object v0

    iget-object v0, v0, Limd;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final setConnectTimeout(I)V
    .locals 4

    .prologue
    .line 268
    iget-object v0, p0, Linp;->a:Lilw;

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lilw;->a(JLjava/util/concurrent/TimeUnit;)V

    .line 269
    return-void
.end method

.method public final setFixedLengthStreamingMode(I)V
    .locals 2

    .prologue
    .line 563
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Linp;->setFixedLengthStreamingMode(J)V

    .line 564
    return-void
.end method

.method public final setFixedLengthStreamingMode(J)V
    .locals 3

    .prologue
    .line 567
    iget-boolean v0, p0, Ljava/net/HttpURLConnection;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 568
    :cond_0
    iget v0, p0, Linp;->chunkLength:I

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already in chunked mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 569
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "contentLength < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 570
    :cond_2
    iput-wide p1, p0, Linp;->e:J

    .line 571
    const-wide/32 v0, 0x7fffffff

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Ljava/net/HttpURLConnection;->fixedContentLength:I

    .line 572
    return-void
.end method

.method public final setIfModifiedSince(J)V
    .locals 7

    .prologue
    .line 500
    invoke-super {p0, p1, p2}, Ljava/net/HttpURLConnection;->setIfModifiedSince(J)V

    .line 501
    iget-wide v0, p0, Linp;->ifModifiedSince:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Linp;->d:Lilt;

    const-string v1, "If-Modified-Since"

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Linp;->ifModifiedSince:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lina;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lilt;->c(Ljava/lang/String;Ljava/lang/String;)Lilt;

    .line 506
    :goto_0
    return-void

    .line 504
    :cond_0
    iget-object v0, p0, Linp;->d:Lilt;

    const-string v1, "If-Modified-Since"

    invoke-virtual {v0, v1}, Lilt;->a(Ljava/lang/String;)Lilt;

    goto :goto_0
.end method

.method public final setInstanceFollowRedirects(Z)V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Linp;->a:Lilw;

    iput-boolean p1, v0, Lilw;->o:Z

    .line 274
    return-void
.end method

.method public final setReadTimeout(I)V
    .locals 4

    .prologue
    .line 281
    iget-object v0, p0, Linp;->a:Lilw;

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lilw;->b(JLjava/util/concurrent/TimeUnit;)V

    .line 282
    return-void
.end method

.method public final setRequestMethod(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 555
    sget-object v0, Line;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    new-instance v0, Ljava/net/ProtocolException;

    sget-object v1, Line;->a:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Expected one of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but was "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 559
    :cond_0
    iput-object p1, p0, Linp;->method:Ljava/lang/String;

    .line 560
    return-void
.end method

.method public final setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 475
    iget-boolean v0, p0, Linp;->connected:Z

    if-eqz v0, :cond_0

    .line 476
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 478
    :cond_0
    if-nez p1, :cond_1

    .line 479
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 481
    :cond_1
    if-nez p2, :cond_2

    .line 487
    invoke-static {}, Limj;->a()Limj;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Ignoring header "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " because its value was null."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Limj;->a(Ljava/lang/String;)V

    .line 497
    :goto_0
    return-void

    .line 492
    :cond_2
    const-string v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "X-Android-Protocols"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 493
    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Linp;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 495
    :cond_4
    iget-object v0, p0, Linp;->d:Lilt;

    invoke-virtual {v0, p1, p2}, Lilt;->c(Ljava/lang/String;Ljava/lang/String;)Lilt;

    goto :goto_0
.end method

.method public final usingProxy()Z
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Linp;->i:Limg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Linp;->i:Limg;

    iget-object v0, v0, Limg;->b:Ljava/net/Proxy;

    .line 463
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 460
    :cond_0
    iget-object v0, p0, Linp;->a:Lilw;

    iget-object v0, v0, Lilw;->b:Ljava/net/Proxy;

    goto :goto_0

    .line 463
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
