.class public final Lbuf;
.super Lbzt;
.source "SourceFile"


# instance fields
.field private W:Leyp;

.field private X:Lfhz;

.field private Y:Landroid/view/View;

.field private Z:Landroid/widget/TextView;

.field private aa:Landroid/widget/TextView;

.field private ab:Landroid/widget/TextView;

.field private ac:Landroid/widget/TextView;

.field private ad:Lfvi;

.field private ae:Leyo;

.field private af:Lgbu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lbzt;-><init>()V

    return-void
.end method

.method static synthetic a(Lbuf;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lbuf;->Y:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lbuf;)Lfhz;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lbuf;->X:Lfhz;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 73
    const v0, 0x7f0400b4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 74
    const v0, 0x7f08012c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbuf;->Y:Landroid/view/View;

    .line 75
    const v0, 0x7f0800a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 76
    new-instance v2, Lfvi;

    iget-object v3, p0, Lbuf;->W:Leyp;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v0, v4}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;Z)V

    iput-object v2, p0, Lbuf;->ad:Lfvi;

    .line 77
    const v0, 0x7f08008b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbuf;->Z:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f080275

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbuf;->aa:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f080276

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbuf;->ab:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0800c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbuf;->ac:Landroid/widget/TextView;

    .line 82
    new-instance v0, Lbug;

    invoke-direct {v0, p0}, Lbug;-><init>(Lbuf;)V

    iput-object v0, p0, Lbuf;->ae:Leyo;

    .line 89
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Lbzt;->a(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lbuf;->j()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lari;->c()Leyp;

    move-result-object v0

    iput-object v0, p0, Lbuf;->W:Leyp;

    .line 65
    invoke-virtual {p0}, Lbuf;->j()Lo;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    iput-object v0, p0, Lbuf;->X:Lfhz;

    .line 66
    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1}, Lbzt;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 96
    return-object v0
.end method

.method public final e()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 101
    invoke-super {p0}, Lbzt;->e()V

    .line 105
    invoke-virtual {p0}, Lbuf;->h()Landroid/os/Bundle;

    move-result-object v0

    .line 106
    if-nez v0, :cond_1

    .line 107
    invoke-virtual {p0}, Lbuf;->a()V

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    const-string v1, "gdata_playlist"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 109
    invoke-virtual {p0}, Lbuf;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "gdata_playlist"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    iput-object v0, p0, Lbuf;->af:Lgbu;
    :try_end_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    iget-object v0, p0, Lbuf;->Z:Landroid/widget/TextView;

    iget-object v1, p0, Lbuf;->af:Lgbu;

    iget-object v1, v1, Lgbu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbuf;->aa:Landroid/widget/TextView;

    iget-object v1, p0, Lbuf;->af:Lgbu;

    iget-object v1, v1, Lgbu;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbuf;->ab:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbuf;->k()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f100000

    iget-object v3, p0, Lbuf;->af:Lgbu;

    iget v3, v3, Lgbu;->j:I

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lbuf;->af:Lgbu;

    iget v5, v5, Lgbu;->j:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbuf;->af:Lgbu;

    iget-object v0, v0, Lgbu;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbuf;->ac:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lbuf;->af:Lgbu;

    iget-object v0, v0, Lgbu;->i:Landroid/net/Uri;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbuf;->af:Lgbu;

    iget-object v0, v0, Lgbu;->i:Landroid/net/Uri;

    :goto_2
    if-eqz v0, :cond_0

    iget-object v1, p0, Lbuf;->ad:Lfvi;

    iget-object v2, p0, Lbuf;->ae:Leyo;

    invoke-virtual {v1, v0, v2}, Lfvi;->a(Landroid/net/Uri;Leyo;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lbuf;->a()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {p0}, Lbuf;->a()V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {p0}, Lbuf;->a()V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lbuf;->ac:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lbuf;->ac:Landroid/widget/TextView;

    iget-object v1, p0, Lbuf;->af:Lgbu;

    iget-object v1, v1, Lgbu;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lbuf;->af:Lgbu;

    iget-object v0, v0, Lgbu;->h:Landroid/net/Uri;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbuf;->af:Lgbu;

    iget-object v0, v0, Lgbu;->h:Landroid/net/Uri;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lbuf;->af:Lgbu;

    iget-object v0, v0, Lgbu;->g:Landroid/net/Uri;

    goto :goto_2

    .line 110
    :cond_5
    const-string v1, "innertube_playlist_header"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 111
    invoke-virtual {p0}, Lbuf;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "innertube_playlist_header"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    :try_start_1
    new-instance v1, Lhsb;

    invoke-direct {v1}, Lhsb;-><init>()V

    invoke-static {v1, v0}, Lidh;->a(Lidh;[B)Lidh;

    new-instance v0, Lflv;

    invoke-direct {v0, v1}, Lflv;-><init>(Lhsb;)V
    :try_end_1
    .catch Lidg; {:try_start_1 .. :try_end_1} :catch_3

    iget-object v1, p0, Lbuf;->Z:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflv;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lbuf;->aa:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflv;->h()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lbuf;->ab:Landroid/widget/TextView;

    iget-object v2, v0, Lflv;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_9

    invoke-virtual {v0}, Lflv;->f()Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, Lflv;->b:Ljava/lang/CharSequence;

    iget-object v2, v0, Lflv;->b:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, v0, Lflv;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_6

    iget-object v2, v0, Lflv;->a:Lhsb;

    iget-object v2, v2, Lhsb;->h:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v0, Lflv;->c:Ljava/lang/CharSequence;

    :cond_6
    iget-object v2, v0, Lflv;->c:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    new-array v3, v8, [Ljava/lang/CharSequence;

    iget-object v4, v0, Lflv;->b:Ljava/lang/CharSequence;

    aput-object v4, v3, v6

    const-string v4, " \u00b7 "

    aput-object v4, v3, v5

    aput-object v2, v3, v7

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, Lflv;->b:Ljava/lang/CharSequence;

    :cond_7
    iget-object v2, v0, Lflv;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_8

    iget-object v2, v0, Lflv;->a:Lhsb;

    iget-object v2, v2, Lhsb;->i:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v0, Lflv;->d:Ljava/lang/CharSequence;

    :cond_8
    iget-object v2, v0, Lflv;->d:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    new-array v3, v8, [Ljava/lang/CharSequence;

    iget-object v4, v0, Lflv;->b:Ljava/lang/CharSequence;

    aput-object v4, v3, v6

    const-string v4, " \u00b7 "

    aput-object v4, v3, v5

    aput-object v2, v3, v7

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, Lflv;->b:Ljava/lang/CharSequence;

    :cond_9
    iget-object v2, v0, Lflv;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lflv;->g()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lbuf;->ac:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    iget-object v1, v0, Lflv;->a:Lhsb;

    iget-object v1, v1, Lhsb;->m:Lhog;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lbuf;->aa:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbuf;->k()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lbuf;->aa:Landroid/widget/TextView;

    new-instance v2, Lbuh;

    invoke-direct {v2, p0, v0}, Lbuh;-><init>(Lbuf;Lflv;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_a
    iget-object v0, v0, Lflv;->f:Lflz;

    invoke-virtual {v0}, Lflz;->a()Lfnc;

    move-result-object v0

    iget-object v1, p0, Lbuf;->ad:Lfvi;

    iget-object v2, p0, Lbuf;->ae:Leyo;

    invoke-virtual {v1, v0, v2}, Lfvi;->a(Lfnc;Leyo;)V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {p0}, Lbuf;->a()V

    goto/16 :goto_0

    :cond_b
    iget-object v1, p0, Lbuf;->ac:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lbuf;->ac:Landroid/widget/TextView;

    invoke-virtual {v0}, Lflv;->g()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 113
    :cond_c
    invoke-virtual {p0}, Lbuf;->a()V

    goto/16 :goto_0
.end method
