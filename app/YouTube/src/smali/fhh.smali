.class public final Lfhh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/util/SparseIntArray;

.field private static final b:Landroid/util/SparseIntArray;

.field private static final c:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lfhi;

    invoke-direct {v0}, Lfhi;-><init>()V

    sput-object v0, Lfhh;->a:Landroid/util/SparseIntArray;

    .line 30
    new-instance v0, Lfhj;

    invoke-direct {v0}, Lfhj;-><init>()V

    sput-object v0, Lfhh;->b:Landroid/util/SparseIntArray;

    .line 71
    new-instance v0, Lfhk;

    invoke-direct {v0}, Lfhk;-><init>()V

    sput-object v0, Lfhh;->c:Landroid/util/SparseIntArray;

    return-void
.end method

.method public static a(Lhjc;)Lfpi;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 87
    if-nez p0, :cond_0

    .line 88
    const/4 v0, 0x0

    .line 92
    :goto_0
    return-object v0

    .line 91
    :cond_0
    new-instance v2, Lfpk;

    sget-object v1, Lfhh;->a:Landroid/util/SparseIntArray;

    iget v3, p0, Lhjc;->b:I

    invoke-virtual {v1, v3, v0}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    invoke-direct {v2, v1}, Lfpk;-><init>(I)V

    iget-object v1, p0, Lhjc;->c:[Lhje;

    if-eqz v1, :cond_2

    iget-object v3, p0, Lhjc;->c:[Lhje;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    invoke-static {v5}, Lfhh;->a(Lhje;)Lfpm;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v2, v5}, Lfpk;->a(Lfpm;)Lfpk;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lhjc;->d:[Lhjs;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lhjc;->d:[Lhjs;

    array-length v3, v1

    :goto_2
    if-ge v0, v3, :cond_4

    aget-object v4, v1, v0

    invoke-static {v4}, Lfhh;->a(Lhjs;)Lfpu;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v2, v4}, Lfpk;->a(Lfpu;)Lfpk;

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhjc;->e:Lhjg;

    invoke-static {v0}, Lfhh;->a(Lhjg;)Lfpq;

    move-result-object v0

    if-eqz v0, :cond_5

    iput-object v0, v2, Lfpk;->a:Lfpq;

    .line 92
    :cond_5
    invoke-virtual {v2}, Lfpk;->a()Lfpi;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lhje;)Lfpm;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 145
    if-nez p0, :cond_0

    .line 170
    :goto_0
    return-object v0

    .line 150
    :cond_0
    :try_start_0
    iget-object v2, p0, Lhje;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 151
    iget-object v2, p0, Lhje;->c:Ljava/lang/String;

    invoke-static {v2}, La;->E(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 157
    :cond_1
    :goto_1
    new-instance v2, Lfpo;

    sget-object v3, Lfhh;->b:Landroid/util/SparseIntArray;

    iget v4, p0, Lhje;->b:I

    .line 158
    invoke-virtual {v3, v4, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    iget-object v4, p0, Lhje;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v0, v4}, Lfpo;-><init>(ILandroid/net/Uri;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lhje;->e:[Lhjs;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhje;->e:[Lhjs;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 163
    iget-object v3, p0, Lhje;->e:[Lhjs;

    array-length v4, v3

    move v0, v1

    :goto_2
    if-ge v0, v4, :cond_3

    aget-object v1, v3, v0

    .line 164
    invoke-static {v1}, Lfhh;->a(Lhjs;)Lfpu;

    move-result-object v1

    .line 165
    if-eqz v1, :cond_2

    .line 166
    iget-object v1, v1, Lfpu;->b:Landroid/net/Uri;

    invoke-virtual {v2, v1}, Lfpo;->a(Landroid/net/Uri;)Lfpo;

    .line 163
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 154
    :catch_0
    move-exception v2

    const-string v2, "Badly formed InfoCardAction link URL - ignoring"

    invoke-static {v2}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 170
    :cond_3
    invoke-virtual {v2}, Lfpo;->a()Lfpm;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lhjg;)Lfpq;
    .locals 4

    .prologue
    .line 174
    if-nez p0, :cond_0

    .line 175
    const/4 v0, 0x0

    .line 204
    :goto_0
    return-object v0

    .line 178
    :cond_0
    new-instance v1, Lfps;

    iget-object v0, p0, Lhjg;->b:Ljava/lang/String;

    iget-object v2, p0, Lhjg;->d:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lfps;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget v0, p0, Lhjg;->f:I

    iput v0, v1, Lfps;->a:I

    .line 181
    const/4 v0, 0x0

    const/high16 v2, 0x40a00000    # 5.0f

    iget v3, p0, Lhjg;->g:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {v1, v0}, Lfps;->a(F)Lfps;

    .line 182
    iget-object v0, p0, Lhjg;->a:Ljava/lang/String;

    iput-object v0, v1, Lfps;->d:Ljava/lang/String;

    .line 183
    iget-object v0, p0, Lhjg;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 185
    :try_start_0
    iget-object v0, p0, Lhjg;->e:Ljava/lang/String;

    invoke-static {v0}, La;->E(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v1, Lfps;->c:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :cond_1
    :goto_1
    iget-object v0, p0, Lhjg;->c:Lhjm;

    if-eqz v0, :cond_3

    .line 192
    iget-object v0, p0, Lhjg;->c:Lhjm;

    iget-object v0, v0, Lhjm;->b:Ljava/lang/String;

    .line 193
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 194
    iget-object v0, p0, Lhjg;->c:Lhjm;

    iget-object v0, v0, Lhjm;->c:Ljava/lang/String;

    .line 196
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 198
    :try_start_1
    invoke-static {v0}, La;->E(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v1, Lfps;->b:Landroid/net/Uri;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    .line 204
    :cond_3
    :goto_2
    invoke-virtual {v1}, Lfps;->a()Lfpq;

    move-result-object v0

    goto :goto_0

    .line 187
    :catch_0
    move-exception v0

    const-string v0, "Badly formed rating image uri - ignoring"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 200
    :catch_1
    move-exception v0

    const-string v0, "Badly formed app icon - ignoring"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static a(Lhjs;)Lfpu;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 127
    if-nez p0, :cond_0

    .line 140
    :goto_0
    return-object v1

    .line 132
    :cond_0
    :try_start_0
    iget-object v0, p0, Lhjs;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhjs;->c:Ljava/lang/String;

    invoke-static {v0}, La;->E(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v0

    .line 133
    :goto_1
    new-instance v0, Lfpu;

    sget-object v3, Lfhh;->c:Landroid/util/SparseIntArray;

    iget v4, p0, Lhjs;->b:I

    const/4 v5, 0x0

    .line 134
    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    invoke-direct {v0, v3, v2}, Lfpu;-><init>(ILandroid/net/Uri;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move-object v1, v0

    .line 140
    goto :goto_0

    :cond_1
    move-object v2, v1

    .line 132
    goto :goto_1

    .line 137
    :catch_0
    move-exception v0

    const-string v0, "Badly formed InfoCardTrackingEvent base URL - ignoring"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_2
.end method
