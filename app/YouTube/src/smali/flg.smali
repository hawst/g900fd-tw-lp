.class public final Lflg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lhpl;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I

.field public final f:Ljava/lang/String;

.field private g:Lfki;


# direct methods
.method public constructor <init>(Lhpl;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lflg;->a:Lhpl;

    .line 31
    iget-object v0, p1, Lhpl;->a:Ljava/lang/String;

    iput-object v0, p0, Lflg;->c:Ljava/lang/String;

    .line 35
    iget v0, p1, Lhpl;->d:I

    if-eqz v0, :cond_1

    .line 36
    iget v0, p1, Lhpl;->d:I

    iput v0, p0, Lflg;->b:I

    .line 47
    :goto_0
    iget v0, p0, Lflg;->b:I

    if-ne v0, v3, :cond_3

    .line 48
    iput v1, p0, Lflg;->d:I

    .line 49
    iput v1, p0, Lflg;->e:I

    .line 58
    :goto_1
    iget v0, p0, Lflg;->b:I

    if-ne v0, v2, :cond_4

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lflg;->f:Ljava/lang/String;

    .line 74
    :cond_0
    return-void

    .line 39
    :cond_1
    iget-object v0, p0, Lflg;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 40
    iput v2, p0, Lflg;->b:I

    goto :goto_0

    .line 42
    :cond_2
    iput v3, p0, Lflg;->b:I

    goto :goto_0

    .line 53
    :cond_3
    iget v0, p1, Lhpl;->b:I

    iput v0, p0, Lflg;->d:I

    .line 54
    iget v0, p1, Lhpl;->c:I

    iput v0, p0, Lflg;->e:I

    goto :goto_1

    .line 60
    :cond_4
    iget-object v0, p1, Lhpl;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 61
    iget-object v0, p1, Lhpl;->e:Ljava/lang/String;

    iput-object v0, p0, Lflg;->f:Ljava/lang/String;

    .line 64
    iget-object v0, p1, Lhpl;->f:Lhpf;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "OfflineState.offline_refresh_message cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_5
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "OfflineState.short_message cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 94
    iget v1, p0, Lflg;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lfki;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 147
    iget-object v0, p0, Lflg;->g:Lfki;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflg;->a:Lhpl;

    iget-object v0, v0, Lhpl;->f:Lhpf;

    if-nez v0, :cond_1

    .line 148
    :cond_0
    iget-object v0, p0, Lflg;->g:Lfki;

    .line 160
    :goto_0
    return-object v0

    .line 149
    :cond_1
    iget-object v0, p0, Lflg;->a:Lhpl;

    iget-object v0, v0, Lhpl;->f:Lhpf;

    iget-object v0, v0, Lhpf;->a:Lhod;

    if-eqz v0, :cond_3

    .line 150
    new-instance v0, Lfla;

    iget-object v1, p0, Lflg;->a:Lhpl;

    iget-object v1, v1, Lhpl;->f:Lhpf;

    iget-object v1, v1, Lhpf;->a:Lhod;

    invoke-direct {v0, v1, v2}, Lfla;-><init>(Lhod;Lfqh;)V

    iput-object v0, p0, Lflg;->g:Lfki;

    .line 160
    :cond_2
    :goto_1
    iget-object v0, p0, Lflg;->g:Lfki;

    goto :goto_0

    .line 154
    :cond_3
    iget-object v0, p0, Lflg;->a:Lhpl;

    iget-object v0, v0, Lhpl;->f:Lhpf;

    iget-object v0, v0, Lhpf;->b:Lhfz;

    if-eqz v0, :cond_2

    .line 155
    new-instance v0, Lfjt;

    iget-object v1, p0, Lflg;->a:Lhpl;

    iget-object v1, v1, Lhpl;->f:Lhpf;

    iget-object v1, v1, Lhpf;->b:Lhfz;

    invoke-direct {v0, v1, v2}, Lfjt;-><init>(Lhfz;Lfqh;)V

    iput-object v0, p0, Lflg;->g:Lfki;

    goto :goto_1
.end method
