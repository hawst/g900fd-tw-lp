.class public final Ldvs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldxf;


# static fields
.field private static final a:Landroid/util/Pair;


# instance fields
.field private final b:Ljava/util/Map;

.field private final c:Landroid/content/res/Resources;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Ldrf;

.field private final f:Ldrl;

.field private final g:Ldrc;

.field private final h:Ldwq;

.field private final i:Ldyd;

.field private final j:Ldyg;

.field private final k:Ldrn;

.field private final l:Ldra;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    const-string v0, ""

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    sput-object v0, Ldvs;->a:Landroid/util/Pair;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Landroid/content/SharedPreferences;Landroid/content/res/Resources;Ldwq;Ldyd;Ldyg;Ldqz;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Ldvs;->d:Ljava/util/concurrent/Executor;

    .line 84
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Ldvs;->c:Landroid/content/res/Resources;

    .line 87
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Ldvs;->h:Ldwq;

    .line 88
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyd;

    iput-object v0, p0, Ldvs;->i:Ldyd;

    .line 89
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldvs;->j:Ldyg;

    .line 91
    new-instance v0, Ldrj;

    invoke-direct {v0, p7}, Ldrj;-><init>(Ldqz;)V

    .line 92
    new-instance v1, Ldrn;

    invoke-direct {v1, v0}, Ldrn;-><init>(Ldrj;)V

    iput-object v1, p0, Ldvs;->k:Ldrn;

    .line 94
    new-instance v1, Ldrf;

    invoke-direct {v1, v0}, Ldrf;-><init>(Ldrj;)V

    iput-object v1, p0, Ldvs;->e:Ldrf;

    .line 95
    new-instance v1, Ldrl;

    invoke-direct {v1, v0}, Ldrl;-><init>(Ldrj;)V

    iput-object v1, p0, Ldvs;->f:Ldrl;

    .line 96
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Ldvs;->b:Ljava/util/Map;

    .line 98
    new-instance v1, Ldra;

    new-instance v2, Ldrh;

    invoke-direct {v2, p8, v0}, Ldrh;-><init>(Ljava/lang/String;Ldrj;)V

    invoke-direct {v1, v2}, Ldra;-><init>(Ldrc;)V

    iput-object v1, p0, Ldvs;->l:Ldra;

    .line 100
    new-instance v0, Ldrd;

    new-instance v1, Ldrq;

    invoke-direct {v1, p2}, Ldrq;-><init>(Landroid/content/SharedPreferences;)V

    iget-object v2, p0, Ldvs;->l:Ldra;

    invoke-direct {v0, v1, v2}, Ldrd;-><init>(Ldrc;Ldrc;)V

    iput-object v0, p0, Ldvs;->g:Ldrc;

    .line 102
    return-void
.end method

.method static synthetic a()Landroid/util/Pair;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Ldvs;->a:Landroid/util/Pair;

    return-object v0
.end method

.method static synthetic a(Ldvs;)Ldrn;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldvs;->k:Ldrn;

    return-object v0
.end method

.method static synthetic a(Ldvs;Ljava/util/List;Ldst;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    iget-object v1, p2, Ldst;->b:Ldth;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    iget-object v3, v0, Ldst;->b:Ldth;

    invoke-virtual {v3, v1}, Ldth;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    if-eqz v0, :cond_3

    iget-object v1, v0, Ldst;->c:Ljava/lang/String;

    :cond_1
    :goto_1
    return-object v1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p2, Ldst;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v2, p2, Ldst;->c:Ljava/lang/String;

    const/4 v0, 0x2

    move-object v1, v2

    :goto_2
    invoke-static {p1, v1}, La;->a(Ljava/util/List;Ljava/lang/String;)Ldst;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    invoke-direct {p0, p1}, Ldvs;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 321
    move v0, v1

    .line 322
    :goto_0
    iget-object v2, p0, Ldvs;->c:Landroid/content/res/Resources;

    const v3, 0x7f090019

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 323
    invoke-static {p1, v2}, La;->a(Ljava/util/List;Ljava/lang/String;)Ldst;

    move-result-object v3

    .line 324
    if-nez v3, :cond_0

    .line 325
    return-object v2

    .line 321
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Ldvs;Ljava/util/List;)Ljava/util/List;
    .locals 4

    .prologue
    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    new-instance v3, Ldwd;

    invoke-direct {v3, v0}, Ldwd;-><init>(Ldst;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method static synthetic b(Ldvs;)Landroid/util/Pair;
    .locals 2

    .prologue
    .line 47
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    iget-object v0, p0, Ldvs;->i:Ldyd;

    invoke-interface {v0}, Ldyd;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SIGNED_OUT_USER"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldvs;->i:Ldyd;

    invoke-interface {v0}, Ldyd;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Ldvs;->a:Landroid/util/Pair;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ldvs;->i:Ldyd;

    invoke-interface {v0}, Ldyd;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldvs;->i:Ldyd;

    invoke-interface {v1}, Ldyd;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1
.end method

.method private b()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Ldvs;->l:Ldra;

    .line 250
    iget-object v0, p0, Ldvs;->l:Ldra;

    iget-object v0, v0, Ldra;->a:Ldrb;

    iget-object v0, v0, Ldrb;->b:Ldq;

    invoke-virtual {v0}, Ldq;->a()V

    .line 252
    return-void
.end method

.method static synthetic c(Ldvs;)Ldrc;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldvs;->g:Ldrc;

    return-object v0
.end method

.method static synthetic d(Ldvs;)Ldwq;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldvs;->h:Ldwq;

    return-object v0
.end method

.method private d(Leuc;)V
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Ldvs;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Ldvy;

    invoke-direct {v1, p0, p1}, Ldvy;-><init>(Ldvs;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 374
    return-void
.end method

.method static synthetic e(Ldvs;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldvs;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Ldvs;)Ldrf;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldvs;->e:Ldrf;

    return-object v0
.end method

.method static synthetic g(Ldvs;)Ldrl;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldvs;->f:Ldrl;

    return-object v0
.end method

.method static synthetic h(Ldvs;)Ldyg;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldvs;->j:Ldyg;

    return-object v0
.end method


# virtual methods
.method public final a(Ldtd;Leuc;)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Ldvs;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Ldvt;

    invoke-direct {v1, p0, p1, p2}, Ldvt;-><init>(Ldvs;Ldtd;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 152
    return-void
.end method

.method public final a(Ldth;Leuc;)V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Ldvs;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Ldvx;

    invoke-direct {v1, p0, p2, p1}, Ldvx;-><init>(Ldvs;Leuc;Ldth;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 295
    return-void
.end method

.method public final a(Ldth;Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Ldvs;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Ldvw;

    invoke-direct {v1, p0, p3, p1, p2}, Ldvw;-><init>(Ldvs;Leuc;Ldth;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 270
    return-void
.end method

.method public final a(Leuc;)V
    .locals 2

    .prologue
    .line 156
    new-instance v0, Ldvu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p1}, Ldvu;-><init>(Ldvs;Ljava/lang/Void;Leuc;Leuc;)V

    invoke-direct {p0, v0}, Ldvs;->d(Leuc;)V

    .line 225
    return-void
.end method

.method public final b(Leuc;)V
    .locals 2

    .prologue
    .line 229
    new-instance v0, Ldvv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p1}, Ldvv;-><init>(Ldvs;Ljava/lang/Void;Leuc;Leuc;)V

    invoke-direct {p0, v0}, Ldvs;->d(Leuc;)V

    .line 238
    return-void
.end method

.method public final c(Leuc;)V
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0}, Ldvs;->b()V

    .line 244
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldvs;->b(Leuc;)V

    .line 245
    return-void
.end method

.method public final onMdxUserAuthenticationChangedEvent(Ldyc;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 424
    invoke-direct {p0}, Ldvs;->b()V

    .line 425
    return-void
.end method
