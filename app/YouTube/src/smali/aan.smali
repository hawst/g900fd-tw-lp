.class public final Laan;
.super Laao;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Laao;-><init>()V

    .line 35
    iput-object p1, p0, Laan;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Laan;->b:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Laan;->c:Ljava/util/Map;

    .line 38
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 58
    iget-object v0, p0, Laan;->a:Landroid/content/Context;

    invoke-static {v0}, Laav;->a(Landroid/content/Context;)Laav;

    move-result-object v0

    .line 59
    iget-object v1, p0, Laan;->b:Ljava/lang/String;

    iget-object v2, v0, Laav;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, v0, Laav;->c:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Laav;->d:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :goto_0
    :try_start_1
    new-instance v2, Labf;

    invoke-direct {v2}, Labf;-><init>()V

    iget-object v1, p0, Laan;->b:Ljava/lang/String;

    .line 63
    iput-object v1, v2, Labf;->a:Ljava/lang/String;

    .line 64
    const/4 v1, 0x1

    iput-boolean v1, v2, Labf;->c:Z

    iget-object v1, p0, Laan;->c:Ljava/util/Map;

    .line 65
    iput-object v1, v2, Labf;->d:Ljava/util/Map;

    iget-object v1, p0, Laan;->b:Ljava/lang/String;

    .line 66
    iget-object v0, v0, Laav;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v2, Labf;->b:Z

    .line 68
    iget-object v1, p0, Laan;->a:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Laan;->a(Landroid/content/Context;Labf;ZZZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 77
    :goto_1
    return-void

    .line 59
    :cond_1
    :try_start_2
    iget-object v3, v0, Laav;->a:Laau;

    iget-wide v4, v0, Laav;->e:J

    invoke-virtual {v3, v1, v4, v5}, Laau;->a(Ljava/lang/String;J)V

    iget-object v3, v0, Laav;->d:Ljava/util/Map;

    iget-wide v4, v0, Laav;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    const-string v1, "GoogleConversionReporter"

    const-string v2, "Error sending ping"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
