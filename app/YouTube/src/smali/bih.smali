.class public final Lbih;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field private synthetic a:Lbif;


# direct methods
.method public constructor <init>(Lbif;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lbih;->a:Lbif;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 274
    check-cast p1, [Ljava/util/List;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v2, p0, Lbih;->a:Lbif;

    invoke-static {v2, v0}, Lbif;->a(Lbif;Landroid/net/Uri;)Lbik;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lbih;->a:Lbif;

    invoke-static {v2}, Lbif;->b(Lbif;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbih;->a:Lbif;

    invoke-static {v0}, Lbif;->b(Lbif;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/16 v6, 0x8

    const/4 v2, 0x0

    .line 274
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "nothing to upload"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lbih;->a:Lbif;

    invoke-static {v0}, Lbif;->c(Lbif;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbik;

    iget-object v7, v0, Lbik;->e:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v0, v0, Lbik;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    move v0, v3

    :goto_2
    or-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbik;

    iget-object v4, p0, Lbih;->a:Lbif;

    invoke-static {v4}, Lbif;->d(Lbif;)Z

    move-result v7

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v3, :cond_9

    move v4, v3

    :goto_3
    and-int/2addr v4, v7

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x10

    if-lt v7, v8, :cond_a

    :goto_4
    and-int/2addr v3, v4

    if-eqz v3, :cond_c

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->e(Lbif;)Landroid/widget/ImageView;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->e(Lbif;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->f(Lbif;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->f(Lbif;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->g(Lbif;)Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    move-result-object v4

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->c(Lbif;)Landroid/app/Activity;

    move-result-object v7

    iget-object v3, v0, Lbik;->h:Landroid/net/Uri;

    iget-object v0, v4, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->b:Landroid/net/Uri;

    invoke-static {v0, v3}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    if-eqz v3, :cond_5

    :try_start_0
    sget-object v0, Lfqi;->h:Lfqi;

    invoke-virtual {v4, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Lfqi;)V

    :cond_5
    invoke-static {v7, v3}, Lgqo;->a(Landroid/content/Context;Landroid/net/Uri;)Lgqo;
    :try_end_0
    .catch Lgqu; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_5
    invoke-virtual {v4, v3, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->a(Landroid/net/Uri;Lgqo;)V

    :cond_6
    iget-object v0, p0, Lbih;->a:Lbif;

    invoke-static {v0}, Lbif;->c(Lbif;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->g(Lbif;)Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    new-instance v0, Lbil;

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-direct {v0, v3}, Lbil;-><init>(Lbif;)V

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->g(Lbif;)Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;

    move-result-object v3

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v3, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c:Lgqo;

    invoke-static {v4}, Lgqo;->a(Lgqo;)Lgqo;

    move-result-object v4

    if-eqz v4, :cond_b

    invoke-virtual {v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->c()Lgrx;

    move-result-object v3

    invoke-virtual {v0, v4, v3}, Lgsp;->a(Lgqo;Lgrw;)V

    :goto_6
    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->h(Lbif;)Lbxu;

    move-result-object v3

    invoke-virtual {v3, v0}, Lbxu;->a(Lbxy;)V

    :cond_7
    :goto_7
    iget-object v0, p0, Lbih;->a:Lbif;

    invoke-static {v0}, Lbif;->i(Lbif;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbih;->a:Lbif;

    invoke-static {v0}, Lbif;->i(Lbif;)Landroid/view/View;

    move-result-object v0

    if-eqz v1, :cond_10

    :goto_8
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    iget-object v0, p0, Lbih;->a:Lbif;

    invoke-static {v0}, Lbif;->a(Lbif;)V

    goto/16 :goto_0

    :cond_9
    move v4, v2

    goto/16 :goto_3

    :cond_a
    move v3, v2

    goto/16 :goto_4

    :catch_0
    move-exception v0

    const-string v7, "Editing of video not supported, previewing only"

    invoke-static {v7, v0}, Lezp;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v5

    goto :goto_5

    :catch_1
    move-exception v0

    const-string v3, "Cannot read video file"

    invoke-static {v3, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v4}, Lcom/google/android/libraries/youtube/upload/edit/ui/UploadEditVideoFragment;->d()V

    move-object v0, v5

    move-object v3, v5

    goto :goto_5

    :cond_b
    invoke-virtual {v0, v5, v5}, Lgsp;->a(Lgqo;Lgrw;)V

    goto :goto_6

    :cond_c
    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->e(Lbif;)Landroid/widget/ImageView;

    move-result-object v3

    if-eqz v3, :cond_d

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->e(Lbif;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->e(Lbif;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, v0, Lbik;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_d
    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->f(Lbif;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v3, v0, Lbik;->d:Ljava/lang/Long;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->f(Lbif;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, v0, Lbik;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    long-to-int v0, v4

    if-lez v0, :cond_e

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->f(Lbif;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lbih;->a:Lbif;

    invoke-static {v3}, Lbif;->f(Lbif;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {v0}, La;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    :cond_e
    iget-object v0, p0, Lbih;->a:Lbif;

    invoke-static {v0}, Lbif;->f(Lbif;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    :cond_f
    iget-object v0, p0, Lbih;->a:Lbif;

    invoke-static {v0}, Lbif;->f(Lbif;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    :cond_10
    move v2, v6

    goto/16 :goto_8
.end method
