.class public final Ldgf;
.super Levb;
.source "SourceFile"


# instance fields
.field final a:Levn;

.field private final d:Lhog;

.field private final e:Levc;

.field private f:Lflc;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Levn;Lhog;Levc;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Levb;-><init>()V

    .line 39
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldgf;->a:Levn;

    .line 40
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhog;

    iput-object v0, p0, Ldgf;->d:Lhog;

    .line 41
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levc;

    iput-object v0, p0, Ldgf;->e:Levc;

    .line 42
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 70
    iget-object v0, p0, Ldgf;->f:Lflc;

    if-nez v0, :cond_0

    .line 71
    iput-boolean v3, p0, Ldgf;->c:Z

    .line 91
    :goto_0
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Ldgf;->f:Lflc;

    iget-boolean v1, p0, Ldgf;->g:Z

    iget-boolean v2, p0, Ldgf;->h:Z

    invoke-virtual {v0, v1, v2, v3}, Lflc;->a(ZZZ)Lhaz;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_1

    iget-object v1, v0, Lhaz;->c:Lhog;

    if-nez v1, :cond_2

    .line 79
    :cond_1
    iput-boolean v3, p0, Ldgf;->c:Z

    goto :goto_0

    .line 85
    :cond_2
    iget-object v0, v0, Lhaz;->c:Lhog;

    iget-object v1, p0, Ldgf;->d:Lhog;

    invoke-static {v0, v1}, Lidh;->a(Lidh;Lidh;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldgf;->c:Z

    .line 87
    iget-object v0, p0, Ldgf;->e:Levc;

    invoke-interface {v0}, Levc;->a()V

    goto :goto_0

    .line 89
    :cond_3
    iput-boolean v3, p0, Ldgf;->c:Z

    goto :goto_0
.end method


# virtual methods
.method public final handleSequencerHasPreviousNextEvent(Lczt;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 55
    iget-boolean v0, p1, Lczt;->c:Z

    iput-boolean v0, p0, Ldgf;->g:Z

    .line 56
    iget-boolean v0, p1, Lczt;->d:Z

    iput-boolean v0, p0, Ldgf;->h:Z

    .line 57
    invoke-direct {p0}, Ldgf;->a()V

    .line 58
    return-void
.end method

.method public final handleSequencerStage(Lczu;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p1, Lczu;->a:Lgok;

    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p1, Lczu;->c:Lfnx;

    iget-object v0, v0, Lfnx;->f:Lflc;

    iput-object v0, p0, Ldgf;->f:Lflc;

    .line 64
    invoke-direct {p0}, Ldgf;->a()V

    .line 66
    :cond_0
    return-void
.end method
