.class public final Lhic;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lhid;

.field public b:Ljava/lang/String;

.field public c:[B

.field private d:Ljava/lang/String;

.field private e:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64738
    invoke-direct {p0}, Lidf;-><init>()V

    .line 64741
    sget-object v0, Lhid;->a:[Lhid;

    iput-object v0, p0, Lhic;->a:[Lhid;

    .line 64744
    const-string v0, ""

    iput-object v0, p0, Lhic;->b:Ljava/lang/String;

    .line 64747
    const-string v0, ""

    iput-object v0, p0, Lhic;->d:Ljava/lang/String;

    .line 64750
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhic;->c:[B

    .line 64753
    const/4 v0, 0x0

    iput-object v0, p0, Lhic;->e:Lhgz;

    .line 64738
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 64828
    .line 64829
    iget-object v1, p0, Lhic;->a:[Lhid;

    if-eqz v1, :cond_1

    .line 64830
    iget-object v2, p0, Lhic;->a:[Lhid;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 64831
    if-eqz v4, :cond_0

    .line 64832
    const/4 v5, 0x3

    .line 64833
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 64830
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64837
    :cond_1
    iget-object v1, p0, Lhic;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 64838
    const/4 v1, 0x4

    iget-object v2, p0, Lhic;->b:Ljava/lang/String;

    .line 64839
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64841
    :cond_2
    iget-object v1, p0, Lhic;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 64842
    const/4 v1, 0x5

    iget-object v2, p0, Lhic;->d:Ljava/lang/String;

    .line 64843
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64845
    :cond_3
    iget-object v1, p0, Lhic;->c:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_4

    .line 64846
    const/4 v1, 0x7

    iget-object v2, p0, Lhic;->c:[B

    .line 64847
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 64849
    :cond_4
    iget-object v1, p0, Lhic;->e:Lhgz;

    if-eqz v1, :cond_5

    .line 64850
    const/16 v1, 0x8

    iget-object v2, p0, Lhic;->e:Lhgz;

    .line 64851
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64853
    :cond_5
    iget-object v1, p0, Lhic;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64854
    iput v0, p0, Lhic;->J:I

    .line 64855
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64734
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhic;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhic;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhic;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhic;->a:[Lhid;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhid;

    iget-object v3, p0, Lhic;->a:[Lhid;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhic;->a:[Lhid;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhic;->a:[Lhid;

    :goto_2
    iget-object v2, p0, Lhic;->a:[Lhid;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhic;->a:[Lhid;

    new-instance v3, Lhid;

    invoke-direct {v3}, Lhid;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhic;->a:[Lhid;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhic;->a:[Lhid;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhic;->a:[Lhid;

    new-instance v3, Lhid;

    invoke-direct {v3}, Lhid;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhic;->a:[Lhid;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhic;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhic;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhic;->c:[B

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhic;->e:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhic;->e:Lhgz;

    :cond_5
    iget-object v0, p0, Lhic;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x3a -> :sswitch_4
        0x42 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 64803
    iget-object v0, p0, Lhic;->a:[Lhid;

    if-eqz v0, :cond_1

    .line 64804
    iget-object v1, p0, Lhic;->a:[Lhid;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 64805
    if-eqz v3, :cond_0

    .line 64806
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 64804
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64810
    :cond_1
    iget-object v0, p0, Lhic;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 64811
    const/4 v0, 0x4

    iget-object v1, p0, Lhic;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 64813
    :cond_2
    iget-object v0, p0, Lhic;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 64814
    const/4 v0, 0x5

    iget-object v1, p0, Lhic;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 64816
    :cond_3
    iget-object v0, p0, Lhic;->c:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 64817
    const/4 v0, 0x7

    iget-object v1, p0, Lhic;->c:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 64819
    :cond_4
    iget-object v0, p0, Lhic;->e:Lhgz;

    if-eqz v0, :cond_5

    .line 64820
    const/16 v0, 0x8

    iget-object v1, p0, Lhic;->e:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64822
    :cond_5
    iget-object v0, p0, Lhic;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 64824
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64768
    if-ne p1, p0, :cond_1

    .line 64776
    :cond_0
    :goto_0
    return v0

    .line 64769
    :cond_1
    instance-of v2, p1, Lhic;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 64770
    :cond_2
    check-cast p1, Lhic;

    .line 64771
    iget-object v2, p0, Lhic;->a:[Lhid;

    iget-object v3, p1, Lhic;->a:[Lhid;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhic;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhic;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 64772
    :goto_1
    iget-object v2, p0, Lhic;->d:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhic;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 64773
    :goto_2
    iget-object v2, p0, Lhic;->c:[B

    iget-object v3, p1, Lhic;->c:[B

    .line 64774
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhic;->e:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhic;->e:Lhgz;

    if-nez v2, :cond_3

    .line 64775
    :goto_3
    iget-object v2, p0, Lhic;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhic;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 64776
    goto :goto_0

    .line 64771
    :cond_4
    iget-object v2, p0, Lhic;->b:Ljava/lang/String;

    iget-object v3, p1, Lhic;->b:Ljava/lang/String;

    .line 64772
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhic;->d:Ljava/lang/String;

    iget-object v3, p1, Lhic;->d:Ljava/lang/String;

    .line 64773
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 64774
    :cond_6
    iget-object v2, p0, Lhic;->e:Lhgz;

    iget-object v3, p1, Lhic;->e:Lhgz;

    .line 64775
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhic;->I:Ljava/util/List;

    iget-object v3, p1, Lhic;->I:Ljava/util/List;

    .line 64776
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64780
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 64782
    iget-object v2, p0, Lhic;->a:[Lhid;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 64788
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhic;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 64789
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhic;->d:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 64790
    iget-object v2, p0, Lhic;->c:[B

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    .line 64796
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhic;->e:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 64797
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhic;->I:Ljava/util/List;

    if-nez v2, :cond_8

    :goto_3
    add-int/2addr v0, v1

    .line 64798
    return v0

    :cond_2
    move v2, v0

    move v0, v1

    .line 64784
    :goto_4
    iget-object v3, p0, Lhic;->a:[Lhid;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 64785
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhic;->a:[Lhid;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    .line 64784
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 64785
    :cond_3
    iget-object v2, p0, Lhic;->a:[Lhid;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhid;->hashCode()I

    move-result v2

    goto :goto_5

    .line 64788
    :cond_4
    iget-object v0, p0, Lhic;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 64789
    :cond_5
    iget-object v0, p0, Lhic;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_6
    move v2, v0

    move v0, v1

    .line 64792
    :goto_6
    iget-object v3, p0, Lhic;->c:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 64793
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhic;->c:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 64792
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 64796
    :cond_7
    iget-object v0, p0, Lhic;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 64797
    :cond_8
    iget-object v1, p0, Lhic;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
