.class public Ldpr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ldqq;

.field final b:Ldqr;

.field final c:Ldyg;


# direct methods
.method public constructor <init>(Ldqq;Ldqr;Ldyg;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqq;

    iput-object v0, p0, Ldpr;->a:Ldqq;

    .line 33
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqr;

    iput-object v0, p0, Ldpr;->b:Ldqr;

    .line 34
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldpr;->c:Ldyg;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 40
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 41
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 42
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v3

    .line 43
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getInt(I)I

    move-result v4

    .line 44
    iget-object v5, p0, Ldpr;->a:Ldqq;

    invoke-interface {v5, v4}, Ldqq;->a(I)V

    .line 45
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v3

    .line 46
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 48
    if-nez v4, :cond_1

    .line 51
    iget-object v4, p0, Ldpr;->a:Ldqq;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ldqq;->a(Ljava/lang/String;)V

    .line 52
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 53
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    .line 41
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :cond_1
    if-ne v4, v6, :cond_3

    .line 57
    iget-object v4, p0, Ldpr;->a:Ldqq;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ldqq;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    iget-object v2, p0, Ldpr;->c:Ldyg;

    const-string v3, "Chunk stream error: %s"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Ldyg;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    :cond_2
    return-void

    .line 58
    :cond_3
    if-le v4, v6, :cond_0

    .line 60
    :try_start_1
    iget-object v4, p0, Ldpr;->b:Ldqr;

    invoke-interface {v4, v3}, Ldqr;->a(Lorg/json/JSONArray;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
