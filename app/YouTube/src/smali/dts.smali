.class public final Ldts;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcws;

.field private final b:Levn;

.field private final c:Ldwq;

.field private final d:Ldyg;


# direct methods
.method public constructor <init>(Levn;Ldwq;Ldyg;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldts;->b:Levn;

    .line 35
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Ldts;->c:Ldwq;

    .line 36
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldts;->d:Ldyg;

    .line 37
    return-void
.end method

.method private a(Ldtx;)V
    .locals 4

    .prologue
    .line 109
    iget-object v0, p0, Ldts;->d:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x34

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "LocalPlaybackControl: broadcast second screen mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Ldts;->b:Levn;

    invoke-virtual {v0, p1}, Levn;->d(Ljava/lang/Object;)V

    .line 111
    return-void
.end method


# virtual methods
.method public final handleMdxPlaybackChangedEvent(Ldwi;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Ldts;->a:Lcws;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    iget-object v0, p0, Ldts;->c:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    invoke-virtual {v0}, Ldww;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v0, p1, Ldwi;->a:Ldwj;

    invoke-virtual {v0}, Ldwj;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lgoh;

    iget-object v2, v0, Ldwj;->a:Ljava/lang/String;

    iget-object v3, v0, Ldwj;->d:Ljava/lang/String;

    iget v4, v0, Ldwj;->e:I

    iget v0, v0, Ldwj;->b:I

    invoke-direct {v1, v2, v3, v4, v0}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    iget-object v0, p0, Ldts;->d:Ldyg;

    const-string v2, "LocalControl on playback changed videoId: %s, playlistId: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lgoh;->a:Leaa;

    iget-object v5, v5, Leaa;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v1, Lgoh;->a:Leaa;

    iget-object v5, v5, Leaa;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Ldts;->a:Lcws;

    invoke-virtual {v0, v1}, Lcws;->a(Lgoh;)V

    sget-object v0, Ldtx;->a:Ldtx;

    invoke-direct {p0, v0}, Ldts;->a(Ldtx;)V

    goto :goto_0
.end method

.method public final handleMdxPlaylistChangedEvent(Ldwp;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 73
    iget-object v0, p0, Ldts;->a:Lcws;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Ldts;->c:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    invoke-virtual {v0}, Ldww;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p1, Ldwp;->a:Ldwj;

    invoke-virtual {v0}, Ldwj;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Ldwj;->a:Ljava/lang/String;

    iget-object v2, p0, Ldts;->a:Lcws;

    invoke-virtual {v2}, Lcws;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Ldts;->d:Ldyg;

    const-string v1, "LocalControl: reload TV queue"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldts;->a:Lcws;

    invoke-virtual {v0}, Lcws;->o()Z

    goto :goto_0

    :cond_1
    new-instance v1, Lgoh;

    iget-object v2, v0, Ldwj;->a:Ljava/lang/String;

    iget-object v3, v0, Ldwj;->d:Ljava/lang/String;

    iget v0, v0, Ldwj;->e:I

    invoke-direct {v1, v2, v3, v0, v5}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    iget-object v0, p0, Ldts;->d:Ldyg;

    const-string v2, "LocalControl on TV queue changed videoId: %s, playlistId: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v1, Lgoh;->a:Leaa;

    iget-object v4, v4, Leaa;->a:Ljava/lang/String;

    aput-object v4, v3, v5

    const/4 v4, 0x1

    iget-object v5, v1, Lgoh;->a:Leaa;

    iget-object v5, v5, Leaa;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Ldts;->a:Lcws;

    invoke-virtual {v0, v1}, Lcws;->a(Lgoh;)V

    sget-object v0, Ldtx;->b:Ldtx;

    invoke-direct {p0, v0}, Ldts;->a(Ldtx;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldts;->d:Ldyg;

    const-string v1, "LocalControl: TV queue is empty. nothing to show, "

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldts;->a:Lcws;

    invoke-virtual {v0}, Lcws;->b()V

    sget-object v0, Ldtx;->c:Ldtx;

    invoke-direct {p0, v0}, Ldts;->a(Ldtx;)V

    goto :goto_0
.end method

.method public final handleMdxStateChangedEvent(Ldwx;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Ldts;->a:Lcws;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v0, p1, Ldwx;->a:Ldww;

    sget-object v1, Ldtt;->a:[I

    invoke-virtual {v0}, Ldww;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 117
    :goto_0
    return-void

    .line 116
    :pswitch_0
    iget-object v0, p0, Ldts;->a:Lcws;

    invoke-virtual {v0}, Lcws;->a()V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    iget-object v1, p0, Ldts;->a:Lcws;

    invoke-virtual {v1}, Lcws;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Ldts;->a:Lcws;

    invoke-virtual {v0}, Lcws;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldyh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lgoh;

    iget-object v1, p0, Ldts;->a:Lcws;

    invoke-virtual {v1}, Lcws;->e()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    const/4 v3, -0x1

    iget-object v4, p0, Ldts;->a:Lcws;

    invoke-virtual {v4}, Lcws;->g()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    :goto_1
    iget-object v1, p0, Ldts;->a:Lcws;

    invoke-virtual {v1}, Lcws;->a()V

    if-eqz v0, :cond_2

    iget-object v1, p0, Ldts;->d:Ldyg;

    const-string v2, "LocalPlaybackControl, reload video %s: "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, Lgoh;->a:Leaa;

    iget-object v5, v5, Leaa;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Ldts;->a:Lcws;

    invoke-virtual {v1, v0}, Lcws;->a(Lgoh;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lgoh;

    iget-object v1, p0, Ldts;->a:Lcws;

    invoke-virtual {v1}, Lcws;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldts;->a:Lcws;

    invoke-virtual {v2}, Lcws;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldts;->a:Lcws;

    invoke-virtual {v3}, Lcws;->f()I

    move-result v3

    iget-object v4, p0, Ldts;->a:Lcws;

    invoke-virtual {v4}, Lcws;->g()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ldts;->d:Ldyg;

    const-string v1, "LocalPlaybckControl: no video to reload"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    sget-object v0, Ldtx;->c:Ldtx;

    invoke-direct {p0, v0}, Ldts;->a(Ldtx;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
