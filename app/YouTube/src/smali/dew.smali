.class public final enum Ldew;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static enum a:Ldew;

.field private static enum b:Ldew;

.field private static enum c:Ldew;

.field private static enum d:Ldew;

.field private static enum e:Ldew;

.field private static enum f:Ldew;

.field private static enum g:Ldew;

.field private static l:[Ljava/lang/String;

.field private static m:[Ljava/lang/String;

.field private static final synthetic n:[Ldew;


# instance fields
.field private final h:I

.field private final i:I

.field private final j:Ldez;

.field private k:Landroid/graphics/Typeface;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 251
    new-instance v0, Ldew;

    const-string v1, "MONOSPACED_SERIF"

    const v3, 0x7f090164

    const-string v4, "fonts/MonoSerif-Regular.ttf"

    .line 253
    invoke-static {v4}, Ldew;->a(Ljava/lang/String;)Ldez;

    move-result-object v5

    move v4, v2

    invoke-direct/range {v0 .. v5}, Ldew;-><init>(Ljava/lang/String;IIILdez;)V

    sput-object v0, Ldew;->a:Ldew;

    .line 254
    new-instance v3, Ldew;

    const-string v4, "PROPORTIONAL_SERIF"

    const v6, 0x7f090165

    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    .line 256
    invoke-static {v0}, Ldew;->a(Landroid/graphics/Typeface;)Ldez;

    move-result-object v8

    move v5, v9

    move v7, v9

    invoke-direct/range {v3 .. v8}, Ldew;-><init>(Ljava/lang/String;IIILdez;)V

    sput-object v3, Ldew;->b:Ldew;

    .line 257
    new-instance v3, Ldew;

    const-string v4, "MONOSPACED_SANS_SERIF"

    const v6, 0x7f090166

    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    .line 259
    invoke-static {v0}, Ldew;->a(Landroid/graphics/Typeface;)Ldez;

    move-result-object v8

    move v5, v10

    move v7, v10

    invoke-direct/range {v3 .. v8}, Ldew;-><init>(Ljava/lang/String;IIILdez;)V

    sput-object v3, Ldew;->c:Ldew;

    .line 260
    new-instance v3, Ldew;

    const-string v4, "PROPORTIONAL_SANS_SERIF"

    const v6, 0x7f090167

    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    .line 262
    invoke-static {v0}, Ldew;->a(Landroid/graphics/Typeface;)Ldez;

    move-result-object v8

    move v5, v11

    move v7, v11

    invoke-direct/range {v3 .. v8}, Ldew;-><init>(Ljava/lang/String;IIILdez;)V

    sput-object v3, Ldew;->d:Ldew;

    .line 263
    new-instance v3, Ldew;

    const-string v4, "CASUAL"

    const v6, 0x7f090168

    const-string v0, "fonts/ComingSoon-Regular.ttf"

    .line 265
    invoke-static {v0}, Ldew;->a(Ljava/lang/String;)Ldez;

    move-result-object v8

    move v5, v12

    move v7, v12

    invoke-direct/range {v3 .. v8}, Ldew;-><init>(Ljava/lang/String;IIILdez;)V

    sput-object v3, Ldew;->e:Ldew;

    .line 266
    new-instance v3, Ldew;

    const-string v4, "CURSIVE"

    const/4 v5, 0x5

    const v6, 0x7f090169

    const/4 v7, 0x5

    const-string v0, "fonts/DancingScript-Regular.ttf"

    .line 268
    invoke-static {v0}, Ldew;->a(Ljava/lang/String;)Ldez;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Ldew;-><init>(Ljava/lang/String;IIILdez;)V

    sput-object v3, Ldew;->f:Ldew;

    .line 269
    new-instance v3, Ldew;

    const-string v4, "SMALL_CAPITALS"

    const/4 v5, 0x6

    const v6, 0x7f09016a

    const/4 v7, 0x6

    const-string v0, "fonts/CarroisGothicSC-Regular.ttf"

    .line 271
    invoke-static {v0}, Ldew;->a(Ljava/lang/String;)Ldez;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Ldew;-><init>(Ljava/lang/String;IIILdez;)V

    sput-object v3, Ldew;->g:Ldew;

    .line 250
    const/4 v0, 0x7

    new-array v0, v0, [Ldew;

    sget-object v1, Ldew;->a:Ldew;

    aput-object v1, v0, v2

    sget-object v1, Ldew;->b:Ldew;

    aput-object v1, v0, v9

    sget-object v1, Ldew;->c:Ldew;

    aput-object v1, v0, v10

    sget-object v1, Ldew;->d:Ldew;

    aput-object v1, v0, v11

    sget-object v1, Ldew;->e:Ldew;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Ldew;->f:Ldew;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldew;->g:Ldew;

    aput-object v2, v0, v1

    sput-object v0, Ldew;->n:[Ldew;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILdez;)V
    .locals 0

    .prologue
    .line 303
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 304
    iput p3, p0, Ldew;->h:I

    .line 305
    iput p4, p0, Ldew;->i:I

    .line 306
    iput-object p5, p0, Ldew;->j:Ldez;

    .line 307
    return-void
.end method

.method public static a(ILandroid/content/res/AssetManager;)Landroid/graphics/Typeface;
    .locals 4

    .prologue
    .line 338
    invoke-static {}, Ldew;->values()[Ldew;

    move-result-object v1

    .line 339
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 340
    aget-object v2, v1, v0

    iget v2, v2, Ldew;->i:I

    if-ne v2, p0, :cond_1

    .line 341
    aget-object v2, v1, v0

    iget-object v2, v2, Ldew;->k:Landroid/graphics/Typeface;

    if-nez v2, :cond_0

    .line 342
    aget-object v2, v1, v0

    aget-object v3, v1, v0

    iget-object v3, v3, Ldew;->j:Ldez;

    invoke-interface {v3, p1}, Ldez;->a(Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v3

    iput-object v3, v2, Ldew;->k:Landroid/graphics/Typeface;

    .line 344
    :cond_0
    aget-object v0, v1, v0

    iget-object v0, v0, Ldew;->k:Landroid/graphics/Typeface;

    .line 347
    :goto_1
    return-object v0

    .line 339
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 347
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lgpo;)Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 351
    iget v0, p1, Lgpo;->f:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    .line 352
    iget v0, p1, Lgpo;->f:I

    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v0, v1}, Ldew;->a(ILandroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 359
    :goto_0
    return-object v0

    .line 355
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 356
    const-string v0, "captioning"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getUserStyle()Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    .line 359
    :cond_1
    const/4 v0, 0x3

    .line 360
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 359
    invoke-static {v0, v1}, Ldew;->a(ILandroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Typeface;)Ldez;
    .locals 1

    .prologue
    .line 287
    new-instance v0, Ldey;

    invoke-direct {v0, p0}, Ldey;-><init>(Landroid/graphics/Typeface;)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ldez;
    .locals 1

    .prologue
    .line 278
    new-instance v0, Ldex;

    invoke-direct {v0, p0}, Ldex;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 323
    sget-object v0, Ldew;->m:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 324
    invoke-static {}, Ldew;->values()[Ldew;

    move-result-object v1

    .line 325
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Ldew;->m:[Ljava/lang/String;

    .line 326
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 327
    sget-object v2, Ldew;->m:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Ldew;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 326
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 330
    :cond_0
    sget-object v0, Ldew;->m:[Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 312
    sget-object v0, Ldew;->l:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 313
    invoke-static {}, Ldew;->values()[Ldew;

    move-result-object v1

    .line 314
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Ldew;->l:[Ljava/lang/String;

    .line 315
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 316
    sget-object v2, Ldew;->l:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Ldew;->h:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 315
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 319
    :cond_0
    sget-object v0, Ldew;->l:[Ljava/lang/String;

    return-object v0
.end method

.method public static b()I
    .locals 2

    .prologue
    .line 334
    invoke-static {}, Ldew;->values()[Ldew;

    move-result-object v0

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget v0, v0, Ldew;->i:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldew;
    .locals 1

    .prologue
    .line 250
    const-class v0, Ldew;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldew;

    return-object v0
.end method

.method public static values()[Ldew;
    .locals 1

    .prologue
    .line 250
    sget-object v0, Ldew;->n:[Ldew;

    invoke-virtual {v0}, [Ldew;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldew;

    return-object v0
.end method
