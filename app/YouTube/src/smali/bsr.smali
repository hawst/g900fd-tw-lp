.class public final Lbsr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lbhz;

.field final b:Lgnd;

.field public final c:Lfxe;

.field final d:Lbtb;

.field final e:Lbue;

.field final f:Landroid/view/View;

.field final g:Landroid/widget/ImageView;

.field final h:Lcft;

.field final i:Lbty;

.field final j:Lbjq;

.field public final k:Ljava/lang/String;

.field l:Lgbu;

.field m:Lgbu;

.field n:Z

.field private final o:Leyp;

.field private final p:Landroid/widget/ImageView;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Landroid/view/View;

.field private final u:Landroid/widget/ImageView;

.field private final v:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Lbhz;Lgnd;Lfeb;Lfxe;Leyp;Lbty;Lbjq;ILandroid/view/ViewGroup;Lbtb;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhz;

    iput-object v0, p0, Lbsr;->a:Lbhz;

    .line 99
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnd;

    iput-object v0, p0, Lbsr;->b:Lgnd;

    .line 100
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lbsr;->c:Lfxe;

    .line 102
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbsr;->o:Leyp;

    .line 103
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbty;

    iput-object v0, p0, Lbsr;->i:Lbty;

    .line 104
    iput-object p7, p0, Lbsr;->j:Lbjq;

    .line 105
    iget-object v0, p0, Lbsr;->j:Lbjq;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtb;

    iput-object v0, p0, Lbsr;->d:Lbtb;

    .line 108
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbsr;->k:Ljava/lang/String;

    .line 110
    const v0, 0x7f08026a

    .line 111
    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 112
    if-eqz v0, :cond_0

    new-instance v1, Lbue;

    invoke-direct {v1, v0}, Lbue;-><init>(Landroid/widget/ImageView;)V

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lbsr;->e:Lbue;

    .line 115
    const v0, 0x7f08012c

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbsr;->f:Landroid/view/View;

    .line 116
    iget-object v0, p0, Lbsr;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbsr;->f:Landroid/view/View;

    const v1, 0x7f0800a4

    .line 117
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    :goto_1
    iput-object v0, p0, Lbsr;->p:Landroid/widget/ImageView;

    .line 119
    const v0, 0x7f08026e

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbsr;->q:Landroid/widget/TextView;

    .line 120
    const v0, 0x7f080270

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbsr;->r:Landroid/widget/TextView;

    .line 121
    const v0, 0x7f080271

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbsr;->s:Landroid/widget/TextView;

    .line 122
    const v0, 0x7f08026f

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbsr;->t:Landroid/view/View;

    .line 123
    const v0, 0x7f080272

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbsr;->u:Landroid/widget/ImageView;

    .line 124
    const v0, 0x7f080215

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbsr;->g:Landroid/widget/ImageView;

    .line 125
    const v0, 0x7f080273

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbsr;->v:Landroid/widget/ImageView;

    .line 127
    iget-object v0, p0, Lbsr;->t:Landroid/view/View;

    new-instance v1, Lbss;

    invoke-direct {v1, p0}, Lbss;-><init>(Lbsr;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v0, p0, Lbsr;->u:Landroid/widget/ImageView;

    new-instance v1, Lbst;

    invoke-direct {v1, p0}, Lbst;-><init>(Lbsr;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    invoke-direct {p0, v3}, Lbsr;->a(Z)V

    .line 155
    iget-object v0, p0, Lbsr;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 156
    iget-object v0, p0, Lbsr;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 157
    iget-object v0, p0, Lbsr;->g:Landroid/widget/ImageView;

    new-instance v1, Lbsu;

    invoke-direct {v1, p0, p11}, Lbsu;-><init>(Lbsr;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v0, p0, Lbsr;->v:Landroid/widget/ImageView;

    new-instance v1, Lbsv;

    invoke-direct {v1, p0, p11}, Lbsv;-><init>(Lbsr;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    new-instance v0, Lbsw;

    invoke-direct {v0, p0}, Lbsw;-><init>(Lbsr;)V

    .line 204
    new-instance v1, Lcft;

    new-instance v2, Lbsx;

    invoke-direct {v2, p0, p11, v0}, Lbsx;-><init>(Lbsr;Ljava/lang/String;Lbjw;)V

    invoke-direct {v1, p9, v2}, Lcft;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lbsr;->h:Lcft;

    .line 224
    iget-object v0, p0, Lbsr;->b:Lgnd;

    invoke-interface {v0, p11}, Lgnd;->c(Ljava/lang/String;)Lglw;

    move-result-object v0

    .line 225
    iget-object v1, v0, Lglw;->a:Lgbu;

    invoke-direct {p0, v1}, Lbsr;->a(Lgbu;)V

    .line 226
    invoke-virtual {p0, v0}, Lbsr;->a(Lglw;)V

    .line 227
    return-void

    :cond_0
    move-object v0, v2

    .line 112
    goto/16 :goto_0

    :cond_1
    move-object v0, v2

    .line 117
    goto/16 :goto_1
.end method

.method private a(Lgbu;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 245
    iput-object p1, p0, Lbsr;->l:Lgbu;

    .line 247
    iget-object v0, p0, Lbsr;->q:Landroid/widget/TextView;

    iget-object v1, p1, Lgbu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v0, p0, Lbsr;->r:Landroid/widget/TextView;

    iget-object v1, p1, Lgbu;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v0, p0, Lbsr;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lbsr;->a:Lbhz;

    invoke-virtual {v1}, Lbhz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f100000

    iget v3, p1, Lgbu;->j:I

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p1, Lgbu;->j:I

    .line 252
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 249
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v0, p0, Lbsr;->p:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p1, Lgbu;->h:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lgbu;->h:Landroid/net/Uri;

    .line 258
    :goto_0
    if-eqz v0, :cond_0

    .line 259
    iget-object v1, p0, Lbsr;->o:Leyp;

    iget-object v2, p0, Lbsr;->a:Lbhz;

    new-instance v3, Lbsz;

    iget-object v4, p0, Lbsr;->p:Landroid/widget/ImageView;

    invoke-direct {v3, p0, v4}, Lbsz;-><init>(Lbsr;Landroid/widget/ImageView;)V

    .line 261
    invoke-static {v2, v3}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v2

    .line 259
    invoke-interface {v1, v0, v2}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    .line 265
    :cond_0
    iget-object v0, p0, Lbsr;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 266
    iget-object v0, p0, Lbsr;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 267
    return-void

    .line 256
    :cond_1
    iget-object v0, p1, Lgbu;->g:Landroid/net/Uri;

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 330
    iput-boolean p1, p0, Lbsr;->n:Z

    .line 331
    iget-object v1, p0, Lbsr;->g:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const v0, 0x7f020174

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 333
    return-void

    .line 331
    :cond_0
    const v0, 0x7f020173

    goto :goto_0
.end method


# virtual methods
.method a(Lglw;)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lbsr;->h:Lcft;

    .line 271
    if-eqz p1, :cond_0

    iget-object v0, p0, Lbsr;->m:Lgbu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbsr;->m:Lgbu;

    .line 275
    invoke-virtual {p1, v0}, Lglw;->a(Lgbu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 276
    :goto_0
    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lbsr;->h:Lcft;

    invoke-virtual {v0}, Lcft;->f()V

    .line 281
    :goto_1
    return-void

    .line 275
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 279
    :cond_1
    iget-object v0, p0, Lbsr;->h:Lcft;

    invoke-virtual {v0, p1}, Lcft;->a(Lglw;)Landroid/view/View;

    goto :goto_1
.end method

.method public final handleOfflinePlaylistAddEvent(Lblq;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 285
    iget-object v0, p1, Lblq;->a:Lglw;

    .line 286
    iget-object v1, v0, Lglw;->a:Lgbu;

    iget-object v1, v1, Lgbu;->a:Ljava/lang/String;

    iget-object v2, p0, Lbsr;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287
    invoke-virtual {p0, v0}, Lbsr;->a(Lglw;)V

    .line 289
    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistAddFailedEvent(Lblo;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 293
    iget-object v0, p1, Lblo;->a:Ljava/lang/String;

    iget-object v1, p0, Lbsr;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbsr;->a(Lglw;)V

    .line 296
    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistDeleteEvent(Lblp;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 308
    iget-object v0, p1, Lblp;->a:Ljava/lang/String;

    iget-object v1, p0, Lbsr;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbsr;->a(Lglw;)V

    .line 311
    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistProgressEvent(Lblr;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 300
    iget-object v0, p1, Lblr;->a:Lglw;

    .line 301
    iget-object v1, v0, Lglw;->a:Lgbu;

    iget-object v1, v1, Lgbu;->a:Ljava/lang/String;

    iget-object v2, p0, Lbsr;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 302
    invoke-virtual {p0, v0}, Lbsr;->a(Lglw;)V

    .line 304
    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistSyncEvent(Lbls;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 315
    iget-object v0, p1, Lbls;->a:Lglw;

    .line 316
    iget-object v1, v0, Lglw;->a:Lgbu;

    iget-object v1, v1, Lgbu;->a:Ljava/lang/String;

    iget-object v2, p0, Lbsr;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 317
    iget-object v1, v0, Lglw;->a:Lgbu;

    invoke-direct {p0, v1}, Lbsr;->a(Lgbu;)V

    .line 318
    invoke-virtual {p0, v0}, Lbsr;->a(Lglw;)V

    .line 320
    :cond_0
    return-void
.end method

.method public final handlePlaylistLikeActionEvent(Lbui;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 324
    iget-object v0, p0, Lbsr;->l:Lgbu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbsr;->l:Lgbu;

    iget-object v0, v0, Lgbu;->a:Ljava/lang/String;

    iget-object v1, p1, Lbui;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p1, Lbui;->b:Lbrg;

    sget-object v1, Lbrg;->a:Lbrg;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lbsr;->a(Z)V

    .line 327
    :cond_0
    return-void

    .line 325
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
