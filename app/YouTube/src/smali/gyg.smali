.class public final Lgyg;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhjx;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:[Lgyk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2096
    invoke-direct {p0}, Lidf;-><init>()V

    .line 2108
    const/4 v0, 0x0

    iput-object v0, p0, Lgyg;->a:Lhjx;

    .line 2111
    const/4 v0, 0x0

    iput v0, p0, Lgyg;->b:I

    .line 2114
    const-string v0, ""

    iput-object v0, p0, Lgyg;->c:Ljava/lang/String;

    .line 2117
    sget-object v0, Lgyk;->a:[Lgyk;

    iput-object v0, p0, Lgyg;->d:[Lgyk;

    .line 2096
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2181
    .line 2182
    iget-object v0, p0, Lgyg;->a:Lhjx;

    if-eqz v0, :cond_4

    .line 2183
    const/4 v0, 0x1

    iget-object v2, p0, Lgyg;->a:Lhjx;

    .line 2184
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2186
    :goto_0
    iget v2, p0, Lgyg;->b:I

    if-eqz v2, :cond_0

    .line 2187
    const/4 v2, 0x2

    iget v3, p0, Lgyg;->b:I

    .line 2188
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2190
    :cond_0
    iget-object v2, p0, Lgyg;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2191
    const/4 v2, 0x3

    iget-object v3, p0, Lgyg;->c:Ljava/lang/String;

    .line 2192
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2194
    :cond_1
    iget-object v2, p0, Lgyg;->d:[Lgyk;

    if-eqz v2, :cond_3

    .line 2195
    iget-object v2, p0, Lgyg;->d:[Lgyk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 2196
    if-eqz v4, :cond_2

    .line 2197
    const/4 v5, 0x4

    .line 2198
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2195
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2202
    :cond_3
    iget-object v1, p0, Lgyg;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2203
    iput v0, p0, Lgyg;->J:I

    .line 2204
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2092
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lgyg;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lgyg;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lgyg;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgyg;->a:Lhjx;

    if-nez v0, :cond_2

    new-instance v0, Lhjx;

    invoke-direct {v0}, Lhjx;-><init>()V

    iput-object v0, p0, Lgyg;->a:Lhjx;

    :cond_2
    iget-object v0, p0, Lgyg;->a:Lhjx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_3

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    :cond_3
    iput v0, p0, Lgyg;->b:I

    goto :goto_0

    :cond_4
    iput v1, p0, Lgyg;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgyg;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgyg;->d:[Lgyk;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgyk;

    iget-object v3, p0, Lgyg;->d:[Lgyk;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lgyg;->d:[Lgyk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lgyg;->d:[Lgyk;

    :goto_2
    iget-object v2, p0, Lgyg;->d:[Lgyk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lgyg;->d:[Lgyk;

    new-instance v3, Lgyk;

    invoke-direct {v3}, Lgyk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgyg;->d:[Lgyk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lgyg;->d:[Lgyk;

    array-length v0, v0

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lgyg;->d:[Lgyk;

    new-instance v3, Lgyk;

    invoke-direct {v3}, Lgyk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgyg;->d:[Lgyk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 2159
    iget-object v0, p0, Lgyg;->a:Lhjx;

    if-eqz v0, :cond_0

    .line 2160
    const/4 v0, 0x1

    iget-object v1, p0, Lgyg;->a:Lhjx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2162
    :cond_0
    iget v0, p0, Lgyg;->b:I

    if-eqz v0, :cond_1

    .line 2163
    const/4 v0, 0x2

    iget v1, p0, Lgyg;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 2165
    :cond_1
    iget-object v0, p0, Lgyg;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2166
    const/4 v0, 0x3

    iget-object v1, p0, Lgyg;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 2168
    :cond_2
    iget-object v0, p0, Lgyg;->d:[Lgyk;

    if-eqz v0, :cond_4

    .line 2169
    iget-object v1, p0, Lgyg;->d:[Lgyk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 2170
    if-eqz v3, :cond_3

    .line 2171
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 2169
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2175
    :cond_4
    iget-object v0, p0, Lgyg;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 2177
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2131
    if-ne p1, p0, :cond_1

    .line 2138
    :cond_0
    :goto_0
    return v0

    .line 2132
    :cond_1
    instance-of v2, p1, Lgyg;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 2133
    :cond_2
    check-cast p1, Lgyg;

    .line 2134
    iget-object v2, p0, Lgyg;->a:Lhjx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyg;->a:Lhjx;

    if-nez v2, :cond_3

    :goto_1
    iget v2, p0, Lgyg;->b:I

    iget v3, p1, Lgyg;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyg;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyg;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 2136
    :goto_2
    iget-object v2, p0, Lgyg;->d:[Lgyk;

    iget-object v3, p1, Lgyg;->d:[Lgyk;

    .line 2137
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgyg;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgyg;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 2138
    goto :goto_0

    .line 2134
    :cond_4
    iget-object v2, p0, Lgyg;->a:Lhjx;

    iget-object v3, p1, Lgyg;->a:Lhjx;

    invoke-virtual {v2, v3}, Lhjx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyg;->c:Ljava/lang/String;

    iget-object v3, p1, Lgyg;->c:Ljava/lang/String;

    .line 2136
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 2137
    :cond_6
    iget-object v2, p0, Lgyg;->I:Ljava/util/List;

    iget-object v3, p1, Lgyg;->I:Ljava/util/List;

    .line 2138
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2142
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2144
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyg;->a:Lhjx;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2145
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgyg;->b:I

    add-int/2addr v0, v2

    .line 2146
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyg;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2147
    iget-object v2, p0, Lgyg;->d:[Lgyk;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 2153
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lgyg;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_2
    add-int/2addr v0, v1

    .line 2154
    return v0

    .line 2144
    :cond_1
    iget-object v0, p0, Lgyg;->a:Lhjx;

    invoke-virtual {v0}, Lhjx;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2146
    :cond_2
    iget-object v0, p0, Lgyg;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    move v2, v0

    move v0, v1

    .line 2149
    :goto_3
    iget-object v3, p0, Lgyg;->d:[Lgyk;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2150
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgyg;->d:[Lgyk;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    .line 2149
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2150
    :cond_4
    iget-object v2, p0, Lgyg;->d:[Lgyk;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    .line 2153
    :cond_5
    iget-object v1, p0, Lgyg;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
