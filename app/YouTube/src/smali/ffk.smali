.class public final enum Lffk;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lffk;

.field public static final enum b:Lffk;

.field public static final enum c:Lffk;

.field private static enum e:Lffk;

.field private static final synthetic f:[Lffk;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 163
    new-instance v0, Lffk;

    const-string v1, "DURATION_ANY"

    invoke-direct {v0, v1, v2, v2}, Lffk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffk;->a:Lffk;

    .line 164
    new-instance v0, Lffk;

    const-string v1, "DURATION_SHORT"

    invoke-direct {v0, v1, v3, v3}, Lffk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffk;->b:Lffk;

    .line 165
    new-instance v0, Lffk;

    const-string v1, "DURATION_LONG"

    invoke-direct {v0, v1, v4, v4}, Lffk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffk;->c:Lffk;

    .line 166
    new-instance v0, Lffk;

    const-string v1, "DURATION_MEDIUM"

    invoke-direct {v0, v1, v5, v5}, Lffk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffk;->e:Lffk;

    .line 162
    const/4 v0, 0x4

    new-array v0, v0, [Lffk;

    sget-object v1, Lffk;->a:Lffk;

    aput-object v1, v0, v2

    sget-object v1, Lffk;->b:Lffk;

    aput-object v1, v0, v3

    sget-object v1, Lffk;->c:Lffk;

    aput-object v1, v0, v4

    sget-object v1, Lffk;->e:Lffk;

    aput-object v1, v0, v5

    sput-object v0, Lffk;->f:[Lffk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 170
    iput p3, p0, Lffk;->d:I

    .line 171
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lffk;
    .locals 1

    .prologue
    .line 162
    const-class v0, Lffk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lffk;

    return-object v0
.end method

.method public static values()[Lffk;
    .locals 1

    .prologue
    .line 162
    sget-object v0, Lffk;->f:[Lffk;

    invoke-virtual {v0}, [Lffk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lffk;

    return-object v0
.end method
