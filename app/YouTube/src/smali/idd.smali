.class public final Lidd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field b:I

.field private final c:[B


# direct methods
.method constructor <init>([BII)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lidd;->c:[B

    .line 58
    iput p2, p0, Lidd;->b:I

    .line 59
    add-int v0, p2, p3

    iput v0, p0, Lidd;->a:I

    .line 60
    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 592
    if-ltz p0, :cond_0

    .line 593
    invoke-static {p0}, Lidd;->c(I)I

    move-result v0

    .line 596
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static a(J)I
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 845
    const-wide/16 v0, -0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 854
    :goto_0
    return v0

    .line 846
    :cond_0
    const-wide/16 v0, -0x4000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 847
    :cond_1
    const-wide/32 v0, -0x200000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    .line 848
    :cond_2
    const-wide/32 v0, -0x10000000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    .line 849
    :cond_3
    const-wide v0, -0x800000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const/4 v0, 0x5

    goto :goto_0

    .line 850
    :cond_4
    const-wide v0, -0x40000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    const/4 v0, 0x6

    goto :goto_0

    .line 851
    :cond_5
    const-wide/high16 v0, -0x2000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    const/4 v0, 0x7

    goto :goto_0

    .line 852
    :cond_6
    const-wide/high16 v0, -0x100000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    const/16 v0, 0x8

    goto :goto_0

    .line 853
    :cond_7
    const-wide/high16 v0, -0x8000000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    const/16 v0, 0x9

    goto :goto_0

    .line 854
    :cond_8
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 630
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 631
    array-length v1, v0

    invoke-static {v1}, Lidd;->c(I)I

    move-result v1

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v1

    return v0

    .line 634
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "UTF-8 not supported."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a([B)I
    .locals 2

    .prologue
    .line 660
    array-length v0, p0

    invoke-static {v0}, Lidd;->c(I)I

    move-result v0

    array-length v1, p0

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 798
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lidj;->a(II)I

    move-result v0

    invoke-static {v0}, Lidd;->c(I)I

    move-result v0

    return v0
.end method

.method public static b(ILidh;)I
    .locals 3

    .prologue
    .line 457
    invoke-static {p0}, Lidd;->b(I)I

    move-result v0

    invoke-virtual {p1}, Lidh;->a()I

    move-result v1

    invoke-static {v1}, Lidd;->c(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILjava/lang/String;)I
    .locals 2

    .prologue
    .line 439
    invoke-static {p0}, Lidd;->b(I)I

    move-result v0

    invoke-static {p1}, Lidd;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(I[B)I
    .locals 2

    .prologue
    .line 466
    invoke-static {p0}, Lidd;->b(I)I

    move-result v0

    invoke-static {p1}, Lidd;->a([B)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private b(J)V
    .locals 5

    .prologue
    .line 833
    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 834
    long-to-int v0, p1

    invoke-direct {p0, v0}, Lidd;->d(I)V

    .line 835
    return-void

    .line 837
    :cond_0
    long-to-int v0, p1

    and-int/lit8 v0, v0, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Lidd;->d(I)V

    .line 838
    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0
.end method

.method public static c(I)I
    .locals 1

    .prologue
    .line 823
    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 827
    :goto_0
    return v0

    .line 824
    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 825
    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    .line 826
    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    .line 827
    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public static c(II)I
    .locals 2

    .prologue
    .line 403
    invoke-static {p0}, Lidd;->b(I)I

    move-result v0

    invoke-static {p1}, Lidd;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static c(IJ)I
    .locals 3

    .prologue
    .line 387
    invoke-static {p0}, Lidd;->b(I)I

    move-result v0

    invoke-static {p1, p2}, Lidd;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static d(II)I
    .locals 2

    .prologue
    .line 483
    invoke-static {p0}, Lidd;->b(I)I

    move-result v0

    invoke-static {p1}, Lidd;->c(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static d(IJ)I
    .locals 3

    .prologue
    .line 395
    invoke-static {p0}, Lidd;->b(I)I

    move-result v0

    invoke-static {p1, p2}, Lidd;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private d(I)V
    .locals 4

    .prologue
    .line 769
    int-to-byte v0, p1

    iget v1, p0, Lidd;->b:I

    iget v2, p0, Lidd;->a:I

    if-ne v1, v2, :cond_0

    new-instance v0, Lide;

    iget v1, p0, Lidd;->b:I

    iget v2, p0, Lidd;->a:I

    invoke-direct {v0, v1, v2}, Lide;-><init>(II)V

    throw v0

    :cond_0
    iget-object v1, p0, Lidd;->c:[B

    iget v2, p0, Lidd;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lidd;->b:I

    aput-byte v0, v1, v2

    .line 770
    return-void
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 807
    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    .line 808
    invoke-direct {p0, p1}, Lidd;->d(I)V

    .line 809
    return-void

    .line 811
    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Lidd;->d(I)V

    .line 812
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method


# virtual methods
.method public final a(ID)V
    .locals 4

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 90
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    long-to-int v2, v0

    and-int/lit16 v2, v2, 0xff

    invoke-direct {p0, v2}, Lidd;->d(I)V

    const/16 v2, 0x8

    shr-long v2, v0, v2

    long-to-int v2, v2

    and-int/lit16 v2, v2, 0xff

    invoke-direct {p0, v2}, Lidd;->d(I)V

    const/16 v2, 0x10

    shr-long v2, v0, v2

    long-to-int v2, v2

    and-int/lit16 v2, v2, 0xff

    invoke-direct {p0, v2}, Lidd;->d(I)V

    const/16 v2, 0x18

    shr-long v2, v0, v2

    long-to-int v2, v2

    and-int/lit16 v2, v2, 0xff

    invoke-direct {p0, v2}, Lidd;->d(I)V

    const/16 v2, 0x20

    shr-long v2, v0, v2

    long-to-int v2, v2

    and-int/lit16 v2, v2, 0xff

    invoke-direct {p0, v2}, Lidd;->d(I)V

    const/16 v2, 0x28

    shr-long v2, v0, v2

    long-to-int v2, v2

    and-int/lit16 v2, v2, 0xff

    invoke-direct {p0, v2}, Lidd;->d(I)V

    const/16 v2, 0x30

    shr-long v2, v0, v2

    long-to-int v2, v2

    and-int/lit16 v2, v2, 0xff

    invoke-direct {p0, v2}, Lidd;->d(I)V

    const/16 v2, 0x38

    shr-long/2addr v0, v2

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, Lidd;->d(I)V

    .line 91
    return-void
.end method

.method public final a(IF)V
    .locals 2

    .prologue
    .line 96
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 97
    invoke-static {p2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    and-int/lit16 v1, v0, 0xff

    invoke-direct {p0, v1}, Lidd;->d(I)V

    shr-int/lit8 v1, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-direct {p0, v1}, Lidd;->d(I)V

    shr-int/lit8 v1, v0, 0x10

    and-int/lit16 v1, v1, 0xff

    invoke-direct {p0, v1}, Lidd;->d(I)V

    ushr-int/lit8 v0, v0, 0x18

    invoke-direct {p0, v0}, Lidd;->d(I)V

    .line 98
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 118
    if-ltz p2, :cond_0

    invoke-direct {p0, p2}, Lidd;->e(I)V

    .line 119
    :goto_0
    return-void

    .line 118
    :cond_0
    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Lidd;->b(J)V

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 104
    invoke-direct {p0, p2, p3}, Lidd;->b(J)V

    .line 105
    return-void
.end method

.method public final a(ILidh;)V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 161
    iget v0, p2, Lidh;->J:I

    if-gez v0, :cond_0

    invoke-virtual {p2}, Lidh;->a()I

    :cond_0
    iget v0, p2, Lidh;->J:I

    invoke-direct {p0, v0}, Lidd;->e(I)V

    invoke-virtual {p2, p0}, Lidh;->a(Lidd;)V

    .line 162
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 145
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 146
    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-direct {p0, v1}, Lidd;->e(I)V

    invoke-virtual {p0, v0}, Lidd;->b([B)V

    .line 147
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 138
    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 139
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lidd;->d(I)V

    .line 140
    return-void
.end method

.method public final a(I[B)V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 168
    array-length v0, p2

    invoke-direct {p0, v0}, Lidd;->e(I)V

    invoke-virtual {p0, p2}, Lidd;->b([B)V

    .line 169
    return-void
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 183
    invoke-direct {p0, p2}, Lidd;->e(I)V

    .line 184
    return-void
.end method

.method public final b(IJ)V
    .locals 2

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lidd;->e(II)V

    .line 111
    invoke-direct {p0, p2, p3}, Lidd;->b(J)V

    .line 112
    return-void
.end method

.method public final b([B)V
    .locals 4

    .prologue
    .line 774
    array-length v0, p1

    iget v1, p0, Lidd;->a:I

    iget v2, p0, Lidd;->b:I

    sub-int/2addr v1, v2

    if-lt v1, v0, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, Lidd;->c:[B

    iget v3, p0, Lidd;->b:I

    invoke-static {p1, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lidd;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lidd;->b:I

    return-void

    :cond_0
    new-instance v0, Lide;

    iget v1, p0, Lidd;->b:I

    iget v2, p0, Lidd;->a:I

    invoke-direct {v0, v1, v2}, Lide;-><init>(II)V

    throw v0
.end method

.method public final e(II)V
    .locals 1

    .prologue
    .line 793
    invoke-static {p1, p2}, Lidj;->a(II)I

    move-result v0

    invoke-direct {p0, v0}, Lidd;->e(I)V

    .line 794
    return-void
.end method
