.class public final Lgkq;
.super Lxe;
.source "SourceFile"


# instance fields
.field private final a:Lghr;

.field private final b:Lezj;


# direct methods
.method public constructor <init>(Lxi;Lghr;Lezj;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lxe;-><init>(Lxi;)V

    .line 29
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghr;

    iput-object v0, p0, Lgkq;->a:Lghr;

    .line 30
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgkq;->b:Lezj;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Lwp;)Lwm;
    .locals 9

    .prologue
    .line 39
    instance-of v0, p1, Lgkx;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 40
    check-cast v0, Lgkx;

    .line 43
    iget-object v1, p0, Lgkq;->a:Lghr;

    invoke-interface {v1}, Lghr;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    invoke-virtual {v0}, Lgkx;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->d(Ljava/lang/String;)V

    .line 48
    :cond_0
    iget-object v1, p0, Lgkq;->b:Lezj;

    invoke-virtual {v1}, Lezj;->b()J

    move-result-wide v2

    .line 49
    invoke-super {p0, p1}, Lxe;->a(Lwp;)Lwm;

    move-result-object v1

    .line 50
    iget-object v4, p0, Lgkq;->b:Lezj;

    invoke-virtual {v4}, Lezj;->b()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 53
    iget-object v4, p0, Lgkq;->a:Lghr;

    invoke-interface {v4}, Lghr;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 54
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Response for %s took %d ms and had status code %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 58
    invoke-virtual {v0}, Lgkx;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 59
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v2, 0x2

    iget v3, v1, Lwm;->a:I

    .line 60
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    .line 55
    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 54
    invoke-static {v2}, Lezp;->d(Ljava/lang/String;)V

    .line 64
    :cond_1
    iget-object v2, p0, Lgkq;->a:Lghr;

    invoke-interface {v2}, Lghr;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    const-string v2, "Logging response for YouTube API call."

    invoke-static {v2}, Lezp;->d(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v0, v1}, Lgkx;->b(Lwm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 67
    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 72
    :goto_1
    return-object v0

    :cond_3
    invoke-super {p0, p1}, Lxe;->a(Lwp;)Lwm;

    move-result-object v0

    goto :goto_1
.end method
