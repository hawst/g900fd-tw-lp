.class public final Lub;
.super Lnm;
.source "SourceFile"

# interfaces
.implements Lel;


# instance fields
.field f:Landroid/view/View;

.field public g:I

.field public h:Z

.field public i:Z

.field j:Luh;

.field k:Luc;

.field public l:Lue;

.field final m:Lui;

.field private n:Z

.field private o:Z

.field private p:I

.field private q:I

.field private r:Z

.field private s:Z

.field private t:I

.field private final u:Landroid/util/SparseBooleanArray;

.field private v:Landroid/view/View;

.field private w:Lud;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 86
    const v0, 0x7f040004

    const v1, 0x7f040003

    invoke-direct {p0, p1, v0, v1}, Lnm;-><init>(Landroid/content/Context;II)V

    .line 72
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lub;->u:Landroid/util/SparseBooleanArray;

    .line 82
    new-instance v0, Lui;

    invoke-direct {v0, p0}, Lui;-><init>(Lub;)V

    iput-object v0, p0, Lub;->m:Lui;

    .line 87
    return-void
.end method


# virtual methods
.method public final a(Lnv;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p1}, Lnv;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lnv;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lnm;->a(Lnv;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 172
    :cond_1
    invoke-virtual {p1}, Lnv;->isActionViewExpanded()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 174
    check-cast p3, Landroid/support/v7/widget/ActionMenuView;

    .line 175
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 176
    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 177
    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)Lul;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    :cond_2
    return-object v0

    .line 172
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;)Loi;
    .locals 2

    .prologue
    .line 161
    invoke-super {p0, p1}, Lnm;->a(Landroid/view/ViewGroup;)Loi;

    move-result-object v1

    move-object v0, v1

    .line 162
    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ActionMenuView;->a(Lub;)V

    .line 163
    return-object v1
.end method

.method public final a(Landroid/content/Context;Lnr;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 91
    invoke-super {p0, p1, p2}, Lnm;->a(Landroid/content/Context;Lnr;)V

    .line 93
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 95
    invoke-static {p1}, Lna;->a(Landroid/content/Context;)Lna;

    move-result-object v3

    .line 96
    iget-boolean v4, p0, Lub;->o:Z

    if-nez v4, :cond_1

    .line 97
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v4, v5, :cond_5

    :cond_0
    :goto_0
    iput-boolean v0, p0, Lub;->n:Z

    .line 100
    :cond_1
    iget-boolean v0, p0, Lub;->s:Z

    if-nez v0, :cond_2

    .line 101
    iget-object v0, v3, Lna;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lub;->p:I

    .line 105
    :cond_2
    iget-boolean v0, p0, Lub;->h:Z

    if-nez v0, :cond_3

    .line 106
    iget-object v0, v3, Lna;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0005

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lub;->g:I

    .line 109
    :cond_3
    iget v0, p0, Lub;->p:I

    .line 110
    iget-boolean v3, p0, Lub;->n:Z

    if-eqz v3, :cond_6

    .line 111
    iget-object v3, p0, Lub;->f:Landroid/view/View;

    if-nez v3, :cond_4

    .line 112
    new-instance v3, Luf;

    iget-object v4, p0, Lub;->a:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Luf;-><init>(Lub;Landroid/content/Context;)V

    iput-object v3, p0, Lub;->f:Landroid/view/View;

    .line 113
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 114
    iget-object v3, p0, Lub;->f:Landroid/view/View;

    invoke-virtual {v3, v1, v1}, Landroid/view/View;->measure(II)V

    .line 116
    :cond_4
    iget-object v1, p0, Lub;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 121
    :goto_1
    iput v0, p0, Lub;->q:I

    .line 123
    const/high16 v0, 0x42600000    # 56.0f

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lub;->t:I

    .line 126
    iput-object v6, p0, Lub;->v:Landroid/view/View;

    .line 127
    return-void

    .line 97
    :cond_5
    iget-object v4, v3, Lna;->a:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-static {v4}, Lgl;->b(Landroid/view/ViewConfiguration;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    .line 118
    :cond_6
    iput-object v6, p0, Lub;->f:Landroid/view/View;

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/widget/ActionMenuView;)V
    .locals 1

    .prologue
    .line 548
    iput-object p1, p0, Lub;->e:Loi;

    .line 549
    iget-object v0, p0, Lub;->c:Lnr;

    iput-object v0, p1, Landroid/support/v7/widget/ActionMenuView;->a:Lnr;

    .line 550
    return-void
.end method

.method public final a(Lnr;Z)V
    .locals 0

    .prologue
    .line 514
    invoke-virtual {p0}, Lub;->d()Z

    .line 515
    invoke-super {p0, p1, p2}, Lnm;->a(Lnr;Z)V

    .line 516
    return-void
.end method

.method public final a(Lnv;Loj;)V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Loj;->a(Lnv;I)V

    .line 186
    iget-object v0, p0, Lub;->e:Loi;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    .line 187
    check-cast p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    .line 188
    iput-object v0, p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:Lnt;

    .line 190
    iget-object v0, p0, Lub;->w:Lud;

    if-nez v0, :cond_0

    .line 191
    new-instance v0, Lud;

    invoke-direct {v0, p0}, Lud;-><init>(Lub;)V

    iput-object v0, p0, Lub;->w:Lud;

    .line 193
    :cond_0
    iget-object v0, p0, Lub;->w:Lud;

    iput-object v0, p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b:Lnl;

    .line 194
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 539
    if-eqz p1, :cond_0

    .line 541
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lnm;->a(Lol;)Z

    .line 545
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-object v0, p0, Lub;->c:Lnr;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lnr;->a(Z)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 21

    .prologue
    .line 379
    move-object/from16 v0, p0

    iget-object v2, v0, Lub;->c:Lnr;

    invoke-virtual {v2}, Lnr;->h()Ljava/util/ArrayList;

    move-result-object v13

    .line 380
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 381
    move-object/from16 v0, p0

    iget v7, v0, Lub;->g:I

    .line 382
    move-object/from16 v0, p0

    iget v9, v0, Lub;->q:I

    .line 383
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 384
    move-object/from16 v0, p0

    iget-object v2, v0, Lub;->e:Loi;

    check-cast v2, Landroid/view/ViewGroup;

    .line 386
    const/4 v6, 0x0

    .line 387
    const/4 v5, 0x0

    .line 388
    const/4 v8, 0x0

    .line 389
    const/4 v4, 0x0

    .line 390
    const/4 v3, 0x0

    move v10, v3

    :goto_0
    if-ge v10, v14, :cond_2

    .line 391
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnv;

    .line 392
    invoke-virtual {v3}, Lnv;->h()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 393
    add-int/lit8 v6, v6, 0x1

    .line 399
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lub;->i:Z

    if-eqz v11, :cond_1e

    invoke-virtual {v3}, Lnv;->isActionViewExpanded()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 402
    const/4 v3, 0x0

    .line 390
    :goto_2
    add-int/lit8 v7, v10, 0x1

    move v10, v7

    move v7, v3

    goto :goto_0

    .line 394
    :cond_0
    invoke-virtual {v3}, Lnv;->g()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 395
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 397
    :cond_1
    const/4 v4, 0x1

    goto :goto_1

    .line 407
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lub;->n:Z

    if-eqz v3, :cond_4

    if-nez v4, :cond_3

    add-int v3, v6, v5

    if-le v3, v7, :cond_4

    .line 409
    :cond_3
    add-int/lit8 v7, v7, -0x1

    .line 411
    :cond_4
    sub-int v10, v7, v6

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lub;->u:Landroid/util/SparseBooleanArray;

    move-object/from16 v16, v0

    .line 414
    invoke-virtual/range {v16 .. v16}, Landroid/util/SparseBooleanArray;->clear()V

    .line 416
    const/4 v4, 0x0

    .line 417
    const/4 v3, 0x0

    .line 418
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lub;->r:Z

    if-eqz v5, :cond_1d

    .line 419
    move-object/from16 v0, p0

    iget v3, v0, Lub;->t:I

    div-int v3, v9, v3

    .line 420
    move-object/from16 v0, p0

    iget v4, v0, Lub;->t:I

    rem-int v4, v9, v4

    .line 421
    move-object/from16 v0, p0

    iget v5, v0, Lub;->t:I

    div-int/2addr v4, v3

    add-int/2addr v4, v5

    move v5, v4

    .line 425
    :goto_3
    const/4 v4, 0x0

    move v12, v4

    move v7, v8

    move v4, v3

    :goto_4
    if-ge v12, v14, :cond_17

    .line 426
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnv;

    .line 428
    invoke-virtual {v3}, Lnv;->h()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 429
    move-object/from16 v0, p0

    iget-object v6, v0, Lub;->v:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6, v2}, Lub;->a(Lnv;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 430
    move-object/from16 v0, p0

    iget-object v8, v0, Lub;->v:Landroid/view/View;

    if-nez v8, :cond_5

    .line 431
    move-object/from16 v0, p0

    iput-object v6, v0, Lub;->v:Landroid/view/View;

    .line 433
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lub;->r:Z

    if-eqz v8, :cond_7

    .line 434
    const/4 v8, 0x0

    invoke-static {v6, v5, v4, v15, v8}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v8

    sub-int/2addr v4, v8

    .line 439
    :goto_5
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 440
    sub-int v8, v9, v6

    .line 441
    if-nez v7, :cond_1c

    .line 444
    :goto_6
    invoke-virtual {v3}, Lnv;->getGroupId()I

    move-result v7

    .line 445
    if-eqz v7, :cond_6

    .line 446
    const/4 v9, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v9}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 448
    :cond_6
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lnv;->d(Z)V

    move v3, v8

    move v7, v10

    .line 425
    :goto_7
    add-int/lit8 v8, v12, 0x1

    move v12, v8

    move v9, v3

    move v10, v7

    move v7, v6

    goto :goto_4

    .line 437
    :cond_7
    invoke-virtual {v6, v15, v15}, Landroid/view/View;->measure(II)V

    goto :goto_5

    .line 449
    :cond_8
    invoke-virtual {v3}, Lnv;->g()Z

    move-result v6

    if-eqz v6, :cond_16

    .line 452
    invoke-virtual {v3}, Lnv;->getGroupId()I

    move-result v17

    .line 453
    invoke-virtual/range {v16 .. v17}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v18

    .line 454
    if-gtz v10, :cond_9

    if-eqz v18, :cond_e

    :cond_9
    if-lez v9, :cond_e

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lub;->r:Z

    if-eqz v6, :cond_a

    if-lez v4, :cond_e

    :cond_a
    const/4 v6, 0x1

    .line 457
    :goto_8
    if-eqz v6, :cond_1b

    .line 458
    move-object/from16 v0, p0

    iget-object v8, v0, Lub;->v:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8, v2}, Lub;->a(Lnv;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 459
    move-object/from16 v0, p0

    iget-object v8, v0, Lub;->v:Landroid/view/View;

    if-nez v8, :cond_b

    .line 460
    move-object/from16 v0, p0

    iput-object v11, v0, Lub;->v:Landroid/view/View;

    .line 462
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lub;->r:Z

    if-eqz v8, :cond_f

    .line 463
    const/4 v8, 0x0

    invoke-static {v11, v5, v4, v15, v8}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v19

    .line 465
    sub-int v8, v4, v19

    .line 466
    if-nez v19, :cond_1a

    .line 467
    const/4 v4, 0x0

    :goto_9
    move v6, v8

    .line 472
    :goto_a
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 473
    sub-int/2addr v9, v8

    .line 474
    if-nez v7, :cond_c

    move v7, v8

    .line 478
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lub;->r:Z

    if-eqz v8, :cond_11

    .line 479
    if-ltz v9, :cond_10

    const/4 v8, 0x1

    :goto_b
    and-int/2addr v4, v8

    move v11, v4

    move v8, v7

    move v7, v6

    .line 486
    :goto_c
    if-eqz v11, :cond_13

    if-eqz v17, :cond_13

    .line 487
    const/4 v4, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    move v4, v10

    .line 501
    :goto_d
    if-eqz v11, :cond_d

    add-int/lit8 v4, v4, -0x1

    .line 503
    :cond_d
    invoke-virtual {v3, v11}, Lnv;->d(Z)V

    move v6, v8

    move v3, v9

    move/from16 v20, v7

    move v7, v4

    move/from16 v4, v20

    .line 504
    goto :goto_7

    .line 454
    :cond_e
    const/4 v6, 0x0

    goto :goto_8

    .line 470
    :cond_f
    invoke-virtual {v11, v15, v15}, Landroid/view/View;->measure(II)V

    move/from16 v20, v6

    move v6, v4

    move/from16 v4, v20

    goto :goto_a

    .line 479
    :cond_10
    const/4 v8, 0x0

    goto :goto_b

    .line 482
    :cond_11
    add-int v8, v9, v7

    if-lez v8, :cond_12

    const/4 v8, 0x1

    :goto_e
    and-int/2addr v4, v8

    move v11, v4

    move v8, v7

    move v7, v6

    goto :goto_c

    :cond_12
    const/4 v8, 0x0

    goto :goto_e

    .line 488
    :cond_13
    if-eqz v18, :cond_19

    .line 490
    const/4 v4, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 491
    const/4 v4, 0x0

    move v6, v10

    move v10, v4

    :goto_f
    if-ge v10, v12, :cond_18

    .line 492
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnv;

    .line 493
    invoke-virtual {v4}, Lnv;->getGroupId()I

    move-result v18

    move/from16 v0, v18

    move/from16 v1, v17

    if-ne v0, v1, :cond_15

    .line 495
    invoke-virtual {v4}, Lnv;->f()Z

    move-result v18

    if-eqz v18, :cond_14

    add-int/lit8 v6, v6, 0x1

    .line 496
    :cond_14
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Lnv;->d(Z)V

    .line 491
    :cond_15
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_f

    .line 506
    :cond_16
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lnv;->d(Z)V

    move v6, v7

    move v3, v9

    move v7, v10

    goto/16 :goto_7

    .line 509
    :cond_17
    const/4 v2, 0x1

    return v2

    :cond_18
    move v4, v6

    goto :goto_d

    :cond_19
    move v4, v10

    goto :goto_d

    :cond_1a
    move v4, v6

    goto/16 :goto_9

    :cond_1b
    move v11, v6

    move v8, v7

    move v7, v4

    goto :goto_c

    :cond_1c
    move v6, v7

    goto/16 :goto_6

    :cond_1d
    move v5, v4

    goto/16 :goto_3

    :cond_1e
    move v3, v7

    goto/16 :goto_2
.end method

.method public final a(Landroid/view/ViewGroup;I)Z
    .locals 2

    .prologue
    .line 256
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lub;->f:Landroid/view/View;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 257
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lnm;->a(Landroid/view/ViewGroup;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lnv;)Z
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p1}, Lnv;->f()Z

    move-result v0

    return v0
.end method

.method public final a(Lol;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 261
    invoke-virtual {p1}, Lol;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v3

    .line 278
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 264
    :goto_1
    iget-object v1, v0, Lol;->j:Lnr;

    iget-object v2, p0, Lub;->c:Lnr;

    if-eq v1, v2, :cond_1

    .line 265
    iget-object v0, v0, Lol;->j:Lnr;

    check-cast v0, Lol;

    goto :goto_1

    .line 267
    :cond_1
    invoke-virtual {v0}, Lol;->getItem()Landroid/view/MenuItem;

    move-result-object v5

    iget-object v0, p0, Lub;->e:Loi;

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_3

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v1, v2, Loj;

    if-eqz v1, :cond_2

    move-object v1, v2

    check-cast v1, Loj;

    invoke-interface {v1}, Loj;->a()Lnv;

    move-result-object v1

    if-ne v1, v5, :cond_2

    move-object v0, v2

    .line 268
    :goto_3
    if-nez v0, :cond_5

    .line 269
    iget-object v0, p0, Lub;->f:Landroid/view/View;

    if-nez v0, :cond_4

    move v0, v3

    goto :goto_0

    .line 267
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 270
    :cond_4
    iget-object v0, p0, Lub;->f:Landroid/view/View;

    .line 273
    :cond_5
    invoke-virtual {p1}, Lol;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    .line 274
    new-instance v1, Luc;

    iget-object v2, p0, Lub;->b:Landroid/content/Context;

    invoke-direct {v1, p0, v2, p1}, Luc;-><init>(Lub;Landroid/content/Context;Lol;)V

    iput-object v1, p0, Lub;->k:Luc;

    .line 275
    iget-object v1, p0, Lub;->k:Luc;

    iput-object v0, v1, Loe;->b:Landroid/view/View;

    .line 276
    iget-object v0, p0, Lub;->k:Luc;

    invoke-virtual {v0}, Loe;->b()Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MenuPopupHelper cannot be used without an anchor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_6
    invoke-super {p0, p1}, Lnm;->a(Lol;)Z

    .line 278
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lub;->e:Loi;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 204
    invoke-super {p0, p1}, Lnm;->b(Z)V

    .line 209
    iget-object v0, p0, Lub;->e:Loi;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 211
    iget-object v0, p0, Lub;->c:Lnr;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lub;->c:Lnr;

    invoke-virtual {v0}, Lnr;->i()V

    iget-object v4, v0, Lnr;->d:Ljava/util/ArrayList;

    .line 213
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 214
    :goto_0
    if-ge v3, v5, :cond_1

    .line 215
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnv;

    iget-object v0, v0, Lnv;->d:Lek;

    .line 216
    if-eqz v0, :cond_0

    .line 217
    iput-object p0, v0, Lek;->b:Lel;

    .line 214
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 222
    :cond_1
    iget-object v0, p0, Lub;->c:Lnr;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lub;->c:Lnr;

    invoke-virtual {v0}, Lnr;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 226
    :goto_1
    iget-boolean v3, p0, Lub;->n:Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 227
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 228
    if-ne v3, v1, :cond_8

    .line 229
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnv;

    invoke-virtual {v0}, Lnv;->isActionViewExpanded()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    move v2, v0

    .line 235
    :cond_2
    :goto_3
    if-eqz v2, :cond_a

    .line 236
    iget-object v0, p0, Lub;->f:Landroid/view/View;

    if-nez v0, :cond_3

    .line 237
    new-instance v0, Luf;

    iget-object v1, p0, Lub;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Luf;-><init>(Lub;Landroid/content/Context;)V

    iput-object v0, p0, Lub;->f:Landroid/view/View;

    .line 239
    :cond_3
    iget-object v0, p0, Lub;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 240
    iget-object v1, p0, Lub;->e:Loi;

    if-eq v0, v1, :cond_5

    .line 241
    if-eqz v0, :cond_4

    .line 242
    iget-object v1, p0, Lub;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 244
    :cond_4
    iget-object v0, p0, Lub;->e:Loi;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    .line 245
    iget-object v1, p0, Lub;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->a()Lul;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    :cond_5
    :goto_4
    iget-object v0, p0, Lub;->e:Loi;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iget-boolean v1, p0, Lub;->n:Z

    iput-boolean v1, v0, Landroid/support/v7/widget/ActionMenuView;->b:Z

    .line 252
    return-void

    .line 222
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    move v0, v2

    .line 229
    goto :goto_2

    .line 231
    :cond_8
    if-lez v3, :cond_9

    :goto_5
    move v2, v1

    goto :goto_3

    :cond_9
    move v1, v2

    goto :goto_5

    .line 247
    :cond_a
    iget-object v0, p0, Lub;->f:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lub;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lub;->e:Loi;

    if-ne v0, v1, :cond_5

    .line 248
    iget-object v0, p0, Lub;->e:Loi;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lub;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 301
    iget-boolean v0, p0, Lub;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lub;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lub;->c:Lnr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lub;->e:Loi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lub;->l:Lue;

    if-nez v0, :cond_0

    iget-object v0, p0, Lub;->c:Lnr;

    invoke-virtual {v0}, Lnr;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    new-instance v0, Luh;

    iget-object v2, p0, Lub;->b:Landroid/content/Context;

    iget-object v3, p0, Lub;->c:Lnr;

    iget-object v4, p0, Lub;->f:Landroid/view/View;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Luh;-><init>(Lub;Landroid/content/Context;Lnr;Landroid/view/View;Z)V

    .line 304
    new-instance v1, Lue;

    invoke-direct {v1, p0, v0}, Lue;-><init>(Lub;Luh;)V

    iput-object v1, p0, Lub;->l:Lue;

    .line 306
    iget-object v0, p0, Lub;->e:Loi;

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lub;->l:Lue;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 310
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lnm;->a(Lol;)Z

    .line 314
    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 146
    iput-boolean v0, p0, Lub;->n:Z

    .line 147
    iput-boolean v0, p0, Lub;->o:Z

    .line 148
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 323
    iget-object v0, p0, Lub;->l:Lue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lub;->e:Loi;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lub;->e:Loi;

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lub;->l:Lue;

    invoke-virtual {v0, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 325
    const/4 v0, 0x0

    iput-object v0, p0, Lub;->l:Lue;

    move v0, v1

    .line 334
    :goto_0
    return v0

    .line 329
    :cond_0
    iget-object v0, p0, Lub;->j:Luh;

    .line 330
    if-eqz v0, :cond_1

    .line 331
    invoke-virtual {v0}, Loe;->c()V

    move v0, v1

    .line 332
    goto :goto_0

    .line 334
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 342
    invoke-virtual {p0}, Lub;->c()Z

    move-result v0

    .line 343
    invoke-virtual {p0}, Lub;->e()Z

    move-result v1

    or-int/2addr v0, v1

    .line 344
    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lub;->k:Luc;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lub;->k:Luc;

    invoke-virtual {v0}, Luc;->c()V

    .line 355
    const/4 v0, 0x1

    .line 357
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lub;->j:Luh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lub;->j:Luh;

    invoke-virtual {v0}, Luh;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
