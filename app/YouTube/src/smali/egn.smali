.class public final Legn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefb;


# instance fields
.field private final a:Lefb;

.field private final b:[B

.field private final c:[B

.field private d:Legp;


# direct methods
.method public constructor <init>([B[BLefb;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p3, p0, Legn;->a:Lefb;

    .line 44
    iput-object p1, p0, Legn;->b:[B

    .line 45
    iput-object p2, p0, Legn;->c:[B

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Lefg;)Lefb;
    .locals 8

    .prologue
    .line 50
    iget-object v0, p0, Legn;->a:Lefb;

    invoke-interface {v0, p1}, Lefb;->a(Lefg;)Lefb;

    .line 51
    iget-object v0, p1, Lefg;->f:Ljava/lang/String;

    invoke-static {v0}, La;->l(Ljava/lang/String;)J

    move-result-wide v4

    .line 52
    new-instance v1, Legp;

    const/4 v2, 0x1

    iget-object v3, p0, Legn;->b:[B

    iget-wide v6, p1, Lefg;->c:J

    invoke-direct/range {v1 .. v7}, Legp;-><init>(I[BJJ)V

    iput-object v1, p0, Legn;->d:Legp;

    .line 54
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Legn;->d:Legp;

    .line 78
    iget-object v0, p0, Legn;->a:Lefb;

    invoke-interface {v0}, Lefb;->a()V

    .line 79
    return-void
.end method

.method public final a([BII)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 59
    iget-object v0, p0, Legn;->c:[B

    if-nez v0, :cond_1

    .line 61
    iget-object v0, p0, Legn;->d:Legp;

    invoke-virtual {v0, p1, p2, p3}, Legp;->a([BII)V

    .line 62
    iget-object v0, p0, Legn;->a:Lefb;

    invoke-interface {v0, p1, p2, p3}, Lefb;->a([BII)V

    .line 73
    :cond_0
    return-void

    :cond_1
    move v6, v5

    .line 66
    :goto_0
    if-ge v6, p3, :cond_0

    .line 67
    sub-int v0, p3, v6

    iget-object v1, p0, Legn;->c:[B

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 68
    iget-object v0, p0, Legn;->d:Legp;

    add-int v2, p2, v6

    iget-object v4, p0, Legn;->c:[B

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Legp;->a([BII[BI)V

    .line 69
    iget-object v0, p0, Legn;->a:Lefb;

    iget-object v1, p0, Legn;->c:[B

    invoke-interface {v0, v1, v5, v3}, Lefb;->a([BII)V

    .line 70
    add-int v0, v6, v3

    move v6, v0

    .line 71
    goto :goto_0
.end method
