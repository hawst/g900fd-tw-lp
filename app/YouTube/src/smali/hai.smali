.class public final Lhai;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhai;


# instance fields
.field private b:Lhag;

.field private c:Lhae;

.field private d:Lhah;

.field private e:Lhaf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11024
    const/4 v0, 0x0

    new-array v0, v0, [Lhai;

    sput-object v0, Lhai;->a:[Lhai;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11025
    invoke-direct {p0}, Lidf;-><init>()V

    .line 11028
    iput-object v0, p0, Lhai;->b:Lhag;

    .line 11031
    iput-object v0, p0, Lhai;->c:Lhae;

    .line 11034
    iput-object v0, p0, Lhai;->d:Lhah;

    .line 11037
    iput-object v0, p0, Lhai;->e:Lhaf;

    .line 11025
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 11092
    const/4 v0, 0x0

    .line 11093
    iget-object v1, p0, Lhai;->b:Lhag;

    if-eqz v1, :cond_0

    .line 11094
    const v0, 0x48c1324

    iget-object v1, p0, Lhai;->b:Lhag;

    .line 11095
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11097
    :cond_0
    iget-object v1, p0, Lhai;->c:Lhae;

    if-eqz v1, :cond_1

    .line 11098
    const v1, 0x49de31d

    iget-object v2, p0, Lhai;->c:Lhae;

    .line 11099
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11101
    :cond_1
    iget-object v1, p0, Lhai;->d:Lhah;

    if-eqz v1, :cond_2

    .line 11102
    const v1, 0x4a1f695

    iget-object v2, p0, Lhai;->d:Lhah;

    .line 11103
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11105
    :cond_2
    iget-object v1, p0, Lhai;->e:Lhaf;

    if-eqz v1, :cond_3

    .line 11106
    const v1, 0x4b1b53d

    iget-object v2, p0, Lhai;->e:Lhaf;

    .line 11107
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11109
    :cond_3
    iget-object v1, p0, Lhai;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11110
    iput v0, p0, Lhai;->J:I

    .line 11111
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 11021
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhai;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhai;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhai;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhai;->b:Lhag;

    if-nez v0, :cond_2

    new-instance v0, Lhag;

    invoke-direct {v0}, Lhag;-><init>()V

    iput-object v0, p0, Lhai;->b:Lhag;

    :cond_2
    iget-object v0, p0, Lhai;->b:Lhag;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhai;->c:Lhae;

    if-nez v0, :cond_3

    new-instance v0, Lhae;

    invoke-direct {v0}, Lhae;-><init>()V

    iput-object v0, p0, Lhai;->c:Lhae;

    :cond_3
    iget-object v0, p0, Lhai;->c:Lhae;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhai;->d:Lhah;

    if-nez v0, :cond_4

    new-instance v0, Lhah;

    invoke-direct {v0}, Lhah;-><init>()V

    iput-object v0, p0, Lhai;->d:Lhah;

    :cond_4
    iget-object v0, p0, Lhai;->d:Lhah;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhai;->e:Lhaf;

    if-nez v0, :cond_5

    new-instance v0, Lhaf;

    invoke-direct {v0}, Lhaf;-><init>()V

    iput-object v0, p0, Lhai;->e:Lhaf;

    :cond_5
    iget-object v0, p0, Lhai;->e:Lhaf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x24609922 -> :sswitch_1
        0x24ef18ea -> :sswitch_2
        0x250fb4aa -> :sswitch_3
        0x258da9ea -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 11074
    iget-object v0, p0, Lhai;->b:Lhag;

    if-eqz v0, :cond_0

    .line 11075
    const v0, 0x48c1324

    iget-object v1, p0, Lhai;->b:Lhag;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11077
    :cond_0
    iget-object v0, p0, Lhai;->c:Lhae;

    if-eqz v0, :cond_1

    .line 11078
    const v0, 0x49de31d

    iget-object v1, p0, Lhai;->c:Lhae;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11080
    :cond_1
    iget-object v0, p0, Lhai;->d:Lhah;

    if-eqz v0, :cond_2

    .line 11081
    const v0, 0x4a1f695

    iget-object v1, p0, Lhai;->d:Lhah;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11083
    :cond_2
    iget-object v0, p0, Lhai;->e:Lhaf;

    if-eqz v0, :cond_3

    .line 11084
    const v0, 0x4b1b53d

    iget-object v1, p0, Lhai;->e:Lhaf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11086
    :cond_3
    iget-object v0, p0, Lhai;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 11088
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11051
    if-ne p1, p0, :cond_1

    .line 11058
    :cond_0
    :goto_0
    return v0

    .line 11052
    :cond_1
    instance-of v2, p1, Lhai;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 11053
    :cond_2
    check-cast p1, Lhai;

    .line 11054
    iget-object v2, p0, Lhai;->b:Lhag;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhai;->b:Lhag;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhai;->c:Lhae;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhai;->c:Lhae;

    if-nez v2, :cond_3

    .line 11055
    :goto_2
    iget-object v2, p0, Lhai;->d:Lhah;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhai;->d:Lhah;

    if-nez v2, :cond_3

    .line 11056
    :goto_3
    iget-object v2, p0, Lhai;->e:Lhaf;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhai;->e:Lhaf;

    if-nez v2, :cond_3

    .line 11057
    :goto_4
    iget-object v2, p0, Lhai;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhai;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 11058
    goto :goto_0

    .line 11054
    :cond_4
    iget-object v2, p0, Lhai;->b:Lhag;

    iget-object v3, p1, Lhai;->b:Lhag;

    invoke-virtual {v2, v3}, Lhag;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhai;->c:Lhae;

    iget-object v3, p1, Lhai;->c:Lhae;

    .line 11055
    invoke-virtual {v2, v3}, Lhae;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhai;->d:Lhah;

    iget-object v3, p1, Lhai;->d:Lhah;

    .line 11056
    invoke-virtual {v2, v3}, Lhah;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhai;->e:Lhaf;

    iget-object v3, p1, Lhai;->e:Lhaf;

    .line 11057
    invoke-virtual {v2, v3}, Lhaf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhai;->I:Ljava/util/List;

    iget-object v3, p1, Lhai;->I:Ljava/util/List;

    .line 11058
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 11062
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 11064
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhai;->b:Lhag;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 11065
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhai;->c:Lhae;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 11066
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhai;->d:Lhah;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 11067
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhai;->e:Lhaf;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 11068
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhai;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 11069
    return v0

    .line 11064
    :cond_0
    iget-object v0, p0, Lhai;->b:Lhag;

    invoke-virtual {v0}, Lhag;->hashCode()I

    move-result v0

    goto :goto_0

    .line 11065
    :cond_1
    iget-object v0, p0, Lhai;->c:Lhae;

    invoke-virtual {v0}, Lhae;->hashCode()I

    move-result v0

    goto :goto_1

    .line 11066
    :cond_2
    iget-object v0, p0, Lhai;->d:Lhah;

    invoke-virtual {v0}, Lhah;->hashCode()I

    move-result v0

    goto :goto_2

    .line 11067
    :cond_3
    iget-object v0, p0, Lhai;->e:Lhaf;

    invoke-virtual {v0}, Lhaf;->hashCode()I

    move-result v0

    goto :goto_3

    .line 11068
    :cond_4
    iget-object v1, p0, Lhai;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
