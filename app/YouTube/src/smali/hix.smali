.class public final Lhix;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhix;


# instance fields
.field private b:I

.field private c:Lhwl;

.field private d:Ljava/lang/String;

.field private e:Lhip;

.field private f:[Lhgj;

.field private g:[Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lgyo;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhix;

    sput-object v0, Lhix;->a:[Lhix;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput v1, p0, Lhix;->b:I

    iput-object v2, p0, Lhix;->c:Lhwl;

    const-string v0, ""

    iput-object v0, p0, Lhix;->d:Ljava/lang/String;

    iput-object v2, p0, Lhix;->e:Lhip;

    sget-object v0, Lhgj;->a:[Lhgj;

    iput-object v0, p0, Lhix;->f:[Lhgj;

    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Lhix;->g:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhix;->h:Ljava/lang/String;

    iput-object v2, p0, Lhix;->i:Lgyo;

    const-string v0, ""

    iput-object v0, p0, Lhix;->j:Ljava/lang/String;

    iput v1, p0, Lhix;->k:I

    iput v1, p0, Lhix;->l:I

    iput v1, p0, Lhix;->m:I

    iput v1, p0, Lhix;->n:I

    iput-boolean v1, p0, Lhix;->o:Z

    iput-boolean v1, p0, Lhix;->p:Z

    const-string v0, ""

    iput-object v0, p0, Lhix;->q:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhix;->r:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget v0, p0, Lhix;->b:I

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    iget v2, p0, Lhix;->b:I

    invoke-static {v0, v2}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhix;->c:Lhwl;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhix;->c:Lhwl;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhix;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lhix;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhix;->e:Lhip;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lhix;->e:Lhip;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhix;->f:[Lhgj;

    if-eqz v2, :cond_4

    iget-object v3, p0, Lhix;->f:[Lhgj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    if-eqz v5, :cond_3

    const/4 v6, 0x5

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhix;->g:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lhix;->g:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    iget-object v3, p0, Lhix;->g:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    add-int/2addr v0, v2

    iget-object v1, p0, Lhix;->g:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhix;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x7

    iget-object v2, p0, Lhix;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lhix;->i:Lgyo;

    if-eqz v1, :cond_8

    const/16 v1, 0x8

    iget-object v2, p0, Lhix;->i:Lgyo;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lhix;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/16 v1, 0x9

    iget-object v2, p0, Lhix;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lhix;->k:I

    if-eqz v1, :cond_a

    const/16 v1, 0xa

    iget v2, p0, Lhix;->k:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lhix;->l:I

    if-eqz v1, :cond_b

    const/16 v1, 0xb

    iget v2, p0, Lhix;->l:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lhix;->m:I

    if-eqz v1, :cond_c

    const/16 v1, 0xc

    iget v2, p0, Lhix;->m:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Lhix;->n:I

    if-eqz v1, :cond_d

    const/16 v1, 0xd

    iget v2, p0, Lhix;->n:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-boolean v1, p0, Lhix;->o:Z

    if-eqz v1, :cond_e

    const/16 v1, 0xe

    iget-boolean v2, p0, Lhix;->o:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_e
    iget-boolean v1, p0, Lhix;->p:Z

    if-eqz v1, :cond_f

    const/16 v1, 0xf

    iget-boolean v2, p0, Lhix;->p:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Lhix;->q:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    const/16 v1, 0x10

    iget-object v2, p0, Lhix;->q:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-object v1, p0, Lhix;->r:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    const/16 v1, 0x11

    iget-object v2, p0, Lhix;->r:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget-object v1, p0, Lhix;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhix;->J:I

    return v0

    :cond_12
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhix;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhix;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhix;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    :cond_2
    iput v0, p0, Lhix;->b:I

    goto :goto_0

    :cond_3
    iput v1, p0, Lhix;->b:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhix;->c:Lhwl;

    if-nez v0, :cond_4

    new-instance v0, Lhwl;

    invoke-direct {v0}, Lhwl;-><init>()V

    iput-object v0, p0, Lhix;->c:Lhwl;

    :cond_4
    iget-object v0, p0, Lhix;->c:Lhwl;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhix;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhix;->e:Lhip;

    if-nez v0, :cond_5

    new-instance v0, Lhip;

    invoke-direct {v0}, Lhip;-><init>()V

    iput-object v0, p0, Lhix;->e:Lhip;

    :cond_5
    iget-object v0, p0, Lhix;->e:Lhip;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhix;->f:[Lhgj;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgj;

    iget-object v3, p0, Lhix;->f:[Lhgj;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhix;->f:[Lhgj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhix;->f:[Lhgj;

    :goto_2
    iget-object v2, p0, Lhix;->f:[Lhgj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhix;->f:[Lhgj;

    new-instance v3, Lhgj;

    invoke-direct {v3}, Lhgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhix;->f:[Lhgj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lhix;->f:[Lhgj;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Lhix;->f:[Lhgj;

    new-instance v3, Lhgj;

    invoke-direct {v3}, Lhgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhix;->f:[Lhgj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhix;->g:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lhix;->g:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lhix;->g:[Ljava/lang/String;

    :goto_3
    iget-object v2, p0, Lhix;->g:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lhix;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lhix;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhix;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhix;->i:Lgyo;

    if-nez v0, :cond_a

    new-instance v0, Lgyo;

    invoke-direct {v0}, Lgyo;-><init>()V

    iput-object v0, p0, Lhix;->i:Lgyo;

    :cond_a
    iget-object v0, p0, Lhix;->i:Lgyo;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhix;->j:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhix;->k:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhix;->l:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhix;->m:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhix;->n:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhix;->o:Z

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhix;->p:Z

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhix;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhix;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget v1, p0, Lhix;->b:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Lhix;->b:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    :cond_0
    iget-object v1, p0, Lhix;->c:Lhwl;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhix;->c:Lhwl;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v1, p0, Lhix;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhix;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lhix;->e:Lhip;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lhix;->e:Lhip;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v1, p0, Lhix;->f:[Lhgj;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lhix;->f:[Lhgj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    if-eqz v4, :cond_4

    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lhix;->g:[Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lhix;->g:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lhix;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x7

    iget-object v1, p0, Lhix;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lhix;->i:Lgyo;

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    iget-object v1, p0, Lhix;->i:Lgyo;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_8
    iget-object v0, p0, Lhix;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const/16 v0, 0x9

    iget-object v1, p0, Lhix;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_9
    iget v0, p0, Lhix;->k:I

    if-eqz v0, :cond_a

    const/16 v0, 0xa

    iget v1, p0, Lhix;->k:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_a
    iget v0, p0, Lhix;->l:I

    if-eqz v0, :cond_b

    const/16 v0, 0xb

    iget v1, p0, Lhix;->l:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_b
    iget v0, p0, Lhix;->m:I

    if-eqz v0, :cond_c

    const/16 v0, 0xc

    iget v1, p0, Lhix;->m:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_c
    iget v0, p0, Lhix;->n:I

    if-eqz v0, :cond_d

    const/16 v0, 0xd

    iget v1, p0, Lhix;->n:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_d
    iget-boolean v0, p0, Lhix;->o:Z

    if-eqz v0, :cond_e

    const/16 v0, 0xe

    iget-boolean v1, p0, Lhix;->o:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_e
    iget-boolean v0, p0, Lhix;->p:Z

    if-eqz v0, :cond_f

    const/16 v0, 0xf

    iget-boolean v1, p0, Lhix;->p:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_f
    iget-object v0, p0, Lhix;->q:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const/16 v0, 0x10

    iget-object v1, p0, Lhix;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_10
    iget-object v0, p0, Lhix;->r:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const/16 v0, 0x11

    iget-object v1, p0, Lhix;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_11
    iget-object v0, p0, Lhix;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhix;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhix;

    iget v2, p0, Lhix;->b:I

    iget v3, p1, Lhix;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhix;->c:Lhwl;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhix;->c:Lhwl;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhix;->d:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhix;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhix;->e:Lhip;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhix;->e:Lhip;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhix;->f:[Lhgj;

    iget-object v3, p1, Lhix;->f:[Lhgj;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhix;->g:[Ljava/lang/String;

    iget-object v3, p1, Lhix;->g:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhix;->h:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhix;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhix;->i:Lgyo;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhix;->i:Lgyo;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhix;->j:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhix;->j:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_6
    iget v2, p0, Lhix;->k:I

    iget v3, p1, Lhix;->k:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhix;->l:I

    iget v3, p1, Lhix;->l:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhix;->m:I

    iget v3, p1, Lhix;->m:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhix;->n:I

    iget v3, p1, Lhix;->n:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhix;->o:Z

    iget-boolean v3, p1, Lhix;->o:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhix;->p:Z

    iget-boolean v3, p1, Lhix;->p:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhix;->q:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhix;->q:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Lhix;->r:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhix;->r:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_8
    iget-object v2, p0, Lhix;->I:Ljava/util/List;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhix;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhix;->c:Lhwl;

    iget-object v3, p1, Lhix;->c:Lhwl;

    invoke-virtual {v2, v3}, Lhwl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhix;->d:Ljava/lang/String;

    iget-object v3, p1, Lhix;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhix;->e:Lhip;

    iget-object v3, p1, Lhix;->e:Lhip;

    invoke-virtual {v2, v3}, Lhip;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhix;->h:Ljava/lang/String;

    iget-object v3, p1, Lhix;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhix;->i:Lgyo;

    iget-object v3, p1, Lhix;->i:Lgyo;

    invoke-virtual {v2, v3}, Lgyo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhix;->j:Ljava/lang/String;

    iget-object v3, p1, Lhix;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhix;->q:Ljava/lang/String;

    iget-object v3, p1, Lhix;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lhix;->r:Ljava/lang/String;

    iget-object v3, p1, Lhix;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Lhix;->I:Ljava/util/List;

    iget-object v3, p1, Lhix;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhix;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhix;->c:Lhwl;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhix;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhix;->e:Lhip;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Lhix;->f:[Lhgj;

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lhix;->g:[Ljava/lang/String;

    if-nez v0, :cond_7

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhix;->h:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhix;->i:Lgyo;

    if-nez v0, :cond_a

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhix;->j:Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhix;->k:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhix;->l:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhix;->m:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhix;->n:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhix;->o:Z

    if-eqz v0, :cond_c

    move v0, v3

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lhix;->p:Z

    if-eqz v2, :cond_d

    :goto_7
    add-int/2addr v0, v3

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhix;->q:Ljava/lang/String;

    if-nez v0, :cond_e

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhix;->r:Ljava/lang/String;

    if-nez v0, :cond_f

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhix;->I:Ljava/util/List;

    if-nez v2, :cond_10

    :goto_a
    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v0, p0, Lhix;->c:Lhwl;

    invoke-virtual {v0}, Lhwl;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhix;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lhix;->e:Lhip;

    invoke-virtual {v0}, Lhip;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_5
    move v2, v0

    move v0, v1

    :goto_b
    iget-object v5, p0, Lhix;->f:[Lhgj;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhix;->f:[Lhgj;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    move v2, v1

    :goto_c
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_6
    iget-object v2, p0, Lhix;->f:[Lhgj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgj;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_7
    move v0, v1

    :goto_d
    iget-object v5, p0, Lhix;->g:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhix;->g:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_8

    move v2, v1

    :goto_e
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_8
    iget-object v2, p0, Lhix;->g:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_e

    :cond_9
    iget-object v0, p0, Lhix;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_a
    iget-object v0, p0, Lhix;->i:Lgyo;

    invoke-virtual {v0}, Lgyo;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_b
    iget-object v0, p0, Lhix;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_c
    move v0, v4

    goto/16 :goto_6

    :cond_d
    move v3, v4

    goto/16 :goto_7

    :cond_e
    iget-object v0, p0, Lhix;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_8

    :cond_f
    iget-object v0, p0, Lhix;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_10
    iget-object v1, p0, Lhix;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_a
.end method
