.class final Lglf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field final synthetic a:Ldzy;

.field final synthetic b:Lgld;


# direct methods
.method constructor <init>(Lgld;Ldzy;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lglf;->b:Lgld;

    iput-object p2, p0, Lglf;->a:Ldzy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lglf;->b:Lgld;

    invoke-static {v0}, Lgld;->e(Lgld;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lglg;

    invoke-direct {v1, p0, p1}, Lglg;-><init>(Lglf;Lxa;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 149
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 123
    iget-object v0, p0, Lglf;->b:Lgld;

    invoke-static {v0}, Lgld;->a(Lgld;)Lgll;

    move-result-object v0

    iget-object v1, p0, Lglf;->a:Ldzy;

    iget-object v1, v1, Ldzy;->i:Ljava/lang/String;

    iget-object v2, v0, Lgll;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "sent_requests_%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lgll;->b:Landroid/content/SharedPreferences;

    invoke-interface {v4, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v3, v0, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v4, "total_sent_requests"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "total_sent_requests"

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {v0, v1, v2}, Lgll;->a(Ljava/lang/String;Landroid/content/SharedPreferences$Editor;)V

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lglf;->b:Lgld;

    invoke-static {v0}, Lgld;->a(Lgld;)Lgll;

    move-result-object v0

    invoke-virtual {v0}, Lgll;->b()V

    return-void
.end method
