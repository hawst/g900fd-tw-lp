.class public final Lijl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lijm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    sget-object v0, Lijm;->a:Lijm;

    iput-object v0, p0, Lijl;->a:Lijm;

    .line 36
    return-void
.end method


# virtual methods
.method public a(Lijn;)[I
    .locals 5

    .prologue
    .line 144
    iget-object v0, p1, Lijn;->b:[I

    array-length v0, v0

    add-int/lit8 v2, v0, -0x1

    .line 145
    new-array v3, v2, [I

    .line 146
    const/4 v1, 0x0

    .line 147
    const/4 v0, 0x1

    :goto_0
    iget-object v4, p0, Lijl;->a:Lijm;

    iget v4, v4, Lijm;->f:I

    if-ge v0, v4, :cond_1

    if-ge v1, v2, :cond_1

    .line 148
    invoke-virtual {p1, v0}, Lijn;->b(I)I

    move-result v4

    if-nez v4, :cond_0

    .line 149
    iget-object v4, p0, Lijl;->a:Lijm;

    invoke-virtual {v4, v0}, Lijm;->a(I)I

    move-result v4

    aput v4, v3, v1

    .line 150
    add-int/lit8 v1, v1, 0x1

    .line 147
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    :cond_1
    if-eq v1, v2, :cond_2

    .line 154
    invoke-static {}, Lier;->a()Lier;

    move-result-object v0

    throw v0

    .line 156
    :cond_2
    return-object v3
.end method

.method public a(Lijn;Lijn;[I)[I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 162
    iget-object v0, p2, Lijn;->b:[I

    array-length v0, v0

    add-int/lit8 v2, v0, -0x1

    .line 163
    new-array v3, v2, [I

    .line 164
    const/4 v0, 0x1

    :goto_0
    if-gt v0, v2, :cond_0

    .line 165
    sub-int v4, v2, v0

    iget-object v5, p0, Lijl;->a:Lijm;

    invoke-virtual {p2, v0}, Lijn;->a(I)I

    move-result v6

    invoke-virtual {v5, v0, v6}, Lijm;->d(II)I

    move-result v5

    aput v5, v3, v4

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_0
    new-instance v2, Lijn;

    iget-object v0, p0, Lijl;->a:Lijm;

    invoke-direct {v2, v0, v3}, Lijn;-><init>(Lijm;[I)V

    .line 171
    array-length v3, p3

    .line 172
    new-array v4, v3, [I

    move v0, v1

    .line 173
    :goto_1
    if-ge v0, v3, :cond_1

    .line 174
    iget-object v5, p0, Lijl;->a:Lijm;

    aget v6, p3, v0

    invoke-virtual {v5, v6}, Lijm;->a(I)I

    move-result v5

    .line 175
    iget-object v6, p0, Lijl;->a:Lijm;

    invoke-virtual {p1, v5}, Lijn;->b(I)I

    move-result v7

    invoke-virtual {v6, v1, v7}, Lijm;->c(II)I

    move-result v6

    .line 176
    iget-object v7, p0, Lijl;->a:Lijm;

    invoke-virtual {v2, v5}, Lijn;->b(I)I

    move-result v5

    invoke-virtual {v7, v5}, Lijm;->a(I)I

    move-result v5

    .line 177
    iget-object v7, p0, Lijl;->a:Lijm;

    invoke-virtual {v7, v6, v5}, Lijm;->d(II)I

    move-result v5

    aput v5, v4, v0

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 179
    :cond_1
    return-object v4
.end method
