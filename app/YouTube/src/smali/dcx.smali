.class public final Ldcx;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Lddx;
.implements Ldec;


# instance fields
.field final a:Landroid/view/animation/Animation;

.field final b:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 30
    const/high16 v0, 0x10a0000

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcx;->a:Landroid/view/animation/Animation;

    .line 31
    const v0, 0x10a0001

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcx;->b:Landroid/view/animation/Animation;

    .line 32
    iget-object v0, p0, Ldcx;->b:Landroid/view/animation/Animation;

    new-instance v1, Ldcy;

    invoke-direct {v1, p0}, Ldcy;-><init>(Ldcx;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 48
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Ldcx;->setBackgroundColor(I)V

    .line 49
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ldcx;->setVisibility(I)V

    .line 50
    return-void
.end method


# virtual methods
.method public final a_(Z)V
    .locals 1

    .prologue
    .line 64
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 65
    return-void

    .line 64
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Ldcx;->clearAnimation()V

    .line 89
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ldcx;->setVisibility(I)V

    .line 90
    return-void
.end method

.method public final c_(Z)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public final f_()Landroid/view/View;
    .locals 0

    .prologue
    .line 59
    return-object p0
.end method

.method public final r_()Ldeg;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 54
    new-instance v0, Ldeg;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Ldeg;-><init>(IIZ)V

    return-object v0
.end method
