.class public final Lcns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lfrl;

.field private synthetic b:Ljava/lang/String;

.field private synthetic c:Leuc;

.field private synthetic d:Lcnq;


# direct methods
.method public constructor <init>(Lcnq;Lfrl;Ljava/lang/String;Leuc;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcns;->d:Lcnq;

    iput-object p2, p0, Lcns;->a:Lfrl;

    iput-object p3, p0, Lcns;->b:Ljava/lang/String;

    iput-object p4, p0, Lcns;->c:Leuc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 24

    .prologue
    .line 201
    new-instance v11, Lfai;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcns;->d:Lcnq;

    invoke-static {v4}, Lcnq;->a(Lcnq;)Lezj;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcns;->d:Lcnq;

    iget-wide v6, v5, Lcnq;->c:J

    invoke-direct {v11, v4, v6, v7}, Lfai;-><init>(Lezj;J)V

    .line 202
    move-object/from16 v0, p0

    iget-object v4, v0, Lcns;->a:Lfrl;

    iget-object v4, v4, Lfrl;->a:Lhro;

    invoke-static {v4}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v12

    .line 203
    move-object/from16 v0, p0

    iget-object v4, v0, Lcns;->d:Lcnq;

    invoke-static {v4}, Lcnq;->b(Lcnq;)Lcnn;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcns;->a:Lfrl;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcns;->b:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Lcnn;->a(Lfrl;Ljava/lang/String;)Lesq;

    move-result-object v13

    .line 205
    if-nez v13, :cond_0

    .line 206
    move-object/from16 v0, p0

    iget-object v4, v0, Lcns;->c:Leuc;

    const/4 v5, 0x0

    invoke-interface {v4, v12, v5}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 223
    :goto_0
    return-void

    .line 209
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcns;->d:Lcnq;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcns;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcns;->a:Lfrl;

    move-object/from16 v16, v0

    .line 213
    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v16

    iget-object v4, v0, Lfrl;->a:Lhro;

    iget-object v0, v4, Lhro;->d:[Lhqx;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    const/4 v4, 0x0

    move v10, v4

    :goto_1
    move/from16 v0, v19

    if-ge v10, v0, :cond_2

    aget-object v9, v18, v10

    iget-object v4, v9, Lhqx;->c:Lhrf;

    if-eqz v4, :cond_1

    iget-object v4, v9, Lhqx;->c:Lhrf;

    iget-object v4, v4, Lhrf;->a:Lhro;

    if-eqz v4, :cond_1

    new-instance v4, Lfrl;

    iget-object v5, v9, Lhqx;->c:Lhrf;

    iget-object v5, v5, Lhrf;->a:Lhro;

    move-object/from16 v0, v16

    iget-wide v6, v0, Lfrl;->b:J

    move-object/from16 v0, v16

    iget-object v8, v0, Lfrl;->c:[B

    new-instance v20, Lfri;

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Lfrj;

    move-object/from16 v21, v0

    invoke-direct/range {v20 .. v21}, Lfri;-><init>([Lfrj;)V

    iget-object v9, v9, Lhqx;->c:Lhrf;

    iget-object v9, v9, Lhrf;->a:Lhro;

    move-object/from16 v0, v16

    iget-wide v0, v0, Lfrl;->b:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-static {v0, v9, v1, v2}, Lfrl;->a(Lfri;Lhro;J)Lfrf;

    move-result-object v9

    invoke-direct/range {v4 .. v9}, Lfrl;-><init>(Lhro;J[BLfrf;)V

    iget-object v5, v4, Lfrl;->a:Lhro;

    invoke-static {v5}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_1

    :cond_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v4

    .line 209
    :goto_2
    invoke-virtual {v14, v13, v15, v11, v4}, Lcnq;->a(Lesq;Ljava/lang/String;Lfai;Ljava/util/Map;)Lesf;

    move-result-object v5

    .line 214
    if-eqz v5, :cond_5

    iget-object v4, v5, Lesf;->b:Lfoy;

    .line 215
    :goto_3
    if-eqz v4, :cond_3

    iget-object v4, v4, Lfoy;->aj:Lfoo;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcns;->d:Lcnq;

    invoke-static {v4}, Lcnq;->c(Lcnq;)Levn;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 217
    move-object/from16 v0, p0

    iget-object v4, v0, Lcns;->d:Lcnq;

    invoke-static {v4}, Lcnq;->c(Lcnq;)Levn;

    move-result-object v4

    new-instance v6, Lczv;

    invoke-direct {v6, v5}, Lczv;-><init>(Lesf;)V

    invoke-virtual {v4, v6}, Levn;->c(Ljava/lang/Object;)V

    .line 219
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcns;->c:Leuc;

    invoke-interface {v4, v12, v5}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 220
    move-object/from16 v0, p0

    iget-object v4, v0, Lcns;->d:Lcnq;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcns;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcns;->a:Lfrl;

    .line 222
    invoke-virtual {v6}, Lfrl;->i()Lfqy;

    move-result-object v6

    invoke-virtual {v6}, Lfqy;->a()Lfqx;

    move-result-object v6

    .line 220
    invoke-static {v4, v13, v5, v6}, Lcnq;->a(Lcnq;Lesq;Ljava/lang/String;Lfqx;)V

    goto/16 :goto_0

    .line 213
    :cond_4
    invoke-static/range {v17 .. v17}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    goto :goto_2

    .line 214
    :cond_5
    const/4 v4, 0x0

    goto :goto_3
.end method
