.class public final Ldcr;
.super Lded;
.source "SourceFile"

# interfaces
.implements Lddx;
.implements Ldff;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/view/ViewGroup;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/ViewGroup;

.field private e:[Landroid/widget/TextView;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Ldfg;

.field private m:Z

.field private n:I

.field private o:Z

.field private p:Z

.field private q:I

.field private r:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lded;-><init>(Landroid/content/Context;)V

    .line 55
    iput-object p1, p0, Ldcr;->a:Landroid/content/Context;

    .line 56
    invoke-virtual {p0}, Ldcr;->a()V

    .line 57
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 223
    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 227
    iget-boolean v0, p0, Ldcr;->m:Z

    if-eqz v0, :cond_3

    .line 228
    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v1, v0, p1

    if-eqz p2, :cond_2

    iget-object v0, p0, Ldcr;->j:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v1, v2, v2, v0, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldcr;->k:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 231
    :cond_3
    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic a(Ldcr;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ldcr;->d()V

    return-void
.end method

.method static synthetic a(Ldcr;ILandroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-virtual {p2}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Ldcr;->a(IZ)V

    iget-boolean v2, p0, Ldcr;->m:Z

    if-eqz v2, :cond_3

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldcr;->m:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ldcr;->n:I

    if-ge p1, v0, :cond_2

    iget v0, p0, Ldcr;->n:I

    invoke-direct {p0, v0, v1}, Ldcr;->a(IZ)V

    :cond_0
    invoke-direct {p0}, Ldcr;->c()V

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_2
    iget v2, p0, Ldcr;->n:I

    if-ge v0, v2, :cond_0

    invoke-direct {p0, v0, v1}, Ldcr;->a(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Ldcr;->d()V

    goto :goto_1
.end method

.method static synthetic a(Ldcr;Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 28
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Ldcr;->q:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Ldcr;->r:I

    :cond_0
    return-void
.end method

.method static synthetic b(Ldcr;)Ldfg;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ldcr;->l:Ldfg;

    return-object v0
.end method

.method private b(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 236
    iget-object v1, p0, Ldcr;->b:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Ldcr;->e:[Landroid/widget/TextView;

    array-length v1, v1

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(Ldcr;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Ldcr;->q:I

    return v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 197
    iput-boolean v1, p0, Ldcr;->o:Z

    move v0, v1

    .line 198
    :goto_0
    iget v2, p0, Ldcr;->n:I

    if-ge v0, v2, :cond_2

    .line 199
    iget-boolean v2, p0, Ldcr;->o:Z

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Ldcr;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v3

    :goto_1
    iput-boolean v2, p0, Ldcr;->o:Z

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 199
    goto :goto_1

    .line 201
    :cond_2
    iget-boolean v0, p0, Ldcr;->o:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Ldcr;->m:Z

    if-eqz v0, :cond_5

    iget v0, p0, Ldcr;->n:I

    invoke-direct {p0, v0}, Ldcr;->b(I)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    :goto_2
    iput-boolean v3, p0, Ldcr;->o:Z

    .line 202
    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 203
    iget-object v2, p0, Ldcr;->g:Landroid/view/View;

    iget-boolean v0, p0, Ldcr;->o:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Ldcr;->m:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Ldcr;->f:Landroid/view/View;

    iget-boolean v2, p0, Ldcr;->p:Z

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Ldcr;->o:Z

    if-nez v2, :cond_7

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 206
    :cond_4
    return-void

    :cond_5
    move v3, v1

    .line 201
    goto :goto_2

    :cond_6
    move v0, v4

    .line 203
    goto :goto_3

    :cond_7
    move v1, v4

    .line 204
    goto :goto_4
.end method

.method static synthetic d(Ldcr;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Ldcr;->r:I

    return v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 209
    iget-object v1, p0, Ldcr;->l:Ldfg;

    if-nez v1, :cond_0

    .line 220
    :goto_0
    return-void

    .line 212
    :cond_0
    iget v1, p0, Ldcr;->n:I

    new-array v3, v1, [I

    move v1, v0

    .line 214
    :goto_1
    iget v2, p0, Ldcr;->n:I

    if-ge v0, v2, :cond_2

    .line 215
    invoke-direct {p0, v0}, Ldcr;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 216
    add-int/lit8 v2, v1, 0x1

    aput v0, v3, v1

    move v1, v2

    .line 214
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 219
    :cond_2
    iget-object v0, p0, Ldcr;->l:Ldfg;

    invoke-static {v3, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    invoke-interface {v0, v1}, Ldfg;->a([I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 135
    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Ldcr;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Ldcr;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 141
    :cond_0
    iput-boolean v1, p0, Ldcr;->o:Z

    .line 142
    iput-boolean v1, p0, Ldcr;->p:Z

    .line 143
    iput v1, p0, Ldcr;->q:I

    .line 144
    iput v1, p0, Ldcr;->r:I

    .line 145
    invoke-virtual {p0, v2}, Ldcr;->setVisibility(I)V

    .line 146
    return-void
.end method

.method public final a(I)V
    .locals 6

    .prologue
    .line 113
    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 117
    :cond_0
    int-to-float v0, p1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0}, La;->c(I)Ljava/lang/String;

    move-result-object v0

    .line 118
    iget-object v1, p0, Ldcr;->i:Landroid/widget/TextView;

    iget-object v2, p0, Ldcr;->b:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09017a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ldfg;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Ldcr;->l:Ldfg;

    .line 131
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldcr;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040112

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    const v2, 0x7f0802f8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcr;->c:Landroid/widget/TextView;

    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    const v2, 0x7f0802f9

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v2, 0x7f0802fa

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcr;->h:Landroid/widget/TextView;

    iget-object v0, p0, Ldcr;->a:Landroid/content/Context;

    const v2, 0x7f02027b

    invoke-static {v0, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ldcr;->j:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Ldcr;->a:Landroid/content/Context;

    const v2, 0x7f02027e

    invoke-static {v0, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ldcr;->k:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v2, 0x7f080305

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcr;->i:Landroid/widget/TextView;

    new-array v2, v6, [Landroid/view/ViewGroup;

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v3, 0x7f0802fb

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v2, v1

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v3, 0x7f0802fe

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v2, v4

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v3, 0x7f080301

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v2, v5

    const/4 v0, 0x5

    new-array v2, v0, [Landroid/widget/TextView;

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v3, 0x7f0802fc

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v1

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v3, 0x7f0802fd

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v4

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v3, 0x7f0802ff

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v5

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v3, 0x7f080300

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v6

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v3, 0x7f080302

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v7

    iput-object v2, p0, Ldcr;->e:[Landroid/widget/TextView;

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v2, 0x7f080303

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldcr;->f:Landroid/view/View;

    iget-object v0, p0, Ldcr;->f:Landroid/view/View;

    new-instance v2, Ldcs;

    invoke-direct {v2, p0}, Ldcs;-><init>(Ldcr;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Ldcr;->f:Landroid/view/View;

    new-instance v2, Ldct;

    invoke-direct {v2, p0}, Ldct;-><init>(Ldcr;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    const v2, 0x7f080304

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldcr;->g:Landroid/view/View;

    iget-object v0, p0, Ldcr;->g:Landroid/view/View;

    new-instance v2, Ldcu;

    invoke-direct {v2, p0}, Ldcu;-><init>(Ldcr;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v0, v1

    :goto_0
    iget-object v2, p0, Ldcr;->e:[Landroid/widget/TextView;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    new-instance v3, Ldcv;

    invoke-direct {v3, p0, v0}, Ldcv;-><init>(Ldcr;I)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_0
    invoke-virtual {p0}, Ldcr;->a()V

    .line 102
    iput-boolean p3, p0, Ldcr;->m:Z

    .line 103
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Ldcr;->n:I

    .line 105
    iget-object v0, p0, Ldcr;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Ldcr;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_1
    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    array-length v0, v0

    if-ge v2, v0, :cond_3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v4, v0, v2

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    invoke-direct {p0, v2, v1}, Ldcr;->a(IZ)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    if-ne v2, v3, :cond_2

    if-eqz p3, :cond_2

    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    const v4, 0x7f09017d

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ldcr;->e:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 108
    :cond_3
    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 109
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 150
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Ldcr;->setVisibility(I)V

    .line 151
    return-void

    .line 150
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldcr;->p:Z

    .line 125
    invoke-direct {p0}, Ldcr;->c()V

    .line 126
    return-void
.end method

.method public final c_(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 160
    iget-object v0, p0, Ldcr;->b:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v3, p0, Ldcr;->c:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Ldcr;->d:Landroid/view/ViewGroup;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 164
    goto :goto_1

    :cond_2
    move v2, v1

    .line 165
    goto :goto_2
.end method

.method public final r_()Ldeg;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 155
    new-instance v0, Ldeg;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Ldeg;-><init>(IIZ)V

    return-object v0
.end method
