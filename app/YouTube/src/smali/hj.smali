.class public final Lhj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final b:Lhr;


# instance fields
.field public a:Ljava/lang/ref/WeakReference;

.field private c:Ljava/lang/Runnable;

.field private d:Ljava/lang/Runnable;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 596
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 597
    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 598
    new-instance v0, Lhq;

    invoke-direct {v0}, Lhq;-><init>()V

    sput-object v0, Lhj;->b:Lhr;

    .line 608
    :goto_0
    return-void

    .line 599
    :cond_0
    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 600
    new-instance v0, Lho;

    invoke-direct {v0}, Lho;-><init>()V

    sput-object v0, Lhj;->b:Lhr;

    goto :goto_0

    .line 601
    :cond_1
    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 602
    new-instance v0, Lhp;

    invoke-direct {v0}, Lhp;-><init>()V

    sput-object v0, Lhj;->b:Lhr;

    goto :goto_0

    .line 603
    :cond_2
    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 604
    new-instance v0, Lhm;

    invoke-direct {v0}, Lhm;-><init>()V

    sput-object v0, Lhj;->b:Lhr;

    goto :goto_0

    .line 606
    :cond_3
    new-instance v0, Lhk;

    invoke-direct {v0}, Lhk;-><init>()V

    sput-object v0, Lhj;->b:Lhr;

    goto :goto_0
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lhj;->c:Ljava/lang/Runnable;

    .line 28
    iput-object v0, p0, Lhj;->d:Ljava/lang/Runnable;

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lhj;->e:I

    .line 36
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    .line 37
    return-void
.end method

.method static synthetic a(Lhj;I)I
    .locals 1

    .prologue
    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lhj;->e:I

    return v0
.end method

.method static synthetic a(Lhj;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic b(Lhj;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic c(Lhj;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lhj;->e:I

    return v0
.end method


# virtual methods
.method public final a(F)Lhj;
    .locals 2

    .prologue
    .line 640
    iget-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 641
    sget-object v1, Lhj;->b:Lhr;

    invoke-interface {v1, p0, v0, p1}, Lhr;->a(Lhj;Landroid/view/View;F)V

    .line 643
    :cond_0
    return-object p0
.end method

.method public final a(J)Lhj;
    .locals 3

    .prologue
    .line 623
    iget-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 624
    sget-object v1, Lhj;->b:Lhr;

    invoke-interface {v1, v0, p1, p2}, Lhr;->a(Landroid/view/View;J)V

    .line 626
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/view/animation/Interpolator;)Lhj;
    .locals 2

    .prologue
    .line 763
    iget-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 764
    sget-object v1, Lhj;->b:Lhr;

    invoke-interface {v1, v0, p1}, Lhr;->a(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    .line 766
    :cond_0
    return-object p0
.end method

.method public final a(Lhv;)Lhj;
    .locals 2

    .prologue
    .line 1195
    iget-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1196
    sget-object v1, Lhj;->b:Lhr;

    invoke-interface {v1, p0, v0, p1}, Lhr;->a(Lhj;Landroid/view/View;Lhv;)V

    .line 1198
    :cond_0
    return-object p0
.end method

.method public final a(Lhx;)Lhj;
    .locals 2

    .prologue
    .line 1214
    iget-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1215
    sget-object v1, Lhj;->b:Lhr;

    invoke-interface {v1, v0, p1}, Lhr;->a(Landroid/view/View;Lhx;)V

    .line 1217
    :cond_0
    return-object p0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 997
    iget-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 998
    sget-object v1, Lhj;->b:Lhr;

    invoke-interface {v1, p0, v0}, Lhr;->a(Lhj;Landroid/view/View;)V

    .line 1000
    :cond_0
    return-void
.end method

.method public final b(F)Lhj;
    .locals 2

    .prologue
    .line 674
    iget-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 675
    sget-object v1, Lhj;->b:Lhr;

    invoke-interface {v1, p0, v0, p1}, Lhr;->b(Lhj;Landroid/view/View;F)V

    .line 677
    :cond_0
    return-object p0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1115
    iget-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1116
    sget-object v1, Lhj;->b:Lhr;

    invoke-interface {v1, p0, v0}, Lhr;->b(Lhj;Landroid/view/View;)V

    .line 1118
    :cond_0
    return-void
.end method

.method public final c(F)Lhj;
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lhj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 692
    sget-object v1, Lhj;->b:Lhr;

    invoke-interface {v1, p0, v0, p1}, Lhr;->c(Lhj;Landroid/view/View;F)V

    .line 694
    :cond_0
    return-object p0
.end method
