.class public final Lcws;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Lgoh;

.field private B:I

.field private C:Z

.field private D:Z

.field private final E:Ljava/lang/Runnable;

.field final a:Landroid/content/Context;

.field public final b:Levn;

.field final c:Lgeh;

.field public final d:Lcwn;

.field public final e:Lcxi;

.field public f:Lcvq;

.field public g:Ldkf;

.field h:Z

.field public final i:Lcvd;

.field public j:Z

.field private final k:Ldaq;

.field private final l:Ldaw;

.field private final m:Lfxe;

.field private final n:Lgix;

.field private final o:Lglm;

.field private final p:Lgng;

.field private final q:Leyt;

.field private final r:Lfac;

.field private final s:Lcyc;

.field private final t:Ljava/util/concurrent/Executor;

.field private final u:Lcwv;

.field private final v:Landroid/media/AudioManager;

.field private final w:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final x:Landroid/os/Handler;

.field private final y:Lcwq;

.field private final z:Lcwr;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406
    new-instance v0, Lcwt;

    invoke-direct {v0, p0}, Lcwt;-><init>(Lcws;)V

    iput-object v0, p0, Lcws;->E:Ljava/lang/Runnable;

    .line 162
    iput-object v1, p0, Lcws;->a:Landroid/content/Context;

    .line 163
    iput-object v1, p0, Lcws;->b:Levn;

    .line 164
    iput-object v1, p0, Lcws;->c:Lgeh;

    .line 165
    iput-object v1, p0, Lcws;->k:Ldaq;

    .line 166
    iput-object v1, p0, Lcws;->m:Lfxe;

    .line 167
    iput-object v1, p0, Lcws;->q:Leyt;

    .line 168
    iput-object v1, p0, Lcws;->r:Lfac;

    .line 169
    iput-object v1, p0, Lcws;->t:Ljava/util/concurrent/Executor;

    .line 170
    iput-object v1, p0, Lcws;->s:Lcyc;

    .line 171
    iput-object v1, p0, Lcws;->v:Landroid/media/AudioManager;

    .line 172
    iput-object v1, p0, Lcws;->w:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcws;->D:Z

    .line 174
    iput-object v1, p0, Lcws;->y:Lcwq;

    .line 175
    iput-object v1, p0, Lcws;->n:Lgix;

    .line 176
    iput-object v1, p0, Lcws;->p:Lgng;

    .line 177
    iput-object v1, p0, Lcws;->x:Landroid/os/Handler;

    .line 178
    iput-object v1, p0, Lcws;->f:Lcvq;

    .line 179
    iput-object v1, p0, Lcws;->o:Lglm;

    .line 180
    iput-object v1, p0, Lcws;->u:Lcwv;

    .line 181
    iput-object v1, p0, Lcws;->l:Ldaw;

    .line 182
    iput-object v1, p0, Lcws;->d:Lcwn;

    .line 183
    iput-object v1, p0, Lcws;->e:Lcxi;

    .line 184
    iput-object v1, p0, Lcws;->z:Lcwr;

    .line 185
    iput-object v1, p0, Lcws;->i:Lcvd;

    .line 186
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Levn;Lgeh;Ldaw;Lfxe;Leyt;Lfac;Lcyc;Ljava/util/concurrent/Executor;Lgix;Lglm;Lgng;Lcwq;Lcwr;Lcxi;)V
    .locals 5

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406
    new-instance v1, Lcwt;

    invoke-direct {v1, p0}, Lcwt;-><init>(Lcws;)V

    iput-object v1, p0, Lcws;->E:Ljava/lang/Runnable;

    .line 124
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcws;->a:Landroid/content/Context;

    .line 125
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Levn;

    iput-object v1, p0, Lcws;->b:Levn;

    .line 127
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgeh;

    iput-object v1, p0, Lcws;->c:Lgeh;

    .line 128
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfxe;

    iput-object v1, p0, Lcws;->m:Lfxe;

    .line 129
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leyt;

    iput-object v1, p0, Lcws;->q:Leyt;

    .line 130
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfac;

    iput-object v1, p0, Lcws;->r:Lfac;

    .line 131
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lcws;->t:Ljava/util/concurrent/Executor;

    .line 132
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcyc;

    iput-object v1, p0, Lcws;->s:Lcyc;

    .line 133
    iget-object v1, p0, Lcws;->a:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcws;->v:Landroid/media/AudioManager;

    .line 134
    new-instance v1, Lcwu;

    invoke-direct {v1, p0}, Lcwu;-><init>(Lcws;)V

    iput-object v1, p0, Lcws;->w:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 135
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcws;->D:Z

    .line 136
    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwq;

    iput-object v1, p0, Lcws;->y:Lcwq;

    .line 137
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgix;

    iput-object v1, p0, Lcws;->n:Lgix;

    .line 138
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgng;

    iput-object v1, p0, Lcws;->p:Lgng;

    .line 140
    invoke-static/range {p14 .. p14}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwr;

    iput-object v1, p0, Lcws;->z:Lcwr;

    .line 141
    invoke-interface/range {p14 .. p14}, Lcwr;->a()Lcvq;

    move-result-object v1

    iput-object v1, p0, Lcws;->f:Lcvq;

    .line 142
    invoke-interface/range {p14 .. p14}, Lcwr;->b()Ldaq;

    move-result-object v1

    iput-object v1, p0, Lcws;->k:Ldaq;

    .line 143
    iget-object v1, p0, Lcws;->f:Lcvq;

    invoke-interface {v1}, Lcvq;->c()Lcwn;

    move-result-object v1

    iput-object v1, p0, Lcws;->d:Lcwn;

    .line 144
    iget-object v1, p0, Lcws;->d:Lcwn;

    invoke-virtual {p2, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 147
    move-object/from16 v0, p11

    iput-object v0, p0, Lcws;->o:Lglm;

    .line 148
    iput-object p4, p0, Lcws;->l:Ldaw;

    .line 149
    move-object/from16 v0, p15

    iput-object v0, p0, Lcws;->e:Lcxi;

    .line 150
    if-eqz p15, :cond_0

    .line 151
    move-object/from16 v0, p15

    invoke-virtual {p2, v0}, Levn;->a(Ljava/lang/Object;)V

    .line 154
    :cond_0
    new-instance v1, Lcwv;

    invoke-direct {v1, p0}, Lcwv;-><init>(Lcws;)V

    iput-object v1, p0, Lcws;->u:Lcwv;

    .line 155
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcws;->x:Landroid/os/Handler;

    .line 156
    new-instance v1, Lcvd;

    move-object/from16 v0, p14

    invoke-direct {v1, p1, v0, p2}, Lcvd;-><init>(Landroid/content/Context;Lcwr;Levn;)V

    iput-object v1, p0, Lcws;->i:Lcvd;

    .line 157
    iget-object v1, p0, Lcws;->i:Lcvd;

    iget-boolean v2, v1, Lcvd;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcvd;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcvd;->b:Z

    .line 158
    :cond_1
    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    .line 740
    invoke-virtual {p0}, Lcws;->u()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcws;->C:Z

    if-eqz v0, :cond_1

    .line 743
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcws;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 744
    const-string v1, "background_mode"

    invoke-virtual {p0}, Lcws;->u()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 745
    iget-object v1, p0, Lcws;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 746
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcws;->C:Z

    .line 748
    :cond_1
    return-void
.end method

.method private B()V
    .locals 4

    .prologue
    .line 884
    iget-boolean v0, p0, Lcws;->D:Z

    if-eqz v0, :cond_0

    .line 885
    iget-object v0, p0, Lcws;->v:Landroid/media/AudioManager;

    iget-object v1, p0, Lcws;->w:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/high16 v2, -0x80000000

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 890
    :cond_0
    return-void
.end method

.method private C()Lgnd;
    .locals 2

    .prologue
    .line 929
    iget-object v0, p0, Lcws;->n:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 930
    iget-object v0, p0, Lcws;->p:Lgng;

    invoke-virtual {v0}, Lgng;->c()Lgnd;

    move-result-object v0

    .line 932
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcws;->p:Lgng;

    iget-object v1, p0, Lcws;->n:Lgix;

    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    goto :goto_0
.end method

.method private handleVideoStageEvent(Ldae;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 872
    iget v0, p1, Ldae;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcws;->h:Z

    if-nez v0, :cond_0

    .line 873
    invoke-direct {p0}, Lcws;->B()V

    .line 875
    :cond_0
    return-void
.end method

.method private z()V
    .locals 3

    .prologue
    .line 673
    iget-object v0, p0, Lcws;->f:Lcvq;

    iget-object v1, p0, Lcws;->A:Lgoh;

    .line 674
    iget-object v1, v1, Lgoh;->a:Leaa;

    iget-boolean v1, v1, Leaa;->m:Z

    iget v2, p0, Lcws;->B:I

    .line 673
    invoke-interface {v0, v1, v2}, Lcvq;->b(ZI)V

    .line 676
    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->k()V

    .line 677
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 193
    iget-object v0, p0, Lcws;->c:Lgeh;

    invoke-interface {v0}, Lgeh;->n()V

    .line 196
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->l()V

    .line 197
    invoke-virtual {p0, v2}, Lcws;->a(Z)V

    .line 199
    iget-object v0, p0, Lcws;->z:Lcwr;

    invoke-interface {v0}, Lcwr;->a()Lcvq;

    move-result-object v0

    iput-object v0, p0, Lcws;->f:Lcvq;

    .line 200
    iput-object v1, p0, Lcws;->A:Lgoh;

    .line 201
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->j()V

    .line 203
    iput-object v1, p0, Lcws;->g:Ldkf;

    .line 205
    :cond_0
    iput v2, p0, Lcws;->B:I

    .line 206
    iget-object v0, p0, Lcws;->e:Lcxi;

    if-eqz v0, :cond_1

    .line 207
    iget-object v1, p0, Lcws;->e:Lcxi;

    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcws;

    iput-object v0, v1, Lcxi;->b:Lcws;

    iput v2, v1, Lcxi;->c:I

    .line 209
    :cond_1
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0, p1}, Lcvq;->a(F)V

    .line 811
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0, p1}, Lcvq;->a(I)V

    .line 309
    return-void
.end method

.method public final a(Ldlf;)V
    .locals 12

    .prologue
    .line 476
    iget-object v0, p0, Lcws;->b:Levn;

    new-instance v1, Lczj;

    invoke-direct {v1}, Lczj;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 477
    invoke-virtual {p0}, Lcws;->a()V

    .line 478
    iget-boolean v0, p1, Ldlf;->d:Z

    invoke-virtual {p0, v0}, Lcws;->e(Z)V

    .line 479
    invoke-direct {p0}, Lcws;->B()V

    .line 480
    iget-object v0, p1, Ldlf;->a:Lgoh;

    iput-object v0, p0, Lcws;->A:Lgoh;

    .line 481
    iget-object v11, p1, Ldlf;->c:Ldkg;

    .line 482
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ldlj;

    if-ne v0, v1, :cond_2

    new-instance v0, Ldkj;

    iget-object v1, p0, Lcws;->f:Lcvq;

    iget-object v2, p0, Lcws;->b:Levn;

    iget-object v3, p0, Lcws;->y:Lcwq;

    iget-object v4, p0, Lcws;->t:Ljava/util/concurrent/Executor;

    iget-object v5, p0, Lcws;->k:Ldaq;

    iget-object v6, p0, Lcws;->l:Ldaw;

    iget-object v7, p0, Lcws;->q:Leyt;

    iget-object v8, p0, Lcws;->r:Lfac;

    iget-object v9, p0, Lcws;->s:Lcyc;

    move-object v10, v11

    check-cast v10, Ldlj;

    invoke-direct/range {v0 .. v10}, Ldkj;-><init>(Lcvq;Levn;Lcwq;Ljava/util/concurrent/Executor;Ldaq;Ldaw;Leyt;Lfac;Lcyc;Ldlj;)V

    :goto_0
    iput-object v0, p0, Lcws;->g:Ldkf;

    .line 483
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p1, Ldlf;->b:Lcvr;

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcws;->z()V

    .line 485
    :goto_1
    iget-object v0, p0, Lcws;->u:Lcwv;

    invoke-virtual {v0}, Lcwv;->a()V

    .line 487
    :cond_0
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcws;->A:Lgoh;

    if-eqz v0, :cond_1

    .line 488
    iget-object v0, p0, Lcws;->y:Lcwq;

    iget-object v1, p0, Lcws;->A:Lgoh;

    invoke-virtual {v1}, Lgoh;->f()Lgog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcwq;->a(Lgog;)V

    .line 492
    :cond_1
    invoke-virtual {p0}, Lcws;->t()V

    .line 493
    return-void

    .line 482
    :cond_2
    const-class v1, Ldld;

    if-ne v0, v1, :cond_3

    new-instance v0, Ldjx;

    iget-object v1, p0, Lcws;->a:Landroid/content/Context;

    iget-object v2, p0, Lcws;->f:Lcvq;

    iget-object v3, p0, Lcws;->b:Levn;

    iget-object v4, p0, Lcws;->y:Lcwq;

    iget-object v5, p0, Lcws;->q:Leyt;

    iget-object v6, p0, Lcws;->r:Lfac;

    iget-object v7, p0, Lcws;->t:Ljava/util/concurrent/Executor;

    iget-object v8, p0, Lcws;->l:Ldaw;

    iget-object v9, p0, Lcws;->o:Lglm;

    invoke-direct {p0}, Lcws;->C()Lgnd;

    move-result-object v10

    check-cast v11, Ldld;

    invoke-direct/range {v0 .. v11}, Ldjx;-><init>(Landroid/content/Context;Lcvq;Levn;Lcwq;Leyt;Lfac;Ljava/util/concurrent/Executor;Ldaw;Lglm;Lgnd;Ldld;)V

    goto :goto_0

    :cond_3
    const-class v1, Ldlb;

    if-ne v0, v1, :cond_4

    new-instance v0, Ldjv;

    iget-object v1, p0, Lcws;->f:Lcvq;

    iget-object v2, p0, Lcws;->b:Levn;

    iget-object v3, p0, Lcws;->y:Lcwq;

    iget-object v4, p0, Lcws;->k:Ldaq;

    iget-object v5, p0, Lcws;->q:Leyt;

    iget-object v6, p0, Lcws;->r:Lfac;

    iget-object v7, p0, Lcws;->m:Lfxe;

    move-object v8, v11

    check-cast v8, Ldlb;

    invoke-direct/range {v0 .. v8}, Ldjv;-><init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Lfxe;Ldlb;)V

    goto :goto_0

    :cond_4
    const-class v1, Ldll;

    if-ne v0, v1, :cond_5

    new-instance v0, Ldkt;

    iget-object v1, p0, Lcws;->f:Lcvq;

    iget-object v2, p0, Lcws;->b:Levn;

    iget-object v3, p0, Lcws;->y:Lcwq;

    iget-object v4, p0, Lcws;->t:Ljava/util/concurrent/Executor;

    iget-object v5, p0, Lcws;->k:Ldaq;

    iget-object v6, p0, Lcws;->l:Ldaw;

    iget-object v7, p0, Lcws;->q:Leyt;

    iget-object v8, p0, Lcws;->r:Lfac;

    move-object v9, v11

    check-cast v9, Ldll;

    invoke-direct/range {v0 .. v9}, Ldkt;-><init>(Lcvq;Levn;Lcwq;Ljava/util/concurrent/Executor;Ldaq;Ldaw;Leyt;Lfac;Ldll;)V

    goto/16 :goto_0

    :cond_5
    const-class v1, Ldlh;

    if-ne v0, v1, :cond_6

    new-instance v0, Ldkh;

    iget-object v1, p0, Lcws;->f:Lcvq;

    iget-object v2, p0, Lcws;->b:Levn;

    iget-object v3, p0, Lcws;->y:Lcwq;

    iget-object v4, p0, Lcws;->k:Ldaq;

    iget-object v5, p0, Lcws;->q:Leyt;

    iget-object v6, p0, Lcws;->r:Lfac;

    move-object v7, v11

    check-cast v7, Ldlh;

    invoke-direct/range {v0 .. v7}, Ldkh;-><init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Ldlh;)V

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 484
    :cond_7
    iget-object v1, p0, Lcws;->f:Lcvq;

    invoke-interface {v1, v0}, Lcvq;->a(Lcvr;)V

    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->l()V

    goto/16 :goto_1
.end method

.method public final a(Lgec;)V
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0, p1}, Lcvq;->a(Lgec;)V

    .line 700
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->r()V

    .line 703
    :cond_0
    invoke-direct {p0}, Lcws;->A()V

    .line 704
    return-void
.end method

.method public final a(Lgoh;)V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 417
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    iget-object v2, p0, Lcws;->b:Levn;

    new-instance v3, Lczj;

    invoke-direct {v3}, Lczj;-><init>()V

    invoke-virtual {v2, v3}, Levn;->d(Ljava/lang/Object;)V

    .line 419
    invoke-direct {p0}, Lcws;->B()V

    .line 425
    iget-object v2, p0, Lcws;->g:Ldkf;

    instance-of v2, v2, Ldkj;

    if-eqz v2, :cond_0

    if-nez p1, :cond_2

    :cond_0
    :goto_0
    if-eqz v0, :cond_5

    .line 426
    iput-object p1, p0, Lcws;->A:Lgoh;

    .line 427
    iget-object v0, p0, Lcws;->g:Ldkf;

    check-cast v0, Ldkj;

    invoke-virtual {v0, p1}, Ldkj;->a(Lgoh;)V

    .line 428
    iget-object v0, p0, Lcws;->y:Lcwq;

    invoke-virtual {p1}, Lgoh;->f()Lgog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcwq;->a(Lgog;)V

    .line 445
    :cond_1
    :goto_1
    return-void

    .line 425
    :cond_2
    iget-object v2, p1, Lgoh;->a:Leaa;

    iget-boolean v2, v2, Leaa;->h:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcws;->f:Lcvq;

    sget-object v3, Lgol;->b:Lgol;

    invoke-interface {v2, v3}, Lcvq;->a(Lgol;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcws;->f:Lcvq;

    sget-object v3, Lgol;->j:Lgol;

    invoke-interface {v2, v3}, Lcvq;->b(Lgol;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p1, Lgoh;->a:Leaa;

    iget-object v2, v2, Leaa;->a:Ljava/lang/String;

    iget-object v3, p0, Lcws;->f:Lcvq;

    invoke-interface {v3}, Lcvq;->q()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v1

    :goto_2
    if-nez v2, :cond_3

    iget-object v2, p0, Lcws;->g:Ldkf;

    iget-object v3, p1, Lgoh;->a:Leaa;

    iget-object v3, v3, Leaa;->c:Ljava/lang/String;

    invoke-interface {v2, v3}, Ldkf;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_2

    .line 430
    :cond_5
    invoke-virtual {p0}, Lcws;->a()V

    .line 431
    iput-object p1, p0, Lcws;->A:Lgoh;

    .line 432
    invoke-static {}, Lb;->a()V

    iget-object v0, p1, Lgoh;->a:Leaa;

    iget v0, v0, Leaa;->k:I

    iput v0, p0, Lcws;->B:I

    iget-object v0, p1, Lgoh;->a:Leaa;

    iget-boolean v0, v0, Leaa;->g:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcws;->l:Ldaw;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ldjx;

    iget-object v1, p0, Lcws;->a:Landroid/content/Context;

    iget-object v2, p0, Lcws;->f:Lcvq;

    iget-object v3, p0, Lcws;->b:Levn;

    iget-object v4, p0, Lcws;->y:Lcwq;

    iget-object v5, p0, Lcws;->q:Leyt;

    iget-object v6, p0, Lcws;->r:Lfac;

    iget-object v7, p0, Lcws;->t:Ljava/util/concurrent/Executor;

    iget-object v8, p0, Lcws;->l:Ldaw;

    iget-object v9, p0, Lcws;->o:Lglm;

    invoke-direct {p0}, Lcws;->C()Lgnd;

    move-result-object v10

    move-object v11, p1

    invoke-direct/range {v0 .. v11}, Ldjx;-><init>(Landroid/content/Context;Lcvq;Levn;Lcwq;Leyt;Lfac;Ljava/util/concurrent/Executor;Ldaw;Lglm;Lgnd;Lgoh;)V

    :goto_3
    iput-object v0, p0, Lcws;->g:Ldkf;

    .line 433
    iget-object v0, p0, Lcws;->y:Lcwq;

    invoke-virtual {p1}, Lgoh;->f()Lgog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcwq;->a(Lgog;)V

    .line 437
    iget-object v0, p1, Lgoh;->a:Leaa;

    iget-boolean v0, v0, Leaa;->o:Z

    if-eqz v0, :cond_6

    .line 438
    iget-object v0, p0, Lcws;->b:Levn;

    sget-object v1, Ldak;->a:Ldak;

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 440
    :cond_6
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_1

    .line 441
    invoke-direct {p0}, Lcws;->z()V

    .line 442
    iget-object v0, p0, Lcws;->u:Lcwv;

    invoke-virtual {v0}, Lcwv;->a()V

    goto/16 :goto_1

    .line 432
    :cond_7
    iget-object v0, p1, Lgoh;->b:Lgoj;

    sget-object v1, Lgoj;->c:Lgoj;

    if-ne v0, v1, :cond_9

    iget-object v0, p1, Lgoh;->a:Leaa;

    iget-boolean v0, v0, Leaa;->n:Z

    if-eqz v0, :cond_8

    new-instance v0, Ldkh;

    iget-object v1, p0, Lcws;->f:Lcvq;

    iget-object v2, p0, Lcws;->b:Levn;

    iget-object v3, p0, Lcws;->y:Lcwq;

    iget-object v4, p0, Lcws;->k:Ldaq;

    iget-object v5, p0, Lcws;->q:Leyt;

    iget-object v6, p0, Lcws;->r:Lfac;

    invoke-virtual {p1}, Lgoh;->d()Ljava/util/List;

    move-result-object v7

    iget-object v8, p1, Lgoh;->a:Leaa;

    iget v8, v8, Leaa;->d:I

    invoke-virtual {p1}, Lgoh;->e()[B

    move-result-object v9

    iget-object v10, p1, Lgoh;->a:Leaa;

    iget-object v10, v10, Leaa;->l:Ljava/lang/String;

    invoke-direct/range {v0 .. v10}, Ldkh;-><init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Ljava/util/List;I[BLjava/lang/String;)V

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcws;->l:Ldaw;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ldkt;

    iget-object v1, p0, Lcws;->f:Lcvq;

    iget-object v2, p0, Lcws;->b:Levn;

    iget-object v3, p0, Lcws;->y:Lcwq;

    iget-object v4, p0, Lcws;->t:Ljava/util/concurrent/Executor;

    iget-object v5, p0, Lcws;->k:Ldaq;

    iget-object v6, p0, Lcws;->l:Ldaw;

    iget-object v7, p0, Lcws;->q:Leyt;

    iget-object v8, p0, Lcws;->r:Lfac;

    invoke-virtual {p1}, Lgoh;->d()Ljava/util/List;

    move-result-object v9

    iget-object v10, p1, Lgoh;->a:Leaa;

    iget v10, v10, Leaa;->d:I

    invoke-virtual {p1}, Lgoh;->e()[B

    move-result-object v11

    iget-object v12, p1, Lgoh;->a:Leaa;

    iget-object v12, v12, Leaa;->l:Ljava/lang/String;

    invoke-direct/range {v0 .. v12}, Ldkt;-><init>(Lcvq;Levn;Lcwq;Ljava/util/concurrent/Executor;Ldaq;Ldaw;Leyt;Lfac;Ljava/util/List;I[BLjava/lang/String;)V

    goto :goto_3

    :cond_9
    iget-object v0, p1, Lgoh;->a:Leaa;

    iget-boolean v0, v0, Leaa;->n:Z

    if-eqz v0, :cond_a

    new-instance v0, Ldjv;

    iget-object v1, p0, Lcws;->f:Lcvq;

    iget-object v2, p0, Lcws;->b:Levn;

    iget-object v3, p0, Lcws;->y:Lcwq;

    iget-object v4, p0, Lcws;->k:Ldaq;

    iget-object v5, p0, Lcws;->q:Leyt;

    iget-object v6, p0, Lcws;->r:Lfac;

    iget-object v7, p0, Lcws;->m:Lfxe;

    iget-object v8, p1, Lgoh;->a:Leaa;

    iget-object v8, v8, Leaa;->c:Ljava/lang/String;

    iget-object v9, p1, Lgoh;->a:Leaa;

    iget v9, v9, Leaa;->d:I

    invoke-direct/range {v0 .. v9}, Ldjv;-><init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Lfxe;Ljava/lang/String;I)V

    goto/16 :goto_3

    :cond_a
    iget-object v0, p0, Lcws;->l:Ldaw;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ldkj;

    iget-object v1, p0, Lcws;->f:Lcvq;

    iget-object v2, p0, Lcws;->b:Levn;

    iget-object v3, p0, Lcws;->y:Lcwq;

    iget-object v4, p0, Lcws;->t:Ljava/util/concurrent/Executor;

    iget-object v5, p0, Lcws;->k:Ldaq;

    iget-object v6, p0, Lcws;->l:Ldaw;

    iget-object v7, p0, Lcws;->q:Leyt;

    iget-object v8, p0, Lcws;->r:Lfac;

    iget-object v9, p0, Lcws;->s:Lcyc;

    iget-object v10, p0, Lcws;->z:Lcwr;

    invoke-interface {v10}, Lcwr;->c()Z

    move-result v10

    move-object v11, p1

    invoke-direct/range {v0 .. v11}, Ldkj;-><init>(Lcvq;Levn;Lcwq;Ljava/util/concurrent/Executor;Ldaq;Ldaw;Leyt;Lfac;Lcyc;ZLgoh;)V

    goto/16 :goto_3
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 294
    if-eqz p1, :cond_1

    .line 295
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->j()V

    .line 299
    :goto_0
    iget-object v0, p0, Lcws;->u:Lcwv;

    iget-boolean v1, v0, Lcwv;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcwv;->b:Lcws;

    iget-object v1, v1, Lcws;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcwv;->a:Z

    .line 300
    :cond_0
    return-void

    .line 297
    :cond_1
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->k()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcws;->b:Levn;

    new-instance v1, Lczh;

    invoke-direct {v1}, Lczh;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 217
    invoke-virtual {p0}, Lcws;->a()V

    .line 218
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 757
    invoke-virtual {p0}, Lcws;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 770
    :goto_0
    return-void

    .line 761
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcws;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcws;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcws;->C:Z

    .line 762
    if-eqz p1, :cond_1

    .line 763
    invoke-virtual {p0}, Lcws;->b()V

    goto :goto_0

    .line 767
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcws;->a(Z)V

    .line 768
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->w()V

    goto :goto_0
.end method

.method public final b(Lgoh;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 814
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcws;->A:Lgoh;

    if-nez v1, :cond_1

    .line 847
    :cond_0
    :goto_0
    return v0

    .line 818
    :cond_1
    iget-object v1, p1, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 819
    iget-object v1, p1, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcws;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 823
    :cond_2
    iget-object v1, p1, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 824
    iget-object v1, p1, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcws;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 828
    :cond_3
    iget-object v1, p0, Lcws;->g:Ldkf;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcws;->g:Ldkf;

    .line 829
    invoke-interface {v1}, Ldkf;->t()I

    move-result v1

    iget-object v2, p1, Lgoh;->a:Leaa;

    iget v2, v2, Leaa;->d:I

    if-ne v1, v2, :cond_0

    .line 834
    :cond_4
    invoke-virtual {p1}, Lgoh;->d()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 838
    iget-object v1, p0, Lcws;->A:Lgoh;

    iget-object v1, v1, Lgoh;->a:Leaa;

    iget-boolean v1, v1, Leaa;->i:Z

    .line 839
    iget-object v2, p1, Lgoh;->a:Leaa;

    iget-boolean v2, v2, Leaa;->i:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcws;->A:Lgoh;

    .line 840
    invoke-virtual {v1}, Lgoh;->f()Lgog;

    move-result-object v1

    .line 841
    invoke-virtual {p1}, Lgoh;->f()Lgog;

    move-result-object v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcws;->A:Lgoh;

    .line 842
    iget-object v1, v1, Lgoh;->a:Leaa;

    iget-boolean v1, v1, Leaa;->g:Z

    iget-object v2, p0, Lcws;->A:Lgoh;

    iget-object v2, v2, Lgoh;->a:Leaa;

    iget-boolean v2, v2, Leaa;->g:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcws;->A:Lgoh;

    .line 843
    iget-object v1, v1, Lgoh;->a:Leaa;

    iget-boolean v1, v1, Leaa;->h:Z

    .line 844
    iget-object v2, p1, Lgoh;->a:Leaa;

    iget-boolean v2, v2, Leaa;->h:Z

    if-ne v1, v2, :cond_0

    .line 847
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Lcxl;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->u()Lcxl;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 786
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0, p1}, Lcvq;->e(Z)V

    .line 787
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->s()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0, p1}, Lcvq;->d(Z)V

    .line 791
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcws;->A:Lgoh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->q()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 805
    iput-boolean p1, p0, Lcws;->D:Z

    .line 806
    iget-object v0, p0, Lcws;->i:Lcvd;

    iput-boolean p1, v0, Lcvd;->c:Z

    .line 807
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->t()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final f(Z)Ldlf;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 851
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    .line 852
    new-instance v0, Ldlf;

    iget-object v1, p0, Lcws;->A:Lgoh;

    iget-object v2, p0, Lcws;->g:Ldkf;

    .line 854
    invoke-interface {v2}, Ldkf;->f()Ldkg;

    move-result-object v2

    iget-object v3, p0, Lcws;->f:Lcvq;

    .line 855
    invoke-interface {v3, p1}, Lcvq;->f(Z)Lcvr;

    move-result-object v3

    iget-boolean v4, p0, Lcws;->D:Z

    invoke-direct {v0, v1, v2, v3, v4}, Ldlf;-><init>(Lgoh;Ldkg;Lcvr;Z)V

    .line 858
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldlf;

    iget-boolean v1, p0, Lcws;->D:Z

    invoke-direct {v0, v2, v2, v2, v1}, Ldlf;-><init>(Lgoh;Ldkg;Lcvr;Z)V

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->r()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->s()I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->t()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->g()Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->h()Z

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->e()V

    .line 279
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->i()V

    .line 305
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->x()V

    .line 342
    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->q()V

    .line 343
    const/4 v0, 0x1

    .line 346
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-nez v0, :cond_0

    .line 352
    const/4 v0, 0x0

    .line 355
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->s_()Z

    move-result v0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->u_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->t_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->o()V

    .line 378
    :cond_0
    return-void
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->n()V

    .line 385
    :cond_0
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcws;->x:Landroid/os/Handler;

    iget-object v1, p0, Lcws;->E:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 404
    return-void
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->p()Z

    move-result v0

    return v0
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 707
    invoke-virtual {p0}, Lcws;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 711
    invoke-direct {p0}, Lcws;->B()V

    .line 714
    :cond_0
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->o()V

    .line 715
    iget-object v0, p0, Lcws;->g:Ldkf;

    if-eqz v0, :cond_1

    .line 716
    iget-object v0, p0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->r()V

    .line 718
    :cond_1
    invoke-direct {p0}, Lcws;->A()V

    .line 719
    return-void
.end method

.method public final w()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 904
    iget-object v0, p0, Lcws;->d:Lcwn;

    iput-boolean v1, v0, Lcwn;->B:Z

    .line 905
    iput-boolean v1, p0, Lcws;->j:Z

    .line 906
    iget-object v0, p0, Lcws;->c:Lgeh;

    instance-of v0, v0, Lgek;

    if-eqz v0, :cond_0

    .line 907
    iget-object v0, p0, Lcws;->c:Lgeh;

    check-cast v0, Lgek;

    .line 908
    iget-object v1, v0, Lgek;->b:Lgeh;

    iput-object v1, v0, Lgek;->a:Lgeh;

    .line 913
    :goto_0
    return-void

    .line 910
    :cond_0
    const-string v0, "The player is not a RemoteControlAwarePlayer. Will not switch to local route mode"

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final x()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 917
    iget-object v0, p0, Lcws;->d:Lcwn;

    iput-boolean v1, v0, Lcwn;->B:Z

    .line 918
    iput-boolean v1, p0, Lcws;->j:Z

    .line 919
    iget-object v0, p0, Lcws;->c:Lgeh;

    instance-of v0, v0, Lgek;

    if-eqz v0, :cond_0

    .line 920
    iget-object v0, p0, Lcws;->c:Lgeh;

    check-cast v0, Lgek;

    .line 921
    iget-object v1, v0, Lgek;->c:Lgeh;

    iput-object v1, v0, Lgek;->a:Lgeh;

    iget-object v1, v0, Lgek;->b:Lgeh;

    invoke-interface {v1}, Lgeh;->h()V

    iget-object v0, v0, Lgek;->b:Lgeh;

    invoke-interface {v0}, Lgeh;->n()V

    .line 926
    :goto_0
    return-void

    .line 923
    :cond_0
    const-string v0, "The player is not a RemoteControlAwarePlayer. Will not switch to remote route mode"

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final y()Lcvl;
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lcws;->f:Lcvq;

    invoke-interface {v0}, Lcvq;->v()Lcvk;

    move-result-object v0

    return-object v0
.end method
