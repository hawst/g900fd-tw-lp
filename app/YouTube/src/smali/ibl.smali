.class public final Libl;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lgyt;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:[Lhym;

.field private e:[Lhgj;

.field private f:[Libo;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lidf;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Libl;->a:Lgyt;

    const-string v0, ""

    iput-object v0, p0, Libl;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Libl;->c:Ljava/lang/String;

    sget-object v0, Lhym;->a:[Lhym;

    iput-object v0, p0, Libl;->d:[Lhym;

    sget-object v0, Lhgj;->a:[Lhgj;

    iput-object v0, p0, Libl;->e:[Lhgj;

    sget-object v0, Libo;->a:[Libo;

    iput-object v0, p0, Libl;->f:[Libo;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Libl;->a:Lgyt;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    iget-object v2, p0, Libl;->a:Lgyt;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Libl;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Libl;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Libl;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Libl;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Libl;->d:[Lhym;

    if-eqz v2, :cond_3

    iget-object v3, p0, Libl;->d:[Lhym;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    if-eqz v5, :cond_2

    const/4 v6, 0x4

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Libl;->e:[Lhgj;

    if-eqz v2, :cond_5

    iget-object v3, p0, Libl;->e:[Lhgj;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    if-eqz v5, :cond_4

    const/4 v6, 0x5

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    iget-object v2, p0, Libl;->f:[Libo;

    if-eqz v2, :cond_7

    iget-object v2, p0, Libl;->f:[Libo;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    if-eqz v4, :cond_6

    const/4 v5, 0x6

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    iget-object v1, p0, Libl;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Libl;->J:I

    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Libl;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Libl;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Libl;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Libl;->a:Lgyt;

    if-nez v0, :cond_2

    new-instance v0, Lgyt;

    invoke-direct {v0}, Lgyt;-><init>()V

    iput-object v0, p0, Libl;->a:Lgyt;

    :cond_2
    iget-object v0, p0, Libl;->a:Lgyt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libl;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libl;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libl;->d:[Lhym;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhym;

    iget-object v3, p0, Libl;->d:[Lhym;

    if-eqz v3, :cond_3

    iget-object v3, p0, Libl;->d:[Lhym;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Libl;->d:[Lhym;

    :goto_2
    iget-object v2, p0, Libl;->d:[Lhym;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Libl;->d:[Lhym;

    new-instance v3, Lhym;

    invoke-direct {v3}, Lhym;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libl;->d:[Lhym;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Libl;->d:[Lhym;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Libl;->d:[Lhym;

    new-instance v3, Lhym;

    invoke-direct {v3}, Lhym;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libl;->d:[Lhym;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libl;->e:[Lhgj;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgj;

    iget-object v3, p0, Libl;->e:[Lhgj;

    if-eqz v3, :cond_6

    iget-object v3, p0, Libl;->e:[Lhgj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Libl;->e:[Lhgj;

    :goto_4
    iget-object v2, p0, Libl;->e:[Lhgj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Libl;->e:[Lhgj;

    new-instance v3, Lhgj;

    invoke-direct {v3}, Lhgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libl;->e:[Lhgj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Libl;->e:[Lhgj;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Libl;->e:[Lhgj;

    new-instance v3, Lhgj;

    invoke-direct {v3}, Lhgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libl;->e:[Lhgj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libl;->f:[Libo;

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Libo;

    iget-object v3, p0, Libl;->f:[Libo;

    if-eqz v3, :cond_9

    iget-object v3, p0, Libl;->f:[Libo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    iput-object v2, p0, Libl;->f:[Libo;

    :goto_6
    iget-object v2, p0, Libl;->f:[Libo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    iget-object v2, p0, Libl;->f:[Libo;

    new-instance v3, Libo;

    invoke-direct {v3}, Libo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libl;->f:[Libo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    iget-object v0, p0, Libl;->f:[Libo;

    array-length v0, v0

    goto :goto_5

    :cond_b
    iget-object v2, p0, Libl;->f:[Libo;

    new-instance v3, Libo;

    invoke-direct {v3}, Libo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libl;->f:[Libo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Libl;->a:Lgyt;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Libl;->a:Lgyt;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v1, p0, Libl;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Libl;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v1, p0, Libl;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Libl;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v1, p0, Libl;->d:[Lhym;

    if-eqz v1, :cond_4

    iget-object v2, p0, Libl;->d:[Lhym;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    if-eqz v4, :cond_3

    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Libl;->e:[Lhgj;

    if-eqz v1, :cond_6

    iget-object v2, p0, Libl;->e:[Lhgj;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    if-eqz v4, :cond_5

    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    iget-object v1, p0, Libl;->f:[Libo;

    if-eqz v1, :cond_8

    iget-object v1, p0, Libl;->f:[Libo;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    if-eqz v3, :cond_7

    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    iget-object v0, p0, Libl;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Libl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Libl;

    iget-object v2, p0, Libl;->a:Lgyt;

    if-nez v2, :cond_4

    iget-object v2, p1, Libl;->a:Lgyt;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Libl;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Libl;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Libl;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Libl;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Libl;->d:[Lhym;

    iget-object v3, p1, Libl;->d:[Lhym;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libl;->e:[Lhgj;

    iget-object v3, p1, Libl;->e:[Lhgj;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libl;->f:[Libo;

    iget-object v3, p1, Libl;->f:[Libo;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libl;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Libl;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Libl;->a:Lgyt;

    iget-object v3, p1, Libl;->a:Lgyt;

    invoke-virtual {v2, v3}, Lgyt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Libl;->b:Ljava/lang/String;

    iget-object v3, p1, Libl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Libl;->c:Ljava/lang/String;

    iget-object v3, p1, Libl;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Libl;->I:Ljava/util/List;

    iget-object v3, p1, Libl;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libl;->a:Lgyt;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libl;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libl;->c:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Libl;->d:[Lhym;

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Libl;->e:[Lhgj;

    if-nez v0, :cond_8

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    iget-object v0, p0, Libl;->f:[Libo;

    if-nez v0, :cond_a

    mul-int/lit8 v2, v2, 0x1f

    :cond_2
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Libl;->I:Ljava/util/List;

    if-nez v2, :cond_c

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_3
    iget-object v0, p0, Libl;->a:Lgyt;

    invoke-virtual {v0}, Lgyt;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Libl;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Libl;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_6
    move v2, v0

    move v0, v1

    :goto_4
    iget-object v3, p0, Libl;->d:[Lhym;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Libl;->d:[Lhym;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v2, p0, Libl;->d:[Lhym;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhym;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_8
    move v0, v1

    :goto_6
    iget-object v3, p0, Libl;->e:[Lhgj;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Libl;->e:[Lhgj;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v1

    :goto_7
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v2, p0, Libl;->e:[Lhgj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgj;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_a
    move v0, v1

    :goto_8
    iget-object v3, p0, Libl;->f:[Libo;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Libl;->f:[Libo;

    aget-object v2, v2, v0

    if-nez v2, :cond_b

    move v2, v1

    :goto_9
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget-object v2, p0, Libl;->f:[Libo;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Libo;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_c
    iget-object v1, p0, Libl;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
