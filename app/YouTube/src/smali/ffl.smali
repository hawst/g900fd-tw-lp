.class public final enum Lffl;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lffl;

.field public static final enum b:Lffl;

.field public static final enum c:Lffl;

.field private static final synthetic e:[Lffl;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 126
    new-instance v0, Lffl;

    const-string v1, "RESULT_TYPE_ANY"

    invoke-direct {v0, v1, v2, v2}, Lffl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffl;->a:Lffl;

    .line 127
    new-instance v0, Lffl;

    const-string v1, "RESULT_TYPE_CHANNEL"

    invoke-direct {v0, v1, v4, v3}, Lffl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffl;->b:Lffl;

    .line 128
    new-instance v0, Lffl;

    const-string v1, "RESULT_TYPE_PLAYLIST"

    invoke-direct {v0, v1, v3, v5}, Lffl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffl;->c:Lffl;

    .line 125
    new-array v0, v5, [Lffl;

    sget-object v1, Lffl;->a:Lffl;

    aput-object v1, v0, v2

    sget-object v1, Lffl;->b:Lffl;

    aput-object v1, v0, v4

    sget-object v1, Lffl;->c:Lffl;

    aput-object v1, v0, v3

    sput-object v0, Lffl;->e:[Lffl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 132
    iput p3, p0, Lffl;->d:I

    .line 133
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lffl;
    .locals 1

    .prologue
    .line 125
    const-class v0, Lffl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lffl;

    return-object v0
.end method

.method public static values()[Lffl;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lffl;->e:[Lffl;

    invoke-virtual {v0}, [Lffl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lffl;

    return-object v0
.end method
