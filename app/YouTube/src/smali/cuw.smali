.class public final enum Lcuw;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcuw;

.field public static final enum b:Lcuw;

.field public static final enum c:Lcuw;

.field public static final enum d:Lcuw;

.field public static final enum e:Lcuw;

.field public static final enum f:Lcuw;

.field public static final enum g:Lcuw;

.field public static final enum h:Lcuw;

.field public static final enum i:Lcuw;

.field public static final enum j:Lcuw;

.field public static final enum k:Lcuw;

.field public static final enum l:Lcuw;

.field private static enum p:Lcuw;

.field private static enum q:Lcuw;

.field private static final synthetic r:[Lcuw;


# instance fields
.field public final m:I

.field public final n:I

.field public final o:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x3

    const/16 v10, 0xa

    const/4 v2, 0x0

    const/4 v9, 0x7

    .line 31
    new-instance v0, Lcuw;

    const-string v1, "NONE"

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcuw;->a:Lcuw;

    .line 32
    new-instance v3, Lcuw;

    const-string v4, "NO_AD_RETURNED_ERROR"

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/16 v8, 0x12c

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->p:Lcuw;

    .line 33
    new-instance v3, Lcuw;

    const-string v4, "VIDEO_PLAYBACK_ERROR_NO_NETWORK"

    const/4 v5, 0x2

    const/16 v8, 0x195

    move v6, v10

    move v7, v9

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->b:Lcuw;

    .line 35
    new-instance v3, Lcuw;

    const-string v4, "VIDEO_PLAYBACK_ERROR_UNKNOWN_HOST"

    const/16 v6, 0xb

    const/16 v8, 0x191

    move v5, v11

    move v7, v9

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->c:Lcuw;

    .line 37
    new-instance v3, Lcuw;

    const-string v4, "VIDEO_PLAYBACK_ERROR_CANNOT_CONNECT"

    const/4 v5, 0x4

    const/16 v6, 0xc

    const/16 v8, 0x191

    move v7, v9

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->d:Lcuw;

    .line 39
    new-instance v3, Lcuw;

    const-string v4, "VIDEO_PLAYBACK_ERROR_TIMEOUT"

    const/4 v5, 0x5

    const/16 v6, 0xd

    const/16 v8, 0x192

    move v7, v11

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->e:Lcuw;

    .line 41
    new-instance v3, Lcuw;

    const-string v4, "VIDEO_PLAYBACK_UNKNOWN_ERROR"

    const/4 v5, 0x6

    const/16 v6, 0xe

    const/16 v8, 0x195

    move v7, v11

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->f:Lcuw;

    .line 43
    new-instance v3, Lcuw;

    const-string v4, "UNSUPPORTED_VIDEO_FORMAT"

    const/16 v6, 0xf

    const/4 v7, 0x6

    const/16 v8, 0x193

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->g:Lcuw;

    .line 45
    new-instance v3, Lcuw;

    const-string v4, "AD_SURVEY_PARSING_ERROR"

    const/16 v6, 0x14

    const/16 v8, 0x384

    move v5, v12

    move v7, v10

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->h:Lcuw;

    .line 47
    new-instance v3, Lcuw;

    const-string v4, "VAST_AD_PARSING_ERROR"

    const/16 v5, 0x9

    const/16 v6, 0x15

    const/16 v8, 0x384

    move v7, v10

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->i:Lcuw;

    .line 49
    new-instance v3, Lcuw;

    const-string v4, "VMAP_AD_PARSING_ERROR"

    const/16 v6, 0x16

    const/16 v7, 0xb

    const/16 v8, 0x384

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->q:Lcuw;

    .line 51
    new-instance v3, Lcuw;

    const-string v4, "VIDEO_INFO_EXCEPTION"

    const/16 v5, 0xb

    const/16 v6, 0x17

    const/16 v8, 0x195

    move v7, v9

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->j:Lcuw;

    .line 53
    new-instance v3, Lcuw;

    const-string v4, "VAST_REQUEST_ERROR"

    const/16 v5, 0xc

    const/16 v6, 0x18

    const/16 v8, 0x12d

    move v7, v12

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->k:Lcuw;

    .line 55
    new-instance v3, Lcuw;

    const-string v4, "VAST_TOO_MANY_WRAPPERS_ERROR"

    const/16 v5, 0xd

    const/16 v6, 0x19

    const/16 v8, 0x12e

    move v7, v12

    invoke-direct/range {v3 .. v8}, Lcuw;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcuw;->l:Lcuw;

    .line 30
    const/16 v0, 0xe

    new-array v0, v0, [Lcuw;

    sget-object v1, Lcuw;->a:Lcuw;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    sget-object v2, Lcuw;->p:Lcuw;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcuw;->b:Lcuw;

    aput-object v2, v0, v1

    sget-object v1, Lcuw;->c:Lcuw;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, Lcuw;->d:Lcuw;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcuw;->e:Lcuw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcuw;->f:Lcuw;

    aput-object v2, v0, v1

    sget-object v1, Lcuw;->g:Lcuw;

    aput-object v1, v0, v9

    sget-object v1, Lcuw;->h:Lcuw;

    aput-object v1, v0, v12

    const/16 v1, 0x9

    sget-object v2, Lcuw;->i:Lcuw;

    aput-object v2, v0, v1

    sget-object v1, Lcuw;->q:Lcuw;

    aput-object v1, v0, v10

    const/16 v1, 0xb

    sget-object v2, Lcuw;->j:Lcuw;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcuw;->k:Lcuw;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcuw;->l:Lcuw;

    aput-object v2, v0, v1

    sput-object v0, Lcuw;->r:[Lcuw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput p3, p0, Lcuw;->m:I

    .line 64
    iput p4, p0, Lcuw;->n:I

    .line 65
    iput p5, p0, Lcuw;->o:I

    .line 66
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcuw;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcuw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcuw;

    return-object v0
.end method

.method public static values()[Lcuw;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcuw;->r:[Lcuw;

    invoke-virtual {v0}, [Lcuw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcuw;

    return-object v0
.end method
