.class public final Lbym;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldck;


# instance fields
.field a:Lbwa;

.field public final b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field public final c:Ljava/util/LinkedList;

.field final d:Lfxe;

.field final e:Lgku;

.field public final f:Lfeb;

.field final g:Lfew;

.field final h:Lctk;

.field public final i:Leyt;

.field final j:Lbyr;

.field final k:Lbyt;

.field final l:Lbqg;

.field final m:Ljava/util/concurrent/atomic/AtomicReference;

.field private n:Lbzb;

.field private final o:Lgix;

.field private final p:Lcub;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfxe;Lfeb;Lfew;Lcuo;Lctk;Lgix;Lcub;Leyt;Levn;Lbyr;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/Executor;Lbwa;)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 126
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctk;

    iput-object v0, p0, Lbym;->h:Lctk;

    .line 128
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbym;->o:Lgix;

    .line 129
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcub;

    iput-object v0, p0, Lbym;->p:Lcub;

    .line 130
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lbym;->d:Lfxe;

    .line 131
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfeb;

    iput-object v0, p0, Lbym;->f:Lfeb;

    .line 132
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    iput-object v0, p0, Lbym;->g:Lfew;

    .line 133
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbym;->i:Leyt;

    .line 134
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyr;

    iput-object v0, p0, Lbym;->j:Lbyr;

    .line 135
    invoke-static {p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    invoke-static {p14}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwa;

    iput-object v0, p0, Lbym;->a:Lbwa;

    .line 137
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    invoke-interface {p2}, Lfxe;->g()Lgku;

    move-result-object v0

    iput-object v0, p0, Lbym;->e:Lgku;

    .line 139
    new-instance v0, Lbqg;

    invoke-direct {v0, p1, p2, p9, p10}, Lbqg;-><init>(Landroid/app/Activity;Lfxe;Leyt;Levn;)V

    iput-object v0, p0, Lbym;->l:Lbqg;

    .line 140
    new-instance v0, Lbyt;

    invoke-direct {v0, p0}, Lbyt;-><init>(Lbym;)V

    iput-object v0, p0, Lbym;->k:Lbyt;

    .line 141
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbym;->c:Ljava/util/LinkedList;

    .line 142
    iput-object p12, p0, Lbym;->m:Ljava/util/concurrent/atomic/AtomicReference;

    .line 143
    invoke-virtual {p0}, Lbym;->a()V

    .line 144
    return-void
.end method

.method public static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 388
    if-eqz p0, :cond_0

    .line 389
    invoke-virtual {p0, p1}, Landroid/view/View;->setSelected(Z)V

    .line 390
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    .line 392
    :cond_0
    return-void

    .line 390
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbym;->a(Lbzb;)V

    .line 161
    return-void
.end method

.method public a(Lbzb;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    iput-object p1, p0, Lbym;->n:Lbzb;

    .line 181
    iget-object v0, p0, Lbym;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 182
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    invoke-static {v1, v3}, Lbym;->a(Landroid/view/View;Z)V

    .line 183
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v3}, Lbym;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 185
    :cond_0
    return-void
.end method

.method a(Ljava/lang/String;Lflh;)V
    .locals 3

    .prologue
    .line 322
    iget-object v0, p0, Lbym;->k:Lbyt;

    iput-object p1, v0, Lbyt;->a:Ljava/lang/String;

    iput-object p2, v0, Lbyt;->b:Lflh;

    .line 323
    iget-object v0, p0, Lbym;->o:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lbym;->k:Lbyt;

    invoke-virtual {v0}, Lbyt;->a()V

    .line 343
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Lbym;->p:Lcub;

    iget-object v1, p0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lbyo;

    invoke-direct {v2, p0}, Lbyo;-><init>(Lbym;)V

    invoke-virtual {v0, v1, v2}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lgbu;)V
    .locals 4

    .prologue
    .line 350
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 351
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    iget-object v0, p0, Lbym;->o:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    .line 354
    new-instance v0, Lbys;

    invoke-direct {v0, p0, p2}, Lbys;-><init>(Lbym;Lgbu;)V

    .line 355
    iget-object v1, p0, Lbym;->g:Lfew;

    invoke-virtual {v1}, Lfew;->a()Lfex;

    move-result-object v1

    .line 358
    sget-object v2, Lfhy;->a:[B

    invoke-virtual {v1, v2}, Lfex;->a([B)V

    .line 359
    iget-object v2, p2, Lgbu;->a:Ljava/lang/String;

    iput-object v2, v1, Lfex;->a:Ljava/lang/String;

    .line 360
    new-instance v2, Lhrv;

    invoke-direct {v2}, Lhrv;-><init>()V

    const/4 v3, 0x1

    iput v3, v2, Lhrv;->d:I

    iput-object p1, v2, Lhrv;->b:Ljava/lang/String;

    iget-object v3, v1, Lfex;->b:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    iget-object v2, p0, Lbym;->g:Lfew;

    invoke-virtual {v2, v1, v0}, Lfew;->a(Lfex;Lwv;)V

    .line 362
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lbym;->n:Lbzb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbym;->n:Lbzb;

    .line 396
    iget-object v0, v0, Lbzb;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 251
    iget-object v0, p0, Lbym;->n:Lbzb;

    if-nez v0, :cond_0

    .line 252
    const-string v0, "Share video without action target."

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 269
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lbym;->n:Lbzb;

    iget-object v0, v0, Lbzb;->b:Ljava/lang/String;

    .line 256
    if-nez v0, :cond_1

    .line 257
    const-string v0, "Share video without valid title."

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 260
    :cond_1
    iget-object v1, p0, Lbym;->n:Lbzb;

    iget-object v1, v1, Lbzb;->a:Ljava/lang/String;

    .line 261
    iget-object v2, p0, Lbym;->n:Lbzb;

    iget-object v2, v2, Lbzb;->c:Lfmn;

    .line 263
    if-nez v2, :cond_2

    .line 264
    iget-object v2, p0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v1}, La;->I(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v2, v0, v1}, Ldol;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 266
    :cond_2
    new-instance v0, Lbfe;

    invoke-direct {v0, v2}, Lbfe;-><init>(Lfmn;)V

    .line 267
    iget-object v1, p0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Lt;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbfe;->a(Lt;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 273
    iget-object v0, p0, Lbym;->n:Lbzb;

    if-nez v0, :cond_0

    .line 274
    const-string v0, "Flag without action target."

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 298
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lbym;->o:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lbym;->a:Lbwa;

    invoke-virtual {v0}, Lbwa;->a()V

    goto :goto_0

    .line 281
    :cond_1
    iget-object v0, p0, Lbym;->p:Lcub;

    iget-object v1, p0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lbyn;

    invoke-direct {v2, p0}, Lbyn;-><init>(Lbym;)V

    invoke-virtual {v0, v1, v2}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lbym;->n:Lbzb;

    if-nez v0, :cond_0

    .line 307
    const-string v0, "Add to without action target."

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 311
    :goto_0
    return-void

    .line 310
    :cond_0
    iget-object v0, p0, Lbym;->n:Lbzb;

    iget-object v0, v0, Lbzb;->a:Ljava/lang/String;

    iget-object v1, p0, Lbym;->n:Lbzb;

    iget-object v1, v1, Lbzb;->d:Lflh;

    invoke-virtual {p0, v0, v1}, Lbym;->a(Ljava/lang/String;Lflh;)V

    goto :goto_0
.end method
