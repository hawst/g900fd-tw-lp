.class public final Lifo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/concurrent/ScheduledExecutorService;

.field public final b:Landroid/app/Activity;

.field public final c:Landroid/content/BroadcastReceiver;

.field private d:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lifp;

    invoke-direct {v0}, Lifp;-><init>()V

    .line 40
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lifo;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lifo;->d:Ljava/util/concurrent/ScheduledFuture;

    .line 43
    new-instance v0, Lifq;

    invoke-direct {v0, p0}, Lifq;-><init>(Lifo;)V

    iput-object v0, p0, Lifo;->c:Landroid/content/BroadcastReceiver;

    .line 46
    iput-object p1, p0, Lifo;->b:Landroid/app/Activity;

    .line 47
    invoke-virtual {p0}, Lifo;->a()V

    .line 48
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 51
    invoke-virtual {p0}, Lifo;->b()V

    .line 52
    iget-object v0, p0, Lifo;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    :try_start_0
    iget-object v0, p0, Lifo;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lifn;

    iget-object v2, p0, Lifo;->b:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lifn;-><init>(Landroid/app/Activity;)V

    const-wide/16 v2, 0x12c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lifo;->d:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lifo;->d:Ljava/util/concurrent/ScheduledFuture;

    .line 76
    if-eqz v0, :cond_0

    .line 77
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lifo;->d:Ljava/util/concurrent/ScheduledFuture;

    .line 80
    :cond_0
    return-void
.end method
