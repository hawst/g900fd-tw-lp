.class public final Ldiz;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "SourceFile"


# instance fields
.field private final a:Lefc;

.field private b:Lefg;


# direct methods
.method public constructor <init>(Lefc;Lefg;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    .line 29
    iput-object p1, p0, Ldiz;->a:Lefc;

    .line 30
    iput-object p2, p0, Ldiz;->b:Lefg;

    .line 32
    const-string v0, "contentType cannot be empty."

    invoke-static {p3, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldiz;->setContentType(Ljava/lang/String;)V

    .line 33
    return-void
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 37
    new-instance v0, Lefd;

    iget-object v1, p0, Ldiz;->a:Lefc;

    iget-object v2, p0, Ldiz;->b:Lefg;

    invoke-direct {v0, v1, v2}, Lefd;-><init>(Lefc;Lefg;)V

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Ldiz;->b:Lefg;

    iget-wide v0, v0, Lefg;->e:J

    return-wide v0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 57
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-virtual {p0}, Ldiz;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 60
    :try_start_0
    invoke-static {v0, p1}, La;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 63
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 64
    return-void

    .line 62
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 63
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    throw v1
.end method
