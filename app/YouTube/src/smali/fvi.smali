.class public final Lfvi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/widget/ImageView;

.field private final b:Leyp;

.field private final c:Leym;

.field private final d:Lfvj;

.field private e:Lfnc;

.field private f:Lfnb;


# direct methods
.method public constructor <init>(Leyp;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Leyn;

    .line 66
    invoke-virtual {p2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Leyn;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0, p1, v0, p2, v1}, Lfvi;-><init>(Leyp;Leym;Landroid/widget/ImageView;Z)V

    .line 69
    return-void
.end method

.method public constructor <init>(Leyp;Landroid/widget/ImageView;Z)V
    .locals 2

    .prologue
    .line 54
    new-instance v0, Leyn;

    .line 56
    invoke-virtual {p2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Leyn;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 54
    invoke-direct {p0, p1, v0, p2, v1}, Lfvi;-><init>(Leyp;Leym;Landroid/widget/ImageView;Z)V

    .line 59
    return-void
.end method

.method public constructor <init>(Leyp;Leym;Landroid/widget/ImageView;Z)V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lfvi;->b:Leyp;

    .line 77
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leym;

    iput-object v0, p0, Lfvi;->c:Leym;

    .line 78
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    .line 79
    new-instance v1, Lfvj;

    if-nez p4, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, p0, v0}, Lfvj;-><init>(Lfvi;Z)V

    iput-object v1, p0, Lfvi;->d:Lfvj;

    .line 80
    if-eqz p4, :cond_0

    .line 81
    iget-object v0, p0, Lfvi;->d:Lfvj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lfvj;->a(Lfvj;Leyo;)V

    .line 83
    :cond_0
    return-void

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/net/Uri;Leyo;)V
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lfvi;->b:Leyp;

    iget-object v1, p0, Lfvi;->c:Leym;

    iget-object v2, p0, Lfvi;->a:Landroid/widget/ImageView;

    invoke-static {v0, v1, p1, v2, p2}, Leyi;->a(Leyp;Leym;Landroid/net/Uri;Landroid/widget/ImageView;Leyo;)V

    .line 205
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p0}, Lfvi;->b()V

    .line 163
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 164
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 91
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    return-void
.end method

.method public final a(Landroid/net/Uri;Leyo;)V
    .locals 0

    .prologue
    .line 157
    invoke-virtual {p0}, Lfvi;->b()V

    .line 158
    invoke-direct {p0, p1, p2}, Lfvi;->b(Landroid/net/Uri;Leyo;)V

    .line 159
    return-void
.end method

.method public final a(Landroid/widget/ImageView$ScaleType;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 87
    return-void
.end method

.method a(Leyo;)V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lfvi;->e:Lfnc;

    if-nez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    .line 178
    iget-object v1, p0, Lfvi;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    .line 179
    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    .line 180
    :cond_1
    iget-object v0, p0, Lfvi;->d:Lfvj;

    invoke-static {v0, p1}, Lfvj;->a(Lfvj;Leyo;)V

    goto :goto_0

    .line 184
    :cond_2
    iget-object v2, p0, Lfvi;->e:Lfnc;

    .line 185
    invoke-virtual {v2, v0, v1}, Lfnc;->a(II)Lfnb;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_3

    iget-object v1, p0, Lfvi;->f:Lfnb;

    invoke-virtual {v0, v1}, Lfnb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 187
    :cond_3
    iput-object v0, p0, Lfvi;->f:Lfnb;

    .line 188
    iget-object v0, p0, Lfvi;->f:Lfnb;

    if-eqz v0, :cond_5

    .line 189
    iget-object v0, p0, Lfvi;->f:Lfnb;

    iget-object v0, v0, Lfnb;->a:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, Lfvi;->b(Landroid/net/Uri;Leyo;)V

    .line 195
    :cond_4
    :goto_1
    iget-object v0, p0, Lfvi;->d:Lfvj;

    invoke-static {v0}, Lfvj;->a(Lfvj;)V

    goto :goto_0

    .line 191
    :cond_5
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final a(Lfnc;Leyo;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lfvi;->e:Lfnc;

    if-eq p1, v0, :cond_0

    .line 118
    iput-object p1, p0, Lfvi;->e:Lfnc;

    .line 120
    iput-object v1, p0, Lfvi;->f:Lfnb;

    .line 121
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    iget-object v0, p0, Lfvi;->d:Lfvj;

    invoke-static {v0}, Lfvj;->a(Lfvj;)V

    .line 125
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lfnc;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 139
    :cond_1
    :goto_0
    return-void

    .line 133
    :cond_2
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 134
    iget-object v0, p0, Lfvi;->d:Lfvj;

    invoke-static {v0, p2}, Lfvj;->a(Lfvj;Leyo;)V

    goto :goto_0

    .line 138
    :cond_3
    invoke-virtual {p0, p2}, Lfvi;->a(Leyo;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 167
    iget-object v0, p0, Lfvi;->d:Lfvj;

    invoke-static {v0}, Lfvj;->a(Lfvj;)V

    .line 168
    iput-object v1, p0, Lfvi;->e:Lfnc;

    .line 169
    iput-object v1, p0, Lfvi;->f:Lfnb;

    .line 170
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 95
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lfvi;->b()V

    .line 143
    iget-object v0, p0, Lfvi;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 144
    return-void
.end method
