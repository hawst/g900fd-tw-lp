.class public final Lbpo;
.super Lbqd;
.source "SourceFile"


# static fields
.field private static final a:Lbpo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1053
    new-instance v0, Lbpo;

    invoke-direct {v0}, Lbpo;-><init>()V

    sput-object v0, Lbpo;->a:Lbpo;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1050
    invoke-direct {p0}, Lbqd;-><init>()V

    return-void
.end method

.method public static synthetic a()Lbpo;
    .locals 1

    .prologue
    .line 1050
    sget-object v0, Lbpo;->a:Lbpo;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Lfkq;
    .locals 3

    .prologue
    .line 1050
    check-cast p1, Lfjb;

    iget-object v0, p1, Lfjb;->g:Lfkp;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfjb;->a:Lhdt;

    iget-object v0, v0, Lhdt;->m:Lhnh;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfjb;->a:Lhdt;

    iget-object v0, v0, Lhdt;->m:Lhnh;

    iget-object v0, v0, Lhnh;->a:Lhne;

    if-eqz v0, :cond_0

    new-instance v0, Lfkp;

    iget-object v1, p1, Lfjb;->a:Lhdt;

    iget-object v1, v1, Lhdt;->m:Lhnh;

    iget-object v1, v1, Lhnh;->a:Lhne;

    invoke-direct {v0, v1, p1}, Lfkp;-><init>(Lhne;Lfqh;)V

    iput-object v0, p1, Lfjb;->g:Lfkp;

    :cond_0
    iget-object v0, p1, Lfjb;->g:Lfkp;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lfkp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkq;

    iget-object v2, v0, Lfkq;->a:Lhut;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lhut;->g:Lhgq;

    if-eqz v2, :cond_1

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic b(Ljava/lang/Object;)Lhut;
    .locals 5

    .prologue
    .line 1050
    check-cast p1, Lfjb;

    iget-object v0, p1, Lfjb;->a:Lhdt;

    iget-object v2, v0, Lhdt;->k:[Lhut;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget-object v4, v0, Lhut;->e:Lhfr;

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final synthetic c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1050
    check-cast p1, Lfjb;

    invoke-virtual {p1}, Lfjb;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic d(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1050
    check-cast p1, Lfjb;

    iget-object v0, p1, Lfjb;->a:Lhdt;

    iget-object v0, v0, Lhdt;->a:Ljava/lang/String;

    return-object v0
.end method
