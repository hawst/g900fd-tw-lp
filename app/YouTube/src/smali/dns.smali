.class public final Ldns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldna;


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/app/NotificationManager;

.field private final d:Lorg/apache/http/client/HttpClient;

.field private final e:Lfxe;

.field private final f:Lgix;

.field private final g:Lgjm;

.field private final h:Ljava/lang/String;

.field private final i:Z

.field private final j:Ljava/lang/String;

.field private final k:Lgad;

.field private final l:Ldnb;

.field private final m:Ljava/util/concurrent/Executor;

.field private final n:Ljava/lang/Object;

.field private volatile o:Z

.field private volatile p:Lorg/apache/http/client/methods/HttpUriRequest;

.field private volatile q:Z

.field private r:J

.field private s:J

.field private final t:Landroid/os/ConditionVariable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-string v0, "bytes=(\\d+)-(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Ldns;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfxe;Lgix;Lgad;Lgjm;Ldnb;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldns;->b:Landroid/content/Context;

    .line 134
    const-string v0, "notification"

    .line 135
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Ldns;->c:Landroid/app/NotificationManager;

    .line 136
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Ldns;->d:Lorg/apache/http/client/HttpClient;

    .line 137
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Ldns;->m:Ljava/util/concurrent/Executor;

    .line 138
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Ldns;->e:Lfxe;

    .line 139
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Ldns;->f:Lgix;

    .line 140
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgad;

    iput-object v0, p0, Ldns;->k:Lgad;

    .line 141
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjm;

    iput-object v0, p0, Ldns;->g:Lgjm;

    .line 142
    iget-object v0, p7, Lgjm;->g:Lgje;

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, v3}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldns;->h:Ljava/lang/String;

    .line 149
    iget-object v0, p7, Lgjm;->g:Lgje;

    const-string v1, "tracking_account_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Ldns;->i:Z

    .line 150
    iget-object v0, p7, Lgjm;->g:Lgje;

    const-string v1, "account_id"

    invoke-virtual {v0, v1, v3}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldns;->j:Ljava/lang/String;

    .line 151
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnb;

    iput-object v0, p0, Ldns;->l:Ldnb;

    .line 152
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldns;->n:Ljava/lang/Object;

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldns;->o:Z

    .line 154
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Ldns;->t:Landroid/os/ConditionVariable;

    .line 155
    return-void
.end method

.method static synthetic a(Ldns;J)J
    .locals 1

    .prologue
    .line 93
    iput-wide p1, p0, Ldns;->s:J

    return-wide p1
.end method

.method private a()Lorg/apache/http/HttpResponse;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 337
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    iget-object v1, p0, Ldns;->g:Lgjm;

    iget-object v1, v1, Lgjm;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 338
    const-string v1, "Content-Range"

    const-string v2, "bytes */*"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-object v1, p0, Ldns;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 340
    :try_start_0
    iput-object v0, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 341
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 343
    :try_start_1
    iget-object v0, p0, Ldns;->d:Lorg/apache/http/client/HttpClient;

    iget-object v1, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 351
    :goto_0
    return-object v0

    .line 341
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 344
    :catch_0
    move-exception v0

    .line 345
    invoke-direct {p0}, Ldns;->b()V

    .line 346
    new-instance v1, Ldmt;

    invoke-direct {v1, v0, v3}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    throw v1

    .line 347
    :catch_1
    move-exception v0

    .line 348
    iget-object v1, p0, Ldns;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 349
    :try_start_3
    iget-object v2, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 350
    const-string v0, "Range request was aborted"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 351
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 355
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 353
    :cond_0
    :try_start_4
    iget-object v2, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 354
    new-instance v2, Ldmt;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
.end method

.method private a(Ljava/io/InputStream;JZ)Lorg/apache/http/HttpResponse;
    .locals 12

    .prologue
    .line 285
    iget-boolean v0, p0, Ldns;->q:Z

    if-eqz v0, :cond_0

    .line 286
    const/4 v0, 0x0

    .line 328
    :goto_0
    return-object v0

    .line 288
    :cond_0
    const-wide/16 v0, 0x1

    add-long v2, p2, v0

    .line 289
    iget-wide v0, p0, Ldns;->r:J

    const-wide/16 v4, 0x1

    sub-long v4, v0, v4

    if-eqz p4, :cond_1

    const/4 v0, 0x0

    :goto_1
    int-to-long v0, v0

    sub-long v0, v4, v0

    .line 291
    if-nez p4, :cond_2

    cmp-long v4, v2, v0

    if-lez v4, :cond_2

    .line 292
    new-instance v0, Lorg/apache/http/message/BasicHttpResponse;

    new-instance v1, Lorg/apache/http/message/BasicStatusLine;

    sget-object v2, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    const/16 v3, 0x134

    const-string v4, "Already uploaded all possible content for a gated upload."

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/message/BasicStatusLine;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/http/message/BasicHttpResponse;-><init>(Lorg/apache/http/StatusLine;)V

    goto :goto_0

    .line 289
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 296
    :cond_2
    iget-object v4, p0, Ldns;->l:Ldnb;

    iget-object v5, p0, Ldns;->g:Lgjm;

    iget-object v5, v5, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v4, v5, p2, p3}, Ldnb;->b(Ljava/lang/String;J)V

    .line 297
    iget-object v4, p0, Ldns;->l:Ldnb;

    iget-object v5, p0, Ldns;->g:Lgjm;

    iget-object v5, v5, Lgjm;->a:Ljava/lang/String;

    iget-wide v6, p0, Ldns;->r:J

    invoke-interface {v4, v5, v6, v7}, Ldnb;->a(Ljava/lang/String;J)V

    .line 299
    new-instance v8, Lorg/apache/http/client/methods/HttpPut;

    iget-object v4, p0, Ldns;->g:Lgjm;

    iget-object v4, v4, Lgjm;->b:Ljava/lang/String;

    invoke-direct {v8, v4}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 300
    const-string v4, "Content-Type"

    const-string v5, "application/octet-stream"

    invoke-virtual {v8, v4, v5}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const-string v4, "Content-Range"

    const-string v5, "bytes %d-%d/%d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 302
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v0, 0x2

    iget-wide v10, p0, Ldns;->r:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 301
    invoke-virtual {v8, v4, v0}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v2

    .line 306
    :goto_2
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_3

    .line 307
    :try_start_0
    invoke-virtual {p1, v0, v1}, Ljava/io/InputStream;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    sub-long/2addr v0, v4

    goto :goto_2

    .line 309
    :catch_0
    move-exception v0

    .line 310
    new-instance v1, Ldmt;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    throw v1

    .line 314
    :cond_3
    :try_start_1
    new-instance v1, Ldnu;

    iget-wide v4, p0, Ldns;->r:J

    sub-long v2, v4, v2

    if-eqz p4, :cond_4

    const/4 v0, 0x0

    :goto_3
    int-to-long v4, v0

    sub-long v4, v2, v4

    move-object v2, p0

    move-object v3, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Ldnu;-><init>(Ldns;Ljava/io/InputStream;JJ)V

    .line 316
    invoke-virtual {v8, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 317
    iget-object v1, p0, Ldns;->n:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 318
    :try_start_2
    iput-object v8, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 319
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 320
    :try_start_3
    iget-object v0, p0, Ldns;->d:Lorg/apache/http/client/HttpClient;

    iget-object v1, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v0

    goto/16 :goto_0

    .line 314
    :cond_4
    const/4 v0, 0x1

    goto :goto_3

    .line 319
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 321
    :catch_1
    move-exception v0

    .line 322
    invoke-direct {p0}, Ldns;->b()V

    .line 323
    new-instance v1, Ldmt;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    throw v1

    .line 324
    :catch_2
    move-exception v0

    .line 325
    iget-object v1, p0, Ldns;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 326
    :try_start_6
    iget-object v2, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 327
    const-string v0, "Upload request was aborted"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 328
    const/4 v0, 0x0

    monitor-exit v1

    goto/16 :goto_0

    .line 332
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 330
    :cond_5
    :try_start_7
    iget-object v2, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 331
    new-instance v2, Ldmt;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1
.end method

.method static synthetic a(Ldns;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ldns;->b()V

    return-void
.end method

.method private a(Lgcd;)V
    .locals 9

    .prologue
    const v6, 0x7f090110

    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 478
    iget-object v0, p1, Lgcd;->B:Lgch;

    sget-object v2, Lgch;->a:Lgch;

    if-eq v0, v2, :cond_0

    iget-object v0, p1, Lgcd;->B:Lgch;

    sget-object v2, Lgch;->b:Lgch;

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 479
    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_5

    add-int/lit8 v2, v0, 0x1

    const-wide/16 v4, 0x7530

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    invoke-direct {p0}, Ldns;->c()Z
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    if-nez v0, :cond_3

    .line 482
    :cond_1
    :goto_2
    invoke-direct {p0}, Ldns;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 483
    iget-object v0, p0, Ldns;->b:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Ldns;->b:Landroid/content/Context;

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    iget-object v7, p1, Lgcd;->b:Ljava/lang/String;

    invoke-static {v7}, La;->I(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v6, p0, Ldns;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    move-result v6

    const/high16 v7, 0x10000000

    or-int/2addr v6, v7

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v6, "authenticate"

    invoke-virtual {v3, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v6, "uploader_notification"

    invoke-virtual {v3, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v6, p0, Ldns;->b:Landroid/content/Context;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v6, v1, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v3, Landroid/app/Notification;

    const v6, 0x7f0201a6

    invoke-direct {v3, v6, v0, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget-object v0, p0, Ldns;->b:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p1, Lgcd;->j:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v2, v4, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v0, 0x10

    iput v0, v3, Landroid/app/Notification;->flags:I

    iget-object v0, p0, Ldns;->c:Landroid/app/NotificationManager;

    iget-object v1, p1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v8, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 485
    :cond_2
    return-void

    .line 479
    :cond_3
    :try_start_2
    invoke-static {}, Leud;->a()Leud;

    move-result-object v0

    iget-object v3, p0, Ldns;->e:Lfxe;

    iget-object v4, p1, Lgcd;->b:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Lfxe;->a(Ljava/lang/String;Leuc;)V

    invoke-virtual {v0}, Leud;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    iget-object v3, v0, Lgcd;->B:Lgch;

    sget-object v4, Lgch;->b:Lgch;

    if-eq v3, v4, :cond_4

    iget-object v0, v0, Lgcd;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "Upload transcoding finished"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    const-string v0, "Upload streams not found yet"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    move v0, v2

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    const-string v0, "Upload streams not found, polling aborted"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_1
    move-exception v0

    goto/16 :goto_1
.end method

.method private static a(Lorg/apache/http/HttpResponse;)V
    .locals 1

    .prologue
    .line 375
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 376
    if-eqz v0, :cond_0

    .line 377
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 379
    :cond_0
    return-void
.end method

.method static synthetic b(Ldns;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ldns;->n:Ljava/lang/Object;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 360
    iget-object v1, p0, Ldns;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 365
    :try_start_0
    iget-wide v2, p0, Ldns;->r:J

    iget-object v0, p0, Ldns;->g:Lgjm;

    iget-object v0, v0, Lgjm;->g:Lgje;

    const-string v4, "metadata_updated"

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    int-to-long v4, v0

    sub-long/2addr v2, v4

    .line 367
    iget-object v0, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v4, p0, Ldns;->s:J

    cmp-long v0, v4, v2

    if-gez v0, :cond_0

    .line 369
    iget-object v0, p0, Ldns;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 371
    :cond_0
    monitor-exit v1

    return-void

    .line 365
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Lorg/apache/http/HttpResponse;)V
    .locals 16

    .prologue
    .line 426
    move-object/from16 v0, p0

    iget-object v2, v0, Ldns;->l:Ldnb;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldns;->g:Lgjm;

    iget-object v3, v3, Lgjm;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v4, v0, Ldns;->r:J

    invoke-interface {v2, v3, v4, v5}, Ldnb;->b(Ljava/lang/String;J)V

    .line 428
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ldns;->k:Lgad;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lgad;->b(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lgcd;

    move-object v12, v0

    .line 429
    move-object/from16 v0, p0

    iget-object v2, v0, Ldns;->g:Lgjm;

    iget-object v2, v2, Lgjm;->h:Lgje;

    const-string v3, "video_id"

    iget-object v4, v12, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    invoke-direct/range {p0 .. p0}, Ldns;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 432
    move-object/from16 v0, p0

    iget-object v2, v0, Ldns;->l:Ldnb;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldns;->g:Lgjm;

    iget-object v3, v3, Lgjm;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v4, v0, Ldns;->r:J

    invoke-interface {v2, v3, v4, v5}, Ldnb;->b(Ljava/lang/String;J)V

    .line 434
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ldns;->g:Lgjm;

    iget-object v2, v2, Lgjm;->g:Lgje;

    const-string v3, "metadata_updated"

    invoke-virtual {v2, v3}, Lgje;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lfax; {:try_start_0 .. :try_end_0} :catch_2

    move-result v2

    if-eqz v2, :cond_3

    .line 435
    :try_start_1
    const-string v2, "Executing metadata update"

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    invoke-static {}, Leud;->a()Leud;

    move-result-object v13

    invoke-direct/range {p0 .. p0}, Ldns;->c()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "Error updating metadata, auth is null"

    invoke-static {v2}, Lezp;->b(Ljava/lang/String;)V

    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Ldns;->a(Lgcd;)V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lfax; {:try_start_1 .. :try_end_1} :catch_2

    .line 444
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ldns;->e:Lfxe;

    invoke-interface {v2}, Lfxe;->b()V

    .line 445
    move-object/from16 v0, p0

    iget-object v2, v0, Ldns;->g:Lgjm;

    iget-object v2, v2, Lgjm;->g:Lgje;

    const-string v3, "upload_start_time_millis"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lgje;->b(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 446
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ldns;->l:Ldnb;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldns;->g:Lgjm;

    iget-object v3, v3, Lgjm;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Ldns;->g:Lgjm;

    iget-object v4, v4, Lgjm;->h:Lgje;

    invoke-interface {v2, v3, v4}, Ldnb;->a(Ljava/lang/String;Lgje;)V

    .line 447
    return-void

    .line 435
    :cond_2
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Ldns;->e:Lfxe;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldns;->g:Lgjm;

    iget-object v3, v3, Lgjm;->g:Lgje;

    const-string v4, "upload_title"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Ldns;->g:Lgjm;

    iget-object v4, v4, Lgjm;->g:Lgje;

    const-string v5, "upload_description"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v12, Lgcd;->t:Ljava/lang/String;

    iget-object v6, v12, Lgcd;->u:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Ldns;->g:Lgjm;

    iget-object v7, v7, Lgjm;->g:Lgje;

    const-string v8, "upload_keywords"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Ldns;->g:Lgjm;

    iget-object v8, v8, Lgjm;->g:Lgje;

    const-string v9, "upload_privacy"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lgcg;->valueOf(Ljava/lang/String;)Lgcg;

    move-result-object v8

    iget-object v9, v12, Lgcd;->y:Ljava/util/Map;

    iget-object v10, v12, Lgcd;->z:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Ldns;->g:Lgjm;

    iget-object v11, v11, Lgjm;->g:Lgje;

    const-string v14, "upload_location"

    const/4 v15, 0x0

    invoke-virtual {v11, v14, v15}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, v12, Lgcd;->h:Landroid/net/Uri;

    invoke-interface/range {v2 .. v13}, Lfxe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgcg;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Leuc;)V

    invoke-virtual {v13}, Leud;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgcd;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lfax; {:try_start_2 .. :try_end_2} :catch_2

    move-object v12, v2

    goto/16 :goto_0

    :catch_0
    move-exception v2

    :try_start_3
    new-instance v2, Ldmt;

    const-string v3, "Error updating video metadata after upload"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Ldmt;-><init>(Ljava/lang/String;Z)V

    throw v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lfax; {:try_start_3 .. :try_end_3} :catch_2

    .line 439
    :catch_1
    move-exception v2

    .line 440
    const-string v3, "error parsing uploaded video"

    invoke-static {v3, v2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 437
    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Ldns;->a(Lgcd;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lfax; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    .line 441
    :catch_2
    move-exception v2

    .line 442
    const-string v3, "error parsing uploaded video"

    invoke-static {v3, v2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method static synthetic c(Ldns;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Ldns;->r:J

    return-wide v0
.end method

.method private c()Z
    .locals 3

    .prologue
    .line 522
    iget-object v0, p0, Ldns;->f:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    .line 524
    invoke-virtual {v0}, Lgit;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 525
    iget-object v1, v0, Lgit;->b:Lgiv;

    invoke-virtual {v1}, Lgiv;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldns;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Ldns;->i:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldns;->j:Ljava/lang/String;

    .line 526
    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Ldns;)Z
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ldns;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Ldns;)Lgjm;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ldns;->g:Lgjm;

    return-object v0
.end method

.method static synthetic f(Ldns;)Ldnb;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ldns;->l:Ldnb;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldns;->q:Z

    .line 188
    iget-boolean v0, p0, Ldns;->o:Z

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Ldns;->m:Ljava/util/concurrent/Executor;

    new-instance v1, Ldnt;

    invoke-direct {v1, p0}, Ldnt;-><init>(Ldns;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 191
    :cond_0
    iget-object v0, p0, Ldns;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 192
    return-void
.end method

.method public final run()V
    .locals 13

    .prologue
    const/16 v12, 0xc8

    const-wide/16 v8, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 160
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ldns;->g:Lgjm;

    iget-object v4, v4, Lgjm;->a:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x13

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Upload starting ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lezp;->e(Ljava/lang/String;)V

    .line 161
    iget-boolean v3, p0, Ldns;->q:Z

    if-eqz v3, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldns;->g:Lgjm;

    iget-object v1, v1, Lgjm;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2c

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Upload cancelled before the task started ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldmt; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 181
    :cond_0
    :goto_0
    iget-object v0, p0, Ldns;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 182
    :goto_1
    return-void

    .line 161
    :cond_1
    :try_start_1
    iget-object v3, p0, Ldns;->g:Lgjm;

    iget-object v3, v3, Lgjm;->g:Lgje;

    const-string v4, "upload_trim_start_us"

    const-wide/16 v6, -0x1

    invoke-virtual {v3, v4, v6, v7}, Lgje;->b(Ljava/lang/String;J)J

    move-result-wide v4

    iget-object v3, p0, Ldns;->g:Lgjm;

    iget-object v3, v3, Lgjm;->g:Lgje;

    const-string v6, "upload_trim_end_us"

    const-wide/16 v10, -0x1

    invoke-virtual {v3, v6, v10, v11}, Lgje;->b(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v3, v4, v8

    if-eqz v3, :cond_3

    cmp-long v3, v6, v8

    if-eqz v3, :cond_3

    :goto_2
    iget-object v2, p0, Ldns;->g:Lgjm;

    iget-object v2, v2, Lgjm;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    if-eqz v0, :cond_4

    iget-object v1, p0, Ldns;->b:Landroid/content/Context;

    new-instance v0, Lgqw;

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v7}, Lgqw;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;JJ)V

    move-object v2, v0

    :goto_3
    invoke-interface {v2}, Lgcn;->a()J

    move-result-wide v0

    iput-wide v0, p0, Ldns;->r:J

    iget-object v0, p0, Ldns;->l:Ldnb;

    iget-object v1, p0, Ldns;->g:Lgjm;

    iget-object v1, v1, Lgjm;->a:Ljava/lang/String;

    iget-wide v4, p0, Ldns;->r:J

    invoke-interface {v0, v1, v4, v5}, Ldnb;->a(Ljava/lang/String;J)V

    iget-boolean v0, p0, Ldns;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldns;->o:Z

    invoke-direct {p0}, Ldns;->a()Lorg/apache/http/HttpResponse;

    move-result-object v0

    iget-boolean v1, p0, Ldns;->q:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v3, 0x134

    if-ne v1, v3, :cond_f

    invoke-static {v0}, Ldns;->a(Lorg/apache/http/HttpResponse;)V

    const-string v1, "range"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-nez v0, :cond_b

    move-wide v0, v8

    :goto_4
    iget-object v3, p0, Ldns;->n:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ldmt; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iput-wide v0, p0, Ldns;->s:J

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-interface {v2}, Lgcn;->b()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, p0, Ldns;->g:Lgjm;

    iget-object v3, v3, Lgjm;->g:Lgje;

    const-string v4, "metadata_updated"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v3

    invoke-direct {p0, v2, v0, v1, v3}, Ldns;->a(Ljava/io/InputStream;JZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    iget-boolean v1, p0, Ldns;->q:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    if-eq v1, v12, :cond_2

    const/16 v2, 0xc9

    if-ne v1, v2, :cond_d

    :cond_2
    invoke-direct {p0, v0}, Ldns;->b(Lorg/apache/http/HttpResponse;)V
    :try_end_3
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ldmt; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 163
    :try_start_4
    const-string v1, "failure uploading"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 164
    iget-object v1, p0, Ldns;->l:Ldnb;

    iget-object v2, p0, Ldns;->g:Lgjm;

    iget-object v2, v2, Lgjm;->a:Ljava/lang/String;

    new-instance v3, Ldmt;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Ldnb;->a(Ljava/lang/String;Ldmt;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 181
    iget-object v0, p0, Ldns;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    :cond_3
    move v0, v2

    .line 161
    goto/16 :goto_2

    :cond_4
    :try_start_5
    iget-object v6, p0, Ldns;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v3, "content"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "file"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lgcm;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_8

    new-instance v0, Ljava/io/IOException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot resolve as content uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ldmt; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 165
    :catch_1
    move-exception v0

    .line 166
    :try_start_6
    const-string v1, "FATAL failure uploading"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 167
    iget-object v1, p0, Ldns;->l:Ldnb;

    iget-object v2, p0, Ldns;->g:Lgjm;

    iget-object v2, v2, Lgjm;->a:Ljava/lang/String;

    new-instance v3, Ldmt;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Ldnb;->a(Ljava/lang/String;Ldmt;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 181
    iget-object v0, p0, Ldns;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    :cond_7
    move-object v1, v2

    .line 161
    :cond_8
    :try_start_7
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_size"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_9

    new-instance v0, Ljava/io/IOException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Upload cursor is null: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ldmt; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 168
    :catch_2
    move-exception v0

    .line 169
    :try_start_8
    const-string v1, "failure uploading"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170
    iget-object v1, p0, Ldns;->l:Ldnb;

    iget-object v2, p0, Ldns;->g:Lgjm;

    iget-object v2, v2, Lgjm;->a:Ljava/lang/String;

    new-instance v3, Ldmt;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Ldnb;->a(Ljava/lang/String;Ldmt;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 181
    iget-object v0, p0, Ldns;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    .line 161
    :cond_9
    :try_start_9
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_a

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    new-instance v0, Ljava/io/IOException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Upload cursor is empty: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_9
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ldmt; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 171
    :catch_3
    move-exception v0

    .line 172
    :try_start_a
    const-string v1, "failure uploading"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 173
    iget-object v1, p0, Ldns;->l:Ldnb;

    iget-object v2, p0, Ldns;->g:Lgjm;

    iget-object v2, v2, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ldnb;->a(Ljava/lang/String;Ldmt;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 181
    iget-object v0, p0, Ldns;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    .line 161
    :cond_a
    const/4 v2, 0x0

    :try_start_b
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    new-instance v0, Lgcm;

    invoke-direct {v0, v6, v1, v2, v3}, Lgcm;-><init>(Landroid/content/Context;Landroid/net/Uri;J)V

    move-object v2, v0

    goto/16 :goto_3

    :cond_b
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ldns;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_c

    new-instance v1, Ljava/io/IOException;

    const-string v2, "malformed range header=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_b
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ldmt; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 174
    :catch_4
    move-exception v0

    .line 178
    :try_start_c
    const-string v1, "FATAL failure uploading"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 179
    iget-object v1, p0, Ldns;->l:Ldnb;

    iget-object v2, p0, Ldns;->g:Lgjm;

    iget-object v2, v2, Lgjm;->a:Ljava/lang/String;

    new-instance v3, Ldmt;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, Ldmt;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Ldnb;->a(Ljava/lang/String;Ldmt;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 181
    iget-object v0, p0, Ldns;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    .line 161
    :cond_c
    const/4 v0, 0x2

    :try_start_d
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_d
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_d .. :try_end_d} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2
    .catch Ldmt; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    move-result-wide v0

    goto/16 :goto_4

    :catchall_0
    move-exception v0

    :try_start_e
    monitor-exit v3
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :try_start_f
    throw v0
    :try_end_f
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_f .. :try_end_f} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_f .. :try_end_f} :catch_1
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ldmt; {:try_start_f .. :try_end_f} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 181
    :catchall_1
    move-exception v0

    iget-object v1, p0, Ldns;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    throw v0

    .line 161
    :cond_d
    const/16 v2, 0x134

    if-ne v1, v2, :cond_e

    :try_start_10
    invoke-static {v0}, Ldns;->a(Lorg/apache/http/HttpResponse;)V

    iget-object v0, p0, Ldns;->g:Lgjm;

    iget-object v0, v0, Lgjm;->g:Lgje;

    const-string v1, "metadata_updated"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Ldns;->s:J

    iget-wide v2, p0, Ldns;->r:J

    const-wide/16 v4, 0x2

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Ldmt;

    const-string v1, "upload request got http status: 308"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ldmt;-><init>(Ljava/lang/String;Z)V

    throw v0

    :cond_e
    invoke-static {v0}, Ldns;->a(Lorg/apache/http/HttpResponse;)V

    new-instance v0, Ldmt;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "upload request got http status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ldmt;-><init>(Ljava/lang/String;Z)V

    throw v0

    :cond_f
    if-eq v1, v12, :cond_10

    const/16 v2, 0xc9

    if-ne v1, v2, :cond_11

    :cond_10
    invoke-direct {p0, v0}, Ldns;->b(Lorg/apache/http/HttpResponse;)V

    goto/16 :goto_0

    :cond_11
    invoke-static {v0}, Ldns;->a(Lorg/apache/http/HttpResponse;)V

    new-instance v0, Ldmt;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "range request got http status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ldmt;-><init>(Ljava/lang/String;Z)V

    throw v0
    :try_end_10
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2
    .catch Ldmt; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_1
.end method
