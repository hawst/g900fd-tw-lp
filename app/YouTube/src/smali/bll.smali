.class final Lbll;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgmm;


# instance fields
.field private synthetic a:Lbla;


# direct methods
.method constructor <init>(Lbla;)V
    .locals 0

    .prologue
    .line 1425
    iput-object p1, p0, Lbll;->a:Lbla;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1456
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v0, v0, Lbla;->o:Lgmw;

    invoke-interface {v0}, Lgmw;->c()V

    .line 1457
    return-void
.end method

.method public final a(Lfrl;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1430
    invoke-virtual {p1}, Lfrl;->q()Lflg;

    move-result-object v0

    .line 1431
    if-nez v0, :cond_1

    .line 1447
    :cond_0
    :goto_0
    return-void

    .line 1437
    :cond_1
    iget v0, v0, Lflg;->d:I

    int-to-long v0, v0

    .line 1438
    iget-object v2, p0, Lbll;->a:Lbla;

    iget-object v2, v2, Lbla;->f:Lbjx;

    iget-object v3, p0, Lbll;->a:Lbla;

    iget-object v3, v3, Lbla;->d:Lgit;

    invoke-virtual {v2, v3}, Lbjx;->a(Lgit;)J

    move-result-wide v2

    .line 1442
    cmp-long v4, v0, v6

    if-lez v4, :cond_0

    cmp-long v4, v2, v6

    if-eqz v4, :cond_2

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 1445
    :cond_2
    iget-object v2, p0, Lbll;->a:Lbla;

    iget-object v2, v2, Lbla;->o:Lgmw;

    invoke-interface {v2, v0, v1}, Lgmw;->a(J)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1451
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v0, v0, Lbla;->j:Lgmt;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0, p1}, Lgmt;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lgmt;->a(Ljava/io/File;)V

    .line 1452
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1484
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v0, v0, Lbla;->j:Lgmt;

    iget-object v1, v0, Lgmt;->a:Landroid/content/Context;

    iget-object v2, v0, Lgmt;->d:Lexn;

    iget-object v3, v0, Lgmt;->b:Lgit;

    iget-object v3, v3, Lgit;->c:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lgmt;->a(Landroid/content/Context;Lexn;Ljava/lang/String;)V

    iget-object v1, v0, Lgmt;->e:Lgmu;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lgmt;->e:Lgmu;

    invoke-virtual {v0}, Lgmu;->f()V

    .line 1487
    :cond_0
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v0, v0, Lbla;->o:Lgmw;

    invoke-interface {v0}, Lgmw;->c()V

    .line 1488
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1462
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v0, v0, Lbla;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1463
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v0, v0, Lbla;->m:Lbmd;

    invoke-virtual {v0, p1}, Lbmd;->b(Ljava/lang/String;)V

    .line 1464
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v0, v0, Lbla;->j:Lgmt;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0, p1}, Lgmt;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lgmt;->a(Ljava/io/File;)V

    .line 1465
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1492
    iget-object v0, p0, Lbll;->a:Lbla;

    new-instance v1, Lbln;

    invoke-direct {v1}, Lbln;-><init>()V

    invoke-virtual {v0, v1}, Lbla;->a(Ljava/lang/Object;)V

    .line 1493
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1469
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v0, v0, Lbla;->j:Lgmt;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0, p1}, Lgmt;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1470
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1475
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v1, p0, Lbll;->a:Lbla;

    invoke-virtual {v1, p1}, Lbla;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbla;->v(Ljava/lang/String;)V

    .line 1476
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v1, p0, Lbll;->a:Lbla;

    invoke-virtual {v1, p1}, Lbla;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbla;->v(Ljava/lang/String;)V

    .line 1479
    iget-object v0, p0, Lbll;->a:Lbla;

    iget-object v3, v0, Lbla;->p:Lbjm;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, v3, Lbjm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legc;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Legc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v3, Lbjm;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Legc;

    if-eqz v1, :cond_2

    invoke-interface {v1, v0}, Legc;->a(Ljava/lang/String;)Ljava/util/NavigableSet;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Legi;

    invoke-interface {v1, v2}, Legc;->b(Legi;)V

    goto :goto_0

    .line 1480
    :cond_3
    return-void
.end method
