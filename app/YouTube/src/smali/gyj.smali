.class public final Lgyj;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgyj;


# instance fields
.field public b:Lgyv;

.field public c:Lhil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2631
    const/4 v0, 0x0

    new-array v0, v0, [Lgyj;

    sput-object v0, Lgyj;->a:[Lgyj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2632
    invoke-direct {p0}, Lidf;-><init>()V

    .line 2635
    iput-object v0, p0, Lgyj;->b:Lgyv;

    .line 2638
    iput-object v0, p0, Lgyj;->c:Lhil;

    .line 2632
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 2681
    const/4 v0, 0x0

    .line 2682
    iget-object v1, p0, Lgyj;->b:Lgyv;

    if-eqz v1, :cond_0

    .line 2683
    const v0, 0x4912ecb

    iget-object v1, p0, Lgyj;->b:Lgyv;

    .line 2684
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2686
    :cond_0
    iget-object v1, p0, Lgyj;->c:Lhil;

    if-eqz v1, :cond_1

    .line 2687
    const v1, 0x49b784e

    iget-object v2, p0, Lgyj;->c:Lhil;

    .line 2688
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2690
    :cond_1
    iget-object v1, p0, Lgyj;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2691
    iput v0, p0, Lgyj;->J:I

    .line 2692
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 2628
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyj;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyj;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyj;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgyj;->b:Lgyv;

    if-nez v0, :cond_2

    new-instance v0, Lgyv;

    invoke-direct {v0}, Lgyv;-><init>()V

    iput-object v0, p0, Lgyj;->b:Lgyv;

    :cond_2
    iget-object v0, p0, Lgyj;->b:Lgyv;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgyj;->c:Lhil;

    if-nez v0, :cond_3

    new-instance v0, Lhil;

    invoke-direct {v0}, Lhil;-><init>()V

    iput-object v0, p0, Lgyj;->c:Lhil;

    :cond_3
    iget-object v0, p0, Lgyj;->c:Lhil;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2489765a -> :sswitch_1
        0x24dbc272 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 2669
    iget-object v0, p0, Lgyj;->b:Lgyv;

    if-eqz v0, :cond_0

    .line 2670
    const v0, 0x4912ecb

    iget-object v1, p0, Lgyj;->b:Lgyv;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2672
    :cond_0
    iget-object v0, p0, Lgyj;->c:Lhil;

    if-eqz v0, :cond_1

    .line 2673
    const v0, 0x49b784e

    iget-object v1, p0, Lgyj;->c:Lhil;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2675
    :cond_1
    iget-object v0, p0, Lgyj;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 2677
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2650
    if-ne p1, p0, :cond_1

    .line 2655
    :cond_0
    :goto_0
    return v0

    .line 2651
    :cond_1
    instance-of v2, p1, Lgyj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 2652
    :cond_2
    check-cast p1, Lgyj;

    .line 2653
    iget-object v2, p0, Lgyj;->b:Lgyv;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyj;->b:Lgyv;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgyj;->c:Lhil;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyj;->c:Lhil;

    if-nez v2, :cond_3

    .line 2654
    :goto_2
    iget-object v2, p0, Lgyj;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgyj;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 2655
    goto :goto_0

    .line 2653
    :cond_4
    iget-object v2, p0, Lgyj;->b:Lgyv;

    iget-object v3, p1, Lgyj;->b:Lgyv;

    invoke-virtual {v2, v3}, Lgyv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyj;->c:Lhil;

    iget-object v3, p1, Lgyj;->c:Lhil;

    .line 2654
    invoke-virtual {v2, v3}, Lhil;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgyj;->I:Ljava/util/List;

    iget-object v3, p1, Lgyj;->I:Ljava/util/List;

    .line 2655
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2659
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2661
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyj;->b:Lgyv;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2662
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyj;->c:Lhil;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2663
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgyj;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2664
    return v0

    .line 2661
    :cond_0
    iget-object v0, p0, Lgyj;->b:Lgyv;

    invoke-virtual {v0}, Lgyv;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2662
    :cond_1
    iget-object v0, p0, Lgyj;->c:Lhil;

    invoke-virtual {v0}, Lhil;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2663
    :cond_2
    iget-object v1, p0, Lgyj;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
