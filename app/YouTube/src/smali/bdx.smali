.class public abstract Lbdx;
.super Lj;
.source "SourceFile"


# instance fields
.field public W:Lcan;

.field private a:Ljava/util/Set;

.field public b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lj;-><init>()V

    return-void
.end method

.method public static G()Z
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public C()Z
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method

.method public D()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public final E()Lcom/google/android/apps/youtube/app/YouTubeApplication;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lbdx;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    return-object v0
.end method

.method public F()Lhog;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lbdx;->z()Lbgh;

    move-result-object v0

    iget-object v0, v0, Lbgh;->b:Landroid/os/Bundle;

    invoke-static {v0}, Lbgh;->a(Landroid/os/Bundle;)Lhog;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 44
    invoke-super {p0, p1}, Lj;->a(Landroid/app/Activity;)V

    .line 45
    check-cast p1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object p1, p0, Lbdx;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 46
    return-void
.end method

.method protected final a(Lezl;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lbdx;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbdx;->a:Ljava/util/Set;

    .line 39
    :cond_0
    iget-object v0, p0, Lbdx;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method public b()Lcan;
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lbdx;->W:Lcan;

    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lbdx;->a()Ljava/lang/CharSequence;

    move-result-object v0

    .line 74
    if-nez v0, :cond_1

    .line 75
    iget-object v0, p0, Lbdx;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    iput-object v0, p0, Lbdx;->W:Lcan;

    .line 80
    :cond_0
    :goto_0
    iget-object v0, p0, Lbdx;->W:Lcan;

    return-object v0

    .line 77
    :cond_1
    iget-object v0, p0, Lbdx;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    invoke-virtual {v0}, Lcaq;->h()Lcar;

    move-result-object v0

    invoke-virtual {p0}, Lbdx;->a()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lcar;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcar;->a()Lcaq;

    move-result-object v0

    iput-object v0, p0, Lbdx;->W:Lcan;

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method public t()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lj;->t()V

    .line 51
    iget-object v0, p0, Lbdx;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i()V

    .line 52
    return-void
.end method

.method public u()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lj;->u()V

    .line 57
    iget-object v0, p0, Lbdx;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    invoke-interface {v0}, Lcao;->l()V

    .line 58
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lj;->v()V

    .line 63
    iget-object v0, p0, Lbdx;->a:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lbdx;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezl;

    .line 65
    invoke-interface {v0}, Lezl;->d()V

    goto :goto_0

    .line 67
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbdx;->a:Ljava/util/Set;

    .line 69
    :cond_1
    return-void
.end method

.method public z()Lbgh;
    .locals 1

    .prologue
    .line 126
    invoke-static {p0}, La;->a(Lbdx;)Lbgh;

    move-result-object v0

    return-object v0
.end method
