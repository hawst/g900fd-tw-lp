.class public final Lgdd;
.super Lecc;
.source "SourceFile"


# instance fields
.field private final a:Lebz;

.field private final b:Lebw;

.field private final c:[Leby;

.field private d:I

.field private e:I

.field private f:Z

.field private h:J

.field private i:J

.field private j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

.field private k:Lgdb;

.field private l:F


# direct methods
.method public constructor <init>(Lebz;)V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Lecc;-><init>()V

    .line 110
    sget v0, Legz;->a:I

    const/16 v3, 0x9

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 111
    iput-object p1, p0, Lgdd;->a:Lebz;

    .line 112
    new-instance v0, Lebw;

    invoke-direct {v0}, Lebw;-><init>()V

    iput-object v0, p0, Lgdd;->b:Lebw;

    .line 114
    new-array v0, v4, [Leby;

    iput-object v0, p0, Lgdd;->c:[Leby;

    .line 115
    :goto_1
    iget-object v0, p0, Lgdd;->c:[Leby;

    if-ge v2, v4, :cond_1

    .line 116
    iget-object v0, p0, Lgdd;->c:[Leby;

    new-instance v3, Leby;

    invoke-direct {v3, v1}, Leby;-><init>(I)V

    aput-object v3, v0, v2

    .line 115
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 110
    goto :goto_0

    .line 118
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lgdd;->l:F

    .line 119
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 274
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->b()V

    .line 278
    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    invoke-direct {v0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;-><init>()V

    iput-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;
    :try_end_0
    .catch Lgde; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    new-instance v0, Lgdb;

    iget-object v1, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    iget-object v2, p0, Lgdd;->b:Lebw;

    iget-object v2, v2, Lebw;->a:Lebv;

    invoke-direct {v0, v1, v2}, Lgdb;-><init>(Lgdc;Lebv;)V

    iput-object v0, p0, Lgdd;->k:Lgdb;

    .line 285
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    iget v0, p0, Lgdd;->l:F

    invoke-static {v0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->a(F)V

    .line 286
    iget v0, p0, Lecc;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 287
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->a()V

    .line 289
    :cond_1
    :goto_0
    return-void

    .line 280
    :catch_0
    move-exception v0

    const-string v0, "Couldn\'t create the native player."

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 307
    iput v0, p0, Lgdd;->d:I

    .line 308
    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_1

    .line 309
    iget-object v1, p0, Lgdd;->c:[Leby;

    aget-object v1, v1, v0

    iget-object v1, v1, Leby;->b:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 310
    iget-object v1, p0, Lgdd;->c:[Leby;

    aget-object v1, v1, v0

    iget-object v1, v1, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 308
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 313
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 317
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 318
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lgdd;->l:F

    iget-object v1, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->a(F)V

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    invoke-super {p0, p1, p2}, Lecc;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected final a(J)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lgdd;->a:Lebz;

    invoke-interface {v0, p1, p2}, Lebz;->b(J)V

    .line 193
    invoke-direct {p0}, Lgdd;->j()V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgdd;->f:Z

    .line 195
    iput-wide p1, p0, Lgdd;->h:J

    .line 196
    iput-wide p1, p0, Lgdd;->i:J

    .line 197
    invoke-direct {p0}, Lgdd;->i()V

    .line 198
    return-void
.end method

.method protected final a(JJ)V
    .locals 13

    .prologue
    .line 218
    :try_start_0
    iget-object v0, p0, Lgdd;->a:Lebz;

    invoke-interface {v0, p1, p2}, Lebz;->a(J)Z

    .line 219
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_2

    .line 220
    :cond_0
    iget-object v1, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    iget-object v0, v1, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, v1, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->b:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->enqueueEos()V

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->d:Z

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    .line 222
    :cond_2
    iget-boolean v0, p0, Lgdd;->f:Z

    if-nez v0, :cond_3

    iget-wide v0, p0, Lgdd;->i:J

    invoke-virtual {p0}, Lgdd;->f()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x4c4b40

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_8

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_2

    .line 225
    return-void

    .line 220
    :cond_4
    invoke-static {}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->getUnusedBuffer()Ljava/nio/ByteBuffer;

    move-result-object v2

    if-nez v2, :cond_5

    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :goto_3
    iget-object v0, v1, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/lit16 v0, v0, 0xbc

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    if-gt v0, v3, :cond_6

    iget-object v0, v1, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 223
    :catch_0
    move-exception v0

    .line 224
    new-instance v1, Leat;

    invoke-direct {v1, v0}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 220
    :cond_6
    :try_start_1
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    invoke-static {v2, v0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->enqueueBuffer(Ljava/nio/ByteBuffer;I)V

    const/4 v0, 0x1

    goto :goto_0

    .line 222
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lgdd;->a:Lebz;

    iget v1, p0, Lgdd;->e:I

    invoke-virtual {p0}, Lgdd;->f()J

    move-result-wide v2

    iget-object v4, p0, Lgdd;->b:Lebw;

    iget-object v5, p0, Lgdd;->c:[Leby;

    iget v6, p0, Lgdd;->d:I

    aget-object v5, v5, v6

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lebz;->a(IJLebw;Leby;Z)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_4
    const/4 v0, 0x1

    goto :goto_2

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_2

    :pswitch_1
    invoke-direct {p0}, Lgdd;->i()V

    goto :goto_4

    :pswitch_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdd;->f:Z

    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->b:Z

    const/4 v0, 0x0

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lgdd;->c:[Leby;

    iget v1, p0, Lgdd;->d:I

    aget-object v0, v0, v1

    iget-wide v0, v0, Leby;->e:J

    iput-wide v0, p0, Lgdd;->i:J

    iget v0, p0, Lgdd;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgdd;->d:I

    iget v0, p0, Lgdd;->d:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_b

    iget-object v1, p0, Lgdd;->k:Lgdb;

    iget-object v2, p0, Lgdd;->c:[Leby;

    iget v3, p0, Lgdd;->d:I

    new-array v4, v3, [[B

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v3, :cond_9

    aget-object v5, v2, v0

    iget-object v6, v1, Lgdb;->b:Lebv;

    invoke-static {v5, v6}, Lgcz;->a(Leby;Lebv;)[B

    move-result-object v5

    aput-object v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_9
    invoke-static {v4}, La;->a([[B)[B

    move-result-object v0

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-wide v2, v2, Leby;->e:J

    const-wide/16 v4, 0x9

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x64

    div-long/2addr v2, v4

    array-length v4, v0

    add-int/lit8 v4, v4, 0x8

    const/16 v5, 0xe

    new-array v5, v5, [B

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput-byte v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput-byte v7, v5, v6

    const/4 v6, 0x2

    const/4 v7, 0x1

    aput-byte v7, v5, v6

    const/4 v6, 0x3

    const/16 v7, -0x40

    aput-byte v7, v5, v6

    const/4 v6, 0x4

    shr-int/lit8 v7, v4, 0x8

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    const/4 v6, 0x5

    int-to-byte v4, v4

    aput-byte v4, v5, v6

    const/4 v4, 0x6

    const/16 v6, -0x80

    aput-byte v6, v5, v4

    const/4 v4, 0x7

    const/16 v6, -0x80

    aput-byte v6, v5, v4

    const/16 v4, 0x8

    const/4 v6, 0x5

    aput-byte v6, v5, v4

    const/16 v4, 0x9

    const-wide/16 v6, 0x21

    const/16 v8, 0x1e

    shr-long v8, v2, v8

    or-long/2addr v6, v8

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v5, v4

    const/16 v4, 0xa

    const/16 v6, 0x16

    shr-long v6, v2, v6

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v5, v4

    const/16 v4, 0xb

    const-wide/16 v6, 0x1

    const/16 v8, 0xe

    shr-long v8, v2, v8

    const-wide/16 v10, 0xfe

    and-long/2addr v8, v10

    or-long/2addr v6, v8

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v5, v4

    const/16 v4, 0xc

    const/4 v6, 0x7

    shr-long v6, v2, v6

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v5, v4

    const/16 v4, 0xd

    const/4 v6, 0x1

    shl-long v6, v2, v6

    const-wide/16 v8, 0xfe

    and-long/2addr v6, v8

    const-wide/16 v8, 0x1

    or-long/2addr v6, v8

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v5, v4

    const/4 v4, 0x2

    new-array v4, v4, [[B

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v4}, La;->a([[B)[B

    move-result-object v4

    iget-object v0, v1, Lgdb;->a:Lgdc;

    const/4 v5, 0x0

    const/16 v6, 0xb0

    invoke-static {v4, v5, v6}, La;->a([BII)[B

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v1, v5, v2, v3, v6}, Lgdb;->a([BJZ)[B

    move-result-object v2

    invoke-interface {v0, v2}, Lgdc;->a([B)V

    const/16 v0, 0xb0

    :goto_6
    array-length v2, v4

    if-ge v0, v2, :cond_a

    const/16 v2, 0xb6

    array-length v3, v4

    sub-int/2addr v3, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v1, Lgdb;->a:Lgdc;

    invoke-static {v4, v0, v2}, La;->a([BII)[B

    move-result-object v2

    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v6, v7, v5}, Lgdb;->a([BJZ)[B

    move-result-object v2

    invoke-interface {v3, v2}, Lgdc;->a([B)V

    add-int/lit16 v0, v0, 0xb6

    goto :goto_6

    :cond_a
    invoke-direct {p0}, Lgdd;->j()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_b
    const/4 v0, 0x1

    goto/16 :goto_2

    :pswitch_4
    const/4 v0, 0x1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch -0x5
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(JZ)V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lgdd;->a:Lebz;

    iget v1, p0, Lgdd;->e:I

    invoke-interface {v0, v1, p1, p2}, Lebz;->a(IJ)V

    .line 144
    invoke-direct {p0}, Lgdd;->j()V

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgdd;->f:Z

    .line 146
    iput-wide p1, p0, Lgdd;->h:J

    .line 147
    iput-wide p1, p0, Lgdd;->i:J

    .line 148
    return-void
.end method

.method protected final a()Z
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x1

    return v0
.end method

.method protected final b()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->a()V

    .line 205
    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 209
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    iget-boolean v0, v0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    invoke-static {v1}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->setPlaybackState(Z)V

    .line 212
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 210
    goto :goto_0
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    iget-boolean v0, v0, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->d:Z

    goto :goto_0
.end method

.method protected final e()Z
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final f()J
    .locals 4

    .prologue
    .line 166
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    invoke-static {}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->c()J

    move-result-wide v0

    .line 171
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 172
    iput-wide v0, p0, Lgdd;->h:J

    .line 175
    :cond_0
    iget-wide v0, p0, Lgdd;->h:J

    return-wide v0
.end method

.method protected final g()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lgdd;->a:Lebz;

    iget v1, p0, Lgdd;->e:I

    invoke-interface {v0, v1}, Lebz;->b(I)V

    .line 153
    return-void
.end method

.method protected final h()I
    .locals 2

    .prologue
    .line 124
    :try_start_0
    iget-object v0, p0, Lgdd;->a:Lebz;

    invoke-interface {v0}, Lebz;->a()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgdd;->a:Lebz;

    invoke-interface {v1}, Lebz;->b()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 132
    iget-object v1, p0, Lgdd;->a:Lebz;

    invoke-interface {v1, v0}, Lebz;->a(I)Lecb;

    move-result-object v1

    iget-object v1, v1, Lecb;->a:Ljava/lang/String;

    invoke-static {v1}, La;->o(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iput v0, p0, Lgdd;->e:I

    .line 134
    const/4 v0, 0x1

    .line 138
    :goto_1
    return v0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    new-instance v1, Leat;

    invoke-direct {v1, v0}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 131
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected final l()V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;->b()V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lgdd;->j:Lcom/google/android/libraries/youtube/media/m2ts/NativeM2TSPlayer;

    .line 161
    :cond_0
    iget-object v0, p0, Lgdd;->a:Lebz;

    invoke-interface {v0}, Lebz;->d()V

    .line 162
    return-void
.end method

.method protected final m()J
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lgdd;->a:Lebz;

    iget v1, p0, Lgdd;->e:I

    invoke-interface {v0, v1}, Lebz;->a(I)Lecb;

    move-result-object v0

    iget-wide v0, v0, Lecb;->b:J

    return-wide v0
.end method

.method protected final n()J
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lgdd;->a:Lebz;

    invoke-interface {v0}, Lebz;->c()J

    move-result-wide v0

    .line 186
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x3

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0}, Lgdd;->f()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method
