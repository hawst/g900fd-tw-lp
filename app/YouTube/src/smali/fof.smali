.class public final Lfof;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfqh;


# instance fields
.field public final a:Lics;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/util/List;

.field public d:[Ljava/lang/CharSequence;

.field public e:[Ljava/lang/CharSequence;

.field private final f:Lfqh;

.field private g:Ljava/lang/CharSequence;

.field private h:Lfin;


# direct methods
.method public constructor <init>(Lics;Lfqh;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lics;

    iput-object v0, p0, Lfof;->a:Lics;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lfof;->f:Lfqh;

    .line 40
    return-void
.end method

.method public static a([Lhgz;Lfhz;)[Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 122
    array-length v0, p0

    if-nez v0, :cond_0

    .line 123
    const/4 v0, 0x0

    .line 132
    :goto_0
    return-object v0

    .line 125
    :cond_0
    array-length v0, p0

    new-array v2, v0, [Ljava/lang/CharSequence;

    move v0, v1

    .line 126
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_1

    .line 127
    aget-object v3, p0, v0

    invoke-static {v3, p1, v1}, Lfvo;->a(Lhgz;Lfhz;Z)Landroid/text/Spanned;

    move-result-object v3

    aput-object v3, v2, v0

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 132
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lfof;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lfof;->a:Lics;

    iget-object v0, v0, Lics;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfof;->g:Ljava/lang/CharSequence;

    .line 92
    :cond_0
    iget-object v0, p0, Lfof;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Lfin;
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lfof;->h:Lfin;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfof;->a:Lics;

    iget-object v0, v0, Lics;->d:Licr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfof;->a:Lics;

    iget-object v0, v0, Lics;->d:Licr;

    iget-object v0, v0, Licr;->a:Lhaq;

    if-eqz v0, :cond_0

    .line 97
    new-instance v0, Lfin;

    iget-object v1, p0, Lfof;->a:Lics;

    iget-object v1, v1, Lics;->d:Licr;

    iget-object v1, v1, Licr;->a:Lhaq;

    invoke-direct {v0, v1, p0}, Lfin;-><init>(Lhaq;Lfqh;)V

    iput-object v0, p0, Lfof;->h:Lfin;

    .line 99
    :cond_0
    iget-object v0, p0, Lfof;->h:Lfin;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lfof;->a:Lics;

    iget-object v0, v0, Lics;->e:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lfof;->f:Lfqh;

    return-object v0
.end method
