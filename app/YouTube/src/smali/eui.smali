.class public abstract Leui;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;


# static fields
.field private static final a:Ljava/util/concurrent/LinkedBlockingQueue;


# instance fields
.field private final b:Leuc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Leui;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method

.method public constructor <init>(Leuc;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leuc;

    iput-object v0, p0, Leui;->b:Leuc;

    .line 32
    return-void
.end method

.method private static a()Leuj;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Leui;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leuj;

    .line 57
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Leuj;

    invoke-direct {v0}, Leuj;-><init>()V

    goto :goto_0
.end method

.method static synthetic a(Leuj;)V
    .locals 2

    .prologue
    .line 22
    :try_start_0
    sget-object v0, Leui;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Interrupted when releasing runnable to the queue"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Leui;->a()Leuj;

    move-result-object v0

    .line 44
    iget-object v1, p0, Leui;->b:Leuc;

    iput-object v1, v0, Leuj;->a:Leuc;

    iput-object p1, v0, Leuj;->b:Ljava/lang/Object;

    iput-object p2, v0, Leuj;->d:Ljava/lang/Exception;

    const/4 v1, 0x0

    iput-object v1, v0, Leuj;->c:Ljava/lang/Object;

    const/4 v1, 0x0

    iput-boolean v1, v0, Leuj;->e:Z

    .line 45
    invoke-virtual {p0, v0}, Leui;->a(Ljava/lang/Runnable;)V

    .line 46
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 36
    invoke-static {}, Leui;->a()Leuj;

    move-result-object v0

    .line 37
    iget-object v1, p0, Leui;->b:Leuc;

    iput-object v1, v0, Leuj;->a:Leuc;

    iput-object p1, v0, Leuj;->b:Ljava/lang/Object;

    iput-object p2, v0, Leuj;->c:Ljava/lang/Object;

    const/4 v1, 0x0

    iput-object v1, v0, Leuj;->d:Ljava/lang/Exception;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leuj;->e:Z

    .line 38
    invoke-virtual {p0, v0}, Leui;->a(Ljava/lang/Runnable;)V

    .line 39
    return-void
.end method

.method protected abstract a(Ljava/lang/Runnable;)V
.end method
