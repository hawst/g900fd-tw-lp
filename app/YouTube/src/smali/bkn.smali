.class public final Lbkn;
.super Lgng;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lexn;

.field volatile c:Lbla;

.field private final f:Landroid/telephony/TelephonyManager;

.field private final g:Levn;

.field private final h:Lgix;

.field private final i:Ldjf;

.field private final j:Leyp;

.field private final k:Lfxe;

.field private final l:Lgot;

.field private final m:Larh;

.field private final n:Ljava/util/concurrent/Executor;

.field private final o:Lezj;

.field private final p:Lfad;

.field private final q:Lbjx;

.field private final r:Layz;

.field private final s:Lfep;

.field private final t:Lckv;

.field private final u:Ljava/util/HashMap;

.field private final v:Lbkp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Levn;Lgix;Ldjf;Leyp;Lfxe;Lgot;Larh;Ljava/util/concurrent/Executor;Lezj;Lexn;Lbjx;Layz;Lfep;Lckv;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lgng;-><init>()V

    .line 94
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbkn;->a:Landroid/content/Context;

    .line 95
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lbkn;->f:Landroid/telephony/TelephonyManager;

    .line 96
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lbkn;->g:Levn;

    .line 97
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbkn;->h:Lgix;

    .line 98
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldjf;

    iput-object v0, p0, Lbkn;->i:Ldjf;

    .line 99
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbkn;->j:Leyp;

    .line 100
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lbkn;->k:Lfxe;

    .line 101
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgot;

    iput-object v0, p0, Lbkn;->l:Lgot;

    .line 102
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larh;

    iput-object v0, p0, Lbkn;->m:Larh;

    .line 103
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lbkn;->n:Ljava/util/concurrent/Executor;

    .line 104
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lbkn;->o:Lezj;

    .line 105
    invoke-static {p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexn;

    iput-object v0, p0, Lbkn;->b:Lexn;

    .line 106
    invoke-static {p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjx;

    iput-object v0, p0, Lbkn;->q:Lbjx;

    .line 107
    invoke-static {p14}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layz;

    iput-object v0, p0, Lbkn;->r:Layz;

    .line 108
    invoke-static/range {p15 .. p15}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfep;

    iput-object v0, p0, Lbkn;->s:Lfep;

    .line 109
    invoke-static/range {p16 .. p16}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckv;

    iput-object v0, p0, Lbkn;->t:Lckv;

    .line 111
    new-instance v0, Lbkq;

    invoke-direct {v0, p0}, Lbkq;-><init>(Lbkn;)V

    .line 112
    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->a(Landroid/content/Context;Ldmw;)Lfad;

    move-result-object v0

    iput-object v0, p0, Lbkn;->p:Lfad;

    .line 113
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbkn;->u:Ljava/util/HashMap;

    .line 115
    new-instance v0, Lbkp;

    invoke-direct {v0, p0}, Lbkp;-><init>(Lbkn;)V

    iput-object v0, p0, Lbkn;->v:Lbkp;

    .line 117
    invoke-virtual {p3, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 118
    return-void
.end method

.method static a(Landroid/content/Context;Lexn;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 246
    invoke-static {p2}, Lbla;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 247
    invoke-static {p0, p1, p2}, Lgmt;->a(Landroid/content/Context;Lexn;Ljava/lang/String;)V

    .line 248
    return-void
.end method


# virtual methods
.method public final a()Lewi;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lbkn;->v:Lbkp;

    return-object v0
.end method

.method public final a(Lgit;)Lgnd;
    .locals 24

    .prologue
    .line 122
    invoke-static/range {p1 .. p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    invoke-virtual/range {p1 .. p1}, Lgit;->a()Z

    move-result v4

    if-nez v4, :cond_0

    .line 124
    invoke-virtual/range {p0 .. p0}, Lbkn;->c()Lgnd;

    move-result-object v4

    .line 158
    :goto_0
    return-object v4

    .line 127
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lbkn;->u:Ljava/util/HashMap;

    move-object/from16 v23, v0

    monitor-enter v23

    .line 129
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lbkn;->u:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    .line 130
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbla;

    .line 131
    :goto_1
    if-nez v4, :cond_2

    .line 133
    move-object/from16 v0, p0

    iget-object v6, v0, Lbkn;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbkn;->b:Lexn;

    invoke-static/range {p1 .. p1}, Lctv;->a(Lgit;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lbla;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    :try_start_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lgit;->c:Ljava/lang/String;

    invoke-static {v4}, Lbla;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-static {v5, v4}, La;->a(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :try_start_2
    move-object/from16 v0, p1

    iget-object v4, v0, Lgit;->c:Ljava/lang/String;

    invoke-static {v6, v7, v8, v4}, Lgmt;->a(Landroid/content/Context;Lexn;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :goto_2
    new-instance v4, Lbla;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbkn;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbkn;->f:Landroid/telephony/TelephonyManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbkn;->g:Levn;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbkn;->i:Ldjf;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbkn;->j:Leyp;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbkn;->k:Lfxe;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbkn;->l:Lgot;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbkn;->m:Larh;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbkn;->n:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbkn;->o:Lezj;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbkn;->b:Lexn;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbkn;->p:Lfad;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbkn;->q:Lbjx;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbkn;->r:Layz;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbkn;->s:Lfep;

    move-object/from16 v19, v0

    new-instance v20, Lbkl;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbkn;->a:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbkn;->q:Lbjx;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lbkl;-><init>(Landroid/content/Context;Lgit;Lbjx;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lbkn;->t:Lckv;

    move-object/from16 v22, v0

    move-object/from16 v21, p1

    invoke-direct/range {v4 .. v22}, Lbla;-><init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Levn;Ldjf;Leyp;Lfxe;Lgot;Larh;Ljava/util/concurrent/Executor;Lezj;Lexn;Lfad;Lbjx;Layz;Lfep;Lgmw;Lgit;Lckv;)V

    .line 154
    move-object/from16 v0, p0

    iget-object v5, v0, Lbkn;->u:Ljava/util/HashMap;

    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    :cond_2
    monitor-exit v23

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v23
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 130
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 133
    :catch_0
    move-exception v4

    :try_start_3
    const-string v9, "Failed to move legacy database: "

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v9, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_3
    invoke-static {v5, v4}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v6, v7, v8}, Lbkn;->a(Landroid/content/Context;Lexn;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_4
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lbkn;->h:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v1

    .line 169
    invoke-virtual {p0, v1}, Lbkn;->a(Lgit;)Lgnd;

    move-result-object v0

    check-cast v0, Lbla;

    iput-object v0, p0, Lbkn;->c:Lbla;

    .line 170
    iget-object v0, p0, Lbkn;->c:Lbla;

    invoke-virtual {v0}, Lbla;->a()V

    .line 171
    iget-object v0, p0, Lbkn;->g:Levn;

    iget-object v2, p0, Lbkn;->c:Lbla;

    invoke-virtual {v0, v2}, Levn;->a(Ljava/lang/Object;)V

    .line 176
    iget-object v0, p0, Lbkn;->p:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    .line 177
    if-eqz v0, :cond_0

    .line 178
    iget-object v1, v1, Lgit;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldmz;->a(Ljava/lang/String;)V

    .line 180
    :cond_0
    return-void
.end method

.method public final declared-synchronized handleIdentityRemovedEvent(Lfbz;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lfbz;->a:Lgit;

    .line 206
    iget-object v1, p0, Lbkn;->u:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 207
    :try_start_1
    iget-object v2, p0, Lbkn;->u:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    :try_start_2
    iget-object v1, p0, Lbkn;->n:Ljava/util/concurrent/Executor;

    new-instance v2, Lbko;

    invoke-direct {v2, p0, v0}, Lbko;-><init>(Lbkn;Lgit;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216
    monitor-exit p0

    return-void

    .line 208
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 204
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final handleSignInEvent(Lfcb;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 199
    invoke-virtual {p0}, Lbkn;->b()V

    .line 200
    return-void
.end method

.method public final handleSignOutEvent(Lfcc;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lbkn;->p:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    .line 187
    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {v0}, Ldmz;->b()V

    .line 190
    :cond_0
    iget-object v0, p0, Lbkn;->c:Lbla;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lbkn;->g:Levn;

    iget-object v1, p0, Lbkn;->c:Lbla;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 192
    iget-object v0, p0, Lbkn;->c:Lbla;

    invoke-virtual {v0}, Lbla;->b()V

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lbkn;->c:Lbla;

    .line 195
    :cond_1
    return-void
.end method
