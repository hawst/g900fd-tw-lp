.class public final Lbvz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ldxe;

.field public c:Landroid/widget/Toast;

.field public d:Landroid/widget/ProgressBar;

.field public e:Landroid/widget/ImageView;

.field public f:I

.field public g:Ldwq;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldxe;Levn;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbvz;->a:Landroid/content/Context;

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxe;

    iput-object v0, p0, Lbvz;->b:Ldxe;

    .line 49
    invoke-virtual {p3, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 50
    return-void
.end method

.method private onVolumeChanged(Ldxd;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lbvz;->d:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lbvz;->d:Landroid/widget/ProgressBar;

    iget v1, p1, Ldxd;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    iget v0, p1, Ldxd;->a:I

    iput v0, p0, Lbvz;->f:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldwq;)V
    .locals 1

    .prologue
    .line 54
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Lbvz;->g:Ldwq;

    .line 55
    return-void
.end method
