.class public final enum Lehl;
.super Ljava/lang/Enum;


# static fields
.field private static enum A:Lehl;

.field private static enum B:Lehl;

.field private static enum C:Lehl;

.field private static enum D:Lehl;

.field private static enum E:Lehl;

.field private static enum F:Lehl;

.field private static enum G:Lehl;

.field private static enum H:Lehl;

.field private static enum I:Lehl;

.field private static enum J:Lehl;

.field private static enum K:Lehl;

.field private static enum L:Lehl;

.field private static enum M:Lehl;

.field private static enum N:Lehl;

.field private static enum O:Lehl;

.field private static enum P:Lehl;

.field private static enum Q:Lehl;

.field private static enum R:Lehl;

.field private static enum S:Lehl;

.field private static enum T:Lehl;

.field private static final synthetic U:[Lehl;

.field public static final enum a:Lehl;

.field public static final enum b:Lehl;

.field public static final enum c:Lehl;

.field public static final enum d:Lehl;

.field public static final enum e:Lehl;

.field public static final enum f:Lehl;

.field private static enum h:Lehl;

.field private static enum i:Lehl;

.field private static enum j:Lehl;

.field private static enum k:Lehl;

.field private static enum l:Lehl;

.field private static enum m:Lehl;

.field private static enum n:Lehl;

.field private static enum o:Lehl;

.field private static enum p:Lehl;

.field private static enum q:Lehl;

.field private static enum r:Lehl;

.field private static enum s:Lehl;

.field private static enum t:Lehl;

.field private static enum u:Lehl;

.field private static enum v:Lehl;

.field private static enum w:Lehl;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static enum x:Lehl;

.field private static enum y:Lehl;

.field private static enum z:Lehl;


# instance fields
.field public final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lehl;

    const-string v1, "SUCCESS"

    const-string v2, "Ok"

    invoke-direct {v0, v1, v4, v2}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->h:Lehl;

    new-instance v0, Lehl;

    const-string v1, "BAD_AUTHENTICATION"

    const-string v2, "BadAuthentication"

    invoke-direct {v0, v1, v5, v2}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->i:Lehl;

    new-instance v0, Lehl;

    const-string v1, "NEEDS_2F"

    const-string v2, "InvalidSecondFactor"

    invoke-direct {v0, v1, v6, v2}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->j:Lehl;

    new-instance v0, Lehl;

    const-string v1, "NOT_VERIFIED"

    const-string v2, "NotVerified"

    invoke-direct {v0, v1, v7, v2}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->k:Lehl;

    new-instance v0, Lehl;

    const-string v1, "TERMS_NOT_AGREED"

    const-string v2, "TermsNotAgreed"

    invoke-direct {v0, v1, v8, v2}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->l:Lehl;

    new-instance v0, Lehl;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "Unknown"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->m:Lehl;

    new-instance v0, Lehl;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v2, 0x6

    const-string v3, "UNKNOWN_ERR"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->n:Lehl;

    new-instance v0, Lehl;

    const-string v1, "ACCOUNT_DELETED"

    const/4 v2, 0x7

    const-string v3, "AccountDeleted"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->o:Lehl;

    new-instance v0, Lehl;

    const-string v1, "ACCOUNT_DISABLED"

    const/16 v2, 0x8

    const-string v3, "AccountDisabled"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->p:Lehl;

    new-instance v0, Lehl;

    const-string v1, "SERVICE_DISABLED"

    const/16 v2, 0x9

    const-string v3, "ServiceDisabled"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->q:Lehl;

    new-instance v0, Lehl;

    const-string v1, "SERVICE_UNAVAILABLE"

    const/16 v2, 0xa

    const-string v3, "ServiceUnavailable"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->r:Lehl;

    new-instance v0, Lehl;

    const-string v1, "CAPTCHA"

    const/16 v2, 0xb

    const-string v3, "CaptchaRequired"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->s:Lehl;

    new-instance v0, Lehl;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0xc

    const-string v3, "NetworkError"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->t:Lehl;

    new-instance v0, Lehl;

    const-string v1, "USER_CANCEL"

    const/16 v2, 0xd

    const-string v3, "UserCancel"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->u:Lehl;

    new-instance v0, Lehl;

    const-string v1, "PERMISSION_DENIED"

    const/16 v2, 0xe

    const-string v3, "PermissionDenied"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->v:Lehl;

    new-instance v0, Lehl;

    const-string v1, "DEVICE_MANAGEMENT_REQUIRED"

    const/16 v2, 0xf

    const-string v3, "DeviceManagementRequiredOrSyncDisabled"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->w:Lehl;

    new-instance v0, Lehl;

    const-string v1, "DM_INTERNAL_ERROR"

    const/16 v2, 0x10

    const-string v3, "DeviceManagementInternalError"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->x:Lehl;

    new-instance v0, Lehl;

    const-string v1, "DM_SYNC_DISABLED"

    const/16 v2, 0x11

    const-string v3, "DeviceManagementSyncDisabled"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->a:Lehl;

    new-instance v0, Lehl;

    const-string v1, "DM_ADMIN_BLOCKED"

    const/16 v2, 0x12

    const-string v3, "DeviceManagementAdminBlocked"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->b:Lehl;

    new-instance v0, Lehl;

    const-string v1, "DM_ADMIN_PENDING_APPROVAL"

    const/16 v2, 0x13

    const-string v3, "DeviceManagementAdminPendingApproval"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->c:Lehl;

    new-instance v0, Lehl;

    const-string v1, "DM_STALE_SYNC_REQUIRED"

    const/16 v2, 0x14

    const-string v3, "DeviceManagementStaleSyncRequired"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->d:Lehl;

    new-instance v0, Lehl;

    const-string v1, "DM_DEACTIVATED"

    const/16 v2, 0x15

    const-string v3, "DeviceManagementDeactivated"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->e:Lehl;

    new-instance v0, Lehl;

    const-string v1, "DM_REQUIRED"

    const/16 v2, 0x16

    const-string v3, "DeviceManagementRequired"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->f:Lehl;

    new-instance v0, Lehl;

    const-string v1, "CLIENT_LOGIN_DISABLED"

    const/16 v2, 0x17

    const-string v3, "ClientLoginDisabled"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->y:Lehl;

    new-instance v0, Lehl;

    const-string v1, "NEED_PERMISSION"

    const/16 v2, 0x18

    const-string v3, "NeedPermission"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->z:Lehl;

    new-instance v0, Lehl;

    const-string v1, "BAD_PASSWORD"

    const/16 v2, 0x19

    const-string v3, "WeakPassword"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->A:Lehl;

    new-instance v0, Lehl;

    const-string v1, "ALREADY_HAS_GMAIL"

    const/16 v2, 0x1a

    const-string v3, "ALREADY_HAS_GMAIL"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->B:Lehl;

    new-instance v0, Lehl;

    const-string v1, "BAD_REQUEST"

    const/16 v2, 0x1b

    const-string v3, "BadRequest"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->C:Lehl;

    new-instance v0, Lehl;

    const-string v1, "BAD_USERNAME"

    const/16 v2, 0x1c

    const-string v3, "BadUsername"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->D:Lehl;

    new-instance v0, Lehl;

    const-string v1, "LOGIN_FAIL"

    const/16 v2, 0x1d

    const-string v3, "LoginFail"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->E:Lehl;

    new-instance v0, Lehl;

    const-string v1, "NOT_LOGGED_IN"

    const/16 v2, 0x1e

    const-string v3, "NotLoggedIn"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->F:Lehl;

    new-instance v0, Lehl;

    const-string v1, "NO_GMAIL"

    const/16 v2, 0x1f

    const-string v3, "NoGmail"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->G:Lehl;

    new-instance v0, Lehl;

    const-string v1, "REQUEST_DENIED"

    const/16 v2, 0x20

    const-string v3, "RequestDenied"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->H:Lehl;

    new-instance v0, Lehl;

    const-string v1, "SERVER_ERROR"

    const/16 v2, 0x21

    const-string v3, "ServerError"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->I:Lehl;

    new-instance v0, Lehl;

    const-string v1, "USERNAME_UNAVAILABLE"

    const/16 v2, 0x22

    const-string v3, "UsernameUnavailable"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->J:Lehl;

    new-instance v0, Lehl;

    const-string v1, "DELETED_GMAIL"

    const/16 v2, 0x23

    const-string v3, "DeletedGmail"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->K:Lehl;

    new-instance v0, Lehl;

    const-string v1, "SOCKET_TIMEOUT"

    const/16 v2, 0x24

    const-string v3, "SocketTimeout"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->L:Lehl;

    new-instance v0, Lehl;

    const-string v1, "EXISTING_USERNAME"

    const/16 v2, 0x25

    const-string v3, "ExistingUsername"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->M:Lehl;

    new-instance v0, Lehl;

    const-string v1, "NEEDS_BROWSER"

    const/16 v2, 0x26

    const-string v3, "NeedsBrowser"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->N:Lehl;

    new-instance v0, Lehl;

    const-string v1, "GPLUS_OTHER"

    const/16 v2, 0x27

    const-string v3, "GPlusOther"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->O:Lehl;

    new-instance v0, Lehl;

    const-string v1, "GPLUS_NICKNAME"

    const/16 v2, 0x28

    const-string v3, "GPlusNickname"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->P:Lehl;

    new-instance v0, Lehl;

    const-string v1, "GPLUS_INVALID_CHAR"

    const/16 v2, 0x29

    const-string v3, "GPlusInvalidChar"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->Q:Lehl;

    new-instance v0, Lehl;

    const-string v1, "GPLUS_INTERSTITIAL"

    const/16 v2, 0x2a

    const-string v3, "GPlusInterstitial"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->R:Lehl;

    new-instance v0, Lehl;

    const-string v1, "GPLUS_PROFILE_ERROR"

    const/16 v2, 0x2b

    const-string v3, "ProfileUpgradeError"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->S:Lehl;

    new-instance v0, Lehl;

    const-string v1, "INVALID_SCOPE"

    const/16 v2, 0x2c

    const-string v3, "INVALID_SCOPE"

    invoke-direct {v0, v1, v2, v3}, Lehl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lehl;->T:Lehl;

    const/16 v0, 0x2d

    new-array v0, v0, [Lehl;

    sget-object v1, Lehl;->h:Lehl;

    aput-object v1, v0, v4

    sget-object v1, Lehl;->i:Lehl;

    aput-object v1, v0, v5

    sget-object v1, Lehl;->j:Lehl;

    aput-object v1, v0, v6

    sget-object v1, Lehl;->k:Lehl;

    aput-object v1, v0, v7

    sget-object v1, Lehl;->l:Lehl;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lehl;->m:Lehl;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lehl;->n:Lehl;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lehl;->o:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lehl;->p:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lehl;->q:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lehl;->r:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lehl;->s:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lehl;->t:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lehl;->u:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lehl;->v:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lehl;->w:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lehl;->x:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lehl;->a:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lehl;->b:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lehl;->c:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lehl;->d:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lehl;->e:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lehl;->f:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lehl;->y:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lehl;->z:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lehl;->A:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lehl;->B:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lehl;->C:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lehl;->D:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lehl;->E:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lehl;->F:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lehl;->G:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lehl;->H:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lehl;->I:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lehl;->J:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lehl;->K:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lehl;->L:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lehl;->M:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lehl;->N:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lehl;->O:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lehl;->P:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lehl;->Q:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lehl;->R:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lehl;->S:Lehl;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lehl;->T:Lehl;

    aput-object v2, v0, v1

    sput-object v0, Lehl;->U:[Lehl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lehl;->g:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lehl;
    .locals 1

    const-class v0, Lehl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lehl;

    return-object v0
.end method

.method public static values()[Lehl;
    .locals 1

    sget-object v0, Lehl;->U:[Lehl;

    invoke-virtual {v0}, [Lehl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lehl;

    return-object v0
.end method
