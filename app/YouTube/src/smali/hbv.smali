.class public final Lhbv;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhcd;

.field public b:Lhwp;

.field public c:Lhyc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17577
    invoke-direct {p0}, Lidf;-><init>()V

    .line 17580
    iput-object v0, p0, Lhbv;->a:Lhcd;

    .line 17583
    iput-object v0, p0, Lhbv;->b:Lhwp;

    .line 17586
    iput-object v0, p0, Lhbv;->c:Lhyc;

    .line 17577
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 17635
    const/4 v0, 0x0

    .line 17636
    iget-object v1, p0, Lhbv;->a:Lhcd;

    if-eqz v1, :cond_0

    .line 17637
    const v0, 0x30e0084

    iget-object v1, p0, Lhbv;->a:Lhcd;

    .line 17638
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17640
    :cond_0
    iget-object v1, p0, Lhbv;->b:Lhwp;

    if-eqz v1, :cond_1

    .line 17641
    const v1, 0x34da2d9

    iget-object v2, p0, Lhbv;->b:Lhwp;

    .line 17642
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17644
    :cond_1
    iget-object v1, p0, Lhbv;->c:Lhyc;

    if-eqz v1, :cond_2

    .line 17645
    const v1, 0x3c2c61f

    iget-object v2, p0, Lhbv;->c:Lhyc;

    .line 17646
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17648
    :cond_2
    iget-object v1, p0, Lhbv;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17649
    iput v0, p0, Lhbv;->J:I

    .line 17650
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 17573
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbv;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbv;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbv;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhbv;->a:Lhcd;

    if-nez v0, :cond_2

    new-instance v0, Lhcd;

    invoke-direct {v0}, Lhcd;-><init>()V

    iput-object v0, p0, Lhbv;->a:Lhcd;

    :cond_2
    iget-object v0, p0, Lhbv;->a:Lhcd;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhbv;->b:Lhwp;

    if-nez v0, :cond_3

    new-instance v0, Lhwp;

    invoke-direct {v0}, Lhwp;-><init>()V

    iput-object v0, p0, Lhbv;->b:Lhwp;

    :cond_3
    iget-object v0, p0, Lhbv;->b:Lhwp;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhbv;->c:Lhyc;

    if-nez v0, :cond_4

    new-instance v0, Lhyc;

    invoke-direct {v0}, Lhyc;-><init>()V

    iput-object v0, p0, Lhbv;->c:Lhyc;

    :cond_4
    iget-object v0, p0, Lhbv;->c:Lhyc;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18700422 -> :sswitch_1
        0x1a6d16ca -> :sswitch_2
        0x1e1630fa -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 17620
    iget-object v0, p0, Lhbv;->a:Lhcd;

    if-eqz v0, :cond_0

    .line 17621
    const v0, 0x30e0084

    iget-object v1, p0, Lhbv;->a:Lhcd;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 17623
    :cond_0
    iget-object v0, p0, Lhbv;->b:Lhwp;

    if-eqz v0, :cond_1

    .line 17624
    const v0, 0x34da2d9

    iget-object v1, p0, Lhbv;->b:Lhwp;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 17626
    :cond_1
    iget-object v0, p0, Lhbv;->c:Lhyc;

    if-eqz v0, :cond_2

    .line 17627
    const v0, 0x3c2c61f

    iget-object v1, p0, Lhbv;->c:Lhyc;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 17629
    :cond_2
    iget-object v0, p0, Lhbv;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 17631
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 17599
    if-ne p1, p0, :cond_1

    .line 17605
    :cond_0
    :goto_0
    return v0

    .line 17600
    :cond_1
    instance-of v2, p1, Lhbv;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 17601
    :cond_2
    check-cast p1, Lhbv;

    .line 17602
    iget-object v2, p0, Lhbv;->a:Lhcd;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbv;->a:Lhcd;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhbv;->b:Lhwp;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbv;->b:Lhwp;

    if-nez v2, :cond_3

    .line 17603
    :goto_2
    iget-object v2, p0, Lhbv;->c:Lhyc;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhbv;->c:Lhyc;

    if-nez v2, :cond_3

    .line 17604
    :goto_3
    iget-object v2, p0, Lhbv;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhbv;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 17605
    goto :goto_0

    .line 17602
    :cond_4
    iget-object v2, p0, Lhbv;->a:Lhcd;

    iget-object v3, p1, Lhbv;->a:Lhcd;

    invoke-virtual {v2, v3}, Lhcd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbv;->b:Lhwp;

    iget-object v3, p1, Lhbv;->b:Lhwp;

    .line 17603
    invoke-virtual {v2, v3}, Lhwp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhbv;->c:Lhyc;

    iget-object v3, p1, Lhbv;->c:Lhyc;

    .line 17604
    invoke-virtual {v2, v3}, Lhyc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhbv;->I:Ljava/util/List;

    iget-object v3, p1, Lhbv;->I:Ljava/util/List;

    .line 17605
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 17609
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 17611
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbv;->a:Lhcd;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 17612
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbv;->b:Lhwp;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 17613
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbv;->c:Lhyc;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 17614
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbv;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 17615
    return v0

    .line 17611
    :cond_0
    iget-object v0, p0, Lhbv;->a:Lhcd;

    invoke-virtual {v0}, Lhcd;->hashCode()I

    move-result v0

    goto :goto_0

    .line 17612
    :cond_1
    iget-object v0, p0, Lhbv;->b:Lhwp;

    invoke-virtual {v0}, Lhwp;->hashCode()I

    move-result v0

    goto :goto_1

    .line 17613
    :cond_2
    iget-object v0, p0, Lhbv;->c:Lhyc;

    invoke-virtual {v0}, Lhyc;->hashCode()I

    move-result v0

    goto :goto_2

    .line 17614
    :cond_3
    iget-object v1, p0, Lhbv;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
