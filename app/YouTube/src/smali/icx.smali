.class public final Licx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[B

.field private b:I

.field private c:I

.field private d:I

.field private final e:Ljava/io/InputStream;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method constructor <init>([BII)V
    .locals 1

    .prologue
    .line 463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    const v0, 0x7fffffff

    iput v0, p0, Licx;->h:I

    .line 454
    const/16 v0, 0x40

    iput v0, p0, Licx;->j:I

    .line 457
    const/high16 v0, 0x4000000

    iput v0, p0, Licx;->k:I

    .line 464
    iput-object p1, p0, Licx;->a:[B

    .line 465
    add-int v0, p2, p3

    iput v0, p0, Licx;->b:I

    .line 466
    iput p2, p0, Licx;->d:I

    .line 467
    const/4 v0, 0x0

    iput-object v0, p0, Licx;->e:Ljava/io/InputStream;

    .line 468
    return-void
.end method

.method private a(Z)Z
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 601
    iget v1, p0, Licx;->d:I

    iget v2, p0, Licx;->b:I

    if-ge v1, v2, :cond_0

    .line 602
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "refillBuffer() called when buffer wasn\'t empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 606
    :cond_0
    iget v1, p0, Licx;->g:I

    iget v2, p0, Licx;->b:I

    add-int/2addr v1, v2

    iget v2, p0, Licx;->h:I

    if-ne v1, v2, :cond_1

    .line 608
    if-eqz p1, :cond_7

    .line 609
    invoke-static {}, Licz;->a()Licz;

    move-result-object v0

    throw v0

    .line 615
    :cond_1
    iget v1, p0, Licx;->g:I

    iget v2, p0, Licx;->b:I

    add-int/2addr v1, v2

    iput v1, p0, Licx;->g:I

    .line 617
    iput v0, p0, Licx;->d:I

    .line 618
    iput v3, p0, Licx;->b:I

    .line 619
    iget v1, p0, Licx;->b:I

    if-eqz v1, :cond_2

    iget v1, p0, Licx;->b:I

    if-ge v1, v3, :cond_3

    .line 620
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Licx;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x66

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "InputStream#read(byte[]) returned invalid result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nThe InputStream implementation is buggy."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 624
    :cond_3
    iget v1, p0, Licx;->b:I

    if-ne v1, v3, :cond_4

    .line 625
    iput v0, p0, Licx;->b:I

    .line 626
    if-eqz p1, :cond_7

    .line 627
    invoke-static {}, Licz;->a()Licz;

    move-result-object v0

    throw v0

    .line 632
    :cond_4
    invoke-direct {p0}, Licx;->g()V

    .line 633
    iget v0, p0, Licx;->g:I

    iget v1, p0, Licx;->b:I

    add-int/2addr v0, v1

    iget v1, p0, Licx;->c:I

    add-int/2addr v0, v1

    .line 635
    iget v1, p0, Licx;->k:I

    if-gt v0, v1, :cond_5

    if-gez v0, :cond_6

    .line 636
    :cond_5
    invoke-static {}, Licz;->h()Licz;

    move-result-object v0

    throw v0

    .line 638
    :cond_6
    const/4 v0, 0x1

    :cond_7
    return v0
.end method

.method private c(I)[B
    .locals 8

    .prologue
    const/16 v7, 0x1000

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 662
    if-gez p1, :cond_0

    .line 663
    invoke-static {}, Licz;->b()Licz;

    move-result-object v0

    throw v0

    .line 666
    :cond_0
    iget v0, p0, Licx;->g:I

    iget v1, p0, Licx;->d:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iget v1, p0, Licx;->h:I

    if-le v0, v1, :cond_1

    .line 668
    iget v0, p0, Licx;->h:I

    iget v1, p0, Licx;->g:I

    sub-int/2addr v0, v1

    iget v1, p0, Licx;->d:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Licx;->d(I)V

    .line 670
    invoke-static {}, Licz;->a()Licz;

    move-result-object v0

    throw v0

    .line 673
    :cond_1
    iget v0, p0, Licx;->b:I

    iget v1, p0, Licx;->d:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_2

    .line 675
    new-array v0, p1, [B

    .line 676
    iget-object v1, p0, Licx;->a:[B

    iget v3, p0, Licx;->d:I

    invoke-static {v1, v3, v0, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 677
    iget v1, p0, Licx;->d:I

    add-int/2addr v1, p1

    iput v1, p0, Licx;->d:I

    .line 761
    :goto_0
    return-object v0

    .line 679
    :cond_2
    if-ge p1, v7, :cond_4

    .line 684
    new-array v1, p1, [B

    .line 685
    iget v0, p0, Licx;->b:I

    iget v3, p0, Licx;->d:I

    sub-int/2addr v0, v3

    .line 686
    iget-object v3, p0, Licx;->a:[B

    iget v4, p0, Licx;->d:I

    invoke-static {v3, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 687
    iget v3, p0, Licx;->b:I

    iput v3, p0, Licx;->d:I

    .line 692
    invoke-direct {p0, v5}, Licx;->a(Z)Z

    .line 694
    :goto_1
    sub-int v3, p1, v0

    iget v4, p0, Licx;->b:I

    if-le v3, v4, :cond_3

    .line 695
    iget-object v3, p0, Licx;->a:[B

    iget v4, p0, Licx;->b:I

    invoke-static {v3, v2, v1, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 696
    iget v3, p0, Licx;->b:I

    add-int/2addr v0, v3

    .line 697
    iget v3, p0, Licx;->b:I

    iput v3, p0, Licx;->d:I

    .line 698
    invoke-direct {p0, v5}, Licx;->a(Z)Z

    goto :goto_1

    .line 701
    :cond_3
    iget-object v3, p0, Licx;->a:[B

    sub-int v4, p1, v0

    invoke-static {v3, v2, v1, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 702
    sub-int v0, p1, v0

    iput v0, p0, Licx;->d:I

    move-object v0, v1

    .line 704
    goto :goto_0

    .line 716
    :cond_4
    iget v1, p0, Licx;->d:I

    .line 717
    iget v3, p0, Licx;->b:I

    .line 720
    iget v0, p0, Licx;->g:I

    iget v4, p0, Licx;->b:I

    add-int/2addr v0, v4

    iput v0, p0, Licx;->g:I

    .line 721
    iput v2, p0, Licx;->d:I

    .line 722
    iput v2, p0, Licx;->b:I

    .line 725
    sub-int v0, v3, v1

    sub-int v0, p1, v0

    .line 728
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 730
    :goto_2
    if-lez v0, :cond_6

    .line 731
    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v4, v4, [B

    .line 732
    array-length v6, v4

    if-lez v6, :cond_5

    .line 734
    invoke-static {}, Licz;->a()Licz;

    move-result-object v0

    throw v0

    .line 739
    :cond_5
    array-length v6, v4

    sub-int/2addr v0, v6

    .line 743
    invoke-virtual {v5, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_2

    .line 747
    :cond_6
    new-array v4, p1, [B

    .line 750
    sub-int v0, v3, v1

    .line 751
    iget-object v3, p0, Licx;->a:[B

    invoke-static {v3, v1, v4, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v1, v2

    move v3, v0

    .line 754
    :goto_3
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 755
    invoke-virtual {v5, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 756
    array-length v6, v0

    invoke-static {v0, v2, v4, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 757
    array-length v0, v0

    add-int/2addr v3, v0

    .line 754
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_7
    move-object v0, v4

    .line 761
    goto/16 :goto_0
.end method

.method private d(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 772
    if-gez p1, :cond_0

    .line 773
    invoke-static {}, Licz;->b()Licz;

    move-result-object v0

    throw v0

    .line 776
    :cond_0
    iget v0, p0, Licx;->g:I

    iget v1, p0, Licx;->d:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iget v1, p0, Licx;->h:I

    if-le v0, v1, :cond_1

    .line 778
    iget v0, p0, Licx;->h:I

    iget v1, p0, Licx;->g:I

    sub-int/2addr v0, v1

    iget v1, p0, Licx;->d:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Licx;->d(I)V

    .line 780
    invoke-static {}, Licz;->a()Licz;

    move-result-object v0

    throw v0

    .line 783
    :cond_1
    iget v0, p0, Licx;->b:I

    iget v1, p0, Licx;->d:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_3

    .line 785
    iget v0, p0, Licx;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Licx;->d:I

    .line 799
    :cond_2
    return-void

    .line 788
    :cond_3
    iget v0, p0, Licx;->b:I

    iget v1, p0, Licx;->d:I

    sub-int/2addr v0, v1

    .line 789
    iget v1, p0, Licx;->g:I

    add-int/2addr v1, v0

    iput v1, p0, Licx;->g:I

    .line 790
    iput v2, p0, Licx;->d:I

    .line 791
    iput v2, p0, Licx;->b:I

    .line 794
    if-ge v0, p1, :cond_2

    .line 795
    invoke-static {}, Licz;->a()Licz;

    move-result-object v0

    throw v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 550
    iget v0, p0, Licx;->b:I

    iget v1, p0, Licx;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Licx;->b:I

    .line 551
    iget v0, p0, Licx;->g:I

    iget v1, p0, Licx;->b:I

    add-int/2addr v0, v1

    .line 552
    iget v1, p0, Licx;->h:I

    if-le v0, v1, :cond_0

    .line 554
    iget v1, p0, Licx;->h:I

    sub-int/2addr v0, v1

    iput v0, p0, Licx;->c:I

    .line 555
    iget v0, p0, Licx;->b:I

    iget v1, p0, Licx;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, Licx;->b:I

    .line 559
    :goto_0
    return-void

    .line 557
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Licx;->c:I

    goto :goto_0
.end method

.method private h()B
    .locals 3

    .prologue
    .line 649
    iget v0, p0, Licx;->d:I

    iget v1, p0, Licx;->b:I

    if-ne v0, v1, :cond_0

    .line 650
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Licx;->a(Z)Z

    .line 652
    :cond_0
    iget-object v0, p0, Licx;->a:[B

    iget v1, p0, Licx;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Licx;->d:I

    aget-byte v0, v0, v1

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 79
    iget v1, p0, Licx;->d:I

    iget v2, p0, Licx;->b:I

    if-ne v1, v2, :cond_0

    invoke-direct {p0, v0}, Licx;->a(Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    .line 80
    iput v0, p0, Licx;->f:I

    .line 89
    :goto_1
    return v0

    :cond_0
    move v1, v0

    .line 79
    goto :goto_0

    .line 84
    :cond_1
    invoke-virtual {p0}, Licx;->f()I

    move-result v0

    iput v0, p0, Licx;->f:I

    .line 85
    iget v0, p0, Licx;->f:I

    if-nez v0, :cond_2

    .line 87
    invoke-static {}, Licz;->d()Licz;

    move-result-object v0

    throw v0

    .line 89
    :cond_2
    iget v0, p0, Licx;->f:I

    goto :goto_1
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Licx;->f:I

    if-eq v0, p1, :cond_0

    .line 103
    invoke-static {}, Licz;->e()Licz;

    move-result-object v0

    throw v0

    .line 105
    :cond_0
    return-void
.end method

.method public final a(Lida;)V
    .locals 3

    .prologue
    .line 225
    invoke-virtual {p0}, Licx;->f()I

    move-result v0

    .line 226
    iget v1, p0, Licx;->i:I

    iget v2, p0, Licx;->j:I

    if-lt v1, v2, :cond_0

    .line 227
    invoke-static {}, Licz;->g()Licz;

    move-result-object v0

    throw v0

    .line 229
    :cond_0
    if-gez v0, :cond_1

    invoke-static {}, Licz;->b()Licz;

    move-result-object v0

    throw v0

    :cond_1
    iget v1, p0, Licx;->g:I

    iget v2, p0, Licx;->d:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iget v1, p0, Licx;->h:I

    if-le v0, v1, :cond_2

    invoke-static {}, Licz;->a()Licz;

    move-result-object v0

    throw v0

    :cond_2
    iput v0, p0, Licx;->h:I

    invoke-direct {p0}, Licx;->g()V

    .line 230
    iget v0, p0, Licx;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Licx;->i:I

    .line 231
    invoke-virtual {p1, p0}, Lida;->a(Licx;)Lida;

    .line 232
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Licx;->a(I)V

    .line 233
    iget v0, p0, Licx;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Licx;->i:I

    .line 234
    iput v1, p0, Licx;->h:I

    invoke-direct {p0}, Licx;->g()V

    .line 235
    return-void
.end method

.method public final b()J
    .locals 6

    .prologue
    .line 172
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    :goto_0
    const/16 v3, 0x40

    if-ge v2, v3, :cond_1

    invoke-direct {p0}, Licx;->h()B

    move-result v3

    and-int/lit8 v4, v3, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v2

    or-long/2addr v0, v4

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_0

    return-wide v0

    :cond_0
    add-int/lit8 v2, v2, 0x7

    goto :goto_0

    :cond_1
    invoke-static {}, Licz;->c()Licz;

    move-result-object v0

    throw v0
.end method

.method public final b(I)Z
    .locals 12

    .prologue
    .line 114
    invoke-static {p1}, Lidb;->a(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 136
    invoke-static {}, Licz;->f()Licz;

    move-result-object v0

    throw v0

    .line 116
    :pswitch_0
    invoke-virtual {p0}, Licx;->f()I

    .line 117
    const/4 v0, 0x1

    .line 134
    :goto_0
    return v0

    .line 119
    :pswitch_1
    invoke-direct {p0}, Licx;->h()B

    move-result v0

    invoke-direct {p0}, Licx;->h()B

    move-result v1

    invoke-direct {p0}, Licx;->h()B

    move-result v2

    invoke-direct {p0}, Licx;->h()B

    move-result v3

    invoke-direct {p0}, Licx;->h()B

    move-result v4

    invoke-direct {p0}, Licx;->h()B

    move-result v5

    invoke-direct {p0}, Licx;->h()B

    move-result v6

    invoke-direct {p0}, Licx;->h()B

    move-result v7

    int-to-long v8, v0

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    int-to-long v0, v1

    const-wide/16 v10, 0xff

    and-long/2addr v0, v10

    const/16 v10, 0x8

    shl-long/2addr v0, v10

    or-long/2addr v0, v8

    int-to-long v8, v2

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v2, 0x10

    shl-long/2addr v8, v2

    or-long/2addr v0, v8

    int-to-long v2, v3

    const-wide/16 v8, 0xff

    and-long/2addr v2, v8

    const/16 v8, 0x18

    shl-long/2addr v2, v8

    or-long/2addr v0, v2

    int-to-long v2, v4

    const-wide/16 v8, 0xff

    and-long/2addr v2, v8

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v5

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v6

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v7

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 120
    const/4 v0, 0x1

    goto :goto_0

    .line 122
    :pswitch_2
    invoke-virtual {p0}, Licx;->f()I

    move-result v0

    invoke-direct {p0, v0}, Licx;->d(I)V

    .line 123
    const/4 v0, 0x1

    goto :goto_0

    .line 125
    :cond_0
    :pswitch_3
    invoke-virtual {p0}, Licx;->a()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Licx;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    :cond_1
    invoke-static {p1}, Lidb;->b(I)I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lidb;->a(II)I

    move-result v0

    .line 126
    invoke-virtual {p0, v0}, Licx;->a(I)V

    .line 129
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 131
    :pswitch_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 133
    :pswitch_5
    invoke-direct {p0}, Licx;->h()B

    move-result v0

    invoke-direct {p0}, Licx;->h()B

    move-result v1

    invoke-direct {p0}, Licx;->h()B

    move-result v2

    invoke-direct {p0}, Licx;->h()B

    move-result v3

    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, v2, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 v1, v3, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    .line 134
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Licx;->f()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    .prologue
    .line 197
    invoke-virtual {p0}, Licx;->f()I

    move-result v1

    .line 198
    iget v0, p0, Licx;->b:I

    iget v2, p0, Licx;->d:I

    sub-int/2addr v0, v2

    if-gt v1, v0, :cond_0

    if-lez v1, :cond_0

    .line 201
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Licx;->a:[B

    iget v3, p0, Licx;->d:I

    const-string v4, "UTF-8"

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 202
    iget v2, p0, Licx;->d:I

    add-int/2addr v1, v2

    iput v1, p0, Licx;->d:I

    .line 206
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {p0, v1}, Licx;->c(I)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_0
.end method

.method public final e()Licw;
    .locals 3

    .prologue
    .line 239
    invoke-virtual {p0}, Licx;->f()I

    move-result v1

    .line 240
    iget v0, p0, Licx;->b:I

    iget v2, p0, Licx;->d:I

    sub-int/2addr v0, v2

    if-gt v1, v0, :cond_0

    if-lez v1, :cond_0

    .line 243
    iget-object v0, p0, Licx;->a:[B

    iget v2, p0, Licx;->d:I

    invoke-static {v0, v2, v1}, Licw;->a([BII)Licw;

    move-result-object v0

    .line 244
    iget v2, p0, Licx;->d:I

    add-int/2addr v1, v2

    iput v1, p0, Licx;->d:I

    .line 248
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v1}, Licx;->c(I)[B

    move-result-object v0

    invoke-static {v0}, Licw;->a([B)Licw;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()I
    .locals 3

    .prologue
    .line 292
    invoke-direct {p0}, Licx;->h()B

    move-result v0

    .line 293
    if-ltz v0, :cond_1

    .line 322
    :cond_0
    :goto_0
    return v0

    .line 296
    :cond_1
    and-int/lit8 v0, v0, 0x7f

    .line 297
    invoke-direct {p0}, Licx;->h()B

    move-result v1

    if-ltz v1, :cond_2

    .line 298
    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    goto :goto_0

    .line 300
    :cond_2
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    .line 301
    invoke-direct {p0}, Licx;->h()B

    move-result v1

    if-ltz v1, :cond_3

    .line 302
    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    goto :goto_0

    .line 304
    :cond_3
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    .line 305
    invoke-direct {p0}, Licx;->h()B

    move-result v1

    if-ltz v1, :cond_4

    .line 306
    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    goto :goto_0

    .line 308
    :cond_4
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    .line 309
    invoke-direct {p0}, Licx;->h()B

    move-result v1

    shl-int/lit8 v2, v1, 0x1c

    or-int/2addr v0, v2

    .line 310
    if-gez v1, :cond_0

    .line 312
    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_5

    .line 313
    invoke-direct {p0}, Licx;->h()B

    move-result v2

    if-gez v2, :cond_0

    .line 312
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 317
    :cond_5
    invoke-static {}, Licz;->c()Licz;

    move-result-object v0

    throw v0
.end method
