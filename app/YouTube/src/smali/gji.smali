.class public Lgji;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgjg;
.implements Ljava/io/Serializable;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Landroid/net/Uri;

.field public e:Landroid/net/Uri;

.field private f:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lgji;->f:Ljava/util/List;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 208
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgji;->a:I

    .line 209
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgji;->b:I

    .line 210
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgji;->c:I

    .line 211
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgji;->d:Landroid/net/Uri;

    .line 212
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgji;->e:Landroid/net/Uri;

    .line 213
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgji;->f:Ljava/util/List;

    .line 214
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 194
    invoke-virtual {p0}, Lgji;->a()Lgjh;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lgji;->a:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 199
    iget v0, p0, Lgji;->b:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 200
    iget v0, p0, Lgji;->c:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 201
    iget-object v0, p0, Lgji;->d:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 202
    iget-object v0, p0, Lgji;->e:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lgji;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 204
    return-void
.end method


# virtual methods
.method public final a()Lgjh;
    .locals 7

    .prologue
    .line 190
    new-instance v0, Lgjh;

    iget v1, p0, Lgji;->a:I

    iget v2, p0, Lgji;->b:I

    iget v3, p0, Lgji;->c:I

    iget-object v4, p0, Lgji;->d:Landroid/net/Uri;

    iget-object v5, p0, Lgji;->e:Landroid/net/Uri;

    iget-object v6, p0, Lgji;->f:Ljava/util/List;

    invoke-direct/range {v0 .. v6}, Lgjh;-><init>(IIILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Lgji;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lgji;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    return-object p0
.end method

.method public final a(Ljava/util/List;)Lgji;
    .locals 1

    .prologue
    .line 178
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    iget-object v0, p0, Lgji;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 180
    return-object p0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lgji;->a()Lgjh;

    move-result-object v0

    return-object v0
.end method
