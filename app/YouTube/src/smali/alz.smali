.class public final Lalz;
.super Lank;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Lgwd;

.field private final d:Laco;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lgwd;Laco;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lank;-><init>()V

    .line 41
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lalz;->a:Landroid/content/Context;

    .line 42
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lalz;->b:Landroid/os/Handler;

    .line 43
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgwd;

    iput-object v0, p0, Lalz;->c:Lgwd;

    .line 44
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laco;

    iput-object v0, p0, Lalz;->d:Laco;

    .line 45
    return-void
.end method

.method static synthetic a(Lalz;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lalz;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lalz;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lalz;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lalz;)Lgwd;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lalz;->c:Lgwd;

    return-object v0
.end method

.method static synthetic d(Lalz;)Laco;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lalz;->d:Laco;

    return-object v0
.end method


# virtual methods
.method public final a(Laiw;Lajf;Lajl;Lajo;Lait;Lain;Lajr;Laiq;Laiz;Lajc;Laji;Laju;Z)Lanm;
    .locals 19

    .prologue
    .line 62
    invoke-static/range {p1 .. p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    invoke-static/range {p2 .. p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    if-nez p13, :cond_0

    .line 65
    invoke-static/range {p3 .. p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    :goto_0
    invoke-static/range {p5 .. p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-static/range {p6 .. p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    invoke-static/range {p7 .. p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    new-instance v17, Landroid/os/ConditionVariable;

    invoke-direct/range {v17 .. v17}, Landroid/os/ConditionVariable;-><init>()V

    .line 79
    new-instance v16, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct/range {v16 .. v16}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 82
    move-object/from16 v0, p0

    iget-object v0, v0, Lalz;->b:Landroid/os/Handler;

    move-object/from16 v18, v0

    new-instance v1, Lama;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move/from16 v15, p13

    invoke-direct/range {v1 .. v17}, Lama;-><init>(Lalz;Laiw;Lajf;Lajl;Lajo;Lait;Lain;Lajr;Laiq;Laiz;Lajc;Laji;Laju;ZLjava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 107
    invoke-virtual/range {v17 .. v17}, Landroid/os/ConditionVariable;->block()V

    .line 108
    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lanm;

    return-object v1

    .line 67
    :cond_0
    invoke-static/range {p4 .. p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
