.class public final Lgyh;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lgyi;

.field private b:Lhtx;

.field private c:Lgyf;

.field private d:Lhgz;

.field private e:Z

.field private f:Lhut;

.field private g:Ljava/lang/String;

.field private h:Lhel;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2280
    invoke-direct {p0}, Lidf;-><init>()V

    .line 2283
    iput-object v1, p0, Lgyh;->b:Lhtx;

    .line 2286
    iput-object v1, p0, Lgyh;->c:Lgyf;

    .line 2289
    sget-object v0, Lgyi;->a:[Lgyi;

    iput-object v0, p0, Lgyh;->a:[Lgyi;

    .line 2292
    iput-object v1, p0, Lgyh;->d:Lhgz;

    .line 2295
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgyh;->e:Z

    .line 2298
    iput-object v1, p0, Lgyh;->f:Lhut;

    .line 2301
    const-string v0, ""

    iput-object v0, p0, Lgyh;->g:Ljava/lang/String;

    .line 2304
    iput-object v1, p0, Lgyh;->h:Lhel;

    .line 2280
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2392
    .line 2393
    iget-object v0, p0, Lgyh;->b:Lhtx;

    if-eqz v0, :cond_8

    .line 2394
    const/4 v0, 0x1

    iget-object v2, p0, Lgyh;->b:Lhtx;

    .line 2395
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2397
    :goto_0
    iget-object v2, p0, Lgyh;->c:Lgyf;

    if-eqz v2, :cond_0

    .line 2398
    const/4 v2, 0x2

    iget-object v3, p0, Lgyh;->c:Lgyf;

    .line 2399
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2401
    :cond_0
    iget-object v2, p0, Lgyh;->a:[Lgyi;

    if-eqz v2, :cond_2

    .line 2402
    iget-object v2, p0, Lgyh;->a:[Lgyi;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2403
    if-eqz v4, :cond_1

    .line 2404
    const/4 v5, 0x3

    .line 2405
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2402
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2409
    :cond_2
    iget-object v1, p0, Lgyh;->d:Lhgz;

    if-eqz v1, :cond_3

    .line 2410
    const/4 v1, 0x4

    iget-object v2, p0, Lgyh;->d:Lhgz;

    .line 2411
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2413
    :cond_3
    iget-boolean v1, p0, Lgyh;->e:Z

    if-eqz v1, :cond_4

    .line 2414
    const/4 v1, 0x5

    iget-boolean v2, p0, Lgyh;->e:Z

    .line 2415
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2417
    :cond_4
    iget-object v1, p0, Lgyh;->f:Lhut;

    if-eqz v1, :cond_5

    .line 2418
    const/16 v1, 0x8

    iget-object v2, p0, Lgyh;->f:Lhut;

    .line 2419
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2421
    :cond_5
    iget-object v1, p0, Lgyh;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2422
    const/16 v1, 0x9

    iget-object v2, p0, Lgyh;->g:Ljava/lang/String;

    .line 2423
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2425
    :cond_6
    iget-object v1, p0, Lgyh;->h:Lhel;

    if-eqz v1, :cond_7

    .line 2426
    const/16 v1, 0xa

    iget-object v2, p0, Lgyh;->h:Lhel;

    .line 2427
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2429
    :cond_7
    iget-object v1, p0, Lgyh;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2430
    iput v0, p0, Lgyh;->J:I

    .line 2431
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2276
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lgyh;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lgyh;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lgyh;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgyh;->b:Lhtx;

    if-nez v0, :cond_2

    new-instance v0, Lhtx;

    invoke-direct {v0}, Lhtx;-><init>()V

    iput-object v0, p0, Lgyh;->b:Lhtx;

    :cond_2
    iget-object v0, p0, Lgyh;->b:Lhtx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgyh;->c:Lgyf;

    if-nez v0, :cond_3

    new-instance v0, Lgyf;

    invoke-direct {v0}, Lgyf;-><init>()V

    iput-object v0, p0, Lgyh;->c:Lgyf;

    :cond_3
    iget-object v0, p0, Lgyh;->c:Lgyf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgyh;->a:[Lgyi;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgyi;

    iget-object v3, p0, Lgyh;->a:[Lgyi;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lgyh;->a:[Lgyi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lgyh;->a:[Lgyi;

    :goto_2
    iget-object v2, p0, Lgyh;->a:[Lgyi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lgyh;->a:[Lgyi;

    new-instance v3, Lgyi;

    invoke-direct {v3}, Lgyi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgyh;->a:[Lgyi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lgyh;->a:[Lgyi;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lgyh;->a:[Lgyi;

    new-instance v3, Lgyi;

    invoke-direct {v3}, Lgyi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgyh;->a:[Lgyi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lgyh;->d:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgyh;->d:Lhgz;

    :cond_7
    iget-object v0, p0, Lgyh;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgyh;->e:Z

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lgyh;->f:Lhut;

    if-nez v0, :cond_8

    new-instance v0, Lhut;

    invoke-direct {v0}, Lhut;-><init>()V

    iput-object v0, p0, Lgyh;->f:Lhut;

    :cond_8
    iget-object v0, p0, Lgyh;->f:Lhut;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgyh;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lgyh;->h:Lhel;

    if-nez v0, :cond_9

    new-instance v0, Lhel;

    invoke-direct {v0}, Lhel;-><init>()V

    iput-object v0, p0, Lgyh;->h:Lhel;

    :cond_9
    iget-object v0, p0, Lgyh;->h:Lhel;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 2358
    iget-object v0, p0, Lgyh;->b:Lhtx;

    if-eqz v0, :cond_0

    .line 2359
    const/4 v0, 0x1

    iget-object v1, p0, Lgyh;->b:Lhtx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2361
    :cond_0
    iget-object v0, p0, Lgyh;->c:Lgyf;

    if-eqz v0, :cond_1

    .line 2362
    const/4 v0, 0x2

    iget-object v1, p0, Lgyh;->c:Lgyf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2364
    :cond_1
    iget-object v0, p0, Lgyh;->a:[Lgyi;

    if-eqz v0, :cond_3

    .line 2365
    iget-object v1, p0, Lgyh;->a:[Lgyi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 2366
    if-eqz v3, :cond_2

    .line 2367
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 2365
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2371
    :cond_3
    iget-object v0, p0, Lgyh;->d:Lhgz;

    if-eqz v0, :cond_4

    .line 2372
    const/4 v0, 0x4

    iget-object v1, p0, Lgyh;->d:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2374
    :cond_4
    iget-boolean v0, p0, Lgyh;->e:Z

    if-eqz v0, :cond_5

    .line 2375
    const/4 v0, 0x5

    iget-boolean v1, p0, Lgyh;->e:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 2377
    :cond_5
    iget-object v0, p0, Lgyh;->f:Lhut;

    if-eqz v0, :cond_6

    .line 2378
    const/16 v0, 0x8

    iget-object v1, p0, Lgyh;->f:Lhut;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2380
    :cond_6
    iget-object v0, p0, Lgyh;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2381
    const/16 v0, 0x9

    iget-object v1, p0, Lgyh;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 2383
    :cond_7
    iget-object v0, p0, Lgyh;->h:Lhel;

    if-eqz v0, :cond_8

    .line 2384
    const/16 v0, 0xa

    iget-object v1, p0, Lgyh;->h:Lhel;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2386
    :cond_8
    iget-object v0, p0, Lgyh;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 2388
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2322
    if-ne p1, p0, :cond_1

    .line 2333
    :cond_0
    :goto_0
    return v0

    .line 2323
    :cond_1
    instance-of v2, p1, Lgyh;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 2324
    :cond_2
    check-cast p1, Lgyh;

    .line 2325
    iget-object v2, p0, Lgyh;->b:Lhtx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyh;->b:Lhtx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgyh;->c:Lgyf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyh;->c:Lgyf;

    if-nez v2, :cond_3

    .line 2326
    :goto_2
    iget-object v2, p0, Lgyh;->a:[Lgyi;

    iget-object v3, p1, Lgyh;->a:[Lgyi;

    .line 2327
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgyh;->d:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgyh;->d:Lhgz;

    if-nez v2, :cond_3

    .line 2328
    :goto_3
    iget-boolean v2, p0, Lgyh;->e:Z

    iget-boolean v3, p1, Lgyh;->e:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyh;->f:Lhut;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgyh;->f:Lhut;

    if-nez v2, :cond_3

    .line 2330
    :goto_4
    iget-object v2, p0, Lgyh;->g:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Lgyh;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 2331
    :goto_5
    iget-object v2, p0, Lgyh;->h:Lhel;

    if-nez v2, :cond_9

    iget-object v2, p1, Lgyh;->h:Lhel;

    if-nez v2, :cond_3

    .line 2332
    :goto_6
    iget-object v2, p0, Lgyh;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Lgyh;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 2333
    goto :goto_0

    .line 2325
    :cond_4
    iget-object v2, p0, Lgyh;->b:Lhtx;

    iget-object v3, p1, Lgyh;->b:Lhtx;

    invoke-virtual {v2, v3}, Lhtx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyh;->c:Lgyf;

    iget-object v3, p1, Lgyh;->c:Lgyf;

    .line 2326
    invoke-virtual {v2, v3}, Lgyf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 2327
    :cond_6
    iget-object v2, p0, Lgyh;->d:Lhgz;

    iget-object v3, p1, Lgyh;->d:Lhgz;

    .line 2328
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgyh;->f:Lhut;

    iget-object v3, p1, Lgyh;->f:Lhut;

    .line 2330
    invoke-virtual {v2, v3}, Lhut;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lgyh;->g:Ljava/lang/String;

    iget-object v3, p1, Lgyh;->g:Ljava/lang/String;

    .line 2331
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lgyh;->h:Lhel;

    iget-object v3, p1, Lgyh;->h:Lhel;

    .line 2332
    invoke-virtual {v2, v3}, Lhel;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lgyh;->I:Ljava/util/List;

    iget-object v3, p1, Lgyh;->I:Ljava/util/List;

    .line 2333
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2337
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2339
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyh;->b:Lhtx;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2340
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyh;->c:Lgyf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2341
    iget-object v2, p0, Lgyh;->a:[Lgyi;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 2347
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgyh;->d:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2348
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lgyh;->e:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_3
    add-int/2addr v0, v2

    .line 2349
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyh;->f:Lhut;

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 2350
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyh;->g:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 2351
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyh;->h:Lhel;

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 2352
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgyh;->I:Ljava/util/List;

    if-nez v2, :cond_a

    :goto_7
    add-int/2addr v0, v1

    .line 2353
    return v0

    .line 2339
    :cond_1
    iget-object v0, p0, Lgyh;->b:Lhtx;

    invoke-virtual {v0}, Lhtx;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2340
    :cond_2
    iget-object v0, p0, Lgyh;->c:Lgyf;

    invoke-virtual {v0}, Lgyf;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    move v2, v0

    move v0, v1

    .line 2343
    :goto_8
    iget-object v3, p0, Lgyh;->a:[Lgyi;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2344
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgyh;->a:[Lgyi;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_9
    add-int/2addr v2, v3

    .line 2343
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 2344
    :cond_4
    iget-object v2, p0, Lgyh;->a:[Lgyi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgyi;->hashCode()I

    move-result v2

    goto :goto_9

    .line 2347
    :cond_5
    iget-object v0, p0, Lgyh;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2348
    :cond_6
    const/4 v0, 0x2

    goto :goto_3

    .line 2349
    :cond_7
    iget-object v0, p0, Lgyh;->f:Lhut;

    invoke-virtual {v0}, Lhut;->hashCode()I

    move-result v0

    goto :goto_4

    .line 2350
    :cond_8
    iget-object v0, p0, Lgyh;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 2351
    :cond_9
    iget-object v0, p0, Lgyh;->h:Lhel;

    invoke-virtual {v0}, Lhel;->hashCode()I

    move-result v0

    goto :goto_6

    .line 2352
    :cond_a
    iget-object v1, p0, Lgyh;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_7
.end method
