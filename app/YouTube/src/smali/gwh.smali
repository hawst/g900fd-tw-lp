.class public final enum Lgwh;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgwh;

.field public static final enum b:Lgwh;

.field public static final enum c:Lgwh;

.field public static final enum d:Lgwh;

.field public static final enum e:Lgwh;

.field public static final enum f:Lgwh;

.field public static final enum g:Lgwh;

.field public static final enum h:Lgwh;

.field public static final enum i:Lgwh;

.field public static final enum j:Lgwh;

.field private static enum k:Lgwh;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static enum l:Lgwh;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static enum m:Lgwh;

.field private static enum n:Lgwh;

.field private static final synthetic o:[Lgwh;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 287
    new-instance v0, Lgwh;

    const-string v1, "EMBEDDING_DISABLED"

    invoke-direct {v0, v1, v3}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->k:Lgwh;

    .line 295
    new-instance v0, Lgwh;

    const-string v1, "BLOCKED_FOR_APP"

    invoke-direct {v0, v1, v4}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->l:Lgwh;

    .line 305
    new-instance v0, Lgwh;

    const-string v1, "NOT_PLAYABLE"

    invoke-direct {v0, v1, v5}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->a:Lgwh;

    .line 310
    new-instance v0, Lgwh;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v6}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->b:Lgwh;

    .line 318
    new-instance v0, Lgwh;

    const-string v1, "UNAUTHORIZED_OVERLAY"

    invoke-direct {v0, v1, v7}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->c:Lgwh;

    .line 323
    new-instance v0, Lgwh;

    const-string v1, "PLAYER_VIEW_TOO_SMALL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->d:Lgwh;

    .line 328
    new-instance v0, Lgwh;

    const-string v1, "PLAYER_VIEW_NOT_VISIBLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->e:Lgwh;

    .line 332
    new-instance v0, Lgwh;

    const-string v1, "EMPTY_PLAYLIST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->f:Lgwh;

    .line 337
    new-instance v0, Lgwh;

    const-string v1, "AUTOPLAY_DISABLED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->m:Lgwh;

    .line 345
    new-instance v0, Lgwh;

    const-string v1, "USER_DECLINED_RESTRICTED_CONTENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->g:Lgwh;

    .line 349
    new-instance v0, Lgwh;

    const-string v1, "USER_DECLINED_HIGH_BANDWIDTH"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->n:Lgwh;

    .line 355
    new-instance v0, Lgwh;

    const-string v1, "UNEXPECTED_SERVICE_DISCONNECTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->h:Lgwh;

    .line 360
    new-instance v0, Lgwh;

    const-string v1, "INTERNAL_ERROR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->i:Lgwh;

    .line 366
    new-instance v0, Lgwh;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lgwh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwh;->j:Lgwh;

    .line 280
    const/16 v0, 0xe

    new-array v0, v0, [Lgwh;

    sget-object v1, Lgwh;->k:Lgwh;

    aput-object v1, v0, v3

    sget-object v1, Lgwh;->l:Lgwh;

    aput-object v1, v0, v4

    sget-object v1, Lgwh;->a:Lgwh;

    aput-object v1, v0, v5

    sget-object v1, Lgwh;->b:Lgwh;

    aput-object v1, v0, v6

    sget-object v1, Lgwh;->c:Lgwh;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lgwh;->d:Lgwh;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgwh;->e:Lgwh;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lgwh;->f:Lgwh;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lgwh;->m:Lgwh;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lgwh;->g:Lgwh;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lgwh;->n:Lgwh;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lgwh;->h:Lgwh;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lgwh;->i:Lgwh;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lgwh;->j:Lgwh;

    aput-object v2, v0, v1

    sput-object v0, Lgwh;->o:[Lgwh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgwh;
    .locals 1

    .prologue
    .line 280
    const-class v0, Lgwh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgwh;

    return-object v0
.end method

.method public static values()[Lgwh;
    .locals 1

    .prologue
    .line 280
    sget-object v0, Lgwh;->o:[Lgwh;

    invoke-virtual {v0}, [Lgwh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgwh;

    return-object v0
.end method
