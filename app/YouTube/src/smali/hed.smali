.class public final Lhed;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhgz;

.field public c:Lhxf;

.field public d:Lhog;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 36812
    invoke-direct {p0}, Lidf;-><init>()V

    .line 36815
    iput-boolean v1, p0, Lhed;->e:Z

    .line 36818
    iput-object v0, p0, Lhed;->a:Lhgz;

    .line 36821
    iput-object v0, p0, Lhed;->b:Lhgz;

    .line 36824
    iput-object v0, p0, Lhed;->c:Lhxf;

    .line 36827
    iput-object v0, p0, Lhed;->d:Lhog;

    .line 36830
    iput-boolean v1, p0, Lhed;->f:Z

    .line 36812
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 36897
    const/4 v0, 0x0

    .line 36898
    iget-boolean v1, p0, Lhed;->e:Z

    if-eqz v1, :cond_0

    .line 36899
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhed;->e:Z

    .line 36900
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 36902
    :cond_0
    iget-object v1, p0, Lhed;->a:Lhgz;

    if-eqz v1, :cond_1

    .line 36903
    const/4 v1, 0x2

    iget-object v2, p0, Lhed;->a:Lhgz;

    .line 36904
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36906
    :cond_1
    iget-object v1, p0, Lhed;->b:Lhgz;

    if-eqz v1, :cond_2

    .line 36907
    const/4 v1, 0x3

    iget-object v2, p0, Lhed;->b:Lhgz;

    .line 36908
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36910
    :cond_2
    iget-object v1, p0, Lhed;->c:Lhxf;

    if-eqz v1, :cond_3

    .line 36911
    const/4 v1, 0x4

    iget-object v2, p0, Lhed;->c:Lhxf;

    .line 36912
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36914
    :cond_3
    iget-object v1, p0, Lhed;->d:Lhog;

    if-eqz v1, :cond_4

    .line 36915
    const/4 v1, 0x5

    iget-object v2, p0, Lhed;->d:Lhog;

    .line 36916
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36918
    :cond_4
    iget-boolean v1, p0, Lhed;->f:Z

    if-eqz v1, :cond_5

    .line 36919
    const/4 v1, 0x6

    iget-boolean v2, p0, Lhed;->f:Z

    .line 36920
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 36922
    :cond_5
    iget-object v1, p0, Lhed;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36923
    iput v0, p0, Lhed;->J:I

    .line 36924
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 36808
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhed;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhed;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhed;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhed;->e:Z

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhed;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhed;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhed;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhed;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhed;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lhed;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhed;->c:Lhxf;

    if-nez v0, :cond_4

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhed;->c:Lhxf;

    :cond_4
    iget-object v0, p0, Lhed;->c:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhed;->d:Lhog;

    if-nez v0, :cond_5

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhed;->d:Lhog;

    :cond_5
    iget-object v0, p0, Lhed;->d:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhed;->f:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 36873
    iget-boolean v0, p0, Lhed;->e:Z

    if-eqz v0, :cond_0

    .line 36874
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhed;->e:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 36876
    :cond_0
    iget-object v0, p0, Lhed;->a:Lhgz;

    if-eqz v0, :cond_1

    .line 36877
    const/4 v0, 0x2

    iget-object v1, p0, Lhed;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36879
    :cond_1
    iget-object v0, p0, Lhed;->b:Lhgz;

    if-eqz v0, :cond_2

    .line 36880
    const/4 v0, 0x3

    iget-object v1, p0, Lhed;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36882
    :cond_2
    iget-object v0, p0, Lhed;->c:Lhxf;

    if-eqz v0, :cond_3

    .line 36883
    const/4 v0, 0x4

    iget-object v1, p0, Lhed;->c:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36885
    :cond_3
    iget-object v0, p0, Lhed;->d:Lhog;

    if-eqz v0, :cond_4

    .line 36886
    const/4 v0, 0x5

    iget-object v1, p0, Lhed;->d:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36888
    :cond_4
    iget-boolean v0, p0, Lhed;->f:Z

    if-eqz v0, :cond_5

    .line 36889
    const/4 v0, 0x6

    iget-boolean v1, p0, Lhed;->f:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 36891
    :cond_5
    iget-object v0, p0, Lhed;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 36893
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36846
    if-ne p1, p0, :cond_1

    .line 36855
    :cond_0
    :goto_0
    return v0

    .line 36847
    :cond_1
    instance-of v2, p1, Lhed;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 36848
    :cond_2
    check-cast p1, Lhed;

    .line 36849
    iget-boolean v2, p0, Lhed;->e:Z

    iget-boolean v3, p1, Lhed;->e:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhed;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhed;->a:Lhgz;

    if-nez v2, :cond_3

    .line 36850
    :goto_1
    iget-object v2, p0, Lhed;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhed;->b:Lhgz;

    if-nez v2, :cond_3

    .line 36851
    :goto_2
    iget-object v2, p0, Lhed;->c:Lhxf;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhed;->c:Lhxf;

    if-nez v2, :cond_3

    .line 36852
    :goto_3
    iget-object v2, p0, Lhed;->d:Lhog;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhed;->d:Lhog;

    if-nez v2, :cond_3

    .line 36853
    :goto_4
    iget-boolean v2, p0, Lhed;->f:Z

    iget-boolean v3, p1, Lhed;->f:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhed;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhed;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 36855
    goto :goto_0

    .line 36849
    :cond_4
    iget-object v2, p0, Lhed;->a:Lhgz;

    iget-object v3, p1, Lhed;->a:Lhgz;

    .line 36850
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhed;->b:Lhgz;

    iget-object v3, p1, Lhed;->b:Lhgz;

    .line 36851
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhed;->c:Lhxf;

    iget-object v3, p1, Lhed;->c:Lhxf;

    .line 36852
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhed;->d:Lhog;

    iget-object v3, p1, Lhed;->d:Lhog;

    .line 36853
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhed;->I:Ljava/util/List;

    iget-object v3, p1, Lhed;->I:Ljava/util/List;

    .line 36855
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 36859
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 36861
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhed;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 36862
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhed;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    add-int/2addr v0, v4

    .line 36863
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhed;->b:Lhgz;

    if-nez v0, :cond_2

    move v0, v3

    :goto_2
    add-int/2addr v0, v4

    .line 36864
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhed;->c:Lhxf;

    if-nez v0, :cond_3

    move v0, v3

    :goto_3
    add-int/2addr v0, v4

    .line 36865
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhed;->d:Lhog;

    if-nez v0, :cond_4

    move v0, v3

    :goto_4
    add-int/2addr v0, v4

    .line 36866
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lhed;->f:Z

    if-eqz v4, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 36867
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhed;->I:Ljava/util/List;

    if-nez v1, :cond_6

    :goto_6
    add-int/2addr v0, v3

    .line 36868
    return v0

    :cond_0
    move v0, v2

    .line 36861
    goto :goto_0

    .line 36862
    :cond_1
    iget-object v0, p0, Lhed;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 36863
    :cond_2
    iget-object v0, p0, Lhed;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 36864
    :cond_3
    iget-object v0, p0, Lhed;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_3

    .line 36865
    :cond_4
    iget-object v0, p0, Lhed;->d:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    move v1, v2

    .line 36866
    goto :goto_5

    .line 36867
    :cond_6
    iget-object v1, p0, Lhed;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v3

    goto :goto_6
.end method
