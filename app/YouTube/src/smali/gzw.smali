.class public final Lgzw;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9823
    invoke-direct {p0}, Lidf;-><init>()V

    .line 9833
    iput v0, p0, Lgzw;->a:I

    .line 9836
    iput v0, p0, Lgzw;->b:I

    .line 9823
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 9879
    const/4 v0, 0x0

    .line 9880
    iget v1, p0, Lgzw;->a:I

    if-eqz v1, :cond_0

    .line 9881
    const/4 v0, 0x1

    iget v1, p0, Lgzw;->a:I

    .line 9882
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9884
    :cond_0
    iget v1, p0, Lgzw;->b:I

    if-eqz v1, :cond_1

    .line 9885
    const/4 v1, 0x2

    iget v2, p0, Lgzw;->b:I

    .line 9886
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9888
    :cond_1
    iget-object v1, p0, Lgzw;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9889
    iput v0, p0, Lgzw;->J:I

    .line 9890
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 9819
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzw;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzw;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzw;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lgzw;->a:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lgzw;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lgzw;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 9867
    iget v0, p0, Lgzw;->a:I

    if-eqz v0, :cond_0

    .line 9868
    const/4 v0, 0x1

    iget v1, p0, Lgzw;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 9870
    :cond_0
    iget v0, p0, Lgzw;->b:I

    if-eqz v0, :cond_1

    .line 9871
    const/4 v0, 0x2

    iget v1, p0, Lgzw;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 9873
    :cond_1
    iget-object v0, p0, Lgzw;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 9875
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9848
    if-ne p1, p0, :cond_1

    .line 9853
    :cond_0
    :goto_0
    return v0

    .line 9849
    :cond_1
    instance-of v2, p1, Lgzw;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 9850
    :cond_2
    check-cast p1, Lgzw;

    .line 9851
    iget v2, p0, Lgzw;->a:I

    iget v3, p1, Lgzw;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgzw;->b:I

    iget v3, p1, Lgzw;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzw;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzw;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 9853
    goto :goto_0

    .line 9851
    :cond_4
    iget-object v2, p0, Lgzw;->I:Ljava/util/List;

    iget-object v3, p1, Lgzw;->I:Ljava/util/List;

    .line 9853
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 9857
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 9859
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lgzw;->a:I

    add-int/2addr v0, v1

    .line 9860
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lgzw;->b:I

    add-int/2addr v0, v1

    .line 9861
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lgzw;->I:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 9862
    return v0

    .line 9861
    :cond_0
    iget-object v0, p0, Lgzw;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_0
.end method
