.class public final Lgcd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final A:Ljava/lang/String;

.field public final B:Lgch;

.field public final C:Z

.field public final D:Z

.field public final E:Ljava/util/Set;

.field public final F:Z

.field public final G:Z

.field public final H:Ljava/lang/String;

.field private I:Z

.field private J:Landroid/net/Uri;

.field private K:Z

.field public final a:Ljava/util/Set;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;

.field public final d:Landroid/net/Uri;

.field public final e:Landroid/net/Uri;

.field public final f:Landroid/net/Uri;

.field public final g:Landroid/net/Uri;

.field public final h:Landroid/net/Uri;

.field public final i:Landroid/net/Uri;

.field public final j:Ljava/lang/String;

.field public final k:I

.field public final l:J

.field public final m:J

.field public final n:J

.field public final o:Ljava/lang/String;

.field public final p:Ljava/lang/String;

.field public final q:Landroid/net/Uri;

.field public final r:Ljava/util/Date;

.field public final s:Ljava/util/Date;

.field public final t:Ljava/lang/String;

.field public final u:Ljava/lang/String;

.field public final v:Ljava/lang/String;

.field public final w:Ljava/lang/String;

.field public final x:Lgcg;

.field public final y:Ljava/util/Map;

.field public final z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 373
    const-string v0, "yt:cc_default_lang=([a-zA-Z]{2})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;IJJJLjava/lang/String;Landroid/net/Uri;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgcg;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZLgch;ZLjava/util/Set;ZZLandroid/net/Uri;ZLjava/lang/String;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    const-string v2, "youTubeId can\'t be empty"

    invoke-static {p1, v2}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lgcd;->b:Ljava/lang/String;

    .line 270
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-static {v2}, La;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lgcd;->a:Ljava/util/Set;

    .line 271
    iput-object p3, p0, Lgcd;->c:Landroid/net/Uri;

    .line 272
    iput-object p4, p0, Lgcd;->d:Landroid/net/Uri;

    .line 273
    iput-object p5, p0, Lgcd;->e:Landroid/net/Uri;

    .line 274
    iput-object p6, p0, Lgcd;->f:Landroid/net/Uri;

    .line 275
    iput-object p7, p0, Lgcd;->g:Landroid/net/Uri;

    .line 276
    iput-object p8, p0, Lgcd;->h:Landroid/net/Uri;

    .line 277
    iput-object p9, p0, Lgcd;->i:Landroid/net/Uri;

    .line 278
    iput-object p10, p0, Lgcd;->j:Ljava/lang/String;

    .line 279
    iput p11, p0, Lgcd;->k:I

    .line 280
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lgcd;->l:J

    .line 281
    move-wide/from16 v0, p14

    iput-wide v0, p0, Lgcd;->m:J

    .line 282
    move-wide/from16 v0, p16

    iput-wide v0, p0, Lgcd;->n:J

    .line 283
    move-object/from16 v0, p18

    iput-object v0, p0, Lgcd;->o:Ljava/lang/String;

    .line 284
    move-object/from16 v0, p19

    iput-object v0, p0, Lgcd;->q:Landroid/net/Uri;

    .line 285
    move-object/from16 v0, p20

    iput-object v0, p0, Lgcd;->r:Ljava/util/Date;

    .line 286
    move-object/from16 v0, p21

    iput-object v0, p0, Lgcd;->s:Ljava/util/Date;

    .line 287
    move-object/from16 v0, p22

    iput-object v0, p0, Lgcd;->t:Ljava/lang/String;

    .line 288
    move-object/from16 v0, p23

    iput-object v0, p0, Lgcd;->u:Ljava/lang/String;

    .line 289
    move-object/from16 v0, p24

    iput-object v0, p0, Lgcd;->v:Ljava/lang/String;

    .line 290
    move-object/from16 v0, p25

    iput-object v0, p0, Lgcd;->w:Ljava/lang/String;

    .line 291
    move-object/from16 v0, p26

    iput-object v0, p0, Lgcd;->x:Lgcg;

    .line 292
    move-object/from16 v0, p27

    iput-object v0, p0, Lgcd;->y:Ljava/util/Map;

    .line 293
    move-object/from16 v0, p28

    iput-object v0, p0, Lgcd;->z:Ljava/lang/String;

    .line 294
    move-object/from16 v0, p29

    iput-object v0, p0, Lgcd;->A:Ljava/lang/String;

    .line 295
    move/from16 v0, p30

    iput-boolean v0, p0, Lgcd;->I:Z

    .line 296
    invoke-static/range {p31 .. p31}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgch;

    iput-object v2, p0, Lgcd;->B:Lgch;

    .line 297
    move/from16 v0, p32

    iput-boolean v0, p0, Lgcd;->D:Z

    .line 298
    if-nez p33, :cond_1

    const/4 v2, 0x0

    .line 299
    :goto_0
    iput-object v2, p0, Lgcd;->E:Ljava/util/Set;

    .line 300
    if-eqz p24, :cond_0

    const-string v2, "yt:cc=alwayson"

    move-object/from16 v0, p24

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 301
    :cond_0
    move/from16 v0, p34

    iput-boolean v0, p0, Lgcd;->F:Z

    .line 302
    move/from16 v0, p35

    iput-boolean v0, p0, Lgcd;->G:Z

    .line 303
    move-object/from16 v0, p36

    iput-object v0, p0, Lgcd;->J:Landroid/net/Uri;

    .line 304
    move/from16 v0, p37

    iput-boolean v0, p0, Lgcd;->C:Z

    .line 305
    invoke-static/range {p38 .. p38}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    move-object/from16 v0, p18

    iput-object v0, p0, Lgcd;->p:Ljava/lang/String;

    .line 306
    move/from16 v0, p39

    iput-boolean v0, p0, Lgcd;->K:Z

    .line 307
    move-object/from16 v0, p40

    iput-object v0, p0, Lgcd;->H:Ljava/lang/String;

    .line 308
    return-void

    .line 299
    :cond_1
    invoke-static/range {p33 .. p33}, La;->b(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, La;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    goto :goto_0

    :cond_2
    move-object/from16 p18, p38

    .line 305
    goto :goto_1
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0}, Lgcd;->c()Lgcf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lgcd;->J:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgcd;->B:Lgch;

    sget-object v1, Lgch;->a:Lgch;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 357
    iget-boolean v1, p0, Lgcd;->K:Z

    if-eqz v1, :cond_0

    .line 369
    :goto_0
    return v0

    .line 361
    :cond_0
    sget-object v1, Lgce;->a:[I

    iget-object v2, p0, Lgcd;->B:Lgch;

    invoke-virtual {v2}, Lgch;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 365
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 367
    :pswitch_1
    invoke-virtual {p0}, Lgcd;->a()Z

    move-result v0

    goto :goto_0

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c()Lgcf;
    .locals 4

    .prologue
    .line 390
    new-instance v0, Lgcf;

    invoke-direct {v0}, Lgcf;-><init>()V

    iget-object v1, p0, Lgcd;->b:Ljava/lang/String;

    .line 391
    iput-object v1, v0, Lgcf;->a:Ljava/lang/String;

    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, Lgcd;->a:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 392
    iput-object v1, v0, Lgcf;->b:Ljava/util/Set;

    iget-object v1, p0, Lgcd;->c:Landroid/net/Uri;

    .line 393
    iput-object v1, v0, Lgcf;->c:Landroid/net/Uri;

    iget-object v1, p0, Lgcd;->d:Landroid/net/Uri;

    .line 394
    iput-object v1, v0, Lgcf;->d:Landroid/net/Uri;

    iget-object v1, p0, Lgcd;->e:Landroid/net/Uri;

    .line 395
    iput-object v1, v0, Lgcf;->e:Landroid/net/Uri;

    iget-object v1, p0, Lgcd;->f:Landroid/net/Uri;

    .line 396
    iput-object v1, v0, Lgcf;->f:Landroid/net/Uri;

    iget-object v1, p0, Lgcd;->g:Landroid/net/Uri;

    .line 397
    iput-object v1, v0, Lgcf;->g:Landroid/net/Uri;

    iget-object v1, p0, Lgcd;->h:Landroid/net/Uri;

    .line 398
    iput-object v1, v0, Lgcf;->h:Landroid/net/Uri;

    iget-object v1, p0, Lgcd;->i:Landroid/net/Uri;

    .line 399
    iput-object v1, v0, Lgcf;->i:Landroid/net/Uri;

    iget-object v1, p0, Lgcd;->j:Ljava/lang/String;

    .line 400
    iput-object v1, v0, Lgcf;->j:Ljava/lang/String;

    iget v1, p0, Lgcd;->k:I

    .line 401
    iput v1, v0, Lgcf;->k:I

    iget-wide v2, p0, Lgcd;->l:J

    .line 402
    iput-wide v2, v0, Lgcf;->l:J

    iget-wide v2, p0, Lgcd;->m:J

    .line 403
    iput-wide v2, v0, Lgcf;->m:J

    iget-wide v2, p0, Lgcd;->n:J

    .line 404
    iput-wide v2, v0, Lgcf;->n:J

    iget-object v1, p0, Lgcd;->o:Ljava/lang/String;

    .line 405
    iput-object v1, v0, Lgcf;->o:Ljava/lang/String;

    iget-object v1, p0, Lgcd;->q:Landroid/net/Uri;

    .line 406
    iput-object v1, v0, Lgcf;->p:Landroid/net/Uri;

    iget-object v1, p0, Lgcd;->r:Ljava/util/Date;

    .line 407
    iput-object v1, v0, Lgcf;->q:Ljava/util/Date;

    iget-object v1, p0, Lgcd;->s:Ljava/util/Date;

    .line 408
    iput-object v1, v0, Lgcf;->r:Ljava/util/Date;

    iget-object v1, p0, Lgcd;->t:Ljava/lang/String;

    .line 409
    iput-object v1, v0, Lgcf;->s:Ljava/lang/String;

    iget-object v1, p0, Lgcd;->u:Ljava/lang/String;

    .line 410
    iput-object v1, v0, Lgcf;->t:Ljava/lang/String;

    iget-object v1, p0, Lgcd;->v:Ljava/lang/String;

    .line 411
    iput-object v1, v0, Lgcf;->u:Ljava/lang/String;

    iget-object v1, p0, Lgcd;->w:Ljava/lang/String;

    .line 412
    iput-object v1, v0, Lgcf;->v:Ljava/lang/String;

    iget-object v1, p0, Lgcd;->x:Lgcg;

    .line 413
    invoke-virtual {v0, v1}, Lgcf;->a(Lgcg;)Lgcf;

    move-result-object v0

    iget-object v1, p0, Lgcd;->y:Ljava/util/Map;

    .line 414
    iput-object v1, v0, Lgcf;->w:Ljava/util/Map;

    iget-object v1, p0, Lgcd;->z:Ljava/lang/String;

    .line 415
    iput-object v1, v0, Lgcf;->x:Ljava/lang/String;

    iget-object v1, p0, Lgcd;->A:Ljava/lang/String;

    .line 416
    iput-object v1, v0, Lgcf;->y:Ljava/lang/String;

    iget-boolean v1, p0, Lgcd;->I:Z

    .line 417
    iput-boolean v1, v0, Lgcf;->z:Z

    iget-object v1, p0, Lgcd;->B:Lgch;

    .line 418
    invoke-virtual {v0, v1}, Lgcf;->a(Lgch;)Lgcf;

    move-result-object v0

    iget-boolean v1, p0, Lgcd;->D:Z

    .line 419
    iput-boolean v1, v0, Lgcf;->A:Z

    iget-object v1, p0, Lgcd;->E:Ljava/util/Set;

    .line 420
    iput-object v1, v0, Lgcf;->B:Ljava/util/Set;

    iget-boolean v1, p0, Lgcd;->F:Z

    .line 421
    iput-boolean v1, v0, Lgcf;->C:Z

    iget-boolean v1, p0, Lgcd;->G:Z

    .line 422
    iput-boolean v1, v0, Lgcf;->D:Z

    iget-object v1, p0, Lgcd;->J:Landroid/net/Uri;

    .line 423
    iput-object v1, v0, Lgcf;->E:Landroid/net/Uri;

    iget-boolean v1, p0, Lgcd;->C:Z

    .line 424
    iput-boolean v1, v0, Lgcf;->F:Z

    iget-object v1, p0, Lgcd;->p:Ljava/lang/String;

    .line 425
    iput-object v1, v0, Lgcf;->G:Ljava/lang/String;

    iget-boolean v1, p0, Lgcd;->K:Z

    .line 426
    iput-boolean v1, v0, Lgcf;->H:Z

    iget-object v1, p0, Lgcd;->H:Ljava/lang/String;

    .line 427
    iput-object v1, v0, Lgcf;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 317
    instance-of v0, p1, Lgcd;

    if-nez v0, :cond_0

    .line 318
    const/4 v0, 0x0

    .line 321
    :goto_0
    return v0

    .line 320
    :cond_0
    check-cast p1, Lgcd;

    .line 321
    iget-object v0, p0, Lgcd;->b:Ljava/lang/String;

    iget-object v1, p1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 326
    iget-object v0, p0, Lgcd;->b:Ljava/lang/String;

    iget-object v1, p0, Lgcd;->j:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Video[id = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\', title=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\']"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
