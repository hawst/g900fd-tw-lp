.class public final Lfgq;
.super Lfsp;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lfsz;Lgit;)V
    .locals 1

    .prologue
    .line 281
    invoke-direct {p0, p1, p2}, Lfsp;-><init>(Lfsz;Lgit;)V

    .line 272
    const-string v0, ""

    iput-object v0, p0, Lfgq;->a:Ljava/lang/String;

    .line 273
    const-string v0, ""

    iput-object v0, p0, Lfgq;->b:Ljava/lang/String;

    .line 274
    const-string v0, ""

    iput-object v0, p0, Lfgq;->c:Ljava/lang/String;

    .line 275
    const-string v0, ""

    iput-object v0, p0, Lfgq;->d:Ljava/lang/String;

    .line 276
    const-string v0, ""

    iput-object v0, p0, Lfgq;->j:Ljava/lang/String;

    .line 282
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lfgq;
    .locals 1

    .prologue
    .line 285
    invoke-static {p1}, Lfgq;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgq;->a:Ljava/lang/String;

    .line 286
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    const-string v0, "ypc/complete_transaction"

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lfgq;
    .locals 1

    .prologue
    .line 290
    invoke-static {p1}, Lfgq;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgq;->c:Ljava/lang/String;

    .line 291
    return-object p0
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 311
    iget-object v0, p0, Lfgq;->c:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 313
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lfgq;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lfgq;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lfgq;->a([Ljava/lang/String;)V

    .line 314
    return-void
.end method

.method public final c(Ljava/lang/String;)Lfgq;
    .locals 1

    .prologue
    .line 295
    invoke-static {p1}, Lfgq;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgq;->b:Ljava/lang/String;

    .line 296
    return-object p0
.end method

.method public final synthetic c()Lidh;
    .locals 3

    .prologue
    .line 268
    new-instance v0, Libu;

    invoke-direct {v0}, Libu;-><init>()V

    invoke-virtual {p0}, Lfgq;->i()Lhjx;

    move-result-object v1

    iput-object v1, v0, Libu;->a:Lhjx;

    iget-object v1, p0, Lfgq;->c:Ljava/lang/String;

    iput-object v1, v0, Libu;->b:Ljava/lang/String;

    iget-object v1, p0, Lfgq;->a:Ljava/lang/String;

    iput-object v1, v0, Libu;->c:Ljava/lang/String;

    iget-object v1, p0, Lfgq;->b:Ljava/lang/String;

    iput-object v1, v0, Libu;->d:Ljava/lang/String;

    iget-object v1, p0, Lfgq;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lfgq;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lhfg;

    invoke-direct {v1}, Lhfg;-><init>()V

    iput-object v1, v0, Libu;->e:Lhfg;

    iget-object v1, v0, Libu;->e:Lhfg;

    iget-object v2, p0, Lfgq;->d:Ljava/lang/String;

    iput-object v2, v1, Lhfg;->a:Ljava/lang/String;

    iget-object v1, v0, Libu;->e:Lhfg;

    iget-object v2, p0, Lfgq;->j:Ljava/lang/String;

    iput-object v2, v1, Lhfg;->b:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public final d(Ljava/lang/String;)Lfgq;
    .locals 1

    .prologue
    .line 300
    invoke-static {p1}, Lfgq;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgq;->d:Ljava/lang/String;

    .line 301
    return-object p0
.end method

.method public final e(Ljava/lang/String;)Lfgq;
    .locals 1

    .prologue
    .line 305
    invoke-static {p1}, Lfgq;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgq;->j:Ljava/lang/String;

    .line 306
    return-object p0
.end method
