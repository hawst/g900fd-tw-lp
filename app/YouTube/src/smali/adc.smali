.class public final Ladc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Lfxe;

.field public c:Lfxg;

.field public d:I

.field public e:I

.field public f:Ljava/util/List;

.field public g:Leue;

.field final h:Ladd;

.field private final i:Leyp;

.field private final j:Lexd;

.field private k:Leue;

.field private l:Leue;


# direct methods
.method public constructor <init>(Ladd;Lfxe;Leyp;Lexd;)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput v0, p0, Ladc;->d:I

    .line 58
    iput v0, p0, Ladc;->e:I

    .line 70
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladd;

    iput-object v0, p0, Ladc;->h:Ladd;

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ladc;->a:Landroid/os/Handler;

    .line 72
    iput-object p2, p0, Ladc;->b:Lfxe;

    .line 73
    iput-object p3, p0, Ladc;->i:Leyp;

    .line 74
    iput-object p4, p0, Ladc;->j:Lexd;

    .line 75
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 98
    invoke-virtual {p0}, Ladc;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 99
    iget-object v0, p0, Ladc;->f:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v0, "due to no playlist being set."

    .line 101
    :goto_0
    const-string v1, "Ignoring call to next() on YouTubeThumbnailView "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, La;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    :goto_2
    return-void

    .line 99
    :cond_0
    const-string v0, "as already at the end of the playlist."

    goto :goto_0

    .line 101
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 104
    :cond_2
    iget v0, p0, Ladc;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ladc;->e:I

    .line 105
    invoke-virtual {p0}, Ladc;->e()V

    goto :goto_2
.end method

.method a(Lgcd;)V
    .locals 4

    .prologue
    .line 185
    if-eqz p1, :cond_0

    iget-object v0, p1, Lgcd;->f:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 186
    :cond_0
    invoke-virtual {p0}, Ladc;->f()V

    .line 192
    :goto_0
    return-void

    .line 189
    :cond_1
    new-instance v0, Ladf;

    iget-object v1, p1, Lgcd;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Ladf;-><init>(Ladc;Ljava/lang/String;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ladc;->l:Leue;

    .line 190
    iget-object v0, p0, Ladc;->i:Leyp;

    iget-object v1, p1, Lgcd;->f:Landroid/net/Uri;

    iget-object v2, p0, Ladc;->a:Landroid/os/Handler;

    iget-object v3, p0, Ladc;->l:Leue;

    .line 191
    invoke-static {v2, v3}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v2

    .line 190
    invoke-interface {v0, v1, v2}, Leyp;->b(Landroid/net/Uri;Leuc;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 120
    iget-object v0, p0, Ladc;->g:Leue;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Ladc;->g:Leue;

    iput-boolean v2, v0, Leue;->a:Z

    .line 122
    iput-object v1, p0, Ladc;->g:Leue;

    .line 124
    :cond_0
    iget-object v0, p0, Ladc;->k:Leue;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Ladc;->k:Leue;

    iput-boolean v2, v0, Leue;->a:Z

    .line 126
    iput-object v1, p0, Ladc;->k:Leue;

    .line 128
    :cond_1
    iget-object v0, p0, Ladc;->l:Leue;

    if-eqz v0, :cond_2

    .line 129
    iget-object v0, p0, Ladc;->l:Leue;

    iput-boolean v2, v0, Leue;->a:Z

    .line 130
    iput-object v1, p0, Ladc;->l:Leue;

    .line 132
    :cond_2
    iput-object v1, p0, Ladc;->c:Lfxg;

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Ladc;->e:I

    .line 134
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 146
    iget-object v1, p0, Ladc;->f:Ljava/util/List;

    if-nez v1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Ladc;->d:I

    iget-object v2, p0, Ladc;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_2

    iget-object v1, p0, Ladc;->c:Lfxg;

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 153
    iget-object v1, p0, Ladc;->f:Ljava/util/List;

    if-nez v1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Ladc;->d:I

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 166
    iget-object v0, p0, Ladc;->c:Lfxg;

    if-nez v0, :cond_0

    iget-object v0, p0, Ladc;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 167
    :goto_0
    const/4 v1, 0x0

    iget v2, p0, Ladc;->e:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ladc;->e:I

    .line 169
    iget v0, p0, Ladc;->e:I

    iget-object v1, p0, Ladc;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget v0, p0, Ladc;->e:I

    if-ltz v0, :cond_1

    .line 171
    iget v0, p0, Ladc;->e:I

    iput v0, p0, Ladc;->d:I

    .line 172
    iget-object v0, p0, Ladc;->f:Ljava/util/List;

    iget v1, p0, Ladc;->e:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    invoke-virtual {p0, v0}, Ladc;->a(Lgcd;)V

    .line 182
    :goto_1
    return-void

    .line 166
    :cond_0
    const v0, 0x7fffffff

    goto :goto_0

    .line 173
    :cond_1
    iget-object v0, p0, Ladc;->c:Lfxg;

    if-eqz v0, :cond_2

    .line 175
    new-instance v0, Lade;

    invoke-direct {v0, p0}, Lade;-><init>(Ladc;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ladc;->k:Leue;

    .line 176
    iget-object v0, p0, Ladc;->b:Lfxe;

    invoke-interface {v0}, Lfxe;->e()Lgku;

    move-result-object v0

    iget-object v1, p0, Ladc;->c:Lfxg;

    iget-object v2, p0, Ladc;->a:Landroid/os/Handler;

    iget-object v3, p0, Ladc;->k:Leue;

    .line 177
    invoke-static {v2, v3}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v2

    .line 176
    invoke-interface {v0, v1, v2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    goto :goto_1

    .line 180
    :cond_2
    invoke-virtual {p0}, Ladc;->f()V

    goto :goto_1
.end method

.method f()V
    .locals 4

    .prologue
    .line 195
    iget-object v0, p0, Ladc;->j:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lgwj;->a:Lgwj;

    .line 197
    :goto_0
    invoke-virtual {p0}, Ladc;->b()V

    .line 198
    iget-object v1, p0, Ladc;->h:Ladd;

    invoke-virtual {v0}, Lgwj;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ladc;->d()Z

    move-result v2

    invoke-virtual {p0}, Ladc;->c()Z

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Ladd;->a(Ljava/lang/String;ZZ)V

    .line 199
    return-void

    .line 195
    :cond_0
    sget-object v0, Lgwj;->b:Lgwj;

    goto :goto_0
.end method
