.class public final Lael;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final a:Landroid/widget/ImageButton;

.field final b:Landroid/widget/ImageButton;

.field final c:Landroid/widget/TextView;

.field final d:Landroid/view/animation/AnimationSet;

.field final e:Landroid/view/animation/AnimationSet;

.field f:[Ljava/lang/String;

.field g:I

.field h:Ldbt;

.field private final i:F

.field private final j:Landroid/widget/ImageButton;

.field private final k:Landroid/graphics/drawable/StateListDrawable;

.field private final l:Landroid/graphics/drawable/StateListDrawable;

.field private final m:Laen;

.field private final n:Laeo;

.field private o:Landroid/app/AlertDialog$Builder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Laen;)V
    .locals 10

    .prologue
    .line 80
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 82
    const-string v0, "optionsViewListener cannot be null"

    .line 83
    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laen;

    iput-object v0, p0, Lael;->m:Laen;

    .line 85
    invoke-virtual {p0}, Lael;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lael;->i:F

    .line 86
    const/high16 v0, 0x41200000    # 10.0f

    iget v1, p0, Lael;->i:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 88
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lael;->a:Landroid/widget/ImageButton;

    .line 89
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lael;->b:Landroid/widget/ImageButton;

    .line 90
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lael;->j:Landroid/widget/ImageButton;

    .line 91
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lael;->c:Landroid/widget/TextView;

    .line 93
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f09011e

    .line 94
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900a2

    new-instance v3, Laem;

    invoke-direct {v3, p0}, Laem;-><init>(Lael;)V

    .line 95
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iput-object v1, p0, Lael;->o:Landroid/app/AlertDialog$Builder;

    .line 96
    new-instance v1, Laeo;

    invoke-direct {v1, p0}, Laeo;-><init>(Lael;)V

    iput-object v1, p0, Lael;->n:Laeo;

    .line 98
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iput-object v1, p0, Lael;->k:Landroid/graphics/drawable/StateListDrawable;

    .line 99
    iget-object v1, p0, Lael;->k:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->SELECTED_STATE_SET:[I

    const v3, 0x7f020046

    .line 101
    invoke-static {p1, v3}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 99
    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 102
    iget-object v1, p0, Lael;->k:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->EMPTY_STATE_SET:[I

    const v3, 0x7f020045

    .line 104
    invoke-static {p1, v3}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 102
    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 105
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iput-object v1, p0, Lael;->l:Landroid/graphics/drawable/StateListDrawable;

    .line 106
    iget-object v1, p0, Lael;->l:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->SELECTED_STATE_SET:[I

    const v3, 0x7f020044

    .line 108
    invoke-static {p1, v3}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 106
    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 109
    iget-object v1, p0, Lael;->l:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->EMPTY_STATE_SET:[I

    const v3, 0x7f020043

    .line 111
    invoke-static {p1, v3}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 109
    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 113
    iget-object v1, p0, Lael;->a:Landroid/widget/ImageButton;

    iget-object v2, p0, Lael;->k:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    iget-object v1, p0, Lael;->a:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 115
    iget-object v1, p0, Lael;->a:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v0, v2, v0}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 116
    iget-object v1, p0, Lael;->a:Landroid/widget/ImageButton;

    const v2, 0x7f090133

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v1, p0, Lael;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v1, p0, Lael;->a:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 120
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 121
    sget-object v2, Landroid/view/View;->SELECTED_STATE_SET:[I

    const v3, 0x7f020042

    .line 123
    invoke-static {p1, v3}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 121
    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 124
    sget-object v2, Landroid/view/View;->EMPTY_STATE_SET:[I

    const v3, 0x7f020041

    .line 126
    invoke-static {p1, v3}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 124
    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 128
    iget-object v2, p0, Lael;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    iget-object v1, p0, Lael;->b:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    iget-object v1, p0, Lael;->b:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v0, v2, v0}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 131
    iget-object v1, p0, Lael;->b:Landroid/widget/ImageButton;

    const v2, 0x7f090132

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v1, p0, Lael;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    iget-object v1, p0, Lael;->b:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 135
    iget-object v1, p0, Lael;->j:Landroid/widget/ImageButton;

    const v2, 0x7f020054

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 136
    iget-object v1, p0, Lael;->j:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 137
    iget-object v1, p0, Lael;->j:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 138
    invoke-static {p1}, La;->w(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lael;->j:Landroid/widget/ImageButton;

    const v2, 0x7f09013c

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v1, p0, Lael;->j:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    :cond_0
    iget-object v1, p0, Lael;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    .line 144
    iget-object v1, p0, Lael;->c:Landroid/widget/TextView;

    const/4 v2, 0x1

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 145
    iget-object v1, p0, Lael;->c:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 146
    iget-object v1, p0, Lael;->c:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 147
    iget-object v1, p0, Lael;->c:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 148
    iget-object v1, p0, Lael;->c:Landroid/widget/TextView;

    mul-int/lit8 v2, v0, 0x2

    invoke-virtual {v1, v2, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 150
    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, p0, Lael;->d:Landroid/view/animation/AnimationSet;

    .line 153
    iget-object v0, p0, Lael;->d:Landroid/view/animation/AnimationSet;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 154
    iget-object v0, p0, Lael;->d:Landroid/view/animation/AnimationSet;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 155
    iget-object v0, p0, Lael;->d:Landroid/view/animation/AnimationSet;

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 156
    iget-object v9, p0, Lael;->d:Landroid/view/animation/AnimationSet;

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const v2, 0x3ecccccd    # 0.4f

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 158
    iget-object v0, p0, Lael;->d:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, p0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 160
    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, p0, Lael;->e:Landroid/view/animation/AnimationSet;

    .line 161
    iget-object v0, p0, Lael;->e:Landroid/view/animation/AnimationSet;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 162
    iget-object v0, p0, Lael;->e:Landroid/view/animation/AnimationSet;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 163
    iget-object v0, p0, Lael;->e:Landroid/view/animation/AnimationSet;

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 164
    iget-object v9, p0, Lael;->e:Landroid/view/animation/AnimationSet;

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const v4, 0x3ecccccd    # 0.4f

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 166
    iget-object v0, p0, Lael;->e:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, p0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 168
    iget-object v0, p0, Lael;->a:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lael;->addView(Landroid/view/View;)V

    .line 169
    iget-object v0, p0, Lael;->b:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lael;->addView(Landroid/view/View;)V

    .line 170
    iget-object v0, p0, Lael;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lael;->addView(Landroid/view/View;)V

    .line 171
    iget-object v0, p0, Lael;->j:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lael;->addView(Landroid/view/View;)V

    .line 173
    const v0, 0x7f020057

    invoke-virtual {p0, v0}, Lael;->setBackgroundResource(I)V

    .line 174
    const/4 v0, 0x4

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 175
    return-void
.end method

.method private static a(Landroid/view/View;II)I
    .locals 3

    .prologue
    .line 293
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v0, p2, v0

    div-int/lit8 v0, v0, 0x2

    .line 294
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 295
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lael;)Laen;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lael;->m:Laen;

    return-object v0
.end method

.method static synthetic b(Lael;)Landroid/view/animation/AnimationSet;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lael;->e:Landroid/view/animation/AnimationSet;

    return-object v0
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lael;->e:Landroid/view/animation/AnimationSet;

    if-ne p1, v0, :cond_0

    .line 197
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lael;->setVisibility(I)V

    .line 199
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 305
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 308
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 230
    iget-object v0, p0, Lael;->a:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lael;->f:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lael;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 231
    iget-object v0, p0, Lael;->o:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lael;->f:[Ljava/lang/String;

    iget v2, p0, Lael;->g:I

    iget-object v3, p0, Lael;->n:Laeo;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 234
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 235
    iget-object v0, p0, Lael;->m:Laen;

    invoke-interface {v0}, Laen;->b()V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-object v0, p0, Lael;->b:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 237
    iget-object v0, p0, Lael;->h:Ldbt;

    invoke-interface {v0}, Ldbt;->f()V

    .line 238
    iget-object v0, p0, Lael;->m:Laen;

    invoke-interface {v0}, Laen;->b()V

    goto :goto_0

    .line 239
    :cond_2
    iget-object v0, p0, Lael;->j:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 240
    iget-object v0, p0, Lael;->m:Laen;

    invoke-interface {v0}, Laen;->a()V

    goto :goto_0
.end method

.method protected final onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/16 v4, 0x8

    .line 276
    sub-int v1, p5, p3

    .line 277
    sub-int v2, p4, p2

    .line 280
    iget-object v3, p0, Lael;->a:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 281
    iget-object v3, p0, Lael;->a:Landroid/widget/ImageButton;

    invoke-static {v3, v0, v1}, Lael;->a(Landroid/view/View;II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 283
    :cond_0
    iget-object v3, p0, Lael;->b:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v3

    if-eq v3, v4, :cond_1

    .line 284
    iget-object v3, p0, Lael;->b:Landroid/widget/ImageButton;

    invoke-static {v3, v0, v1}, Lael;->a(Landroid/view/View;II)I

    move-result v3

    add-int/2addr v0, v3

    .line 286
    :cond_1
    iget-object v3, p0, Lael;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-eq v3, v4, :cond_2

    .line 287
    iget-object v3, p0, Lael;->c:Landroid/widget/TextView;

    invoke-static {v3, v0, v1}, Lael;->a(Landroid/view/View;II)I

    .line 289
    :cond_2
    iget-object v0, p0, Lael;->j:Landroid/widget/ImageButton;

    iget-object v3, p0, Lael;->j:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v0, v2, v1}, Lael;->a(Landroid/view/View;II)I

    .line 290
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x8

    const/high16 v4, -0x80000000

    .line 250
    invoke-static {v1, p1}, Lael;->getDefaultSize(II)I

    move-result v0

    .line 251
    invoke-static {v1, p2}, Lael;->getDefaultSize(II)I

    move-result v1

    .line 253
    invoke-virtual {p0, v0, v1}, Lael;->setMeasuredDimension(II)V

    .line 255
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 258
    iget-object v2, p0, Lael;->j:Landroid/widget/ImageButton;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/ImageButton;->measure(II)V

    .line 259
    iget-object v2, p0, Lael;->j:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    .line 260
    iget-object v2, p0, Lael;->a:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_0

    .line 261
    iget-object v2, p0, Lael;->a:Landroid/widget/ImageButton;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/ImageButton;->measure(II)V

    .line 262
    iget-object v2, p0, Lael;->a:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    .line 264
    :cond_0
    iget-object v2, p0, Lael;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_1

    .line 265
    iget-object v2, p0, Lael;->b:Landroid/widget/ImageButton;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/ImageButton;->measure(II)V

    .line 266
    iget-object v2, p0, Lael;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    .line 268
    :cond_1
    iget-object v2, p0, Lael;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_2

    .line 270
    iget-object v2, p0, Lael;->c:Landroid/widget/TextView;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 272
    :cond_2
    return-void
.end method

.method public final setVisibility(I)V
    .locals 2

    .prologue
    .line 203
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 204
    iget-object v1, p0, Lael;->m:Laen;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Laen;->a(Z)V

    .line 205
    return-void

    .line 204
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
