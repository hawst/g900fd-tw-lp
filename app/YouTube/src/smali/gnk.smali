.class public final Lgnk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:[Ljava/lang/String;


# instance fields
.field final a:Levi;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "language_code"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "source_language_code"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "language_name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "track_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "format"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "subtitles_path"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "track_vss_id"

    aput-object v2, v0, v1

    sput-object v0, Lgnk;->b:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Levi;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lgnk;->a:Levi;

    .line 49
    return-void
.end method

.method static a(Lgpa;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 104
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v0, p0, Lgpa;->g:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 106
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 107
    if-eqz p0, :cond_0

    .line 108
    const-string v1, "video_id"

    iget-object v2, p0, Lgpa;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v1, "language_code"

    iget-object v2, p0, Lgpa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v1, "source_language_code"

    iget-object v2, p0, Lgpa;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v1, "language_name"

    iget-object v2, p0, Lgpa;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v1, "track_name"

    iget-object v2, p0, Lgpa;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v1, "format"

    iget v2, p0, Lgpa;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 114
    const-string v1, "subtitles_path"

    iget-object v2, p0, Lgpa;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v1, "track_vss_id"

    iget-object v2, p0, Lgpa;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Ljava/util/List;
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 69
    iget-object v0, p0, Lgnk;->a:Levi;

    invoke-interface {v0}, Levi;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "subtitles"

    sget-object v2, Lgnk;->b:[Ljava/lang/String;

    const-string v3, "video_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 79
    :try_start_0
    new-instance v8, Lgnl;

    invoke-direct {v8, v7}, Lgnl;-><init>(Landroid/database/Cursor;)V

    .line 80
    new-instance v9, Ljava/util/ArrayList;

    iget-object v0, v8, Lgnl;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    iget-object v0, v8, Lgnl;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v8, Lgnl;->a:Landroid/database/Cursor;

    iget v1, v8, Lgnl;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v8, Lgnl;->a:Landroid/database/Cursor;

    iget v2, v8, Lgnl;->d:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v8, Lgnl;->a:Landroid/database/Cursor;

    iget v3, v8, Lgnl;->e:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v8, Lgnl;->a:Landroid/database/Cursor;

    iget v4, v8, Lgnl;->b:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v8, Lgnl;->a:Landroid/database/Cursor;

    iget v5, v8, Lgnl;->f:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-object v5, v8, Lgnl;->a:Landroid/database/Cursor;

    iget v6, v8, Lgnl;->g:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v8, Lgnl;->a:Landroid/database/Cursor;

    iget v10, v8, Lgnl;->h:I

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lgpa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lgpa;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    return-object v9
.end method
