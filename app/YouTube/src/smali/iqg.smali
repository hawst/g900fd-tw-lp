.class final Liqg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lipu;


# instance fields
.field public final a:Lipq;

.field public final b:Liql;

.field c:Z


# direct methods
.method public constructor <init>(Liql;)V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lipq;

    invoke-direct {v0}, Lipq;-><init>()V

    invoke-direct {p0, p1, v0}, Liqg;-><init>(Liql;Lipq;)V

    .line 38
    return-void
.end method

.method private constructor <init>(Liql;Lipq;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    iput-object p2, p0, Liqg;->a:Lipq;

    .line 33
    iput-object p1, p0, Liqg;->b:Liql;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(B)J
    .locals 8

    .prologue
    const-wide/16 v2, -0x1

    .line 239
    iget-boolean v0, p0, Liqg;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_0
    const-wide/16 v0, 0x0

    .line 242
    :cond_1
    iget-object v4, p0, Liqg;->a:Lipq;

    invoke-virtual {v4, p1, v0, v1}, Lipq;->a(BJ)J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    .line 243
    iget-object v0, p0, Liqg;->a:Lipq;

    iget-wide v0, v0, Lipq;->b:J

    .line 244
    iget-object v4, p0, Liqg;->b:Liql;

    iget-object v5, p0, Liqg;->a:Lipq;

    const-wide/16 v6, 0x800

    invoke-interface {v4, v5, v6, v7}, Liql;->b(Lipq;J)J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-nez v4, :cond_1

    move-wide v0, v2

    .line 246
    :cond_2
    return-wide v0
.end method

.method public final a()Liqm;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Liqg;->b:Liql;

    invoke-interface {v0}, Liql;->a()Liqm;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 64
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    iget-boolean v0, p0, Liqg;->c:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_1
    iget-object v0, p0, Liqg;->a:Lipq;

    iget-wide v0, v0, Lipq;->b:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_2

    .line 67
    iget-object v0, p0, Liqg;->b:Liql;

    iget-object v1, p0, Liqg;->a:Lipq;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Liql;->b(Lipq;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 69
    :cond_2
    return-void
.end method

.method public final b(Lipq;J)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v0, -0x1

    .line 45
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    cmp-long v2, p2, v4

    if-gez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_1
    iget-boolean v2, p0, Liqg;->c:Z

    if-eqz v2, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_2
    iget-object v2, p0, Liqg;->a:Lipq;

    iget-wide v2, v2, Lipq;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 50
    iget-object v2, p0, Liqg;->b:Liql;

    iget-object v3, p0, Liqg;->a:Lipq;

    const-wide/16 v4, 0x800

    invoke-interface {v2, v3, v4, v5}, Liql;->b(Lipq;J)J

    move-result-wide v2

    .line 51
    cmp-long v2, v2, v0

    if-nez v2, :cond_3

    .line 55
    :goto_0
    return-wide v0

    .line 54
    :cond_3
    iget-object v0, p0, Liqg;->a:Lipq;

    iget-wide v0, v0, Lipq;->b:J

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 55
    iget-object v2, p0, Liqg;->a:Lipq;

    invoke-virtual {v2, p1, v0, v1}, Lipq;->b(Lipq;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final c()Lipq;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Liqg;->a:Lipq;

    return-object v0
.end method

.method public final c(J)Lipv;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1, p2}, Liqg;->a(J)V

    .line 83
    iget-object v0, p0, Liqg;->a:Lipq;

    invoke-virtual {v0, p1, p2}, Lipq;->c(J)Lipv;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Liqg;->c:Z

    if-eqz v0, :cond_0

    .line 292
    :goto_0
    return-void

    .line 289
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Liqg;->c:Z

    .line 290
    iget-object v0, p0, Liqg;->b:Liql;

    invoke-interface {v0}, Liql;->close()V

    .line 291
    iget-object v0, p0, Liqg;->a:Lipq;

    invoke-virtual {v0}, Lipq;->n()V

    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 59
    iget-boolean v0, p0, Liqg;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    iget-object v0, p0, Liqg;->a:Lipq;

    invoke-virtual {v0}, Lipq;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Liqg;->b:Liql;

    iget-object v1, p0, Liqg;->a:Lipq;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Liql;->b(Lipq;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(J)[B
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Liqg;->a(J)V

    .line 93
    iget-object v0, p0, Liqg;->a:Lipq;

    invoke-virtual {v0, p1, p2}, Lipq;->e(J)[B

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 250
    new-instance v0, Liqh;

    invoke-direct {v0, p0}, Liqh;-><init>(Liqg;)V

    return-object v0
.end method

.method public final f(J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 227
    iget-boolean v0, p0, Liqg;->c:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    iget-object v0, p0, Liqg;->a:Lipq;

    iget-wide v0, v0, Lipq;->b:J

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 233
    iget-object v2, p0, Liqg;->a:Lipq;

    invoke-virtual {v2, v0, v1}, Lipq;->f(J)V

    .line 234
    sub-long/2addr p1, v0

    .line 228
    :cond_1
    cmp-long v0, p1, v4

    if-lez v0, :cond_2

    .line 229
    iget-object v0, p0, Liqg;->a:Lipq;

    iget-wide v0, v0, Lipq;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Liqg;->b:Liql;

    iget-object v1, p0, Liqg;->a:Lipq;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Liql;->b(Lipq;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 230
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 236
    :cond_2
    return-void
.end method

.method public final g()B
    .locals 2

    .prologue
    .line 72
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Liqg;->a(J)V

    .line 73
    iget-object v0, p0, Liqg;->a:Lipq;

    invoke-virtual {v0}, Lipq;->g()B

    move-result v0

    return v0
.end method

.method public final h()S
    .locals 2

    .prologue
    .line 197
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Liqg;->a(J)V

    .line 198
    iget-object v0, p0, Liqg;->a:Lipq;

    invoke-virtual {v0}, Lipq;->h()S

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 207
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Liqg;->a(J)V

    .line 208
    iget-object v0, p0, Liqg;->a:Lipq;

    invoke-virtual {v0}, Lipq;->i()I

    move-result v0

    return v0
.end method

.method public final j()S
    .locals 2

    .prologue
    .line 202
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Liqg;->a(J)V

    .line 203
    iget-object v0, p0, Liqg;->a:Lipq;

    invoke-virtual {v0}, Lipq;->h()S

    move-result v0

    invoke-static {v0}, Liqo;->a(S)S

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 212
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Liqg;->a(J)V

    .line 213
    iget-object v0, p0, Liqg;->a:Lipq;

    invoke-virtual {v0}, Lipq;->i()I

    move-result v0

    invoke-static {v0}, Liqo;->a(I)I

    move-result v0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 191
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Liqg;->a(B)J

    move-result-wide v0

    .line 192
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 193
    :cond_0
    iget-object v2, p0, Liqg;->a:Lipq;

    invoke-virtual {v2, v0, v1}, Lipq;->d(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "buffer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Liqg;->b:Liql;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
