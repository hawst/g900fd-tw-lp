.class public final Lhez;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhxf;

.field public c:Lhgz;

.field private d:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42995
    invoke-direct {p0}, Lidf;-><init>()V

    .line 42998
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhez;->d:J

    .line 43001
    iput-object v2, p0, Lhez;->a:Lhgz;

    .line 43004
    iput-object v2, p0, Lhez;->b:Lhxf;

    .line 43007
    iput-object v2, p0, Lhez;->c:Lhgz;

    .line 42995
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    .line 43062
    const/4 v0, 0x0

    .line 43063
    iget-wide v2, p0, Lhez;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 43064
    const/4 v0, 0x1

    iget-wide v2, p0, Lhez;->d:J

    .line 43065
    invoke-static {v0, v2, v3}, Lidd;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 43067
    :cond_0
    iget-object v1, p0, Lhez;->a:Lhgz;

    if-eqz v1, :cond_1

    .line 43068
    const/4 v1, 0x2

    iget-object v2, p0, Lhez;->a:Lhgz;

    .line 43069
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43071
    :cond_1
    iget-object v1, p0, Lhez;->b:Lhxf;

    if-eqz v1, :cond_2

    .line 43072
    const/4 v1, 0x3

    iget-object v2, p0, Lhez;->b:Lhxf;

    .line 43073
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43075
    :cond_2
    iget-object v1, p0, Lhez;->c:Lhgz;

    if-eqz v1, :cond_3

    .line 43076
    const/4 v1, 0x4

    iget-object v2, p0, Lhez;->c:Lhgz;

    .line 43077
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43079
    :cond_3
    iget-object v1, p0, Lhez;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43080
    iput v0, p0, Lhez;->J:I

    .line 43081
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 42991
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhez;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhez;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhez;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhez;->d:J

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhez;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhez;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhez;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhez;->b:Lhxf;

    if-nez v0, :cond_3

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhez;->b:Lhxf;

    :cond_3
    iget-object v0, p0, Lhez;->b:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhez;->c:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhez;->c:Lhgz;

    :cond_4
    iget-object v0, p0, Lhez;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 4

    .prologue
    .line 43044
    iget-wide v0, p0, Lhez;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 43045
    const/4 v0, 0x1

    iget-wide v2, p0, Lhez;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 43047
    :cond_0
    iget-object v0, p0, Lhez;->a:Lhgz;

    if-eqz v0, :cond_1

    .line 43048
    const/4 v0, 0x2

    iget-object v1, p0, Lhez;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 43050
    :cond_1
    iget-object v0, p0, Lhez;->b:Lhxf;

    if-eqz v0, :cond_2

    .line 43051
    const/4 v0, 0x3

    iget-object v1, p0, Lhez;->b:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 43053
    :cond_2
    iget-object v0, p0, Lhez;->c:Lhgz;

    if-eqz v0, :cond_3

    .line 43054
    const/4 v0, 0x4

    iget-object v1, p0, Lhez;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 43056
    :cond_3
    iget-object v0, p0, Lhez;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 43058
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43021
    if-ne p1, p0, :cond_1

    .line 43028
    :cond_0
    :goto_0
    return v0

    .line 43022
    :cond_1
    instance-of v2, p1, Lhez;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 43023
    :cond_2
    check-cast p1, Lhez;

    .line 43024
    iget-wide v2, p0, Lhez;->d:J

    iget-wide v4, p1, Lhez;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhez;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhez;->a:Lhgz;

    if-nez v2, :cond_3

    .line 43025
    :goto_1
    iget-object v2, p0, Lhez;->b:Lhxf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhez;->b:Lhxf;

    if-nez v2, :cond_3

    .line 43026
    :goto_2
    iget-object v2, p0, Lhez;->c:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhez;->c:Lhgz;

    if-nez v2, :cond_3

    .line 43027
    :goto_3
    iget-object v2, p0, Lhez;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhez;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 43028
    goto :goto_0

    .line 43024
    :cond_4
    iget-object v2, p0, Lhez;->a:Lhgz;

    iget-object v3, p1, Lhez;->a:Lhgz;

    .line 43025
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhez;->b:Lhxf;

    iget-object v3, p1, Lhez;->b:Lhxf;

    .line 43026
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhez;->c:Lhgz;

    iget-object v3, p1, Lhez;->c:Lhgz;

    .line 43027
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhez;->I:Ljava/util/List;

    iget-object v3, p1, Lhez;->I:Ljava/util/List;

    .line 43028
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 43032
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 43034
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhez;->d:J

    iget-wide v4, p0, Lhez;->d:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 43035
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhez;->a:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 43036
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhez;->b:Lhxf;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 43037
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhez;->c:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 43038
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhez;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 43039
    return v0

    .line 43035
    :cond_0
    iget-object v0, p0, Lhez;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 43036
    :cond_1
    iget-object v0, p0, Lhez;->b:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_1

    .line 43037
    :cond_2
    iget-object v0, p0, Lhez;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 43038
    :cond_3
    iget-object v1, p0, Lhez;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
