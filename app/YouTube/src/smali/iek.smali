.class public final Liek;
.super Leqd;


# instance fields
.field private c:[Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Leqd;-><init>()V

    sget-object v0, Leqm;->b:[Ljava/lang/String;

    iput-object v0, p0, Liek;->c:[Ljava/lang/String;

    sget-object v0, Leqm;->b:[Ljava/lang/String;

    iput-object v0, p0, Liek;->d:[Ljava/lang/String;

    sget-object v0, Leqm;->a:[I

    iput-object v0, p0, Liek;->e:[I

    const/4 v0, 0x0

    iput-object v0, p0, Liek;->a:Leqh;

    const/4 v0, -0x1

    iput v0, p0, Liek;->b:I

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Liek;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Liek;

    iget-object v2, p0, Liek;->c:[Ljava/lang/String;

    iget-object v3, p1, Liek;->c:[Ljava/lang/String;

    invoke-static {v2, v3}, Leqj;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Liek;->d:[Ljava/lang/String;

    iget-object v3, p1, Liek;->d:[Ljava/lang/String;

    invoke-static {v2, v3}, Leqj;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Liek;->e:[I

    iget-object v3, p1, Liek;->e:[I

    invoke-static {v2, v3}, Leqj;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Liek;->c:[Ljava/lang/String;

    invoke-static {v0}, Leqj;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Liek;->d:[Ljava/lang/String;

    invoke-static {v1}, Leqj;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Liek;->e:[I

    invoke-static {v1}, Leqj;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method
