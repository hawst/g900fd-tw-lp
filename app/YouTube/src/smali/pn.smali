.class final Lpn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;


# instance fields
.field private synthetic a:Lpi;


# direct methods
.method constructor <init>(Lpi;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Lpn;->a:Lpi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->e(Lpi;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 570
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-virtual {v0}, Lpi;->a()Z

    .line 571
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->a(Lpi;)Lpm;

    move-result-object v0

    iget-object v0, v0, Lpm;->a:Lpc;

    invoke-virtual {v0}, Lpc;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 572
    iget-object v1, p0, Lpn;->a:Lpi;

    invoke-static {v1}, Lpi;->a(Lpi;)Lpm;

    move-result-object v1

    iget-object v1, v1, Lpm;->a:Lpc;

    invoke-virtual {v1, v0}, Lpc;->a(Landroid/content/pm/ResolveInfo;)I

    move-result v0

    .line 573
    iget-object v1, p0, Lpn;->a:Lpi;

    invoke-static {v1}, Lpi;->a(Lpi;)Lpm;

    move-result-object v1

    iget-object v1, v1, Lpm;->a:Lpc;

    invoke-virtual {v1, v0}, Lpc;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 574
    if-eqz v0, :cond_0

    .line 575
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 576
    iget-object v1, p0, Lpn;->a:Lpi;

    invoke-virtual {v1}, Lpi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 580
    :cond_0
    :goto_0
    return-void

    .line 578
    :cond_1
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->f(Lpi;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 579
    iget-object v0, p0, Lpn;->a:Lpi;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lpi;->a(Lpi;Z)Z

    .line 580
    iget-object v0, p0, Lpn;->a:Lpi;

    iget-object v1, p0, Lpn;->a:Lpi;

    invoke-static {v1}, Lpi;->g(Lpi;)I

    move-result v1

    invoke-static {v0, v1}, Lpi;->a(Lpi;I)V

    goto :goto_0

    .line 582
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public final onDismiss()V
    .locals 2

    .prologue
    .line 602
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->h(Lpi;)Landroid/widget/PopupWindow$OnDismissListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->h(Lpi;)Landroid/widget/PopupWindow$OnDismissListener;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    .line 603
    :cond_0
    iget-object v0, p0, Lpn;->a:Lpi;

    iget-object v0, v0, Lpi;->c:Lek;

    if-eqz v0, :cond_1

    .line 604
    iget-object v0, p0, Lpn;->a:Lpi;

    iget-object v0, v0, Lpi;->c:Lek;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lek;->a(Z)V

    .line 606
    :cond_1
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    .prologue
    .line 538
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lpm;

    .line 539
    invoke-virtual {v0, p3}, Lpm;->getItemViewType(I)I

    move-result v0

    .line 540
    packed-switch v0, :pswitch_data_0

    .line 563
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 542
    :pswitch_0
    iget-object v0, p0, Lpn;->a:Lpi;

    const v1, 0x7fffffff

    invoke-static {v0, v1}, Lpi;->a(Lpi;I)V

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 545
    :pswitch_1
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-virtual {v0}, Lpi;->a()Z

    .line 546
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->d(Lpi;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 548
    if-lez p3, :cond_0

    .line 549
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->a(Lpi;)Lpm;

    move-result-object v0

    iget-object v2, v0, Lpm;->a:Lpc;

    iget-object v3, v2, Lpc;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-virtual {v2}, Lpc;->d()V

    iget-object v0, v2, Lpc;->b:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpd;

    iget-object v1, v2, Lpc;->b:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpd;

    if-eqz v1, :cond_1

    iget v1, v1, Lpd;->b:F

    iget v4, v0, Lpd;->b:F

    sub-float/2addr v1, v4

    const/high16 v4, 0x40a00000    # 5.0f

    add-float/2addr v1, v4

    :goto_1
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, v0, Lpd;->a:Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Lpd;->a:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v5, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lpg;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v0, v4, v6, v7, v1}, Lpg;-><init>(Landroid/content/ComponentName;JF)V

    iget-object v1, v2, Lpc;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, v2, Lpc;->g:Z

    invoke-virtual {v2}, Lpc;->e()V

    iget-boolean v0, v2, Lpc;->f:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No preceding call to #readHistoricalData"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_2
    :try_start_1
    iget-boolean v0, v2, Lpc;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, v2, Lpc;->g:Z

    iget-object v0, v2, Lpc;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lph;

    invoke-direct {v0, v2}, Lph;-><init>(Lpc;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v2, Lpc;->c:Ljava/util/List;

    aput-object v5, v1, v4

    const/4 v4, 0x1

    iget-object v5, v2, Lpc;->d:Ljava/lang/String;

    aput-object v5, v1, v4

    invoke-static {v0, v1}, La;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_3
    iget-object v0, v2, Lpc;->e:Lpe;

    invoke-virtual {v2}, Lpc;->notifyChanged()V

    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 554
    :cond_5
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->a(Lpi;)Lpm;

    move-result-object v0

    iget-boolean v0, v0, Lpm;->b:Z

    if-eqz v0, :cond_6

    .line 555
    :goto_2
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->a(Lpi;)Lpm;

    move-result-object v0

    iget-object v0, v0, Lpm;->a:Lpc;

    invoke-virtual {v0, p3}, Lpc;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 556
    if-eqz v0, :cond_0

    .line 557
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 558
    iget-object v1, p0, Lpn;->a:Lpi;

    invoke-virtual {v1}, Lpi;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 554
    :cond_6
    add-int/lit8 p3, p3, 0x1

    goto :goto_2

    .line 540
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 589
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->e(Lpi;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 590
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0}, Lpi;->a(Lpi;)Lpm;

    move-result-object v0

    invoke-virtual {v0}, Lpm;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 591
    iget-object v0, p0, Lpn;->a:Lpi;

    invoke-static {v0, v2}, Lpi;->a(Lpi;Z)Z

    .line 592
    iget-object v0, p0, Lpn;->a:Lpi;

    iget-object v1, p0, Lpn;->a:Lpi;

    invoke-static {v1}, Lpi;->g(Lpi;)I

    move-result v1

    invoke-static {v0, v1}, Lpi;->a(Lpi;I)V

    .line 597
    :cond_0
    return v2

    .line 595
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method
