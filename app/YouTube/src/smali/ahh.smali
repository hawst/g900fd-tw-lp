.class public final Lahh;
.super Laiu;
.source "SourceFile"


# instance fields
.field private final a:Ladt;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ladt;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Laiu;-><init>()V

    .line 23
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladt;

    iput-object v0, p0, Lahh;->a:Ladt;

    .line 24
    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lahh;->b:Landroid/os/Handler;

    .line 26
    new-instance v0, Lahi;

    invoke-direct {v0, p0, p1}, Lahi;-><init>(Lahh;Ladt;)V

    iput-object v0, p0, Lahh;->c:Ljava/lang/Runnable;

    .line 32
    new-instance v0, Lahj;

    invoke-direct {v0, p0, p1}, Lahj;-><init>(Lahh;Ladt;)V

    iput-object v0, p0, Lahh;->d:Ljava/lang/Runnable;

    .line 38
    return-void
.end method

.method static synthetic a(Lahh;)Ladt;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lahh;->a:Ladt;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lahh;->b:Landroid/os/Handler;

    new-instance v1, Lahk;

    invoke-direct {v1, p0}, Lahk;-><init>(Lahh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 48
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lahh;->b:Landroid/os/Handler;

    iget-object v1, p0, Lahh;->c:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 73
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lahh;->b:Landroid/os/Handler;

    new-instance v1, Lahl;

    invoke-direct {v1, p0, p1, p2}, Lahl;-><init>(Lahh;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 58
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lahh;->b:Landroid/os/Handler;

    new-instance v1, Lahm;

    invoke-direct {v1, p0, p1}, Lahm;-><init>(Lahh;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 68
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lahh;->b:Landroid/os/Handler;

    iget-object v1, p0, Lahh;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 78
    return-void
.end method
