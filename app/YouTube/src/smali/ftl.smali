.class public final Lftl;
.super Lfsp;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field public c:Lfnn;

.field private d:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lfsz;Lgit;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lfsp;-><init>(Lfsz;Lgit;)V

    .line 32
    sget-object v0, Lfhy;->a:[B

    invoke-virtual {p0, v0}, Lftl;->a([B)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lftl;
    .locals 1

    .prologue
    .line 51
    invoke-static {p1}, Lftl;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lftl;->d:Ljava/lang/String;

    .line 52
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const-string v0, "conversation/share_to_conversation"

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lftl;
    .locals 1

    .prologue
    .line 56
    invoke-static {p1}, Lftl;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lftl;->j:Ljava/lang/String;

    .line 57
    return-object p0
.end method

.method protected final b()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lftl;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lftl;->a:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 90
    :cond_0
    iget-object v0, p0, Lftl;->b:[Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v0, p0, Lftl;->b:[Ljava/lang/String;

    array-length v0, v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 93
    :cond_1
    iget-object v0, p0, Lftl;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lftl;->j:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 96
    :cond_2
    iget-object v0, p0, Lftl;->c:Lfnn;

    if-eqz v0, :cond_3

    .line 97
    iget-object v0, p0, Lftl;->c:Lfnn;

    invoke-virtual {v0}, Lfnn;->a()Landroid/graphics/PointF;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lftl;->c:Lfnn;

    invoke-virtual {v0}, Lfnn;->b()Landroid/graphics/PointF;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lftl;->c:Lfnn;

    invoke-virtual {v0}, Lfnn;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :cond_3
    return-void

    .line 91
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic c()Lidh;
    .locals 4

    .prologue
    .line 18
    new-instance v0, Lhvi;

    invoke-direct {v0}, Lhvi;-><init>()V

    invoke-virtual {p0}, Lftl;->i()Lhjx;

    move-result-object v1

    iput-object v1, v0, Lhvi;->a:Lhjx;

    iget-object v1, p0, Lftl;->a:[Ljava/lang/String;

    iput-object v1, v0, Lhvi;->c:[Ljava/lang/String;

    iget-object v1, p0, Lftl;->b:[Ljava/lang/String;

    iput-object v1, v0, Lhvi;->d:[Ljava/lang/String;

    iget-object v1, p0, Lftl;->d:Ljava/lang/String;

    iput-object v1, v0, Lhvi;->b:Ljava/lang/String;

    iget-object v1, p0, Lftl;->j:Ljava/lang/String;

    iput-object v1, v0, Lhvi;->e:Ljava/lang/String;

    iget-object v1, p0, Lftl;->c:Lfnn;

    if-eqz v1, :cond_0

    new-instance v1, Lial;

    invoke-direct {v1}, Lial;-><init>()V

    iget-object v2, p0, Lftl;->c:Lfnn;

    invoke-virtual {v2}, Lfnn;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lial;->b:Ljava/lang/String;

    new-instance v2, Liam;

    invoke-direct {v2}, Liam;-><init>()V

    iput-object v2, v1, Lial;->c:Liam;

    iget-object v2, v1, Lial;->c:Liam;

    iget-object v3, p0, Lftl;->c:Lfnn;

    invoke-virtual {v3}, Lfnn;->a()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iput v3, v2, Liam;->a:F

    iget-object v2, v1, Lial;->c:Liam;

    iget-object v3, p0, Lftl;->c:Lfnn;

    invoke-virtual {v3}, Lfnn;->a()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iput v3, v2, Liam;->b:F

    iget-object v2, v1, Lial;->c:Liam;

    iget-object v3, p0, Lftl;->c:Lfnn;

    invoke-virtual {v3}, Lfnn;->b()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iput v3, v2, Liam;->c:F

    iget-object v2, v1, Lial;->c:Liam;

    iget-object v3, p0, Lftl;->c:Lfnn;

    invoke-virtual {v3}, Lfnn;->b()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iput v3, v2, Liam;->d:F

    const/4 v2, 0x1

    new-array v2, v2, [Lial;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lhvi;->f:[Lial;

    :cond_0
    return-object v0
.end method
