.class public final Lbnj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lfxe;

.field private final b:Ldsn;


# direct methods
.method public constructor <init>(Lfxe;Ldsn;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lbnj;->a:Lfxe;

    .line 28
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsn;

    iput-object v0, p0, Lbnj;->b:Ldsn;

    .line 29
    return-void
.end method


# virtual methods
.method public final handleSequencerStageEvent(Lczu;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 33
    iget-object v0, p1, Lczu;->a:Lgok;

    sget-object v1, Lgok;->d:Lgok;

    if-ne v0, v1, :cond_1

    .line 34
    iget-object v1, p1, Lczu;->b:Lfrl;

    .line 35
    iget-object v0, v1, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Error adding to watch history: empty video id."

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 36
    iget-object v0, v1, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbnj;->a:Lfxe;

    new-instance v2, Lbnk;

    invoke-direct {v2, p0}, Lbnk;-><init>(Lbnj;)V

    invoke-interface {v1, v0, v2}, Lfxe;->i(Ljava/lang/String;Leuc;)V

    .line 39
    :cond_1
    return-void

    .line 35
    :cond_2
    iget-object v0, p0, Lbnj;->b:Ldsn;

    iget-object v0, v0, Ldsn;->d:Ldwq;

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lfrl;->h()Lflp;

    move-result-object v0

    iget-boolean v0, v0, Lflp;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
