.class public final Lfnw;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;


# instance fields
.field public final a:Libd;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Lfnc;


# direct methods
.method public constructor <init>(Libd;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lfic;-><init>()V

    .line 24
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Libd;

    iput-object v0, p0, Lfnw;->a:Libd;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 62
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 63
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lfnw;->a:Libd;

    iget-object v0, v0, Libd;->f:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lfnw;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 29
    iget-object v0, p0, Lfnw;->a:Libd;

    iget-object v0, v0, Libd;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnw;->b:Ljava/lang/CharSequence;

    .line 31
    :cond_0
    iget-object v0, p0, Lfnw;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfnw;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lfnw;->a:Libd;

    iget-object v0, v0, Libd;->d:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnw;->c:Ljava/lang/CharSequence;

    .line 38
    :cond_0
    iget-object v0, p0, Lfnw;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lfnw;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lfnw;->a:Libd;

    iget-object v0, v0, Libd;->e:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnw;->d:Ljava/lang/CharSequence;

    .line 45
    :cond_0
    iget-object v0, p0, Lfnw;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final f()Lfnc;
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lfnw;->e:Lfnc;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfnw;->a:Libd;

    iget-object v1, v1, Libd;->b:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfnw;->e:Lfnc;

    .line 52
    :cond_0
    iget-object v0, p0, Lfnw;->e:Lfnc;

    return-object v0
.end method
