.class public Lfnr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Liav;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field private final d:Lfqh;

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;


# direct methods
.method public constructor <init>(Liav;Lfqh;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liav;

    iput-object v0, p0, Lfnr;->a:Liav;

    .line 35
    iput-object p2, p0, Lfnr;->d:Lfqh;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 50
    iget-object v0, p0, Lfnr;->e:Ljava/util/List;

    if-nez v0, :cond_2

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfnr;->a:Liav;

    iget-object v1, v1, Liav;->c:[Liau;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfnr;->e:Ljava/util/List;

    .line 52
    iget-object v0, p0, Lfnr;->a:Liav;

    iget-object v1, v0, Liav;->c:[Liau;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 53
    iget-object v4, v3, Liau;->b:Liay;

    if-eqz v4, :cond_1

    .line 54
    iget-object v4, p0, Lfnr;->e:Ljava/util/List;

    new-instance v5, Lfnt;

    iget-object v3, v3, Liau;->b:Liay;

    invoke-direct {v5, v3}, Lfnt;-><init>(Liay;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :cond_1
    iget-object v4, v3, Liau;->c:Liax;

    if-eqz v4, :cond_0

    .line 56
    iget-object v4, p0, Lfnr;->e:Ljava/util/List;

    new-instance v5, Lfns;

    iget-object v3, v3, Liau;->c:Liax;

    invoke-direct {v5, v3}, Lfns;-><init>(Liax;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 60
    :cond_2
    iget-object v0, p0, Lfnr;->e:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 3

    .prologue
    .line 98
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 99
    invoke-virtual {p0}, Lfnr;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 100
    instance-of v2, v0, Lfat;

    if-eqz v2, :cond_0

    .line 101
    check-cast v0, Lfat;

    invoke-interface {v0, p1}, Lfat;->a(Lfau;)V

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {p0}, Lfnr;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnu;

    .line 105
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    goto :goto_1

    .line 107
    :cond_2
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 6

    .prologue
    .line 72
    iget-object v0, p0, Lfnr;->f:Ljava/util/List;

    if-nez v0, :cond_1

    .line 73
    iget-object v0, p0, Lfnr;->a:Liav;

    iget-object v0, v0, Liav;->d:Liaz;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lfnr;->a:Liav;

    iget-object v0, v0, Liav;->d:Liaz;

    iget-object v1, v0, Liaz;->b:[Liba;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfnr;->f:Ljava/util/List;

    .line 76
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 77
    iget-object v4, p0, Lfnr;->f:Ljava/util/List;

    new-instance v5, Lfnu;

    invoke-direct {v5, v3}, Lfnu;-><init>(Liba;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfnr;->f:Ljava/util/List;

    .line 83
    :cond_1
    iget-object v0, p0, Lfnr;->f:Ljava/util/List;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfnr;->a:Liav;

    iget-object v0, v0, Liav;->e:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lfnr;->d:Lfqh;

    return-object v0
.end method
