.class public Lfio;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhas;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Z

.field public f:Ljava/lang/CharSequence;

.field public g:Ljava/lang/CharSequence;

.field private final h:Lfqh;

.field private i:Lfnq;

.field private j:Ljava/util/List;

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;


# direct methods
.method public constructor <init>(Lhas;Lfqh;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhas;

    iput-object v0, p0, Lfio;->a:Lhas;

    .line 44
    iput-object p2, p0, Lfio;->h:Lfqh;

    .line 45
    return-void
.end method


# virtual methods
.method public final a()Lfnq;
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lfio;->i:Lfnq;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lfnq;

    iget-object v1, p0, Lfio;->a:Lhas;

    iget-object v1, v1, Lhas;->b:Lias;

    iget-object v1, v1, Lias;->a:Liat;

    invoke-direct {v0, v1}, Lfnq;-><init>(Liat;)V

    iput-object v0, p0, Lfio;->i:Lfnq;

    .line 70
    :cond_0
    iget-object v0, p0, Lfio;->i:Lfnq;

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 2

    .prologue
    .line 159
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 160
    invoke-virtual {p0}, Lfio;->a()Lfnq;

    move-result-object v0

    .line 161
    if-eqz v0, :cond_0

    .line 162
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    .line 164
    :cond_0
    invoke-virtual {p0}, Lfio;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnp;

    .line 165
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    goto :goto_0

    .line 167
    :cond_1
    invoke-virtual {p0}, Lfio;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnw;

    .line 168
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    goto :goto_1

    .line 170
    :cond_2
    invoke-virtual {p0}, Lfio;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnu;

    .line 171
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    goto :goto_2

    .line 173
    :cond_3
    return-void
.end method

.method public final b()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfio;->j:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfio;->k:Ljava/util/List;

    .line 76
    iget-object v0, p0, Lfio;->a:Lhas;

    iget-object v3, v0, Lhas;->d:[Liaw;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 77
    iget-object v5, v0, Liaw;->c:Libe;

    if-eqz v5, :cond_0

    .line 78
    iget-object v0, v0, Liaw;->c:Libe;

    .line 79
    iget-object v5, v0, Libe;->a:Lhgz;

    invoke-static {v5}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v5

    iput-object v5, p0, Lfio;->d:Ljava/lang/CharSequence;

    .line 80
    iget-object v5, v0, Libe;->b:[Libd;

    .line 81
    array-length v6, v5

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    .line 82
    iget-object v8, p0, Lfio;->j:Ljava/util/List;

    new-instance v9, Lfnw;

    invoke-direct {v9, v7}, Lfnw;-><init>(Libd;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 84
    :cond_0
    iget-object v5, v0, Liaw;->b:Liar;

    if-eqz v5, :cond_1

    .line 85
    iget-object v0, v0, Liaw;->b:Liar;

    .line 86
    iget-object v5, v0, Liar;->a:Lhgz;

    invoke-static {v5}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v5

    iput-object v5, p0, Lfio;->f:Ljava/lang/CharSequence;

    .line 87
    iget-object v5, v0, Liar;->b:[Liaq;

    .line 88
    array-length v6, v5

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    .line 89
    iget-object v8, p0, Lfio;->k:Ljava/util/List;

    new-instance v9, Lfnp;

    invoke-direct {v9, v7}, Lfnp;-><init>(Liaq;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 76
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 93
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfio;->e:Z

    .line 94
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lfio;->e:Z

    if-nez v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lfio;->b()V

    .line 107
    :cond_0
    iget-object v0, p0, Lfio;->j:Ljava/util/List;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lfio;->a:Lhas;

    iget-object v0, v0, Lhas;->g:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lfio;->h:Lfqh;

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lfio;->e:Z

    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lfio;->b()V

    .line 121
    :cond_0
    iget-object v0, p0, Lfio;->k:Ljava/util/List;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 6

    .prologue
    .line 133
    iget-object v0, p0, Lfio;->l:Ljava/util/List;

    if-nez v0, :cond_1

    .line 134
    iget-object v0, p0, Lfio;->a:Lhas;

    iget-object v0, v0, Lhas;->c:Liaz;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lfio;->a:Lhas;

    iget-object v0, v0, Lhas;->c:Liaz;

    iget-object v1, v0, Liaz;->b:[Liba;

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfio;->l:Ljava/util/List;

    .line 137
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 138
    iget-object v4, p0, Lfio;->l:Ljava/util/List;

    new-instance v5, Lfnu;

    invoke-direct {v5, v3}, Lfnu;-><init>(Liba;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 141
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfio;->l:Ljava/util/List;

    .line 144
    :cond_1
    iget-object v0, p0, Lfio;->l:Ljava/util/List;

    return-object v0
.end method
