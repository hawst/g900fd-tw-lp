.class public final Lfhx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private final b:Lezj;

.field private final c:Lfdd;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lezj;Lfdd;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lfhx;->a:Landroid/content/SharedPreferences;

    .line 33
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lfhx;->b:Lezj;

    .line 34
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdd;

    iput-object v0, p0, Lfhx;->c:Lfdd;

    .line 35
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 38
    invoke-static {}, Lb;->b()V

    .line 39
    iget-object v0, p0, Lfhx;->c:Lfdd;

    new-instance v1, Lfdf;

    iget-object v2, v0, Lfdd;->b:Lfsz;

    iget-object v0, v0, Lfdd;->c:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lfdf;-><init>(Lfsz;Lgit;)V

    .line 40
    sget-object v0, Lfhy;->a:[B

    invoke-virtual {v1, v0}, Lfdf;->a([B)V

    .line 43
    iget-object v0, p0, Lfhx;->c:Lfdd;

    iget-object v0, v0, Lfdd;->e:Lfde;

    invoke-virtual {v0, v1}, Lfde;->c(Lfsp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfji;

    .line 45
    iget-object v0, v0, Lfji;->a:Lheg;

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lfhx;->a:Landroid/content/SharedPreferences;

    .line 47
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "com.google.android.libraries.youtube.innertube.pref.inner_tube_config"

    .line 48
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.google.android.libraries.youtube.innertube.pref.inner_tube_config_last_sync_timestamp"

    iget-object v2, p0, Lfhx;->b:Lezj;

    .line 51
    invoke-virtual {v2}, Lezj;->b()J

    move-result-wide v2

    .line 49
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 52
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 53
    const-string v0, "New config values downloaded"

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 54
    return-void
.end method
