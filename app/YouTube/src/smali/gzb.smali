.class public final Lgzb;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;

.field private b:Lhgz;

.field private c:[D

.field private d:Lhgz;

.field private e:Lhog;

.field private f:I

.field private g:[Lgzc;

.field private h:[Lhxi;

.field private i:Lhgz;

.field private j:Lhgz;

.field private k:Lhgz;

.field private l:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5721
    invoke-direct {p0}, Lidf;-><init>()V

    .line 5871
    iput-object v1, p0, Lgzb;->a:Lhgz;

    .line 5874
    iput-object v1, p0, Lgzb;->b:Lhgz;

    .line 5877
    sget-object v0, Lidj;->c:[D

    iput-object v0, p0, Lgzb;->c:[D

    .line 5880
    iput-object v1, p0, Lgzb;->d:Lhgz;

    .line 5883
    iput-object v1, p0, Lgzb;->e:Lhog;

    .line 5886
    const/4 v0, 0x0

    iput v0, p0, Lgzb;->f:I

    .line 5889
    sget-object v0, Lgzc;->a:[Lgzc;

    iput-object v0, p0, Lgzb;->g:[Lgzc;

    .line 5892
    sget-object v0, Lhxi;->a:[Lhxi;

    iput-object v0, p0, Lgzb;->h:[Lhxi;

    .line 5895
    iput-object v1, p0, Lgzb;->i:Lhgz;

    .line 5898
    iput-object v1, p0, Lgzb;->j:Lhgz;

    .line 5901
    iput-object v1, p0, Lgzb;->k:Lhgz;

    .line 5904
    iput-object v1, p0, Lgzb;->l:Lhgz;

    .line 5721
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 6032
    .line 6033
    iget-object v0, p0, Lgzb;->a:Lhgz;

    if-eqz v0, :cond_d

    .line 6034
    const/4 v0, 0x1

    iget-object v2, p0, Lgzb;->a:Lhgz;

    .line 6035
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6037
    :goto_0
    iget-object v2, p0, Lgzb;->b:Lhgz;

    if-eqz v2, :cond_0

    .line 6038
    const/4 v2, 0x2

    iget-object v3, p0, Lgzb;->b:Lhgz;

    .line 6039
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6041
    :cond_0
    iget-object v2, p0, Lgzb;->c:[D

    if-eqz v2, :cond_1

    iget-object v2, p0, Lgzb;->c:[D

    array-length v2, v2

    if-lez v2, :cond_1

    .line 6042
    iget-object v2, p0, Lgzb;->c:[D

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x8

    .line 6043
    add-int/2addr v0, v2

    .line 6044
    iget-object v2, p0, Lgzb;->c:[D

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 6046
    :cond_1
    iget-object v2, p0, Lgzb;->d:Lhgz;

    if-eqz v2, :cond_2

    .line 6047
    const/4 v2, 0x4

    iget-object v3, p0, Lgzb;->d:Lhgz;

    .line 6048
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6050
    :cond_2
    iget-object v2, p0, Lgzb;->e:Lhog;

    if-eqz v2, :cond_3

    .line 6051
    const/4 v2, 0x5

    iget-object v3, p0, Lgzb;->e:Lhog;

    .line 6052
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6054
    :cond_3
    iget v2, p0, Lgzb;->f:I

    if-eqz v2, :cond_4

    .line 6055
    const/4 v2, 0x6

    iget v3, p0, Lgzb;->f:I

    .line 6056
    invoke-static {v2, v3}, Lidd;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6058
    :cond_4
    iget-object v2, p0, Lgzb;->g:[Lgzc;

    if-eqz v2, :cond_6

    .line 6059
    iget-object v3, p0, Lgzb;->g:[Lgzc;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 6060
    if-eqz v5, :cond_5

    .line 6061
    const/4 v6, 0x7

    .line 6062
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 6059
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 6066
    :cond_6
    iget-object v2, p0, Lgzb;->h:[Lhxi;

    if-eqz v2, :cond_8

    .line 6067
    iget-object v2, p0, Lgzb;->h:[Lhxi;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 6068
    if-eqz v4, :cond_7

    .line 6069
    const/16 v5, 0x8

    .line 6070
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6067
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6074
    :cond_8
    iget-object v1, p0, Lgzb;->i:Lhgz;

    if-eqz v1, :cond_9

    .line 6075
    const/16 v1, 0x9

    iget-object v2, p0, Lgzb;->i:Lhgz;

    .line 6076
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6078
    :cond_9
    iget-object v1, p0, Lgzb;->j:Lhgz;

    if-eqz v1, :cond_a

    .line 6079
    const/16 v1, 0xa

    iget-object v2, p0, Lgzb;->j:Lhgz;

    .line 6080
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6082
    :cond_a
    iget-object v1, p0, Lgzb;->k:Lhgz;

    if-eqz v1, :cond_b

    .line 6083
    const/16 v1, 0xb

    iget-object v2, p0, Lgzb;->k:Lhgz;

    .line 6084
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6086
    :cond_b
    iget-object v1, p0, Lgzb;->l:Lhgz;

    if-eqz v1, :cond_c

    .line 6087
    const/16 v1, 0xc

    iget-object v2, p0, Lgzb;->l:Lhgz;

    .line 6088
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6090
    :cond_c
    iget-object v1, p0, Lgzb;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6091
    iput v0, p0, Lgzb;->J:I

    .line 6092
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5717
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lgzb;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lgzb;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lgzb;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzb;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzb;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lgzb;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgzb;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzb;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lgzb;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x19

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgzb;->c:[D

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [D

    iget-object v3, p0, Lgzb;->c:[D

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lgzb;->c:[D

    :goto_1
    iget-object v2, p0, Lgzb;->c:[D

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lgzb;->c:[D

    invoke-virtual {p1}, Lidc;->k()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lgzb;->c:[D

    invoke-virtual {p1}, Lidc;->k()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lgzb;->d:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzb;->d:Lhgz;

    :cond_5
    iget-object v0, p0, Lgzb;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lgzb;->e:Lhog;

    if-nez v0, :cond_6

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lgzb;->e:Lhog;

    :cond_6
    iget-object v0, p0, Lgzb;->e:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lgzb;->f:I

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgzb;->g:[Lgzc;

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lgzc;

    iget-object v3, p0, Lgzb;->g:[Lgzc;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lgzb;->g:[Lgzc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lgzb;->g:[Lgzc;

    :goto_3
    iget-object v2, p0, Lgzb;->g:[Lgzc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lgzb;->g:[Lgzc;

    new-instance v3, Lgzc;

    invoke-direct {v3}, Lgzc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzb;->g:[Lgzc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lgzb;->g:[Lgzc;

    array-length v0, v0

    goto :goto_2

    :cond_9
    iget-object v2, p0, Lgzb;->g:[Lgzc;

    new-instance v3, Lgzc;

    invoke-direct {v3}, Lgzc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzb;->g:[Lgzc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgzb;->h:[Lhxi;

    if-nez v0, :cond_b

    move v0, v1

    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lhxi;

    iget-object v3, p0, Lgzb;->h:[Lhxi;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lgzb;->h:[Lhxi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Lgzb;->h:[Lhxi;

    :goto_5
    iget-object v2, p0, Lgzb;->h:[Lhxi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Lgzb;->h:[Lhxi;

    new-instance v3, Lhxi;

    invoke-direct {v3}, Lhxi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzb;->h:[Lhxi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_b
    iget-object v0, p0, Lgzb;->h:[Lhxi;

    array-length v0, v0

    goto :goto_4

    :cond_c
    iget-object v2, p0, Lgzb;->h:[Lhxi;

    new-instance v3, Lhxi;

    invoke-direct {v3}, Lhxi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzb;->h:[Lhxi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lgzb;->i:Lhgz;

    if-nez v0, :cond_d

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzb;->i:Lhgz;

    :cond_d
    iget-object v0, p0, Lgzb;->i:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lgzb;->j:Lhgz;

    if-nez v0, :cond_e

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzb;->j:Lhgz;

    :cond_e
    iget-object v0, p0, Lgzb;->j:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lgzb;->k:Lhgz;

    if-nez v0, :cond_f

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzb;->k:Lhgz;

    :cond_f
    iget-object v0, p0, Lgzb;->k:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lgzb;->l:Lhgz;

    if-nez v0, :cond_10

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzb;->l:Lhgz;

    :cond_10
    iget-object v0, p0, Lgzb;->l:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 5980
    iget-object v1, p0, Lgzb;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 5981
    const/4 v1, 0x1

    iget-object v2, p0, Lgzb;->a:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 5983
    :cond_0
    iget-object v1, p0, Lgzb;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 5984
    const/4 v1, 0x2

    iget-object v2, p0, Lgzb;->b:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 5986
    :cond_1
    iget-object v1, p0, Lgzb;->c:[D

    if-eqz v1, :cond_2

    .line 5987
    iget-object v2, p0, Lgzb;->c:[D

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-wide v4, v2, v1

    .line 5988
    const/4 v6, 0x3

    invoke-virtual {p1, v6, v4, v5}, Lidd;->a(ID)V

    .line 5987
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5991
    :cond_2
    iget-object v1, p0, Lgzb;->d:Lhgz;

    if-eqz v1, :cond_3

    .line 5992
    const/4 v1, 0x4

    iget-object v2, p0, Lgzb;->d:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 5994
    :cond_3
    iget-object v1, p0, Lgzb;->e:Lhog;

    if-eqz v1, :cond_4

    .line 5995
    const/4 v1, 0x5

    iget-object v2, p0, Lgzb;->e:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 5997
    :cond_4
    iget v1, p0, Lgzb;->f:I

    if-eqz v1, :cond_5

    .line 5998
    const/4 v1, 0x6

    iget v2, p0, Lgzb;->f:I

    invoke-virtual {p1, v1, v2}, Lidd;->b(II)V

    .line 6000
    :cond_5
    iget-object v1, p0, Lgzb;->g:[Lgzc;

    if-eqz v1, :cond_7

    .line 6001
    iget-object v2, p0, Lgzb;->g:[Lgzc;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 6002
    if-eqz v4, :cond_6

    .line 6003
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 6001
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6007
    :cond_7
    iget-object v1, p0, Lgzb;->h:[Lhxi;

    if-eqz v1, :cond_9

    .line 6008
    iget-object v1, p0, Lgzb;->h:[Lhxi;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 6009
    if-eqz v3, :cond_8

    .line 6010
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 6008
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6014
    :cond_9
    iget-object v0, p0, Lgzb;->i:Lhgz;

    if-eqz v0, :cond_a

    .line 6015
    const/16 v0, 0x9

    iget-object v1, p0, Lgzb;->i:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 6017
    :cond_a
    iget-object v0, p0, Lgzb;->j:Lhgz;

    if-eqz v0, :cond_b

    .line 6018
    const/16 v0, 0xa

    iget-object v1, p0, Lgzb;->j:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 6020
    :cond_b
    iget-object v0, p0, Lgzb;->k:Lhgz;

    if-eqz v0, :cond_c

    .line 6021
    const/16 v0, 0xb

    iget-object v1, p0, Lgzb;->k:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 6023
    :cond_c
    iget-object v0, p0, Lgzb;->l:Lhgz;

    if-eqz v0, :cond_d

    .line 6024
    const/16 v0, 0xc

    iget-object v1, p0, Lgzb;->l:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 6026
    :cond_d
    iget-object v0, p0, Lgzb;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 6028
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5926
    if-ne p1, p0, :cond_1

    .line 5941
    :cond_0
    :goto_0
    return v0

    .line 5927
    :cond_1
    instance-of v2, p1, Lgzb;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 5928
    :cond_2
    check-cast p1, Lgzb;

    .line 5929
    iget-object v2, p0, Lgzb;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzb;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzb;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzb;->b:Lhgz;

    if-nez v2, :cond_3

    .line 5930
    :goto_2
    iget-object v2, p0, Lgzb;->c:[D

    iget-object v3, p1, Lgzb;->c:[D

    .line 5931
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([D[D)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgzb;->d:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzb;->d:Lhgz;

    if-nez v2, :cond_3

    .line 5932
    :goto_3
    iget-object v2, p0, Lgzb;->e:Lhog;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgzb;->e:Lhog;

    if-nez v2, :cond_3

    .line 5933
    :goto_4
    iget v2, p0, Lgzb;->f:I

    iget v3, p1, Lgzb;->f:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzb;->g:[Lgzc;

    iget-object v3, p1, Lgzb;->g:[Lgzc;

    .line 5935
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgzb;->h:[Lhxi;

    iget-object v3, p1, Lgzb;->h:[Lhxi;

    .line 5936
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgzb;->i:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lgzb;->i:Lhgz;

    if-nez v2, :cond_3

    .line 5937
    :goto_5
    iget-object v2, p0, Lgzb;->j:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lgzb;->j:Lhgz;

    if-nez v2, :cond_3

    .line 5938
    :goto_6
    iget-object v2, p0, Lgzb;->k:Lhgz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lgzb;->k:Lhgz;

    if-nez v2, :cond_3

    .line 5939
    :goto_7
    iget-object v2, p0, Lgzb;->l:Lhgz;

    if-nez v2, :cond_b

    iget-object v2, p1, Lgzb;->l:Lhgz;

    if-nez v2, :cond_3

    .line 5940
    :goto_8
    iget-object v2, p0, Lgzb;->I:Ljava/util/List;

    if-nez v2, :cond_c

    iget-object v2, p1, Lgzb;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 5941
    goto :goto_0

    .line 5929
    :cond_4
    iget-object v2, p0, Lgzb;->a:Lhgz;

    iget-object v3, p1, Lgzb;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzb;->b:Lhgz;

    iget-object v3, p1, Lgzb;->b:Lhgz;

    .line 5930
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 5931
    :cond_6
    iget-object v2, p0, Lgzb;->d:Lhgz;

    iget-object v3, p1, Lgzb;->d:Lhgz;

    .line 5932
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgzb;->e:Lhog;

    iget-object v3, p1, Lgzb;->e:Lhog;

    .line 5933
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    .line 5936
    :cond_8
    iget-object v2, p0, Lgzb;->i:Lhgz;

    iget-object v3, p1, Lgzb;->i:Lhgz;

    .line 5937
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lgzb;->j:Lhgz;

    iget-object v3, p1, Lgzb;->j:Lhgz;

    .line 5938
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lgzb;->k:Lhgz;

    iget-object v3, p1, Lgzb;->k:Lhgz;

    .line 5939
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lgzb;->l:Lhgz;

    iget-object v3, p1, Lgzb;->l:Lhgz;

    .line 5940
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Lgzb;->I:Ljava/util/List;

    iget-object v3, p1, Lgzb;->I:Ljava/util/List;

    .line 5941
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 5945
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5947
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzb;->a:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 5948
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzb;->b:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5949
    iget-object v2, p0, Lgzb;->c:[D

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    .line 5955
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgzb;->d:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 5956
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzb;->e:Lhog;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 5957
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgzb;->f:I

    add-int/2addr v0, v2

    .line 5958
    iget-object v2, p0, Lgzb;->g:[Lgzc;

    if-nez v2, :cond_8

    mul-int/lit8 v2, v0, 0x1f

    .line 5964
    :cond_1
    iget-object v0, p0, Lgzb;->h:[Lhxi;

    if-nez v0, :cond_a

    mul-int/lit8 v2, v2, 0x1f

    .line 5970
    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgzb;->i:Lhgz;

    if-nez v0, :cond_c

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 5971
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzb;->j:Lhgz;

    if-nez v0, :cond_d

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 5972
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzb;->k:Lhgz;

    if-nez v0, :cond_e

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 5973
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzb;->l:Lhgz;

    if-nez v0, :cond_f

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 5974
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzb;->I:Ljava/util/List;

    if-nez v2, :cond_10

    :goto_8
    add-int/2addr v0, v1

    .line 5975
    return v0

    .line 5947
    :cond_3
    iget-object v0, p0, Lgzb;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5948
    :cond_4
    iget-object v0, p0, Lgzb;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_5
    move v2, v0

    move v0, v1

    .line 5951
    :goto_9
    iget-object v3, p0, Lgzb;->c:[D

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 5952
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lgzb;->c:[D

    aget-wide v4, v3, v0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-object v3, p0, Lgzb;->c:[D

    aget-wide v6, v3, v0

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    const/16 v3, 0x20

    ushr-long/2addr v6, v3

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v2, v3

    .line 5951
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 5955
    :cond_6
    iget-object v0, p0, Lgzb;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 5956
    :cond_7
    iget-object v0, p0, Lgzb;->e:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_8
    move v2, v0

    move v0, v1

    .line 5960
    :goto_a
    iget-object v3, p0, Lgzb;->g:[Lgzc;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 5961
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgzb;->g:[Lgzc;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v1

    :goto_b
    add-int/2addr v2, v3

    .line 5960
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 5961
    :cond_9
    iget-object v2, p0, Lgzb;->g:[Lgzc;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgzc;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_a
    move v0, v1

    .line 5966
    :goto_c
    iget-object v3, p0, Lgzb;->h:[Lhxi;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 5967
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgzb;->h:[Lhxi;

    aget-object v2, v2, v0

    if-nez v2, :cond_b

    move v2, v1

    :goto_d
    add-int/2addr v2, v3

    .line 5966
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 5967
    :cond_b
    iget-object v2, p0, Lgzb;->h:[Lhxi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhxi;->hashCode()I

    move-result v2

    goto :goto_d

    .line 5970
    :cond_c
    iget-object v0, p0, Lgzb;->i:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 5971
    :cond_d
    iget-object v0, p0, Lgzb;->j:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 5972
    :cond_e
    iget-object v0, p0, Lgzb;->k:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 5973
    :cond_f
    iget-object v0, p0, Lgzb;->l:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 5974
    :cond_10
    iget-object v1, p0, Lgzb;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_8
.end method
