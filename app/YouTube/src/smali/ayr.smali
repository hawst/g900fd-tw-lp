.class public final Layr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/res/Resources;

.field public final c:Leyp;

.field public final d:Landroid/app/NotificationManager;

.field public e:Ljava/lang/String;

.field public volatile f:Ljava/lang/String;

.field public g:Lba;

.field public h:Lba;

.field public i:Z

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Layr;->a:Landroid/content/Context;

    .line 53
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Layr;->c:Leyp;

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Layr;->b:Landroid/content/res/Resources;

    .line 55
    const-string v0, "notification"

    .line 56
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Layr;->d:Landroid/app/NotificationManager;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Layr;->i:Z

    .line 58
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Layr;->d:Landroid/app/NotificationManager;

    const/16 v1, 0x3ed

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Layr;->i:Z

    .line 66
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Layr;->j:I

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Layr;->b:Landroid/content/res/Resources;

    const v1, 0x7f0a007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Layr;->j:I

    .line 210
    :cond_0
    iget v0, p0, Layr;->j:I

    return v0
.end method
