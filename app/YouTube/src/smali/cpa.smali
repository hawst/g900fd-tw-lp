.class public final Lcpa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlr;
.implements Lggl;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/content/Context;

.field public final c:Lezj;

.field final d:Lefj;

.field final e:Lefj;

.field public final f:Ldoo;

.field public g:J

.field public h:Z

.field private i:J

.field private volatile j:J

.field private volatile k:I


# direct methods
.method constructor <init>(Landroid/content/Context;Lezj;Lefj;Lefj;Ldoo;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcpa;->g:J

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcpa;->h:Z

    .line 99
    iput-object p1, p0, Lcpa;->b:Landroid/content/Context;

    .line 100
    iput-object p2, p0, Lcpa;->c:Lezj;

    .line 101
    iput-object p3, p0, Lcpa;->d:Lefj;

    .line 102
    iput-object p4, p0, Lcpa;->e:Lefj;

    .line 103
    iput-object p5, p0, Lcpa;->f:Ldoo;

    .line 105
    iput-object p6, p0, Lcpa;->a:Ljava/lang/String;

    .line 107
    invoke-virtual {p2}, Lezj;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x7530

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcpa;->i:J

    .line 108
    return-void
.end method

.method private a(Z)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v2, 0x0

    .line 144
    iget-object v0, p0, Lcpa;->c:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v8

    .line 145
    iget-wide v0, p0, Lcpa;->j:J

    cmp-long v0, v0, v10

    if-lez v0, :cond_2

    if-nez p1, :cond_0

    iget-wide v0, p0, Lcpa;->i:J

    cmp-long v0, v8, v0

    if-lez v0, :cond_2

    .line 148
    :cond_0
    iget v0, p0, Lcpa;->k:I

    const/16 v1, 0x7d0

    if-le v0, v1, :cond_1

    .line 149
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v0, "cpn"

    iget-object v1, p0, Lcpa;->a:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "bytes_transferred"

    iget-wide v4, p0, Lcpa;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "time_window_millis"

    iget v1, p0, Lcpa;->k:I

    int-to-long v4, v1

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcpa;->k:I

    if-nez v0, :cond_3

    iget-wide v0, p0, Lcpa;->j:J

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x42

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "bandwidthElapsed is zero.  bandwidthBytes is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 152
    :cond_1
    :goto_0
    const-wide/16 v0, 0x7530

    add-long/2addr v0, v8

    iput-wide v0, p0, Lcpa;->i:J

    .line 153
    iput-wide v10, p0, Lcpa;->j:J

    .line 154
    const/4 v0, 0x0

    iput v0, p0, Lcpa;->k:I

    .line 156
    :cond_2
    return-void

    .line 149
    :cond_3
    iget-wide v0, p0, Lcpa;->j:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    iget v3, p0, Lcpa;->k:I

    int-to-long v4, v3

    div-long v4, v0, v4

    iget-object v0, p0, Lcpa;->f:Ldoo;

    iget-object v1, p0, Lcpa;->b:Landroid/content/Context;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v6}, Ldoo;->a(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 132
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcpa;->a(Z)V

    .line 133
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcpa;->g:J

    .line 134
    return-void
.end method

.method public final declared-synchronized a(IJJ)V
    .locals 2

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcpa;->j:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcpa;->j:J

    .line 118
    iget v0, p0, Lcpa;->k:I

    add-int/2addr v0, p1

    iput v0, p0, Lcpa;->k:I

    .line 119
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcpa;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcpa;->d:Lefj;

    invoke-virtual {v0, p0}, Lefj;->b(Lggl;)V

    .line 139
    iget-object v0, p0, Lcpa;->e:Lefj;

    invoke-virtual {v0, p0}, Lefj;->b(Lggl;)V

    .line 140
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcpa;->a(Z)V

    .line 141
    return-void
.end method
