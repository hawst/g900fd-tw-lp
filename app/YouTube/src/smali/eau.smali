.class public Leau;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/os/Handler;

.field final b:Leay;

.field final c:Ljava/util/concurrent/CopyOnWriteArraySet;

.field final d:[Z

.field e:Z

.field f:I

.field g:I


# direct methods
.method public constructor <init>(III)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput v2, p0, Leau;->f:I

    .line 44
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Leau;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 45
    new-array v0, p1, [Z

    iput-object v0, p0, Leau;->d:[Z

    .line 46
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Leau;->d:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 47
    iget-object v1, p0, Leau;->d:[Z

    aput-boolean v2, v1, v0

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    new-instance v0, Leax;

    invoke-direct {v0, p0}, Leax;-><init>(Leau;)V

    iput-object v0, p0, Leau;->a:Landroid/os/Handler;

    .line 55
    new-instance v0, Leay;

    iget-object v1, p0, Leau;->a:Landroid/os/Handler;

    iget-boolean v2, p0, Leau;->e:Z

    iget-object v3, p0, Leau;->d:[Z

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Leay;-><init>(Landroid/os/Handler;Z[ZII)V

    iput-object v0, p0, Leau;->b:Leay;

    .line 57
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Leau;->b:Leay;

    iget-object v0, v0, Leay;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public a(IZ)V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Leau;->d:[Z

    aget-boolean v0, v0, p1

    if-eq v0, p2, :cond_0

    .line 87
    iget-object v0, p0, Leau;->d:[Z

    aput-boolean p2, v0, p1

    .line 88
    iget-object v0, p0, Leau;->b:Leay;

    iget-object v1, v0, Leay;->a:Landroid/os/Handler;

    const/16 v2, 0x8

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, p1, v0}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 90
    :cond_0
    return-void

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Leau;->b:Leay;

    iget-object v0, v0, Leay;->a:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 122
    return-void
.end method

.method a(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 170
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 192
    :cond_0
    return-void

    .line 172
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Leau;->f:I

    .line 173
    iget-object v0, p0, Leau;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leaw;

    .line 174
    iget-boolean v2, p0, Leau;->e:Z

    iget v2, p0, Leau;->f:I

    invoke-interface {v0, v2}, Leaw;->a(I)V

    goto :goto_0

    .line 179
    :pswitch_1
    iget v0, p0, Leau;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Leau;->g:I

    .line 180
    iget v0, p0, Leau;->g:I

    if-nez v0, :cond_0

    .line 181
    iget-object v0, p0, Leau;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leaw;

    .line 182
    invoke-interface {v0}, Leaw;->a()V

    goto :goto_1

    .line 188
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Leat;

    .line 189
    iget-object v1, p0, Leau;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leaw;

    .line 190
    invoke-interface {v1, v0}, Leaw;->a(Leat;)V

    goto :goto_2

    .line 170
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Leav;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Leau;->b:Leay;

    iget v1, v0, Leay;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Leay;->c:I

    iget-object v0, v0, Leay;->a:Landroid/os/Handler;

    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 138
    return-void
.end method

.method public a(Leaw;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Leau;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-boolean v0, p0, Leau;->e:Z

    if-eq v0, p1, :cond_1

    .line 100
    iput-boolean p1, p0, Leau;->e:Z

    .line 101
    iget v0, p0, Leau;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Leau;->g:I

    .line 102
    iget-object v0, p0, Leau;->b:Leay;

    iget-object v2, v0, Leay;->a:Landroid/os/Handler;

    const/4 v3, 0x3

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 103
    iget-object v0, p0, Leau;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leaw;

    .line 104
    iget v2, p0, Leau;->f:I

    invoke-interface {v0, v2}, Leaw;->a(I)V

    goto :goto_1

    :cond_0
    move v0, v1

    .line 102
    goto :goto_0

    .line 107
    :cond_1
    return-void
.end method

.method public varargs a([Lecc;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Leau;->b:Leay;

    iget-object v0, v0, Leay;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 82
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Leau;->f:I

    return v0
.end method

.method public b(Leav;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Leau;->b:Leay;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p3}, Leay;->a(Leav;ILjava/lang/Object;)V

    .line 143
    return-void
.end method

.method public b(Leaw;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Leau;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 72
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Leau;->e:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Leau;->g:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Leau;->b:Leay;

    iget-object v0, v0, Leay;->a:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 127
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Leau;->b:Leay;

    invoke-virtual {v0}, Leay;->a()V

    .line 132
    iget-object v0, p0, Leau;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 133
    return-void
.end method

.method public g()J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 147
    iget-object v2, p0, Leau;->b:Leay;

    iget-wide v4, v2, Leay;->d:J

    cmp-long v3, v4, v0

    if-nez v3, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, v2, Leay;->d:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public h()J
    .locals 4

    .prologue
    .line 152
    iget-object v0, p0, Leau;->b:Leay;

    iget-wide v0, v0, Leay;->e:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public i()J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 157
    iget-object v2, p0, Leau;->b:Leay;

    iget-wide v4, v2, Leay;->f:J

    cmp-long v3, v4, v0

    if-nez v3, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, v2, Leay;->f:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method
