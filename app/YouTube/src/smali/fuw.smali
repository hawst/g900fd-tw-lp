.class public Lfuw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lfus;

.field final b:Lfdg;

.field final c:Levn;

.field final d:Lfuu;

.field final e:Lfrz;

.field final f:Leyt;

.field final g:Lful;


# direct methods
.method public constructor <init>(Lfus;Lfdg;Levn;Lfuu;Lfrz;Leyt;Lful;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfus;

    iput-object v0, p0, Lfuw;->a:Lfus;

    .line 39
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdg;

    iput-object v0, p0, Lfuw;->b:Lfdg;

    .line 40
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lfuw;->c:Levn;

    .line 41
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfuu;

    iput-object v0, p0, Lfuw;->d:Lfuu;

    .line 43
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lfuw;->e:Lfrz;

    .line 44
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lfuw;->f:Leyt;

    .line 45
    iput-object p7, p0, Lfuw;->g:Lful;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lfuv;
    .locals 8

    .prologue
    .line 56
    instance-of v0, p1, Lfkj;

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Lfuy;

    iget-object v1, p0, Lfuw;->b:Lfdg;

    iget-object v2, p0, Lfuw;->d:Lfuu;

    iget-object v3, p0, Lfuw;->c:Levn;

    iget-object v4, p0, Lfuw;->f:Leyt;

    invoke-direct {v0, v1, v2, v3, v4}, Lfuy;-><init>(Lfdg;Lfuu;Levn;Leyt;)V

    .line 62
    check-cast p1, Lfkj;

    invoke-virtual {v0, p1}, Lfuy;->a(Lfkj;)V

    .line 101
    :goto_0
    return-object v0

    .line 65
    :cond_0
    instance-of v0, p1, Lfkl;

    if-eqz v0, :cond_1

    .line 66
    new-instance v0, Lfva;

    iget-object v1, p0, Lfuw;->b:Lfdg;

    iget-object v2, p0, Lfuw;->d:Lfuu;

    iget-object v3, p0, Lfuw;->a:Lfus;

    iget-object v4, p0, Lfuw;->c:Levn;

    iget-object v5, p0, Lfuw;->f:Leyt;

    iget-object v6, p0, Lfuw;->e:Lfrz;

    move-object v7, p1

    check-cast v7, Lfkl;

    invoke-direct/range {v0 .. v7}, Lfva;-><init>(Lfdg;Lfuu;Lfus;Levn;Leyt;Lfrz;Lfkl;)V

    goto :goto_0

    .line 75
    :cond_1
    instance-of v0, p1, Lfnh;

    if-eqz v0, :cond_2

    .line 76
    new-instance v0, Lfvk;

    iget-object v1, p0, Lfuw;->d:Lfuu;

    iget-object v2, p0, Lfuw;->c:Levn;

    check-cast p1, Lfnh;

    invoke-direct {v0, v1, v2, p1}, Lfvk;-><init>(Lfuu;Levn;Lfnh;)V

    goto :goto_0

    .line 81
    :cond_2
    instance-of v0, p1, Lfkd;

    if-eqz v0, :cond_3

    .line 82
    new-instance v0, Lfuj;

    iget-object v1, p0, Lfuw;->d:Lfuu;

    iget-object v2, p0, Lfuw;->c:Levn;

    check-cast p1, Lfkd;

    iget-object v3, p0, Lfuw;->g:Lful;

    invoke-direct {v0, v1, v2, p1, v3}, Lfuj;-><init>(Lfuu;Levn;Lfkd;Lful;)V

    goto :goto_0

    .line 88
    :cond_3
    instance-of v0, p1, Lfmc;

    if-eqz v0, :cond_4

    .line 89
    new-instance v0, Lfvf;

    iget-object v1, p0, Lfuw;->b:Lfdg;

    iget-object v2, p0, Lfuw;->d:Lfuu;

    iget-object v3, p0, Lfuw;->c:Levn;

    iget-object v4, p0, Lfuw;->f:Leyt;

    invoke-direct {v0, v1, v2, v3, v4}, Lfvf;-><init>(Lfdg;Lfuu;Levn;Leyt;)V

    .line 94
    check-cast p1, Lfmc;

    invoke-virtual {v0, p1}, Lfvf;->a(Lfmc;)V

    goto :goto_0

    .line 97
    :cond_4
    instance-of v0, p1, Lfso;

    if-eqz v0, :cond_5

    .line 98
    new-instance v0, Lfvm;

    iget-object v1, p0, Lfuw;->d:Lfuu;

    check-cast p1, Lfso;

    invoke-direct {v0, v1, p1}, Lfvm;-><init>(Lfuu;Lfso;)V

    goto :goto_0

    .line 101
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
