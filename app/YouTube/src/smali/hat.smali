.class public final Lhat;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhrb;

.field private b:Lhra;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12566
    invoke-direct {p0}, Lidf;-><init>()V

    .line 12569
    iput-object v0, p0, Lhat;->a:Lhrb;

    .line 12572
    iput-object v0, p0, Lhat;->b:Lhra;

    .line 12566
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 12615
    const/4 v0, 0x0

    .line 12616
    iget-object v1, p0, Lhat;->a:Lhrb;

    if-eqz v1, :cond_0

    .line 12617
    const v0, 0x392f096

    iget-object v1, p0, Lhat;->a:Lhrb;

    .line 12618
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12620
    :cond_0
    iget-object v1, p0, Lhat;->b:Lhra;

    if-eqz v1, :cond_1

    .line 12621
    const v1, 0x3c6724e

    iget-object v2, p0, Lhat;->b:Lhra;

    .line 12622
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12624
    :cond_1
    iget-object v1, p0, Lhat;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12625
    iput v0, p0, Lhat;->J:I

    .line 12626
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 12562
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhat;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhat;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhat;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhat;->a:Lhrb;

    if-nez v0, :cond_2

    new-instance v0, Lhrb;

    invoke-direct {v0}, Lhrb;-><init>()V

    iput-object v0, p0, Lhat;->a:Lhrb;

    :cond_2
    iget-object v0, p0, Lhat;->a:Lhrb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhat;->b:Lhra;

    if-nez v0, :cond_3

    new-instance v0, Lhra;

    invoke-direct {v0}, Lhra;-><init>()V

    iput-object v0, p0, Lhat;->b:Lhra;

    :cond_3
    iget-object v0, p0, Lhat;->b:Lhra;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1c9784b2 -> :sswitch_1
        0x1e339272 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 12603
    iget-object v0, p0, Lhat;->a:Lhrb;

    if-eqz v0, :cond_0

    .line 12604
    const v0, 0x392f096

    iget-object v1, p0, Lhat;->a:Lhrb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 12606
    :cond_0
    iget-object v0, p0, Lhat;->b:Lhra;

    if-eqz v0, :cond_1

    .line 12607
    const v0, 0x3c6724e

    iget-object v1, p0, Lhat;->b:Lhra;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 12609
    :cond_1
    iget-object v0, p0, Lhat;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 12611
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12584
    if-ne p1, p0, :cond_1

    .line 12589
    :cond_0
    :goto_0
    return v0

    .line 12585
    :cond_1
    instance-of v2, p1, Lhat;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 12586
    :cond_2
    check-cast p1, Lhat;

    .line 12587
    iget-object v2, p0, Lhat;->a:Lhrb;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhat;->a:Lhrb;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhat;->b:Lhra;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhat;->b:Lhra;

    if-nez v2, :cond_3

    .line 12588
    :goto_2
    iget-object v2, p0, Lhat;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhat;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 12589
    goto :goto_0

    .line 12587
    :cond_4
    iget-object v2, p0, Lhat;->a:Lhrb;

    iget-object v3, p1, Lhat;->a:Lhrb;

    invoke-virtual {v2, v3}, Lhrb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhat;->b:Lhra;

    iget-object v3, p1, Lhat;->b:Lhra;

    .line 12588
    invoke-virtual {v2, v3}, Lhra;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhat;->I:Ljava/util/List;

    iget-object v3, p1, Lhat;->I:Ljava/util/List;

    .line 12589
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 12593
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 12595
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhat;->a:Lhrb;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 12596
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhat;->b:Lhra;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 12597
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhat;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 12598
    return v0

    .line 12595
    :cond_0
    iget-object v0, p0, Lhat;->a:Lhrb;

    invoke-virtual {v0}, Lhrb;->hashCode()I

    move-result v0

    goto :goto_0

    .line 12596
    :cond_1
    iget-object v0, p0, Lhat;->b:Lhra;

    invoke-virtual {v0}, Lhra;->hashCode()I

    move-result v0

    goto :goto_1

    .line 12597
    :cond_2
    iget-object v1, p0, Lhat;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
