.class public final Lctw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgkl;


# instance fields
.field private final a:Lgix;

.field private final b:Landroid/content/Context;

.field private final c:Lghp;


# direct methods
.method public constructor <init>(Lgix;Landroid/content/Context;Lghp;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lctw;->a:Lgix;

    .line 38
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lctw;->b:Landroid/content/Context;

    .line 39
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghp;

    iput-object v0, p0, Lctw;->c:Lghp;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/util/Map;Lgkt;)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lctw;->b:Landroid/content/Context;

    invoke-static {v0}, La;->n(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lctw;->c:Lghp;

    invoke-interface {v0}, Lghp;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 49
    :cond_0
    invoke-interface {p2}, Lgkt;->z_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->L(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    .line 52
    :cond_1
    invoke-interface {p2}, Lgkp;->k()Lgiv;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lctw;->a:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    iget-object v0, v0, Lgit;->b:Lgiv;

    .line 53
    :cond_2
    invoke-virtual {v0}, Lgiv;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 54
    const-string v1, "X-Goog-PageId"

    invoke-virtual {v0}, Lgiv;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    :cond_3
    return-void
.end method
