.class public final Lgzm;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhul;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8758
    invoke-direct {p0}, Lidf;-><init>()V

    .line 8761
    const/4 v0, 0x0

    iput-object v0, p0, Lgzm;->a:Lhul;

    .line 8758
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 8798
    const/4 v0, 0x0

    .line 8799
    iget-object v1, p0, Lgzm;->a:Lhul;

    if-eqz v1, :cond_0

    .line 8800
    const v0, 0x2f1c7f5

    iget-object v1, p0, Lgzm;->a:Lhul;

    .line 8801
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8803
    :cond_0
    iget-object v1, p0, Lgzm;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8804
    iput v0, p0, Lgzm;->J:I

    .line 8805
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 8754
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzm;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzm;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzm;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzm;->a:Lhul;

    if-nez v0, :cond_2

    new-instance v0, Lhul;

    invoke-direct {v0}, Lhul;-><init>()V

    iput-object v0, p0, Lgzm;->a:Lhul;

    :cond_2
    iget-object v0, p0, Lgzm;->a:Lhul;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x178e3faa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 8789
    iget-object v0, p0, Lgzm;->a:Lhul;

    if-eqz v0, :cond_0

    .line 8790
    const v0, 0x2f1c7f5

    iget-object v1, p0, Lgzm;->a:Lhul;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8792
    :cond_0
    iget-object v0, p0, Lgzm;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 8794
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8772
    if-ne p1, p0, :cond_1

    .line 8776
    :cond_0
    :goto_0
    return v0

    .line 8773
    :cond_1
    instance-of v2, p1, Lgzm;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 8774
    :cond_2
    check-cast p1, Lgzm;

    .line 8775
    iget-object v2, p0, Lgzm;->a:Lhul;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzm;->a:Lhul;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzm;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzm;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 8776
    goto :goto_0

    .line 8775
    :cond_4
    iget-object v2, p0, Lgzm;->a:Lhul;

    iget-object v3, p1, Lgzm;->a:Lhul;

    invoke-virtual {v2, v3}, Lhul;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzm;->I:Ljava/util/List;

    iget-object v3, p1, Lgzm;->I:Ljava/util/List;

    .line 8776
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8780
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 8782
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzm;->a:Lhul;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 8783
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzm;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 8784
    return v0

    .line 8782
    :cond_0
    iget-object v0, p0, Lgzm;->a:Lhul;

    invoke-virtual {v0}, Lhul;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8783
    :cond_1
    iget-object v1, p0, Lgzm;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
