.class final Lbvo;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private synthetic a:Landroid/app/Activity;

.field private synthetic b:Lbvn;


# direct methods
.method constructor <init>(Lbvn;Landroid/os/Looper;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lbvo;->b:Lbvn;

    iput-object p3, p0, Lbvo;->a:Landroid/app/Activity;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 179
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 181
    :pswitch_0
    iget-object v0, p0, Lbvo;->b:Lbvn;

    invoke-static {v0}, Lbvn;->a(Lbvn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lbvo;->b:Lbvn;

    invoke-static {v0}, Lbvn;->b(Lbvn;)V

    .line 185
    iget-object v0, p0, Lbvo;->b:Lbvn;

    invoke-static {v0}, Lbvn;->c(Lbvn;)Landroid/os/Handler;

    move-result-object v0

    .line 186
    invoke-static {p0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    .line 185
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 189
    :pswitch_1
    iget-object v0, p0, Lbvo;->b:Lbvn;

    invoke-static {v0}, Lbvn;->d(Lbvn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lbvo;->b:Lbvn;

    invoke-static {v0}, Lbvn;->e(Lbvn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lbvo;->b:Lbvn;

    invoke-static {v0}, Lbvn;->f(Lbvn;)Ldwo;

    move-result-object v0

    sget-object v1, Ldwo;->i:Ldwo;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lbvo;->b:Lbvn;

    .line 198
    invoke-static {v0}, Lbvn;->f(Lbvn;)Ldwo;

    move-result-object v0

    sget-object v1, Ldwo;->a:Ldwo;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lbvo;->b:Lbvn;

    .line 199
    invoke-static {v0}, Lbvn;->f(Lbvn;)Ldwo;

    move-result-object v0

    sget-object v1, Ldwo;->e:Ldwo;

    if-ne v0, v1, :cond_0

    .line 200
    :cond_1
    iget-object v0, p0, Lbvo;->a:Landroid/app/Activity;

    const v1, 0x7f090301

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lbvo;->b:Lbvn;

    .line 201
    invoke-static {v4}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v4

    invoke-interface {v4}, Ldwq;->n()Ldwr;

    move-result-object v4

    invoke-virtual {v4}, Ldwr;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 200
    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 203
    iget-object v0, p0, Lbvo;->b:Lbvn;

    invoke-static {v0}, Lbvn;->h(Lbvn;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0900a2

    .line 204
    :goto_1
    iget-object v2, p0, Lbvo;->b:Lbvn;

    invoke-static {v2}, Lbvn;->i(Lbvn;)Lbvx;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lbvx;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 203
    :cond_2
    const v0, 0x7f09030d

    goto :goto_1

    .line 179
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
