.class public abstract Ldjp;
.super Ldjr;
.source "SourceFile"


# instance fields
.field public final a:Ldaq;

.field private l:Leue;


# direct methods
.method public constructor <init>(Lcvq;Levn;Lcwq;Leyt;Lfac;Ldaq;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct/range {p0 .. p5}, Ldjr;-><init>(Lcvq;Levn;Lcwq;Leyt;Lfac;)V

    .line 38
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaq;

    iput-object v0, p0, Ldjp;->a:Ldaq;

    .line 39
    return-void
.end method

.method public constructor <init>(Lcvq;Levn;Lcwq;Leyt;Lfac;Ldaq;Ldkz;)V
    .locals 7

    .prologue
    .line 50
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Ldjr;-><init>(Lcvq;Levn;Lcwq;Leyt;Lfac;Ldkz;)V

    .line 57
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaq;

    iput-object v0, p0, Ldjp;->a:Ldaq;

    .line 58
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Ldjp;->l:Leue;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Ldjp;->l:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Ldjp;->l:Leue;

    .line 97
    :cond_0
    return-void
.end method

.method final a(I)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-virtual {p0}, Ldjp;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldjp;->f:Lgok;

    sget-object v2, Lgok;->e:Lgok;

    invoke-virtual {v1, v2}, Lgok;->a(Lgok;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    invoke-virtual {p0}, Ldjp;->q()V

    .line 89
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v1, p0, Ldjp;->g:Lfrl;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v1, p0, Ldjp;->g:Lfrl;

    invoke-virtual {v1}, Lfrl;->g()Lflo;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 73
    iget-object v1, p0, Ldjp;->g:Lfrl;

    invoke-virtual {v1}, Lfrl;->g()Lflo;

    move-result-object v1

    invoke-virtual {v1}, Lflo;->e()Lfij;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_1
    :goto_1
    move-object v3, v0

    .line 76
    :goto_2
    if-nez v3, :cond_2

    .line 77
    invoke-virtual {p0}, Ldjp;->e()Ljava/lang/String;

    move-result-object v3

    .line 80
    :cond_2
    new-instance v0, Ldjq;

    invoke-direct {v0, p0}, Ldjq;-><init>(Ldjp;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldjp;->l:Leue;

    .line 81
    iget-object v0, p0, Ldjp;->a:Ldaq;

    .line 82
    invoke-virtual {p0}, Ldjp;->b()Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-virtual {p0}, Ldjp;->d()[B

    move-result-object v2

    .line 85
    invoke-virtual {p0}, Ldjp;->s()Ljava/lang/String;

    move-result-object v4

    .line 86
    invoke-virtual {p0}, Ldjp;->c()I

    move-result v5

    iget-object v7, p0, Ldjp;->l:Leue;

    move v6, p1

    .line 81
    invoke-virtual/range {v0 .. v7}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    goto :goto_0

    .line 73
    :cond_3
    iget-object v1, v1, Lfij;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v3, v0

    goto :goto_2
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected abstract c()I
.end method

.method protected abstract d()[B
.end method

.method protected abstract e()Ljava/lang/String;
.end method
