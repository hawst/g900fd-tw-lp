.class public final Lcyb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcyd;


# static fields
.field private static final a:Landroid/util/SparseArray;


# instance fields
.field private final b:Lcyc;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 18
    sput-object v0, Lcyb;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    sget-object v2, Lcye;->b:Lcye;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 21
    sget-object v0, Lcyb;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    sget-object v2, Lcye;->c:Lcye;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 24
    sget-object v0, Lcyb;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    sget-object v2, Lcye;->d:Lcye;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Lcyc;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcyb;->b:Lcyc;

    .line 33
    return-void
.end method


# virtual methods
.method public final a()Lcye;
    .locals 2

    .prologue
    .line 37
    sget-object v0, Lcyb;->a:Landroid/util/SparseArray;

    iget-object v1, p0, Lcyb;->b:Lcyc;

    invoke-interface {v1}, Lcyc;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_0

    .line 38
    sget-object v0, Lcye;->a:Lcye;

    .line 40
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcyb;->a:Landroid/util/SparseArray;

    iget-object v1, p0, Lcyb;->b:Lcyc;

    invoke-interface {v1}, Lcyc;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcye;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcyb;->b:Lcyc;

    invoke-interface {v0}, Lcyc;->l()Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcyb;->b:Lcyc;

    invoke-interface {v0}, Lcyc;->m()I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcyb;->b:Lcyc;

    invoke-interface {v0}, Lcyc;->n()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcyb;->b:Lcyc;

    invoke-interface {v0}, Lcyc;->o()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcyb;->b:Lcyc;

    invoke-interface {v0}, Lcyc;->p()Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcyb;->b:Lcyc;

    invoke-interface {v0}, Lcyc;->q()Z

    move-result v0

    return v0
.end method
