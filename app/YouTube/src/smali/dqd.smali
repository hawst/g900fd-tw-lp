.class final Ldqd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/nio/ByteBuffer;

.field private b:Ljava/lang/Exception;

.field private c:Ldqe;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    sget-object v0, Ldqe;->a:Ldqe;

    iput-object v0, p0, Ldqd;->c:Ldqe;

    .line 197
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    sget-object v0, Ldqe;->c:Ldqe;

    iput-object v0, p0, Ldqd;->c:Ldqe;

    .line 208
    iput-object p1, p0, Ldqd;->b:Ljava/lang/Exception;

    .line 209
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    sget-object v0, Ldqe;->b:Ldqe;

    iput-object v0, p0, Ldqd;->c:Ldqe;

    .line 202
    iput-object p1, p0, Ldqd;->a:Ljava/nio/ByteBuffer;

    .line 203
    return-void
.end method


# virtual methods
.method public final a()Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 217
    sget-object v0, Ldqe;->b:Ldqe;

    iget-object v1, p0, Ldqd;->c:Ldqe;

    invoke-virtual {v0, v1}, Ldqe;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "data accessed when state was not DATA"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_0
    iget-object v0, p0, Ldqd;->a:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public final b()Ljava/lang/Exception;
    .locals 2

    .prologue
    .line 229
    sget-object v0, Ldqe;->c:Ldqe;

    iget-object v1, p0, Ldqd;->c:Ldqe;

    invoke-virtual {v0, v1}, Ldqe;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "data accessed when state was not ERROR"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    iget-object v0, p0, Ldqd;->b:Ljava/lang/Exception;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 241
    sget-object v0, Ldqe;->a:Ldqe;

    iget-object v1, p0, Ldqd;->c:Ldqe;

    invoke-virtual {v0, v1}, Ldqe;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 248
    sget-object v0, Ldqe;->c:Ldqe;

    iget-object v1, p0, Ldqd;->c:Ldqe;

    invoke-virtual {v0, v1}, Ldqe;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
