.class public final Lfmn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lfqh;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lhvm;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lfmo;

    invoke-direct {v0}, Lfmo;-><init>()V

    sput-object v0, Lfmn;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lhvm;

    invoke-direct {v0}, Lhvm;-><init>()V

    invoke-static {p1, v0}, La;->b(Landroid/os/Parcel;Lidh;)Lidh;

    move-result-object v0

    check-cast v0, Lhvm;

    invoke-direct {p0, v0}, Lfmn;-><init>(Lhvm;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Lhvm;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvm;

    iput-object v0, p0, Lfmn;->a:Lhvm;

    .line 56
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 67
    iget-object v0, p0, Lfmn;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfmn;->d:Ljava/util/List;

    .line 69
    iget-object v0, p0, Lfmn;->a:Lhvm;

    iget-object v0, v0, Lhvm;->b:[Lhvh;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lfmn;->a:Lhvm;

    iget-object v1, v0, Lhvm;->b:[Lhvh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 71
    iget-object v4, p0, Lfmn;->d:Ljava/util/List;

    new-instance v5, Lfmp;

    iget-object v3, v3, Lhvh;->b:Lhvg;

    invoke-direct {v5, v3}, Lfmp;-><init>(Lhvg;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    iget-object v0, p0, Lfmn;->d:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 6

    .prologue
    .line 79
    iget-object v0, p0, Lfmn;->e:Ljava/util/List;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfmn;->e:Ljava/util/List;

    .line 81
    iget-object v0, p0, Lfmn;->a:Lhvm;

    iget-object v0, v0, Lhvm;->c:[Lhvh;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lfmn;->a:Lhvm;

    iget-object v1, v0, Lhvm;->c:[Lhvh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 83
    iget-object v4, p0, Lfmn;->e:Ljava/util/List;

    new-instance v5, Lfmp;

    iget-object v3, v3, Lhvh;->b:Lhvg;

    invoke-direct {v5, v3}, Lfmp;-><init>(Lhvg;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_0
    iget-object v0, p0, Lfmn;->e:Ljava/util/List;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lfmn;->a:Lhvm;

    iget-object v0, v0, Lhvm;->g:[B

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lfmn;->a:Lhvm;

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lidh;)V

    .line 116
    return-void
.end method
