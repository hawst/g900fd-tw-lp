.class public final Lhfi;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhjx;

.field public b:Lhyk;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45371
    invoke-direct {p0}, Lidf;-><init>()V

    .line 45374
    iput-object v0, p0, Lhfi;->a:Lhjx;

    .line 45377
    iput-object v0, p0, Lhfi;->b:Lhyk;

    .line 45380
    const-string v0, ""

    iput-object v0, p0, Lhfi;->c:Ljava/lang/String;

    .line 45371
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 45429
    const/4 v0, 0x0

    .line 45430
    iget-object v1, p0, Lhfi;->a:Lhjx;

    if-eqz v1, :cond_0

    .line 45431
    const/4 v0, 0x1

    iget-object v1, p0, Lhfi;->a:Lhjx;

    .line 45432
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 45434
    :cond_0
    iget-object v1, p0, Lhfi;->b:Lhyk;

    if-eqz v1, :cond_1

    .line 45435
    const/4 v1, 0x2

    iget-object v2, p0, Lhfi;->b:Lhyk;

    .line 45436
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45438
    :cond_1
    iget-object v1, p0, Lhfi;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 45439
    const/4 v1, 0x3

    iget-object v2, p0, Lhfi;->c:Ljava/lang/String;

    .line 45440
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45442
    :cond_2
    iget-object v1, p0, Lhfi;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45443
    iput v0, p0, Lhfi;->J:I

    .line 45444
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 45367
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhfi;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhfi;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhfi;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhfi;->a:Lhjx;

    if-nez v0, :cond_2

    new-instance v0, Lhjx;

    invoke-direct {v0}, Lhjx;-><init>()V

    iput-object v0, p0, Lhfi;->a:Lhjx;

    :cond_2
    iget-object v0, p0, Lhfi;->a:Lhjx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhfi;->b:Lhyk;

    if-nez v0, :cond_3

    new-instance v0, Lhyk;

    invoke-direct {v0}, Lhyk;-><init>()V

    iput-object v0, p0, Lhfi;->b:Lhyk;

    :cond_3
    iget-object v0, p0, Lhfi;->b:Lhyk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfi;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 45414
    iget-object v0, p0, Lhfi;->a:Lhjx;

    if-eqz v0, :cond_0

    .line 45415
    const/4 v0, 0x1

    iget-object v1, p0, Lhfi;->a:Lhjx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 45417
    :cond_0
    iget-object v0, p0, Lhfi;->b:Lhyk;

    if-eqz v0, :cond_1

    .line 45418
    const/4 v0, 0x2

    iget-object v1, p0, Lhfi;->b:Lhyk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 45420
    :cond_1
    iget-object v0, p0, Lhfi;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 45421
    const/4 v0, 0x3

    iget-object v1, p0, Lhfi;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 45423
    :cond_2
    iget-object v0, p0, Lhfi;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 45425
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 45393
    if-ne p1, p0, :cond_1

    .line 45399
    :cond_0
    :goto_0
    return v0

    .line 45394
    :cond_1
    instance-of v2, p1, Lhfi;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 45395
    :cond_2
    check-cast p1, Lhfi;

    .line 45396
    iget-object v2, p0, Lhfi;->a:Lhjx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfi;->a:Lhjx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhfi;->b:Lhyk;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhfi;->b:Lhyk;

    if-nez v2, :cond_3

    .line 45397
    :goto_2
    iget-object v2, p0, Lhfi;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhfi;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 45398
    :goto_3
    iget-object v2, p0, Lhfi;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhfi;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 45399
    goto :goto_0

    .line 45396
    :cond_4
    iget-object v2, p0, Lhfi;->a:Lhjx;

    iget-object v3, p1, Lhfi;->a:Lhjx;

    invoke-virtual {v2, v3}, Lhjx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhfi;->b:Lhyk;

    iget-object v3, p1, Lhfi;->b:Lhyk;

    .line 45397
    invoke-virtual {v2, v3}, Lhyk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhfi;->c:Ljava/lang/String;

    iget-object v3, p1, Lhfi;->c:Ljava/lang/String;

    .line 45398
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhfi;->I:Ljava/util/List;

    iget-object v3, p1, Lhfi;->I:Ljava/util/List;

    .line 45399
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 45403
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 45405
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfi;->a:Lhjx;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 45406
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfi;->b:Lhyk;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 45407
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfi;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 45408
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhfi;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 45409
    return v0

    .line 45405
    :cond_0
    iget-object v0, p0, Lhfi;->a:Lhjx;

    invoke-virtual {v0}, Lhjx;->hashCode()I

    move-result v0

    goto :goto_0

    .line 45406
    :cond_1
    iget-object v0, p0, Lhfi;->b:Lhyk;

    invoke-virtual {v0}, Lhyk;->hashCode()I

    move-result v0

    goto :goto_1

    .line 45407
    :cond_2
    iget-object v0, p0, Lhfi;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 45408
    :cond_3
    iget-object v1, p0, Lhfi;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
