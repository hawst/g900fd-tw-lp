.class final Ldqh;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private synthetic a:Ldpt;

.field private synthetic b:Ldqg;


# direct methods
.method constructor <init>(Ldqg;Ljava/lang/String;Ldpt;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Ldqh;->b:Ldqg;

    iput-object p3, p0, Ldqh;->a:Ldpt;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 187
    :try_start_0
    iget-object v0, p0, Ldqh;->b:Ldqg;

    invoke-static {v0}, Ldqg;->a(Ldqg;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 188
    iget-object v0, p0, Ldqh;->b:Ldqg;

    invoke-static {v0}, Ldqg;->b(Ldqg;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Ldqh;->b:Ldqg;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ldqg;->a(Ldqg;Z)Z

    .line 190
    iget-object v0, p0, Ldqh;->b:Ldqg;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ldqg;->b(Ldqg;Z)V

    .line 193
    :cond_0
    iget-object v0, p0, Ldqh;->b:Ldqg;

    new-instance v1, Ldqi;

    const-string v2, "Testing for buffered proxy"

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Ldqi;-><init>(Ldqg;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v1}, Ldqi;->start()V

    .line 194
    iget-object v0, p0, Ldqh;->b:Ldqg;

    iget-object v1, p0, Ldqh;->b:Ldqg;

    invoke-static {v1}, Ldqg;->c(Ldqg;)Ldpo;

    move-result-object v1

    iget-object v2, p0, Ldqh;->a:Ldpt;

    invoke-interface {v1, v2}, Ldpo;->a(Ldpt;)Ldqq;

    move-result-object v1

    invoke-static {v0, v1}, Ldqg;->a(Ldqg;Ldqq;)Ldqq;

    .line 195
    iget-object v0, p0, Ldqh;->b:Ldqg;

    invoke-static {v0}, Ldqg;->e(Ldqg;)Ldqq;

    move-result-object v0

    iget-object v1, p0, Ldqh;->b:Ldqg;

    invoke-static {v1}, Ldqg;->d(Ldqg;)Ldqr;

    move-result-object v1

    invoke-interface {v0, v1}, Ldqq;->a(Ldqr;)V

    .line 196
    iget-object v0, p0, Ldqh;->b:Ldqg;

    invoke-static {v0}, Ldqg;->f(Ldqg;)V

    .line 197
    iget-object v0, p0, Ldqh;->b:Ldqg;

    iget-boolean v0, v0, Ldqg;->b:Z

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Ldqh;->b:Ldqg;

    invoke-static {v0}, Ldqg;->g(Ldqg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :cond_1
    iget-object v0, p0, Ldqh;->b:Ldqg;

    invoke-static {v0, v4}, Ldqg;->c(Ldqg;Z)V

    .line 203
    iget-object v0, p0, Ldqh;->b:Ldqg;

    invoke-static {v0}, Ldqg;->h(Ldqg;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 204
    return-void

    .line 202
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ldqh;->b:Ldqg;

    invoke-static {v1, v4}, Ldqg;->c(Ldqg;Z)V

    .line 203
    iget-object v1, p0, Ldqh;->b:Ldqg;

    invoke-static {v1}, Ldqg;->h(Ldqg;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
