.class public final Laeh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field final b:Landroid/widget/TextView;

.field final c:Landroid/widget/TextView;

.field final d:Landroid/widget/ProgressBar;

.field e:Laej;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const v0, 0x7f0800e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laeh;->b:Landroid/widget/TextView;

    .line 42
    const v0, 0x7f0800e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laeh;->c:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f0800de

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Laeh;->d:Landroid/widget/ProgressBar;

    .line 45
    iget-object v0, p0, Laeh;->d:Landroid/widget/ProgressBar;

    new-instance v1, Laei;

    invoke-direct {v1, p0}, Laei;-><init>(Laeh;)V

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 46
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 85
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 86
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 2

    .prologue
    .line 49
    iget v0, p0, Laeh;->f:I

    if-eq v0, p1, :cond_0

    .line 50
    iput p1, p0, Laeh;->f:I

    .line 51
    iget-object v0, p0, Laeh;->c:Landroid/widget/TextView;

    invoke-static {p1}, La;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laeh;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 53
    :cond_0
    iget v0, p0, Laeh;->a:I

    if-eq v0, p2, :cond_1

    .line 54
    iput p2, p0, Laeh;->a:I

    .line 55
    iget-object v0, p0, Laeh;->b:Landroid/widget/TextView;

    invoke-static {p2}, La;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laeh;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 58
    :cond_1
    iput p3, p0, Laeh;->g:I

    .line 59
    iget-object v0, p0, Laeh;->d:Landroid/widget/ProgressBar;

    iget v1, p0, Laeh;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Laeh;->d:Landroid/widget/ProgressBar;

    iget v1, p0, Laeh;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Laeh;->d:Landroid/widget/ProgressBar;

    iget v1, p0, Laeh;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 60
    return-void
.end method
