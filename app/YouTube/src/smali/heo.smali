.class public final Lheo;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhog;

.field private b:Lhut;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40610
    invoke-direct {p0}, Lidf;-><init>()V

    .line 40613
    iput-object v0, p0, Lheo;->a:Lhog;

    .line 40616
    iput-object v0, p0, Lheo;->b:Lhut;

    .line 40610
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 40659
    const/4 v0, 0x0

    .line 40660
    iget-object v1, p0, Lheo;->a:Lhog;

    if-eqz v1, :cond_0

    .line 40661
    const/4 v0, 0x1

    iget-object v1, p0, Lheo;->a:Lhog;

    .line 40662
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 40664
    :cond_0
    iget-object v1, p0, Lheo;->b:Lhut;

    if-eqz v1, :cond_1

    .line 40665
    const/4 v1, 0x2

    iget-object v2, p0, Lheo;->b:Lhut;

    .line 40666
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40668
    :cond_1
    iget-object v1, p0, Lheo;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40669
    iput v0, p0, Lheo;->J:I

    .line 40670
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 40606
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lheo;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lheo;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lheo;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lheo;->a:Lhog;

    if-nez v0, :cond_2

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lheo;->a:Lhog;

    :cond_2
    iget-object v0, p0, Lheo;->a:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lheo;->b:Lhut;

    if-nez v0, :cond_3

    new-instance v0, Lhut;

    invoke-direct {v0}, Lhut;-><init>()V

    iput-object v0, p0, Lheo;->b:Lhut;

    :cond_3
    iget-object v0, p0, Lheo;->b:Lhut;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 40647
    iget-object v0, p0, Lheo;->a:Lhog;

    if-eqz v0, :cond_0

    .line 40648
    const/4 v0, 0x1

    iget-object v1, p0, Lheo;->a:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 40650
    :cond_0
    iget-object v0, p0, Lheo;->b:Lhut;

    if-eqz v0, :cond_1

    .line 40651
    const/4 v0, 0x2

    iget-object v1, p0, Lheo;->b:Lhut;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 40653
    :cond_1
    iget-object v0, p0, Lheo;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 40655
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40628
    if-ne p1, p0, :cond_1

    .line 40633
    :cond_0
    :goto_0
    return v0

    .line 40629
    :cond_1
    instance-of v2, p1, Lheo;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 40630
    :cond_2
    check-cast p1, Lheo;

    .line 40631
    iget-object v2, p0, Lheo;->a:Lhog;

    if-nez v2, :cond_4

    iget-object v2, p1, Lheo;->a:Lhog;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lheo;->b:Lhut;

    if-nez v2, :cond_5

    iget-object v2, p1, Lheo;->b:Lhut;

    if-nez v2, :cond_3

    .line 40632
    :goto_2
    iget-object v2, p0, Lheo;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lheo;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 40633
    goto :goto_0

    .line 40631
    :cond_4
    iget-object v2, p0, Lheo;->a:Lhog;

    iget-object v3, p1, Lheo;->a:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lheo;->b:Lhut;

    iget-object v3, p1, Lheo;->b:Lhut;

    .line 40632
    invoke-virtual {v2, v3}, Lhut;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lheo;->I:Ljava/util/List;

    iget-object v3, p1, Lheo;->I:Ljava/util/List;

    .line 40633
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 40637
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 40639
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheo;->a:Lhog;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 40640
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lheo;->b:Lhut;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 40641
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lheo;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 40642
    return v0

    .line 40639
    :cond_0
    iget-object v0, p0, Lheo;->a:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_0

    .line 40640
    :cond_1
    iget-object v0, p0, Lheo;->b:Lhut;

    invoke-virtual {v0}, Lhut;->hashCode()I

    move-result v0

    goto :goto_1

    .line 40641
    :cond_2
    iget-object v1, p0, Lheo;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
