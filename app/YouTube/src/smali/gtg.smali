.class public final Lgtg;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private a:F

.field private synthetic b:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V
    .locals 1

    .prologue
    .line 712
    iput-object p1, p0, Lgtg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 715
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lgtg;->a:F

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 744
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgtg;->removeMessages(I)V

    .line 745
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lgtg;->a:F

    .line 746
    return-void
.end method

.method public final a(JF)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 733
    iget v0, p0, Lgtg;->a:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    .line 734
    iget v0, p0, Lgtg;->a:F

    sub-float v0, p3, v0

    .line 735
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v3, p0, Lgtg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-static {v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->f(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 736
    :goto_0
    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    .line 737
    :cond_0
    invoke-virtual {p0, v1}, Lgtg;->removeMessages(I)V

    .line 738
    invoke-virtual {p0, v1, p1, p2}, Lgtg;->sendEmptyMessageDelayed(IJ)Z

    .line 739
    iput p3, p0, Lgtg;->a:F

    .line 741
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 735
    goto :goto_0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 719
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 730
    :cond_0
    :goto_0
    return-void

    .line 721
    :pswitch_0
    iget-object v0, p0, Lgtg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgtg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->b(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 724
    iget-object v0, p0, Lgtg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->c(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 725
    iget-object v0, p0, Lgtg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->d(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V

    .line 727
    :cond_1
    iget-object v0, p0, Lgtg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->e(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;)V

    goto :goto_0

    .line 719
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
