.class public final Lchz;
.super Lcbg;
.source "SourceFile"


# instance fields
.field final f:Lcid;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILeyp;Lcbk;Lcid;Lcbl;Lckf;)V
    .locals 7

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    invoke-static {p1, p2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    move-object v6, p7

    .line 45
    invoke-direct/range {v0 .. v6}, Lcbg;-><init>(Landroid/view/View;Landroid/app/Activity;Leyp;Lcbk;Lcbl;Lckf;)V

    .line 52
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcid;

    iput-object v0, p0, Lchz;->f:Lcid;

    .line 53
    iget-object v0, p0, Lchz;->a:Landroid/view/View;

    const v1, 0x7f0801cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchz;->g:Landroid/widget/TextView;

    .line 54
    iget-object v0, p0, Lchz;->a:Landroid/view/View;

    const v1, 0x7f0801ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lchz;->k:Landroid/view/View;

    .line 55
    iget-object v0, p0, Lchz;->a:Landroid/view/View;

    const v1, 0x7f0801c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchz;->h:Landroid/widget/TextView;

    .line 56
    iget-object v0, p0, Lchz;->a:Landroid/view/View;

    const v1, 0x7f0801cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchz;->i:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lchz;->a:Landroid/view/View;

    const v1, 0x7f0801cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchz;->j:Landroid/widget/TextView;

    .line 58
    return-void
.end method

.method private a(Lfsg;Lfwf;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 62
    invoke-super {p0, p1, p2}, Lcbg;->a(Lfsg;Lfwe;)Landroid/view/View;

    .line 63
    iget-boolean v2, p2, Lfwf;->q:Z

    if-eqz v2, :cond_0

    .line 64
    iget-object v0, p0, Lchz;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lchz;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 80
    :goto_0
    iget-object v2, p0, Lchz;->h:Landroid/widget/TextView;

    .line 81
    sget-object v0, Lcib;->a:[I

    iget v3, p2, Lfwf;->j:I

    add-int/lit8 v3, v3, -0x1

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 80
    :goto_1
    invoke-virtual {v2, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 82
    iget-boolean v0, p2, Lfwf;->p:Z

    if-eqz v0, :cond_4

    .line 83
    iget-object v0, p0, Lchz;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lchz;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    :goto_2
    iget-object v0, p0, Lchz;->a:Landroid/view/View;

    return-object v0

    .line 66
    :cond_0
    iget-object v2, p2, Lfwf;->m:Ljava/lang/String;

    if-eqz v2, :cond_2

    :cond_1
    :goto_3
    if-eqz v0, :cond_3

    .line 67
    iget-object v0, p0, Lchz;->k:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lchz;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lchz;->g:Landroid/widget/TextView;

    const v2, 0x7f0902b9

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 70
    iget-object v0, p0, Lchz;->g:Landroid/widget/TextView;

    new-instance v2, Lcia;

    invoke-direct {v2, p0, p2}, Lcia;-><init>(Lchz;Lfwf;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 66
    :cond_2
    iget v2, p2, Lfwf;->l:I

    iget-object v3, p2, Lfwf;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-gt v2, v3, :cond_1

    move v0, v1

    goto :goto_3

    .line 77
    :cond_3
    iget-object v0, p0, Lchz;->k:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lchz;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 81
    :pswitch_0
    const v0, 0x7f020134

    goto :goto_1

    :pswitch_1
    const v0, 0x7f020107

    goto :goto_1

    :pswitch_2
    const v0, 0x7f02011e

    goto :goto_1

    :pswitch_3
    const v0, 0x7f020100

    goto :goto_1

    .line 86
    :cond_4
    iget-object v0, p0, Lchz;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lchz;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfwe;)Landroid/view/View;
    .locals 1

    .prologue
    .line 20
    check-cast p2, Lfwf;

    invoke-direct {p0, p1, p2}, Lchz;->a(Lfsg;Lfwf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 20
    check-cast p2, Lfwf;

    invoke-direct {p0, p1, p2}, Lchz;->a(Lfsg;Lfwf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
