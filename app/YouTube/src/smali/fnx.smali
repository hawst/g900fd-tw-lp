.class public final Lfnx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lfat;
.implements Lfqh;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Libi;

.field public final b:Lfmn;

.field public final c:Lfmi;

.field public final d:Lfnk;

.field public final e:Lflw;

.field public final f:Lflc;

.field public final g:Lfjm;

.field public final h:Lhog;

.field public final i:Ljava/lang/String;

.field public j:Lfjh;

.field public k:Lfoe;

.field public l:Lfim;

.field public m:Lfln;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 243
    new-instance v0, Lfny;

    invoke-direct {v0}, Lfny;-><init>()V

    sput-object v0, Lfnx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Libi;

    invoke-direct {v0}, Libi;-><init>()V

    invoke-static {p1, v0}, La;->b(Landroid/os/Parcel;Lidh;)Lidh;

    move-result-object v0

    check-cast v0, Libi;

    invoke-direct {p0, v0}, Lfnx;-><init>(Libi;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Libi;)V
    .locals 17

    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static/range {p1 .. p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Libi;

    move-object/from16 v0, p0

    iput-object v1, v0, Lfnx;->a:Libi;

    .line 54
    const/4 v1, 0x0

    .line 55
    move-object/from16 v0, p1

    iget-object v2, v0, Libi;->c:Lhog;

    move-object/from16 v0, p0

    iput-object v2, v0, Lfnx;->h:Lhog;

    .line 56
    move-object/from16 v0, p0

    iget-object v2, v0, Lfnx;->h:Lhog;

    if-eqz v2, :cond_6

    .line 57
    move-object/from16 v0, p0

    iget-object v2, v0, Lfnx;->h:Lhog;

    iget-object v2, v2, Lhog;->a:[B

    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lfnx;->h:Lhog;

    iget-object v2, v2, Lhog;->i:Libf;

    if-eqz v2, :cond_5

    .line 59
    move-object/from16 v0, p0

    iget-object v1, v0, Lfnx;->h:Lhog;

    iget-object v1, v1, Lhog;->i:Libf;

    iget-object v1, v1, Libf;->a:Ljava/lang/String;

    .line 66
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iput-object v1, v0, Lfnx;->i:Ljava/lang/String;

    .line 70
    move-object/from16 v0, p1

    iget-object v1, v0, Libi;->a:Libj;

    if-eqz v1, :cond_7

    .line 71
    move-object/from16 v0, p1

    iget-object v1, v0, Libi;->a:Libj;

    iget-object v1, v1, Libj;->a:Lhwb;

    move-object v5, v1

    .line 76
    :goto_1
    const/4 v1, 0x0

    .line 77
    const/4 v2, 0x0

    .line 78
    const/4 v3, 0x0

    .line 79
    const/4 v4, 0x0

    .line 80
    if-eqz v5, :cond_f

    .line 81
    iget-object v6, v5, Lhwb;->a:Lhwf;

    if-eqz v6, :cond_1

    .line 82
    iget-object v1, v5, Lhwb;->a:Lhwf;

    iget-object v1, v1, Lhwf;->a:Lhul;

    .line 85
    :cond_1
    iget-object v6, v5, Lhwb;->b:Lhwd;

    if-eqz v6, :cond_2

    iget-object v6, v5, Lhwb;->b:Lhwd;

    iget-object v6, v6, Lhwd;->a:Lhsd;

    if-eqz v6, :cond_2

    .line 87
    iget-object v2, v5, Lhwb;->b:Lhwd;

    iget-object v6, v2, Lhwd;->a:Lhsd;

    .line 88
    new-instance v2, Lflw;

    invoke-direct {v2, v6}, Lflw;-><init>(Lhsd;)V

    .line 91
    :cond_2
    iget-object v6, v5, Lhwb;->c:Lhwc;

    if-eqz v6, :cond_3

    iget-object v6, v5, Lhwb;->c:Lhwc;

    iget-object v6, v6, Lhwc;->a:Lhay;

    if-eqz v6, :cond_3

    .line 93
    new-instance v3, Lflc;

    iget-object v6, v5, Lhwb;->c:Lhwc;

    iget-object v6, v6, Lhwc;->a:Lhay;

    invoke-direct {v3, v6}, Lflc;-><init>(Lhay;)V

    .line 96
    :cond_3
    iget-object v6, v5, Lhwb;->d:Lhen;

    if-eqz v6, :cond_e

    iget-object v6, v5, Lhwb;->d:Lhen;

    iget-object v6, v6, Lhen;->a:Lhem;

    if-eqz v6, :cond_e

    .line 98
    new-instance v4, Lfjm;

    iget-object v5, v5, Lhwb;->d:Lhen;

    iget-object v5, v5, Lhen;->a:Lhem;

    invoke-direct {v4, v5}, Lfjm;-><init>(Lhem;)V

    move-object v6, v4

    move-object v7, v3

    move-object v8, v2

    .line 105
    :goto_2
    const/4 v3, 0x0

    .line 106
    const/4 v2, 0x0

    .line 107
    const/4 v4, 0x0

    .line 108
    if-eqz v1, :cond_b

    .line 109
    new-instance v5, Lfmi;

    invoke-direct {v5, v1}, Lfmi;-><init>(Lhul;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lfnx;->c:Lfmi;

    .line 111
    iget-object v9, v1, Lhul;->a:[Lhun;

    array-length v10, v9

    const/4 v1, 0x0

    move/from16 v16, v1

    move-object v1, v4

    move/from16 v4, v16

    :goto_3
    if-ge v4, v10, :cond_4

    aget-object v5, v9, v4

    .line 112
    iget-object v11, v5, Lhun;->b:Lhky;

    .line 113
    if-eqz v11, :cond_a

    .line 114
    iget-object v12, v11, Lhky;->a:[Lhla;

    array-length v13, v12

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v13, :cond_a

    aget-object v14, v12, v5

    .line 115
    if-nez v3, :cond_8

    iget-object v15, v14, Lhla;->h:Lhzs;

    if-eqz v15, :cond_8

    .line 117
    iget-object v3, v14, Lhla;->h:Lhzs;

    .line 121
    new-instance v1, Lfkj;

    move-object/from16 v0, p0

    iget-object v14, v0, Lfnx;->c:Lfmi;

    invoke-direct {v1, v11, v14}, Lfkj;-><init>(Lhky;Lfqh;)V

    .line 122
    if-eqz v2, :cond_9

    .line 139
    :cond_4
    :goto_5
    if-eqz v3, :cond_c

    .line 140
    new-instance v4, Lfnk;

    invoke-direct {v4, v3, v1}, Lfnk;-><init>(Lhzs;Lfqh;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lfnx;->d:Lfnk;

    .line 144
    :goto_6
    if-eqz v2, :cond_d

    .line 145
    new-instance v1, Lfmn;

    invoke-direct {v1, v2}, Lfmn;-><init>(Lhvm;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lfnx;->b:Lfmn;

    .line 149
    :goto_7
    move-object/from16 v0, p0

    iput-object v8, v0, Lfnx;->e:Lflw;

    .line 150
    move-object/from16 v0, p0

    iput-object v7, v0, Lfnx;->f:Lflc;

    .line 151
    move-object/from16 v0, p0

    iput-object v6, v0, Lfnx;->g:Lfjm;

    .line 152
    return-void

    .line 60
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lfnx;->h:Lhog;

    iget-object v2, v2, Lhog;->w:Lhpm;

    if-eqz v2, :cond_0

    .line 61
    move-object/from16 v0, p0

    iget-object v1, v0, Lfnx;->h:Lhog;

    iget-object v1, v1, Lhog;->w:Lhpm;

    iget-object v1, v1, Lhpm;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 64
    :cond_6
    sget-object v2, Lfhy;->a:[B

    goto/16 :goto_0

    .line 73
    :cond_7
    const/4 v1, 0x0

    move-object v5, v1

    goto/16 :goto_1

    .line 125
    :cond_8
    if-nez v2, :cond_9

    iget-object v15, v14, Lhla;->z:Lhvm;

    if-eqz v15, :cond_9

    .line 126
    iget-object v2, v14, Lhla;->z:Lhvm;

    .line 127
    if-nez v3, :cond_4

    .line 128
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 111
    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 135
    :cond_b
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lfnx;->c:Lfmi;

    .line 136
    const/4 v1, 0x0

    goto :goto_5

    .line 142
    :cond_c
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lfnx;->d:Lfnk;

    goto :goto_6

    .line 147
    :cond_d
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lfnx;->b:Lfmn;

    goto :goto_7

    :cond_e
    move-object v6, v4

    move-object v7, v3

    move-object v8, v2

    goto/16 :goto_2

    :cond_f
    move-object v6, v4

    move-object v7, v3

    move-object v8, v2

    goto/16 :goto_2
.end method


# virtual methods
.method public final a()Lgyq;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lfnx;->a:Libi;

    iget-object v0, v0, Libi;->e:Lhrh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnx;->a:Libi;

    iget-object v0, v0, Libi;->e:Lhrh;

    iget-object v0, v0, Lhrh;->a:Lgyq;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lfau;)V
    .locals 1

    .prologue
    .line 289
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 290
    iget-object v0, p0, Lfnx;->f:Lflc;

    .line 291
    if-eqz v0, :cond_0

    .line 292
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    .line 294
    :cond_0
    iget-object v0, p0, Lfnx;->c:Lfmi;

    .line 295
    if-eqz v0, :cond_1

    .line 296
    invoke-virtual {v0, p1}, Lfmi;->a(Lfau;)V

    .line 298
    :cond_1
    iget-object v0, p0, Lfnx;->e:Lflw;

    .line 299
    if-eqz v0, :cond_2

    .line 300
    invoke-virtual {v0, p1}, Lflw;->a(Lfau;)V

    .line 302
    :cond_2
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lfnx;->a:Libi;

    iget-object v0, v0, Libi;->d:[B

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lfnx;->a:Libi;

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lidh;)V

    .line 270
    return-void
.end method
