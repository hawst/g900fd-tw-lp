.class public final Lgxt;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[B

.field public b:Ljava/lang/String;

.field public c:Lhgz;

.field public d:Lhxf;

.field public e:Lhxf;

.field public f:Ljava/lang/String;

.field private g:Lgxs;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lhut;

.field private l:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 488
    invoke-direct {p0}, Lidf;-><init>()V

    .line 491
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lgxt;->a:[B

    .line 494
    const-string v0, ""

    iput-object v0, p0, Lgxt;->b:Ljava/lang/String;

    .line 497
    iput-object v1, p0, Lgxt;->c:Lhgz;

    .line 500
    iput-object v1, p0, Lgxt;->g:Lgxs;

    .line 503
    iput-object v1, p0, Lgxt;->d:Lhxf;

    .line 506
    iput-boolean v2, p0, Lgxt;->h:Z

    .line 509
    iput-boolean v2, p0, Lgxt;->i:Z

    .line 512
    iput-object v1, p0, Lgxt;->e:Lhxf;

    .line 515
    iput-boolean v2, p0, Lgxt;->j:Z

    .line 518
    const-string v0, ""

    iput-object v0, p0, Lgxt;->f:Ljava/lang/String;

    .line 521
    iput-object v1, p0, Lgxt;->k:Lhut;

    .line 524
    iput-object v1, p0, Lgxt;->l:Lhgz;

    .line 488
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 632
    const/4 v0, 0x0

    .line 633
    iget-object v1, p0, Lgxt;->a:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    .line 634
    const/4 v0, 0x2

    iget-object v1, p0, Lgxt;->a:[B

    .line 635
    invoke-static {v0, v1}, Lidd;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 637
    :cond_0
    iget-object v1, p0, Lgxt;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 638
    const/4 v1, 0x3

    iget-object v2, p0, Lgxt;->b:Ljava/lang/String;

    .line 639
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 641
    :cond_1
    iget-object v1, p0, Lgxt;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 642
    const/4 v1, 0x4

    iget-object v2, p0, Lgxt;->c:Lhgz;

    .line 643
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 645
    :cond_2
    iget-object v1, p0, Lgxt;->g:Lgxs;

    if-eqz v1, :cond_3

    .line 646
    const/4 v1, 0x5

    iget-object v2, p0, Lgxt;->g:Lgxs;

    .line 647
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 649
    :cond_3
    iget-object v1, p0, Lgxt;->d:Lhxf;

    if-eqz v1, :cond_4

    .line 650
    const/4 v1, 0x6

    iget-object v2, p0, Lgxt;->d:Lhxf;

    .line 651
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 653
    :cond_4
    iget-boolean v1, p0, Lgxt;->h:Z

    if-eqz v1, :cond_5

    .line 654
    const/4 v1, 0x7

    iget-boolean v2, p0, Lgxt;->h:Z

    .line 655
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 657
    :cond_5
    iget-boolean v1, p0, Lgxt;->i:Z

    if-eqz v1, :cond_6

    .line 658
    const/16 v1, 0x8

    iget-boolean v2, p0, Lgxt;->i:Z

    .line 659
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 661
    :cond_6
    iget-object v1, p0, Lgxt;->e:Lhxf;

    if-eqz v1, :cond_7

    .line 662
    const/16 v1, 0x9

    iget-object v2, p0, Lgxt;->e:Lhxf;

    .line 663
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 665
    :cond_7
    iget-boolean v1, p0, Lgxt;->j:Z

    if-eqz v1, :cond_8

    .line 666
    const/16 v1, 0xa

    iget-boolean v2, p0, Lgxt;->j:Z

    .line 667
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 669
    :cond_8
    iget-object v1, p0, Lgxt;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 670
    const/16 v1, 0xb

    iget-object v2, p0, Lgxt;->f:Ljava/lang/String;

    .line 671
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 673
    :cond_9
    iget-object v1, p0, Lgxt;->k:Lhut;

    if-eqz v1, :cond_a

    .line 674
    const/16 v1, 0xc

    iget-object v2, p0, Lgxt;->k:Lhut;

    .line 675
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 677
    :cond_a
    iget-object v1, p0, Lgxt;->l:Lhgz;

    if-eqz v1, :cond_b

    .line 678
    const/16 v1, 0xd

    iget-object v2, p0, Lgxt;->l:Lhgz;

    .line 679
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 681
    :cond_b
    iget-object v1, p0, Lgxt;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 682
    iput v0, p0, Lgxt;->J:I

    .line 683
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 484
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgxt;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgxt;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgxt;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lgxt;->a:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgxt;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgxt;->c:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgxt;->c:Lhgz;

    :cond_2
    iget-object v0, p0, Lgxt;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lgxt;->g:Lgxs;

    if-nez v0, :cond_3

    new-instance v0, Lgxs;

    invoke-direct {v0}, Lgxs;-><init>()V

    iput-object v0, p0, Lgxt;->g:Lgxs;

    :cond_3
    iget-object v0, p0, Lgxt;->g:Lgxs;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lgxt;->d:Lhxf;

    if-nez v0, :cond_4

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lgxt;->d:Lhxf;

    :cond_4
    iget-object v0, p0, Lgxt;->d:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgxt;->h:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgxt;->i:Z

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lgxt;->e:Lhxf;

    if-nez v0, :cond_5

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lgxt;->e:Lhxf;

    :cond_5
    iget-object v0, p0, Lgxt;->e:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgxt;->j:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgxt;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lgxt;->k:Lhut;

    if-nez v0, :cond_6

    new-instance v0, Lhut;

    invoke-direct {v0}, Lhut;-><init>()V

    iput-object v0, p0, Lgxt;->k:Lhut;

    :cond_6
    iget-object v0, p0, Lgxt;->k:Lhut;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lgxt;->l:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgxt;->l:Lhgz;

    :cond_7
    iget-object v0, p0, Lgxt;->l:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 590
    iget-object v0, p0, Lgxt;->a:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 591
    const/4 v0, 0x2

    iget-object v1, p0, Lgxt;->a:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 593
    :cond_0
    iget-object v0, p0, Lgxt;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 594
    const/4 v0, 0x3

    iget-object v1, p0, Lgxt;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 596
    :cond_1
    iget-object v0, p0, Lgxt;->c:Lhgz;

    if-eqz v0, :cond_2

    .line 597
    const/4 v0, 0x4

    iget-object v1, p0, Lgxt;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 599
    :cond_2
    iget-object v0, p0, Lgxt;->g:Lgxs;

    if-eqz v0, :cond_3

    .line 600
    const/4 v0, 0x5

    iget-object v1, p0, Lgxt;->g:Lgxs;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 602
    :cond_3
    iget-object v0, p0, Lgxt;->d:Lhxf;

    if-eqz v0, :cond_4

    .line 603
    const/4 v0, 0x6

    iget-object v1, p0, Lgxt;->d:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 605
    :cond_4
    iget-boolean v0, p0, Lgxt;->h:Z

    if-eqz v0, :cond_5

    .line 606
    const/4 v0, 0x7

    iget-boolean v1, p0, Lgxt;->h:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 608
    :cond_5
    iget-boolean v0, p0, Lgxt;->i:Z

    if-eqz v0, :cond_6

    .line 609
    const/16 v0, 0x8

    iget-boolean v1, p0, Lgxt;->i:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 611
    :cond_6
    iget-object v0, p0, Lgxt;->e:Lhxf;

    if-eqz v0, :cond_7

    .line 612
    const/16 v0, 0x9

    iget-object v1, p0, Lgxt;->e:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 614
    :cond_7
    iget-boolean v0, p0, Lgxt;->j:Z

    if-eqz v0, :cond_8

    .line 615
    const/16 v0, 0xa

    iget-boolean v1, p0, Lgxt;->j:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 617
    :cond_8
    iget-object v0, p0, Lgxt;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 618
    const/16 v0, 0xb

    iget-object v1, p0, Lgxt;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 620
    :cond_9
    iget-object v0, p0, Lgxt;->k:Lhut;

    if-eqz v0, :cond_a

    .line 621
    const/16 v0, 0xc

    iget-object v1, p0, Lgxt;->k:Lhut;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 623
    :cond_a
    iget-object v0, p0, Lgxt;->l:Lhgz;

    if-eqz v0, :cond_b

    .line 624
    const/16 v0, 0xd

    iget-object v1, p0, Lgxt;->l:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 626
    :cond_b
    iget-object v0, p0, Lgxt;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 628
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 546
    if-ne p1, p0, :cond_1

    .line 561
    :cond_0
    :goto_0
    return v0

    .line 547
    :cond_1
    instance-of v2, p1, Lgxt;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 548
    :cond_2
    check-cast p1, Lgxt;

    .line 549
    iget-object v2, p0, Lgxt;->a:[B

    iget-object v3, p1, Lgxt;->a:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgxt;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgxt;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 550
    :goto_1
    iget-object v2, p0, Lgxt;->c:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgxt;->c:Lhgz;

    if-nez v2, :cond_3

    .line 551
    :goto_2
    iget-object v2, p0, Lgxt;->g:Lgxs;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgxt;->g:Lgxs;

    if-nez v2, :cond_3

    .line 552
    :goto_3
    iget-object v2, p0, Lgxt;->d:Lhxf;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgxt;->d:Lhxf;

    if-nez v2, :cond_3

    .line 553
    :goto_4
    iget-boolean v2, p0, Lgxt;->h:Z

    iget-boolean v3, p1, Lgxt;->h:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lgxt;->i:Z

    iget-boolean v3, p1, Lgxt;->i:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgxt;->e:Lhxf;

    if-nez v2, :cond_8

    iget-object v2, p1, Lgxt;->e:Lhxf;

    if-nez v2, :cond_3

    .line 556
    :goto_5
    iget-boolean v2, p0, Lgxt;->j:Z

    iget-boolean v3, p1, Lgxt;->j:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgxt;->f:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Lgxt;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 558
    :goto_6
    iget-object v2, p0, Lgxt;->k:Lhut;

    if-nez v2, :cond_a

    iget-object v2, p1, Lgxt;->k:Lhut;

    if-nez v2, :cond_3

    .line 559
    :goto_7
    iget-object v2, p0, Lgxt;->l:Lhgz;

    if-nez v2, :cond_b

    iget-object v2, p1, Lgxt;->l:Lhgz;

    if-nez v2, :cond_3

    .line 560
    :goto_8
    iget-object v2, p0, Lgxt;->I:Ljava/util/List;

    if-nez v2, :cond_c

    iget-object v2, p1, Lgxt;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 561
    goto :goto_0

    .line 549
    :cond_4
    iget-object v2, p0, Lgxt;->b:Ljava/lang/String;

    iget-object v3, p1, Lgxt;->b:Ljava/lang/String;

    .line 550
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgxt;->c:Lhgz;

    iget-object v3, p1, Lgxt;->c:Lhgz;

    .line 551
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgxt;->g:Lgxs;

    iget-object v3, p1, Lgxt;->g:Lgxs;

    .line 552
    invoke-virtual {v2, v3}, Lgxs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgxt;->d:Lhxf;

    iget-object v3, p1, Lgxt;->d:Lhxf;

    .line 553
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lgxt;->e:Lhxf;

    iget-object v3, p1, Lgxt;->e:Lhxf;

    .line 556
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lgxt;->f:Ljava/lang/String;

    iget-object v3, p1, Lgxt;->f:Ljava/lang/String;

    .line 558
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lgxt;->k:Lhut;

    iget-object v3, p1, Lgxt;->k:Lhut;

    .line 559
    invoke-virtual {v2, v3}, Lhut;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lgxt;->l:Lhgz;

    iget-object v3, p1, Lgxt;->l:Lhgz;

    .line 560
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Lgxt;->I:Ljava/util/List;

    iget-object v3, p1, Lgxt;->I:Ljava/util/List;

    .line 561
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 565
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 567
    iget-object v2, p0, Lgxt;->a:[B

    if-nez v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    .line 573
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgxt;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 574
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxt;->c:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 575
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxt;->g:Lgxs;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 576
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxt;->d:Lhxf;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 577
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lgxt;->h:Z

    if-eqz v0, :cond_6

    move v0, v3

    :goto_4
    add-int/2addr v0, v2

    .line 578
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lgxt;->i:Z

    if-eqz v0, :cond_7

    move v0, v3

    :goto_5
    add-int/2addr v0, v2

    .line 579
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxt;->e:Lhxf;

    if-nez v0, :cond_8

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 580
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lgxt;->j:Z

    if-eqz v2, :cond_9

    :goto_7
    add-int/2addr v0, v3

    .line 581
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxt;->f:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 582
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxt;->k:Lhut;

    if-nez v0, :cond_b

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 583
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxt;->l:Lhgz;

    if-nez v0, :cond_c

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 584
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgxt;->I:Ljava/util/List;

    if-nez v2, :cond_d

    :goto_b
    add-int/2addr v0, v1

    .line 585
    return v0

    :cond_1
    move v2, v0

    move v0, v1

    .line 569
    :goto_c
    iget-object v5, p0, Lgxt;->a:[B

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 570
    mul-int/lit8 v2, v2, 0x1f

    iget-object v5, p0, Lgxt;->a:[B

    aget-byte v5, v5, v0

    add-int/2addr v2, v5

    .line 569
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 573
    :cond_2
    iget-object v0, p0, Lgxt;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 574
    :cond_3
    iget-object v0, p0, Lgxt;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 575
    :cond_4
    iget-object v0, p0, Lgxt;->g:Lgxs;

    invoke-virtual {v0}, Lgxs;->hashCode()I

    move-result v0

    goto :goto_2

    .line 576
    :cond_5
    iget-object v0, p0, Lgxt;->d:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_6
    move v0, v4

    .line 577
    goto :goto_4

    :cond_7
    move v0, v4

    .line 578
    goto :goto_5

    .line 579
    :cond_8
    iget-object v0, p0, Lgxt;->e:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_9
    move v3, v4

    .line 580
    goto :goto_7

    .line 581
    :cond_a
    iget-object v0, p0, Lgxt;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 582
    :cond_b
    iget-object v0, p0, Lgxt;->k:Lhut;

    invoke-virtual {v0}, Lhut;->hashCode()I

    move-result v0

    goto :goto_9

    .line 583
    :cond_c
    iget-object v0, p0, Lgxt;->l:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_a

    .line 584
    :cond_d
    iget-object v1, p0, Lgxt;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_b
.end method
