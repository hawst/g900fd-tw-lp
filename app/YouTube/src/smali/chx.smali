.class public final Lchx;
.super Lfsa;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0, p2, p3}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 33
    const v0, 0x7f040107

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lchx;->a:Landroid/view/View;

    .line 34
    iget-object v0, p0, Lchx;->a:Landroid/view/View;

    const v1, 0x7f0802f0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchx;->b:Landroid/widget/TextView;

    .line 35
    iget-object v0, p0, Lchx;->a:Landroid/view/View;

    const v1, 0x7f0802f1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchx;->c:Landroid/widget/TextView;

    .line 36
    iget-object v0, p0, Lchx;->a:Landroid/view/View;

    const v1, 0x7f0800a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchx;->d:Landroid/widget/TextView;

    .line 37
    return-void
.end method

.method private a(Lfmx;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 41
    iget-object v0, p0, Lchx;->d:Landroid/widget/TextView;

    iget-object v2, p1, Lfmx;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_0

    iget-object v2, p1, Lfmx;->a:Lhwj;

    iget-object v2, v2, Lhwj;->a:Lhgz;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lfmx;->a:Lhwj;

    iget-object v2, v2, Lhwj;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, p1, Lfmx;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v2, p1, Lfmx;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    iget-object v2, p0, Lchx;->c:Landroid/widget/TextView;

    iget-object v0, p1, Lfmx;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    iget-object v0, p1, Lfmx;->a:Lhwj;

    iget-object v0, v0, Lhwj;->b:[Lhgz;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lfmx;->a:Lhwj;

    iget-object v3, v0, Lhwj;->b:[Lhgz;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    invoke-static {v5}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v5

    iget-object v6, p1, Lfmx;->c:Ljava/lang/CharSequence;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    iput-object v5, p1, Lfmx;->c:Ljava/lang/CharSequence;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/CharSequence;

    iget-object v7, p1, Lfmx;->c:Ljava/lang/CharSequence;

    aput-object v7, v6, v1

    const/4 v7, 0x1

    const-string v8, "line.separator"

    invoke-static {v8}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    aput-object v5, v6, v7

    invoke-static {v6}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    iput-object v5, p1, Lfmx;->c:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_2
    iget-object v0, p1, Lfmx;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v0, p0, Lchx;->b:Landroid/widget/TextView;

    iget-object v1, p1, Lfmx;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p1, Lfmx;->a:Lhwj;

    iget-object v1, v1, Lhwj;->c:Lhoy;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lfmx;->a:Lhwj;

    iget-object v1, v1, Lhwj;->c:Lhoy;

    iget-object v1, v1, Lhoy;->a:Lhgz;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lfmx;->a:Lhwj;

    iget-object v1, v1, Lhwj;->c:Lhoy;

    iget-object v1, v1, Lhoy;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p1, Lfmx;->d:Ljava/lang/CharSequence;

    :cond_3
    iget-object v1, p1, Lfmx;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    iget-object v0, p0, Lchx;->a:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 21
    check-cast p2, Lfmx;

    invoke-direct {p0, p2}, Lchx;->a(Lfmx;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 21
    check-cast p2, Lfmx;

    invoke-direct {p0, p2}, Lchx;->a(Lfmx;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
