.class final Lfwz;
.super Lfbd;
.source "SourceFile"


# direct methods
.method constructor <init>(Lfwu;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lfbd;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lfah;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 68
    const-string v0, "http://gdata.youtube.com/schemas/2007/userevents.cat"

    const-string v1, "scheme"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "term"

    invoke-interface {p2, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->A(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 70
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    const/4 v0, 0x0

    .line 72
    const-string v1, "BULLETIN_POSTED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 73
    sget-object v0, Lgbn;->j:Lgbn;

    move-object v1, v0

    .line 93
    :goto_0
    if-eqz v1, :cond_a

    .line 94
    const-class v0, Lgbo;

    invoke-virtual {p1, v0}, Lfah;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbo;

    iput-object v1, v0, Lgbo;->c:Lgbn;

    .line 100
    :cond_0
    :goto_1
    return-void

    .line 74
    :cond_1
    const-string v1, "VIDEO_UPLOADED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75
    sget-object v0, Lgbn;->e:Lgbn;

    move-object v1, v0

    goto :goto_0

    .line 76
    :cond_2
    const-string v1, "VIDEO_RATED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 77
    sget-object v0, Lgbn;->a:Lgbn;

    move-object v1, v0

    goto :goto_0

    .line 78
    :cond_3
    const-string v1, "VIDEO_SHARED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 79
    sget-object v0, Lgbn;->b:Lgbn;

    move-object v1, v0

    goto :goto_0

    .line 80
    :cond_4
    const-string v1, "VIDEO_FAVORITED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 81
    sget-object v0, Lgbn;->c:Lgbn;

    move-object v1, v0

    goto :goto_0

    .line 82
    :cond_5
    const-string v1, "VIDEO_COMMENTED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 83
    sget-object v0, Lgbn;->d:Lgbn;

    move-object v1, v0

    goto :goto_0

    .line 84
    :cond_6
    const-string v1, "VIDEO_RECOMMENDED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 85
    sget-object v0, Lgbn;->f:Lgbn;

    move-object v1, v0

    goto :goto_0

    .line 86
    :cond_7
    const-string v1, "VIDEO_ADDED_TO_PLAYLIST"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 87
    sget-object v0, Lgbn;->g:Lgbn;

    move-object v1, v0

    goto :goto_0

    .line 88
    :cond_8
    const-string v1, "FRIEND_ADDED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 89
    sget-object v0, Lgbn;->h:Lgbn;

    move-object v1, v0

    goto :goto_0

    .line 90
    :cond_9
    const-string v1, "USER_SUBSCRIPTION_ADDED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 91
    sget-object v0, Lgbn;->i:Lgbn;

    move-object v1, v0

    goto :goto_0

    .line 96
    :cond_a
    const-string v1, "Unexpected event action "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_c
    move-object v1, v0

    goto/16 :goto_0
.end method
