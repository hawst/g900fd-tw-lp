.class public final Legj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Legd;
.implements Ljava/util/Comparator;


# instance fields
.field private final a:J

.field private final b:Ljava/util/TreeSet;

.field private c:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide p1, p0, Legj;->a:J

    .line 20
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Legj;->b:Ljava/util/TreeSet;

    .line 21
    return-void
.end method

.method private b(Legc;J)V
    .locals 4

    .prologue
    .line 58
    :goto_0
    iget-wide v0, p0, Legj;->c:J

    add-long/2addr v0, p2

    iget-wide v2, p0, Legj;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 59
    iget-object v0, p0, Legj;->b:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legi;

    invoke-interface {p1, v0}, Legc;->b(Legi;)V

    goto :goto_0

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Legc;J)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Legj;->b(Legc;J)V

    .line 26
    return-void
.end method

.method public final a(Legc;Legi;)V
    .locals 4

    .prologue
    .line 30
    iget-object v0, p0, Legj;->b:Ljava/util/TreeSet;

    invoke-virtual {v0, p2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 31
    iget-wide v0, p0, Legj;->c:J

    iget-wide v2, p2, Legi;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Legj;->c:J

    .line 32
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Legj;->b(Legc;J)V

    .line 33
    return-void
.end method

.method public final a(Legc;Legi;Legi;)V
    .locals 0

    .prologue
    .line 43
    invoke-virtual {p0, p2}, Legj;->a(Legi;)V

    .line 44
    invoke-virtual {p0, p1, p3}, Legj;->a(Legc;Legi;)V

    .line 45
    return-void
.end method

.method public final a(Legi;)V
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Legj;->b:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 38
    iget-wide v0, p0, Legj;->c:J

    iget-wide v2, p1, Legi;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Legj;->c:J

    .line 39
    return-void
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 11
    check-cast p1, Legi;

    check-cast p2, Legi;

    iget-wide v0, p1, Legi;->f:J

    iget-wide v2, p2, Legi;->f:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Legi;->a(Legi;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p1, Legi;->f:J

    iget-wide v2, p2, Legi;->f:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
