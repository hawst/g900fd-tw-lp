.class public final Lcua;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcua;


# instance fields
.field public final b:Ljava/lang/String;

.field final c:Lhgz;

.field public final d:Landroid/text/Spanned;

.field public final e:Lfnc;

.field public final f:Lfnc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcua;

    invoke-direct {v0}, Lcua;-><init>()V

    sput-object v0, Lcua;->a:Lcua;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object v1, p0, Lcua;->b:Ljava/lang/String;

    .line 64
    iput-object v1, p0, Lcua;->c:Lhgz;

    .line 65
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcua;->d:Landroid/text/Spanned;

    .line 66
    iput-object v1, p0, Lcua;->e:Lfnc;

    .line 67
    iput-object v1, p0, Lcua;->f:Lfnc;

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lfid;)V
    .locals 4

    .prologue
    .line 34
    .line 36
    iget-object v0, p2, Lfid;->a:Lgxt;

    iget-object v0, v0, Lgxt;->c:Lhgz;

    .line 37
    invoke-virtual {p2}, Lfid;->c()Lfnc;

    move-result-object v1

    .line 38
    iget-object v2, p2, Lfid;->b:Lfnc;

    if-nez v2, :cond_0

    new-instance v2, Lfnc;

    iget-object v3, p2, Lfid;->a:Lgxt;

    iget-object v3, v3, Lgxt;->e:Lhxf;

    invoke-direct {v2, v3}, Lfnc;-><init>(Lhxf;)V

    iput-object v2, p2, Lfid;->b:Lfnc;

    :cond_0
    iget-object v2, p2, Lfid;->b:Lfnc;

    .line 34
    invoke-direct {p0, p1, v0, v1, v2}, Lcua;-><init>(Ljava/lang/String;Lhgz;Lfnc;Lfnc;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lhgz;Lfnc;Lfnc;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcua;->b:Ljava/lang/String;

    .line 56
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgz;

    iput-object v0, p0, Lcua;->c:Lhgz;

    .line 57
    invoke-static {p2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcua;->d:Landroid/text/Spanned;

    .line 58
    iput-object p3, p0, Lcua;->e:Lfnc;

    .line 59
    iput-object p4, p0, Lcua;->f:Lfnc;

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcua;->b:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcua;->c:Lhgz;

    .line 45
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    :goto_0
    iput-object v0, p0, Lcua;->d:Landroid/text/Spanned;

    .line 46
    if-eqz p3, :cond_1

    new-instance v0, Lfnc;

    invoke-direct {v0, p3}, Lfnc;-><init>(Landroid/net/Uri;)V

    :goto_1
    iput-object v0, p0, Lcua;->e:Lfnc;

    .line 47
    iput-object v1, p0, Lcua;->f:Lfnc;

    .line 48
    return-void

    :cond_0
    move-object v0, v1

    .line 45
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 46
    goto :goto_1
.end method
