.class public final Lhbk;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15522
    invoke-direct {p0}, Lidf;-><init>()V

    .line 15525
    const/4 v0, 0x0

    iput v0, p0, Lhbk;->a:I

    .line 15528
    const-string v0, ""

    iput-object v0, p0, Lhbk;->b:Ljava/lang/String;

    .line 15522
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 15571
    const/4 v0, 0x0

    .line 15572
    iget v1, p0, Lhbk;->a:I

    if-eqz v1, :cond_0

    .line 15573
    const/4 v0, 0x2

    iget v1, p0, Lhbk;->a:I

    .line 15574
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15576
    :cond_0
    iget-object v1, p0, Lhbk;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 15577
    const/4 v1, 0x3

    iget-object v2, p0, Lhbk;->b:Ljava/lang/String;

    .line 15578
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15580
    :cond_1
    iget-object v1, p0, Lhbk;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15581
    iput v0, p0, Lhbk;->J:I

    .line 15582
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 15518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbk;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbk;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbk;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lhbk;->a:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lhbk;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhbk;->b:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 15559
    iget v0, p0, Lhbk;->a:I

    if-eqz v0, :cond_0

    .line 15560
    const/4 v0, 0x2

    iget v1, p0, Lhbk;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 15562
    :cond_0
    iget-object v0, p0, Lhbk;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 15563
    const/4 v0, 0x3

    iget-object v1, p0, Lhbk;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 15565
    :cond_1
    iget-object v0, p0, Lhbk;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 15567
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 15540
    if-ne p1, p0, :cond_1

    .line 15545
    :cond_0
    :goto_0
    return v0

    .line 15541
    :cond_1
    instance-of v2, p1, Lhbk;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 15542
    :cond_2
    check-cast p1, Lhbk;

    .line 15543
    iget v2, p0, Lhbk;->a:I

    iget v3, p1, Lhbk;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhbk;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbk;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 15544
    :goto_1
    iget-object v2, p0, Lhbk;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbk;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 15545
    goto :goto_0

    .line 15543
    :cond_4
    iget-object v2, p0, Lhbk;->b:Ljava/lang/String;

    iget-object v3, p1, Lhbk;->b:Ljava/lang/String;

    .line 15544
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbk;->I:Ljava/util/List;

    iget-object v3, p1, Lhbk;->I:Ljava/util/List;

    .line 15545
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 15549
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 15551
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhbk;->a:I

    add-int/2addr v0, v2

    .line 15552
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbk;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 15553
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbk;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 15554
    return v0

    .line 15552
    :cond_0
    iget-object v0, p0, Lhbk;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 15553
    :cond_1
    iget-object v1, p0, Lhbk;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
