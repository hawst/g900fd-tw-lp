.class public final Lkd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Object;

.field public b:Lke;


# direct methods
.method private constructor <init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    const/16 v0, 0xe

    if-lt p1, v0, :cond_0

    .line 264
    new-instance v0, Lkh;

    invoke-direct {v0}, Lkh;-><init>()V

    iput-object v0, p0, Lkd;->b:Lke;

    .line 270
    :goto_0
    iget-object v0, p0, Lkd;->b:Lke;

    invoke-interface {v0, p2, p3}, Lke;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lkd;->a:Ljava/lang/Object;

    .line 271
    return-void

    .line 265
    :cond_0
    const/16 v0, 0x9

    if-lt p1, v0, :cond_1

    .line 266
    new-instance v0, Lkg;

    invoke-direct {v0}, Lkg;-><init>()V

    iput-object v0, p0, Lkd;->b:Lke;

    goto :goto_0

    .line 268
    :cond_1
    new-instance v0, Lkf;

    invoke-direct {v0}, Lkf;-><init>()V

    iput-object v0, p0, Lkd;->b:Lke;

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 254
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {p0, v0, p1, p2}, Lkd;-><init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 256
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Lkd;
    .locals 1

    .prologue
    .line 250
    new-instance v0, Lkd;

    invoke-direct {v0, p0, p1}, Lkd;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lkd;->b:Lke;

    iget-object v1, p0, Lkd;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lke;->f(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Lkd;->b:Lke;

    iget-object v1, p0, Lkd;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lke;->d(Ljava/lang/Object;)V

    .line 431
    return-void
.end method
