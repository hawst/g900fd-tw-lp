.class public final Lfia;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lhog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    sput-object v0, Lfia;->a:Lhog;

    return-void
.end method

.method public static a(Landroid/net/Uri;)Lhog;
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    .line 25
    new-instance v1, Lhyn;

    invoke-direct {v1}, Lhyn;-><init>()V

    iput-object v1, v0, Lhog;->k:Lhyn;

    .line 26
    iget-object v1, v0, Lhog;->k:Lhyn;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lhyn;->a:Ljava/lang/String;

    .line 27
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lhog;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    .line 43
    new-instance v1, Lhbm;

    invoke-direct {v1}, Lhbm;-><init>()V

    iput-object v1, v0, Lhog;->c:Lhbm;

    .line 44
    iget-object v1, v0, Lhog;->c:Lhbm;

    iput-object p0, v1, Lhbm;->a:Ljava/lang/String;

    .line 45
    return-object v0
.end method

.method public static a([B)Lhog;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    .line 56
    if-eqz p0, :cond_0

    .line 58
    :try_start_0
    invoke-static {v0, p0}, Lidh;->a(Lidh;[B)Lidh;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lhog;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 75
    if-nez p0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 79
    :cond_1
    invoke-static {p0}, Lidh;->a(Lidh;)[B

    move-result-object v1

    invoke-static {v1}, Lfia;->a([B)Lhog;

    move-result-object v1

    .line 80
    sget-object v2, Lidj;->f:[B

    iput-object v2, v1, Lhog;->a:[B

    .line 81
    sget-object v2, Lhls;->a:[Lhls;

    iput-object v2, v1, Lhog;->b:[Lhls;

    .line 83
    sget-object v2, Lfia;->a:Lhog;

    invoke-static {v1, v2}, Lidh;->a(Lidh;Lidh;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lhog;Lhog;Z)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 103
    if-ne p0, p1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v0, v1

    .line 108
    goto :goto_0

    .line 111
    :cond_3
    iget-object v2, p0, Lhog;->r:Lhar;

    if-eqz v2, :cond_5

    .line 112
    iget-object v0, p1, Lhog;->r:Lhar;

    if-eqz v0, :cond_4

    .line 113
    iget-object v0, p0, Lhog;->r:Lhar;

    iget-object v0, v0, Lhar;->a:Ljava/lang/String;

    iget-object v1, p1, Lhog;->r:Lhar;

    iget-object v1, v1, Lhar;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 117
    goto :goto_0

    .line 120
    :cond_5
    iget-object v2, p0, Lhog;->c:Lhbm;

    if-eqz v2, :cond_8

    .line 121
    iget-object v0, p1, Lhog;->c:Lhbm;

    if-eqz v0, :cond_7

    .line 122
    if-eqz p2, :cond_6

    iget-object v0, p0, Lhog;->c:Lhbm;

    iget-object v0, v0, Lhbm;->b:Ljava/lang/String;

    iget-object v2, p1, Lhog;->c:Lhbm;

    iget-object v2, v2, Lhbm;->b:Ljava/lang/String;

    .line 123
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 124
    goto :goto_0

    .line 126
    :cond_6
    iget-object v0, p0, Lhog;->c:Lhbm;

    iget-object v0, v0, Lhbm;->a:Ljava/lang/String;

    iget-object v1, p1, Lhog;->c:Lhbm;

    iget-object v1, v1, Lhbm;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    :cond_7
    move v0, v1

    .line 128
    goto :goto_0

    .line 131
    :cond_8
    iget-object v2, p0, Lhog;->m:Lhca;

    if-eqz v2, :cond_a

    .line 132
    iget-object v0, p1, Lhog;->m:Lhca;

    if-eqz v0, :cond_9

    .line 133
    iget-object v0, p0, Lhog;->m:Lhca;

    iget-object v0, v0, Lhca;->a:Ljava/lang/String;

    iget-object v1, p1, Lhog;->m:Lhca;

    iget-object v1, v1, Lhca;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    :cond_9
    move v0, v1

    .line 137
    goto :goto_0

    .line 140
    :cond_a
    iget-object v2, p0, Lhog;->e:Lhhp;

    if-eqz v2, :cond_b

    .line 142
    iget-object v2, p1, Lhog;->e:Lhhp;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 144
    :cond_b
    iget-object v2, p0, Lhog;->p:Lhnu;

    if-eqz v2, :cond_d

    .line 145
    iget-object v0, p1, Lhog;->p:Lhnu;

    if-eqz v0, :cond_c

    .line 146
    iget-object v0, p0, Lhog;->p:Lhnu;

    iget-object v0, v0, Lhnu;->a:Ljava/lang/String;

    iget-object v1, p1, Lhog;->p:Lhnu;

    iget-object v1, v1, Lhnu;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    :cond_c
    move v0, v1

    .line 150
    goto :goto_0

    .line 153
    :cond_d
    iget-object v2, p0, Lhog;->s:Lhpa;

    if-eqz v2, :cond_e

    .line 155
    iget-object v2, p1, Lhog;->s:Lhpa;

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 157
    :cond_e
    iget-object v2, p0, Lhog;->g:Lhuh;

    if-eqz v2, :cond_11

    .line 158
    iget-object v0, p1, Lhog;->g:Lhuh;

    if-eqz v0, :cond_10

    .line 159
    if-eqz p2, :cond_f

    iget-object v0, p0, Lhog;->g:Lhuh;

    iget-object v0, v0, Lhuh;->b:Ljava/lang/String;

    iget-object v2, p1, Lhog;->g:Lhuh;

    iget-object v2, v2, Lhuh;->b:Ljava/lang/String;

    .line 160
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    .line 161
    goto/16 :goto_0

    .line 163
    :cond_f
    iget-object v0, p0, Lhog;->g:Lhuh;

    iget-object v0, v0, Lhuh;->a:Ljava/lang/String;

    iget-object v1, p1, Lhog;->g:Lhuh;

    iget-object v1, v1, Lhuh;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/16 :goto_0

    :cond_10
    move v0, v1

    .line 165
    goto/16 :goto_0

    .line 168
    :cond_11
    iget-object v2, p0, Lhog;->k:Lhyn;

    if-eqz v2, :cond_13

    .line 169
    iget-object v0, p1, Lhog;->k:Lhyn;

    if-eqz v0, :cond_12

    .line 170
    iget-object v0, p0, Lhog;->k:Lhyn;

    iget-object v0, v0, Lhyn;->a:Ljava/lang/String;

    iget-object v1, p1, Lhog;->k:Lhyn;

    iget-object v1, v1, Lhyn;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/16 :goto_0

    :cond_12
    move v0, v1

    .line 172
    goto/16 :goto_0

    .line 175
    :cond_13
    iget-object v2, p0, Lhog;->i:Libf;

    if-eqz v2, :cond_19

    .line 176
    iget-object v2, p1, Lhog;->i:Libf;

    if-eqz v2, :cond_18

    .line 177
    iget-object v2, p0, Lhog;->i:Libf;

    iget-object v3, p1, Lhog;->i:Libf;

    if-eqz p2, :cond_14

    iget-object v4, v2, Libf;->d:Ljava/lang/String;

    iget-object v5, v3, Libf;->d:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    iget-object v4, v2, Libf;->e:Ljava/lang/String;

    iget-object v5, v3, Libf;->e:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    iget-object v4, v2, Libf;->i:Ljava/lang/String;

    iget-object v5, v3, Libf;->i:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    iget v4, v2, Libf;->h:F

    iget v5, v3, Libf;->h:F

    invoke-static {v4, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_15

    iget v4, v2, Libf;->g:F

    iget v5, v3, Libf;->g:F

    invoke-static {v4, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_15

    :cond_14
    iget-boolean v4, v2, Libf;->f:Z

    iget-boolean v5, v3, Libf;->f:Z

    if-ne v4, v5, :cond_15

    iget v4, v2, Libf;->c:I

    iget v5, v3, Libf;->c:I

    if-eq v4, v5, :cond_16

    :cond_15
    move v0, v1

    goto/16 :goto_0

    :cond_16
    iget-object v4, v2, Libf;->a:Ljava/lang/String;

    iget-object v5, v3, Libf;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_17

    iget-object v2, v2, Libf;->b:Ljava/lang/String;

    iget-object v3, v3, Libf;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_17
    move v0, v1

    goto/16 :goto_0

    :cond_18
    move v0, v1

    .line 182
    goto/16 :goto_0

    .line 184
    :cond_19
    iget-object v0, p0, Lhog;->D:Lheq;

    if-eqz v0, :cond_1b

    .line 185
    iget-object v0, p1, Lhog;->D:Lheq;

    if-eqz v0, :cond_1a

    .line 186
    iget-object v0, p0, Lhog;->D:Lheq;

    iget-object v0, v0, Lheq;->a:Ljava/lang/String;

    iget-object v1, p1, Lhog;->D:Lheq;

    iget-object v1, v1, Lheq;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/16 :goto_0

    :cond_1a
    move v0, v1

    .line 190
    goto/16 :goto_0

    :cond_1b
    move v0, v1

    .line 195
    goto/16 :goto_0
.end method
