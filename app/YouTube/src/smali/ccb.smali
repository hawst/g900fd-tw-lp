.class public final Lccb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsj;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:I

.field private final c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

.field private final d:Landroid/widget/FrameLayout;

.field private final e:Landroid/view/View;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lccb;->a:Landroid/content/res/Resources;

    .line 35
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010112

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 37
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    iput v0, p0, Lccb;->b:I

    .line 39
    const v0, 0x7f040033

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iput-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    .line 40
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    const v1, 0x7f080106

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lccb;->e:Landroid/view/View;

    .line 41
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    const v1, 0x7f08010c

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lccb;->d:Landroid/widget/FrameLayout;

    .line 42
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    const v1, 0x7f08010d

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lccb;->f:Landroid/view/View;

    .line 43
    return-void
.end method

.method private a(FF)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iget-object v2, p0, Lccb;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a00b6

    .line 119
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    float-to-int v3, p1

    iget-object v4, p0, Lccb;->a:Landroid/content/res/Resources;

    const v5, 0x7f0a00b7

    .line 120
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    .line 118
    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setPadding(IIII)V

    .line 123
    iget-object v2, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iget-object v0, p0, Lccb;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a00bd

    .line 124
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v3, v0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    move v0, v1

    .line 125
    :goto_0
    iget-object v4, p0, Lccb;->a:Landroid/content/res/Resources;

    const v5, 0x7f0a00bf

    .line 126
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    .line 127
    invoke-virtual {v2, v3, v0, v4, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a(IIII)V

    .line 128
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lccb;->a:Landroid/content/res/Resources;

    const v4, 0x7f0a00ba

    .line 125
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    return-object v0
.end method

.method public final a(Lfsg;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f0a00b4

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 73
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->hasOnClickListeners()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iget v2, p0, Lccb;->b:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a(I)V

    .line 83
    :goto_1
    iget-object v0, p0, Lccb;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 86
    iget-boolean v0, p1, Lfsg;->d:Z

    if-nez v0, :cond_2

    .line 87
    iget-object v0, p0, Lccb;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lccb;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 113
    :goto_2
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    return-object v0

    :cond_0
    move v0, v1

    .line 73
    goto :goto_0

    .line 76
    :cond_1
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 89
    :cond_2
    invoke-virtual {p1}, Lfsg;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lfsg;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    iget-object v0, p0, Lccb;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lccb;->d:Landroid/widget/FrameLayout;

    const v1, 0x7f0200ab

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 92
    iget-object v0, p0, Lccb;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-direct {p0, v0, v3}, Lccb;->a(FF)V

    goto :goto_2

    .line 95
    :cond_3
    invoke-virtual {p1}, Lfsg;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 96
    iget-object v0, p0, Lccb;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lccb;->d:Landroid/widget/FrameLayout;

    const v1, 0x7f0200b4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 98
    iget-object v0, p0, Lccb;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-direct {p0, v0, v3}, Lccb;->a(FF)V

    goto :goto_2

    .line 101
    :cond_4
    invoke-virtual {p1}, Lfsg;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 102
    iget-object v0, p0, Lccb;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lccb;->d:Landroid/widget/FrameLayout;

    const v1, 0x7f0200ac

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 104
    invoke-direct {p0, v3, v3}, Lccb;->a(FF)V

    goto :goto_2

    .line 108
    :cond_5
    iget-object v0, p0, Lccb;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lccb;->d:Landroid/widget/FrameLayout;

    const v1, 0x7f0200af

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 110
    invoke-direct {p0, v3, v3}, Lccb;->a(FF)V

    goto :goto_2
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lccb;->d:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lccb;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 48
    iget-object v1, p0, Lccb;->d:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lccb;->f:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 49
    iget-object v1, p0, Lccb;->d:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lccb;->f:Landroid/view/View;

    .line 52
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 49
    invoke-virtual {v1, p1, v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 53
    iput-object p1, p0, Lccb;->f:Landroid/view/View;

    .line 54
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lccb;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setClickable(Z)V

    .line 69
    return-void
.end method
