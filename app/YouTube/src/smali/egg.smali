.class public final Legg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefc;


# instance fields
.field private final a:Legc;

.field private final b:Lefc;

.field private final c:Lefc;

.field private final d:Lefc;

.field private final e:Legh;

.field private final f:Z

.field private final g:Z

.field private h:Lefc;

.field private i:Landroid/net/Uri;

.field private j:Ljava/lang/String;

.field private k:J

.field private l:J

.field private m:Legi;

.field private n:Z

.field private o:J


# direct methods
.method public constructor <init>(Legc;Lefc;Lefc;Lefb;ZZLegh;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Legg;->a:Legc;

    .line 98
    iput-object p3, p0, Legg;->b:Lefc;

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Legg;->f:Z

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Legg;->g:Z

    .line 101
    iput-object p2, p0, Legg;->d:Lefc;

    .line 102
    if-eqz p4, :cond_0

    .line 103
    new-instance v0, Lefz;

    invoke-direct {v0, p2, p4}, Lefz;-><init>(Lefc;Lefb;)V

    iput-object v0, p0, Legg;->c:Lefc;

    .line 107
    :goto_0
    iput-object p7, p0, Legg;->e:Legh;

    .line 108
    return-void

    .line 105
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Legg;->c:Lefc;

    goto :goto_0
.end method

.method private a(Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 223
    iget-boolean v0, p0, Legg;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Legg;->h:Lefc;

    iget-object v1, p0, Legg;->b:Lefc;

    if-eq v0, v1, :cond_0

    instance-of v0, p1, Legf;

    if-eqz v0, :cond_1

    .line 226
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Legg;->n:Z

    .line 228
    :cond_1
    return-void
.end method

.method private b()V
    .locals 9

    .prologue
    .line 173
    :try_start_0
    iget-boolean v0, p0, Legg;->n:Z

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x0

    move-object v2, v0

    .line 180
    :goto_0
    if-nez v2, :cond_2

    .line 183
    iget-object v0, p0, Legg;->d:Lefc;

    iput-object v0, p0, Legg;->h:Lefc;

    .line 184
    new-instance v0, Lefg;

    iget-object v1, p0, Legg;->i:Landroid/net/Uri;

    iget-wide v2, p0, Legg;->k:J

    iget-wide v4, p0, Legg;->l:J

    iget-object v6, p0, Legg;->j:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 200
    :goto_1
    iget-object v1, p0, Legg;->h:Lefc;

    invoke-interface {v1, v0}, Lefc;->a(Lefg;)J

    .line 204
    return-void

    .line 175
    :cond_0
    iget-boolean v0, p0, Legg;->f:Z

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Legg;->a:Legc;

    iget-object v1, p0, Legg;->j:Ljava/lang/String;

    iget-wide v2, p0, Legg;->k:J

    invoke-interface {v0, v1, v2, v3}, Legc;->a(Ljava/lang/String;J)Legi;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 178
    :cond_1
    iget-object v0, p0, Legg;->a:Legc;

    iget-object v1, p0, Legg;->j:Ljava/lang/String;

    iget-wide v2, p0, Legg;->k:J

    invoke-interface {v0, v1, v2, v3}, Legc;->b(Ljava/lang/String;J)Legi;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 185
    :cond_2
    iget-boolean v0, v2, Legi;->d:Z

    if-eqz v0, :cond_3

    .line 187
    iget-object v0, v2, Legi;->e:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 188
    iget-wide v4, p0, Legg;->k:J

    iget-wide v6, v2, Legi;->b:J

    sub-long v7, v4, v6

    .line 189
    iget-wide v2, v2, Legi;->c:J

    sub-long/2addr v2, v7

    iget-wide v4, p0, Legg;->l:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 190
    new-instance v0, Lefg;

    iget-wide v2, p0, Legg;->k:J

    iget-object v6, p0, Legg;->j:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;J)V

    .line 191
    iget-object v1, p0, Legg;->b:Lefc;

    move-object v2, p0

    .line 197
    :goto_2
    iput-object v1, v2, Legg;->h:Lefc;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 201
    :catch_0
    move-exception v0

    .line 203
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 194
    :cond_3
    :try_start_1
    iput-object v2, p0, Legg;->m:Legi;

    .line 195
    iget-wide v0, v2, Legi;->c:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_5

    iget-wide v4, p0, Legg;->l:J

    .line 196
    :goto_4
    new-instance v0, Lefg;

    iget-object v1, p0, Legg;->i:Landroid/net/Uri;

    iget-wide v2, p0, Legg;->k:J

    iget-object v6, p0, Legg;->j:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 197
    iget-object v1, p0, Legg;->c:Lefc;

    if-eqz v1, :cond_6

    iget-object v1, p0, Legg;->c:Lefc;

    move-object v2, p0

    goto :goto_2

    .line 195
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    iget-wide v0, v2, Legi;->c:J

    iget-wide v2, p0, Legg;->l:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_4

    .line 197
    :cond_6
    iget-object v1, p0, Legg;->d:Lefc;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v2, p0

    goto :goto_2
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 208
    iget-object v0, p0, Legg;->h:Lefc;

    if-nez v0, :cond_1

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    :try_start_0
    iget-object v0, p0, Legg;->h:Lefc;

    invoke-interface {v0}, Lefc;->a()V

    .line 213
    const/4 v0, 0x0

    iput-object v0, p0, Legg;->h:Lefc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    iget-object v0, p0, Legg;->m:Legi;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Legg;->a:Legc;

    iget-object v1, p0, Legg;->m:Legi;

    invoke-interface {v0, v1}, Legc;->a(Legi;)V

    .line 217
    iput-object v3, p0, Legg;->m:Legi;

    goto :goto_0

    .line 215
    :catchall_0
    move-exception v0

    iget-object v1, p0, Legg;->m:Legi;

    if-eqz v1, :cond_2

    .line 216
    iget-object v1, p0, Legg;->a:Legc;

    iget-object v2, p0, Legg;->m:Legi;

    invoke-interface {v1, v2}, Legc;->a(Legi;)V

    .line 217
    iput-object v3, p0, Legg;->m:Legi;

    :cond_2
    throw v0
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    .line 132
    :try_start_0
    iget-object v0, p0, Legg;->h:Lefc;

    invoke-interface {v0, p1, p2, p3}, Lefc;->a([BII)I

    move-result v0

    .line 133
    if-ltz v0, :cond_2

    .line 134
    iget-object v1, p0, Legg;->h:Lefc;

    iget-object v2, p0, Legg;->b:Lefc;

    if-ne v1, v2, :cond_0

    .line 135
    iget-wide v2, p0, Legg;->o:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Legg;->o:J

    .line 137
    :cond_0
    iget-wide v2, p0, Legg;->k:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Legg;->k:J

    .line 138
    iget-wide v2, p0, Legg;->l:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Legg;->l:J

    .line 146
    :cond_1
    :goto_0
    return v0

    .line 140
    :cond_2
    invoke-direct {p0}, Legg;->c()V

    .line 141
    iget-wide v2, p0, Legg;->l:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 142
    invoke-direct {p0}, Legg;->b()V

    .line 143
    invoke-virtual {p0, p1, p2, p3}, Legg;->a([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    .line 148
    invoke-direct {p0, v0}, Legg;->a(Ljava/io/IOException;)V

    .line 149
    throw v0
.end method

.method public final a(Lefg;)J
    .locals 4

    .prologue
    .line 112
    iget-boolean v0, p1, Lefg;->b:Z

    invoke-static {v0}, La;->c(Z)V

    .line 115
    iget-wide v0, p1, Lefg;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 117
    :try_start_0
    iget-object v0, p1, Lefg;->a:Landroid/net/Uri;

    iput-object v0, p0, Legg;->i:Landroid/net/Uri;

    .line 118
    iget-object v0, p1, Lefg;->f:Ljava/lang/String;

    iput-object v0, p0, Legg;->j:Ljava/lang/String;

    .line 119
    iget-wide v0, p1, Lefg;->d:J

    iput-wide v0, p0, Legg;->k:J

    .line 120
    iget-wide v0, p1, Lefg;->e:J

    iput-wide v0, p0, Legg;->l:J

    .line 121
    invoke-direct {p0}, Legg;->b()V

    .line 122
    iget-wide v0, p1, Lefg;->e:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    .line 115
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    invoke-direct {p0, v0}, Legg;->a(Ljava/io/IOException;)V

    .line 125
    throw v0
.end method

.method public final a()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 155
    iget-object v0, p0, Legg;->e:Legh;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Legg;->o:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    iget-object v0, p0, Legg;->e:Legh;

    iget-object v1, p0, Legg;->a:Legc;

    invoke-interface {v1}, Legc;->b()J

    move-result-wide v2

    iget-wide v4, p0, Legg;->o:J

    invoke-interface {v0, v2, v3, v4, v5}, Legh;->a(JJ)V

    iput-wide v6, p0, Legg;->o:J

    .line 157
    :cond_0
    :try_start_0
    invoke-direct {p0}, Legg;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    invoke-direct {p0, v0}, Legg;->a(Ljava/io/IOException;)V

    .line 160
    throw v0
.end method
