.class public final Lyy;
.super Lyi;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:J

.field private d:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    const-string v0, "hmhd"

    invoke-direct {p0, v0}, Lyi;-><init>(Ljava/lang/String;)V

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lyy;->c(Ljava/nio/ByteBuffer;)J

    .line 64
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lyy;->a:I

    .line 65
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lyy;->b:I

    .line 66
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lyy;->c:J

    .line 67
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lyy;->d:J

    .line 68
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    .line 70
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lyy;->d(Ljava/nio/ByteBuffer;)V

    .line 75
    iget v0, p0, Lyy;->a:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 76
    iget v0, p0, Lyy;->b:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 77
    iget-wide v0, p0, Lyy;->c:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 78
    iget-wide v0, p0, Lyy;->d:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 79
    const-wide/16 v0, 0x0

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 80
    return-void
.end method

.method protected final d_()J
    .locals 2

    .prologue
    .line 58
    const-wide/16 v0, 0x14

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 84
    const-string v0, "HintMediaHeaderBox{maxPduSize="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lyy;->a:I

    iget v2, p0, Lyy;->b:I

    iget-wide v4, p0, Lyy;->c:J

    iget-wide v6, p0, Lyy;->d:J

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x66

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", avgPduSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxBitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", avgBitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
