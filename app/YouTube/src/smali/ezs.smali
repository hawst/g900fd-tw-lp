.class public abstract Lezs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lewi;


# instance fields
.field private volatile a:Ljava/lang/Object;

.field private volatile b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lezs;->b:Z

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lezs;->b:Z

    .line 53
    new-instance v0, Lezt;

    invoke-direct {v0, p0}, Lezt;-><init>(Lezs;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 61
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/Object;
.end method

.method declared-synchronized b()V
    .locals 1

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lezs;->b:Z

    if-nez v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lezs;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lezs;->a:Ljava/lang/Object;

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lezs;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :cond_0
    monitor-exit p0

    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lezs;->b:Z

    if-nez v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lezs;->b()V

    .line 78
    :cond_0
    iget-object v0, p0, Lezs;->a:Ljava/lang/Object;

    return-object v0
.end method
