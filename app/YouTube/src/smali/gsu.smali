.class final Lgsu;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private synthetic b:Lgsp;


# direct methods
.method constructor <init>(Lgsp;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lgsu;->b:Lgsp;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 360
    iput-object p2, p0, Lgsu;->a:Landroid/graphics/drawable/Drawable;

    .line 361
    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 365
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 366
    iget-object v0, p0, Lgsu;->b:Lgsp;

    invoke-static {v0}, Lgsp;->e(Lgsp;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v0

    iget-object v1, p0, Lgsu;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroupOverlay;->remove(Landroid/graphics/drawable/Drawable;)V

    .line 368
    :cond_0
    return-void
.end method
