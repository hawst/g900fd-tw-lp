.class public abstract Lfsp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lftd;


# instance fields
.field private a:Z

.field private b:Lfsq;

.field private final c:Lfsz;

.field private final d:Lgiv;

.field public e:Ljava/util/Map;

.field public f:[B

.field public g:Z

.field public h:Z

.field public i:I


# direct methods
.method public constructor <init>(Lfsz;Lgit;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p2, Lgit;->b:Lgiv;

    invoke-direct {p0, p1, v0}, Lfsp;-><init>(Lfsz;Lgiv;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lfsz;Lgiv;)V
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lfsq;->a:Lfsq;

    invoke-direct {p0, p1, p2, v0}, Lfsp;-><init>(Lfsz;Lgiv;Lfsq;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Lfsz;Lgiv;Lfsq;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v1, p0, Lfsp;->a:Z

    .line 55
    sget-object v0, Lfsq;->a:Lfsq;

    iput-object v0, p0, Lfsp;->b:Lfsq;

    .line 58
    iput v1, p0, Lfsp;->i:I

    .line 67
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsz;

    iput-object v0, p0, Lfsp;->c:Lfsz;

    .line 68
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgiv;

    iput-object v0, p0, Lfsp;->d:Lgiv;

    .line 69
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsq;

    iput-object v0, p0, Lfsp;->b:Lfsq;

    .line 70
    return-void
.end method

.method public static a(I)I
    .locals 0

    .prologue
    .line 250
    if-ltz p0, :cond_0

    :goto_0
    return p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 216
    move v3, v2

    move v0, v2

    .line 217
    :goto_0
    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    aget-object v4, p0, v3

    .line 218
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 219
    add-int/lit8 v0, v0, 0x1

    .line 217
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 222
    :cond_1
    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lb;->c(Z)V

    .line 223
    return-void

    :cond_2
    move v0, v2

    .line 222
    goto :goto_1
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 233
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a(Lfsq;)V
    .locals 2

    .prologue
    .line 96
    sget-object v0, Lfsq;->a:Lfsq;

    if-eq p1, v0, :cond_0

    const-string v0, "NO_CACHE_KEY_VALUE"

    invoke-virtual {p0}, Lfsp;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "You must override getCacheKey() in order to use forced caching."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    iput-object p1, p0, Lfsp;->b:Lfsq;

    .line 102
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 275
    iput-boolean p1, p0, Lfsp;->g:Z

    .line 276
    return-void
.end method

.method public final a([B)V
    .locals 0

    .prologue
    .line 148
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iput-object p1, p0, Lfsp;->f:[B

    .line 150
    return-void
.end method

.method public abstract b()V
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "NO_CACHE_KEY_VALUE"

    return-object v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lfsp;->b:Lfsq;

    sget-object v1, Lfsq;->a:Lfsq;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lfsp;->b:Lfsq;

    sget-object v1, Lfsq;->c:Lfsq;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/util/Map;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lfsp;->e:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfsp;->e:Ljava/util/Map;

    .line 134
    :cond_0
    iget-object v0, p0, Lfsp;->e:Ljava/util/Map;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 154
    invoke-virtual {p0}, Lfsp;->b()V

    .line 155
    iget-object v0, p0, Lfsp;->f:[B

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must set clickTrackingParams."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_0
    return-void
.end method

.method public final i()Lhjx;
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lfsp;->c:Lfsz;

    .line 189
    invoke-static {}, Lb;->b()V

    new-instance v1, Lhjx;

    invoke-direct {v1}, Lhjx;-><init>()V

    new-instance v2, Lhyp;

    invoke-direct {v2}, Lhyp;-><init>()V

    iput-object v2, v1, Lhjx;->b:Lhyp;

    iget-object v0, v0, Lfsz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggz;

    invoke-interface {v0, v1, p0}, Lggz;->a(Lhjx;Lgkp;)V

    goto :goto_0

    .line 190
    :cond_0
    new-instance v0, Lhcp;

    invoke-direct {v0}, Lhcp;-><init>()V

    .line 191
    iget-object v2, p0, Lfsp;->f:[B

    iput-object v2, v0, Lhcp;->a:[B

    .line 192
    iput-object v0, v1, Lhjx;->e:Lhcp;

    .line 193
    iget-object v0, v1, Lhjx;->a:Lhcv;

    if-nez v0, :cond_1

    .line 194
    new-instance v0, Lhcv;

    invoke-direct {v0}, Lhcv;-><init>()V

    iput-object v0, v1, Lhjx;->a:Lhcv;

    .line 196
    :cond_1
    iget v0, p0, Lfsp;->i:I

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, v1, Lhjx;->a:Lhcv;

    iget v2, p0, Lfsp;->i:I

    iput v2, v0, Lhcv;->q:I

    .line 202
    :cond_2
    iget-boolean v0, p0, Lfsp;->h:Z

    if-eqz v0, :cond_4

    .line 203
    iget-object v0, v1, Lhjx;->d:Lhtu;

    if-nez v0, :cond_3

    .line 204
    new-instance v0, Lhtu;

    invoke-direct {v0}, Lhtu;-><init>()V

    iput-object v0, v1, Lhjx;->d:Lhtu;

    .line 206
    :cond_3
    iget-object v0, v1, Lhjx;->d:Lhtu;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lhtu;->a:Z

    .line 208
    :cond_4
    return-object v1
.end method

.method public final j()Lggq;
    .locals 3

    .prologue
    .line 258
    new-instance v1, Lggq;

    invoke-direct {v1}, Lggq;-><init>()V

    .line 259
    const-string v0, "serviceName"

    invoke-virtual {p0}, Lfsp;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 260
    const-string v2, "clickTrackingParams"

    iget-object v0, p0, Lfsp;->f:[B

    if-nez v0, :cond_0

    sget-object v0, Lfhy;->a:[B

    :cond_0
    invoke-virtual {v1, v2, v0}, Lggq;->a(Ljava/lang/String;[B)Lggq;

    .line 261
    iget-object v0, p0, Lfsp;->d:Lgiv;

    invoke-virtual {v0}, Lgiv;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 262
    const-string v0, "managingAccount"

    iget-object v2, p0, Lfsp;->d:Lgiv;

    invoke-virtual {v2}, Lgiv;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 263
    const-string v0, "onBehalfOf"

    iget-object v2, p0, Lfsp;->d:Lgiv;

    invoke-virtual {v2}, Lgiv;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 265
    :cond_1
    return-object v1
.end method

.method public final k()Lgiv;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lfsp;->d:Lgiv;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lfsp;->g:Z

    return v0
.end method
