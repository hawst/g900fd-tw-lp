.class final Lgdw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgdt;
.implements Lged;


# instance fields
.field final synthetic a:Lgdu;


# direct methods
.method constructor <init>(Lgdu;)V
    .locals 0

    .prologue
    .line 819
    iput-object p1, p0, Lgdw;->a:Lgdu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lgds;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 883
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->i(Lgdu;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 884
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->j(Lgdu;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->k(Lgdu;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 885
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->k(Lgdu;)I

    move-result v0

    invoke-interface {p1, v0}, Lgds;->a(I)V

    .line 887
    :cond_0
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->l(Lgdu;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->m(Lgdu;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 888
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->n(Lgdu;)Lgei;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lgei;->a(I)V

    .line 890
    :cond_1
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v2}, Lgdu;->e(Lgdu;Z)Z

    .line 891
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->j(Lgdu;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->h(Lgdu;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 892
    :cond_2
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v2}, Lgdu;->f(Lgdu;Z)V

    .line 895
    :cond_3
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->o(Lgdu;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 896
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-virtual {v0}, Lgdu;->e()V

    .line 899
    :cond_4
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 825
    invoke-static {}, Lezp;->a()V

    .line 826
    iget-object v0, p0, Lgdw;->a:Lgdu;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lgdu;->a(Lgdu;Z)Z

    .line 827
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->d(Lgdu;)Lfqj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 828
    iget-object v0, p0, Lgdw;->a:Lgdu;

    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->d(Lgdu;)Lfqj;

    move-result-object v1

    invoke-static {v0, v1}, Lgdu;->a(Lgdu;Lfqj;)V

    .line 829
    iget-object v0, p0, Lgdw;->a:Lgdu;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lgdu;->b(Lgdu;Lfqj;)Lfqj;

    .line 831
    :cond_0
    return-void
.end method

.method public final a(Lgds;)V
    .locals 2

    .prologue
    .line 853
    invoke-static {}, Lezp;->a()V

    .line 854
    iget-object v0, p0, Lgdw;->a:Lgdu;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lgdu;->c(Lgdu;Z)Z

    .line 855
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-interface {p1}, Lgds;->f()I

    move-result v1

    invoke-static {v0, v1}, Lgdu;->b(Lgdu;I)I

    .line 856
    invoke-direct {p0, p1}, Lgdw;->b(Lgds;)V

    .line 857
    return-void
.end method

.method public final a(Lgds;II)V
    .locals 2

    .prologue
    .line 862
    invoke-static {}, Lezp;->a()V

    .line 863
    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 866
    :cond_1
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->f(Lgdu;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 867
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->e(Lgdu;)Lgec;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lgec;->a(II)V

    .line 876
    :goto_1
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->h(Lgdu;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 877
    iget-object v0, p0, Lgdw;->a:Lgdu;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lgdu;->d(Lgdu;Z)Z

    .line 878
    invoke-direct {p0, p1}, Lgdw;->b(Lgds;)V

    goto :goto_0

    .line 869
    :cond_2
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->g(Lgdu;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lgdx;

    invoke-direct {v1, p0, p2, p3}, Lgdx;-><init>(Lgdw;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public final a(II)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 976
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "media player info "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 977
    packed-switch p1, :pswitch_data_0

    .line 984
    :goto_0
    return v5

    .line 979
    :pswitch_0
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->v(Lgdu;)Lfqj;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Buffering data from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 980
    iget-object v0, p0, Lgdw;->a:Lgdu;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lgdu;->f(Lgdu;Z)V

    goto :goto_0

    .line 983
    :pswitch_1
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v5}, Lgdu;->f(Lgdu;Z)V

    goto :goto_0

    .line 977
    nop

    :pswitch_data_0
    .packed-switch 0x2bd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 835
    invoke-static {}, Lezp;->a()V

    .line 836
    return-void
.end method

.method public final b(II)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 933
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->t(Lgdu;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 934
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v2}, Lgdu;->e(Lgdu;Z)Z

    .line 935
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v3}, Lgdu;->i(Lgdu;Z)Z

    .line 936
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x46

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "MediaPlayer error during prepare [what="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extra="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 943
    :goto_0
    iget-object v4, p0, Lgdw;->a:Lgdu;

    if-ne p1, v2, :cond_3

    const/16 v0, -0x3ec

    if-eq p2, v0, :cond_0

    const/high16 v0, -0x80000000

    if-ne p2, v0, :cond_3

    :cond_0
    iget-object v0, v4, Lgdu;->b:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    new-instance v0, Lggp;

    const-string v1, "net.nomobiledata"

    invoke-virtual {v4}, Lgdu;->i()I

    move-result v4

    invoke-direct {v0, v1, v4}, Lggp;-><init>(Ljava/lang/String;I)V

    .line 945
    :goto_2
    invoke-virtual {v0}, Lggp;->a()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->u(Lgdu;)Lgea;

    move-result-object v1

    iget v1, v1, Lgea;->d:I

    const/4 v4, 0x3

    if-ge v1, v4, :cond_6

    move v1, v2

    :goto_3
    if-eqz v1, :cond_8

    .line 946
    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->u(Lgdu;)Lgea;

    move-result-object v1

    invoke-virtual {v1}, Lgea;->a()V

    .line 948
    const/16 v1, 0x64

    if-ne p1, v1, :cond_1

    .line 950
    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->e(Lgdu;)Lgec;

    move-result-object v1

    invoke-interface {v1}, Lgec;->d()V

    .line 953
    :cond_1
    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->n(Lgdu;)Lgei;

    move-result-object v1

    invoke-interface {v1, v0}, Lgei;->a(Lggp;)V

    .line 955
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->j(Lgdu;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 956
    iget-object v0, p0, Lgdw;->a:Lgdu;

    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->v(Lgdu;)Lfqj;

    move-result-object v1

    invoke-static {v0, v1}, Lgdu;->c(Lgdu;Lfqj;)V

    .line 960
    :goto_4
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->u(Lgdu;)Lgea;

    move-result-object v0

    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->k(Lgdu;)I

    move-result v1

    iput v1, v0, Lgea;->c:I

    iget-object v1, v0, Lgea;->b:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v1, v0, Lgea;->b:Landroid/os/Handler;

    iget-object v0, v0, Lgea;->a:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 971
    :goto_5
    return v2

    .line 938
    :cond_2
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v3}, Lgdu;->e(Lgdu;Z)Z

    .line 939
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v2}, Lgdu;->i(Lgdu;Z)Z

    .line 940
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x47

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "MediaPlayer error during playback [what="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extra="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 943
    goto/16 :goto_1

    :cond_4
    if-ne p1, v2, :cond_5

    sget-object v0, Lgdu;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_5

    new-instance v1, Lggp;

    sget-object v0, Lgdu;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4}, Lgdu;->i()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0xc

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "e"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v4, v5}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    move-object v0, v1

    goto/16 :goto_2

    :cond_5
    new-instance v0, Lggp;

    const-string v1, "android.fw"

    invoke-virtual {v4}, Lgdu;->i()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x18

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "w"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "e"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v1, v4, v5}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    goto/16 :goto_2

    :cond_6
    move v1, v3

    .line 945
    goto/16 :goto_3

    .line 958
    :cond_7
    iget-object v0, p0, Lgdw;->a:Lgdu;

    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->v(Lgdu;)Lfqj;

    move-result-object v1

    iget-object v3, p0, Lgdw;->a:Lgdu;

    invoke-static {v3}, Lgdu;->k(Lgdu;)I

    move-result v3

    invoke-static {v0, v1, v3}, Lgdu;->a(Lgdu;Lfqj;I)V

    goto/16 :goto_4

    .line 962
    :cond_8
    const-string v1, "Reporting MediaPlayer error"

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    .line 964
    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->u(Lgdu;)Lgea;

    move-result-object v1

    invoke-virtual {v1}, Lgea;->b()V

    .line 965
    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1, v3}, Lgdu;->e(Lgdu;Z)Z

    .line 966
    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1, v3}, Lgdu;->i(Lgdu;Z)Z

    .line 968
    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-virtual {v1}, Lgdu;->g()V

    .line 969
    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->n(Lgdu;)Lgei;

    move-result-object v1

    invoke-virtual {v0}, Lggp;->b()Lggp;

    move-result-object v0

    invoke-interface {v1, v0}, Lgei;->a(Lggp;)V

    goto/16 :goto_5
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 842
    invoke-static {}, Lezp;->a()V

    .line 843
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-virtual {v0}, Lgdu;->h()V

    .line 844
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v1}, Lgdu;->b(Lgdu;Z)V

    .line 845
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->e(Lgdu;)Lgec;

    move-result-object v0

    invoke-interface {v0}, Lgec;->c()V

    .line 846
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v1}, Lgdu;->a(Lgdu;Z)Z

    .line 847
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    const/16 v0, 0x64

    .line 904
    const/16 v1, 0x5a

    if-le p1, v1, :cond_1

    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->p(Lgdu;)I

    move-result v1

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->p(Lgdu;)I

    move-result v1

    if-ne v1, v0, :cond_1

    :cond_0
    move p1, v0

    .line 907
    :cond_1
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, p1}, Lgdu;->c(Lgdu;I)I

    .line 908
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 924
    invoke-static {}, Lezp;->a()V

    .line 925
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v2}, Lgdu;->d(Lgdu;I)I

    .line 926
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v2}, Lgdu;->h(Lgdu;Z)Z

    .line 927
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->n(Lgdu;)Lgei;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lgei;->a(I)V

    .line 928
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0, v2}, Lgdu;->b(Lgdu;Z)V

    .line 929
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 914
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->n(Lgdu;)Lgei;

    move-result-object v0

    iget-object v1, p0, Lgdw;->a:Lgdu;

    invoke-static {v1}, Lgdu;->k(Lgdu;)I

    move-result v1

    invoke-interface {v0, v1}, Lgei;->c(I)V

    .line 915
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->q(Lgdu;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->r(Lgdu;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->s(Lgdu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    iget-object v0, p0, Lgdw;->a:Lgdu;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lgdu;->g(Lgdu;Z)Z

    .line 918
    iget-object v0, p0, Lgdw;->a:Lgdu;

    invoke-static {v0}, Lgdu;->n(Lgdu;)Lgei;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lgei;->a(I)V

    .line 920
    :cond_0
    return-void
.end method
