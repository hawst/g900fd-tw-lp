.class public final Lgsh;
.super Lecc;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

.field private b:J

.field private c:Lgsa;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;Lgsa;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lecc;-><init>()V

    .line 28
    iput-object p1, p0, Lgsh;->a:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    .line 29
    iput-object p2, p0, Lgsh;->c:Lgsa;

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(J)V
    .locals 7

    .prologue
    .line 74
    iget-object v0, p0, Lgsh;->c:Lgsa;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lgsa;->b(JZ)Lgrs;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_0

    .line 79
    iget-object v1, v0, Lgrs;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 81
    invoke-virtual {v0}, Lgrs;->c()V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    iget-object v1, p0, Lgsh;->a:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;

    new-instance v2, Lgsi;

    invoke-direct {v2, p0, v0}, Lgsi;-><init>(Lgsh;Lgrs;)V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoWithPreviewView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected final a(JJ)V
    .locals 1

    .prologue
    .line 54
    iput-wide p1, p0, Lgsh;->b:J

    .line 55
    return-void
.end method

.method protected final a(JZ)V
    .locals 1

    .prologue
    .line 49
    iput-wide p1, p0, Lgsh;->b:J

    .line 50
    return-void
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method protected final e()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method

.method protected final f()J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lgsh;->b:J

    return-wide v0
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method

.method protected final m()J
    .locals 2

    .prologue
    .line 59
    const-wide/16 v0, -0x2

    return-wide v0
.end method

.method protected final n()J
    .locals 2

    .prologue
    .line 69
    const-wide/16 v0, -0x3

    return-wide v0
.end method
