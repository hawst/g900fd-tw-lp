.class public final Lijk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lijl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lijl;

    invoke-direct {v0}, Lijl;-><init>()V

    sput-object v0, Lijk;->a:Lijl;

    return-void
.end method

.method private static a(I[I[I[I[[I)Liga;
    .locals 19

    .prologue
    .line 294
    move-object/from16 v0, p3

    array-length v1, v0

    new-array v9, v1, [I

    .line 296
    const/16 v1, 0x64

    .line 297
    :goto_0
    add-int/lit8 v8, v1, -0x1

    if-lez v1, :cond_1b

    .line 298
    const/4 v1, 0x0

    :goto_1
    array-length v2, v9

    if-ge v1, v2, :cond_0

    .line 299
    aget v2, p3, v1

    aget-object v3, p4, v1

    aget v4, v9, v1

    aget v3, v3, v4

    aput v3, p1, v2

    .line 298
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 302
    :cond_0
    :try_start_0
    move-object/from16 v0, p1

    array-length v1, v0

    if-nez v1, :cond_1

    invoke-static {}, Liet;->a()Liet;

    move-result-object v1

    throw v1
    :try_end_0
    .catch Lier; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    :catch_0
    move-exception v1

    array-length v1, v9

    if-nez v1, :cond_17

    .line 307
    invoke-static {}, Lier;->a()Lier;

    move-result-object v1

    throw v1

    .line 302
    :cond_1
    const/4 v1, 0x1

    add-int/lit8 v2, p0, 0x1

    shl-int v7, v1, v2

    if-eqz p2, :cond_2

    :try_start_1
    move-object/from16 v0, p2

    array-length v1, v0

    div-int/lit8 v2, v7, 0x2

    add-int/lit8 v2, v2, 0x3

    if-gt v1, v2, :cond_3

    :cond_2
    if-ltz v7, :cond_3

    const/16 v1, 0x200

    if-le v7, v1, :cond_4

    :cond_3
    invoke-static {}, Lier;->a()Lier;

    move-result-object v1

    throw v1

    :cond_4
    sget-object v10, Lijk;->a:Lijl;

    new-instance v3, Lijn;

    iget-object v1, v10, Lijl;->a:Lijm;

    move-object/from16 v0, p1

    invoke-direct {v3, v1, v0}, Lijn;-><init>(Lijm;[I)V

    new-array v4, v7, [I

    const/4 v1, 0x0

    move v2, v7

    :goto_2
    if-lez v2, :cond_6

    iget-object v5, v10, Lijl;->a:Lijm;

    iget-object v5, v5, Lijm;->b:[I

    aget v5, v5, v2

    invoke-virtual {v3, v5}, Lijn;->b(I)I

    move-result v5

    sub-int v6, v7, v2

    aput v5, v4, v6

    if-eqz v5, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_6
    if-nez v1, :cond_7

    const/4 v1, 0x0

    :goto_3
    move-object/from16 v0, p1

    array-length v2, v0

    const/4 v3, 0x4

    if-ge v2, v3, :cond_13

    invoke-static {}, Liet;->a()Liet;

    move-result-object v1

    throw v1

    :cond_7
    iget-object v1, v10, Lijl;->a:Lijm;

    iget-object v2, v1, Lijm;->e:Lijn;

    move-object/from16 v0, p2

    array-length v3, v0

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v3, :cond_8

    aget v5, p2, v1

    iget-object v6, v10, Lijl;->a:Lijm;

    move-object/from16 v0, p1

    array-length v11, v0

    add-int/lit8 v11, v11, -0x1

    sub-int v5, v11, v5

    iget-object v6, v6, Lijm;->b:[I

    aget v5, v6, v5

    new-instance v6, Lijn;

    iget-object v11, v10, Lijl;->a:Lijm;

    const/4 v12, 0x2

    new-array v12, v12, [I

    const/4 v13, 0x0

    iget-object v14, v10, Lijl;->a:Lijm;

    const/4 v15, 0x0

    invoke-virtual {v14, v15, v5}, Lijm;->c(II)I

    move-result v5

    aput v5, v12, v13

    const/4 v5, 0x1

    const/4 v13, 0x1

    aput v13, v12, v5

    invoke-direct {v6, v11, v12}, Lijn;-><init>(Lijm;[I)V

    invoke-virtual {v2, v6}, Lijn;->c(Lijn;)Lijn;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    new-instance v1, Lijn;

    iget-object v2, v10, Lijl;->a:Lijm;

    invoke-direct {v1, v2, v4}, Lijn;-><init>(Lijm;[I)V

    iget-object v2, v10, Lijl;->a:Lijm;

    const/4 v3, 0x1

    invoke-virtual {v2, v7, v3}, Lijm;->a(II)Lijn;

    move-result-object v3

    iget-object v2, v3, Lijn;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    iget-object v4, v1, Lijn;->b:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_1c

    move-object v4, v1

    :goto_5
    iget-object v1, v10, Lijl;->a:Lijm;

    iget-object v2, v1, Lijm;->d:Lijn;

    iget-object v1, v10, Lijl;->a:Lijm;

    iget-object v1, v1, Lijm;->e:Lijn;

    :goto_6
    iget-object v5, v3, Lijn;->b:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    div-int/lit8 v6, v7, 0x2

    if-lt v5, v6, :cond_e

    invoke-virtual {v3}, Lijn;->a()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-static {}, Lier;->a()Lier;

    move-result-object v1

    throw v1

    :cond_9
    iget-object v5, v10, Lijl;->a:Lijm;

    iget-object v5, v5, Lijm;->d:Lijn;

    iget-object v6, v3, Lijn;->b:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v6}, Lijn;->a(I)I

    move-result v6

    iget-object v11, v10, Lijl;->a:Lijm;

    invoke-virtual {v11, v6}, Lijm;->a(I)I

    move-result v11

    :goto_7
    iget-object v6, v4, Lijn;->b:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    iget-object v12, v3, Lijn;->b:[I

    array-length v12, v12

    add-int/lit8 v12, v12, -0x1

    if-lt v6, v12, :cond_d

    invoke-virtual {v4}, Lijn;->a()Z

    move-result v6

    if-nez v6, :cond_d

    iget-object v6, v4, Lijn;->b:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    iget-object v12, v3, Lijn;->b:[I

    array-length v12, v12

    add-int/lit8 v12, v12, -0x1

    sub-int/2addr v6, v12

    iget-object v12, v10, Lijl;->a:Lijm;

    iget-object v13, v4, Lijn;->b:[I

    array-length v13, v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v4, v13}, Lijn;->a(I)I

    move-result v13

    invoke-virtual {v12, v13, v11}, Lijm;->d(II)I

    move-result v12

    iget-object v13, v10, Lijl;->a:Lijm;

    invoke-virtual {v13, v6, v12}, Lijm;->a(II)Lijn;

    move-result-object v13

    invoke-virtual {v5, v13}, Lijn;->a(Lijn;)Lijn;

    move-result-object v5

    if-gez v6, :cond_a

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_a
    if-nez v12, :cond_b

    iget-object v6, v3, Lijn;->a:Lijm;

    iget-object v6, v6, Lijm;->d:Lijn;

    :goto_8
    invoke-virtual {v4, v6}, Lijn;->b(Lijn;)Lijn;

    move-result-object v4

    goto :goto_7

    :cond_b
    iget-object v13, v3, Lijn;->b:[I

    array-length v13, v13

    add-int/2addr v6, v13

    new-array v14, v6, [I

    const/4 v6, 0x0

    :goto_9
    if-ge v6, v13, :cond_c

    iget-object v15, v3, Lijn;->a:Lijm;

    iget-object v0, v3, Lijn;->b:[I

    move-object/from16 v16, v0

    aget v16, v16, v6

    move/from16 v0, v16

    invoke-virtual {v15, v0, v12}, Lijm;->d(II)I

    move-result v15

    aput v15, v14, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    :cond_c
    new-instance v6, Lijn;

    iget-object v12, v3, Lijn;->a:Lijm;

    invoke-direct {v6, v12, v14}, Lijn;-><init>(Lijm;[I)V

    goto :goto_8

    :cond_d
    invoke-virtual {v5, v1}, Lijn;->c(Lijn;)Lijn;

    move-result-object v5

    invoke-virtual {v5, v2}, Lijn;->b(Lijn;)Lijn;

    move-result-object v2

    invoke-virtual {v2}, Lijn;->b()Lijn;

    move-result-object v2

    move-object/from16 v17, v2

    move-object v2, v1

    move-object/from16 v1, v17

    move-object/from16 v18, v4

    move-object v4, v3

    move-object/from16 v3, v18

    goto/16 :goto_6

    :cond_e
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lijn;->a(I)I

    move-result v2

    if-nez v2, :cond_f

    invoke-static {}, Lier;->a()Lier;

    move-result-object v1

    throw v1

    :cond_f
    iget-object v4, v10, Lijl;->a:Lijm;

    invoke-virtual {v4, v2}, Lijm;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lijn;->c(I)Lijn;

    move-result-object v1

    invoke-virtual {v3, v2}, Lijn;->c(I)Lijn;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Lijn;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v2, v3, v1

    const/4 v1, 0x0

    aget-object v1, v3, v1

    const/4 v2, 0x1

    aget-object v2, v3, v2

    invoke-virtual {v10, v1}, Lijl;->a(Lijn;)[I

    move-result-object v3

    invoke-virtual {v10, v2, v1, v3}, Lijl;->a(Lijn;Lijn;[I)[I

    move-result-object v2

    const/4 v1, 0x0

    :goto_a
    array-length v4, v3

    if-ge v1, v4, :cond_12

    move-object/from16 v0, p1

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    iget-object v5, v10, Lijl;->a:Lijm;

    aget v6, v3, v1

    if-nez v6, :cond_10

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_10
    iget-object v5, v5, Lijm;->c:[I

    aget v5, v5, v6

    sub-int/2addr v4, v5

    if-gez v4, :cond_11

    invoke-static {}, Lier;->a()Lier;

    move-result-object v1

    throw v1

    :cond_11
    iget-object v5, v10, Lijl;->a:Lijm;

    aget v6, p1, v4

    aget v11, v2, v1

    invoke-virtual {v5, v6, v11}, Lijm;->c(II)I

    move-result v5

    aput v5, p1, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_12
    array-length v1, v3

    goto/16 :goto_3

    :cond_13
    const/4 v2, 0x0

    aget v2, p1, v2

    move-object/from16 v0, p1

    array-length v3, v0

    if-le v2, v3, :cond_14

    invoke-static {}, Liet;->a()Liet;

    move-result-object v1

    throw v1

    :cond_14
    if-nez v2, :cond_15

    move-object/from16 v0, p1

    array-length v2, v0

    if-ge v7, v2, :cond_16

    const/4 v2, 0x0

    move-object/from16 v0, p1

    array-length v3, v0

    sub-int/2addr v3, v7

    aput v3, p1, v2

    :cond_15
    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lijd;->a([ILjava/lang/String;)Liga;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-object/from16 v0, p2

    array-length v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    return-object v2

    :cond_16
    invoke-static {}, Liet;->a()Liet;

    move-result-object v1

    throw v1
    :try_end_1
    .catch Lier; {:try_start_1 .. :try_end_1} :catch_0

    .line 309
    :cond_17
    const/4 v1, 0x0

    :goto_b
    array-length v2, v9

    if-ge v1, v2, :cond_1a

    .line 310
    aget v2, v9, v1

    aget-object v3, p4, v1

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_18

    .line 311
    aget v2, v9, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v9, v1

    move v1, v8

    .line 312
    goto/16 :goto_0

    .line 314
    :cond_18
    const/4 v2, 0x0

    aput v2, v9, v1

    .line 315
    array-length v2, v9

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_19

    .line 316
    invoke-static {}, Lier;->a()Lier;

    move-result-object v1

    throw v1

    .line 309
    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_1a
    move v1, v8

    goto/16 :goto_0

    .line 321
    :cond_1b
    invoke-static {}, Lier;->a()Lier;

    move-result-object v1

    throw v1

    :cond_1c
    move-object v4, v3

    move-object v3, v1

    goto/16 :goto_5
.end method

.method public static a(Lifx;Lifb;Lifb;Lifb;Lifb;II)Liga;
    .locals 20

    .prologue
    .line 58
    new-instance v1, Lijb;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lijb;-><init>(Lifx;Lifb;Lifb;Lifb;Lifb;)V

    .line 59
    const/4 v10, 0x0

    .line 60
    const/4 v7, 0x0

    .line 61
    const/4 v3, 0x0

    .line 62
    const/4 v2, 0x0

    move v12, v2

    move-object v2, v1

    move-object v1, v3

    move-object v3, v10

    :goto_0
    const/4 v4, 0x2

    if-ge v12, v4, :cond_4f

    .line 63
    if-eqz p1, :cond_4e

    .line 64
    const/4 v4, 0x1

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-static/range {v1 .. v6}, Lijk;->a(Lifx;Lijb;Lifb;ZII)Liji;

    move-result-object v10

    .line 67
    :goto_1
    if-eqz p3, :cond_4d

    .line 68
    const/4 v4, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p3

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-static/range {v1 .. v6}, Lijk;->a(Lifx;Lijb;Lifb;ZII)Liji;

    move-result-object v9

    .line 71
    :goto_2
    if-nez v10, :cond_0

    if-nez v9, :cond_0

    const/4 v1, 0x0

    .line 72
    :goto_3
    if-nez v1, :cond_b

    .line 73
    invoke-static {}, Liew;->a()Liew;

    move-result-object v1

    throw v1

    .line 71
    :cond_0
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Liji;->a()Liiz;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_1
    if-nez v9, :cond_2

    const/4 v1, 0x0

    move-object v11, v1

    :goto_4
    if-nez v11, :cond_8

    const/4 v1, 0x0

    goto :goto_3

    :cond_2
    invoke-virtual {v9}, Liji;->a()Liiz;

    move-result-object v1

    move-object v11, v1

    goto :goto_4

    :cond_3
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Liji;->a()Liiz;

    move-result-object v1

    if-nez v1, :cond_6

    :cond_4
    if-nez v10, :cond_5

    const/4 v1, 0x0

    move-object v11, v1

    goto :goto_4

    :cond_5
    invoke-virtual {v10}, Liji;->a()Liiz;

    move-result-object v1

    move-object v11, v1

    goto :goto_4

    :cond_6
    invoke-virtual {v10}, Liji;->a()Liiz;

    move-result-object v1

    invoke-virtual {v9}, Liji;->a()Liiz;

    move-result-object v3

    iget v4, v1, Liiz;->a:I

    iget v5, v3, Liiz;->a:I

    if-eq v4, v5, :cond_7

    iget v4, v1, Liiz;->b:I

    iget v5, v3, Liiz;->b:I

    if-eq v4, v5, :cond_7

    iget v4, v1, Liiz;->e:I

    iget v3, v3, Liiz;->e:I

    if-eq v4, v3, :cond_7

    const/4 v1, 0x0

    move-object v11, v1

    goto :goto_4

    :cond_7
    move-object v11, v1

    goto :goto_4

    :cond_8
    invoke-static {v10}, Lijk;->a(Liji;)Lijb;

    move-result-object v6

    invoke-static {v9}, Lijk;->a(Liji;)Lijb;

    move-result-object v1

    if-nez v6, :cond_9

    move-object v3, v1

    :goto_5
    new-instance v1, Lijg;

    invoke-direct {v1, v11, v3}, Lijg;-><init>(Liiz;Lijb;)V

    goto :goto_3

    :cond_9
    if-nez v1, :cond_a

    move-object v3, v6

    goto :goto_5

    :cond_a
    new-instance v3, Lijb;

    iget-object v4, v6, Lijb;->a:Lifx;

    iget-object v5, v6, Lijb;->b:Lifb;

    iget-object v6, v6, Lijb;->c:Lifb;

    iget-object v7, v1, Lijb;->d:Lifb;

    iget-object v8, v1, Lijb;->e:Lifb;

    invoke-direct/range {v3 .. v8}, Lijb;-><init>(Lifx;Lifb;Lifb;Lifb;Lifb;)V

    goto :goto_5

    .line 75
    :cond_b
    if-nez v12, :cond_d

    iget-object v3, v1, Lijg;->b:Lijb;

    if-eqz v3, :cond_d

    iget-object v3, v1, Lijg;->b:Lijb;

    iget v3, v3, Lijb;->h:I

    iget v4, v2, Lijb;->h:I

    if-lt v3, v4, :cond_c

    iget-object v3, v1, Lijg;->b:Lijb;

    iget v3, v3, Lijb;->i:I

    iget v4, v2, Lijb;->i:I

    if-le v3, v4, :cond_d

    .line 78
    :cond_c
    iget-object v3, v1, Lijg;->b:Lijb;

    .line 62
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    move-object v7, v9

    move-object v2, v3

    move-object v3, v10

    goto/16 :goto_0

    .line 80
    :cond_d
    iput-object v2, v1, Lijg;->b:Lijb;

    move-object v11, v1

    move-object v3, v10

    move-object v1, v9

    .line 84
    :goto_6
    iget v4, v11, Lijg;->c:I

    add-int/lit8 v16, v4, 0x1

    .line 85
    const/4 v4, 0x0

    iget-object v5, v11, Lijg;->a:[Lijh;

    aput-object v3, v5, v4

    .line 86
    iget-object v4, v11, Lijg;->a:[Lijh;

    aput-object v1, v4, v16

    .line 88
    if-eqz v3, :cond_13

    const/4 v6, 0x1

    .line 89
    :goto_7
    const/4 v14, 0x1

    move/from16 v10, p6

    move/from16 v9, p5

    :goto_8
    move/from16 v0, v16

    if-gt v14, v0, :cond_25

    .line 90
    if-eqz v6, :cond_14

    move v12, v14

    .line 91
    :goto_9
    iget-object v1, v11, Lijg;->a:[Lijh;

    aget-object v1, v1, v12

    if-nez v1, :cond_24

    .line 93
    if-eqz v12, :cond_e

    move/from16 v0, v16

    if-ne v12, v0, :cond_16

    .line 97
    :cond_e
    new-instance v3, Liji;

    if-nez v12, :cond_15

    const/4 v1, 0x1

    :goto_a
    invoke-direct {v3, v2, v1}, Liji;-><init>(Lijb;Z)V

    move-object v15, v3

    .line 101
    :goto_b
    iget-object v1, v11, Lijg;->a:[Lijh;

    aput-object v15, v1, v12

    .line 102
    const/4 v13, -0x1

    .line 105
    iget v8, v2, Lijb;->h:I

    :goto_c
    iget v1, v2, Lijb;->i:I

    if-gt v8, v1, :cond_24

    .line 106
    if-eqz v6, :cond_17

    const/4 v1, 0x1

    :goto_d
    const/4 v3, 0x0

    sub-int v4, v12, v1

    invoke-static {v11, v4}, Lijk;->a(Lijg;I)Z

    move-result v4

    if-eqz v4, :cond_f

    sub-int v3, v12, v1

    iget-object v4, v11, Lijg;->a:[Lijh;

    aget-object v3, v4, v3

    invoke-virtual {v3, v8}, Lijh;->c(I)Lijc;

    move-result-object v3

    :cond_f
    if-eqz v3, :cond_19

    if-eqz v6, :cond_18

    iget v7, v3, Lijc;->b:I

    .line 107
    :goto_e
    if-ltz v7, :cond_10

    iget v1, v2, Lijb;->g:I

    if-le v7, v1, :cond_11

    .line 108
    :cond_10
    const/4 v1, -0x1

    if-eq v13, v1, :cond_12

    move v7, v13

    .line 113
    :cond_11
    iget v4, v2, Lijb;->f:I

    iget v5, v2, Lijb;->g:I

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v10}, Lijk;->a(Lifx;IIZIIII)Lijc;

    move-result-object v1

    .line 115
    if-eqz v1, :cond_12

    .line 116
    invoke-virtual {v15, v8, v1}, Lijh;->a(ILijc;)V

    .line 118
    invoke-virtual {v1}, Lijc;->c()I

    move-result v3

    invoke-static {v9, v3}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 119
    invoke-virtual {v1}, Lijc;->c()I

    move-result v1

    invoke-static {v10, v1}, Ljava/lang/Math;->max(II)I

    move-result v10

    move v13, v7

    .line 105
    :cond_12
    add-int/lit8 v8, v8, 0x1

    goto :goto_c

    .line 88
    :cond_13
    const/4 v6, 0x0

    goto :goto_7

    .line 90
    :cond_14
    sub-int v12, v16, v14

    goto :goto_9

    .line 97
    :cond_15
    const/4 v1, 0x0

    goto :goto_a

    .line 99
    :cond_16
    new-instance v1, Lijh;

    invoke-direct {v1, v2}, Lijh;-><init>(Lijb;)V

    move-object v15, v1

    goto :goto_b

    .line 106
    :cond_17
    const/4 v1, -0x1

    goto :goto_d

    :cond_18
    iget v7, v3, Lijc;->a:I

    goto :goto_e

    :cond_19
    iget-object v3, v11, Lijg;->a:[Lijh;

    aget-object v3, v3, v12

    invoke-virtual {v3, v8}, Lijh;->a(I)Lijc;

    move-result-object v3

    if-eqz v3, :cond_1b

    if-eqz v6, :cond_1a

    iget v7, v3, Lijc;->a:I

    goto :goto_e

    :cond_1a
    iget v7, v3, Lijc;->b:I

    goto :goto_e

    :cond_1b
    sub-int v4, v12, v1

    invoke-static {v11, v4}, Lijk;->a(Lijg;I)Z

    move-result v4

    if-eqz v4, :cond_1c

    sub-int v3, v12, v1

    iget-object v4, v11, Lijg;->a:[Lijh;

    aget-object v3, v4, v3

    invoke-virtual {v3, v8}, Lijh;->a(I)Lijc;

    move-result-object v3

    :cond_1c
    if-eqz v3, :cond_1e

    if-eqz v6, :cond_1d

    iget v7, v3, Lijc;->b:I

    goto :goto_e

    :cond_1d
    iget v7, v3, Lijc;->a:I

    goto :goto_e

    :cond_1e
    const/4 v3, 0x0

    move v4, v3

    move v3, v12

    :goto_f
    sub-int v5, v3, v1

    invoke-static {v11, v5}, Lijk;->a(Lijg;I)Z

    move-result v5

    if-eqz v5, :cond_22

    sub-int v5, v3, v1

    iget-object v3, v11, Lijg;->a:[Lijh;

    aget-object v3, v3, v5

    iget-object v7, v3, Lijh;->b:[Lijc;

    array-length v0, v7

    move/from16 v17, v0

    const/4 v3, 0x0

    :goto_10
    move/from16 v0, v17

    if-ge v3, v0, :cond_21

    aget-object v18, v7, v3

    if-eqz v18, :cond_20

    if-eqz v6, :cond_1f

    move-object/from16 v0, v18

    iget v3, v0, Lijc;->b:I

    :goto_11
    mul-int/2addr v1, v4

    move-object/from16 v0, v18

    iget v4, v0, Lijc;->b:I

    move-object/from16 v0, v18

    iget v5, v0, Lijc;->a:I

    sub-int/2addr v4, v5

    mul-int/2addr v1, v4

    add-int v7, v3, v1

    goto/16 :goto_e

    :cond_1f
    move-object/from16 v0, v18

    iget v3, v0, Lijc;->a:I

    goto :goto_11

    :cond_20
    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_21
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v5

    goto :goto_f

    :cond_22
    if-eqz v6, :cond_23

    iget-object v1, v11, Lijg;->b:Lijb;

    iget v7, v1, Lijb;->f:I

    goto/16 :goto_e

    :cond_23
    iget-object v1, v11, Lijg;->b:Lijb;

    iget v7, v1, Lijb;->g:I

    goto/16 :goto_e

    .line 89
    :cond_24
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_8

    .line 123
    :cond_25
    invoke-virtual {v11}, Lijg;->a()I

    move-result v1

    iget v2, v11, Lijg;->c:I

    add-int/lit8 v2, v2, 0x2

    filled-new-array {v1, v2}, [I

    move-result-object v1

    const-class v2, Lija;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[Lija;

    const/4 v2, 0x0

    :goto_12
    array-length v3, v1

    if-ge v2, v3, :cond_27

    const/4 v3, 0x0

    :goto_13
    aget-object v4, v1, v2

    array-length v4, v4

    if-ge v3, v4, :cond_26

    aget-object v4, v1, v2

    new-instance v5, Lija;

    invoke-direct {v5}, Lija;-><init>()V

    aput-object v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :cond_26
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    :cond_27
    const/4 v7, -0x1

    iget-object v2, v11, Lijg;->a:[Lijh;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v11, v2}, Lijg;->a(Lijh;)V

    iget-object v2, v11, Lijg;->a:[Lijh;

    iget v3, v11, Lijg;->c:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v11, v2}, Lijg;->a(Lijh;)V

    const/16 v2, 0x3a0

    move v6, v2

    :goto_14
    iget-object v2, v11, Lijg;->a:[Lijh;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    if-eqz v2, :cond_28

    iget-object v2, v11, Lijg;->a:[Lijh;

    iget v3, v11, Lijg;->c:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    if-nez v2, :cond_2d

    :cond_28
    iget-object v2, v11, Lijg;->a:[Lijh;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    if-nez v2, :cond_30

    const/4 v4, 0x0

    :cond_29
    iget-object v2, v11, Lijg;->a:[Lijh;

    iget v3, v11, Lijg;->c:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    if-nez v2, :cond_34

    const/4 v3, 0x0

    :cond_2a
    add-int v8, v4, v3

    if-nez v8, :cond_37

    const/4 v2, 0x0

    :goto_15
    if-lez v2, :cond_2b

    if-lt v2, v6, :cond_4b

    :cond_2b
    iget-object v5, v11, Lijg;->a:[Lijh;

    array-length v6, v5

    const/4 v2, 0x0

    move v3, v2

    move v2, v7

    :goto_16
    if-ge v3, v6, :cond_41

    aget-object v7, v5, v3

    add-int/lit8 v4, v2, 0x1

    if-eqz v7, :cond_40

    iget-object v7, v7, Lijh;->b:[Lijc;

    array-length v8, v7

    const/4 v2, 0x0

    :goto_17
    if-ge v2, v8, :cond_40

    aget-object v9, v7, v2

    if-eqz v9, :cond_2c

    iget v10, v9, Lijc;->e:I

    const/4 v12, -0x1

    if-eq v10, v12, :cond_2c

    iget v10, v9, Lijc;->e:I

    aget-object v10, v1, v10

    aget-object v10, v10, v4

    iget v9, v9, Lijc;->d:I

    invoke-virtual {v10, v9}, Lija;->a(I)V

    :cond_2c
    add-int/lit8 v2, v2, 0x1

    goto :goto_17

    :cond_2d
    iget-object v2, v11, Lijg;->a:[Lijh;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v4, v2, Lijh;->b:[Lijc;

    iget-object v2, v11, Lijg;->a:[Lijh;

    iget v3, v11, Lijg;->c:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    iget-object v5, v2, Lijh;->b:[Lijc;

    const/4 v2, 0x0

    move v3, v2

    :goto_18
    array-length v2, v4

    if-ge v3, v2, :cond_28

    aget-object v2, v4, v3

    if-eqz v2, :cond_2f

    aget-object v2, v5, v3

    if-eqz v2, :cond_2f

    aget-object v2, v4, v3

    iget v2, v2, Lijc;->e:I

    aget-object v8, v5, v3

    iget v8, v8, Lijc;->e:I

    if-ne v2, v8, :cond_2f

    const/4 v2, 0x1

    :goto_19
    iget v8, v11, Lijg;->c:I

    if-gt v2, v8, :cond_2f

    iget-object v8, v11, Lijg;->a:[Lijh;

    aget-object v8, v8, v2

    iget-object v8, v8, Lijh;->b:[Lijc;

    aget-object v8, v8, v3

    if-eqz v8, :cond_2e

    aget-object v9, v4, v3

    iget v9, v9, Lijc;->e:I

    iput v9, v8, Lijc;->e:I

    invoke-virtual {v8}, Lijc;->a()Z

    move-result v8

    if-nez v8, :cond_2e

    iget-object v8, v11, Lijg;->a:[Lijh;

    aget-object v8, v8, v2

    iget-object v8, v8, Lijh;->b:[Lijc;

    const/4 v9, 0x0

    aput-object v9, v8, v3

    :cond_2e
    add-int/lit8 v2, v2, 0x1

    goto :goto_19

    :cond_2f
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_18

    :cond_30
    const/4 v4, 0x0

    iget-object v2, v11, Lijg;->a:[Lijh;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v8, v2, Lijh;->b:[Lijc;

    const/4 v2, 0x0

    move v5, v2

    :goto_1a
    array-length v2, v8

    if-ge v5, v2, :cond_29

    aget-object v2, v8, v5

    if-eqz v2, :cond_32

    aget-object v2, v8, v5

    iget v9, v2, Lijc;->e:I

    const/4 v3, 0x0

    const/4 v2, 0x1

    move/from16 v19, v2

    move v2, v3

    move v3, v4

    move/from16 v4, v19

    :goto_1b
    iget v10, v11, Lijg;->c:I

    add-int/lit8 v10, v10, 0x1

    if-ge v4, v10, :cond_33

    const/4 v10, 0x2

    if-ge v2, v10, :cond_33

    iget-object v10, v11, Lijg;->a:[Lijh;

    aget-object v10, v10, v4

    iget-object v10, v10, Lijh;->b:[Lijc;

    aget-object v10, v10, v5

    if-eqz v10, :cond_31

    invoke-static {v9, v2, v10}, Lijg;->a(IILijc;)I

    move-result v2

    invoke-virtual {v10}, Lijc;->a()Z

    move-result v10

    if-nez v10, :cond_31

    add-int/lit8 v3, v3, 0x1

    :cond_31
    add-int/lit8 v4, v4, 0x1

    goto :goto_1b

    :cond_32
    move v3, v4

    :cond_33
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v4, v3

    goto :goto_1a

    :cond_34
    const/4 v3, 0x0

    iget-object v2, v11, Lijg;->a:[Lijh;

    iget v5, v11, Lijg;->c:I

    add-int/lit8 v5, v5, 0x1

    aget-object v2, v2, v5

    iget-object v9, v2, Lijh;->b:[Lijc;

    const/4 v2, 0x0

    move v5, v2

    :goto_1c
    array-length v2, v9

    if-ge v5, v2, :cond_2a

    aget-object v2, v9, v5

    if-eqz v2, :cond_36

    aget-object v2, v9, v5

    iget v10, v2, Lijc;->e:I

    const/4 v8, 0x0

    iget v2, v11, Lijg;->c:I

    add-int/lit8 v2, v2, 0x1

    move/from16 v19, v2

    move v2, v8

    move/from16 v8, v19

    :goto_1d
    if-lez v8, :cond_36

    const/4 v12, 0x2

    if-ge v2, v12, :cond_36

    iget-object v12, v11, Lijg;->a:[Lijh;

    aget-object v12, v12, v8

    iget-object v12, v12, Lijh;->b:[Lijc;

    aget-object v12, v12, v5

    if-eqz v12, :cond_35

    invoke-static {v10, v2, v12}, Lijg;->a(IILijc;)I

    move-result v2

    invoke-virtual {v12}, Lijc;->a()Z

    move-result v12

    if-nez v12, :cond_35

    add-int/lit8 v3, v3, 0x1

    :cond_35
    add-int/lit8 v8, v8, -0x1

    goto :goto_1d

    :cond_36
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1c

    :cond_37
    const/4 v2, 0x1

    move v5, v2

    :goto_1e
    iget v2, v11, Lijg;->c:I

    add-int/lit8 v2, v2, 0x1

    if-ge v5, v2, :cond_3f

    iget-object v2, v11, Lijg;->a:[Lijh;

    aget-object v2, v2, v5

    iget-object v9, v2, Lijh;->b:[Lijc;

    const/4 v2, 0x0

    move v4, v2

    :goto_1f
    array-length v2, v9

    if-ge v4, v2, :cond_3e

    aget-object v2, v9, v4

    if-eqz v2, :cond_3d

    aget-object v2, v9, v4

    invoke-virtual {v2}, Lijc;->a()Z

    move-result v2

    if-nez v2, :cond_3d

    aget-object v10, v9, v4

    iget-object v2, v11, Lijg;->a:[Lijh;

    add-int/lit8 v3, v5, -0x1

    aget-object v2, v2, v3

    iget-object v3, v2, Lijh;->b:[Lijc;

    iget-object v2, v11, Lijg;->a:[Lijh;

    add-int/lit8 v12, v5, 0x1

    aget-object v2, v2, v12

    if-eqz v2, :cond_4c

    iget-object v2, v11, Lijg;->a:[Lijh;

    add-int/lit8 v12, v5, 0x1

    aget-object v2, v2, v12

    iget-object v2, v2, Lijh;->b:[Lijc;

    :goto_20
    const/16 v12, 0xe

    new-array v12, v12, [Lijc;

    const/4 v13, 0x2

    aget-object v14, v3, v4

    aput-object v14, v12, v13

    const/4 v13, 0x3

    aget-object v14, v2, v4

    aput-object v14, v12, v13

    if-lez v4, :cond_38

    const/4 v13, 0x0

    add-int/lit8 v14, v4, -0x1

    aget-object v14, v9, v14

    aput-object v14, v12, v13

    const/4 v13, 0x4

    add-int/lit8 v14, v4, -0x1

    aget-object v14, v3, v14

    aput-object v14, v12, v13

    const/4 v13, 0x5

    add-int/lit8 v14, v4, -0x1

    aget-object v14, v2, v14

    aput-object v14, v12, v13

    :cond_38
    const/4 v13, 0x1

    if-le v4, v13, :cond_39

    const/16 v13, 0x8

    add-int/lit8 v14, v4, -0x2

    aget-object v14, v9, v14

    aput-object v14, v12, v13

    const/16 v13, 0xa

    add-int/lit8 v14, v4, -0x2

    aget-object v14, v3, v14

    aput-object v14, v12, v13

    const/16 v13, 0xb

    add-int/lit8 v14, v4, -0x2

    aget-object v14, v2, v14

    aput-object v14, v12, v13

    :cond_39
    array-length v13, v9

    add-int/lit8 v13, v13, -0x1

    if-ge v4, v13, :cond_3a

    const/4 v13, 0x1

    add-int/lit8 v14, v4, 0x1

    aget-object v14, v9, v14

    aput-object v14, v12, v13

    const/4 v13, 0x6

    add-int/lit8 v14, v4, 0x1

    aget-object v14, v3, v14

    aput-object v14, v12, v13

    const/4 v13, 0x7

    add-int/lit8 v14, v4, 0x1

    aget-object v14, v2, v14

    aput-object v14, v12, v13

    :cond_3a
    array-length v13, v9

    add-int/lit8 v13, v13, -0x2

    if-ge v4, v13, :cond_3b

    const/16 v13, 0x9

    add-int/lit8 v14, v4, 0x2

    aget-object v14, v9, v14

    aput-object v14, v12, v13

    const/16 v13, 0xc

    add-int/lit8 v14, v4, 0x2

    aget-object v3, v3, v14

    aput-object v3, v12, v13

    const/16 v3, 0xd

    add-int/lit8 v13, v4, 0x2

    aget-object v2, v2, v13

    aput-object v2, v12, v3

    :cond_3b
    const/4 v2, 0x0

    :goto_21
    const/16 v3, 0xe

    if-ge v2, v3, :cond_3d

    aget-object v3, v12, v2

    if-eqz v3, :cond_3c

    invoke-virtual {v3}, Lijc;->a()Z

    move-result v13

    if-eqz v13, :cond_3c

    iget v13, v3, Lijc;->c:I

    iget v14, v10, Lijc;->c:I

    if-ne v13, v14, :cond_3c

    iget v3, v3, Lijc;->e:I

    iput v3, v10, Lijc;->e:I

    const/4 v3, 0x1

    :goto_22
    if-nez v3, :cond_3d

    add-int/lit8 v2, v2, 0x1

    goto :goto_21

    :cond_3c
    const/4 v3, 0x0

    goto :goto_22

    :cond_3d
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_1f

    :cond_3e
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_1e

    :cond_3f
    move v2, v8

    goto/16 :goto_15

    :cond_40
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto/16 :goto_16

    :cond_41
    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lija;->a()[I

    move-result-object v2

    iget v3, v11, Lijg;->c:I

    invoke-virtual {v11}, Lijg;->a()I

    move-result v4

    mul-int/2addr v3, v4

    invoke-virtual {v11}, Lijg;->b()I

    move-result v4

    const/4 v5, 0x2

    shl-int v4, v5, v4

    sub-int/2addr v3, v4

    array-length v4, v2

    if-nez v4, :cond_45

    if-lez v3, :cond_42

    const/16 v2, 0x3a0

    if-le v3, v2, :cond_43

    :cond_42
    invoke-static {}, Liew;->a()Liew;

    move-result-object v1

    throw v1

    :cond_43
    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-virtual {v2, v3}, Lija;->a(I)V

    :cond_44
    :goto_23
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11}, Lijg;->a()I

    move-result v2

    iget v3, v11, Lijg;->c:I

    mul-int/2addr v2, v3

    new-array v5, v2, [I

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_24
    invoke-virtual {v11}, Lijg;->a()I

    move-result v3

    if-ge v2, v3, :cond_49

    const/4 v3, 0x0

    :goto_25
    iget v8, v11, Lijg;->c:I

    if-ge v3, v8, :cond_48

    aget-object v8, v1, v2

    add-int/lit8 v9, v3, 0x1

    aget-object v8, v8, v9

    invoke-virtual {v8}, Lija;->a()[I

    move-result-object v8

    iget v9, v11, Lijg;->c:I

    mul-int/2addr v9, v2

    add-int/2addr v9, v3

    array-length v10, v8

    if-nez v10, :cond_46

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :goto_26
    add-int/lit8 v3, v3, 0x1

    goto :goto_25

    :cond_45
    const/4 v4, 0x0

    aget v2, v2, v4

    if-eq v2, v3, :cond_44

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-virtual {v2, v3}, Lija;->a(I)V

    goto :goto_23

    :cond_46
    array-length v10, v8

    const/4 v12, 0x1

    if-ne v10, v12, :cond_47

    const/4 v10, 0x0

    aget v8, v8, v10

    aput v8, v5, v9

    goto :goto_26

    :cond_47
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_26

    :cond_48
    add-int/lit8 v2, v2, 0x1

    goto :goto_24

    :cond_49
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    new-array v3, v1, [[I

    const/4 v1, 0x0

    move v2, v1

    :goto_27
    array-length v1, v3

    if-ge v2, v1, :cond_4a

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    aput-object v1, v3, v2

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_27

    :cond_4a
    invoke-virtual {v11}, Lijg;->b()I

    move-result v1

    invoke-static {v4}, Liiw;->a(Ljava/util/Collection;)[I

    move-result-object v2

    invoke-static {v7}, Liiw;->a(Ljava/util/Collection;)[I

    move-result-object v4

    invoke-static {v1, v5, v2, v4, v3}, Lijk;->a(I[I[I[I[[I)Liga;

    move-result-object v1

    return-object v1

    :cond_4b
    move v6, v2

    goto/16 :goto_14

    :cond_4c
    move-object v2, v3

    goto/16 :goto_20

    :cond_4d
    move-object v9, v7

    goto/16 :goto_2

    :cond_4e
    move-object v10, v3

    goto/16 :goto_1

    :cond_4f
    move-object v11, v1

    move-object v1, v7

    goto/16 :goto_6
.end method

.method private static a(Liji;)Lijb;
    .locals 12

    .prologue
    .line 143
    if-nez p0, :cond_0

    .line 144
    const/4 v0, 0x0

    .line 169
    :goto_0
    return-object v0

    .line 146
    :cond_0
    invoke-virtual {p0}, Liji;->a()Liiz;

    move-result-object v4

    if-nez v4, :cond_2

    const/4 v0, 0x0

    .line 147
    :cond_1
    const/4 v2, -0x1

    array-length v4, v0

    const/4 v1, 0x0

    move v3, v2

    :goto_1
    if-ge v1, v4, :cond_b

    aget v2, v0, v1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_1

    .line 146
    :cond_2
    iget-object v1, p0, Lijh;->a:Lijb;

    iget-boolean v0, p0, Liji;->c:Z

    if-eqz v0, :cond_4

    iget-object v0, v1, Lijb;->b:Lifb;

    :goto_2
    iget-boolean v2, p0, Liji;->c:Z

    if-eqz v2, :cond_5

    iget-object v1, v1, Lijb;->c:Lifb;

    :goto_3
    iget v0, v0, Lifb;->b:F

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Liji;->b(I)I

    move-result v0

    iget v1, v1, Lifb;->b:F

    float-to-int v1, v1

    invoke-virtual {p0, v1}, Liji;->b(I)I

    move-result v5

    sub-int v1, v5, v0

    int-to-float v1, v1

    iget v2, v4, Liiz;->e:I

    int-to-float v2, v2

    div-float v6, v1, v2

    iget-object v7, p0, Lijh;->b:[Lijc;

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    move v11, v0

    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v11

    :goto_4
    if-ge v3, v5, :cond_9

    aget-object v8, v7, v3

    if-eqz v8, :cond_3

    aget-object v8, v7, v3

    invoke-virtual {v8}, Lijc;->b()V

    iget v9, v8, Lijc;->e:I

    sub-int/2addr v9, v2

    if-nez v9, :cond_6

    add-int/lit8 v0, v0, 0x1

    :cond_3
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_4
    iget-object v0, v1, Lijb;->d:Lifb;

    goto :goto_2

    :cond_5
    iget-object v1, v1, Lijb;->e:Lifb;

    goto :goto_3

    :cond_6
    const/4 v10, 0x1

    if-ne v9, v10, :cond_7

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/4 v0, 0x1

    iget v2, v8, Lijc;->e:I

    goto :goto_5

    :cond_7
    iget v9, v8, Lijc;->e:I

    iget v10, v4, Liiz;->e:I

    if-lt v9, v10, :cond_8

    const/4 v8, 0x0

    aput-object v8, v7, v3

    goto :goto_5

    :cond_8
    iget v2, v8, Lijc;->e:I

    const/4 v0, 0x1

    goto :goto_5

    :cond_9
    float-to-double v0, v6

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    iget v0, v4, Liiz;->e:I

    new-array v0, v0, [I

    iget-object v2, p0, Lijh;->b:[Lijc;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_a

    iget v4, v4, Lijc;->e:I

    aget v5, v0, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v0, v4

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 148
    :cond_b
    const/4 v2, 0x0

    .line 149
    array-length v4, v0

    const/4 v1, 0x0

    move v11, v1

    move v1, v2

    move v2, v11

    :goto_7
    if-ge v2, v4, :cond_c

    aget v5, v0, v2

    .line 150
    sub-int v6, v3, v5

    add-int/2addr v1, v6

    .line 151
    if-gtz v5, :cond_c

    .line 152
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 155
    :cond_c
    iget-object v4, p0, Lijh;->b:[Lijc;

    .line 156
    const/4 v2, 0x0

    move v8, v1

    move v1, v2

    :goto_8
    if-lez v8, :cond_d

    aget-object v2, v4, v1

    if-nez v2, :cond_d

    .line 157
    add-int/lit8 v2, v8, -0x1

    .line 156
    add-int/lit8 v1, v1, 0x1

    move v8, v2

    goto :goto_8

    .line 159
    :cond_d
    const/4 v2, 0x0

    .line 160
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    move v11, v1

    move v1, v2

    move v2, v11

    :goto_9
    if-ltz v2, :cond_e

    .line 161
    aget v5, v0, v2

    sub-int v5, v3, v5

    add-int/2addr v1, v5

    .line 162
    aget v5, v0, v2

    if-gtz v5, :cond_e

    .line 163
    add-int/lit8 v2, v2, -0x1

    goto :goto_9

    :cond_e
    move v0, v1

    .line 166
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    move v7, v0

    move v0, v1

    :goto_a
    if-lez v7, :cond_f

    aget-object v1, v4, v0

    if-nez v1, :cond_f

    .line 167
    add-int/lit8 v1, v7, -0x1

    .line 166
    add-int/lit8 v0, v0, -0x1

    move v7, v1

    goto :goto_a

    .line 169
    :cond_f
    iget-object v9, p0, Lijh;->a:Lijb;

    iget-boolean v10, p0, Liji;->c:Z

    iget-object v3, v9, Lijb;->b:Lifb;

    iget-object v6, v9, Lijb;->c:Lifb;

    iget-object v4, v9, Lijb;->d:Lifb;

    iget-object v5, v9, Lijb;->e:Lifb;

    if-lez v8, :cond_17

    if-eqz v10, :cond_12

    iget-object v0, v9, Lijb;->b:Lifb;

    :goto_b
    iget v1, v0, Lifb;->b:F

    float-to-int v1, v1

    sub-int/2addr v1, v8

    if-gez v1, :cond_10

    const/4 v1, 0x0

    :cond_10
    new-instance v2, Lifb;

    iget v0, v0, Lifb;->a:F

    int-to-float v1, v1

    invoke-direct {v2, v0, v1}, Lifb;-><init>(FF)V

    if-eqz v10, :cond_13

    :goto_c
    if-lez v7, :cond_16

    if-eqz v10, :cond_14

    iget-object v0, v9, Lijb;->c:Lifb;

    :goto_d
    iget v1, v0, Lifb;->b:F

    float-to-int v1, v1

    add-int/2addr v1, v7

    iget-object v3, v9, Lijb;->a:Lifx;

    iget v3, v3, Lifx;->b:I

    if-lt v1, v3, :cond_11

    iget-object v1, v9, Lijb;->a:Lifx;

    iget v1, v1, Lifx;->b:I

    add-int/lit8 v1, v1, -0x1

    :cond_11
    new-instance v3, Lifb;

    iget v0, v0, Lifb;->a:F

    int-to-float v1, v1

    invoke-direct {v3, v0, v1}, Lifb;-><init>(FF)V

    if-eqz v10, :cond_15

    :goto_e
    invoke-virtual {v9}, Lijb;->a()V

    new-instance v0, Lijb;

    iget-object v1, v9, Lijb;->a:Lifx;

    invoke-direct/range {v0 .. v5}, Lijb;-><init>(Lifx;Lifb;Lifb;Lifb;Lifb;)V

    goto/16 :goto_0

    :cond_12
    iget-object v0, v9, Lijb;->d:Lifb;

    goto :goto_b

    :cond_13
    move-object v4, v2

    move-object v2, v3

    goto :goto_c

    :cond_14
    iget-object v0, v9, Lijb;->e:Lifb;

    goto :goto_d

    :cond_15
    move-object v5, v3

    move-object v3, v6

    goto :goto_e

    :cond_16
    move-object v3, v6

    goto :goto_e

    :cond_17
    move-object v2, v3

    goto :goto_c
.end method

.method private static a(Lifx;IIZIIII)Lijc;
    .locals 6

    .prologue
    .line 400
    if-eqz p3, :cond_4

    const/4 v0, -0x1

    :goto_0
    const/4 v1, 0x0

    move v2, v1

    move v3, v0

    move v4, p3

    move v0, p4

    :goto_1
    const/4 v1, 0x2

    if-ge v2, v1, :cond_8

    move v1, v0

    :goto_2
    if-eqz v4, :cond_0

    if-ge v1, p1, :cond_1

    :cond_0
    if-nez v4, :cond_6

    if-ge v1, p2, :cond_6

    :cond_1
    invoke-virtual {p0, v1, p5}, Lifx;->a(II)Z

    move-result v0

    if-ne v4, v0, :cond_6

    sub-int v0, p4, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v5, 0x2

    if-le v0, v5, :cond_5

    .line 405
    :goto_3
    const/16 v0, 0x8

    new-array v2, v0, [I

    const/4 v1, 0x0

    if-eqz p3, :cond_9

    const/4 v0, 0x1

    :goto_4
    move v3, v1

    move v4, p4

    move v1, p3

    :goto_5
    if-eqz p3, :cond_2

    if-lt v4, p2, :cond_3

    :cond_2
    if-nez p3, :cond_c

    if-lt v4, p1, :cond_c

    :cond_3
    const/16 v5, 0x8

    if-ge v3, v5, :cond_c

    invoke-virtual {p0, v4, p5}, Lifx;->a(II)Z

    move-result v5

    if-ne v5, v1, :cond_a

    aget v5, v2, v3

    add-int/lit8 v5, v5, 0x1

    aput v5, v2, v3

    add-int/2addr v4, v0

    goto :goto_5

    .line 400
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    add-int v0, v1, v3

    move v1, v0

    goto :goto_2

    :cond_6
    neg-int v3, v3

    if-nez v4, :cond_7

    const/4 v0, 0x1

    :goto_6
    add-int/lit8 v2, v2, 0x1

    move v4, v0

    move v0, v1

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_6

    :cond_8
    move p4, v0

    goto :goto_3

    .line 405
    :cond_9
    const/4 v0, -0x1

    goto :goto_4

    :cond_a
    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_b

    const/4 v1, 0x1

    goto :goto_5

    :cond_b
    const/4 v1, 0x0

    goto :goto_5

    :cond_c
    const/16 v0, 0x8

    if-eq v3, v0, :cond_f

    if-eqz p3, :cond_d

    if-eq v4, p2, :cond_e

    :cond_d
    if-nez p3, :cond_10

    if-ne v4, p1, :cond_10

    :cond_e
    const/4 v0, 0x7

    if-ne v3, v0, :cond_10

    .line 406
    :cond_f
    :goto_7
    if-nez v2, :cond_11

    .line 407
    const/4 v0, 0x0

    .line 447
    :goto_8
    return-object v0

    .line 405
    :cond_10
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_7

    .line 410
    :cond_11
    invoke-static {v2}, Liiw;->a([I)I

    move-result v3

    .line 411
    if-eqz p3, :cond_12

    .line 412
    add-int v0, p4, v3

    move v1, p4

    move p4, v0

    .line 436
    :goto_9
    add-int/lit8 v0, p6, -0x2

    if-gt v0, v3, :cond_14

    add-int/lit8 v0, p7, 0x2

    if-gt v3, v0, :cond_14

    const/4 v0, 0x1

    :goto_a
    if-nez v0, :cond_15

    .line 439
    const/4 v0, 0x0

    goto :goto_8

    .line 414
    :cond_12
    const/4 v0, 0x0

    :goto_b
    array-length v1, v2

    shr-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_13

    .line 415
    aget v1, v2, v0

    .line 416
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v0

    aget v4, v2, v4

    aput v4, v2, v0

    .line 417
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v0

    aput v1, v2, v4

    .line 414
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 420
    :cond_13
    sub-int v0, p4, v3

    move v1, v0

    goto :goto_9

    .line 436
    :cond_14
    const/4 v0, 0x0

    goto :goto_a

    .line 442
    :cond_15
    invoke-static {v2}, Lijj;->a([I)I

    move-result v2

    .line 443
    int-to-long v4, v2

    invoke-static {v4, v5}, Liiw;->a(J)I

    move-result v3

    .line 444
    const/4 v0, -0x1

    if-ne v3, v0, :cond_16

    .line 445
    const/4 v0, 0x0

    goto :goto_8

    .line 447
    :cond_16
    new-instance v0, Lijc;

    invoke-static {v2}, Lijk;->a(I)[I

    move-result-object v2

    const/4 v4, 0x0

    aget v4, v2, v4

    const/4 v5, 0x2

    aget v5, v2, v5

    sub-int/2addr v4, v5

    const/4 v5, 0x4

    aget v5, v2, v5

    add-int/2addr v4, v5

    const/4 v5, 0x6

    aget v2, v2, v5

    sub-int v2, v4, v2

    add-int/lit8 v2, v2, 0x9

    rem-int/lit8 v2, v2, 0x9

    invoke-direct {v0, v1, p4, v2, v3}, Lijc;-><init>(IIII)V

    goto :goto_8
.end method

.method private static a(Lifx;Lijb;Lifb;ZII)Liji;
    .locals 11

    .prologue
    .line 206
    new-instance v10, Liji;

    invoke-direct {v10, p1, p3}, Liji;-><init>(Lijb;Z)V

    .line 208
    const/4 v0, 0x0

    move v9, v0

    :goto_0
    const/4 v0, 0x2

    if-ge v9, v0, :cond_4

    .line 209
    if-nez v9, :cond_1

    const/4 v0, 0x1

    move v8, v0

    .line 210
    :goto_1
    iget v0, p2, Lifb;->a:F

    float-to-int v4, v0

    .line 211
    iget v0, p2, Lifb;->b:F

    float-to-int v5, v0

    :goto_2
    iget v0, p1, Lijb;->i:I

    if-gt v5, v0, :cond_3

    iget v0, p1, Lijb;->h:I

    if-lt v5, v0, :cond_3

    .line 213
    const/4 v1, 0x0

    iget v2, p0, Lifx;->a:I

    move-object v0, p0

    move v3, p3

    move v6, p4

    move/from16 v7, p5

    invoke-static/range {v0 .. v7}, Lijk;->a(Lifx;IIZIIII)Lijc;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {v10, v5, v0}, Liji;->a(ILijc;)V

    .line 217
    if-eqz p3, :cond_2

    .line 218
    iget v4, v0, Lijc;->a:I

    .line 212
    :cond_0
    :goto_3
    add-int/2addr v5, v8

    goto :goto_2

    .line 209
    :cond_1
    const/4 v0, -0x1

    move v8, v0

    goto :goto_1

    .line 220
    :cond_2
    iget v4, v0, Lijc;->b:I

    goto :goto_3

    .line 208
    :cond_3
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 225
    :cond_4
    return-object v10
.end method

.method private static a(Lijg;I)Z
    .locals 1

    .prologue
    .line 350
    if-ltz p1, :cond_0

    iget v0, p0, Lijg;->c:I

    add-int/lit8 v0, v0, 0x1

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(I)[I
    .locals 4

    .prologue
    .line 574
    const/16 v0, 0x8

    new-array v2, v0, [I

    .line 575
    const/4 v1, 0x0

    .line 576
    const/4 v0, 0x7

    .line 578
    :goto_0
    and-int/lit8 v3, p0, 0x1

    if-eq v3, v1, :cond_0

    .line 579
    and-int/lit8 v1, p0, 0x1

    .line 580
    add-int/lit8 v0, v0, -0x1

    .line 581
    if-ltz v0, :cond_1

    .line 582
    :cond_0
    aget v3, v2, v0

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v0

    .line 586
    shr-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 588
    :cond_1
    return-object v2
.end method
