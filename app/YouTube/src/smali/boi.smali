.class public final Lboi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final d:Ljava/util/WeakHashMap;


# instance fields
.field public final a:Lbok;

.field public b:Lboq;

.field public c:Z

.field private final e:Landroid/app/Activity;

.field private final f:Landroid/content/res/Resources;

.field private final g:Landroid/view/LayoutInflater;

.field private final h:Landroid/view/View;

.field private final i:Landroid/app/Dialog;

.field private final j:Landroid/widget/ListView;

.field private final k:I

.field private final l:I

.field private m:I

.field private n:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lboi;->d:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-boolean v2, p0, Lboi;->c:Z

    .line 115
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lboi;->e:Landroid/app/Activity;

    .line 117
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 118
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iput v1, p0, Lboi;->k:I

    .line 119
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Lboi;->l:I

    .line 121
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lboi;->g:Landroid/view/LayoutInflater;

    .line 122
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lboi;->f:Landroid/content/res/Resources;

    .line 124
    new-instance v0, Lbok;

    invoke-direct {v0, p0}, Lbok;-><init>(Lboi;)V

    iput-object v0, p0, Lboi;->a:Lbok;

    .line 125
    iget-object v0, p0, Lboi;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f040048

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lboi;->h:Landroid/view/View;

    .line 126
    iget-object v0, p0, Lboi;->h:Landroid/view/View;

    const v1, 0x7f080145

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lboi;->j:Landroid/widget/ListView;

    .line 127
    iget-object v0, p0, Lboi;->j:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 131
    new-instance v0, Lboj;

    const v1, 0x7f0d012d

    invoke-direct {v0, p0, p1, v1}, Lboj;-><init>(Lboi;Landroid/content/Context;I)V

    iput-object v0, p0, Lboi;->i:Landroid/app/Dialog;

    .line 148
    iget-object v0, p0, Lboi;->i:Landroid/app/Dialog;

    iget-object v1, p0, Lboi;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 149
    iget-object v0, p0, Lboi;->i:Landroid/app/Dialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 151
    iget-object v0, p0, Lboi;->i:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 152
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 153
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 154
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a009c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 157
    const v1, 0x1030002

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 158
    const/16 v1, 0x300

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 161
    iget-object v1, p0, Lboi;->i:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 163
    sget-object v0, Lboi;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    return-void
.end method

.method static synthetic a(Lboi;)Lbok;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lboi;->a:Lbok;

    return-object v0
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 85
    sget-object v0, Lboi;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    .line 86
    invoke-virtual {v0}, Lboi;->b()V

    goto :goto_0

    .line 88
    :cond_0
    return-void
.end method

.method static synthetic b(Lboi;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lboi;->e:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lboi;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lboi;->g:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method public final a(IIILbop;)I
    .locals 3

    .prologue
    .line 228
    if-lez p2, :cond_0

    iget-object v0, p0, Lboi;->f:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 229
    :goto_0
    const/4 v1, -0x1

    iget-object v2, p0, Lboi;->a:Lbok;

    invoke-static {v2, p1, v0, v1, p4}, Lbok;->a(Lbok;ILjava/lang/String;ILbop;)I

    move-result v0

    return v0

    .line 228
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILbop;)I
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lboi;->a:Lbok;

    .line 210
    iget-object v0, v0, Lbok;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, -0x1

    .line 209
    invoke-virtual {p0, v0, p1, v1, p2}, Lboi;->a(IIILbop;)I

    move-result v0

    return v0
.end method

.method public final a(Lboo;Lbop;)I
    .locals 5

    .prologue
    .line 247
    iget-object v0, p0, Lboi;->a:Lbok;

    iget-object v1, p0, Lboi;->a:Lbok;

    .line 248
    iget-object v1, v1, Lbok;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 247
    new-instance v2, Lbol;

    iget v3, v0, Lbok;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, v0, Lbok;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v0, v3, p1, p2}, Lbol;-><init>(Lbok;Ljava/lang/Integer;Lboo;Lbop;)V

    iget-object v3, v0, Lbok;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v1, v0, Lbok;->b:Landroid/util/SparseArray;

    iget-object v3, v2, Lbol;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {v0}, Lbok;->notifyDataSetChanged()V

    iget-object v0, v2, Lbol;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 251
    return v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 278
    iget-object v1, p0, Lboi;->a:Lbok;

    iget-object v0, v1, Lbok;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbol;

    if-eqz v0, :cond_0

    iget-object v2, v1, Lbok;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, v1, Lbok;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    :cond_0
    invoke-virtual {v1}, Lbok;->notifyDataSetChanged()V

    .line 279
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 200
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 202
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lboi;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 317
    const/4 v0, 0x0

    iput-object v0, p0, Lboi;->n:Landroid/view/View;

    .line 318
    iget-object v0, p0, Lboi;->i:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 320
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 173
    iget-boolean v0, p0, Lboi;->c:Z

    if-eqz v0, :cond_2

    .line 174
    iget-object v0, p0, Lboi;->b:Lboq;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lboi;->b:Lboq;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, p0, v1, v2}, Lboq;->a(Lboi;Ljava/lang/Object;Z)V

    .line 177
    :cond_0
    iget-object v0, p0, Lboi;->j:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    .line 178
    iget-object v0, p0, Lboi;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lboi;->a:Lbok;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 180
    :cond_1
    iget-object v0, p0, Lboi;->a:Lbok;

    invoke-virtual {v0}, Lbok;->notifyDataSetChanged()V

    .line 181
    iget-object v0, p0, Lboi;->h:Landroid/view/View;

    iget v1, p0, Lboi;->k:I

    iget v2, p0, Lboi;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lboi;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lboi;->m:I

    .line 182
    iget-object v0, p0, Lboi;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v2, p0, Lboi;->i:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v3, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Lboi;->m:I

    add-int/2addr v3, v4

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    if-le v3, v0, :cond_3

    iget v0, v1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lboi;->m:I

    sub-int/2addr v0, v1

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    :goto_0
    iget-object v0, p0, Lboi;->i:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v0, p0, Lboi;->i:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iput-object p1, p0, Lboi;->n:Landroid/view/View;

    .line 184
    :cond_2
    return-void

    .line 182
    :cond_3
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lboi;->a:Lbok;

    iget-object v0, v0, Lbok;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbol;

    iget-object v0, v0, Lbol;->b:Lbop;

    .line 190
    if-eqz v0, :cond_0

    iget-object v1, p0, Lboi;->n:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 191
    iget-object v1, p0, Lboi;->n:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lbop;->a(Ljava/lang/Object;)V

    .line 192
    invoke-virtual {p0}, Lboi;->b()V

    .line 194
    :cond_0
    return-void
.end method
