.class public final Lhgm;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:Lhxf;

.field public d:Ljava/lang/String;

.field public e:Lhog;

.field public f:Lhgz;

.field private g:Ljava/lang/String;

.field private h:Lhgz;

.field private i:Lhgz;

.field private j:Lhqy;

.field private k:[B


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 50970
    invoke-direct {p0}, Lidf;-><init>()V

    .line 50973
    iput-wide v2, p0, Lhgm;->a:J

    .line 50976
    iput-wide v2, p0, Lhgm;->b:J

    .line 50979
    const-string v0, ""

    iput-object v0, p0, Lhgm;->g:Ljava/lang/String;

    .line 50982
    iput-object v1, p0, Lhgm;->c:Lhxf;

    .line 50985
    const-string v0, ""

    iput-object v0, p0, Lhgm;->d:Ljava/lang/String;

    .line 50988
    iput-object v1, p0, Lhgm;->e:Lhog;

    .line 50991
    iput-object v1, p0, Lhgm;->h:Lhgz;

    .line 50994
    iput-object v1, p0, Lhgm;->i:Lhgz;

    .line 50997
    iput-object v1, p0, Lhgm;->f:Lhgz;

    .line 51000
    iput-object v1, p0, Lhgm;->j:Lhqy;

    .line 51003
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhgm;->k:[B

    .line 50970
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 51105
    const/4 v0, 0x0

    .line 51106
    iget-wide v2, p0, Lhgm;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 51107
    const/4 v0, 0x1

    iget-wide v2, p0, Lhgm;->a:J

    .line 51108
    invoke-static {v0, v2, v3}, Lidd;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 51110
    :cond_0
    iget-wide v2, p0, Lhgm;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 51111
    const/4 v1, 0x2

    iget-wide v2, p0, Lhgm;->b:J

    .line 51112
    invoke-static {v1, v2, v3}, Lidd;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 51114
    :cond_1
    iget-object v1, p0, Lhgm;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 51115
    const/4 v1, 0x3

    iget-object v2, p0, Lhgm;->g:Ljava/lang/String;

    .line 51116
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51118
    :cond_2
    iget-object v1, p0, Lhgm;->c:Lhxf;

    if-eqz v1, :cond_3

    .line 51119
    const/4 v1, 0x4

    iget-object v2, p0, Lhgm;->c:Lhxf;

    .line 51120
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51122
    :cond_3
    iget-object v1, p0, Lhgm;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 51123
    const/4 v1, 0x5

    iget-object v2, p0, Lhgm;->d:Ljava/lang/String;

    .line 51124
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51126
    :cond_4
    iget-object v1, p0, Lhgm;->e:Lhog;

    if-eqz v1, :cond_5

    .line 51127
    const/4 v1, 0x6

    iget-object v2, p0, Lhgm;->e:Lhog;

    .line 51128
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51130
    :cond_5
    iget-object v1, p0, Lhgm;->h:Lhgz;

    if-eqz v1, :cond_6

    .line 51131
    const/4 v1, 0x7

    iget-object v2, p0, Lhgm;->h:Lhgz;

    .line 51132
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51134
    :cond_6
    iget-object v1, p0, Lhgm;->i:Lhgz;

    if-eqz v1, :cond_7

    .line 51135
    const/16 v1, 0x8

    iget-object v2, p0, Lhgm;->i:Lhgz;

    .line 51136
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51138
    :cond_7
    iget-object v1, p0, Lhgm;->f:Lhgz;

    if-eqz v1, :cond_8

    .line 51139
    const/16 v1, 0x9

    iget-object v2, p0, Lhgm;->f:Lhgz;

    .line 51140
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51142
    :cond_8
    iget-object v1, p0, Lhgm;->j:Lhqy;

    if-eqz v1, :cond_9

    .line 51143
    const/16 v1, 0xa

    iget-object v2, p0, Lhgm;->j:Lhqy;

    .line 51144
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51146
    :cond_9
    iget-object v1, p0, Lhgm;->k:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_a

    .line 51147
    const/16 v1, 0xc

    iget-object v2, p0, Lhgm;->k:[B

    .line 51148
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 51150
    :cond_a
    iget-object v1, p0, Lhgm;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51151
    iput v0, p0, Lhgm;->J:I

    .line 51152
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 50966
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhgm;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhgm;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhgm;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhgm;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhgm;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgm;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhgm;->c:Lhxf;

    if-nez v0, :cond_2

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhgm;->c:Lhxf;

    :cond_2
    iget-object v0, p0, Lhgm;->c:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgm;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhgm;->e:Lhog;

    if-nez v0, :cond_3

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhgm;->e:Lhog;

    :cond_3
    iget-object v0, p0, Lhgm;->e:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lhgm;->h:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhgm;->h:Lhgz;

    :cond_4
    iget-object v0, p0, Lhgm;->h:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lhgm;->i:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhgm;->i:Lhgz;

    :cond_5
    iget-object v0, p0, Lhgm;->i:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lhgm;->f:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhgm;->f:Lhgz;

    :cond_6
    iget-object v0, p0, Lhgm;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhgm;->j:Lhqy;

    if-nez v0, :cond_7

    new-instance v0, Lhqy;

    invoke-direct {v0}, Lhqy;-><init>()V

    iput-object v0, p0, Lhgm;->j:Lhqy;

    :cond_7
    iget-object v0, p0, Lhgm;->j:Lhqy;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhgm;->k:[B

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x62 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 51066
    iget-wide v0, p0, Lhgm;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 51067
    const/4 v0, 0x1

    iget-wide v2, p0, Lhgm;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 51069
    :cond_0
    iget-wide v0, p0, Lhgm;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 51070
    const/4 v0, 0x2

    iget-wide v2, p0, Lhgm;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 51072
    :cond_1
    iget-object v0, p0, Lhgm;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 51073
    const/4 v0, 0x3

    iget-object v1, p0, Lhgm;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 51075
    :cond_2
    iget-object v0, p0, Lhgm;->c:Lhxf;

    if-eqz v0, :cond_3

    .line 51076
    const/4 v0, 0x4

    iget-object v1, p0, Lhgm;->c:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51078
    :cond_3
    iget-object v0, p0, Lhgm;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 51079
    const/4 v0, 0x5

    iget-object v1, p0, Lhgm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 51081
    :cond_4
    iget-object v0, p0, Lhgm;->e:Lhog;

    if-eqz v0, :cond_5

    .line 51082
    const/4 v0, 0x6

    iget-object v1, p0, Lhgm;->e:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51084
    :cond_5
    iget-object v0, p0, Lhgm;->h:Lhgz;

    if-eqz v0, :cond_6

    .line 51085
    const/4 v0, 0x7

    iget-object v1, p0, Lhgm;->h:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51087
    :cond_6
    iget-object v0, p0, Lhgm;->i:Lhgz;

    if-eqz v0, :cond_7

    .line 51088
    const/16 v0, 0x8

    iget-object v1, p0, Lhgm;->i:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51090
    :cond_7
    iget-object v0, p0, Lhgm;->f:Lhgz;

    if-eqz v0, :cond_8

    .line 51091
    const/16 v0, 0x9

    iget-object v1, p0, Lhgm;->f:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51093
    :cond_8
    iget-object v0, p0, Lhgm;->j:Lhqy;

    if-eqz v0, :cond_9

    .line 51094
    const/16 v0, 0xa

    iget-object v1, p0, Lhgm;->j:Lhqy;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51096
    :cond_9
    iget-object v0, p0, Lhgm;->k:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_a

    .line 51097
    const/16 v0, 0xc

    iget-object v1, p0, Lhgm;->k:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 51099
    :cond_a
    iget-object v0, p0, Lhgm;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 51101
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 51024
    if-ne p1, p0, :cond_1

    .line 51038
    :cond_0
    :goto_0
    return v0

    .line 51025
    :cond_1
    instance-of v2, p1, Lhgm;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 51026
    :cond_2
    check-cast p1, Lhgm;

    .line 51027
    iget-wide v2, p0, Lhgm;->a:J

    iget-wide v4, p1, Lhgm;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lhgm;->b:J

    iget-wide v4, p1, Lhgm;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhgm;->g:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgm;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 51029
    :goto_1
    iget-object v2, p0, Lhgm;->c:Lhxf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhgm;->c:Lhxf;

    if-nez v2, :cond_3

    .line 51030
    :goto_2
    iget-object v2, p0, Lhgm;->d:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhgm;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 51031
    :goto_3
    iget-object v2, p0, Lhgm;->e:Lhog;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhgm;->e:Lhog;

    if-nez v2, :cond_3

    .line 51032
    :goto_4
    iget-object v2, p0, Lhgm;->h:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhgm;->h:Lhgz;

    if-nez v2, :cond_3

    .line 51033
    :goto_5
    iget-object v2, p0, Lhgm;->i:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhgm;->i:Lhgz;

    if-nez v2, :cond_3

    .line 51034
    :goto_6
    iget-object v2, p0, Lhgm;->f:Lhgz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhgm;->f:Lhgz;

    if-nez v2, :cond_3

    .line 51035
    :goto_7
    iget-object v2, p0, Lhgm;->j:Lhqy;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhgm;->j:Lhqy;

    if-nez v2, :cond_3

    .line 51036
    :goto_8
    iget-object v2, p0, Lhgm;->k:[B

    iget-object v3, p1, Lhgm;->k:[B

    .line 51037
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhgm;->I:Ljava/util/List;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhgm;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 51038
    goto :goto_0

    .line 51027
    :cond_4
    iget-object v2, p0, Lhgm;->g:Ljava/lang/String;

    iget-object v3, p1, Lhgm;->g:Ljava/lang/String;

    .line 51029
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhgm;->c:Lhxf;

    iget-object v3, p1, Lhgm;->c:Lhxf;

    .line 51030
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhgm;->d:Ljava/lang/String;

    iget-object v3, p1, Lhgm;->d:Ljava/lang/String;

    .line 51031
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhgm;->e:Lhog;

    iget-object v3, p1, Lhgm;->e:Lhog;

    .line 51032
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhgm;->h:Lhgz;

    iget-object v3, p1, Lhgm;->h:Lhgz;

    .line 51033
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhgm;->i:Lhgz;

    iget-object v3, p1, Lhgm;->i:Lhgz;

    .line 51034
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhgm;->f:Lhgz;

    iget-object v3, p1, Lhgm;->f:Lhgz;

    .line 51035
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lhgm;->j:Lhqy;

    iget-object v3, p1, Lhgm;->j:Lhqy;

    .line 51036
    invoke-virtual {v2, v3}, Lhqy;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    .line 51037
    :cond_c
    iget-object v2, p0, Lhgm;->I:Ljava/util/List;

    iget-object v3, p1, Lhgm;->I:Ljava/util/List;

    .line 51038
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 51042
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 51044
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhgm;->a:J

    iget-wide v4, p0, Lhgm;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 51045
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhgm;->b:J

    iget-wide v4, p0, Lhgm;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 51046
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgm;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 51047
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgm;->c:Lhxf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 51048
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgm;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 51049
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgm;->e:Lhog;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 51050
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgm;->h:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 51051
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgm;->i:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 51052
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgm;->f:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 51053
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgm;->j:Lhqy;

    if-nez v0, :cond_8

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 51054
    iget-object v2, p0, Lhgm;->k:[B

    if-nez v2, :cond_9

    mul-int/lit8 v2, v0, 0x1f

    .line 51060
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhgm;->I:Ljava/util/List;

    if-nez v2, :cond_a

    :goto_8
    add-int/2addr v0, v1

    .line 51061
    return v0

    .line 51046
    :cond_1
    iget-object v0, p0, Lhgm;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 51047
    :cond_2
    iget-object v0, p0, Lhgm;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_1

    .line 51048
    :cond_3
    iget-object v0, p0, Lhgm;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 51049
    :cond_4
    iget-object v0, p0, Lhgm;->e:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_3

    .line 51050
    :cond_5
    iget-object v0, p0, Lhgm;->h:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_4

    .line 51051
    :cond_6
    iget-object v0, p0, Lhgm;->i:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_5

    .line 51052
    :cond_7
    iget-object v0, p0, Lhgm;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_6

    .line 51053
    :cond_8
    iget-object v0, p0, Lhgm;->j:Lhqy;

    invoke-virtual {v0}, Lhqy;->hashCode()I

    move-result v0

    goto :goto_7

    :cond_9
    move v2, v0

    move v0, v1

    .line 51056
    :goto_9
    iget-object v3, p0, Lhgm;->k:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 51057
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhgm;->k:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 51056
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 51060
    :cond_a
    iget-object v1, p0, Lhgm;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_8
.end method
