.class public abstract Lckz;
.super Landroid/app/Application;
.source "SourceFile"

# interfaces
.implements Lcwy;
.implements Letz;


# instance fields
.field public a:Letc;

.field public b:Z

.field private c:Lcla;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public b()Letc;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Letc;

    invoke-direct {v0, p0}, Letc;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public e()V
    .locals 6

    .prologue
    .line 76
    invoke-virtual {p0}, Lckz;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->a(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lckz;->c:Lcla;

    invoke-virtual {v0}, Lcla;->ar()Lcyc;

    move-result-object v0

    iget-object v1, p0, Lckz;->a:Letc;

    invoke-virtual {v1}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyc;->a(Landroid/content/SharedPreferences;)V

    .line 80
    iget-object v0, p0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-static {p0}, La;->r(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lckz;->b:Z

    .line 83
    iget-boolean v0, p0, Lckz;->b:Z

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lckz;->a:Letc;

    .line 85
    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 86
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "version"

    .line 87
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "device_id"

    .line 88
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "device_key"

    .line 89
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 90
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 93
    :cond_0
    new-instance v0, Lewj;

    invoke-virtual {p0}, Lckz;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lckz;->a:Letc;

    invoke-virtual {v2}, Letc;->i()Levn;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lewj;-><init>(Landroid/content/Context;Levn;)V

    .line 96
    iget-object v0, p0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->c()Lexz;

    move-result-object v0

    iget-object v1, v0, Lexz;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Leyb;

    invoke-direct {v2, v0}, Leyb;-><init>(Lexz;)V

    iget-object v0, v0, Lexz;->d:Leyg;

    invoke-interface {v0}, Leyg;->K()J

    move-result-wide v4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v4, v5, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 97
    iget-object v0, p0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    iget-object v1, p0, Lckz;->a:Letc;

    invoke-virtual {v1}, Letc;->g()Lexr;

    move-result-object v1

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 98
    return-void

    .line 82
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "?"

    return-object v0
.end method

.method public h()Lftg;
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract m()Lcla;
.end method

.method public n()Lcla;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lckz;->c:Lcla;

    return-object v0
.end method

.method public synthetic o()Lezg;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lckz;->n()Lcla;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 32
    invoke-virtual {p0}, Lckz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-boolean v0, p0, Lckz;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lckz;->d:Z

    invoke-virtual {p0}, Lckz;->b()Letc;

    move-result-object v0

    iput-object v0, p0, Lckz;->a:Letc;

    invoke-virtual {p0}, Lckz;->m()Lcla;

    move-result-object v0

    iput-object v0, p0, Lckz;->c:Lcla;

    invoke-virtual {p0}, Lckz;->e()V

    .line 35
    :cond_0
    return-void
.end method

.method public final p()Letc;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lckz;->a:Letc;

    return-object v0
.end method
