.class public final Lba;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/content/Context;

.field b:Ljava/lang/CharSequence;

.field c:Ljava/lang/CharSequence;

.field d:Landroid/app/PendingIntent;

.field e:Landroid/graphics/Bitmap;

.field f:Ljava/lang/CharSequence;

.field g:I

.field h:Z

.field i:Lbk;

.field j:I

.field k:I

.field l:Z

.field m:Ljava/util/ArrayList;

.field n:Z

.field o:I

.field p:I

.field q:Landroid/app/Notification;

.field public r:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 874
    const/4 v0, 0x1

    iput-boolean v0, p0, Lba;->h:Z

    .line 884
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lba;->m:Ljava/util/ArrayList;

    .line 885
    iput-boolean v4, p0, Lba;->n:Z

    .line 888
    iput v4, p0, Lba;->o:I

    .line 889
    iput v4, p0, Lba;->p:I

    .line 892
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lba;->q:Landroid/app/Notification;

    .line 907
    iput-object p1, p0, Lba;->a:Landroid/content/Context;

    .line 910
    iget-object v0, p0, Lba;->q:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 911
    iget-object v0, p0, Lba;->q:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 912
    iput v4, p0, Lba;->g:I

    .line 913
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lba;->r:Ljava/util/ArrayList;

    .line 914
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 1262
    if-eqz p2, :cond_0

    .line 1263
    iget-object v0, p0, Lba;->q:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1267
    :goto_0
    return-void

    .line 1265
    :cond_0
    iget-object v0, p0, Lba;->q:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method

.method protected static e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 1536
    if-nez p0, :cond_1

    .line 1540
    :cond_0
    :goto_0
    return-object p0

    .line 1537
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 1538
    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 1532
    invoke-static {}, Law;->a()Lbc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbc;->a(Lba;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lba;
    .locals 1

    .prologue
    .line 958
    iget-object v0, p0, Lba;->q:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 959
    return-object p0
.end method

.method public final a(IIZ)Lba;
    .locals 1

    .prologue
    .line 1031
    iput p1, p0, Lba;->j:I

    .line 1032
    iput p2, p0, Lba;->k:I

    .line 1033
    const/4 v0, 0x0

    iput-boolean v0, p0, Lba;->l:Z

    .line 1034
    return-object p0
.end method

.method public final a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lba;
    .locals 2

    .prologue
    .line 1432
    iget-object v0, p0, Lba;->m:Ljava/util/ArrayList;

    new-instance v1, Lax;

    invoke-direct {v1, p1, p2, p3}, Lax;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1433
    return-object p0
.end method

.method public final a(J)Lba;
    .locals 1

    .prologue
    .line 921
    iget-object v0, p0, Lba;->q:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 922
    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Lba;
    .locals 0

    .prologue
    .line 1054
    iput-object p1, p0, Lba;->d:Landroid/app/PendingIntent;

    .line 1055
    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)Lba;
    .locals 0

    .prologue
    .line 1118
    iput-object p1, p0, Lba;->e:Landroid/graphics/Bitmap;

    .line 1119
    return-object p0
.end method

.method public final a(Landroid/widget/RemoteViews;)Lba;
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Lba;->q:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 1042
    return-object p0
.end method

.method public final a(Lbk;)Lba;
    .locals 2

    .prologue
    .line 1464
    iget-object v0, p0, Lba;->i:Lbk;

    if-eq v0, p1, :cond_0

    .line 1465
    iput-object p1, p0, Lba;->i:Lbk;

    .line 1466
    iget-object v0, p0, Lba;->i:Lbk;

    if-eqz v0, :cond_0

    .line 1467
    iget-object v0, p0, Lba;->i:Lbk;

    iget-object v1, v0, Lbk;->b:Lba;

    if-eq v1, p0, :cond_0

    iput-object p0, v0, Lbk;->b:Lba;

    iget-object v1, v0, Lbk;->b:Lba;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbk;->b:Lba;

    invoke-virtual {v1, v0}, Lba;->a(Lbk;)Lba;

    .line 1470
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lba;
    .locals 1

    .prologue
    .line 982
    invoke-static {p1}, Lba;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lba;->b:Ljava/lang/CharSequence;

    .line 983
    return-object p0
.end method

.method public final a(Z)Lba;
    .locals 1

    .prologue
    .line 1196
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lba;->a(IZ)V

    .line 1197
    return-object p0
.end method

.method public final b(I)Lba;
    .locals 0

    .prologue
    .line 1481
    iput p1, p0, Lba;->o:I

    .line 1482
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Lba;
    .locals 1

    .prologue
    .line 990
    invoke-static {p1}, Lba;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lba;->c:Ljava/lang/CharSequence;

    .line 991
    return-object p0
.end method

.method public final b(Z)Lba;
    .locals 1

    .prologue
    .line 1216
    const/16 v0, 0x10

    invoke-direct {p0, v0, p1}, Lba;->a(IZ)V

    .line 1217
    return-object p0
.end method

.method public final c(I)Lba;
    .locals 1

    .prologue
    .line 1493
    const/4 v0, 0x1

    iput v0, p0, Lba;->p:I

    .line 1494
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)Lba;
    .locals 1

    .prologue
    .line 1022
    invoke-static {p1}, Lba;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lba;->f:Ljava/lang/CharSequence;

    .line 1023
    return-object p0
.end method

.method public final d(Ljava/lang/CharSequence;)Lba;
    .locals 2

    .prologue
    .line 1099
    iget-object v0, p0, Lba;->q:Landroid/app/Notification;

    invoke-static {p1}, Lba;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 1100
    return-object p0
.end method
