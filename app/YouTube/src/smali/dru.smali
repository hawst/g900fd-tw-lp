.class final Ldru;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private synthetic a:Ldrr;


# direct methods
.method constructor <init>(Ldrr;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Ldru;->a:Ldrr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMdxStateChangedEvent(Ldwx;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Ldru;->a:Ldrr;

    invoke-virtual {v0}, Ldrr;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 179
    :cond_0
    sget-object v0, Ldrs;->a:[I

    iget-object v1, p1, Ldwx;->a:Ldww;

    invoke-virtual {v1}, Ldww;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 187
    iget-object v0, p0, Ldru;->a:Ldrr;

    iget-object v0, v0, Ldrr;->i:Ldyg;

    iget-object v1, p1, Ldwx;->a:Ldww;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Resuming scan in state "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Ldru;->a:Ldrr;

    const/4 v1, 0x0

    iput-boolean v1, v0, Ldrr;->m:Z

    .line 189
    iget-object v0, p0, Ldru;->a:Ldrr;

    invoke-static {v0}, Ldrr;->a(Ldrr;)V

    goto :goto_0

    .line 182
    :pswitch_0
    iget-object v0, p0, Ldru;->a:Ldrr;

    iget-object v0, v0, Ldrr;->i:Ldyg;

    iget-object v1, p1, Ldwx;->a:Ldww;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Pausing scan in state "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Ldru;->a:Ldrr;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldrr;->m:Z

    .line 184
    iget-object v0, p0, Ldru;->a:Ldrr;

    invoke-static {v0}, Ldrr;->a(Ldrr;)V

    goto :goto_0

    .line 179
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final onMdxVolumeChangedEvent(Ldxd;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Ldru;->a:Ldrr;

    iget v1, p1, Ldxd;->a:I

    iput v1, v0, Ldrr;->n:I

    .line 197
    iget-object v0, p0, Ldru;->a:Ldrr;

    invoke-virtual {v0}, Ldrr;->e()V

    .line 198
    return-void
.end method
