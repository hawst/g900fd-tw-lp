.class public abstract Lbmf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldna;


# instance fields
.field public final a:Lgml;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lezj;

.field private e:Lfet;

.field private f:Lcwg;

.field private g:Lftg;

.field private h:Leuk;

.field private i:[B

.field private final j:Ldix;

.field private final k:Lbmh;

.field private final l:I

.field private final m:Lgku;

.field private final n:Ljava/io/File;

.field private final o:Lggn;

.field private p:J

.field private volatile q:Z


# direct methods
.method public constructor <init>(Lgml;Lfet;Lcwg;Lftg;Leuk;Lezj;Lgjm;Ldix;ILgku;Ljava/io/File;Lggn;)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p1, p0, Lbmf;->a:Lgml;

    .line 100
    iput-object p2, p0, Lbmf;->e:Lfet;

    .line 101
    iput-object p3, p0, Lbmf;->f:Lcwg;

    .line 102
    iput-object p4, p0, Lbmf;->g:Lftg;

    .line 103
    iput-object p5, p0, Lbmf;->h:Leuk;

    .line 104
    iput-object p6, p0, Lbmf;->d:Lezj;

    .line 105
    iget-object v0, p7, Lgjm;->a:Ljava/lang/String;

    iput-object v0, p0, Lbmf;->b:Ljava/lang/String;

    .line 106
    invoke-static {p7}, La;->c(Lgjm;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmf;->c:Ljava/lang/String;

    .line 107
    invoke-static {p7}, La;->e(Lgjm;)[B

    move-result-object v0

    iput-object v0, p0, Lbmf;->i:[B

    .line 108
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldix;

    iput-object v0, p0, Lbmf;->j:Ldix;

    .line 109
    iput p9, p0, Lbmf;->l:I

    .line 110
    iput-object p10, p0, Lbmf;->m:Lgku;

    .line 111
    iput-object p11, p0, Lbmf;->n:Ljava/io/File;

    .line 112
    invoke-static {p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggn;

    iput-object v0, p0, Lbmf;->o:Lggn;

    .line 114
    new-instance v0, Lbmh;

    invoke-direct {v0, p0}, Lbmh;-><init>(Lbmf;)V

    iput-object v0, p0, Lbmf;->k:Lbmh;

    .line 115
    iget-object v0, p0, Lbmf;->k:Lbmh;

    iput-object v0, p8, Ldix;->c:Ldiy;

    .line 116
    return-void
.end method

.method private a(Landroid/net/Uri;)J
    .locals 2

    .prologue
    .line 531
    invoke-static {}, Leud;->a()Leud;

    move-result-object v0

    .line 532
    iget-object v1, p0, Lbmf;->m:Lgku;

    invoke-interface {v1, p1, v0}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 534
    :try_start_0
    invoke-virtual {v0}, Leud;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 536
    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
.end method

.method private a(Lfqj;)Lfqj;
    .locals 6

    .prologue
    .line 551
    .line 552
    iget-object v0, p1, Lfqj;->a:Lhgy;

    iget-wide v0, v0, Lhgy;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 553
    new-instance v0, Lfql;

    invoke-direct {v0, p1}, Lfql;-><init>(Lfqj;)V

    .line 554
    iget-object v1, p1, Lfqj;->d:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lbmf;->a(Landroid/net/Uri;)J

    move-result-wide v2

    iget-object v1, v0, Lfql;->a:Lhgy;

    iput-wide v2, v1, Lhgy;->k:J

    .line 555
    iget-object v1, v0, Lfql;->a:Lhgy;

    iget-object v2, v0, Lfql;->b:Landroid/net/Uri$Builder;

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lhgy;->c:Ljava/lang/String;

    new-instance p1, Lfqj;

    iget-object v1, v0, Lfql;->a:Lhgy;

    iget-object v2, v0, Lfql;->c:Ljava/lang/String;

    iget-wide v4, v0, Lfql;->d:J

    invoke-direct {p1, v1, v2, v4, v5}, Lfqj;-><init>(Lhgy;Ljava/lang/String;J)V

    .line 557
    :cond_0
    return-object p1
.end method

.method private a(Lfrf;Lgly;ZLfqy;)Lglx;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 368
    .line 369
    if-eqz p2, :cond_e

    .line 370
    if-eqz p3, :cond_1

    .line 371
    iget-object v0, p2, Lgly;->b:Lglx;

    .line 379
    :goto_0
    if-eqz v0, :cond_3

    .line 380
    iget-object v2, v0, Lglx;->a:Lfqj;

    iget-object v2, v2, Lfqj;->a:Lhgy;

    iget v2, v2, Lhgy;->b:I

    .line 382
    invoke-virtual {p1, v2}, Lfrf;->a(I)Lfqj;

    move-result-object v3

    .line 383
    if-eqz v3, :cond_2

    .line 384
    invoke-direct {p0, v3}, Lbmf;->a(Lfqj;)Lfqj;

    move-result-object v3

    .line 389
    iget-object v4, v3, Lfqj;->a:Lhgy;

    iget-wide v4, v4, Lhgy;->k:J

    iget-object v6, v0, Lglx;->a:Lfqj;

    iget-object v6, v6, Lfqj;->a:Lhgy;

    iget-wide v6, v6, Lhgy;->k:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 390
    iget-object v4, v3, Lfqj;->a:Lhgy;

    iget-wide v4, v4, Lhgy;->j:J

    iget-object v6, v0, Lglx;->a:Lfqj;

    iget-object v6, v6, Lfqj;->a:Lhgy;

    iget-wide v6, v6, Lhgy;->j:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 391
    new-instance v1, Lglx;

    iget-boolean v2, v0, Lglx;->b:Z

    iget-wide v4, v0, Lglx;->c:J

    invoke-direct {v1, v3, v2, v4, v5}, Lglx;-><init>(Lfqj;ZJ)V

    .line 415
    :cond_0
    :goto_1
    return-object v1

    .line 373
    :cond_1
    iget-object v0, p2, Lgly;->a:Lglx;

    goto :goto_0

    .line 396
    :cond_2
    iget-object v0, p0, Lbmf;->a:Lgml;

    iget-object v3, p0, Lbmf;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lgml;->a(Ljava/lang/String;I)Z

    .line 397
    :cond_3
    if-eqz p3, :cond_6

    .line 402
    invoke-static {}, Lfqm;->k()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lfrf;->a(I)Lfqj;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 407
    :goto_2
    if-eqz v0, :cond_0

    .line 411
    new-instance v1, Lglx;

    .line 412
    invoke-direct {p0, v0}, Lbmf;->a(Lfqj;)Lfqj;

    move-result-object v0

    invoke-direct {v1, v0, p3}, Lglx;-><init>(Lfqj;Z)V

    .line 414
    iget-object v0, p0, Lbmf;->a:Lgml;

    iget-object v2, p0, Lbmf;->c:Ljava/lang/String;

    iget-object v3, v1, Lglx;->a:Lfqj;

    invoke-virtual {v0, v2, v3, p3}, Lgml;->a(Ljava/lang/String;Lfqj;Z)Z

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 402
    goto :goto_2

    .line 404
    :cond_6
    iget v4, p0, Lbmf;->l:I

    iget-object v0, p0, Lbmf;->o:Lggn;

    .line 405
    invoke-virtual {v0}, Lggn;->a()Lfrb;

    move-result-object v0

    .line 404
    invoke-virtual {p4, v0}, Lfqy;->a(Lfrb;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p1, Lfrf;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    move v3, v0

    :goto_3
    if-eqz v3, :cond_9

    invoke-static {}, Lfqm;->c()Ljava/util/Set;

    move-result-object v0

    move-object v2, v0

    :goto_4
    if-eqz v3, :cond_a

    iget-object v0, p1, Lfrf;->c:Ljava/util/List;

    :goto_5
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v6, v0, Lfqj;->a:Lhgy;

    iget v6, v6, Lhgy;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_8
    const/4 v0, 0x0

    move v3, v0

    goto :goto_3

    :cond_9
    invoke-static {}, Lfqm;->l()Ljava/util/Set;

    move-result-object v0

    move-object v2, v0

    goto :goto_4

    :cond_a
    iget-object v0, p1, Lfrf;->b:Ljava/util/List;

    goto :goto_5

    :cond_b
    new-instance v0, Lfrg;

    invoke-direct {v0, p1}, Lfrg;-><init>(Lfrf;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget v3, v3, Lhgy;->g:I

    if-gt v3, v4, :cond_c

    goto/16 :goto_2

    :cond_d
    move-object v0, v1

    goto/16 :goto_2

    :cond_e
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(Lglx;JJ)V
    .locals 6

    .prologue
    .line 428
    invoke-virtual {p1}, Lglx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    :goto_0
    return-void

    .line 434
    :cond_0
    iget-object v0, p1, Lglx;->a:Lfqj;

    iget-object v0, v0, Lfqj;->a:Lhgy;

    iget-wide v0, v0, Lhgy;->k:J

    iget-wide v2, p1, Lglx;->c:J

    sub-long/2addr v0, v2

    .line 435
    iget-object v2, p0, Lbmf;->j:Ldix;

    iget-object v3, v2, Ldix;->a:Legc;

    if-eqz v3, :cond_1

    iget-object v3, v2, Ldix;->b:Ljava/io/File;

    invoke-static {v3}, La;->f(Ljava/io/File;)J

    move-result-wide v4

    sub-long v0, v4, v0

    iget-object v2, v2, Ldix;->e:Lcyc;

    invoke-interface {v2}, Lcyc;->R()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    .line 436
    new-instance v0, Legf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Legf;-><init>(Ljava/io/IOException;)V

    throw v0

    .line 435
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 440
    :cond_2
    iget-object v0, p0, Lbmf;->b:Ljava/lang/String;

    iget-object v1, p1, Lglx;->a:Lfqj;

    iget-object v1, v1, Lfqj;->a:Lhgy;

    iget v1, v1, Lhgy;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] start stream<"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "> download"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 442
    iget-object v0, p0, Lbmf;->j:Ldix;

    iget-object v1, p1, Lglx;->a:Lfqj;

    const-wide/16 v2, 0x0

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Ldix;->a(Lfqj;JJ)V

    goto :goto_0
.end method

.method private b(J)V
    .locals 11

    .prologue
    .line 488
    iget-wide v0, p0, Lbmf;->p:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 521
    :cond_0
    :goto_0
    return-void

    .line 492
    :cond_1
    iget-object v0, p0, Lbmf;->n:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 493
    const/4 v1, 0x0

    .line 498
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lbmf;->n:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "offline.log"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    :try_start_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s,%s,%s\n"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lbmf;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 504
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lbmf;->d:Lezj;

    .line 505
    invoke-virtual {v5}, Lezj;->a()J

    move-result-wide v6

    iget-wide v8, p0, Lbmf;->p:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 500
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 507
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 511
    :try_start_2
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 517
    :catch_0
    move-exception v0

    goto :goto_0

    .line 511
    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_0

    .line 514
    :try_start_3
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 517
    :catch_2
    move-exception v0

    goto/16 :goto_0

    .line 511
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 514
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 517
    :cond_2
    :goto_3
    throw v0

    :catch_3
    move-exception v1

    goto :goto_3

    .line 511
    :catchall_1
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 333
    iget v0, p0, Lbmf;->l:I

    sget-object v1, Lflj;->a:Lflj;

    iget v1, v1, Lflj;->d:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 134
    iget-object v1, p0, Lbmf;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] cancel"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->e(Ljava/lang/String;)V

    .line 135
    iput-boolean v0, p0, Lbmf;->q:Z

    .line 136
    and-int/lit16 v1, p1, 0x80

    if-nez v1, :cond_0

    .line 137
    :goto_0
    iget-object v1, p0, Lbmf;->j:Ldix;

    invoke-virtual {v1, v0}, Ldix;->a(Z)V

    .line 138
    return-void

    .line 136
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract a(J)V
.end method

.method protected abstract a(JJ)V
.end method

.method protected abstract a(Lfrl;)V
.end method

.method protected abstract a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V
.end method

.method protected abstract a()Z
.end method

.method protected abstract b()V
.end method

.method public final run()V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 120
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 122
    :try_start_0
    iget-object v0, p0, Lbmf;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "pudl task["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] start"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lbmf;->h:Leuk;

    iget-object v1, p0, Lbmf;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Leuk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrl;

    invoke-virtual {p0}, Lbmf;->a()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_8

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbmf;->e:Lfet;

    invoke-virtual {v0}, Lfet;->a()Lftj;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lftj;->m:Z

    iget-object v1, p0, Lbmf;->i:[B

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbmf;->i:[B

    invoke-virtual {v0, v1}, Lftj;->a([B)V

    :goto_0
    iget-object v1, p0, Lbmf;->c:Ljava/lang/String;

    iput-object v1, v0, Lftj;->a:Ljava/lang/String;

    iget-object v1, p0, Lbmf;->f:Lcwg;

    invoke-virtual {v1, v0}, Lcwg;->a(Lftj;)V

    iget-object v1, p0, Lbmf;->g:Lftg;

    invoke-interface {v1, v0}, Lftg;->a(Lftj;)V

    iget-object v1, p0, Lbmf;->e:Lfet;

    invoke-virtual {v1, v0}, Lfet;->a(Lftj;)Lfrl;
    :try_end_1
    .catch Lfdv; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :try_start_2
    invoke-virtual {p0, v0}, Lbmf;->a(Lfrl;)V
    :try_end_2
    .catch Lbmg; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    invoke-virtual {v0}, Lfrl;->g()Lflo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lflo;->a()Z

    move-result v4

    if-nez v4, :cond_4

    :cond_1
    iget-object v2, p0, Lbmf;->b:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v0, "null"

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x33

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] received actionable playability error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    const-string v0, "Playability error"

    const/4 v1, 0x0

    sget-object v2, Lglv;->h:Lglv;

    invoke-virtual {p0, v0, v1, v2}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 130
    :goto_2
    return-void

    .line 122
    :cond_2
    :try_start_4
    sget-object v1, Lfhy;->a:[B

    invoke-virtual {v0, v1}, Lftj;->a([B)V
    :try_end_4
    .catch Lfdv; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_5
    iget-object v1, p0, Lbmf;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] failed to retrieve player response"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "Cannot retrieve player response from the server."

    sget-object v2, Lglv;->g:Lglv;

    invoke-virtual {p0, v1, v0, v2}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 129
    :catch_1
    move-exception v0

    .line 124
    iget-object v1, p0, Lbmf;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] error while pinning video"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 125
    const-string v1, "Error encountered while pinning the video"

    sget-object v2, Lglv;->d:Lglv;

    invoke-virtual {p0, v1, v0, v2}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V

    goto :goto_2

    .line 122
    :catch_2
    move-exception v0

    :try_start_6
    iget-object v1, p0, Lbmf;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] failed to save player response."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->b(Ljava/lang/String;)V

    const-string v1, "Fail to save playerResponse"

    sget-object v2, Lglv;->d:Lglv;

    invoke-virtual {p0, v1, v0, v2}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V

    goto/16 :goto_2

    :catch_3
    move-exception v0

    const-string v1, "Error trying to write to local disk."

    sget-object v2, Lglv;->e:Lglv;

    invoke-virtual {p0, v1, v0, v2}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V

    goto/16 :goto_2

    :cond_3
    iget v0, v1, Lflo;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    invoke-virtual {v0}, Lfrl;->q()Lflg;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lflg;->a()Z

    move-result v4

    if-nez v4, :cond_7

    :cond_5
    iget-object v2, p0, Lbmf;->b:Ljava/lang/String;

    if-nez v1, :cond_6

    const-string v0, "null"

    :goto_3
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] received offline state error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    const-string v0, "Offline state error"

    const/4 v1, 0x0

    sget-object v2, Lglv;->h:Lglv;

    invoke-virtual {p0, v0, v1, v2}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V

    goto/16 :goto_2

    :cond_6
    iget v0, v1, Lflg;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_3

    :cond_7
    iget-object v1, p0, Lbmf;->h:Leuk;

    iget-object v4, p0, Lbmf;->c:Ljava/lang/String;

    invoke-interface {v1, v4, v0}, Leuk;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_8
    iget-object v1, v0, Lfrl;->d:Lfrf;

    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lbmf;->p:J
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :try_start_7
    invoke-virtual {v0}, Lfrl;->i()Lfqy;

    move-result-object v0

    iget-object v4, p0, Lbmf;->a:Lgml;

    iget-object v5, p0, Lbmf;->c:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lgml;->a(Ljava/lang/String;Lgni;)Lgly;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v1, v4, v5, v0}, Lbmf;->a(Lfrf;Lgly;ZLfqy;)Lglx;

    move-result-object v5

    invoke-direct {p0}, Lbmf;->c()Z

    move-result v6

    if-nez v6, :cond_9

    if-nez v5, :cond_9

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No valid video stream to offline."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_4
    move-exception v0

    :goto_4
    :try_start_8
    instance-of v1, v0, Ldjo;

    if-eqz v1, :cond_10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lglv;->g:Lglv;

    invoke-virtual {p0, v1, v0, v4}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :goto_5
    :try_start_9
    invoke-direct {p0, v2, v3}, Lbmf;->b(J)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    goto/16 :goto_2

    :cond_9
    const/4 v6, 0x1

    :try_start_a
    invoke-direct {p0, v1, v4, v6, v0}, Lbmf;->a(Lfrf;Lgly;ZLfqy;)Lglx;

    move-result-object v0

    new-instance v8, Lgly;

    invoke-direct {v8, v5, v0}, Lgly;-><init>(Lglx;Lglx;)V

    invoke-virtual {v8}, Lgly;->d()J

    move-result-wide v0

    invoke-virtual {v8}, Lgly;->e()J
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_7
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-wide v6

    :try_start_b
    iget-object v2, p0, Lbmf;->k:Lbmh;

    iput-wide v6, v2, Lbmh;->b:J

    invoke-virtual {p0, v6, v7}, Lbmf;->a(J)V

    invoke-virtual {p0, v0, v1, v6, v7}, Lbmf;->a(JJ)V

    iget-object v0, p0, Lbmf;->d:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lbmf;->p:J

    iget-object v0, p0, Lbmf;->k:Lbmh;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lbmh;->a:J

    iget-object v1, v8, Lgly;->a:Lglx;

    invoke-direct {p0}, Lbmf;->c()Z

    move-result v0

    if-nez v0, :cond_a

    if-nez v1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No valid video stream to offline."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_5
    move-exception v0

    move-wide v2, v6

    goto :goto_4

    :cond_a
    if-eqz v1, :cond_b

    const-wide/16 v2, 0x0

    iget-object v0, v1, Lglx;->a:Lfqj;

    iget-object v0, v0, Lfqj;->a:Lhgy;

    iget-wide v4, v0, Lhgy;->k:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lbmf;->a(Lglx;JJ)V

    iget-object v0, v1, Lglx;->a:Lfqj;

    iget-object v0, v0, Lfqj;->a:Lhgy;

    iget-wide v0, v0, Lhgy;->k:J

    iget-object v2, p0, Lbmf;->k:Lbmh;

    iput-wide v0, v2, Lbmh;->a:J

    :cond_b
    iget-boolean v0, p0, Lbmf;->q:Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    if-eqz v0, :cond_c

    :try_start_c
    invoke-direct {p0, v6, v7}, Lbmf;->b(J)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    goto/16 :goto_2

    :cond_c
    :try_start_d
    iget-object v1, v8, Lgly;->b:Lglx;

    invoke-direct {p0}, Lbmf;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    if-nez v1, :cond_d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No valid audio stream to offline."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :catch_6
    move-exception v0

    :goto_6
    :try_start_e
    iget-object v1, p0, Lbmf;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x29

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] error while downloading video"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "Error encountered while downloading the video"

    sget-object v2, Lglv;->e:Lglv;

    invoke-virtual {p0, v1, v0, v2}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :try_start_f
    invoke-direct {p0, v6, v7}, Lbmf;->b(J)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_2

    :cond_d
    if-eqz v1, :cond_e

    const-wide/16 v2, 0x0

    :try_start_10
    iget-object v0, v1, Lglx;->a:Lfqj;

    iget-object v0, v0, Lfqj;->a:Lhgy;

    iget-wide v4, v0, Lhgy;->k:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lbmf;->a(Lglx;JJ)V

    :cond_e
    iget-boolean v0, p0, Lbmf;->q:Z
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    if-eqz v0, :cond_f

    :try_start_11
    invoke-direct {p0, v6, v7}, Lbmf;->b(J)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_1

    goto/16 :goto_2

    :cond_f
    :try_start_12
    invoke-virtual {p0, v6, v7, v6, v7}, Lbmf;->a(JJ)V

    invoke-virtual {p0}, Lbmf;->b()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_12} :catch_6
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :try_start_13
    invoke-direct {p0, v6, v7}, Lbmf;->b(J)V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_1

    goto/16 :goto_2

    :cond_10
    :try_start_14
    instance-of v1, v0, Lefo;

    if-nez v1, :cond_11

    instance-of v1, v0, Ljava/net/SocketTimeoutException;

    if-eqz v1, :cond_12

    :cond_11
    const-string v1, "Error trying to read from network."

    sget-object v4, Lglv;->g:Lglv;

    invoke-virtual {p0, v1, v0, v4}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_5

    :catchall_0
    move-exception v0

    move-wide v6, v2

    :goto_7
    :try_start_15
    invoke-direct {p0, v6, v7}, Lbmf;->b(J)V

    throw v0
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_1

    :cond_12
    :try_start_16
    instance-of v1, v0, Lefl;

    if-eqz v1, :cond_13

    const-string v1, "Error trying to read from local disk."

    sget-object v4, Lglv;->f:Lglv;

    invoke-virtual {p0, v1, v0, v4}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V

    goto/16 :goto_5

    :cond_13
    instance-of v1, v0, Legf;

    if-eqz v1, :cond_14

    const-string v1, "Error trying to write to local disk."

    sget-object v4, Lglv;->e:Lglv;

    invoke-virtual {p0, v1, v0, v4}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V

    goto/16 :goto_5

    :cond_14
    const-string v1, "Error trying to download video for offline."

    sget-object v4, Lglv;->d:Lglv;

    invoke-virtual {p0, v1, v0, v4}, Lbmf;->a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto/16 :goto_5

    :catchall_1
    move-exception v0

    goto :goto_7

    :catch_7
    move-exception v0

    move-wide v6, v2

    goto/16 :goto_6
.end method
