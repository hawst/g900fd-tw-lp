.class final Lgdl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lecu;


# instance fields
.field a:Lger;

.field b:Lfqj;

.field c:Lfqj;

.field private synthetic d:Lgdj;


# direct methods
.method constructor <init>(Lgdj;)V
    .locals 0

    .prologue
    .line 1298
    iput-object p1, p0, Lgdl;->d:Lgdj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 7

    .prologue
    .line 1397
    iget-object v0, p0, Lgdl;->d:Lgdj;

    invoke-static {v0}, Lgdj;->b(Lgdj;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgdl;->b:Lfqj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdl;->c:Lfqj;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgdl;->d:Lgdj;

    .line 1398
    invoke-static {v0}, Lgdj;->b(Lgdj;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgdl;->c:Lfqj;

    if-eqz v0, :cond_2

    .line 1399
    :cond_1
    iget-object v0, p0, Lgdl;->d:Lgdj;

    invoke-static {v0}, Lgdj;->c(Lgdj;)Lgei;

    move-result-object v0

    iget-object v1, p0, Lgdl;->b:Lfqj;

    iget-object v2, p0, Lgdl;->c:Lfqj;

    iget-object v3, p0, Lgdl;->a:Lger;

    .line 1402
    iget-object v3, v3, Lger;->c:Lfqj;

    iget-object v4, p0, Lgdl;->a:Lger;

    .line 1403
    iget-object v4, v4, Lger;->d:[Lfre;

    iget-object v5, p0, Lgdl;->a:Lger;

    .line 1404
    iget-object v5, v5, Lger;->e:[Ljava/lang/String;

    move v6, p1

    .line 1399
    invoke-interface/range {v0 .. v6}, Lgei;->a(Lfqj;Lfqj;Lfqj;[Lfre;[Ljava/lang/String;I)V

    .line 1407
    :cond_2
    return-void
.end method

.method public final a(ILjava/lang/String;I)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 1371
    iget-object v0, p0, Lgdl;->d:Lgdj;

    iget-object v0, v0, Lgdj;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lgdl;->d:Lgdj;

    .line 1372
    invoke-static {v0}, Lgdj;->a(Lgdj;)Lfrf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdl;->a:Lger;

    if-nez v0, :cond_1

    .line 1394
    :cond_0
    :goto_0
    return-void

    .line 1376
    :cond_1
    packed-switch p3, :pswitch_data_0

    move v1, v2

    .line 1377
    :goto_1
    if-eq v1, v2, :cond_0

    .line 1381
    iget-object v0, p0, Lgdl;->d:Lgdj;

    .line 1382
    invoke-static {v0}, Lgdj;->a(Lgdj;)Lfrf;

    move-result-object v0

    .line 1383
    invoke-static {p2}, Lfrk;->a(Ljava/lang/String;)I

    move-result v2

    .line 1384
    invoke-static {p2}, Lfrk;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1382
    iget-object v0, v0, Lfrf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v5, v0, Lfqj;->a:Lhgy;

    iget v5, v5, Lhgy;->b:I

    if-ne v5, v2, :cond_2

    iget-object v5, v0, Lfqj;->a:Lhgy;

    iget-object v5, v5, Lhgy;->q:Ljava/lang/String;

    invoke-static {v5, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1381
    :goto_2
    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    .line 1386
    invoke-virtual {v0}, Lfqj;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1387
    iput-object v0, p0, Lgdl;->b:Lfqj;

    .line 1393
    :goto_3
    invoke-virtual {p0, v1}, Lgdl;->a(I)V

    goto :goto_0

    .line 1376
    :pswitch_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x1

    move v1, v0

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x2

    move v1, v0

    goto :goto_1

    .line 1382
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1388
    :cond_4
    invoke-virtual {v0}, Lfqj;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1389
    iput-object v0, p0, Lgdl;->c:Lfqj;

    goto :goto_3

    .line 1376
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
