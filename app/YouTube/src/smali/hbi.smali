.class public final Lhbi;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhbi;


# instance fields
.field public b:Licu;

.field public c:Lhxb;

.field public d:Lhnx;

.field private e:Lhlr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15242
    const/4 v0, 0x0

    new-array v0, v0, [Lhbi;

    sput-object v0, Lhbi;->a:[Lhbi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15243
    invoke-direct {p0}, Lidf;-><init>()V

    .line 15246
    iput-object v0, p0, Lhbi;->b:Licu;

    .line 15249
    iput-object v0, p0, Lhbi;->e:Lhlr;

    .line 15252
    iput-object v0, p0, Lhbi;->c:Lhxb;

    .line 15255
    iput-object v0, p0, Lhbi;->d:Lhnx;

    .line 15243
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 15310
    const/4 v0, 0x0

    .line 15311
    iget-object v1, p0, Lhbi;->b:Licu;

    if-eqz v1, :cond_0

    .line 15312
    const v0, 0x3084dbb

    iget-object v1, p0, Lhbi;->b:Licu;

    .line 15313
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15315
    :cond_0
    iget-object v1, p0, Lhbi;->e:Lhlr;

    if-eqz v1, :cond_1

    .line 15316
    const v1, 0x308ffc6

    iget-object v2, p0, Lhbi;->e:Lhlr;

    .line 15317
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15319
    :cond_1
    iget-object v1, p0, Lhbi;->c:Lhxb;

    if-eqz v1, :cond_2

    .line 15320
    const v1, 0x30905d8

    iget-object v2, p0, Lhbi;->c:Lhxb;

    .line 15321
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15323
    :cond_2
    iget-object v1, p0, Lhbi;->d:Lhnx;

    if-eqz v1, :cond_3

    .line 15324
    const v1, 0x396214a

    iget-object v2, p0, Lhbi;->d:Lhnx;

    .line 15325
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15327
    :cond_3
    iget-object v1, p0, Lhbi;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15328
    iput v0, p0, Lhbi;->J:I

    .line 15329
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 15239
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbi;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbi;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbi;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhbi;->b:Licu;

    if-nez v0, :cond_2

    new-instance v0, Licu;

    invoke-direct {v0}, Licu;-><init>()V

    iput-object v0, p0, Lhbi;->b:Licu;

    :cond_2
    iget-object v0, p0, Lhbi;->b:Licu;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhbi;->e:Lhlr;

    if-nez v0, :cond_3

    new-instance v0, Lhlr;

    invoke-direct {v0}, Lhlr;-><init>()V

    iput-object v0, p0, Lhbi;->e:Lhlr;

    :cond_3
    iget-object v0, p0, Lhbi;->e:Lhlr;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhbi;->c:Lhxb;

    if-nez v0, :cond_4

    new-instance v0, Lhxb;

    invoke-direct {v0}, Lhxb;-><init>()V

    iput-object v0, p0, Lhbi;->c:Lhxb;

    :cond_4
    iget-object v0, p0, Lhbi;->c:Lhxb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhbi;->d:Lhnx;

    if-nez v0, :cond_5

    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lhbi;->d:Lhnx;

    :cond_5
    iget-object v0, p0, Lhbi;->d:Lhnx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18426dda -> :sswitch_1
        0x1847fe32 -> :sswitch_2
        0x18482ec2 -> :sswitch_3
        0x1cb10a52 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 15292
    iget-object v0, p0, Lhbi;->b:Licu;

    if-eqz v0, :cond_0

    .line 15293
    const v0, 0x3084dbb

    iget-object v1, p0, Lhbi;->b:Licu;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 15295
    :cond_0
    iget-object v0, p0, Lhbi;->e:Lhlr;

    if-eqz v0, :cond_1

    .line 15296
    const v0, 0x308ffc6

    iget-object v1, p0, Lhbi;->e:Lhlr;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 15298
    :cond_1
    iget-object v0, p0, Lhbi;->c:Lhxb;

    if-eqz v0, :cond_2

    .line 15299
    const v0, 0x30905d8

    iget-object v1, p0, Lhbi;->c:Lhxb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 15301
    :cond_2
    iget-object v0, p0, Lhbi;->d:Lhnx;

    if-eqz v0, :cond_3

    .line 15302
    const v0, 0x396214a

    iget-object v1, p0, Lhbi;->d:Lhnx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 15304
    :cond_3
    iget-object v0, p0, Lhbi;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 15306
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 15269
    if-ne p1, p0, :cond_1

    .line 15276
    :cond_0
    :goto_0
    return v0

    .line 15270
    :cond_1
    instance-of v2, p1, Lhbi;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 15271
    :cond_2
    check-cast p1, Lhbi;

    .line 15272
    iget-object v2, p0, Lhbi;->b:Licu;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbi;->b:Licu;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhbi;->e:Lhlr;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbi;->e:Lhlr;

    if-nez v2, :cond_3

    .line 15273
    :goto_2
    iget-object v2, p0, Lhbi;->c:Lhxb;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhbi;->c:Lhxb;

    if-nez v2, :cond_3

    .line 15274
    :goto_3
    iget-object v2, p0, Lhbi;->d:Lhnx;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhbi;->d:Lhnx;

    if-nez v2, :cond_3

    .line 15275
    :goto_4
    iget-object v2, p0, Lhbi;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhbi;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 15276
    goto :goto_0

    .line 15272
    :cond_4
    iget-object v2, p0, Lhbi;->b:Licu;

    iget-object v3, p1, Lhbi;->b:Licu;

    invoke-virtual {v2, v3}, Licu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbi;->e:Lhlr;

    iget-object v3, p1, Lhbi;->e:Lhlr;

    .line 15273
    invoke-virtual {v2, v3}, Lhlr;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhbi;->c:Lhxb;

    iget-object v3, p1, Lhbi;->c:Lhxb;

    .line 15274
    invoke-virtual {v2, v3}, Lhxb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhbi;->d:Lhnx;

    iget-object v3, p1, Lhbi;->d:Lhnx;

    .line 15275
    invoke-virtual {v2, v3}, Lhnx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhbi;->I:Ljava/util/List;

    iget-object v3, p1, Lhbi;->I:Ljava/util/List;

    .line 15276
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 15280
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 15282
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbi;->b:Licu;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 15283
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbi;->e:Lhlr;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 15284
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbi;->c:Lhxb;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 15285
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbi;->d:Lhnx;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 15286
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbi;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 15287
    return v0

    .line 15282
    :cond_0
    iget-object v0, p0, Lhbi;->b:Licu;

    invoke-virtual {v0}, Licu;->hashCode()I

    move-result v0

    goto :goto_0

    .line 15283
    :cond_1
    iget-object v0, p0, Lhbi;->e:Lhlr;

    invoke-virtual {v0}, Lhlr;->hashCode()I

    move-result v0

    goto :goto_1

    .line 15284
    :cond_2
    iget-object v0, p0, Lhbi;->c:Lhxb;

    invoke-virtual {v0}, Lhxb;->hashCode()I

    move-result v0

    goto :goto_2

    .line 15285
    :cond_3
    iget-object v0, p0, Lhbi;->d:Lhnx;

    invoke-virtual {v0}, Lhnx;->hashCode()I

    move-result v0

    goto :goto_3

    .line 15286
    :cond_4
    iget-object v1, p0, Lhbi;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
