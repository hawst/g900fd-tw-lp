.class public final Lhio;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhio;


# instance fields
.field public b:Lhhj;

.field public c:Lhhk;

.field public d:Lhhm;

.field public e:Lhhn;

.field private f:Lheb;

.field private g:Lhdv;

.field private h:Lhds;

.field private i:Lhdz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhio;

    sput-object v0, Lhio;->a:[Lhio;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v0, p0, Lhio;->f:Lheb;

    iput-object v0, p0, Lhio;->g:Lhdv;

    iput-object v0, p0, Lhio;->h:Lhds;

    iput-object v0, p0, Lhio;->i:Lhdz;

    iput-object v0, p0, Lhio;->b:Lhhj;

    iput-object v0, p0, Lhio;->c:Lhhk;

    iput-object v0, p0, Lhio;->d:Lhhm;

    iput-object v0, p0, Lhio;->e:Lhhn;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhio;->f:Lheb;

    if-eqz v1, :cond_0

    const v0, 0x3049143

    iget-object v1, p0, Lhio;->f:Lheb;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhio;->g:Lhdv;

    if-eqz v1, :cond_1

    const v1, 0x3064567

    iget-object v2, p0, Lhio;->g:Lhdv;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhio;->h:Lhds;

    if-eqz v1, :cond_2

    const v1, 0x3070f41

    iget-object v2, p0, Lhio;->h:Lhds;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhio;->i:Lhdz;

    if-eqz v1, :cond_3

    const v1, 0x32b52b9

    iget-object v2, p0, Lhio;->i:Lhdz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lhio;->b:Lhhj;

    if-eqz v1, :cond_4

    const v1, 0x3993a2b

    iget-object v2, p0, Lhio;->b:Lhhj;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhio;->c:Lhhk;

    if-eqz v1, :cond_5

    const v1, 0x3993a43

    iget-object v2, p0, Lhio;->c:Lhhk;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhio;->d:Lhhm;

    if-eqz v1, :cond_6

    const v1, 0x3993a4a

    iget-object v2, p0, Lhio;->d:Lhhm;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhio;->e:Lhhn;

    if-eqz v1, :cond_7

    const v1, 0x3993a79

    iget-object v2, p0, Lhio;->e:Lhhn;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lhio;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhio;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhio;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhio;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhio;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhio;->f:Lheb;

    if-nez v0, :cond_2

    new-instance v0, Lheb;

    invoke-direct {v0}, Lheb;-><init>()V

    iput-object v0, p0, Lhio;->f:Lheb;

    :cond_2
    iget-object v0, p0, Lhio;->f:Lheb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhio;->g:Lhdv;

    if-nez v0, :cond_3

    new-instance v0, Lhdv;

    invoke-direct {v0}, Lhdv;-><init>()V

    iput-object v0, p0, Lhio;->g:Lhdv;

    :cond_3
    iget-object v0, p0, Lhio;->g:Lhdv;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhio;->h:Lhds;

    if-nez v0, :cond_4

    new-instance v0, Lhds;

    invoke-direct {v0}, Lhds;-><init>()V

    iput-object v0, p0, Lhio;->h:Lhds;

    :cond_4
    iget-object v0, p0, Lhio;->h:Lhds;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhio;->i:Lhdz;

    if-nez v0, :cond_5

    new-instance v0, Lhdz;

    invoke-direct {v0}, Lhdz;-><init>()V

    iput-object v0, p0, Lhio;->i:Lhdz;

    :cond_5
    iget-object v0, p0, Lhio;->i:Lhdz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhio;->b:Lhhj;

    if-nez v0, :cond_6

    new-instance v0, Lhhj;

    invoke-direct {v0}, Lhhj;-><init>()V

    iput-object v0, p0, Lhio;->b:Lhhj;

    :cond_6
    iget-object v0, p0, Lhio;->b:Lhhj;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhio;->c:Lhhk;

    if-nez v0, :cond_7

    new-instance v0, Lhhk;

    invoke-direct {v0}, Lhhk;-><init>()V

    iput-object v0, p0, Lhio;->c:Lhhk;

    :cond_7
    iget-object v0, p0, Lhio;->c:Lhhk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lhio;->d:Lhhm;

    if-nez v0, :cond_8

    new-instance v0, Lhhm;

    invoke-direct {v0}, Lhhm;-><init>()V

    iput-object v0, p0, Lhio;->d:Lhhm;

    :cond_8
    iget-object v0, p0, Lhio;->d:Lhhm;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhio;->e:Lhhn;

    if-nez v0, :cond_9

    new-instance v0, Lhhn;

    invoke-direct {v0}, Lhhn;-><init>()V

    iput-object v0, p0, Lhio;->e:Lhhn;

    :cond_9
    iget-object v0, p0, Lhio;->e:Lhhn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18248a1a -> :sswitch_1
        0x18322b3a -> :sswitch_2
        0x18387a0a -> :sswitch_3
        0x195a95ca -> :sswitch_4
        0x1cc9d15a -> :sswitch_5
        0x1cc9d21a -> :sswitch_6
        0x1cc9d252 -> :sswitch_7
        0x1cc9d3ca -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget-object v0, p0, Lhio;->f:Lheb;

    if-eqz v0, :cond_0

    const v0, 0x3049143

    iget-object v1, p0, Lhio;->f:Lheb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhio;->g:Lhdv;

    if-eqz v0, :cond_1

    const v0, 0x3064567

    iget-object v1, p0, Lhio;->g:Lhdv;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhio;->h:Lhds;

    if-eqz v0, :cond_2

    const v0, 0x3070f41

    iget-object v1, p0, Lhio;->h:Lhds;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhio;->i:Lhdz;

    if-eqz v0, :cond_3

    const v0, 0x32b52b9

    iget-object v1, p0, Lhio;->i:Lhdz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhio;->b:Lhhj;

    if-eqz v0, :cond_4

    const v0, 0x3993a2b

    iget-object v1, p0, Lhio;->b:Lhhj;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Lhio;->c:Lhhk;

    if-eqz v0, :cond_5

    const v0, 0x3993a43

    iget-object v1, p0, Lhio;->c:Lhhk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v0, p0, Lhio;->d:Lhhm;

    if-eqz v0, :cond_6

    const v0, 0x3993a4a

    iget-object v1, p0, Lhio;->d:Lhhm;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_6
    iget-object v0, p0, Lhio;->e:Lhhn;

    if-eqz v0, :cond_7

    const v0, 0x3993a79

    iget-object v1, p0, Lhio;->e:Lhhn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_7
    iget-object v0, p0, Lhio;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhio;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhio;

    iget-object v2, p0, Lhio;->f:Lheb;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhio;->f:Lheb;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhio;->g:Lhdv;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhio;->g:Lhdv;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhio;->h:Lhds;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhio;->h:Lhds;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhio;->i:Lhdz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhio;->i:Lhdz;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhio;->b:Lhhj;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhio;->b:Lhhj;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhio;->c:Lhhk;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhio;->c:Lhhk;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhio;->d:Lhhm;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhio;->d:Lhhm;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Lhio;->e:Lhhn;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhio;->e:Lhhn;

    if-nez v2, :cond_3

    :goto_8
    iget-object v2, p0, Lhio;->I:Ljava/util/List;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhio;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhio;->f:Lheb;

    iget-object v3, p1, Lhio;->f:Lheb;

    invoke-virtual {v2, v3}, Lheb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhio;->g:Lhdv;

    iget-object v3, p1, Lhio;->g:Lhdv;

    invoke-virtual {v2, v3}, Lhdv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhio;->h:Lhds;

    iget-object v3, p1, Lhio;->h:Lhds;

    invoke-virtual {v2, v3}, Lhds;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhio;->i:Lhdz;

    iget-object v3, p1, Lhio;->i:Lhdz;

    invoke-virtual {v2, v3}, Lhdz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhio;->b:Lhhj;

    iget-object v3, p1, Lhio;->b:Lhhj;

    invoke-virtual {v2, v3}, Lhhj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhio;->c:Lhhk;

    iget-object v3, p1, Lhio;->c:Lhhk;

    invoke-virtual {v2, v3}, Lhhk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhio;->d:Lhhm;

    iget-object v3, p1, Lhio;->d:Lhhm;

    invoke-virtual {v2, v3}, Lhhm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lhio;->e:Lhhn;

    iget-object v3, p1, Lhio;->e:Lhhn;

    invoke-virtual {v2, v3}, Lhhn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Lhio;->I:Ljava/util/List;

    iget-object v3, p1, Lhio;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhio;->f:Lheb;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhio;->g:Lhdv;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhio;->h:Lhds;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhio;->i:Lhdz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhio;->b:Lhhj;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhio;->c:Lhhk;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhio;->d:Lhhm;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhio;->e:Lhhn;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhio;->I:Ljava/util/List;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lhio;->f:Lheb;

    invoke-virtual {v0}, Lheb;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhio;->g:Lhdv;

    invoke-virtual {v0}, Lhdv;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhio;->h:Lhds;

    invoke-virtual {v0}, Lhds;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhio;->i:Lhdz;

    invoke-virtual {v0}, Lhdz;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lhio;->b:Lhhj;

    invoke-virtual {v0}, Lhhj;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lhio;->c:Lhhk;

    invoke-virtual {v0}, Lhhk;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    iget-object v0, p0, Lhio;->d:Lhhm;

    invoke-virtual {v0}, Lhhm;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_7
    iget-object v0, p0, Lhio;->e:Lhhn;

    invoke-virtual {v0}, Lhhn;->hashCode()I

    move-result v0

    goto :goto_7

    :cond_8
    iget-object v1, p0, Lhio;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_8
.end method
