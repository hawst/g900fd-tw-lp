.class public final Lepx;
.super Lelv;


# static fields
.field private static final a:Lepx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lepx;

    invoke-direct {v0}, Lepx;-><init>()V

    sput-object v0, Lepx;->a:Lepx;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.ads.adshield.AdShieldCreatorImpl"

    invoke-direct {p0, v0}, Lelv;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Lepy;
    .locals 2

    :try_start_0
    sget-object v0, Lepx;->a:Lepx;

    invoke-static {p1}, Lelu;->a(Ljava/lang/Object;)Lelr;

    move-result-object v1

    invoke-virtual {v0, p1}, Lepx;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqe;

    invoke-interface {v0, p0, v1}, Leqe;->a(Ljava/lang/String;Lelr;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lepz;->a(Landroid/os/IBinder;)Lepy;
    :try_end_0
    .catch Lelw; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    new-instance v0, Lepw;

    invoke-direct {v0, p0, p1}, Lepw;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Leqf;->a(Landroid/os/IBinder;)Leqe;

    move-result-object v0

    return-object v0
.end method
