.class public final Limd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Limb;

.field public final b:Lima;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Lilr;

.field public final f:Lils;

.field public final g:Limf;

.field public h:Limd;

.field public i:Limd;

.field final j:Limd;

.field private volatile k:Lill;


# direct methods
.method constructor <init>(Lime;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iget-object v0, p1, Lime;->a:Limb;

    iput-object v0, p0, Limd;->a:Limb;

    .line 51
    iget-object v0, p1, Lime;->b:Lima;

    iput-object v0, p0, Limd;->b:Lima;

    .line 52
    iget v0, p1, Lime;->c:I

    iput v0, p0, Limd;->c:I

    .line 53
    iget-object v0, p1, Lime;->d:Ljava/lang/String;

    iput-object v0, p0, Limd;->d:Ljava/lang/String;

    .line 54
    iget-object v0, p1, Lime;->e:Lilr;

    iput-object v0, p0, Limd;->e:Lilr;

    .line 55
    iget-object v0, p1, Lime;->f:Lilt;

    invoke-virtual {v0}, Lilt;->a()Lils;

    move-result-object v0

    iput-object v0, p0, Limd;->f:Lils;

    .line 56
    iget-object v0, p1, Lime;->g:Limf;

    iput-object v0, p0, Limd;->g:Limf;

    .line 57
    iget-object v0, p1, Lime;->h:Limd;

    iput-object v0, p0, Limd;->h:Limd;

    .line 58
    iget-object v0, p1, Lime;->i:Limd;

    iput-object v0, p0, Limd;->i:Limd;

    .line 59
    iget-object v0, p1, Lime;->j:Limd;

    iput-object v0, p0, Limd;->j:Limd;

    .line 60
    return-void
.end method


# virtual methods
.method public final a()Lime;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lime;

    invoke-direct {v0, p0}, Lime;-><init>(Limd;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Limd;->f:Lils;

    invoke-virtual {v0, p1}, Lils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 2

    .prologue
    .line 188
    iget v0, p0, Limd;->c:I

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    .line 189
    const-string v0, "WWW-Authenticate"

    .line 195
    :goto_0
    iget-object v1, p0, Limd;->f:Lils;

    invoke-static {v1, v0}, Ling;->b(Lils;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    :goto_1
    return-object v0

    .line 190
    :cond_0
    iget v0, p0, Limd;->c:I

    const/16 v1, 0x197

    if-ne v0, v1, :cond_1

    .line 191
    const-string v0, "Proxy-Authenticate"

    goto :goto_0

    .line 193
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method public final c()Lill;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Limd;->k:Lill;

    .line 204
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Limd;->f:Lils;

    invoke-static {v0}, Lill;->a(Lils;)Lill;

    move-result-object v0

    iput-object v0, p0, Limd;->k:Lill;

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 208
    iget-object v0, p0, Limd;->b:Lima;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Limd;->c:I

    iget-object v2, p0, Limd;->d:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Limd;->a:Limb;

    iget-object v3, v3, Limb;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x35

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Response{protocol="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", code="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
