.class final Laxb;
.super Laxa;
.source "SourceFile"


# instance fields
.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Laxa;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 107
    iget-object v0, p0, Laxb;->b:Landroid/view/View;

    const v1, 0x7f080131

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laxb;->d:Landroid/widget/TextView;

    .line 108
    iget-object v0, p0, Laxb;->b:Landroid/view/View;

    const v1, 0x7f080132

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laxb;->e:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Laxb;->b:Landroid/view/View;

    const v1, 0x7f080106

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laxb;->f:Landroid/view/View;

    .line 110
    iget-object v0, p0, Laxb;->b:Landroid/view/View;

    const v1, 0x7f08012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laxb;->g:Landroid/widget/TextView;

    .line 111
    return-void
.end method


# virtual methods
.method public final a(ILgcd;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f07009b

    const/16 v4, 0x8

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 115
    iget-object v0, p0, Laxb;->d:Landroid/widget/TextView;

    iget-object v3, p2, Lgcd;->p:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Laxb;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 117
    if-nez p1, :cond_2

    .line 118
    iget-object v0, p0, Laxb;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 124
    :cond_0
    :goto_0
    iget-object v0, p2, Lgcd;->B:Lgch;

    sget-object v3, Lgch;->a:Lgch;

    if-eq v0, v3, :cond_1

    iget-object v0, p2, Lgcd;->B:Lgch;

    sget-object v3, Lgch;->b:Lgch;

    if-ne v0, v3, :cond_3

    .line 125
    invoke-virtual {p2}, Lgcd;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    .line 127
    :goto_1
    if-eqz v0, :cond_5

    .line 128
    iget-object v0, p0, Laxb;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 129
    iget-object v0, p0, Laxb;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Laxb;->e:Landroid/widget/TextView;

    iget-object v3, p0, Laxb;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 131
    iget-object v0, p0, Laxb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 132
    iget-object v0, p2, Lgcd;->r:Ljava/util/Date;

    iget-object v3, p0, Laxb;->a:Landroid/content/res/Resources;

    invoke-static {v0, v3}, La;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 133
    iget-object v3, p0, Laxb;->e:Landroid/widget/TextView;

    iget-object v4, p0, Laxb;->a:Landroid/content/res/Resources;

    const v5, 0x7f100014

    iget-wide v6, p2, Lgcd;->l:J

    long-to-int v6, v6

    new-array v7, v8, [Ljava/lang/Object;

    if-eqz v0, :cond_4

    :goto_2
    aput-object v0, v7, v1

    iget-wide v0, p2, Lgcd;->l:J

    .line 138
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v2

    .line 134
    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Laxb;->c:Landroid/widget/TextView;

    iget-object v1, p0, Laxb;->a:Landroid/content/res/Resources;

    const v2, 0x7f07009c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 140
    iget-object v0, p0, Laxb;->e:Landroid/widget/TextView;

    iget-object v1, p0, Laxb;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 150
    :goto_3
    invoke-super {p0, p1, p2}, Laxa;->a(ILgcd;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 120
    :cond_2
    iget-object v0, p0, Laxb;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 125
    goto :goto_1

    .line 133
    :cond_4
    const-string v0, ""

    goto :goto_2

    .line 142
    :cond_5
    iget-object v0, p0, Laxb;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 143
    iget-object v0, p0, Laxb;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Laxb;->e:Landroid/widget/TextView;

    iget-object v1, p0, Laxb;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 145
    iget-object v0, p0, Laxb;->e:Landroid/widget/TextView;

    iget-object v1, p0, Laxb;->a:Landroid/content/res/Resources;

    iget-object v2, p2, Lgcd;->B:Lgch;

    iget v2, v2, Lgch;->u:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Laxb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 147
    iget-object v0, p0, Laxb;->c:Landroid/widget/TextView;

    iget-object v1, p0, Laxb;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 148
    iget-object v0, p0, Laxb;->e:Landroid/widget/TextView;

    iget-object v1, p0, Laxb;->a:Landroid/content/res/Resources;

    const v2, 0x7f07009c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3
.end method

.method public final bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 96
    check-cast p2, Lgcd;

    invoke-virtual {p0, p1, p2}, Laxb;->a(ILgcd;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
