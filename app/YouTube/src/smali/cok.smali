.class public Lcok;
.super Lghl;
.source "SourceFile"


# static fields
.field private static final b:Landroid/net/Uri;

.field private static final c:Landroid/net/Uri;


# instance fields
.field public final a:Landroid/content/SharedPreferences;

.field private final d:Lgix;

.field private final j:Lgku;

.field private final k:Lgjp;

.field private final l:Lcyc;

.field private final m:Lggr;

.field private final n:Landroid/net/ConnectivityManager;

.field private final o:Landroid/telephony/TelephonyManager;

.field private final p:Leuc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-string v0, "https://www.youtube.com/leanback_ajax?action_environment=1"

    .line 58
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcok;->b:Landroid/net/Uri;

    .line 59
    const-string v0, "https://www.youtube-nocookie.com/device_204"

    .line 60
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcok;->c:Landroid/net/Uri;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lezj;Landroid/content/SharedPreferences;Lgix;Lcyc;Lggr;Lgjp;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0, p2, p3, p4}, Lghl;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lezj;)V

    .line 329
    new-instance v0, Lcol;

    invoke-direct {v0, p0}, Lcol;-><init>(Lcok;)V

    iput-object v0, p0, Lcok;->p:Leuc;

    .line 86
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    const-string v0, "connectivity"

    .line 88
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcok;->n:Landroid/net/ConnectivityManager;

    .line 89
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcok;->o:Landroid/telephony/TelephonyManager;

    .line 91
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    .line 92
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lcok;->d:Lgix;

    .line 93
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyc;

    iput-object v0, p0, Lcok;->l:Lcyc;

    .line 94
    const-string v0, "deviceClassification cannot be null"

    invoke-static {p8, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggr;

    iput-object v0, p0, Lcok;->m:Lggr;

    .line 97
    new-instance v0, Lcom;

    invoke-direct {v0, p0}, Lcom;-><init>(Lcok;)V

    sget-object v1, Lgik;->a:Lgik;

    invoke-virtual {p0, v1, v0}, Lcok;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcok;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lcok;->j:Lgku;

    .line 98
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lcok;->k:Lgjp;

    .line 99
    return-void
.end method

.method static synthetic a(Lcok;Ljava/lang/String;J)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const-wide/16 v10, 0x0

    .line 45
    iget-object v0, p0, Lcok;->d:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v3

    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v4, "dev_retention_first_geo"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "dev_retention_first_geo"

    invoke-interface {v0, v4, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "dev_retention_first_active"

    invoke-interface {v0, v4, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    if-eqz v3, :cond_1

    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v4, "dev_retention_first_login"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "dev_retention_first_login"

    invoke-interface {v0, v4, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    sget-object v0, Lcok;->c:Landroid/net/Uri;

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v4

    const-string v5, "app_anon_id"

    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_uuid"

    invoke-interface {v0, v6, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcok;->a:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "dev_retention_uuid"

    invoke-interface {v6, v7, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    invoke-virtual {v4, v5, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v4

    const-string v0, "firstactive"

    iget-object v5, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_first_active"

    invoke-interface {v5, v6, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v5, "firstactivegeo"

    iget-object v6, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v7, "dev_retention_first_geo"

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v5, "dev_retention_first_login"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "firstlogin"

    iget-object v5, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_first_login"

    invoke-interface {v5, v6, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    :cond_3
    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v5, "dev_retention_prev_active"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "prevactive"

    iget-object v5, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_prev_active"

    invoke-interface {v5, v6, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    :cond_4
    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v5, "dev_retention_prev_login"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "prevlogin"

    iget-object v5, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_prev_login"

    invoke-interface {v5, v6, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    :cond_5
    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v5, "dev_retention_intercepted_url"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "intercepted_url"

    iget-object v5, p0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_intercepted_url"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v5, "dev_retention_intercepted_url"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_6
    const-string v5, "loginstate"

    if-eqz v3, :cond_a

    const-string v0, "1"

    :goto_1
    invoke-virtual {v4, v5, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    iget-object v0, p0, Lcok;->m:Lggr;

    invoke-virtual {v0, v4}, Lggr;->a(Lfao;)Lfao;

    iget-object v0, p0, Lcok;->n:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    if-ne v5, v2, :cond_b

    const-string v0, "wifi"

    :goto_2
    if-eqz v0, :cond_7

    const-string v1, "cnetwork"

    invoke-virtual {v4, v1, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    :cond_7
    iget-object v0, v4, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x10

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Retention ping: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcok;->k:Lgjp;

    const-string v1, "drp"

    const v4, 0x323467f

    invoke-static {v1, v4}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v1

    invoke-virtual {v1, v0}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    iput-boolean v2, v1, Lgjt;->d:Z

    iget-object v0, p0, Lcok;->k:Lgjp;

    sget-object v2, Lggu;->b:Lwu;

    invoke-virtual {v0, v1, v2}, Lgjp;->a(Lgjt;Lwu;)V

    iget-object v0, p0, Lcok;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dev_retention_prev_active"

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    if-eqz v3, :cond_8

    const-string v1, "dev_retention_prev_login"

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_8
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_a
    const-string v0, "0"

    goto :goto_1

    :cond_b
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    if-eqz v5, :cond_c

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    const/4 v6, 0x4

    if-eq v5, v6, :cond_c

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    const/4 v6, 0x5

    if-eq v5, v6, :cond_c

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_c

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_e

    :cond_c
    iget-object v0, p0, Lcok;->o:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcok;->o:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_d
    const-string v0, "mobile"

    goto/16 :goto_2

    :cond_e
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    const/4 v6, 0x7

    if-ne v5, v6, :cond_f

    const-string v0, "bluetooth"

    goto/16 :goto_2

    :cond_f
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    const/16 v6, 0x9

    if-ne v5, v6, :cond_10

    const-string v0, "ethernet"

    goto/16 :goto_2

    :cond_10
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v5, 0x6

    if-ne v0, v5, :cond_11

    const-string v0, "wimax"

    goto/16 :goto_2

    :cond_11
    move-object v0, v1

    goto/16 :goto_2
.end method


# virtual methods
.method public a(J)V
    .locals 7

    .prologue
    .line 180
    iget-object v0, p0, Lcok;->f:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    .line 181
    sub-long v2, v0, p1

    const-wide/32 v4, 0xdbba00

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v2, p0, Lcok;->l:Lcyc;

    invoke-interface {v2}, Lcyc;->s()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 187
    iget-object v2, p0, Lcok;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "dev_retention_last_ping_time_ms"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 188
    iget-object v0, p0, Lcok;->j:Lgku;

    sget-object v1, Lcok;->b:Landroid/net/Uri;

    iget-object v2, p0, Lcok;->p:Leuc;

    invoke-interface {v0, v1, v2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    goto :goto_0
.end method
