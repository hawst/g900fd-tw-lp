.class public final Lahg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldfv;


# instance fields
.field public a:Lang;


# direct methods
.method public constructor <init>(Lang;)V
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    const-string v0, "service cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lang;

    iput-object v0, p0, Lahg;->a:Lang;

    .line 245
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lahg;->a:Lang;

    if-eqz v0, :cond_0

    .line 267
    :try_start_0
    iget-object v0, p0, Lahg;->a:Lang;

    invoke-interface {v0}, Lang;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    :cond_0
    return-void

    .line 268
    :catch_0
    move-exception v0

    .line 270
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lahg;->a:Lang;

    if-eqz v0, :cond_0

    .line 255
    :try_start_0
    iget-object v0, p0, Lahg;->a:Lang;

    invoke-interface {v0, p1}, Lang;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :cond_0
    return-void

    .line 256
    :catch_0
    move-exception v0

    .line 258
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lahg;->a:Lang;

    if-eqz v0, :cond_0

    .line 279
    :try_start_0
    iget-object v0, p0, Lahg;->a:Lang;

    invoke-interface {v0}, Lang;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :cond_0
    return-void

    .line 280
    :catch_0
    move-exception v0

    .line 282
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lahg;->a:Lang;

    if-eqz v0, :cond_0

    .line 291
    :try_start_0
    iget-object v0, p0, Lahg;->a:Lang;

    invoke-interface {v0}, Lang;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :cond_0
    return-void

    .line 292
    :catch_0
    move-exception v0

    .line 294
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lahg;->a:Lang;

    if-eqz v0, :cond_0

    .line 303
    :try_start_0
    iget-object v0, p0, Lahg;->a:Lang;

    invoke-interface {v0}, Lang;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :cond_0
    return-void

    .line 304
    :catch_0
    move-exception v0

    .line 306
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
