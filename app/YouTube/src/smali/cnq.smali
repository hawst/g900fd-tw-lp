.class public final Lcnq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:J


# instance fields
.field public final a:Lcnn;

.field public final b:Ljava/util/concurrent/Executor;

.field public c:J

.field private final e:Lezj;

.field private final f:Lcnm;

.field private final g:Lgoc;

.field private final h:Lcnx;

.field private final i:Lfac;

.field private j:Lcvl;

.field private k:Lcxl;

.field private l:Lcnu;

.field private m:Levn;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 38
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcnq;->d:J

    return-void
.end method

.method public constructor <init>(Lcnn;Lcnx;Ljava/util/concurrent/Executor;Lezj;Lcnm;Lgoc;Lfac;)V
    .locals 10

    .prologue
    .line 74
    sget-wide v8, Lcnq;->d:J

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcnq;-><init>(Lcnn;Lcnx;Ljava/util/concurrent/Executor;Lezj;Lcnm;Lgoc;Lfac;J)V

    .line 83
    return-void
.end method

.method public constructor <init>(Lcnn;Lcnx;Ljava/util/concurrent/Executor;Lezj;Lcnm;Lgoc;Lfac;J)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnn;

    iput-object v0, p0, Lcnq;->a:Lcnn;

    .line 94
    iput-object p2, p0, Lcnq;->h:Lcnx;

    .line 95
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcnq;->b:Ljava/util/concurrent/Executor;

    .line 96
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcnq;->e:Lezj;

    .line 97
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnm;

    iput-object v0, p0, Lcnq;->f:Lcnm;

    .line 98
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoc;

    iput-object v0, p0, Lcnq;->g:Lgoc;

    .line 99
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfac;

    iput-object v0, p0, Lcnq;->i:Lfac;

    .line 100
    iput-wide p8, p0, Lcnq;->c:J

    .line 101
    return-void
.end method

.method static synthetic a(Lcnq;)Lezj;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcnq;->e:Lezj;

    return-object v0
.end method

.method static synthetic a(Lcnq;Lfoy;)Lfoy;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcnq;->b(Lfoy;)Lfoy;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcnq;Lesq;Ljava/lang/String;Lfqx;)V
    .locals 14

    .prologue
    .line 36
    iget-object v0, p0, Lcnq;->j:Lcvl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnq;->k:Lcxl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnq;->l:Lcnu;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcnq;->b:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcnq;->j:Lcvl;

    iget-object v4, p0, Lcnq;->k:Lcxl;

    iget-object v5, p0, Lcnq;->l:Lcnu;

    iget-object v6, p0, Lcnq;->f:Lcnm;

    iget-object v7, p0, Lcnq;->g:Lgoc;

    sget-wide v8, Lcnq;->d:J

    iget-object v12, p0, Lcnq;->m:Levn;

    move-object v1, p0

    move-object v10, p1

    move-object/from16 v11, p3

    move-object/from16 v13, p2

    invoke-static/range {v1 .. v13}, Lcmx;->a(Lcnq;Ljava/util/concurrent/Executor;Lcvl;Lcxl;Lcnu;Lcnm;Lgoc;JLesq;Lfqx;Levn;Ljava/lang/String;)Lcmx;

    goto :goto_0
.end method

.method static synthetic b(Lcnq;)Lcnn;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcnq;->a:Lcnn;

    return-object v0
.end method

.method private b(Lfoy;)Lfoy;
    .locals 3

    .prologue
    .line 265
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 268
    :goto_0
    return-object v0

    .line 266
    :cond_0
    invoke-virtual {p1}, Lfoy;->b()Lfpc;

    move-result-object v0

    iget-object v1, p0, Lcnq;->i:Lfac;

    const/16 v2, 0xc

    .line 267
    invoke-virtual {v1, v2}, Lfac;->a(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lfpc;->h:Ljava/lang/String;

    .line 268
    invoke-virtual {v0}, Lfpc;->a()Lfoy;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Lcnq;)Levn;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcnq;->m:Levn;

    return-object v0
.end method


# virtual methods
.method protected final a(Lesq;Ljava/lang/String;Lfai;Ljava/util/Map;)Lesf;
    .locals 3

    .prologue
    .line 255
    invoke-virtual {p1}, Lesq;->a()Less;

    move-result-object v1

    .line 256
    if-nez v1, :cond_0

    .line 257
    const/4 v0, 0x0

    .line 261
    :goto_0
    return-object v0

    .line 259
    :cond_0
    iget-object v0, p0, Lcnq;->a:Lcnn;

    invoke-interface {v0, v1, p2, p3, p4}, Lcnn;->a(Less;Ljava/lang/String;Lfai;Ljava/util/Map;)Lfoy;

    move-result-object v2

    .line 261
    new-instance v0, Lesf;

    invoke-direct {p0, v2}, Lcnq;->b(Lfoy;)Lfoy;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lesf;-><init>(Less;Lfoy;)V

    goto :goto_0
.end method

.method public final a(Lcvl;Lcxl;Lcnu;Levn;)V
    .locals 4

    .prologue
    .line 108
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvl;

    iput-object v0, p0, Lcnq;->j:Lcvl;

    .line 109
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxl;

    iput-object v0, p0, Lcnq;->k:Lcxl;

    .line 110
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnu;

    iput-object v0, p0, Lcnq;->l:Lcnu;

    .line 111
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcnq;->m:Levn;

    .line 113
    new-instance v0, Lcoz;

    sget-wide v2, Lcnq;->d:J

    invoke-direct {v0, p3, p4, v2, v3}, Lcoz;-><init>(Lcnu;Levn;J)V

    invoke-interface {p1, v0}, Lcvl;->a(Lcvm;)V

    .line 115
    return-void
.end method

.method public final a(Less;Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcnq;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcnt;

    invoke-direct {v1, p0, p1, p2, p3}, Lcnt;-><init>(Lcnq;Less;Ljava/lang/String;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 248
    return-void
.end method

.method public final a(Lfoy;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcnq;->a:Lcnn;

    invoke-interface {v0, p1}, Lcnn;->a(Lfoy;)V

    .line 120
    return-void
.end method

.method public a(Lfrl;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 299
    invoke-virtual {p1}, Lfrl;->s()Lgyr;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 300
    invoke-virtual {p1}, Lfrl;->s()Lgyr;

    move-result-object v0

    iget-object v0, v0, Lgyr;->a:Ljava/lang/String;

    .line 301
    :goto_0
    iget-object v2, p0, Lcnq;->h:Lcnx;

    if-eqz v0, :cond_0

    .line 302
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 301
    :cond_0
    invoke-virtual {v2, v1}, Lcnx;->a(Ljava/util/regex/Pattern;)V

    .line 303
    return-void

    :cond_1
    move-object v0, v1

    .line 300
    goto :goto_0
.end method
