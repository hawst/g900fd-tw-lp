.class public final Lipq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lipt;
.implements Lipu;
.implements Ljava/lang/Cloneable;


# instance fields
.field a:Liqi;

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method private c([B)V
    .locals 3

    .prologue
    .line 492
    const/4 v0, 0x0

    .line 493
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 494
    array-length v1, p1

    sub-int/2addr v1, v0

    invoke-virtual {p0, p1, v0, v1}, Lipq;->a([BII)I

    move-result v1

    .line 495
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 496
    :cond_0
    add-int/2addr v0, v1

    .line 497
    goto :goto_0

    .line 498
    :cond_1
    return-void
.end method

.method private g(J)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 400
    sget-object v6, Liqo;->a:Ljava/nio/charset/Charset;

    iget-wide v0, p0, Lipq;->b:J

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Liqo;->a(JJJ)V

    if-nez v6, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "charset == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount > Integer.MAX_VALUE: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    cmp-long v0, p1, v2

    if-nez v0, :cond_3

    const-string v0, ""

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    iget-object v1, p0, Lipq;->a:Liqi;

    iget v0, v1, Liqi;->b:I

    int-to-long v2, v0

    add-long/2addr v2, p1

    iget v0, v1, Liqi;->c:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-lez v0, :cond_4

    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lipq;->e(J)[B

    move-result-object v1

    invoke-direct {v0, v1, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/String;

    iget-object v2, v1, Liqi;->a:[B

    iget v3, v1, Liqi;->b:I

    long-to-int v4, p1

    invoke-direct {v0, v2, v3, v4, v6}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iget v2, v1, Liqi;->b:I

    int-to-long v2, v2

    add-long/2addr v2, p1

    long-to-int v2, v2

    iput v2, v1, Liqi;->b:I

    iget-wide v2, p0, Lipq;->b:J

    sub-long/2addr v2, p1

    iput-wide v2, p0, Lipq;->b:J

    iget v2, v1, Liqi;->b:I

    iget v3, v1, Liqi;->c:I

    if-ne v2, v3, :cond_2

    invoke-virtual {v1}, Liqi;->a()Liqi;

    move-result-object v2

    iput-object v2, p0, Lipq;->a:Liqi;

    sget-object v2, Liqj;->a:Liqj;

    invoke-virtual {v2, v1}, Liqj;->a(Liqi;)V

    goto :goto_0
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    .line 501
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, Liqo;->a(JJJ)V

    .line 503
    iget-object v1, p0, Lipq;->a:Liqi;

    .line 504
    if-nez v1, :cond_1

    const/4 v0, -0x1

    .line 516
    :cond_0
    :goto_0
    return v0

    .line 505
    :cond_1
    iget v0, v1, Liqi;->c:I

    iget v2, v1, Liqi;->b:I

    sub-int/2addr v0, v2

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 506
    iget-object v2, v1, Liqi;->a:[B

    iget v3, v1, Liqi;->b:I

    invoke-static {v2, v3, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 508
    iget v2, v1, Liqi;->b:I

    add-int/2addr v2, v0

    iput v2, v1, Liqi;->b:I

    .line 509
    iget-wide v2, p0, Lipq;->b:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lipq;->b:J

    .line 511
    iget v2, v1, Liqi;->b:I

    iget v3, v1, Liqi;->c:I

    if-ne v2, v3, :cond_0

    .line 512
    invoke-virtual {v1}, Liqi;->a()Liqi;

    move-result-object v2

    iput-object v2, p0, Lipq;->a:Liqi;

    .line 513
    sget-object v2, Liqj;->a:Liqj;

    invoke-virtual {v2, v1}, Liqj;->a(Liqi;)V

    goto :goto_0
.end method

.method public final a(B)J
    .locals 2

    .prologue
    .line 779
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lipq;->a(BJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(BJ)J
    .locals 12

    .prologue
    .line 787
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fromIndex < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 789
    :cond_0
    iget-object v2, p0, Lipq;->a:Liqi;

    .line 790
    if-nez v2, :cond_1

    const-wide/16 v0, -0x1

    .line 806
    :goto_0
    return-wide v0

    .line 791
    :cond_1
    const-wide/16 v0, 0x0

    .line 793
    :cond_2
    iget v3, v2, Liqi;->c:I

    iget v4, v2, Liqi;->b:I

    sub-int/2addr v3, v4

    .line 794
    int-to-long v4, v3

    cmp-long v4, p2, v4

    if-ltz v4, :cond_3

    .line 795
    int-to-long v4, v3

    sub-long/2addr p2, v4

    .line 803
    :goto_1
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 804
    iget-object v2, v2, Liqi;->d:Liqi;

    .line 805
    iget-object v3, p0, Lipq;->a:Liqi;

    if-ne v2, v3, :cond_2

    .line 806
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 797
    :cond_3
    iget-object v6, v2, Liqi;->a:[B

    .line 798
    iget v4, v2, Liqi;->b:I

    int-to-long v4, v4

    add-long/2addr v4, p2

    iget v7, v2, Liqi;->c:I

    int-to-long v8, v7

    :goto_2
    cmp-long v7, v4, v8

    if-gez v7, :cond_5

    .line 799
    long-to-int v7, v4

    aget-byte v7, v6, v7

    if-ne v7, p1, :cond_4

    add-long/2addr v0, v4

    iget v2, v2, Liqi;->b:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0

    .line 798
    :cond_4
    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    goto :goto_2

    .line 801
    :cond_5
    const-wide/16 p2, 0x0

    goto :goto_1
.end method

.method public final a(Liql;)J
    .locals 6

    .prologue
    .line 592
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 593
    :cond_0
    const-wide/16 v0, 0x0

    .line 594
    :goto_0
    const-wide/16 v2, 0x800

    invoke-interface {p1, p0, v2, v3}, Liql;->b(Lipq;J)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 595
    add-long/2addr v0, v2

    goto :goto_0

    .line 597
    :cond_1
    return-wide v0
.end method

.method public final a(I)Lipq;
    .locals 4

    .prologue
    .line 601
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lipq;->d(I)Liqi;

    move-result-object v0

    .line 602
    iget-object v1, v0, Liqi;->a:[B

    iget v2, v0, Liqi;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, Liqi;->c:I

    int-to-byte v0, p1

    aput-byte v0, v1, v2

    .line 603
    iget-wide v0, p0, Lipq;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lipq;->b:J

    .line 604
    return-object p0
.end method

.method public final a(Lipv;)Lipq;
    .locals 3

    .prologue
    .line 550
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "byteString == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 551
    :cond_0
    iget-object v0, p1, Lipv;->b:[B

    const/4 v1, 0x0

    iget-object v2, p1, Lipv;->b:[B

    array-length v2, v2

    invoke-virtual {p0, v0, v1, v2}, Lipq;->b([BII)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lipq;
    .locals 3

    .prologue
    .line 555
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "string == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 557
    :cond_0
    sget-object v0, Liqo;->a:Ljava/nio/charset/Charset;

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "string == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "charset == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lipq;->b([BII)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final a([B)Lipq;
    .locals 2

    .prologue
    .line 568
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 569
    :cond_0
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lipq;->b([BII)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final a()Liqm;
    .locals 1

    .prologue
    .line 816
    sget-object v0, Liqm;->a:Liqm;

    return-object v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 94
    iget-wide v0, p0, Lipq;->b:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 95
    :cond_0
    return-void
.end method

.method public final a(Lipq;J)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 730
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 731
    :cond_0
    if-ne p1, p0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == this"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 732
    :cond_1
    iget-wide v0, p1, Lipq;->b:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Liqo;->a(JJJ)V

    .line 734
    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_a

    .line 736
    iget-object v0, p1, Lipq;->a:Liqi;

    iget v0, v0, Liqi;->c:I

    iget-object v1, p1, Lipq;->a:Liqi;

    iget v1, v1, Liqi;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    cmp-long v0, p2, v0

    if-gez v0, :cond_6

    .line 737
    iget-object v0, p0, Lipq;->a:Liqi;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lipq;->a:Liqi;

    iget-object v0, v0, Liqi;->e:Liqi;

    .line 738
    :goto_1
    if-eqz v0, :cond_2

    iget v1, v0, Liqi;->c:I

    iget v4, v0, Liqi;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    add-long/2addr v4, p2

    const-wide/16 v6, 0x800

    cmp-long v1, v4, v6

    if-lez v1, :cond_9

    .line 741
    :cond_2
    iget-object v1, p1, Lipq;->a:Liqi;

    long-to-int v4, p2

    iget v0, v1, Liqi;->c:I

    iget v5, v1, Liqi;->b:I

    sub-int/2addr v0, v5

    sub-int/2addr v0, v4

    if-lez v4, :cond_3

    if-gtz v0, :cond_5

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 737
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 741
    :cond_5
    if-ge v4, v0, :cond_8

    sget-object v0, Liqj;->a:Liqj;

    invoke-virtual {v0}, Liqj;->a()Liqi;

    move-result-object v0

    iget-object v5, v1, Liqi;->a:[B

    iget v6, v1, Liqi;->b:I

    iget-object v7, v0, Liqi;->a:[B

    iget v8, v0, Liqi;->b:I

    invoke-static {v5, v6, v7, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v5, v1, Liqi;->b:I

    add-int/2addr v5, v4

    iput v5, v1, Liqi;->b:I

    iget v5, v0, Liqi;->c:I

    add-int/2addr v4, v5

    iput v4, v0, Liqi;->c:I

    iget-object v1, v1, Liqi;->e:Liqi;

    invoke-virtual {v1, v0}, Liqi;->a(Liqi;)Liqi;

    :goto_2
    iput-object v0, p1, Lipq;->a:Liqi;

    .line 752
    :cond_6
    iget-object v0, p1, Lipq;->a:Liqi;

    .line 753
    iget v1, v0, Liqi;->c:I

    iget v4, v0, Liqi;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    .line 754
    invoke-virtual {v0}, Liqi;->a()Liqi;

    move-result-object v1

    iput-object v1, p1, Lipq;->a:Liqi;

    .line 755
    iget-object v1, p0, Lipq;->a:Liqi;

    if-nez v1, :cond_b

    .line 756
    iput-object v0, p0, Lipq;->a:Liqi;

    .line 757
    iget-object v0, p0, Lipq;->a:Liqi;

    iget-object v1, p0, Lipq;->a:Liqi;

    iget-object v6, p0, Lipq;->a:Liqi;

    iput-object v6, v1, Liqi;->e:Liqi;

    iput-object v6, v0, Liqi;->d:Liqi;

    .line 763
    :cond_7
    :goto_3
    iget-wide v0, p1, Lipq;->b:J

    sub-long/2addr v0, v4

    iput-wide v0, p1, Lipq;->b:J

    .line 764
    iget-wide v0, p0, Lipq;->b:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lipq;->b:J

    .line 765
    sub-long/2addr p2, v4

    .line 766
    goto/16 :goto_0

    .line 741
    :cond_8
    sget-object v5, Liqj;->a:Liqj;

    invoke-virtual {v5}, Liqj;->a()Liqi;

    move-result-object v5

    iget-object v6, v1, Liqi;->a:[B

    iget v7, v1, Liqi;->b:I

    add-int/2addr v4, v7

    iget-object v7, v5, Liqi;->a:[B

    iget v8, v5, Liqi;->b:I

    invoke-static {v6, v4, v7, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v4, v1, Liqi;->c:I

    sub-int/2addr v4, v0

    iput v4, v1, Liqi;->c:I

    iget v4, v5, Liqi;->c:I

    add-int/2addr v0, v4

    iput v0, v5, Liqi;->c:I

    invoke-virtual {v1, v5}, Liqi;->a(Liqi;)Liqi;

    move-object v0, v1

    goto :goto_2

    .line 744
    :cond_9
    iget-object v1, p1, Lipq;->a:Liqi;

    long-to-int v2, p2

    invoke-virtual {v1, v0, v2}, Liqi;->a(Liqi;I)V

    .line 745
    iget-wide v0, p1, Lipq;->b:J

    sub-long/2addr v0, p2

    iput-wide v0, p1, Lipq;->b:J

    .line 746
    iget-wide v0, p0, Lipq;->b:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lipq;->b:J

    .line 767
    :cond_a
    return-void

    .line 759
    :cond_b
    iget-object v1, p0, Lipq;->a:Liqi;

    iget-object v1, v1, Liqi;->e:Liqi;

    .line 760
    invoke-virtual {v1, v0}, Liqi;->a(Liqi;)Liqi;

    move-result-object v0

    .line 761
    iget-object v1, v0, Liqi;->e:Liqi;

    if-ne v1, v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_c
    iget-object v1, v0, Liqi;->e:Liqi;

    iget v1, v1, Liqi;->c:I

    iget-object v6, v0, Liqi;->e:Liqi;

    iget v6, v6, Liqi;->b:I

    sub-int/2addr v1, v6

    iget v6, v0, Liqi;->c:I

    iget v7, v0, Liqi;->b:I

    sub-int/2addr v6, v7

    add-int/2addr v1, v6

    const/16 v6, 0x800

    if-gt v1, v6, :cond_7

    iget-object v1, v0, Liqi;->e:Liqi;

    iget v6, v0, Liqi;->c:I

    iget v7, v0, Liqi;->b:I

    sub-int/2addr v6, v7

    invoke-virtual {v0, v1, v6}, Liqi;->a(Liqi;I)V

    invoke-virtual {v0}, Liqi;->a()Liqi;

    sget-object v1, Liqj;->a:Liqj;

    invoke-virtual {v1, v0}, Liqj;->a(Liqi;)V

    goto :goto_3
.end method

.method public final b(J)B
    .locals 7

    .prologue
    .line 252
    iget-wide v0, p0, Lipq;->b:J

    const-wide/16 v4, 0x1

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Liqo;->a(JJJ)V

    .line 253
    iget-object v0, p0, Lipq;->a:Liqi;

    .line 254
    :goto_0
    iget v1, v0, Liqi;->c:I

    iget v2, v0, Liqi;->b:I

    sub-int/2addr v1, v2

    .line 255
    int-to-long v2, v1

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    iget-object v1, v0, Liqi;->a:[B

    iget v0, v0, Liqi;->b:I

    long-to-int v2, p1

    add-int/2addr v0, v2

    aget-byte v0, v1, v0

    return v0

    .line 256
    :cond_0
    int-to-long v2, v1

    sub-long/2addr p1, v2

    .line 253
    iget-object v0, v0, Liqi;->d:Liqi;

    goto :goto_0
.end method

.method public final b(Lipq;J)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 770
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 771
    :cond_0
    cmp-long v0, p2, v2

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 772
    :cond_1
    iget-wide v0, p0, Lipq;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const-wide/16 p2, -0x1

    .line 775
    :goto_0
    return-wide p2

    .line 773
    :cond_2
    iget-wide v0, p0, Lipq;->b:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_3

    iget-wide p2, p0, Lipq;->b:J

    .line 774
    :cond_3
    invoke-virtual {p1, p0, p2, p3}, Lipq;->a(Lipq;J)V

    goto :goto_0
.end method

.method public final b(I)Lipq;
    .locals 5

    .prologue
    .line 608
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lipq;->d(I)Liqi;

    move-result-object v0

    .line 609
    iget-object v1, v0, Liqi;->a:[B

    .line 610
    iget v2, v0, Liqi;->c:I

    .line 611
    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x8

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 612
    add-int/lit8 v2, v3, 0x1

    int-to-byte v4, p1

    aput-byte v4, v1, v3

    .line 613
    iput v2, v0, Liqi;->c:I

    .line 614
    iget-wide v0, p0, Lipq;->b:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lipq;->b:J

    .line 615
    return-object p0
.end method

.method public final b([BII)Lipq;
    .locals 6

    .prologue
    .line 573
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 574
    :cond_0
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, Liqo;->a(JJJ)V

    .line 576
    add-int v0, p2, p3

    .line 577
    :goto_0
    if-ge p2, v0, :cond_1

    .line 578
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lipq;->d(I)Liqi;

    move-result-object v1

    .line 580
    sub-int v2, v0, p2

    iget v3, v1, Liqi;->c:I

    rsub-int v3, v3, 0x800

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 581
    iget-object v3, v1, Liqi;->a:[B

    iget v4, v1, Liqi;->c:I

    invoke-static {p1, p2, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 583
    add-int/2addr p2, v2

    .line 584
    iget v3, v1, Liqi;->c:I

    add-int/2addr v2, v3

    iput v2, v1, Liqi;->c:I

    goto :goto_0

    .line 587
    :cond_1
    iget-wide v0, p0, Lipq;->b:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lipq;->b:J

    .line 588
    return-object p0
.end method

.method public final synthetic b(Lipv;)Lipt;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lipq;->a(Lipv;)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;)Lipt;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lipq;->a(Ljava/lang/String;)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b([B)Lipt;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lipq;->a([B)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 810
    return-void
.end method

.method public final c()Lipq;
    .locals 0

    .prologue
    .line 60
    return-object p0
.end method

.method public final c(I)Lipq;
    .locals 5

    .prologue
    .line 623
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lipq;->d(I)Liqi;

    move-result-object v0

    .line 624
    iget-object v1, v0, Liqi;->a:[B

    .line 625
    iget v2, v0, Liqi;->c:I

    .line 626
    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x18

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 627
    add-int/lit8 v2, v3, 0x1

    ushr-int/lit8 v4, p1, 0x10

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 628
    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v4, p1, 0x8

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 629
    add-int/lit8 v2, v3, 0x1

    int-to-byte v4, p1

    aput-byte v4, v1, v3

    .line 630
    iput v2, v0, Liqi;->c:I

    .line 631
    iget-wide v0, p0, Lipq;->b:J

    const-wide/16 v2, 0x4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lipq;->b:J

    .line 632
    return-object p0
.end method

.method public final synthetic c([BII)Lipt;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1, p2, p3}, Lipq;->b([BII)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final c(J)Lipv;
    .locals 3

    .prologue
    .line 372
    new-instance v0, Lipv;

    invoke-virtual {p0, p1, p2}, Lipq;->e(J)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lipv;-><init>([B)V

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lipq;->o()Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 813
    return-void
.end method

.method final d(I)Liqi;
    .locals 3

    .prologue
    const/16 v2, 0x800

    .line 665
    if-lez p1, :cond_0

    if-le p1, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 667
    :cond_1
    iget-object v0, p0, Lipq;->a:Liqi;

    if-nez v0, :cond_3

    .line 668
    sget-object v0, Liqj;->a:Liqj;

    invoke-virtual {v0}, Liqj;->a()Liqi;

    move-result-object v0

    iput-object v0, p0, Lipq;->a:Liqi;

    .line 669
    iget-object v1, p0, Lipq;->a:Liqi;

    iget-object v2, p0, Lipq;->a:Liqi;

    iget-object v0, p0, Lipq;->a:Liqi;

    iput-object v0, v2, Liqi;->e:Liqi;

    iput-object v0, v1, Liqi;->d:Liqi;

    .line 676
    :cond_2
    :goto_0
    return-object v0

    .line 672
    :cond_3
    iget-object v0, p0, Lipq;->a:Liqi;

    iget-object v0, v0, Liqi;->e:Liqi;

    .line 673
    iget v1, v0, Liqi;->c:I

    add-int/2addr v1, p1

    if-le v1, v2, :cond_2

    .line 674
    sget-object v1, Liqj;->a:Liqj;

    invoke-virtual {v1}, Liqj;->a()Liqi;

    move-result-object v1

    invoke-virtual {v0, v1}, Liqi;->a(Liqi;)Liqi;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lipr;

    invoke-direct {v0, p0}, Lipr;-><init>(Lipq;)V

    return-object v0
.end method

.method final d(J)Ljava/lang/String;
    .locals 5

    .prologue
    const-wide/16 v2, 0x1

    .line 454
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    sub-long v0, p1, v2

    invoke-virtual {p0, v0, v1}, Lipq;->b(J)B

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 456
    sub-long v0, p1, v2

    invoke-direct {p0, v0, v1}, Lipq;->g(J)Ljava/lang/String;

    move-result-object v0

    .line 457
    const-wide/16 v2, 0x2

    invoke-virtual {p0, v2, v3}, Lipq;->f(J)V

    .line 464
    :goto_0
    return-object v0

    .line 462
    :cond_0
    invoke-direct {p0, p1, p2}, Lipq;->g(J)Ljava/lang/String;

    move-result-object v0

    .line 463
    invoke-virtual {p0, v2, v3}, Lipq;->f(J)V

    goto :goto_0
.end method

.method public final synthetic e(I)Lipt;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lipq;->c(I)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 90
    iget-wide v0, p0, Lipq;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(J)[B
    .locals 7

    .prologue
    .line 477
    iget-wide v0, p0, Lipq;->b:J

    const-wide/16 v2, 0x0

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Liqo;->a(JJJ)V

    .line 478
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 479
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount > Integer.MAX_VALUE: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 482
    :cond_0
    long-to-int v0, p1

    new-array v0, v0, [B

    .line 483
    invoke-direct {p0, v0}, Lipq;->c([B)V

    .line 484
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 14

    .prologue
    const-wide/16 v0, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 831
    if-ne p0, p1, :cond_0

    move v0, v6

    .line 860
    :goto_0
    return v0

    .line 832
    :cond_0
    instance-of v2, p1, Lipq;

    if-nez v2, :cond_1

    move v0, v7

    goto :goto_0

    .line 833
    :cond_1
    check-cast p1, Lipq;

    .line 834
    iget-wide v2, p0, Lipq;->b:J

    iget-wide v4, p1, Lipq;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    move v0, v7

    goto :goto_0

    .line 835
    :cond_2
    iget-wide v2, p0, Lipq;->b:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_3

    move v0, v6

    goto :goto_0

    .line 837
    :cond_3
    iget-object v5, p0, Lipq;->a:Liqi;

    .line 838
    iget-object v4, p1, Lipq;->a:Liqi;

    .line 839
    iget v3, v5, Liqi;->b:I

    .line 840
    iget v2, v4, Liqi;->b:I

    .line 842
    :goto_1
    iget-wide v8, p0, Lipq;->b:J

    cmp-long v8, v0, v8

    if-gez v8, :cond_8

    .line 843
    iget v8, v5, Liqi;->c:I

    sub-int/2addr v8, v3

    iget v9, v4, Liqi;->c:I

    sub-int/2addr v9, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-long v10, v8

    move v8, v7

    .line 845
    :goto_2
    int-to-long v12, v8

    cmp-long v9, v12, v10

    if-gez v9, :cond_5

    .line 846
    iget-object v12, v5, Liqi;->a:[B

    add-int/lit8 v9, v3, 0x1

    aget-byte v12, v12, v3

    iget-object v13, v4, Liqi;->a:[B

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v13, v2

    if-eq v12, v2, :cond_4

    move v0, v7

    goto :goto_0

    .line 845
    :cond_4
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v2, v3

    move v3, v9

    goto :goto_2

    .line 849
    :cond_5
    iget v8, v5, Liqi;->c:I

    if-ne v3, v8, :cond_6

    .line 850
    iget-object v5, v5, Liqi;->d:Liqi;

    .line 851
    iget v3, v5, Liqi;->b:I

    .line 854
    :cond_6
    iget v8, v4, Liqi;->c:I

    if-ne v2, v8, :cond_7

    .line 855
    iget-object v4, v4, Liqi;->d:Liqi;

    .line 856
    iget v2, v4, Liqi;->b:I

    .line 842
    :cond_7
    add-long/2addr v0, v10

    goto :goto_1

    :cond_8
    move v0, v6

    .line 860
    goto :goto_0
.end method

.method public final synthetic f(I)Lipt;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lipq;->b(I)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lips;

    invoke-direct {v0, p0}, Lips;-><init>(Lipq;)V

    return-object v0
.end method

.method public final f(J)V
    .locals 7

    .prologue
    .line 533
    :cond_0
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    .line 534
    iget-object v0, p0, Lipq;->a:Liqi;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 536
    :cond_1
    iget-object v0, p0, Lipq;->a:Liqi;

    iget v0, v0, Liqi;->c:I

    iget-object v1, p0, Lipq;->a:Liqi;

    iget v1, v1, Liqi;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 537
    iget-wide v2, p0, Lipq;->b:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lipq;->b:J

    .line 538
    int-to-long v2, v0

    sub-long/2addr p1, v2

    .line 539
    iget-object v1, p0, Lipq;->a:Liqi;

    iget v2, v1, Liqi;->b:I

    add-int/2addr v0, v2

    iput v0, v1, Liqi;->b:I

    .line 541
    iget-object v0, p0, Lipq;->a:Liqi;

    iget v0, v0, Liqi;->b:I

    iget-object v1, p0, Lipq;->a:Liqi;

    iget v1, v1, Liqi;->c:I

    if-ne v0, v1, :cond_0

    .line 542
    iget-object v0, p0, Lipq;->a:Liqi;

    .line 543
    invoke-virtual {v0}, Liqi;->a()Liqi;

    move-result-object v1

    iput-object v1, p0, Lipq;->a:Liqi;

    .line 544
    sget-object v1, Liqj;->a:Liqj;

    invoke-virtual {v1, v0}, Liqj;->a(Liqi;)V

    goto :goto_0

    .line 547
    :cond_2
    return-void
.end method

.method public final g()B
    .locals 10

    .prologue
    .line 230
    iget-wide v0, p0, Lipq;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "size == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    iget-object v0, p0, Lipq;->a:Liqi;

    .line 233
    iget v1, v0, Liqi;->b:I

    .line 234
    iget v2, v0, Liqi;->c:I

    .line 236
    iget-object v3, v0, Liqi;->a:[B

    .line 237
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    .line 238
    iget-wide v6, p0, Lipq;->b:J

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lipq;->b:J

    .line 240
    if-ne v4, v2, :cond_1

    .line 241
    invoke-virtual {v0}, Liqi;->a()Liqi;

    move-result-object v2

    iput-object v2, p0, Lipq;->a:Liqi;

    .line 242
    sget-object v2, Liqj;->a:Liqj;

    invoke-virtual {v2, v0}, Liqj;->a(Liqi;)V

    .line 247
    :goto_0
    return v1

    .line 244
    :cond_1
    iput v4, v0, Liqi;->b:I

    goto :goto_0
.end method

.method public final synthetic g(I)Lipt;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lipq;->a(I)Lipq;

    move-result-object v0

    return-object v0
.end method

.method public final h()S
    .locals 10

    .prologue
    const-wide/16 v8, 0x2

    .line 261
    iget-wide v0, p0, Lipq;->b:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size < 2: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lipq;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_0
    iget-object v0, p0, Lipq;->a:Liqi;

    .line 264
    iget v1, v0, Liqi;->b:I

    .line 265
    iget v2, v0, Liqi;->c:I

    .line 268
    sub-int v3, v2, v1

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    .line 269
    invoke-virtual {p0}, Lipq;->g()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    .line 270
    invoke-virtual {p0}, Lipq;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 271
    int-to-short v0, v0

    .line 286
    :goto_0
    return v0

    .line 274
    :cond_1
    iget-object v3, v0, Liqi;->a:[B

    .line 275
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v1, v3

    .line 277
    iget-wide v6, p0, Lipq;->b:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lipq;->b:J

    .line 279
    if-ne v5, v2, :cond_2

    .line 280
    invoke-virtual {v0}, Liqi;->a()Liqi;

    move-result-object v2

    iput-object v2, p0, Lipq;->a:Liqi;

    .line 281
    sget-object v2, Liqj;->a:Liqj;

    invoke-virtual {v2, v0}, Liqj;->a(Liqi;)V

    .line 286
    :goto_1
    int-to-short v0, v1

    goto :goto_0

    .line 283
    :cond_2
    iput v5, v0, Liqi;->b:I

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 864
    iget-object v1, p0, Lipq;->a:Liqi;

    .line 865
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 873
    :goto_0
    return v0

    .line 866
    :cond_0
    const/4 v0, 0x1

    .line 868
    :cond_1
    iget v2, v1, Liqi;->b:I

    iget v4, v1, Liqi;->c:I

    :goto_1
    if-ge v2, v4, :cond_2

    .line 869
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, v1, Liqi;->a:[B

    aget-byte v3, v3, v2

    add-int/2addr v3, v0

    .line 868
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    .line 871
    :cond_2
    iget-object v1, v1, Liqi;->d:Liqi;

    .line 872
    iget-object v2, p0, Lipq;->a:Liqi;

    if-ne v1, v2, :cond_1

    goto :goto_0
.end method

.method public final i()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x4

    .line 290
    iget-wide v0, p0, Lipq;->b:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size < 4: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lipq;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 292
    :cond_0
    iget-object v1, p0, Lipq;->a:Liqi;

    .line 293
    iget v0, v1, Liqi;->b:I

    .line 294
    iget v2, v1, Liqi;->c:I

    .line 297
    sub-int v3, v2, v0

    const/4 v4, 0x4

    if-ge v3, v4, :cond_1

    .line 298
    invoke-virtual {p0}, Lipq;->g()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    .line 299
    invoke-virtual {p0}, Lipq;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 300
    invoke-virtual {p0}, Lipq;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    .line 301
    invoke-virtual {p0}, Lipq;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 318
    :goto_0
    return v0

    .line 304
    :cond_1
    iget-object v3, v1, Liqi;->a:[B

    .line 305
    add-int/lit8 v4, v0, 0x1

    aget-byte v0, v3, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v5, v4, 0x1

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v0, v4

    add-int/lit8 v4, v5, 0x1

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v0, v5

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    .line 309
    iget-wide v6, p0, Lipq;->b:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lipq;->b:J

    .line 311
    if-ne v5, v2, :cond_2

    .line 312
    invoke-virtual {v1}, Liqi;->a()Liqi;

    move-result-object v2

    iput-object v2, p0, Lipq;->a:Liqi;

    .line 313
    sget-object v2, Liqj;->a:Liqj;

    invoke-virtual {v2, v1}, Liqj;->a(Liqi;)V

    goto :goto_0

    .line 315
    :cond_2
    iput v5, v1, Liqi;->b:I

    goto :goto_0
.end method

.method public final j()S
    .locals 1

    .prologue
    .line 356
    invoke-virtual {p0}, Lipq;->h()S

    move-result v0

    invoke-static {v0}, Liqo;->a(S)S

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 360
    invoke-virtual {p0}, Lipq;->i()I

    move-result v0

    invoke-static {v0}, Liqo;->a(I)I

    move-result v0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 448
    const/16 v0, 0xa

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lipq;->a(BJ)J

    move-result-wide v0

    .line 449
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 450
    :cond_0
    invoke-virtual {p0, v0, v1}, Lipq;->d(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()[B
    .locals 2

    .prologue
    .line 470
    :try_start_0
    iget-wide v0, p0, Lipq;->b:J

    invoke-virtual {p0, v0, v1}, Lipq;->e(J)[B
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 471
    :catch_0
    move-exception v0

    .line 472
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 525
    :try_start_0
    iget-wide v0, p0, Lipq;->b:J

    invoke-virtual {p0, v0, v1}, Lipq;->f(J)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 528
    return-void

    .line 526
    :catch_0
    move-exception v0

    .line 527
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final o()Lipq;
    .locals 6

    .prologue
    .line 901
    new-instance v1, Lipq;

    invoke-direct {v1}, Lipq;-><init>()V

    .line 902
    iget-wide v2, p0, Lipq;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    move-object v0, v1

    .line 909
    :goto_0
    return-object v0

    .line 904
    :cond_0
    iget-object v0, p0, Lipq;->a:Liqi;

    iget-object v0, v0, Liqi;->a:[B

    iget-object v2, p0, Lipq;->a:Liqi;

    iget v2, v2, Liqi;->b:I

    iget-object v3, p0, Lipq;->a:Liqi;

    iget v3, v3, Liqi;->c:I

    iget-object v4, p0, Lipq;->a:Liqi;

    iget v4, v4, Liqi;->b:I

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lipq;->b([BII)Lipq;

    .line 905
    iget-object v0, p0, Lipq;->a:Liqi;

    iget-object v0, v0, Liqi;->d:Liqi;

    :goto_1
    iget-object v2, p0, Lipq;->a:Liqi;

    if-eq v0, v2, :cond_1

    .line 906
    iget-object v2, v0, Liqi;->a:[B

    iget v3, v0, Liqi;->b:I

    iget v4, v0, Liqi;->c:I

    iget v5, v0, Liqi;->b:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lipq;->b([BII)Lipq;

    .line 905
    iget-object v0, v0, Liqi;->d:Liqi;

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 909
    goto :goto_0
.end method

.method public final bridge synthetic p()Lipt;
    .locals 0

    .prologue
    .line 47
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 877
    iget-wide v0, p0, Lipq;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 878
    const-string v0, "Buffer[size=0]"

    .line 892
    :goto_0
    return-object v0

    .line 881
    :cond_0
    iget-wide v0, p0, Lipq;->b:J

    const-wide/16 v2, 0x10

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 882
    invoke-virtual {p0}, Lipq;->o()Lipq;

    move-result-object v0

    new-instance v1, Lipv;

    invoke-virtual {v0}, Lipq;->m()[B

    move-result-object v0

    invoke-direct {v1, v0}, Lipv;-><init>([B)V

    .line 883
    const-string v0, "Buffer[size=%s data=%s]"

    new-array v2, v4, [Ljava/lang/Object;

    iget-wide v4, p0, Lipq;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1}, Lipv;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v7

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 887
    :cond_1
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 888
    iget-object v0, p0, Lipq;->a:Liqi;

    iget-object v0, v0, Liqi;->a:[B

    iget-object v2, p0, Lipq;->a:Liqi;

    iget v2, v2, Liqi;->b:I

    iget-object v3, p0, Lipq;->a:Liqi;

    iget v3, v3, Liqi;->c:I

    iget-object v4, p0, Lipq;->a:Liqi;

    iget v4, v4, Liqi;->b:I

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/security/MessageDigest;->update([BII)V

    .line 889
    iget-object v0, p0, Lipq;->a:Liqi;

    iget-object v0, v0, Liqi;->d:Liqi;

    :goto_1
    iget-object v2, p0, Lipq;->a:Liqi;

    if-eq v0, v2, :cond_2

    .line 890
    iget-object v2, v0, Liqi;->a:[B

    iget v3, v0, Liqi;->b:I

    iget v4, v0, Liqi;->c:I

    iget v5, v0, Liqi;->b:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Ljava/security/MessageDigest;->update([BII)V

    .line 889
    iget-object v0, v0, Liqi;->d:Liqi;

    goto :goto_1

    .line 892
    :cond_2
    const-string v0, "Buffer[size=%s md5=%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lipq;->b:J

    .line 893
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-static {v1}, Lipv;->a([B)Lipv;

    move-result-object v1

    invoke-virtual {v1}, Lipv;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    .line 892
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 895
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
