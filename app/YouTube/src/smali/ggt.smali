.class public final enum Lggt;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lggt;

.field public static final enum b:Lggt;

.field private static enum d:Lggt;

.field private static enum e:Lggt;

.field private static enum f:Lggt;

.field private static enum g:Lggt;

.field private static enum h:Lggt;

.field private static final synthetic i:[Lggt;


# instance fields
.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    new-instance v0, Lggt;

    const-string v1, "ANDROID"

    const-string v2, "android"

    invoke-direct {v0, v1, v4, v2}, Lggt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggt;->a:Lggt;

    .line 36
    new-instance v0, Lggt;

    const-string v1, "ANDROID_TV"

    const-string v2, "android_tv"

    invoke-direct {v0, v1, v5, v2}, Lggt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggt;->d:Lggt;

    .line 37
    new-instance v0, Lggt;

    const-string v1, "TVANDROID"

    const-string v2, "tvandroid"

    invoke-direct {v0, v1, v6, v2}, Lggt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggt;->e:Lggt;

    .line 38
    new-instance v0, Lggt;

    const-string v1, "ANDROID_INSTANT"

    const-string v2, "android_instant"

    invoke-direct {v0, v1, v7, v2}, Lggt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggt;->f:Lggt;

    .line 39
    new-instance v0, Lggt;

    const-string v1, "ANDROID_KIDS"

    const-string v2, "android_kids"

    invoke-direct {v0, v1, v8, v2}, Lggt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggt;->g:Lggt;

    .line 40
    new-instance v0, Lggt;

    const-string v1, "OTHERAPP"

    const/4 v2, 0x5

    const-string v3, "otherapp"

    invoke-direct {v0, v1, v2, v3}, Lggt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggt;->b:Lggt;

    .line 41
    new-instance v0, Lggt;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x6

    const-string v3, "unknown_interface"

    invoke-direct {v0, v1, v2, v3}, Lggt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggt;->h:Lggt;

    .line 34
    const/4 v0, 0x7

    new-array v0, v0, [Lggt;

    sget-object v1, Lggt;->a:Lggt;

    aput-object v1, v0, v4

    sget-object v1, Lggt;->d:Lggt;

    aput-object v1, v0, v5

    sget-object v1, Lggt;->e:Lggt;

    aput-object v1, v0, v6

    sget-object v1, Lggt;->f:Lggt;

    aput-object v1, v0, v7

    sget-object v1, Lggt;->g:Lggt;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lggt;->b:Lggt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lggt;->h:Lggt;

    aput-object v2, v0, v1

    sput-object v0, Lggt;->i:[Lggt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput-object p3, p0, Lggt;->c:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lggt;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lggt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lggt;

    return-object v0
.end method

.method public static values()[Lggt;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lggt;->i:[Lggt;

    invoke-virtual {v0}, [Lggt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lggt;

    return-object v0
.end method
