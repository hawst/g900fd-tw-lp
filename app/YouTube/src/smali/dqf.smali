.class final enum Ldqf;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldqf;

.field public static final enum b:Ldqf;

.field public static final enum c:Ldqf;

.field public static final enum d:Ldqf;

.field public static final enum e:Ldqf;

.field public static final enum f:Ldqf;

.field public static final enum g:Ldqf;

.field private static final synthetic h:[Ldqf;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 253
    new-instance v0, Ldqf;

    const-string v1, "BODY"

    invoke-direct {v0, v1, v3}, Ldqf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqf;->a:Ldqf;

    new-instance v0, Ldqf;

    const-string v1, "CHUNK_DATA"

    invoke-direct {v0, v1, v4}, Ldqf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqf;->b:Ldqf;

    new-instance v0, Ldqf;

    const-string v1, "CHUNK_END"

    invoke-direct {v0, v1, v5}, Ldqf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqf;->c:Ldqf;

    new-instance v0, Ldqf;

    const-string v1, "CHUNK_SIZE"

    invoke-direct {v0, v1, v6}, Ldqf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqf;->d:Ldqf;

    new-instance v0, Ldqf;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v7}, Ldqf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqf;->e:Ldqf;

    new-instance v0, Ldqf;

    const-string v1, "HEADER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldqf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqf;->f:Ldqf;

    new-instance v0, Ldqf;

    const-string v1, "TRAILER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ldqf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldqf;->g:Ldqf;

    .line 252
    const/4 v0, 0x7

    new-array v0, v0, [Ldqf;

    sget-object v1, Ldqf;->a:Ldqf;

    aput-object v1, v0, v3

    sget-object v1, Ldqf;->b:Ldqf;

    aput-object v1, v0, v4

    sget-object v1, Ldqf;->c:Ldqf;

    aput-object v1, v0, v5

    sget-object v1, Ldqf;->d:Ldqf;

    aput-object v1, v0, v6

    sget-object v1, Ldqf;->e:Ldqf;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldqf;->f:Ldqf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldqf;->g:Ldqf;

    aput-object v2, v0, v1

    sput-object v0, Ldqf;->h:[Ldqf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 252
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldqf;
    .locals 1

    .prologue
    .line 252
    const-class v0, Ldqf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldqf;

    return-object v0
.end method

.method public static values()[Ldqf;
    .locals 1

    .prologue
    .line 252
    sget-object v0, Ldqf;->h:[Ldqf;

    invoke-virtual {v0}, [Ldqf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldqf;

    return-object v0
.end method
