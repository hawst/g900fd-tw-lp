.class final Ldzu;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field a:Ljava/io/InputStream;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Ldzu;->a:Ljava/io/InputStream;

    return-void
.end method


# virtual methods
.method public final available()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ldzu;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 71
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldzu;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    goto :goto_0
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ldzu;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 39
    const/4 v0, -0x1

    .line 41
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldzu;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Ldzu;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 53
    const/4 v0, -0x1

    .line 55
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldzu;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    goto :goto_0
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Ldzu;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 62
    const-wide/16 v0, 0x0

    .line 64
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Ldzu;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    goto :goto_0
.end method
