.class public final Lgro;
.super Lgrn;
.source "SourceFile"


# instance fields
.field private final d:Lgqo;

.field private final e:Ljava/util/Iterator;

.field private f:J

.field private g:J


# direct methods
.method public constructor <init>(Lgqo;Ljava/util/List;Lgru;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const-wide/16 v2, -0x1

    .line 40
    invoke-direct {p0, p3, p4, p5}, Lgrn;-><init>(Lgru;Ljava/lang/String;I)V

    .line 23
    iput-wide v2, p0, Lgro;->f:J

    .line 24
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgro;->g:J

    .line 42
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 43
    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lb;->b(Z)V

    .line 45
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    move-wide v2, v0

    .line 46
    goto :goto_0

    .line 44
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 48
    :cond_1
    iput-object p1, p0, Lgro;->d:Lgqo;

    .line 49
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lgro;->e:Ljava/util/Iterator;

    .line 50
    return-void
.end method


# virtual methods
.method public final a(JZ)Lgrb;
    .locals 5

    .prologue
    .line 81
    iput-wide p1, p0, Lgro;->g:J

    .line 83
    sget-object v0, Lgrb;->a:Lgrb;

    .line 84
    if-eqz p3, :cond_2

    .line 85
    sget-object v0, Lgrb;->b:Lgrb;

    move-object v1, v0

    .line 88
    :goto_0
    iget-wide v2, p0, Lgro;->f:J

    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    .line 89
    iget-object v0, p0, Lgro;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lgro;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lgro;->f:J

    .line 91
    iget-object v0, p0, Lgro;->d:Lgqo;

    iget-wide v2, p0, Lgro;->f:J

    invoke-virtual {v0, v2, v3}, Lgqo;->b(J)Ljava/lang/Long;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-lez v2, :cond_0

    .line 94
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Lgrf;->b:Lgrf;

    invoke-static {v0, v1, v2}, Lgrb;->a(JLgrf;)Lgrb;

    move-result-object v1

    .line 101
    :cond_0
    :goto_1
    return-object v1

    .line 97
    :cond_1
    sget-object v1, Lgrb;->b:Lgrb;

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(J)Z
    .locals 3

    .prologue
    .line 76
    iget-wide v0, p0, Lgro;->f:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JJ)Z
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lgrn;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lgro;->g:J

    cmp-long v0, p3, v0

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lgrb;
    .locals 3

    .prologue
    .line 63
    invoke-super {p0}, Lgrn;->b()Lgrb;

    move-result-object v0

    .line 64
    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lgro;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    sget-object v0, Lgrb;->b:Lgrb;

    .line 71
    :cond_0
    :goto_0
    return-object v0

    .line 68
    :cond_1
    iget-object v0, p0, Lgro;->e:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lgro;->f:J

    .line 69
    iget-wide v0, p0, Lgro;->f:J

    sget-object v2, Lgrf;->a:Lgrf;

    invoke-static {v0, v1, v2}, Lgrb;->a(JLgrf;)Lgrb;

    move-result-object v0

    goto :goto_0
.end method
