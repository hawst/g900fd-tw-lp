.class public final Lfsu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfth;


# instance fields
.field private final b:Lfsz;

.field private final c:Lgix;

.field private final d:Ljava/util/List;

.field private final e:Ljava/lang/String;

.field private final f:Lfac;


# direct methods
.method public constructor <init>(Lfsz;Lgix;Ljava/util/List;Ljava/lang/String;Lfac;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsz;

    iput-object v0, p0, Lfsu;->b:Lfsz;

    .line 32
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lfsu;->c:Lgix;

    .line 33
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lfsu;->d:Ljava/util/List;

    .line 34
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfsu;->e:Ljava/lang/String;

    .line 35
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfac;

    iput-object v0, p0, Lfsu;->f:Lfac;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Lhrn;
    .locals 4

    .prologue
    .line 45
    invoke-static {}, Lb;->b()V

    .line 46
    new-instance v1, Lftj;

    iget-object v0, p0, Lfsu;->b:Lfsz;

    iget-object v2, p0, Lfsu;->c:Lgix;

    .line 47
    invoke-interface {v2}, Lgix;->d()Lgit;

    move-result-object v2

    iget-object v3, p0, Lfsu;->f:Lfac;

    invoke-direct {v1, v0, v2, v3}, Lftj;-><init>(Lfsz;Lgit;Lfac;)V

    .line 49
    iget-object v0, p0, Lfsu;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftg;

    .line 50
    invoke-interface {v0, v1}, Lftg;->a(Lftj;)V

    goto :goto_0

    .line 53
    :cond_0
    sget-object v0, Lfhy;->a:[B

    invoke-virtual {v1, v0}, Lftj;->a([B)V

    .line 54
    const-string v0, ""

    iput-object v0, v1, Lftj;->a:Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lfsu;->e:Ljava/lang/String;

    iput-object v0, v1, Lftj;->d:Ljava/lang/String;

    .line 56
    invoke-virtual {v1}, Lftj;->m()Lhrn;

    move-result-object v0

    return-object v0
.end method
