.class public final Lcfu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field final a:Lfus;

.field final b:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

.field c:Lgbu;

.field private final d:Landroid/content/res/Resources;

.field private final e:Leyp;

.field private final f:Lgnd;

.field private final g:Lexd;

.field private final h:Lbjx;

.field private final i:Lboi;

.field private final j:Lfsj;

.field private final k:Landroid/view/View$OnClickListener;

.field private final l:Landroid/view/View;

.field private final m:Landroid/widget/TextView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/TextView;

.field private final p:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

.field private final q:Lcfw;

.field private final r:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Leyp;Lgnd;Lexd;Lfus;Lbjx;Lboi;)V
    .locals 3

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcfu;->j:Lfsj;

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcfu;->d:Landroid/content/res/Resources;

    .line 80
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcfu;->e:Leyp;

    .line 81
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnd;

    iput-object v0, p0, Lcfu;->f:Lgnd;

    .line 82
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lcfu;->g:Lexd;

    .line 83
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfus;

    iput-object v0, p0, Lcfu;->a:Lfus;

    .line 84
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjx;

    iput-object v0, p0, Lcfu;->h:Lbjx;

    .line 85
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcfu;->i:Lboi;

    .line 87
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 88
    const v1, 0x7f0400a0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcfu;->l:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lcfu;->l:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcfu;->m:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcfu;->m:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 92
    iget-object v0, p0, Lcfu;->l:Landroid/view/View;

    const v1, 0x7f080136

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcfu;->n:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Lcfu;->l:Landroid/view/View;

    const v1, 0x7f080132

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcfu;->o:Landroid/widget/TextView;

    .line 95
    iget-object v0, p0, Lcfu;->l:Landroid/view/View;

    const v1, 0x7f080135

    .line 96
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    .line 95
    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    iput-object v0, p0, Lcfu;->b:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    .line 97
    new-instance v0, Lcfw;

    invoke-direct {v0, p0}, Lcfw;-><init>(Lcfu;)V

    iput-object v0, p0, Lcfu;->q:Lcfw;

    .line 99
    iget-object v0, p0, Lcfu;->l:Landroid/view/View;

    const v1, 0x7f080218

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    iput-object v0, p0, Lcfu;->p:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    .line 100
    iget-object v0, p0, Lcfu;->l:Landroid/view/View;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcfu;->r:Landroid/view/View;

    .line 101
    iget-object v0, p0, Lcfu;->l:Landroid/view/View;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 103
    new-instance v0, Lcfv;

    invoke-direct {v0, p0}, Lcfv;-><init>(Lcfu;)V

    iput-object v0, p0, Lcfu;->k:Landroid/view/View$OnClickListener;

    .line 109
    return-void
.end method

.method private a(Lglw;)V
    .locals 10

    .prologue
    const/high16 v9, 0x7f100000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 155
    const v3, 0x7f07009b

    .line 157
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lglw;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 158
    iget-object v0, p0, Lcfu;->c:Lgbu;

    iget-object v0, v0, Lgbu;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 159
    invoke-virtual {p1}, Lglw;->a()I

    move-result v4

    .line 160
    iget-object v5, p1, Lglw;->a:Lgbu;

    iget v5, v5, Lgbu;->j:I

    .line 161
    invoke-virtual {p1}, Lglw;->b()Z

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x5c

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Updating progress on playlist="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", numFinished="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", size="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", isFinished= "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 163
    const/4 v0, 0x0

    .line 165
    iget-object v4, p1, Lglw;->a:Lgbu;

    iget-boolean v4, v4, Lgbu;->l:Z

    if-eqz v4, :cond_0

    .line 166
    iget-object v1, p0, Lcfu;->o:Landroid/widget/TextView;

    const v4, 0x7f090197

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    move v4, v0

    move v1, v2

    move v0, v3

    move v3, v2

    .line 188
    :goto_0
    if-eqz v3, :cond_6

    .line 189
    iget-object v3, p0, Lcfu;->p:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a()V

    .line 193
    :goto_1
    iget-object v3, p0, Lcfu;->b:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a(Z)V

    .line 194
    iget-object v3, p0, Lcfu;->p:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    .line 195
    iget-object v2, p0, Lcfu;->p:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v4, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    const/16 v4, 0x64

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(II)V

    .line 208
    :goto_2
    iget-object v2, p0, Lcfu;->o:Landroid/widget/TextView;

    iget-object v3, p0, Lcfu;->d:Landroid/content/res/Resources;

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 209
    iget-object v2, p0, Lcfu;->j:Lfsj;

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcfu;->k:Landroid/view/View$OnClickListener;

    :goto_3
    invoke-interface {v2, v0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 210
    return-void

    .line 169
    :cond_0
    invoke-virtual {p1}, Lglw;->a()I

    move-result v0

    int-to-float v0, v0

    iget-object v4, p1, Lglw;->a:Lgbu;

    iget v4, v4, Lgbu;->j:I

    int-to-float v4, v4

    div-float v4, v0, v4

    .line 170
    iget-object v0, p0, Lcfu;->o:Landroid/widget/TextView;

    iget-object v5, p0, Lcfu;->d:Landroid/content/res/Resources;

    .line 173
    iget-object v6, p1, Lglw;->a:Lgbu;

    iget v6, v6, Lgbu;->j:I

    new-array v7, v1, [Ljava/lang/Object;

    .line 174
    iget-object v8, p1, Lglw;->a:Lgbu;

    iget v8, v8, Lgbu;->j:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    .line 171
    invoke-virtual {v5, v9, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 170
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcfu;->g:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 176
    :goto_4
    iget-object v5, p0, Lcfu;->g:Lexd;

    invoke-interface {v5}, Lexd;->c()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcfu;->h:Lbjx;

    .line 177
    invoke-virtual {v5}, Lbjx;->c()Z

    move-result v5

    if-eqz v5, :cond_3

    move v5, v1

    .line 178
    :goto_5
    if-nez v0, :cond_1

    if-eqz v5, :cond_5

    .line 180
    :cond_1
    iget-object v3, p0, Lcfu;->o:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    const v0, 0x7f090193

    :goto_6
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 182
    const v3, 0x7f07009c

    move v0, v3

    move v3, v2

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 175
    goto :goto_4

    :cond_3
    move v5, v2

    .line 177
    goto :goto_5

    .line 180
    :cond_4
    const v0, 0x7f090194

    goto :goto_6

    :cond_5
    move v0, v3

    move v3, v1

    .line 184
    goto/16 :goto_0

    .line 191
    :cond_6
    iget-object v3, p0, Lcfu;->p:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->b()V

    goto/16 :goto_1

    .line 198
    :cond_7
    iget-object v0, p0, Lcfu;->o:Landroid/widget/TextView;

    iget-object v4, p0, Lcfu;->d:Landroid/content/res/Resources;

    iget-object v5, p0, Lcfu;->c:Lgbu;

    iget v5, v5, Lgbu;->j:I

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, p0, Lcfu;->c:Lgbu;

    iget v7, v7, Lgbu;->j:I

    .line 202
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    .line 199
    invoke-virtual {v4, v9, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 198
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    iget-object v0, p0, Lcfu;->b:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a(Z)V

    .line 204
    iget-object v0, p0, Lcfu;->p:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->b()V

    .line 205
    iget-object v0, p0, Lcfu;->p:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    move v0, v3

    goto/16 :goto_2

    .line 209
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_3
.end method

.method private handleOfflinePlaylistAddEvent(Lblq;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 140
    iget-object v0, p1, Lblq;->a:Lglw;

    .line 141
    iget-object v1, p0, Lcfu;->c:Lgbu;

    iget-object v1, v1, Lgbu;->a:Ljava/lang/String;

    iget-object v2, v0, Lglw;->a:Lgbu;

    iget-object v2, v2, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    invoke-direct {p0, v0}, Lcfu;->a(Lglw;)V

    .line 144
    :cond_0
    return-void
.end method

.method private handlePlaylistProgress(Lblr;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 148
    iget-object v0, p1, Lblr;->a:Lglw;

    .line 149
    iget-object v1, p0, Lcfu;->c:Lgbu;

    iget-object v1, v1, Lgbu;->a:Ljava/lang/String;

    iget-object v2, v0, Lglw;->a:Lgbu;

    iget-object v2, v2, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    invoke-direct {p0, v0}, Lcfu;->a(Lglw;)V

    .line 152
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 4

    .prologue
    .line 43
    check-cast p2, Lgbu;

    iput-object p2, p0, Lcfu;->c:Lgbu;

    iget-object v0, p0, Lcfu;->m:Landroid/widget/TextView;

    iget-object v1, p2, Lgbu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcfu;->n:Landroid/widget/TextView;

    iget-object v1, p2, Lgbu;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcfu;->b:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget v1, p2, Lgbu;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcfu;->e:Leyp;

    iget-object v1, p2, Lgbu;->h:Landroid/net/Uri;

    iget-object v2, p0, Lcfu;->b:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    iget-object v3, p0, Lcfu;->q:Lcfw;

    invoke-static {v0, v1, v2, v3}, Leyi;->a(Leyp;Landroid/net/Uri;Landroid/widget/ImageView;Leyo;)V

    iget-object v0, p0, Lcfu;->i:Lboi;

    iget-object v1, p0, Lcfu;->r:Landroid/view/View;

    invoke-static {v0, v1, p2}, La;->a(Lboi;Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcfu;->f:Lgnd;

    iget-object v1, p2, Lgbu;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgnd;->c(Ljava/lang/String;)Lglw;

    move-result-object v0

    invoke-direct {p0, v0}, Lcfu;->a(Lglw;)V

    iget-object v0, p0, Lcfu;->j:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
