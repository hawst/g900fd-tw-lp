.class public Lfkg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lhjj;

.field private b:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lfkh;

    invoke-direct {v0}, Lfkh;-><init>()V

    sput-object v0, Lfkg;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lhjj;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjj;

    iput-object v0, p0, Lfkg;->a:Lhjj;

    .line 29
    return-void
.end method

.method public static a([B)Lfkg;
    .locals 2

    .prologue
    .line 55
    :try_start_0
    new-instance v0, Lhjj;

    invoke-direct {v0}, Lhjj;-><init>()V

    .line 57
    new-instance v1, Lfkg;

    invoke-static {v0, p0}, Lidh;->a(Lidh;[B)Lidh;

    move-result-object v0

    check-cast v0, Lhjj;

    invoke-direct {v1, v0}, Lfkg;-><init>(Lhjj;)V
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 59
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 32
    iget-object v0, p0, Lfkg;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfkg;->b:Ljava/util/List;

    .line 34
    iget-object v0, p0, Lfkg;->a:Lhjj;

    iget-object v1, v0, Lhjj;->a:[Lhjq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 35
    iget-object v4, p0, Lfkg;->b:Ljava/util/List;

    new-instance v5, Lfkf;

    iget-object v3, v3, Lhjq;->b:Lhjo;

    invoke-direct {v5, v3}, Lfkf;-><init>(Lhjo;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_0
    iget-object v0, p0, Lfkg;->b:Ljava/util/List;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lfkg;->a:Lhjj;

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    .line 94
    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 96
    return-void
.end method
