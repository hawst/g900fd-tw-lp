.class final Ldbq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field a:Z

.field private final b:Ljava/lang/String;

.field private synthetic c:Ldbo;


# direct methods
.method constructor <init>(Ldbo;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Ldbq;->c:Ldbo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput-object p2, p0, Ldbq;->b:Ljava/lang/String;

    .line 168
    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 3

    .prologue
    .line 205
    iget-boolean v0, p0, Ldbq;->a:Z

    if-nez v0, :cond_0

    .line 206
    const-string v1, "Couldn\'t retrieve ad overlay for video "

    iget-object v0, p0, Ldbq;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 208
    :cond_0
    return-void

    .line 206
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 161
    check-cast p1, Lfnx;

    iget-boolean v0, p0, Ldbq;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lfnx;->a()Lgyq;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbq;->c:Ldbo;

    iget-object v0, v0, Ldbo;->d:Lfoy;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lfii;

    invoke-virtual {p1}, Lfnx;->a()Lgyq;

    move-result-object v1

    invoke-direct {v0, v1}, Lfii;-><init>(Lgyq;)V

    iget-object v1, p0, Ldbq;->c:Ldbo;

    iget-object v1, v1, Ldbo;->d:Lfoy;

    iget-object v1, v1, Lfoy;->W:Lfkg;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldbq;->c:Ldbo;

    iget-object v1, v1, Ldbo;->d:Lfoy;

    iget-object v1, v1, Lfoy;->W:Lfkg;

    invoke-virtual {v1}, Lfkg;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_2
    iget-object v1, p0, Ldbq;->c:Ldbo;

    iget-object v1, v1, Ldbo;->a:Ldbm;

    iget-object v2, v0, Lfii;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, v0, Lfii;->a:Lgyq;

    iget-object v2, v2, Lgyq;->a:Lhgz;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lfii;->a:Lgyq;

    iget-object v2, v2, Lgyq;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfii;->b:Ljava/lang/String;

    :cond_3
    iget-object v2, v0, Lfii;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Ldbm;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lfii;->a()Lfnb;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Ldbq;->c:Ldbo;

    new-instance v2, Ldbp;

    iget-object v3, p0, Ldbq;->c:Ldbo;

    invoke-direct {v2, v3}, Ldbp;-><init>(Ldbo;)V

    invoke-static {v2}, Leue;->a(Leuc;)Leue;

    move-result-object v2

    iput-object v2, v1, Ldbo;->f:Leue;

    iget-object v1, p0, Ldbq;->c:Ldbo;

    iget-object v1, v1, Ldbo;->b:Leyp;

    invoke-virtual {v0}, Lfii;->a()Lfnb;

    move-result-object v2

    iget-object v2, v2, Lfnb;->a:Landroid/net/Uri;

    iget-object v3, p0, Ldbq;->c:Ldbo;

    iget-object v3, v3, Ldbo;->c:Landroid/os/Handler;

    iget-object v4, p0, Ldbq;->c:Ldbo;

    iget-object v4, v4, Ldbo;->f:Leue;

    invoke-static {v3, v4}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    :cond_4
    iget-object v1, p0, Ldbq;->c:Ldbo;

    iget-object v2, v0, Lfii;->d:Lhog;

    if-nez v2, :cond_5

    iget-object v2, v0, Lfii;->a:Lgyq;

    iget-object v2, v2, Lgyq;->d:Lhog;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lfii;->a:Lgyq;

    iget-object v2, v2, Lgyq;->d:Lhog;

    iput-object v2, v0, Lfii;->d:Lhog;

    :cond_5
    iget-object v2, v0, Lfii;->d:Lhog;

    iput-object v2, v1, Ldbo;->e:Lhog;

    :cond_6
    iget-object v1, p0, Ldbq;->c:Ldbo;

    iget-object v2, v0, Lfii;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, v0, Lfii;->a:Lgyq;

    iget-object v2, v2, Lgyq;->b:Lhgz;

    if-eqz v2, :cond_7

    iget-object v2, v0, Lfii;->a:Lgyq;

    iget-object v2, v2, Lgyq;->b:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfii;->c:Ljava/lang/String;

    :cond_7
    iget-object v0, v0, Lfii;->c:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ldbq;->c:Ldbo;

    iget-object v0, v0, Ldbo;->d:Lfoy;

    iget-object v0, v0, Lfoy;->t:Landroid/net/Uri;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Ldbo;->a(Z)V

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method
