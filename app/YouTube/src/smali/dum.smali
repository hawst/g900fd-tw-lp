.class final enum Ldum;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldum;

.field public static final enum b:Ldum;

.field public static final enum c:Ldum;

.field public static final enum d:Ldum;

.field private static final synthetic e:[Ldum;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Ldum;

    const-string v1, "CREATED"

    invoke-direct {v0, v1, v2}, Ldum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldum;->a:Ldum;

    new-instance v0, Ldum;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, Ldum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldum;->b:Ldum;

    new-instance v0, Ldum;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v4}, Ldum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldum;->c:Ldum;

    new-instance v0, Ldum;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v5}, Ldum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldum;->d:Ldum;

    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [Ldum;

    sget-object v1, Ldum;->a:Ldum;

    aput-object v1, v0, v2

    sget-object v1, Ldum;->b:Ldum;

    aput-object v1, v0, v3

    sget-object v1, Ldum;->c:Ldum;

    aput-object v1, v0, v4

    sget-object v1, Ldum;->d:Ldum;

    aput-object v1, v0, v5

    sput-object v0, Ldum;->e:[Ldum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldum;
    .locals 1

    .prologue
    .line 61
    const-class v0, Ldum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldum;

    return-object v0
.end method

.method public static values()[Ldum;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Ldum;->e:[Ldum;

    invoke-virtual {v0}, [Ldum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldum;

    return-object v0
.end method
