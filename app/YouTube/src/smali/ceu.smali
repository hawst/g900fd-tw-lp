.class public final Lceu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Lfvi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const v0, 0x7f04008f

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lceu;->a:Landroid/view/View;

    .line 31
    iget-object v0, p0, Lceu;->a:Landroid/view/View;

    const v1, 0x7f080223

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lceu;->b:Landroid/widget/TextView;

    .line 32
    iget-object v0, p0, Lceu;->a:Landroid/view/View;

    const v1, 0x7f080224

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lceu;->c:Landroid/widget/TextView;

    .line 33
    iget-object v0, p0, Lceu;->a:Landroid/view/View;

    const v1, 0x7f080222

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lceu;->d:Landroid/widget/ImageView;

    .line 34
    new-instance v0, Lfvi;

    iget-object v1, p0, Lceu;->d:Landroid/widget/ImageView;

    invoke-direct {v0, p2, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lceu;->e:Lfvi;

    .line 35
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    .line 22
    check-cast p2, Lfjq;

    iget-object v0, p0, Lceu;->b:Landroid/widget/TextView;

    iget-object v1, p2, Lfjq;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    iget-object v1, p2, Lfjq;->a:Lhez;

    iget-object v1, v1, Lhez;->c:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjq;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v1, p2, Lfjq;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lceu;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lfjq;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfjq;->a:Lhez;

    iget-object v1, v1, Lhez;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjq;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfjq;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lceu;->e:Lfvi;

    iget-object v1, p2, Lfjq;->d:Lfnc;

    if-nez v1, :cond_2

    new-instance v1, Lfnc;

    iget-object v2, p2, Lfjq;->a:Lhez;

    iget-object v2, v2, Lhez;->b:Lhxf;

    invoke-direct {v1, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, p2, Lfjq;->d:Lfnc;

    :cond_2
    iget-object v1, p2, Lfjq;->d:Lfnc;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    iget-object v0, p0, Lceu;->a:Landroid/view/View;

    return-object v0
.end method
