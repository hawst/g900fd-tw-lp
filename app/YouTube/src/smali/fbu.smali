.class public final Lfbu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgkb;


# instance fields
.field final a:Lfcd;

.field final b:Landroid/content/Context;

.field volatile c:Ljava/util/concurrent/ConcurrentHashMap;

.field private final d:Ljava/util/concurrent/Executor;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v0, p0, Lfbu;->b:Landroid/content/Context;

    .line 53
    iput-object v0, p0, Lfbu;->a:Lfcd;

    .line 54
    iput-object v0, p0, Lfbu;->d:Ljava/util/concurrent/Executor;

    .line 55
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lfbu;->c:Ljava/util/concurrent/ConcurrentHashMap;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfcd;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lfbu;->b:Landroid/content/Context;

    .line 44
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfcd;

    iput-object v0, p0, Lfbu;->a:Lfcd;

    .line 45
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lfbu;->d:Ljava/util/concurrent/Executor;

    .line 46
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lfbu;->c:Ljava/util/concurrent/ConcurrentHashMap;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Lgiv;Z)Ljava/util/concurrent/Future;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 96
    invoke-virtual {p1}, Lgiv;->c()Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-static {}, Leud;->a()Leud;

    move-result-object v2

    .line 98
    iget-object v0, p0, Lfbu;->a:Lfcd;

    invoke-virtual {v0, v1}, Lfcd;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Account removed from device."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 101
    invoke-static {v0}, Lfbw;->a(Ljava/lang/Exception;)Lfbw;

    move-result-object v0

    .line 99
    invoke-virtual {v2, v3, v0}, Leud;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 132
    :goto_0
    return-object v2

    .line 105
    :cond_0
    iget-object v0, p0, Lfbu;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 106
    if-nez p2, :cond_1

    if-eqz v0, :cond_1

    .line 107
    invoke-static {v0}, Lfbw;->a(Ljava/lang/String;)Lfbw;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Leud;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 110
    :cond_1
    iget-object v0, p0, Lfbu;->d:Ljava/util/concurrent/Executor;

    new-instance v3, Lfbv;

    invoke-direct {v3, p0, v1, v2}, Lfbv;-><init>(Lfbu;Ljava/lang/String;Leud;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 139
    iget-object v0, p0, Lfbu;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 140
    iget-object v2, p0, Lfbu;->b:Landroid/content/Context;

    invoke-static {v2, v0}, Lehi;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 142
    :cond_0
    iget-object v0, p0, Lfbu;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 143
    return-void
.end method
