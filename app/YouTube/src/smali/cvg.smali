.class public final Lcvg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbj;


# instance fields
.field private synthetic a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbe;

    move-result-object v0

    iget-object v1, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    .line 246
    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcws;

    move-result-object v1

    invoke-virtual {v1}, Lcws;->p()Z

    move-result v1

    iget-object v2, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcws;

    move-result-object v2

    invoke-virtual {v2}, Lcws;->q()Z

    move-result v2

    .line 245
    invoke-virtual {v0, v1, v2}, Ldbe;->a(ZZ)V

    .line 247
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 308
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lezf;

    move-result-object v0

    invoke-virtual {v0}, Lezf;->a()V

    .line 309
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;

    move-result-object v0

    long-to-int v1, p1

    invoke-interface {v0, v1}, Ldbt;->a(I)V

    .line 310
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lezf;

    move-result-object v0

    invoke-virtual {v0}, Lezf;->a()V

    .line 252
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->a()V

    .line 253
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lezf;

    move-result-object v0

    invoke-virtual {v0}, Lezf;->a()V

    .line 258
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->b()V

    .line 259
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lezf;

    move-result-object v0

    invoke-virtual {v0}, Lezf;->a()V

    .line 264
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->f(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->b()V

    .line 269
    :goto_0
    return-void

    .line 267
    :cond_0
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->a()V

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 279
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lezf;

    move-result-object v0

    invoke-virtual {v0}, Lezf;->a()V

    .line 281
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)J

    move-result-wide v0

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    .line 284
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcws;

    move-result-object v0

    invoke-virtual {v0}, Lcws;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->e()V

    .line 289
    :goto_0
    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ldbt;->a(I)V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lezf;

    move-result-object v0

    invoke-virtual {v0}, Lezf;->a()V

    .line 294
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->d()V

    .line 295
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lezf;

    move-result-object v0

    invoke-virtual {v0}, Lezf;->a()V

    .line 303
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopSelf()V

    .line 304
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lezf;

    move-result-object v0

    invoke-virtual {v0}, Lezf;->a()V

    .line 274
    iget-object v0, p0, Lcvg;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->l()V

    .line 275
    return-void
.end method
