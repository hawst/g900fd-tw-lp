.class public final Lgyp;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4269
    invoke-direct {p0}, Lidf;-><init>()V

    .line 4272
    iput v1, p0, Lgyp;->a:I

    .line 4275
    iput v1, p0, Lgyp;->b:I

    .line 4278
    iput v1, p0, Lgyp;->c:I

    .line 4281
    const-string v0, ""

    iput-object v0, p0, Lgyp;->d:Ljava/lang/String;

    .line 4284
    iput-boolean v1, p0, Lgyp;->e:Z

    .line 4269
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 4345
    const/4 v0, 0x0

    .line 4346
    iget v1, p0, Lgyp;->a:I

    if-eqz v1, :cond_0

    .line 4347
    const/4 v0, 0x1

    iget v1, p0, Lgyp;->a:I

    .line 4348
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4350
    :cond_0
    iget v1, p0, Lgyp;->b:I

    if-eqz v1, :cond_1

    .line 4351
    const/4 v1, 0x2

    iget v2, p0, Lgyp;->b:I

    .line 4352
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4354
    :cond_1
    iget v1, p0, Lgyp;->c:I

    if-eqz v1, :cond_2

    .line 4355
    const/4 v1, 0x3

    iget v2, p0, Lgyp;->c:I

    .line 4356
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4358
    :cond_2
    iget-object v1, p0, Lgyp;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4359
    const/4 v1, 0x5

    iget-object v2, p0, Lgyp;->d:Ljava/lang/String;

    .line 4360
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4362
    :cond_3
    iget-boolean v1, p0, Lgyp;->e:Z

    if-eqz v1, :cond_4

    .line 4363
    const/4 v1, 0x6

    iget-boolean v2, p0, Lgyp;->e:Z

    .line 4364
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4366
    :cond_4
    iget-object v1, p0, Lgyp;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4367
    iput v0, p0, Lgyp;->J:I

    .line 4368
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4265
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyp;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyp;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyp;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lgyp;->a:I

    goto :goto_0

    :cond_3
    iput v2, p0, Lgyp;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-ne v0, v1, :cond_5

    :cond_4
    iput v0, p0, Lgyp;->b:I

    goto/16 :goto_0

    :cond_5
    iput v2, p0, Lgyp;->b:I

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-ne v0, v6, :cond_7

    :cond_6
    iput v0, p0, Lgyp;->c:I

    goto/16 :goto_0

    :cond_7
    iput v2, p0, Lgyp;->c:I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgyp;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgyp;->e:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 4324
    iget v0, p0, Lgyp;->a:I

    if-eqz v0, :cond_0

    .line 4325
    const/4 v0, 0x1

    iget v1, p0, Lgyp;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 4327
    :cond_0
    iget v0, p0, Lgyp;->b:I

    if-eqz v0, :cond_1

    .line 4328
    const/4 v0, 0x2

    iget v1, p0, Lgyp;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 4330
    :cond_1
    iget v0, p0, Lgyp;->c:I

    if-eqz v0, :cond_2

    .line 4331
    const/4 v0, 0x3

    iget v1, p0, Lgyp;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 4333
    :cond_2
    iget-object v0, p0, Lgyp;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4334
    const/4 v0, 0x5

    iget-object v1, p0, Lgyp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 4336
    :cond_3
    iget-boolean v0, p0, Lgyp;->e:Z

    if-eqz v0, :cond_4

    .line 4337
    const/4 v0, 0x6

    iget-boolean v1, p0, Lgyp;->e:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 4339
    :cond_4
    iget-object v0, p0, Lgyp;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 4341
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4299
    if-ne p1, p0, :cond_1

    .line 4307
    :cond_0
    :goto_0
    return v0

    .line 4300
    :cond_1
    instance-of v2, p1, Lgyp;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 4301
    :cond_2
    check-cast p1, Lgyp;

    .line 4302
    iget v2, p0, Lgyp;->a:I

    iget v3, p1, Lgyp;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgyp;->b:I

    iget v3, p1, Lgyp;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgyp;->c:I

    iget v3, p1, Lgyp;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyp;->d:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyp;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 4305
    :goto_1
    iget-boolean v2, p0, Lgyp;->e:Z

    iget-boolean v3, p1, Lgyp;->e:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyp;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyp;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 4307
    goto :goto_0

    .line 4302
    :cond_4
    iget-object v2, p0, Lgyp;->d:Ljava/lang/String;

    iget-object v3, p1, Lgyp;->d:Ljava/lang/String;

    .line 4305
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyp;->I:Ljava/util/List;

    iget-object v3, p1, Lgyp;->I:Ljava/util/List;

    .line 4307
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4311
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 4313
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgyp;->a:I

    add-int/2addr v0, v2

    .line 4314
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgyp;->b:I

    add-int/2addr v0, v2

    .line 4315
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgyp;->c:I

    add-int/2addr v0, v2

    .line 4316
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyp;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 4317
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lgyp;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    add-int/2addr v0, v2

    .line 4318
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgyp;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 4319
    return v0

    .line 4316
    :cond_0
    iget-object v0, p0, Lgyp;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4317
    :cond_1
    const/4 v0, 0x2

    goto :goto_1

    .line 4318
    :cond_2
    iget-object v1, p0, Lgyp;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
