.class public final Ldjx;
.super Ldjr;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final l:Ldaw;

.field final m:Lglm;

.field final n:Lgnd;

.field final o:[B

.field final p:Ljava/util/concurrent/Executor;

.field final q:Ljava/lang/String;

.field final r:Ljava/lang/String;

.field volatile s:Lgbu;

.field volatile t:Ljava/util/List;

.field volatile u:[I

.field volatile v:I

.field private final w:Ljava/util/concurrent/Executor;

.field private x:I

.field private y:Ldjy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcvq;Levn;Lcwq;Leyt;Lfac;Ljava/util/concurrent/Executor;Ldaw;Lglm;Lgnd;Ldld;)V
    .locals 13

    .prologue
    .line 160
    new-instance v8, Leva;

    invoke-direct {v8}, Leva;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Ldjx;-><init>(Landroid/content/Context;Lcvq;Levn;Lcwq;Leyt;Lfac;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ldaw;Lglm;Lgnd;Ldld;)V

    .line 172
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcvq;Levn;Lcwq;Leyt;Lfac;Ljava/util/concurrent/Executor;Ldaw;Lglm;Lgnd;Lgoh;)V
    .locals 13

    .prologue
    .line 89
    new-instance v8, Leva;

    invoke-direct {v8}, Leva;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Ldjx;-><init>(Landroid/content/Context;Lcvq;Levn;Lcwq;Leyt;Lfac;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ldaw;Lglm;Lgnd;Lgoh;)V

    .line 101
    sget-object v0, Lgok;->a:Lgok;

    invoke-virtual {p0, v0}, Ldjx;->a(Lgok;)V

    .line 102
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcvq;Levn;Lcwq;Leyt;Lfac;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ldaw;Lglm;Lgnd;Ldld;)V
    .locals 8

    .prologue
    .line 188
    move-object/from16 v0, p12

    iget-object v7, v0, Ldld;->i:Ldkz;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v1 .. v7}, Ldjr;-><init>(Lcvq;Levn;Lcwq;Leyt;Lfac;Ldkz;)V

    .line 194
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Ldjx;->a:Landroid/content/Context;

    .line 196
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldaw;

    iput-object v1, p0, Ldjx;->l:Ldaw;

    .line 197
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lglm;

    iput-object v1, p0, Ldjx;->m:Lglm;

    .line 198
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgnd;

    iput-object v1, p0, Ldjx;->n:Lgnd;

    .line 199
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Ldjx;->w:Ljava/util/concurrent/Executor;

    .line 200
    move-object/from16 v0, p12

    iget-object v1, v0, Ldld;->e:[B

    .line 201
    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Ldjx;->o:[B

    .line 202
    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Ldjx;->p:Ljava/util/concurrent/Executor;

    .line 204
    move-object/from16 v0, p12

    iget-object v1, v0, Ldld;->c:Ljava/lang/String;

    iput-object v1, p0, Ldjx;->q:Ljava/lang/String;

    .line 205
    move-object/from16 v0, p12

    iget-object v1, v0, Ldld;->d:Ljava/lang/String;

    iput-object v1, p0, Ldjx;->r:Ljava/lang/String;

    .line 206
    move-object/from16 v0, p12

    iget-object v1, v0, Ldld;->a:Lfrl;

    iput-object v1, p0, Ldjx;->g:Lfrl;

    .line 207
    move-object/from16 v0, p12

    iget-object v1, v0, Ldld;->b:Lfnx;

    iput-object v1, p0, Ldjx;->h:Lfnx;

    .line 208
    move-object/from16 v0, p12

    iget v1, v0, Ldld;->f:I

    iput v1, p0, Ldjx;->v:I

    .line 209
    move-object/from16 v0, p12

    iget v1, v0, Ldld;->g:I

    iput v1, p0, Ldjx;->x:I

    .line 210
    move-object/from16 v0, p12

    iget-boolean v1, v0, Ldld;->h:Z

    iput-boolean v1, p0, Ldjx;->j:Z

    .line 211
    sget-object v1, Lgok;->a:Lgok;

    invoke-virtual {p0, v1}, Ldjx;->a(Lgok;)V

    .line 212
    iget-object v1, p0, Ldjx;->g:Lfrl;

    if-eqz v1, :cond_0

    .line 213
    sget-object v1, Lgok;->d:Lgok;

    invoke-virtual {p0, v1}, Ldjx;->a(Lgok;)V

    .line 214
    iget-object v1, p0, Ldjx;->h:Lfnx;

    if-eqz v1, :cond_0

    .line 215
    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {p0, v1}, Ldjx;->a(Lgok;)V

    .line 218
    :cond_0
    invoke-virtual {p0}, Ldjx;->h()V

    .line 219
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcvq;Levn;Lcwq;Leyt;Lfac;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ldaw;Lglm;Lgnd;Lgoh;)V
    .locals 7

    .prologue
    .line 118
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Ldjr;-><init>(Lcvq;Levn;Lcwq;Leyt;Lfac;)V

    .line 119
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Ldjx;->a:Landroid/content/Context;

    .line 120
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldaw;

    iput-object v1, p0, Ldjx;->l:Ldaw;

    .line 121
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lglm;

    iput-object v1, p0, Ldjx;->m:Lglm;

    .line 122
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgnd;

    iput-object v1, p0, Ldjx;->n:Lgnd;

    .line 123
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Ldjx;->w:Ljava/util/concurrent/Executor;

    .line 125
    invoke-virtual/range {p12 .. p12}, Lgoh;->e()[B

    move-result-object v1

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Ldjx;->o:[B

    .line 126
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Ldjx;->p:Ljava/util/concurrent/Executor;

    .line 128
    move-object/from16 v0, p12

    iget-object v1, v0, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    const/4 v1, 0x0

    iput-object v1, p0, Ldjx;->q:Ljava/lang/String;

    .line 130
    move-object/from16 v0, p12

    iget-object v1, v0, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->c:Ljava/lang/String;

    iput-object v1, p0, Ldjx;->r:Ljava/lang/String;

    .line 140
    :goto_0
    move-object/from16 v0, p12

    iget-object v1, v0, Lgoh;->a:Leaa;

    iget v1, v1, Leaa;->d:I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Ldjx;->x:I

    .line 141
    iget v1, p0, Ldjx;->x:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Ldjx;->v:I

    .line 142
    invoke-virtual {p0}, Ldjx;->h()V

    .line 143
    return-void

    .line 132
    :cond_0
    move-object/from16 v0, p12

    iget-object v1, v0, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->a:Ljava/lang/String;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Ldjx;->q:Ljava/lang/String;

    .line 133
    const/4 v1, 0x0

    iput-object v1, p0, Ldjx;->r:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Ldjx;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ldjx;->w()V

    return-void
.end method

.method private declared-synchronized b(I)V
    .locals 2

    .prologue
    .line 421
    monitor-enter p0

    :try_start_0
    sget-object v0, Lgok;->b:Lgok;

    invoke-virtual {p0, v0}, Ldjx;->a(Lgok;)V

    .line 422
    iget-object v0, p0, Ldjx;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 424
    :goto_0
    const/4 v1, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ldjx;->x:I

    .line 425
    new-instance v0, Ldkc;

    invoke-direct {v0, p0, p1}, Ldkc;-><init>(Ldjx;I)V

    iput-object v0, p0, Ldjx;->y:Ldjy;

    .line 426
    iget-object v0, p0, Ldjx;->w:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Ldjx;->y:Ldjy;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    monitor-exit p0

    return-void

    .line 422
    :cond_0
    const v0, 0x7fffffff

    goto :goto_0

    .line 421
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private v()V
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Ldjx;->j:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ldjx;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldjx;->b(I)V

    .line 279
    :goto_0
    return-void

    .line 277
    :cond_0
    iget v0, p0, Ldjx;->v:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Ldjx;->b(I)V

    goto :goto_0
.end method

.method private w()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 336
    iget-object v0, p0, Ldjx;->u:[I

    if-eqz v0, :cond_2

    .line 338
    iget v0, p0, Ldjx;->v:I

    .line 339
    iget-boolean v1, p0, Ldjx;->k:Z

    if-eqz v1, :cond_0

    .line 340
    iget-object v0, p0, Ldjx;->u:[I

    iget v1, p0, Ldjx;->v:I

    aget v0, v0, v1

    .line 344
    :cond_0
    iget-object v1, p0, Ldjx;->u:[I

    array-length v3, v1

    move v1, v2

    .line 345
    :goto_0
    if-ge v1, v3, :cond_1

    .line 346
    iget-object v4, p0, Ldjx;->u:[I

    aput v1, v4, v1

    .line 345
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 350
    :cond_1
    iget-object v1, p0, Ldjx;->u:[I

    aput v0, v1, v2

    .line 351
    iget-object v1, p0, Ldjx;->u:[I

    aput v2, v1, v0

    .line 353
    iget-object v0, p0, Ldjx;->e:Lfac;

    iget-object v1, p0, Ldjx;->u:[I

    const/4 v4, 0x1

    invoke-virtual {v0}, Lfac;->a()Ljava/security/SecureRandom;

    move-result-object v5

    if-eqz v1, :cond_2

    if-nez v5, :cond_3

    .line 355
    :cond_2
    return-void

    .line 353
    :cond_3
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-ltz v3, :cond_2

    if-ge v2, v3, :cond_2

    array-length v0, v1

    if-eqz v0, :cond_2

    if-ge v2, v0, :cond_2

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_2

    add-int v3, v0, v2

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v5, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/2addr v4, v2

    aget v6, v1, v3

    aget v7, v1, v4

    aput v7, v1, v3

    aput v6, v1, v4

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private x()Z
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Ldjx;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    iget v0, p0, Ldjx;->v:I

    iget-object v1, p0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 375
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldjx;->y:Ldjy;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Ldjx;->y:Ldjy;

    invoke-static {v0}, Ldjy;->a(Ldjy;)V

    .line 377
    const/4 v0, 0x0

    iput-object v0, p0, Ldjx;->y:Ldjy;

    .line 379
    :cond_0
    iget-object v0, p0, Ldjx;->g:Lfrl;

    if-eqz v0, :cond_2

    .line 380
    iget-object v0, p0, Ldjx;->h:Lfnx;

    if-eqz v0, :cond_1

    .line 381
    sget-object v0, Lgok;->e:Lgok;

    iput-object v0, p0, Ldjx;->f:Lgok;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    :goto_0
    monitor-exit p0

    return-void

    .line 383
    :cond_1
    :try_start_1
    sget-object v0, Lgok;->d:Lgok;

    iput-object v0, p0, Ldjx;->f:Lgok;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 375
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 386
    :cond_2
    :try_start_2
    sget-object v0, Lgok;->a:Lgok;

    invoke-virtual {p0, v0}, Ldjx;->a(Lgok;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method final a(I)V
    .locals 2

    .prologue
    .line 412
    new-instance v0, Ldjy;

    iget v1, p0, Ldjx;->x:I

    invoke-direct {v0, p0, v1}, Ldjy;-><init>(Ldjx;I)V

    iput-object v0, p0, Ldjx;->y:Ldjy;

    .line 413
    iget-object v0, p0, Ldjx;->w:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Ldjx;->y:Ldjy;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 414
    return-void
.end method

.method protected final a(Lgok;)V
    .locals 0

    .prologue
    .line 223
    invoke-super {p0, p1}, Ldjr;->a(Lgok;)V

    .line 224
    invoke-virtual {p0}, Ldjx;->g()V

    .line 225
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 324
    if-eqz p1, :cond_0

    .line 326
    invoke-direct {p0}, Ldjx;->w()V

    .line 327
    const/4 v0, 0x0

    iput v0, p0, Ldjx;->v:I

    .line 331
    :goto_0
    iput-boolean p1, p0, Ldjx;->k:Z

    .line 332
    invoke-virtual {p0}, Ldjx;->g()V

    .line 333
    return-void

    .line 329
    :cond_0
    iget-object v0, p0, Ldjx;->u:[I

    iget v1, p0, Ldjx;->v:I

    aget v0, v0, v1

    iput v0, p0, Ldjx;->v:I

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 359
    iput-boolean p1, p0, Ldjx;->j:Z

    .line 360
    invoke-virtual {p0}, Ldjx;->g()V

    .line 361
    return-void
.end method

.method public final f()Ldkg;
    .locals 10

    .prologue
    .line 229
    new-instance v0, Ldld;

    iget-object v1, p0, Ldjx;->q:Ljava/lang/String;

    iget-object v2, p0, Ldjx;->r:Ljava/lang/String;

    iget-object v3, p0, Ldjx;->o:[B

    iget-object v4, p0, Ldjx;->g:Lfrl;

    iget-object v5, p0, Ldjx;->h:Lfnx;

    iget v6, p0, Ldjx;->v:I

    iget v7, p0, Ldjx;->x:I

    iget-boolean v8, p0, Ldjx;->j:Z

    .line 238
    invoke-super {p0}, Ldjr;->f()Ldkg;

    move-result-object v9

    check-cast v9, Ldkz;

    invoke-direct/range {v0 .. v9}, Ldld;-><init>(Ljava/lang/String;Ljava/lang/String;[BLfrl;Lfnx;IIZLdkz;)V

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Ldjx;->a()V

    .line 244
    invoke-super {p0}, Ldjr;->k()V

    .line 245
    iget v0, p0, Ldjx;->x:I

    invoke-direct {p0, v0}, Ldjx;->b(I)V

    .line 246
    return-void
.end method

.method protected final m()Z
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x1

    return v0
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 250
    invoke-virtual {p0}, Ldjx;->a()V

    .line 251
    invoke-super {p0}, Ldjr;->n()V

    .line 252
    invoke-direct {p0}, Ldjx;->v()V

    .line 253
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Ldjx;->a()V

    .line 258
    invoke-super {p0}, Ldjr;->o()V

    .line 259
    iget-boolean v0, p0, Ldjx;->j:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ldjx;->v:I

    if-nez v0, :cond_0

    iget-object v0, p0, Ldjx;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Ldjx;->b(I)V

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_0
    iget v0, p0, Ldjx;->v:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Ldjx;->b(I)V

    goto :goto_0
.end method

.method public final p()V
    .locals 0

    .prologue
    .line 268
    invoke-virtual {p0}, Ldjx;->a()V

    .line 269
    invoke-super {p0}, Ldjr;->p()V

    .line 270
    invoke-direct {p0}, Ldjx;->v()V

    .line 271
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 283
    invoke-virtual {p0}, Ldjx;->a()V

    .line 284
    invoke-super {p0}, Ldjr;->q()V

    .line 285
    iget v0, p0, Ldjx;->x:I

    invoke-direct {p0, v0}, Ldjx;->b(I)V

    .line 286
    return-void
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 402
    invoke-virtual {p0}, Ldjx;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldjx;->r:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final s_()Z
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x0

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 392
    iget v0, p0, Ldjx;->v:I

    return v0
.end method

.method public final t_()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 295
    iget-object v1, p0, Ldjx;->t:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 296
    iget-boolean v1, p0, Ldjx;->j:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Ldjx;->x()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 298
    :cond_1
    return v0
.end method

.method u()Z
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Ldjx;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u_()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 303
    iget-object v1, p0, Ldjx;->t:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 304
    iget-boolean v1, p0, Ldjx;->j:Z

    if-nez v1, :cond_0

    iget v1, p0, Ldjx;->v:I

    if-lez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 306
    :cond_1
    return v0
.end method

.method public final v_()Z
    .locals 1

    .prologue
    .line 311
    invoke-virtual {p0}, Ldjx;->t_()Z

    move-result v0

    return v0
.end method
