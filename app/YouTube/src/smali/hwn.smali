.class public final Lhwn;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:[Lhgy;

.field public c:[Lhgy;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Lhli;

.field private g:[Lhgy;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lidf;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhwn;->a:J

    sget-object v0, Lhgy;->a:[Lhgy;

    iput-object v0, p0, Lhwn;->b:[Lhgy;

    sget-object v0, Lhgy;->a:[Lhgy;

    iput-object v0, p0, Lhwn;->c:[Lhgy;

    const-string v0, ""

    iput-object v0, p0, Lhwn;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhwn;->e:Ljava/lang/String;

    sget-object v0, Lhgy;->a:[Lhgy;

    iput-object v0, p0, Lhwn;->g:[Lhgy;

    const-string v0, ""

    iput-object v0, p0, Lhwn;->h:Ljava/lang/String;

    sget-object v0, Lhli;->a:[Lhli;

    iput-object v0, p0, Lhwn;->f:[Lhli;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-wide v2, p0, Lhwn;->a:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    iget-wide v2, p0, Lhwn;->a:J

    invoke-static {v0, v2, v3}, Lidd;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhwn;->b:[Lhgy;

    if-eqz v2, :cond_1

    iget-object v3, p0, Lhwn;->b:[Lhgy;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    if-eqz v5, :cond_0

    const/4 v6, 0x2

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lhwn;->c:[Lhgy;

    if-eqz v2, :cond_3

    iget-object v3, p0, Lhwn;->c:[Lhgy;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    if-eqz v5, :cond_2

    const/4 v6, 0x3

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lhwn;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x4

    iget-object v3, p0, Lhwn;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhwn;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x5

    iget-object v3, p0, Lhwn;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lhwn;->g:[Lhgy;

    if-eqz v2, :cond_7

    iget-object v3, p0, Lhwn;->g:[Lhgy;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    if-eqz v5, :cond_6

    const/4 v6, 0x6

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhwn;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x7

    iget-object v3, p0, Lhwn;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lhwn;->f:[Lhli;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lhwn;->f:[Lhli;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    if-eqz v4, :cond_9

    const/16 v5, 0x8

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_a
    iget-object v1, p0, Lhwn;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhwn;->J:I

    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhwn;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhwn;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhwn;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lhwn;->a:J

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhwn;->b:[Lhgy;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgy;

    iget-object v3, p0, Lhwn;->b:[Lhgy;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhwn;->b:[Lhgy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhwn;->b:[Lhgy;

    :goto_2
    iget-object v2, p0, Lhwn;->b:[Lhgy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhwn;->b:[Lhgy;

    new-instance v3, Lhgy;

    invoke-direct {v3}, Lhgy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwn;->b:[Lhgy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhwn;->b:[Lhgy;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhwn;->b:[Lhgy;

    new-instance v3, Lhgy;

    invoke-direct {v3}, Lhgy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwn;->b:[Lhgy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhwn;->c:[Lhgy;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgy;

    iget-object v3, p0, Lhwn;->c:[Lhgy;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lhwn;->c:[Lhgy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lhwn;->c:[Lhgy;

    :goto_4
    iget-object v2, p0, Lhwn;->c:[Lhgy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lhwn;->c:[Lhgy;

    new-instance v3, Lhgy;

    invoke-direct {v3}, Lhgy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwn;->c:[Lhgy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lhwn;->c:[Lhgy;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhwn;->c:[Lhgy;

    new-instance v3, Lhgy;

    invoke-direct {v3}, Lhgy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwn;->c:[Lhgy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwn;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwn;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhwn;->g:[Lhgy;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgy;

    iget-object v3, p0, Lhwn;->g:[Lhgy;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lhwn;->g:[Lhgy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Lhwn;->g:[Lhgy;

    :goto_6
    iget-object v2, p0, Lhwn;->g:[Lhgy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lhwn;->g:[Lhgy;

    new-instance v3, Lhgy;

    invoke-direct {v3}, Lhgy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwn;->g:[Lhgy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lhwn;->g:[Lhgy;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Lhwn;->g:[Lhgy;

    new-instance v3, Lhgy;

    invoke-direct {v3}, Lhgy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwn;->g:[Lhgy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwn;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhwn;->f:[Lhli;

    if-nez v0, :cond_c

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lhli;

    iget-object v3, p0, Lhwn;->f:[Lhli;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lhwn;->f:[Lhli;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Lhwn;->f:[Lhli;

    :goto_8
    iget-object v2, p0, Lhwn;->f:[Lhli;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Lhwn;->f:[Lhli;

    new-instance v3, Lhli;

    invoke-direct {v3}, Lhli;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwn;->f:[Lhli;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Lhwn;->f:[Lhli;

    array-length v0, v0

    goto :goto_7

    :cond_d
    iget-object v2, p0, Lhwn;->f:[Lhli;

    new-instance v3, Lhli;

    invoke-direct {v3}, Lhli;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwn;->f:[Lhli;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-wide v2, p0, Lhwn;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-wide v2, p0, Lhwn;->a:J

    invoke-virtual {p1, v1, v2, v3}, Lidd;->b(IJ)V

    :cond_0
    iget-object v1, p0, Lhwn;->b:[Lhgy;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lhwn;->b:[Lhgy;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    if-eqz v4, :cond_1

    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lhwn;->c:[Lhgy;

    if-eqz v1, :cond_4

    iget-object v2, p0, Lhwn;->c:[Lhgy;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    if-eqz v4, :cond_3

    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lhwn;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x4

    iget-object v2, p0, Lhwn;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lhwn;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x5

    iget-object v2, p0, Lhwn;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_6
    iget-object v1, p0, Lhwn;->g:[Lhgy;

    if-eqz v1, :cond_8

    iget-object v2, p0, Lhwn;->g:[Lhgy;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    if-eqz v4, :cond_7

    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_8
    iget-object v1, p0, Lhwn;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x7

    iget-object v2, p0, Lhwn;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_9
    iget-object v1, p0, Lhwn;->f:[Lhli;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lhwn;->f:[Lhli;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    if-eqz v3, :cond_a

    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lhwn;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhwn;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhwn;

    iget-wide v2, p0, Lhwn;->a:J

    iget-wide v4, p1, Lhwn;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhwn;->b:[Lhgy;

    iget-object v3, p1, Lhwn;->b:[Lhgy;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhwn;->c:[Lhgy;

    iget-object v3, p1, Lhwn;->c:[Lhgy;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhwn;->d:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhwn;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhwn;->e:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhwn;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhwn;->g:[Lhgy;

    iget-object v3, p1, Lhwn;->g:[Lhgy;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhwn;->h:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhwn;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhwn;->f:[Lhli;

    iget-object v3, p1, Lhwn;->f:[Lhli;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhwn;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhwn;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhwn;->d:Ljava/lang/String;

    iget-object v3, p1, Lhwn;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhwn;->e:Ljava/lang/String;

    iget-object v3, p1, Lhwn;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhwn;->h:Ljava/lang/String;

    iget-object v3, p1, Lhwn;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhwn;->I:Ljava/util/List;

    iget-object v3, p1, Lhwn;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhwn;->a:J

    iget-wide v4, p0, Lhwn;->a:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lhwn;->b:[Lhgy;

    if-nez v2, :cond_4

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lhwn;->c:[Lhgy;

    if-nez v0, :cond_6

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhwn;->d:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwn;->e:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Lhwn;->g:[Lhgy;

    if-nez v2, :cond_a

    mul-int/lit8 v2, v0, 0x1f

    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhwn;->h:Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Lhwn;->f:[Lhli;

    if-nez v2, :cond_d

    mul-int/lit8 v2, v0, 0x1f

    :cond_3
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhwn;->I:Ljava/util/List;

    if-nez v2, :cond_f

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_4
    move v2, v0

    move v0, v1

    :goto_4
    iget-object v3, p0, Lhwn;->b:[Lhgy;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhwn;->b:[Lhgy;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v2, p0, Lhwn;->b:[Lhgy;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgy;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    move v0, v1

    :goto_6
    iget-object v3, p0, Lhwn;->c:[Lhgy;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhwn;->c:[Lhgy;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v1

    :goto_7
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_7
    iget-object v2, p0, Lhwn;->c:[Lhgy;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgy;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_8
    iget-object v0, p0, Lhwn;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lhwn;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_a
    move v2, v0

    move v0, v1

    :goto_8
    iget-object v3, p0, Lhwn;->g:[Lhgy;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhwn;->g:[Lhgy;

    aget-object v2, v2, v0

    if-nez v2, :cond_b

    move v2, v1

    :goto_9
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget-object v2, p0, Lhwn;->g:[Lhgy;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgy;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_c
    iget-object v0, p0, Lhwn;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_d
    move v2, v0

    move v0, v1

    :goto_a
    iget-object v3, p0, Lhwn;->f:[Lhli;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhwn;->f:[Lhli;

    aget-object v2, v2, v0

    if-nez v2, :cond_e

    move v2, v1

    :goto_b
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_e
    iget-object v2, p0, Lhwn;->f:[Lhli;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhli;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_f
    iget-object v1, p0, Lhwn;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_3
.end method
