.class public final Lgxw;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lgxy;

.field public b:Lgxv;

.field private c:Lhgz;

.field private d:Lhgz;

.field private e:[B

.field private f:[Lgxx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 978
    invoke-direct {p0}, Lidf;-><init>()V

    .line 1155
    sget-object v0, Lgxy;->a:[Lgxy;

    iput-object v0, p0, Lgxw;->a:[Lgxy;

    .line 1158
    iput-object v1, p0, Lgxw;->c:Lhgz;

    .line 1161
    iput-object v1, p0, Lgxw;->d:Lhgz;

    .line 1164
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lgxw;->e:[B

    .line 1167
    iput-object v1, p0, Lgxw;->b:Lgxv;

    .line 1170
    sget-object v0, Lgxx;->a:[Lgxx;

    iput-object v0, p0, Lgxw;->f:[Lgxx;

    .line 978
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1260
    .line 1261
    iget-object v0, p0, Lgxw;->a:[Lgxy;

    if-eqz v0, :cond_1

    .line 1262
    iget-object v3, p0, Lgxw;->a:[Lgxy;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1263
    if-eqz v5, :cond_0

    .line 1264
    const/4 v6, 0x1

    .line 1265
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1262
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1269
    :cond_2
    iget-object v2, p0, Lgxw;->c:Lhgz;

    if-eqz v2, :cond_3

    .line 1270
    const/4 v2, 0x2

    iget-object v3, p0, Lgxw;->c:Lhgz;

    .line 1271
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1273
    :cond_3
    iget-object v2, p0, Lgxw;->d:Lhgz;

    if-eqz v2, :cond_4

    .line 1274
    const/4 v2, 0x5

    iget-object v3, p0, Lgxw;->d:Lhgz;

    .line 1275
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1277
    :cond_4
    iget-object v2, p0, Lgxw;->e:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1278
    const/4 v2, 0x6

    iget-object v3, p0, Lgxw;->e:[B

    .line 1279
    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 1281
    :cond_5
    iget-object v2, p0, Lgxw;->b:Lgxv;

    if-eqz v2, :cond_6

    .line 1282
    const/4 v2, 0x7

    iget-object v3, p0, Lgxw;->b:Lgxv;

    .line 1283
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1285
    :cond_6
    iget-object v2, p0, Lgxw;->f:[Lgxx;

    if-eqz v2, :cond_8

    .line 1286
    iget-object v2, p0, Lgxw;->f:[Lgxx;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 1287
    if-eqz v4, :cond_7

    .line 1288
    const/16 v5, 0x8

    .line 1289
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1286
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1293
    :cond_8
    iget-object v1, p0, Lgxw;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1294
    iput v0, p0, Lgxw;->J:I

    .line 1295
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 974
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lgxw;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lgxw;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lgxw;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgxw;->a:[Lgxy;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgxy;

    iget-object v3, p0, Lgxw;->a:[Lgxy;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lgxw;->a:[Lgxy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lgxw;->a:[Lgxy;

    :goto_2
    iget-object v2, p0, Lgxw;->a:[Lgxy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lgxw;->a:[Lgxy;

    new-instance v3, Lgxy;

    invoke-direct {v3}, Lgxy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgxw;->a:[Lgxy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lgxw;->a:[Lgxy;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lgxw;->a:[Lgxy;

    new-instance v3, Lgxy;

    invoke-direct {v3}, Lgxy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgxw;->a:[Lgxy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgxw;->c:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgxw;->c:Lhgz;

    :cond_5
    iget-object v0, p0, Lgxw;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgxw;->d:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgxw;->d:Lhgz;

    :cond_6
    iget-object v0, p0, Lgxw;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lgxw;->e:[B

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lgxw;->b:Lgxv;

    if-nez v0, :cond_7

    new-instance v0, Lgxv;

    invoke-direct {v0}, Lgxv;-><init>()V

    iput-object v0, p0, Lgxw;->b:Lgxv;

    :cond_7
    iget-object v0, p0, Lgxw;->b:Lgxv;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgxw;->f:[Lgxx;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgxx;

    iget-object v3, p0, Lgxw;->f:[Lgxx;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lgxw;->f:[Lgxx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Lgxw;->f:[Lgxx;

    :goto_4
    iget-object v2, p0, Lgxw;->f:[Lgxx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lgxw;->f:[Lgxx;

    new-instance v3, Lgxx;

    invoke-direct {v3}, Lgxx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgxw;->f:[Lgxx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lgxw;->f:[Lgxx;

    array-length v0, v0

    goto :goto_3

    :cond_a
    iget-object v2, p0, Lgxw;->f:[Lgxx;

    new-instance v3, Lgxx;

    invoke-direct {v3}, Lgxx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgxw;->f:[Lgxx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1228
    iget-object v1, p0, Lgxw;->a:[Lgxy;

    if-eqz v1, :cond_1

    .line 1229
    iget-object v2, p0, Lgxw;->a:[Lgxy;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1230
    if-eqz v4, :cond_0

    .line 1231
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 1229
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1235
    :cond_1
    iget-object v1, p0, Lgxw;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 1236
    const/4 v1, 0x2

    iget-object v2, p0, Lgxw;->c:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 1238
    :cond_2
    iget-object v1, p0, Lgxw;->d:Lhgz;

    if-eqz v1, :cond_3

    .line 1239
    const/4 v1, 0x5

    iget-object v2, p0, Lgxw;->d:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 1241
    :cond_3
    iget-object v1, p0, Lgxw;->e:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1242
    const/4 v1, 0x6

    iget-object v2, p0, Lgxw;->e:[B

    invoke-virtual {p1, v1, v2}, Lidd;->a(I[B)V

    .line 1244
    :cond_4
    iget-object v1, p0, Lgxw;->b:Lgxv;

    if-eqz v1, :cond_5

    .line 1245
    const/4 v1, 0x7

    iget-object v2, p0, Lgxw;->b:Lgxv;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 1247
    :cond_5
    iget-object v1, p0, Lgxw;->f:[Lgxx;

    if-eqz v1, :cond_7

    .line 1248
    iget-object v1, p0, Lgxw;->f:[Lgxx;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 1249
    if-eqz v3, :cond_6

    .line 1250
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 1248
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1254
    :cond_7
    iget-object v0, p0, Lgxw;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 1256
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1186
    if-ne p1, p0, :cond_1

    .line 1195
    :cond_0
    :goto_0
    return v0

    .line 1187
    :cond_1
    instance-of v2, p1, Lgxw;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1188
    :cond_2
    check-cast p1, Lgxw;

    .line 1189
    iget-object v2, p0, Lgxw;->a:[Lgxy;

    iget-object v3, p1, Lgxw;->a:[Lgxy;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgxw;->c:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgxw;->c:Lhgz;

    if-nez v2, :cond_3

    .line 1190
    :goto_1
    iget-object v2, p0, Lgxw;->d:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgxw;->d:Lhgz;

    if-nez v2, :cond_3

    .line 1191
    :goto_2
    iget-object v2, p0, Lgxw;->e:[B

    iget-object v3, p1, Lgxw;->e:[B

    .line 1192
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgxw;->b:Lgxv;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgxw;->b:Lgxv;

    if-nez v2, :cond_3

    .line 1193
    :goto_3
    iget-object v2, p0, Lgxw;->f:[Lgxx;

    iget-object v3, p1, Lgxw;->f:[Lgxx;

    .line 1194
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgxw;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgxw;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 1195
    goto :goto_0

    .line 1189
    :cond_4
    iget-object v2, p0, Lgxw;->c:Lhgz;

    iget-object v3, p1, Lgxw;->c:Lhgz;

    .line 1190
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgxw;->d:Lhgz;

    iget-object v3, p1, Lgxw;->d:Lhgz;

    .line 1191
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 1192
    :cond_6
    iget-object v2, p0, Lgxw;->b:Lgxv;

    iget-object v3, p1, Lgxw;->b:Lgxv;

    .line 1193
    invoke-virtual {v2, v3}, Lgxv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    .line 1194
    :cond_7
    iget-object v2, p0, Lgxw;->I:Ljava/util/List;

    iget-object v3, p1, Lgxw;->I:Ljava/util/List;

    .line 1195
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1199
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1201
    iget-object v2, p0, Lgxw;->a:[Lgxy;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 1207
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgxw;->c:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1208
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxw;->d:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1209
    iget-object v2, p0, Lgxw;->e:[B

    if-nez v2, :cond_7

    mul-int/lit8 v2, v0, 0x1f

    .line 1215
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgxw;->b:Lgxv;

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1216
    iget-object v2, p0, Lgxw;->f:[Lgxx;

    if-nez v2, :cond_9

    mul-int/lit8 v2, v0, 0x1f

    .line 1222
    :cond_2
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lgxw;->I:Ljava/util/List;

    if-nez v2, :cond_b

    :goto_3
    add-int/2addr v0, v1

    .line 1223
    return v0

    :cond_3
    move v2, v0

    move v0, v1

    .line 1203
    :goto_4
    iget-object v3, p0, Lgxw;->a:[Lgxy;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 1204
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgxw;->a:[Lgxy;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    .line 1203
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1204
    :cond_4
    iget-object v2, p0, Lgxw;->a:[Lgxy;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgxy;->hashCode()I

    move-result v2

    goto :goto_5

    .line 1207
    :cond_5
    iget-object v0, p0, Lgxw;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1208
    :cond_6
    iget-object v0, p0, Lgxw;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_7
    move v2, v0

    move v0, v1

    .line 1211
    :goto_6
    iget-object v3, p0, Lgxw;->e:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1212
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lgxw;->e:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 1211
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1215
    :cond_8
    iget-object v0, p0, Lgxw;->b:Lgxv;

    invoke-virtual {v0}, Lgxv;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_9
    move v2, v0

    move v0, v1

    .line 1218
    :goto_7
    iget-object v3, p0, Lgxw;->f:[Lgxx;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1219
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgxw;->f:[Lgxx;

    aget-object v2, v2, v0

    if-nez v2, :cond_a

    move v2, v1

    :goto_8
    add-int/2addr v2, v3

    .line 1218
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1219
    :cond_a
    iget-object v2, p0, Lgxw;->f:[Lgxx;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgxx;->hashCode()I

    move-result v2

    goto :goto_8

    .line 1222
    :cond_b
    iget-object v1, p0, Lgxw;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
