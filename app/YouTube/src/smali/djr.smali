.class public abstract Ldjr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldkf;


# instance fields
.field private a:Lcwq;

.field public final b:Lcvq;

.field public final c:Levn;

.field public final d:Leyt;

.field public final e:Lfac;

.field public volatile f:Lgok;

.field public volatile g:Lfrl;

.field public volatile h:Lfnx;

.field public volatile i:Lfqg;

.field public volatile j:Z

.field public volatile k:Z


# direct methods
.method public constructor <init>(Lcvq;Levn;Lcwq;Leyt;Lfac;)V
    .locals 3

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvq;

    iput-object v0, p0, Ldjr;->b:Lcvq;

    .line 62
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldjr;->c:Levn;

    .line 63
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Ldjr;->d:Leyt;

    .line 64
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfac;

    iput-object v0, p0, Ldjr;->e:Lfac;

    .line 65
    iput-object p3, p0, Ldjr;->a:Lcwq;

    .line 66
    iget-object v0, p0, Ldjr;->c:Levn;

    const-class v1, Ldac;

    new-instance v2, Ldju;

    invoke-direct {v2, p0}, Ldju;-><init>(Ldjr;)V

    invoke-virtual {v0, p0, v1, v2}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;Levq;)Levr;

    .line 67
    iget-object v0, p0, Ldjr;->c:Levn;

    const-class v1, Ldao;

    new-instance v2, Ldjt;

    invoke-direct {v2, p0}, Ldjt;-><init>(Ldjr;)V

    invoke-virtual {v0, p0, v1, v2}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;Levq;)Levr;

    .line 71
    iget-object v0, p0, Ldjr;->c:Levn;

    const-class v1, Ldaj;

    new-instance v2, Ldjs;

    invoke-direct {v2, p0}, Ldjs;-><init>(Ldjr;)V

    invoke-virtual {v0, p0, v1, v2}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;Levq;)Levr;

    .line 75
    iget-object v0, p0, Ldjr;->a:Lcwq;

    invoke-virtual {v0}, Lcwq;->a()V

    .line 76
    const-string v1, "SequencerStage: New "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 77
    return-void

    .line 76
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcvq;Levn;Lcwq;Leyt;Lfac;Ldkz;)V
    .locals 2

    .prologue
    .line 86
    invoke-direct/range {p0 .. p5}, Ldjr;-><init>(Lcvq;Levn;Lcwq;Leyt;Lfac;)V

    .line 89
    new-instance v0, Lfqg;

    iget-object v1, p6, Ldkz;->a:[B

    invoke-direct {v0, p5, v1}, Lfqg;-><init>(Lfac;[B)V

    iput-object v0, p0, Ldjr;->i:Lfqg;

    .line 91
    return-void
.end method

.method private u()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 126
    iget-object v0, p0, Ldjr;->f:Lgok;

    const/4 v1, 0x2

    new-array v1, v1, [Lgok;

    const/4 v3, 0x0

    sget-object v4, Lgok;->d:Lgok;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    sget-object v4, Lgok;->e:Lgok;

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, Lgok;->a([Lgok;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldjr;->g:Lfrl;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrl;

    move-object v1, v0

    .line 127
    :goto_0
    iget-object v0, p0, Ldjr;->f:Lgok;

    sget-object v3, Lgok;->e:Lgok;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Ldjr;->h:Lfnx;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnx;

    .line 128
    :goto_1
    iget-object v2, p0, Ldjr;->i:Lfqg;

    .line 130
    iget-object v3, p0, Ldjr;->c:Levn;

    new-instance v4, Lczu;

    iget-object v5, p0, Ldjr;->f:Lgok;

    invoke-direct {v4, v5, v1, v0, v2}, Lczu;-><init>(Lgok;Lfrl;Lfnx;Lfqg;)V

    invoke-virtual {v3, v4}, Levn;->d(Ljava/lang/Object;)V

    .line 135
    return-void

    :cond_0
    move-object v1, v2

    .line 126
    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 127
    goto :goto_1
.end method

.method private v()V
    .locals 2

    .prologue
    .line 156
    new-instance v0, Lfqg;

    iget-object v1, p0, Ldjr;->e:Lfac;

    invoke-direct {v0, v1}, Lfqg;-><init>(Lfac;)V

    iput-object v0, p0, Ldjr;->i:Lfqg;

    .line 157
    return-void
.end method


# virtual methods
.method abstract a(I)V
.end method

.method protected final a(Lczb;)V
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lgok;->c:Lgok;

    invoke-virtual {p0, v0}, Ldjr;->a(Lgok;)V

    .line 139
    iget-object v0, p0, Ldjr;->c:Levn;

    invoke-virtual {v0, p1}, Levn;->c(Ljava/lang/Object;)V

    .line 140
    return-void
.end method

.method protected final a(Ldan;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 248
    new-array v3, v5, [Ldan;

    sget-object v2, Ldan;->b:Ldan;

    aput-object v2, v3, v1

    sget-object v2, Ldan;->c:Ldan;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    sget-object v4, Ldan;->d:Ldan;

    aput-object v4, v3, v2

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v4, v3, v2

    if-ne p1, v4, :cond_1

    :goto_1
    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Ldjr;->c:Levn;

    new-instance v1, Lczo;

    invoke-direct {v1}, Lczo;-><init>()V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 254
    :cond_0
    iget-object v0, p0, Ldjr;->c:Levn;

    new-instance v1, Ldam;

    invoke-direct {v1, p1}, Ldam;-><init>(Ldan;)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 255
    return-void

    .line 248
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected a(Lgok;)V
    .locals 3

    .prologue
    .line 99
    iput-object p1, p0, Ldjr;->f:Lgok;

    .line 100
    const-string v1, "SequencerStage: "

    invoke-virtual {p1}, Lgok;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 101
    invoke-direct {p0}, Ldjr;->u()V

    .line 102
    return-void

    .line 100
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 236
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ldjr;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ldkg;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Ldkz;

    iget-object v1, p0, Ldjr;->i:Lfqg;

    iget-object v1, v1, Lfqg;->a:[B

    invoke-direct {v0, v1}, Ldkz;-><init>([B)V

    return-object v0
.end method

.method protected final g()V
    .locals 6

    .prologue
    .line 143
    iget-object v0, p0, Ldjr;->c:Levn;

    new-instance v1, Lczt;

    .line 144
    invoke-virtual {p0}, Ldjr;->u_()Z

    move-result v2

    invoke-virtual {p0}, Ldjr;->t_()Z

    move-result v3

    iget-boolean v4, p0, Ldjr;->j:Z

    iget-boolean v5, p0, Ldjr;->k:Z

    invoke-direct {v1, v2, v3, v4, v5}, Lczt;-><init>(ZZZZ)V

    .line 143
    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 145
    return-void
.end method

.method protected final h()V
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Ldjr;->c:Levn;

    new-instance v1, Ldal;

    invoke-virtual {p0}, Ldjr;->s()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ldal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 149
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Ldjr;->u()V

    .line 162
    invoke-virtual {p0}, Ldjr;->g()V

    .line 163
    return-void
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 167
    invoke-virtual {p0}, Ldjr;->a()V

    .line 168
    iget-object v0, p0, Ldjr;->c:Levn;

    new-instance v1, Lczs;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lczs;-><init>(Z)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 169
    iget-object v0, p0, Ldjr;->a:Lcwq;

    invoke-virtual {v0}, Lcwq;->b()V

    .line 170
    iget-object v0, p0, Ldjr;->c:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 171
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Ldjr;->v()V

    .line 176
    sget-object v0, Ldan;->a:Ldan;

    invoke-virtual {p0, v0}, Ldjr;->a(Ldan;)V

    .line 177
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 181
    sget-object v0, Ldan;->a:Ldan;

    invoke-virtual {p0, v0}, Ldjr;->a(Ldan;)V

    .line 185
    iget-object v1, p0, Ldjr;->f:Lgok;

    invoke-virtual {p0}, Ldjr;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lgok;->e:Lgok;

    :goto_0
    invoke-virtual {v1, v0}, Lgok;->a(Lgok;)Z

    move-result v0

    .line 189
    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Ldjr;->b:Lcvq;

    iget-object v1, p0, Ldjr;->g:Lfrl;

    invoke-interface {v0, v1}, Lcvq;->a(Lfrl;)V

    .line 194
    :goto_1
    return-void

    .line 185
    :cond_0
    sget-object v0, Lgok;->d:Lgok;

    goto :goto_0

    .line 192
    :cond_1
    invoke-virtual {p0}, Ldjr;->k()V

    goto :goto_1
.end method

.method protected abstract m()Z
.end method

.method public n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 200
    invoke-direct {p0}, Ldjr;->v()V

    .line 201
    sget-object v0, Ldan;->b:Ldan;

    invoke-virtual {p0, v0}, Ldjr;->a(Ldan;)V

    .line 202
    iget-object v0, p0, Ldjr;->b:Lcvq;

    invoke-interface {v0, v1, v1}, Lcvq;->a(ZI)V

    .line 203
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 207
    invoke-direct {p0}, Ldjr;->v()V

    .line 208
    sget-object v0, Ldan;->c:Ldan;

    invoke-virtual {p0, v0}, Ldjr;->a(Ldan;)V

    .line 209
    iget-object v0, p0, Ldjr;->b:Lcvq;

    invoke-interface {v0, v1, v1}, Lcvq;->a(ZI)V

    .line 210
    return-void
.end method

.method public p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 214
    invoke-direct {p0}, Ldjr;->v()V

    .line 215
    sget-object v0, Ldan;->d:Ldan;

    invoke-virtual {p0, v0}, Ldjr;->a(Ldan;)V

    .line 216
    iget-object v0, p0, Ldjr;->b:Lcvq;

    invoke-interface {v0, v1, v1}, Lcvq;->a(ZI)V

    .line 217
    return-void
.end method

.method public q()V
    .locals 1

    .prologue
    .line 226
    sget-object v0, Ldan;->e:Ldan;

    invoke-virtual {p0, v0}, Ldjr;->a(Ldan;)V

    .line 227
    return-void
.end method

.method public r()V
    .locals 0

    .prologue
    .line 232
    return-void
.end method
