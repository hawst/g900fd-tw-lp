.class public final Lccc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field final a:Lfhz;

.field private final b:Landroid/content/Context;

.field private final c:Lfsj;

.field private final d:Leyp;

.field private final e:Landroid/view/View;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/view/View;

.field private final k:Landroid/view/View;

.field private final l:Landroid/view/ViewGroup;

.field private final m:Landroid/view/ViewGroup;

.field private n:Lfiv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Lfhz;Leyp;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lccc;->b:Landroid/content/Context;

    .line 55
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lccc;->c:Lfsj;

    .line 56
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lccc;->a:Lfhz;

    .line 57
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lccc;->d:Leyp;

    .line 58
    const v0, 0x7f040036

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lccc;->e:Landroid/view/View;

    .line 59
    iget-object v0, p0, Lccc;->e:Landroid/view/View;

    const v1, 0x7f0800c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccc;->f:Landroid/widget/TextView;

    .line 60
    iget-object v0, p0, Lccc;->e:Landroid/view/View;

    const v1, 0x7f080110

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccc;->g:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lccc;->e:Landroid/view/View;

    const v1, 0x7f080117

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccc;->h:Landroid/widget/TextView;

    .line 62
    iget-object v0, p0, Lccc;->e:Landroid/view/View;

    const v1, 0x7f080115

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccc;->i:Landroid/widget/TextView;

    .line 63
    iget-object v0, p0, Lccc;->e:Landroid/view/View;

    const v1, 0x7f080113

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lccc;->j:Landroid/view/View;

    .line 64
    iget-object v0, p0, Lccc;->e:Landroid/view/View;

    const v1, 0x7f080116

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lccc;->k:Landroid/view/View;

    .line 65
    iget-object v0, p0, Lccc;->e:Landroid/view/View;

    const v1, 0x7f080111

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lccc;->l:Landroid/view/ViewGroup;

    .line 66
    iget-object v0, p0, Lccc;->e:Landroid/view/View;

    const v1, 0x7f080112

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lccc;->m:Landroid/view/ViewGroup;

    .line 68
    iget-object v0, p0, Lccc;->e:Landroid/view/View;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 69
    return-void
.end method

.method private a(Landroid/view/ViewGroup;Ljava/util/List;Z)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    const/16 v3, 0x8

    .line 100
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 101
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 102
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 103
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 131
    :cond_0
    return-void

    .line 106
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfib;

    .line 107
    iget-object v1, p0, Lccc;->b:Landroid/content/Context;

    const v5, 0x7f040017

    invoke-static {v1, v5, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 108
    iget-object v1, v0, Lfib;->a:Lgxq;

    iget-object v6, v1, Lgxq;->b:Lhog;

    .line 109
    new-instance v7, Lfvi;

    iget-object v8, p0, Lccc;->d:Leyp;

    const v1, 0x7f08008a

    .line 111
    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-direct {v7, v8, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    .line 112
    const v1, 0x7f08008b

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 113
    new-instance v8, Lccd;

    invoke-direct {v8, p0, v6}, Lccd;-><init>(Lccc;Lhog;)V

    invoke-virtual {v5, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    if-eqz p3, :cond_3

    .line 122
    iget-object v6, v0, Lfib;->b:Ljava/lang/CharSequence;

    if-nez v6, :cond_2

    iget-object v6, v0, Lfib;->a:Lgxq;

    iget-object v6, v6, Lgxq;->d:Lhgz;

    invoke-static {v6}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v6

    iput-object v6, v0, Lfib;->b:Ljava/lang/CharSequence;

    :cond_2
    iget-object v0, v0, Lfib;->b:Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 123
    invoke-virtual {v7, v3}, Lfvi;->a(I)V

    .line 129
    :goto_1
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 125
    :cond_3
    invoke-virtual {v0}, Lfib;->a()Lfnc;

    move-result-object v6

    invoke-virtual {v7, v6, v9}, Lfvi;->a(Lfnc;Leyo;)V

    .line 126
    invoke-virtual {v0}, Lfib;->a()Lfnc;

    move-result-object v0

    invoke-virtual {v0}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    invoke-virtual {v7, v0}, Lfvi;->a(I)V

    .line 127
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v0, v3

    .line 126
    goto :goto_2
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x8

    .line 31
    check-cast p2, Lfiv;

    iget-object v0, p0, Lccc;->n:Lfiv;

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lccc;->c:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iput-object p2, p0, Lccc;->n:Lfiv;

    iget-object v0, p0, Lccc;->f:Landroid/widget/TextView;

    iget-object v4, p2, Lfiv;->b:Ljava/lang/CharSequence;

    if-nez v4, :cond_1

    iget-object v4, p2, Lfiv;->a:Lhcb;

    iget-object v4, v4, Lhcb;->a:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p2, Lfiv;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v4, p2, Lfiv;->b:Ljava/lang/CharSequence;

    invoke-static {v0, v4}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lccc;->g:Landroid/widget/TextView;

    iget-object v4, p2, Lfiv;->e:Ljava/lang/CharSequence;

    if-nez v4, :cond_2

    iget-object v4, p2, Lfiv;->a:Lhcb;

    iget-object v4, v4, Lhcb;->f:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p2, Lfiv;->e:Ljava/lang/CharSequence;

    :cond_2
    iget-object v4, p2, Lfiv;->e:Ljava/lang/CharSequence;

    invoke-static {v0, v4}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lccc;->h:Landroid/widget/TextView;

    iget-object v4, p2, Lfiv;->d:Ljava/lang/CharSequence;

    if-nez v4, :cond_3

    iget-object v4, p2, Lfiv;->a:Lhcb;

    iget-object v4, v4, Lhcb;->d:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p2, Lfiv;->d:Ljava/lang/CharSequence;

    :cond_3
    iget-object v4, p2, Lfiv;->d:Ljava/lang/CharSequence;

    invoke-static {v0, v4}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lccc;->i:Landroid/widget/TextView;

    iget-object v4, p2, Lfiv;->c:Ljava/lang/CharSequence;

    if-nez v4, :cond_4

    iget-object v4, p2, Lfiv;->a:Lhcb;

    iget-object v4, v4, Lhcb;->e:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p2, Lfiv;->c:Ljava/lang/CharSequence;

    :cond_4
    iget-object v4, p2, Lfiv;->c:Ljava/lang/CharSequence;

    invoke-static {v0, v4}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lccc;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lccc;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_5

    move v0, v1

    :goto_1
    iget-object v4, p0, Lccc;->k:Landroid/view/View;

    if-eqz v0, :cond_6

    move v0, v2

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lccc;->l:Landroid/view/ViewGroup;

    iget-object v0, p2, Lfiv;->f:Ljava/util/List;

    if-nez v0, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    iget-object v5, p2, Lfiv;->a:Lhcb;

    iget-object v5, v5, Lhcb;->b:[Lgxq;

    array-length v5, v5

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p2, Lfiv;->f:Ljava/util/List;

    iget-object v0, p2, Lfiv;->a:Lhcb;

    iget-object v5, v0, Lhcb;->b:[Lgxq;

    array-length v6, v5

    move v0, v2

    :goto_3
    if-ge v0, v6, :cond_7

    aget-object v7, v5, v0

    iget-object v8, p2, Lfiv;->f:Ljava/util/List;

    new-instance v9, Lfib;

    invoke-direct {v9, v7}, Lfib;-><init>(Lgxq;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v3

    goto :goto_2

    :cond_7
    iget-object v0, p2, Lfiv;->f:Ljava/util/List;

    invoke-direct {p0, v4, v0, v1}, Lccc;->a(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    iget-object v0, p0, Lccc;->m:Landroid/view/ViewGroup;

    invoke-virtual {p2}, Lfiv;->a()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v0, v4, v2}, Lccc;->a(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    iget-object v0, p0, Lccc;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_8

    iget-object v0, p0, Lccc;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_8

    iget-object v0, p0, Lccc;->l:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_8

    iget-object v0, p0, Lccc;->m:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_9

    :cond_8
    :goto_4
    iget-object v0, p0, Lccc;->j:Landroid/view/View;

    if-eqz v1, :cond_a

    :goto_5
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lccc;->c:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move v1, v2

    goto :goto_4

    :cond_a
    move v2, v3

    goto :goto_5
.end method
