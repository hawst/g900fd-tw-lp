.class public final Ldcl;
.super Lded;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lddb;


# instance fields
.field private final a:Lezj;

.field private final b:Landroid/content/res/Resources;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/os/Handler;

.field private f:Lddc;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f0201cb

    invoke-direct {p0, p1, v0}, Ldcl;-><init>(Landroid/content/Context;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/16 v4, 0x11

    const/4 v3, -0x2

    const/4 v2, -0x1

    .line 49
    invoke-direct {p0, p1}, Lded;-><init>(Landroid/content/Context;)V

    .line 51
    new-instance v0, Lezj;

    invoke-direct {v0}, Lezj;-><init>()V

    iput-object v0, p0, Ldcl;->a:Lezj;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Ldcl;->b:Landroid/content/res/Resources;

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ldcl;->e:Landroid/os/Handler;

    .line 56
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldcl;->c:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Ldcl;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 58
    iget-object v0, p0, Ldcl;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 59
    iget-object v0, p0, Ldcl;->c:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 60
    iget-object v0, p0, Ldcl;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2, v2}, Ldcl;->addView(Landroid/view/View;II)V

    .line 62
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    .line 63
    iget-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 64
    iget-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 65
    iget-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Ldcl;->addView(Landroid/view/View;)V

    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldcl;->setClickable(Z)V

    .line 71
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ldcl;->setVisibility(I)V

    .line 72
    return-void
.end method

.method static synthetic a(Ldcl;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 107
    iget-object v3, p0, Ldcl;->c:Landroid/widget/TextView;

    iget-object v0, p0, Ldcl;->c:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    iget-object v3, p0, Ldcl;->d:Landroid/widget/ImageView;

    if-ne p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 109
    return-void

    :cond_0
    move v0, v2

    .line 107
    goto :goto_0

    :cond_1
    move v1, v2

    .line 108
    goto :goto_1
.end method

.method static synthetic a(Ldcl;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Ldcl;->a(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ldcl;->setVisibility(I)V

    .line 104
    return-void
.end method

.method public final a(JZZ)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 86
    iget-object v0, p0, Ldcl;->a:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    .line 87
    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    .line 88
    iget-object v2, p0, Ldcl;->c:Landroid/widget/TextView;

    iget-object v3, p0, Ldcl;->b:Landroid/content/res/Resources;

    const v4, 0x7f09012c

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    .line 90
    invoke-virtual {p0}, Ldcl;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, p1, p2, p3, p4}, La;->a(Landroid/content/Context;JZZ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 88
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v2, p0, Ldcl;->c:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Ldcl;->a(Landroid/view/View;)V

    .line 92
    iget-object v2, p0, Ldcl;->e:Landroid/os/Handler;

    new-instance v3, Ldcm;

    invoke-direct {v3, p0}, Ldcm;-><init>(Ldcl;)V

    sub-long v0, p1, v0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 98
    :goto_0
    invoke-virtual {p0, v7}, Ldcl;->setVisibility(I)V

    .line 99
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Ldcl;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Lddc;)V
    .locals 1

    .prologue
    .line 81
    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddc;

    iput-object v0, p0, Ldcl;->f:Lddc;

    .line 82
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Ldcl;->f:Lddc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcl;->d:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 114
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ldcl;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Ldcl;->f:Lddc;

    invoke-interface {v0}, Lddc;->a()V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    const-string v0, "Play button clicked in LiveOverlay, but no listener was registered"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final r_()Ldeg;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 76
    new-instance v0, Ldeg;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Ldeg;-><init>(IIZ)V

    return-object v0
.end method
