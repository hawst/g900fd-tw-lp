.class public final Lego;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefc;


# instance fields
.field private final a:Lefc;

.field private final b:[B

.field private c:Legp;


# direct methods
.method public constructor <init>([BLefc;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p2, p0, Lego;->a:Lefc;

    .line 27
    iput-object p1, p0, Lego;->b:[B

    .line 28
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lego;->a:Lefc;

    invoke-interface {v0, p1, p2, p3}, Lefc;->a([BII)I

    move-result v0

    .line 42
    if-gez v0, :cond_0

    .line 43
    const/4 v0, -0x1

    .line 46
    :goto_0
    return v0

    .line 45
    :cond_0
    iget-object v1, p0, Lego;->c:Legp;

    invoke-virtual {v1, p1, p2, v0}, Legp;->a([BII)V

    goto :goto_0
.end method

.method public final a(Lefg;)J
    .locals 10

    .prologue
    .line 32
    iget-object v0, p0, Lego;->a:Lefc;

    invoke-interface {v0, p1}, Lefc;->a(Lefg;)J

    move-result-wide v8

    .line 33
    iget-object v0, p1, Lefg;->f:Ljava/lang/String;

    invoke-static {v0}, La;->l(Ljava/lang/String;)J

    move-result-wide v4

    .line 34
    new-instance v1, Legp;

    const/4 v2, 0x2

    iget-object v3, p0, Lego;->b:[B

    iget-wide v6, p1, Lefg;->c:J

    invoke-direct/range {v1 .. v7}, Legp;-><init>(I[BJJ)V

    iput-object v1, p0, Lego;->c:Legp;

    .line 36
    return-wide v8
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lego;->c:Legp;

    .line 52
    iget-object v0, p0, Lego;->a:Lefc;

    invoke-interface {v0}, Lefc;->a()V

    .line 53
    return-void
.end method
