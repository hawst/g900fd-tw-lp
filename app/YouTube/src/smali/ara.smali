.class public final Lara;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V
    .locals 2

    .prologue
    .line 2384
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2385
    new-instance v0, Ljava/lang/ref/WeakReference;

    .line 2386
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lara;->a:Ljava/lang/ref/WeakReference;

    .line 2387
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 2391
    iget-object v0, p0, Lara;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 2392
    if-eqz v0, :cond_0

    .line 2393
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 2402
    :cond_0
    :goto_0
    return-void

    .line 2395
    :pswitch_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->j(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    goto :goto_0

    .line 2398
    :pswitch_1
    invoke-static {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Z)V

    goto :goto_0

    .line 2393
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
