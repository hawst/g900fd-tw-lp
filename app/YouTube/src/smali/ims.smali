.class public final Lims;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:J

.field public final b:Limb;

.field public final c:Limd;

.field public d:Ljava/util/Date;

.field public e:Ljava/lang/String;

.field public f:Ljava/util/Date;

.field public g:Ljava/lang/String;

.field public h:Ljava/util/Date;

.field public i:J

.field public j:J

.field public k:Ljava/lang/String;

.field public l:I


# direct methods
.method public constructor <init>(JLimb;Limd;)V
    .locals 5

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lims;->l:I

    .line 102
    iput-wide p1, p0, Lims;->a:J

    .line 103
    iput-object p3, p0, Lims;->b:Limb;

    .line 104
    iput-object p4, p0, Lims;->c:Limd;

    .line 106
    if-eqz p4, :cond_7

    .line 107
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p4, Limd;->f:Lils;

    iget-object v1, v1, Lils;->a:[Ljava/lang/String;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_7

    .line 108
    iget-object v1, p4, Limd;->f:Lils;

    invoke-virtual {v1, v0}, Lils;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    iget-object v2, p4, Limd;->f:Lils;

    invoke-virtual {v2, v0}, Lils;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 110
    const-string v3, "Date"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 111
    invoke-static {v2}, Lina;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lims;->d:Ljava/util/Date;

    .line 112
    iput-object v2, p0, Lims;->e:Ljava/lang/String;

    .line 107
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_1
    const-string v3, "Expires"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 114
    invoke-static {v2}, Lina;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lims;->h:Ljava/util/Date;

    goto :goto_1

    .line 115
    :cond_2
    const-string v3, "Last-Modified"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 116
    invoke-static {v2}, Lina;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lims;->f:Ljava/util/Date;

    .line 117
    iput-object v2, p0, Lims;->g:Ljava/lang/String;

    goto :goto_1

    .line 118
    :cond_3
    const-string v3, "ETag"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 119
    iput-object v2, p0, Lims;->k:Ljava/lang/String;

    goto :goto_1

    .line 120
    :cond_4
    const-string v3, "Age"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 121
    invoke-static {v2}, La;->Q(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lims;->l:I

    goto :goto_1

    .line 122
    :cond_5
    sget-object v3, Ling;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 123
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lims;->i:J

    goto :goto_1

    .line 124
    :cond_6
    sget-object v3, Ling;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lims;->j:J

    goto :goto_1

    .line 129
    :cond_7
    return-void
.end method

.method public static a(Limb;)Z
    .locals 1

    .prologue
    .line 278
    const-string v0, "If-Modified-Since"

    invoke-virtual {p0, v0}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "If-None-Match"

    invoke-virtual {p0, v0}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
