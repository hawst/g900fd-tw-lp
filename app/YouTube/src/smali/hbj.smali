.class public final Lhbj;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhbj;


# instance fields
.field private b:I

.field private c:Lhbk;

.field private d:Lhbk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15389
    const/4 v0, 0x0

    new-array v0, v0, [Lhbj;

    sput-object v0, Lhbj;->a:[Lhbj;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15390
    invoke-direct {p0}, Lidf;-><init>()V

    .line 15393
    const/4 v0, 0x0

    iput v0, p0, Lhbj;->b:I

    .line 15396
    iput-object v1, p0, Lhbj;->c:Lhbk;

    .line 15399
    iput-object v1, p0, Lhbj;->d:Lhbk;

    .line 15390
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 15448
    const/4 v0, 0x0

    .line 15449
    iget v1, p0, Lhbj;->b:I

    if-eqz v1, :cond_0

    .line 15450
    const/4 v0, 0x1

    iget v1, p0, Lhbj;->b:I

    .line 15451
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15453
    :cond_0
    iget-object v1, p0, Lhbj;->c:Lhbk;

    if-eqz v1, :cond_1

    .line 15454
    const/4 v1, 0x2

    iget-object v2, p0, Lhbj;->c:Lhbk;

    .line 15455
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15457
    :cond_1
    iget-object v1, p0, Lhbj;->d:Lhbk;

    if-eqz v1, :cond_2

    .line 15458
    const/4 v1, 0x4

    iget-object v2, p0, Lhbj;->d:Lhbk;

    .line 15459
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15461
    :cond_2
    iget-object v1, p0, Lhbj;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15462
    iput v0, p0, Lhbj;->J:I

    .line 15463
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 15386
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbj;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbj;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbj;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lhbj;->b:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lhbj;->b:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhbj;->c:Lhbk;

    if-nez v0, :cond_4

    new-instance v0, Lhbk;

    invoke-direct {v0}, Lhbk;-><init>()V

    iput-object v0, p0, Lhbj;->c:Lhbk;

    :cond_4
    iget-object v0, p0, Lhbj;->c:Lhbk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhbj;->d:Lhbk;

    if-nez v0, :cond_5

    new-instance v0, Lhbk;

    invoke-direct {v0}, Lhbk;-><init>()V

    iput-object v0, p0, Lhbj;->d:Lhbk;

    :cond_5
    iget-object v0, p0, Lhbj;->d:Lhbk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 15433
    iget v0, p0, Lhbj;->b:I

    if-eqz v0, :cond_0

    .line 15434
    const/4 v0, 0x1

    iget v1, p0, Lhbj;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 15436
    :cond_0
    iget-object v0, p0, Lhbj;->c:Lhbk;

    if-eqz v0, :cond_1

    .line 15437
    const/4 v0, 0x2

    iget-object v1, p0, Lhbj;->c:Lhbk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 15439
    :cond_1
    iget-object v0, p0, Lhbj;->d:Lhbk;

    if-eqz v0, :cond_2

    .line 15440
    const/4 v0, 0x4

    iget-object v1, p0, Lhbj;->d:Lhbk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 15442
    :cond_2
    iget-object v0, p0, Lhbj;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 15444
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 15412
    if-ne p1, p0, :cond_1

    .line 15418
    :cond_0
    :goto_0
    return v0

    .line 15413
    :cond_1
    instance-of v2, p1, Lhbj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 15414
    :cond_2
    check-cast p1, Lhbj;

    .line 15415
    iget v2, p0, Lhbj;->b:I

    iget v3, p1, Lhbj;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhbj;->c:Lhbk;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbj;->c:Lhbk;

    if-nez v2, :cond_3

    .line 15416
    :goto_1
    iget-object v2, p0, Lhbj;->d:Lhbk;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbj;->d:Lhbk;

    if-nez v2, :cond_3

    .line 15417
    :goto_2
    iget-object v2, p0, Lhbj;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhbj;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 15418
    goto :goto_0

    .line 15415
    :cond_4
    iget-object v2, p0, Lhbj;->c:Lhbk;

    iget-object v3, p1, Lhbj;->c:Lhbk;

    .line 15416
    invoke-virtual {v2, v3}, Lhbk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbj;->d:Lhbk;

    iget-object v3, p1, Lhbj;->d:Lhbk;

    .line 15417
    invoke-virtual {v2, v3}, Lhbk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhbj;->I:Ljava/util/List;

    iget-object v3, p1, Lhbj;->I:Ljava/util/List;

    .line 15418
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 15422
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 15424
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhbj;->b:I

    add-int/2addr v0, v2

    .line 15425
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbj;->c:Lhbk;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 15426
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbj;->d:Lhbk;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 15427
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbj;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 15428
    return v0

    .line 15425
    :cond_0
    iget-object v0, p0, Lhbj;->c:Lhbk;

    invoke-virtual {v0}, Lhbk;->hashCode()I

    move-result v0

    goto :goto_0

    .line 15426
    :cond_1
    iget-object v0, p0, Lhbj;->d:Lhbk;

    invoke-virtual {v0}, Lhbk;->hashCode()I

    move-result v0

    goto :goto_1

    .line 15427
    :cond_2
    iget-object v1, p0, Lhbj;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
