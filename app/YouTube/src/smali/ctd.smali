.class public final Lctd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lggz;
.implements Lgkl;
.implements Lgkv;


# static fields
.field private static final a:Landroid/util/Pair;


# instance fields
.field private final b:Lfbu;

.field private final c:Lgix;

.field private final d:Landroid/content/Context;

.field private final e:Lghp;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const-string v0, ""

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    sput-object v0, Lctd;->a:Landroid/util/Pair;

    return-void
.end method

.method public constructor <init>(Lfbu;Lgix;Landroid/content/Context;Lghp;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbu;

    iput-object v0, p0, Lctd;->b:Lfbu;

    .line 53
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lctd;->c:Lgix;

    .line 54
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lctd;->d:Landroid/content/Context;

    .line 55
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghp;

    iput-object v0, p0, Lctd;->e:Lghp;

    .line 56
    return-void
.end method

.method private a(Lgiv;)Landroid/util/Pair;
    .locals 2

    .prologue
    .line 113
    invoke-static {}, Lb;->b()V

    .line 115
    :try_start_0
    invoke-virtual {p1}, Lgiv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lctd;->b:Lfbu;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lfbu;->a(Lgiv;Z)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbw;

    .line 117
    invoke-virtual {v0}, Lfbw;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-virtual {v0}, Lfbw;->c()Landroid/util/Pair;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_1
    sget-object v0, Lctd;->a:Landroid/util/Pair;

    goto :goto_0

    .line 125
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private a(Lgkp;)Lgiv;
    .locals 1

    .prologue
    .line 100
    invoke-interface {p1}, Lgkp;->k()Lgiv;

    move-result-object v0

    .line 101
    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lctd;->c:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    iget-object v0, v0, Lgit;->b:Lgiv;

    .line 104
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lhjx;Lgkp;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0, p2}, Lctd;->a(Lgkp;)Lgiv;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lgiv;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p1, Lhjx;->b:Lhyp;

    invoke-virtual {v0}, Lgiv;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lhyp;->a:Ljava/lang/String;

    .line 93
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/Map;Lgkt;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lctd;->d:Landroid/content/Context;

    invoke-static {v0}, La;->n(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lctd;->e:Lghp;

    invoke-interface {v0}, Lghp;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 67
    :cond_0
    invoke-interface {p2}, Lgkt;->z_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->L(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    .line 70
    :cond_1
    invoke-direct {p0, p2}, Lctd;->a(Lgkp;)Lgiv;

    move-result-object v0

    invoke-direct {p0, v0}, Lctd;->a(Lgiv;)Landroid/util/Pair;

    move-result-object v0

    .line 71
    sget-object v1, Lctd;->a:Landroid/util/Pair;

    if-eq v0, v1, :cond_2

    .line 72
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_2
    return-void
.end method

.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lctd;->c:Lgix;

    .line 81
    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-direct {p0, v0}, Lctd;->a(Lgiv;)Landroid/util/Pair;

    move-result-object v1

    .line 82
    sget-object v0, Lctd;->a:Landroid/util/Pair;

    if-eq v1, v0, :cond_0

    .line 83
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_0
    return-void
.end method
