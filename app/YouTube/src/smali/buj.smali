.class public final Lbuj;
.super Lftx;
.source "SourceFile"

# interfaces
.implements Lfve;


# instance fields
.field private final a:Lfsi;

.field private b:Lfvc;


# direct methods
.method public constructor <init>(Lfdg;Levn;Ljava/lang/Object;Leyt;Lfsi;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lftx;-><init>(Lfdg;Levn;Ljava/lang/Object;Leyt;)V

    .line 40
    invoke-static {p5}, Lf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsi;

    iput-object v0, p0, Lbuj;->a:Lfsi;

    .line 41
    return-void
.end method

.method private a(Lbt;)V
    .locals 3

    .prologue
    .line 86
    sget-object v0, Lfjl;->a:Lfjl;

    invoke-virtual {p0, v0}, Lbuj;->b(Lfjl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lbuj;->b:Lfvc;

    if-nez v0, :cond_1

    .line 92
    new-instance v0, Lfvc;

    invoke-virtual {p0}, Lbuj;->c()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, Lfvc;-><init>(Ljava/lang/Object;Landroid/view/View$OnClickListener;Lfve;)V

    iput-object v0, p0, Lbuj;->b:Lfvc;

    .line 94
    :cond_1
    iget-object v0, p0, Lbuj;->b:Lfvc;

    iput-object p1, v0, Lfvc;->d:Lbt;

    .line 95
    iget-object v0, p0, Lbuj;->a:Lfsi;

    iget-object v1, p0, Lbuj;->b:Lfvc;

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lbuj;->a:Lfsi;

    iget-object v1, p0, Lbuj;->b:Lfvc;

    invoke-virtual {v0, v1}, Lfsi;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfjl;)V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lbuj;->b(Lfjl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    :goto_0
    return-void

    .line 52
    :cond_0
    new-instance v0, Lfuc;

    invoke-direct {v0}, Lfuc;-><init>()V

    invoke-direct {p0, v0}, Lbuj;->a(Lbt;)V

    .line 53
    invoke-super {p0, p1}, Lftx;->a(Lfjl;)V

    goto :goto_0
.end method

.method protected final a(Lhel;Lfjl;)V
    .locals 3

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lftx;->a(Lhel;Lfjl;)V

    .line 59
    iget-object v0, p0, Lbuj;->b:Lfvc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbuj;->a:Lfsi;

    iget-object v1, p0, Lbuj;->b:Lfvc;

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lbuj;->b:Lfvc;

    .line 60
    :cond_0
    invoke-virtual {p0}, Lbuj;->e()V

    .line 62
    if-eqz p1, :cond_1

    iget-object v0, p1, Lhel;->d:Lhsd;

    if-nez v0, :cond_2

    .line 68
    :cond_1
    :goto_0
    return-void

    .line 65
    :cond_2
    new-instance v0, Lflw;

    iget-object v1, p1, Lhel;->d:Lhsd;

    invoke-direct {v0, v1}, Lflw;-><init>(Lhsd;)V

    .line 66
    iget-object v1, p0, Lbuj;->a:Lfsi;

    iget-object v2, v0, Lflw;->b:Ljava/util/List;

    invoke-virtual {v1, v2}, Lfsi;->a(Ljava/util/Collection;)V

    .line 67
    invoke-virtual {v0}, Lflw;->f()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbuj;->b(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 44
    invoke-super {p0, p1}, Lftx;->b(Ljava/util/List;)V

    .line 45
    return-void
.end method

.method protected final a(Lxa;Lfjl;)V
    .locals 3

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lftx;->a(Lxa;Lfjl;)V

    .line 73
    new-instance v0, Lfub;

    invoke-virtual {p1}, Lxa;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lfub;-><init>(Ljava/lang/CharSequence;Z)V

    invoke-direct {p0, v0}, Lbuj;->a(Lbt;)V

    .line 74
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 82
    invoke-virtual {p0}, Lbuj;->e()V

    .line 83
    return-void
.end method

.method public final o_()V
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lfjl;->a:Lfjl;

    invoke-virtual {p0, v0}, Lbuj;->a(Lfjl;)V

    .line 79
    return-void
.end method
