.class public final Leaq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leba;


# instance fields
.field final a:Lu;

.field private final b:Leex;

.field private final c:Ljava/util/List;

.field private final d:Ljava/util/HashMap;

.field private final e:Landroid/os/Handler;

.field private final f:J

.field private final g:J

.field private final h:F

.field private final i:F

.field private j:I

.field private k:J

.field private l:I

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(Leex;Landroid/os/Handler;Lu;IIFF)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x3e8

    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Leaq;->b:Leex;

    .line 115
    iput-object v0, p0, Leaq;->e:Landroid/os/Handler;

    .line 116
    iput-object v0, p0, Leaq;->a:Lu;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Leaq;->c:Ljava/util/List;

    .line 118
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Leaq;->d:Ljava/util/HashMap;

    .line 119
    int-to-long v0, p4

    mul-long/2addr v0, v2

    iput-wide v0, p0, Leaq;->f:J

    .line 120
    int-to-long v0, p5

    mul-long/2addr v0, v2

    iput-wide v0, p0, Leaq;->g:J

    .line 121
    iput p6, p0, Leaq;->h:F

    .line 122
    iput p7, p0, Leaq;->i:F

    .line 123
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Leaq;->e:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leaq;->a:Lu;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Leaq;->e:Landroid/os/Handler;

    new-instance v1, Lear;

    invoke-direct {v1, p0, p1}, Lear;-><init>(Leaq;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 250
    :cond_0
    return-void
.end method

.method private c()V
    .locals 14

    .prologue
    const-wide/16 v12, -0x1

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 202
    .line 205
    iget v0, p0, Leaq;->l:I

    move v1, v2

    move v3, v0

    move v4, v2

    move v5, v2

    move v6, v2

    .line 206
    :goto_0
    iget-object v0, p0, Leaq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 207
    iget-object v0, p0, Leaq;->d:Ljava/util/HashMap;

    iget-object v7, p0, Leaq;->c:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leas;

    .line 208
    iget-boolean v7, v0, Leas;->c:Z

    or-int/2addr v6, v7

    .line 209
    iget-boolean v7, v0, Leas;->d:Z

    or-int/2addr v5, v7

    .line 210
    iget-wide v10, v0, Leas;->e:J

    cmp-long v7, v10, v12

    if-eqz v7, :cond_0

    move v7, v8

    :goto_1
    or-int/2addr v4, v7

    .line 211
    iget v0, v0, Leas;->b:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 206
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v7, v2

    .line 210
    goto :goto_1

    .line 214
    :cond_1
    iget-object v0, p0, Leaq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    if-nez v5, :cond_7

    if-nez v6, :cond_2

    if-eqz v4, :cond_7

    :cond_2
    const/4 v0, 0x2

    if-eq v3, v0, :cond_3

    if-ne v3, v8, :cond_7

    iget-boolean v0, p0, Leaq;->m:Z

    if-eqz v0, :cond_7

    :cond_3
    move v0, v8

    :goto_2
    iput-boolean v0, p0, Leaq;->m:Z

    .line 217
    iget-boolean v0, p0, Leaq;->m:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Leaq;->n:Z

    if-nez v0, :cond_8

    .line 218
    sget-object v0, Lefv;->a:Lefv;

    invoke-virtual {v0, v2}, Lefv;->c(I)V

    .line 219
    iput-boolean v8, p0, Leaq;->n:Z

    .line 220
    invoke-direct {p0, v8}, Leaq;->a(Z)V

    .line 227
    :cond_4
    :goto_3
    iput-wide v12, p0, Leaq;->k:J

    .line 228
    iget-boolean v0, p0, Leaq;->m:Z

    if-eqz v0, :cond_9

    .line 229
    :goto_4
    iget-object v0, p0, Leaq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 230
    iget-object v0, p0, Leaq;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 231
    iget-object v1, p0, Leaq;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leas;

    .line 232
    iget-wide v0, v0, Leas;->e:J

    .line 233
    cmp-long v3, v0, v12

    if-eqz v3, :cond_6

    iget-wide v4, p0, Leaq;->k:J

    cmp-long v3, v4, v12

    if-eqz v3, :cond_5

    iget-wide v4, p0, Leaq;->k:J

    cmp-long v3, v0, v4

    if-gez v3, :cond_6

    .line 235
    :cond_5
    iput-wide v0, p0, Leaq;->k:J

    .line 229
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    move v0, v2

    .line 214
    goto :goto_2

    .line 221
    :cond_8
    iget-boolean v0, p0, Leaq;->m:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Leaq;->n:Z

    if-eqz v0, :cond_4

    if-nez v6, :cond_4

    .line 222
    sget-object v0, Lefv;->a:Lefv;

    invoke-virtual {v0, v2}, Lefv;->d(I)V

    .line 223
    iput-boolean v2, p0, Leaq;->n:Z

    .line 224
    invoke-direct {p0, v2}, Leaq;->a(Z)V

    goto :goto_3

    .line 239
    :cond_9
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Leaq;->b:Leex;

    iget v1, p0, Leaq;->j:I

    invoke-interface {v0, v1}, Leex;->b(I)V

    .line 143
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Leaq;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 135
    iget-object v0, p0, Leaq;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leas;

    .line 136
    iget v1, p0, Leaq;->j:I

    iget v0, v0, Leas;->a:I

    sub-int v0, v1, v0

    iput v0, p0, Leaq;->j:I

    .line 137
    invoke-direct {p0}, Leaq;->c()V

    .line 138
    return-void
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Leaq;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v0, p0, Leaq;->d:Ljava/util/HashMap;

    new-instance v1, Leas;

    invoke-direct {v1, p2}, Leas;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    iget v0, p0, Leaq;->j:I

    add-int/2addr v0, p2

    iput v0, p0, Leaq;->j:I

    .line 130
    return-void
.end method

.method public final a(Ljava/lang/Object;JJZZ)Z
    .locals 4

    .prologue
    .line 154
    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    move v1, v0

    .line 155
    :goto_0
    iget-object v0, p0, Leaq;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leas;

    .line 156
    iget v2, v0, Leas;->b:I

    if-ne v2, v1, :cond_0

    iget-wide v2, v0, Leas;->e:J

    cmp-long v2, v2, p4

    if-nez v2, :cond_0

    iget-boolean v2, v0, Leas;->c:Z

    if-ne v2, p6, :cond_0

    iget-boolean v2, v0, Leas;->d:Z

    if-eq v2, p7, :cond_8

    :cond_0
    const/4 v2, 0x1

    .line 159
    :goto_1
    if-eqz v2, :cond_1

    .line 160
    iput v1, v0, Leas;->b:I

    .line 161
    iput-wide p4, v0, Leas;->e:J

    .line 162
    iput-boolean p6, v0, Leas;->c:Z

    .line 163
    iput-boolean p7, v0, Leas;->d:Z

    .line 167
    :cond_1
    iget-object v0, p0, Leaq;->b:Leex;

    invoke-interface {v0}, Leex;->a()I

    move-result v3

    .line 168
    int-to-float v0, v3

    iget v1, p0, Leaq;->j:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Leaq;->i:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_9

    const/4 v0, 0x0

    .line 169
    :goto_2
    iget v1, p0, Leaq;->l:I

    if-eq v1, v0, :cond_b

    const/4 v1, 0x1

    .line 170
    :goto_3
    if-eqz v1, :cond_2

    .line 171
    iput v0, p0, Leaq;->l:I

    .line 175
    :cond_2
    if-nez v2, :cond_3

    if-eqz v1, :cond_4

    .line 176
    :cond_3
    invoke-direct {p0}, Leaq;->c()V

    .line 179
    :cond_4
    iget v0, p0, Leaq;->j:I

    if-ge v3, v0, :cond_c

    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-eqz v0, :cond_c

    iget-wide v0, p0, Leaq;->k:J

    cmp-long v0, p4, v0

    if-gtz v0, :cond_c

    const/4 v0, 0x1

    :goto_4
    return v0

    .line 154
    :cond_5
    sub-long v0, p4, p2

    iget-wide v2, p0, Leaq;->g:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_6
    iget-wide v2, p0, Leaq;->f:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_7

    const/4 v0, 0x2

    move v1, v0

    goto :goto_0

    :cond_7
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    .line 156
    :cond_8
    const/4 v2, 0x0

    goto :goto_1

    .line 168
    :cond_9
    iget v1, p0, Leaq;->h:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a

    const/4 v0, 0x2

    goto :goto_2

    :cond_a
    const/4 v0, 0x1

    goto :goto_2

    .line 169
    :cond_b
    const/4 v1, 0x0

    goto :goto_3

    .line 179
    :cond_c
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final b()Leex;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Leaq;->b:Leex;

    return-object v0
.end method
