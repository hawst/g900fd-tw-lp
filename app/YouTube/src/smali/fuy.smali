.class public final Lfuy;
.super Lfud;
.source "SourceFile"


# instance fields
.field private c:Lfqh;


# direct methods
.method public constructor <init>(Lfdg;Lfuu;Levn;Leyt;)V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p2}, Lfuu;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsl;

    invoke-direct {p0, p1, v0, p3, p4}, Lfud;-><init>(Lfdg;Lfsl;Levn;Leyt;)V

    .line 50
    const-class v0, Lfkj;

    invoke-virtual {p2, v0}, Lfuu;->a(Ljava/lang/Class;)V

    .line 51
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-virtual {p3, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method private b(Lfkj;)V
    .locals 6

    .prologue
    .line 80
    if-nez p1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p1, Lfkj;->c:Ljava/util/List;

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lfkj;->a:Lhky;

    iget-object v1, v1, Lhky;->b:[Lhkz;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p1, Lfkj;->c:Ljava/util/List;

    iget-object v0, p1, Lfkj;->a:Lhky;

    iget-object v1, v0, Lhky;->b:[Lhkz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    iget-object v4, v3, Lhkz;->b:Lhoj;

    if-eqz v4, :cond_3

    iget-object v4, p1, Lfkj;->c:Ljava/util/List;

    new-instance v5, Lfjk;

    iget-object v3, v3, Lhkz;->b:Lhoj;

    invoke-direct {v5, v3}, Lfjk;-><init>(Lhoj;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v4, v3, Lhkz;->c:Lhtn;

    if-eqz v4, :cond_2

    iget-object v4, p1, Lfkj;->c:Ljava/util/List;

    new-instance v5, Lfjk;

    iget-object v3, v3, Lhkz;->c:Lhtn;

    invoke-direct {v5, v3}, Lfjk;-><init>(Lhtn;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    iget-object v0, p1, Lfkj;->c:Ljava/util/List;

    invoke-virtual {p0, v0}, Lfuy;->b(Ljava/util/List;)V

    .line 85
    invoke-virtual {p1}, Lfkj;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfuy;->a(Ljava/util/Collection;)V

    .line 86
    iget-object v0, p0, Lfuy;->c:Lfqh;

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p1, Lfkj;->b:Lfqh;

    iput-object v0, p0, Lfuy;->c:Lfqh;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfkj;)V
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lfuy;->h()V

    .line 67
    invoke-virtual {p1}, Lfkj;->b()Lfkk;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lfkk;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    invoke-virtual {p0, v0}, Lfuy;->a(Ljava/lang/Object;)V

    .line 71
    :cond_0
    invoke-direct {p0, p1}, Lfuy;->b(Lfkj;)V

    .line 72
    iget-object v0, p1, Lfkj;->b:Lfqh;

    iput-object v0, p0, Lfuy;->c:Lfqh;

    .line 73
    return-void
.end method

.method protected final a(Lhel;Lfjl;)V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0, p1, p2}, Lfud;->a(Lhel;Lfjl;)V

    .line 94
    if-eqz p1, :cond_0

    iget-object v0, p1, Lhel;->c:Lhky;

    if-nez v0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    new-instance v0, Lfkj;

    iget-object v1, p1, Lhel;->c:Lhky;

    iget-object v2, p0, Lfuy;->c:Lfqh;

    invoke-direct {v0, v1, v2}, Lfkj;-><init>(Lhky;Lfqh;)V

    .line 100
    sget-object v1, Lfjl;->b:Lfjl;

    if-ne p2, v1, :cond_2

    .line 101
    invoke-virtual {p0, v0}, Lfuy;->a(Lfkj;)V

    goto :goto_0

    .line 103
    :cond_2
    invoke-direct {p0, v0}, Lfuy;->b(Lfkj;)V

    goto :goto_0
.end method

.method public final handleHideEnclosingActionEvent(Lfhg;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p1, Lfgz;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lfuy;->b(Ljava/lang/Object;)V

    .line 63
    return-void
.end method

.method public final handleServiceResponseRemoveEvent(Lfml;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 57
    iget-object v0, p1, Lfmk;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lfuy;->b(Ljava/lang/Object;)V

    .line 58
    return-void
.end method
