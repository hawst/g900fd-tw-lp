.class public final Lckt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldmw;


# instance fields
.field public final a:Ljava/util/WeakHashMap;

.field public b:Lfad;

.field public c:Z

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lckt;->d:Landroid/content/Context;

    .line 30
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lckt;->a:Ljava/util/WeakHashMap;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    .line 112
    invoke-interface {v0}, Ldmw;->a()V

    goto :goto_0

    .line 114
    :cond_0
    return-void
.end method

.method public final a(Lgjm;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    .line 119
    invoke-interface {v0, p1}, Ldmw;->a(Lgjm;)V

    goto :goto_0

    .line 121
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    .line 154
    invoke-interface {v0}, Ldmw;->b()V

    goto :goto_0

    .line 156
    :cond_0
    return-void
.end method

.method public final b(Lgjm;)V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    .line 126
    invoke-interface {v0, p1}, Ldmw;->b(Lgjm;)V

    goto :goto_0

    .line 128
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lckt;->b:Lfad;

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lckt;->d:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->a(Landroid/content/Context;Ldmw;)Lfad;

    move-result-object v0

    iput-object v0, p0, Lckt;->b:Lfad;

    .line 100
    :cond_0
    return-void
.end method

.method public final c(Lgjm;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    .line 133
    invoke-interface {v0, p1}, Ldmw;->c(Lgjm;)V

    goto :goto_0

    .line 135
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lckt;->b:Lfad;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lckt;->b:Lfad;

    iget-object v1, p0, Lckt;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lfad;->a(Landroid/content/Context;)V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lckt;->b:Lfad;

    .line 107
    :cond_0
    return-void
.end method

.method public final d(Lgjm;)V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    .line 140
    invoke-interface {v0, p1}, Ldmw;->d(Lgjm;)V

    goto :goto_0

    .line 142
    :cond_0
    return-void
.end method

.method public final e(Lgjm;)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    .line 147
    invoke-interface {v0, p1}, Ldmw;->e(Lgjm;)V

    goto :goto_0

    .line 149
    :cond_0
    return-void
.end method
