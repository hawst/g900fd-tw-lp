.class public final Lfbl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field private synthetic a:Lfbh;


# direct methods
.method public constructor <init>(Lfbh;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lfbl;->a:Lfbh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lfbl;->a:Lfbh;

    iget-object v0, v0, Lfbh;->i:Lfbn;

    invoke-interface {v0}, Lfbn;->b()V

    .line 201
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 196
    check-cast p1, Lfjy;

    iget-object v0, p1, Lfjy;->b:Lfjp;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfjy;->a:Lhhd;

    iget-object v0, v0, Lhhd;->a:Lhey;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfjy;->a:Lhhd;

    iget-object v0, v0, Lhhd;->a:Lhey;

    iget-object v0, v0, Lhey;->a:Lhew;

    if-eqz v0, :cond_0

    new-instance v0, Lfjp;

    iget-object v1, p1, Lfjy;->a:Lhhd;

    iget-object v1, v1, Lhhd;->a:Lhey;

    iget-object v1, v1, Lhey;->a:Lhew;

    invoke-direct {v0, v1}, Lfjp;-><init>(Lhew;)V

    iput-object v0, p1, Lfjy;->b:Lfjp;

    :cond_0
    iget-object v1, p1, Lfjy;->b:Lfjp;

    iget-object v2, p0, Lfbl;->a:Lfbh;

    iget-object v0, v2, Lfbh;->d:Lfjp;

    invoke-virtual {v1, v0}, Lfjp;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lfbh;->d:Lfjp;

    :goto_0
    iget-object v0, p0, Lfbl;->a:Lfbh;

    invoke-virtual {v1}, Lfjp;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfbh;->a(Ljava/util/List;)V

    iget-object v0, p0, Lfbl;->a:Lfbh;

    iget-object v0, v0, Lfbh;->i:Lfbn;

    return-void

    :cond_1
    iput-object v1, v2, Lfbh;->d:Lfjp;

    iget-object v0, v2, Lfbh;->b:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    invoke-virtual {v1}, Lfjp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbt;

    iget-object v4, v2, Lfbh;->b:Lfsi;

    invoke-virtual {v4, v0}, Lfsi;->b(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget-object v0, v1, Lfjp;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_3

    iget-object v0, v1, Lfjp;->a:Lhew;

    iget-object v0, v0, Lhew;->a:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, v1, Lfjp;->b:Ljava/lang/CharSequence;

    :cond_3
    iget-object v0, v1, Lfjp;->b:Ljava/lang/CharSequence;

    iget-object v0, v1, Lfjp;->c:Lfjo;

    if-nez v0, :cond_4

    iget-object v0, v1, Lfjp;->a:Lhew;

    iget-object v0, v0, Lhew;->d:Lhev;

    if-eqz v0, :cond_4

    iget-object v0, v1, Lfjp;->a:Lhew;

    iget-object v0, v0, Lhew;->d:Lhev;

    iget-object v0, v0, Lhev;->a:Lheu;

    if-eqz v0, :cond_4

    new-instance v0, Lfjo;

    iget-object v3, v1, Lfjp;->a:Lhew;

    iget-object v3, v3, Lhew;->d:Lhev;

    iget-object v3, v3, Lhev;->a:Lheu;

    invoke-direct {v0, v3}, Lfjo;-><init>(Lheu;)V

    iput-object v0, v1, Lfjp;->c:Lfjo;

    :cond_4
    iget-object v0, v1, Lfjp;->c:Lfjo;

    iput-object v0, v2, Lfbh;->h:Lfjo;

    iget-object v0, v2, Lfbh;->h:Lfjo;

    invoke-virtual {v2, v0}, Lfbh;->a(Lfjo;)V

    goto :goto_0
.end method
