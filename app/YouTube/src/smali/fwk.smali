.class public final Lfwk;
.super Lghl;
.source "SourceFile"

# interfaces
.implements Lfxe;


# instance fields
.field private A:Lgku;

.field private B:Lgku;

.field private C:Lgku;

.field private D:Lgku;

.field private E:Lgku;

.field private F:Lgku;

.field private G:Lgku;

.field private H:Lgku;

.field private I:Lgku;

.field private J:Lgku;

.field private K:Lgku;

.field private L:Lgku;

.field private M:Lfwh;

.field private N:Lgku;

.field private O:Lgku;

.field private P:Lgku;

.field private Q:Lgku;

.field private R:Lgku;

.field private S:Lgku;

.field private T:Lgku;

.field private U:Lgku;

.field private V:Lgku;

.field private W:Lgku;

.field private X:Lgku;

.field private Y:Lgku;

.field private Z:Lgku;

.field final a:Lfxj;

.field private aa:Lgku;

.field private ab:Lgku;

.field private ac:Lgkb;

.field private final ad:Lgkc;

.field private final b:Lfap;

.field private c:Lfxi;

.field private d:Lfxi;

.field private j:Lfxi;

.field private k:Lfxi;

.field private l:Lgad;

.field private m:Lfyg;

.field private n:Lfyg;

.field private o:Lfyf;

.field private p:Leuk;

.field private q:Leuk;

.field private r:Leuk;

.field private s:Leuk;

.field private t:Leuk;

.field private u:Leuk;

.field private v:Leuk;

.field private w:Leuk;

.field private x:Leuk;

.field private y:Leuk;

.field private z:Leuk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lezj;Lfbc;Lfxj;Lgix;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 185
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p5

    move-object v4, p8

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lghl;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Ljava/lang/String;Lezj;)V

    .line 186
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    new-instance v0, Lfap;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lfap;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lfwk;->b:Lfap;

    .line 189
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxj;

    iput-object v0, p0, Lfwk;->a:Lfxj;

    .line 190
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    .line 191
    new-instance v0, Lgkc;

    invoke-direct {v0, p7}, Lgkc;-><init>(Lgix;)V

    iput-object v0, p0, Lfwk;->ad:Lgkc;

    .line 192
    return-void
.end method

.method private i()Lgku;
    .locals 8

    .prologue
    .line 470
    iget-object v0, p0, Lfwk;->j:Lfxi;

    new-instance v1, Lgab;

    iget-object v2, p0, Lfwk;->i:Lfbc;

    invoke-direct {v1, v2}, Lgab;-><init>(Lfbc;)V

    invoke-virtual {p0, v0, v1}, Lfwk;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    .line 474
    invoke-virtual {p0, v0}, Lfwk;->a(Lgku;)Lgjx;

    move-result-object v3

    .line 476
    iget-object v2, p0, Lfwk;->p:Leuk;

    iget-object v4, p0, Lfwk;->a:Lfxj;

    iget-object v5, p0, Lfwk;->f:Lezj;

    .line 477
    new-instance v1, Lfzz;

    const-wide/32 v6, 0x6ddd00

    invoke-direct/range {v1 .. v7}, Lfzz;-><init>(Leuk;Lgku;Lfxj;Lezj;J)V

    .line 480
    return-object v1
.end method


# virtual methods
.method public final a()Lfxj;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lfwk;->a:Lfxj;

    return-object v0
.end method

.method public final a(ILeuc;)V
    .locals 3

    .prologue
    .line 833
    iget-object v0, p0, Lfwk;->L:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    const/16 v2, 0xf

    .line 834
    invoke-virtual {v1, v2}, Lfxj;->a(I)Lfxg;

    move-result-object v1

    .line 833
    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 836
    return-void
.end method

.method public final a(Landroid/net/Uri;Leuc;)V
    .locals 2

    .prologue
    .line 969
    iget-object v0, p0, Lfwk;->Z:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    .line 970
    invoke-static {p1}, Lfxj;->b(Landroid/net/Uri;)Lfxg;

    move-result-object v1

    .line 969
    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 972
    return-void
.end method

.method public final a(Leuc;)V
    .locals 3

    .prologue
    .line 782
    iget-object v0, p0, Lfwk;->I:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    const-string v2, "default"

    invoke-virtual {v1, v2}, Lfxj;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 783
    return-void
.end method

.method public final a(Lfxm;Ljava/lang/String;Ljava/lang/String;Lfxn;Leuc;)V
    .locals 7

    .prologue
    .line 866
    iget-object v1, p0, Lfwk;->a:Lfxj;

    iget v2, v1, Lfxj;->c:I

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxm;

    invoke-virtual {v0}, Lfxm;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, v1, Lfxj;->a:Lfxl;

    invoke-interface {v3}, Lfxl;->a()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "standardfeeds"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-static {p3}, Lfxj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :goto_0
    if-eqz p4, :cond_1

    const-string v0, "time"

    invoke-virtual {p4}, Lfxn;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    invoke-static {v3}, Lfxj;->a(Landroid/net/Uri$Builder;)V

    const/4 v0, 0x1

    invoke-static {v3, v0, v2}, Lfxj;->a(Landroid/net/Uri$Builder;II)V

    iget-object v0, v1, Lfxj;->d:Lgjk;

    if-eqz v0, :cond_2

    const-string v2, "safeSearch"

    iget-object v0, v1, Lfxj;->d:Lgjk;

    iget-boolean v0, v0, Lgjk;->a:Z

    if-eqz v0, :cond_5

    const-string v0, "strict"

    :goto_1
    invoke-virtual {v3, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    iget-object v0, v1, Lfxj;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, "restriction"

    iget-object v1, v1, Lfxj;->e:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v0

    .line 868
    iget-object v1, p0, Lfwk;->F:Lgku;

    invoke-interface {v1, v0, p5}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 869
    return-void

    .line 866
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_5
    const-string v0, "none"

    goto :goto_1
.end method

.method public final a(Lgir;Ljava/util/List;Lgkb;Lgiz;Lfxh;)V
    .locals 8

    .prologue
    .line 204
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkb;

    iput-object v0, p0, Lfwk;->ac:Lgkb;

    .line 209
    iget-object v0, p0, Lghl;->g:Ljava/lang/String;

    const-string v1, "this instance does not support persistent caching"

    invoke-static {v0, v1}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lghl;->e:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lghl;->g:Ljava/lang/String;

    new-instance v2, Leuq;

    const-wide/32 v4, 0x1400000

    invoke-direct {v2, v1, v4, v5}, Leuq;-><init>(Ljava/lang/String;J)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 211
    new-instance v0, Lfxi;

    sget-object v1, Lewv;->b:Lewv;

    iget-object v4, p0, Lfwk;->b:Lfap;

    move-object v2, p1

    move-object v3, p2

    move-object v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lfxi;-><init>(Lewv;Lgir;Ljava/util/List;Lfap;Lfxh;Lgiz;)V

    iput-object v0, p0, Lfwk;->c:Lfxi;

    .line 218
    new-instance v0, Lfxi;

    sget-object v1, Lewv;->d:Lewv;

    const-string v2, "application/atom+xml"

    iget-object v5, p0, Lfwk;->b:Lfap;

    move-object v3, p1

    move-object v4, p2

    move-object v6, p5

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lfxi;-><init>(Lewv;Ljava/lang/String;Lgir;Ljava/util/List;Lfap;Lfxh;Lgiz;)V

    iput-object v0, p0, Lfwk;->d:Lfxi;

    .line 226
    new-instance v0, Lfxi;

    sget-object v1, Lewv;->c:Lewv;

    const-string v2, "application/atom+xml"

    iget-object v5, p0, Lfwk;->b:Lfap;

    move-object v3, p1

    move-object v4, p2

    move-object v6, p5

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lfxi;-><init>(Lewv;Ljava/lang/String;Lgir;Ljava/util/List;Lfap;Lfxh;Lgiz;)V

    iput-object v0, p0, Lfwk;->j:Lfxi;

    .line 234
    new-instance v0, Lfxi;

    sget-object v1, Lewv;->f:Lewv;

    const-string v2, "application/atom+xml"

    iget-object v5, p0, Lfwk;->b:Lfap;

    move-object v3, p1

    move-object v4, p2

    move-object v6, p5

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lfxi;-><init>(Lewv;Ljava/lang/String;Lgir;Ljava/util/List;Lfap;Lfxh;Lgiz;)V

    iput-object v0, p0, Lfwk;->k:Lfxi;

    .line 243
    new-instance v0, Lgad;

    iget-object v1, p0, Lghl;->i:Lfbc;

    invoke-direct {v0, v1}, Lgad;-><init>(Lfbc;)V

    iput-object v0, p0, Lfwk;->l:Lgad;

    .line 245
    const/16 v0, 0x14

    invoke-static {v0}, Lfwk;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lfwk;->u:Leuk;

    .line 247
    const/16 v0, 0x1f4

    invoke-static {v0}, Lfwk;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lfwk;->p:Leuk;

    .line 248
    const/16 v0, 0x1f4

    invoke-static {v0}, Lfwk;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lfwk;->q:Leuk;

    .line 250
    new-instance v0, Lgbh;

    iget-object v1, p0, Lfwk;->c:Lfxi;

    iget-object v2, p0, Lfwk;->l:Lgad;

    .line 251
    invoke-virtual {p0, v1, v2}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v1

    invoke-virtual {p0, v1}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v1

    iget-object v2, p0, Lfwk;->q:Leuk;

    iget-object v3, p0, Lfwk;->a:Lfxj;

    iget-object v4, p0, Lghl;->f:Lezj;

    invoke-direct {v0, v1, v2, v3, v4}, Lgbh;-><init>(Lgku;Leuk;Lfxj;Lezj;)V

    .line 253
    iget-object v1, p0, Lfwk;->p:Leuk;

    const-wide/32 v2, 0xdbba00

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    move-result-object v1

    iput-object v1, p0, Lfwk;->A:Lgku;

    .line 254
    iget-object v1, p0, Lfwk;->p:Leuk;

    const-wide/16 v2, 0x0

    .line 255
    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    move-result-object v1

    iget-object v2, p0, Lfwk;->ad:Lgkc;

    .line 254
    invoke-static {v1, p3, v2}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v1

    iput-object v1, p0, Lfwk;->B:Lgku;

    .line 257
    iget-object v1, p0, Lfwk;->p:Leuk;

    const-wide/32 v2, 0x1b7740

    .line 258
    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iget-object v1, p0, Lfwk;->ad:Lgkc;

    .line 257
    invoke-static {v0, p3, v1}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    iput-object v0, p0, Lfwk;->C:Lgku;

    .line 261
    const/16 v0, 0x32

    invoke-static {v0}, Lfwk;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lfwk;->t:Leuk;

    .line 262
    new-instance v0, Leuw;

    iget-object v1, p0, Lfwk;->t:Leuk;

    iget-object v2, p0, Lfwk;->p:Leuk;

    new-instance v3, Lfwr;

    invoke-direct {v3, p0}, Lfwr;-><init>(Lfwk;)V

    invoke-direct {v0, v1, v2, v3}, Leuw;-><init>(Leuk;Leuk;Leux;)V

    iput-object v0, p0, Lfwk;->v:Leuk;

    .line 266
    iget-object v0, p0, Lfwk;->c:Lfxi;

    new-instance v1, Lgac;

    iget-object v2, p0, Lghl;->i:Lfbc;

    invoke-direct {v1, v2}, Lgac;-><init>(Lfbc;)V

    invoke-virtual {p0, v0, v1}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iput-object v0, p0, Lfwk;->D:Lgku;

    .line 270
    const/16 v0, 0x64

    invoke-static {v0}, Lfwk;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lfwk;->w:Leuk;

    .line 271
    const/16 v0, 0x32

    invoke-static {v0}, Lfwk;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lfwk;->x:Leuk;

    .line 272
    new-instance v0, Leuw;

    iget-object v1, p0, Lfwk;->x:Leuk;

    iget-object v2, p0, Lfwk;->w:Leuk;

    new-instance v3, Lfwq;

    invoke-direct {v3, p0}, Lfwq;-><init>(Lfwk;)V

    invoke-direct {v0, v1, v2, v3}, Leuw;-><init>(Leuk;Leuk;Leux;)V

    iput-object v0, p0, Lfwk;->y:Leuk;

    .line 275
    const/16 v0, 0x64

    invoke-static {v0}, Lfwk;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lfwk;->z:Leuk;

    .line 276
    new-instance v0, Lfyf;

    iget-object v1, p0, Lghl;->i:Lfbc;

    invoke-direct {v0, v1}, Lfyf;-><init>(Lfbc;)V

    iput-object v0, p0, Lfwk;->o:Lfyf;

    .line 277
    iget-object v0, p0, Lghl;->i:Lfbc;

    .line 278
    invoke-static {v0}, Lfyg;->c(Lfbc;)Lfyg;

    move-result-object v0

    iput-object v0, p0, Lfwk;->n:Lfyg;

    .line 279
    iget-object v0, p0, Lfwk;->c:Lfxi;

    iget-object v1, p0, Lfwk;->o:Lfyf;

    .line 280
    invoke-virtual {p0, v0, v1}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iput-object v0, p0, Lfwk;->E:Lgku;

    .line 281
    iget-object v0, p0, Lfwk;->E:Lgku;

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->y:Leuk;

    const-wide/32 v2, 0x1b7740

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    .line 282
    iget-object v0, p0, Lfwk;->E:Lgku;

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->y:Leuk;

    const-wide/32 v2, 0x493e0

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    .line 283
    iget-object v0, p0, Lfwk;->E:Lgku;

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->y:Leuk;

    const-wide/32 v2, 0x1b7740

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iget-object v1, p0, Lfwk;->ac:Lgkb;

    iget-object v2, p0, Lfwk;->ad:Lgkc;

    invoke-static {v0, v1, v2}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    iput-object v0, p0, Lfwk;->N:Lgku;

    .line 284
    iget-object v0, p0, Lfwk;->c:Lfxi;

    iget-object v1, p0, Lfwk;->n:Lfyg;

    invoke-virtual {p0, v0, v1}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->w:Leuk;

    const-wide/32 v2, 0x1b7740

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iput-object v0, p0, Lfwk;->H:Lgku;

    .line 285
    iget-object v0, p0, Lfwk;->H:Lgku;

    iget-object v1, p0, Lfwk;->ad:Lgkc;

    invoke-static {v0, p3, v1}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    .line 288
    iget-object v0, p0, Lfwk;->D:Lgku;

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->v:Leuk;

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iput-object v0, p0, Lfwk;->F:Lgku;

    .line 289
    iget-object v0, p0, Lfwk;->D:Lgku;

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->v:Leuk;

    const-wide/32 v2, 0x1b7740

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    .line 290
    new-instance v0, Lfzm;

    iget-object v1, p0, Lghl;->i:Lfbc;

    invoke-direct {v0, v1}, Lfzm;-><init>(Lfbc;)V

    iget-object v1, p0, Lfwk;->c:Lfxi;

    invoke-virtual {p0, v1, v0}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->z:Leuk;

    const-wide/32 v2, 0x493e0

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iput-object v0, p0, Lfwk;->G:Lgku;

    .line 291
    new-instance v0, Lfwu;

    iget-object v1, p0, Lghl;->i:Lfbc;

    invoke-direct {v0, v1}, Lfwu;-><init>(Lfbc;)V

    iget-object v1, p0, Lfwk;->c:Lfxi;

    invoke-virtual {p0, v1, v0}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->u:Leuk;

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    .line 293
    iget-object v0, p0, Lfwk;->G:Lgku;

    iget-object v1, p0, Lfwk;->ad:Lgkc;

    invoke-static {v0, p3, v1}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    iput-object v0, p0, Lfwk;->I:Lgku;

    .line 295
    iget-object v0, p0, Lfwk;->D:Lgku;

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->v:Leuk;

    const-wide/32 v2, 0x1b7740

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iget-object v1, p0, Lfwk;->ac:Lgkb;

    iget-object v2, p0, Lfwk;->ad:Lgkc;

    invoke-static {v0, v1, v2}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    iput-object v0, p0, Lfwk;->K:Lgku;

    .line 296
    iget-object v0, p0, Lfwk;->D:Lgku;

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->v:Leuk;

    const-wide/32 v2, 0x493e0

    invoke-virtual {p0, v1, v0, v2, v3}, Lghl;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iget-object v1, p0, Lfwk;->ac:Lgkb;

    iget-object v2, p0, Lfwk;->ad:Lgkc;

    invoke-static {v0, v1, v2}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    iput-object v0, p0, Lfwk;->L:Lgku;

    .line 297
    new-instance v0, Lfwh;

    const-string v1, "uploads"

    invoke-direct {v0, v1}, Lfwh;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lfwk;->M:Lfwh;

    .line 298
    iget-object v0, p0, Lfwk;->K:Lgku;

    new-instance v1, Lfzb;

    const/16 v2, 0x32

    invoke-direct {v1, v0, v2}, Lfzb;-><init>(Lgku;I)V

    iput-object v1, p0, Lfwk;->O:Lgku;

    .line 300
    new-instance v0, Lfyx;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    iget-object v2, p0, Lfwk;->F:Lgku;

    invoke-direct {v0, v1, v2}, Lfyx;-><init>(Lfxj;Lgku;)V

    .line 301
    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lfwk;->J:Lgku;

    .line 303
    iget-object v0, p0, Lghl;->i:Lfbc;

    .line 304
    invoke-static {v0}, Lfxo;->a(Lfbc;)Lfxo;

    move-result-object v0

    .line 305
    iget-object v1, p0, Lfwk;->j:Lfxi;

    .line 306
    invoke-virtual {p0, v1, v0}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v1

    iget-object v2, p0, Lfwk;->ad:Lgkc;

    .line 305
    invoke-static {v1, p3, v2}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v1

    iput-object v1, p0, Lfwk;->P:Lgku;

    .line 308
    iget-object v1, p0, Lghl;->e:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lfwk;->P:Lgku;

    invoke-static {v1, v2}, Lgjx;->a(Ljava/util/concurrent/Executor;Lgku;)Lgjx;

    move-result-object v1

    iput-object v1, p0, Lfwk;->Q:Lgku;

    .line 309
    new-instance v1, Lfwl;

    iget-object v2, p0, Lfwk;->p:Leuk;

    iget-object v3, p0, Lfwk;->Q:Lgku;

    invoke-direct {v1, p0, v2, v3}, Lfwl;-><init>(Lfwk;Leuk;Lgku;)V

    iput-object v1, p0, Lfwk;->S:Lgku;

    .line 310
    iget-object v1, p0, Lfwk;->k:Lfxi;

    .line 311
    invoke-virtual {p0, v1, v0}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iget-object v1, p0, Lfwk;->ad:Lgkc;

    .line 310
    invoke-static {v0, p3, v1}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    iput-object v0, p0, Lfwk;->R:Lgku;

    .line 314
    iget-object v0, p0, Lghl;->i:Lfbc;

    .line 315
    invoke-static {v0}, Lfyg;->b(Lfbc;)Lfyg;

    move-result-object v0

    iput-object v0, p0, Lfwk;->m:Lfyg;

    .line 316
    iget-object v0, p0, Lfwk;->j:Lfxi;

    iget-object v1, p0, Lfwk;->m:Lfyg;

    invoke-virtual {p0, v0, v1}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iget-object v1, p0, Lfwk;->ac:Lgkb;

    iget-object v2, p0, Lfwk;->ad:Lgkc;

    invoke-static {v0, v1, v2}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    new-instance v1, Lfwh;

    const-string v2, "playlists"

    invoke-direct {v1, v2}, Lfwh;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfwk;->x:Leuk;

    invoke-static {v1, v2, v0}, Lfwk;->a(Lewh;Leuk;Lgku;)Lgki;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lfwk;->T:Lgku;

    .line 317
    new-instance v0, Lfwh;

    const-string v1, "playlists"

    invoke-direct {v0, v1}, Lfwh;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfwk;->x:Leuk;

    iget-object v2, p0, Lfwk;->R:Lgku;

    invoke-static {v0, v1, v2}, Lfwk;->a(Lewh;Leuk;Lgku;)Lgki;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lfwk;->U:Lgku;

    .line 318
    iget-object v0, p0, Lfwk;->j:Lfxi;

    iget-object v1, p0, Lfwk;->l:Lgad;

    invoke-virtual {p0, v0, v1}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iget-object v1, p0, Lfwk;->ac:Lgkb;

    iget-object v2, p0, Lfwk;->ad:Lgkc;

    invoke-static {v0, v1, v2}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    new-instance v1, Lfwh;

    const-string v2, "favorites"

    invoke-direct {v1, v2}, Lfwh;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfwk;->t:Leuk;

    invoke-static {v1, v2, v0}, Lfwk;->a(Lewh;Leuk;Lgku;)Lgki;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lfwk;->V:Lgku;

    .line 319
    new-instance v0, Lfwh;

    const-string v1, "watch_later"

    invoke-direct {v0, v1}, Lfwh;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfwk;->t:Leuk;

    iget-object v2, p0, Lfwk;->P:Lgku;

    invoke-static {v0, v1, v2}, Lfwk;->a(Lewh;Leuk;Lgku;)Lgki;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lfwk;->W:Lgku;

    .line 320
    new-instance v0, Lfwh;

    const-string v1, "watch_history"

    invoke-direct {v0, v1}, Lfwh;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfwk;->t:Leuk;

    iget-object v2, p0, Lfwk;->P:Lgku;

    invoke-static {v0, v1, v2}, Lfwk;->a(Lewh;Leuk;Lgku;)Lgki;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lfwk;->X:Lgku;

    .line 321
    new-instance v0, Lfwh;

    const-string v1, "favorites"

    invoke-direct {v0, v1}, Lfwh;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfwk;->t:Leuk;

    iget-object v2, p0, Lfwk;->R:Lgku;

    invoke-static {v0, v1, v2}, Lfwk;->a(Lewh;Leuk;Lgku;)Lgki;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    .line 322
    iget-object v0, p0, Lfwk;->j:Lfxi;

    new-instance v1, Lfzf;

    invoke-direct {v1}, Lfzf;-><init>()V

    invoke-virtual {p0, v0, v1}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iget-object v1, p0, Lfwk;->ac:Lgkb;

    iget-object v2, p0, Lfwk;->ad:Lgkc;

    invoke-static {v0, v1, v2}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lfwk;->Y:Lgku;

    .line 323
    new-instance v0, Lfwh;

    const-string v1, "uploads"

    invoke-direct {v0, v1}, Lfwh;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfwk;->t:Leuk;

    iget-object v2, p0, Lfwk;->R:Lgku;

    invoke-static {v0, v1, v2}, Lfwk;->a(Lewh;Leuk;Lgku;)Lgki;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lfwk;->Z:Lgku;

    .line 324
    iget-object v0, p0, Lfwk;->d:Lfxi;

    iget-object v1, p0, Lfwk;->l:Lgad;

    invoke-virtual {p0, v0, v1}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iget-object v1, p0, Lfwk;->ac:Lgkb;

    iget-object v2, p0, Lfwk;->ad:Lgkc;

    invoke-static {v0, v1, v2}, Lgjz;->a(Lgku;Lgkb;Lgkc;)Lgjz;

    move-result-object v0

    new-instance v1, Lfwh;

    const-string v2, "uploads"

    invoke-direct {v1, v2}, Lfwh;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfwk;->t:Leuk;

    invoke-static {v1, v2, v0}, Lfwk;->a(Lewh;Leuk;Lgku;)Lgki;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lfwk;->aa:Lgku;

    .line 326
    const/16 v0, 0x12c

    invoke-static {v0}, Lfwk;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lfwk;->r:Leuk;

    .line 327
    new-instance v0, Leuw;

    iget-object v1, p0, Lfwk;->r:Leuk;

    iget-object v2, p0, Lfwk;->p:Leuk;

    new-instance v3, Lfwp;

    invoke-direct {v3, p0}, Lfwp;-><init>(Lfwk;)V

    invoke-direct {v0, v1, v2, v3}, Leuw;-><init>(Leuk;Leuk;Leux;)V

    iput-object v0, p0, Lfwk;->s:Leuk;

    .line 330
    new-instance v0, Lfxy;

    iget-object v1, p0, Lghl;->i:Lfbc;

    invoke-direct {v0, v1}, Lfxy;-><init>(Lfbc;)V

    iget-object v1, p0, Lfwk;->c:Lfxi;

    invoke-virtual {p0, v1, v0}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    iget-object v1, p0, Lfwk;->s:Leuk;

    iget-object v2, p0, Lghl;->f:Lezj;

    const-wide/32 v4, 0xea60

    invoke-static {v1, v0, v2, v4, v5}, Lghi;->a(Leuk;Lgku;Lezj;J)Lghi;

    .line 331
    new-instance v0, Lfxx;

    iget-object v1, p0, Lghl;->i:Lfbc;

    invoke-direct {v0, v1}, Lfxx;-><init>(Lfbc;)V

    iget-object v1, p0, Lfwk;->c:Lfxi;

    invoke-virtual {p0, v1, v0}, Lghl;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    invoke-virtual {p0, v0}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v0

    const/16 v1, 0x14

    invoke-static {v1}, Lfwk;->a(I)Leul;

    move-result-object v1

    new-instance v2, Leuw;

    iget-object v3, p0, Lfwk;->s:Leuk;

    new-instance v4, Lfwo;

    invoke-direct {v4, p0}, Lfwo;-><init>(Lfwk;)V

    invoke-direct {v2, v1, v3, v4}, Leuw;-><init>(Leuk;Leuk;Leux;)V

    const-wide/32 v4, 0xea60

    invoke-virtual {p0, v2, v0, v4, v5}, Lghl;->a(Leuk;Lgku;J)Lghi;

    .line 333
    new-instance v0, Lfwn;

    invoke-direct {v0, p0}, Lfwn;-><init>(Lfwk;)V

    iget-object v1, p0, Lfwk;->P:Lgku;

    invoke-virtual {p0, v1}, Lghl;->a(Lgku;)Lgjx;

    move-result-object v1

    iget-object v2, p0, Lfwk;->z:Leuk;

    invoke-static {v0, v2, v1}, Lfwk;->a(Lewh;Leuk;Lgku;)Lgki;

    move-result-object v0

    iput-object v0, p0, Lfwk;->ab:Lgku;

    .line 335
    invoke-direct {p0}, Lfwk;->i()Lgku;

    .line 336
    new-instance v0, Lfyz;

    invoke-direct {p0}, Lfwk;->i()Lgku;

    move-result-object v1

    invoke-direct {v0, v1}, Lfyz;-><init>(Lgku;)V

    .line 337
    return-void
.end method

.method public final a(Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 787
    iget-object v0, p0, Lfwk;->B:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    invoke-virtual {v1, p1}, Lfxj;->a(Ljava/lang/String;)Lfxg;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 788
    return-void
.end method

.method public final a(Ljava/lang/String;Lfxk;Ljava/lang/String;Leuc;)V
    .locals 8

    .prologue
    .line 900
    iget-object v1, p0, Lfwk;->S:Lgku;

    iget-object v0, p0, Lfwk;->a:Lfxj;

    .line 901
    iget-object v0, v0, Lfxj;->a:Lfxl;

    invoke-interface {v0}, Lfxl;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "videos"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "complaints"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxk;

    invoke-virtual {v0}, Lfxk;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'><summary>"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1f4

    const-string v5, ""

    invoke-static {p3, v4, v5}, Lfaq;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x6a

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</summary><category scheme=\'http://gdata.youtube.com/schemas/2007/complaint-reasons.cat\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "term=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'/></entry>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->B(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v2, v0}, Lfxg;->a(Landroid/net/Uri;[B)Lfxg;

    move-result-object v0

    .line 900
    invoke-interface {v1, v0, p4}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 903
    return-void
.end method

.method public final a(Ljava/lang/String;Lgcg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Leuc;)V
    .locals 14

    .prologue
    .line 886
    iget-object v4, p0, Lfwk;->a:Lfxj;

    const-string v1, "filename may not be empty"

    invoke-static {p1, v1}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-static/range {p2 .. p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Slug"

    invoke-interface {v5, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "<?xml version=\'1.0\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:media=\'http://search.yahoo.com/mrss/\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\' xmlns:gml=\'http://www.opengis.net/gml\' xmlns:georss=\'http://www.georss.org/georss\'><media:group><media:title type=\'plain\'>"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/16 v1, 0x3c

    move-object/from16 v0, p3

    invoke-static {v0, v1, p1}, Lfaq;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x1388

    const-string v2, ""

    move-object/from16 v0, p4

    invoke-static {v0, v1, v2}, Lfaq;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x34

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "<media:description type=\'plain\'>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</media:description>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string p5, "People"

    :cond_0
    const/16 v2, 0x1f4

    const-string v3, ""

    move-object/from16 v0, p6

    invoke-static {v0, v2, v3}, Lfaq;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    sget-object v2, Lgcg;->c:Lgcg;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_2

    const-string v2, "<yt:private/>"

    move-object v3, v2

    :goto_1
    if-eqz p7, :cond_4

    const-string v2, "<georss:where><gml:Point><gml:pos>"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p7

    iget-object v9, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p7

    iget-object v10, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x26

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, "</gml:pos></gml:Point></georss:where>"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit16 v10, v10, 0xa4

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</media:title>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "<media:category scheme=\'http://gdata.youtube.com/schemas/2007/categories.cat\'>"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "</media:category><media:keywords>"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "</media:keywords>"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "</media:group>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</entry>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v4, Lfxj;->a:Lfxl;

    invoke-interface {v2}, Lfxl;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "/resumable/feeds/api/users/default/uploads"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1}, La;->B(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lfxg;

    invoke-direct {v3, v2, v5, v1}, Lfxg;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    .line 888
    iget-object v1, p0, Lfwk;->Y:Lgku;

    move-object/from16 v0, p8

    invoke-interface {v1, v3, v0}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 889
    return-void

    .line 886
    :cond_1
    const-string v1, ""

    goto/16 :goto_0

    :cond_2
    sget-object v2, Lgcg;->a:Lgcg;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_3

    const-string v2, "allowed"

    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x2f

    invoke-direct {v3, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "<yt:accessControl action=\'list\' permission=\'"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'/>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_1

    :cond_3
    const-string v2, "denied"

    goto :goto_3

    :cond_4
    const-string v2, ""

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgcg;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Leuc;)V
    .locals 2

    .prologue
    .line 961
    iget-object v0, p0, Lfwk;->a:Lfxj;

    invoke-static/range {p1 .. p10}, Lfxj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgcg;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Lfxg;

    move-result-object v0

    .line 964
    iget-object v1, p0, Lfwk;->aa:Lgku;

    invoke-interface {v1, v0, p11}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 965
    return-void
.end method

.method public final a(Ljava/lang/String;ZLeuc;)V
    .locals 8

    .prologue
    .line 936
    iget-object v1, p0, Lfwk;->T:Lgku;

    iget-object v0, p0, Lfwk;->a:Lfxj;

    .line 937
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lfxj;->a:Lfxl;

    invoke-interface {v0}, Lfxl;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "users"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "default"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "playlists"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const-string v0, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'><title type=\'text\'>"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/16 v0, 0x3c

    const-string v4, ""

    invoke-static {p1, v0, v4}, Lfaq;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-eqz p2, :cond_0

    const-string v0, "<yt:private/>"

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x23

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</title><summary></summary>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</entry>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->B(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v2, v0}, Lfxg;->a(Landroid/net/Uri;[B)Lfxg;

    move-result-object v0

    .line 936
    invoke-interface {v1, v0, p3}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 939
    return-void

    .line 937
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 700
    iget-object v0, p0, Lfwk;->t:Leuk;

    iget-object v1, p0, Lfwk;->M:Lfwh;

    invoke-interface {v0, v1}, Leuk;->a(Lewh;)V

    .line 701
    return-void
.end method

.method public final b(ILeuc;)V
    .locals 4

    .prologue
    .line 840
    iget-object v0, p0, Lfwk;->K:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    const/16 v2, 0xf

    .line 841
    iget-object v1, v1, Lfxj;->a:Lfxl;

    invoke-interface {v1}, Lfxl;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "/feeds/api/users/default/favorites"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v1, v3, v2}, Lfxj;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v1

    .line 840
    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 843
    return-void
.end method

.method public final b(Leuc;)V
    .locals 3

    .prologue
    .line 976
    iget-object v0, p0, Lfwk;->ab:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    .line 977
    iget-object v1, v1, Lfxj;->a:Lfxl;

    invoke-interface {v1}, Lfxl;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "channels"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'></entry>"

    invoke-static {v2}, La;->B(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lfxg;->a(Landroid/net/Uri;[B)Lfxg;

    move-result-object v1

    .line 976
    invoke-interface {v0, v1, p1}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 978
    return-void
.end method

.method public final b(Ljava/lang/String;Leuc;)V
    .locals 4

    .prologue
    .line 797
    iget-object v0, p0, Lfwk;->H:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v1, Lfxj;->a:Lfxl;

    invoke-interface {v1}, Lfxl;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "playlists"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lfxj;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 798
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lfwk;->p:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 706
    iget-object v0, p0, Lfwk;->q:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 707
    iget-object v0, p0, Lfwk;->r:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 708
    iget-object v0, p0, Lfwk;->s:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 709
    iget-object v0, p0, Lfwk;->t:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 710
    iget-object v0, p0, Lfwk;->u:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 711
    iget-object v0, p0, Lfwk;->v:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 712
    iget-object v0, p0, Lfwk;->w:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 713
    iget-object v0, p0, Lfwk;->x:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 714
    iget-object v0, p0, Lfwk;->y:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 715
    iget-object v0, p0, Lfwk;->z:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 716
    return-void
.end method

.method public final c(ILeuc;)V
    .locals 4

    .prologue
    .line 847
    iget-object v0, p0, Lfwk;->O:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    const/16 v2, 0xf

    .line 848
    iget-object v1, v1, Lfxj;->a:Lfxl;

    invoke-interface {v1}, Lfxl;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "/feeds/api/users/default/watch_later"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v1, v3, v2}, Lfxj;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v1

    .line 847
    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 850
    return-void
.end method

.method public final c(Ljava/lang/String;Leuc;)V
    .locals 1

    .prologue
    .line 808
    iget-object v0, p0, Lfwk;->J:Lgku;

    invoke-interface {v0, p1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 809
    return-void
.end method

.method public final d()Lgku;
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lfwk;->A:Lgku;

    return-object v0
.end method

.method public final d(ILeuc;)V
    .locals 5

    .prologue
    .line 854
    iget-object v0, p0, Lfwk;->K:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    const/16 v2, 0xf

    .line 855
    iget-object v1, v1, Lfxj;->a:Lfxl;

    invoke-interface {v1}, Lfxl;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "/feeds/api/users/default/watch_history"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "inline"

    const-string v4, "true"

    invoke-virtual {v1, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v1, v3, v2}, Lfxj;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v1

    .line 854
    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 857
    return-void
.end method

.method public final d(Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 818
    iget-object v0, p0, Lfwk;->A:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    invoke-virtual {v1, p1}, Lfxj;->a(Ljava/lang/String;)Lfxg;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 819
    return-void
.end method

.method public final e()Lgku;
    .locals 1

    .prologue
    .line 982
    iget-object v0, p0, Lfwk;->F:Lgku;

    return-object v0
.end method

.method public final e(Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 823
    iget-object v0, p0, Lfwk;->a:Lfxj;

    const-string v1, "videoId cannot be empty"

    invoke-static {p1, v1}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {v0}, Lfxj;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v0

    .line 827
    iget-object v1, p0, Lfwk;->p:Leuk;

    invoke-interface {v1, v0}, Leuk;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 828
    iget-object v1, p0, Lfwk;->C:Lgku;

    invoke-interface {v1, v0, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 829
    return-void
.end method

.method public final f()Lgku;
    .locals 1

    .prologue
    .line 1002
    iget-object v0, p0, Lfwk;->L:Lgku;

    return-object v0
.end method

.method public final f(Ljava/lang/String;Leuc;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 894
    iget-object v0, p0, Lfwk;->G:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    const-string v2, "username cannot be empty"

    invoke-static {p1, v2}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {v1, p1}, Lfxj;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 895
    return-void
.end method

.method public final g()Lgku;
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lfwk;->N:Lgku;

    return-object v0
.end method

.method public final g(Ljava/lang/String;Leuc;)V
    .locals 6

    .prologue
    .line 907
    iget-object v0, p0, Lfwk;->V:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    .line 908
    iget-object v1, v1, Lfxj;->a:Lfxl;

    invoke-interface {v1}, Lfxl;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "users"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "default"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "favorites"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\'><id>"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</id></entry>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, La;->B(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lfxg;->a(Landroid/net/Uri;[B)Lfxg;

    move-result-object v1

    .line 907
    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 910
    return-void
.end method

.method public final h(Ljava/lang/String;Leuc;)V
    .locals 6

    .prologue
    .line 921
    iget-object v0, p0, Lfwk;->W:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    .line 922
    iget-object v1, v1, Lfxj;->a:Lfxl;

    invoke-interface {v1}, Lfxl;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "users"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "default"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "watch_later"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'><id>"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</id></entry>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, La;->B(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lfxg;->a(Landroid/net/Uri;[B)Lfxg;

    move-result-object v1

    .line 921
    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 924
    return-void
.end method

.method public final i(Ljava/lang/String;Leuc;)V
    .locals 6

    .prologue
    .line 928
    iget-object v0, p0, Lfwk;->X:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    .line 929
    iget-object v1, v1, Lfxj;->a:Lfxl;

    invoke-interface {v1}, Lfxl;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "users"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "default"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "watch_history"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'><id>"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</id></entry>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, La;->B(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lfxg;->a(Landroid/net/Uri;[B)Lfxg;

    move-result-object v1

    .line 928
    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 931
    return-void
.end method

.method public final j(Ljava/lang/String;Leuc;)V
    .locals 3

    .prologue
    .line 943
    iget-object v0, p0, Lfwk;->U:Lgku;

    iget-object v1, p0, Lfwk;->a:Lfxj;

    .line 944
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, v1, Lfxj;->a:Lfxl;

    invoke-interface {v1}, Lfxl;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "users"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "default"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "playlists"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v1

    .line 943
    invoke-interface {v0, v1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 946
    return-void
.end method
