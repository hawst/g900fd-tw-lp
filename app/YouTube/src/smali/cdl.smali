.class public final Lcdl;
.super Lcbd;
.source "SourceFile"


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

.field private final d:Lfsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Lfhz;Lfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1, p3, p4, p5}, Lcbd;-><init>(Landroid/content/Context;Lfhz;Lfdw;Lfrz;)V

    .line 39
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcdl;->d:Lfsj;

    .line 41
    const v0, 0x7f04005f

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcdl;->b:Landroid/view/View;

    .line 42
    iget-object v0, p0, Lcdl;->b:Landroid/view/View;

    const v1, 0x7f0801be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iput-object v0, p0, Lcdl;->c:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 44
    iget-object v0, p0, Lcdl;->b:Landroid/view/View;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 45
    return-void
.end method

.method private a(Lfsg;Lfjs;)Landroid/view/View;
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p2}, Lfjs;->b()Lhog;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcdl;->c:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget-object v2, p2, Lfjs;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_0

    iget-object v2, p2, Lfjs;->a:Lhfw;

    iget-object v2, v2, Lhfw;->a:Lhgz;

    if-eqz v2, :cond_0

    iget-object v2, p2, Lfjs;->a:Lhfw;

    iget-object v2, v2, Lhfw;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, p2, Lfjs;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v2, p2, Lfjs;->b:Ljava/lang/CharSequence;

    .line 52
    iget-object v3, p2, Lfjs;->c:Ljava/lang/CharSequence;

    if-nez v3, :cond_1

    iget-object v3, p2, Lfjs;->a:Lhfw;

    iget-object v3, v3, Lhfw;->b:Lhgz;

    if-eqz v3, :cond_1

    iget-object v3, p2, Lfjs;->a:Lhfw;

    iget-object v3, v3, Lhfw;->b:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, p2, Lfjs;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v3, p2, Lfjs;->c:Ljava/lang/CharSequence;

    .line 51
    invoke-virtual {p0, v2, v3, v0}, Lcdl;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lhog;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcdl;->d:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfjs;

    invoke-direct {p0, p1, p2}, Lcdl;->a(Lfsg;Lfjs;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfjs;

    invoke-direct {p0, p1, p2}, Lcdl;->a(Lfsg;Lfjs;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
