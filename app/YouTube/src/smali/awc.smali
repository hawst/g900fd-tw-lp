.class final Lawc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;
.implements Ljava/lang/Runnable;


# instance fields
.field public a:Z

.field public b:Z

.field private final c:Lawb;

.field private d:Landroid/graphics/Bitmap;

.field private synthetic e:Lawb;


# direct methods
.method constructor <init>(Lawb;Lawb;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 111
    iput-object p1, p0, Lawc;->e:Lawb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-boolean v0, p0, Lawc;->a:Z

    .line 108
    iput-boolean v0, p0, Lawc;->b:Z

    .line 112
    iput-object p2, p0, Lawc;->c:Lawb;

    .line 113
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lawc;->c:Lawb;

    invoke-static {v0}, Lawb;->a(Lawb;)Lawc;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lawc;->c:Lawb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lawb;->a(Lawb;Lawc;)Lawc;

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 104
    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lawc;->c:Lawb;

    invoke-static {v0}, Lawb;->a(Lawb;)Lawc;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lawc;->b:Z

    iput-object p2, p0, Lawc;->d:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lawc;->e:Lawb;

    invoke-static {v0}, Lawb;->b(Lawb;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lawc;->e:Lawb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lawb;->a(Lawb;Z)Z

    invoke-static {}, Lawb;->a()Landroid/os/Handler;

    move-result-object v0

    invoke-static {}, Lawb;->a()Landroid/os/Handler;

    move-result-object v1

    invoke-static {v1, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lawc;->run()V

    goto :goto_0
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 139
    iget-object v0, p0, Lawc;->c:Lawb;

    invoke-static {v0}, Lawb;->a(Lawb;)Lawc;

    move-result-object v0

    if-ne v0, p0, :cond_2

    .line 140
    iget-object v0, p0, Lawc;->c:Lawb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lawb;->a(Lawb;Lawc;)Lawc;

    .line 141
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lawc;->e:Lawb;

    invoke-static {v1}, Lawb;->c(Lawb;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lawc;->d:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 142
    iget-object v1, p0, Lawc;->c:Lawb;

    invoke-static {v1}, Lawb;->d(Lawb;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 143
    iget-object v1, p0, Lawc;->e:Lawb;

    invoke-static {v1}, Lawb;->e(Lawb;)Lawd;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lawc;->e:Lawb;

    invoke-static {v1}, Lawb;->e(Lawb;)Lawd;

    move-result-object v1

    iget-object v2, p0, Lawc;->e:Lawb;

    iget-object v2, p0, Lawc;->d:Landroid/graphics/Bitmap;

    invoke-interface {v1}, Lawd;->a()V

    .line 146
    :cond_0
    iget-object v1, p0, Lawc;->c:Lawb;

    invoke-static {v1}, Lawb;->d(Lawb;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v1

    sget-object v2, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne v1, v2, :cond_1

    .line 147
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 148
    iget-object v2, p0, Lawc;->e:Lawb;

    iget-object v3, p0, Lawc;->c:Lawb;

    invoke-static {v3}, Lawb;->d(Lawb;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v2, v1, v3, v0}, Lawb;->a(Landroid/graphics/Matrix;Landroid/widget/ImageView;Landroid/graphics/drawable/BitmapDrawable;)V

    .line 149
    iget-object v0, p0, Lawc;->c:Lawb;

    invoke-static {v0}, Lawb;->d(Lawb;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 151
    :cond_1
    iget-boolean v0, p0, Lawc;->a:Z

    if-eqz v0, :cond_3

    .line 152
    iget-object v0, p0, Lawc;->e:Lawb;

    invoke-static {v0}, Lawb;->f(Lawb;)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 153
    iget-object v0, p0, Lawc;->e:Lawb;

    invoke-static {v0}, Lawb;->f(Lawb;)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lawc;->e:Lawb;

    invoke-static {v1}, Lawb;->g(Lawb;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 154
    iget-object v0, p0, Lawc;->c:Lawb;

    invoke-static {v0}, Lawb;->d(Lawb;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lawc;->e:Lawb;

    invoke-static {v1}, Lawb;->f(Lawb;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 159
    :cond_2
    :goto_0
    return-void

    .line 155
    :cond_3
    iget-object v0, p0, Lawc;->e:Lawb;

    invoke-static {v0}, Lawb;->e(Lawb;)Lawd;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 156
    iget-object v0, p0, Lawc;->e:Lawb;

    invoke-static {v0}, Lawb;->e(Lawb;)Lawd;

    move-result-object v0

    iget-object v1, p0, Lawc;->e:Lawb;

    invoke-interface {v0}, Lawd;->b()V

    goto :goto_0
.end method
