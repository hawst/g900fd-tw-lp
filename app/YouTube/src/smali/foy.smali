.class public Lfoy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lghz;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final a:Lfoy;

.field public static final al:Lfpd;


# instance fields
.field public final A:Ljava/util/List;

.field public final B:Ljava/util/List;

.field public final C:Ljava/util/List;

.field public final D:Ljava/util/List;

.field public final E:Ljava/util/List;

.field public final F:Ljava/util/List;

.field final G:Ljava/util/List;

.field public final H:Ljava/util/List;

.field public final I:Ljava/util/List;

.field public final J:Ljava/util/List;

.field final K:Ljava/util/List;

.field public final L:Ljava/util/List;

.field public final M:Ljava/util/List;

.field public final N:Landroid/net/Uri;

.field public final O:Landroid/net/Uri;

.field public final P:Z

.field public final Q:Z

.field public final R:J

.field public final S:I

.field public final T:Z

.field public final U:Z

.field public final V:Lhqz;

.field public final W:Lfkg;

.field public final X:Landroid/net/Uri;

.field public final Y:Z

.field public final Z:Lfoy;

.field public final aa:Lfoy;

.field final ab:J

.field public final ac:Ljava/lang/String;

.field public final ad:Ljava/lang/String;

.field public final ae:Ljava/lang/String;

.field public final af:Z

.field public final ag:Z

.field public final ah:Ljava/util/List;

.field public final ai:Ljava/util/List;

.field public final aj:Lfoo;

.field public final ak:Ljava/util/List;

.field private final am:Ljava/lang/String;

.field private final an:Landroid/net/Uri;

.field public final b:Ljava/util/List;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:[B

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field final j:Ljava/lang/String;

.field final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Lfpb;

.field public final n:Ljava/lang/String;

.field public final o:I

.field public final p:Lfrf;

.field public final q:Lflp;

.field public final r:Lfqy;

.field public final s:Lflr;

.field public final t:Landroid/net/Uri;

.field public final u:Ljava/util/List;

.field public final v:Ljava/util/List;

.field public final w:Ljava/util/List;

.field public final x:Ljava/util/List;

.field public final y:Ljava/util/List;

.field public final z:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lfoy;

    invoke-direct {v0}, Lfoy;-><init>()V

    sput-object v0, Lfoy;->a:Lfoy;

    .line 1957
    new-instance v0, Lfoz;

    invoke-direct {v0}, Lfoz;-><init>()V

    sput-object v0, Lfoy;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 2232
    new-instance v0, Lfpd;

    invoke-direct {v0}, Lfpd;-><init>()V

    sput-object v0, Lfoy;->al:Lfpd;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 608
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->b:Ljava/util/List;

    .line 609
    iput-object v1, p0, Lfoy;->c:Ljava/lang/String;

    .line 610
    iput-object v1, p0, Lfoy;->d:Ljava/lang/String;

    .line 611
    iput-object v1, p0, Lfoy;->e:Ljava/lang/String;

    .line 612
    iput-object v1, p0, Lfoy;->f:Ljava/lang/String;

    .line 613
    iput-object v1, p0, Lfoy;->g:[B

    .line 614
    iput-object v1, p0, Lfoy;->h:Ljava/lang/String;

    .line 615
    iput-object v1, p0, Lfoy;->i:Ljava/lang/String;

    .line 616
    iput-object v1, p0, Lfoy;->j:Ljava/lang/String;

    .line 617
    iput-object v1, p0, Lfoy;->am:Ljava/lang/String;

    .line 618
    iput-object v1, p0, Lfoy;->an:Landroid/net/Uri;

    .line 619
    iput-object v1, p0, Lfoy;->k:Ljava/lang/String;

    .line 620
    iput-object v1, p0, Lfoy;->l:Ljava/lang/String;

    .line 621
    sget-object v0, Lfpb;->d:Lfpb;

    iput-object v0, p0, Lfoy;->m:Lfpb;

    .line 622
    iput-object v1, p0, Lfoy;->n:Ljava/lang/String;

    .line 623
    iput v2, p0, Lfoy;->o:I

    .line 624
    iput-object v1, p0, Lfoy;->p:Lfrf;

    .line 625
    new-instance v0, Lflp;

    invoke-direct {v0}, Lflp;-><init>()V

    iput-object v0, p0, Lfoy;->q:Lflp;

    .line 626
    new-instance v0, Lfqy;

    invoke-direct {v0}, Lfqy;-><init>()V

    iput-object v0, p0, Lfoy;->r:Lfqy;

    .line 627
    iput-object v1, p0, Lfoy;->s:Lflr;

    .line 628
    iput-object v1, p0, Lfoy;->t:Landroid/net/Uri;

    .line 629
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->u:Ljava/util/List;

    .line 630
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->v:Ljava/util/List;

    .line 631
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->w:Ljava/util/List;

    .line 632
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->x:Ljava/util/List;

    .line 633
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->y:Ljava/util/List;

    .line 634
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->z:Ljava/util/List;

    .line 635
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->A:Ljava/util/List;

    .line 636
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->B:Ljava/util/List;

    .line 637
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->C:Ljava/util/List;

    .line 638
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->D:Ljava/util/List;

    .line 639
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->E:Ljava/util/List;

    .line 640
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->F:Ljava/util/List;

    .line 641
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->G:Ljava/util/List;

    .line 642
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->H:Ljava/util/List;

    .line 643
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->I:Ljava/util/List;

    .line 644
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->J:Ljava/util/List;

    .line 645
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->K:Ljava/util/List;

    .line 646
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->L:Ljava/util/List;

    .line 647
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->M:Ljava/util/List;

    .line 648
    iput-object v1, p0, Lfoy;->N:Landroid/net/Uri;

    .line 649
    iput-object v1, p0, Lfoy;->O:Landroid/net/Uri;

    .line 650
    iput-boolean v2, p0, Lfoy;->P:Z

    .line 651
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfoy;->Q:Z

    .line 652
    iput-wide v4, p0, Lfoy;->R:J

    .line 653
    const/4 v0, -0x1

    iput v0, p0, Lfoy;->S:I

    .line 654
    iput-boolean v2, p0, Lfoy;->T:Z

    .line 655
    iput-boolean v2, p0, Lfoy;->U:Z

    .line 656
    iput-object v1, p0, Lfoy;->V:Lhqz;

    .line 657
    iput-object v1, p0, Lfoy;->W:Lfkg;

    .line 658
    iput-wide v4, p0, Lfoy;->ab:J

    .line 659
    iput-boolean v2, p0, Lfoy;->af:Z

    .line 660
    iput-boolean v2, p0, Lfoy;->ag:Z

    .line 661
    iput-object v1, p0, Lfoy;->X:Landroid/net/Uri;

    .line 662
    iput-boolean v2, p0, Lfoy;->Y:Z

    .line 663
    iput-object v1, p0, Lfoy;->Z:Lfoy;

    .line 664
    iput-object v1, p0, Lfoy;->aa:Lfoy;

    .line 665
    invoke-direct {p0}, Lfoy;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfoy;->ae:Ljava/lang/String;

    .line 666
    invoke-direct {p0}, Lfoy;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfoy;->ac:Ljava/lang/String;

    .line 667
    invoke-direct {p0}, Lfoy;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfoy;->ad:Ljava/lang/String;

    .line 668
    iput-object v1, p0, Lfoy;->ah:Ljava/util/List;

    .line 669
    iput-object v1, p0, Lfoy;->ai:Ljava/util/List;

    .line 670
    iput-object v1, p0, Lfoy;->aj:Lfoo;

    .line 671
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfoy;->ak:Ljava/util/List;

    .line 672
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 65

    .prologue
    .line 2078
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v3

    .line 2079
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2080
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 2081
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 2082
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 2083
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v8

    .line 2084
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .line 2085
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 2086
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 2087
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    const-class v2, Landroid/net/Uri;

    .line 2088
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    .line 2089
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    .line 2090
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v15

    const-class v2, Lfpb;

    .line 2091
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v2, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v16

    check-cast v16, Lfpb;

    .line 2092
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v17

    .line 2093
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v18

    const-class v2, Lfrf;

    .line 2094
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v19

    check-cast v19, Lfrf;

    const-class v2, Lflp;

    .line 2095
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v20

    check-cast v20, Lflp;

    const-class v2, Lfqy;

    .line 2096
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v21

    check-cast v21, Lfqy;

    const-class v2, Lflr;

    .line 2097
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v22

    check-cast v22, Lflr;

    const-class v2, Landroid/net/Uri;

    .line 2098
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Landroid/net/Uri;

    .line 2099
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v24

    .line 2100
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v25

    .line 2101
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v26

    .line 2102
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v27

    .line 2103
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sget-object v28, Lfpf;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v2, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v28

    .line 2104
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v29

    .line 2105
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v30

    .line 2106
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v31

    .line 2107
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v32

    .line 2108
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v33

    .line 2109
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v34

    .line 2110
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v35

    .line 2111
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v36

    .line 2112
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v37

    .line 2113
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v38

    .line 2114
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v39

    .line 2115
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v40

    .line 2116
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v41

    .line 2117
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v42

    const-class v2, Landroid/net/Uri;

    .line 2118
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v43

    check-cast v43, Landroid/net/Uri;

    const-class v2, Landroid/net/Uri;

    .line 2119
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v44

    check-cast v44, Landroid/net/Uri;

    .line 2120
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/16 v45, 0x1

    move/from16 v0, v45

    if-ne v2, v0, :cond_0

    const/16 v45, 0x1

    .line 2121
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/16 v46, 0x1

    move/from16 v0, v46

    if-ne v2, v0, :cond_1

    const/16 v46, 0x1

    .line 2122
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v47

    .line 2123
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v49

    .line 2124
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/16 v50, 0x1

    move/from16 v0, v50

    if-ne v2, v0, :cond_2

    const/16 v50, 0x1

    .line 2125
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/16 v51, 0x1

    move/from16 v0, v51

    if-ne v2, v0, :cond_3

    const/16 v51, 0x1

    :goto_3
    new-instance v2, Lhqz;

    invoke-direct {v2}, Lhqz;-><init>()V

    .line 2126
    move-object/from16 v0, p1

    invoke-static {v0, v2}, La;->b(Landroid/os/Parcel;Lidh;)Lidh;

    move-result-object v52

    check-cast v52, Lhqz;

    const-class v2, Lfkg;

    .line 2128
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 2127
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v53

    check-cast v53, Lfkg;

    .line 2129
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v54

    .line 2130
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/16 v56, 0x1

    move/from16 v0, v56

    if-ne v2, v0, :cond_4

    const/16 v56, 0x1

    .line 2131
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/16 v57, 0x1

    move/from16 v0, v57

    if-ne v2, v0, :cond_5

    const/16 v57, 0x1

    :goto_5
    const-class v2, Landroid/net/Uri;

    .line 2132
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v58

    check-cast v58, Landroid/net/Uri;

    const-class v2, Lfoy;

    .line 2133
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v59

    check-cast v59, Lfoy;

    const-class v2, Lfoy;

    .line 2134
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v60

    check-cast v60, Lfoy;

    .line 2135
    invoke-static/range {p1 .. p1}, Lfoy;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v61

    .line 2136
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sget-object v62, Lfpi;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-virtual {v0, v2, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v62

    const-class v2, Lfoo;

    .line 2137
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v63

    check-cast v63, Lfoo;

    .line 2138
    invoke-static/range {p1 .. p1}, Lfoy;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v64

    move-object/from16 v2, p0

    .line 2078
    invoke-direct/range {v2 .. v64}, Lfoy;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lfpb;Ljava/lang/String;ILfrf;Lflp;Lfqy;Lflr;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;Landroid/net/Uri;ZZJIZZLhqz;Lfkg;JZZLandroid/net/Uri;Lfoy;Lfoy;Ljava/util/List;Ljava/util/List;Lfoo;Ljava/util/List;)V

    .line 2139
    return-void

    .line 2120
    :cond_0
    const/16 v45, 0x0

    goto/16 :goto_0

    .line 2121
    :cond_1
    const/16 v46, 0x0

    goto/16 :goto_1

    .line 2124
    :cond_2
    const/16 v50, 0x0

    goto/16 :goto_2

    .line 2125
    :cond_3
    const/16 v51, 0x0

    goto/16 :goto_3

    .line 2130
    :cond_4
    const/16 v56, 0x0

    goto :goto_4

    .line 2131
    :cond_5
    const/16 v57, 0x0

    goto :goto_5
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lfpb;Ljava/lang/String;ILfrf;Lflp;Lfqy;Lflr;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;Landroid/net/Uri;ZZJIZZLhqz;Lfkg;JZZLandroid/net/Uri;Lfoy;Lfoy;Ljava/util/List;Ljava/util/List;Lfoo;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 535
    invoke-static {p1}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->b:Ljava/util/List;

    .line 536
    iput-object p2, p0, Lfoy;->c:Ljava/lang/String;

    .line 537
    iput-object p3, p0, Lfoy;->d:Ljava/lang/String;

    .line 538
    if-eqz p4, :cond_0

    .line 539
    :goto_0
    iput-object p4, p0, Lfoy;->e:Ljava/lang/String;

    .line 540
    if-eqz p5, :cond_2

    .line 541
    :goto_1
    iput-object p5, p0, Lfoy;->f:Ljava/lang/String;

    .line 542
    iput-object p6, p0, Lfoy;->g:[B

    .line 543
    iput-object p7, p0, Lfoy;->h:Ljava/lang/String;

    .line 544
    iput-object p8, p0, Lfoy;->i:Ljava/lang/String;

    .line 545
    iput-object p9, p0, Lfoy;->j:Ljava/lang/String;

    .line 546
    iput-object p10, p0, Lfoy;->am:Ljava/lang/String;

    .line 547
    iput-object p11, p0, Lfoy;->an:Landroid/net/Uri;

    .line 548
    iput-object p12, p0, Lfoy;->k:Ljava/lang/String;

    .line 549
    move-object/from16 v0, p13

    iput-object v0, p0, Lfoy;->l:Ljava/lang/String;

    .line 550
    move-object/from16 v0, p14

    iput-object v0, p0, Lfoy;->m:Lfpb;

    .line 551
    move-object/from16 v0, p15

    iput-object v0, p0, Lfoy;->n:Ljava/lang/String;

    .line 552
    move/from16 v0, p16

    iput v0, p0, Lfoy;->o:I

    .line 553
    move-object/from16 v0, p17

    iput-object v0, p0, Lfoy;->p:Lfrf;

    .line 554
    invoke-static/range {p18 .. p18}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflp;

    iput-object v2, p0, Lfoy;->q:Lflp;

    .line 555
    invoke-static/range {p19 .. p19}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfqy;

    iput-object v2, p0, Lfoy;->r:Lfqy;

    .line 556
    move-object/from16 v0, p20

    iput-object v0, p0, Lfoy;->s:Lflr;

    .line 557
    move-object/from16 v0, p21

    iput-object v0, p0, Lfoy;->t:Landroid/net/Uri;

    .line 558
    invoke-static/range {p22 .. p22}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->u:Ljava/util/List;

    .line 559
    invoke-static/range {p23 .. p23}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->v:Ljava/util/List;

    .line 560
    invoke-static/range {p24 .. p24}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->w:Ljava/util/List;

    .line 561
    invoke-static/range {p25 .. p25}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->x:Ljava/util/List;

    .line 562
    invoke-static/range {p26 .. p26}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->y:Ljava/util/List;

    .line 563
    invoke-static/range {p27 .. p27}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->z:Ljava/util/List;

    .line 564
    invoke-static/range {p28 .. p28}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->A:Ljava/util/List;

    .line 565
    invoke-static/range {p29 .. p29}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->B:Ljava/util/List;

    .line 566
    invoke-static/range {p30 .. p30}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->C:Ljava/util/List;

    .line 567
    invoke-static/range {p31 .. p31}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->D:Ljava/util/List;

    .line 568
    invoke-static/range {p32 .. p32}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->E:Ljava/util/List;

    .line 569
    invoke-static/range {p33 .. p33}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->F:Ljava/util/List;

    .line 570
    invoke-static/range {p34 .. p34}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->G:Ljava/util/List;

    .line 571
    invoke-static/range {p35 .. p35}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->H:Ljava/util/List;

    .line 572
    invoke-static/range {p36 .. p36}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->I:Ljava/util/List;

    .line 573
    invoke-static/range {p37 .. p37}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->J:Ljava/util/List;

    .line 574
    invoke-static/range {p38 .. p38}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->K:Ljava/util/List;

    .line 575
    invoke-static/range {p39 .. p39}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->L:Ljava/util/List;

    .line 576
    invoke-static/range {p40 .. p40}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->M:Ljava/util/List;

    .line 577
    move-object/from16 v0, p41

    iput-object v0, p0, Lfoy;->N:Landroid/net/Uri;

    .line 578
    move-object/from16 v0, p42

    iput-object v0, p0, Lfoy;->O:Landroid/net/Uri;

    .line 579
    move/from16 v0, p43

    iput-boolean v0, p0, Lfoy;->P:Z

    .line 580
    move/from16 v0, p44

    iput-boolean v0, p0, Lfoy;->Q:Z

    .line 581
    move-wide/from16 v0, p45

    iput-wide v0, p0, Lfoy;->R:J

    .line 582
    move/from16 v0, p47

    iput v0, p0, Lfoy;->S:I

    .line 583
    move/from16 v0, p48

    iput-boolean v0, p0, Lfoy;->T:Z

    .line 584
    move/from16 v0, p49

    iput-boolean v0, p0, Lfoy;->U:Z

    .line 585
    move-object/from16 v0, p50

    iput-object v0, p0, Lfoy;->V:Lhqz;

    .line 586
    move-object/from16 v0, p51

    iput-object v0, p0, Lfoy;->W:Lfkg;

    .line 587
    move-wide/from16 v0, p52

    iput-wide v0, p0, Lfoy;->ab:J

    .line 588
    move/from16 v0, p54

    iput-boolean v0, p0, Lfoy;->af:Z

    .line 589
    move/from16 v0, p55

    iput-boolean v0, p0, Lfoy;->ag:Z

    .line 590
    move-object/from16 v0, p56

    iput-object v0, p0, Lfoy;->X:Landroid/net/Uri;

    .line 591
    if-eqz p56, :cond_4

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, p0, Lfoy;->Y:Z

    .line 592
    move-object/from16 v0, p57

    iput-object v0, p0, Lfoy;->Z:Lfoy;

    .line 593
    move-object/from16 v0, p58

    iput-object v0, p0, Lfoy;->aa:Lfoy;

    .line 594
    invoke-direct {p0}, Lfoy;->l()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lfoy;->ae:Ljava/lang/String;

    .line 595
    invoke-direct {p0}, Lfoy;->k()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lfoy;->ac:Ljava/lang/String;

    .line 596
    invoke-direct {p0}, Lfoy;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lfoy;->ad:Ljava/lang/String;

    .line 598
    invoke-static/range {p59 .. p59}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->ah:Ljava/util/List;

    .line 599
    invoke-static/range {p60 .. p60}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->ai:Ljava/util/List;

    .line 600
    move-object/from16 v0, p61

    iput-object v0, p0, Lfoy;->aj:Lfoo;

    .line 601
    invoke-static/range {p62 .. p62}, La;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfoy;->ak:Ljava/util/List;

    .line 602
    return-void

    .line 538
    :cond_0
    if-eqz p58, :cond_1

    .line 539
    move-object/from16 v0, p58

    iget-object p4, v0, Lfoy;->e:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    const/4 p4, 0x0

    goto/16 :goto_0

    .line 540
    :cond_2
    if-eqz p58, :cond_3

    .line 541
    move-object/from16 v0, p58

    iget-object p5, v0, Lfoy;->f:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    const/4 p5, 0x0

    goto/16 :goto_1

    .line 591
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method static synthetic a(Lfoy;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lfoy;->am:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    .prologue
    .line 2039
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2040
    sget-object v1, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2041
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 948
    if-eqz p0, :cond_1

    .line 949
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http"

    invoke-static {v0, v1}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-static {v0, v1}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 950
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "www.youtube"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lfoy;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lfoy;->an:Landroid/net/Uri;

    return-object v0
.end method

.method private static b(Landroid/os/Parcel;)Ljava/util/List;
    .locals 4

    .prologue
    .line 2058
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2059
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 2060
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2062
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2063
    const-class v3, Lfpe;

    invoke-static {v3, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2065
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lfoy;)Lflp;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lfoy;->q:Lflp;

    return-object v0
.end method

.method static synthetic d(Lfoy;)Lfqy;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lfoy;->r:Lfqy;

    return-object v0
.end method

.method static synthetic e(Lfoy;)Lflr;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lfoy;->s:Lflr;

    return-object v0
.end method

.method static synthetic f(Lfoy;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lfoy;->ag:Z

    return v0
.end method

.method private j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 879
    new-instance v1, Lfah;

    invoke-direct {v1}, Lfah;-><init>()V

    .line 881
    :goto_0
    if-eqz p0, :cond_1

    .line 882
    iget-object v0, p0, Lfoy;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Lfah;->offer(Ljava/lang/Object;)Z

    .line 883
    iget-object p0, p0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 882
    :cond_0
    iget-object v0, p0, Lfoy;->k:Ljava/lang/String;

    goto :goto_1

    .line 885
    :cond_1
    const-string v0, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 889
    new-instance v1, Lfah;

    invoke-direct {v1}, Lfah;-><init>()V

    .line 891
    :goto_0
    if-eqz p0, :cond_1

    .line 892
    iget-object v0, p0, Lfoy;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Lfah;->offer(Ljava/lang/Object;)Z

    .line 893
    iget-object p0, p0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 892
    :cond_0
    iget-object v0, p0, Lfoy;->l:Ljava/lang/String;

    goto :goto_1

    .line 895
    :cond_1
    const-string v0, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 903
    new-instance v0, Lfah;

    invoke-direct {v0}, Lfah;-><init>()V

    .line 906
    :goto_0
    if-eqz p0, :cond_0

    .line 907
    iget-wide v2, p0, Lfoy;->ab:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfah;->offer(Ljava/lang/Object;)Z

    .line 908
    iget-object p0, p0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 910
    :cond_0
    const-string v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lgia;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lfoy;->i()Lfpd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lezj;)Z
    .locals 4

    .prologue
    .line 846
    invoke-virtual {p1}, Lezj;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lfoy;->R:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lfpc;
    .locals 4

    .prologue
    .line 675
    new-instance v0, Lfpc;

    invoke-direct {v0}, Lfpc;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    .line 676
    iget-object v2, p0, Lfoy;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lfpc;->b:Ljava/util/List;

    .line 677
    iget-object v1, p0, Lfoy;->c:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->i:Ljava/lang/String;

    .line 678
    iget-object v1, p0, Lfoy;->d:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->c:Ljava/lang/String;

    .line 679
    iget-object v1, p0, Lfoy;->e:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->d:Ljava/lang/String;

    .line 680
    iget-object v1, p0, Lfoy;->f:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->e:Ljava/lang/String;

    iget-object v1, p0, Lfoy;->g:[B

    .line 681
    iput-object v1, v0, Lfpc;->f:[B

    .line 682
    iget-object v1, p0, Lfoy;->h:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->g:Ljava/lang/String;

    .line 683
    iget-object v1, p0, Lfoy;->i:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->h:Ljava/lang/String;

    .line 684
    iget-object v1, p0, Lfoy;->j:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->j:Ljava/lang/String;

    iget-object v1, p0, Lfoy;->am:Ljava/lang/String;

    .line 685
    iput-object v1, v0, Lfpc;->k:Ljava/lang/String;

    iget-object v1, p0, Lfoy;->an:Landroid/net/Uri;

    .line 686
    iput-object v1, v0, Lfpc;->l:Landroid/net/Uri;

    .line 687
    iget-object v1, p0, Lfoy;->k:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->m:Ljava/lang/String;

    .line 688
    iget-object v1, p0, Lfoy;->l:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->n:Ljava/lang/String;

    .line 689
    iget-object v1, p0, Lfoy;->m:Lfpb;

    iput-object v1, v0, Lfpc;->o:Lfpb;

    .line 690
    iget-object v1, p0, Lfoy;->n:Ljava/lang/String;

    iput-object v1, v0, Lfpc;->p:Ljava/lang/String;

    .line 691
    iget v1, p0, Lfoy;->o:I

    iput v1, v0, Lfpc;->q:I

    iget-object v1, p0, Lfoy;->p:Lfrf;

    .line 692
    iput-object v1, v0, Lfpc;->r:Lfrf;

    iget-object v1, p0, Lfoy;->q:Lflp;

    .line 693
    iput-object v1, v0, Lfpc;->s:Lflp;

    iget-object v1, p0, Lfoy;->s:Lflr;

    .line 694
    iput-object v1, v0, Lfpc;->u:Lflr;

    iget-object v1, p0, Lfoy;->r:Lfqy;

    .line 695
    iput-object v1, v0, Lfpc;->t:Lfqy;

    .line 696
    iget-object v1, p0, Lfoy;->t:Landroid/net/Uri;

    iput-object v1, v0, Lfpc;->v:Landroid/net/Uri;

    .line 697
    iget-object v1, p0, Lfoy;->u:Ljava/util/List;

    iput-object v1, v0, Lfpc;->w:Ljava/util/List;

    .line 698
    iget-object v1, p0, Lfoy;->v:Ljava/util/List;

    iput-object v1, v0, Lfpc;->x:Ljava/util/List;

    .line 699
    iget-object v1, p0, Lfoy;->w:Ljava/util/List;

    iput-object v1, v0, Lfpc;->y:Ljava/util/List;

    .line 700
    iget-object v1, p0, Lfoy;->x:Ljava/util/List;

    iput-object v1, v0, Lfpc;->z:Ljava/util/List;

    .line 701
    iget-object v1, p0, Lfoy;->y:Ljava/util/List;

    iput-object v1, v0, Lfpc;->A:Ljava/util/List;

    .line 702
    iget-object v1, p0, Lfoy;->z:Ljava/util/List;

    iput-object v1, v0, Lfpc;->B:Ljava/util/List;

    .line 703
    iget-object v1, p0, Lfoy;->A:Ljava/util/List;

    iput-object v1, v0, Lfpc;->C:Ljava/util/List;

    .line 704
    iget-object v1, p0, Lfoy;->B:Ljava/util/List;

    iput-object v1, v0, Lfpc;->D:Ljava/util/List;

    .line 705
    iget-object v1, p0, Lfoy;->C:Ljava/util/List;

    iput-object v1, v0, Lfpc;->E:Ljava/util/List;

    .line 706
    iget-object v1, p0, Lfoy;->D:Ljava/util/List;

    iput-object v1, v0, Lfpc;->F:Ljava/util/List;

    .line 707
    iget-object v1, p0, Lfoy;->E:Ljava/util/List;

    iput-object v1, v0, Lfpc;->G:Ljava/util/List;

    .line 708
    iget-object v1, p0, Lfoy;->F:Ljava/util/List;

    iput-object v1, v0, Lfpc;->H:Ljava/util/List;

    .line 709
    iget-object v1, p0, Lfoy;->G:Ljava/util/List;

    iput-object v1, v0, Lfpc;->I:Ljava/util/List;

    .line 710
    iget-object v1, p0, Lfoy;->H:Ljava/util/List;

    iput-object v1, v0, Lfpc;->J:Ljava/util/List;

    .line 711
    iget-object v1, p0, Lfoy;->I:Ljava/util/List;

    iput-object v1, v0, Lfpc;->K:Ljava/util/List;

    .line 712
    iget-object v1, p0, Lfoy;->J:Ljava/util/List;

    iput-object v1, v0, Lfpc;->L:Ljava/util/List;

    .line 713
    iget-object v1, p0, Lfoy;->K:Ljava/util/List;

    iput-object v1, v0, Lfpc;->M:Ljava/util/List;

    .line 714
    iget-object v1, p0, Lfoy;->L:Ljava/util/List;

    iput-object v1, v0, Lfpc;->N:Ljava/util/List;

    .line 715
    iget-object v1, p0, Lfoy;->M:Ljava/util/List;

    iput-object v1, v0, Lfpc;->O:Ljava/util/List;

    .line 716
    iget-object v1, p0, Lfoy;->N:Landroid/net/Uri;

    iput-object v1, v0, Lfpc;->P:Landroid/net/Uri;

    .line 717
    iget-object v1, p0, Lfoy;->O:Landroid/net/Uri;

    iput-object v1, v0, Lfpc;->Q:Landroid/net/Uri;

    .line 718
    iget-boolean v1, p0, Lfoy;->P:Z

    iput-boolean v1, v0, Lfpc;->T:Z

    .line 719
    iget-boolean v1, p0, Lfoy;->Q:Z

    iput-boolean v1, v0, Lfpc;->U:Z

    .line 720
    iget-wide v2, p0, Lfoy;->R:J

    iput-wide v2, v0, Lfpc;->R:J

    .line 721
    iget v1, p0, Lfoy;->S:I

    iput v1, v0, Lfpc;->S:I

    .line 722
    iget-boolean v1, p0, Lfoy;->T:Z

    iput-boolean v1, v0, Lfpc;->V:Z

    .line 723
    iget-boolean v1, p0, Lfoy;->U:Z

    iput-boolean v1, v0, Lfpc;->W:Z

    iget-object v1, p0, Lfoy;->V:Lhqz;

    .line 724
    iput-object v1, v0, Lfpc;->X:Lhqz;

    iget-object v1, p0, Lfoy;->W:Lfkg;

    .line 725
    iput-object v1, v0, Lfpc;->Y:Lfkg;

    .line 726
    iget-wide v2, p0, Lfoy;->ab:J

    iput-wide v2, v0, Lfpc;->Z:J

    .line 727
    iget-boolean v1, p0, Lfoy;->af:Z

    iput-boolean v1, v0, Lfpc;->aa:Z

    .line 728
    iget-boolean v1, p0, Lfoy;->ag:Z

    iput-boolean v1, v0, Lfpc;->ab:Z

    .line 729
    iget-object v1, p0, Lfoy;->X:Landroid/net/Uri;

    iput-object v1, v0, Lfpc;->ac:Landroid/net/Uri;

    .line 730
    iget-object v1, p0, Lfoy;->Z:Lfoy;

    iput-object v1, v0, Lfpc;->ad:Lfoy;

    .line 731
    iget-object v1, p0, Lfoy;->aa:Lfoy;

    iput-object v1, v0, Lfpc;->ae:Lfoy;

    .line 732
    iget-object v1, p0, Lfoy;->ah:Ljava/util/List;

    iput-object v1, v0, Lfpc;->af:Ljava/util/List;

    .line 733
    iget-object v1, p0, Lfoy;->ai:Ljava/util/List;

    iput-object v1, v0, Lfpc;->ah:Ljava/util/List;

    .line 734
    iget-object v1, p0, Lfoy;->aj:Lfoo;

    iput-object v1, v0, Lfpc;->ai:Lfoo;

    .line 735
    iget-object v1, p0, Lfoy;->ak:Ljava/util/List;

    iput-object v1, v0, Lfpc;->ak:Ljava/util/List;

    return-object v0
.end method

.method public final b(Lezj;)Z
    .locals 1

    .prologue
    .line 859
    invoke-virtual {p0}, Lfoy;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lfoy;->a(Lezj;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 785
    iget-boolean v0, p0, Lfoy;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfoy;->am:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 821
    iget-object v0, p0, Lfoy;->p:Lfrf;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfoy;->Y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfoy;->aj:Lfoo;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1954
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Lfoy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2144
    if-nez p1, :cond_1

    .line 2216
    :cond_0
    :goto_0
    return v0

    .line 2147
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2150
    check-cast p1, Lfoy;

    .line 2151
    iget-object v1, p0, Lfoy;->c:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2152
    iget-object v1, p0, Lfoy;->d:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2153
    iget-object v1, p0, Lfoy;->e:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2154
    iget-object v1, p0, Lfoy;->f:Ljava/lang/String;

    .line 2155
    iget-object v2, p1, Lfoy;->f:Ljava/lang/String;

    .line 2154
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2156
    iget-object v1, p0, Lfoy;->g:[B

    iget-object v2, p1, Lfoy;->g:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2157
    iget-object v1, p0, Lfoy;->h:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2158
    iget-object v1, p0, Lfoy;->i:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2159
    iget-object v1, p0, Lfoy;->j:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfoy;->am:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->am:Ljava/lang/String;

    .line 2160
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfoy;->an:Landroid/net/Uri;

    iget-object v2, p1, Lfoy;->an:Landroid/net/Uri;

    .line 2161
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2162
    iget-object v1, p0, Lfoy;->k:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2163
    iget-object v1, p0, Lfoy;->l:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2164
    iget-object v1, p0, Lfoy;->m:Lfpb;

    iget-object v2, p1, Lfoy;->m:Lfpb;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2165
    iget-object v1, p0, Lfoy;->n:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->n:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfoy;->p:Lfrf;

    iget-object v2, p1, Lfoy;->p:Lfrf;

    .line 2166
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfoy;->q:Lflp;

    iget-object v2, p1, Lfoy;->q:Lflp;

    .line 2167
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfoy;->r:Lfqy;

    iget-object v2, p1, Lfoy;->r:Lfqy;

    .line 2168
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2169
    iget-object v1, p0, Lfoy;->t:Landroid/net/Uri;

    iget-object v2, p1, Lfoy;->t:Landroid/net/Uri;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2170
    iget v1, p0, Lfoy;->o:I

    iget v2, p1, Lfoy;->o:I

    if-ne v1, v2, :cond_0

    .line 2171
    iget-boolean v1, p0, Lfoy;->P:Z

    iget-boolean v2, p1, Lfoy;->P:Z

    if-ne v1, v2, :cond_0

    .line 2172
    iget-boolean v1, p0, Lfoy;->Q:Z

    iget-boolean v2, p1, Lfoy;->Q:Z

    if-ne v1, v2, :cond_0

    .line 2173
    iget-wide v2, p0, Lfoy;->R:J

    iget-wide v4, p1, Lfoy;->R:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2174
    iget v1, p0, Lfoy;->S:I

    iget v2, p1, Lfoy;->S:I

    if-ne v1, v2, :cond_0

    .line 2175
    iget-object v1, p0, Lfoy;->b:Ljava/util/List;

    iget-object v2, p1, Lfoy;->b:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2176
    iget-object v1, p0, Lfoy;->u:Ljava/util/List;

    iget-object v2, p1, Lfoy;->u:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2177
    iget-object v1, p0, Lfoy;->v:Ljava/util/List;

    iget-object v2, p1, Lfoy;->v:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2178
    iget-object v1, p0, Lfoy;->w:Ljava/util/List;

    iget-object v2, p1, Lfoy;->w:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2179
    iget-object v1, p0, Lfoy;->x:Ljava/util/List;

    iget-object v2, p1, Lfoy;->x:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2180
    iget-object v1, p0, Lfoy;->y:Ljava/util/List;

    iget-object v2, p1, Lfoy;->y:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2181
    iget-object v1, p0, Lfoy;->z:Ljava/util/List;

    iget-object v2, p1, Lfoy;->z:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2182
    iget-object v1, p0, Lfoy;->A:Ljava/util/List;

    iget-object v2, p1, Lfoy;->A:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2183
    iget-object v1, p0, Lfoy;->B:Ljava/util/List;

    iget-object v2, p1, Lfoy;->B:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2184
    iget-object v1, p0, Lfoy;->C:Ljava/util/List;

    iget-object v2, p1, Lfoy;->C:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2185
    iget-object v1, p0, Lfoy;->D:Ljava/util/List;

    iget-object v2, p1, Lfoy;->D:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2186
    iget-object v1, p0, Lfoy;->E:Ljava/util/List;

    iget-object v2, p1, Lfoy;->E:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2187
    iget-object v1, p0, Lfoy;->F:Ljava/util/List;

    iget-object v2, p1, Lfoy;->F:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2188
    iget-object v1, p0, Lfoy;->G:Ljava/util/List;

    iget-object v2, p1, Lfoy;->G:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2189
    iget-object v1, p0, Lfoy;->H:Ljava/util/List;

    iget-object v2, p1, Lfoy;->H:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2190
    iget-object v1, p0, Lfoy;->I:Ljava/util/List;

    iget-object v2, p1, Lfoy;->I:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2191
    iget-object v1, p0, Lfoy;->J:Ljava/util/List;

    iget-object v2, p1, Lfoy;->J:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2192
    iget-object v1, p0, Lfoy;->K:Ljava/util/List;

    .line 2193
    iget-object v2, p1, Lfoy;->K:Ljava/util/List;

    .line 2192
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2194
    iget-object v1, p0, Lfoy;->L:Ljava/util/List;

    iget-object v2, p1, Lfoy;->L:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2195
    iget-object v1, p0, Lfoy;->M:Ljava/util/List;

    .line 2196
    iget-object v2, p1, Lfoy;->M:Ljava/util/List;

    .line 2195
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2198
    iget-object v1, p0, Lfoy;->N:Landroid/net/Uri;

    iget-object v2, p1, Lfoy;->N:Landroid/net/Uri;

    .line 2197
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2199
    iget-object v1, p0, Lfoy;->O:Landroid/net/Uri;

    .line 2200
    iget-object v2, p1, Lfoy;->O:Landroid/net/Uri;

    .line 2199
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2201
    iget-object v1, p0, Lfoy;->X:Landroid/net/Uri;

    iget-object v2, p1, Lfoy;->X:Landroid/net/Uri;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2202
    iget-object v1, p0, Lfoy;->Z:Lfoy;

    iget-object v2, p1, Lfoy;->Z:Lfoy;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2203
    iget-object v1, p0, Lfoy;->aa:Lfoy;

    iget-object v2, p1, Lfoy;->aa:Lfoy;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2204
    iget-boolean v1, p0, Lfoy;->U:Z

    iget-boolean v2, p1, Lfoy;->U:Z

    if-ne v1, v2, :cond_0

    .line 2205
    iget-boolean v1, p0, Lfoy;->af:Z

    iget-boolean v2, p1, Lfoy;->af:Z

    if-ne v1, v2, :cond_0

    .line 2206
    iget-boolean v1, p0, Lfoy;->ag:Z

    iget-boolean v2, p1, Lfoy;->ag:Z

    if-ne v1, v2, :cond_0

    .line 2207
    iget-boolean v1, p0, Lfoy;->af:Z

    iget-boolean v2, p1, Lfoy;->af:Z

    if-ne v1, v2, :cond_0

    .line 2208
    iget-object v1, p0, Lfoy;->ah:Ljava/util/List;

    .line 2209
    iget-object v2, p1, Lfoy;->ah:Ljava/util/List;

    .line 2208
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2210
    iget-object v1, p0, Lfoy;->ai:Ljava/util/List;

    iget-object v2, p1, Lfoy;->ai:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2211
    iget-object v1, p0, Lfoy;->aj:Lfoo;

    iget-object v2, p1, Lfoy;->aj:Lfoo;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2212
    iget-object v1, p0, Lfoy;->s:Lflr;

    iget-object v2, p1, Lfoy;->s:Lflr;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2213
    iget-object v1, p0, Lfoy;->ak:Ljava/util/List;

    iget-object v2, p1, Lfoy;->ak:Ljava/util/List;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2214
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 842
    iget-object v0, p0, Lfoy;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Landroid/net/Uri;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 868
    iget-object v1, p0, Lfoy;->p:Lfrf;

    if-nez v1, :cond_1

    .line 875
    :cond_0
    :goto_0
    return-object v0

    .line 871
    :cond_1
    iget-object v1, p0, Lfoy;->p:Lfrf;

    iget-object v1, v1, Lfrf;->a:Ljava/util/List;

    .line 872
    if-eqz v1, :cond_0

    .line 875
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v0, v0, Lfqj;->d:Landroid/net/Uri;

    goto :goto_0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 918
    const/4 v1, 0x0

    .line 919
    iget-object v0, p0, Lfoy;->aa:Lfoy;

    .line 920
    :goto_0
    if-eqz v0, :cond_0

    .line 921
    add-int/lit8 v1, v1, 0x1

    .line 922
    iget-object v0, v0, Lfoy;->aa:Lfoy;

    goto :goto_0

    .line 924
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2223
    invoke-static {v0}, Lb;->c(Z)V

    .line 2224
    return v0
.end method

.method public final i()Lfpd;
    .locals 1

    .prologue
    .line 2229
    new-instance v0, Lfpd;

    invoke-direct {v0, p0}, Lfpd;-><init>(Lfoy;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 740
    iget-boolean v0, p0, Lfoy;->Y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfoy;->X:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "VastAd Wrapper: [wrapperUri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 742
    :goto_0
    return-object v0

    .line 741
    :cond_0
    iget-object v0, p0, Lfoy;->k:Ljava/lang/String;

    iget-object v1, p0, Lfoy;->c:Ljava/lang/String;

    .line 742
    iget-object v2, p0, Lfoy;->j:Ljava/lang/String;

    iget-object v3, p0, Lfoy;->l:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x3d

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "VastAd: [vastAdId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", adVideoId="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",videoTitle= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", vastAdSystem = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1976
    iget-object v0, p0, Lfoy;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1977
    iget-object v0, p0, Lfoy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1978
    iget-object v0, p0, Lfoy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1979
    iget-object v0, p0, Lfoy;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1980
    iget-object v0, p0, Lfoy;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1981
    iget-object v0, p0, Lfoy;->g:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 1982
    iget-object v0, p0, Lfoy;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1983
    iget-object v0, p0, Lfoy;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1984
    iget-object v0, p0, Lfoy;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1985
    iget-object v0, p0, Lfoy;->am:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1986
    iget-object v0, p0, Lfoy;->an:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1987
    iget-object v0, p0, Lfoy;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1988
    iget-object v0, p0, Lfoy;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1989
    iget-object v0, p0, Lfoy;->m:Lfpb;

    invoke-virtual {v0}, Lfpb;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1990
    iget-object v0, p0, Lfoy;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1991
    iget v0, p0, Lfoy;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1992
    iget-object v0, p0, Lfoy;->p:Lfrf;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1993
    iget-object v0, p0, Lfoy;->q:Lflp;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1994
    iget-object v0, p0, Lfoy;->r:Lfqy;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1995
    iget-object v0, p0, Lfoy;->s:Lflr;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1996
    iget-object v0, p0, Lfoy;->t:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1997
    iget-object v0, p0, Lfoy;->u:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1998
    iget-object v0, p0, Lfoy;->v:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1999
    iget-object v0, p0, Lfoy;->w:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2000
    iget-object v0, p0, Lfoy;->x:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2001
    iget-object v0, p0, Lfoy;->y:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2002
    iget-object v0, p0, Lfoy;->z:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2003
    iget-object v0, p0, Lfoy;->A:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2004
    iget-object v0, p0, Lfoy;->B:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2005
    iget-object v0, p0, Lfoy;->C:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2006
    iget-object v0, p0, Lfoy;->D:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2007
    iget-object v0, p0, Lfoy;->E:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2008
    iget-object v0, p0, Lfoy;->F:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2009
    iget-object v0, p0, Lfoy;->G:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2010
    iget-object v0, p0, Lfoy;->H:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2011
    iget-object v0, p0, Lfoy;->I:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2012
    iget-object v0, p0, Lfoy;->J:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2013
    iget-object v0, p0, Lfoy;->K:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2014
    iget-object v0, p0, Lfoy;->L:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2015
    iget-object v0, p0, Lfoy;->M:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2016
    iget-object v0, p0, Lfoy;->N:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2017
    iget-object v0, p0, Lfoy;->O:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2018
    iget-boolean v0, p0, Lfoy;->P:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2019
    iget-boolean v0, p0, Lfoy;->Q:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2020
    iget-wide v4, p0, Lfoy;->R:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 2021
    iget v0, p0, Lfoy;->S:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2022
    iget-boolean v0, p0, Lfoy;->T:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2023
    iget-boolean v0, p0, Lfoy;->U:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2024
    iget-object v0, p0, Lfoy;->V:Lhqz;

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lidh;)V

    .line 2025
    iget-object v0, p0, Lfoy;->W:Lfkg;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2026
    iget-wide v4, p0, Lfoy;->ab:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 2027
    iget-boolean v0, p0, Lfoy;->af:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2028
    iget-boolean v0, p0, Lfoy;->ag:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2029
    iget-object v0, p0, Lfoy;->X:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2030
    iget-object v0, p0, Lfoy;->Z:Lfoy;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2031
    iget-object v0, p0, Lfoy;->aa:Lfoy;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2032
    iget-object v0, p0, Lfoy;->ah:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpe;

    invoke-virtual {v0}, Lfpe;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_0
    move v0, v2

    .line 2018
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2019
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2022
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2023
    goto :goto_3

    :cond_4
    move v0, v2

    .line 2027
    goto :goto_4

    :cond_5
    move v1, v2

    .line 2028
    goto :goto_5

    .line 2032
    :cond_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2033
    iget-object v0, p0, Lfoy;->ai:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2034
    iget-object v0, p0, Lfoy;->aj:Lfoo;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2035
    iget-object v0, p0, Lfoy;->ak:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2036
    return-void
.end method
