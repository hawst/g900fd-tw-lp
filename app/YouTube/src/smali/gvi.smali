.class public final Lgvi;
.super Lidh;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lgvj;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field private o:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lidh;-><init>()V

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lgvi;->a:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lgvi;->b:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lgvi;->c:Lgvj;

    .line 30
    iput v1, p0, Lgvi;->d:I

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lgvi;->e:Ljava/lang/String;

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lgvi;->f:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lgvi;->g:Ljava/lang/String;

    .line 42
    iput-boolean v1, p0, Lgvi;->h:Z

    .line 45
    iput-boolean v1, p0, Lgvi;->i:Z

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lgvi;->j:Ljava/lang/String;

    .line 51
    iput-boolean v1, p0, Lgvi;->k:Z

    .line 54
    iput-boolean v1, p0, Lgvi;->l:Z

    .line 57
    iput-boolean v1, p0, Lgvi;->o:Z

    .line 60
    iput-boolean v1, p0, Lgvi;->m:Z

    .line 63
    iput-boolean v1, p0, Lgvi;->n:Z

    .line 10
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    iget-object v1, p0, Lgvi;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 153
    const/4 v0, 0x1

    iget-object v1, p0, Lgvi;->a:Ljava/lang/String;

    .line 154
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 156
    :cond_0
    iget-object v1, p0, Lgvi;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 157
    const/4 v1, 0x2

    iget-object v2, p0, Lgvi;->b:Ljava/lang/String;

    .line 158
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    :cond_1
    iget-object v1, p0, Lgvi;->c:Lgvj;

    if-eqz v1, :cond_2

    .line 161
    const/4 v1, 0x3

    iget-object v2, p0, Lgvi;->c:Lgvj;

    .line 162
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_2
    iget v1, p0, Lgvi;->d:I

    if-eqz v1, :cond_3

    .line 165
    const/4 v1, 0x4

    iget v2, p0, Lgvi;->d:I

    .line 166
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_3
    iget-object v1, p0, Lgvi;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 169
    const/4 v1, 0x5

    iget-object v2, p0, Lgvi;->e:Ljava/lang/String;

    .line 170
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_4
    iget-object v1, p0, Lgvi;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 173
    const/4 v1, 0x7

    iget-object v2, p0, Lgvi;->f:Ljava/lang/String;

    .line 174
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_5
    iget-object v1, p0, Lgvi;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 177
    const/16 v1, 0x8

    iget-object v2, p0, Lgvi;->g:Ljava/lang/String;

    .line 178
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_6
    iget-boolean v1, p0, Lgvi;->h:Z

    if-eqz v1, :cond_7

    .line 181
    const/16 v1, 0x9

    iget-boolean v2, p0, Lgvi;->h:Z

    .line 182
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 184
    :cond_7
    iget-boolean v1, p0, Lgvi;->i:Z

    if-eqz v1, :cond_8

    .line 185
    const/16 v1, 0xa

    iget-boolean v2, p0, Lgvi;->i:Z

    .line 186
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 188
    :cond_8
    iget-object v1, p0, Lgvi;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 189
    const/16 v1, 0xb

    iget-object v2, p0, Lgvi;->j:Ljava/lang/String;

    .line 190
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_9
    iget-boolean v1, p0, Lgvi;->k:Z

    if-eqz v1, :cond_a

    .line 193
    const/16 v1, 0xc

    iget-boolean v2, p0, Lgvi;->k:Z

    .line 194
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 196
    :cond_a
    iget-boolean v1, p0, Lgvi;->l:Z

    if-eqz v1, :cond_b

    .line 197
    const/16 v1, 0xd

    iget-boolean v2, p0, Lgvi;->l:Z

    .line 198
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 200
    :cond_b
    iget-boolean v1, p0, Lgvi;->o:Z

    if-eqz v1, :cond_c

    .line 201
    const/16 v1, 0xe

    iget-boolean v2, p0, Lgvi;->o:Z

    .line 202
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 204
    :cond_c
    iget-boolean v1, p0, Lgvi;->m:Z

    if-eqz v1, :cond_d

    .line 205
    const/16 v1, 0xf

    iget-boolean v2, p0, Lgvi;->m:Z

    .line 206
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 208
    :cond_d
    iget-boolean v1, p0, Lgvi;->n:Z

    if-eqz v1, :cond_e

    .line 209
    const/16 v1, 0x10

    iget-boolean v2, p0, Lgvi;->n:Z

    .line 210
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 212
    :cond_e
    iput v0, p0, Lgvi;->J:I

    .line 213
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lidj;->a(Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvi;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvi;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgvi;->c:Lgvj;

    if-nez v0, :cond_1

    new-instance v0, Lgvj;

    invoke-direct {v0}, Lgvj;-><init>()V

    iput-object v0, p0, Lgvi;->c:Lgvj;

    :cond_1
    iget-object v0, p0, Lgvi;->c:Lgvj;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->i()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lgvi;->d:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lgvi;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvi;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvi;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvi;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgvi;->h:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgvi;->i:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvi;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgvi;->k:Z

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgvi;->l:Z

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgvi;->o:Z

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgvi;->m:Z

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgvi;->n:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x5a -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lgvi;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    const/4 v0, 0x1

    iget-object v1, p0, Lgvi;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 105
    :cond_0
    iget-object v0, p0, Lgvi;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    const/4 v0, 0x2

    iget-object v1, p0, Lgvi;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 108
    :cond_1
    iget-object v0, p0, Lgvi;->c:Lgvj;

    if-eqz v0, :cond_2

    .line 109
    const/4 v0, 0x3

    iget-object v1, p0, Lgvi;->c:Lgvj;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 111
    :cond_2
    iget v0, p0, Lgvi;->d:I

    if-eqz v0, :cond_3

    .line 112
    const/4 v0, 0x4

    iget v1, p0, Lgvi;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 114
    :cond_3
    iget-object v0, p0, Lgvi;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 115
    const/4 v0, 0x5

    iget-object v1, p0, Lgvi;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 117
    :cond_4
    iget-object v0, p0, Lgvi;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 118
    const/4 v0, 0x7

    iget-object v1, p0, Lgvi;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 120
    :cond_5
    iget-object v0, p0, Lgvi;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 121
    const/16 v0, 0x8

    iget-object v1, p0, Lgvi;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 123
    :cond_6
    iget-boolean v0, p0, Lgvi;->h:Z

    if-eqz v0, :cond_7

    .line 124
    const/16 v0, 0x9

    iget-boolean v1, p0, Lgvi;->h:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 126
    :cond_7
    iget-boolean v0, p0, Lgvi;->i:Z

    if-eqz v0, :cond_8

    .line 127
    const/16 v0, 0xa

    iget-boolean v1, p0, Lgvi;->i:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 129
    :cond_8
    iget-object v0, p0, Lgvi;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 130
    const/16 v0, 0xb

    iget-object v1, p0, Lgvi;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 132
    :cond_9
    iget-boolean v0, p0, Lgvi;->k:Z

    if-eqz v0, :cond_a

    .line 133
    const/16 v0, 0xc

    iget-boolean v1, p0, Lgvi;->k:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 135
    :cond_a
    iget-boolean v0, p0, Lgvi;->l:Z

    if-eqz v0, :cond_b

    .line 136
    const/16 v0, 0xd

    iget-boolean v1, p0, Lgvi;->l:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 138
    :cond_b
    iget-boolean v0, p0, Lgvi;->o:Z

    if-eqz v0, :cond_c

    .line 139
    const/16 v0, 0xe

    iget-boolean v1, p0, Lgvi;->o:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 141
    :cond_c
    iget-boolean v0, p0, Lgvi;->m:Z

    if-eqz v0, :cond_d

    .line 142
    const/16 v0, 0xf

    iget-boolean v1, p0, Lgvi;->m:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 144
    :cond_d
    iget-boolean v0, p0, Lgvi;->n:Z

    if-eqz v0, :cond_e

    .line 145
    const/16 v0, 0x10

    iget-boolean v1, p0, Lgvi;->n:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 147
    :cond_e
    return-void
.end method

.method public final b()Lgvi;
    .locals 2

    .prologue
    .line 89
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgvi;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    iget-object v1, p0, Lgvi;->c:Lgvj;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lgvi;->c:Lgvj;

    invoke-virtual {v1}, Lgvj;->b()Lgvj;

    move-result-object v1

    iput-object v1, v0, Lgvi;->c:Lgvj;

    .line 96
    :cond_0
    return-object v0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0}, Lgvi;->b()Lgvi;

    move-result-object v0

    return-object v0
.end method
