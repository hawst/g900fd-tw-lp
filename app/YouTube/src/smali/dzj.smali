.class final Ldzj;
.super Ljava/lang/Thread;
.source "SourceFile"

# interfaces
.implements Ldzn;


# static fields
.field private static synthetic d:Z


# instance fields
.field private a:Ldza;

.field private b:Ljava/lang/RuntimeException;

.field private final c:Ldzl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 263
    const-class v0, Ldzh;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ldzj;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZLdzl;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 292
    invoke-direct {p0, p3}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 271
    iput-object v0, p0, Ldzj;->a:Ldza;

    .line 278
    iput-object v0, p0, Ldzj;->b:Ljava/lang/RuntimeException;

    .line 288
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 293
    iput-object p2, p0, Ldzj;->c:Ldzl;

    .line 294
    if-eqz p1, :cond_0

    .line 295
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldzj;->setDaemon(Z)V

    .line 297
    :cond_0
    new-instance v0, Ldzk;

    invoke-direct {v0, p0}, Ldzk;-><init>(Ldzj;)V

    invoke-virtual {p0, v0}, Ldzj;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 308
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Ldza;
    .locals 1

    .prologue
    .line 331
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Ldzj;->a:Ldza;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldzj;->b:Ljava/lang/RuntimeException;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 333
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 341
    :catch_0
    move-exception v0

    goto :goto_0

    .line 343
    :cond_0
    :try_start_2
    iget-object v0, p0, Ldzj;->b:Ljava/lang/RuntimeException;

    if-eqz v0, :cond_1

    .line 344
    iget-object v0, p0, Ldzj;->b:Ljava/lang/RuntimeException;

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 331
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 346
    :cond_1
    :try_start_3
    sget-boolean v0, Ldzj;->d:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Ldzj;->a:Ldza;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 347
    :cond_2
    iget-object v0, p0, Ldzj;->a:Ldza;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 354
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 355
    return-void
.end method

.method public final run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 364
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 366
    :try_start_1
    new-instance v0, Ldza;

    invoke-direct {v0}, Ldza;-><init>()V

    iput-object v0, p0, Ldzj;->a:Ldza;

    .line 367
    invoke-static {}, Ldzh;->c()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-lez v0, :cond_4

    .line 368
    new-instance v1, Ldzm;

    iget-object v0, p0, Ldzj;->a:Ldza;

    invoke-direct {v1, v0}, Ldzm;-><init>(Ldyo;)V

    .line 369
    invoke-static {}, Ldzh;->c()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-gtz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x49

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid period for PeriodicAlarm (must be positive): "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 372
    :catch_0
    move-exception v0

    .line 373
    :try_start_2
    iput-object v0, p0, Ldzj;->b:Ljava/lang/RuntimeException;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 376
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 382
    invoke-static {}, Ldzh;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 384
    :try_start_4
    iget-object v0, p0, Ldzj;->a:Ldza;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Ldzj;->a:Ldza;

    iget-object v0, v0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    :try_start_5
    monitor-enter v1
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    iget-wide v4, v1, Ldzm;->b:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "switchOn() is called when a PeriodicAlarm is already on"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 376
    :catchall_1
    move-exception v0

    :try_start_8
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    throw v0

    .line 378
    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 382
    :catchall_3
    move-exception v0

    invoke-static {}, Ldzh;->d()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 384
    :try_start_a
    iget-object v1, p0, Ldzj;->a:Ldza;

    if-eqz v1, :cond_2

    .line 385
    iget-object v1, p0, Ldzj;->a:Ldza;

    iget-object v1, v1, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    .line 388
    :cond_2
    :goto_1
    throw v0

    .line 369
    :cond_3
    :try_start_b
    iput-wide v2, v1, Ldzm;->b:J

    iput-object p0, v1, Ldzm;->c:Ldzn;

    iget-object v0, v1, Ldzm;->a:Ldzp;

    iget-wide v2, v1, Ldzm;->b:J

    invoke-virtual {v0, v2, v3, v1}, Ldzp;->a(JLdzq;)V

    monitor-exit v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 370
    :try_start_c
    invoke-static {}, Ldzh;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 376
    :cond_4
    :try_start_d
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 378
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 380
    :try_start_e
    iget-object v0, p0, Ldzj;->c:Ldzl;

    iget-object v1, p0, Ldzj;->a:Ldza;

    invoke-virtual {v0, v1}, Ldzl;->a(Ldza;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 382
    invoke-static {}, Ldzh;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 384
    :try_start_f
    iget-object v0, p0, Ldzj;->a:Ldza;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Ldzj;->a:Ldza;

    iget-object v0, v0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_1

    goto :goto_0

    .line 389
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_1

    .line 374
    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 320
    invoke-virtual {p0}, Ldzj;->getId()J

    move-result-wide v0

    invoke-virtual {p0}, Ldzj;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Thread["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
