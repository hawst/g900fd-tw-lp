.class final Laxz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field private synthetic a:Laxx;


# direct methods
.method constructor <init>(Laxx;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Laxz;->a:Laxx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 3

    .prologue
    .line 153
    const-string v1, "Browse request failed: "

    invoke-virtual {p1}, Lxa;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 154
    return-void

    .line 153
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 90
    check-cast p1, Lfis;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {p1}, Lfis;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmz;

    iget-object v5, v0, Lfmz;->a:Lhxa;

    iget-object v5, v5, Lhxa;->a:Lhog;

    iget-object v5, v5, Lhog;->c:Lhbm;

    iget-object v5, v5, Lhbm;->a:Ljava/lang/String;

    const-string v6, "FEmusic"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lfmz;->c()Lfmi;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lfmi;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, Lfmr;

    if-eqz v4, :cond_1

    check-cast v0, Lfmr;

    invoke-virtual {v0}, Lfmr;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v5, v0, Lfje;

    if-eqz v5, :cond_3

    check-cast v0, Lfje;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    instance-of v5, v0, Lfjf;

    if-eqz v5, :cond_2

    check-cast v0, Lfjf;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    move-object v0, v1

    :goto_2
    iget-object v1, p0, Laxz;->a:Laxx;

    iget-object v1, v1, Laxx;->e:Ljava/util/Random;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflb;

    invoke-interface {v0}, Lflb;->b()Lhog;

    move-result-object v0

    new-instance v1, Lgoh;

    invoke-direct {v1, v0}, Lgoh;-><init>(Lhog;)V

    iget-object v0, p0, Laxz;->a:Laxx;

    iget-object v0, v0, Laxx;->b:Lcws;

    invoke-virtual {v0, v1}, Lcws;->a(Lgoh;)V

    iget-object v0, p0, Laxz;->a:Laxx;

    iget-object v0, v0, Laxx;->b:Lcws;

    invoke-virtual {v0}, Lcws;->v()V

    iget-object v0, p0, Laxz;->a:Laxx;

    iget-object v0, v0, Laxx;->b:Lcws;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcws;->a(F)V

    :cond_6
    return-void

    :cond_7
    move-object v0, v2

    goto :goto_2

    :cond_8
    move-object v0, v3

    goto/16 :goto_0
.end method
