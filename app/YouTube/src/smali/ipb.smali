.class final Lipb;
.super Limi;
.source "SourceFile"

# interfaces
.implements Lint;


# instance fields
.field final synthetic a:Lios;

.field private b:Lins;


# direct methods
.method constructor <init>(Lios;)V
    .locals 4

    .prologue
    .line 564
    iput-object p1, p0, Lipb;->a:Lios;

    .line 565
    const-string v0, "OkHttp %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Lios;->a(Lios;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Limi;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 566
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    .line 569
    sget-object v0, Linr;->e:Linr;

    .line 570
    sget-object v2, Linr;->e:Linr;

    .line 572
    :try_start_0
    iget-object v1, p0, Lipb;->a:Lios;

    iget-object v1, v1, Lios;->g:Lipi;

    iget-object v3, p0, Lipb;->a:Lios;

    iget-object v3, v3, Lios;->h:Ljava/net/Socket;

    invoke-static {v3}, Liqa;->b(Ljava/net/Socket;)Liql;

    move-result-object v3

    invoke-static {v3}, Liqa;->a(Liql;)Lipu;

    move-result-object v3

    iget-object v4, p0, Lipb;->a:Lios;

    iget-boolean v4, v4, Lios;->b:Z

    invoke-interface {v1, v3, v4}, Lipi;->a(Lipu;Z)Lins;

    move-result-object v1

    iput-object v1, p0, Lipb;->b:Lins;

    .line 573
    iget-object v1, p0, Lipb;->a:Lios;

    iget-boolean v1, v1, Lios;->b:Z

    if-nez v1, :cond_0

    .line 574
    iget-object v1, p0, Lipb;->b:Lins;

    invoke-interface {v1}, Lins;->a()V

    .line 576
    :cond_0
    iget-object v1, p0, Lipb;->b:Lins;

    invoke-interface {v1, p0}, Lins;->a(Lint;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 578
    sget-object v0, Linr;->a:Linr;

    .line 579
    sget-object v1, Linr;->h:Linr;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585
    :try_start_1
    iget-object v2, p0, Lipb;->a:Lios;

    invoke-static {v2, v0, v1}, Lios;->a(Lios;Linr;Linr;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 588
    :goto_0
    iget-object v0, p0, Lipb;->b:Lins;

    invoke-static {v0}, Limo;->a(Ljava/io/Closeable;)V

    .line 589
    :goto_1
    return-void

    .line 581
    :catch_0
    move-exception v1

    :try_start_2
    sget-object v1, Linr;->b:Linr;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 582
    :try_start_3
    sget-object v0, Linr;->b:Linr;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 585
    :try_start_4
    iget-object v2, p0, Lipb;->a:Lios;

    invoke-static {v2, v1, v0}, Lios;->a(Lios;Linr;Linr;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 588
    :goto_2
    iget-object v0, p0, Lipb;->b:Lins;

    invoke-static {v0}, Limo;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 584
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 585
    :goto_3
    :try_start_5
    iget-object v3, p0, Lipb;->a:Lios;

    invoke-static {v3, v1, v2}, Lios;->a(Lios;Linr;Linr;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 588
    :goto_4
    iget-object v1, p0, Lipb;->b:Lins;

    invoke-static {v1}, Limo;->a(Ljava/io/Closeable;)V

    throw v0

    :catch_1
    move-exception v1

    goto :goto_4

    .line 584
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 4

    .prologue
    .line 755
    if-nez p1, :cond_1

    .line 756
    iget-object v1, p0, Lipb;->a:Lios;

    monitor-enter v1

    .line 757
    :try_start_0
    iget-object v0, p0, Lipb;->a:Lios;

    iget-wide v2, v0, Lios;->d:J

    add-long/2addr v2, p2

    iput-wide v2, v0, Lios;->d:J

    .line 758
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 759
    monitor-exit v1

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 759
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 761
    :cond_1
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-virtual {v0, p1}, Lios;->a(I)Lipe;

    move-result-object v1

    .line 762
    if-eqz v1, :cond_0

    .line 763
    monitor-enter v1

    .line 764
    :try_start_1
    invoke-virtual {v1, p2, p3}, Lipe;->a(J)V

    .line 765
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method public final a(ILinr;)V
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0, p1}, Lios;->a(Lios;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 668
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0, p1, p2}, Lios;->a(Lios;ILinr;)V

    .line 675
    :cond_0
    :goto_0
    return-void

    .line 671
    :cond_1
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-virtual {v0, p1}, Lios;->b(I)Lipe;

    move-result-object v0

    .line 672
    if-eqz v0, :cond_0

    .line 673
    invoke-virtual {v0, p2}, Lipe;->c(Linr;)V

    goto :goto_0
.end method

.method public final a(ILipv;)V
    .locals 4

    .prologue
    .line 736
    iget-object v0, p2, Lipv;->b:[B

    array-length v0, v0

    .line 738
    iget-object v2, p0, Lipb;->a:Lios;

    monitor-enter v2

    .line 739
    :try_start_0
    iget-object v0, p0, Lipb;->a:Lios;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lios;->b(Lios;Z)Z

    .line 742
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0}, Lios;->e(Lios;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 743
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 744
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 745
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 746
    if-le v1, p1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lipe;

    invoke-virtual {v1}, Lipe;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 747
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lipe;

    sget-object v1, Linr;->g:Linr;

    invoke-virtual {v0, v1}, Lipe;->c(Linr;)V

    .line 748
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 751
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(ILjava/util/List;)V
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0, p1, p2}, Lios;->a(Lios;ILjava/util/List;)V

    .line 778
    return-void
.end method

.method public final a(ZII)V
    .locals 3

    .prologue
    .line 724
    if-eqz p1, :cond_1

    .line 725
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0, p2}, Lios;->c(Lios;I)Lcw;

    move-result-object v0

    .line 726
    if-eqz v0, :cond_0

    .line 727
    invoke-virtual {v0}, Lcw;->t()V

    .line 733
    :cond_0
    :goto_0
    return-void

    .line 731
    :cond_1
    iget-object v0, p0, Lipb;->a:Lios;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, p2, p3, v2}, Lios;->b(Lios;ZIILcw;)V

    goto :goto_0
.end method

.method public final a(ZILipu;I)V
    .locals 4

    .prologue
    .line 594
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0, p2}, Lios;->a(Lios;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 595
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0, p2, p3, p4, p1}, Lios;->a(Lios;ILipu;IZ)V

    .line 608
    :cond_0
    :goto_0
    return-void

    .line 598
    :cond_1
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-virtual {v0, p2}, Lios;->a(I)Lipe;

    move-result-object v0

    .line 599
    if-nez v0, :cond_2

    .line 600
    iget-object v0, p0, Lipb;->a:Lios;

    sget-object v1, Linr;->c:Linr;

    invoke-virtual {v0, p2, v1}, Lios;->a(ILinr;)V

    .line 601
    int-to-long v0, p4

    invoke-interface {p3, v0, v1}, Lipu;->f(J)V

    goto :goto_0

    .line 604
    :cond_2
    sget-boolean v1, Lipe;->i:Z

    if-nez v1, :cond_3

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    iget-object v1, v0, Lipe;->f:Lipg;

    int-to-long v2, p4

    invoke-virtual {v1, p3, v2, v3}, Lipg;->a(Lipu;J)V

    .line 605
    if-eqz p1, :cond_0

    .line 606
    invoke-virtual {v0}, Lipe;->e()V

    goto :goto_0
.end method

.method public final a(ZLioo;)V
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 678
    .line 679
    const/4 v0, 0x0

    .line 680
    iget-object v6, p0, Lipb;->a:Lios;

    monitor-enter v6

    .line 681
    :try_start_0
    iget-object v2, p0, Lipb;->a:Lios;

    iget-object v2, v2, Lios;->f:Lioo;

    const/high16 v3, 0x10000

    invoke-virtual {v2, v3}, Lioo;->c(I)I

    move-result v2

    .line 682
    if-eqz p1, :cond_0

    iget-object v3, p0, Lipb;->a:Lios;

    iget-object v3, v3, Lios;->f:Lioo;

    const/4 v7, 0x0

    iput v7, v3, Lioo;->c:I

    iput v7, v3, Lioo;->b:I

    iput v7, v3, Lioo;->a:I

    iget-object v3, v3, Lioo;->d:[I

    const/4 v7, 0x0

    invoke-static {v3, v7}, Ljava/util/Arrays;->fill([II)V

    .line 683
    :cond_0
    iget-object v3, p0, Lipb;->a:Lios;

    iget-object v3, v3, Lios;->f:Lioo;

    :goto_0
    const/16 v7, 0xa

    if-ge v1, v7, :cond_2

    invoke-virtual {p2, v1}, Lioo;->a(I)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p2, v1}, Lioo;->b(I)I

    move-result v7

    iget-object v8, p2, Lioo;->d:[I

    aget v8, v8, v1

    invoke-virtual {v3, v1, v7, v8}, Lioo;->a(III)Lioo;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 684
    :cond_2
    iget-object v1, p0, Lipb;->a:Lios;

    iget-object v1, v1, Lios;->a:Lima;

    sget-object v3, Lima;->d:Lima;

    if-ne v1, v3, :cond_3

    .line 685
    invoke-static {}, Lios;->d()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v3, Lipd;

    const-string v7, "OkHttp %s ACK Settings"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lipb;->a:Lios;

    invoke-static {v10}, Lios;->a(Lios;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-direct {v3, p0, v7, v8, p2}, Lipd;-><init>(Lipb;Ljava/lang/String;[Ljava/lang/Object;Lioo;)V

    invoke-interface {v1, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 687
    :cond_3
    iget-object v1, p0, Lipb;->a:Lios;

    iget-object v1, v1, Lios;->f:Lioo;

    const/high16 v3, 0x10000

    invoke-virtual {v1, v3}, Lioo;->c(I)I

    move-result v1

    .line 688
    const/4 v3, -0x1

    if-eq v1, v3, :cond_8

    if-eq v1, v2, :cond_8

    .line 689
    sub-int/2addr v1, v2

    int-to-long v2, v1

    .line 690
    iget-object v1, p0, Lipb;->a:Lios;

    invoke-static {v1}, Lios;->g(Lios;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 691
    iget-object v1, p0, Lipb;->a:Lios;

    iget-wide v8, v1, Lios;->d:J

    add-long/2addr v8, v2

    iput-wide v8, v1, Lios;->d:J

    cmp-long v7, v2, v4

    if-lez v7, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 692
    :cond_4
    iget-object v1, p0, Lipb;->a:Lios;

    const/4 v7, 0x1

    invoke-static {v1, v7}, Lios;->a(Lios;Z)Z

    .line 694
    :cond_5
    iget-object v1, p0, Lipb;->a:Lios;

    invoke-static {v1}, Lios;->e(Lios;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 695
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0}, Lios;->e(Lios;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lipb;->a:Lios;

    invoke-static {v1}, Lios;->e(Lios;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Lipe;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lipe;

    .line 698
    :cond_6
    :goto_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 699
    if-eqz v0, :cond_7

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7

    .line 700
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0}, Lios;->e(Lios;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lipe;

    .line 701
    monitor-enter v0

    .line 702
    :try_start_1
    invoke-virtual {v0, v2, v3}, Lipe;->a(J)V

    .line 703
    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 698
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 704
    :cond_7
    return-void

    :cond_8
    move-wide v2, v4

    goto :goto_1
.end method

.method public final a(ZZILjava/util/List;Linw;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 612
    iget-object v1, p0, Lipb;->a:Lios;

    invoke-static {v1, p3}, Lios;->a(Lios;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 613
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0, p3, p4, p2}, Lios;->a(Lios;ILjava/util/List;Z)V

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    iget-object v6, p0, Lipb;->a:Lios;

    monitor-enter v6

    .line 619
    :try_start_0
    iget-object v1, p0, Lipb;->a:Lios;

    invoke-static {v1}, Lios;->b(Lios;)Z

    move-result v1

    if-eqz v1, :cond_2

    monitor-exit v6

    goto :goto_0

    .line 652
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 621
    :cond_2
    :try_start_1
    iget-object v1, p0, Lipb;->a:Lios;

    invoke-virtual {v1, p3}, Lios;->a(I)Lipe;

    move-result-object v3

    .line 623
    if-nez v3, :cond_8

    .line 625
    sget-object v1, Linw;->b:Linw;

    if-eq p5, v1, :cond_3

    sget-object v1, Linw;->c:Linw;

    if-ne p5, v1, :cond_4

    :cond_3
    move v2, v0

    :cond_4
    if-eqz v2, :cond_5

    .line 626
    iget-object v0, p0, Lipb;->a:Lios;

    sget-object v1, Linr;->c:Linr;

    invoke-virtual {v0, p3, v1}, Lios;->a(ILinr;)V

    .line 627
    monitor-exit v6

    goto :goto_0

    .line 631
    :cond_5
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-static {v0}, Lios;->c(Lios;)I

    move-result v0

    if-gt p3, v0, :cond_6

    monitor-exit v6

    goto :goto_0

    .line 634
    :cond_6
    rem-int/lit8 v0, p3, 0x2

    iget-object v1, p0, Lipb;->a:Lios;

    invoke-static {v1}, Lios;->d(Lios;)I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-ne v0, v1, :cond_7

    monitor-exit v6

    goto :goto_0

    .line 637
    :cond_7
    new-instance v0, Lipe;

    iget-object v2, p0, Lipb;->a:Lios;

    move v1, p3

    move v3, p1

    move v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lipe;-><init>(ILios;ZZLjava/util/List;)V

    .line 639
    iget-object v1, p0, Lipb;->a:Lios;

    invoke-static {v1, p3}, Lios;->b(Lios;I)I

    .line 640
    iget-object v1, p0, Lipb;->a:Lios;

    invoke-static {v1}, Lios;->e(Lios;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    invoke-static {}, Lios;->d()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lipc;

    const-string v3, "OkHttp %s stream %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, p0, Lipb;->a:Lios;

    invoke-static {v7}, Lios;->a(Lios;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-direct {v2, p0, v3, v4, v0}, Lipc;-><init>(Lipb;Ljava/lang/String;[Ljava/lang/Object;Lipe;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 650
    monitor-exit v6

    goto/16 :goto_0

    .line 652
    :cond_8
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 655
    sget-object v1, Linw;->a:Linw;

    if-ne p5, v1, :cond_9

    move v1, v0

    :goto_1
    if-eqz v1, :cond_a

    .line 656
    sget-object v0, Linr;->b:Linr;

    invoke-virtual {v3, v0}, Lipe;->b(Linr;)V

    .line 657
    iget-object v0, p0, Lipb;->a:Lios;

    invoke-virtual {v0, p3}, Lios;->b(I)Lipe;

    goto/16 :goto_0

    :cond_9
    move v1, v2

    .line 655
    goto :goto_1

    .line 662
    :cond_a
    sget-boolean v1, Lipe;->i:Z

    if-nez v1, :cond_b

    invoke-static {v3}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_b
    const/4 v1, 0x0

    monitor-enter v3

    :try_start_2
    iget-object v4, v3, Lipe;->e:Ljava/util/List;

    if-nez v4, :cond_f

    sget-object v4, Linw;->c:Linw;

    if-ne p5, v4, :cond_c

    move v2, v0

    :cond_c
    if-eqz v2, :cond_e

    sget-object v1, Linr;->b:Linr;

    :goto_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_12

    invoke-virtual {v3, v1}, Lipe;->b(Linr;)V

    .line 663
    :cond_d
    :goto_3
    if-eqz p2, :cond_0

    invoke-virtual {v3}, Lipe;->e()V

    goto/16 :goto_0

    .line 662
    :cond_e
    :try_start_3
    iput-object p4, v3, Lipe;->e:Ljava/util/List;

    invoke-virtual {v3}, Lipe;->a()Z

    move-result v0

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_f
    :try_start_4
    sget-object v4, Linw;->b:Linw;

    if-ne p5, v4, :cond_10

    move v2, v0

    :cond_10
    if-eqz v2, :cond_11

    sget-object v1, Linr;->d:Linr;

    goto :goto_2

    :cond_11
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, v3, Lipe;->e:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v2, p4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object v2, v3, Lipe;->e:Ljava/util/List;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :cond_12
    if-nez v0, :cond_d

    iget-object v0, v3, Lipe;->d:Lios;

    iget v1, v3, Lipe;->c:I

    invoke-virtual {v0, v1}, Lios;->b(I)Lipe;

    goto :goto_3
.end method
