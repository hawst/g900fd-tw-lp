.class public Lftv;
.super Lexx;
.source "SourceFile"


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/SharedPreferences;

.field private final d:Lezj;

.field private final e:Lfhx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lftv;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lftv;->b:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/SharedPreferences;Lezj;Lfhx;Lead;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p4}, Lexx;-><init>(Lead;)V

    .line 43
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lftv;->c:Landroid/content/SharedPreferences;

    .line 44
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lftv;->d:Lezj;

    .line 45
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhx;

    iput-object v0, p0, Lftv;->e:Lfhx;

    .line 46
    return-void
.end method

.method public static synthetic a(JJLandroid/content/SharedPreferences;)J
    .locals 2

    .prologue
    .line 21
    invoke-static {p0, p1, p2, p3, p4}, Lftv;->b(JJLandroid/content/SharedPreferences;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static b(JJLandroid/content/SharedPreferences;)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 73
    const-string v2, "com.google.android.libraries.youtube.innertube.pref.inner_tube_config_last_sync_timestamp"

    invoke-interface {p4, v2, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 77
    cmp-long v4, p0, v2

    if-gez v4, :cond_0

    .line 79
    const-string v2, "Resetting config resync schedule"

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    .line 81
    invoke-interface {p4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "com.google.android.libraries.youtube.innertube.pref.inner_tube_config_last_sync_timestamp"

    .line 82
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 83
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 88
    :goto_0
    return-wide v0

    :cond_0
    add-long v0, v2, p2

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 7

    .prologue
    .line 50
    iget-object v0, p0, Lftv;->d:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iget-object v2, p0, Lftv;->d:Lezj;

    invoke-virtual {v2}, Lezj;->b()J

    move-result-wide v2

    iget-object v4, p0, Lexx;->a:Lead;

    iget-wide v4, v4, Lead;->e:J

    iget-object v6, p0, Lftv;->c:Landroid/content/SharedPreferences;

    invoke-static {v2, v3, v4, v5, v6}, Lftv;->b(JJLandroid/content/SharedPreferences;)J

    move-result-wide v2

    const-wide/16 v4, 0x32

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 52
    :try_start_0
    iget-object v0, p0, Lftv;->e:Lfhx;

    invoke-virtual {v0}, Lfhx;->a()V
    :try_end_0
    .catch Lfdv; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :cond_0
    :goto_1
    return-void

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 54
    :catch_0
    move-exception v0

    const-string v0, "Scheduled config refresh failed"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    goto :goto_1
.end method
