.class public final Laoh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbm;


# instance fields
.field a:Lain;

.field private final b:Laoi;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lain;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lain;

    iput-object v0, p0, Laoh;->a:Lain;

    .line 26
    new-instance v0, Laoi;

    invoke-direct {v0, p1}, Laoi;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Laoh;->b:Laoi;

    .line 28
    :try_start_0
    iget-object v0, p0, Laoh;->b:Laoi;

    invoke-interface {p2, v0}, Lain;->a(Land;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Laoh;->a:Lain;

    if-eqz v0, :cond_0

    .line 47
    :try_start_0
    iget-object v0, p0, Laoh;->a:Lain;

    invoke-interface {v0}, Lain;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Laoh;->a:Lain;

    if-eqz v0, :cond_0

    .line 92
    :try_start_0
    iget-object v0, p0, Laoh;->a:Lain;

    invoke-interface {v0, p1, p2}, Lain;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Laoh;->a:Lain;

    if-eqz v0, :cond_0

    .line 103
    :try_start_0
    iget-object v0, p0, Laoh;->a:Lain;

    invoke-interface {v0, p1}, Lain;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ldbn;)V
    .locals 2

    .prologue
    .line 40
    iget-object v1, p0, Laoh;->b:Laoi;

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbn;

    iput-object v0, v1, Laoi;->a:Ldbn;

    .line 41
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Laoh;->a:Lain;

    if-eqz v0, :cond_0

    .line 70
    :try_start_0
    iget-object v0, p0, Laoh;->a:Lain;

    invoke-interface {v0, p1}, Lain;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Laoh;->a:Lain;

    if-eqz v0, :cond_0

    .line 59
    :try_start_0
    iget-object v0, p0, Laoh;->a:Lain;

    invoke-interface {v0, p1, p2, p3, p4}, Lain;->a(Ljava/lang/String;ZZLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Laoh;->a:Lain;

    if-eqz v0, :cond_0

    .line 81
    :try_start_0
    iget-object v0, p0, Laoh;->a:Lain;

    invoke-interface {v0, p1}, Lain;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
