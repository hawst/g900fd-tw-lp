.class final Ldnf;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private synthetic a:Ldne;


# direct methods
.method constructor <init>(Ldne;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Ldnf;->a:Ldne;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/16 v2, 0x1f

    const/4 v11, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 174
    iget-object v5, p0, Ldnf;->a:Ldne;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v1, v5, Ldne;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, v5, Ldne;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v5, Ldne;->g:I

    iget v2, v5, Ldne;->h:I

    if-ne v0, v2, :cond_1a

    iget-boolean v0, v5, Ldne;->y:Z

    if-nez v0, :cond_1a

    :goto_1
    iput-boolean v3, v5, Ldne;->i:Z

    iget-boolean v0, v5, Ldne;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, v5, Ldne;->c:Ldni;

    iget v2, v5, Ldne;->g:I

    invoke-interface {v0, v2}, Ldni;->a(I)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :cond_2
    return-void

    .line 174
    :pswitch_0
    iget-object v0, v5, Ldne;->j:Ldnc;

    invoke-virtual {v0}, Ldnc;->a()V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v11}, Ldnc;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldnk;

    iget-object v6, v1, Ldnk;->a:Ljava/lang/String;

    iget-object v7, v1, Ldnk;->i:Ljava/lang/String;

    iget-object v8, v1, Ldnk;->c:Lgjn;

    invoke-virtual {v8}, Lgjn;->name()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x30

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Restoring task: (filePath="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "; identityId="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; status="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lezp;->e(Ljava/lang/String;)V

    iget-object v6, v5, Ldne;->k:Ljava/util/Map;

    iget-object v7, v1, Ldnk;->a:Ljava/lang/String;

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, v5, Ldne;->q:Lezz;

    iget v7, v1, Ldnk;->j:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7, v1}, Lezz;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    invoke-virtual {v1}, Ldnk;->b()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, v1, Ldnk;->c:Lgjn;

    sget-object v7, Lgjn;->a:Lgjn;

    if-eq v6, v7, :cond_3

    sget-object v6, Lgjn;->a:Lgjn;

    iput-object v6, v1, Ldnk;->c:Lgjn;

    iput v3, v1, Ldnk;->d:I

    iget-object v6, v5, Ldne;->j:Ldnc;

    invoke-virtual {v6, v1}, Ldnc;->b(Ldnk;)V

    goto/16 :goto_3

    :cond_4
    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v0}, Ldnc;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    goto/16 :goto_2

    :cond_5
    new-instance v2, Ljava/util/HashMap;

    iget-object v1, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    iget-object v1, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldnk;

    iget-object v7, v1, Ldnk;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ldnk;->a()Lgjm;

    move-result-object v1

    invoke-interface {v2, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_6
    iput-object v0, v5, Ldne;->z:Ljava/lang/String;

    iget-object v0, v5, Ldne;->c:Ldni;

    invoke-interface {v0, v2}, Ldni;->a(Ljava/util/Map;)V

    iput-boolean v3, v5, Ldne;->x:Z

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_7

    move v0, v3

    :goto_5
    iget-boolean v1, v5, Ldne;->v:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, v5, Ldne;->v:Z

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :cond_7
    move v0, v4

    goto :goto_5

    :pswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_8

    move v0, v3

    :goto_6
    iget-boolean v1, v5, Ldne;->u:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, v5, Ldne;->u:Z

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :cond_8
    move v0, v4

    goto :goto_6

    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, v5, Ldne;->w:I

    if-eq v1, v0, :cond_0

    iput v0, v5, Ldne;->w:I

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :pswitch_5
    iget-boolean v0, v5, Ldne;->x:Z

    invoke-static {v0}, Lb;->c(Z)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldnk;

    iget-object v1, v5, Ldne;->k:Ljava/util/Map;

    iget-object v2, v0, Ldnk;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v5, Ldne;->k:Ljava/util/Map;

    iget-object v2, v0, Ldnk;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldnk;

    invoke-virtual {v1}, Ldnk;->b()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, v5, Ldne;->j:Ldnc;

    invoke-virtual {v2, v1}, Ldnc;->c(Ldnk;)V

    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v0}, Ldnc;->a(Ldnk;)V

    iget-object v1, v5, Ldne;->k:Ljava/util/Map;

    iget-object v2, v0, Ldnk;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Ldne;->q:Lezz;

    iget v2, v0, Ldnk;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lezz;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    iget-object v1, v5, Ldne;->c:Ldni;

    invoke-virtual {v0}, Ldnk;->a()Lgjm;

    move-result-object v0

    invoke-interface {v1, v0}, Ldni;->d(Lgjm;)V

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :cond_9
    iget-object v1, v5, Ldne;->n:Ljava/util/HashSet;

    iget-object v0, v0, Ldnk;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :cond_a
    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v0}, Ldnc;->a(Ldnk;)V

    iget-object v1, v5, Ldne;->z:Ljava/lang/String;

    iget-object v2, v0, Ldnk;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v5, Ldne;->k:Ljava/util/Map;

    iget-object v2, v0, Ldnk;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Ldne;->q:Lezz;

    iget v2, v0, Ldnk;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lezz;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    iget-object v1, v5, Ldne;->c:Ldni;

    invoke-virtual {v0}, Ldnk;->a()Lgjm;

    move-result-object v0

    invoke-interface {v1, v0}, Ldni;->d(Lgjm;)V

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :pswitch_6
    iget-boolean v0, v5, Ldne;->x:Z

    invoke-static {v0}, Lb;->c(Z)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v1, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v0}, Ldnc;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    iget-object v1, v5, Ldne;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldna;

    if-eqz v1, :cond_c

    invoke-interface {v1, v2}, Ldna;->a(I)V

    :cond_c
    iget-object v1, v5, Ldne;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Ldne;->m:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Ldne;->n:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v1, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnk;

    iget-object v1, v5, Ldne;->q:Lezz;

    invoke-interface {v1, v0}, Lezz;->a(Ljava/lang/Object;)Z

    iget v1, v0, Ldnk;->d:I

    or-int/2addr v1, v2

    iput v1, v0, Ldnk;->d:I

    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v0}, Ldnc;->c(Ldnk;)V

    invoke-virtual {v0}, Ldnk;->a()Lgjm;

    move-result-object v0

    iget-object v1, v5, Ldne;->b:Ldnn;

    invoke-interface {v1, v0}, Ldnn;->a(Lgjm;)Ljava/lang/Runnable;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    :cond_d
    iget-object v1, v5, Ldne;->c:Ldni;

    invoke-interface {v1, v0}, Ldni;->b(Lgjm;)V

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, v5, Ldne;->k:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnk;

    if-eqz v0, :cond_2

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-long v6, v1

    shl-long/2addr v6, v2

    iget v1, p1, Landroid/os/Message;->arg2:I

    int-to-long v8, v1

    add-long/2addr v6, v8

    iput-wide v6, v0, Ldnk;->f:J

    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v0}, Ldnc;->b(Ldnk;)V

    iget-object v1, v5, Ldne;->c:Ldni;

    invoke-virtual {v0}, Ldnk;->a()Lgjm;

    move-result-object v0

    invoke-interface {v1, v0}, Ldni;->e(Lgjm;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, v5, Ldne;->k:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnk;

    if-eqz v0, :cond_2

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-long v6, v1

    shl-long/2addr v6, v2

    iget v1, p1, Landroid/os/Message;->arg2:I

    int-to-long v8, v1

    add-long/2addr v6, v8

    iget-wide v8, v0, Ldnk;->e:J

    cmp-long v1, v8, v6

    if-eqz v1, :cond_e

    iget-object v1, v5, Ldne;->m:Ljava/util/Map;

    iget-object v2, v0, Ldnk;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_e
    iput-wide v6, v0, Ldnk;->e:J

    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v0}, Ldnc;->b(Ldnk;)V

    iget-object v1, v5, Ldne;->c:Ldni;

    invoke-virtual {v0}, Ldnk;->a()Lgjm;

    move-result-object v0

    invoke-interface {v1, v0}, Ldni;->f(Lgjm;)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldnk;

    if-eqz v2, :cond_2

    iget-object v6, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v6, :cond_f

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lgje;

    :goto_7
    iput-object v0, v2, Ldnk;->h:Lgje;

    sget-object v0, Lgjn;->c:Lgjn;

    iput-object v0, v2, Ldnk;->c:Lgjn;

    iget-object v0, v5, Ldne;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v5, Ldne;->m:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v5, Ldne;->n:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-boolean v0, v5, Ldne;->B:Z

    if-eqz v0, :cond_10

    iget-object v0, v5, Ldne;->j:Ldnc;

    invoke-virtual {v0, v2}, Ldnc;->b(Ldnk;)V

    :goto_8
    iget-object v0, v5, Ldne;->c:Ldni;

    invoke-virtual {v2}, Ldnk;->a()Lgjm;

    move-result-object v1

    invoke-interface {v0, v1}, Ldni;->c(Lgjm;)V

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :cond_f
    new-instance v0, Lgje;

    invoke-direct {v0}, Lgje;-><init>()V

    goto :goto_7

    :cond_10
    iget-object v0, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v5, Ldne;->q:Lezz;

    invoke-interface {v0, v2}, Lezz;->a(Ljava/lang/Object;)Z

    iget-object v0, v5, Ldne;->j:Ldnc;

    invoke-virtual {v0, v2}, Ldnc;->c(Ldnk;)V

    goto :goto_8

    :pswitch_a
    iget-object v0, v5, Ldne;->k:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnk;

    if-eqz v0, :cond_2

    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v3, :cond_12

    move v2, v3

    :goto_9
    iget-object v6, v0, Ldnk;->a:Ljava/lang/String;

    iget-object v1, v5, Ldne;->m:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_13

    move v1, v4

    :goto_a
    add-int/lit8 v1, v1, 0x1

    if-nez v2, :cond_11

    iget v2, v5, Ldne;->A:I

    if-le v1, v2, :cond_16

    :cond_11
    const-string v2, "transfer fatal fail "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_14

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_b
    invoke-static {v1}, Lezp;->b(Ljava/lang/String;)V

    sget-object v1, Lgjn;->d:Lgjn;

    iput-object v1, v0, Ldnk;->c:Lgjn;

    iget-boolean v1, v5, Ldne;->B:Z

    if-eqz v1, :cond_15

    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v0}, Ldnc;->b(Ldnk;)V

    :goto_c
    iget-object v1, v5, Ldne;->l:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Ldne;->m:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Ldne;->n:Ljava/util/HashSet;

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v1, v5, Ldne;->c:Ldni;

    invoke-virtual {v0}, Ldnk;->a()Lgjm;

    move-result-object v0

    invoke-interface {v1, v0}, Ldni;->c(Lgjm;)V

    :goto_d
    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :cond_12
    move v2, v4

    goto :goto_9

    :cond_13
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_a

    :cond_14
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_b

    :cond_15
    iget-object v1, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Ldne;->q:Lezz;

    invoke-interface {v1, v0}, Lezz;->a(Ljava/lang/Object;)Z

    iget-object v1, v5, Ldne;->j:Ldnc;

    invoke-virtual {v1, v0}, Ldnc;->c(Ldnk;)V

    goto :goto_c

    :cond_16
    const-string v2, "transfer fail "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_17

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_e
    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    iget-object v0, v5, Ldne;->m:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v5, Ldne;->l:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v5, Ldne;->n:Ljava/util/HashSet;

    invoke-virtual {v0, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    shl-int v0, v3, v1

    mul-int/lit16 v0, v0, 0x3e8

    const v1, 0x927c0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, v5, Ldne;->d:Ljava/util/Random;

    const/16 v2, 0x1388

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0xc

    invoke-virtual {v5, v1, v6, v0}, Ldne;->a(ILjava/lang/Object;I)I

    goto :goto_d

    :cond_17
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_e

    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v5, Ldne;->n:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Ldne;->a()V

    goto/16 :goto_0

    :pswitch_c
    const-string v0, "Pausing all tasks"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    iget-object v0, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnk;

    iget-object v2, v0, Ldnk;->a:Ljava/lang/String;

    iget-object v6, v0, Ldnk;->i:Ljava/lang/String;

    iget-object v7, v0, Ldnk;->c:Lgjn;

    invoke-virtual {v7}, Lgjn;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x2e

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Pausing task: (filePath="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, "; identityId="

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "; status="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    iget-object v2, v0, Ldnk;->c:Lgjn;

    sget-object v6, Lgjn;->b:Lgjn;

    invoke-virtual {v2, v6}, Lgjn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    const/16 v2, 0x80

    invoke-virtual {v5, v0, v2}, Ldne;->a(Ldnk;I)V

    :cond_18
    iget-object v2, v5, Ldne;->j:Ldnc;

    invoke-virtual {v2, v0}, Ldnc;->b(Ldnk;)V

    goto :goto_f

    :cond_19
    iput-object v11, v5, Ldne;->z:Ljava/lang/String;

    iget-object v0, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, v5, Ldne;->q:Lezz;

    invoke-interface {v0}, Lezz;->a()V

    iget-object v0, v5, Ldne;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, v5, Ldne;->n:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, v5, Ldne;->j:Ldnc;

    invoke-virtual {v0}, Ldnc;->a()V

    const-string v0, "Removing all tasks"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    iget-object v0, v5, Ldne;->j:Ldnc;

    iget-object v0, v0, Ldnc;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "transfers"

    invoke-virtual {v0, v1, v11, v11}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, v5, Ldne;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, v5, Ldne;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, v5, Ldne;->q:Lezz;

    invoke-interface {v0}, Lezz;->a()V

    iget-object v0, v5, Ldne;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, v5, Ldne;->n:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto/16 :goto_0

    :cond_1a
    move v3, v4

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
