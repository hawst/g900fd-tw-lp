.class public final Lgzx;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9939
    invoke-direct {p0}, Lidf;-><init>()V

    .line 9942
    iput-boolean v0, p0, Lgzx;->a:Z

    .line 9945
    iput-boolean v0, p0, Lgzx;->b:Z

    .line 9948
    iput-boolean v0, p0, Lgzx;->c:Z

    .line 9939
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 9997
    const/4 v0, 0x0

    .line 9998
    iget-boolean v1, p0, Lgzx;->a:Z

    if-eqz v1, :cond_0

    .line 9999
    const/4 v0, 0x1

    iget-boolean v1, p0, Lgzx;->a:Z

    .line 10000
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 10002
    :cond_0
    iget-boolean v1, p0, Lgzx;->b:Z

    if-eqz v1, :cond_1

    .line 10003
    const/4 v1, 0x2

    iget-boolean v2, p0, Lgzx;->b:Z

    .line 10004
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10006
    :cond_1
    iget-boolean v1, p0, Lgzx;->c:Z

    if-eqz v1, :cond_2

    .line 10007
    const/4 v1, 0x3

    iget-boolean v2, p0, Lgzx;->c:Z

    .line 10008
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10010
    :cond_2
    iget-object v1, p0, Lgzx;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10011
    iput v0, p0, Lgzx;->J:I

    .line 10012
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 9935
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzx;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzx;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzx;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgzx;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgzx;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgzx;->c:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 9982
    iget-boolean v0, p0, Lgzx;->a:Z

    if-eqz v0, :cond_0

    .line 9983
    const/4 v0, 0x1

    iget-boolean v1, p0, Lgzx;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 9985
    :cond_0
    iget-boolean v0, p0, Lgzx;->b:Z

    if-eqz v0, :cond_1

    .line 9986
    const/4 v0, 0x2

    iget-boolean v1, p0, Lgzx;->b:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 9988
    :cond_1
    iget-boolean v0, p0, Lgzx;->c:Z

    if-eqz v0, :cond_2

    .line 9989
    const/4 v0, 0x3

    iget-boolean v1, p0, Lgzx;->c:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 9991
    :cond_2
    iget-object v0, p0, Lgzx;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 9993
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9961
    if-ne p1, p0, :cond_1

    .line 9967
    :cond_0
    :goto_0
    return v0

    .line 9962
    :cond_1
    instance-of v2, p1, Lgzx;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 9963
    :cond_2
    check-cast p1, Lgzx;

    .line 9964
    iget-boolean v2, p0, Lgzx;->a:Z

    iget-boolean v3, p1, Lgzx;->a:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lgzx;->b:Z

    iget-boolean v3, p1, Lgzx;->b:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lgzx;->c:Z

    iget-boolean v3, p1, Lgzx;->c:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzx;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzx;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 9967
    goto :goto_0

    .line 9964
    :cond_4
    iget-object v2, p0, Lgzx;->I:Ljava/util/List;

    iget-object v3, p1, Lgzx;->I:Ljava/util/List;

    .line 9967
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 9971
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 9973
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lgzx;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 9974
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lgzx;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 9975
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lgzx;->c:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 9976
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lgzx;->I:Ljava/util/List;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_3
    add-int/2addr v0, v1

    .line 9977
    return v0

    :cond_0
    move v0, v2

    .line 9973
    goto :goto_0

    :cond_1
    move v0, v2

    .line 9974
    goto :goto_1

    :cond_2
    move v1, v2

    .line 9975
    goto :goto_2

    .line 9976
    :cond_3
    iget-object v0, p0, Lgzx;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_3
.end method
