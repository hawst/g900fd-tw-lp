.class public final Lgzo;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgzo;


# instance fields
.field private b:Lgzn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8985
    const/4 v0, 0x0

    new-array v0, v0, [Lgzo;

    sput-object v0, Lgzo;->a:[Lgzo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8986
    invoke-direct {p0}, Lidf;-><init>()V

    .line 8989
    const/4 v0, 0x0

    iput-object v0, p0, Lgzo;->b:Lgzn;

    .line 8986
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 9026
    const/4 v0, 0x0

    .line 9027
    iget-object v1, p0, Lgzo;->b:Lgzn;

    if-eqz v1, :cond_0

    .line 9028
    const v0, 0x43b1a35

    iget-object v1, p0, Lgzo;->b:Lgzn;

    .line 9029
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9031
    :cond_0
    iget-object v1, p0, Lgzo;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9032
    iput v0, p0, Lgzo;->J:I

    .line 9033
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 8982
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzo;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzo;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzo;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzo;->b:Lgzn;

    if-nez v0, :cond_2

    new-instance v0, Lgzn;

    invoke-direct {v0}, Lgzn;-><init>()V

    iput-object v0, p0, Lgzo;->b:Lgzn;

    :cond_2
    iget-object v0, p0, Lgzo;->b:Lgzn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x21d8d1aa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 9017
    iget-object v0, p0, Lgzo;->b:Lgzn;

    if-eqz v0, :cond_0

    .line 9018
    const v0, 0x43b1a35

    iget-object v1, p0, Lgzo;->b:Lgzn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 9020
    :cond_0
    iget-object v0, p0, Lgzo;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 9022
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9000
    if-ne p1, p0, :cond_1

    .line 9004
    :cond_0
    :goto_0
    return v0

    .line 9001
    :cond_1
    instance-of v2, p1, Lgzo;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 9002
    :cond_2
    check-cast p1, Lgzo;

    .line 9003
    iget-object v2, p0, Lgzo;->b:Lgzn;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzo;->b:Lgzn;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzo;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzo;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 9004
    goto :goto_0

    .line 9003
    :cond_4
    iget-object v2, p0, Lgzo;->b:Lgzn;

    iget-object v3, p1, Lgzo;->b:Lgzn;

    invoke-virtual {v2, v3}, Lgzn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzo;->I:Ljava/util/List;

    iget-object v3, p1, Lgzo;->I:Ljava/util/List;

    .line 9004
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9008
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 9010
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzo;->b:Lgzn;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 9011
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzo;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 9012
    return v0

    .line 9010
    :cond_0
    iget-object v0, p0, Lgzo;->b:Lgzn;

    invoke-virtual {v0}, Lgzn;->hashCode()I

    move-result v0

    goto :goto_0

    .line 9011
    :cond_1
    iget-object v1, p0, Lgzo;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
