.class public final Lboc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field public b:Lbof;

.field public c:Z

.field d:Lfoy;

.field e:Z

.field private final f:Landroid/widget/ListView;

.field private final g:I

.field private final h:Ljava/util/List;

.field private final i:Lfgk;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Levn;Lfgk;Leyp;Lfhz;Lfdw;Lfrz;Landroid/view/ViewStub;Landroid/view/ViewStub;Landroid/view/ViewStub;Landroid/widget/ListView;ILcws;)V
    .locals 9

    .prologue
    .line 149
    const/4 v6, 0x0

    .line 154
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lbnp;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leyp;

    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcws;

    iget-object v3, v3, Lcws;->d:Lcwn;

    invoke-direct {v4, v0, v1, v2, v3}, Lbnp;-><init>(Landroid/app/Activity;Leyp;Landroid/view/ViewStub;Lcwn;)V

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lbnt;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leyp;

    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfhz;

    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    invoke-direct {v4, v0, v1, v2, v3}, Lbnt;-><init>(Landroid/app/Activity;Leyp;Lfhz;Landroid/view/ViewStub;)V

    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbzv;

    move-object v1, p4

    move-object v2, p5

    move-object/from16 v3, p9

    move-object v4, p6

    move-object/from16 v5, p7

    invoke-direct/range {v0 .. v5}, Lbzv;-><init>(Leyp;Lfhz;Landroid/view/ViewStub;Lfdw;Lfrz;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Lbxq;

    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leyp;

    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfhz;

    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    invoke-direct {v4, v0, v1, v2, v3}, Lbxq;-><init>(Levn;Leyp;Lfhz;Landroid/view/ViewStub;)V

    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v1, p3

    move-object/from16 v2, p11

    move v3, v6

    move-object v4, v7

    move-object v5, v8

    .line 149
    invoke-direct/range {v0 .. v5}, Lboc;-><init>(Lfgk;Landroid/widget/ListView;ILjava/util/List;Ljava/util/List;)V

    .line 167
    return-void
.end method

.method private constructor <init>(Lfgk;Landroid/widget/ListView;ILjava/util/List;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lboc;->f:Landroid/widget/ListView;

    .line 229
    iput p3, p0, Lboc;->g:I

    .line 230
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgk;

    iput-object v0, p0, Lboc;->i:Lfgk;

    .line 231
    const/4 v0, 0x1

    iput-boolean v0, p0, Lboc;->c:Z

    .line 233
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lboc;->h:Ljava/util/List;

    .line 234
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lboc;->a:Ljava/util/List;

    .line 235
    return-void
.end method

.method private a(Lfoy;Lfrl;)V
    .locals 1

    .prologue
    .line 282
    invoke-virtual {p0}, Lboc;->a()V

    .line 283
    iput-object p1, p0, Lboc;->d:Lfoy;

    .line 284
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lboc;->a(Lfrl;I)V

    .line 285
    return-void
.end method

.method private a(Lfrl;)V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lboc;->d:Lfoy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lboc;->b:Lbof;

    if-eqz v0, :cond_1

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    invoke-static {p1}, Lboc;->b(Lfrl;)Lfoy;

    move-result-object v0

    .line 297
    if-eqz v0, :cond_0

    .line 301
    invoke-virtual {p0}, Lboc;->a()V

    .line 302
    iput-object v0, p0, Lboc;->d:Lfoy;

    .line 303
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lboc;->a(Lfrl;I)V

    goto :goto_0
.end method

.method private a(Lfrl;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 308
    iget-object v0, p0, Lboc;->d:Lfoy;

    iget-object v0, v0, Lfoy;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    if-ne p2, v1, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_5

    .line 310
    iget-object v0, p0, Lboc;->i:Lfgk;

    invoke-virtual {v0}, Lfgk;->a()Lfgn;

    move-result-object v0

    iget-object v2, p0, Lboc;->d:Lfoy;

    .line 311
    iget-object v2, v2, Lfoy;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lfgn;->a(Ljava/lang/String;)Lfgn;

    move-result-object v2

    .line 312
    iput-boolean v1, v2, Lfgn;->c:Z

    iget-object v0, p0, Lboc;->d:Lfoy;

    .line 314
    iget-object v0, v0, Lfoy;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lboc;->d:Lfoy;

    .line 315
    iget-object v0, v0, Lfoy;->f:Ljava/lang/String;

    .line 313
    :goto_1
    invoke-virtual {v2, v0}, Lfgn;->b(Ljava/lang/String;)Lfgn;

    move-result-object v0

    .line 316
    iget-object v1, p0, Lboc;->d:Lfoy;

    iget-object v1, v1, Lfoy;->g:[B

    if-eqz v1, :cond_0

    .line 317
    iget-object v1, p0, Lboc;->d:Lfoy;

    iget-object v1, v1, Lfoy;->g:[B

    invoke-virtual {v0, v1}, Lfgn;->a([B)V

    .line 319
    :cond_0
    iget-object v1, p0, Lboc;->i:Lfgk;

    new-instance v2, Lboh;

    invoke-direct {v2, p0, p2, p1}, Lboh;-><init>(Lboc;ILfrl;)V

    invoke-virtual {v1, v0, v2}, Lfgk;->a(Lfgn;Lwv;)V

    .line 324
    :goto_2
    return-void

    .line 308
    :cond_1
    iget-object v0, p0, Lboc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbog;

    invoke-interface {v0}, Lbog;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 315
    :cond_4
    const-string v0, ""

    goto :goto_1

    .line 322
    :cond_5
    invoke-virtual {p0, p2, p1}, Lboc;->a(ILfrl;)Lboe;

    move-result-object v0

    iput-object v0, p0, Lboc;->b:Lbof;

    goto :goto_2
.end method

.method private static b(Lfrl;)Lfoy;
    .locals 1

    .prologue
    .line 457
    const/4 v0, 0x0

    .line 458
    if-eqz p0, :cond_0

    .line 459
    invoke-virtual {p0}, Lfrl;->o()Lfoy;

    move-result-object v0

    .line 462
    :cond_0
    return-object v0
.end method

.method private handleAdCompleteEvent(Lcyu;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 267
    iget-object v0, p1, Lcyu;->a:Lcyv;

    sget-object v1, Lcyv;->c:Lcyv;

    if-ne v0, v1, :cond_0

    .line 270
    iget-object v0, p0, Lboc;->b:Lbof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lboc;->b:Lbof;

    .line 271
    invoke-interface {v0}, Lbof;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lboc;->c:Z

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lboc;->b:Lbof;

    invoke-interface {v0}, Lbof;->a()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget-object v1, p0, Lboc;->f:Landroid/widget/ListView;

    iget v2, p0, Lboc;->g:I

    neg-int v0, v0

    const/16 v3, 0x190

    invoke-virtual {v1, v2, v0, v3}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(III)V

    .line 276
    :cond_0
    return-void
.end method

.method private handleVideoStageEvent(Ldac;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 239
    sget-object v0, Lbod;->a:[I

    iget-object v1, p1, Ldac;->a:Lgol;

    invoke-virtual {v1}, Lgol;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 242
    :pswitch_0
    iget-object v0, p1, Ldac;->d:Lfoy;

    iget-object v1, p1, Ldac;->b:Lfrl;

    invoke-direct {p0, v0, v1}, Lboc;->a(Lfoy;Lfrl;)V

    goto :goto_0

    .line 245
    :pswitch_1
    iput-boolean v3, p0, Lboc;->e:Z

    .line 246
    iget-object v0, p1, Ldac;->d:Lfoy;

    iget-object v1, p1, Ldac;->b:Lfrl;

    iget-object v2, p0, Lboc;->d:Lfoy;

    if-eq v2, v0, :cond_1

    invoke-direct {p0, v0, v1}, Lboc;->a(Lfoy;Lfrl;)V

    :cond_1
    iput-boolean v3, p0, Lboc;->e:Z

    iget-object v0, p0, Lboc;->b:Lbof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lboc;->b:Lbof;

    invoke-interface {v0}, Lbof;->c()V

    goto :goto_0

    .line 250
    :pswitch_2
    iget-object v0, p1, Ldac;->b:Lfrl;

    invoke-direct {p0, v0}, Lboc;->a(Lfrl;)V

    goto :goto_0

    .line 253
    :pswitch_3
    iput-boolean v3, p0, Lboc;->e:Z

    .line 254
    iget-object v0, p1, Ldac;->b:Lfrl;

    invoke-static {v0}, Lboc;->b(Lfrl;)Lfoy;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lboc;->d:Lfoy;

    if-nez v2, :cond_2

    invoke-direct {p0, v0}, Lboc;->a(Lfrl;)V

    :goto_1
    iput-boolean v3, p0, Lboc;->e:Z

    iget-object v0, p0, Lboc;->b:Lbof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lboc;->b:Lbof;

    invoke-interface {v0}, Lbof;->c()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lboc;->d:Lfoy;

    if-ne v0, v1, :cond_0

    goto :goto_1

    .line 257
    :pswitch_4
    invoke-virtual {p0}, Lboc;->a()V

    goto :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method a(ILfrl;)Lboe;
    .locals 3

    .prologue
    .line 339
    iget-object v0, p0, Lboc;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboe;

    .line 341
    const/4 v2, 0x1

    if-eq p1, v2, :cond_1

    invoke-interface {v0}, Lboe;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    iget-object v2, p0, Lboc;->d:Lfoy;

    .line 342
    invoke-interface {v0, v2, p2}, Lboe;->a(Lfoy;Lfrl;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 346
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 419
    iget-object v0, p0, Lboc;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbof;

    .line 420
    invoke-interface {v0}, Lbof;->d()V

    goto :goto_0

    .line 422
    :cond_0
    iget-object v0, p0, Lboc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbof;

    .line 423
    invoke-interface {v0}, Lbof;->d()V

    goto :goto_1

    .line 425
    :cond_1
    iput-object v2, p0, Lboc;->b:Lbof;

    .line 426
    const/4 v0, 0x1

    iput-boolean v0, p0, Lboc;->c:Z

    .line 427
    iput-object v2, p0, Lboc;->d:Lfoy;

    .line 428
    const/4 v0, 0x0

    iput-boolean v0, p0, Lboc;->e:Z

    .line 429
    return-void
.end method
