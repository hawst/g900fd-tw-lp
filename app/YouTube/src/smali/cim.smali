.class public Lcim;
.super Lfsa;
.source "SourceFile"


# instance fields
.field final a:Lfhz;

.field private final b:Leyp;

.field private final c:Lfsj;

.field private final d:Landroid/content/res/Resources;

.field private final e:Landroid/view/LayoutInflater;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/LinearLayout;

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;Lfhz;Lfdw;Lfrz;)V
    .locals 3

    .prologue
    .line 62
    invoke-direct {p0, p5, p6}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 63
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcim;->b:Leyp;

    .line 64
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcim;->c:Lfsj;

    .line 65
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcim;->a:Lfhz;

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcim;->d:Landroid/content/res/Resources;

    .line 67
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcim;->e:Landroid/view/LayoutInflater;

    .line 69
    iget-object v0, p0, Lcim;->e:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcim;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcim;->f:Landroid/view/View;

    .line 70
    iget-object v0, p0, Lcim;->f:Landroid/view/View;

    invoke-interface {p3, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 71
    return-void
.end method

.method private a(Lfsg;Lfnr;)Landroid/view/View;
    .locals 12

    .prologue
    const v11, 0x7f08008b

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/16 v4, 0x8

    const/4 v10, 0x0

    .line 75
    invoke-super {p0, p1, p2}, Lfsa;->a(Lfsg;Lfqh;)Landroid/view/View;

    .line 76
    iget-boolean v0, p0, Lcim;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcim;->d:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcim;->i:I

    if-ne v0, v1, :cond_0

    .line 77
    iget-object v0, p0, Lcim;->c:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    .line 81
    :cond_0
    iget-boolean v0, p0, Lcim;->h:Z

    if-nez v0, :cond_2

    .line 82
    iget-object v0, p0, Lcim;->f:Landroid/view/View;

    const v1, 0x7f08029f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcim;->g:Landroid/widget/LinearLayout;

    .line 83
    iget-object v0, p0, Lcim;->f:Landroid/view/View;

    const v1, 0x7f08029d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 84
    iget-object v1, p2, Lfnr;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfnr;->a:Liav;

    iget-object v1, v1, Liav;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfnr;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfnr;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v1, p2, Lfnr;->a:Liav;

    iget-object v1, v1, Liav;->b:Lhog;

    .line 86
    new-instance v2, Lcin;

    invoke-direct {v2, p0, v1}, Lcin;-><init>(Lcim;Lhog;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcim;->f:Landroid/view/View;

    const v1, 0x7f0802a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 96
    iget-object v1, p0, Lcim;->f:Landroid/view/View;

    const v2, 0x7f0802a1

    .line 97
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 100
    invoke-virtual {p2}, Lfnr;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 101
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 102
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcim;->f:Landroid/view/View;

    const v1, 0x7f08033a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_2

    .line 105
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 118
    :cond_2
    iget-object v0, p0, Lcim;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 119
    invoke-virtual {p2}, Lfnr;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 120
    instance-of v1, v0, Lfnt;

    if-eqz v1, :cond_b

    .line 121
    iget-object v4, p0, Lcim;->g:Landroid/widget/LinearLayout;

    check-cast v0, Lfnt;

    .line 122
    iget-object v1, p0, Lcim;->e:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcim;->b()I

    move-result v2

    invoke-virtual {v1, v2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    iget-object v1, v0, Lfnt;->a:Liay;

    iget-object v1, v1, Liay;->d:Lhog;

    new-instance v2, Lcio;

    invoke-direct {v2, p0, v1}, Lcio;-><init>(Lcim;Lhog;)V

    invoke-virtual {v6, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0802a2

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v1, 0x7f080135

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    new-instance v2, Lfvi;

    iget-object v8, p0, Lcim;->b:Leyp;

    iget-object v9, v1, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    invoke-direct {v2, v8, v9}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iget-object v8, v0, Lfnt;->e:Lfnc;

    if-nez v8, :cond_4

    new-instance v8, Lfnc;

    iget-object v9, v0, Lfnt;->a:Liay;

    iget-object v9, v9, Liay;->a:Lhxf;

    invoke-direct {v8, v9}, Lfnc;-><init>(Lhxf;)V

    iput-object v8, v0, Lfnt;->e:Lfnc;

    :cond_4
    iget-object v8, v0, Lfnt;->e:Lfnc;

    invoke-virtual {v8}, Lfnc;->b()Z

    move-result v9

    invoke-virtual {v1, v9}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b(Z)V

    invoke-virtual {v2, v8, v10}, Lfvi;->a(Lfnc;Leyo;)V

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v8, v0, Lfnt;->b:Ljava/lang/CharSequence;

    if-nez v8, :cond_5

    iget-object v8, v0, Lfnt;->a:Liay;

    iget-object v8, v8, Liay;->b:Lhgz;

    invoke-static {v8}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v8

    iput-object v8, v0, Lfnt;->b:Ljava/lang/CharSequence;

    :cond_5
    iget-object v8, v0, Lfnt;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f080136

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v7, v0, Lfnt;->c:Ljava/lang/CharSequence;

    if-nez v7, :cond_6

    iget-object v7, v0, Lfnt;->a:Liay;

    iget-object v7, v7, Liay;->e:Lhgz;

    invoke-static {v7}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v7

    iput-object v7, v0, Lfnt;->c:Ljava/lang/CharSequence;

    :cond_6
    iget-object v7, v0, Lfnt;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget-object v2, v0, Lfnt;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    iget-object v2, v0, Lfnt;->a:Liay;

    iget-object v2, v2, Liay;->c:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v0, Lfnt;->d:Ljava/lang/CharSequence;

    :cond_7
    iget-object v0, v0, Lfnt;->d:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 108
    :cond_8
    iget-object v2, p2, Lfnr;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_9

    iget-object v2, p2, Lfnr;->a:Liav;

    iget-object v2, v2, Liav;->d:Liaz;

    if-eqz v2, :cond_9

    iget-object v2, p2, Lfnr;->a:Liav;

    iget-object v2, v2, Liav;->d:Liaz;

    iget-object v2, v2, Liaz;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, p2, Lfnr;->c:Ljava/lang/CharSequence;

    :cond_9
    iget-object v2, p2, Lfnr;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-virtual {p2}, Lfnr;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnu;

    .line 111
    if-nez v2, :cond_a

    move v4, v5

    :goto_3
    invoke-virtual {p0, v0, v4}, Lcim;->a(Lfnu;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 112
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 113
    goto :goto_2

    :cond_a
    move v4, v3

    .line 111
    goto :goto_3

    .line 123
    :cond_b
    instance-of v1, v0, Lfns;

    if-eqz v1, :cond_3

    .line 124
    iget-object v2, p0, Lcim;->g:Landroid/widget/LinearLayout;

    check-cast v0, Lfns;

    .line 125
    iget-object v1, p0, Lcim;->e:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcim;->c()I

    move-result v4

    invoke-virtual {v1, v4, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iget-object v1, v0, Lfns;->a:Liax;

    iget-object v1, v1, Liax;->d:Lhog;

    new-instance v6, Lcip;

    invoke-direct {v6, p0, v1}, Lcip;-><init>(Lcim;Lhog;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f080134

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v7, v0, Lfns;->b:Ljava/lang/CharSequence;

    if-nez v7, :cond_c

    iget-object v7, v0, Lfns;->a:Liax;

    iget-object v7, v7, Liax;->b:Lhgz;

    invoke-static {v7}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v7

    iput-object v7, v0, Lfns;->b:Ljava/lang/CharSequence;

    :cond_c
    iget-object v7, v0, Lfns;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f080136

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v7, v0, Lfns;->c:Ljava/lang/CharSequence;

    if-nez v7, :cond_d

    iget-object v7, v0, Lfns;->a:Liax;

    iget-object v7, v7, Liax;->e:Lhgz;

    invoke-static {v7}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v7

    iput-object v7, v0, Lfns;->c:Ljava/lang/CharSequence;

    :cond_d
    iget-object v7, v0, Lfns;->c:Ljava/lang/CharSequence;

    invoke-static {v1, v7}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    const v1, 0x7f080135

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    iget-object v6, v1, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget-object v7, v0, Lfns;->d:Ljava/lang/CharSequence;

    if-nez v7, :cond_e

    iget-object v7, v0, Lfns;->a:Liax;

    iget-object v7, v7, Liax;->c:Lhgz;

    invoke-static {v7}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v7

    iput-object v7, v0, Lfns;->d:Ljava/lang/CharSequence;

    :cond_e
    iget-object v7, v0, Lfns;->d:Ljava/lang/CharSequence;

    invoke-static {v6, v7}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    new-instance v6, Lfvi;

    iget-object v7, p0, Lcim;->b:Leyp;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    invoke-direct {v6, v7, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iget-object v1, v0, Lfns;->e:Lfnc;

    if-nez v1, :cond_f

    new-instance v1, Lfnc;

    iget-object v7, v0, Lfns;->a:Liax;

    iget-object v7, v7, Liax;->a:Lhxf;

    invoke-direct {v1, v7}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, v0, Lfns;->e:Lfnc;

    :cond_f
    iget-object v0, v0, Lfns;->e:Lfnc;

    invoke-virtual {v6, v0, v10}, Lfvi;->a(Lfnc;Leyo;)V

    .line 124
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 129
    :cond_10
    iput-boolean v5, p0, Lcim;->h:Z

    .line 130
    iget-object v0, p0, Lcim;->d:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcim;->i:I

    .line 131
    iget-object v0, p0, Lcim;->c:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 221
    const v0, 0x7f040132

    return v0
.end method

.method protected a(Lfnu;Z)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 199
    iget-object v0, p0, Lcim;->e:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcim;->d()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 201
    const v0, 0x7f08008b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lfnu;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    new-instance v2, Lfvi;

    iget-object v3, p0, Lcim;->b:Leyp;

    const v0, 0x7f0800a4

    .line 205
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v2, v3, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    .line 206
    invoke-virtual {p1}, Lfnu;->d()Lfnc;

    move-result-object v0

    invoke-virtual {v2, v0, v4}, Lfvi;->a(Lfnc;Leyo;)V

    .line 208
    iget-object v0, p1, Lfnu;->a:Liba;

    iget-object v0, v0, Liba;->d:Lhog;

    .line 209
    new-instance v2, Lciq;

    invoke-direct {v2, p0, v0}, Lciq;-><init>(Lcim;Lhog;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    return-object v1
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 41
    check-cast p2, Lfnr;

    invoke-direct {p0, p1, p2}, Lcim;->a(Lfsg;Lfnr;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 41
    check-cast p2, Lfnr;

    invoke-direct {p0, p1, p2}, Lcim;->a(Lfsg;Lfnr;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 225
    const v0, 0x7f040135

    return v0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 229
    const v0, 0x7f040134

    return v0
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 233
    const v0, 0x7f040137

    return v0
.end method
