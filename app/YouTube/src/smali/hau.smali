.class public final Lhau;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhau;


# instance fields
.field public b:Lhof;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12672
    const/4 v0, 0x0

    new-array v0, v0, [Lhau;

    sput-object v0, Lhau;->a:[Lhau;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12673
    invoke-direct {p0}, Lidf;-><init>()V

    .line 12676
    const/4 v0, 0x0

    iput-object v0, p0, Lhau;->b:Lhof;

    .line 12679
    const-string v0, ""

    iput-object v0, p0, Lhau;->c:Ljava/lang/String;

    .line 12673
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 12722
    const/4 v0, 0x0

    .line 12723
    iget-object v1, p0, Lhau;->b:Lhof;

    if-eqz v1, :cond_0

    .line 12724
    const/4 v0, 0x1

    iget-object v1, p0, Lhau;->b:Lhof;

    .line 12725
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12727
    :cond_0
    iget-object v1, p0, Lhau;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 12728
    const/4 v1, 0x2

    iget-object v2, p0, Lhau;->c:Ljava/lang/String;

    .line 12729
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12731
    :cond_1
    iget-object v1, p0, Lhau;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12732
    iput v0, p0, Lhau;->J:I

    .line 12733
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 12669
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhau;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhau;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhau;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhau;->b:Lhof;

    if-nez v0, :cond_2

    new-instance v0, Lhof;

    invoke-direct {v0}, Lhof;-><init>()V

    iput-object v0, p0, Lhau;->b:Lhof;

    :cond_2
    iget-object v0, p0, Lhau;->b:Lhof;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhau;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 12710
    iget-object v0, p0, Lhau;->b:Lhof;

    if-eqz v0, :cond_0

    .line 12711
    const/4 v0, 0x1

    iget-object v1, p0, Lhau;->b:Lhof;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 12713
    :cond_0
    iget-object v0, p0, Lhau;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 12714
    const/4 v0, 0x2

    iget-object v1, p0, Lhau;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 12716
    :cond_1
    iget-object v0, p0, Lhau;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 12718
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12691
    if-ne p1, p0, :cond_1

    .line 12696
    :cond_0
    :goto_0
    return v0

    .line 12692
    :cond_1
    instance-of v2, p1, Lhau;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 12693
    :cond_2
    check-cast p1, Lhau;

    .line 12694
    iget-object v2, p0, Lhau;->b:Lhof;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhau;->b:Lhof;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhau;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhau;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 12695
    :goto_2
    iget-object v2, p0, Lhau;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhau;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 12696
    goto :goto_0

    .line 12694
    :cond_4
    iget-object v2, p0, Lhau;->b:Lhof;

    iget-object v3, p1, Lhau;->b:Lhof;

    invoke-virtual {v2, v3}, Lhof;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhau;->c:Ljava/lang/String;

    iget-object v3, p1, Lhau;->c:Ljava/lang/String;

    .line 12695
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhau;->I:Ljava/util/List;

    iget-object v3, p1, Lhau;->I:Ljava/util/List;

    .line 12696
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 12700
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 12702
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhau;->b:Lhof;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 12703
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhau;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 12704
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhau;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 12705
    return v0

    .line 12702
    :cond_0
    iget-object v0, p0, Lhau;->b:Lhof;

    invoke-virtual {v0}, Lhof;->hashCode()I

    move-result v0

    goto :goto_0

    .line 12703
    :cond_1
    iget-object v0, p0, Lhau;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 12704
    :cond_2
    iget-object v1, p0, Lhau;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
