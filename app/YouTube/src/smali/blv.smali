.class final Lblv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldmw;


# instance fields
.field final synthetic a:Lbla;


# direct methods
.method constructor <init>(Lbla;)V
    .locals 0

    .prologue
    .line 1314
    iput-object p1, p0, Lblv;->a:Lbla;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1318
    iget-object v0, p0, Lblv;->a:Lbla;

    iget-object v0, v0, Lbla;->l:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1319
    return-void
.end method

.method public final a(Lgjm;)V
    .locals 3

    .prologue
    .line 1323
    iget-object v0, p1, Lgjm;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "pudl transfer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " added"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 1324
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1421
    return-void
.end method

.method public final b(Lgjm;)V
    .locals 0

    .prologue
    .line 1329
    return-void
.end method

.method public final c(Lgjm;)V
    .locals 7

    .prologue
    .line 1333
    iget-object v0, p1, Lgjm;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p1, Lgjm;->e:J

    iget-wide v4, p1, Lgjm;->f:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x44

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "pudl transfer: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " progress "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 1336
    invoke-static {p1}, La;->a(Lgjm;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1337
    iget-object v0, p0, Lblv;->a:Lbla;

    iget-object v0, v0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lblw;

    invoke-direct {v1, p0, p1}, Lblw;-><init>(Lblv;Lgjm;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 1353
    :cond_0
    return-void
.end method

.method public final d(Lgjm;)V
    .locals 5

    .prologue
    .line 1357
    iget-object v0, p1, Lgjm;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lgjm;->c:Lgjn;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl transfer: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " status "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 1359
    invoke-static {p1}, La;->a(Lgjm;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1360
    iget-object v0, p0, Lblv;->a:Lbla;

    iget-object v0, v0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lblx;

    invoke-direct {v1, p0, p1}, Lblx;-><init>(Lblv;Lgjm;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 1395
    :cond_0
    return-void
.end method

.method public final e(Lgjm;)V
    .locals 3

    .prologue
    .line 1399
    iget-object v0, p1, Lgjm;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "pudl transfer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 1401
    invoke-static {p1}, La;->a(Lgjm;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1402
    invoke-static {p1}, La;->c(Lgjm;)Ljava/lang/String;

    move-result-object v0

    .line 1405
    iget-object v1, p0, Lblv;->a:Lbla;

    .line 1406
    iget-object v1, v1, Lbla;->m:Lbmd;

    invoke-virtual {v1, v0}, Lbmd;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1407
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    .line 1408
    invoke-virtual {v0, p1}, Lbme;->b(Lgjm;)V

    .line 1411
    invoke-virtual {v0}, Lbme;->a()I

    move-result v2

    if-nez v2, :cond_0

    .line 1412
    iget-object v2, p0, Lblv;->a:Lbla;

    iget-object v2, v2, Lbla;->m:Lbmd;

    iget-object v0, v0, Lbme;->a:Lgbu;

    iget-object v0, v0, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lbmd;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1416
    :cond_1
    return-void
.end method
