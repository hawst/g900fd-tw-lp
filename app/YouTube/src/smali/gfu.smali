.class public final Lgfu;
.super Ledx;
.source "SourceFile"


# static fields
.field public static final k:Ljava/util/UUID;


# instance fields
.field private final l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 28
    new-instance v0, Ljava/util/UUID;

    const-wide v2, -0x121074568629b532L    # -3.563403477674908E221

    const-wide v4, -0x5c37d8232ae2de13L

    invoke-direct {v0, v2, v3, v4, v5}, Ljava/util/UUID;-><init>(JJ)V

    sput-object v0, Lgfu;->k:Ljava/util/UUID;

    return-void
.end method

.method public constructor <init>(ZLandroid/os/Looper;Ledw;Landroid/os/Handler;Ledz;)V
    .locals 6

    .prologue
    .line 37
    sget-object v1, Lgfu;->k:Ljava/util/UUID;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ledx;-><init>(Ljava/util/UUID;Landroid/os/Looper;Ledw;Landroid/os/Handler;Ledz;)V

    .line 38
    iput-boolean p1, p0, Lgfu;->l:Z

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lgfu;->l:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Ledx;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
