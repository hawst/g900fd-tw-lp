.class public final Lequ;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.REPLY_INTERNAL_GOOGLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lequ;->a:Landroid/content/Intent;

    iget-object v0, p0, Lequ;->a:Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lequ;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lequ;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lequ;
    .locals 2

    iget-object v0, p0, Lequ;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lequ;
    .locals 3

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    iget-object v1, p0, Lequ;->a:Landroid/content/Intent;

    const-string v2, "com.google.android.gms.plus.intent.extra.INTERNAL_PREFILLED_PLUS_MENTION"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lequ;
    .locals 2

    iget-object v0, p0, Lequ;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.INTERNAL_REPLY_ACTIVITY_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lequ;
    .locals 2

    iget-object v0, p0, Lequ;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lequ;
    .locals 2

    iget-object v0, p0, Lequ;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.SHARE_CONTEXT_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Lequ;
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "clientApplicationId must not be empty."

    invoke-static {v0, v1}, Lb;->b(ZLjava/lang/Object;)V

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lequ;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_APPLICATION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "clientApplicationId must be parsable to an int."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
