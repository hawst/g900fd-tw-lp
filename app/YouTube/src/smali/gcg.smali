.class public final enum Lgcg;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgcg;

.field public static final enum b:Lgcg;

.field public static final enum c:Lgcg;

.field private static final synthetic e:[Lgcg;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 82
    new-instance v0, Lgcg;

    const-string v1, "PUBLIC"

    const v2, 0x7f0900c1

    invoke-direct {v0, v1, v3, v2}, Lgcg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgcg;->a:Lgcg;

    .line 83
    new-instance v0, Lgcg;

    const-string v1, "UNLISTED"

    const v2, 0x7f0900c3

    invoke-direct {v0, v1, v4, v2}, Lgcg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgcg;->b:Lgcg;

    .line 84
    new-instance v0, Lgcg;

    const-string v1, "PRIVATE"

    const v2, 0x7f0900c2

    invoke-direct {v0, v1, v5, v2}, Lgcg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgcg;->c:Lgcg;

    .line 81
    const/4 v0, 0x3

    new-array v0, v0, [Lgcg;

    sget-object v1, Lgcg;->a:Lgcg;

    aput-object v1, v0, v3

    sget-object v1, Lgcg;->b:Lgcg;

    aput-object v1, v0, v4

    sget-object v1, Lgcg;->c:Lgcg;

    aput-object v1, v0, v5

    sput-object v0, Lgcg;->e:[Lgcg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 89
    iput p3, p0, Lgcg;->d:I

    .line 90
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgcg;
    .locals 1

    .prologue
    .line 81
    const-class v0, Lgcg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgcg;

    return-object v0
.end method

.method public static values()[Lgcg;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lgcg;->e:[Lgcg;

    invoke-virtual {v0}, [Lgcg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgcg;

    return-object v0
.end method
