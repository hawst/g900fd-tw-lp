.class public final Lbva;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbxi;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/content/res/Resources;

.field private d:Z

.field private e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbva;->d:Z

    .line 27
    iput-object p1, p0, Lbva;->a:Landroid/view/View;

    .line 28
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbva;->c:Landroid/content/res/Resources;

    .line 29
    const v0, 0x7f0801f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbva;->b:Landroid/widget/TextView;

    .line 30
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    return-void
.end method

.method private c(Lfmy;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 82
    iget-object v2, p0, Lbva;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfmy;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f020275

    :goto_0
    invoke-virtual {v2, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 83
    iget-object v1, p0, Lbva;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfmy;->c()Z

    move-result v0

    iget-object v2, p0, Lbva;->c:Landroid/content/res/Resources;

    if-eqz v0, :cond_2

    const v0, 0x7f0700bd

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    invoke-virtual {p1}, Lfmy;->b()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lbva;->b:Landroid/widget/TextView;

    const v1, 0x7f090266

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 89
    :goto_2
    return-void

    .line 82
    :cond_0
    iget-object v0, p1, Lfmy;->a:Lhwp;

    iget-object v0, v0, Lhwp;->j:Lhiq;

    if-eqz v0, :cond_1

    iget v0, v0, Lhiq;->a:I

    const/16 v3, 0x1f

    if-ne v0, v3, :cond_1

    const v0, 0x7f020276

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 83
    :cond_2
    const v0, 0x7f0700bb

    goto :goto_1

    .line 87
    :cond_3
    iget-object v0, p0, Lbva;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfmy;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private d(Lfmy;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v2, p0, Lbva;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfmy;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f020278

    :goto_0
    invoke-virtual {v2, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 111
    iget-object v0, p0, Lbva;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lbva;->c:Landroid/content/res/Resources;

    const v2, 0x7f0700bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 112
    invoke-virtual {p1}, Lfmy;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113
    iget-object v0, p0, Lbva;->b:Landroid/widget/TextView;

    const v1, 0x7f090267

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 117
    :goto_1
    return-void

    .line 110
    :cond_0
    iget-object v0, p1, Lfmy;->a:Lhwp;

    iget-object v0, v0, Lhwp;->j:Lhiq;

    if-eqz v0, :cond_1

    iget v0, v0, Lhiq;->a:I

    const/16 v3, 0x1f

    if-ne v0, v3, :cond_1

    const v0, 0x7f020279

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 115
    :cond_2
    iget-object v0, p0, Lbva;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfmy;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lbva;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 73
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lbva;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lbva;->b:Landroid/widget/TextView;

    invoke-static {v0, v1, p1}, La;->a(Landroid/content/Context;Landroid/view/View;I)V

    .line 79
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lbva;->e:Landroid/view/View$OnClickListener;

    .line 36
    return-void
.end method

.method public final a(Lfmy;)V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbva;->d:Z

    .line 51
    iget-object v0, p0, Lbva;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 52
    iget-boolean v0, p1, Lfmy;->d:Z

    if-eqz v0, :cond_0

    .line 53
    invoke-direct {p0, p1}, Lbva;->d(Lfmy;)V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-direct {p0, p1}, Lbva;->c(Lfmy;)V

    goto :goto_0
.end method

.method public final b(Lfmy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    iput-boolean v1, p0, Lbva;->d:Z

    .line 62
    iget-object v0, p0, Lbva;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 63
    iget-boolean v0, p1, Lfmy;->d:Z

    if-eqz v0, :cond_0

    .line 64
    invoke-direct {p0, p1}, Lbva;->c(Lfmy;)V

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-direct {p0, p1}, Lbva;->d(Lfmy;)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lbva;->d:Z

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lbva;->e:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 46
    :cond_0
    return-void
.end method
