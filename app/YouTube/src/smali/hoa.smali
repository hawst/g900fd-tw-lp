.class public final Lhoa;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhgz;

.field public c:Lhxf;

.field public d:Lhoc;

.field public e:[B

.field public f:Lhgz;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Lhgz;

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Lhoa;->a:Lhgz;

    iput-object v1, p0, Lhoa;->b:Lhgz;

    iput-object v1, p0, Lhoa;->c:Lhxf;

    iput-object v1, p0, Lhoa;->d:Lhoc;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhoa;->e:[B

    const-string v0, ""

    iput-object v0, p0, Lhoa;->g:Ljava/lang/String;

    iput-boolean v2, p0, Lhoa;->h:Z

    iput-object v1, p0, Lhoa;->f:Lhgz;

    iput-object v1, p0, Lhoa;->i:Lhgz;

    iput-boolean v2, p0, Lhoa;->j:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhoa;->a:Lhgz;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhoa;->a:Lhgz;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhoa;->b:Lhgz;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhoa;->b:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhoa;->c:Lhxf;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhoa;->c:Lhxf;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhoa;->d:Lhoc;

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Lhoa;->d:Lhoc;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lhoa;->e:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_4

    const/16 v1, 0x9

    iget-object v2, p0, Lhoa;->e:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhoa;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/16 v1, 0xa

    iget-object v2, p0, Lhoa;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lhoa;->h:Z

    if-eqz v1, :cond_6

    const/16 v1, 0xb

    iget-boolean v2, p0, Lhoa;->h:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhoa;->f:Lhgz;

    if-eqz v1, :cond_7

    const/16 v1, 0xd

    iget-object v2, p0, Lhoa;->f:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lhoa;->i:Lhgz;

    if-eqz v1, :cond_8

    const/16 v1, 0xe

    iget-object v2, p0, Lhoa;->i:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Lhoa;->j:Z

    if-eqz v1, :cond_9

    const/16 v1, 0xf

    iget-boolean v2, p0, Lhoa;->j:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lhoa;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhoa;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhoa;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhoa;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhoa;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhoa;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhoa;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhoa;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhoa;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhoa;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lhoa;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhoa;->c:Lhxf;

    if-nez v0, :cond_4

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhoa;->c:Lhxf;

    :cond_4
    iget-object v0, p0, Lhoa;->c:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhoa;->d:Lhoc;

    if-nez v0, :cond_5

    new-instance v0, Lhoc;

    invoke-direct {v0}, Lhoc;-><init>()V

    iput-object v0, p0, Lhoa;->d:Lhoc;

    :cond_5
    iget-object v0, p0, Lhoa;->d:Lhoc;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhoa;->e:[B

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhoa;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhoa;->h:Z

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lhoa;->f:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhoa;->f:Lhgz;

    :cond_6
    iget-object v0, p0, Lhoa;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhoa;->i:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhoa;->i:Lhgz;

    :cond_7
    iget-object v0, p0, Lhoa;->i:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhoa;->j:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x4a -> :sswitch_5
        0x52 -> :sswitch_6
        0x58 -> :sswitch_7
        0x6a -> :sswitch_8
        0x72 -> :sswitch_9
        0x78 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget-object v0, p0, Lhoa;->a:Lhgz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhoa;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhoa;->b:Lhgz;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhoa;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhoa;->c:Lhxf;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhoa;->c:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhoa;->d:Lhoc;

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Lhoa;->d:Lhoc;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhoa;->e:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0x9

    iget-object v1, p0, Lhoa;->e:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_4
    iget-object v0, p0, Lhoa;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0xa

    iget-object v1, p0, Lhoa;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_5
    iget-boolean v0, p0, Lhoa;->h:Z

    if-eqz v0, :cond_6

    const/16 v0, 0xb

    iget-boolean v1, p0, Lhoa;->h:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_6
    iget-object v0, p0, Lhoa;->f:Lhgz;

    if-eqz v0, :cond_7

    const/16 v0, 0xd

    iget-object v1, p0, Lhoa;->f:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_7
    iget-object v0, p0, Lhoa;->i:Lhgz;

    if-eqz v0, :cond_8

    const/16 v0, 0xe

    iget-object v1, p0, Lhoa;->i:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_8
    iget-boolean v0, p0, Lhoa;->j:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xf

    iget-boolean v1, p0, Lhoa;->j:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_9
    iget-object v0, p0, Lhoa;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhoa;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhoa;

    iget-object v2, p0, Lhoa;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhoa;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhoa;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhoa;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhoa;->c:Lhxf;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhoa;->c:Lhxf;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhoa;->d:Lhoc;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhoa;->d:Lhoc;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhoa;->e:[B

    iget-object v3, p1, Lhoa;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhoa;->g:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhoa;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_5
    iget-boolean v2, p0, Lhoa;->h:Z

    iget-boolean v3, p1, Lhoa;->h:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhoa;->f:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhoa;->f:Lhgz;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhoa;->i:Lhgz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhoa;->i:Lhgz;

    if-nez v2, :cond_3

    :goto_7
    iget-boolean v2, p0, Lhoa;->j:Z

    iget-boolean v3, p1, Lhoa;->j:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhoa;->I:Ljava/util/List;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhoa;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhoa;->a:Lhgz;

    iget-object v3, p1, Lhoa;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhoa;->b:Lhgz;

    iget-object v3, p1, Lhoa;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhoa;->c:Lhxf;

    iget-object v3, p1, Lhoa;->c:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhoa;->d:Lhoc;

    iget-object v3, p1, Lhoa;->d:Lhoc;

    invoke-virtual {v2, v3}, Lhoc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhoa;->g:Ljava/lang/String;

    iget-object v3, p1, Lhoa;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhoa;->f:Lhgz;

    iget-object v3, p1, Lhoa;->f:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhoa;->i:Lhgz;

    iget-object v3, p1, Lhoa;->i:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lhoa;->I:Ljava/util/List;

    iget-object v3, p1, Lhoa;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhoa;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhoa;->b:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhoa;->c:Lhxf;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhoa;->d:Lhoc;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    iget-object v2, p0, Lhoa;->e:[B

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhoa;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhoa;->h:Z

    if-eqz v0, :cond_7

    move v0, v3

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhoa;->f:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhoa;->i:Lhgz;

    if-nez v0, :cond_9

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lhoa;->j:Z

    if-eqz v2, :cond_a

    :goto_8
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhoa;->I:Ljava/util/List;

    if-nez v2, :cond_b

    :goto_9
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lhoa;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhoa;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhoa;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhoa;->d:Lhoc;

    invoke-virtual {v0}, Lhoc;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_5
    move v2, v0

    move v0, v1

    :goto_a
    iget-object v5, p0, Lhoa;->e:[B

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v5, p0, Lhoa;->e:[B

    aget-byte v5, v5, v0

    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_6
    iget-object v0, p0, Lhoa;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_7
    move v0, v4

    goto :goto_5

    :cond_8
    iget-object v0, p0, Lhoa;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lhoa;->i:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_7

    :cond_a
    move v3, v4

    goto :goto_8

    :cond_b
    iget-object v1, p0, Lhoa;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_9
.end method
