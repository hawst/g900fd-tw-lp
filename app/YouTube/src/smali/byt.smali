.class final Lbyt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Lflh;

.field c:Lawl;

.field d:Landroid/app/Dialog;

.field e:Landroid/app/Dialog;

.field final synthetic f:Lbym;

.field private g:Lbtr;


# direct methods
.method public constructor <init>(Lbym;)V
    .locals 0

    .prologue
    .line 549
    iput-object p1, p0, Lbyt;->f:Lbym;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 567
    iget-object v0, p0, Lbyt;->e:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 568
    new-instance v0, Lawl;

    iget-object v1, p0, Lbyt;->f:Lbym;

    iget-object v1, v1, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v2, 0x7f0400b1

    invoke-direct {v0, v1, v2}, Lawl;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lbyt;->c:Lawl;

    iget-object v0, p0, Lbyt;->f:Lbym;

    iget-object v0, v0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040024

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    new-instance v0, Lbyv;

    invoke-direct {v0, p0}, Lbyv;-><init>(Lbyt;)V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lbyx;

    iget-object v1, p0, Lbyt;->f:Lbym;

    iget-object v2, v1, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v4, p0, Lbyt;->c:Lawl;

    iget-object v1, p0, Lbyt;->f:Lbym;

    iget-object v5, v1, Lbym;->e:Lgku;

    iget-object v1, p0, Lbyt;->f:Lbym;

    iget-object v6, v1, Lbym;->i:Leyt;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lbyx;-><init>(Lbyt;Landroid/app/Activity;Ldob;Lcba;Lgku;Leyt;)V

    iput-object v0, p0, Lbyt;->g:Lbtr;

    new-instance v0, Leyv;

    iget-object v1, p0, Lbyt;->f:Lbym;

    iget-object v1, v1, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1}, Leyv;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0902a8

    invoke-virtual {v0, v1}, Leyv;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbyt;->e:Landroid/app/Dialog;

    .line 572
    :cond_0
    iget-object v0, p0, Lbyt;->b:Lflh;

    if-eqz v0, :cond_2

    .line 573
    iget-object v0, p0, Lbyt;->c:Lawl;

    iget v1, v0, Lawl;->a:I

    if-gez v1, :cond_1

    iput v9, v0, Lawl;->a:I

    iput v10, v0, Lawl;->b:I

    const/4 v1, 0x4

    iput v1, v0, Lawl;->c:I

    invoke-virtual {v0}, Lawl;->notifyDataSetChanged()V

    .line 578
    :cond_1
    :goto_0
    iget-object v0, p0, Lbyt;->g:Lbtr;

    new-array v1, v8, [Lfxg;

    iget-object v2, p0, Lbyt;->f:Lbym;

    iget-object v2, v2, Lbym;->d:Lfxe;

    invoke-interface {v2}, Lfxe;->a()Lfxj;

    move-result-object v2

    iget v3, v2, Lfxj;->c:I

    iget-object v2, v2, Lfxj;->a:Lfxl;

    invoke-interface {v2}, Lfxl;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "/feeds/api/users/default/playlists"

    invoke-virtual {v2, v4}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static {v2, v8, v3}, Lfxj;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lbtr;->a([Lfxg;)V

    .line 579
    iget-object v0, p0, Lbyt;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 580
    return-void

    .line 575
    :cond_2
    iget-object v0, p0, Lbyt;->c:Lawl;

    iget v1, v0, Lawl;->a:I

    if-ltz v1, :cond_1

    const/4 v1, -0x1

    iput v1, v0, Lawl;->a:I

    iput v9, v0, Lawl;->b:I

    iput v10, v0, Lawl;->c:I

    invoke-virtual {v0}, Lawl;->notifyDataSetChanged()V

    goto :goto_0
.end method
