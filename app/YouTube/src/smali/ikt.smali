.class public abstract Likt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lyj;


# static fields
.field private static a:Ljava/util/logging/Logger;

.field private static synthetic c:Z

.field public static k:I


# instance fields
.field private b:Ljava/nio/ByteBuffer;

.field public l:Ljava/lang/String;

.field public m:Lyn;

.field public n:Ljava/nio/ByteBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Likt;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Likt;->c:Z

    .line 49
    const v0, 0x19000

    sput v0, Likt;->k:I

    .line 50
    const-class v0, Likt;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Likt;->a:Ljava/util/logging/Logger;

    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    .line 61
    iput-object p1, p0, Likt;->l:Ljava/lang/String;

    .line 62
    return-void
.end method

.method private declared-synchronized g()V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 146
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Likt;->b:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_8

    .line 147
    iget-object v4, p0, Likt;->b:Ljava/nio/ByteBuffer;

    .line 148
    const/4 v2, 0x0

    iput-object v2, p0, Likt;->b:Ljava/nio/ByteBuffer;

    .line 149
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 150
    invoke-virtual {p0, v4}, Likt;->a(Ljava/nio/ByteBuffer;)V

    .line 151
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-lez v2, :cond_0

    .line 152
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Likt;->n:Ljava/nio/ByteBuffer;

    .line 154
    :cond_0
    sget-boolean v2, Likt;->c:Z

    if-nez v2, :cond_8

    invoke-virtual {p0}, Likt;->d_()J

    move-result-wide v6

    iget-object v2, p0, Likt;->n:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Likt;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    :goto_0
    int-to-long v2, v2

    add-long/2addr v2, v6

    invoke-static {v2, v3}, La;->d(J)I

    move-result v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {p0, v5}, Likt;->b(Ljava/nio/ByteBuffer;)V

    iget-object v2, p0, Likt;->n:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Likt;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    :goto_1
    iget-object v2, p0, Likt;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, p0, Likt;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v2, v0

    .line 154
    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-eq v2, v3, :cond_3

    sget-object v1, Likt;->a:Ljava/util/logging/Logger;

    iget-object v2, p0, Likt;->l:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2f

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ": remaining differs "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->severe(Ljava/lang/String;)V

    :goto_2
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_3
    if-lt v3, v6, :cond_7

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    if-eq v7, v8, :cond_6

    sget-object v1, Likt;->a:Ljava/util/logging/Logger;

    const-string v2, "%s: buffers differ at %d: %2X/%2X"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Likt;->l:Ljava/lang/String;

    aput-object v10, v6, v9

    const/4 v9, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v9

    const/4 v3, 0x2

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v3, 0x3

    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->severe(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    new-array v2, v2, [B

    invoke-virtual {v4, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v4, "original      : "

    const/4 v5, 0x4

    invoke-static {v1, v5}, Lyd;->a([BI)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-virtual {v3, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v4, "reconstructed : "

    const/4 v1, 0x4

    invoke-static {v2, v1}, Lyd;->a([BI)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-virtual {v3, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :cond_6
    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v2, v2, -0x1

    goto/16 :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_2

    .line 156
    :cond_8
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/nio/ByteBuffer;)V
.end method

.method public a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/ByteBuffer;JLyb;)V
    .locals 7

    .prologue
    .line 104
    instance-of v0, p1, Ljava/nio/channels/FileChannel;

    if-eqz v0, :cond_1

    sget v0, Likt;->k:I

    int-to-long v0, v0

    cmp-long v0, p3, v0

    if-lez v0, :cond_1

    move-object v0, p1

    .line 109
    check-cast v0, Ljava/nio/channels/FileChannel;

    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    move-object v2, p1

    check-cast v2, Ljava/nio/channels/FileChannel;

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v0

    iput-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 110
    check-cast v0, Ljava/nio/channels/FileChannel;

    check-cast p1, Ljava/nio/channels/FileChannel;

    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v2

    add-long/2addr v2, p3

    invoke-virtual {v0, v2, v3}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 115
    :goto_0
    iget-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_0

    .line 116
    invoke-direct {p0}, Likt;->g()V

    .line 119
    :cond_0
    return-void

    .line 112
    :cond_1
    sget-boolean v0, Likt;->c:Z

    if-nez v0, :cond_2

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p3, v0

    if-ltz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 113
    :cond_2
    invoke-static {p1, p3, p4}, Lyc;->a(Ljava/nio/channels/ReadableByteChannel;J)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 115
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/nio/channels/WritableByteChannel;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 122
    invoke-virtual {p0}, Likt;->l_()J

    move-result-wide v2

    invoke-static {v2, v3}, La;->d(J)I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 123
    iget-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Likt;->d_()J

    move-result-wide v2

    iget-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    :goto_0
    int-to-long v6, v0

    add-long/2addr v2, v6

    const-wide/16 v6, 0x8

    add-long/2addr v2, v6

    :goto_1
    const-wide v6, 0x100000000L

    cmp-long v0, v2, v6

    if-gez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_4

    invoke-virtual {p0}, Likt;->l_()J

    move-result-wide v0

    invoke-static {v4, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    iget-object v0, p0, Likt;->l:Ljava/lang/String;

    invoke-static {v0}, Lye;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    :goto_2
    const-string v0, "uuid"

    iget-object v1, p0, Likt;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 124
    :cond_1
    iget-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_5

    .line 125
    invoke-virtual {p0, v4}, Likt;->b(Ljava/nio/ByteBuffer;)V

    .line 126
    iget-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_6

    .line 127
    iget-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 128
    :goto_3
    iget-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-lez v0, :cond_6

    .line 129
    iget-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    goto :goto_3

    :cond_2
    move v0, v1

    .line 123
    goto :goto_0

    :cond_3
    iget-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    int-to-long v2, v0

    goto :goto_1

    :cond_4
    const-wide/16 v0, 0x1

    invoke-static {v4, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    iget-object v0, p0, Likt;->l:Ljava/lang/String;

    invoke-static {v0}, Lye;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Likt;->l_()J

    move-result-wide v0

    invoke-static {v4, v0, v1}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    goto :goto_2

    .line 133
    :cond_5
    iget-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 134
    iget-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 136
    :cond_6
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 137
    invoke-interface {p1, v4}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 138
    return-void
.end method

.method public final a(Lyn;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Likt;->m:Lyn;

    .line 201
    return-void
.end method

.method public b()Lye;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Likt;->m:Lyn;

    invoke-interface {v0}, Lyn;->b()Lye;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(Ljava/nio/ByteBuffer;)V
.end method

.method public abstract d_()J
.end method

.method public final k_()Lyn;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Likt;->m:Lyn;

    return-object v0
.end method

.method public l_()J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 175
    iget-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Likt;->d_()J

    move-result-wide v2

    .line 176
    :goto_0
    const-wide v4, 0xfffffff8L

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    add-int/lit8 v4, v0, 0x8

    const-string v0, "uuid"

    .line 178
    iget-object v5, p0, Likt;->l:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x10

    :goto_2
    add-int/2addr v0, v4

    int-to-long v4, v0

    add-long/2addr v2, v4

    .line 179
    iget-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_3

    :goto_3
    int-to-long v0, v1

    add-long/2addr v0, v2

    .line 180
    return-wide v0

    .line 175
    :cond_0
    iget-object v0, p0, Likt;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    int-to-long v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 176
    goto :goto_1

    :cond_2
    move v0, v1

    .line 178
    goto :goto_2

    .line 179
    :cond_3
    iget-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    goto :goto_3
.end method

.method public final m_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Likt;->l:Ljava/lang/String;

    return-object v0
.end method
