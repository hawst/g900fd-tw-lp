.class public final Lapo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcwz;


# instance fields
.field a:Lajf;

.field private final b:Lapp;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lajf;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajf;

    iput-object v0, p0, Lapo;->a:Lajf;

    .line 25
    new-instance v0, Lapp;

    invoke-direct {v0, p1}, Lapp;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lapo;->b:Lapp;

    .line 27
    :try_start_0
    iget-object v0, p0, Lapo;->b:Lapp;

    invoke-interface {p2, v0}, Lajf;->a(Lanv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcxa;)V
    .locals 2

    .prologue
    .line 39
    iget-object v1, p0, Lapo;->b:Lapp;

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxa;

    iput-object v0, v1, Lapp;->a:Lcxa;

    .line 40
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lapo;->a:Lajf;

    if-eqz v0, :cond_0

    .line 46
    :try_start_0
    iget-object v0, p0, Lapo;->a:Lajf;

    invoke-interface {v0, p1}, Lajf;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
