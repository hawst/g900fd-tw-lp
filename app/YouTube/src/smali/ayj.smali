.class final Layj;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private synthetic a:Layg;


# direct methods
.method public constructor <init>(Layg;)V
    .locals 1

    .prologue
    .line 161
    iput-object p1, p0, Layj;->a:Layg;

    .line 164
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 165
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 169
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 180
    const-string v0, "Unknown message sent to handler."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 183
    :goto_0
    return-void

    .line 171
    :pswitch_0
    iget-object v0, p0, Layj;->a:Layg;

    iget-object v0, v0, Layg;->d:Ldwq;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Layj;->a:Layg;

    iget-object v0, v0, Layg;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->l()Ldwo;

    move-result-object v0

    .line 173
    sget-object v1, Ldwo;->j:Ldwo;

    if-eq v0, v1, :cond_0

    sget-object v1, Ldwo;->a:Ldwo;

    if-eq v0, v1, :cond_0

    .line 174
    iget-object v0, p0, Layj;->a:Layg;

    iget-object v1, p0, Layj;->a:Layg;

    iget-object v1, v1, Layg;->d:Ldwq;

    invoke-interface {v1}, Ldwq;->r()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Layg;->c:I

    .line 177
    :cond_0
    const/4 v0, 0x0

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Layj;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
