.class public final Lgcb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgjg;
.implements Ljava/io/Serializable;


# instance fields
.field private A:Ljava/lang/String;

.field public a:Landroid/net/Uri;

.field public b:Landroid/net/Uri;

.field public c:Landroid/net/Uri;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:I

.field public i:Lgcc;

.field public j:Ljava/lang/String;

.field public k:Landroid/net/Uri;

.field public l:Z

.field public m:Z

.field public n:Landroid/net/Uri;

.field public o:I

.field public p:Landroid/net/Uri;

.field public q:I

.field public r:Landroid/net/Uri;

.field public s:I

.field public t:Landroid/net/Uri;

.field public u:Landroid/net/Uri;

.field public v:Landroid/net/Uri;

.field public w:Landroid/net/Uri;

.field public x:J

.field public y:J

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    const/4 v0, -0x1

    iput v0, p0, Lgcb;->h:I

    .line 207
    sget-object v0, Lgcc;->c:Lgcc;

    iput-object v0, p0, Lgcb;->i:Lgcc;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 433
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->a:Landroid/net/Uri;

    .line 434
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->b:Landroid/net/Uri;

    .line 435
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->c:Landroid/net/Uri;

    .line 436
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcb;->d:Ljava/lang/String;

    .line 437
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcb;->A:Ljava/lang/String;

    .line 438
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcb;->f:Ljava/lang/String;

    .line 439
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcb;->e:Ljava/lang/String;

    .line 440
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcb;->g:Ljava/lang/String;

    .line 441
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgcb;->h:I

    .line 442
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcc;

    iput-object v0, p0, Lgcb;->i:Lgcc;

    .line 443
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcb;->j:Ljava/lang/String;

    .line 444
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->k:Landroid/net/Uri;

    .line 445
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lgcb;->l:Z

    .line 446
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lgcb;->m:Z

    .line 447
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->n:Landroid/net/Uri;

    .line 448
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgcb;->o:I

    .line 449
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->p:Landroid/net/Uri;

    .line 450
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgcb;->q:I

    .line 451
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->r:Landroid/net/Uri;

    .line 452
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgcb;->s:I

    .line 453
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->t:Landroid/net/Uri;

    .line 454
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->u:Landroid/net/Uri;

    .line 455
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->v:Landroid/net/Uri;

    .line 456
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->w:Landroid/net/Uri;

    .line 457
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgcb;->x:J

    .line 458
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgcb;->y:J

    .line 459
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgcb;->z:I

    .line 460
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 399
    invoke-virtual {p0}, Lgcb;->a()Lgca;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lgcb;->a:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 404
    iget-object v0, p0, Lgcb;->b:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 405
    iget-object v0, p0, Lgcb;->c:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 406
    iget-object v0, p0, Lgcb;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 407
    iget-object v0, p0, Lgcb;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 408
    iget-object v0, p0, Lgcb;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 409
    iget-object v0, p0, Lgcb;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 410
    iget-object v0, p0, Lgcb;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 411
    iget v0, p0, Lgcb;->h:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 412
    iget-object v0, p0, Lgcb;->i:Lgcc;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 413
    iget-object v0, p0, Lgcb;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 414
    iget-object v0, p0, Lgcb;->k:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 415
    iget-boolean v0, p0, Lgcb;->l:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 416
    iget-boolean v0, p0, Lgcb;->m:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 417
    iget-object v0, p0, Lgcb;->n:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 418
    iget v0, p0, Lgcb;->o:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 419
    iget-object v0, p0, Lgcb;->p:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 420
    iget v0, p0, Lgcb;->q:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 421
    iget-object v0, p0, Lgcb;->r:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 422
    iget v0, p0, Lgcb;->s:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 423
    iget-object v0, p0, Lgcb;->t:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 424
    iget-object v0, p0, Lgcb;->u:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 425
    iget-object v0, p0, Lgcb;->v:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 426
    iget-object v0, p0, Lgcb;->w:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 427
    iget-wide v0, p0, Lgcb;->x:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 428
    iget-wide v0, p0, Lgcb;->y:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 429
    iget v0, p0, Lgcb;->z:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 430
    return-void
.end method


# virtual methods
.method public final a()Lgca;
    .locals 33

    .prologue
    .line 368
    new-instance v3, Lgca;

    move-object/from16 v0, p0

    iget-object v4, v0, Lgcb;->a:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgcb;->b:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgcb;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v7, v0, Lgcb;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lgcb;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lgcb;->A:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lgcb;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lgcb;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v12, v0, Lgcb;->h:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lgcb;->i:Lgcc;

    move-object/from16 v0, p0

    iget-object v14, v0, Lgcb;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lgcb;->k:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgcb;->l:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgcb;->m:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcb;->n:Landroid/net/Uri;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lgcb;->o:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcb;->p:Landroid/net/Uri;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lgcb;->q:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcb;->r:Landroid/net/Uri;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lgcb;->s:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcb;->t:Landroid/net/Uri;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcb;->u:Landroid/net/Uri;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcb;->v:Landroid/net/Uri;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcb;->w:Landroid/net/Uri;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lgcb;->x:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lgcb;->y:J

    move-wide/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lgcb;->z:I

    move/from16 v32, v0

    invoke-direct/range {v3 .. v32}, Lgca;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILgcc;Ljava/lang/String;Landroid/net/Uri;ZZLandroid/net/Uri;ILandroid/net/Uri;ILandroid/net/Uri;ILandroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JJI)V

    return-object v3
.end method

.method public final a(Ljava/lang/String;)Lgcb;
    .locals 1

    .prologue
    .line 257
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcb;->A:Ljava/lang/String;

    .line 258
    return-object p0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0}, Lgcb;->a()Lgca;

    move-result-object v0

    return-object v0
.end method
