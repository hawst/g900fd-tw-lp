.class public final Lbyy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field private final a:Lbrg;

.field private final b:Ljava/lang/String;

.field private synthetic c:Lbym;


# direct methods
.method public constructor <init>(Lbym;Lbrg;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 423
    iput-object p1, p0, Lbyy;->c:Lbym;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrg;

    iput-object v0, p0, Lbyy;->a:Lbrg;

    .line 425
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbyy;->b:Ljava/lang/String;

    .line 426
    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 450
    const-string v0, "Error rating"

    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 451
    iget-object v0, p0, Lbyy;->c:Lbym;

    iget-object v0, v0, Lbym;->i:Leyt;

    invoke-interface {v0, p1}, Leyt;->c(Ljava/lang/Throwable;)V

    .line 452
    iget-object v0, p0, Lbyy;->c:Lbym;

    iget-object v1, p0, Lbyy;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbym;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lbyy;->c:Lbym;

    iget-object v0, v0, Lbym;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 454
    iget-object v1, p0, Lbyy;->c:Lbym;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    invoke-static {v1, v3}, Lbym;->a(Landroid/view/View;Z)V

    .line 455
    iget-object v1, p0, Lbyy;->c:Lbym;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v3}, Lbym;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 458
    :cond_0
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 418
    sget-object v0, Lbyp;->a:[I

    iget-object v1, p0, Lbyy;->a:Lbrg;

    invoke-virtual {v1}, Lbrg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lbyy;->c:Lbym;

    iget-object v1, p0, Lbyy;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbym;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyy;->c:Lbym;

    iget-object v0, p0, Lbyy;->a:Lbrg;

    :cond_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lbyy;->c:Lbym;

    iget-object v0, v0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x7f090298

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lbyy;->c:Lbym;

    iget-object v0, v0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x7f090299

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lbyy;->c:Lbym;

    iget-object v0, v0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x7f09029a

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
