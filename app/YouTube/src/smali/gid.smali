.class public final Lgid;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 36
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "next"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "previous"

    aput-object v3, v1, v2

    .line 37
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lgid;->a:Ljava/util/Set;

    .line 36
    return-void
.end method

.method public static a()Lfbb;
    .locals 1

    .prologue
    .line 43
    const-string v0, "/feed"

    invoke-static {v0}, Lgid;->a(Ljava/lang/String;)Lfbb;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lfbb;
    .locals 4

    .prologue
    .line 53
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lfbb;

    invoke-direct {v0}, Lfbb;-><init>()V

    .line 55
    new-instance v1, Lgii;

    invoke-direct {v1}, Lgii;-><init>()V

    .line 56
    invoke-virtual {v0, p0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/openSearch:totalResults"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgih;

    invoke-direct {v3}, Lgih;-><init>()V

    .line 66
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/openSearch:startIndex"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgig;

    invoke-direct {v3}, Lgig;-><init>()V

    .line 72
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/openSearch:itemsPerPage"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgif;

    invoke-direct {v3}, Lgif;-><init>()V

    .line 78
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/link"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgie;

    invoke-direct {v3}, Lgie;-><init>()V

    .line 84
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 100
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lfbb;
    .locals 2

    .prologue
    .line 104
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    new-instance v0, Lfbb;

    invoke-direct {v0}, Lfbb;-><init>()V

    .line 106
    new-instance v1, Lgij;

    invoke-direct {v1}, Lgij;-><init>()V

    invoke-virtual {v0, p0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 117
    return-object v0
.end method

.method static synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lgid;->a:Ljava/util/Set;

    return-object v0
.end method
