.class public abstract Lafh;
.super Lgwp;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Ldef;

.field public final c:Ladr;

.field public final d:Ldbz;

.field public final e:Ldcr;

.field public final f:Ldge;

.field public final g:Ldcl;

.field public final h:Ldcq;

.field public final i:Ldcw;

.field private final j:Landroid/content/Context;

.field private final k:Ladq;

.field private final l:Laer;

.field private final m:Laep;

.field private final n:Laet;

.field private o:Lgwz;

.field private p:Lgww;

.field private q:Lgwt;

.field private r:Lgwq;

.field private s:Ljava/lang/String;

.field private t:Lgwi;

.field private u:Z

.field private v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ladq;Ldef;)V
    .locals 3

    .prologue
    .line 91
    invoke-direct {p0}, Lgwp;-><init>()V

    .line 92
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lafh;->j:Landroid/content/Context;

    .line 93
    const-string v0, "activityProxy cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladq;

    iput-object v0, p0, Lafh;->k:Ladq;

    .line 94
    const-string v0, "playerOverlaysLayout cannot be null"

    .line 95
    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    iput-object v0, p0, Lafh;->b:Ldef;

    .line 97
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lafh;->a:Landroid/os/Handler;

    .line 99
    new-instance v0, Laet;

    new-instance v1, Laga;

    invoke-direct {v1, p0}, Laga;-><init>(Lafh;)V

    invoke-direct {v0, p3, p2, v1}, Laet;-><init>(Landroid/view/View;Ladq;Laev;)V

    iput-object v0, p0, Lafh;->n:Laet;

    .line 102
    invoke-static {p1}, La;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Laea;

    iget-object v1, p0, Lafh;->n:Laet;

    invoke-direct {v0, p1, p3, v1}, Laea;-><init>(Landroid/content/Context;Landroid/view/View;Laet;)V

    iput-object v0, p0, Lafh;->c:Ladr;

    .line 107
    :goto_0
    iget-object v0, p0, Lafh;->c:Ladr;

    new-instance v1, Lagb;

    invoke-direct {v1, p0}, Lagb;-><init>(Lafh;)V

    invoke-interface {v0, v1}, Ladr;->a(Lads;)V

    .line 109
    new-instance v0, Lafz;

    invoke-direct {v0, p0}, Lafz;-><init>(Lafh;)V

    new-instance v1, Laer;

    invoke-direct {v1, p1, p2, v0, p3}, Laer;-><init>(Landroid/content/Context;Ladq;Laes;Ldef;)V

    iput-object v1, p0, Lafh;->l:Laer;

    .line 111
    new-instance v0, Laep;

    new-instance v1, Lafy;

    invoke-direct {v1, p0}, Lafy;-><init>(Lafh;)V

    invoke-direct {v0, p1, v1, p3}, Laep;-><init>(Landroid/content/Context;Laeq;Landroid/view/View;)V

    iput-object v0, p0, Lafh;->m:Laep;

    .line 115
    new-instance v0, Ldbz;

    iget-object v1, p0, Lafh;->c:Ladr;

    invoke-interface {v1}, Ladr;->a()I

    move-result v1

    invoke-direct {v0, p1, v1}, Ldbz;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lafh;->d:Ldbz;

    .line 116
    new-instance v0, Ldcr;

    invoke-direct {v0, p1}, Ldcr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lafh;->e:Ldcr;

    .line 117
    new-instance v0, Ldge;

    invoke-direct {v0, p1}, Ldge;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lafh;->f:Ldge;

    .line 118
    new-instance v0, Ldcl;

    const v1, 0x7f020049

    invoke-direct {v0, p1, v1}, Ldcl;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lafh;->g:Ldcl;

    .line 119
    new-instance v0, Ldcq;

    invoke-direct {v0, p1}, Ldcq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lafh;->h:Ldcq;

    .line 120
    new-instance v0, Ldcw;

    invoke-direct {v0, p1}, Ldcw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lafh;->i:Ldcw;

    .line 121
    sget-object v0, Lgwi;->a:Lgwi;

    iput-object v0, p0, Lafh;->t:Lgwi;

    .line 123
    const/4 v0, 0x7

    new-array v0, v0, [Ldec;

    const/4 v1, 0x0

    iget-object v2, p0, Lafh;->i:Ldcw;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lafh;->h:Ldcq;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lafh;->c:Ladr;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lafh;->f:Ldge;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lafh;->d:Ldbz;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lafh;->e:Ldcr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lafh;->g:Ldcl;

    aput-object v2, v0, v1

    invoke-virtual {p3, v0}, Ldef;->a([Ldec;)V

    .line 131
    return-void

    .line 105
    :cond_0
    new-instance v0, Ladv;

    iget-object v1, p0, Lafh;->n:Laet;

    invoke-direct {v0, p1, v1}, Ladv;-><init>(Landroid/content/Context;Laet;)V

    iput-object v0, p0, Lafh;->c:Ladr;

    goto/16 :goto_0
.end method

.method static synthetic a(Lafh;)Lgwz;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lafh;->o:Lgwz;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This YouTubePlayer has been released"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    return-void
.end method

.method static synthetic a(Lafh;Ljava/lang/String;Lgwh;)V
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lafh;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lafh;->n()V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, La;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, p2}, Lafh;->a(Lgwh;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lafh;Z)V
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lafh;->K()Z

    move-result v0

    iput-boolean v0, p0, Lafh;->u:Z

    invoke-virtual {p0}, Lafh;->n()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lafh;->m:Laep;

    invoke-virtual {v0}, Laep;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lafh;->m:Laep;

    invoke-virtual {v0}, Laep;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lafh;->m:Laep;

    invoke-virtual {v0}, Laep;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lafh;->m:Laep;

    invoke-virtual {v0}, Laep;->dismiss()V

    goto :goto_0
.end method

.method static synthetic b(Lafh;)Lgww;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lafh;->p:Lgww;

    return-object v0
.end method

.method static synthetic c(Lafh;)Lgwt;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lafh;->q:Lgwt;

    return-object v0
.end method

.method static synthetic d(Lafh;)Lgwq;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lafh;->r:Lgwq;

    return-object v0
.end method

.method static synthetic e(Lafh;)Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lafh;->u:Z

    return v0
.end method

.method static synthetic f(Lafh;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lafh;->j:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lafh;)Ladq;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lafh;->k:Ladq;

    return-object v0
.end method

.method static synthetic h(Lafh;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lafh;->s:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public abstract A()V
.end method

.method public abstract B()Z
.end method

.method public abstract C()Z
.end method

.method public abstract D()Z
.end method

.method public abstract E()V
.end method

.method public abstract F()V
.end method

.method public abstract G()I
.end method

.method public abstract H()I
.end method

.method public abstract I()V
.end method

.method public abstract J()V
.end method

.method public abstract K()Z
.end method

.method public abstract L()V
.end method

.method public final M()V
    .locals 2

    .prologue
    .line 586
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    :goto_0
    return-void

    .line 589
    :cond_0
    iget-object v0, p0, Lafh;->l:Laer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laer;->c(Z)V

    goto :goto_0
.end method

.method public final N()V
    .locals 2

    .prologue
    .line 593
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 597
    :goto_0
    return-void

    .line 596
    :cond_0
    iget-object v0, p0, Lafh;->l:Laer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laer;->c(Z)V

    goto :goto_0
.end method

.method public final O()V
    .locals 2

    .prologue
    .line 600
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 604
    :goto_0
    return-void

    .line 603
    :cond_0
    iget-object v0, p0, Lafh;->l:Laer;

    const/4 v1, 0x1

    iput-boolean v1, v0, Laer;->p:Z

    invoke-virtual {v0}, Laer;->d()V

    goto :goto_0
.end method

.method public final P()V
    .locals 2

    .prologue
    .line 607
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 611
    :goto_0
    return-void

    .line 610
    :cond_0
    iget-object v0, p0, Lafh;->l:Laer;

    const/4 v1, 0x0

    iput-boolean v1, v0, Laer;->p:Z

    invoke-virtual {v0}, Laer;->d()V

    goto :goto_0
.end method

.method public final Q()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 614
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 618
    :goto_0
    return-void

    .line 617
    :cond_0
    iget-object v0, p0, Lafh;->n:Laet;

    iget-object v1, v0, Laet;->a:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, v0, Laet;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public final R()V
    .locals 1

    .prologue
    .line 621
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 625
    :goto_0
    return-void

    .line 624
    :cond_0
    iget-object v0, p0, Lafh;->n:Laet;

    invoke-virtual {v0}, Laet;->a()V

    goto :goto_0
.end method

.method public final S()V
    .locals 2

    .prologue
    .line 641
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafi;

    invoke-direct {v1, p0}, Lafi;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 653
    return-void
.end method

.method public final T()V
    .locals 2

    .prologue
    .line 656
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafq;

    invoke-direct {v1, p0}, Lafq;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 668
    return-void
.end method

.method public final U()V
    .locals 2

    .prologue
    .line 671
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafr;

    invoke-direct {v1, p0}, Lafr;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 683
    return-void
.end method

.method public final V()V
    .locals 2

    .prologue
    .line 688
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafs;

    invoke-direct {v1, p0}, Lafs;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 700
    return-void
.end method

.method public final W()V
    .locals 2

    .prologue
    .line 718
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafu;

    invoke-direct {v1, p0}, Lafu;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 730
    return-void
.end method

.method public final X()V
    .locals 2

    .prologue
    .line 733
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafv;

    invoke-direct {v1, p0}, Lafv;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 745
    return-void
.end method

.method public final Y()V
    .locals 2

    .prologue
    .line 748
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafw;

    invoke-direct {v1, p0}, Lafw;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 760
    return-void
.end method

.method public final Z()V
    .locals 2

    .prologue
    .line 780
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafj;

    invoke-direct {v1, p0}, Lafj;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 792
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 228
    invoke-direct {p0}, Lafh;->a()V

    .line 229
    iget-object v0, p0, Lafh;->l:Laer;

    invoke-virtual {v0, p1}, Laer;->a(I)V

    .line 230
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 280
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v2

    if-nez v2, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    iget-object v2, p0, Lafh;->l:Laer;

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    iget v4, v2, Laer;->e:I

    if-eq v3, v4, :cond_0

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    iput v3, v2, Laer;->e:I

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v0, :cond_3

    :goto_1
    iget-boolean v3, v2, Laer;->g:Z

    if-eqz v3, :cond_4

    iget-boolean v3, v2, Laer;->j:Z

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    iget-object v0, v2, Laer;->b:Laes;

    invoke-interface {v0, v1}, Laes;->a(Z)V

    :cond_2
    :goto_2
    iput-boolean v1, v2, Laer;->m:Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    iget-boolean v3, v2, Laer;->n:Z

    if-eqz v3, :cond_2

    iget-boolean v3, v2, Laer;->m:Z

    if-eqz v3, :cond_2

    if-nez v0, :cond_2

    invoke-virtual {v2}, Laer;->e()V

    goto :goto_2
.end method

.method public final a(Lgwh;)V
    .locals 2

    .prologue
    .line 763
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafx;

    invoke-direct {v1, p0, p1}, Lafx;-><init>(Lafh;Lgwh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 775
    return-void
.end method

.method public final a(Lgwq;)V
    .locals 0

    .prologue
    .line 222
    invoke-direct {p0}, Lafh;->a()V

    .line 223
    iput-object p1, p0, Lafh;->r:Lgwq;

    .line 224
    return-void
.end method

.method public final a(Lgwt;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0}, Lafh;->a()V

    .line 217
    iput-object p1, p0, Lafh;->q:Lgwt;

    .line 218
    return-void
.end method

.method public final a(Lgww;)V
    .locals 0

    .prologue
    .line 210
    invoke-direct {p0}, Lafh;->a()V

    .line 211
    iput-object p1, p0, Lafh;->p:Lgww;

    .line 212
    return-void
.end method

.method public final a(Lgwz;)V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0}, Lafh;->a()V

    .line 204
    iput-object p1, p0, Lafh;->o:Lgwz;

    .line 205
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 247
    invoke-static {p1}, Lgwi;->valueOf(Ljava/lang/String;)Lgwi;

    move-result-object v0

    .line 248
    invoke-direct {p0}, Lafh;->a()V

    .line 249
    sget-object v1, Lafp;->a:[I

    invoke-virtual {v0}, Lgwi;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 265
    const-string v0, "Unhandled PlayerStyle"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lafh;->t:Lgwi;

    .line 269
    :goto_0
    iput-object v0, p0, Lafh;->t:Lgwi;

    .line 270
    return-void

    .line 251
    :pswitch_0
    iget-object v1, p0, Lafh;->c:Ladr;

    invoke-interface {v1, v3}, Ladr;->a(Z)V

    .line 252
    iget-object v1, p0, Lafh;->c:Ladr;

    invoke-interface {v1, v3}, Ladr;->b(Z)V

    .line 253
    iget-object v1, p0, Lafh;->b:Ldef;

    invoke-virtual {v1, v4}, Ldef;->setFocusable(Z)V

    goto :goto_0

    .line 256
    :pswitch_1
    iget-object v1, p0, Lafh;->c:Ladr;

    invoke-interface {v1, v4}, Ladr;->a(Z)V

    .line 257
    iget-object v1, p0, Lafh;->c:Ladr;

    invoke-interface {v1, v3}, Ladr;->b(Z)V

    .line 258
    iget-object v1, p0, Lafh;->b:Ldef;

    invoke-virtual {v1, v4}, Ldef;->setFocusable(Z)V

    goto :goto_0

    .line 261
    :pswitch_2
    iget-object v1, p0, Lafh;->c:Ladr;

    invoke-interface {v1, v4}, Ladr;->b(Z)V

    .line 262
    iget-object v1, p0, Lafh;->b:Ldef;

    invoke-virtual {v1, v3}, Ldef;->setFocusable(Z)V

    goto :goto_0

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0}, Lafh;->a()V

    .line 289
    invoke-virtual {p0, p1, p2}, Lafh;->c(Ljava/lang/String;I)V

    .line 290
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 300
    invoke-direct {p0}, Lafh;->a()V

    .line 301
    invoke-virtual {p0, p1, p2, p3}, Lafh;->c(Ljava/lang/String;II)V

    .line 302
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 558
    iput-object p1, p0, Lafh;->s:Ljava/lang/String;

    .line 559
    iget-object v0, p0, Lafh;->c:Ladr;

    invoke-interface {v0, p2}, Ladr;->a(Ljava/lang/String;)V

    .line 560
    return-void
.end method

.method public final a(Ljava/util/List;II)V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Lafh;->a()V

    .line 313
    invoke-virtual {p0, p1, p2, p3}, Lafh;->c(Ljava/util/List;II)V

    .line 314
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 181
    invoke-virtual {p0, p1}, Lafh;->c(Z)V

    .line 182
    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 439
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 440
    const/4 v0, 0x0

    .line 442
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lafh;->c(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 470
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 471
    :cond_0
    const/4 v0, 0x0

    .line 484
    :goto_0
    return v0

    .line 474
    :cond_1
    const-string v0, "playerStyle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lafh;->a(Ljava/lang/String;)V

    .line 476
    iget-object v0, p0, Lafh;->l:Laer;

    const-string v1, "fullscreenHelperState"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-boolean v3, v0, Laer;->h:Z

    const-string v2, "controlFlags"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Laer;->a(I)V

    const-string v2, "defaultRequestedOrientation"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Laer;->i:I

    const-string v2, "isFullscreen"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, v0, Laer;->b:Laes;

    invoke-interface {v0, v3}, Laes;->a(Z)V

    .line 482
    :cond_2
    const-string v0, "apiPlayerState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lafh;->a([B)Z

    move-result v0

    goto :goto_0
.end method

.method public abstract a([B)Z
.end method

.method public final aa()V
    .locals 2

    .prologue
    .line 795
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafk;

    invoke-direct {v1, p0}, Lafk;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 807
    return-void
.end method

.method public final ab()V
    .locals 2

    .prologue
    .line 810
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafl;

    invoke-direct {v1, p0}, Lafl;-><init>(Lafh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 822
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 240
    invoke-direct {p0}, Lafh;->a()V

    .line 241
    iget-object v0, p0, Lafh;->l:Laer;

    invoke-virtual {p0}, Lafh;->k()I

    move-result v1

    or-int/2addr v1, p1

    invoke-virtual {v0, v1}, Laer;->a(I)V

    .line 242
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 703
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Laft;

    invoke-direct {v1, p0, p1}, Laft;-><init>(Lafh;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 715
    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 294
    invoke-direct {p0}, Lafh;->a()V

    .line 295
    invoke-virtual {p0, p1, p2}, Lafh;->d(Ljava/lang/String;I)V

    .line 296
    return-void
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 306
    invoke-direct {p0}, Lafh;->a()V

    .line 307
    invoke-virtual {p0, p1, p2, p3}, Lafh;->d(Ljava/lang/String;II)V

    .line 308
    return-void
.end method

.method public final b(Ljava/util/List;II)V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Lafh;->a()V

    .line 319
    invoke-virtual {p0, p1, p2, p3}, Lafh;->d(Ljava/util/List;II)V

    .line 320
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0}, Lafh;->a()V

    .line 275
    invoke-virtual {p0, p1}, Lafh;->g(Z)V

    .line 276
    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 448
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 449
    const/4 v0, 0x0

    .line 451
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lafh;->d(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 409
    invoke-direct {p0}, Lafh;->a()V

    .line 410
    invoke-virtual {p0, p1}, Lafh;->e(I)V

    .line 411
    return-void
.end method

.method public abstract c(Ljava/lang/String;I)V
.end method

.method public abstract c(Ljava/lang/String;II)V
.end method

.method public abstract c(Ljava/util/List;II)V
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 346
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    :goto_0
    return-void

    .line 349
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lafh;->v:Z

    .line 350
    iget-object v0, p0, Lafh;->l:Laer;

    iget-object v1, v0, Laer;->o:Ldnz;

    invoke-virtual {v1}, Ldnz;->a()V

    iget-object v1, v0, Laer;->c:Ldoj;

    invoke-virtual {v1}, Ldoj;->disable()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Laer;->f:Z

    .line 351
    iget-object v0, p0, Lafh;->n:Laet;

    invoke-virtual {v0}, Laet;->a()V

    .line 352
    iput-object v2, p0, Lafh;->o:Lgwz;

    .line 353
    iput-object v2, p0, Lafh;->p:Lgww;

    .line 354
    iput-object v2, p0, Lafh;->q:Lgwt;

    .line 355
    iput-object v2, p0, Lafh;->r:Lgwq;

    .line 356
    invoke-virtual {p0, p1}, Lafh;->j(Z)V

    goto :goto_0
.end method

.method public abstract c(ILandroid/view/KeyEvent;)Z
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 415
    invoke-direct {p0}, Lafh;->a()V

    .line 416
    invoke-virtual {p0, p1}, Lafh;->f(I)V

    .line 417
    return-void
.end method

.method public abstract d(Ljava/lang/String;I)V
.end method

.method public abstract d(Ljava/lang/String;II)V
.end method

.method public abstract d(Ljava/util/List;II)V
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 421
    invoke-direct {p0}, Lafh;->a()V

    .line 422
    invoke-virtual {p0, p1}, Lafh;->f(Z)V

    .line 423
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lafh;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract d(ILandroid/view/KeyEvent;)Z
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lafh;->l:Laer;

    iget-boolean v1, v0, Laer;->g:Z

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Laer;->l:Z

    if-nez v1, :cond_0

    iget-object v0, v0, Laer;->b:Laes;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Laes;->b(Z)V

    goto :goto_0
.end method

.method public abstract e(I)V
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 427
    invoke-direct {p0}, Lafh;->a()V

    .line 428
    invoke-virtual {p0, p1}, Lafh;->h(Z)V

    .line 429
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    :goto_0
    return-void

    .line 158
    :cond_0
    invoke-virtual {p0}, Lafh;->I()V

    goto :goto_0
.end method

.method public abstract f(I)V
.end method

.method public abstract f(Z)V
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lafh;->k:Ladq;

    iget-object v0, v0, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    invoke-virtual {p0, v0}, Lafh;->i(Z)V

    goto :goto_0
.end method

.method public final g(I)V
    .locals 2

    .prologue
    .line 840
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafn;

    invoke-direct {v1, p0, p1}, Lafn;-><init>(Lafh;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 852
    return-void
.end method

.method public abstract g(Z)V
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lafh;->u:Z

    .line 176
    iget-object v0, p0, Lafh;->m:Laep;

    invoke-virtual {v0}, Laep;->dismiss()V

    goto :goto_0
.end method

.method public abstract h(Z)V
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lafh;->p:Lgww;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lafh;->v:Z

    if-nez v0, :cond_0

    .line 188
    :try_start_0
    iget-object v0, p0, Lafh;->p:Lgww;

    sget-object v1, Lgwh;->h:Lgwh;

    invoke-virtual {v1}, Lgwh;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lgww;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lafh;->c(Z)V

    .line 194
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 190
    new-instance v1, Ll;

    invoke-direct {v1, v0}, Ll;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public abstract i(Z)V
.end method

.method public final j()Lgxl;
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lafh;->b:Ldef;

    new-instance v1, Lgxo;

    invoke-direct {v1, v0}, Lgxo;-><init>(Ljava/lang/Object;)V

    return-object v1
.end method

.method public abstract j(Z)V
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Lafh;->a()V

    .line 235
    iget-object v0, p0, Lafh;->l:Laer;

    invoke-virtual {v0}, Laer;->c()I

    move-result v0

    return v0
.end method

.method public final k(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 565
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    iget-object v3, p0, Lafh;->l:Laer;

    iput-boolean p1, v3, Laer;->g:Z

    if-eqz p1, :cond_7

    iget-boolean v0, v3, Laer;->j:Z

    if-eqz v0, :cond_b

    iget-object v0, v3, Laer;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-eq v0, v4, :cond_5

    move v0, v1

    :goto_1
    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Laer;->b(I)V

    iget-object v4, v3, Laer;->c:Ldoj;

    iget-boolean v4, v4, Ldoj;->a:Z

    if-nez v4, :cond_2

    iget-object v4, v3, Laer;->c:Ldoj;

    invoke-virtual {v4}, Ldoj;->enable()V

    :cond_2
    :goto_2
    iget-boolean v4, v3, Laer;->n:Z

    if-eqz v4, :cond_3

    if-eqz v0, :cond_6

    iput-boolean v1, v3, Laer;->m:Z

    :cond_3
    :goto_3
    iget-boolean v2, v3, Laer;->l:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_4

    iget-boolean v0, v3, Laer;->d:Z

    if-nez v0, :cond_0

    :cond_4
    iget-object v0, v3, Laer;->b:Laes;

    invoke-interface {v0, v1}, Laes;->b(Z)V

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    iput-boolean v2, v3, Laer;->m:Z

    invoke-virtual {v3}, Laer;->e()V

    goto :goto_3

    :cond_7
    iput-boolean v2, v3, Laer;->m:Z

    iget-boolean v0, v3, Laer;->j:Z

    if-eqz v0, :cond_9

    iget-boolean v0, v3, Laer;->k:Z

    if-nez v0, :cond_8

    iget-object v0, v3, Laer;->c:Ldoj;

    invoke-virtual {v0}, Ldoj;->disable()V

    :cond_8
    invoke-virtual {v3}, Laer;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, v3, Laer;->i:I

    invoke-virtual {v3, v0}, Laer;->b(I)V

    :cond_9
    iget-boolean v0, v3, Laer;->n:Z

    if-eqz v0, :cond_a

    iget-object v0, v3, Laer;->o:Ldnz;

    invoke-virtual {v0, v2}, Ldnz;->b(Z)V

    :cond_a
    iget-boolean v0, v3, Laer;->l:Z

    if-nez v0, :cond_0

    iget-object v0, v3, Laer;->b:Laes;

    invoke-interface {v0, v2}, Laes;->b(Z)V

    goto :goto_0

    :cond_b
    move v0, v2

    goto :goto_2
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0}, Lafh;->a()V

    .line 325
    invoke-virtual {p0}, Lafh;->y()V

    .line 326
    return-void
.end method

.method public final l(Z)V
    .locals 2

    .prologue
    .line 825
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafm;

    invoke-direct {v1, p0, p1}, Lafm;-><init>(Lafh;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 837
    return-void
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 330
    invoke-direct {p0}, Lafh;->a()V

    .line 331
    invoke-virtual {p0}, Lafh;->z()V

    .line 332
    return-void
.end method

.method public final m(Z)V
    .locals 2

    .prologue
    .line 855
    iget-object v0, p0, Lafh;->a:Landroid/os/Handler;

    new-instance v1, Lafo;

    invoke-direct {v1, p0, p1}, Lafo;-><init>(Lafh;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 867
    return-void
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 336
    invoke-direct {p0}, Lafh;->a()V

    .line 337
    invoke-virtual {p0}, Lafh;->A()V

    .line 338
    return-void
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 361
    invoke-direct {p0}, Lafh;->a()V

    .line 362
    invoke-virtual {p0}, Lafh;->B()Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 367
    invoke-direct {p0}, Lafh;->a()V

    .line 368
    invoke-virtual {p0}, Lafh;->C()Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 373
    invoke-direct {p0}, Lafh;->a()V

    .line 374
    invoke-virtual {p0}, Lafh;->D()Z

    move-result v0

    return v0
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 379
    invoke-direct {p0}, Lafh;->a()V

    .line 380
    invoke-virtual {p0}, Lafh;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Called next at end of playlist"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_0
    invoke-virtual {p0}, Lafh;->E()V

    .line 384
    return-void
.end method

.method public final s()V
    .locals 2

    .prologue
    .line 388
    invoke-direct {p0}, Lafh;->a()V

    .line 389
    invoke-virtual {p0}, Lafh;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Called previous at start of playlist"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_0
    invoke-virtual {p0}, Lafh;->F()V

    .line 393
    return-void
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 397
    invoke-direct {p0}, Lafh;->a()V

    .line 398
    invoke-virtual {p0}, Lafh;->G()I

    move-result v0

    return v0
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 403
    invoke-direct {p0}, Lafh;->a()V

    .line 404
    invoke-virtual {p0}, Lafh;->H()I

    move-result v0

    return v0
.end method

.method public final v()V
    .locals 0

    .prologue
    .line 433
    invoke-direct {p0}, Lafh;->a()V

    .line 434
    invoke-virtual {p0}, Lafh;->J()V

    .line 435
    return-void
.end method

.method public final w()Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 457
    invoke-virtual {p0}, Lafh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 458
    const/4 v0, 0x0

    .line 465
    :goto_0
    return-object v0

    .line 461
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 462
    const-string v1, "playerStyle"

    iget-object v2, p0, Lafh;->t:Lgwi;

    invoke-virtual {v2}, Lgwi;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v1, "fullscreenHelperState"

    iget-object v2, p0, Lafh;->l:Laer;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "isFullscreen"

    iget-boolean v5, v2, Laer;->g:Z

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "defaultRequestedOrientation"

    iget v5, v2, Laer;->i:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "controlFlags"

    invoke-virtual {v2}, Laer;->c()I

    move-result v2

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 464
    const-string v1, "apiPlayerState"

    invoke-virtual {p0}, Lafh;->x()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method public abstract x()[B
.end method

.method public abstract y()V
.end method

.method public abstract z()V
.end method
