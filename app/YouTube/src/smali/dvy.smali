.class final Ldvy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Leuc;

.field private synthetic b:Ldvs;


# direct methods
.method constructor <init>(Ldvs;Leuc;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Ldvy;->b:Ldvs;

    iput-object p2, p0, Ldvy;->a:Leuc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 351
    iget-object v0, p0, Ldvy;->b:Ldvs;

    invoke-static {v0}, Ldvs;->b(Ldvs;)Landroid/util/Pair;

    move-result-object v3

    .line 352
    invoke-static {}, Ldvs;->a()Landroid/util/Pair;

    move-result-object v0

    if-ne v3, v0, :cond_1

    .line 353
    iget-object v0, p0, Ldvy;->a:Leuc;

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Authentication failed."

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v7, v1}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 372
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v0, p0, Ldvy;->b:Ldvs;

    .line 357
    invoke-static {v0}, Ldvs;->c(Ldvs;)Ldrc;

    move-result-object v2

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 359
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string v1, "SIGNED_OUT_USER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 362
    iget-object v0, p0, Ldvy;->b:Ldvs;

    invoke-static {v0}, Ldvs;->c(Ldvs;)Ldrc;

    move-result-object v0

    const-string v1, "SIGNED_OUT_USER"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 365
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    .line 366
    iget-object v1, p0, Ldvy;->b:Ldvs;

    invoke-static {v1}, Ldvs;->c(Ldvs;)Ldrc;

    move-result-object v6

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-interface {v6, v1, v2, v0}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldst;)V

    goto :goto_1

    .line 369
    :cond_2
    iget-object v0, p0, Ldvy;->a:Leuc;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Ldvy;->a:Leuc;

    invoke-interface {v0, v7, v4}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
