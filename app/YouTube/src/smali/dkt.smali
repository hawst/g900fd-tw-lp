.class public final Ldkt;
.super Ldkh;
.source "SourceFile"


# instance fields
.field final l:Ljava/util/concurrent/Executor;

.field m:Lgky;

.field private final t:Ldaw;


# direct methods
.method public constructor <init>(Lcvq;Levn;Lcwq;Ljava/util/concurrent/Executor;Ldaq;Ldaw;Leyt;Lfac;Ldll;)V
    .locals 9

    .prologue
    .line 110
    move-object/from16 v0, p9

    iget-object v8, v0, Ldll;->b:Ldlh;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v8}, Ldkh;-><init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Ldlh;)V

    .line 118
    move-object/from16 v0, p9

    iget-object v1, v0, Ldll;->a:Lfnx;

    iput-object v1, p0, Ldkt;->h:Lfnx;

    .line 119
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldaw;

    iput-object v1, p0, Ldkt;->t:Ldaw;

    .line 120
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Ldkt;->l:Ljava/util/concurrent/Executor;

    .line 121
    iget-object v1, p0, Ldkt;->h:Lfnx;

    if-eqz v1, :cond_0

    .line 122
    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {p0, v1}, Ldkt;->a(Lgok;)V

    .line 124
    :cond_0
    invoke-virtual {p0}, Ldkt;->h()V

    .line 125
    return-void
.end method

.method public constructor <init>(Lcvq;Levn;Lcwq;Ljava/util/concurrent/Executor;Ldaq;Ldaw;Leyt;Lfac;Ljava/util/List;I[BLjava/lang/String;)V
    .locals 11

    .prologue
    .line 81
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p5

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move/from16 v8, p10

    move-object/from16 v9, p11

    move-object/from16 v10, p12

    invoke-direct/range {v0 .. v10}, Ldkh;-><init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Ljava/util/List;I[BLjava/lang/String;)V

    .line 92
    invoke-static/range {p6 .. p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaw;

    iput-object v0, p0, Ldkt;->t:Ldaw;

    .line 93
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Ldkt;->l:Ljava/util/concurrent/Executor;

    .line 94
    invoke-virtual {p0}, Ldkt;->h()V

    .line 95
    return-void
.end method

.method static synthetic a(Ldkt;Landroid/os/Handler;Lgky;)V
    .locals 2

    .prologue
    .line 34
    :try_start_0
    invoke-virtual {p2}, Lgky;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnx;

    new-instance v1, Ldkv;

    invoke-direct {v1, p0, v0}, Ldkv;-><init>(Ldkt;Lfnx;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ldky;

    invoke-direct {v1, p0, v0}, Ldky;-><init>(Ldkt;Ljava/lang/Exception;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ldky;

    invoke-direct {v1, p0, v0}, Ldky;-><init>(Ldkt;Ljava/lang/Exception;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Ldkt;->q:Leue;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Ldkt;->q:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Ldkt;->q:Leue;

    .line 173
    :cond_0
    iget-object v0, p0, Ldkt;->m:Lgky;

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Ldkt;->m:Lgky;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgky;->cancel(Z)Z

    .line 176
    :cond_1
    iget-object v0, p0, Ldkt;->h:Lfnx;

    if-eqz v0, :cond_3

    .line 177
    sget-object v0, Lgok;->e:Lgok;

    iput-object v0, p0, Ldkt;->f:Lgok;

    .line 185
    :cond_2
    :goto_0
    return-void

    .line 178
    :cond_3
    iget-object v0, p0, Ldkt;->g:Lfrl;

    if-eqz v0, :cond_4

    .line 179
    sget-object v0, Lgok;->d:Lgok;

    iput-object v0, p0, Ldkt;->f:Lgok;

    goto :goto_0

    .line 181
    :cond_4
    iget-object v0, p0, Ldkt;->f:Lgok;

    sget-object v1, Lgok;->a:Lgok;

    if-eq v0, v1, :cond_2

    .line 182
    sget-object v0, Lgok;->a:Lgok;

    invoke-virtual {p0, v0}, Ldkt;->a(Lgok;)V

    goto :goto_0
.end method

.method protected final c(I)V
    .locals 9

    .prologue
    const/4 v5, -0x1

    .line 194
    iget-object v0, p0, Ldkt;->n:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ldkt;->s:I

    .line 195
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ldkt;->s:I

    .line 196
    iget-object v0, p0, Ldkt;->n:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    if-ltz p1, :cond_1

    .line 197
    iget-object v0, p0, Ldkt;->c:Levn;

    new-instance v1, Lczr;

    invoke-direct {v1}, Lczr;-><init>()V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 198
    new-instance v0, Ldkw;

    invoke-direct {v0, p0}, Ldkw;-><init>(Ldkt;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldkt;->q:Leue;

    .line 200
    iget-object v0, p0, Ldkt;->a:Ldaq;

    iget-object v1, p0, Ldkt;->n:[Ljava/lang/String;

    aget-object v1, v1, p1

    iget-object v2, p0, Ldkt;->o:[B

    iget-object v3, p0, Ldkt;->p:Ljava/lang/String;

    const-string v4, ""

    iget-object v7, p0, Ldkt;->q:Leue;

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    .line 208
    invoke-static {}, Lgky;->a()Lgky;

    move-result-object v0

    iput-object v0, p0, Ldkt;->m:Lgky;

    .line 209
    iget-object v2, p0, Ldkt;->t:Ldaw;

    iget-object v0, p0, Ldkt;->n:[Ljava/lang/String;

    aget-object v3, v0, p1

    const-string v4, ""

    const-string v6, ""

    iget-object v7, p0, Ldkt;->o:[B

    iget-object v8, p0, Ldkt;->m:Lgky;

    invoke-virtual/range {v2 .. v8}, Ldaw;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLwv;)V

    .line 216
    sget-object v0, Lgok;->b:Lgok;

    invoke-virtual {p0, v0}, Ldkt;->a(Lgok;)V

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-object v0, p0, Ldkt;->n:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 218
    iget v0, p0, Ldkt;->s:I

    iput v0, p0, Ldkt;->r:I

    .line 219
    sget-object v0, Lgok;->f:Lgok;

    invoke-virtual {p0, v0}, Ldkt;->a(Lgok;)V

    goto :goto_0
.end method

.method public final f()Ldkg;
    .locals 3

    .prologue
    .line 129
    new-instance v1, Ldll;

    iget-object v2, p0, Ldkt;->h:Lfnx;

    .line 131
    invoke-super {p0}, Ldkh;->f()Ldkg;

    move-result-object v0

    check-cast v0, Ldlh;

    invoke-direct {v1, v2, v0}, Ldll;-><init>(Lfnx;Ldlh;)V

    return-object v1
.end method

.method protected final m()Z
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public final s_()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 137
    iget-object v1, p0, Ldkt;->f:Lgok;

    sget-object v2, Lgok;->d:Lgok;

    if-ne v1, v2, :cond_1

    .line 138
    iget-object v1, p0, Ldkt;->c:Levn;

    new-instance v2, Lczr;

    invoke-direct {v2}, Lczr;-><init>()V

    invoke-virtual {v1, v2}, Levn;->c(Ljava/lang/Object;)V

    .line 139
    iget-object v1, p0, Ldkt;->m:Lgky;

    if-eqz v1, :cond_0

    .line 140
    iget-object v1, p0, Ldkt;->m:Lgky;

    invoke-virtual {v1, v0}, Lgky;->cancel(Z)Z

    .line 142
    :cond_0
    invoke-static {}, Lgky;->a()Lgky;

    move-result-object v0

    iput-object v0, p0, Ldkt;->m:Lgky;

    .line 143
    iget-object v0, p0, Ldkt;->t:Ldaw;

    iget-object v1, p0, Ldkt;->n:[Ljava/lang/String;

    iget v2, p0, Ldkt;->r:I

    aget-object v1, v1, v2

    const-string v2, ""

    const/4 v3, -0x1

    const-string v4, ""

    iget-object v5, p0, Ldkt;->o:[B

    iget-object v6, p0, Ldkt;->m:Lgky;

    invoke-virtual/range {v0 .. v6}, Ldaw;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLwv;)V

    .line 151
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 152
    iget-object v1, p0, Ldkt;->l:Ljava/util/concurrent/Executor;

    new-instance v2, Ldku;

    invoke-direct {v2, p0, v0}, Ldku;-><init>(Ldkt;Landroid/os/Handler;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 161
    const/4 v0, 0x1

    .line 163
    :cond_1
    return v0
.end method
