.class public final Lagk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbn;


# instance fields
.field public a:Land;


# direct methods
.method public constructor <init>(Land;)V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const-string v0, "service cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Land;

    iput-object v0, p0, Lagk;->a:Land;

    .line 114
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lagk;->a:Land;

    if-eqz v0, :cond_0

    .line 124
    :try_start_0
    iget-object v0, p0, Lagk;->a:Land;

    invoke-interface {v0}, Land;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :cond_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 127
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lagk;->a:Land;

    if-eqz v0, :cond_0

    .line 148
    :try_start_0
    iget-object v0, p0, Lagk;->a:Land;

    invoke-interface {v0, p1, p2}, Land;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :cond_0
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 151
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lagk;->a:Land;

    if-eqz v0, :cond_0

    .line 136
    :try_start_0
    iget-object v0, p0, Lagk;->a:Land;

    invoke-interface {v0}, Land;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :cond_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    .line 139
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lagk;->a:Land;

    if-eqz v0, :cond_0

    .line 160
    :try_start_0
    iget-object v0, p0, Lagk;->a:Land;

    invoke-interface {v0}, Land;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :cond_0
    return-void

    .line 161
    :catch_0
    move-exception v0

    .line 163
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
