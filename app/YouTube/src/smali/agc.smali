.class public final Lagc;
.super Laio;
.source "SourceFile"


# instance fields
.field public a:Lagk;

.field private final b:Ldbm;

.field private final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ldbm;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Laio;-><init>()V

    .line 25
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbm;

    iput-object v0, p0, Lagc;->b:Ldbm;

    .line 26
    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lagc;->c:Landroid/os/Handler;

    .line 27
    return-void
.end method

.method static synthetic a(Lagc;)Lagk;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lagc;->a:Lagk;

    return-object v0
.end method

.method static synthetic a(Lagc;Lagk;)Lagk;
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lagc;->a:Lagk;

    return-object p1
.end method

.method static synthetic b(Lagc;)Ldbm;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lagc;->b:Ldbm;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lagc;->c:Landroid/os/Handler;

    new-instance v1, Lage;

    invoke-direct {v1, p0}, Lage;-><init>(Lagc;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 55
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lagc;->c:Landroid/os/Handler;

    new-instance v1, Lagi;

    invoke-direct {v1, p0, p1, p2}, Lagi;-><init>(Lagc;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 96
    return-void
.end method

.method public final a(Land;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lagc;->c:Landroid/os/Handler;

    new-instance v1, Lagd;

    invoke-direct {v1, p0, p1}, Lagd;-><init>(Lagc;Land;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 45
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lagc;->c:Landroid/os/Handler;

    new-instance v1, Lagj;

    invoke-direct {v1, p0, p1}, Lagj;-><init>(Lagc;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 106
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lagc;->c:Landroid/os/Handler;

    new-instance v1, Lagg;

    invoke-direct {v1, p0, p1}, Lagg;-><init>(Lagc;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 76
    return-void
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 7

    .prologue
    .line 60
    iget-object v6, p0, Lagc;->c:Landroid/os/Handler;

    new-instance v0, Lagf;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lagf;-><init>(Lagc;Ljava/lang/String;ZZLjava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 66
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lagc;->c:Landroid/os/Handler;

    new-instance v1, Lagh;

    invoke-direct {v1, p0, p1}, Lagh;-><init>(Lagc;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 86
    return-void
.end method
