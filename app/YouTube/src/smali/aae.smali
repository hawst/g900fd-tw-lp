.class public final Laae;
.super Ljava/util/AbstractList;
.source "SourceFile"


# instance fields
.field private a:[J

.field private b:[J

.field private c:Lye;

.field private d:Ljava/util/HashMap;

.field private e:Ljava/util/HashMap;

.field private f:[Laad;


# direct methods
.method public constructor <init>(Lzr;)V
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laae;->d:Ljava/util/HashMap;

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laae;->e:Ljava/util/HashMap;

    .line 38
    invoke-virtual {p1}, Lzr;->b()Lye;

    move-result-object v0

    invoke-direct {p0, v0}, Laae;->a(Lye;)V

    .line 42
    invoke-virtual {p1}, Lzr;->h()Lzi;

    move-result-object v0

    invoke-virtual {v0}, Lzi;->h()Lzh;

    move-result-object v1

    .line 43
    invoke-virtual {p1}, Lzr;->h()Lzi;

    move-result-object v0

    invoke-virtual {v0}, Lzi;->j()Lyk;

    move-result-object v0

    .line 44
    invoke-virtual {p1}, Lzr;->h()Lzi;

    move-result-object v3

    invoke-virtual {v3}, Lzi;->i()Lzj;

    move-result-object v3

    .line 47
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lyk;->c()[J

    move-result-object v0

    move-object v7, v0

    .line 48
    :goto_0
    if-eqz v3, :cond_3

    iget-object v0, v3, Lzj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    array-length v0, v7

    if-lez v0, :cond_3

    if-eqz v1, :cond_3

    .line 49
    invoke-virtual {v1}, Lzh;->f()J

    move-result-wide v4

    cmp-long v0, v4, v10

    if-lez v0, :cond_3

    .line 50
    array-length v0, v7

    invoke-virtual {v3, v0}, Lzj;->a(I)[J

    move-result-object v8

    .line 54
    iget-wide v4, v1, Lzh;->a:J

    cmp-long v0, v4, v10

    if-lez v0, :cond_1

    .line 55
    invoke-virtual {v1}, Lzh;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, La;->d(J)I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Laae;->b:[J

    .line 56
    iget-object v0, p0, Laae;->b:[J

    iget-wide v4, v1, Lzh;->a:J

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 60
    :goto_1
    iget-object v0, p0, Laae;->b:[J

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Laae;->a:[J

    move v0, v2

    move v1, v2

    .line 62
    :goto_2
    array-length v3, v8

    if-ge v0, v3, :cond_3

    .line 63
    aget-wide v10, v8, v0

    .line 64
    aget-wide v4, v7, v0

    move v3, v2

    .line 65
    :goto_3
    int-to-long v12, v3

    cmp-long v6, v12, v10

    if-gez v6, :cond_2

    .line 66
    iget-object v6, p0, Laae;->b:[J

    aget-wide v12, v6, v1

    .line 67
    iget-object v6, p0, Laae;->a:[J

    aput-wide v4, v6, v1

    .line 68
    add-long/2addr v4, v12

    .line 69
    add-int/lit8 v6, v1, 0x1

    .line 65
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v6

    goto :goto_3

    .line 47
    :cond_0
    new-array v0, v2, [J

    move-object v7, v0

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, v1, Lzh;->b:[J

    iput-object v0, p0, Laae;->b:[J

    goto :goto_1

    .line 62
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 77
    :cond_3
    iget-object v0, p1, Likt;->m:Lyn;

    const-class v1, Lzv;

    invoke-interface {v0, v1}, Lyn;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 79
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_8

    .line 80
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 81
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzv;

    const-class v1, Lzy;

    invoke-virtual {v0, v1}, Lzv;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 82
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lzy;

    .line 83
    iget-wide v6, v1, Lzy;->a:J

    invoke-virtual {p1}, Lzr;->g()Lzs;

    move-result-object v0

    iget-wide v8, v0, Lzs;->c:J

    cmp-long v0, v6, v8

    if-nez v0, :cond_4

    .line 84
    invoke-virtual {p1}, Lzr;->b()Lye;

    move-result-object v0

    const-class v5, Lzw;

    invoke-virtual {v0, v5}, Lye;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzw;

    .line 85
    invoke-virtual {p1}, Lzr;->g()Lzs;

    move-result-object v6

    iget-wide v6, v6, Lzs;->c:J

    invoke-static {v0, v6, v7, v1}, Laae;->a(Lzw;JLzy;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_4

    .line 90
    :cond_5
    iget-object v0, p0, Laae;->b:[J

    if-eqz v0, :cond_6

    iget-object v0, p0, Laae;->a:[J

    if-nez v0, :cond_7

    .line 91
    :cond_6
    new-array v0, v2, [J

    iput-object v0, p0, Laae;->b:[J

    .line 92
    new-array v0, v2, [J

    iput-object v0, p0, Laae;->a:[J

    .line 95
    :cond_7
    invoke-direct {p0, v3}, Laae;->a(Ljava/util/Map;)V

    .line 99
    :cond_8
    return-void
.end method

.method private static a(Lzw;JLzy;)Ljava/util/Map;
    .locals 21

    .prologue
    .line 184
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 185
    const-class v2, Lzz;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lzw;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 186
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzz;

    .line 187
    invoke-virtual {v2}, Lzz;->g()Laaa;

    move-result-object v3

    iget-wide v4, v3, Laaa;->a:J

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 189
    invoke-virtual {v2}, Lzz;->g()Laaa;

    move-result-object v3

    iget v3, v3, Likv;->p:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_3

    .line 190
    invoke-virtual {v2}, Lzz;->g()Laaa;

    move-result-object v3

    iget-wide v4, v3, Laaa;->b:J

    .line 195
    :goto_1
    const-class v3, Laab;

    invoke-virtual {v2, v3}, Lzz;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Laab;

    .line 196
    iget v2, v3, Laab;->a:I

    int-to-long v6, v2

    add-long v12, v4, v6

    .line 197
    iget-object v2, v3, Likt;->m:Lyn;

    check-cast v2, Lzz;

    invoke-virtual {v2}, Lzz;->g()Laaa;

    move-result-object v11

    .line 199
    const-wide/16 v6, 0x0

    .line 200
    iget-object v2, v3, Laab;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laac;

    .line 202
    invoke-virtual {v3}, Laab;->f()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 203
    iget-wide v0, v2, Laac;->b:J

    move-wide/from16 v16, v0

    .line 204
    add-long v18, v6, v12

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v8, v2, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    add-long v6, v6, v16

    goto :goto_2

    .line 189
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 192
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lzw;->g()J

    move-result-wide v4

    goto :goto_1

    .line 207
    :cond_4
    iget v2, v11, Likv;->p:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_6

    .line 208
    iget-wide v0, v11, Laaa;->d:J

    move-wide/from16 v16, v0

    .line 209
    add-long v18, v6, v12

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v8, v2, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    add-long v6, v6, v16

    goto :goto_2

    .line 207
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 212
    :cond_6
    if-nez p3, :cond_7

    .line 213
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "File doesn\'t contain trex box but track fragments aren\'t fully self contained. Cannot determine sample size."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 215
    :cond_7
    move-object/from16 v0, p3

    iget-wide v0, v0, Lzy;->c:J

    move-wide/from16 v16, v0

    .line 216
    add-long v18, v6, v12

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v8, v2, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    add-long v6, v6, v16

    .line 220
    goto :goto_2

    .line 224
    :cond_8
    return-object v8
.end method

.method private a(Ljava/util/Map;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 103
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 105
    iget-object v1, p0, Laae;->b:[J

    array-length v1, v1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v1, v3

    new-array v3, v1, [J

    .line 106
    iget-object v1, p0, Laae;->b:[J

    iget-object v4, p0, Laae;->b:[J

    array-length v4, v4

    invoke-static {v1, v0, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 107
    iget-object v1, p0, Laae;->a:[J

    array-length v1, v1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v1, v4

    new-array v4, v1, [J

    .line 108
    iget-object v1, p0, Laae;->a:[J

    iget-object v5, p0, Laae;->a:[J

    array-length v5, v5

    invoke-static {v1, v0, v4, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v1, v0

    .line 109
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 110
    iget-object v0, p0, Laae;->a:[J

    array-length v0, v0

    add-int v5, v1, v0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v4, v5

    .line 111
    iget-object v0, p0, Laae;->b:[J

    array-length v0, v0

    add-int v5, v1, v0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v5

    .line 109
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 113
    :cond_0
    iput-object v3, p0, Laae;->b:[J

    .line 114
    iput-object v4, p0, Laae;->a:[J

    .line 115
    return-void
.end method

.method private a(Lye;)V
    .locals 12

    .prologue
    .line 138
    iput-object p1, p0, Laae;->c:Lye;

    .line 140
    const-wide/16 v0, 0x0

    .line 141
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 142
    iget-object v2, p0, Laae;->c:Lye;

    iget-object v2, v2, Liku;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 143
    invoke-interface {v0}, Lyj;->l_()J

    move-result-wide v6

    .line 144
    const-string v1, "mdat"

    invoke-interface {v0}, Lyj;->m_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    instance-of v1, v0, Laad;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 146
    check-cast v1, Laad;

    iget-object v1, v1, Laad;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    int-to-long v8, v1

    add-long/2addr v8, v2

    .line 147
    iget-object v10, p0, Laae;->d:Ljava/util/HashMap;

    move-object v1, v0

    check-cast v1, Laad;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v1, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v10, p0, Laae;->e:Ljava/util/HashMap;

    move-object v1, v0

    check-cast v1, Laad;

    add-long/2addr v8, v6

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v10, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    check-cast v0, Laad;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 154
    :cond_0
    add-long v0, v2, v6

    move-wide v2, v0

    .line 155
    goto :goto_0

    .line 151
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Sample need to be in mdats and mdats need to be instanceof MediaDataBox"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_2
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v0

    new-array v0, v0, [Laad;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laad;

    iput-object v0, p0, Laae;->f:[Laad;

    .line 157
    return-void
.end method


# virtual methods
.method public final synthetic get(I)Ljava/lang/Object;
    .locals 14

    .prologue
    .line 15
    iget-object v0, p0, Laae;->a:[J

    aget-wide v2, v0, p1

    iget-object v0, p0, Laae;->b:[J

    aget-wide v0, v0, p1

    invoke-static {v0, v1}, La;->d(J)I

    move-result v4

    iget-object v5, p0, Laae;->f:[Laad;

    array-length v6, v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_1

    aget-object v7, v5, v1

    iget-object v0, p0, Laae;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v0, p0, Laae;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v0, v8, v2

    if-gtz v0, :cond_0

    int-to-long v12, v4

    add-long/2addr v12, v2

    cmp-long v0, v12, v10

    if-gtz v0, :cond_0

    sub-long v0, v2, v8

    invoke-virtual {v7, v0, v1, v4}, Laad;->a(JI)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v5, 0x5e

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "The sample with offset "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is NOT located within an mdat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Laae;->b:[J

    array-length v0, v0

    return v0
.end method
