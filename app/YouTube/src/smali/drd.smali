.class public final Ldrd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldrc;


# instance fields
.field private final a:Ldrc;

.field private final b:Ldrc;


# direct methods
.method public constructor <init>(Ldrc;Ldrc;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Ldrd;->a:Ldrc;

    .line 25
    iput-object p2, p0, Ldrd;->b:Ldrc;

    .line 26
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 73
    const-string v0, "SIGNED_OUT_USER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ldth;)Ldst;
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Ldrd;->a:Ldrc;

    invoke-interface {v0, p1, p2, p3}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldth;)Ldst;

    move-result-object v1

    .line 65
    const/4 v0, 0x0

    .line 66
    invoke-static {p1}, Ldrd;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    iget-object v0, p0, Ldrd;->b:Ldrc;

    invoke-interface {v0, p1, p2, p3}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldth;)Ldst;

    move-result-object v0

    .line 69
    :cond_0
    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldth;Ljava/lang/String;)Ldst;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Ldrd;->a:Ldrc;

    invoke-interface {v0, p1, p2, p3, p4}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldth;Ljava/lang/String;)Ldst;

    move-result-object v1

    .line 55
    const/4 v0, 0x0

    .line 56
    invoke-static {p1}, Ldrd;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    iget-object v0, p0, Ldrd;->b:Ldrc;

    invoke-interface {v0, p1, p2, p3, p4}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldth;Ljava/lang/String;)Ldst;

    move-result-object v0

    .line 59
    :cond_0
    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .prologue
    .line 30
    iget-object v0, p0, Ldrd;->a:Ldrc;

    invoke-interface {v0, p1, p2}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 31
    const/4 v0, 0x0

    .line 32
    invoke-static {p1}, Ldrd;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    iget-object v0, p0, Ldrd;->b:Ldrc;

    invoke-interface {v0, p1, p2}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 35
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    iget-object v5, v0, Ldst;->b:Ldth;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    iget-object v4, v0, Ldst;->b:Ldth;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-object v2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldst;)V
    .locals 2

    .prologue
    .line 41
    .line 42
    invoke-virtual {p0, p1, p2}, Ldrd;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p3, Ldst;->b:Ldth;

    invoke-static {v0, v1}, La;->a(Ljava/util/List;Ldth;)Ldst;

    move-result-object v0

    .line 43
    if-eqz v0, :cond_0

    .line 44
    iget-object v0, v0, Ldst;->b:Ldth;

    invoke-virtual {p0, p1, p2, v0}, Ldrd;->a(Ljava/lang/String;Ljava/lang/String;Ldth;)Ldst;

    .line 46
    :cond_0
    iget-object v0, p0, Ldrd;->a:Ldrc;

    invoke-interface {v0, p1, p2, p3}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldst;)V

    .line 47
    invoke-static {p1}, Ldrd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Ldrd;->b:Ldrc;

    invoke-interface {v0, p1, p2, p3}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldst;)V

    .line 50
    :cond_1
    return-void
.end method
