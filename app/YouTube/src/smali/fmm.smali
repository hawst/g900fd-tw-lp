.class public final Lfmm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lhhf;

.field private b:Ljava/util/List;

.field private c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lhhf;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lfmm;->a:Lhhf;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 5

    .prologue
    .line 34
    iget-object v0, p0, Lfmm;->b:Ljava/util/List;

    if-nez v0, :cond_3

    .line 35
    iget-object v0, p0, Lfmm;->a:Lhhf;

    iget-object v0, v0, Lhhf;->a:[Lhva;

    if-eqz v0, :cond_2

    .line 36
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfmm;->b:Ljava/util/List;

    .line 37
    iget-object v0, p0, Lfmm;->a:Lhhf;

    iget-object v1, v0, Lhhf;->a:[Lhva;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 38
    iget-object v4, v3, Lhva;->b:Lhpj;

    if-eqz v4, :cond_1

    .line 39
    iget-object v3, p0, Lfmm;->b:Ljava/util/List;

    new-instance v4, Lflf;

    invoke-direct {v4}, Lflf;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_1
    iget-object v3, v3, Lhva;->c:Lhbe;

    if-eqz v3, :cond_0

    .line 41
    iget-object v3, p0, Lfmm;->b:Ljava/util/List;

    new-instance v4, Lfir;

    invoke-direct {v4}, Lfir;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 45
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfmm;->b:Ljava/util/List;

    .line 48
    :cond_3
    iget-object v0, p0, Lfmm;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 5

    .prologue
    .line 52
    iget-object v0, p0, Lfmm;->c:Ljava/util/List;

    if-nez v0, :cond_3

    .line 53
    iget-object v0, p0, Lfmm;->a:Lhhf;

    iget-object v0, v0, Lhhf;->b:[Lhpk;

    if-eqz v0, :cond_2

    .line 54
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfmm;->c:Ljava/util/List;

    .line 56
    iget-object v0, p0, Lfmm;->a:Lhhf;

    iget-object v1, v0, Lhhf;->b:[Lhpk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 57
    iget-object v4, v3, Lhpk;->b:Lhpj;

    if-eqz v4, :cond_1

    .line 58
    iget-object v3, p0, Lfmm;->c:Ljava/util/List;

    new-instance v4, Lflf;

    invoke-direct {v4}, Lflf;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_1
    iget-object v3, v3, Lhpk;->c:Lhbe;

    if-eqz v3, :cond_0

    .line 60
    iget-object v3, p0, Lfmm;->c:Ljava/util/List;

    new-instance v4, Lfir;

    invoke-direct {v4}, Lfir;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 64
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfmm;->c:Ljava/util/List;

    .line 67
    :cond_3
    iget-object v0, p0, Lfmm;->c:Ljava/util/List;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 88
    instance-of v1, p1, Lfmm;

    if-eqz v1, :cond_0

    .line 89
    check-cast p1, Lfmm;

    .line 90
    invoke-virtual {p0}, Lfmm;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 91
    invoke-virtual {p0}, Lfmm;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 92
    invoke-virtual {p1}, Lfmm;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v1, v3, :cond_0

    .line 93
    invoke-virtual {p1}, Lfmm;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v2, v1, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v0

    .line 97
    :cond_1
    invoke-virtual {p0}, Lfmm;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lfmm;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {p0}, Lfmm;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lfmm;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
