.class final Lgmo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/concurrent/Executor;

.field volatile b:Lgnm;

.field volatile c:Z

.field private synthetic d:Lgml;


# direct methods
.method public constructor <init>(Lgml;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 1170
    iput-object p1, p0, Lgmo;->d:Lgml;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1171
    iput-object p2, p0, Lgmo;->a:Ljava/util/concurrent/Executor;

    .line 1172
    new-instance v0, Lgmp;

    invoke-direct {v0, p0, p1}, Lgmp;-><init>(Lgmo;Lgml;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 1178
    return-void
.end method


# virtual methods
.method declared-synchronized a()V
    .locals 11

    .prologue
    .line 1197
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgmo;->c:Z

    if-nez v0, :cond_6

    .line 1198
    new-instance v0, Lgnm;

    invoke-direct {v0}, Lgnm;-><init>()V

    iput-object v0, p0, Lgmo;->b:Lgnm;

    .line 1199
    iget-object v0, p0, Lgmo;->d:Lgml;

    iget-object v0, v0, Lgml;->a:Levi;

    invoke-interface {v0}, Levi;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "videos"

    const/4 v2, 0x0

    const-string v3, "media_status != ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, Lglv;->a:Lglv;

    iget v6, v6, Lglv;->j:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    :try_start_1
    new-instance v8, Lgms;

    iget-object v2, p0, Lgmo;->d:Lgml;

    iget-object v3, p0, Lgmo;->d:Lgml;

    iget-object v3, v3, Lgml;->b:Lgmt;

    invoke-direct {v8, v2, v1, v3}, Lgms;-><init>(Lgml;Landroid/database/Cursor;Lgmt;)V

    iget-object v9, p0, Lgmo;->b:Lgnm;

    :cond_0
    :goto_0
    iget-object v2, v8, Lgms;->a:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v8, Lgms;->b:Lgns;

    invoke-virtual {v2}, Lgns;->a()Lgcd;

    move-result-object v10

    invoke-virtual {v9, v10}, Lgnm;->a(Lgcd;)V

    iget-object v2, v8, Lgms;->a:Landroid/database/Cursor;

    iget v3, v8, Lgms;->c:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v8, Lgms;->a:Landroid/database/Cursor;

    iget v3, v8, Lgms;->c:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, Lfrl;->a([BJ)Lfrl;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v2, v10, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v9, v2}, Lgnm;->a(Ljava/lang/String;)Lgno;

    move-result-object v2

    iget-object v4, v8, Lgms;->a:Landroid/database/Cursor;

    iget v5, v8, Lgms;->d:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iget-object v6, v8, Lgms;->a:Landroid/database/Cursor;

    iget v7, v8, Lgms;->e:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Lgno;->a(Lfrl;JJ)V

    iget-object v3, v8, Lgms;->a:Landroid/database/Cursor;

    iget v4, v8, Lgms;->f:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v8, Lgms;->a:Landroid/database/Cursor;

    iget v4, v8, Lgms;->f:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lgno;->a(J)V

    :cond_1
    iget-object v3, v8, Lgms;->a:Landroid/database/Cursor;

    iget v4, v8, Lgms;->g:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v8, Lgms;->a:Landroid/database/Cursor;

    iget v4, v8, Lgms;->g:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lglv;->a(I)Lglv;

    move-result-object v3

    invoke-virtual {v2, v3}, Lgno;->a(Lglv;)V

    iget-object v3, v8, Lgms;->h:Lgml;

    iget-object v4, v10, Lgcd;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lgml;->a(Ljava/lang/String;Lgni;)Lgly;

    move-result-object v3

    invoke-virtual {v3}, Lgly;->d()J

    move-result-wide v4

    invoke-virtual {v3}, Lgly;->e()J

    move-result-wide v6

    invoke-virtual {v2, v4, v5, v6, v7}, Lgno;->a(JJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1197
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1199
    :cond_2
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v1, p0, Lgmo;->d:Lgml;

    iget-object v1, v1, Lgml;->f:Lgnp;

    invoke-virtual {v1}, Lgnp;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgcd;

    iget-object v3, p0, Lgmo;->b:Lgnm;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lgnm;->d(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lgmo;->d:Lgml;

    iget-object v1, v1, Lgml;->f:Lgnp;

    invoke-virtual {v1}, Lgnp;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbu;

    iget-object v3, p0, Lgmo;->b:Lgnm;

    invoke-virtual {v3, v1}, Lgnm;->a(Lgbu;)V

    goto :goto_2

    :cond_4
    const-string v1, "playlist_video"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "playlist_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "video_id"

    aput-object v4, v2, v3

    const-string v3, "playlist_id IS NOT NULL"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v1

    :try_start_4
    const-string v0, "playlist_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    const-string v2, "video_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lgmo;->b:Lgnm;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lgnm;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_3

    :catchall_2
    move-exception v0

    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgmo;->c:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1202
    :cond_6
    monitor-exit p0

    return-void
.end method

.method declared-synchronized b()V
    .locals 10

    .prologue
    .line 1207
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lgmo;->c:Z

    if-eqz v2, :cond_4

    .line 1213
    iget-object v2, p0, Lgmo;->d:Lgml;

    iget-object v2, v2, Lgml;->c:Lewi;

    invoke-interface {v2}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iget-object v3, p0, Lgmo;->b:Lgnm;

    iget-object v3, v3, Lgnm;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lgno;

    move-object v4, v0

    invoke-virtual {v4}, Lgno;->b()Lglv;

    move-result-object v3

    sget-object v5, Lglv;->b:Lglv;

    if-ne v3, v5, :cond_0

    iget-object v3, p0, Lgmo;->d:Lgml;

    iget-object v3, v3, Lgml;->e:Lgnj;

    iget-object v5, v4, Lgno;->a:Lgcd;

    iget-object v5, v5, Lgcd;->b:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v7}, Lgnj;->a(Ljava/lang/String;Lgni;)Lgly;

    move-result-object v5

    iget-object v3, v5, Lgly;->a:Lglx;

    if-nez v3, :cond_1

    iget-object v3, v5, Lgly;->b:Lglx;

    :cond_1
    iget-object v5, v4, Lgno;->a:Lgcd;

    iget-object v5, v5, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v3}, Lglx;->a()I

    move-result v7

    iget-object v3, v3, Lglx;->a:Lfqj;

    iget-object v3, v3, Lfqj;->a:Lhgy;

    iget-wide v8, v3, Lhgy;->j:J

    invoke-static {v5, v7, v8, v9}, La;->a(Ljava/lang/String;IJ)Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Legc;

    if-eqz v3, :cond_2

    invoke-interface {v3}, Legc;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v4, v3}, Lgno;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1207
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1213
    :cond_3
    :try_start_1
    iget-object v2, p0, Lgmo;->d:Lgml;

    iget-object v2, v2, Lgml;->d:Lgmm;

    invoke-interface {v2}, Lgmm;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1215
    :cond_4
    monitor-exit p0

    return-void

    :cond_5
    move v3, v5

    goto :goto_1
.end method
