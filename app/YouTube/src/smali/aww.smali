.class public final Laww;
.super Lawa;
.source "SourceFile"


# instance fields
.field final a:Lawt;

.field private final b:Lawo;


# direct methods
.method public constructor <init>(Lawo;Lawt;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lawa;-><init>()V

    .line 27
    const-string v0, "videoRendererFactory cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawo;

    iput-object v0, p0, Laww;->b:Lawo;

    .line 29
    const-string v0, "transferRendererFactory cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawt;

    iput-object v0, p0, Laww;->a:Lawt;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lawm;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lawx;

    iget-object v1, p0, Laww;->b:Lawo;

    invoke-interface {v1, p1, p2}, Lawo;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lawm;

    move-result-object v1

    iget-object v2, p0, Laww;->a:Lawt;

    .line 36
    invoke-virtual {v2, p1, p2}, Lawt;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lawm;

    move-result-object v2

    invoke-direct {v0, p0, p1, v1, v2}, Lawx;-><init>(Laww;Landroid/view/View;Lawm;Lawm;)V

    return-object v0
.end method
