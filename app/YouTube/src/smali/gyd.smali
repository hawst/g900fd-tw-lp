.class public final Lgyd;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1827
    invoke-direct {p0}, Lidf;-><init>()V

    .line 1835
    const/4 v0, 0x0

    iput v0, p0, Lgyd;->a:I

    .line 1827
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1872
    const/4 v0, 0x0

    .line 1873
    iget v1, p0, Lgyd;->a:I

    if-eqz v1, :cond_0

    .line 1874
    const/4 v0, 0x1

    iget v1, p0, Lgyd;->a:I

    .line 1875
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1877
    :cond_0
    iget-object v1, p0, Lgyd;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1878
    iput v0, p0, Lgyd;->J:I

    .line 1879
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 1823
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyd;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyd;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyd;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lgyd;->a:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lgyd;->a:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 1863
    iget v0, p0, Lgyd;->a:I

    if-eqz v0, :cond_0

    .line 1864
    const/4 v0, 0x1

    iget v1, p0, Lgyd;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 1866
    :cond_0
    iget-object v0, p0, Lgyd;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 1868
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1846
    if-ne p1, p0, :cond_1

    .line 1850
    :cond_0
    :goto_0
    return v0

    .line 1847
    :cond_1
    instance-of v2, p1, Lgyd;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1848
    :cond_2
    check-cast p1, Lgyd;

    .line 1849
    iget v2, p0, Lgyd;->a:I

    iget v3, p1, Lgyd;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyd;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyd;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 1850
    goto :goto_0

    .line 1849
    :cond_4
    iget-object v2, p0, Lgyd;->I:Ljava/util/List;

    iget-object v3, p1, Lgyd;->I:Ljava/util/List;

    .line 1850
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1854
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1856
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lgyd;->a:I

    add-int/2addr v0, v1

    .line 1857
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lgyd;->I:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1858
    return v0

    .line 1857
    :cond_0
    iget-object v0, p0, Lgyd;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_0
.end method
