.class public final Lcei;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfry;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lfsj;

.field private final c:Lfsl;

.field private final d:Landroid/widget/LinearLayout;

.field private final e:Ljava/util/LinkedList;

.field private final f:Ljava/util/LinkedList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Lfsl;)V
    .locals 4

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcei;->a:Landroid/content/Context;

    .line 45
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcei;->b:Lfsj;

    .line 46
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsl;

    iput-object v0, p0, Lcei;->c:Lfsl;

    .line 48
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    .line 49
    iget-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 50
    iget-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 54
    iget-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 55
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcei;->e:Ljava/util/LinkedList;

    .line 56
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcei;->f:Ljava/util/LinkedList;

    .line 58
    iget-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 59
    return-void
.end method

.method private static a(Landroid/view/View;Lfsg;)V
    .locals 1

    .prologue
    .line 167
    const v0, 0x7f080018

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 168
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 30
    check-cast p2, Lfui;

    iget-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget v6, p2, Lfui;->a:I

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_4

    iget-object v0, p0, Lcei;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    if-nez v0, :cond_5

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcei;->a:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v4, v2, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v5, v0

    :goto_1
    if-ltz v1, :cond_2

    iget-object v0, p2, Lfui;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p2, Lfui;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    :goto_2
    if-eqz v4, :cond_1

    iget-object v0, p0, Lcei;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsg;

    if-nez v0, :cond_0

    new-instance v0, Lfsg;

    invoke-direct {v0, p1}, Lfsg;-><init>(Lfsg;)V

    :cond_0
    iget v7, p1, Lfsg;->c:I

    div-int/2addr v7, v6

    iput v7, v0, Lfsg;->c:I

    iput-boolean v2, v0, Lfsg;->d:Z

    iput v1, v0, Lfsg;->e:I

    iput v6, v0, Lfsg;->f:I

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    iget-object v8, p0, Lcei;->c:Lfsl;

    invoke-virtual {v8, v7}, Lfsl;->b(Ljava/lang/Class;)Lfsh;

    move-result-object v8

    if-nez v8, :cond_3

    move-object v0, v3

    :goto_3
    if-eqz v0, :cond_1

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move-object v4, v3

    goto :goto_2

    :cond_3
    invoke-interface {v8, v0, v4}, Lfsh;->a(Lfsg;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v4

    const v9, 0x7f080017

    invoke-virtual {v4, v9, v8}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v8, 0x7f080019

    invoke-virtual {v4, v8, v7}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-static {v4, v0}, Lcei;->a(Landroid/view/View;Lfsg;)V

    move-object v0, v4

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcei;->b:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_5
    move-object v5, v0

    goto :goto_1
.end method

.method public final a(Lfsl;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 128
    iget-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    move v2, v3

    .line 129
    :goto_0
    if-ge v2, v4, :cond_2

    .line 130
    iget-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 131
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 132
    if-eqz v5, :cond_1

    .line 133
    const v1, 0x7f080018

    invoke-virtual {v5, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfsg;

    if-eqz v1, :cond_0

    iget-object v6, p0, Lcei;->f:Ljava/util/LinkedList;

    invoke-virtual {v6, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-static {v5, v1}, Lcei;->a(Landroid/view/View;Lfsg;)V

    .line 134
    :cond_0
    invoke-virtual {p1, v5}, Lfsl;->a(Landroid/view/View;)V

    .line 135
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 137
    :cond_1
    iget-object v1, p0, Lcei;->e:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 129
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 139
    :cond_2
    iget-object v0, p0, Lcei;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 140
    return-void
.end method
