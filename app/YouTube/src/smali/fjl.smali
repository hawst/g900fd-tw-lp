.class public final enum Lfjl;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfjl;

.field public static final enum b:Lfjl;

.field public static final enum c:Lfjl;

.field private static enum d:Lfjl;

.field private static enum e:Lfjl;

.field private static final synthetic f:[Lfjl;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lfjl;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lfjl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfjl;->d:Lfjl;

    .line 30
    new-instance v0, Lfjl;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v3}, Lfjl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfjl;->a:Lfjl;

    .line 31
    new-instance v0, Lfjl;

    const-string v1, "RELOAD"

    invoke-direct {v0, v1, v4}, Lfjl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfjl;->b:Lfjl;

    .line 32
    new-instance v0, Lfjl;

    const-string v1, "TIMED"

    invoke-direct {v0, v1, v5}, Lfjl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfjl;->c:Lfjl;

    .line 33
    new-instance v0, Lfjl;

    const-string v1, "INVALIDATION"

    invoke-direct {v0, v1, v6}, Lfjl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfjl;->e:Lfjl;

    .line 28
    const/4 v0, 0x5

    new-array v0, v0, [Lfjl;

    sget-object v1, Lfjl;->d:Lfjl;

    aput-object v1, v0, v2

    sget-object v1, Lfjl;->a:Lfjl;

    aput-object v1, v0, v3

    sget-object v1, Lfjl;->b:Lfjl;

    aput-object v1, v0, v4

    sget-object v1, Lfjl;->c:Lfjl;

    aput-object v1, v0, v5

    sget-object v1, Lfjl;->e:Lfjl;

    aput-object v1, v0, v6

    sput-object v0, Lfjl;->f:[Lfjl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfjl;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lfjl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfjl;

    return-object v0
.end method

.method public static values()[Lfjl;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lfjl;->f:[Lfjl;

    invoke-virtual {v0}, [Lfjl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfjl;

    return-object v0
.end method
