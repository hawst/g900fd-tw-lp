.class public final Lcef;
.super Lfsb;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Leyp;

.field private final c:Lfhz;

.field private final d:Lfdw;

.field private final e:Lfrz;

.field private final f:Lfsj;

.field private final g:Lboi;

.field private h:Lceh;

.field private i:Lceh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;Lfhz;Lboi;Lfdw;Lfrz;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p4, p6, p7}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 51
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcef;->a:Landroid/content/Context;

    .line 52
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcef;->b:Leyp;

    .line 53
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcef;->c:Lfhz;

    .line 54
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lcef;->d:Lfdw;

    .line 56
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lcef;->e:Lfrz;

    .line 57
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcef;->f:Lfsj;

    .line 58
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcef;->g:Lboi;

    .line 60
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Lfsj;->a(Z)V

    .line 61
    invoke-interface {p3, p0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 62
    return-void
.end method

.method private a(Lfsg;Lfkb;)Landroid/view/View;
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lcef;->a:Landroid/content/Context;

    invoke-static {v0, p1}, La;->a(Landroid/content/Context;Lfsg;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 68
    iget-object v0, p0, Lcef;->i:Lceh;

    if-nez v0, :cond_0

    .line 69
    const v0, 0x7f040073

    invoke-direct {p0, v0}, Lcef;->a(I)Lceh;

    move-result-object v0

    iput-object v0, p0, Lcef;->i:Lceh;

    .line 71
    :cond_0
    iget-object v0, p0, Lcef;->i:Lceh;

    .line 80
    :goto_0
    invoke-virtual {p2}, Lfkb;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lceh;->a(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v1, p2, Lfkb;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfkb;->a:Lhhm;

    iget-object v1, v1, Lhhm;->e:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkb;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfkb;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lceh;->b(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v1, p2, Lfkb;->b:Lfnc;

    if-nez v1, :cond_2

    new-instance v1, Lfnc;

    iget-object v2, p2, Lfkb;->a:Lhhm;

    iget-object v2, v2, Lhhm;->b:Lhxf;

    invoke-direct {v1, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, p2, Lfkb;->b:Lfnc;

    :cond_2
    iget-object v1, p2, Lfkb;->b:Lfnc;

    iget-object v2, p2, Lfkb;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_3

    iget-object v2, p2, Lfkb;->a:Lhhm;

    iget-object v2, v2, Lhhm;->g:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, p2, Lfkb;->d:Ljava/lang/CharSequence;

    :cond_3
    iget-object v2, p2, Lfkb;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lceh;->a(Lfnc;Ljava/lang/CharSequence;)V

    .line 83
    iget-object v1, p0, Lcef;->f:Lfsj;

    invoke-virtual {v0, v1, p2}, Lceh;->a(Lfsj;Lfkb;)V

    .line 84
    iget-object v1, p0, Lcef;->f:Lfsj;

    invoke-interface {v1}, Lfsj;->a()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcef;->g:Lboi;

    iget-object v0, v0, Lcbx;->d:Landroid/view/View;

    invoke-static {v1, v2, v0, p2}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 86
    iget-object v0, p0, Lcef;->f:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 73
    :cond_4
    iget-object v0, p0, Lcef;->h:Lceh;

    if-nez v0, :cond_5

    .line 74
    const v0, 0x7f0400c8

    invoke-direct {p0, v0}, Lcef;->a(I)Lceh;

    move-result-object v0

    iput-object v0, p0, Lcef;->h:Lceh;

    .line 76
    :cond_5
    iget-object v0, p0, Lcef;->h:Lceh;

    .line 77
    invoke-virtual {v0}, Lceh;->a()V

    goto :goto_0
.end method

.method private a(I)Lceh;
    .locals 7

    .prologue
    .line 90
    new-instance v0, Lceh;

    iget-object v1, p0, Lcef;->a:Landroid/content/Context;

    iget-object v2, p0, Lcef;->b:Leyp;

    iget-object v3, p0, Lcef;->c:Lfhz;

    iget-object v5, p0, Lcef;->d:Lfdw;

    iget-object v6, p0, Lcef;->e:Lfrz;

    move v4, p1

    invoke-direct/range {v0 .. v6}, Lceh;-><init>(Landroid/content/Context;Leyp;Lfhz;ILfdw;Lfrz;)V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lfkb;

    invoke-direct {p0, p1, p2}, Lcef;->a(Lfsg;Lfkb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lfkb;

    invoke-direct {p0, p1, p2}, Lcef;->a(Lfsg;Lfkb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lfkb;

    invoke-direct {p0, p1, p2}, Lcef;->a(Lfsg;Lfkb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
