.class public final Lgfy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefc;
.implements Lorg/chromium/net/HttpUrlRequestListener;


# instance fields
.field private final a:Lorg/chromium/net/HttpUrlRequestFactory;

.field private final b:Legt;

.field private final c:Lega;

.field private final d:I

.field private final e:I

.field private final f:Landroid/os/ConditionVariable;

.field private volatile g:Lorg/chromium/net/HttpUrlRequest;

.field private h:Lgfz;

.field private i:Z


# direct methods
.method public constructor <init>(Lorg/chromium/net/HttpUrlRequestFactory;Legt;Lega;II)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lgfy;->f:Landroid/os/ConditionVariable;

    .line 59
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/net/HttpUrlRequestFactory;

    iput-object v0, p0, Lgfy;->a:Lorg/chromium/net/HttpUrlRequestFactory;

    .line 60
    iput-object p2, p0, Lgfy;->b:Legt;

    .line 61
    iput-object p3, p0, Lgfy;->c:Lega;

    .line 62
    iput p4, p0, Lgfy;->d:I

    .line 63
    iput p5, p0, Lgfy;->e:I

    .line 64
    return-void
.end method

.method static synthetic a(Lgfy;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lgfy;->e:I

    return v0
.end method


# virtual methods
.method public final a([BII)I
    .locals 3

    .prologue
    .line 149
    const-string v0, "Cronet read()"

    invoke-static {v0}, Lfaj;->a(Ljava/lang/String;)Lfal;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lgfy;->h:Lgfz;

    invoke-virtual {v1, p1, p2, p3}, Lgfz;->a([BII)I

    move-result v1

    .line 151
    iget-object v2, p0, Lgfy;->c:Lega;

    if-eqz v2, :cond_0

    .line 152
    iget-object v2, p0, Lgfy;->c:Lega;

    invoke-interface {v2, v1}, Lega;->a(I)V

    .line 154
    :cond_0
    invoke-static {v0}, Lfaj;->a(Lfal;)V

    .line 155
    return v1
.end method

.method public final a(Lefg;)J
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    .line 68
    iget-boolean v0, p0, Lgfy;->i:Z

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "Opening an already open source."

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 73
    :cond_0
    const-string v0, "Cronet creating request"

    invoke-static {v0}, Lfaj;->a(Ljava/lang/String;)Lfal;

    move-result-object v6

    .line 74
    iget-wide v0, p1, Lefg;->d:J

    iget-wide v2, p1, Lefg;->e:J

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x3f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Opening offset "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " length "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 75
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 76
    iget-wide v0, p1, Lefg;->d:J

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x1b

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "bytes="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    iget-wide v4, p1, Lefg;->e:J

    cmp-long v1, v4, v10

    if-eqz v1, :cond_1

    .line 78
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p1, Lefg;->d:J

    iget-wide v8, p1, Lefg;->e:J

    add-long/2addr v4, v8

    const-wide/16 v8, 0x1

    sub-long/2addr v4, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 80
    :cond_1
    const-string v1, "Range"

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    new-instance v0, Lgfz;

    invoke-direct {v0, p0}, Lgfz;-><init>(Lgfy;)V

    iput-object v0, p0, Lgfy;->h:Lgfz;

    .line 83
    iget-object v4, p0, Lgfy;->h:Lgfz;

    iget-object v0, p0, Lgfy;->a:Lorg/chromium/net/HttpUrlRequestFactory;

    iget-object v1, p1, Lefg;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/net/HttpUrlRequestFactory;->a(Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    iput-object v0, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    .line 84
    invoke-static {v6}, Lfaj;->a(Lfal;)V

    .line 87
    const-string v0, "Cronet connecting"

    invoke-static {v0}, Lfaj;->a(Ljava/lang/String;)Lfal;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lgfy;->f:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 89
    iget-object v1, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v1}, Lorg/chromium/net/HttpUrlRequest;->d()V

    .line 91
    iget-object v1, p0, Lgfy;->f:Landroid/os/ConditionVariable;

    iget v2, p0, Lgfy;->d:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v1

    if-nez v1, :cond_2

    .line 92
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Timed out waiting for headers."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_2
    invoke-static {v0}, Lfaj;->a(Lfal;)V

    .line 97
    iget-object v0, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->b()I

    move-result v0

    .line 98
    const/16 v1, 0xc8

    if-lt v0, v1, :cond_3

    const/16 v1, 0x12b

    if-le v0, v1, :cond_4

    .line 99
    :cond_3
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed fetch, responseCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 103
    :cond_4
    iget-object v0, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->f()Ljava/lang/String;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lgfy;->b:Legt;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lgfy;->b:Legt;

    invoke-interface {v1, v0}, Legt;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 105
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unacceptable contentType: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 109
    :cond_6
    iget-object v0, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->a()J

    move-result-wide v0

    .line 110
    iget-wide v2, p1, Lefg;->e:J

    cmp-long v2, v2, v10

    if-nez v2, :cond_7

    .line 111
    cmp-long v2, v0, v10

    if-nez v2, :cond_8

    .line 112
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Couldn\'t determine content length."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_7
    cmp-long v2, v0, v10

    if-nez v2, :cond_a

    .line 116
    iget-wide v0, p1, Lefg;->e:J

    .line 122
    :cond_8
    iget-object v2, p0, Lgfy;->c:Lega;

    if-eqz v2, :cond_9

    .line 123
    iget-object v2, p0, Lgfy;->c:Lega;

    invoke-interface {v2}, Lega;->b()V

    .line 125
    :cond_9
    const/4 v2, 0x1

    iput-boolean v2, p0, Lgfy;->i:Z

    .line 126
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Fetching "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    .line 128
    return-wide v0

    .line 117
    :cond_a
    iget-wide v2, p1, Lefg;->e:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_8

    .line 118
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Server returned incorrect content length."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 133
    const-string v0, "close() called"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->e()V

    .line 136
    iput-object v1, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    .line 137
    iput-object v1, p0, Lgfy;->h:Lgfz;

    .line 139
    :cond_0
    iget-boolean v0, p0, Lgfy;->i:Z

    if-eqz v0, :cond_2

    .line 140
    iget-object v0, p0, Lgfy;->c:Lega;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lgfy;->c:Lega;

    invoke-interface {v0}, Lega;->c()V

    .line 143
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgfy;->i:Z

    .line 145
    :cond_2
    return-void
.end method

.method public final a(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    if-eq v0, p1, :cond_0

    .line 171
    const-string v0, "Request objects out of sync."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 175
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lgfy;->f:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto :goto_0
.end method

.method public final b(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lgfy;->g:Lorg/chromium/net/HttpUrlRequest;

    if-eq v0, p1, :cond_0

    .line 161
    const-string v0, "Request objects out of sync."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 166
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lgfy;->f:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto :goto_0
.end method
