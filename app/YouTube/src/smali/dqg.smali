.class public Ldqg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbt;


# static fields
.field public static final a:Ljava/util/List;

.field private static final c:Ljava/lang/String;


# instance fields
.field public b:Z

.field private final d:Landroid/content/Context;

.field private final e:Ldpo;

.field private final f:Ldqr;

.field private g:Ldqn;

.field private h:Ldqq;

.field private i:Ldpt;

.field private final j:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private k:Z

.field private l:Ljava/lang/Thread;

.field private m:Z

.field private n:Ljava/util/concurrent/CountDownLatch;

.field private final o:Ljava/util/concurrent/ExecutorService;

.field private final p:Ljava/util/Queue;

.field private q:I

.field private r:I

.field private s:Ljava/util/concurrent/CountDownLatch;

.field private final t:Ljava/util/Timer;

.field private u:Ljava/util/TimerTask;

.field private v:J

.field private final w:Ljava/util/concurrent/ScheduledExecutorService;

.field private x:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-class v0, Ldqg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldqg;->c:Ljava/lang/String;

    .line 99
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Ldqg;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldpo;Ldqr;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-boolean v0, p0, Ldqg;->k:Z

    .line 115
    iput-boolean v0, p0, Ldqg;->m:Z

    .line 129
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Ldqg;->p:Ljava/util/Queue;

    .line 131
    const/16 v0, 0x1388

    iput v0, p0, Ldqg;->q:I

    .line 156
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldqg;->d:Landroid/content/Context;

    .line 157
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldpo;

    iput-object v0, p0, Ldqg;->e:Ldpo;

    .line 158
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqr;

    iput-object v0, p0, Ldqg;->f:Ldqr;

    .line 160
    invoke-direct {p0}, Ldqg;->h()V

    .line 161
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Ldqg;->s:Ljava/util/concurrent/CountDownLatch;

    .line 162
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    .line 163
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ldqg;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 164
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Ldqg;->o:Ljava/util/concurrent/ExecutorService;

    .line 165
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Ldqg;->w:Ljava/util/concurrent/ScheduledExecutorService;

    .line 166
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Timer - Reconnect to RC server"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ldqg;->t:Ljava/util/Timer;

    .line 167
    return-void
.end method

.method static synthetic a(Ldqg;Ldqq;)Ldqq;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Ldqg;->h:Ldqq;

    return-object p1
.end method

.method static synthetic a(Ldqg;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic a(Ldqg;Ldtc;Ldte;Ljava/util/List;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const/4 v7, 0x2

    .line 44
    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Ldqg;->s:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldqg;->s:Ljava/util/concurrent/CountDownLatch;

    sget v1, Ldqn;->a:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x5

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-boolean v0, p0, Ldqg;->b:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Ldqg;->p:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    invoke-static {p3, v7}, Ldqg;->a(Ljava/util/List;I)V

    sget-object v1, Ldqg;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ldqg;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, " still connecting, but not done"

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x24

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Dropping call for method:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], because"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_2
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ldqg;->c:Ljava/lang/String;

    const-string v2, "Interrupted while waiting to connect."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    const-string v0, " not connected"

    goto :goto_1

    :cond_4
    :try_start_1
    iget-object v0, p0, Ldqg;->h:Ldqq;

    invoke-interface {v0, p1, p2}, Ldqq;->a(Ldtc;Ldte;)I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Ldqg;->p:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Ldqg;->r:I

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    sget-object v1, Ldqg;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x23

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Exception while sending message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_5
    iget v0, p0, Ldqg;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldqg;->r:I

    if-ge v0, v7, :cond_6

    sget-object v0, Ldqg;->c:Ljava/lang/String;

    iget v1, p0, Ldqg;->r:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x32

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Increasing recent errors and retrying: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_6
    sget-object v0, Ldqg;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2e

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Too many errors on sending "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). Reconnecting..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Ldqg;->g()V

    goto/16 :goto_2
.end method

.method static synthetic a(Ldqg;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 44
    invoke-static {p1, p2}, Ldqg;->a(Ljava/util/List;I)V

    return-void
.end method

.method static synthetic a(Ldqg;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 44
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    return-void
.end method

.method private static a(Ljava/util/List;I)V
    .locals 2

    .prologue
    .line 479
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 482
    :cond_0
    return-void
.end method

.method static synthetic a(Ldqg;Z)Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldqg;->b:Z

    return v0
.end method

.method static synthetic b(Ldqg;Z)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldqg;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Ldqg;->l:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Ldqg;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 377
    :cond_0
    iget-object v0, p0, Ldqg;->h:Ldqq;

    const/4 v1, 0x1

    invoke-interface {v0, v1, p1}, Ldqq;->a(ZZ)V

    .line 378
    return-void
.end method

.method static synthetic b(Ldqg;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Ldqg;->b:Z

    return v0
.end method

.method static synthetic c(Ldqg;)Ldpo;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->e:Ldpo;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Ldqg;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Ldqg;Z)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldqg;->d(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 612
    iput-boolean p1, p0, Ldqg;->b:Z

    .line 613
    iget-object v1, p0, Ldqg;->d:Landroid/content/Context;

    if-eqz p1, :cond_0

    sget-object v0, Ldta;->d:Ldta;

    invoke-virtual {v0}, Ldta;->a()Landroid/content/Intent;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 615
    return-void

    .line 613
    :cond_0
    sget-object v0, Ldta;->e:Ldta;

    .line 614
    invoke-virtual {v0}, Ldta;->a()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d(Ldqg;)Ldqr;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->f:Ldqr;

    return-object v0
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 618
    if-eqz p1, :cond_0

    .line 619
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    .line 620
    iget-object v0, p0, Ldqg;->d:Landroid/content/Context;

    sget-object v1, Ldta;->f:Ldta;

    invoke-virtual {v1}, Ldta;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 625
    :goto_0
    return-void

    .line 622
    :cond_0
    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 623
    iget-object v0, p0, Ldqg;->d:Landroid/content/Context;

    sget-object v1, Ldta;->g:Ldta;

    invoke-virtual {v1}, Ldta;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic d(Ldqg;Z)Z
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Ldqg;->m:Z

    return p1
.end method

.method static synthetic e(Ldqg;)Ldqq;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->h:Ldqq;

    return-object v0
.end method

.method private static f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 452
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    .line 453
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 454
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 455
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    .line 456
    :cond_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 458
    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_1

    .line 459
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 466
    :goto_0
    return-object v0

    .line 463
    :catch_0
    move-exception v0

    .line 464
    sget-object v1, Ldqg;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Ldqg;)V
    .locals 5

    .prologue
    .line 44
    :try_start_0
    iget-object v0, p0, Ldqg;->h:Ldqq;

    invoke-interface {v0}, Ldqq;->a()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldqg;->c(Z)V

    invoke-direct {p0}, Ldqg;->h()V
    :try_end_0
    .catch Ldqy; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    new-instance v0, Ldqj;

    const-string v1, "HangingGetThread"

    invoke-direct {v0, p0, v1}, Ldqj;-><init>(Ldqg;Ljava/lang/String;)V

    iput-object v0, p0, Ldqg;->l:Ljava/lang/Thread;

    iget-object v0, p0, Ldqg;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ldqg;->c:Ljava/lang/String;

    iget v2, v0, Ldqy;->a:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x35

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unexpected response when binding channel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget v0, v0, Ldqy;->a:I

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-direct {p0}, Ldqg;->g()V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldqg;->a(Z)V

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Ldqg;->c:Ljava/lang/String;

    const-string v2, "Error connecting to Remote Control server:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0}, Ldqg;->g()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 470
    invoke-direct {p0, v1}, Ldqg;->c(Z)V

    .line 471
    invoke-direct {p0, v1}, Ldqg;->d(Z)V

    .line 472
    invoke-direct {p0, v1}, Ldqg;->b(Z)V

    .line 473
    iget-object v0, p0, Ldqg;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldqg;->k:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Ldqg;->k:Z

    iget-object v0, p0, Ldqg;->i:Ldpt;

    invoke-virtual {p0, v0}, Ldqg;->a(Ldpt;)Ljava/util/concurrent/CountDownLatch;

    .line 474
    :cond_0
    :goto_0
    iget-object v0, p0, Ldqg;->d:Landroid/content/Context;

    sget-object v1, Ldta;->h:Ldta;

    invoke-virtual {v1}, Ldta;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 475
    return-void

    .line 473
    :cond_1
    iget-object v0, p0, Ldqg;->d:Landroid/content/Context;

    invoke-static {v0}, La;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Ldta;->c:Ldta;

    invoke-virtual {v0}, Ldta;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Ldqg;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_2
    iget-object v0, p0, Ldqg;->s:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Ldqg;->v:J

    shl-long/2addr v0, v4

    const-wide/32 v2, 0x3a980

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    iget-wide v0, p0, Ldqg;->v:J

    shl-long/2addr v0, v4

    iput-wide v0, p0, Ldqg;->v:J

    :cond_3
    sget-object v0, Ldqg;->c:Ljava/lang/String;

    iget-wide v0, p0, Ldqg;->v:J

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Reconnecting in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Ldqg;->s:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ldqm;

    invoke-direct {v0, p0}, Ldqm;-><init>(Ldqg;)V

    iput-object v0, p0, Ldqg;->u:Ljava/util/TimerTask;

    iget-object v0, p0, Ldqg;->t:Ljava/util/Timer;

    iget-object v1, p0, Ldqg;->u:Ljava/util/TimerTask;

    iget-wide v2, p0, Ldqg;->v:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method static synthetic g(Ldqg;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ldqg;->i()V

    return-void
.end method

.method static synthetic h(Ldqg;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 491
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldqg;->k:Z

    .line 492
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    add-int/lit16 v0, v0, 0x7d0

    int-to-long v0, v0

    iput-wide v0, p0, Ldqg;->v:J

    .line 493
    return-void
.end method

.method static synthetic i(Ldqg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Ldqg;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized i()V
    .locals 6

    .prologue
    .line 496
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldqg;->p:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqn;

    iput-object v0, p0, Ldqg;->g:Ldqn;

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Ldqg;->o:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Ldqk;

    invoke-direct {v1, p0}, Ldqk;-><init>(Ldqg;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 519
    iget-object v1, p0, Ldqg;->w:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Ldql;

    invoke-direct {v2, p0, v0}, Ldql;-><init>(Ldqg;Ljava/util/concurrent/Future;)V

    iget v0, p0, Ldqg;->q:I

    int-to-long v4, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v4, v5, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Ldqg;->x:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 531
    :cond_0
    monitor-exit p0

    return-void

    .line 496
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic j(Ldqg;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic k(Ldqg;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Ldqg;->m:Z

    return v0
.end method

.method static synthetic l(Ldqg;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ldqg;->g()V

    return-void
.end method

.method static synthetic m(Ldqg;)Ldqn;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->g:Ldqn;

    return-object v0
.end method

.method static synthetic n(Ldqg;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->p:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic o(Ldqg;)Ljava/util/concurrent/ScheduledFuture;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->x:Ljava/util/concurrent/ScheduledFuture;

    return-object v0
.end method

.method static synthetic p(Ldqg;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Ldqg;->q:I

    return v0
.end method

.method static synthetic q(Ldqg;)Ldpt;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldqg;->i:Ldpt;

    return-object v0
.end method


# virtual methods
.method public final a(Ldpt;)Ljava/util/concurrent/CountDownLatch;
    .locals 4

    .prologue
    .line 171
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 173
    sget-object v0, Ldqg;->c:Ljava/lang/String;

    const-string v1, "Already in the process of connecting. Ignoring connect request"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    .line 207
    :goto_0
    return-object v0

    .line 176
    :cond_0
    iput-object p1, p0, Ldqg;->i:Ldpt;

    .line 177
    const/4 v0, 0x0

    iput v0, p0, Ldqg;->r:I

    .line 178
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldqg;->d(Z)V

    .line 181
    iget-object v0, p0, Ldqg;->s:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 183
    new-instance v0, Ldqh;

    const-string v1, "asyncConnect"

    invoke-direct {v0, p0, v1, p1}, Ldqh;-><init>(Ldqg;Ljava/lang/String;Ldpt;)V

    .line 206
    invoke-virtual {v0}, Ldqh;->start()V

    .line 207
    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    goto :goto_0
.end method

.method public final declared-synchronized a(Ldtc;Ldte;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldqg;->p:Ljava/util/Queue;

    new-instance v1, Ldqn;

    invoke-direct {v1, p1, p2, p3}, Ldqn;-><init>(Ldtc;Ldte;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 230
    iget-object v0, p0, Ldqg;->g:Ldqn;

    if-nez v0, :cond_0

    .line 231
    invoke-direct {p0}, Ldqg;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    :cond_0
    monitor-exit p0

    return-void

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 245
    iget-object v0, p0, Ldqg;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 246
    iget-object v0, p0, Ldqg;->p:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqn;

    sget-object v2, Ldqg;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x12

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Dropping message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Ldqn;->b:Ljava/util/List;

    const/4 v2, 0x4

    invoke-static {v0, v2}, Ldqg;->a(Ljava/util/List;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ldqg;->p:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 249
    iget-object v0, p0, Ldqg;->u:Ljava/util/TimerTask;

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Ldqg;->u:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Ldqg;->u:Ljava/util/TimerTask;

    .line 254
    :cond_1
    :try_start_0
    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :goto_1
    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 259
    sget-object v0, Ldqg;->c:Ljava/lang/String;

    const-string v1, "Timed out while waiting for BC to connect. Will attempt stopping the connection anyway."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_2
    iget-boolean v0, p0, Ldqg;->b:Z

    if-eqz v0, :cond_3

    .line 263
    invoke-direct {p0, p1}, Ldqg;->b(Z)V

    .line 265
    :cond_3
    invoke-direct {p0, v6}, Ldqg;->c(Z)V

    .line 266
    invoke-direct {p0, v6}, Ldqg;->d(Z)V

    .line 267
    iget-object v0, p0, Ldqg;->d:Landroid/content/Context;

    sget-object v1, Ldta;->a:Ldta;

    invoke-virtual {v1}, Ldta;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 268
    return-void

    .line 255
    :catch_0
    move-exception v0

    .line 256
    sget-object v1, Ldqg;->c:Ljava/lang/String;

    const-string v2, "Interrupted while waiting for BC to connect"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 286
    iget-object v0, p0, Ldqg;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
