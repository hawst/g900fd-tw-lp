.class final Lggd;
.super Lecj;
.source "SourceFile"


# instance fields
.field private final f:Ledm;

.field private final g:Leee;


# direct methods
.method public constructor <init>(Lefc;Lefg;ILeee;Ledm;)V
    .locals 1

    .prologue
    .line 419
    iget-object v0, p5, Ledm;->b:Lecw;

    invoke-direct {p0, p1, p2, v0, p3}, Lecj;-><init>(Lefc;Lefg;Lecw;I)V

    .line 420
    iput-object p4, p0, Lggd;->g:Leee;

    .line 421
    iput-object p5, p0, Lggd;->f:Ledm;

    .line 422
    return-void
.end method


# virtual methods
.method protected final a(Lefx;)V
    .locals 6

    .prologue
    .line 426
    iget-object v0, p0, Lggd;->g:Leee;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Leee;->a(Lefx;Leby;)I

    move-result v0

    .line 427
    const/16 v1, 0x1a

    if-eq v0, v1, :cond_0

    .line 428
    new-instance v1, Lebx;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid initialization data "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lebx;-><init>(Ljava/lang/String;)V

    throw v1

    .line 430
    :cond_0
    iget-object v0, p0, Lggd;->g:Leee;

    invoke-interface {v0}, Leee;->a()Leef;

    move-result-object v0

    iget-object v1, p0, Lggd;->f:Ledm;

    iget-object v1, v1, Ledm;->g:Ledj;

    iget v2, v0, Leef;->a:I

    int-to-long v2, v2

    iget-wide v4, v1, Ledj;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    iget v2, v0, Leef;->a:I

    iget-wide v4, v1, Ledj;->b:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x4f

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Sidx length mismatch: sidxLen = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ExpectedLen = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lggd;->g:Leee;

    iget-object v2, p0, Lggd;->f:Ledm;

    iget v3, v0, Leef;->b:I

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v2, v3}, Lggc;->a(Leee;Ledk;I)J

    move-result-wide v2

    iget-object v1, v0, Leef;->c:[I

    iget v0, v0, Leef;->b:I

    add-int/lit8 v0, v0, -0x1

    aget v0, v1, v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    iget-object v2, p0, Lggd;->f:Ledm;

    iget-wide v2, v2, Ledm;->f:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lggd;->f:Ledm;

    iget-wide v2, v2, Ledm;->f:J

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x56

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "ContentLength mismatch: Actual = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Expected = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 431
    :cond_2
    return-void
.end method
