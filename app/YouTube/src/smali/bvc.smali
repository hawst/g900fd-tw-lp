.class public final Lbvc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldso;


# instance fields
.field private final a:Ldsn;

.field private b:Lboi;

.field private final c:Lbvm;

.field private final d:I

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Ldsn;Lboi;Lbvm;I)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsn;

    iput-object v0, p0, Lbvc;->a:Ldsn;

    .line 51
    iput-object p2, p0, Lbvc;->b:Lboi;

    .line 52
    iput-object p3, p0, Lbvc;->c:Lbvm;

    .line 53
    iput p4, p0, Lbvc;->d:I

    .line 54
    invoke-virtual {p1, p0}, Ldsn;->a(Ldso;)V

    .line 55
    return-void
.end method

.method public static a(Ldsn;Leyt;Ldaq;)Lbvc;
    .locals 4

    .prologue
    .line 78
    new-instance v0, Lbvc;

    const/4 v1, 0x0

    .line 81
    new-instance v2, Lbvf;

    invoke-direct {v2, p1, p2}, Lbvf;-><init>(Leyt;Ldaq;)V

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lbvc;-><init>(Ldsn;Lboi;Lbvm;I)V

    return-object v0
.end method

.method private a(Ldwq;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 154
    iget-object v0, p0, Lbvc;->b:Lboi;

    if-nez v0, :cond_0

    .line 169
    :goto_0
    return-void

    .line 157
    :cond_0
    if-eqz p1, :cond_2

    move v0, v1

    .line 158
    :goto_1
    iget-boolean v3, p0, Lbvc;->f:Z

    if-nez v3, :cond_4

    if-eqz v0, :cond_4

    .line 159
    iget v0, p0, Lbvc;->d:I

    if-ne v0, v1, :cond_3

    const v0, 0x7f090202

    .line 161
    :goto_2
    iget-object v3, p0, Lbvc;->b:Lboi;

    iget-object v4, p0, Lbvc;->c:Lbvm;

    .line 162
    invoke-interface {v4, p1}, Lbvm;->a(Ldwq;)Lbop;

    move-result-object v4

    .line 161
    const/4 v5, -0x1

    invoke-virtual {v3, v2, v0, v5, v4}, Lboi;->a(IIILbop;)I

    move-result v0

    iput v0, p0, Lbvc;->e:I

    .line 163
    iput-boolean v1, p0, Lbvc;->f:Z

    .line 168
    :cond_1
    :goto_3
    iget-object v0, p0, Lbvc;->b:Lboi;

    iget-object v3, p0, Lbvc;->b:Lboi;

    iget-object v3, v3, Lboi;->a:Lbok;

    invoke-virtual {v3}, Lbok;->getCount()I

    move-result v3

    if-lez v3, :cond_5

    :goto_4
    iput-boolean v1, v0, Lboi;->c:Z

    goto :goto_0

    :cond_2
    move v0, v2

    .line 157
    goto :goto_1

    .line 159
    :cond_3
    const v0, 0x7f0902fa

    goto :goto_2

    .line 164
    :cond_4
    iget-boolean v3, p0, Lbvc;->f:Z

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    .line 165
    iget-object v0, p0, Lbvc;->b:Lboi;

    iget v3, p0, Lbvc;->e:I

    invoke-virtual {v0, v3}, Lboi;->a(I)V

    .line 166
    iput-boolean v2, p0, Lbvc;->f:Z

    goto :goto_3

    :cond_5
    move v1, v2

    .line 168
    goto :goto_4
.end method

.method static synthetic a(Ljava/lang/String;Ldwq;Leyt;Ldaq;)V
    .locals 8

    .prologue
    .line 24
    sget-object v2, Ldaq;->a:[B

    const-string v3, ""

    const-string v4, ""

    const/4 v5, -0x1

    const/4 v6, 0x0

    new-instance v7, Lbvl;

    invoke-direct {v7, p1, p2}, Lbvl;-><init>(Ldwq;Leyt;)V

    move-object v0, p3

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    return-void
.end method

.method public static b(Ldsn;Leyt;Ldaq;)Lbvc;
    .locals 4

    .prologue
    .line 93
    new-instance v0, Lbvc;

    const/4 v1, 0x0

    .line 96
    new-instance v2, Lbvh;

    invoke-direct {v2, p1, p2}, Lbvh;-><init>(Leyt;Ldaq;)V

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lbvc;-><init>(Ldsn;Lboi;Lbvm;I)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lbvc;->a:Ldsn;

    invoke-virtual {v0, p0}, Ldsn;->b(Ldso;)V

    .line 129
    return-void
.end method

.method public final a(Lboi;)V
    .locals 2

    .prologue
    .line 145
    iget-boolean v0, p0, Lbvc;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvc;->b:Lboi;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lbvc;->b:Lboi;

    iget v1, p0, Lbvc;->e:I

    invoke-virtual {v0, v1}, Lboi;->a(I)V

    .line 147
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbvc;->f:Z

    .line 149
    :cond_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lbvc;->b:Lboi;

    .line 150
    invoke-virtual {p0}, Lbvc;->b()V

    .line 151
    return-void
.end method

.method public final a(Ldwq;Z)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lbvc;->a(Ldwq;)V

    .line 134
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lbvc;->a:Ldsn;

    iget-object v0, v0, Ldsn;->d:Ldwq;

    invoke-direct {p0, v0}, Lbvc;->a(Ldwq;)V

    .line 142
    return-void
.end method
