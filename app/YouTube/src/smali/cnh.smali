.class public abstract Lcnh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlr;


# instance fields
.field private final a:Levn;


# direct methods
.method public constructor <init>(Levn;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcnh;->a:Levn;

    .line 41
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(II)V
.end method

.method public abstract a(Lcuv;)V
.end method

.method protected abstract a(Lcwx;)V
.end method

.method protected abstract a(Lcyz;)V
.end method

.method protected abstract a(Lczx;)V
.end method

.method protected abstract a(Ldad;)V
.end method

.method public abstract a(Lesm;)V
.end method

.method public abstract a(Lfpi;I)V
.end method

.method public abstract a(Lfpi;Lfpm;)V
.end method

.method protected abstract a(Lggp;)V
.end method

.method public abstract b()V
.end method

.method public abstract b(Lcuv;)V
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method

.method public abstract h()V
.end method

.method public abstract i()V
.end method

.method public abstract j()Lcnj;
.end method

.method public abstract k()V
.end method

.method public abstract l()V
.end method

.method public m()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcnh;->a:Levn;

    const-class v1, Lcnh;

    invoke-virtual {v0, p0, v1}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 130
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcnh;->a:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 135
    return-void
.end method

.method public onPlayerGeometryChanged(Lcwx;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcnh;->a(Lcwx;)V

    .line 74
    return-void
.end method

.method public relayInfoCardPingRequestEvent(Lcyz;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcnh;->a(Lcyz;)V

    .line 67
    return-void
.end method

.method public relayMedialibErrorEvent(Lggp;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcnh;->a(Lggp;)V

    .line 46
    return-void
.end method

.method public relaySurveyProgressEvent(Lczx;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcnh;->a(Lczx;)V

    .line 60
    return-void
.end method

.method public relayVideoTimeEvent(Ldad;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcnh;->a(Ldad;)V

    .line 53
    return-void
.end method
