.class public abstract Lghl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lorg/apache/http/client/HttpClient;

.field public final e:Ljava/util/concurrent/Executor;

.field public final f:Lezj;

.field public final g:Ljava/lang/String;

.field public final h:Lgik;

.field public final i:Lfbc;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lghl;->e:Ljava/util/concurrent/Executor;

    .line 110
    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lghl;->a:Lorg/apache/http/client/HttpClient;

    .line 111
    new-instance v0, Lgik;

    sget-object v1, Lewv;->b:Lewv;

    invoke-direct {v0, v1}, Lgik;-><init>(Lewv;)V

    iput-object v0, p0, Lghl;->h:Lgik;

    .line 112
    iput-object v2, p0, Lghl;->i:Lfbc;

    .line 113
    iput-object v2, p0, Lghl;->g:Ljava/lang/String;

    .line 114
    iput-object v2, p0, Lghl;->f:Lezj;

    .line 115
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lezj;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lghl;->e:Ljava/util/concurrent/Executor;

    .line 83
    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lghl;->a:Lorg/apache/http/client/HttpClient;

    .line 84
    const-string v0, "clock can\'t be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lghl;->f:Lezj;

    .line 85
    new-instance v0, Lgik;

    sget-object v1, Lewv;->b:Lewv;

    invoke-direct {v0, v1}, Lgik;-><init>(Lewv;)V

    iput-object v0, p0, Lghl;->h:Lgik;

    .line 86
    iput-object v2, p0, Lghl;->i:Lfbc;

    .line 87
    iput-object v2, p0, Lghl;->g:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Lezj;)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lghl;->e:Ljava/util/concurrent/Executor;

    .line 101
    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lghl;->a:Lorg/apache/http/client/HttpClient;

    .line 102
    const-string v0, "xmlParser cannot be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbc;

    iput-object v0, p0, Lghl;->i:Lfbc;

    .line 103
    const-string v0, "clock cannot be null"

    invoke-static {p4, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lghl;->f:Lezj;

    .line 104
    new-instance v0, Lgik;

    sget-object v1, Lewv;->b:Lewv;

    invoke-direct {v0, v1}, Lgik;-><init>(Lewv;)V

    iput-object v0, p0, Lghl;->h:Lgik;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lghl;->g:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Ljava/lang/String;Lezj;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lghl;->e:Ljava/util/concurrent/Executor;

    .line 65
    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lghl;->a:Lorg/apache/http/client/HttpClient;

    .line 66
    const-string v0, "xmlParser can\'t be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbc;

    iput-object v0, p0, Lghl;->i:Lfbc;

    .line 67
    const-string v0, "cachePath can\'t be null"

    invoke-static {p4, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lghl;->g:Ljava/lang/String;

    .line 68
    const-string v0, "clock can\'t be null"

    invoke-static {p5, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lghl;->f:Lezj;

    .line 69
    new-instance v0, Lgik;

    sget-object v1, Lewv;->b:Lewv;

    invoke-direct {v0, v1}, Lgik;-><init>(Lewv;)V

    iput-object v0, p0, Lghl;->h:Lgik;

    .line 70
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lezj;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lghl;->e:Ljava/util/concurrent/Executor;

    .line 74
    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lghl;->a:Lorg/apache/http/client/HttpClient;

    .line 75
    const-string v0, "clock can\'t be null"

    invoke-static {p4, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lghl;->f:Lezj;

    .line 76
    iput-object p3, p0, Lghl;->g:Ljava/lang/String;

    .line 77
    new-instance v0, Lgik;

    sget-object v1, Lewv;->b:Lewv;

    invoke-direct {v0, v1}, Lgik;-><init>(Lewv;)V

    iput-object v0, p0, Lghl;->h:Lgik;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lghl;->i:Lfbc;

    .line 79
    return-void
.end method

.method public static a(I)Leul;
    .locals 1

    .prologue
    .line 142
    new-instance v0, Leul;

    invoke-direct {v0, p0}, Leul;-><init>(I)V

    return-object v0
.end method

.method public static a(Lewh;Leuk;Lgku;)Lgki;
    .locals 1

    .prologue
    .line 163
    new-instance v0, Lgki;

    invoke-direct {v0, p0, p1, p2}, Lgki;-><init>(Lewh;Leuk;Lgku;)V

    return-object v0
.end method


# virtual methods
.method public final a(Leuk;Lgku;J)Lghi;
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lghl;->f:Lezj;

    const-string v1, "this instance does not contain a clock"

    invoke-static {v0, v1}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v0, p0, Lghl;->f:Lezj;

    invoke-static {p1, p2, v0, p3, p4}, Lghi;->a(Leuk;Lgku;Lezj;J)Lghi;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lgku;)Lgjx;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lghl;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, p1}, Lgjx;->a(Ljava/util/concurrent/Executor;Lgku;)Lgjx;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lgib;Lghv;)Lgko;
    .locals 2

    .prologue
    .line 134
    new-instance v0, Lgko;

    iget-object v1, p0, Lghl;->a:Lorg/apache/http/client/HttpClient;

    invoke-direct {v0, v1, p1, p2}, Lgko;-><init>(Lorg/apache/http/client/HttpClient;Lgib;Lghv;)V

    return-object v0
.end method

.method public final h()Leun;
    .locals 3

    .prologue
    .line 156
    iget-object v0, p0, Lghl;->g:Ljava/lang/String;

    const-string v1, "this instance does not support persistent caching"

    invoke-static {v0, v1}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    new-instance v0, Leut;

    iget-object v1, p0, Lghl;->g:Ljava/lang/String;

    invoke-direct {v0, v1}, Leut;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lghl;->e:Ljava/util/concurrent/Executor;

    .line 158
    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x1

    iput-boolean v2, v0, Leun;->e:Z

    new-instance v2, Leus;

    invoke-direct {v2, v0}, Leus;-><init>(Leun;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-object v0
.end method
