.class final Lbas;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field private synthetic a:Lbad;


# direct methods
.method constructor <init>(Lbad;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lbas;->a:Lbad;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 289
    iget-object v0, p0, Lbas;->a:Lbad;

    invoke-static {v0}, Lbad;->b(Lbad;)Lgnd;

    move-result-object v0

    invoke-interface {v0}, Lgnd;->d()Lgml;

    move-result-object v2

    iget-object v0, p0, Lbas;->a:Lbad;

    invoke-static {v0}, Lbad;->b(Lbad;)Lgnd;

    move-result-object v0

    invoke-interface {v0}, Lgnd;->g()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmb;

    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lgml;->h(Ljava/lang/String;)Lesq;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v1, Lesq;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Less;

    iget-object v5, v0, Lgmb;->a:Lgcd;

    iget-object v5, v5, Lgcd;->b:Ljava/lang/String;

    iget-object v1, v1, Less;->e:Ljava/lang/String;

    iget-object v6, v2, Lgml;->g:Lgmh;

    iget-object v6, v6, Lgmh;->a:Levi;

    invoke-interface {v6}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, "UPDATE ads SET vast_playback_count = 0 WHERE original_video_id = ? AND ad_break_id = ?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v5, 0x1

    aput-object v1, v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v0, v2, Lgml;->g:Lgmh;

    invoke-virtual {v0}, Lgmh;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmj;

    iget-object v4, v0, Lgmj;->b:Lgmg;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lgmj;->b:Lgmg;

    iget-object v4, v4, Lgmg;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, v0, Lgmj;->b:Lgmg;

    iget-object v4, v4, Lgmg;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lgml;->j(Ljava/lang/String;)V

    iget-object v0, v0, Lgmj;->b:Lgmg;

    iget-object v0, v0, Lgmg;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 289
    iget-object v0, p0, Lbas;->a:Lbad;

    const-string v1, "All offline ad playback counts have been reset to 0!"

    invoke-static {v0, v1, v2}, Leze;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    new-instance v0, Lbal;

    iget-object v1, p0, Lbas;->a:Lbad;

    invoke-direct {v0, v1}, Lbal;-><init>(Lbad;)V

    new-array v1, v2, [Ljava/lang/Void;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lbal;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
