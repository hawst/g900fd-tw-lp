.class public final Lagl;
.super Lair;
.source "SourceFile"


# instance fields
.field public a:Lahg;

.field private final b:Ldfu;

.field private final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ldfu;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lair;-><init>()V

    .line 23
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfu;

    iput-object v0, p0, Lagl;->b:Ldfu;

    .line 24
    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lagl;->c:Landroid/os/Handler;

    .line 25
    return-void
.end method

.method static synthetic a(Lagl;)Lahg;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lagl;->a:Lahg;

    return-object v0
.end method

.method static synthetic a(Lagl;Lahg;)Lahg;
    .locals 0

    .prologue
    .line 15
    iput-object p1, p0, Lagl;->a:Lahg;

    return-object p1
.end method

.method static synthetic b(Lagl;)Ldfu;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lagl;->b:Ldfu;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagx;

    invoke-direct {v1, p0}, Lagx;-><init>(Lagl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 53
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Laha;

    invoke-direct {v1, p0, p1}, Laha;-><init>(Lagl;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 73
    return-void
.end method

.method public final a(Lang;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagm;

    invoke-direct {v1, p0, p1}, Lagm;-><init>(Lagl;Lang;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 43
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lahd;

    invoke-direct {v1, p0, p1}, Lahd;-><init>(Lagl;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 104
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagp;

    invoke-direct {v1, p0, p1, p2}, Lagp;-><init>(Lagl;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagz;

    invoke-direct {v1, p0, p1}, Lagz;-><init>(Lagl;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 63
    return-void
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagr;

    invoke-direct {v1, p0, p1, p2}, Lagr;-><init>(Lagl;ZZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 177
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lahb;

    invoke-direct {v1, p0}, Lahb;-><init>(Lagl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 83
    return-void
.end method

.method public final b(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lahf;

    invoke-direct {v1, p0, p1}, Lahf;-><init>(Lagl;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 125
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lahe;

    invoke-direct {v1, p0, p1}, Lahe;-><init>(Lagl;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 114
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lags;

    invoke-direct {v1, p0, p1}, Lags;-><init>(Lagl;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 187
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lahc;

    invoke-direct {v1, p0}, Lahc;-><init>(Lagl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 93
    return-void
.end method

.method public final c(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagq;

    invoke-direct {v1, p0, p1}, Lagq;-><init>(Lagl;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 166
    return-void
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagv;

    invoke-direct {v1, p0, p1}, Lagv;-><init>(Lagl;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 217
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagy;

    invoke-direct {v1, p0, p1}, Lagy;-><init>(Lagl;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 237
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagn;

    invoke-direct {v1, p0}, Lagn;-><init>(Lagl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 135
    return-void
.end method

.method public final d(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagw;

    invoke-direct {v1, p0, p1}, Lagw;-><init>(Lagl;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 227
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lago;

    invoke-direct {v1, p0}, Lago;-><init>(Lagl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 145
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagt;

    invoke-direct {v1, p0}, Lagt;-><init>(Lagl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 197
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lagl;->c:Landroid/os/Handler;

    new-instance v1, Lagu;

    invoke-direct {v1, p0}, Lagu;-><init>(Lagl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 207
    return-void
.end method
