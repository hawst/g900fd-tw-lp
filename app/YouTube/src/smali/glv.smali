.class public final enum Lglv;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lglv;

.field public static final enum b:Lglv;

.field public static final enum c:Lglv;

.field public static final enum d:Lglv;

.field public static final enum e:Lglv;

.field public static final enum f:Lglv;

.field public static final enum g:Lglv;

.field public static final enum h:Lglv;

.field public static final enum i:Lglv;

.field private static l:Landroid/util/SparseArray;

.field private static final synthetic m:[Lglv;


# instance fields
.field public final j:I

.field public final k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 10
    new-instance v1, Lglv;

    const-string v2, "DELETED"

    const/4 v3, -0x1

    invoke-direct {v1, v2, v0, v3, v0}, Lglv;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lglv;->a:Lglv;

    .line 13
    new-instance v1, Lglv;

    const-string v2, "COMPLETE"

    invoke-direct {v1, v2, v5, v0, v0}, Lglv;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lglv;->b:Lglv;

    .line 16
    new-instance v1, Lglv;

    const-string v2, "ACTIVE"

    invoke-direct {v1, v2, v6, v5, v0}, Lglv;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lglv;->c:Lglv;

    .line 19
    new-instance v1, Lglv;

    const-string v2, "FAILED_UNKNOWN"

    invoke-direct {v1, v2, v7, v6, v5}, Lglv;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lglv;->d:Lglv;

    .line 22
    new-instance v1, Lglv;

    const-string v2, "DISK_WRITE_ERROR"

    invoke-direct {v1, v2, v8, v7, v5}, Lglv;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lglv;->e:Lglv;

    .line 25
    new-instance v1, Lglv;

    const-string v2, "DISK_READ_ERROR"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3, v8, v5}, Lglv;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lglv;->f:Lglv;

    .line 28
    new-instance v1, Lglv;

    const-string v2, "NETWORK_READ_ERROR"

    const/4 v3, 0x6

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4, v0}, Lglv;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lglv;->g:Lglv;

    .line 31
    new-instance v1, Lglv;

    const-string v2, "CANNOT_OFFLINE"

    const/4 v3, 0x7

    const/4 v4, 0x6

    invoke-direct {v1, v2, v3, v4, v5}, Lglv;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lglv;->h:Lglv;

    .line 34
    new-instance v1, Lglv;

    const-string v2, "PAUSED"

    const/16 v3, 0x8

    const/4 v4, 0x7

    invoke-direct {v1, v2, v3, v4, v0}, Lglv;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lglv;->i:Lglv;

    .line 8
    const/16 v1, 0x9

    new-array v1, v1, [Lglv;

    sget-object v2, Lglv;->a:Lglv;

    aput-object v2, v1, v0

    sget-object v2, Lglv;->b:Lglv;

    aput-object v2, v1, v5

    sget-object v2, Lglv;->c:Lglv;

    aput-object v2, v1, v6

    sget-object v2, Lglv;->d:Lglv;

    aput-object v2, v1, v7

    sget-object v2, Lglv;->e:Lglv;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lglv;->f:Lglv;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lglv;->g:Lglv;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lglv;->h:Lglv;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lglv;->i:Lglv;

    aput-object v3, v1, v2

    sput-object v1, Lglv;->m:[Lglv;

    .line 54
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    sput-object v1, Lglv;->l:Landroid/util/SparseArray;

    .line 55
    invoke-static {}, Lglv;->values()[Lglv;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 56
    sget-object v4, Lglv;->l:Landroid/util/SparseArray;

    iget v5, v3, Lglv;->j:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput p3, p0, Lglv;->j:I

    .line 41
    iput-boolean p4, p0, Lglv;->k:Z

    .line 42
    return-void
.end method

.method public static a(I)Lglv;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lglv;->l:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglv;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lglv;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lglv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lglv;

    return-object v0
.end method

.method public static values()[Lglv;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lglv;->m:[Lglv;

    invoke-virtual {v0}, [Lglv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lglv;

    return-object v0
.end method
