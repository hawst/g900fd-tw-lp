.class public final Lgdu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgeh;


# static fields
.field static final a:Landroid/util/SparseArray;


# instance fields
.field private A:I

.field private B:I

.field private final C:Lgea;

.field private final D:Lgei;

.field private final E:Ljava/lang/String;

.field private final F:Lges;

.field final b:Lexd;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/concurrent/atomic/AtomicReference;

.field private final e:Lgdz;

.field private final f:Landroid/os/Handler;

.field private final g:Lgdy;

.field private volatile h:Z

.field private volatile i:Z

.field private volatile j:Z

.field private volatile k:Z

.field private volatile l:Z

.field private volatile m:Z

.field private volatile n:Z

.field private volatile o:Z

.field private p:Lfrf;

.field private q:Lfqy;

.field private r:Lfqj;

.field private s:Lfqj;

.field private t:Ljava/lang/String;

.field private u:Lgec;

.field private v:Z

.field private w:Z

.field private x:Z

.field private final y:Lgdw;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 55
    sput-object v0, Lgdu;->a:Landroid/util/SparseArray;

    const/16 v1, -0x3ea

    const-string v2, "net.dns"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 56
    sget-object v0, Lgdu;->a:Landroid/util/SparseArray;

    const/16 v1, -0x3eb

    const-string v2, "net.connect"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 57
    sget-object v0, Lgdu;->a:Landroid/util/SparseArray;

    const/16 v1, -0x3ec

    const-string v2, "net.timeout"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 58
    sget-object v0, Lgdu;->a:Landroid/util/SparseArray;

    const/16 v1, -0x3ed

    const-string v2, "net.closed"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 59
    sget-object v0, Lgdu;->a:Landroid/util/SparseArray;

    const/16 v1, -0x3ef

    const-string v2, "fmt.decode"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 60
    sget-object v0, Lgdu;->a:Landroid/util/SparseArray;

    const/16 v1, -0x3f2

    const-string v2, "fmt.unplayable"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 61
    sget-object v0, Lgdu;->a:Landroid/util/SparseArray;

    const/high16 v1, -0x80000000

    const-string v2, "net.timeout"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lezj;Lexd;Lges;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 127
    new-instance v6, Lgdv;

    invoke-direct {v6}, Lgdv;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lgdu;-><init>(Landroid/content/Context;Lezj;Lexd;Lges;Ljava/lang/String;Lgdy;)V

    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lezj;Lexd;Lges;Ljava/lang/String;Lgdy;)V
    .locals 3

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lgdu;->c:Landroid/content/Context;

    .line 144
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lgdu;->b:Lexd;

    .line 145
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lges;

    iput-object v0, p0, Lgdu;->F:Lges;

    .line 146
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgdu;->E:Ljava/lang/String;

    .line 147
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdy;

    iput-object v0, p0, Lgdu;->g:Lgdy;

    .line 149
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    .line 150
    new-instance v0, Lgdw;

    invoke-direct {v0, p0}, Lgdw;-><init>(Lgdu;)V

    iput-object v0, p0, Lgdu;->y:Lgdw;

    .line 152
    new-instance v0, Lgdz;

    invoke-direct {v0, p0}, Lgdz;-><init>(Lgdu;)V

    iput-object v0, p0, Lgdu;->e:Lgdz;

    .line 153
    iget-object v0, p0, Lgdu;->e:Lgdz;

    invoke-virtual {v0}, Lgdz;->start()V

    .line 155
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lgdu;->f:Landroid/os/Handler;

    .line 157
    new-instance v0, Lgea;

    invoke-direct {v0, p0}, Lgea;-><init>(Lgdu;)V

    iput-object v0, p0, Lgdu;->C:Lgea;

    .line 158
    iget-object v0, p0, Lgdu;->C:Lgea;

    invoke-virtual {v0}, Lgea;->start()V

    .line 159
    new-instance v1, Lget;

    new-instance v2, Lgdf;

    .line 160
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    invoke-direct {v2, v0}, Lgdf;-><init>(Lezj;)V

    invoke-direct {v1, v2}, Lget;-><init>(Lgei;)V

    iput-object v1, p0, Lgdu;->D:Lgei;

    .line 161
    return-void
.end method

.method private a(Lfqj;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 319
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    iput-boolean v1, p0, Lgdu;->x:Z

    .line 321
    iput-boolean v2, p0, Lgdu;->i:Z

    .line 322
    iput-boolean v1, p0, Lgdu;->h:Z

    .line 323
    iput-boolean v2, p0, Lgdu;->w:Z

    .line 324
    iput-object p1, p0, Lgdu;->s:Lfqj;

    .line 325
    iput v1, p0, Lgdu;->z:I

    .line 326
    invoke-direct {p0, p1}, Lgdu;->b(Lfqj;)V

    .line 327
    return-void
.end method

.method private a(Lfqj;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 305
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iput-boolean v2, p0, Lgdu;->x:Z

    .line 307
    iget-boolean v0, p0, Lgdu;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdu;->s:Lfqj;

    invoke-virtual {p1, v0}, Lfqj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lgdu;->h:Z

    .line 308
    iput-boolean v2, p0, Lgdu;->i:Z

    .line 309
    iput-object p1, p0, Lgdu;->s:Lfqj;

    .line 310
    iput p2, p0, Lgdu;->z:I

    .line 311
    iget-object v0, p1, Lfqj;->a:Lhgy;

    iget v0, v0, Lhgy;->b:I

    const/16 v3, 0x5d

    if-ne v0, v3, :cond_1

    :goto_1
    iput-boolean v1, p0, Lgdu;->w:Z

    .line 312
    invoke-direct {p0, p1}, Lgdu;->b(Lfqj;)V

    .line 313
    return-void

    :cond_0
    move v0, v2

    .line 307
    goto :goto_0

    :cond_1
    move v1, v2

    .line 311
    goto :goto_1
.end method

.method private static a(Lgds;Lgec;)V
    .locals 1

    .prologue
    .line 402
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    invoke-interface {p1}, Lgec;->f()Landroid/view/SurfaceHolder;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 405
    invoke-interface {p1}, Lgec;->f()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {p0, v0}, Lgds;->a(Landroid/view/SurfaceHolder;)V

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 406
    :cond_1
    invoke-interface {p1}, Lgec;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    invoke-interface {p1}, Lgec;->e()Landroid/view/Surface;

    move-result-object v0

    invoke-interface {p0, v0}, Lgds;->a(Landroid/view/Surface;)V

    goto :goto_0
.end method

.method static synthetic a(Lgdu;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-static {}, Lezp;->a()V

    iput-boolean v0, p0, Lgdu;->j:Z

    iput-boolean v0, p0, Lgdu;->k:Z

    invoke-direct {p0, v0}, Lgdu;->b(Z)V

    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgds;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lgdu;->m:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lgdu;->n:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lgdu;->D:Lgei;

    const/4 v2, 0x4

    invoke-interface {v1, v2}, Lgei;->a(I)V

    :cond_0
    invoke-interface {v0}, Lgds;->d()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lgdu;I)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgds;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lgdu;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lgdu;->D:Lgei;

    invoke-interface {v1, p1}, Lgei;->b(I)V

    invoke-interface {v0, p1}, Lgds;->a(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lgdu;->D:Lgei;

    invoke-interface {v0, p1}, Lgei;->b(I)V

    iget-object v0, p0, Lgdu;->s:Lfqj;

    invoke-direct {p0, v0, p1}, Lgdu;->a(Lfqj;I)V

    iget-object v0, p0, Lgdu;->D:Lgei;

    invoke-interface {v0, p1}, Lgei;->c(I)V

    goto :goto_0
.end method

.method static synthetic a(Lgdu;Lfqj;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lgdu;->b(Lfqj;)V

    return-void
.end method

.method static synthetic a(Lgdu;Lfqj;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lgdu;->a(Lfqj;I)V

    return-void
.end method

.method static synthetic a(Lgdu;Lgds;Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 40
    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    :try_start_0
    iget-boolean v0, p0, Lgdu;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgdu;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lgdu;->D:Lgei;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lgei;->a(I)V

    :cond_0
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-static {p1, v0}, Lgdu;->a(Lgds;Lgec;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "x-disconnect-at-highwatermark"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "User-Agent"

    iget-object v2, p0, Lgdu;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lgdu;->c:Landroid/content/Context;

    invoke-interface {p1, v1, p2, v0}, Lgds;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    invoke-interface {p1}, Lgds;->a()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgdu;->b(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Media Player error preparing video"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lgdu;->D:Lgei;

    new-instance v2, Lggp;

    const-string v3, "android.fw.prepare"

    invoke-direct {v2, v3, v5, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    invoke-interface {v1, v2}, Lgei;->a(Lggp;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Media Player error preparing video"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lgdu;->D:Lgei;

    new-instance v2, Lggp;

    const-string v3, "android.fw.ise"

    invoke-direct {v2, v3, v5, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    invoke-interface {v1, v2}, Lgei;->a(Lggp;)V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2b

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Media Player null pointer preparing video "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lgdu;->D:Lgei;

    new-instance v1, Lggp;

    const-string v2, "android.fw.npe"

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    invoke-direct {v1, v2, v5, v3}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    invoke-interface {v0, v1}, Lgei;->a(Lggp;)V

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 651
    iget-boolean v0, p0, Lgdu;->j:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lgdu;->w:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgdu;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lgdu;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lgdu;->v:Z

    return p1
.end method

.method static synthetic b(Lgdu;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lgdu;->A:I

    return p1
.end method

.method static synthetic b(Lgdu;Lfqj;)Lfqj;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lgdu;->r:Lfqj;

    return-object v0
.end method

.method private b(Lfqj;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 330
    invoke-static {}, Lezp;->a()V

    .line 331
    iget-object v0, p0, Lgdu;->e:Lgdz;

    invoke-virtual {v0}, Lgdz;->a()V

    .line 332
    iget-wide v0, p1, Lfqj;->c:J

    long-to-int v0, v0

    iput v0, p0, Lgdu;->A:I

    .line 333
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-interface {v0}, Lgec;->c()V

    .line 334
    iget-boolean v0, p0, Lgdu;->v:Z

    if-nez v0, :cond_0

    .line 335
    iput-object p1, p0, Lgdu;->r:Lfqj;

    .line 337
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-interface {v0}, Lgec;->d()V

    .line 354
    :goto_0
    return-void

    .line 342
    :cond_0
    :try_start_0
    iget-object v0, p0, Lgdu;->g:Lgdy;

    iget-boolean v1, p0, Lgdu;->i:Z

    invoke-interface {v0, p1, v1}, Lgdy;->a(Lfqj;Z)Lgds;

    move-result-object v0

    .line 343
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lgds;->b(I)V

    .line 344
    iget-object v1, p0, Lgdu;->y:Lgdw;

    invoke-interface {v0, v1}, Lgds;->a(Lgdt;)V

    .line 345
    iget-object v1, p0, Lgdu;->D:Lgei;

    new-instance v2, Lgct;

    invoke-direct {v2}, Lgct;-><init>()V

    invoke-interface {v1, v2}, Lgei;->a(Lgcs;)V

    .line 346
    iget-object v1, p0, Lgdu;->e:Lgdz;

    iget-object v2, p0, Lgdu;->t:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lfqj;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v2, v3, v0

    iget-object v0, v1, Lgdz;->a:Landroid/os/Handler;

    iget-object v1, v1, Lgdz;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 348
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgdu;->c(Z)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 349
    :catch_0
    move-exception v0

    .line 350
    const-string v1, "Factory failed to create a MediaPlayer for the stream"

    invoke-static {v1}, Lezp;->b(Ljava/lang/String;)V

    .line 351
    iget-object v1, p0, Lgdu;->D:Lgei;

    new-instance v2, Lggp;

    const-string v3, "android.fw.create"

    invoke-direct {v2, v3, v5, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    invoke-interface {v1, v2}, Lgei;->a(Lggp;)V

    goto :goto_0
.end method

.method static synthetic b(Lgdu;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 40
    iput-boolean v0, p0, Lgdu;->x:Z

    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgds;

    if-eqz v0, :cond_3

    invoke-static {}, Lezp;->a()V

    :try_start_0
    iget-boolean v1, p0, Lgdu;->i:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lgdu;->k:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lgdu;->j:Z

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lgds;->b()V

    iget-object v0, p0, Lgdu;->u:Lgec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdu;->u:Lgec;

    const/16 v1, 0x1f4

    invoke-interface {v0, v1}, Lgec;->a(I)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdu;->k:Z

    :cond_1
    iget-boolean v0, p0, Lgdu;->n:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lgdu;->j:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lgdu;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgdu;->D:Lgei;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lgei;->a(I)V

    :cond_2
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgdu;->n:Z

    :cond_3
    :goto_1
    return-void

    :cond_4
    invoke-direct {p0}, Lgdu;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lgds;->b()V

    iget-object v0, p0, Lgdu;->u:Lgec;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgdu;->u:Lgec;

    const/16 v1, 0x1f4

    invoke-interface {v0, v1}, Lgec;->a(I)V

    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdu;->k:Z

    iget-boolean v0, p0, Lgdu;->n:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lgdu;->D:Lgei;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lgei;->a(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method static synthetic b(Lgdu;Z)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgdu;->c(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 655
    iget-boolean v0, p0, Lgdu;->l:Z

    if-eq v0, p1, :cond_0

    .line 656
    iput-boolean p1, p0, Lgdu;->l:Z

    .line 657
    iget-object v1, p0, Lgdu;->D:Lgei;

    iget-boolean v0, p0, Lgdu;->l:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    :goto_0
    invoke-interface {v1, v0}, Lgei;->a(I)V

    .line 660
    :cond_0
    return-void

    .line 657
    :cond_1
    const/4 v0, 0x6

    goto :goto_0
.end method

.method static synthetic c(Lgdu;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lgdu;->B:I

    return p1
.end method

.method static synthetic c(Lgdu;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 40
    iget-object v0, p0, Lgdu;->C:Lgea;

    invoke-virtual {v0}, Lgea;->b()V

    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgds;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lgdu;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lezp;->a()V

    :try_start_0
    invoke-interface {v0}, Lgds;->c()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lgdu;->k:Z

    iget-object v0, p0, Lgdu;->D:Lgei;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lgei;->a(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgdu;->b(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lgdu;->x:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdu;->x:Z

    iget-object v0, p0, Lgdu;->D:Lgei;

    invoke-interface {v0, v2}, Lgei;->a(I)V

    goto :goto_0
.end method

.method static synthetic c(Lgdu;Lfqj;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lgdu;->a(Lfqj;)V

    return-void
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 677
    iget-object v0, p0, Lgdu;->u:Lgec;

    if-eqz v0, :cond_0

    .line 678
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "stayAwake: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 679
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-interface {v0, p1}, Lgec;->a(Z)V

    .line 681
    :cond_0
    return-void
.end method

.method static synthetic c(Lgdu;Z)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdu;->j:Z

    return v0
.end method

.method static synthetic d(Lgdu;I)I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lgdu;->z:I

    return v0
.end method

.method static synthetic d(Lgdu;)Lfqj;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgdu;->r:Lfqj;

    return-object v0
.end method

.method static synthetic d(Lgdu;Z)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdu;->h:Z

    return v0
.end method

.method static synthetic e(Lgdu;)Lgec;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgdu;->u:Lgec;

    return-object v0
.end method

.method static synthetic e(Lgdu;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lgdu;->m:Z

    return p1
.end method

.method static synthetic f(Lgdu;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgdu;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lgdu;Z)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lgdu;->b(Z)V

    return-void
.end method

.method static synthetic g(Lgdu;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgdu;->f:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic g(Lgdu;Z)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgdu;->o:Z

    return v0
.end method

.method static synthetic h(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgdu;->h:Z

    return v0
.end method

.method static synthetic h(Lgdu;Z)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgdu;->k:Z

    return v0
.end method

.method static synthetic i(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lgdu;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic i(Lgdu;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lgdu;->n:Z

    return p1
.end method

.method static synthetic j(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgdu;->i:Z

    return v0
.end method

.method static synthetic k(Lgdu;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lgdu;->z:I

    return v0
.end method

.method static synthetic l(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgdu;->m:Z

    return v0
.end method

.method static synthetic m(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgdu;->n:Z

    return v0
.end method

.method static synthetic n(Lgdu;)Lgei;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgdu;->D:Lgei;

    return-object v0
.end method

.method static synthetic o(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgdu;->x:Z

    return v0
.end method

.method static synthetic p(Lgdu;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lgdu;->B:I

    return v0
.end method

.method static synthetic q(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgdu;->k:Z

    return v0
.end method

.method static synthetic r(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgdu;->l:Z

    return v0
.end method

.method static synthetic s(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgdu;->o:Z

    return v0
.end method

.method static synthetic t(Lgdu;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgdu;->j:Z

    return v0
.end method

.method static synthetic u(Lgdu;)Lgea;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgdu;->C:Lgea;

    return-object v0
.end method

.method static synthetic v(Lgdu;)Lfqj;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgdu;->s:Lfqj;

    return-object v0
.end method

.method static synthetic w(Lgdu;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgds;

    .line 595
    if-eqz v0, :cond_0

    .line 596
    invoke-interface {v0, p1, p1}, Lgds;->a(FF)V

    .line 598
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lgdu;->D:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Landroid/os/Handler;)V

    .line 170
    return-void
.end method

.method public final a(Lfrf;ILjava/lang/String;Lfqy;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 192
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrf;

    iput-object v0, p0, Lgdu;->p:Lfrf;

    .line 193
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqy;

    iput-object v0, p0, Lgdu;->q:Lfqy;

    .line 194
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgdu;->t:Ljava/lang/String;

    .line 198
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p4, v0}, Lgdu;->b(Lfrf;Lfqy;Z)Lger;
    :try_end_0
    .catch Lgej; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 204
    iget-object v0, v2, Lger;->a:[Lfqj;

    aget-object v1, v0, v6

    .line 205
    iget-object v3, v2, Lger;->c:Lfqj;

    .line 206
    iget-object v0, p0, Lgdu;->D:Lgei;

    .line 210
    iget-object v4, v2, Lger;->d:[Lfre;

    .line 211
    iget-object v5, v2, Lger;->e:[Ljava/lang/String;

    move-object v2, v1

    .line 206
    invoke-interface/range {v0 .. v6}, Lgei;->a(Lfqj;Lfqj;Lfqj;[Lfre;[Ljava/lang/String;I)V

    .line 213
    iget-object v0, p0, Lgdu;->D:Lgei;

    invoke-interface {v0}, Lgei;->a()V

    .line 214
    iget-boolean v0, p1, Lfrf;->h:Z

    if-eqz v0, :cond_0

    .line 215
    invoke-direct {p0, v1}, Lgdu;->a(Lfqj;)V

    .line 219
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    iget-object v1, p0, Lgdu;->D:Lgei;

    new-instance v2, Lggp;

    const-string v3, "fmt.noneavailable"

    invoke-direct {v2, v3, v6, v0}, Lggp;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    invoke-interface {v1, v2}, Lgei;->a(Lggp;)V

    goto :goto_0

    .line 217
    :cond_0
    invoke-direct {p0, v1, p2}, Lgdu;->a(Lfqj;I)V

    goto :goto_0
.end method

.method public final a(Lgec;)V
    .locals 1

    .prologue
    .line 610
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    iput-object p1, p0, Lgdu;->u:Lgec;

    .line 612
    iget-object v0, p0, Lgdu;->y:Lgdw;

    invoke-interface {p1, v0}, Lgec;->a(Lged;)V

    .line 613
    invoke-interface {p1}, Lgec;->h()Z

    move-result v0

    iput-boolean v0, p0, Lgdu;->v:Z

    .line 614
    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgds;

    .line 615
    if-eqz v0, :cond_0

    .line 616
    invoke-static {v0, p1}, Lgdu;->a(Lgds;Lgec;)V

    .line 618
    :cond_0
    iget-boolean v0, p0, Lgdu;->k:Z

    if-eqz v0, :cond_1

    .line 619
    const/16 v0, 0x1f4

    invoke-interface {p1, v0}, Lgec;->a(I)V

    .line 621
    :cond_1
    iget-boolean v0, p0, Lgdu;->k:Z

    invoke-direct {p0, v0}, Lgdu;->c(Z)V

    .line 622
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lgdu;->C:Lgea;

    invoke-virtual {v0}, Lgea;->quit()Z

    .line 669
    iget-object v0, p0, Lgdu;->e:Lgdz;

    invoke-virtual {v0}, Lgdz;->quit()Z

    .line 670
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgdu;->c(Z)V

    .line 671
    if-eqz p1, :cond_0

    iget-object v0, p0, Lgdu;->u:Lgec;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-interface {v0}, Lgec;->g()V

    .line 674
    :cond_0
    return-void
.end method

.method public final a(Lfrf;Lfqy;Z)[Lfqj;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 224
    iget-boolean v0, p1, Lfrf;->h:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lfrf;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    :cond_0
    sget-object v0, Lges;->a:[Lfqj;

    .line 233
    :goto_0
    return-object v0

    .line 229
    :cond_1
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lgdu;->b(Lfrf;Lfqy;Z)Lger;
    :try_end_0
    .catch Lgej; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 233
    const/4 v0, 0x1

    new-array v0, v0, [Lfqj;

    iget-object v1, v1, Lger;->a:[Lfqj;

    aget-object v1, v1, v2

    aput-object v1, v0, v2

    goto :goto_0

    .line 231
    :catch_0
    move-exception v0

    sget-object v0, Lges;->a:[Lfqj;

    goto :goto_0
.end method

.method public final b(Lfrf;Lfqy;Z)Lger;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 240
    if-eqz p3, :cond_0

    .line 241
    new-instance v0, Lgej;

    invoke-direct {v0}, Lgej;-><init>()V

    throw v0

    .line 243
    :cond_0
    iget-object v0, p0, Lgdu;->F:Lges;

    .line 246
    invoke-static {}, Lfqm;->l()Ljava/util/Set;

    move-result-object v3

    .line 247
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    const/4 v6, 0x1

    move-object v1, p2

    move-object v2, p1

    move v7, v5

    move v8, v5

    .line 243
    invoke-interface/range {v0 .. v8}, Lges;->a(Lfqy;Lfrf;Ljava/util/Set;Ljava/util/Set;ZZZZ)Lger;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 256
    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdu;->p:Lfrf;

    if-nez v0, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lgdu;->p:Lfrf;

    iget-boolean v0, v0, Lfrf;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lgdu;->p:Lfrf;

    invoke-virtual {v0}, Lfrf;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 265
    :try_start_0
    iget-object v0, p0, Lgdu;->p:Lfrf;

    iget-object v1, p0, Lgdu;->q:Lfqy;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lgdu;->b(Lfrf;Lfqy;Z)Lger;
    :try_end_0
    .catch Lgej; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 269
    iget-object v0, v2, Lger;->a:[Lfqj;

    aget-object v1, v0, v3

    .line 270
    iget-object v0, p0, Lgdu;->s:Lfqj;

    invoke-virtual {v1, v0}, Lfqj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    iget-object v3, v2, Lger;->c:Lfqj;

    .line 274
    iget-object v0, p0, Lgdu;->D:Lgei;

    .line 278
    iget-object v4, v2, Lger;->d:[Lfre;

    .line 279
    iget-object v5, v2, Lger;->e:[Ljava/lang/String;

    const/4 v6, 0x1

    move-object v2, v1

    .line 274
    invoke-interface/range {v0 .. v6}, Lgei;->a(Lfqj;Lfqj;Lfqj;[Lfre;[Ljava/lang/String;I)V

    .line 281
    iget-object v0, p0, Lgdu;->D:Lgei;

    invoke-interface {v0}, Lgei;->b()V

    .line 282
    invoke-virtual {p0}, Lgdu;->i()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lgdu;->a(Lfqj;I)V

    goto :goto_0

    .line 267
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 525
    iget-boolean v0, p0, Lgdu;->i:Z

    if-nez v0, :cond_0

    iget v0, p0, Lgdu;->z:I

    if-eq v0, p1, :cond_0

    .line 526
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdu;->o:Z

    .line 527
    iput p1, p0, Lgdu;->z:I

    .line 528
    iget-object v0, p0, Lgdu;->e:Lgdz;

    iget v1, p0, Lgdu;->A:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, v0, Lgdz;->a:Landroid/os/Handler;

    iget-object v0, v0, Lgdz;->a:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-static {v0, v3, v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 530
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lgdu;->D:Lgei;

    invoke-interface {v0, p1}, Lgei;->b(Landroid/os/Handler;)V

    .line 178
    return-void
.end method

.method public final c()Lfqj;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lgdu;->s:Lfqj;

    return-object v0
.end method

.method public final d()Lfqj;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lgdu;->s:Lfqj;

    return-object v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lgdu;->e:Lgdz;

    iget-object v0, v0, Lgdz;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 445
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgdu;->c(Z)V

    .line 446
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lgdu;->e:Lgdz;

    iget-object v0, v0, Lgdz;->a:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 494
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgdu;->c(Z)V

    .line 495
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lgdu;->C:Lgea;

    invoke-virtual {v0}, Lgea;->b()V

    .line 555
    iget-object v0, p0, Lgdu;->e:Lgdz;

    invoke-virtual {v0}, Lgdz;->a()V

    .line 556
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgdu;->c(Z)V

    .line 557
    const/4 v0, 0x0

    iput-object v0, p0, Lgdu;->p:Lfrf;

    .line 558
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lgdu;->C:Lgea;

    invoke-virtual {v0}, Lgea;->b()V

    .line 563
    iget-object v0, p0, Lgdu;->e:Lgdz;

    invoke-virtual {v0}, Lgdz;->b()V

    .line 564
    const/4 v0, 0x0

    iput-object v0, p0, Lgdu;->p:Lfrf;

    .line 565
    return-void
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgds;

    .line 426
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lgdu;->j:Z

    if-eqz v1, :cond_0

    .line 427
    invoke-interface {v0}, Lgds;->e()I

    move-result v0

    iput v0, p0, Lgdu;->z:I

    .line 429
    :cond_0
    iget v0, p0, Lgdu;->z:I

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 434
    iget v0, p0, Lgdu;->A:I

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 439
    iget v0, p0, Lgdu;->B:I

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iget v1, p0, Lgdu;->A:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 602
    iget-boolean v0, p0, Lgdu;->l:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgdu;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lgdu;->u:Lgec;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-interface {v0}, Lgec;->c()V

    .line 590
    :cond_0
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 629
    iget-object v0, p0, Lgdu;->u:Lgec;

    if-eqz v0, :cond_1

    .line 630
    invoke-direct {p0, v2}, Lgdu;->c(Z)V

    .line 631
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-interface {v0}, Lgec;->c()V

    .line 632
    iget-object v0, p0, Lgdu;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgds;

    .line 633
    if-eqz v0, :cond_0

    .line 634
    invoke-interface {v0, v1}, Lgds;->a(Landroid/view/Surface;)V

    .line 635
    invoke-interface {v0, v1}, Lgds;->a(Landroid/view/SurfaceHolder;)V

    .line 637
    :cond_0
    iput-boolean v2, p0, Lgdu;->v:Z

    .line 638
    iget-object v0, p0, Lgdu;->u:Lgec;

    invoke-interface {v0, v1}, Lgec;->a(Lged;)V

    .line 641
    invoke-virtual {p0}, Lgdu;->h()V

    .line 642
    iput-object v1, p0, Lgdu;->u:Lgec;

    .line 644
    :cond_1
    return-void
.end method
