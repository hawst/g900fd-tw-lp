.class public final Lcvd;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Z

.field c:Z

.field private final d:Lcwr;

.field private final e:Levn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcwr;Levn;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 30
    iput-object p1, p0, Lcvd;->a:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcvd;->d:Lcwr;

    .line 32
    iput-object p3, p0, Lcvd;->e:Levn;

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvd;->c:Z

    .line 37
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 61
    const-string v0, "Audio becoming noisy. Pausing if needed"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 62
    iget-boolean v0, p0, Lcvd;->c:Z

    if-nez v0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-object v0, p0, Lcvd;->d:Lcwr;

    invoke-interface {v0}, Lcwr;->a()Lcvq;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    .line 69
    invoke-interface {v0}, Lcvq;->i()V

    .line 70
    iget-object v0, p0, Lcvd;->e:Levn;

    new-instance v1, Lcza;

    invoke-direct {v1}, Lcza;-><init>()V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method
