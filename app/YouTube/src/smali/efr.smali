.class public final Lefr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field b:Left;

.field public c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {p1}, Legz;->a(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lefr;->a:Ljava/util/concurrent/ExecutorService;

    .line 101
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 149
    iget-boolean v0, p0, Lefr;->c:Z

    invoke-static {v0}, La;->c(Z)V

    .line 150
    iget-object v0, p0, Lefr;->b:Left;

    iget-object v1, v0, Left;->a:Lefu;

    invoke-interface {v1}, Lefu;->d()V

    iget-object v1, v0, Left;->b:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v0, v0, Left;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 151
    :cond_0
    return-void
.end method

.method public final a(Lefu;Lefs;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 112
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    .line 113
    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 114
    iget-boolean v0, p0, Lefr;->c:Z

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, La;->c(Z)V

    iput-boolean v1, p0, Lefr;->c:Z

    new-instance v0, Left;

    invoke-direct {v0, p0, v3, p1, p2}, Left;-><init>(Lefr;Landroid/os/Looper;Lefu;Lefs;)V

    iput-object v0, p0, Lefr;->b:Left;

    iget-object v0, p0, Lefr;->a:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lefr;->b:Left;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 115
    return-void

    :cond_1
    move v0, v2

    .line 113
    goto :goto_0
.end method
