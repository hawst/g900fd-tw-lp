.class public final Lbbp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfhz;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private final b:Levn;

.field private final c:Lglm;

.field private final d:Lftt;

.field private final e:Lgjp;

.field private final f:Lgoc;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Levn;Lglm;Lftt;Lgjp;Lgoc;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 77
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lbbp;->b:Levn;

    .line 78
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglm;

    iput-object v0, p0, Lbbp;->c:Lglm;

    .line 79
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftt;

    iput-object v0, p0, Lbbp;->d:Lftt;

    .line 80
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lbbp;->e:Lgjp;

    .line 81
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoc;

    iput-object v0, p0, Lbbp;->f:Lgoc;

    .line 82
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 156
    :try_start_0
    iget-object v0, p0, Lbbp;->f:Lgoc;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lgod;->h:Lgod;

    invoke-virtual {v0, v1, v2}, Lgoc;->a(Landroid/net/Uri;Lgod;)Landroid/net/Uri;
    :try_end_0
    .catch Lfax; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 159
    :goto_0
    return-object v0

    .line 158
    :catch_0
    move-exception v0

    const-string v1, "Failed macro substitution for URI: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 159
    const/4 v0, 0x0

    goto :goto_0

    .line 158
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a([Lhls;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 134
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 152
    :cond_0
    return-void

    .line 138
    :cond_1
    array-length v2, p1

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 139
    if-eqz v3, :cond_2

    iget-object v4, v3, Lhls;->b:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 140
    iget-object v4, v3, Lhls;->b:Ljava/lang/String;

    invoke-direct {p0, v4}, Lbbp;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 141
    if-eqz v4, :cond_2

    .line 142
    iget-object v5, p0, Lbbp;->e:Lgjp;

    const-string v5, "appendpointlogging"

    const v6, 0x323467f

    invoke-static {v5, v6}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v5

    .line 144
    invoke-virtual {v5, v4}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    .line 145
    iput-boolean v1, v5, Lgjt;->d:Z

    .line 146
    new-instance v4, Lbbr;

    invoke-direct {v4, v3}, Lbbr;-><init>(Lhls;)V

    iput-object v4, v5, Lgjt;->i:Lgkm;

    .line 147
    iget-object v3, p0, Lbbp;->e:Lgjp;

    sget-object v4, Lggu;->b:Lwu;

    invoke-virtual {v3, v5, v4}, Lgjp;->a(Lgjt;Lwu;)V

    .line 138
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Lhog;)[B
    .locals 1

    .prologue
    .line 272
    if-eqz p0, :cond_0

    iget-object v0, p0, Lhog;->a:[B

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lhog;->a:[B

    .line 275
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lfhy;->a:[B

    goto :goto_0
.end method


# virtual methods
.method public final a(Lhog;Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0, v1}, Lbbp;->a(Lhog;Ljava/lang/Object;ILandroid/os/Bundle;)V

    .line 94
    return-void
.end method

.method public final a(Lhog;Ljava/lang/Object;ILandroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 116
    iget-object v2, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    and-int/lit8 v2, p3, 0x1

    if-eqz v2, :cond_1

    move v4, v1

    :goto_0
    and-int/lit8 v2, p3, 0x2

    if-nez v2, :cond_2

    move v3, v1

    :goto_1
    if-eqz p4, :cond_3

    move v2, v1

    :goto_2
    :try_start_0
    iget-object v5, p1, Lhog;->k:Lhyn;

    if-eqz v5, :cond_4

    iget-object v0, p1, Lhog;->k:Lhyn;

    iget-object v0, v0, Lhyn;->a:Ljava/lang/String;

    invoke-static {v0}, La;->M(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lbby;

    iget-object v3, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v3, v2}, Lbby;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 121
    :goto_3
    if-eqz v0, :cond_0

    .line 122
    iget-object v2, p1, Lhog;->b:[Lhls;

    invoke-direct {p0, v2}, Lbbp;->a([Lhls;)V

    .line 123
    invoke-interface {v0}, Lbbv;->a()V

    .line 124
    iget-object v0, p0, Lbbp;->b:Levn;

    new-instance v2, Lbbq;

    invoke-direct {v2, p1}, Lbbq;-><init>(Lhog;)V

    invoke-virtual {v0, v2}, Levn;->c(Ljava/lang/Object;)V

    .line 125
    iget-object v0, p0, Lbbp;->b:Levn;

    new-instance v2, Lazy;

    invoke-direct {v2}, Lazy;-><init>()V

    invoke-virtual {v0, v2}, Levn;->d(Ljava/lang/Object;)V

    .line 131
    :cond_0
    :goto_4
    return-void

    :cond_1
    move v4, v0

    .line 118
    goto :goto_0

    :cond_2
    move v3, v0

    goto :goto_1

    :cond_3
    move v2, v0

    goto :goto_2

    :cond_4
    iget-object v5, p1, Lhog;->x:Lgzp;

    if-eqz v5, :cond_5

    iget-object v0, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, p1}, La;->c(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lhog;)Lbbv;

    move-result-object v0

    goto :goto_3

    :cond_5
    iget-object v5, p1, Lhog;->s:Lhpa;

    if-eqz v5, :cond_6

    invoke-static {p1}, La;->a(Lhog;)Lbgh;

    move-result-object v2

    new-instance v0, Lbbw;

    iget-object v3, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v3, v2}, Lbbw;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lbgh;)V
    :try_end_0
    .catch Lcsp; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 127
    :catch_0
    move-exception v0

    .line 129
    iget-object v2, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcsp;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Leze;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    goto :goto_4

    .line 118
    :cond_6
    :try_start_1
    iget-object v5, p1, Lhog;->w:Lhpm;

    if-eqz v5, :cond_8

    iget-object v2, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lgoh;

    invoke-direct {v4, p1}, Lgoh;-><init>(Lhog;)V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lgoh;->a(Z)V

    new-instance v5, Lgom;

    invoke-direct {v5, v4}, Lgom;-><init>(Lgoh;)V

    if-nez v3, :cond_7

    move v0, v1

    :cond_7
    invoke-virtual {v5, v0}, Lgom;->a(Z)V

    new-instance v0, Lbbz;

    invoke-direct {v0, v2, v5}, Lbbz;-><init>(Landroid/app/Activity;Lgom;)V

    goto :goto_3

    :cond_8
    iget-object v5, p0, Lbbp;->c:Lglm;

    invoke-interface {v5}, Lglm;->a()Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v0, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v2, 0x7f0901d9

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Leze;->a(Landroid/content/Context;II)V

    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_9
    iget-object v5, p1, Lhog;->n:Lhap;

    if-eqz v5, :cond_a

    new-instance v0, Lcsp;

    const-string v2, "Settings not supported"

    invoke-direct {v0, v2}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    iget-object v5, p1, Lhog;->y:Lhao;

    if-eqz v5, :cond_b

    iget-object v2, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p1, Lhog;->y:Lhao;

    new-instance v0, Lbbs;

    iget-object v4, v3, Lhao;->a:Ljava/lang/String;

    iget-object v3, v3, Lhao;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v4, v3}, Lbbs;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_b
    iget-object v5, p1, Lhog;->c:Lhbm;

    if-eqz v5, :cond_c

    iget-object v2, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lhog;->c:Lhbm;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1, v4}, La;->a(Lhog;Z)Lbgh;

    move-result-object v3

    new-instance v0, Lbbw;

    invoke-direct {v0, v2, v3}, Lbbw;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lbgh;)V

    goto/16 :goto_3

    :cond_c
    iget-object v4, p1, Lhog;->q:Lhbz;

    if-eqz v4, :cond_d

    new-instance v0, Lcsp;

    const-string v2, "Capture not supported"

    invoke-direct {v0, v2}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget-object v4, p1, Lhog;->d:Lhfd;

    if-eqz v4, :cond_e

    new-instance v0, Lcsp;

    const-string v2, "Create Channel not supported"

    invoke-direct {v0, v2}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    iget-object v4, p1, Lhog;->A:Lhgd;

    if-eqz v4, :cond_f

    iget-object v0, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, p1}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lhog;)Lbbv;

    move-result-object v0

    goto/16 :goto_3

    :cond_f
    iget-object v4, p1, Lhog;->f:Lhja;

    if-eqz v4, :cond_10

    new-instance v0, Lcsp;

    const-string v2, "Capture not supported"

    invoke-direct {v0, v2}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    iget-object v4, p1, Lhog;->p:Lhnu;

    if-eqz v4, :cond_14

    iget-object v2, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lhog;->p:Lhnu;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnu;

    iget-object v0, v0, Lhnu;->a:Ljava/lang/String;

    const-string v3, "uploads"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-static {}, La;->d()Lbgh;

    move-result-object v3

    invoke-virtual {v3, p1}, Lbgh;->a(Lhog;)V

    new-instance v0, Lbbw;

    invoke-direct {v0, v2, v3}, Lbbw;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lbgh;)V

    goto/16 :goto_3

    :cond_11
    const-string v2, "purchases"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    const-string v0, "Purchases V2 User Feed requested but not supported"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    new-instance v0, Lcsp;

    const-string v2, "Purchases V2 User Feed requested but not supported"

    invoke-direct {v0, v2}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    new-instance v2, Lcsp;

    const-string v3, "Unknown V2 User Feed requested: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_13

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-direct {v2, v0}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_13
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_14
    iget-object v4, p1, Lhog;->j:Lhtf;

    if-eqz v4, :cond_15

    new-instance v0, Lcsp;

    const-string v2, "Purchases not supported"

    invoke-direct {v0, v2}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    iget-object v4, p1, Lhog;->g:Lhuh;

    if-eqz v4, :cond_16

    new-instance v0, Lbbx;

    iget-object v2, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v2, p1}, Lbbx;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lhog;)V

    goto/16 :goto_3

    :cond_16
    iget-object v4, p1, Lhog;->l:Lhvu;

    if-eqz v4, :cond_17

    new-instance v0, Lcsp;

    const-string v2, "Sign in not supported"

    invoke-direct {v0, v2}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_17
    iget-object v4, p1, Lhog;->h:Lhwx;

    if-eqz v4, :cond_18

    new-instance v0, Lcsp;

    const-string v2, "Subscription Manager not supported"

    invoke-direct {v0, v2}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_18
    iget-object v4, p1, Lhog;->z:Lhyd;

    if-eqz v4, :cond_19

    iget-object v0, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, p1}, La;->b(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lhog;)Lbbv;

    move-result-object v0

    goto/16 :goto_3

    :cond_19
    iget-object v4, p1, Lhog;->i:Libf;

    if-nez v4, :cond_1a

    iget-object v4, p1, Lhog;->u:Libk;

    if-eqz v4, :cond_1e

    :cond_1a
    if-eqz v2, :cond_1c

    iget-object v2, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lgoh;

    invoke-direct {v0, p1}, Lgoh;-><init>(Lhog;)V

    new-instance v3, Lgom;

    invoke-direct {v3, v0}, Lgom;-><init>(Lgoh;)V

    if-eqz p4, :cond_1b

    invoke-virtual {v3, p4}, Lgom;->a(Landroid/os/Bundle;)V

    :cond_1b
    new-instance v0, Lbbz;

    invoke-direct {v0, v2, v3}, Lbbz;-><init>(Landroid/app/Activity;Lgom;)V

    goto/16 :goto_3

    :cond_1c
    iget-object v2, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lgoh;

    invoke-direct {v4, p1}, Lgoh;-><init>(Lhog;)V

    new-instance v5, Lgom;

    invoke-direct {v5, v4}, Lgom;-><init>(Lgoh;)V

    if-nez v3, :cond_1d

    move v0, v1

    :cond_1d
    invoke-virtual {v5, v0}, Lgom;->a(Z)V

    new-instance v0, Lbbz;

    invoke-direct {v0, v2, v5}, Lbbz;-><init>(Landroid/app/Activity;Lgom;)V

    goto/16 :goto_3

    :cond_1e
    iget-object v0, p1, Lhog;->C:Licv;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lbfw;

    invoke-direct {v2}, Lbfw;-><init>()V

    invoke-static {v0, p1, v2}, Lbbu;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lhog;Li;)Lbbu;

    move-result-object v0

    goto/16 :goto_3

    :cond_1f
    iget-object v0, p1, Lhog;->B:Licd;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lbfo;

    invoke-direct {v2}, Lbfo;-><init>()V

    invoke-static {v0, p1, v2}, Lbbu;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lhog;Li;)Lbbu;

    move-result-object v0

    goto/16 :goto_3

    :cond_20
    new-instance v0, Lcsp;

    const-string v2, "Unknown NavigationData encountered"

    invoke-direct {v0, v2}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcsp; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public final a(Lhut;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 166
    :try_start_0
    iget-object v0, p0, Lbbp;->d:Lftt;

    .line 167
    const/4 v1, 0x0

    iget-object v0, v0, Lftt;->a:Ljava/util/Map;

    invoke-static {p1}, La;->a(Lhut;)Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfts;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2}, Lfts;->a(Lhut;Ljava/lang/Object;)Lbbv;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    .line 168
    iget-object v1, p1, Lhut;->c:[Lhls;

    invoke-direct {p0, v1}, Lbbp;->a([Lhls;)V

    .line 169
    invoke-interface {v0}, Lbbv;->a()V

    .line 173
    :goto_1
    return-void

    .line 167
    :cond_0
    new-instance v0, Lftu;

    const-string v1, "Unknown ServiceData encountered"

    invoke-direct {v0, v1}, Lftu;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lftu; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :catch_0
    move-exception v0

    .line 171
    iget-object v1, p0, Lbbp;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lftu;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Leze;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
