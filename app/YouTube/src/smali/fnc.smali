.class public final Lfnc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lhxf;

.field public final b:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lfnc;->a:Lhxf;

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfnc;->b:Ljava/util/List;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lfnc;->a:Lhxf;

    .line 45
    new-instance v0, Lfnb;

    invoke-direct {v0, p1}, Lfnb;-><init>(Landroid/net/Uri;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfnc;->b:Ljava/util/List;

    .line 46
    return-void
.end method

.method public constructor <init>(Lhxf;)V
    .locals 6

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lfnc;->a:Lhxf;

    .line 32
    if-eqz p1, :cond_0

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lhxf;->b:[Lhxg;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfnc;->b:Ljava/util/List;

    .line 34
    iget-object v1, p1, Lhxf;->b:[Lhxg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 35
    iget-object v4, p0, Lfnc;->b:Ljava/util/List;

    new-instance v5, Lfnb;

    invoke-direct {v5, v3}, Lfnb;-><init>(Lhxg;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfnc;->b:Ljava/util/List;

    .line 40
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(I)Lfnb;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 98
    invoke-virtual {p0}, Lfnc;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 113
    :cond_0
    :goto_0
    return-object v0

    .line 102
    :cond_1
    if-gtz p1, :cond_2

    .line 103
    invoke-virtual {p0}, Lfnc;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lfnc;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnb;

    goto :goto_0

    .line 106
    :cond_2
    iget-object v0, p0, Lfnc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnb;

    .line 107
    iget v2, v0, Lfnb;->b:I

    if-lt v2, p1, :cond_3

    goto :goto_0

    .line 113
    :cond_4
    invoke-virtual {p0}, Lfnc;->c()Lfnb;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(II)Lfnb;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 125
    if-ltz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 126
    if-ltz p2, :cond_2

    :goto_1
    invoke-static {v1}, Lb;->b(Z)V

    .line 128
    invoke-virtual {p0}, Lfnc;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 145
    :cond_0
    return-object v3

    :cond_1
    move v0, v2

    .line 125
    goto :goto_0

    :cond_2
    move v1, v2

    .line 126
    goto :goto_1

    .line 136
    :cond_3
    iget-object v0, p0, Lfnc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnb;

    .line 137
    iget v2, v0, Lfnb;->b:I

    sub-int v2, p1, v2

    .line 138
    iget v5, v0, Lfnb;->c:I

    sub-int v5, p2, v5

    .line 139
    mul-int/2addr v2, v2

    mul-int/2addr v5, v5

    add-int/2addr v2, v5

    .line 140
    if-eqz v3, :cond_4

    if-ge v2, v1, :cond_5

    :cond_4
    move-object v1, v0

    move v0, v2

    :goto_3
    move-object v3, v1

    move v1, v0

    .line 144
    goto :goto_2

    :cond_5
    move v0, v1

    move-object v1, v3

    goto :goto_3
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lfnc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-object v0, p0, Lfnc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lfnc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnb;

    iget v2, v0, Lfnb;->c:I

    iget-object v0, p0, Lfnc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnb;

    iget v0, v0, Lfnb;->b:I

    if-ne v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method final c()Lfnb;
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lfnc;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfnc;->b:Ljava/util/List;

    iget-object v1, p0, Lfnc;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnb;

    goto :goto_0
.end method
