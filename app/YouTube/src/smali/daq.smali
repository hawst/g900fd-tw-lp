.class public final Ldaq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[B

.field public static final b:J


# instance fields
.field private final c:Levn;

.field private final d:Lfet;

.field private final e:Lcwg;

.field private final f:Lgix;

.field private final g:Lgng;

.field private final h:Ljava/util/concurrent/Executor;

.field private final i:Ljava/util/concurrent/Executor;

.field private final j:Lftg;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 43
    sget-object v0, Lfhy;->a:[B

    sput-object v0, Ldaq;->a:[B

    .line 45
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ldaq;->b:J

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object v0, p0, Ldaq;->c:Levn;

    .line 70
    iput-object v0, p0, Ldaq;->d:Lfet;

    .line 71
    iput-object v0, p0, Ldaq;->e:Lcwg;

    .line 72
    iput-object v0, p0, Ldaq;->f:Lgix;

    .line 73
    iput-object v0, p0, Ldaq;->g:Lgng;

    .line 74
    iput-object v0, p0, Ldaq;->h:Ljava/util/concurrent/Executor;

    .line 75
    iput-object v0, p0, Ldaq;->i:Ljava/util/concurrent/Executor;

    .line 76
    iput-object v0, p0, Ldaq;->j:Lftg;

    .line 77
    return-void
.end method

.method public constructor <init>(Levn;Lfet;Lcwg;Lgix;Lgng;Ljava/util/concurrent/Executor;Lftg;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldaq;->c:Levn;

    .line 88
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfet;

    iput-object v0, p0, Ldaq;->d:Lfet;

    .line 89
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    iput-object v0, p0, Ldaq;->e:Lcwg;

    .line 90
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Ldaq;->f:Lgix;

    .line 91
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgng;

    iput-object v0, p0, Ldaq;->g:Lgng;

    .line 92
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Ldaq;->h:Ljava/util/concurrent/Executor;

    .line 93
    new-instance v0, Leva;

    invoke-direct {v0}, Leva;-><init>()V

    iput-object v0, p0, Ldaq;->i:Ljava/util/concurrent/Executor;

    .line 94
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftg;

    iput-object v0, p0, Ldaq;->j:Lftg;

    .line 95
    return-void
.end method

.method static synthetic a(Ldaq;)Levn;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Ldaq;->c:Levn;

    return-object v0
.end method

.method static synthetic a(Ldaq;Ljava/lang/String;Lfrl;)Lfrl;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ldaq;->a(Ljava/lang/String;Lfrl;)Lfrl;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lfrl;)Lfrl;
    .locals 8

    .prologue
    .line 245
    if-nez p2, :cond_2

    const/4 v0, 0x0

    move-object v1, v0

    .line 247
    :goto_0
    if-eqz v1, :cond_1

    .line 251
    :try_start_0
    iget-object v0, p0, Ldaq;->f:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 252
    iget-object v0, p0, Ldaq;->g:Lgng;

    iget-object v2, p0, Ldaq;->f:Lgix;

    .line 253
    invoke-interface {v2}, Lgix;->d()Lgit;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    .line 260
    :goto_1
    iget-wide v2, v1, Lfrf;->i:J

    .line 259
    invoke-interface {v0, p1, v2, v3}, Lgnd;->b(Ljava/lang/String;J)Lgly;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lgly;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 264
    invoke-virtual {v0}, Lgly;->a()Lfqj;

    move-result-object v2

    .line 265
    invoke-virtual {v0}, Lgly;->b()Lfqj;

    move-result-object v3

    .line 263
    const-wide/16 v6, 0x0

    iget-object v0, p2, Lfrl;->a:Lhro;

    iget-object v0, v0, Lhro;->b:Lhwn;

    if-eqz v0, :cond_0

    iget-wide v6, v0, Lhwn;->a:J

    :cond_0
    iget-wide v4, p2, Lfrl;->b:J

    move-object v1, p2

    invoke-virtual/range {v1 .. v7}, Lfrl;->a(Lfqj;Lfqj;JJ)Lfrl;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 271
    :cond_1
    :goto_2
    return-object p2

    .line 246
    :cond_2
    iget-object v0, p2, Lfrl;->d:Lfrf;

    move-object v1, v0

    goto :goto_0

    .line 255
    :cond_3
    :try_start_1
    iget-object v0, p0, Ldaq;->g:Lgng;

    invoke-virtual {v0}, Lgng;->c()Lgnd;
    :try_end_1
    .catch Lidg; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method static synthetic a(Ldaq;Leuc;Lfrl;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Ldaq;->i:Ljava/util/concurrent/Executor;

    new-instance v1, Ldas;

    invoke-direct {v1, p0, p1, p2}, Ldas;-><init>(Ldaq;Leuc;Lfrl;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Ldaq;Leuc;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Ldaq;->i:Ljava/util/concurrent/Executor;

    new-instance v1, Ldat;

    invoke-direct {v1, p0, p1, p2}, Ldat;-><init>(Ldaq;Leuc;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lftj;)Lgky;
    .locals 2

    .prologue
    .line 234
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 235
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    iget-object v0, p0, Ldaq;->c:Levn;

    new-instance v1, Lczm;

    invoke-direct {v1}, Lczm;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 237
    new-instance v0, Ldav;

    invoke-direct {v0, p0, p2, p1}, Ldav;-><init>(Ldaq;Lftj;Ljava/lang/String;)V

    .line 238
    iget-object v1, p0, Ldaq;->d:Lfet;

    invoke-virtual {v1, p2, v0}, Lfet;->a(Lftj;Lwv;)V

    .line 239
    return-object v0
.end method

.method public final a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;II)Lgky;
    .locals 1

    .prologue
    .line 159
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 160
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    invoke-virtual/range {p0 .. p6}, Ldaq;->b(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;II)Lftj;

    move-result-object v0

    .line 171
    invoke-virtual {p0, p1, v0}, Ldaq;->a(Ljava/lang/String;Lftj;)Lgky;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V
    .locals 10

    .prologue
    .line 118
    invoke-static/range {p7 .. p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v9, p0, Ldaq;->h:Ljava/util/concurrent/Executor;

    new-instance v0, Ldar;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Ldar;-><init>(Ldaq;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    invoke-interface {v9, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 140
    return-void
.end method

.method public b(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;II)Lftj;
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Ldaq;->d:Lfet;

    invoke-virtual {v0}, Lfet;->a()Lftj;

    move-result-object v0

    .line 217
    invoke-virtual {v0, p2}, Lftj;->a([B)V

    .line 218
    iput-object p1, v0, Lftj;->a:Ljava/lang/String;

    .line 219
    iput-object p4, v0, Lftj;->c:Ljava/lang/String;

    .line 220
    iput p5, v0, Lftj;->j:I

    .line 221
    iput p6, v0, Lftj;->F:I

    .line 222
    iput-object p3, v0, Lftj;->b:Ljava/lang/String;

    .line 223
    iget-object v1, p0, Ldaq;->e:Lcwg;

    invoke-virtual {v1, v0}, Lcwg;->a(Lftj;)V

    .line 224
    iget-object v1, p0, Ldaq;->j:Lftg;

    invoke-interface {v1, v0}, Lftg;->a(Lftj;)V

    .line 225
    return-object v0
.end method
