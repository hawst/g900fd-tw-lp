.class public final Lcht;
.super Lfsb;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lfsj;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lfvi;

.field private final f:Landroid/view/View;

.field private final g:Lfun;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;ILeyp;Lfhz;Lfun;Lfdw;Lfrz;)V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0, p5, p2, p7, p8}, Lfsb;-><init>(Lfhz;Lfsj;Lfdw;Lfrz;)V

    .line 52
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcht;->b:Lfsj;

    .line 55
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfun;

    iput-object v0, p0, Lcht;->g:Lfun;

    .line 57
    const/4 v0, 0x0

    invoke-static {p1, p3, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcht;->a:Landroid/view/View;

    .line 58
    iget-object v0, p0, Lcht;->a:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcht;->c:Landroid/widget/TextView;

    .line 59
    iget-object v0, p0, Lcht;->a:Landroid/view/View;

    const v1, 0x7f08011d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcht;->d:Landroid/widget/TextView;

    .line 60
    new-instance v1, Lfvi;

    iget-object v0, p0, Lcht;->a:Landroid/view/View;

    const v2, 0x7f0800a4

    .line 61
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p4, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcht;->e:Lfvi;

    .line 62
    iget-object v0, p0, Lcht;->a:Landroid/view/View;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcht;->f:Landroid/view/View;

    .line 64
    iget-object v0, p0, Lcht;->a:Landroid/view/View;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 65
    return-void
.end method

.method private a(Lfsg;Lfmr;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 69
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 70
    iget-object v2, p0, Lcht;->c:Landroid/widget/TextView;

    iget-object v3, p2, Lfmr;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_0

    iget-object v3, p2, Lfmr;->a:Lhvp;

    iget-object v3, v3, Lhvp;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, p2, Lfmr;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v3, p2, Lfmr;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {p2}, Lfmr;->g()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 76
    iget-object v2, p0, Lcht;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lfmr;->g()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v2, p0, Lcht;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    :goto_0
    iget-object v2, p2, Lfmr;->c:Lfnc;

    if-nez v2, :cond_1

    new-instance v2, Lfnc;

    iget-object v3, p2, Lfmr;->a:Lhvp;

    iget-object v3, v3, Lhvp;->b:Lhxf;

    invoke-direct {v2, v3}, Lfnc;-><init>(Lhxf;)V

    iput-object v2, p2, Lfmr;->c:Lfnc;

    :cond_1
    iget-object v2, p2, Lfmr;->c:Lfnc;

    .line 88
    iget-object v3, p0, Lcht;->e:Lfvi;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lfvi;->a(Lfnc;Leyo;)V

    .line 89
    iget-object v3, p0, Lcht;->e:Lfvi;

    invoke-virtual {v2}, Lfnc;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_1
    invoke-virtual {v3, v0}, Lfvi;->a(I)V

    .line 91
    iget-object v0, p0, Lcht;->g:Lfun;

    iget-object v1, p0, Lcht;->b:Lfsj;

    .line 92
    invoke-interface {v1}, Lfsj;->a()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcht;->f:Landroid/view/View;

    .line 94
    iget-object v3, p2, Lfmr;->d:Lfkp;

    if-nez v3, :cond_2

    iget-object v3, p2, Lfmr;->a:Lhvp;

    iget-object v3, v3, Lhvp;->h:Lhvo;

    if-eqz v3, :cond_2

    iget-object v3, p2, Lfmr;->a:Lhvp;

    iget-object v3, v3, Lhvp;->h:Lhvo;

    iget-object v3, v3, Lhvo;->a:Lhne;

    if-eqz v3, :cond_2

    new-instance v3, Lfkp;

    iget-object v4, p2, Lfmr;->a:Lhvp;

    iget-object v4, v4, Lhvp;->h:Lhvo;

    iget-object v4, v4, Lhvo;->a:Lhne;

    invoke-direct {v3, v4, p2}, Lfkp;-><init>(Lhne;Lfqh;)V

    iput-object v3, p2, Lfmr;->d:Lfkp;

    :cond_2
    iget-object v3, p2, Lfmr;->d:Lfkp;

    .line 91
    invoke-virtual {v0, v1, v2, v3, p2}, Lfun;->a(Landroid/view/View;Landroid/view/View;Lfkp;Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lcht;->b:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 78
    :cond_3
    invoke-virtual {p2}, Lfmr;->h()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 81
    iget-object v2, p0, Lcht;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lfmr;->h()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v2, p0, Lcht;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 84
    :cond_4
    iget-object v2, p0, Lcht;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 89
    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    check-cast p2, Lfmr;

    invoke-direct {p0, p1, p2}, Lcht;->a(Lfsg;Lfmr;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    check-cast p2, Lfmr;

    invoke-direct {p0, p1, p2}, Lcht;->a(Lfsg;Lfmr;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    check-cast p2, Lfmr;

    invoke-direct {p0, p1, p2}, Lcht;->a(Lfsg;Lfmr;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
