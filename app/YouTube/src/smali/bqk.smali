.class public final Lbqk;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;
.implements Landroid/graphics/drawable/Drawable$Callback;


# static fields
.field private static final a:Landroid/animation/TimeInterpolator;


# instance fields
.field private final b:Landroid/animation/ValueAnimator;

.field private final c:Ljava/util/HashSet;

.field private d:Lbqm;

.field private e:Lbqm;

.field private f:Lbqm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 66
    new-instance v0, Lbuy;

    invoke-direct {v0}, Lbuy;-><init>()V

    sput-object v0, Lbqk;->a:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Lbqm;I)V
    .locals 4

    .prologue
    .line 115
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 116
    const/4 v0, 0x0

    new-array v0, v0, [I

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    .line 117
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 118
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 119
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 120
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    sget-object v1, Lbqk;->a:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 121
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 122
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 124
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbqk;->c:Ljava/util/HashSet;

    .line 126
    invoke-virtual {p0, p1}, Lbqk;->a(Lbqm;)V

    .line 129
    invoke-direct {p0}, Lbqk;->b()V

    .line 130
    return-void

    .line 117
    nop

    :array_0
    .array-data 4
        0xff
        0x0
    .end array-data
.end method

.method private a()Lbqm;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lbqk;->f:Lbqm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqk;->f:Lbqm;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbqk;->d:Lbqm;

    goto :goto_0
.end method

.method private a(Lbql;)V
    .locals 1

    .prologue
    .line 400
    if-eqz p1, :cond_0

    .line 401
    iget-object v0, p0, Lbqk;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 403
    :cond_0
    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 440
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "previousDrawableHolder must be null in static state."

    invoke-static {v0, v3}, Lb;->d(ZLjava/lang/Object;)V

    .line 442
    iget-object v0, p0, Lbqk;->d:Lbqm;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "currentDrawableHolder must not be null in static state."

    invoke-static {v0, v3}, Lb;->d(ZLjava/lang/Object;)V

    .line 444
    iget-object v0, p0, Lbqk;->f:Lbqm;

    if-nez v0, :cond_2

    :goto_2
    const-string v0, "nextDrawableHolder must be null in static state."

    invoke-static {v1, v0}, Lb;->d(ZLjava/lang/Object;)V

    .line 446
    invoke-direct {p0}, Lbqk;->c()Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    .line 447
    invoke-direct {p0}, Lbqk;->f()Z

    move-result v0

    iget-object v1, p0, Lbqk;->e:Lbqm;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbqk;->d:Lbqm;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbqk;->f:Lbqm;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x38

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "All drawables must be unique. Previous "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", current "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", next "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb;->d(ZLjava/lang/Object;)V

    .line 451
    return-void

    :cond_0
    move v0, v2

    .line 440
    goto :goto_0

    :cond_1
    move v0, v2

    .line 442
    goto :goto_1

    :cond_2
    move v1, v2

    .line 444
    goto :goto_2
.end method

.method private static b(Lbql;)V
    .locals 0

    .prologue
    .line 414
    if-eqz p0, :cond_0

    .line 415
    invoke-interface {p0}, Lbql;->a()V

    .line 417
    :cond_0
    return-void
.end method

.method private b(Lbqm;)V
    .locals 2

    .prologue
    .line 355
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqm;

    iput-object v0, p0, Lbqk;->d:Lbqm;

    .line 356
    iget-object v0, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 357
    iget-object v0, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lbqk;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 358
    iget-object v0, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 359
    return-void
.end method

.method private b(Lbqm;Lbql;)V
    .locals 3

    .prologue
    .line 385
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "handleTransitionToSameDrawable "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 386
    invoke-direct {p0}, Lbqk;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    invoke-static {p2}, Lbqk;->b(Lbql;)V

    .line 388
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbqk;->d(Lbqm;)V

    .line 393
    :goto_0
    return-void

    .line 389
    :cond_0
    invoke-direct {p0}, Lbqk;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 391
    invoke-static {p2}, Lbqk;->b(Lbql;)V

    goto :goto_0

    .line 392
    :cond_1
    invoke-direct {p0}, Lbqk;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 393
    invoke-direct {p0, p2}, Lbqk;->a(Lbql;)V

    goto :goto_0

    .line 395
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "In a bad state."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c(Lbqm;)V
    .locals 2

    .prologue
    .line 362
    iput-object p1, p0, Lbqk;->e:Lbqm;

    .line 363
    if-eqz p1, :cond_0

    .line 364
    iget-object v0, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 365
    iget-object v0, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lbqk;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 366
    iget-object v0, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 368
    :cond_0
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbqk;->d:Lbqm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqk;->f:Lbqm;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lbqm;)V
    .locals 2

    .prologue
    .line 371
    iput-object p1, p0, Lbqk;->f:Lbqm;

    .line 372
    if-eqz p1, :cond_0

    .line 373
    iget-object v0, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 374
    iget-object v0, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lbqk;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 375
    iget-object v0, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 377
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbqk;->d:Lbqm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqk;->f:Lbqm;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqk;->d:Lbqm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqk;->f:Lbqm;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 500
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbqk;->e:Lbqm;

    iget-object v0, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    .line 502
    :goto_0
    iget-object v2, p0, Lbqk;->d:Lbqm;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lbqk;->d:Lbqm;

    iget-object v2, v2, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    .line 504
    :goto_1
    iget-object v5, p0, Lbqk;->f:Lbqm;

    if-eqz v5, :cond_0

    iget-object v1, p0, Lbqk;->f:Lbqm;

    iget-object v1, v1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    .line 507
    :cond_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    if-eq v0, v2, :cond_3

    :cond_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    if-eq v0, v1, :cond_3

    :cond_2
    if-eqz v2, :cond_6

    if-eqz v1, :cond_6

    if-ne v2, v1, :cond_6

    :cond_3
    move v0, v3

    .line 511
    :goto_2
    if-nez v0, :cond_7

    move v0, v3

    :goto_3
    return v0

    :cond_4
    move-object v0, v1

    .line 500
    goto :goto_0

    :cond_5
    move-object v2, v1

    .line 502
    goto :goto_1

    :cond_6
    move v0, v4

    .line 507
    goto :goto_2

    :cond_7
    move v0, v4

    .line 511
    goto :goto_3
.end method


# virtual methods
.method public final a(Lbqm;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136
    invoke-static {}, Lb;->a()V

    .line 140
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 144
    :cond_0
    invoke-direct {p0, v1}, Lbqk;->c(Lbqm;)V

    .line 145
    invoke-direct {p0, p1}, Lbqk;->b(Lbqm;)V

    .line 146
    invoke-direct {p0, v1}, Lbqk;->d(Lbqm;)V

    .line 148
    invoke-direct {p0}, Lbqk;->b()V

    .line 149
    return-void
.end method

.method public final a(Lbqm;Lbql;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 173
    invoke-static {}, Lb;->a()V

    .line 175
    iget-object v0, p0, Lbqk;->d:Lbqm;

    invoke-virtual {p1, v0}, Lbqm;->a(Lbqm;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177
    invoke-direct {p0, p1, p2}, Lbqk;->b(Lbqm;Lbql;)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 186
    :cond_2
    iget-object v0, p0, Lbqk;->d:Lbqm;

    invoke-virtual {p1, v0}, Lbqm;->a(Lbqm;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 188
    invoke-direct {p0}, Lbqk;->b()V

    .line 189
    invoke-direct {p0, p1, p2}, Lbqk;->b(Lbqm;Lbql;)V

    goto :goto_0

    .line 193
    :cond_3
    invoke-direct {p0, p1}, Lbqk;->d(Lbqm;)V

    .line 194
    invoke-direct {p0, p2}, Lbqk;->a(Lbql;)V

    .line 196
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    const-string v3, "previousDrawableHolder must be null in static state."

    invoke-static {v0, v3}, Lb;->d(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbqk;->d:Lbqm;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    const-string v3, "currentDrawableHolder must not be null in static state."

    invoke-static {v0, v3}, Lb;->d(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbqk;->f:Lbqm;

    if-eqz v0, :cond_6

    :goto_3
    const-string v0, "nextDrawableHolder must not be null in static state."

    invoke-static {v1, v0}, Lb;->d(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lbqk;->d()Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    invoke-direct {p0}, Lbqk;->f()Z

    move-result v0

    iget-object v1, p0, Lbqk;->e:Lbqm;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbqk;->d:Lbqm;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbqk;->f:Lbqm;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x38

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "All drawables must be unique. Previous "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", current "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", next "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb;->d(ZLjava/lang/Object;)V

    .line 198
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lbqk;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 196
    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto/16 :goto_2

    :cond_6
    move v1, v2

    goto :goto_3
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lbqk;->d:Lbqm;

    iget-object v0, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 225
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lbqk;->e:Lbqm;

    iget-object v0, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 228
    :cond_0
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 252
    invoke-direct {p0}, Lbqk;->a()Lbqm;

    move-result-object v0

    iget-object v0, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 247
    invoke-direct {p0}, Lbqk;->a()Lbqm;

    move-result-object v0

    iget-object v0, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 242
    const/4 v0, -0x3

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 268
    invoke-virtual {p0}, Lbqk;->invalidateSelf()V

    .line 269
    return-void
.end method

.method public final isStateful()Z
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x1

    return v0
.end method

.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 327
    iget-object v0, p0, Lbqk;->e:Lbqm;

    invoke-direct {p0, v0}, Lbqk;->b(Lbqm;)V

    .line 328
    invoke-direct {p0, v1}, Lbqk;->c(Lbqm;)V

    .line 329
    invoke-direct {p0, v1}, Lbqk;->d(Lbqm;)V

    .line 331
    invoke-direct {p0}, Lbqk;->b()V

    .line 332
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 314
    invoke-direct {p0, v0}, Lbqk;->c(Lbqm;)V

    .line 315
    invoke-direct {p0, v0}, Lbqk;->d(Lbqm;)V

    .line 316
    iget-object v0, p0, Lbqk;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbql;

    invoke-static {v0}, Lbqk;->b(Lbql;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbqk;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 318
    invoke-direct {p0}, Lbqk;->b()V

    .line 319
    const-string v0, "crossFadeAnimator ended"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 320
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 336
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "crossFadeAnimator should never repeat."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 298
    const-string v0, "crossFadeAnimator started"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lbqk;->d:Lbqm;

    invoke-direct {p0, v0}, Lbqk;->c(Lbqm;)V

    .line 303
    iget-object v0, p0, Lbqk;->f:Lbqm;

    invoke-direct {p0, v0}, Lbqk;->b(Lbqm;)V

    .line 304
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbqk;->d(Lbqm;)V

    .line 306
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "previousDrawableHolder must not be null in static state."

    invoke-static {v0, v3}, Lb;->d(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbqk;->d:Lbqm;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "currentDrawableHolder must not be null in static state."

    invoke-static {v0, v3}, Lb;->d(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbqk;->f:Lbqm;

    if-nez v0, :cond_2

    :goto_2
    const-string v0, "nextDrawableHolder must be null in static state."

    invoke-static {v1, v0}, Lb;->d(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lbqk;->e()Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    invoke-direct {p0}, Lbqk;->f()Z

    move-result v0

    iget-object v1, p0, Lbqk;->e:Lbqm;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbqk;->d:Lbqm;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbqk;->f:Lbqm;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x38

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "All drawables must be unique. Previous "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", current "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", next "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb;->d(ZLjava/lang/Object;)V

    .line 307
    return-void

    :cond_0
    move v0, v2

    .line 306
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    .line 286
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lbqk;->e:Lbqm;

    iget-object v1, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 288
    invoke-virtual {p0}, Lbqk;->invalidateSelf()V

    .line 291
    :cond_0
    invoke-direct {p0}, Lbqk;->a()Lbqm;

    move-result-object v1

    .line 292
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    .line 293
    iget-object v3, p0, Lbqk;->e:Lbqm;

    iget-object v0, p0, Lbqk;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbql;

    if-eqz v0, :cond_1

    invoke-interface {v0, v2, v3, v1}, Lbql;->a(FLbqm;Lbqm;)V

    goto :goto_0

    .line 294
    :cond_2
    return-void
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lbqk;->d:Lbqm;

    iget-object v0, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 258
    iget-object v0, p0, Lbqk;->e:Lbqm;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lbqk;->e:Lbqm;

    iget-object v0, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 261
    :cond_0
    iget-object v0, p0, Lbqk;->f:Lbqm;

    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lbqk;->f:Lbqm;

    iget-object v0, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 264
    :cond_1
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0, p2, p3, p4}, Lbqk;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 274
    return-void
.end method

.method public final setAlpha(I)V
    .locals 2

    .prologue
    .line 232
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Set alpha on the inner drawables instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2

    .prologue
    .line 237
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Set color filter on the inner drawables instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setState([I)Z
    .locals 1

    .prologue
    .line 341
    invoke-direct {p0}, Lbqk;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqk;->d:Lbqm;

    iget-object v0, v0, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    invoke-virtual {p0}, Lbqk;->invalidateSelf()V

    .line 343
    const/4 v0, 0x1

    .line 345
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 278
    invoke-virtual {p0, p2}, Lbqk;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 279
    return-void
.end method
