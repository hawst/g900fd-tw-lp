.class public final Lfwa;
.super Lxt;
.source "SourceFile"

# interfaces
.implements Lgkt;


# instance fields
.field private final j:Lfwb;

.field private final k:Lgiq;

.field private final l:Lgix;

.field private final m:Ljava/util/List;

.field private final n:Lghm;

.field private final o:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILfwb;Lwv;Lgiq;Lgix;Ljava/util/List;Lghm;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 57
    const-string v2, ""

    invoke-virtual {p2}, Lfwb;->e()Lorg/json/JSONObject;

    move-result-object v3

    move-object v0, p0

    move v1, p1

    move-object v4, p3

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lxt;-><init>(ILjava/lang/String;Lorg/json/JSONObject;Lwv;Lwu;)V

    .line 58
    iput-object p2, p0, Lfwa;->j:Lfwb;

    .line 59
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgiq;

    iput-object v0, p0, Lfwa;->k:Lgiq;

    .line 60
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lfwa;->l:Lgix;

    .line 61
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lfwa;->m:Ljava/util/List;

    .line 62
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghm;

    iput-object v0, p0, Lfwa;->n:Lghm;

    .line 63
    iput-object p8, p0, Lfwa;->o:Ljava/lang/String;

    .line 64
    return-void
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 8

    .prologue
    .line 130
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    :try_start_0
    const-string v0, "curl "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-virtual {p0}, Lfwa;->c()Ljava/util/Map;

    move-result-object v3

    .line 135
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 136
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x7

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "-H \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lwb; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    const-string v1, "Curl command line not available"

    invoke-static {v1, v0}, Lezp;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 142
    :cond_0
    const-string v0, "-H \"Content-Type:application/json\" "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string v0, "-d \""

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 145
    const-string v0, "\" "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {p0}, Lfwa;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string v0, "Curl commandline"

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 149
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 87
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5f

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lfwa;->n:Lghm;

    invoke-virtual {v1}, Lghm;->f()Landroid/net/Uri;

    move-result-object v1

    .line 89
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, p0, Lfwa;->n:Lghm;

    .line 90
    invoke-virtual {v2}, Lghm;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, p0, Lfwa;->j:Lfwb;

    .line 91
    invoke-virtual {v2}, Lfwb;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "key"

    iget-object v3, p0, Lfwa;->o:Ljava/lang/String;

    .line 92
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "firstPartyProperty"

    const-string v3, "youTube"

    .line 93
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "language"

    .line 94
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 95
    iget-object v0, p0, Lfwa;->j:Lfwb;

    invoke-virtual {v0}, Lfwb;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 96
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 98
    :cond_0
    iget-object v0, p0, Lfwa;->l:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    const-string v0, "onBehalfOf"

    iget-object v1, p0, Lfwa;->l:Lgix;

    .line 101
    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    iget-object v1, v1, Lgit;->b:Lgiv;

    invoke-virtual {v1}, Lgiv;->d()Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 103
    :cond_1
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lwm;)Lwt;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lfwa;->n:Lghm;

    invoke-virtual {v0}, Lghm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lfwa;->j:Lfwb;

    invoke-virtual {v0}, Lfwb;->e()Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v0}, Lfwa;->a(Lorg/json/JSONObject;)V

    .line 112
    :cond_0
    invoke-super {p0, p1}, Lxt;->a(Lwm;)Lwt;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lxa;)Lxa;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lfwa;->n:Lghm;

    invoke-virtual {v0}, Lghm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lfwa;->j:Lfwb;

    invoke-virtual {v0}, Lfwb;->e()Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v0}, Lfwa;->a(Lorg/json/JSONObject;)V

    .line 122
    :cond_0
    invoke-super {p0, p1}, Lxt;->a(Lxa;)Lxa;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 4

    .prologue
    .line 68
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 74
    iget-object v0, p0, Lfwa;->k:Lgiq;

    invoke-virtual {p0}, Lfwa;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lfwa;->h()[B

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lgiq;->a(Ljava/util/Map;Ljava/lang/String;[B)V

    .line 77
    iget-object v0, p0, Lfwa;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkl;

    .line 78
    invoke-interface {v0, v1, p0}, Lgkl;->a(Ljava/util/Map;Lgkt;)V

    goto :goto_0

    .line 81
    :cond_0
    return-object v1
.end method

.method public final k()Lgiv;
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method public final z_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lfwa;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
