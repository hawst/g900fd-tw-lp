.class public final Lfpd;
.super Lgia;
.source "SourceFile"


# instance fields
.field private a:Lfoy;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 2298
    invoke-direct {p0}, Lgia;-><init>()V

    return-void
.end method

.method public constructor <init>(Lfoy;)V
    .locals 0

    .prologue
    .line 2300
    invoke-direct {p0}, Lgia;-><init>()V

    .line 2301
    iput-object p1, p0, Lfpd;->a:Lfoy;

    .line 2302
    return-void
.end method

.method private static f(Lorg/json/JSONObject;Ljava/lang/String;)Lflp;
    .locals 2

    .prologue
    .line 2456
    invoke-static {p0, p1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2457
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2458
    new-instance v0, Lflp;

    invoke-direct {v0}, Lflp;-><init>()V

    .line 2467
    :goto_0
    return-object v0

    .line 2460
    :cond_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 2463
    :try_start_0
    new-instance v1, Leab;

    invoke-direct {v1}, Leab;-><init>()V

    invoke-virtual {v1, v0}, Leab;->b([B)Lida;

    move-result-object v0

    check-cast v0, Leab;
    :try_end_0
    .catch Licz; {:try_start_0 .. :try_end_0} :catch_0

    .line 2467
    new-instance v1, Lflp;

    invoke-direct {v1, v0}, Lflp;-><init>(Leab;)V

    move-object v0, v1

    goto :goto_0

    .line 2465
    :catch_0
    move-exception v0

    new-instance v0, Lflp;

    invoke-direct {v0}, Lflp;-><init>()V

    goto :goto_0
.end method

.method private static g(Lorg/json/JSONObject;Ljava/lang/String;)Lfqy;
    .locals 2

    .prologue
    .line 2476
    invoke-static {p0, p1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2477
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2478
    new-instance v0, Lfqy;

    invoke-direct {v0}, Lfqy;-><init>()V

    .line 2487
    :goto_0
    return-object v0

    .line 2480
    :cond_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 2481
    new-instance v1, Lhre;

    invoke-direct {v1}, Lhre;-><init>()V

    .line 2483
    :try_start_0
    invoke-static {v1, v0}, Lidh;->a(Lidh;[B)Lidh;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 2487
    new-instance v0, Lfqy;

    invoke-direct {v0, v1}, Lfqy;-><init>(Lhre;)V

    goto :goto_0

    .line 2485
    :catch_0
    move-exception v0

    new-instance v0, Lfqy;

    invoke-direct {v0}, Lfqy;-><init>()V

    goto :goto_0
.end method

.method private static h(Lorg/json/JSONObject;Ljava/lang/String;)Lflr;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2496
    invoke-static {p0, p1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2497
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2507
    :goto_0
    return-object v0

    .line 2500
    :cond_0
    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 2501
    new-instance v2, Lhrb;

    invoke-direct {v2}, Lhrb;-><init>()V

    .line 2503
    :try_start_0
    invoke-static {v2, v1}, Lhrb;->a(Lidh;[B)Lidh;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 2507
    new-instance v0, Lflr;

    invoke-direct {v0, v2}, Lflr;-><init>(Lhrb;)V

    goto :goto_0

    .line 2505
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static i(Lorg/json/JSONObject;Ljava/lang/String;)Lhqz;
    .locals 2

    .prologue
    .line 2525
    invoke-static {p0, p1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2526
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2528
    const/4 v0, 0x0

    .line 2534
    :goto_0
    return-object v0

    .line 2530
    :cond_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 2532
    :try_start_0
    new-instance v1, Lhqz;

    invoke-direct {v1}, Lhqz;-><init>()V

    .line 2534
    invoke-static {v1, v0}, Lidh;->a(Lidh;[B)Lidh;

    move-result-object v0

    check-cast v0, Lhqz;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2536
    :catch_0
    move-exception v0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Invalid protobuf"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2306
    const/4 v0, 0x1

    return v0
.end method

.method protected final synthetic a(Lorg/json/JSONObject;I)Ljava/lang/Object;
    .locals 66

    .prologue
    .line 2237
    const/4 v3, 0x1

    move/from16 v0, p2

    if-eq v0, v3, :cond_0

    new-instance v3, Lorg/json/JSONException;

    const-string v4, "Unsupported version"

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v3, Lfoy;

    const-string v4, "impressionUris"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    const-string v5, "adVideoId"

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "originalVideoId"

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "contentPlayerAdParams"

    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "contentPlayerAdNextParams"

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "requestTrackingParams"

    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v9, 0x0

    :goto_0
    const-string v10, "adBreakId"

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const-string v12, "title"

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "adOwnerName"

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "adOwnerUri"

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lfpd;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    const-string v15, "vastAdId"

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "vastAdSystem"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-string v17, "billingPartner"

    const-class v18, Lfpb;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v17

    check-cast v17, Lfpb;

    const-string v18, "adFormat"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const-string v19, "duration"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    const/16 v20, 0x0

    const-string v21, "playbackTracking"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lfpd;->f(Lorg/json/JSONObject;Ljava/lang/String;)Lflp;

    move-result-object v21

    const-string v22, "playerConfig"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lfpd;->g(Lorg/json/JSONObject;Ljava/lang/String;)Lfqy;

    move-result-object v22

    const-string v23, "playerAttestation"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lfpd;->h(Lorg/json/JSONObject;Ljava/lang/String;)Lflr;

    move-result-object v23

    const-string v24, "clickthroughUri"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lfpd;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    const-string v25, "startPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v25

    const-string v26, "firstQuartilePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v26

    const-string v27, "midpointPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v27

    const-string v28, "thirdQuartilePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v28

    sget-object v29, Lfpf;->d:Lfph;

    const-string v30, "progressPings"

    move-object/from16 v0, v29

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lfph;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v29

    const-string v30, "skipPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v30

    const-string v31, "skipShownPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v31

    const-string v32, "engagedViewPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v32

    const-string v33, "completePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v33

    const-string v34, "closePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v34

    const-string v35, "pausePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v35

    const-string v36, "resumePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v36

    const-string v37, "mutePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v37

    const-string v38, "fullscreenPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v38

    const-string v39, "endFullscreenPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v39

    const-string v40, "clickthroughPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v40

    const-string v41, "videoTitleClickedPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v41

    const-string v42, "errorPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v42

    const-string v43, "exclusionReasonPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v43

    const-string v44, "videoAdTrackingTemplateUri"

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, Lfpd;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v44

    const-string v45, "adSenseBaseConversionUri"

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, Lfpd;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v45

    const-string v46, "shouldPingVssOnEngaged"

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v46

    const-string v47, "fallbackHint"

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v47

    const-string v48, "expirationTimeMillis"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v48

    const-string v50, "assetFrequencyCap"

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v50

    const-string v51, "isPublicVideo"

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v51

    const-string v52, "showCtaAnnotations"

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v52

    const-string v53, "adAnnotations"

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, Lfpd;->i(Lorg/json/JSONObject;Ljava/lang/String;)Lhqz;

    move-result-object v53

    const-string v54, "adInfoCards"

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-static {v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    invoke-static/range {v54 .. v54}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v55

    if-eqz v55, :cond_3

    const/16 v54, 0x0

    :cond_1
    const-string v55, "requestTimeMilliseconds"

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v55

    const-string v57, "offlineShouldCountPlayback"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v57

    const-string v58, "shouldAllowQueuedOfflinePings"

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v58

    const-string v59, "adWrapperUri"

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-static {v0, v1}, Lfpd;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v59

    const-string v60, "prefetchedAd"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v60

    invoke-virtual {v0, v1, v2}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v60

    check-cast v60, Lfoy;

    const-string v61, "parentWrapper"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v61

    invoke-virtual {v0, v1, v2}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v61

    check-cast v61, Lfoy;

    const/16 v62, 0x0

    const-string v63, "infoCards"

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v63

    if-eqz v63, :cond_4

    const/16 v63, 0x0

    :goto_1
    sget-object v64, Lfoo;->b:Lfor;

    const-string v65, "survey"

    move-object/from16 v0, v64

    move-object/from16 v1, p1

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Lfor;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v64

    check-cast v64, Lfoo;

    const-string v65, "activeViewPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-static {v0, v1}, Lfpd;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v65

    invoke-direct/range {v3 .. v65}, Lfoy;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lfpb;Ljava/lang/String;ILfrf;Lflp;Lfqy;Lflr;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;Landroid/net/Uri;ZZJIZZLhqz;Lfkg;JZZLandroid/net/Uri;Lfoy;Lfoy;Ljava/util/List;Ljava/util/List;Lfoo;Ljava/util/List;)V

    return-object v3

    :cond_2
    const/4 v10, 0x2

    invoke-static {v9, v10}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v9

    goto/16 :goto_0

    :cond_3
    const/16 v55, 0x2

    invoke-static/range {v54 .. v55}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v54

    invoke-static/range {v54 .. v54}, Lfkg;->a([B)Lfkg;

    move-result-object v54

    if-nez v54, :cond_1

    new-instance v3, Lorg/json/JSONException;

    const-string v4, "Invalid info card byte array"

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_4
    sget-object v63, Lfpi;->e:Lfpl;

    const-string v64, "infoCards"

    move-object/from16 v0, v63

    move-object/from16 v1, p1

    move-object/from16 v2, v64

    invoke-virtual {v0, v1, v2}, Lfpl;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v63

    goto :goto_1
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 2311
    const-string v0, "impressionUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->b:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2312
    const-string v0, "adVideoId"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->c:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2313
    const-string v0, "originalVideoId"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->d:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2314
    const-string v0, "contentPlayerAdParams"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->e:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2315
    const-string v0, "contentPlayerAdNextParams"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->f:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2316
    const-string v1, "requestTrackingParams"

    iget-object v0, p0, Lfpd;->a:Lfoy;

    iget-object v0, v0, Lfoy;->g:[B

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v1, v0}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2318
    const-string v0, "adBreakId"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->h:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2319
    const-string v0, "title"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->j:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2320
    const-string v0, "adOwnerName"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    invoke-static {v1}, Lfoy;->a(Lfoy;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2321
    const-string v0, "adOwnerUri"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    invoke-static {v1}, Lfoy;->b(Lfoy;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2322
    const-string v0, "vastAdId"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->k:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2323
    const-string v0, "vastAdSystem"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->l:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2324
    const-string v0, "billingPartner"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->m:Lfpb;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Enum;)V

    .line 2325
    const-string v0, "adFormat"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->n:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2326
    const-string v0, "duration"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget v1, v1, Lfoy;->o:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2327
    const-string v0, "playbackTracking"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    .line 2328
    invoke-static {v1}, Lfoy;->c(Lfoy;)Lflp;

    move-result-object v1

    invoke-virtual {v1}, Lflp;->a()Leab;

    move-result-object v1

    invoke-virtual {v1}, Leab;->c()[B

    move-result-object v1

    .line 2327
    invoke-static {v1, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2329
    const-string v0, "playerConfig"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    .line 2330
    invoke-static {v1}, Lfoy;->d(Lfoy;)Lfqy;

    move-result-object v1

    invoke-virtual {v1}, Lfqy;->u()Lhre;

    move-result-object v1

    .line 2329
    invoke-static {v1}, Lidh;->a(Lidh;)[B

    move-result-object v1

    invoke-static {v1, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2331
    const-string v0, "clickthroughUri"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->t:Landroid/net/Uri;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2332
    const-string v0, "startPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->u:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2333
    const-string v0, "firstQuartilePingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->v:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2334
    const-string v0, "midpointPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->w:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2335
    const-string v0, "thirdQuartilePingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->x:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2336
    const-string v0, "progressPings"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->y:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2337
    const-string v0, "skipPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->z:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2338
    const-string v0, "skipShownPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->A:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2339
    const-string v0, "engagedViewPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->B:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2340
    const-string v0, "completePingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->C:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2341
    const-string v0, "closePingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->D:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2342
    const-string v0, "pausePingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->E:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2343
    const-string v0, "resumePingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->F:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2344
    const-string v0, "mutePingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->G:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2345
    const-string v0, "fullscreenPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->H:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2346
    const-string v0, "endFullscreenPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->I:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2347
    const-string v0, "clickthroughPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->J:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2348
    const-string v0, "videoTitleClickedPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->K:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2349
    const-string v0, "errorPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->L:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2350
    const-string v0, "exclusionReasonPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->M:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2351
    const-string v0, "videoAdTrackingTemplateUri"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->N:Landroid/net/Uri;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2352
    const-string v0, "adSenseBaseConversionUri"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->O:Landroid/net/Uri;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2353
    const-string v0, "shouldPingVssOnEngaged"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-boolean v1, v1, Lfoy;->P:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2354
    const-string v0, "fallbackHint"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-boolean v1, v1, Lfoy;->Q:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2355
    const-string v0, "expirationTimeMillis"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-wide v2, v1, Lfoy;->R:J

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 2356
    const-string v0, "assetFrequencyCap"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget v1, v1, Lfoy;->S:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2357
    const-string v0, "isPublicVideo"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-boolean v1, v1, Lfoy;->T:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2358
    const-string v0, "showCtaAnnotations"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-boolean v1, v1, Lfoy;->U:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2359
    iget-object v0, p0, Lfpd;->a:Lfoy;

    iget-object v0, v0, Lfoy;->V:Lhqz;

    if-eqz v0, :cond_0

    .line 2360
    iget-object v0, p0, Lfpd;->a:Lfoy;

    iget-object v0, v0, Lfoy;->V:Lhqz;

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    .line 2361
    const-string v1, "adAnnotations"

    invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2363
    :cond_0
    iget-object v0, p0, Lfpd;->a:Lfoy;

    iget-object v0, v0, Lfoy;->W:Lfkg;

    if-eqz v0, :cond_1

    .line 2364
    iget-object v0, p0, Lfpd;->a:Lfoy;

    iget-object v0, v0, Lfoy;->W:Lfkg;

    iget-object v0, v0, Lfkg;->a:Lhjj;

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    .line 2365
    const-string v1, "adInfoCards"

    invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2367
    :cond_1
    iget-object v0, p0, Lfpd;->a:Lfoy;

    invoke-static {v0}, Lfoy;->e(Lfoy;)Lflr;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2368
    iget-object v0, p0, Lfpd;->a:Lfoy;

    invoke-static {v0}, Lfoy;->e(Lfoy;)Lflr;

    move-result-object v0

    invoke-virtual {v0}, Lflr;->b()Lhrb;

    move-result-object v0

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    .line 2369
    const-string v1, "playerAttestation"

    invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2371
    :cond_2
    const-string v0, "requestTimeMilliseconds"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-wide v2, v1, Lfoy;->ab:J

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 2372
    const-string v0, "offlineShouldCountPlayback"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-boolean v1, v1, Lfoy;->af:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2373
    const-string v0, "shouldAllowQueuedOfflinePings"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    invoke-static {v1}, Lfoy;->f(Lfoy;)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2374
    const-string v0, "adWrapperUri"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->X:Landroid/net/Uri;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2375
    const-string v0, "prefetchedAd"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->Z:Lfoy;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Lghz;)V

    .line 2376
    const-string v0, "parentWrapper"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->aa:Lfoy;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Lghz;)V

    .line 2377
    const-string v0, "infoCards"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->ai:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2378
    const-string v0, "survey"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->aj:Lfoo;

    invoke-static {p1, v0, v1}, Lfpd;->a(Lorg/json/JSONObject;Ljava/lang/String;Lghz;)V

    .line 2379
    const-string v0, "activeViewPingUris"

    iget-object v1, p0, Lfpd;->a:Lfoy;

    iget-object v1, v1, Lfoy;->ak:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lfpd;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 2380
    return-void

    .line 2316
    :cond_3
    iget-object v0, p0, Lfpd;->a:Lfoy;

    .line 2317
    iget-object v0, v0, Lfoy;->g:[B

    invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
