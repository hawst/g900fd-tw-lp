.class final Lbez;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field private synthetic a:Lhog;

.field private synthetic b:Lbev;


# direct methods
.method constructor <init>(Lbev;Lhog;)V
    .locals 0

    .prologue
    .line 424
    iput-object p1, p0, Lbez;->b:Lbev;

    iput-object p2, p0, Lbez;->a:Lhog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 3

    .prologue
    .line 446
    iget-object v0, p0, Lbez;->b:Lbev;

    invoke-static {v0}, Lbev;->e(Lbev;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lbez;->b:Lbev;

    invoke-static {v1}, Lbev;->g(Lbev;)Leyt;

    move-result-object v1

    invoke-interface {v1, p1}, Leyt;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;Z)V

    .line 447
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 424
    check-cast p1, Lfmh;

    iget-object v1, p0, Lbez;->b:Lbev;

    new-instance v2, Lfqg;

    iget-object v3, p0, Lbez;->b:Lbev;

    invoke-static {v3}, Lbev;->b(Lbev;)Letc;

    move-result-object v3

    invoke-virtual {v3}, Letc;->k()Lfac;

    move-result-object v3

    iget-object v4, p0, Lbez;->a:Lhog;

    invoke-direct {v2, v3, v4}, Lfqg;-><init>(Lfac;Lhog;)V

    invoke-static {v1, v2}, Lbev;->a(Lbev;Lfqg;)Lfqg;

    iget-object v1, p0, Lbez;->b:Lbev;

    invoke-static {v1}, Lbev;->d(Lbev;)Lfdw;

    move-result-object v1

    iget-object v2, p0, Lbez;->b:Lbev;

    invoke-static {v2}, Lbev;->c(Lbev;)Lfqg;

    move-result-object v2

    sget-object v3, Lfqi;->c:Lfqi;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lfdw;->a(Lfqg;Lfqi;Lhcq;)V

    iget-object v1, p1, Lfmh;->a:Lhuj;

    iget-object v1, v1, Lhuj;->b:Lhuk;

    if-eqz v1, :cond_0

    iget-object v2, v1, Lhuk;->a:Lhul;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lbez;->b:Lbev;

    invoke-static {v0}, Lbev;->e(Lbev;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lbez;->b:Lbev;

    iget-object v1, v1, Lbev;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090208

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lbez;->b:Lbev;

    iget-object v1, p1, Lfmh;->a:Lhuj;

    iget-object v1, v1, Lhuj;->a:Lhtx;

    invoke-static {v1}, La;->a(Lhtx;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbev;->a(Lbev;Ljava/lang/String;)Ljava/lang/String;

    return-void

    :cond_1
    iget-object v1, v1, Lhuk;->a:Lhul;

    iget-object v1, v1, Lhul;->a:[Lhun;

    array-length v1, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbez;->b:Lbev;

    invoke-static {v0}, Lbev;->e(Lbev;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    iget-object v0, p0, Lbez;->b:Lbev;

    invoke-static {v0}, Lbev;->f(Lbev;)Lfvh;

    move-result-object v0

    invoke-virtual {p1}, Lfmh;->a()Lfmi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfvh;->b(Lfmi;)V

    goto :goto_1
.end method
