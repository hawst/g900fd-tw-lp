.class public final Lgop;
.super Lghl;
.source "SourceFile"

# interfaces
.implements Lgot;


# instance fields
.field private final a:Leuk;

.field private final b:Lgku;

.field private final c:Lgku;

.field private final d:Lgku;

.field private final j:Lgku;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Lezj;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Lghl;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Lezj;)V

    .line 64
    const/16 v0, 0x14

    invoke-static {v0}, Lgop;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lgop;->a:Leuk;

    .line 66
    invoke-direct {p0}, Lgop;->a()Lgku;

    move-result-object v0

    iput-object v0, p0, Lgop;->b:Lgku;

    .line 67
    invoke-direct {p0}, Lgop;->b()Lgku;

    move-result-object v0

    iput-object v0, p0, Lgop;->c:Lgku;

    .line 68
    invoke-direct {p0}, Lgop;->c()Lgku;

    move-result-object v0

    iput-object v0, p0, Lgop;->d:Lgku;

    .line 69
    invoke-direct {p0}, Lgop;->d()Lgku;

    move-result-object v0

    iput-object v0, p0, Lgop;->j:Lgku;

    .line 70
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Ljava/lang/String;Lezj;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct/range {p0 .. p5}, Lghl;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Ljava/lang/String;Lezj;)V

    .line 50
    const/16 v0, 0x14

    invoke-static {v0}, Lgop;->a(I)Leul;

    move-result-object v0

    iput-object v0, p0, Lgop;->a:Leuk;

    .line 52
    invoke-direct {p0}, Lgop;->a()Lgku;

    move-result-object v0

    iput-object v0, p0, Lgop;->b:Lgku;

    .line 53
    invoke-direct {p0}, Lgop;->b()Lgku;

    move-result-object v0

    iput-object v0, p0, Lgop;->c:Lgku;

    .line 54
    invoke-direct {p0}, Lgop;->c()Lgku;

    move-result-object v0

    iput-object v0, p0, Lgop;->d:Lgku;

    .line 55
    invoke-direct {p0}, Lgop;->d()Lgku;

    move-result-object v0

    iput-object v0, p0, Lgop;->j:Lgku;

    .line 56
    return-void
.end method

.method private a()Lgku;
    .locals 6

    .prologue
    .line 103
    new-instance v0, Lgor;

    invoke-direct {v0}, Lgor;-><init>()V

    .line 104
    const/16 v1, 0x14

    .line 105
    invoke-static {v1}, Lgop;->a(I)Leul;

    move-result-object v1

    .line 108
    invoke-virtual {p0, v0, v0}, Lgop;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    .line 110
    iget-object v2, p0, Lgop;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 111
    invoke-virtual {p0}, Lgop;->h()Leun;

    move-result-object v2

    .line 112
    const-wide/32 v4, 0x240c8400

    invoke-virtual {p0, v2, v0, v4, v5}, Lgop;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    .line 115
    :cond_0
    invoke-virtual {p0, v0}, Lgop;->a(Lgku;)Lgjx;

    move-result-object v0

    .line 116
    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lgop;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    .line 118
    return-object v0
.end method

.method private b()Lgku;
    .locals 4

    .prologue
    .line 122
    new-instance v0, Lgou;

    iget-object v1, p0, Lgop;->i:Lfbc;

    invoke-direct {v0, v1}, Lgou;-><init>(Lfbc;)V

    .line 125
    invoke-virtual {p0, v0, v0}, Lgop;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lgop;->g:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 128
    invoke-virtual {p0}, Lgop;->h()Leun;

    move-result-object v1

    .line 129
    const-wide/32 v2, 0x240c8400

    invoke-virtual {p0, v1, v0, v2, v3}, Lgop;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    .line 132
    :cond_0
    invoke-virtual {p0, v0}, Lgop;->a(Lgku;)Lgjx;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lgop;->a:Leuk;

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lgop;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    .line 135
    return-object v0
.end method

.method private c()Lgku;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lgou;

    iget-object v1, p0, Lgop;->i:Lfbc;

    invoke-direct {v0, v1}, Lgou;-><init>(Lfbc;)V

    .line 140
    new-instance v1, Lghx;

    invoke-direct {v1}, Lghx;-><init>()V

    .line 142
    invoke-virtual {p0, v0, v1}, Lgop;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    .line 143
    invoke-virtual {p0, v0}, Lgop;->a(Lgku;)Lgjx;

    move-result-object v0

    .line 144
    return-object v0
.end method

.method private d()Lgku;
    .locals 4

    .prologue
    .line 148
    new-instance v0, Lgoq;

    iget-object v1, p0, Lgop;->i:Lfbc;

    invoke-direct {v0, v1}, Lgoq;-><init>(Lfbc;)V

    .line 149
    invoke-virtual {p0, v0}, Lgop;->a(Lgku;)Lgjx;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lgop;->a:Leuk;

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lgop;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    .line 152
    return-object v0
.end method


# virtual methods
.method public final a(Lgpa;Leuc;)V
    .locals 1

    .prologue
    .line 84
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p1, Lgpa;->e:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 87
    iget-object v0, p1, Lgpa;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lgop;->j:Lgku;

    invoke-interface {v0, p1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lgop;->c:Lgku;

    invoke-interface {v0, p1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 75
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 77
    new-instance v0, Lgos;

    invoke-direct {v0, p2}, Lgos;-><init>(Leuc;)V

    .line 78
    iget-object v1, p0, Lgop;->b:Lgku;

    invoke-interface {v1, p1, v0}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 79
    return-void
.end method

.method public final b(Lgpa;Leuc;)V
    .locals 1

    .prologue
    .line 97
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p1, Lgpa;->e:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lgop;->d:Lgku;

    invoke-interface {v0, p1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 100
    return-void
.end method
