.class public final Ldmp;
.super Ldmo;
.source "SourceFile"


# instance fields
.field private final a:Ljava/security/Key;

.field private final b:Ljavax/crypto/spec/IvParameterSpec;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLdnb;ZZLdmq;Ljava/security/Key;)V
    .locals 13

    .prologue
    .line 66
    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-wide/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Ldmo;-><init>(Ljava/lang/String;Ljava/lang/String;JLdnb;ZZLdmq;)V

    .line 67
    move-object/from16 v0, p9

    iput-object v0, p0, Ldmp;->a:Ljava/security/Key;

    .line 68
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, La;->y(Ljava/lang/String;)Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v2

    iput-object v2, p0, Ldmp;->b:Ljavax/crypto/spec/IvParameterSpec;

    .line 69
    return-void
.end method


# virtual methods
.method protected final a(Ljava/nio/channels/FileChannel;Ljava/nio/ByteBuffer;)I
    .locals 8

    .prologue
    .line 73
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    new-array v1, v0, [B

    .line 74
    invoke-virtual {p2, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 76
    const/4 v2, 0x0

    :try_start_0
    array-length v3, v1

    iget-object v4, p0, Ldmp;->a:Ljava/security/Key;

    iget-object v5, p0, Ldmp;->b:Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    invoke-static/range {v1 .. v7}, La;->a([BIILjava/security/Key;Ljavax/crypto/spec/IvParameterSpec;J)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v0

    return v0

    .line 77
    :catch_0
    move-exception v0

    .line 80
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
