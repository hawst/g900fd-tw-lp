.class public final Lebv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:F

.field public final f:I

.field public final g:I

.field public final h:I

.field public i:I

.field public j:I

.field public final k:Ljava/util/List;

.field public l:Landroid/media/MediaFormat;

.field private m:I


# direct methods
.method constructor <init>(Landroid/media/MediaFormat;)V
    .locals 6

    .prologue
    const/16 v5, 0xf

    const/4 v4, -0x1

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lebv;->l:Landroid/media/MediaFormat;

    .line 90
    const-string v0, "mime"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebv;->a:Ljava/lang/String;

    .line 91
    const-string v0, "max-input-size"

    invoke-static {p1, v0}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebv;->b:I

    .line 92
    const-string v0, "width"

    invoke-static {p1, v0}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebv;->c:I

    .line 93
    const-string v0, "height"

    invoke-static {p1, v0}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebv;->d:I

    .line 94
    const-string v0, "channel-count"

    invoke-static {p1, v0}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebv;->f:I

    .line 95
    const-string v0, "sample-rate"

    invoke-static {p1, v0}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebv;->g:I

    .line 96
    const-string v0, "bitrate"

    invoke-static {p1, v0}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebv;->h:I

    .line 97
    const-string v0, "com.google.android.videos.pixelWidthHeightRatio"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getFloat(Ljava/lang/String;)F

    move-result v0

    :goto_0
    iput v0, p0, Lebv;->e:F

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lebv;->k:Ljava/util/List;

    .line 99
    const/4 v0, 0x0

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "csd-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "csd-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 101
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    new-array v2, v2, [B

    .line 102
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 103
    iget-object v3, p0, Lebv;->k:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 97
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0

    .line 106
    :cond_1
    iput v4, p0, Lebv;->i:I

    .line 107
    iput v4, p0, Lebv;->j:I

    .line 108
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIFIIILjava/util/List;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lebv;->a:Ljava/lang/String;

    .line 114
    iput p2, p0, Lebv;->b:I

    .line 115
    iput p3, p0, Lebv;->c:I

    .line 116
    iput p4, p0, Lebv;->d:I

    .line 117
    iput p5, p0, Lebv;->e:F

    .line 118
    iput p6, p0, Lebv;->f:I

    .line 119
    iput p7, p0, Lebv;->g:I

    .line 120
    iput p8, p0, Lebv;->h:I

    .line 121
    if-nez p9, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p9

    :cond_0
    iput-object p9, p0, Lebv;->k:Ljava/util/List;

    .line 123
    iput v0, p0, Lebv;->i:I

    .line 124
    iput v0, p0, Lebv;->j:I

    .line 125
    return-void
.end method

.method private static final a(Landroid/media/MediaFormat;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0, p1}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IIIFLjava/util/List;)Lebv;
    .locals 10

    .prologue
    const/4 v6, -0x1

    .line 61
    new-instance v0, Lebv;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v7, v6

    move v8, v6

    move-object v9, p5

    invoke-direct/range {v0 .. v9}, Lebv;-><init>(Ljava/lang/String;IIIFIIILjava/util/List;)V

    return-object v0
.end method

.method static final a(Landroid/media/MediaFormat;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 245
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 246
    invoke-virtual {p0, p1, p2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 248
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/media/MediaFormat;)V
    .locals 2

    .prologue
    .line 238
    const-string v0, "max-width"

    iget v1, p0, Lebv;->i:I

    invoke-static {p1, v0, v1}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 239
    const-string v0, "max-height"

    iget v1, p0, Lebv;->j:I

    invoke-static {p1, v0, v1}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    .line 240
    return-void
.end method

.method public a(Lebv;Z)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 187
    iget v0, p0, Lebv;->b:I

    iget v1, p1, Lebv;->b:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lebv;->c:I

    iget v1, p1, Lebv;->c:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lebv;->d:I

    iget v1, p1, Lebv;->d:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lebv;->e:F

    iget v1, p1, Lebv;->e:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    if-nez p2, :cond_0

    iget v0, p0, Lebv;->i:I

    iget v1, p1, Lebv;->i:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lebv;->j:I

    iget v1, p1, Lebv;->j:I

    if-ne v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lebv;->f:I

    iget v1, p1, Lebv;->f:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lebv;->g:I

    iget v1, p1, Lebv;->g:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lebv;->a:Ljava/lang/String;

    iget-object v1, p1, Lebv;->a:Ljava/lang/String;

    .line 191
    invoke-static {v0, v1}, Legz;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lebv;->h:I

    iget v1, p1, Lebv;->h:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lebv;->k:Ljava/util/List;

    .line 193
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p1, Lebv;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 201
    :cond_1
    :goto_0
    return v3

    :cond_2
    move v2, v3

    .line 196
    :goto_1
    iget-object v0, p0, Lebv;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 197
    iget-object v0, p0, Lebv;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v1, p1, Lebv;->k:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 201
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 167
    if-ne p0, p1, :cond_1

    .line 168
    const/4 v0, 0x1

    .line 173
    :cond_0
    :goto_0
    return v0

    .line 170
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 173
    check-cast p1, Lebv;

    invoke-virtual {p0, p1, v0}, Lebv;->a(Lebv;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 145
    iget v0, p0, Lebv;->m:I

    if-nez v0, :cond_2

    .line 146
    iget-object v0, p0, Lebv;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xb

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v3, 0x20f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 148
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lebv;->b:I

    add-int/2addr v0, v2

    .line 149
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lebv;->c:I

    add-int/2addr v0, v2

    .line 150
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lebv;->d:I

    add-int/2addr v0, v2

    .line 151
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lebv;->e:F

    invoke-static {v2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 152
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lebv;->i:I

    add-int/2addr v0, v2

    .line 153
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lebv;->j:I

    add-int/2addr v0, v2

    .line 154
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lebv;->f:I

    add-int/2addr v0, v2

    .line 155
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lebv;->g:I

    add-int/2addr v0, v2

    .line 156
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lebv;->h:I

    add-int/2addr v0, v2

    .line 157
    :goto_1
    iget-object v2, p0, Lebv;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 158
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lebv;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/2addr v0, v2

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 146
    :cond_0
    iget-object v0, p0, Lebv;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 160
    :cond_1
    iput v0, p0, Lebv;->m:I

    .line 162
    :cond_2
    iget v0, p0, Lebv;->m:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 12

    .prologue
    .line 206
    iget-object v0, p0, Lebv;->a:Ljava/lang/String;

    iget v1, p0, Lebv;->b:I

    iget v2, p0, Lebv;->c:I

    iget v3, p0, Lebv;->d:I

    iget v4, p0, Lebv;->e:F

    iget v5, p0, Lebv;->f:I

    iget v6, p0, Lebv;->g:I

    iget v7, p0, Lebv;->h:I

    iget v8, p0, Lebv;->i:I

    iget v9, p0, Lebv;->j:I

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit16 v11, v11, 0x86

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "MediaFormat("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, ", "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
