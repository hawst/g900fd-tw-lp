.class public final Lfrn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lflb;
.implements Lfqh;


# instance fields
.field public final a:Lhzy;

.field public b:Lfro;

.field private final c:Lfqh;


# direct methods
.method public constructor <init>(Lhzy;Lfqh;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzy;

    iput-object v0, p0, Lfrn;->a:Lhzy;

    .line 28
    iput-object p2, p0, Lfrn;->c:Lfqh;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfrn;->a:Lhzy;

    iget-object v0, v0, Lhzy;->b:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 63
    iget-object v1, p0, Lfrn;->a:Lhzy;

    iget-object v1, v1, Lhzy;->a:Liaa;

    if-nez v1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 66
    :cond_1
    iget-object v1, p0, Lfrn;->a:Lhzy;

    iget-object v1, v1, Lhzy;->a:Liaa;

    iget-object v2, v1, Liaa;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 67
    if-ne p1, v4, :cond_2

    .line 68
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lfrn;->a:Lhzy;

    iget-object v0, v0, Lhzy;->c:Lhog;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lfrn;->a:Lhzy;

    iget-object v0, v0, Lhzy;->d:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lfrn;->c:Lfqh;

    return-object v0
.end method
