.class public final Lbuk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lfrz;


# instance fields
.field private final A:Landroid/widget/ImageView;

.field private B:Lflw;

.field private final C:Levn;

.field private D:Lfqg;

.field private final E:Lfdg;

.field private final F:Leyt;

.field private G:Lbuj;

.field private final a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private final b:Lbvc;

.field private final c:Ljava/util/concurrent/atomic/AtomicReference;

.field private d:Z

.field private e:I

.field private final f:Lcws;

.field private g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private h:Landroid/widget/ListView;

.field private i:Landroid/widget/FrameLayout;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/ImageView;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/ImageView;

.field private o:Lfsi;

.field private final p:Landroid/content/res/Resources;

.field private q:Lboi;

.field private r:Lbrh;

.field private s:Lbuo;

.field private final t:I

.field private final u:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

.field private final v:Landroid/widget/ImageView;

.field private final w:Landroid/widget/FrameLayout;

.field private final x:Landroid/view/View;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lfdg;Leyt;Lcom/google/android/apps/youtube/app/WatchWhileActivity;ILandroid/view/View;Lbvc;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdg;

    iput-object v0, p0, Lbuk;->E:Lfdg;

    .line 136
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbuk;->F:Leyt;

    .line 137
    iput p4, p0, Lbuk;->t:I

    .line 138
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 139
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvc;

    iput-object v0, p0, Lbuk;->b:Lbvc;

    .line 142
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicReference;

    iput-object v0, p0, Lbuk;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 144
    invoke-virtual {p3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 145
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v1

    .line 146
    iget-object v0, v0, Lckz;->a:Letc;

    .line 148
    invoke-virtual {v1}, Lari;->S()Lcws;

    move-result-object v1

    iput-object v1, p0, Lbuk;->f:Lcws;

    .line 150
    invoke-virtual {p3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lbuk;->p:Landroid/content/res/Resources;

    .line 151
    sget-object v1, Lbuo;->a:Lbuo;

    iput-object v1, p0, Lbuk;->s:Lbuo;

    .line 153
    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    iput-object v0, p0, Lbuk;->C:Levn;

    .line 155
    const v0, 0x7f080322

    .line 156
    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "parentView must contain the set_content_slider"

    .line 155
    invoke-static {v0, v1}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    iput-object v0, p0, Lbuk;->u:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    .line 158
    const v0, 0x7f0802d8

    .line 159
    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "parentView must contain the set_bar"

    .line 158
    invoke-static {v0, v1}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lbuk;->x:Landroid/view/View;

    .line 160
    const v0, 0x7f080323

    .line 161
    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "parentView must contain the set_content"

    .line 160
    invoke-static {v0, v1}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lbuk;->w:Landroid/widget/FrameLayout;

    .line 162
    const v0, 0x7f0802d9

    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbuk;->v:Landroid/widget/ImageView;

    .line 163
    const v0, 0x7f0802da

    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbuk;->A:Landroid/widget/ImageView;

    .line 165
    iget-object v0, p0, Lbuk;->u:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    new-instance v1, Lbun;

    invoke-direct {v1, p0}, Lbun;-><init>(Lbuk;)V

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:Lbwq;

    .line 166
    iget-object v0, p0, Lbuk;->u:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setEnabled(Z)V

    .line 167
    invoke-direct {p0}, Lbuk;->c()V

    .line 169
    iput-boolean v2, p0, Lbuk;->d:Z

    .line 170
    const/4 v0, -0x1

    iput v0, p0, Lbuk;->e:I

    .line 171
    return-void
.end method

.method static synthetic a(Lbuk;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->k:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a(Lbuk;Lbuo;)Lbuo;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lbuk;->s:Lbuo;

    return-object p1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 349
    const/4 v0, -0x1

    iput v0, p0, Lbuk;->e:I

    .line 350
    const/4 v0, 0x0

    iput-object v0, p0, Lbuk;->B:Lflw;

    .line 351
    iget-object v0, p0, Lbuk;->G:Lbuj;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lbuk;->G:Lbuj;

    invoke-virtual {v0}, Lbuj;->b()V

    .line 354
    :cond_0
    invoke-direct {p0}, Lbuk;->c()V

    .line 355
    invoke-direct {p0}, Lbuk;->b()V

    .line 356
    return-void
.end method

.method static synthetic a(Lbuk;Z)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lbuk;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 502
    iget-object v0, p0, Lbuk;->z:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 503
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lbuk;->z:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 508
    :goto_0
    return-void

    .line 506
    :cond_0
    iget-object v0, p0, Lbuk;->z:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lbuk;->C:Levn;

    new-instance v1, Lbuq;

    invoke-direct {v1, p1}, Lbuq;-><init>(Z)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 293
    return-void
.end method

.method static synthetic b(Lbuk;)Lcws;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->f:Lcws;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 359
    iget-boolean v0, p0, Lbuk;->d:Z

    if-nez v0, :cond_0

    .line 377
    :goto_0
    return-void

    .line 363
    :cond_0
    iget-object v0, p0, Lbuk;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 364
    iget-object v0, p0, Lbuk;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 365
    iget-object v0, p0, Lbuk;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 366
    iget-object v0, p0, Lbuk;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lbuk;->o:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 368
    iget-object v0, p0, Lbuk;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lbuk;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 370
    iget-object v0, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 371
    iget-object v0, p0, Lbuk;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 372
    iget-object v0, p0, Lbuk;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    iget-object v0, p0, Lbuk;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    iget-object v0, p0, Lbuk;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 375
    iget-object v0, p0, Lbuk;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 376
    iget-object v0, p0, Lbuk;->r:Lbrh;

    invoke-virtual {v0, v2}, Lbrh;->a(Lfkn;)V

    goto :goto_0
.end method

.method static synthetic c(Lbuk;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->l:Landroid/widget/ImageView;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 457
    iget-object v0, p0, Lbuk;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 458
    iget-object v0, p0, Lbuk;->u:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    sget-object v1, Lbuo;->c:Lbuo;

    invoke-static {v1}, Lbuo;->a(Lbuo;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    .line 459
    return-void
.end method

.method static synthetic d(Lbuk;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->m:Landroid/widget/ImageView;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 475
    sget-object v0, Lbuo;->b:Lbuo;

    iput-object v0, p0, Lbuk;->s:Lbuo;

    .line 476
    iget-object v0, p0, Lbuk;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lbuk;->u:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    sget-object v1, Lbuo;->b:Lbuo;

    invoke-static {v1}, Lbuo;->a(Lbuo;)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    .line 478
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbuk;->a(Z)V

    .line 479
    return-void
.end method

.method static synthetic e(Lbuk;)Lflw;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->B:Lflw;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 482
    sget-object v0, Lbuo;->c:Lbuo;

    iput-object v0, p0, Lbuk;->s:Lbuo;

    .line 483
    iget-object v0, p0, Lbuk;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 484
    iget-object v0, p0, Lbuk;->u:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    sget-object v1, Lbuo;->c:Lbuo;

    invoke-static {v1}, Lbuo;->a(Lbuo;)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    .line 485
    invoke-direct {p0, v2}, Lbuk;->a(Z)V

    .line 486
    return-void
.end method

.method static synthetic f(Lbuk;)Lcom/google/android/apps/youtube/app/WatchWhileActivity;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    return-object v0
.end method

.method static synthetic g(Lbuk;)Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->x:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(Lbuk;)Z
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->u:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    sget-object v1, Lbuo;->b:Lbuo;

    invoke-static {v1}, Lbuo;->a(Lbuo;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(I)Z

    move-result v0

    return v0
.end method

.method private handlePlaybackServiceException(Lczb;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 322
    sget-object v0, Lbum;->b:[I

    iget-object v1, p1, Lczb;->a:Lczc;

    invoke-virtual {v1}, Lczc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 329
    :goto_0
    return-void

    .line 328
    :pswitch_0
    invoke-direct {p0}, Lbuk;->a()V

    goto :goto_0

    .line 322
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private handleSequencerHasPreviousNextEvent(Lczt;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 297
    iget-boolean v0, p0, Lbuk;->d:Z

    if-nez v0, :cond_0

    .line 302
    :goto_0
    return-void

    .line 300
    :cond_0
    iget-object v0, p0, Lbuk;->k:Landroid/widget/ImageView;

    iget-boolean v1, p1, Lczt;->c:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 301
    iget-object v0, p0, Lbuk;->l:Landroid/widget/ImageView;

    iget-boolean v1, p1, Lczt;->d:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0
.end method

.method private handleSequencerStageEvent(Lczu;)V
    .locals 11
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 306
    sget-object v0, Lbum;->a:[I

    iget-object v1, p1, Lczu;->a:Lgok;

    invoke-virtual {v1}, Lgok;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 314
    :goto_0
    return-void

    .line 309
    :pswitch_0
    invoke-direct {p0}, Lbuk;->a()V

    goto :goto_0

    .line 313
    :pswitch_1
    iget-object v9, p1, Lczu;->c:Lfnx;

    iget-object v0, p1, Lczu;->d:Lfqg;

    iput-object v0, p0, Lbuk;->D:Lfqg;

    iget-object v0, v9, Lfnx;->e:Lflw;

    iput-object v0, p0, Lbuk;->B:Lflw;

    iget-object v0, p0, Lbuk;->B:Lflw;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lbuk;->c()V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lbuk;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v8

    iget-object v0, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, p0}, La;->d(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfrz;)Lboi;

    move-result-object v0

    iput-object v0, p0, Lbuk;->q:Lboi;

    iget-object v0, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget v0, p0, Lbuk;->t:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iput-object v0, p0, Lbuk;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iget-object v0, p0, Lbuk;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const v2, 0x7f08028e

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbuk;->h:Landroid/widget/ListView;

    const v0, 0x7f0400ff

    iget-object v2, p0, Lbuk;->h:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lbuk;->h:Landroid/widget/ListView;

    iget-object v2, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v0, p0, Lbuk;->h:Landroid/widget/ListView;

    const v2, 0x7f0400fe

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    iget-object v0, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    const v1, 0x7f0802e3

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbuk;->j:Landroid/widget/TextView;

    new-instance v10, Lbul;

    invoke-direct {v10, p0}, Lbul;-><init>(Lbuk;)V

    new-instance v0, Lbrh;

    iget-object v1, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v8}, Lari;->aD()Lcst;

    move-result-object v2

    invoke-virtual {v8}, Lari;->aO()Lcub;

    move-result-object v3

    invoke-virtual {v8}, Lari;->t()Lfeb;

    move-result-object v4

    invoke-virtual {v8}, Lari;->ay()Leyt;

    move-result-object v5

    iget-object v6, p0, Lbuk;->C:Levn;

    invoke-virtual {v8}, Lari;->o()Lglm;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lbrh;-><init>(Landroid/app/Activity;Lgix;Lcub;Lfeb;Leyt;Levn;Lglm;)V

    iput-object v0, p0, Lbuk;->r:Lbrh;

    iget-object v0, p0, Lbuk;->r:Lbrh;

    iget-object v1, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lbrh;->b(Landroid/view/View;)V

    iget-object v0, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    const v1, 0x7f0802e5

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbuk;->k:Landroid/widget/ImageView;

    iget-object v0, p0, Lbuk;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    const v1, 0x7f0802e4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbuk;->l:Landroid/widget/ImageView;

    iget-object v0, p0, Lbuk;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    const v1, 0x7f0802e6

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbuk;->m:Landroid/widget/ImageView;

    iget-object v0, p0, Lbuk;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbuk;->n:Landroid/widget/ImageView;

    iget-object v0, p0, Lbuk;->n:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lbuk;->n:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    new-instance v0, Lfsi;

    invoke-direct {v0}, Lfsi;-><init>()V

    iput-object v0, p0, Lbuk;->o:Lfsi;

    new-instance v0, Lcgz;

    iget-object v1, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v8}, Lari;->c()Leyp;

    move-result-object v2

    iget-object v3, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, v3, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    iget-object v4, p0, Lbuk;->c:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v5, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v6, p0, Lbuk;->b:Lbvc;

    invoke-static {v6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lbvc;

    invoke-virtual {v8}, Lari;->ae()Lfdw;

    move-result-object v7

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcgz;-><init>(Landroid/content/Context;Leyp;Lfhz;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lbvc;Lfdw;Lfrz;)V

    iget-object v1, p0, Lbuk;->o:Lfsi;

    const-class v2, Lflx;

    invoke-virtual {v1, v2, v0}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    new-instance v0, Lcex;

    iget-object v1, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lbuk;->C:Levn;

    const v3, 0x7f0400fb

    const v4, 0x7f0400fc

    invoke-direct {v0, v1, v2, v3, v4}, Lcex;-><init>(Landroid/content/Context;Levn;II)V

    iget-object v1, p0, Lbuk;->o:Lfsi;

    const-class v2, Lfvc;

    invoke-virtual {v1, v2, v0}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    iget-object v0, p0, Lbuk;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lbuk;->o:Lfsi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lbuk;->h:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    new-instance v0, Lbup;

    invoke-direct {v0, p0}, Lbup;-><init>(Lbuk;)V

    iget-object v1, p0, Lbuk;->v:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lbuk;->A:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lbuk;->x:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbuk;->w:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lbuk;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lbuk;->x:Landroid/view/View;

    const v1, 0x7f0802db

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbuk;->y:Landroid/widget/TextView;

    iget-object v0, p0, Lbuk;->x:Landroid/view/View;

    const v1, 0x7f0802dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbuk;->z:Landroid/widget/TextView;

    iget-object v0, p0, Lbuk;->z:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v0, Lbuj;

    iget-object v1, p0, Lbuk;->E:Lfdg;

    iget-object v2, p0, Lbuk;->C:Levn;

    invoke-static {}, Levn;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lbuk;->F:Leyt;

    iget-object v5, p0, Lbuk;->o:Lfsi;

    invoke-direct/range {v0 .. v5}, Lbuj;-><init>(Lfdg;Levn;Ljava/lang/Object;Leyt;Lfsi;)V

    iput-object v0, p0, Lbuk;->G:Lbuj;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbuk;->d:Z

    invoke-direct {p0}, Lbuk;->b()V

    :cond_1
    iget-object v0, p0, Lbuk;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a()V

    iget-object v0, p0, Lbuk;->i:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v1, v9, Lfnx;->e:Lflw;

    iget-object v0, v1, Lflw;->a:Lhsd;

    iget v0, v0, Lhsd;->l:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    invoke-direct {p0}, Lbuk;->d()V

    :goto_2
    iget-object v0, p0, Lbuk;->y:Landroid/widget/TextView;

    iget-object v1, p0, Lbuk;->B:Lflw;

    iget-object v1, v1, Lflw;->a:Lhsd;

    iget-object v1, v1, Lhsd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v9, Lfnx;->f:Lflc;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lflc;->c()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Lflc;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, Lbuk;->B:Lflw;

    invoke-virtual {v3}, Lflw;->c()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lbuk;->B:Lflw;

    invoke-virtual {v3}, Lflw;->b()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v0}, Lfvo;->a([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lbuk;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbuk;->j:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lbuk;->k:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lflc;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lbuk;->l:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lflc;->b()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_5
    iget-object v0, p0, Lbuk;->B:Lflw;

    iget-object v0, v0, Lflw;->a:Lhsd;

    iget-boolean v0, v0, Lhsd;->g:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lbuk;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lbuk;->p:Landroid/content/res/Resources;

    const v2, 0x7f090310

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbuk;->v:Landroid/widget/ImageView;

    iget-object v1, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v2, 0x7f0200ec

    invoke-static {v1, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lbuk;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_6
    iget-object v0, p0, Lbuk;->r:Lbrh;

    iget-object v1, p0, Lbuk;->B:Lflw;

    iget-object v2, v1, Lflw;->c:Lfkn;

    if-nez v2, :cond_3

    iget-object v2, v1, Lflw;->a:Lhsd;

    iget-object v2, v2, Lhsd;->i:Lhll;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lflw;->a:Lhsd;

    iget-object v2, v2, Lhsd;->i:Lhll;

    iget-object v2, v2, Lhll;->a:Lhlk;

    if-eqz v2, :cond_3

    new-instance v2, Lfkn;

    iget-object v3, v1, Lflw;->a:Lhsd;

    iget-object v3, v3, Lhsd;->i:Lhll;

    iget-object v3, v3, Lhll;->a:Lhlk;

    invoke-direct {v2, v3}, Lfkn;-><init>(Lhlk;)V

    iput-object v2, v1, Lflw;->c:Lfkn;

    :cond_3
    iget-object v1, v1, Lflw;->c:Lfkn;

    invoke-virtual {v0, v1}, Lbrh;->a(Lfkn;)V

    iget-object v0, p0, Lbuk;->o:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    iget-object v0, p0, Lbuk;->o:Lfsi;

    iget-object v1, p0, Lbuk;->B:Lflw;

    iget-object v1, v1, Lflw;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lbuk;->G:Lbuj;

    iget-object v1, p0, Lbuk;->B:Lflw;

    invoke-virtual {v1}, Lflw;->f()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbuj;->a(Ljava/util/List;)V

    iget-object v0, p0, Lbuk;->B:Lflw;

    iget-object v0, v0, Lflw;->a:Lhsd;

    iget v0, v0, Lhsd;->c:I

    iget-object v1, p0, Lbuk;->h:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lbuk;->h:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_5
    iget-object v0, v1, Lflw;->a:Lhsd;

    iget v0, v0, Lhsd;->l:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    :goto_7
    if-nez v0, :cond_8

    iget-object v0, v1, Lflw;->a:Lhsd;

    iget v0, v0, Lhsd;->l:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_8
    if-eqz v0, :cond_8

    sget-object v0, Lbum;->c:[I

    iget-object v1, p0, Lbuk;->s:Lbuo;

    invoke-virtual {v1}, Lbuo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    invoke-direct {p0}, Lbuk;->e()V

    goto/16 :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_7

    :cond_7
    const/4 v0, 0x0

    goto :goto_8

    :pswitch_2
    invoke-direct {p0}, Lbuk;->d()V

    goto/16 :goto_2

    :cond_8
    invoke-direct {p0}, Lbuk;->e()V

    goto/16 :goto_2

    :cond_9
    const/16 v0, 0x8

    goto/16 :goto_3

    :cond_a
    const/16 v0, 0x8

    goto/16 :goto_4

    :cond_b
    iget-object v0, p0, Lbuk;->B:Lflw;

    invoke-virtual {v0}, Lflw;->b()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lbuk;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbuk;->j:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lbuk;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lbuk;->B:Lflw;

    invoke-virtual {v1}, Lflw;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbuk;->k:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lbuk;->l:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_c
    iget-object v0, p0, Lbuk;->v:Landroid/widget/ImageView;

    iget-object v1, p0, Lbuk;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v2, 0x7f0200eb

    invoke-static {v1, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lbuk;->q:Lboi;

    iget-object v1, p0, Lbuk;->n:Landroid/widget/ImageView;

    iget-object v2, p0, Lbuk;->B:Lflw;

    invoke-static {v0, v1, v2}, La;->a(Lboi;Landroid/view/View;Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 306
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 313
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic i(Lbuk;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->v:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic j(Lbuk;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->A:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic k(Lbuk;)Lcom/google/android/apps/youtube/app/ui/SliderLayout;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbuk;->u:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    return-object v0
.end method


# virtual methods
.method public final B()Lfqg;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lbuk;->D:Lfqg;

    return-object v0
.end method

.method public final handlePlaylistLikeActionEvent(Lbui;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 337
    iget-object v0, p0, Lbuk;->B:Lflw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbuk;->B:Lflw;

    .line 338
    iget-object v0, v0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->d:Ljava/lang/String;

    iget-object v1, p1, Lbui;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbuk;->r:Lbrh;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lbuk;->r:Lbrh;

    iget-object v1, p1, Lbui;->b:Lbrg;

    invoke-virtual {v0, v1}, Lbrh;->a(Lbrg;)V

    .line 342
    :cond_0
    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 553
    add-int v0, p2, p3

    .line 559
    if-ne v0, p4, :cond_0

    iget v0, p0, Lbuk;->e:I

    iget-object v1, p0, Lbuk;->o:Lfsi;

    invoke-virtual {v1}, Lfsi;->getCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 560
    iget-object v0, p0, Lbuk;->o:Lfsi;

    invoke-virtual {v0}, Lfsi;->getCount()I

    move-result v0

    iput v0, p0, Lbuk;->e:I

    .line 561
    iget-object v0, p0, Lbuk;->G:Lbuj;

    sget-object v1, Lfjl;->a:Lfjl;

    invoke-virtual {v0, v1}, Lbuj;->a(Lfjl;)V

    .line 563
    :cond_0
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 545
    return-void
.end method
