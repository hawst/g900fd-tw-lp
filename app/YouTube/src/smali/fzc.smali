.class final Lfzc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgku;


# instance fields
.field final a:Lgku;

.field private final b:I


# direct methods
.method public constructor <init>(Lgku;I)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgku;

    iput-object v0, p0, Lfzc;->a:Lgku;

    .line 63
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "pageSize must be greater than zero"

    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    .line 64
    iput p2, p0, Lfzc;->b:I

    .line 65
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Leuc;)V
    .locals 3

    .prologue
    .line 56
    check-cast p1, Lfxg;

    iget-object v0, p1, Lfxg;->a:Landroid/net/Uri;

    const/4 v1, 0x1

    iget v2, p0, Lfzc;->b:I

    invoke-static {v0, v1, v2}, Lfzb;->a(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lfzc;->a:Lgku;

    invoke-virtual {p1, v0}, Lfxg;->b(Landroid/net/Uri;)Lfxg;

    move-result-object v0

    new-instance v2, Lfzd;

    invoke-direct {v2, p0, p1, p2}, Lfzd;-><init>(Lfzc;Lfxg;Leuc;)V

    invoke-interface {v1, v0, v2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    return-void
.end method
