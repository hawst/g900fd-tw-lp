.class final Lilx;
.super Limh;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Limh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Liln;Linc;)Linn;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p1, Liln;->f:Lios;

    if-eqz v0, :cond_0

    new-instance v0, Link;

    iget-object v1, p1, Liln;->f:Lios;

    invoke-direct {v0, p2, v1}, Link;-><init>(Linc;Lios;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Linf;

    iget-object v1, p1, Liln;->e:Limt;

    invoke-direct {v0, p2, v1}, Linf;-><init>(Linc;Limt;)V

    goto :goto_0
.end method

.method public final a(Lilw;)Lu;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p1, Lilw;->f:Lu;

    return-object v0
.end method

.method public final a(Liln;Lima;)V
    .locals 2

    .prologue
    .line 71
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "protocol == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p1, Liln;->g:Lima;

    .line 72
    return-void
.end method

.method public final a(Liln;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p1}, Liln;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Liln;->a:Lilo;

    monitor-enter v1

    :try_start_0
    iget-object v0, p1, Liln;->k:Ljava/lang/Object;

    if-eq v0, p2, :cond_1

    monitor-exit v1

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p1, Liln;->k:Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p1, Liln;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lilo;Liln;)V
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p2}, Liln;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Liln;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Liln;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p2, Liln;->c:Ljava/net/Socket;

    invoke-static {v0}, Limo;->a(Ljava/net/Socket;)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    :try_start_0
    invoke-static {}, Limj;->a()Limj;

    move-result-object v0

    iget-object v1, p2, Liln;->c:Ljava/net/Socket;

    invoke-virtual {v0, v1}, Limj;->b(Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    monitor-enter p1

    :try_start_1
    iget-object v0, p1, Lilo;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    iget v0, p2, Liln;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Liln;->j:I

    iget-object v0, p2, Liln;->f:Lios;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "spdyConnection != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    invoke-static {}, Limj;->a()Limj;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unable to untagSocket(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Limj;->a(Ljava/lang/String;)V

    iget-object v0, p2, Liln;->c:Ljava/net/Socket;

    invoke-static {v0}, Limo;->a(Ljava/net/Socket;)V

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p2, Liln;->h:J

    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p1, Lilo;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p1, Lilo;->c:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Lilt;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 83
    const-string v0, ":"

    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lilt;->b(Ljava/lang/String;Ljava/lang/String;)Lilt;

    .line 84
    :goto_0
    return-void

    .line 83
    :cond_0
    const-string v0, ":"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lilt;->b(Ljava/lang/String;Ljava/lang/String;)Lilt;

    goto :goto_0

    :cond_1
    const-string v0, ""

    invoke-virtual {p1, v0, p2}, Lilt;->b(Ljava/lang/String;Ljava/lang/String;)Lilt;

    goto :goto_0
.end method

.method public final a(Lilw;Liln;Linc;Limb;)V
    .locals 10

    .prologue
    const/high16 v9, 0x10000

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 104
    invoke-virtual {p2, p3}, Liln;->a(Ljava/lang/Object;)V

    iget-boolean v0, p2, Liln;->d:Z

    if-nez v0, :cond_13

    iget-object v0, p2, Liln;->b:Limg;

    iget-object v1, v0, Limg;->a:Lilj;

    iget-object v1, v1, Lilj;->e:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v1, :cond_0

    iget-object v0, v0, Limg;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iget v1, p1, Lilw;->p:I

    iget v4, p1, Lilw;->q:I

    iget v5, p1, Lilw;->r:I

    iget-boolean v6, p2, Liln;->d:Z

    if-eqz v6, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p4}, Limb;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Limb;->a()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Limo;->a(Ljava/net/URL;)I

    move-result v4

    const-string v0, "https"

    invoke-static {v0}, Limo;->a(Ljava/lang/String;)I

    move-result v0

    if-ne v4, v0, :cond_4

    move-object v0, v1

    :goto_2
    new-instance v5, Limc;

    invoke-direct {v5}, Limc;-><init>()V

    new-instance v6, Ljava/net/URL;

    const-string v7, "https"

    const-string v8, "/"

    invoke-direct {v6, v7, v1, v4, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v5, v6}, Limc;->a(Ljava/net/URL;)Limc;

    move-result-object v1

    const-string v4, "Host"

    invoke-virtual {v1, v4, v0}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    move-result-object v0

    const-string v1, "Proxy-Connection"

    const-string v4, "Keep-Alive"

    invoke-virtual {v0, v1, v4}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    move-result-object v0

    const-string v1, "User-Agent"

    invoke-virtual {p4, v1}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v4, "User-Agent"

    invoke-virtual {v0, v4, v1}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    :cond_2
    const-string v1, "Proxy-Authorization"

    invoke-virtual {p4, v1}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v4, "Proxy-Authorization"

    invoke-virtual {v0, v4, v1}, Limc;->a(Ljava/lang/String;Ljava/lang/String;)Limc;

    :cond_3
    invoke-virtual {v0}, Limc;->a()Limb;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xc

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    iget-object v6, p2, Liln;->b:Limg;

    iget-object v6, v6, Limg;->b:Ljava/net/Proxy;

    invoke-virtual {v6}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v6

    sget-object v7, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v6, v7, :cond_6

    iget-object v6, p2, Liln;->b:Limg;

    iget-object v6, v6, Limg;->b:Ljava/net/Proxy;

    invoke-virtual {v6}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v6

    sget-object v7, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v6, v7, :cond_9

    :cond_6
    iget-object v6, p2, Liln;->b:Limg;

    iget-object v6, v6, Limg;->a:Lilj;

    iget-object v6, v6, Lilj;->d:Ljavax/net/SocketFactory;

    invoke-virtual {v6}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v6

    iput-object v6, p2, Liln;->c:Ljava/net/Socket;

    :goto_3
    iget-object v6, p2, Liln;->c:Ljava/net/Socket;

    invoke-virtual {v6, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    invoke-static {}, Limj;->a()Limj;

    move-result-object v6

    iget-object v7, p2, Liln;->c:Ljava/net/Socket;

    iget-object v8, p2, Liln;->b:Limg;

    iget-object v8, v8, Limg;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v6, v7, v8, v1}, Limj;->a(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V

    iget-object v1, p2, Liln;->b:Limg;

    iget-object v1, v1, Limg;->a:Lilj;

    iget-object v1, v1, Lilj;->e:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v1, :cond_10

    invoke-static {}, Limj;->a()Limj;

    move-result-object v6

    if-eqz v0, :cond_7

    invoke-virtual {p2, v0, v4, v5}, Liln;->a(Limb;II)V

    :cond_7
    iget-object v0, p2, Liln;->b:Limg;

    iget-object v0, v0, Limg;->a:Lilj;

    iget-object v0, v0, Lilj;->e:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v1, p2, Liln;->c:Ljava/net/Socket;

    iget-object v4, p2, Liln;->b:Limg;

    iget-object v4, v4, Limg;->a:Lilj;

    iget-object v4, v4, Lilj;->b:Ljava/lang/String;

    iget-object v5, p2, Liln;->b:Limg;

    iget-object v5, v5, Limg;->a:Lilj;

    iget v5, v5, Lilj;->c:I

    invoke-virtual {v0, v1, v4, v5, v2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    iput-object v0, p2, Liln;->c:Ljava/net/Socket;

    iget-object v0, p2, Liln;->c:Ljava/net/Socket;

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iget-object v1, p2, Liln;->b:Limg;

    iget-object v1, v1, Limg;->a:Lilj;

    iget-object v1, v1, Lilj;->b:Ljava/lang/String;

    iget-object v4, p2, Liln;->b:Limg;

    iget-object v4, v4, Limg;->d:Ljava/lang/String;

    invoke-virtual {v6, v0, v1, v4}, Limj;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p2, Liln;->b:Limg;

    iget-object v1, v1, Limg;->d:Ljava/lang/String;

    const-string v4, "SSLv3"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    move v1, v2

    :goto_4
    if-eqz v1, :cond_8

    iget-object v4, p2, Liln;->b:Limg;

    iget-object v4, v4, Limg;->a:Lilj;

    iget-object v4, v4, Lilj;->h:Ljava/util/List;

    invoke-virtual {v6, v0, v4}, Limj;->a(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    :cond_8
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    iget-object v4, p2, Liln;->b:Limg;

    iget-object v4, v4, Limg;->a:Lilj;

    iget-object v4, v4, Lilj;->f:Ljavax/net/ssl/HostnameVerifier;

    iget-object v5, p2, Liln;->b:Limg;

    iget-object v5, v5, Limg;->a:Lilj;

    iget-object v5, v5, Lilj;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v7

    invoke-interface {v4, v5, v7}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v4

    if-nez v4, :cond_b

    new-instance v0, Ljava/io/IOException;

    iget-object v1, p2, Liln;->b:Limg;

    iget-object v1, v1, Limg;->a:Lilj;

    iget-object v1, v1, Lilj;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Hostname \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' was not verified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    new-instance v6, Ljava/net/Socket;

    iget-object v7, p2, Liln;->b:Limg;

    iget-object v7, v7, Limg;->b:Ljava/net/Proxy;

    invoke-direct {v6, v7}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    iput-object v6, p2, Liln;->c:Ljava/net/Socket;

    goto/16 :goto_3

    :cond_a
    move v1, v3

    goto :goto_4

    :cond_b
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v4

    invoke-static {v4}, Lilr;->a(Ljavax/net/ssl/SSLSession;)Lilr;

    move-result-object v4

    iput-object v4, p2, Liln;->i:Lilr;

    if-eqz v1, :cond_c

    invoke-virtual {v6, v0}, Limj;->a(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-static {v1}, Lima;->a(Ljava/lang/String;)Lima;

    move-result-object v1

    iput-object v1, p2, Liln;->g:Lima;

    :cond_c
    iget-object v1, p2, Liln;->g:Lima;

    sget-object v4, Lima;->c:Lima;

    if-eq v1, v4, :cond_d

    iget-object v1, p2, Liln;->g:Lima;

    sget-object v4, Lima;->d:Lima;

    if-ne v1, v4, :cond_f

    :cond_d
    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    new-instance v0, Lipa;

    iget-object v1, p2, Liln;->b:Limg;

    iget-object v1, v1, Limg;->a:Lilj;

    iget-object v1, v1, Lilj;->b:Ljava/lang/String;

    iget-object v4, p2, Liln;->c:Ljava/net/Socket;

    invoke-direct {v0, v1, v2, v4}, Lipa;-><init>(Ljava/lang/String;ZLjava/net/Socket;)V

    iget-object v1, p2, Liln;->g:Lima;

    iput-object v1, v0, Lipa;->d:Lima;

    new-instance v1, Lios;

    invoke-direct {v1, v0}, Lios;-><init>(Lipa;)V

    iput-object v1, p2, Liln;->f:Lios;

    iget-object v0, p2, Liln;->f:Lios;

    iget-object v1, v0, Lios;->i:Linu;

    invoke-interface {v1}, Linu;->a()V

    iget-object v1, v0, Lios;->i:Linu;

    iget-object v4, v0, Lios;->e:Lioo;

    invoke-interface {v1, v4}, Linu;->b(Lioo;)V

    iget-object v1, v0, Lios;->e:Lioo;

    invoke-virtual {v1, v9}, Lioo;->c(I)I

    move-result v1

    if-eq v1, v9, :cond_e

    iget-object v0, v0, Lios;->i:Linu;

    sub-int/2addr v1, v9

    int-to-long v4, v1

    invoke-interface {v0, v3, v4, v5}, Linu;->a(IJ)V

    :cond_e
    :goto_5
    iput-boolean v2, p2, Liln;->d:Z

    invoke-virtual {p2}, Liln;->e()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v1, p1, Lilw;->l:Lilo;

    invoke-virtual {p2}, Liln;->e()Z

    move-result v0

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_f
    new-instance v0, Limt;

    iget-object v1, p2, Liln;->a:Lilo;

    iget-object v3, p2, Liln;->c:Ljava/net/Socket;

    invoke-direct {v0, v1, p2, v3}, Limt;-><init>(Lilo;Liln;Ljava/net/Socket;)V

    iput-object v0, p2, Liln;->e:Limt;

    goto :goto_5

    :cond_10
    new-instance v0, Limt;

    iget-object v1, p2, Liln;->a:Lilo;

    iget-object v3, p2, Liln;->c:Ljava/net/Socket;

    invoke-direct {v0, v1, p2, v3}, Limt;-><init>(Lilo;Liln;Ljava/net/Socket;)V

    iput-object v0, p2, Liln;->e:Limt;

    goto :goto_5

    :cond_11
    iget-object v0, v1, Lilo;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v2, v1, Lilo;->c:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {p2}, Liln;->b()Z

    move-result v0

    if-eqz v0, :cond_12

    monitor-enter v1

    :try_start_0
    iget-object v0, v1, Lilo;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_12
    iget-object v0, p1, Lilw;->a:Limn;

    iget-object v1, p2, Liln;->b:Limg;

    invoke-virtual {v0, v1}, Limn;->b(Limg;)V

    :cond_13
    iget v0, p1, Lilw;->q:I

    iget v1, p1, Lilw;->r:I

    iget-boolean v2, p2, Liln;->d:Z

    if-nez v2, :cond_14

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setTimeouts - not connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_14
    iget-object v2, p2, Liln;->e:Limt;

    if-eqz v2, :cond_15

    iget-object v2, p2, Liln;->c:Ljava/net/Socket;

    invoke-virtual {v2, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v2, p2, Liln;->e:Limt;

    invoke-virtual {v2, v0, v1}, Limt;->a(II)V

    .line 105
    :cond_15
    return-void
.end method

.method public final a(Liln;)Z
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p1}, Liln;->a()Z

    move-result v0

    return v0
.end method

.method public final b(Liln;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p1, Liln;->j:I

    return v0
.end method

.method public final b(Lilw;)Limn;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p1, Lilw;->a:Limn;

    return-object v0
.end method

.method public final b(Liln;Linc;)V
    .locals 0

    .prologue
    .line 75
    invoke-virtual {p1, p2}, Liln;->a(Ljava/lang/Object;)V

    .line 76
    return-void
.end method

.method public final c(Liln;)Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p1, Liln;->e:Limt;

    if-eqz v0, :cond_0

    iget-object v0, p1, Liln;->e:Limt;

    invoke-virtual {v0}, Limt;->b()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
