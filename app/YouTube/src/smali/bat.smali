.class final Lbat;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field private synthetic a:Lbad;


# direct methods
.method constructor <init>(Lbad;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lbat;->a:Lbad;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 9

    .prologue
    .line 203
    iget-object v0, p0, Lbat;->a:Lbad;

    invoke-static {v0}, Lbad;->b(Lbad;)Lgnd;

    move-result-object v0

    invoke-interface {v0}, Lgnd;->d()Lgml;

    move-result-object v2

    .line 204
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 206
    iget-object v0, p0, Lbat;->a:Lbad;

    invoke-static {v0}, Lbad;->b(Lbad;)Lgnd;

    move-result-object v0

    invoke-interface {v0}, Lgnd;->g()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmb;

    .line 207
    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lgml;->h(Ljava/lang/String;)Lesq;

    move-result-object v1

    .line 208
    if-eqz v1, :cond_0

    .line 210
    iget-object v1, v1, Lesq;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Less;

    .line 213
    iget-object v7, v0, Lgmb;->a:Lgcd;

    iget-object v7, v7, Lgcd;->b:Ljava/lang/String;

    iget-object v8, v1, Less;->e:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Lgml;->a(Ljava/lang/String;Ljava/lang/String;)Lfoy;

    move-result-object v7

    .line 214
    if-eqz v7, :cond_1

    .line 219
    :try_start_0
    iget-object v8, v0, Lgmb;->a:Lgcd;

    iget-object v8, v8, Lgcd;->b:Ljava/lang/String;

    .line 220
    iget-object v1, v1, Less;->e:Ljava/lang/String;

    .line 221
    invoke-virtual {v7}, Lfoy;->b()Lfpc;

    move-result-object v7

    iput-wide v4, v7, Lfpc;->R:J

    invoke-virtual {v7}, Lfpc;->a()Lfoy;

    move-result-object v7

    .line 218
    invoke-virtual {v2, v8, v1, v7}, Lgml;->b(Ljava/lang/String;Ljava/lang/String;Lfoy;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 225
    :catch_0
    move-exception v1

    goto :goto_0

    .line 227
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 199
    check-cast p1, [Ljava/lang/Long;

    invoke-direct {p0, p1}, Lbat;->a([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 199
    iget-object v0, p0, Lbat;->a:Lbad;

    const-string v1, "All offline ad expiration times have been changed!"

    invoke-static {v0, v1, v2}, Leze;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    new-instance v0, Lbal;

    iget-object v1, p0, Lbat;->a:Lbad;

    invoke-direct {v0, v1}, Lbal;-><init>(Lbad;)V

    new-array v1, v2, [Ljava/lang/Void;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lbal;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
