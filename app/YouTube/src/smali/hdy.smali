.class public final Lhdy;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhpn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35078
    invoke-direct {p0}, Lidf;-><init>()V

    .line 35081
    const/4 v0, 0x0

    iput-object v0, p0, Lhdy;->a:Lhpn;

    .line 35078
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 35118
    const/4 v0, 0x0

    .line 35119
    iget-object v1, p0, Lhdy;->a:Lhpn;

    if-eqz v1, :cond_0

    .line 35120
    const v0, 0x467e3ba

    iget-object v1, p0, Lhdy;->a:Lhpn;

    .line 35121
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 35123
    :cond_0
    iget-object v1, p0, Lhdy;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35124
    iput v0, p0, Lhdy;->J:I

    .line 35125
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 35074
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhdy;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhdy;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhdy;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhdy;->a:Lhpn;

    if-nez v0, :cond_2

    new-instance v0, Lhpn;

    invoke-direct {v0}, Lhpn;-><init>()V

    iput-object v0, p0, Lhdy;->a:Lhpn;

    :cond_2
    iget-object v0, p0, Lhdy;->a:Lhpn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x233f1dd2 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 35109
    iget-object v0, p0, Lhdy;->a:Lhpn;

    if-eqz v0, :cond_0

    .line 35110
    const v0, 0x467e3ba

    iget-object v1, p0, Lhdy;->a:Lhpn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 35112
    :cond_0
    iget-object v0, p0, Lhdy;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 35114
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 35092
    if-ne p1, p0, :cond_1

    .line 35096
    :cond_0
    :goto_0
    return v0

    .line 35093
    :cond_1
    instance-of v2, p1, Lhdy;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 35094
    :cond_2
    check-cast p1, Lhdy;

    .line 35095
    iget-object v2, p0, Lhdy;->a:Lhpn;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhdy;->a:Lhpn;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhdy;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhdy;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 35096
    goto :goto_0

    .line 35095
    :cond_4
    iget-object v2, p0, Lhdy;->a:Lhpn;

    iget-object v3, p1, Lhdy;->a:Lhpn;

    invoke-virtual {v2, v3}, Lhpn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhdy;->I:Ljava/util/List;

    iget-object v3, p1, Lhdy;->I:Ljava/util/List;

    .line 35096
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 35100
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 35102
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdy;->a:Lhpn;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 35103
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhdy;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 35104
    return v0

    .line 35102
    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhpn;

    invoke-virtual {v0}, Lhpn;->hashCode()I

    move-result v0

    goto :goto_0

    .line 35103
    :cond_1
    iget-object v1, p0, Lhdy;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
