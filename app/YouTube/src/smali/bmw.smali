.class public final Lbmw;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Lddx;
.implements Ldec;


# instance fields
.field private a:Z

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 30
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lbmw;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 31
    const v0, 0x7f0200ba

    invoke-virtual {p0, v0}, Lbmw;->setImageResource(I)V

    .line 33
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lbmw;->setVisibility(I)V

    .line 34
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lbmw;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbmw;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lbmw;->c:I

    :goto_0
    invoke-super {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    return-void

    .line 77
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a_(Z)V
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lbmw;->b:Z

    if-ne v0, p1, :cond_0

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iput-boolean p1, p0, Lbmw;->b:Z

    .line 72
    invoke-direct {p0}, Lbmw;->c()V

    goto :goto_0
.end method

.method public final c_(Z)V
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lbmw;->a:Z

    if-ne v0, p1, :cond_0

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    iput-boolean p1, p0, Lbmw;->a:Z

    .line 63
    invoke-direct {p0}, Lbmw;->c()V

    goto :goto_0
.end method

.method public final f_()Landroid/view/View;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public final r_()Ldeg;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 43
    new-instance v0, Ldeg;

    invoke-direct {v0, v1, v1}, Ldeg;-><init>(II)V

    .line 45
    return-object v0
.end method

.method public final setVisibility(I)V
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lbmw;->c:I

    if-ne v0, p1, :cond_0

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    iput p1, p0, Lbmw;->c:I

    .line 54
    invoke-direct {p0}, Lbmw;->c()V

    goto :goto_0
.end method
