.class public final Lgsv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lgsv;


# instance fields
.field public final b:F

.field public final c:J

.field public d:Z

.field public e:Lgsx;

.field public f:Lgsz;

.field public g:Z

.field private h:Landroid/animation/ObjectAnimator;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    new-instance v0, Lgsv;

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, v2, v3}, Lgsv;-><init>(IJ)V

    sput-object v0, Lgsv;->a:Lgsv;

    return-void
.end method

.method public constructor <init>(IJ)V
    .locals 8

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    int-to-float v0, p1

    iput v0, p0, Lgsv;->b:F

    .line 71
    iput-wide p2, p0, Lgsv;->c:J

    .line 72
    new-instance v1, Lgsy;

    const-wide/16 v2, 0x0

    int-to-float v6, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lgsy;-><init>(JJF)V

    iput-object v1, p0, Lgsv;->e:Lgsx;

    .line 73
    return-void
.end method

.method static synthetic a(DDF)D
    .locals 4

    .prologue
    .line 27
    sub-double v0, p2, p0

    float-to-double v2, p4

    mul-double/2addr v0, v2

    add-double/2addr v0, p0

    return-wide v0
.end method

.method static synthetic a(Lgsv;)F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lgsv;->b:F

    return v0
.end method

.method static synthetic a(Lgsv;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic a(Lgsv;Lgsx;)Lgsx;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lgsv;->e:Lgsx;

    return-object p1
.end method

.method static synthetic b(Lgsv;)V
    .locals 0

    .prologue
    .line 27
    invoke-virtual {p0}, Lgsv;->b()V

    return-void
.end method

.method static synthetic c(Lgsv;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lgsv;->g:Z

    return v0
.end method

.method static synthetic d(Lgsv;)Lgsz;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lgsv;->f:Lgsz;

    return-object v0
.end method


# virtual methods
.method public final a(F)J
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lgsv;->e:Lgsx;

    invoke-interface {v0, p1}, Lgsx;->c(F)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lgsy;ZZ)V
    .locals 6

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 250
    iget-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 252
    const/4 v0, 0x0

    iput-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    .line 256
    :cond_0
    iget-object v0, p0, Lgsv;->e:Lgsx;

    instance-of v0, v0, Lgsy;

    invoke-static {v0}, Lb;->c(Z)V

    .line 258
    if-eqz p3, :cond_1

    .line 259
    iget-object v0, p0, Lgsv;->e:Lgsx;

    check-cast v0, Lgsy;

    .line 266
    if-nez p2, :cond_2

    move-object v3, p1

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    .line 273
    :goto_0
    new-instance v4, Lgsw;

    invoke-direct {v4, p0, v3, v1, p1}, Lgsw;-><init>(Lgsv;Lgsy;Lgsy;Lgsy;)V

    .line 278
    invoke-virtual {v4, v2}, Lgsw;->a(F)V

    .line 279
    const-string v1, "transitionProgress"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v0, v2, v3

    .line 280
    invoke-static {v4, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    .line 281
    iget-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    new-instance v1, Lgta;

    invoke-direct {v1, p0, p1}, Lgta;-><init>(Lgsv;Lgsy;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 282
    iget-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 283
    iget-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 284
    iput-object v4, p0, Lgsv;->e:Lgsx;

    .line 290
    :goto_1
    iput-boolean p2, p0, Lgsv;->d:Z

    .line 291
    return-void

    .line 286
    :cond_1
    iput-object p1, p0, Lgsv;->e:Lgsx;

    .line 287
    invoke-virtual {p0}, Lgsv;->b()V

    goto :goto_1

    :cond_2
    move-object v3, v0

    move v0, v2

    move v2, v1

    move-object v1, p1

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgsv;->h:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lgsv;->f:Lgsz;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lgsv;->f:Lgsz;

    invoke-interface {v0}, Lgsz;->a()V

    .line 297
    :cond_0
    return-void
.end method
