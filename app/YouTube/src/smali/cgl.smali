.class public final Lcgl;
.super Lfsb;
.source "SourceFile"


# instance fields
.field a:Lflu;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

.field private final g:Lfvi;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Lccs;

.field private final m:Lboi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;Lboi;Lfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0, p3, p5, p6}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 53
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcgl;->m:Lboi;

    .line 55
    const v0, 0x7f0400b2

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcgl;->b:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcgl;->b:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgl;->c:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lcgl;->b:Landroid/view/View;

    const v1, 0x7f080131

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgl;->d:Landroid/widget/TextView;

    .line 58
    iget-object v0, p0, Lcgl;->b:Landroid/view/View;

    const v1, 0x7f080126

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgl;->e:Landroid/widget/TextView;

    .line 59
    iget-object v0, p0, Lcgl;->b:Landroid/view/View;

    const v1, 0x7f080135

    .line 60
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    iput-object v0, p0, Lcgl;->f:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    .line 61
    new-instance v0, Lfvi;

    iget-object v1, p0, Lcgl;->f:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    .line 63
    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    invoke-direct {v0, p2, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcgl;->g:Lfvi;

    .line 64
    iget-object v0, p0, Lcgl;->b:Landroid/view/View;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcgl;->h:Landroid/view/View;

    .line 65
    iget-object v0, p0, Lcgl;->b:Landroid/view/View;

    const v1, 0x7f080267

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcgl;->i:Landroid/view/View;

    .line 67
    new-instance v0, Lccs;

    iget-object v1, p0, Lcgl;->b:Landroid/view/View;

    invoke-direct {v0, p2, v1}, Lccs;-><init>(Leyp;Landroid/view/View;)V

    iput-object v0, p0, Lcgl;->j:Lccs;

    .line 68
    iget-object v0, p0, Lcgl;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v0, p0, Lcgl;->i:Landroid/view/View;

    new-instance v1, Lcgm;

    invoke-direct {v1, p0, p3}, Lcgm;-><init>(Lcgl;Lfhz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method

.method private a(Lfsg;Lflu;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 84
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 85
    iput-object p2, p0, Lcgl;->a:Lflu;

    .line 88
    iget-object v0, p0, Lcgl;->j:Lccs;

    invoke-virtual {v0, p2}, Lccs;->a(Lfju;)Landroid/view/View;

    .line 91
    invoke-virtual {p2}, Lflu;->g()Lfly;

    move-result-object v2

    .line 93
    iget-object v0, p0, Lcgl;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Lfly;->c()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcgl;->d:Landroid/widget/TextView;

    iget-object v3, v2, Lfly;->e:Ljava/lang/CharSequence;

    if-nez v3, :cond_0

    iget-object v3, v2, Lfly;->a:Lhsi;

    iget-object v3, v3, Lhsi;->f:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfly;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v3, v2, Lfly;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lcgl;->e:Landroid/widget/TextView;

    iget-object v3, v2, Lfly;->d:Ljava/lang/CharSequence;

    if-nez v3, :cond_1

    iget-object v3, v2, Lfly;->a:Lhsi;

    iget-object v3, v3, Lhsi;->h:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfly;->d:Ljava/lang/CharSequence;

    :cond_1
    iget-object v3, v2, Lfly;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcgl;->f:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 97
    iget-object v3, v2, Lfly;->a:Lhsi;

    iget-wide v4, v3, Lhsi;->d:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 96
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v0, v2, Lfly;->c:Lfma;

    if-nez v0, :cond_2

    iget-object v0, v2, Lfly;->a:Lhsi;

    iget-object v0, v0, Lhsi;->j:Lhsk;

    if-eqz v0, :cond_2

    new-instance v0, Lfma;

    iget-object v3, v2, Lfly;->a:Lhsi;

    iget-object v3, v3, Lhsi;->j:Lhsk;

    invoke-direct {v0, v3}, Lfma;-><init>(Lhsk;)V

    iput-object v0, v2, Lfly;->c:Lfma;

    :cond_2
    iget-object v0, v2, Lfly;->c:Lfma;

    .line 101
    if-eqz v0, :cond_4

    .line 103
    invoke-virtual {v0}, Lfma;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 104
    iget-object v2, p0, Lcgl;->f:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b(Z)V

    .line 105
    iget-object v2, p0, Lcgl;->g:Lfvi;

    invoke-virtual {v0}, Lfma;->d()Lfnc;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lfvi;->a(Lfnc;Leyo;)V

    .line 116
    :goto_0
    iget-object v0, p0, Lcgl;->h:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcgl;->m:Lboi;

    iget-object v1, p0, Lcgl;->h:Landroid/view/View;

    invoke-static {v0, v1, p2}, La;->a(Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lcgl;->b:Landroid/view/View;

    return-object v0

    .line 107
    :cond_3
    iget-object v2, p0, Lcgl;->f:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    invoke-virtual {v2, v6}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b(Z)V

    .line 108
    iget-object v2, p0, Lcgl;->g:Lfvi;

    invoke-virtual {v0}, Lfma;->c()Lfnc;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lfvi;->a(Lfnc;Leyo;)V

    goto :goto_0

    .line 112
    :cond_4
    iget-object v0, p0, Lcgl;->f:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b(Z)V

    .line 113
    iget-object v3, p0, Lcgl;->g:Lfvi;

    iget-object v0, v2, Lfly;->b:Lfnc;

    if-nez v0, :cond_5

    iget-object v0, v2, Lfly;->a:Lhsi;

    iget-object v0, v0, Lhsi;->c:[Lhxf;

    array-length v0, v0

    if-lez v0, :cond_6

    iget-object v0, v2, Lfly;->a:Lhsi;

    iget-object v0, v0, Lhsi;->c:[Lhxf;

    aget-object v0, v0, v6

    :goto_1
    new-instance v4, Lfnc;

    invoke-direct {v4, v0}, Lfnc;-><init>(Lhxf;)V

    iput-object v4, v2, Lfly;->b:Lfnc;

    :cond_5
    iget-object v0, v2, Lfly;->b:Lfnc;

    invoke-virtual {v3, v0, v1}, Lfvi;->a(Lfnc;Leyo;)V

    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    check-cast p2, Lflu;

    invoke-direct {p0, p1, p2}, Lcgl;->a(Lfsg;Lflu;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    check-cast p2, Lflu;

    invoke-direct {p0, p1, p2}, Lcgl;->a(Lfsg;Lflu;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    check-cast p2, Lflu;

    invoke-direct {p0, p1, p2}, Lcgl;->a(Lfsg;Lflu;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
