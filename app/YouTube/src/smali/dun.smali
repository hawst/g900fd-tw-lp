.class public final Ldun;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldqr;
.implements Ldwq;


# static fields
.field static final a:Landroid/content/IntentFilter;

.field private static final i:Lorg/json/JSONObject;

.field private static final j:Landroid/net/Uri;

.field private static final k:J

.field private static final l:Ljava/util/regex/Pattern;

.field private static final m:Landroid/content/IntentFilter;

.field private static final n:Ljava/util/Map;


# instance fields
.field private final A:Z

.field private B:Z

.field private C:Ldst;

.field private final D:Ldxg;

.field private final E:Ljava/util/Map;

.field private F:Ljava/util/Set;

.field private final G:Ljava/lang/String;

.field private H:Ldwj;

.field final b:Landroid/content/Context;

.field final c:Ldur;

.field final d:Lduy;

.field final e:Ldyg;

.field f:Ldwr;

.field final g:Landroid/os/Handler;

.field h:Z

.field private final o:Lezj;

.field private final p:Ldqg;

.field private final q:Ldxx;

.field private final r:Lexd;

.field private final s:Ldrf;

.field private final t:Ldro;

.field private final u:Ljava/util/Map;

.field private final v:Ljava/util/Map;

.field private final w:Ljava/util/Map;

.field private final x:Landroid/content/SharedPreferences;

.field private final y:Ldxn;

.field private final z:Ldyd;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 100
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    sput-object v0, Ldun;->i:Lorg/json/JSONObject;

    .line 104
    const-string v0, "http://"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Ldun;->j:Landroid/net/Uri;

    .line 106
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ldun;->k:J

    .line 112
    const-string v0, ".*#dial$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Ldun;->l:Ljava/util/regex/Pattern;

    .line 114
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Ldun;->a:Landroid/content/IntentFilter;

    .line 115
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Ldun;->m:Landroid/content/IntentFilter;

    .line 118
    sget-object v0, Ldun;->a:Landroid/content/IntentFilter;

    sget-object v1, Ldta;->h:Ldta;

    invoke-virtual {v1}, Ldta;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Ldun;->a:Landroid/content/IntentFilter;

    sget-object v1, Ldta;->c:Ldta;

    invoke-virtual {v1}, Ldta;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Ldun;->a:Landroid/content/IntentFilter;

    sget-object v1, Ldta;->b:Ldta;

    invoke-virtual {v1}, Ldta;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Ldun;->m:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Ldun;->m:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 120
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 121
    const-string v1, "LOUNGE_SCREEN"

    sget-object v2, Ldsz;->b:Ldsz;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    const-string v1, "REMOTE_CONTROL"

    sget-object v2, Ldsz;->a:Ldsz;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Ldun;->n:Ljava/util/Map;

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lezj;Lexd;Landroid/content/SharedPreferences;Ldpo;Lduy;Ldyd;Ldyg;Ldqz;Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    sget-object v0, Ldwj;->f:Ldwj;

    iput-object v0, p0, Ldun;->H:Ldwj;

    .line 201
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Ldun;->o:Lezj;

    .line 202
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Ldun;->x:Landroid/content/SharedPreferences;

    .line 203
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lduy;

    iput-object v0, p0, Ldun;->d:Lduy;

    .line 204
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyd;

    iput-object v0, p0, Ldun;->z:Ldyd;

    .line 205
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldun;->e:Ldyg;

    .line 206
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldun;->G:Ljava/lang/String;

    .line 207
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldun;->b:Landroid/content/Context;

    .line 208
    iput-object p3, p0, Ldun;->r:Lexd;

    .line 209
    iput-boolean p12, p0, Ldun;->A:Z

    .line 211
    new-instance v0, Ldrj;

    invoke-direct {v0, p9}, Ldrj;-><init>(Ldqz;)V

    .line 212
    new-instance v1, Ldxj;

    new-instance v2, Ldrn;

    invoke-direct {v2, v0}, Ldrn;-><init>(Ldrj;)V

    invoke-direct {v1, v2, p9}, Ldxj;-><init>(Ldre;Ldqz;)V

    iput-object v1, p0, Ldun;->q:Ldxx;

    .line 215
    new-instance v1, Ldxi;

    invoke-direct {v1}, Ldxi;-><init>()V

    iput-object v1, p0, Ldun;->D:Ldxg;

    .line 216
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Ldun;->u:Ljava/util/Map;

    .line 217
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Ldun;->v:Ljava/util/Map;

    .line 220
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Ldun;->w:Ljava/util/Map;

    .line 221
    new-instance v1, Ldrf;

    invoke-direct {v1, v0}, Ldrf;-><init>(Ldrj;)V

    iput-object v1, p0, Ldun;->s:Ldrf;

    .line 222
    new-instance v1, Ldrl;

    invoke-direct {v1, v0}, Ldrl;-><init>(Ldrj;)V

    iput-object v1, p0, Ldun;->t:Ldro;

    .line 224
    new-instance v0, Ldur;

    invoke-direct {v0, p0}, Ldur;-><init>(Ldun;)V

    iput-object v0, p0, Ldun;->c:Ldur;

    .line 225
    new-instance v0, Ldxn;

    invoke-direct {v0, p11}, Ldxn;-><init>(Z)V

    iput-object v0, p0, Ldun;->y:Ldxn;

    .line 227
    new-instance v0, Landroid/os/HandlerThread;

    .line 228
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 229
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 231
    new-instance v1, Lduu;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lduu;-><init>(Ldun;Landroid/os/Looper;)V

    iput-object v1, p0, Ldun;->g:Landroid/os/Handler;

    .line 232
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldun;->E:Ljava/util/Map;

    .line 233
    new-instance v0, Lduw;

    invoke-direct {v0, p0}, Lduw;-><init>(Ldun;)V

    sget-object v1, Ldun;->m:Landroid/content/IntentFilter;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 235
    new-instance v0, Ldqg;

    invoke-direct {v0, p1, p5, p0}, Ldqg;-><init>(Landroid/content/Context;Ldpo;Ldqr;)V

    iput-object v0, p0, Ldun;->p:Ldqg;

    .line 239
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 711
    invoke-virtual {p0}, Ldun;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->b:Ldww;

    if-eq v0, v1, :cond_0

    .line 729
    :goto_0
    return-void

    .line 714
    :cond_0
    iget-object v0, p0, Ldun;->z:Ldyd;

    new-instance v1, Lduo;

    invoke-direct {v1, p0}, Lduo;-><init>(Ldun;)V

    invoke-interface {v0, v1}, Ldyd;->a(Leuc;)V

    goto :goto_0
.end method

.method static synthetic a(Ldun;Ldst;)Ldst;
    .locals 4

    .prologue
    .line 87
    iget-object v0, p1, Ldst;->e:Ldtb;

    if-eqz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, Ldun;->e:Ldyg;

    const-string v1, "No lounge token yet, will request"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldun;->v:Ljava/util/Map;

    iget-object v1, p1, Ldst;->b:Ldth;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldun;->e:Ldyg;

    const-string v1, "Found lounge token in cache"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldun;->v:Ljava/util/Map;

    iget-object v1, p1, Ldst;->b:Ldth;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldtb;

    move-object v1, v0

    :goto_1
    if-nez v1, :cond_2

    iget-object v0, p0, Ldun;->e:Ldyg;

    iget-object v1, p1, Ldst;->b:Ldth;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to retrieve lounge token for screenId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->b(Ljava/lang/String;)V

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldun;->s:Ldrf;

    const/4 v1, 0x1

    new-array v1, v1, [Ldth;

    const/4 v2, 0x0

    iget-object v3, p1, Ldst;->b:Ldth;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldrf;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p1, Ldst;->b:Ldth;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldtb;

    move-object v1, v0

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v1}, Ldst;->a(Ldtb;)Ldst;

    move-result-object v0

    iget-object v2, p0, Ldun;->v:Ljava/util/Map;

    iget-object v3, p1, Ldst;->b:Ldth;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object p1, v0

    goto :goto_0
.end method

.method private a(Lorg/json/JSONObject;Ldsz;)Ldsx;
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 1452
    :try_start_0
    const-string v0, "hasCc"

    .line 1453
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "hasCc"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1455
    :goto_0
    new-instance v7, Ldsy;

    invoke-direct {v7}, Ldsy;-><init>()V

    .line 1456
    iput-object p2, v7, Ldsy;->a:Ldsz;

    const-string v1, "id"

    .line 1457
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Ldsy;->b:Ljava/lang/String;

    const-string v1, "name"

    .line 1458
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Ldsy;->c:Ljava/lang/String;

    new-instance v1, Ldss;

    const-string v2, "clientName"

    .line 1459
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ldss;-><init>(Ljava/lang/String;)V

    iput-object v1, v7, Ldsy;->d:Ldss;

    const-string v1, "user"

    .line 1460
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Ldsy;->e:Ljava/lang/String;

    .line 1461
    iput-boolean v0, v7, Ldsy;->f:Z

    .line 1462
    new-instance v0, Ldsx;

    iget-object v1, v7, Ldsy;->a:Ldsz;

    iget-object v2, v7, Ldsy;->b:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, v7, Ldsy;->c:Ljava/lang/String;

    iget-object v5, v7, Ldsy;->d:Ldss;

    iget-object v6, v7, Ldsy;->e:Ljava/lang/String;

    iget-boolean v7, v7, Ldsy;->f:Z

    invoke-direct/range {v0 .. v7}, Ldsx;-><init>(Ldsz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldss;Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1466
    :goto_1
    return-object v0

    :cond_0
    move v0, v9

    .line 1453
    goto :goto_0

    .line 1463
    :catch_0
    move-exception v0

    .line 1464
    iget-object v1, p0, Ldun;->e:Ldyg;

    const-string v2, "Error parsing device object"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v9

    invoke-virtual {v1, v2, v3}, Ldyg;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v8

    .line 1466
    goto :goto_1
.end method

.method static synthetic a(Ldun;I)Ldwh;
    .locals 1

    .prologue
    .line 87
    packed-switch p1, :pswitch_data_0

    sget-object v0, Ldwh;->i:Ldwh;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Ldwh;->b:Ldwh;

    goto :goto_0

    :pswitch_1
    sget-object v0, Ldwh;->c:Ldwh;

    goto :goto_0

    :pswitch_2
    sget-object v0, Ldwh;->d:Ldwh;

    goto :goto_0

    :pswitch_3
    sget-object v0, Ldwh;->f:Ldwh;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lorg/json/JSONObject;)Ldwj;
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 1367
    new-instance v0, Ldwk;

    invoke-direct {v0}, Ldwk;-><init>()V

    .line 1368
    const-string v2, "listId"

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldwk;->b(Ljava/lang/String;)Ldwk;

    move-result-object v2

    .line 1369
    const-string v0, "videoId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "videoId"

    const-string v3, ""

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ldwk;->a(Ljava/lang/String;)Ldwk;

    move-result-object v2

    .line 1371
    const-string v0, "currentIndex"

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_1

    add-int/lit8 v0, v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Ldwk;->a(I)Ldwk;

    move-result-object v0

    .line 1372
    invoke-virtual {v0}, Ldwk;->a()Ldwj;

    move-result-object v0

    return-object v0

    .line 1369
    :cond_0
    const-string v0, "video_id"

    const-string v3, ""

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1371
    goto :goto_1
.end method

.method static synthetic a(Ldun;Ldwr;)Ldwr;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Ldun;->f:Ldwr;

    return-object p1
.end method

.method static synthetic a(Ldun;)Ldyg;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->e:Ldyg;

    return-object v0
.end method

.method private a(Ldst;Ldus;)V
    .locals 8

    .prologue
    .line 977
    iput-object p1, p0, Ldun;->C:Ldst;

    .line 978
    iget-object v0, p0, Ldun;->b:Landroid/content/Context;

    iget-object v1, p0, Ldun;->c:Ldur;

    sget-object v2, Ldun;->a:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 982
    new-instance v0, Ldpu;

    invoke-direct {v0}, Ldpu;-><init>()V

    iget-object v1, p1, Ldst;->e:Ldtb;

    iput-object v1, v0, Ldpu;->c:Ldtb;

    iget-object v1, p2, Ldus;->b:Ldwj;

    invoke-virtual {v1}, Ldwj;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Ldtc;->u:Ldtc;

    iput-object v1, v0, Ldpu;->a:Ldtc;

    iget-object v1, p2, Ldus;->b:Ldwj;

    invoke-static {v1}, Ldun;->e(Ldwj;)Ldte;

    move-result-object v1

    iput-object v1, v0, Ldpu;->b:Ldte;

    :cond_0
    iget-boolean v1, p2, Ldus;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldpu;->d:Z

    :cond_1
    invoke-virtual {v0}, Ldpu;->a()Ldpt;

    move-result-object v1

    .line 983
    iget-object v0, p1, Ldst;->b:Ldth;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Connecting to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " with "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 984
    invoke-virtual {v1}, Ldpt;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 985
    iget-object v3, p0, Ldun;->e:Ldyg;

    iget-object v0, v1, Ldpt;->a:Ldtc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 986
    invoke-virtual {v1}, Ldpt;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v1, Ldpt;->b:Ldte;

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x3

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 985
    invoke-virtual {v3, v0}, Ldyg;->a(Ljava/lang/String;)V

    .line 991
    :goto_1
    iget-object v0, p0, Ldun;->p:Ldqg;

    invoke-virtual {v0, v1}, Ldqg;->a(Ldpt;)Ljava/util/concurrent/CountDownLatch;

    .line 992
    return-void

    .line 986
    :cond_2
    const-string v0, "{}"

    goto :goto_0

    .line 988
    :cond_3
    iget-object v0, p0, Ldun;->e:Ldyg;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "no message."

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldyg;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Ldtc;Ldte;)V
    .locals 6

    .prologue
    .line 663
    iget-object v0, p0, Ldun;->e:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Sending "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 664
    iget-object v0, p0, Ldun;->p:Ldqg;

    sget-object v1, Ldqg;->a:Ljava/util/List;

    invoke-virtual {v0, p1, p2, v1}, Ldqg;->a(Ldtc;Ldte;Ljava/util/List;)V

    .line 665
    return-void
.end method

.method private a(Ldtm;Ldus;)V
    .locals 8

    .prologue
    .line 1059
    iget-boolean v0, p1, Ldtm;->d:Z

    if-nez v0, :cond_0

    .line 1060
    iget-object v0, p0, Ldun;->e:Ldyg;

    const-string v1, "Not launching non-Dial device"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 1122
    :goto_0
    return-void

    .line 1064
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldun;->B:Z

    .line 1065
    iget-object v0, p0, Ldun;->e:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Ldtm;->b:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Sending launch request for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 1066
    iget-object v1, p0, Ldun;->q:Ldxx;

    .line 1067
    iget-object v2, p1, Ldtm;->b:Landroid/net/Uri;

    .line 1068
    iget-object v0, p2, Ldus;->b:Ldwj;

    iget-object v3, v0, Ldwj;->a:Ljava/lang/String;

    .line 1069
    iget-object v0, p2, Ldus;->b:Ldwj;

    iget v0, v0, Ldwj;->b:I

    int-to-long v4, v0

    const/4 v6, 0x0

    new-instance v7, Ldup;

    invoke-direct {v7, p0, p1, p2}, Ldup;-><init>(Ldun;Ldtm;Ldus;)V

    .line 1066
    invoke-interface/range {v1 .. v7}, Ldxx;->a(Landroid/net/Uri;Ljava/lang/String;JLdtd;Ldxy;)V

    goto :goto_0
.end method

.method static synthetic a(Ldun;Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->p:Ldqg;

    iget-boolean v0, v0, Ldqg;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldun;->p:Ldqg;

    invoke-virtual {v0}, Ldqg;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Ldun;->p:Ldqg;

    invoke-virtual {v0, p2}, Ldqg;->a(Z)V

    :cond_1
    invoke-direct {p0}, Ldun;->z()V

    iget-object v0, p0, Ldun;->d:Lduy;

    const-string v1, ""

    iput-object v1, v0, Lduy;->l:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, v0, Lduy;->k:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Ldun;Ldst;Ldus;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ldun;->a(Ldst;Ldus;)V

    return-void
.end method

.method static synthetic a(Ldun;Ldtc;Ldte;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ldun;->a(Ldtc;Ldte;)V

    return-void
.end method

.method static synthetic a(Ldun;Ldtm;Ldst;)V
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->u:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldun;->x:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p1, Ldtm;->e:Ldtj;

    invoke-virtual {v1}, Ldtj;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Ldst;->b:Ldth;

    invoke-virtual {v2}, Ldth;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method static synthetic a(Ldun;Ldtm;Ldus;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ldun;->a(Ldtm;Ldus;)V

    return-void
.end method

.method private a(Ldwo;)V
    .locals 2

    .prologue
    .line 1184
    sget-object v0, Ldwo;->g:Ldwo;

    if-ne p1, v0, :cond_1

    .line 1186
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->f:Ldxb;

    invoke-virtual {v0}, Ldxb;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1187
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldxb;->b:Ldxb;

    invoke-virtual {v0, v1}, Lduy;->a(Ldxb;)V

    .line 1188
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldwo;->c:Ldwo;

    invoke-virtual {v0, v1}, Lduy;->b(Ldwo;)V

    .line 1203
    :cond_0
    :goto_0
    iget-object v0, p0, Ldun;->d:Lduy;

    invoke-virtual {v0, p1}, Lduy;->a(Ldwo;)V

    .line 1204
    return-void

    .line 1192
    :cond_1
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->f:Ldxb;

    invoke-virtual {v0}, Ldxb;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1193
    iget-object v0, p0, Ldun;->d:Lduy;

    invoke-virtual {v0}, Lduy;->c()V

    .line 1195
    :cond_2
    iget-boolean v0, p1, Ldwo;->l:Z

    if-eqz v0, :cond_3

    .line 1196
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldxb;->c:Ldxb;

    invoke-virtual {v0, v1}, Lduy;->a(Ldxb;)V

    goto :goto_0

    .line 1197
    :cond_3
    sget-object v0, Ldwo;->b:Ldwo;

    if-ne p1, v0, :cond_4

    .line 1198
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldxb;->d:Ldxb;

    invoke-virtual {v0, v1}, Lduy;->a(Ldxb;)V

    goto :goto_0

    .line 1200
    :cond_4
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldxb;->a:Ldxb;

    invoke-virtual {v0, v1}, Lduy;->a(Ldxb;)V

    goto :goto_0
.end method

.method static synthetic a(Ldun;Z)Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldun;->B:Z

    return v0
.end method

.method static synthetic b(Ldun;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Ldun;Ldst;)Ldst;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Ldun;->C:Ldst;

    return-object v0
.end method

.method private b(Lorg/json/JSONObject;)Ljava/util/Map;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 1470
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1471
    sget-object v0, Ldsz;->b:Ldsz;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1472
    sget-object v0, Ldsz;->a:Ldsz;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1475
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    const-string v0, "devices"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v2, v3

    .line 1476
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1478
    :try_start_1
    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 1479
    sget-object v0, Ldun;->n:Ljava/util/Map;

    const-string v1, "type"

    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsz;

    .line 1480
    if-eqz v0, :cond_0

    .line 1481
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-direct {p0, v6, v0}, Ldun;->a(Lorg/json/JSONObject;Ldsz;)Ldsx;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1476
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1483
    :catch_0
    move-exception v0

    .line 1484
    :try_start_2
    iget-object v1, p0, Ldun;->e:Ldyg;

    const-string v6, "Error parsing lounge status message"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-virtual {v1, v6, v7}, Ldyg;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 1487
    :catch_1
    move-exception v0

    .line 1488
    iget-object v1, p0, Ldun;->e:Ldyg;

    const-string v2, "Error parsing lounge status message"

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v0, v5, v3

    invoke-virtual {v1, v2, v5}, Ldyg;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1490
    :cond_1
    return-object v4
.end method

.method static synthetic b(Ldun;Ldtm;Ldus;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 87
    invoke-virtual {p0, p1}, Ldun;->a(Ldtm;)Ldst;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v0, p0, Ldun;->e:Ldyg;

    const-string v4, "Found associated cloud screen %s for device %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v2

    aput-object p1, v5, v1

    invoke-virtual {v0, v4, v5}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v0, p1, Ldtm;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldun;->D:Ldxg;

    iget-object v4, p1, Ldtm;->b:Landroid/net/Uri;

    invoke-interface {v0, v4}, Ldxg;->a(Landroid/net/Uri;)Ldsp;

    move-result-object v0

    :goto_0
    iget-object v4, p0, Ldun;->e:Ldyg;

    iget v5, v0, Ldsp;->c:I

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x2c

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Application status for device is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ldyg;->a(Ljava/lang/String;)V

    iget v0, v0, Ldsp;->c:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Ldun;->t:Ldro;

    new-array v4, v1, [Ldst;

    aput-object v3, v4, v2

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ldro;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Ldun;->e:Ldyg;

    const-string v1, "Screen appears to be online. Will not send a launch request."

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    invoke-direct {p0, v3, p2}, Ldun;->a(Ldst;Ldus;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    iget-object v0, p1, Ldtm;->a:Ldsp;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    invoke-direct {p0, p1, p2}, Ldun;->a(Ldtm;Ldus;)V

    if-eqz v3, :cond_0

    invoke-direct {p0, v3, p2}, Ldun;->a(Ldst;Ldus;)V

    goto :goto_2
.end method

.method static synthetic b(Ldun;Ldwr;)V
    .locals 5

    .prologue
    .line 87
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldwr;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldun;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldwr;->f()Ldwf;

    move-result-object v0

    iget-object v1, v0, Ldwf;->a:Ldtm;

    iget-object v1, v1, Ldtm;->a:Ldsp;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ldwf;->a:Ldtm;

    iget-object v1, v1, Ldtm;->a:Ldsp;

    iget-boolean v1, v1, Ldsp;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldun;->E:Ljava/util/Map;

    iget-object v2, v0, Ldwf;->a:Ldtm;

    iget-object v2, v2, Ldtm;->e:Ldtj;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldun;->E:Ljava/util/Map;

    iget-object v0, v0, Ldwf;->a:Ldtm;

    iget-object v0, v0, Ldtm;->e:Ldtj;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    :goto_0
    iget-object v1, p0, Ldun;->e:Ldyg;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Sending stop request to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldyg;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Ldun;->q:Ldxx;

    invoke-interface {v1, v0}, Ldxx;->a(Landroid/net/Uri;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, v0, Ldwf;->a:Ldtm;

    iget-boolean v1, v1, Ldtm;->d:Z

    if-eqz v1, :cond_2

    iget-object v0, v0, Ldwf;->a:Ldtm;

    iget-object v0, v0, Ldtm;->b:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v1, p0, Ldun;->D:Ldxg;

    invoke-interface {v1, v0}, Ldxg;->a(Landroid/net/Uri;)Ldsp;

    move-result-object v1

    if-eqz v1, :cond_2

    iget v2, v1, Ldsp;->c:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    iget-object v1, v1, Ldsp;->a:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ldwj;)V
    .locals 1

    .prologue
    .line 616
    iput-object p1, p0, Ldun;->H:Ldwj;

    .line 617
    iget-object v0, p0, Ldun;->H:Ldwj;

    invoke-virtual {v0}, Ldwj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 618
    sget-object v0, Ldwo;->i:Ldwo;

    invoke-direct {p0, v0}, Ldun;->a(Ldwo;)V

    .line 620
    :cond_0
    return-void
.end method

.method static synthetic b(Ldun;Z)Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldun;->h:Z

    return v0
.end method

.method static synthetic c(Ldun;)Ldst;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->C:Ldst;

    return-object v0
.end method

.method private c(Ldwj;)V
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->i:Ldwj;

    iget-object v1, p1, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldwj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 652
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->g:Ldwo;

    sget-object v1, Ldwo;->c:Ldwo;

    if-eq v0, v1, :cond_0

    .line 653
    invoke-virtual {p0}, Ldun;->a()V

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 658
    :cond_1
    invoke-direct {p0, p1}, Ldun;->b(Ldwj;)V

    .line 659
    sget-object v0, Ldtc;->u:Ldtc;

    invoke-static {p1}, Ldun;->e(Ldwj;)Ldte;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Ldun;->a(Ldtc;Ldte;)V

    goto :goto_0
.end method

.method private c(Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 1494
    const-string v0, "currentTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1495
    iget-object v0, p0, Ldun;->d:Lduy;

    const-string v1, "currentTime"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lduy;->a(J)V

    .line 1500
    :cond_0
    :goto_0
    return-void

    .line 1496
    :cond_1
    const-string v0, "current_time"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1498
    iget-object v0, p0, Ldun;->d:Lduy;

    const-string v1, "current_time"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lduy;->a(J)V

    goto :goto_0
.end method

.method static synthetic d(Ldun;)Ldqg;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->p:Ldqg;

    return-object v0
.end method

.method private d(Ldwj;)Ldwj;
    .locals 6

    .prologue
    .line 685
    invoke-virtual {p1}, Ldwj;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 686
    sget-object v0, Ldwj;->f:Ldwj;

    .line 701
    :goto_0
    return-object v0

    .line 689
    :cond_0
    invoke-virtual {p1}, Ldwj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 690
    iget-object v0, p1, Ldwj;->d:Ljava/lang/String;

    .line 694
    :goto_1
    iget v1, p1, Ldwj;->b:I

    int-to-long v2, v1

    const-wide/16 v4, 0x1388

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    const/4 v1, 0x0

    .line 698
    :goto_2
    new-instance v2, Ldwk;

    invoke-direct {v2, p1}, Ldwk;-><init>(Ldwj;)V

    .line 699
    invoke-virtual {v2, v0}, Ldwk;->b(Ljava/lang/String;)Ldwk;

    move-result-object v0

    .line 700
    iput v1, v0, Ldwk;->b:I

    .line 701
    invoke-virtual {v0}, Ldwk;->a()Ldwj;

    move-result-object v0

    goto :goto_0

    .line 690
    :cond_1
    iget-object v0, p0, Ldun;->C:Ldst;

    .line 691
    invoke-static {v0}, Ldyh;->a(Ldst;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 696
    :cond_2
    iget v1, p1, Ldwj;->b:I

    goto :goto_2
.end method

.method private d(Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 1503
    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1504
    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ldwo;->a(I)Ldwo;

    move-result-object v0

    invoke-direct {p0, v0}, Ldun;->a(Ldwo;)V

    .line 1506
    :cond_0
    return-void
.end method

.method private static e(Ldwj;)Ldte;
    .locals 4

    .prologue
    .line 1014
    new-instance v1, Ldte;

    invoke-direct {v1}, Ldte;-><init>()V

    .line 1015
    const-string v0, "videoId"

    iget-object v2, p0, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1016
    const-string v0, "listId"

    iget-object v2, p0, Ldwj;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1017
    const-string v2, "currentIndex"

    .line 1020
    iget v0, p0, Ldwj;->e:I

    if-lez v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    .line 1019
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 1017
    invoke-virtual {v1, v2, v0}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1021
    const-string v0, "currentTime"

    .line 1023
    iget v2, p0, Ldwj;->b:I

    div-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 1021
    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1024
    return-object v1

    .line 1020
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic e(Ldun;)Ldxn;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->y:Ldxn;

    return-object v0
.end method

.method private e(Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 1541
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->j:Lfoy;

    if-eqz v0, :cond_0

    const-string v0, "currentTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1542
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-string v2, "currentTime"

    .line 1543
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 1542
    invoke-virtual {v0, v2, v3}, Lduy;->b(J)V

    .line 1545
    :cond_0
    return-void
.end method

.method static synthetic f(Ldun;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->g:Landroid/os/Handler;

    return-object v0
.end method

.method private f(Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 1548
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->j:Lfoy;

    if-eqz v0, :cond_1

    const-string v0, "adState"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1549
    const-string v0, "adState"

    .line 1550
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ldwo;->a(I)Ldwo;

    move-result-object v0

    .line 1549
    iget-boolean v1, v0, Ldwo;->l:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldun;->d:Lduy;

    iget-object v1, v1, Lduy;->f:Ldxb;

    invoke-virtual {v1}, Ldxb;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldun;->d:Lduy;

    sget-object v2, Ldxb;->b:Ldxb;

    invoke-virtual {v1, v2}, Lduy;->a(Ldxb;)V

    iget-object v1, p0, Ldun;->d:Lduy;

    sget-object v2, Ldwo;->g:Ldwo;

    invoke-virtual {v1, v2}, Lduy;->a(Ldwo;)V

    :cond_0
    iget-object v1, p0, Ldun;->d:Lduy;

    invoke-virtual {v1, v0}, Lduy;->b(Ldwo;)V

    .line 1552
    :cond_1
    return-void
.end method

.method static synthetic g(Ldun;)Lduy;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->d:Lduy;

    return-object v0
.end method

.method static synthetic h(Ldun;)Ldxx;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->q:Ldxx;

    return-object v0
.end method

.method static synthetic i(Ldun;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->E:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic j(Ldun;)Lexd;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldun;->r:Lexd;

    return-object v0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 705
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldxb;->a:Ldxb;

    invoke-virtual {v0, v1}, Lduy;->a(Ldxb;)V

    .line 706
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldwo;->a:Ldwo;

    invoke-virtual {v0, v1}, Lduy;->a(Ldwo;)V

    invoke-virtual {v0}, Lduy;->b()V

    sget-object v1, Ldwj;->f:Ldwj;

    invoke-virtual {v0, v1}, Lduy;->a(Ldwj;)V

    .line 707
    iget-object v0, p0, Ldun;->d:Lduy;

    invoke-virtual {v0}, Lduy;->c()V

    .line 708
    return-void
.end method


# virtual methods
.method public final a(Ldtm;)Ldst;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 737
    iget-object v0, p0, Ldun;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 738
    iget-object v0, p0, Ldun;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    .line 758
    :goto_0
    return-object v0

    .line 741
    :cond_0
    invoke-virtual {p1}, Ldtm;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Ldtm;->a:Ldsp;

    iget-object v0, v0, Ldsp;->b:Ldth;

    .line 742
    :goto_1
    if-nez v0, :cond_4

    .line 744
    iget-object v0, p1, Ldtm;->e:Ldtj;

    invoke-virtual {v0}, Ldtj;->toString()Ljava/lang/String;

    move-result-object v1

    .line 745
    iget-object v0, p0, Ldun;->x:Landroid/content/SharedPreferences;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 746
    new-instance v0, Ldth;

    iget-object v3, p0, Ldun;->x:Landroid/content/SharedPreferences;

    const-string v4, ""

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldth;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 751
    :goto_2
    iget-object v0, p0, Ldun;->s:Ldrf;

    const/4 v3, 0x1

    new-array v3, v3, [Ldth;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldrf;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldtb;

    .line 752
    if-nez v0, :cond_3

    .line 753
    iget-object v0, p0, Ldun;->e:Ldyg;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to retrieve lounge token for screenId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->b(Ljava/lang/String;)V

    move-object v0, v2

    .line 754
    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 741
    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 748
    goto :goto_0

    .line 756
    :cond_3
    new-instance v3, Ldst;

    iget-object v4, p1, Ldtm;->c:Ljava/lang/String;

    invoke-direct {v3, v1, v4, v2, v0}, Ldst;-><init>(Ldth;Ljava/lang/String;Ldss;Ldtb;)V

    .line 757
    iget-object v0, p0, Ldun;->u:Ljava/util/Map;

    invoke-interface {v0, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v3

    .line 758
    goto :goto_0

    :cond_4
    move-object v1, v0

    goto :goto_2
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 366
    sget-object v0, Ldtc;->l:Ldtc;

    sget-object v1, Ldte;->a:Ldte;

    invoke-direct {p0, v0, v1}, Ldun;->a(Ldtc;Ldte;)V

    .line 367
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 424
    new-instance v0, Ldte;

    invoke-direct {v0}, Ldte;-><init>()V

    .line 425
    const-string v1, "newTime"

    div-int/lit16 v2, p1, 0x3e8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 426
    sget-object v1, Ldtc;->t:Ldtc;

    invoke-direct {p0, v1, v0}, Ldun;->a(Ldtc;Ldte;)V

    .line 427
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 409
    new-instance v0, Ldte;

    invoke-direct {v0}, Ldte;-><init>()V

    .line 410
    const-string v1, "delta"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 411
    const-string v1, "volume"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 412
    sget-object v1, Ldtc;->w:Ldtc;

    invoke-direct {p0, v1, v0}, Ldun;->a(Ldtc;Ldte;)V

    .line 413
    return-void
.end method

.method public final a(Ldwj;)V
    .locals 3

    .prologue
    .line 354
    invoke-virtual {p1}, Ldwj;->a()Z

    move-result v0

    invoke-static {v0}, Lb;->b(Z)V

    .line 355
    invoke-direct {p0, p1}, Ldun;->d(Ldwj;)Ldwj;

    move-result-object v0

    .line 357
    iget-object v1, p0, Ldun;->d:Lduy;

    iget-object v1, v1, Lduy;->d:Ldww;

    sget-object v2, Ldww;->f:Ldww;

    if-ne v1, v2, :cond_0

    .line 358
    iget-object v1, p0, Ldun;->f:Ldwr;

    invoke-virtual {p0, v1, v0}, Ldun;->a(Ldwr;Ldwj;)V

    .line 362
    :goto_0
    return-void

    .line 361
    :cond_0
    invoke-direct {p0, v0}, Ldun;->c(Ldwj;)V

    goto :goto_0
.end method

.method public final a(Ldwr;Ldwj;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 286
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    invoke-direct {p0, p2}, Ldun;->d(Ldwj;)Ldwj;

    move-result-object v2

    .line 289
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->d:Ldww;

    sget-object v3, Ldww;->b:Ldww;

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Ldun;->f:Ldwr;

    invoke-virtual {p1, v0}, Ldwr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 290
    invoke-virtual {v2}, Ldwj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    invoke-direct {p0, v2}, Ldun;->c(Ldwj;)V

    .line 313
    :goto_1
    return-void

    .line 289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 293
    :cond_1
    iget-object v0, p0, Ldun;->e:Ldyg;

    const-string v1, "Ignore connectAndPlay on a CONNECTED remote control,no videoId specified."

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 299
    :cond_2
    iput-object p1, p0, Ldun;->f:Ldwr;

    .line 300
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v3, Ldww;->a:Ldww;

    invoke-virtual {v0, v3}, Lduy;->a(Ldww;)V

    .line 301
    invoke-direct {p0}, Ldun;->z()V

    .line 302
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldun;->F:Ljava/util/Set;

    .line 303
    invoke-direct {p0, v2}, Ldun;->b(Ldwj;)V

    .line 305
    new-instance v0, Ldus;

    invoke-direct {v0, p1, v2, v1}, Ldus;-><init>(Ldwr;Ldwj;Z)V

    .line 310
    iget-object v1, p0, Ldun;->g:Landroid/os/Handler;

    iget-object v2, p0, Ldun;->g:Landroid/os/Handler;

    const/4 v3, 0x3

    .line 311
    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 310
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method

.method public final a(Lgpa;)V
    .locals 0

    .prologue
    .line 552
    invoke-virtual {p0, p1}, Ldun;->b(Lgpa;)V

    .line 553
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 431
    new-instance v0, Ldte;

    invoke-direct {v0}, Ldte;-><init>()V

    .line 432
    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 433
    const-string v1, "videoSources"

    const-string v2, "XX"

    invoke-virtual {v0, v1, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 434
    sget-object v1, Ldtc;->a:Ldtc;

    invoke-direct {p0, v1, v0}, Ldun;->a(Ldtc;Ldte;)V

    .line 435
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 345
    new-instance v0, Ldwk;

    invoke-direct {v0}, Ldwk;-><init>()V

    .line 346
    invoke-virtual {v0, p2}, Ldwk;->b(Ljava/lang/String;)Ldwk;

    move-result-object v0

    .line 347
    invoke-virtual {v0, p1}, Ldwk;->a(Ljava/lang/String;)Ldwk;

    move-result-object v0

    .line 348
    invoke-virtual {v0, p3}, Ldwk;->a(I)Ldwk;

    move-result-object v0

    .line 349
    invoke-virtual {v0}, Ldwk;->a()Ldwj;

    move-result-object v0

    .line 345
    invoke-virtual {p0, v0}, Ldun;->a(Ldwj;)V

    .line 350
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 439
    new-instance v0, Ldte;

    invoke-direct {v0}, Ldte;-><init>()V

    .line 440
    const-string v1, "videoIds"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 441
    const-string v1, "videoSources"

    const-string v2, "XX"

    invoke-virtual {v0, v1, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 442
    sget-object v1, Ldtc;->b:Ldtc;

    invoke-direct {p0, v1, v0}, Ldun;->a(Ldtc;Ldte;)V

    .line 443
    return-void
.end method

.method public final a(Lorg/json/JSONArray;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1229
    invoke-virtual {p0}, Ldun;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->e:Ldww;

    if-ne v0, v1, :cond_1

    .line 1349
    :cond_0
    :goto_0
    return-void

    .line 1233
    :cond_1
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1237
    invoke-virtual {p1, v9}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1240
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-le v0, v8, :cond_2

    .line 1241
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 1245
    :goto_1
    invoke-static {v1}, Ldtc;->a(Ljava/lang/String;)Ldtc;

    move-result-object v2

    .line 1246
    if-nez v2, :cond_3

    .line 1247
    iget-object v0, p0, Ldun;->e:Ldyg;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid method: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Ignoring."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1243
    :cond_2
    sget-object v0, Ldun;->i:Lorg/json/JSONObject;

    goto :goto_1

    .line 1251
    :cond_3
    iget-object v1, p0, Ldun;->e:Ldyg;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xc

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Received "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ldyg;->a(Ljava/lang/String;)V

    .line 1253
    sget-object v1, Lduq;->a:[I

    invoke-virtual {v2}, Ldtc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 1255
    :pswitch_0
    invoke-direct {p0, v0}, Ldun;->b(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v1

    .line 1256
    sget-object v0, Ldsz;->a:Ldsz;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Ldun;->F:Ljava/util/Set;

    .line 1257
    sget-object v0, Ldsz;->b:Ldsz;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1258
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1259
    iget-object v1, p0, Ldun;->d:Lduy;

    sget-object v2, Ldww;->b:Ldww;

    invoke-virtual {v1, v2}, Lduy;->a(Ldww;)V

    .line 1260
    invoke-direct {p0}, Ldun;->A()V

    .line 1261
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsx;

    .line 1262
    iget-boolean v1, v0, Ldsx;->c:Z

    iput-boolean v1, p0, Ldun;->h:Z

    .line 1263
    iget-object v1, p0, Ldun;->w:Ljava/util/Map;

    iget-object v2, p0, Ldun;->C:Ldst;

    iget-object v2, v2, Ldst;->b:Ldth;

    iget-object v0, v0, Ldsx;->b:Ldss;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1265
    :cond_4
    iget-boolean v0, p0, Ldun;->B:Z

    if-nez v0, :cond_0

    .line 1266
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v1, p0, Ldun;->f:Ldwr;

    invoke-virtual {v0, v1}, Lduy;->a(Ldwr;)V

    .line 1267
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldwh;->g:Ldwh;

    invoke-virtual {v0, v1}, Lduy;->a(Ldwh;)V

    goto/16 :goto_0

    .line 1272
    :pswitch_1
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldww;->b:Ldww;

    invoke-virtual {v0, v1}, Lduy;->a(Ldww;)V

    .line 1273
    invoke-direct {p0}, Ldun;->A()V

    goto/16 :goto_0

    .line 1276
    :pswitch_2
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v1, p0, Ldun;->f:Ldwr;

    invoke-virtual {v0, v1}, Lduy;->a(Ldwr;)V

    .line 1278
    invoke-virtual {p0, v9}, Ldun;->a(Z)V

    goto/16 :goto_0

    .line 1281
    :pswitch_3
    sget-object v1, Ldsz;->a:Ldsz;

    invoke-direct {p0, v0, v1}, Ldun;->a(Lorg/json/JSONObject;Ldsz;)Ldsx;

    move-result-object v0

    .line 1282
    if-eqz v0, :cond_0

    .line 1283
    iget-object v1, p0, Ldun;->F:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1287
    :pswitch_4
    sget-object v1, Ldsz;->a:Ldsz;

    invoke-direct {p0, v0, v1}, Ldun;->a(Lorg/json/JSONObject;Ldsz;)Ldsx;

    move-result-object v0

    .line 1288
    if-eqz v0, :cond_0

    .line 1289
    iget-object v1, p0, Ldun;->F:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1294
    :pswitch_5
    iget-object v1, p0, Ldun;->d:Lduy;

    invoke-virtual {v1}, Lduy;->b()V

    .line 1295
    invoke-direct {p0, v0}, Ldun;->a(Lorg/json/JSONObject;)Ldwj;

    move-result-object v1

    .line 1296
    invoke-virtual {v1}, Ldwj;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1297
    iget-object v2, p0, Ldun;->d:Lduy;

    invoke-virtual {v2, v1}, Lduy;->a(Ldwj;)V

    .line 1298
    sget-object v1, Ldwj;->f:Ldwj;

    iget-object v2, p0, Ldun;->H:Ldwj;

    invoke-virtual {v1, v2}, Ldwj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Ldun;->d:Lduy;

    iget-object v1, v1, Lduy;->i:Ldwj;

    iget-object v2, p0, Ldun;->H:Ldwj;

    iget-object v2, v2, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ldwj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Ldun;->H:Ldwj;

    iget-object v1, v1, Ldwj;->c:Lgpa;

    if-eqz v1, :cond_5

    iget-object v1, p0, Ldun;->H:Ldwj;

    iget-object v1, v1, Ldwj;->c:Lgpa;

    invoke-virtual {p0, v1}, Ldun;->b(Lgpa;)V

    :cond_5
    sget-object v1, Ldwj;->f:Ldwj;

    iput-object v1, p0, Ldun;->H:Ldwj;

    .line 1299
    :cond_6
    invoke-direct {p0, v0}, Ldun;->c(Lorg/json/JSONObject;)V

    .line 1300
    invoke-direct {p0, v0}, Ldun;->d(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 1303
    :cond_7
    sget-object v0, Ldwj;->f:Ldwj;

    iput-object v0, p0, Ldun;->H:Ldwj;

    .line 1304
    invoke-direct {p0}, Ldun;->z()V

    .line 1305
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1308
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v1, p0, Ldun;->d:Lduy;

    iget-object v1, v1, Lduy;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lduy;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1313
    :pswitch_6
    iget-object v1, p0, Ldun;->H:Ldwj;

    invoke-virtual {v1}, Ldwj;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1316
    invoke-direct {p0, v0}, Ldun;->c(Lorg/json/JSONObject;)V

    .line 1319
    invoke-direct {p0, v0}, Ldun;->d(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 1322
    :pswitch_7
    invoke-direct {p0, v0}, Ldun;->a(Lorg/json/JSONObject;)Ldwj;

    move-result-object v1

    const-string v2, "firstVideoId"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Ldun;->d:Lduy;

    const-string v3, "firstVideoId"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lduy;->b(Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->i:Ldwj;

    iget-object v2, v1, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ldwj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Ldun;->e:Ldyg;

    const-string v2, "CloudRemoteControl: playlist modified with new pending videoId: %s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Ldwj;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v0, v2, v3}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1}, Ldwj;->a()Z

    move-result v0

    if-nez v0, :cond_a

    sget-object v0, Ldwj;->f:Ldwj;

    iput-object v0, p0, Ldun;->H:Ldwj;

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v1}, Ldwj;->a()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Ldun;->d:Lduy;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lduy;->b(Ljava/lang/String;)V

    iget-object v0, p0, Ldun;->d:Lduy;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lduy;->a(Ljava/lang/String;)V

    sget-object v0, Ldwj;->f:Ldwj;

    iput-object v0, p0, Ldun;->H:Ldwj;

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Ldun;->H:Ldwj;

    iget-object v2, v1, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ldwj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object v1, p0, Ldun;->H:Ldwj;

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v1, v1, Ldwj;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lduy;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1325
    :pswitch_8
    const-string v1, "videoId"

    iget-object v2, p0, Ldun;->d:Lduy;

    iget-object v2, v2, Lduy;->i:Ldwj;

    iget-object v2, v2, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "format"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v8}, La;->b(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "languageCode"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "languageName"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "trackName"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0, v1, v2}, Lgpa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lgpa;

    move-result-object v0

    iget-object v1, p0, Ldun;->d:Lduy;

    iget-object v2, v1, Lduy;->i:Ldwj;

    iget-object v2, v2, Ldwj;->c:Lgpa;

    invoke-static {v2, v0}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ldwk;

    iget-object v3, v1, Lduy;->i:Ldwj;

    invoke-direct {v2, v3}, Ldwk;-><init>(Ldwj;)V

    iput-object v0, v2, Ldwk;->c:Lgpa;

    invoke-virtual {v2}, Ldwk;->a()Ldwj;

    move-result-object v2

    iput-object v2, v1, Lduy;->i:Ldwj;

    iget-object v1, v1, Lduy;->a:Levn;

    new-instance v2, Ldwy;

    invoke-direct {v2, v0}, Ldwy;-><init>(Lgpa;)V

    invoke-virtual {v1, v2}, Levn;->d(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1328
    :pswitch_9
    const-string v1, "errors"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "errors"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldwh;->i:Ldwh;

    invoke-virtual {v0, v1}, Lduy;->a(Ldwh;)V

    goto/16 :goto_0

    .line 1331
    :pswitch_a
    const-string v1, "volume"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Ldun;->d:Lduy;

    iput v0, v1, Lduy;->r:I

    iget-object v1, v1, Lduy;->a:Levn;

    new-instance v2, Ldxd;

    invoke-direct {v2, v0}, Ldxd;-><init>(I)V

    invoke-virtual {v1, v2}, Levn;->d(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1334
    :pswitch_b
    iget-boolean v1, p0, Ldun;->A:Z

    if-eqz v1, :cond_0

    .line 1335
    :try_start_0
    new-instance v2, Lfpc;

    invoke-direct {v2}, Lfpc;-><init>()V

    const-string v1, "adVideoId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "adVideoId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lfpc;->i:Ljava/lang/String;

    new-instance v3, Lhgy;

    invoke-direct {v3}, Lhgy;-><init>()V

    const-string v1, "http://www.youtube.com/watch?v="

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v1, "adVideoId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_e

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    iput-object v1, v3, Lhgy;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lfpc;->a(Lhgy;)Lfpc;

    :goto_3
    const-string v1, "contentVideoId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lfpc;->c:Ljava/lang/String;

    const-string v1, "isSkippable"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "isSkippable"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    sget-object v1, Ldun;->j:Landroid/net/Uri;

    invoke-virtual {v2, v1}, Lfpc;->f(Landroid/net/Uri;)Lfpc;

    :cond_c
    const-string v1, "duration"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v2, Lfpc;->q:I

    const-string v1, "clickThroughUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v1, "clickThroughUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v2, Lfpc;->v:Landroid/net/Uri;

    :cond_d
    iget-object v1, p0, Ldun;->o:Lezj;

    invoke-virtual {v1}, Lezj;->a()J

    move-result-wide v4

    sget-wide v6, Ldun;->k:J

    add-long/2addr v4, v6

    iput-wide v4, v2, Lfpc;->R:J

    iget-object v1, p0, Ldun;->d:Lduy;

    invoke-virtual {v2}, Lfpc;->a()Lfoy;

    move-result-object v2

    iput-object v2, v1, Lduy;->j:Lfoy;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1336
    :goto_4
    invoke-direct {p0, v0}, Ldun;->e(Lorg/json/JSONObject;)V

    .line 1337
    invoke-direct {p0, v0}, Ldun;->f(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 1335
    :cond_e
    :try_start_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    iget-object v2, p0, Ldun;->e:Ldyg;

    const-string v3, "Error receiving adPlaying message"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v1, v4, v9

    invoke-virtual {v2, v3, v4}, Ldyg;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Ldun;->d:Lduy;

    invoke-virtual {v1}, Lduy;->c()V

    goto :goto_4

    :cond_f
    :try_start_2
    new-instance v3, Lhgy;

    invoke-direct {v3}, Lhgy;-><init>()V

    const-string v1, "adVideoUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, "adVideoUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    iput-object v1, v3, Lhgy;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lfpc;->a(Lhgy;)Lfpc;

    goto/16 :goto_3

    :cond_10
    const-string v1, "http://"
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5

    .line 1341
    :pswitch_c
    iget-boolean v1, p0, Ldun;->A:Z

    if-eqz v1, :cond_0

    .line 1342
    invoke-direct {p0, v0}, Ldun;->e(Lorg/json/JSONObject;)V

    .line 1343
    invoke-direct {p0, v0}, Ldun;->f(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 1253
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 317
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->d:Ldww;

    sget-object v1, Ldww;->e:Ldww;

    if-ne v0, v1, :cond_0

    .line 328
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Ldun;->d:Lduy;

    sget-object v1, Ldww;->d:Ldww;

    invoke-virtual {v0, v1}, Lduy;->a(Ldww;)V

    .line 323
    iget-object v0, p0, Ldun;->g:Landroid/os/Handler;

    const/4 v1, 0x4

    new-instance v2, Ldut;

    iget-object v3, p0, Ldun;->f:Ldwr;

    invoke-direct {v2, v3, p1}, Ldut;-><init>(Ldwr;Z)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 325
    iget-object v1, p0, Ldun;->g:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 326
    iget-object v1, p0, Ldun;->g:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 327
    iget-object v1, p0, Ldun;->g:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final a(Ldwr;)Z
    .locals 2

    .prologue
    .line 593
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 594
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->d:Ldww;

    .line 595
    sget-object v1, Ldww;->b:Ldww;

    if-eq v0, v1, :cond_0

    sget-object v1, Ldww;->a:Ldww;

    if-ne v0, v1, :cond_1

    .line 596
    :cond_0
    iget-object v0, p0, Ldun;->f:Ldwr;

    invoke-virtual {p1, v0}, Ldwr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 371
    sget-object v0, Ldtc;->k:Ldtc;

    sget-object v1, Ldte;->a:Ldte;

    invoke-direct {p0, v0, v1}, Ldun;->a(Ldtc;Ldte;)V

    .line 372
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 417
    new-instance v0, Ldte;

    invoke-direct {v0}, Ldte;-><init>()V

    .line 418
    const-string v1, "volume"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 419
    sget-object v1, Ldtc;->w:Ldtc;

    invoke-direct {p0, v1, v0}, Ldun;->a(Ldtc;Ldte;)V

    .line 420
    return-void
.end method

.method b(Lgpa;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1139
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->i:Ldwj;

    invoke-virtual {v0}, Ldwj;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1140
    iget-object v0, p0, Ldun;->e:Ldyg;

    const-string v1, "Can not send subtitle track, no confirmed video"

    invoke-virtual {v0, v1}, Ldyg;->b(Ljava/lang/String;)V

    .line 1174
    :goto_0
    return-void

    .line 1144
    :cond_0
    iget-object v0, p1, Lgpa;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1146
    new-instance v0, Ldte;

    invoke-direct {v0}, Ldte;-><init>()V

    .line 1147
    const-string v1, "videoId"

    iget-object v2, p0, Ldun;->d:Lduy;

    iget-object v2, v2, Lduy;->i:Ldwj;

    iget-object v2, v2, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1148
    sget-object v1, Ldtc;->v:Ldtc;

    invoke-direct {p0, v1, v0}, Ldun;->a(Ldtc;Ldte;)V

    goto :goto_0

    .line 1152
    :cond_1
    new-instance v1, Ldte;

    invoke-direct {v1}, Ldte;-><init>()V

    .line 1153
    const-string v0, "format"

    iget v2, p1, Lgpa;->f:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1154
    const-string v0, "kind"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1155
    const-string v0, "languageCode"

    iget-object v2, p1, Lgpa;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1156
    const-string v0, "languageName"

    iget-object v2, p1, Lgpa;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1157
    const-string v0, "sourceLanguageCode"

    iget-object v2, p1, Lgpa;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1158
    const-string v0, "trackName"

    iget-object v2, p1, Lgpa;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1159
    const-string v0, "videoId"

    iget-object v2, p1, Lgpa;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1162
    new-instance v2, Ldes;

    iget-object v0, p0, Ldun;->b:Landroid/content/Context;

    iget-object v3, p0, Ldun;->x:Landroid/content/SharedPreferences;

    sget-object v4, Ldes;->a:Ldfe;

    invoke-direct {v2, v0, v3, v9, v4}, Ldes;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;ZLdfe;)V

    .line 1167
    invoke-virtual {v2}, Ldes;->b()F

    move-result v0

    .line 1168
    new-instance v3, Lorg/json/JSONObject;

    .line 1169
    invoke-virtual {v2}, Ldes;->c()Lgpo;

    move-result-object v4

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const-string v6, "background"

    iget v7, v4, Lgpo;->a:I

    invoke-static {v7}, Lgpo;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "backgroundOpacity"

    iget v7, v4, Lgpo;->a:I

    invoke-static {v7}, Lgpo;->b(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "color"

    iget v7, v4, Lgpo;->e:I

    invoke-static {v7}, Lgpo;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "textOpacity"

    iget v7, v4, Lgpo;->e:I

    invoke-static {v7}, Lgpo;->b(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "fontSizeRelative"

    const-string v7, "%.2f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "windowColor"

    iget v6, v4, Lgpo;->b:I

    invoke-static {v6}, Lgpo;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "windowOpacity"

    iget v6, v4, Lgpo;->b:I

    invoke-static {v6}, Lgpo;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, v4, Lgpo;->d:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "none"

    :goto_1
    const-string v6, "charEdgeStyle"

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, ""

    iget v4, v4, Lgpo;->f:I

    packed-switch v4, :pswitch_data_1

    :goto_2
    const-string v4, "fontFamilyOption"

    invoke-virtual {v5, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {v3, v5}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 1170
    const-string v0, "style"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 1171
    invoke-virtual {v2}, Ldes;->a()V

    .line 1173
    sget-object v0, Ldtc;->v:Ldtc;

    invoke-direct {p0, v0, v1}, Ldun;->a(Ldtc;Ldte;)V

    goto/16 :goto_0

    .line 1169
    :pswitch_0
    const-string v0, "uniform"

    goto :goto_1

    :pswitch_1
    const-string v0, "dropShadow"

    goto :goto_1

    :pswitch_2
    const-string v0, "depressed"

    goto :goto_1

    :pswitch_3
    const-string v0, "raised"

    goto :goto_1

    :pswitch_4
    const-string v0, "monoSerif"

    goto :goto_2

    :pswitch_5
    const-string v0, "propSerif"

    goto :goto_2

    :pswitch_6
    const-string v0, "monoSans"

    goto :goto_2

    :pswitch_7
    const-string v0, "propSans"

    goto :goto_2

    :pswitch_8
    const-string v0, "casual"

    goto :goto_2

    :pswitch_9
    const-string v0, "cursive"

    goto :goto_2

    :pswitch_a
    const-string v0, "smallCaps"

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 447
    new-instance v0, Ldte;

    invoke-direct {v0}, Ldte;-><init>()V

    .line 448
    const-string v1, "listId"

    invoke-virtual {v0, v1, p1}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 449
    sget-object v1, Ldtc;->b:Ldtc;

    invoke-direct {p0, v1, v0}, Ldun;->a(Ldtc;Ldte;)V

    .line 450
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 469
    sget-object v0, Ldwj;->f:Ldwj;

    iput-object v0, p0, Ldun;->H:Ldwj;

    .line 470
    invoke-direct {p0}, Ldun;->z()V

    .line 471
    sget-object v0, Ldtc;->A:Ldtc;

    sget-object v1, Ldte;->a:Ldte;

    invoke-direct {p0, v0, v1}, Ldun;->a(Ldtc;Ldte;)V

    .line 472
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 454
    new-instance v0, Ldte;

    invoke-direct {v0}, Ldte;-><init>()V

    .line 455
    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Ldte;->a(Ljava/lang/String;Ljava/lang/String;)Ldte;

    .line 456
    sget-object v1, Ldtc;->q:Ldtc;

    invoke-direct {p0, v1, v0}, Ldun;->a(Ldtc;Ldte;)V

    .line 457
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x1

    return v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 385
    sget-object v0, Ldtc;->n:Ldtc;

    new-instance v1, Ldte;

    invoke-direct {v1}, Ldte;-><init>()V

    invoke-direct {p0, v0, v1}, Ldun;->a(Ldtc;Ldte;)V

    .line 386
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x1

    return v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 398
    sget-object v0, Ldtc;->g:Ldtc;

    new-instance v1, Ldte;

    invoke-direct {v1}, Ldte;-><init>()V

    invoke-direct {p0, v0, v1}, Ldun;->a(Ldtc;Ldte;)V

    .line 399
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ldww;
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->d:Ldww;

    return-object v0
.end method

.method public final j()Ldwh;
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->e:Ldwh;

    return-object v0
.end method

.method public final k()Ldxb;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->f:Ldxb;

    return-object v0
.end method

.method public final l()Ldwo;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->g:Ldwo;

    return-object v0
.end method

.method public final m()Ldwo;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->h:Ldwo;

    return-object v0
.end method

.method public final n()Ldwr;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Ldun;->f:Ldwr;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Ldun;->H:Ldwj;

    sget-object v1, Ldwj;->f:Ldwj;

    invoke-virtual {v0, v1}, Ldwj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldun;->H:Ldwj;

    .line 497
    iget-object v0, v0, Ldwj;->a:Ljava/lang/String;

    .line 498
    :goto_0
    return-object v0

    .line 497
    :cond_0
    iget-object v0, p0, Ldun;->d:Lduy;

    .line 498
    iget-object v0, v0, Lduy;->i:Ldwj;

    iget-object v0, v0, Ldwj;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onMdxUserAuthenticationChangedEvent(Ldyc;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 564
    invoke-direct {p0}, Ldun;->A()V

    .line 565
    return-void
.end method

.method public final p()I
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Ldun;->H:Ldwj;

    sget-object v1, Ldwj;->f:Ldwj;

    invoke-virtual {v0, v1}, Ldwj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldun;->H:Ldwj;

    .line 505
    iget v0, v0, Ldwj;->e:I

    .line 506
    :goto_0
    return v0

    .line 505
    :cond_0
    iget-object v0, p0, Ldun;->d:Lduy;

    .line 506
    iget-object v0, v0, Lduy;->i:Ldwj;

    iget v0, v0, Ldwj;->e:I

    goto :goto_0
.end method

.method public final q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Ldun;->H:Ldwj;

    sget-object v1, Ldwj;->f:Ldwj;

    invoke-virtual {v0, v1}, Ldwj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 512
    iget-object v0, p0, Ldun;->H:Ldwj;

    iget-object v0, v0, Ldwj;->d:Ljava/lang/String;

    .line 517
    :goto_0
    return-object v0

    .line 514
    :cond_0
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->i:Ldwj;

    sget-object v1, Ldwj;->f:Ldwj;

    invoke-virtual {v0, v1}, Ldwj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 515
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->i:Ldwj;

    iget-object v0, v0, Ldwj;->d:Ljava/lang/String;

    goto :goto_0

    .line 517
    :cond_1
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method public final r()J
    .locals 6

    .prologue
    .line 527
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-wide v2, v0, Lduy;->n:J

    iget-object v1, v0, Lduy;->f:Ldxb;

    invoke-virtual {v1}, Ldxb;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lduy;->g:Ldwo;

    sget-object v4, Ldwo;->c:Ldwo;

    if-ne v1, v4, :cond_0

    iget-object v1, v0, Lduy;->c:Lezj;

    invoke-virtual {v1}, Lezj;->b()J

    move-result-wide v4

    iget-wide v0, v0, Lduy;->o:J

    sub-long v0, v4, v0

    :goto_0
    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final s()J
    .locals 6

    .prologue
    .line 532
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-wide v2, v0, Lduy;->p:J

    iget-object v1, v0, Lduy;->f:Ldxb;

    invoke-virtual {v1}, Ldxb;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lduy;->h:Ldwo;

    sget-object v4, Ldwo;->c:Ldwo;

    if-ne v1, v4, :cond_0

    iget-object v1, v0, Lduy;->c:Lezj;

    invoke-virtual {v1}, Lezj;->b()J

    move-result-wide v4

    iget-wide v0, v0, Lduy;->q:J

    sub-long v0, v4, v0

    :goto_0
    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final t()Lfoy;
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Ldun;->d:Lduy;

    iget-object v0, v0, Lduy;->j:Lfoy;

    return-object v0
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 542
    sget-object v0, Ldtc;->y:Ldtc;

    sget-object v1, Ldte;->a:Ldte;

    invoke-direct {p0, v0, v1}, Ldun;->a(Ldtc;Ldte;)V

    .line 543
    return-void
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 547
    iget-boolean v0, p0, Ldun;->h:Z

    return v0
.end method

.method public final w()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 602
    invoke-virtual {p0}, Ldun;->k()Ldxb;

    move-result-object v0

    invoke-virtual {v0}, Ldxb;->a()Z

    move-result v0

    return v0
.end method

.method public final x()Ljava/lang/String;
    .locals 3

    .prologue
    .line 569
    iget-object v0, p0, Ldun;->f:Ldwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldun;->f:Ldwr;

    invoke-virtual {v0}, Ldwr;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p0, Ldun;->f:Ldwr;

    check-cast v0, Ldwd;

    .line 573
    iget-object v1, p0, Ldun;->w:Ljava/util/Map;

    .line 574
    iget-object v2, v0, Ldwd;->a:Ldst;

    iget-object v2, v2, Ldst;->b:Ldth;

    .line 573
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldss;

    .line 575
    iget-object v0, v0, Ldwd;->a:Ldst;

    iget-object v0, v0, Ldst;->d:Ldss;

    .line 576
    if-eqz v1, :cond_0

    .line 577
    invoke-virtual {v1}, Ldss;->toString()Ljava/lang/String;

    move-result-object v0

    .line 582
    :goto_0
    return-object v0

    .line 578
    :cond_0
    if-eqz v0, :cond_1

    .line 579
    invoke-virtual {v0}, Ldss;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 582
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Z
    .locals 4

    .prologue
    .line 332
    iget-object v0, p0, Ldun;->e:Ldyg;

    iget-object v1, p0, Ldun;->F:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Connected remotes are "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Ldun;->F:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Ldun;->F:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsx;

    .line 335
    iget-object v2, v0, Ldsx;->a:Ljava/lang/String;

    iget-object v3, p0, Ldun;->G:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, v0, Ldsx;->a:Ljava/lang/String;

    sget-object v2, Ldun;->l:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    const/4 v0, 0x0

    .line 340
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
