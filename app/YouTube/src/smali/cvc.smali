.class public final Lcvc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgod;


# instance fields
.field private final a:Lezj;

.field private final b:Lezf;

.field private final c:Lexd;

.field private final d:Lfaw;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lezj;Lezf;Lexd;Lfaw;)V
    .locals 3

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "a."

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcvc;->e:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcvc;->a:Lezj;

    .line 37
    iput-object p3, p0, Lcvc;->b:Lezf;

    .line 38
    iput-object p4, p0, Lcvc;->c:Lexd;

    .line 39
    iput-object p5, p0, Lcvc;->d:Lfaw;

    .line 40
    return-void

    .line 35
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    sparse-switch p2, :sswitch_data_0

    .line 61
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 46
    :sswitch_0
    iget-object v0, p0, Lcvc;->c:Lexd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvc;->c:Lexd;

    invoke-interface {v0}, Lexd;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, "0"

    goto :goto_0

    .line 48
    :sswitch_1
    iget-object v0, p0, Lcvc;->b:Lezf;

    if-nez v0, :cond_1

    .line 49
    const-string v0, "userPresenceTracker is not supported and should not expect receiving LACT macro"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 50
    const-string v0, "-1"

    goto :goto_0

    .line 52
    :cond_1
    iget-object v0, p0, Lcvc;->b:Lezf;

    invoke-virtual {v0}, Lezf;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    :sswitch_2
    iget-object v0, p0, Lcvc;->e:Ljava/lang/String;

    goto :goto_0

    .line 56
    :sswitch_3
    iget-object v0, p0, Lcvc;->d:Lfaw;

    if-eqz v0, :cond_2

    const/high16 v0, 0x42c80000    # 100.0f

    iget-object v1, p0, Lcvc;->d:Lfaw;

    .line 57
    invoke-virtual {v1}, Lfaw;->a()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, "0"

    goto :goto_0

    .line 59
    :sswitch_4
    iget-object v0, p0, Lcvc;->a:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 44
    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0x19 -> :sswitch_1
        0x1f -> :sswitch_2
        0x21 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method
