.class public final Ldvb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldwv;


# instance fields
.field final a:Ljava/util/concurrent/CopyOnWriteArrayList;

.field final b:Ljava/util/concurrent/CopyOnWriteArrayList;

.field final c:Leuc;

.field final d:Landroid/content/Context;

.field final e:Ldwq;

.field final f:Ljava/util/Map;

.field final g:Ljava/util/Map;

.field final h:Ljava/util/Map;

.field final i:Ljava/util/concurrent/CopyOnWriteArrayList;

.field final j:Landroid/os/Handler;

.field final k:Lexd;

.field final l:Ldyg;

.field m:Z

.field private final n:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final o:Ljava/util/concurrent/Executor;

.field private final p:Landroid/content/SharedPreferences;

.field private final q:Ldxf;

.field private final r:Ldxv;

.field private final s:Z

.field private final t:Ljava/util/Set;

.field private final u:Levn;

.field private v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Levn;Landroid/content/SharedPreferences;Ldxf;Lexd;Ldwq;ZLdyg;)V
    .locals 7

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxf;

    iput-object v0, p0, Ldvb;->q:Ldxf;

    .line 124
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Ldvb;->k:Lexd;

    .line 125
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Ldvb;->e:Ldwq;

    .line 126
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldvb;->l:Ldyg;

    .line 127
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldvb;->u:Levn;

    .line 128
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Ldvb;->p:Landroid/content/SharedPreferences;

    .line 129
    iput-boolean p8, p0, Ldvb;->s:Z

    .line 130
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iput-object p1, p0, Ldvb;->d:Landroid/content/Context;

    .line 132
    iput-object p2, p0, Ldvb;->o:Ljava/util/concurrent/Executor;

    .line 134
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 135
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ldvb;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 136
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ldvb;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 138
    new-instance v0, Ldvn;

    invoke-direct {v0, p0}, Ldvn;-><init>(Ldvb;)V

    iput-object v0, p0, Ldvb;->c:Leuc;

    .line 139
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ldvb;->f:Ljava/util/Map;

    .line 140
    new-instance v0, Ldxn;

    invoke-direct {v0, p8}, Ldxn;-><init>(Z)V

    iput-object v0, p0, Ldvb;->r:Ldxv;

    .line 141
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ldvb;->g:Ljava/util/Map;

    .line 142
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ldvb;->h:Ljava/util/Map;

    .line 143
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ldvb;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 145
    new-instance v0, Ldvc;

    invoke-direct {v0, p0}, Ldvc;-><init>(Ldvb;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 152
    new-instance v0, Ldxi;

    invoke-direct {v0}, Ldxi;-><init>()V

    .line 153
    new-instance v1, Ldvd;

    invoke-direct {v1, p0, v0}, Ldvd;-><init>(Ldvb;Ldxi;)V

    .line 154
    invoke-static {p2, v1}, Lgjx;->a(Ljava/util/concurrent/Executor;Lgku;)Lgjx;

    move-result-object v6

    .line 161
    new-instance v0, Ldve;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p5

    move-object/from16 v4, p9

    move-object v5, p7

    invoke-direct/range {v0 .. v6}, Ldve;-><init>(Ldvb;Lexd;Ldxf;Ldyg;Ldwq;Lgjx;)V

    iput-object v0, p0, Ldvb;->j:Landroid/os/Handler;

    .line 233
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldvb;->t:Ljava/util/Set;

    .line 234
    return-void
.end method

.method private a(Ldtj;)Ldwf;
    .locals 3

    .prologue
    .line 454
    iget-object v0, p0, Ldvb;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    .line 455
    check-cast v0, Ldwf;

    .line 456
    iget-object v2, v0, Ldwf;->a:Ldtm;

    iget-object v2, v2, Ldtm;->e:Ldtj;

    invoke-virtual {v2, p1}, Ldtj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 460
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ldvb;Ldtj;)Ldwf;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Ldvb;->a(Ldtj;)Ldwf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ldvb;Ldwd;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Ldvb;->a(Ldwd;I)V

    return-void
.end method

.method static synthetic a(Ldvb;Ldwf;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Ldvb;->a(Ldwf;I)V

    return-void
.end method

.method private a(Ldwd;I)V
    .locals 6

    .prologue
    .line 487
    iget-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    :goto_0
    return-void

    .line 490
    :cond_0
    iget-object v0, p0, Ldvb;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    check-cast v0, Ldwd;

    iget-object v2, v0, Ldwd;->a:Ldst;

    iget-object v2, v2, Ldst;->b:Ldth;

    iget-object v3, p1, Ldwd;->a:Ldst;

    iget-object v3, v3, Ldst;->b:Ldth;

    invoke-virtual {v2, v3}, Ldth;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldvb;->l:Ldyg;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1a

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Removing duplicate screen "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldyg;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ldvb;->b(Ldwr;)V

    goto :goto_1

    .line 491
    :cond_2
    iget-object v0, p0, Ldvb;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 492
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 493
    iget-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    :goto_2
    invoke-direct {p0, p1}, Ldvb;->c(Ldwr;)V

    goto :goto_0

    .line 495
    :cond_3
    iget-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method private a(Ldwf;I)V
    .locals 5

    .prologue
    .line 443
    iget-object v0, p1, Ldwf;->a:Ldtm;

    iget-object v0, v0, Ldtm;->e:Ldtj;

    invoke-direct {p0, v0}, Ldvb;->a(Ldtj;)Ldwf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Ldvb;->l:Ldyg;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Removing duplicate device "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldyg;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ldvb;->a(Ldwf;)V

    .line 444
    :cond_0
    iget-object v0, p0, Ldvb;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 446
    iget-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    :goto_0
    invoke-direct {p0, p1}, Ldvb;->c(Ldwr;)V

    .line 451
    return-void

    .line 448
    :cond_1
    iget-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private c(Ldwr;)V
    .locals 3

    .prologue
    .line 290
    iget-object v0, p0, Ldvb;->u:Levn;

    new-instance v1, Ldwt;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Ldwt;-><init>(ZLdwr;)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 291
    return-void
.end method


# virtual methods
.method declared-synchronized a(Ldtm;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 652
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ldvb;->d()V

    .line 653
    iget-object v0, p0, Ldvb;->g:Ljava/util/Map;

    iget-object v1, p1, Ldtm;->e:Ldtj;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Ldvb;->g:Ljava/util/Map;

    iget-object v1, p1, Ldtm;->e:Ldtj;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvj;

    iget-object v0, v0, Ldvj;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 656
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p1, Ldtm;->c:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 652
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method public final a(Ldtd;Leub;)V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Ldvb;->q:Ldxf;

    new-instance v1, Ldvh;

    invoke-direct {v1, p0, p2}, Ldvh;-><init>(Ldvb;Leub;)V

    invoke-interface {v0, p1, v1}, Ldxf;->a(Ldtd;Leuc;)V

    .line 313
    return-void
.end method

.method public final a(Ldtj;Ldvl;)V
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Ldvb;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    move-object v1, v0

    .line 249
    check-cast v1, Ldwf;

    .line 250
    iget-object v1, v1, Ldwf;->a:Ldtm;

    iget-object v1, v1, Ldtm;->e:Ldtj;

    invoke-virtual {v1, p1}, Ldtj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 251
    invoke-interface {p2, v0}, Ldvl;->a(Ldwr;)V

    .line 287
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Ldvb;->f:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    iget-object v0, p0, Ldvb;->r:Ldxv;

    new-instance v1, Ldvf;

    invoke-direct {v1, p0, p1, p2}, Ldvf;-><init>(Ldvb;Ldtj;Ldvl;)V

    invoke-interface {v0, v1}, Ldxv;->a(Ldxw;)V

    .line 284
    iget-object v0, p0, Ldvb;->j:Landroid/os/Handler;

    const/4 v1, 0x3

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 286
    iget-object v1, p0, Ldvb;->j:Landroid/os/Handler;

    const-wide/16 v2, 0x251c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method a(Ldwf;)V
    .locals 4

    .prologue
    .line 475
    iget-object v0, p0, Ldvb;->l:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Removing dial device "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 476
    iget-object v0, p0, Ldvb;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 477
    iget-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 478
    iget-object v0, p0, Ldvb;->h:Ljava/util/Map;

    iget-object v1, p1, Ldwf;->a:Ldtm;

    iget-object v1, v1, Ldtm;->e:Ldtj;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    invoke-virtual {p0, p1}, Ldvb;->a(Ldwr;)V

    .line 480
    return-void
.end method

.method a(Ldwr;)V
    .locals 3

    .prologue
    .line 294
    iget-object v0, p0, Ldvb;->u:Levn;

    new-instance v1, Ldwt;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Ldwt;-><init>(ZLdwr;)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 295
    return-void
.end method

.method public final a(Ldwr;Leuc;)V
    .locals 4

    .prologue
    .line 386
    invoke-virtual {p1}, Ldwr;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387
    iget-object v0, p0, Ldvb;->e:Ldwq;

    invoke-interface {v0, p1}, Ldwq;->a(Ldwr;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldvb;->l:Ldyg;

    const-string v1, "Disconnecting screen before removing it"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldvb;->e:Ldwq;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ldwq;->a(Z)V

    .line 388
    :cond_0
    invoke-virtual {p1}, Ldwr;->e()Ldwd;

    move-result-object v0

    .line 389
    invoke-virtual {p0, v0}, Ldvb;->b(Ldwr;)V

    .line 390
    iget-object v1, p0, Ldvb;->q:Ldxf;

    iget-object v2, v0, Ldwd;->a:Ldst;

    iget-object v2, v2, Ldst;->b:Ldth;

    new-instance v3, Ldvo;

    invoke-direct {v3, v0, p2}, Ldvo;-><init>(Ldwd;Leuc;)V

    invoke-interface {v1, v2, v3}, Ldxf;->a(Ldth;Leuc;)V

    .line 393
    :cond_1
    return-void
.end method

.method public final a(Ldwr;Ljava/lang/String;Leuc;)V
    .locals 4

    .prologue
    .line 366
    iget-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 367
    invoke-virtual {p1}, Ldwr;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 368
    invoke-virtual {p1}, Ldwr;->f()Ldwf;

    move-result-object v1

    .line 369
    iget-object v1, v1, Ldwf;->a:Ldtm;

    invoke-virtual {v1, p2}, Ldtm;->a(Ljava/lang/String;)Ldtm;

    move-result-object v1

    .line 370
    new-instance v2, Ldwf;

    invoke-direct {v2, v1}, Ldwf;-><init>(Ldtm;)V

    .line 371
    invoke-direct {p0, v2, v0}, Ldvb;->a(Ldwf;I)V

    .line 372
    iget-object v0, p0, Ldvb;->o:Ljava/util/concurrent/Executor;

    new-instance v1, Ldvm;

    invoke-direct {v1, p0, p1, v2, p3}, Ldvm;-><init>(Ldvb;Ldwr;Ldwr;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    invoke-virtual {p1}, Ldwr;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    invoke-virtual {p1}, Ldwr;->e()Ldwd;

    move-result-object v1

    .line 375
    iget-object v2, v1, Ldwd;->a:Ldst;

    invoke-virtual {v2, p2}, Ldst;->a(Ljava/lang/String;)Ldst;

    move-result-object v2

    .line 376
    new-instance v3, Ldwd;

    invoke-direct {v3, v2}, Ldwd;-><init>(Ldst;)V

    .line 377
    invoke-direct {p0, v3, v0}, Ldvb;->a(Ldwd;I)V

    .line 378
    iget-object v0, p0, Ldvb;->q:Ldxf;

    iget-object v2, v3, Ldwd;->a:Ldst;

    iget-object v2, v2, Ldst;->b:Ldth;

    new-instance v3, Ldvo;

    invoke-direct {v3, v1, p3}, Ldvo;-><init>(Ldwd;Leuc;)V

    invoke-interface {v0, v2, p2, v3}, Ldxf;->a(Ldth;Ljava/lang/String;Leuc;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 341
    iget-object v0, p0, Ldvb;->t:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 342
    iget-object v0, p0, Ldvb;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    iput-boolean v1, p0, Ldvb;->m:Z

    .line 344
    iget-object v0, p0, Ldvb;->j:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 345
    iget-object v0, p0, Ldvb;->j:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 347
    :cond_0
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Ldvb;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method b(Ldwr;)V
    .locals 4

    .prologue
    .line 514
    iget-object v0, p0, Ldvb;->l:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Removing cloud screen "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Ldvb;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 516
    iget-object v0, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 517
    invoke-virtual {p0, p1}, Ldvb;->a(Ldwr;)V

    .line 518
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 351
    iget-object v0, p0, Ldvb;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    iput-boolean v4, p0, Ldvb;->m:Z

    .line 353
    iget-object v0, p0, Ldvb;->q:Ldxf;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ldxf;->c(Leuc;)V

    iget-object v0, p0, Ldvb;->q:Ldxf;

    iget-object v1, p0, Ldvb;->c:Leuc;

    invoke-interface {v0, v1}, Ldxf;->a(Leuc;)V

    invoke-virtual {p0}, Ldvb;->c()V

    .line 354
    iput-boolean v4, p0, Ldvb;->m:Z

    iget-object v0, p0, Ldvb;->j:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Ldvb;->j:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 356
    :cond_0
    iget-object v0, p0, Ldvb;->t:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 357
    return-void
.end method

.method c()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 409
    iget-object v0, p0, Ldvb;->j:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 411
    iget-object v0, p0, Ldvb;->k:Lexd;

    invoke-interface {v0}, Lexd;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldvb;->k:Lexd;

    invoke-interface {v0}, Lexd;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Ldvb;->k:Lexd;

    .line 412
    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 413
    :cond_1
    iget-object v0, p0, Ldvb;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 414
    iget-object v0, p0, Ldvb;->l:Ldyg;

    const-string v1, "Not connected to wifi/eth or network not available. Will mark all devices as unavailable."

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 417
    :cond_2
    iget-object v0, p0, Ldvb;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    iget-object v2, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Ldvb;->a(Ldwr;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ldvb;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 430
    :goto_1
    return-void

    .line 421
    :cond_4
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 422
    invoke-static {v0}, La;->e(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 423
    iget-object v1, p0, Ldvb;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 425
    iget-object v1, p0, Ldvb;->j:Landroid/os/Handler;

    iget-object v2, p0, Ldvb;->j:Landroid/os/Handler;

    .line 426
    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x251c

    .line 425
    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 428
    iget-object v1, p0, Ldvb;->l:Ldyg;

    const-string v2, "Starting new DIAL search."

    invoke-virtual {v1, v2}, Ldyg;->a(Ljava/lang/String;)V

    .line 429
    iget-object v1, p0, Ldvb;->r:Ldxv;

    new-instance v2, Ldvp;

    invoke-direct {v2, p0, v0}, Ldvp;-><init>(Ldvb;Ljava/util/Set;)V

    invoke-interface {v1, v2}, Ldxv;->a(Ldxw;)V

    goto :goto_1
.end method

.method declared-synchronized d()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 588
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ldvb;->s:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldvb;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 618
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 592
    :cond_1
    :try_start_1
    iget-object v0, p0, Ldvb;->p:Landroid/content/SharedPreferences;

    const-string v2, "youtube.mdx:dial_devices"

    const-string v3, "[]"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 594
    :try_start_2
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 595
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 596
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 597
    if-eqz v0, :cond_3

    const-string v4, "id"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 598
    new-instance v4, Ldtj;

    const-string v5, "id"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ldtj;-><init>(Ljava/lang/String;)V

    .line 599
    const-string v5, "name"

    const-string v6, "unnamed"

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 600
    const-string v6, "ssid"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 601
    const-string v7, "mac"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 602
    const-string v8, "timeout"

    const/4 v9, -0x1

    invoke-virtual {v0, v8, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 603
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-gtz v8, :cond_2

    move-object v0, v1

    .line 607
    :cond_2
    new-instance v8, Ldvj;

    invoke-direct {v8, v5, v7, v0, v6}, Ldvj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 609
    iget-object v0, p0, Ldvb;->g:Ljava/util/Map;

    invoke-interface {v0, v4, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    iget-object v0, p0, Ldvb;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 595
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 613
    :catch_0
    move-exception v0

    .line 614
    :try_start_3
    const-string v1, "Error loading dial devices from pref"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 617
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldvb;->v:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 588
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized e()V
    .locals 6

    .prologue
    .line 621
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ldvb;->s:Z

    if-nez v0, :cond_0

    .line 623
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    .line 624
    iget-object v1, p0, Ldvb;->p:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "youtube.mdx:dial_devices"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 649
    :goto_0
    monitor-exit p0

    return-void

    .line 627
    :cond_0
    :goto_1
    :try_start_1
    iget-object v0, p0, Ldvb;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_1

    .line 628
    iget-object v0, p0, Ldvb;->g:Ljava/util/Map;

    iget-object v1, p0, Ldvb;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 621
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 631
    :cond_1
    :try_start_2
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 632
    iget-object v0, p0, Ldvb;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldtj;

    .line 633
    iget-object v1, p0, Ldvb;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldvj;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 634
    if-eqz v1, :cond_2

    .line 636
    :try_start_3
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "id"

    .line 637
    invoke-virtual {v4, v5, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v4, "name"

    .line 638
    iget-object v5, v1, Ldvj;->a:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v4, "ssid"

    .line 639
    iget-object v5, v1, Ldvj;->d:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v4, "mac"

    .line 640
    iget-object v5, v1, Ldvj;->b:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v4, "timeout"

    .line 641
    iget-object v1, v1, Ldvj;->c:Ljava/lang/Integer;

    invoke-virtual {v0, v4, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    .line 636
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 642
    :catch_0
    move-exception v0

    .line 643
    :try_start_4
    const-string v1, "Error saving dial devices to pref"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 648
    :cond_3
    iget-object v0, p0, Ldvb;->p:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "youtube.mdx:dial_devices"

    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public final onMdxScreenDisconnecting(Ldwu;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 538
    iget-object v0, p1, Ldwu;->a:Ldwr;

    .line 539
    iget-object v1, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 541
    invoke-virtual {v0}, Ldwr;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 542
    iget-object v1, p0, Ldvb;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 543
    iget-object v1, p0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 544
    invoke-virtual {p0, v0}, Ldvb;->a(Ldwr;)V

    .line 547
    :cond_0
    return-void
.end method
