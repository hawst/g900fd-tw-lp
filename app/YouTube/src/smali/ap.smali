.class final Lap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lck;


# instance fields
.field final a:I

.field b:Lan;

.field c:Lcj;

.field d:Z

.field e:Z

.field f:Ljava/lang/Object;

.field g:Z

.field h:Z

.field i:Z

.field j:Z

.field k:Lap;

.field private l:Landroid/os/Bundle;

.field private m:Z

.field private n:Z

.field private synthetic o:Lao;


# direct methods
.method public constructor <init>(Lao;ILandroid/os/Bundle;Lan;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lap;->o:Lao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    iput p2, p0, Lap;->a:I

    .line 230
    iput-object p3, p0, Lap;->l:Landroid/os/Bundle;

    .line 231
    iput-object p4, p0, Lap;->b:Lan;

    .line 232
    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 235
    iget-boolean v0, p0, Lap;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lap;->i:Z

    if-eqz v0, :cond_1

    .line 239
    iput-boolean v3, p0, Lap;->g:Z

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-boolean v0, p0, Lap;->g:Z

    if-nez v0, :cond_0

    .line 248
    iput-boolean v3, p0, Lap;->g:Z

    .line 250
    iget-object v0, p0, Lap;->c:Lcj;

    if-nez v0, :cond_2

    iget-object v0, p0, Lap;->b:Lan;

    if-eqz v0, :cond_2

    .line 252
    iget-object v0, p0, Lap;->b:Lan;

    iget v1, p0, Lap;->a:I

    iget-object v2, p0, Lap;->l:Landroid/os/Bundle;

    invoke-interface {v0, v1}, Lan;->b_(I)Lcj;

    move-result-object v0

    iput-object v0, p0, Lap;->c:Lcj;

    .line 254
    :cond_2
    iget-object v0, p0, Lap;->c:Lcj;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lap;->c:Lcj;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lap;->c:Lcj;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 257
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Object returned from onCreateLoader must not be a non-static inner member class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lap;->c:Lcj;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_3
    iget-boolean v0, p0, Lap;->n:Z

    if-nez v0, :cond_5

    .line 262
    iget-object v0, p0, Lap;->c:Lcj;

    iget v1, p0, Lap;->a:I

    iget-object v2, v0, Lcj;->b:Lck;

    if-eqz v2, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is already a listener registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iput-object p0, v0, Lcj;->b:Lck;

    iput v1, v0, Lcj;->a:I

    .line 263
    iput-boolean v3, p0, Lap;->n:Z

    .line 265
    :cond_5
    iget-object v0, p0, Lap;->c:Lcj;

    invoke-virtual {v0}, Lcj;->a()V

    goto :goto_0
.end method

.method public final a(Lcj;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 362
    iget-boolean v0, p0, Lap;->m:Z

    if-eqz v0, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->b:Ldy;

    iget v1, p0, Lap;->a:I

    invoke-virtual {v0, v1}, Ldy;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 376
    iget-object v0, p0, Lap;->k:Lap;

    .line 377
    if-eqz v0, :cond_2

    .line 381
    iput-object v3, p0, Lap;->k:Lap;

    .line 383
    iget-object v1, p0, Lap;->o:Lao;

    iget-object v1, v1, Lao;->b:Ldy;

    iget v2, p0, Lap;->a:I

    invoke-virtual {v1, v2, v3}, Ldy;->a(ILjava/lang/Object;)V

    .line 384
    invoke-virtual {p0}, Lap;->c()V

    .line 385
    iget-object v1, p0, Lap;->o:Lao;

    invoke-virtual {v1, v0}, Lao;->a(Lap;)V

    goto :goto_0

    .line 391
    :cond_2
    iget-object v0, p0, Lap;->f:Ljava/lang/Object;

    if-ne v0, p2, :cond_3

    iget-boolean v0, p0, Lap;->d:Z

    if-nez v0, :cond_4

    .line 392
    :cond_3
    iput-object p2, p0, Lap;->f:Ljava/lang/Object;

    .line 393
    iput-boolean v4, p0, Lap;->d:Z

    .line 394
    iget-boolean v0, p0, Lap;->g:Z

    if-eqz v0, :cond_4

    .line 395
    invoke-virtual {p0, p1, p2}, Lap;->b(Lcj;Ljava/lang/Object;)V

    .line 405
    :cond_4
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->c:Ldy;

    iget v1, p0, Lap;->a:I

    invoke-virtual {v0, v1}, Ldy;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lap;

    .line 406
    if-eqz v0, :cond_5

    if-eq v0, p0, :cond_5

    .line 407
    const/4 v1, 0x0

    iput-boolean v1, v0, Lap;->e:Z

    .line 408
    invoke-virtual {v0}, Lap;->c()V

    .line 409
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->c:Ldy;

    iget v1, p0, Lap;->a:I

    iget-object v2, v0, Ldy;->c:[I

    iget v3, v0, Ldy;->e:I

    invoke-static {v2, v3, v1}, Ldo;->a([III)I

    move-result v1

    if-ltz v1, :cond_5

    iget-object v2, v0, Ldy;->d:[Ljava/lang/Object;

    aget-object v2, v2, v1

    sget-object v3, Ldy;->a:Ljava/lang/Object;

    if-eq v2, v3, :cond_5

    iget-object v2, v0, Ldy;->d:[Ljava/lang/Object;

    sget-object v3, Ldy;->a:Ljava/lang/Object;

    aput-object v3, v2, v1

    iput-boolean v4, v0, Ldy;->b:Z

    .line 412
    :cond_5
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->e:Lo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lap;->o:Lao;

    invoke-virtual {v0}, Lao;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 413
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->e:Lo;

    iget-object v0, v0, Lo;->b:Lv;

    invoke-virtual {v0}, Lv;->d()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 451
    :goto_0
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mId="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lap;->a:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 452
    const-string v0, " mArgs="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lap;->l:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 453
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCallbacks="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lap;->b:Lan;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 454
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mLoader="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lap;->c:Lcj;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 455
    iget-object v0, p0, Lap;->c:Lcj;

    if-eqz v0, :cond_2

    .line 456
    iget-object v0, p0, Lap;->c:Lcj;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mId="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v2, v0, Lcj;->a:I

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v2, " mListener="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, v0, Lcj;->b:Lck;

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-boolean v2, v0, Lcj;->c:Z

    if-eqz v2, :cond_0

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mStarted="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v2, v0, Lcj;->c:Z

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Z)V

    const-string v2, " mContentChanged="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Z)V

    const-string v2, " mProcessingChange="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Z)V

    :cond_0
    iget-boolean v2, v0, Lcj;->d:Z

    if-nez v2, :cond_1

    iget-boolean v2, v0, Lcj;->e:Z

    if-eqz v2, :cond_2

    :cond_1
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "mAbandoned="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v1, v0, Lcj;->d:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, " mReset="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, v0, Lcj;->e:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 458
    :cond_2
    iget-boolean v0, p0, Lap;->d:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lap;->e:Z

    if-eqz v0, :cond_4

    .line 459
    :cond_3
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHaveData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lap;->d:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 460
    const-string v0, "  mDeliveredData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lap;->e:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 461
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lap;->f:Ljava/lang/Object;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 463
    :cond_4
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lap;->g:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 464
    const-string v0, " mReportNextStart="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lap;->j:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 465
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lap;->m:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 466
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lap;->h:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 467
    const-string v0, " mRetainingStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lap;->i:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 468
    const-string v0, " mListenerRegistered="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lap;->n:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 469
    iget-object v0, p0, Lap;->k:Lap;

    if-eqz v0, :cond_5

    .line 470
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Loader "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lap;->k:Lap;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 472
    iget-object p0, p0, Lap;->k:Lap;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 474
    :cond_5
    return-void
.end method

.method final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 314
    iput-boolean v1, p0, Lap;->g:Z

    .line 316
    iget-boolean v0, p0, Lap;->h:Z

    if-nez v0, :cond_0

    .line 317
    iget-object v0, p0, Lap;->c:Lcj;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lap;->n:Z

    if-eqz v0, :cond_0

    .line 319
    iput-boolean v1, p0, Lap;->n:Z

    .line 320
    iget-object v0, p0, Lap;->c:Lcj;

    invoke-virtual {v0, p0}, Lcj;->a(Lck;)V

    .line 321
    iget-object v0, p0, Lap;->c:Lcj;

    invoke-virtual {v0}, Lcj;->c()V

    .line 324
    :cond_0
    return-void
.end method

.method final b(Lcj;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 418
    iget-object v0, p0, Lap;->b:Lan;

    if-eqz v0, :cond_1

    .line 419
    const/4 v0, 0x0

    .line 420
    iget-object v1, p0, Lap;->o:Lao;

    iget-object v1, v1, Lao;->e:Lo;

    if-eqz v1, :cond_3

    .line 421
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->e:Lo;

    iget-object v0, v0, Lo;->b:Lv;

    iget-object v0, v0, Lv;->k:Ljava/lang/String;

    .line 422
    iget-object v1, p0, Lap;->o:Lao;

    iget-object v1, v1, Lao;->e:Lo;

    iget-object v1, v1, Lo;->b:Lv;

    const-string v2, "onLoadFinished"

    iput-object v2, v1, Lv;->k:Ljava/lang/String;

    move-object v1, v0

    .line 425
    :goto_0
    :try_start_0
    iget-object v0, p0, Lap;->b:Lan;

    invoke-interface {v0, p1, p2}, Lan;->a(Lcj;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 429
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->e:Lo;

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->e:Lo;

    iget-object v0, v0, Lo;->b:Lv;

    iput-object v1, v0, Lv;->k:Ljava/lang/String;

    .line 433
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lap;->e:Z

    .line 435
    :cond_1
    return-void

    .line 429
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lap;->o:Lao;

    iget-object v2, v2, Lao;->e:Lo;

    if-eqz v2, :cond_2

    .line 430
    iget-object v2, p0, Lap;->o:Lao;

    iget-object v2, v2, Lao;->e:Lo;

    iget-object v2, v2, Lo;->b:Lv;

    iput-object v1, v2, Lv;->k:Ljava/lang/String;

    :cond_2
    throw v0

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method final c()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 327
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lap;->m:Z

    .line 329
    iget-boolean v0, p0, Lap;->e:Z

    .line 330
    iput-boolean v4, p0, Lap;->e:Z

    .line 331
    iget-object v1, p0, Lap;->b:Lan;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lap;->c:Lcj;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lap;->d:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->e:Lo;

    if-eqz v0, :cond_5

    .line 335
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->e:Lo;

    iget-object v0, v0, Lo;->b:Lv;

    iget-object v0, v0, Lv;->k:Ljava/lang/String;

    .line 336
    iget-object v1, p0, Lap;->o:Lao;

    iget-object v1, v1, Lao;->e:Lo;

    iget-object v1, v1, Lo;->b:Lv;

    const-string v3, "onLoaderReset"

    iput-object v3, v1, Lv;->k:Ljava/lang/String;

    move-object v1, v0

    .line 339
    :goto_1
    :try_start_0
    iget-object v0, p0, Lap;->b:Lan;

    iget-object v3, p0, Lap;->c:Lcj;

    invoke-interface {v0, v3}, Lan;->a(Lcj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->e:Lo;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lap;->o:Lao;

    iget-object v0, v0, Lao;->e:Lo;

    iget-object v0, v0, Lo;->b:Lv;

    iput-object v1, v0, Lv;->k:Ljava/lang/String;

    .line 346
    :cond_0
    iput-object v2, p0, Lap;->b:Lan;

    .line 347
    iput-object v2, p0, Lap;->f:Ljava/lang/Object;

    .line 348
    iput-boolean v4, p0, Lap;->d:Z

    .line 349
    iget-object v0, p0, Lap;->c:Lcj;

    if-eqz v0, :cond_2

    .line 350
    iget-boolean v0, p0, Lap;->n:Z

    if-eqz v0, :cond_1

    .line 351
    iput-boolean v4, p0, Lap;->n:Z

    .line 352
    iget-object v0, p0, Lap;->c:Lcj;

    invoke-virtual {v0, p0}, Lcj;->a(Lck;)V

    .line 354
    :cond_1
    iget-object v0, p0, Lap;->c:Lcj;

    invoke-virtual {v0}, Lcj;->e()V

    .line 356
    :cond_2
    iget-object v0, p0, Lap;->k:Lap;

    if-eqz v0, :cond_4

    .line 357
    iget-object p0, p0, Lap;->k:Lap;

    goto :goto_0

    .line 341
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lap;->o:Lao;

    iget-object v2, v2, Lao;->e:Lo;

    if-eqz v2, :cond_3

    .line 342
    iget-object v2, p0, Lap;->o:Lao;

    iget-object v2, v2, Lao;->e:Lo;

    iget-object v2, v2, Lo;->b:Lv;

    iput-object v1, v2, Lv;->k:Ljava/lang/String;

    :cond_3
    throw v0

    .line 359
    :cond_4
    return-void

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 439
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 440
    const-string v1, "LoaderInfo{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    iget v1, p0, Lap;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 444
    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    iget-object v1, p0, Lap;->c:Lcj;

    invoke-static {v1, v0}, La;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 446
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
