.class public Lfjb;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhdt;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field public f:Lfnc;

.field public g:Lfkp;

.field private final h:Lfqh;

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhdt;Lfqh;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lfic;-><init>()V

    .line 39
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdt;

    iput-object v0, p0, Lfjb;->a:Lhdt;

    .line 40
    iput-object p2, p0, Lfjb;->h:Lfqh;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 151
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 152
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lfjb;->a:Lhdt;

    iget-object v0, v0, Lhdt;->h:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lfjb;->i:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lfjb;->a:Lhdt;

    iget-object v0, v0, Lhdt;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfjb;->i:Ljava/lang/CharSequence;

    .line 51
    :cond_0
    iget-object v0, p0, Lfjb;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lfjb;->a:Lhdt;

    iget-object v0, v0, Lhdt;->j:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lfjb;->h:Lfqh;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 112
    iget-object v0, p0, Lfjb;->j:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 113
    iget-object v0, p0, Lfjb;->a:Lhdt;

    iget-object v1, v0, Lhdt;->i:[Lhbi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 114
    iget-object v4, v3, Lhbi;->b:Licu;

    if-eqz v4, :cond_0

    .line 115
    iget-object v3, v3, Lhbi;->b:Licu;

    iget-object v3, v3, Licu;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, p0, Lfjb;->j:Ljava/lang/CharSequence;

    .line 113
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_1
    iget-object v0, p0, Lfjb;->j:Ljava/lang/CharSequence;

    return-object v0
.end method
