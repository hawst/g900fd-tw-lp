.class public final Lza;
.super Likv;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, "mdhd"

    invoke-direct {p0, v0}, Likv;-><init>(Ljava/lang/String;)V

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lza;->c(Ljava/nio/ByteBuffer;)J

    .line 99
    iget v0, p0, Likv;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 100
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lza;->a:J

    .line 101
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lza;->b:J

    .line 102
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lza;->c:J

    .line 103
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lza;->d:J

    .line 110
    :goto_0
    invoke-static {p1}, La;->i(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lza;->e:Ljava/lang/String;

    .line 111
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    .line 112
    return-void

    .line 105
    :cond_0
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lza;->a:J

    .line 106
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lza;->b:J

    .line 107
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lza;->c:J

    .line 108
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lza;->d:J

    goto :goto_0
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lza;->d(Ljava/nio/ByteBuffer;)V

    .line 133
    iget v0, p0, Likv;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 134
    iget-wide v0, p0, Lza;->a:J

    invoke-static {p1, v0, v1}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 135
    iget-wide v0, p0, Lza;->b:J

    invoke-static {p1, v0, v1}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 136
    iget-wide v0, p0, Lza;->c:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 137
    iget-wide v0, p0, Lza;->d:J

    invoke-static {p1, v0, v1}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 144
    :goto_0
    iget-object v0, p0, Lza;->e:Ljava/lang/String;

    invoke-static {p1, v0}, Lyf;->a(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    .line 145
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 146
    return-void

    .line 139
    :cond_0
    iget-wide v0, p0, Lza;->a:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 140
    iget-wide v0, p0, Lza;->b:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 141
    iget-wide v0, p0, Lza;->c:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 142
    iget-wide v0, p0, Lza;->d:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    goto :goto_0
.end method

.method protected final d_()J
    .locals 4

    .prologue
    const-wide/16 v2, 0x2

    .line 64
    iget v0, p0, Likv;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 66
    const-wide/16 v0, 0x20

    .line 70
    :goto_0
    add-long/2addr v0, v2

    .line 71
    add-long/2addr v0, v2

    .line 72
    return-wide v0

    .line 68
    :cond_0
    const-wide/16 v0, 0x14

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    const-string v1, "MediaHeaderBox["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string v1, "creationTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lza;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 119
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    const-string v1, "modificationTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lza;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 121
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string v1, "timescale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lza;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 123
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const-string v1, "duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lza;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 125
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    const-string v1, "language="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lza;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
