.class public final Lfbh;
.super Lftx;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Set;

.field b:Lfsi;

.field c:Landroid/widget/ListView;

.field d:Lfjp;

.field e:Landroid/graphics/Rect;

.field f:I

.field g:Z

.field h:Lfjo;

.field i:Lfbn;

.field j:Landroid/view/View;

.field public k:Lfcz;

.field private n:Landroid/widget/EditText;

.field private o:Lfvi;

.field private p:I


# direct methods
.method public constructor <init>(Lfcz;Lfbn;Landroid/view/View;Levn;Leyt;Leyp;Lfut;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 118
    invoke-direct {p0, p1, p4, v3, p5}, Lftx;-><init>(Lfdg;Levn;Ljava/lang/Object;Leyt;)V

    .line 120
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfbh;->a:Ljava/util/Set;

    .line 122
    const v0, 0x7f080146

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lfbh;->c:Landroid/widget/ListView;

    .line 123
    const v0, 0x7f080155

    .line 124
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 125
    new-instance v1, Lfvi;

    invoke-direct {v1, p6, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lfbh;->o:Lfvi;

    .line 127
    const v0, 0x7f080157

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lfbh;->n:Landroid/widget/EditText;

    .line 128
    const v0, 0x7f080147

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfbh;->j:Landroid/view/View;

    .line 129
    invoke-virtual {p0, v3}, Lfbh;->a(Lfjo;)V

    .line 131
    iput-object p1, p0, Lfbh;->k:Lfcz;

    .line 132
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbn;

    iput-object v0, p0, Lfbh;->i:Lfbn;

    .line 134
    new-instance v0, Lfsi;

    invoke-direct {v0}, Lfsi;-><init>()V

    iput-object v0, p0, Lfbh;->b:Lfsi;

    .line 135
    const-class v0, Lfjp;

    iget-object v1, p0, Lfbh;->b:Lfsi;

    invoke-interface {p7, v0, v1}, Lfut;->a(Ljava/lang/Class;Lfsi;)V

    .line 136
    iget-object v0, p0, Lfbh;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lfbh;->b:Lfsi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 138
    iget-object v0, p0, Lfbh;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setStackFromBottom(Z)V

    .line 140
    iget-object v0, p0, Lfbh;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setTranscriptMode(I)V

    .line 141
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfbh;->e:Landroid/graphics/Rect;

    .line 142
    iget-object v0, p0, Lfbh;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 143
    new-instance v1, Lfbi;

    invoke-direct {v1, p0}, Lfbi;-><init>(Lfbh;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 150
    iget-object v0, p0, Lfbh;->c:Landroid/widget/ListView;

    new-instance v1, Lfbj;

    invoke-direct {v1, p0}, Lfbj;-><init>(Lfbh;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 166
    iget-object v0, p0, Lfbh;->c:Landroid/widget/ListView;

    new-instance v1, Lfbk;

    invoke-direct {v1, p0}, Lfbk;-><init>(Lfbh;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 188
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 215
    invoke-virtual {p0}, Lfbh;->e()V

    .line 216
    invoke-virtual {p0}, Lfbh;->f()V

    .line 217
    return-void
.end method

.method public final a(Lfjk;)V
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x1

    iput v0, p0, Lfbh;->p:I

    .line 230
    invoke-super {p0, p1}, Lftx;->a(Lfjk;)V

    .line 231
    return-void
.end method

.method a(Lfjo;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 369
    if-eqz p1, :cond_3

    .line 370
    iget-object v0, p0, Lfbh;->o:Lfvi;

    iget-object v1, p1, Lfjo;->b:Lfnc;

    if-nez v1, :cond_0

    iget-object v1, p1, Lfjo;->a:Lheu;

    iget-object v1, v1, Lheu;->a:Lhxf;

    if-eqz v1, :cond_0

    new-instance v1, Lfnc;

    iget-object v2, p1, Lfjo;->a:Lheu;

    iget-object v2, v2, Lheu;->a:Lhxf;

    invoke-direct {v1, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, p1, Lfjo;->b:Lfnc;

    :cond_0
    iget-object v1, p1, Lfjo;->b:Lfnc;

    invoke-virtual {v0, v1, v3}, Lfvi;->a(Lfnc;Leyo;)V

    .line 371
    iget-object v0, p0, Lfbh;->n:Landroid/widget/EditText;

    iget-object v1, p1, Lfjo;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p1, Lfjo;->a:Lheu;

    iget-object v1, v1, Lheu;->b:Lhgz;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lfjo;->a:Lheu;

    iget-object v1, v1, Lheu;->b:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p1, Lfjo;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p1, Lfjo;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 372
    iget-object v0, p1, Lfjo;->a:Lheu;

    iget-object v0, v0, Lheu;->c:Lhut;

    if-nez v0, :cond_2

    .line 373
    iget-object v0, p0, Lfbh;->n:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 378
    :cond_2
    :goto_0
    return-void

    .line 376
    :cond_3
    iget-object v0, p0, Lfbh;->o:Lfvi;

    const-string v1, "https://yt3.ggpht.com/-cQqeVomlEvY/AAAAAAAAAAI/AAAAAAAAAAA/BmwPbUIDv14/s100-c-k-no/photo.jpg"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lfvi;->a(Landroid/net/Uri;Leyo;)V

    goto :goto_0
.end method

.method protected final a(Lhel;Lfjl;)V
    .locals 4

    .prologue
    .line 323
    invoke-super {p0, p1, p2}, Lftx;->a(Lhel;Lfjl;)V

    .line 324
    if-eqz p1, :cond_0

    iget-object v0, p1, Lhel;->g:Lhew;

    if-nez v0, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    new-instance v1, Lfjp;

    iget-object v0, p1, Lhel;->g:Lhew;

    invoke-direct {v1, v0}, Lfjp;-><init>(Lhew;)V

    .line 329
    invoke-virtual {v1}, Lfjp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbt;

    iget-object v3, p0, Lfbh;->b:Lfsi;

    invoke-virtual {v3, v0}, Lfsi;->b(Ljava/lang/Object;)V

    goto :goto_1

    .line 330
    :cond_2
    invoke-virtual {v1}, Lfjp;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfbh;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method a(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 220
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjk;

    .line 221
    sget-object v1, Lfjl;->c:Lfjl;

    iget-object v3, v0, Lfjk;->c:Lfjl;

    if-ne v3, v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 222
    invoke-virtual {p0, v0, v0}, Lftx;->a(Ljava/lang/Object;Lfjk;)V

    goto :goto_0

    .line 221
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 225
    :cond_2
    return-void
.end method

.method protected final a(Lxa;Lfjl;)V
    .locals 1

    .prologue
    .line 235
    invoke-super {p0, p1, p2}, Lftx;->a(Lxa;Lfjl;)V

    .line 236
    iget v0, p0, Lfbh;->p:I

    if-lez v0, :cond_0

    .line 239
    invoke-virtual {p0}, Lfbh;->g()V

    .line 240
    iget v0, p0, Lfbh;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lfbh;->p:I

    .line 242
    :cond_0
    return-void
.end method
