.class public final Lhcd;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhcf;

.field public c:Lhcf;

.field public d:Lhce;

.field private e:Lhgz;

.field private f:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20743
    invoke-direct {p0}, Lidf;-><init>()V

    .line 20746
    iput-object v0, p0, Lhcd;->a:Lhgz;

    .line 20749
    iput-object v0, p0, Lhcd;->b:Lhcf;

    .line 20752
    iput-object v0, p0, Lhcd;->c:Lhcf;

    .line 20755
    iput-object v0, p0, Lhcd;->e:Lhgz;

    .line 20758
    iput-object v0, p0, Lhcd;->d:Lhce;

    .line 20761
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhcd;->f:[B

    .line 20743
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 20833
    const/4 v0, 0x0

    .line 20834
    iget-object v1, p0, Lhcd;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 20835
    const/4 v0, 0x1

    iget-object v1, p0, Lhcd;->a:Lhgz;

    .line 20836
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20838
    :cond_0
    iget-object v1, p0, Lhcd;->b:Lhcf;

    if-eqz v1, :cond_1

    .line 20839
    const/4 v1, 0x2

    iget-object v2, p0, Lhcd;->b:Lhcf;

    .line 20840
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20842
    :cond_1
    iget-object v1, p0, Lhcd;->c:Lhcf;

    if-eqz v1, :cond_2

    .line 20843
    const/4 v1, 0x3

    iget-object v2, p0, Lhcd;->c:Lhcf;

    .line 20844
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20846
    :cond_2
    iget-object v1, p0, Lhcd;->e:Lhgz;

    if-eqz v1, :cond_3

    .line 20847
    const/4 v1, 0x4

    iget-object v2, p0, Lhcd;->e:Lhgz;

    .line 20848
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20850
    :cond_3
    iget-object v1, p0, Lhcd;->d:Lhce;

    if-eqz v1, :cond_4

    .line 20851
    const/4 v1, 0x5

    iget-object v2, p0, Lhcd;->d:Lhce;

    .line 20852
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20854
    :cond_4
    iget-object v1, p0, Lhcd;->f:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 20855
    const/4 v1, 0x7

    iget-object v2, p0, Lhcd;->f:[B

    .line 20856
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 20858
    :cond_5
    iget-object v1, p0, Lhcd;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20859
    iput v0, p0, Lhcd;->J:I

    .line 20860
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 20739
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhcd;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhcd;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhcd;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhcd;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhcd;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhcd;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhcd;->b:Lhcf;

    if-nez v0, :cond_3

    new-instance v0, Lhcf;

    invoke-direct {v0}, Lhcf;-><init>()V

    iput-object v0, p0, Lhcd;->b:Lhcf;

    :cond_3
    iget-object v0, p0, Lhcd;->b:Lhcf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhcd;->c:Lhcf;

    if-nez v0, :cond_4

    new-instance v0, Lhcf;

    invoke-direct {v0}, Lhcf;-><init>()V

    iput-object v0, p0, Lhcd;->c:Lhcf;

    :cond_4
    iget-object v0, p0, Lhcd;->c:Lhcf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhcd;->e:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhcd;->e:Lhgz;

    :cond_5
    iget-object v0, p0, Lhcd;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhcd;->d:Lhce;

    if-nez v0, :cond_6

    new-instance v0, Lhce;

    invoke-direct {v0}, Lhce;-><init>()V

    iput-object v0, p0, Lhcd;->d:Lhce;

    :cond_6
    iget-object v0, p0, Lhcd;->d:Lhce;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhcd;->f:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 20809
    iget-object v0, p0, Lhcd;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 20810
    const/4 v0, 0x1

    iget-object v1, p0, Lhcd;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 20812
    :cond_0
    iget-object v0, p0, Lhcd;->b:Lhcf;

    if-eqz v0, :cond_1

    .line 20813
    const/4 v0, 0x2

    iget-object v1, p0, Lhcd;->b:Lhcf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 20815
    :cond_1
    iget-object v0, p0, Lhcd;->c:Lhcf;

    if-eqz v0, :cond_2

    .line 20816
    const/4 v0, 0x3

    iget-object v1, p0, Lhcd;->c:Lhcf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 20818
    :cond_2
    iget-object v0, p0, Lhcd;->e:Lhgz;

    if-eqz v0, :cond_3

    .line 20819
    const/4 v0, 0x4

    iget-object v1, p0, Lhcd;->e:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 20821
    :cond_3
    iget-object v0, p0, Lhcd;->d:Lhce;

    if-eqz v0, :cond_4

    .line 20822
    const/4 v0, 0x5

    iget-object v1, p0, Lhcd;->d:Lhce;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 20824
    :cond_4
    iget-object v0, p0, Lhcd;->f:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    .line 20825
    const/4 v0, 0x7

    iget-object v1, p0, Lhcd;->f:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 20827
    :cond_5
    iget-object v0, p0, Lhcd;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 20829
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 20777
    if-ne p1, p0, :cond_1

    .line 20786
    :cond_0
    :goto_0
    return v0

    .line 20778
    :cond_1
    instance-of v2, p1, Lhcd;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 20779
    :cond_2
    check-cast p1, Lhcd;

    .line 20780
    iget-object v2, p0, Lhcd;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhcd;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhcd;->b:Lhcf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhcd;->b:Lhcf;

    if-nez v2, :cond_3

    .line 20781
    :goto_2
    iget-object v2, p0, Lhcd;->c:Lhcf;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhcd;->c:Lhcf;

    if-nez v2, :cond_3

    .line 20782
    :goto_3
    iget-object v2, p0, Lhcd;->e:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhcd;->e:Lhgz;

    if-nez v2, :cond_3

    .line 20783
    :goto_4
    iget-object v2, p0, Lhcd;->d:Lhce;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhcd;->d:Lhce;

    if-nez v2, :cond_3

    .line 20784
    :goto_5
    iget-object v2, p0, Lhcd;->f:[B

    iget-object v3, p1, Lhcd;->f:[B

    .line 20785
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhcd;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhcd;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 20786
    goto :goto_0

    .line 20780
    :cond_4
    iget-object v2, p0, Lhcd;->a:Lhgz;

    iget-object v3, p1, Lhcd;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhcd;->b:Lhcf;

    iget-object v3, p1, Lhcd;->b:Lhcf;

    .line 20781
    invoke-virtual {v2, v3}, Lhcf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhcd;->c:Lhcf;

    iget-object v3, p1, Lhcd;->c:Lhcf;

    .line 20782
    invoke-virtual {v2, v3}, Lhcf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhcd;->e:Lhgz;

    iget-object v3, p1, Lhcd;->e:Lhgz;

    .line 20783
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhcd;->d:Lhce;

    iget-object v3, p1, Lhcd;->d:Lhce;

    .line 20784
    invoke-virtual {v2, v3}, Lhce;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    .line 20785
    :cond_9
    iget-object v2, p0, Lhcd;->I:Ljava/util/List;

    iget-object v3, p1, Lhcd;->I:Ljava/util/List;

    .line 20786
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20790
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 20792
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcd;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 20793
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcd;->b:Lhcf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 20794
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcd;->c:Lhcf;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 20795
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcd;->e:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 20796
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcd;->d:Lhce;

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 20797
    iget-object v2, p0, Lhcd;->f:[B

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    .line 20803
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhcd;->I:Ljava/util/List;

    if-nez v2, :cond_7

    :goto_5
    add-int/2addr v0, v1

    .line 20804
    return v0

    .line 20792
    :cond_1
    iget-object v0, p0, Lhcd;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 20793
    :cond_2
    iget-object v0, p0, Lhcd;->b:Lhcf;

    invoke-virtual {v0}, Lhcf;->hashCode()I

    move-result v0

    goto :goto_1

    .line 20794
    :cond_3
    iget-object v0, p0, Lhcd;->c:Lhcf;

    invoke-virtual {v0}, Lhcf;->hashCode()I

    move-result v0

    goto :goto_2

    .line 20795
    :cond_4
    iget-object v0, p0, Lhcd;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    .line 20796
    :cond_5
    iget-object v0, p0, Lhcd;->d:Lhce;

    invoke-virtual {v0}, Lhce;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_6
    move v2, v0

    move v0, v1

    .line 20799
    :goto_6
    iget-object v3, p0, Lhcd;->f:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 20800
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhcd;->f:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 20799
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 20803
    :cond_7
    iget-object v1, p0, Lhcd;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
