.class public final enum Laws;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Laws;

.field private static enum b:Laws;

.field private static final synthetic c:[Laws;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Laws;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v2}, Laws;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laws;->b:Laws;

    .line 46
    new-instance v0, Laws;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v3}, Laws;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laws;->a:Laws;

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [Laws;

    sget-object v1, Laws;->b:Laws;

    aput-object v1, v0, v2

    sget-object v1, Laws;->a:Laws;

    aput-object v1, v0, v3

    sput-object v0, Laws;->c:[Laws;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laws;
    .locals 1

    .prologue
    .line 44
    const-class v0, Laws;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laws;

    return-object v0
.end method

.method public static values()[Laws;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Laws;->c:[Laws;

    invoke-virtual {v0}, [Laws;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laws;

    return-object v0
.end method
