.class public final Ldmc;
.super Ljava/util/Observable;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/ScheduledExecutorService;

.field private b:Ljava/util/concurrent/ScheduledFuture;

.field private c:Ldmf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 48
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Ldmc;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 55
    return-void
.end method

.method static synthetic a(Ldmc;Ljava/util/concurrent/ScheduledFuture;)Ljava/util/concurrent/ScheduledFuture;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Ldmc;->b:Ljava/util/concurrent/ScheduledFuture;

    return-object p1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldmc;->c:Ldmf;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Ldmc;->c:Ldmf;

    invoke-virtual {v0}, Ldmf;->a()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Ldmc;->c:Ldmf;

    .line 131
    :cond_0
    return-void
.end method

.method static synthetic a(Ldmc;)V
    .locals 0

    .prologue
    .line 30
    invoke-virtual {p0}, Ldmc;->setChanged()V

    return-void
.end method

.method static synthetic b(Ldmc;)Ljava/util/concurrent/ScheduledFuture;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ldmc;->b:Ljava/util/concurrent/ScheduledFuture;

    return-object v0
.end method

.method static synthetic c(Ldmc;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ldmc;->a:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method


# virtual methods
.method protected final handleVideoStageEvent(Ldac;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 59
    sget-object v1, Ldmd;->a:[I

    iget-object v2, p1, Ldac;->a:Lgol;

    invoke-virtual {v2}, Lgol;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 68
    invoke-direct {p0}, Ldmc;->a()V

    .line 71
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 65
    :pswitch_1
    iget-object v1, p1, Ldac;->b:Lfrl;

    invoke-direct {p0}, Ldmc;->a()V

    if-eqz v1, :cond_0

    if-nez v1, :cond_2

    :cond_1
    :goto_1
    if-eqz v0, :cond_0

    new-instance v2, Ldmf;

    invoke-virtual {v1}, Lfrl;->l()Lfrn;

    move-result-object v1

    invoke-direct {v2, p0, v0, v1}, Ldmf;-><init>(Ldmc;Liaa;Lfrn;)V

    iput-object v2, p0, Ldmc;->c:Ldmf;

    iget-object v0, p0, Ldmc;->c:Ldmf;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Ldmf;->b:J

    const/4 v1, 0x1

    iput v1, v0, Ldmf;->i:I

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lfrl;->l()Lfrn;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v0, v2, Lfrn;->a:Lhzy;

    iget-object v0, v0, Lhzy;->a:Liaa;

    goto :goto_1

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final handleYouTubePlayerStateEvent(Ldae;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Ldmc;->c:Ldmf;

    if-nez v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 78
    :cond_0
    iget v0, p1, Ldae;->a:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 80
    :pswitch_1
    iget-object v0, p0, Ldmc;->c:Ldmf;

    invoke-virtual {v0}, Ldmf;->b()V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ldmf;->a(I)V

    goto :goto_0

    .line 83
    :pswitch_2
    iget-object v0, p0, Ldmc;->c:Ldmf;

    invoke-virtual {v0}, Ldmf;->b()V

    sget-object v1, Ldmd;->b:[I

    iget v2, v0, Ldmf;->i:I

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ldmf;->a(I)V

    iget-wide v2, v0, Ldmf;->c:J

    iget-wide v4, v0, Ldmf;->f:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ldmf;->a(J)V

    goto :goto_0

    :pswitch_4
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ldmf;->a(I)V

    iget-wide v2, v0, Ldmf;->d:J

    iget-wide v4, v0, Ldmf;->h:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ldmf;->a(J)V

    goto :goto_0

    :pswitch_5
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ldmf;->a(I)V

    goto :goto_0

    .line 86
    :pswitch_6
    iget-object v0, p0, Ldmc;->c:Ldmf;

    invoke-virtual {v0}, Ldmf;->b()V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ldmf;->a(I)V

    goto :goto_0

    .line 89
    :pswitch_7
    iget-object v0, p0, Ldmc;->c:Ldmf;

    invoke-virtual {v0}, Ldmf;->b()V

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ldmf;->a(I)V

    goto :goto_0

    .line 94
    :pswitch_8
    invoke-direct {p0}, Ldmc;->a()V

    goto :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_2
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_6
    .end packed-switch

    .line 83
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method
