.class final Lgfe;
.super Lgfd;
.source "SourceFile"


# instance fields
.field final synthetic a:Lgfb;

.field private b:I


# direct methods
.method constructor <init>(Lgfb;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lgfe;->a:Lgfb;

    invoke-direct {p0, p1}, Lgfd;-><init>(Lgfb;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 394
    iget-object v0, p0, Lgfe;->a:Lgfb;

    iget-boolean v0, v0, Lgfb;->d:Z

    if-eqz v0, :cond_5

    .line 395
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 398
    :cond_0
    iget-object v0, p0, Lgfe;->a:Lgfb;

    invoke-virtual {v0}, Lgfb;->p()V

    .line 399
    invoke-super {p0, p1}, Lgfd;->handleMessage(Landroid/os/Message;)Z

    move-result v0

    .line 405
    :goto_0
    return v0

    .line 400
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    .line 401
    iget-object v0, p0, Lgfe;->a:Lgfb;

    iget-object v0, v0, Lgfb;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->i()I

    move-result v0

    iget-object v1, p0, Lgfe;->a:Lgfb;

    iget-object v1, v1, Lgfb;->b:Lgeh;

    invoke-interface {v1}, Lgeh;->i()I

    move-result v1

    sub-int/2addr v0, v1

    const/16 v2, 0x32

    if-le v0, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Foreground sync is ahead by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->d(Ljava/lang/String;)V

    iput v4, p0, Lgfe;->b:I

    iget-object v1, p0, Lgfe;->a:Lgfb;

    iget-object v1, v1, Lgfb;->a:Lgeh;

    invoke-interface {v1}, Lgeh;->f()V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lgff;

    invoke-direct {v2, p0}, Lgff;-><init>(Lgfe;)V

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 403
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 401
    :cond_3
    const/16 v2, -0x32

    if-ge v0, v2, :cond_4

    iget v2, p0, Lgfe;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lgfe;->b:I

    const/4 v3, 0x2

    if-ge v2, v3, :cond_4

    iget v0, p0, Lgfe;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x38

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Foreground sync is behind. Retry seek ahead: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lgfe;->a:Lgfb;

    iget-object v0, v0, Lgfb;->a:Lgeh;

    add-int/lit16 v1, v1, 0xfa0

    invoke-interface {v0, v1}, Lgeh;->b(I)V

    goto :goto_1

    :cond_4
    iget v1, p0, Lgfe;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x43

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Foreground synced with time diff: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", retries: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    iput v4, p0, Lgfe;->b:I

    iget-object v0, p0, Lgfe;->a:Lgfb;

    invoke-virtual {v0}, Lgfb;->p()V

    goto :goto_1

    .line 405
    :cond_5
    invoke-super {p0, p1}, Lgfd;->handleMessage(Landroid/os/Message;)Z

    move-result v0

    goto/16 :goto_0
.end method
