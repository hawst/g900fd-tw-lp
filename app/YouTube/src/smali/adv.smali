.class public final Ladv;
.super Ldee;
.source "SourceFile"

# interfaces
.implements Ladr;
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private a:Lafg;

.field private b:Lads;

.field private final c:Laet;

.field private final d:Laek;

.field private final e:Laex;

.field private final f:Landroid/widget/ProgressBar;

.field private final g:Landroid/widget/ImageButton;

.field private final h:Landroid/widget/ImageButton;

.field private final i:Landroid/widget/ImageButton;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/TextView;

.field private final l:Lddv;

.field private final m:Landroid/os/Handler;

.field private final n:Landroid/view/animation/Animation;

.field private final o:Ldel;

.field private p:Ldbu;

.field private q:Ldbs;

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Laet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 93
    invoke-direct {p0, p1}, Ldee;-><init>(Landroid/content/Context;)V

    .line 95
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laet;

    iput-object v0, p0, Ladv;->c:Laet;

    .line 97
    new-instance v0, Laek;

    new-instance v1, Lady;

    invoke-direct {v1, p0}, Lady;-><init>(Ladv;)V

    new-instance v2, Ladz;

    invoke-direct {v2, p0}, Ladz;-><init>(Ladv;)V

    invoke-direct {v0, p1, v1, v2}, Laek;-><init>(Landroid/content/Context;Laen;Lafa;)V

    iput-object v0, p0, Ladv;->d:Laek;

    .line 98
    new-instance v0, Laex;

    invoke-direct {v0, p1}, Laex;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ladv;->e:Laex;

    .line 99
    iget-object v0, p0, Ladv;->d:Laek;

    invoke-virtual {p0, v0}, Ladv;->addView(Landroid/view/View;)V

    .line 100
    iget-object v0, p0, Ladv;->e:Laex;

    invoke-virtual {p0, v0}, Ladv;->addView(Landroid/view/View;)V

    .line 102
    sget-object v0, Ldbs;->a:Ldbs;

    iput-object v0, p0, Ladv;->q:Ldbs;

    .line 104
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ladv;->f:Landroid/widget/ProgressBar;

    .line 105
    iget-object v0, p0, Ladv;->f:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 106
    iget-object v0, p0, Ladv;->f:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Ladv;->addView(Landroid/view/View;)V

    .line 108
    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    .line 109
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    const v1, 0x7f020049

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 110
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    const v1, 0x7f090134

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Ladv;->addView(Landroid/view/View;)V

    .line 114
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ladv;->j:Landroid/widget/ImageView;

    .line 115
    iget-object v0, p0, Ladv;->j:Landroid/widget/ImageView;

    const v1, 0x7f02004c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 116
    iget-object v0, p0, Ladv;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Ladv;->addView(Landroid/view/View;)V

    .line 118
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ladv;->k:Landroid/widget/TextView;

    .line 119
    iget-object v0, p0, Ladv;->k:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 120
    iget-object v0, p0, Ladv;->k:Landroid/widget/TextView;

    const v1, 0x7f020055

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 121
    iget-object v0, p0, Ladv;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 122
    iget-object v0, p0, Ladv;->k:Landroid/widget/TextView;

    iget-object v1, p0, Ladv;->d:Laek;

    iget v1, v1, Laek;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 123
    iget-object v0, p0, Ladv;->k:Landroid/widget/TextView;

    const/4 v1, -0x2

    invoke-virtual {p0, v0, v3, v1}, Ladv;->addView(Landroid/view/View;II)V

    .line 125
    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    .line 126
    iget-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    const v1, 0x7f020047

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 127
    iget-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    const v1, 0x7f090137

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Ladv;->addView(Landroid/view/View;)V

    .line 131
    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    .line 132
    iget-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    const v1, 0x7f02004a

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 133
    iget-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    const v1, 0x7f090138

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Ladv;->addView(Landroid/view/View;)V

    .line 137
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Ladv;->m:Landroid/os/Handler;

    .line 139
    new-instance v0, Lddv;

    new-instance v1, Ladx;

    invoke-direct {v1, p0}, Ladx;-><init>(Ladv;)V

    invoke-direct {v0, v1}, Lddv;-><init>(Lddw;)V

    iput-object v0, p0, Ladv;->l:Lddv;

    .line 141
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Ladv;->n:Landroid/view/animation/Animation;

    .line 142
    iget-object v0, p0, Ladv;->n:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 143
    iget-object v0, p0, Ladv;->n:Landroid/view/animation/Animation;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 145
    new-instance v0, Ldel;

    invoke-direct {v0, p1}, Ldel;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ladv;->o:Ldel;

    .line 147
    sget-object v0, Ldbu;->a:Ldbu;

    invoke-virtual {p0, v0}, Ladv;->a(Ldbu;)V

    .line 149
    invoke-virtual {p0}, Ladv;->e()V

    .line 150
    return-void
.end method

.method static synthetic a(Ladv;)Lafg;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ladv;->a:Lafg;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 338
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Ladv;->n:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 341
    :cond_0
    return-void
.end method

.method private static a(Landroid/view/View;II)V
    .locals 4

    .prologue
    .line 508
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, p2, v1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 509
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 562
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 563
    return-void

    .line 562
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic a(Ladv;Z)Z
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Ladv;->v:Z

    return p1
.end method

.method static synthetic b(Ladv;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ladv;->h()V

    return-void
.end method

.method static synthetic c(Ladv;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ladv;->g()V

    return-void
.end method

.method static synthetic d(Ladv;)Lads;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ladv;->b:Lads;

    return-object v0
.end method

.method static synthetic e(Ladv;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ladv;->i()V

    return-void
.end method

.method static synthetic f(Ladv;)Laek;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ladv;->d:Laek;

    return-object v0
.end method

.method static synthetic g(Ladv;)Laex;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ladv;->e:Laex;

    return-object v0
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 310
    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v1, Ldbs;->b:Ldbs;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v1, Ldbs;->d:Ldbs;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Ladv;->y:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Ladv;->m:Landroid/os/Handler;

    .line 311
    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 312
    iget-object v0, p0, Ladv;->m:Landroid/os/Handler;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 314
    :cond_1
    return-void
.end method

.method static synthetic h(Ladv;)Ldbs;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ladv;->q:Ldbs;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 344
    const/4 v0, 0x0

    iput-boolean v0, p0, Ladv;->z:Z

    .line 345
    iget-object v0, p0, Ladv;->m:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 346
    iget-object v0, p0, Ladv;->n:Landroid/view/animation/Animation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 347
    iget-boolean v0, p0, Ladv;->u:Z

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Ladv;->d:Laek;

    invoke-virtual {v0}, Laek;->clearAnimation()V

    .line 350
    :cond_0
    iget-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    .line 351
    iget-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    .line 352
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    .line 353
    iget-object v0, p0, Ladv;->n:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 354
    return-void
.end method

.method private i()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 516
    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v3, Ldbs;->c:Ldbs;

    if-ne v0, v3, :cond_5

    .line 517
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    const v3, 0x7f020049

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 518
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Ladv;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090134

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 528
    :goto_0
    iget-boolean v0, p0, Ladv;->v:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Ladv;->d:Laek;

    .line 529
    invoke-virtual {v0}, Laek;->getTop()I

    move-result v0

    iget-object v3, p0, Ladv;->g:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBottom()I

    move-result v3

    if-ge v0, v3, :cond_7

    move v0, v1

    .line 530
    :goto_1
    iget-boolean v3, p0, Ladv;->y:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Ladv;->q:Ldbs;

    invoke-virtual {v3}, Ldbs;->a()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Ladv;->q:Ldbs;

    sget-object v4, Ldbs;->a:Ldbs;

    if-ne v3, v4, :cond_d

    :cond_0
    move v3, v2

    .line 532
    :goto_2
    invoke-virtual {p0}, Ladv;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_9

    .line 533
    invoke-virtual {p0, v3}, Ladv;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 534
    iget-object v4, p0, Ladv;->f:Landroid/widget/ProgressBar;

    if-ne v5, v4, :cond_1

    iget-object v4, p0, Ladv;->q:Ldbs;

    sget-object v6, Ldbs;->d:Ldbs;

    if-eq v4, v6, :cond_4

    :cond_1
    iget-object v4, p0, Ladv;->j:Landroid/widget/ImageView;

    if-eq v5, v4, :cond_2

    iget-object v4, p0, Ladv;->k:Landroid/widget/TextView;

    if-ne v5, v4, :cond_3

    :cond_2
    iget-object v4, p0, Ladv;->q:Ldbs;

    .line 536
    invoke-virtual {v4}, Ldbs;->a()Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    iget-object v4, p0, Ladv;->e:Laex;

    if-ne v5, v4, :cond_8

    iget-boolean v4, p0, Ladv;->u:Z

    if-eqz v4, :cond_8

    :cond_4
    move v4, v1

    .line 534
    :goto_3
    invoke-static {v5, v4}, Ladv;->a(Landroid/view/View;Z)V

    .line 532
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 519
    :cond_5
    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v3, Ldbs;->b:Ldbs;

    if-ne v0, v3, :cond_6

    .line 520
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    const v3, 0x7f020048

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 521
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Ladv;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090135

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 523
    :cond_6
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    const v3, 0x7f02004b

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 524
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    .line 525
    invoke-virtual {p0}, Ladv;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090136

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 524
    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 529
    goto :goto_1

    :cond_8
    move v4, v2

    .line 536
    goto :goto_3

    .line 540
    :cond_9
    iget-boolean v3, p0, Ladv;->u:Z

    if-eqz v3, :cond_a

    iget-boolean v3, p0, Ladv;->x:Z

    if-eqz v3, :cond_b

    :cond_a
    iget-object v3, p0, Ladv;->q:Ldbs;

    sget-object v4, Ldbs;->d:Ldbs;

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Ladv;->q:Ldbs;

    .line 541
    invoke-virtual {v3}, Ldbs;->a()Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_b
    move v3, v1

    .line 540
    :goto_4
    invoke-static {p0, v3}, Ladv;->a(Landroid/view/View;Z)V

    .line 555
    :goto_5
    iget-boolean v3, p0, Ladv;->y:Z

    if-nez v3, :cond_14

    iget-object v3, p0, Ladv;->q:Ldbs;

    sget-object v4, Ldbs;->d:Ldbs;

    if-eq v3, v4, :cond_14

    iget-object v3, p0, Ladv;->p:Ldbu;

    iget-boolean v3, v3, Ldbu;->l:Z

    if-eqz v3, :cond_14

    if-nez v0, :cond_14

    move v0, v1

    .line 557
    :goto_6
    iget-object v4, p0, Ladv;->h:Landroid/widget/ImageButton;

    if-eqz v0, :cond_15

    iget-boolean v3, p0, Ladv;->s:Z

    if-eqz v3, :cond_15

    move v3, v1

    :goto_7
    invoke-static {v4, v3}, Ladv;->a(Landroid/view/View;Z)V

    .line 558
    iget-object v3, p0, Ladv;->i:Landroid/widget/ImageButton;

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Ladv;->t:Z

    if-eqz v0, :cond_16

    :goto_8
    invoke-static {v3, v1}, Ladv;->a(Landroid/view/View;Z)V

    .line 559
    return-void

    :cond_c
    move v3, v2

    .line 541
    goto :goto_4

    .line 543
    :cond_d
    iget-object v4, p0, Ladv;->j:Landroid/widget/ImageView;

    iget-object v3, p0, Ladv;->q:Ldbs;

    invoke-virtual {v3}, Ldbs;->a()Z

    move-result v3

    if-eqz v3, :cond_e

    if-nez v0, :cond_e

    move v3, v1

    :goto_9
    invoke-static {v4, v3}, Ladv;->a(Landroid/view/View;Z)V

    .line 544
    iget-object v4, p0, Ladv;->k:Landroid/widget/TextView;

    iget-object v3, p0, Ladv;->q:Ldbs;

    invoke-virtual {v3}, Ldbs;->a()Z

    move-result v3

    if-eqz v3, :cond_f

    if-nez v0, :cond_f

    move v3, v1

    :goto_a
    invoke-static {v4, v3}, Ladv;->a(Landroid/view/View;Z)V

    .line 545
    iget-object v4, p0, Ladv;->f:Landroid/widget/ProgressBar;

    iget-object v3, p0, Ladv;->q:Ldbs;

    sget-object v5, Ldbs;->d:Ldbs;

    if-ne v3, v5, :cond_10

    if-nez v0, :cond_10

    move v3, v1

    :goto_b
    invoke-static {v4, v3}, Ladv;->a(Landroid/view/View;Z)V

    .line 546
    iget-object v4, p0, Ladv;->g:Landroid/widget/ImageButton;

    iget-object v3, p0, Ladv;->q:Ldbs;

    invoke-virtual {v3}, Ldbs;->a()Z

    move-result v3

    if-nez v3, :cond_11

    iget-object v3, p0, Ladv;->q:Ldbs;

    sget-object v5, Ldbs;->d:Ldbs;

    if-eq v3, v5, :cond_11

    iget-object v3, p0, Ladv;->p:Ldbu;

    iget-boolean v3, v3, Ldbu;->k:Z

    if-eqz v3, :cond_11

    if-nez v0, :cond_11

    move v3, v1

    :goto_c
    invoke-static {v4, v3}, Ladv;->a(Landroid/view/View;Z)V

    .line 549
    iget-object v4, p0, Ladv;->d:Laek;

    iget-boolean v3, p0, Ladv;->u:Z

    if-nez v3, :cond_12

    move v3, v1

    :goto_d
    invoke-static {v4, v3}, Ladv;->a(Landroid/view/View;Z)V

    .line 550
    iget-object v4, p0, Ladv;->e:Laex;

    iget-object v3, p0, Ladv;->p:Ldbu;

    iget-boolean v3, v3, Ldbu;->g:Z

    if-eqz v3, :cond_13

    iget-boolean v3, p0, Ladv;->u:Z

    if-eqz v3, :cond_13

    move v3, v1

    :goto_e
    invoke-static {v4, v3}, Ladv;->a(Landroid/view/View;Z)V

    .line 552
    invoke-virtual {p0, v2}, Ladv;->setVisibility(I)V

    goto/16 :goto_5

    :cond_e
    move v3, v2

    .line 543
    goto :goto_9

    :cond_f
    move v3, v2

    .line 544
    goto :goto_a

    :cond_10
    move v3, v2

    .line 545
    goto :goto_b

    :cond_11
    move v3, v2

    .line 546
    goto :goto_c

    :cond_12
    move v3, v2

    .line 549
    goto :goto_d

    :cond_13
    move v3, v2

    .line 550
    goto :goto_e

    :cond_14
    move v0, v2

    .line 555
    goto/16 :goto_6

    :cond_15
    move v3, v2

    .line 557
    goto/16 :goto_7

    :cond_16
    move v1, v2

    .line 558
    goto/16 :goto_8
.end method

.method static synthetic i(Ladv;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Ladv;->r:Z

    return v0
.end method

.method private n(Z)V
    .locals 3

    .prologue
    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, Ladv;->z:Z

    .line 327
    iget-object v2, p0, Ladv;->n:Landroid/view/animation/Animation;

    if-eqz p1, :cond_1

    const-wide/16 v0, 0x64

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 329
    iget-boolean v0, p0, Ladv;->u:Z

    if-nez v0, :cond_0

    .line 330
    iget-object v0, p0, Ladv;->d:Laek;

    invoke-direct {p0, v0}, Ladv;->a(Landroid/view/View;)V

    .line 332
    :cond_0
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Ladv;->a(Landroid/view/View;)V

    .line 333
    iget-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Ladv;->a(Landroid/view/View;)V

    .line 334
    iget-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Ladv;->a(Landroid/view/View;)V

    .line 335
    return-void

    .line 327
    :cond_1
    const-wide/16 v0, 0x1f4

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Ladv;->d:Laek;

    invoke-virtual {v0}, Laek;->a()I

    move-result v0

    return v0
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Ladv;->d:Laek;

    invoke-virtual {v0, p1, p2, p3}, Laek;->a(III)V

    .line 256
    iget-object v0, p0, Ladv;->e:Laex;

    invoke-virtual {v0, p1, p2, p3}, Laex;->a(III)V

    .line 257
    iget-object v0, p0, Ladv;->l:Lddv;

    invoke-virtual {v0, p1, p2}, Lddv;->a(II)V

    .line 258
    return-void
.end method

.method public final a(Lads;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Ladv;->b:Lads;

    .line 166
    return-void
.end method

.method public final a(Ldbs;)V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Ladv;->q:Ldbs;

    if-eq v0, p1, :cond_0

    .line 181
    iput-object p1, p0, Ladv;->q:Ldbs;

    .line 182
    invoke-virtual {p0}, Ladv;->d()V

    .line 187
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-direct {p0}, Ladv;->g()V

    goto :goto_0
.end method

.method public final a(Ldbt;)V
    .locals 3

    .prologue
    .line 159
    new-instance v0, Lafg;

    iget-object v1, p0, Ladv;->c:Laet;

    invoke-direct {v0, p1, v1}, Lafg;-><init>(Ldbt;Laet;)V

    iput-object v0, p0, Ladv;->a:Lafg;

    .line 160
    iget-object v0, p0, Ladv;->d:Laek;

    iget-object v1, p0, Ladv;->a:Lafg;

    iput-object v1, v0, Laek;->e:Ldbt;

    iget-object v2, v0, Laek;->b:Lael;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbt;

    iput-object v0, v2, Lael;->h:Ldbt;

    .line 161
    return-void
.end method

.method public final a(Ldbu;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 262
    iput-object p1, p0, Ladv;->p:Ldbu;

    .line 263
    iget-object v0, p0, Ladv;->d:Laek;

    sget-object v1, Ldbu;->d:Ldbu;

    if-ne p1, v1, :cond_0

    iget-object v1, v0, Laek;->c:Laez;

    invoke-virtual {v1, v3}, Laez;->setVisibility(I)V

    iget-object v0, v0, Laek;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    :goto_0
    iget-object v1, p0, Ladv;->e:Laex;

    sget-object v0, Ldbu;->c:Ldbu;

    if-ne p1, v0, :cond_1

    sget-object v0, Lafb;->b:[I

    :goto_1
    iget-object v2, v1, Laex;->a:Laew;

    invoke-virtual {v2, v0}, Laew;->setState([I)Z

    invoke-virtual {v1}, Laex;->invalidate()V

    .line 265
    return-void

    .line 263
    :cond_0
    iget-object v1, v0, Laek;->c:Laez;

    invoke-virtual {v1, p1}, Laez;->a(Ldbu;)V

    iget-object v1, v0, Laek;->c:Laez;

    invoke-virtual {v1, v2}, Laez;->setVisibility(I)V

    iget-object v0, v0, Laek;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 264
    :cond_1
    sget-object v0, Ldbu;->d:Ldbu;

    if-ne p1, v0, :cond_2

    sget-object v0, Lafb;->c:[I

    goto :goto_1

    :cond_2
    sget-object v0, Lafb;->a:[I

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Ladv;->d:Laek;

    iget-object v0, v0, Laek;->b:Lael;

    iget-object v0, v0, Lael;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 191
    if-eqz p2, :cond_0

    sget-object v0, Ldbs;->e:Ldbs;

    :goto_0
    iput-object v0, p0, Ladv;->q:Ldbs;

    .line 192
    iget-object v1, p0, Ladv;->j:Landroid/widget/ImageView;

    if-eqz p2, :cond_1

    const v0, 0x7f02004b

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 194
    iget-object v0, p0, Ladv;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    invoke-direct {p0}, Ladv;->i()V

    .line 197
    invoke-virtual {p0}, Ladv;->d()V

    .line 198
    return-void

    .line 191
    :cond_0
    sget-object v0, Ldbs;->f:Ldbs;

    goto :goto_0

    .line 192
    :cond_1
    const v0, 0x7f02004c

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 578
    iget-object v0, p0, Ladv;->o:Ldel;

    new-instance v1, Ladw;

    invoke-direct {v1, p0}, Ladw;-><init>(Ladv;)V

    invoke-virtual {v0, p1, v1}, Ldel;->a(Ljava/util/List;Lden;)V

    .line 584
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 307
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 269
    iput-boolean p1, p0, Ladv;->u:Z

    .line 270
    invoke-direct {p0}, Ladv;->i()V

    .line 271
    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Ladv;->d:Laek;

    iget-object v0, v0, Laek;->b:Lael;

    iput-object p1, v0, Lael;->f:[Ljava/lang/String;

    iput p2, v0, Lael;->g:I

    .line 213
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 275
    iput-boolean p1, p0, Ladv;->x:Z

    .line 276
    if-eqz p1, :cond_1

    .line 277
    invoke-virtual {p0}, Ladv;->e()V

    .line 282
    :cond_0
    invoke-direct {p0}, Ladv;->i()V

    .line 284
    :goto_0
    return-void

    .line 279
    :cond_1
    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v1, Ldbs;->c:Ldbs;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v1, Ldbs;->g:Ldbs;

    if-ne v0, v1, :cond_0

    .line 280
    :cond_2
    invoke-virtual {p0}, Ladv;->d()V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 239
    iget-object v0, p0, Ladv;->d:Laek;

    invoke-virtual {v0, v1, v1, v1}, Laek;->a(III)V

    .line 240
    iget-object v0, p0, Ladv;->e:Laex;

    invoke-virtual {v0, v1, v1, v1}, Laex;->a(III)V

    .line 241
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Ladv;->d:Laek;

    iget-object v0, v0, Laek;->b:Lael;

    iget-object v1, v0, Lael;->a:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 208
    return-void

    .line 207
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 288
    iget-boolean v0, p0, Ladv;->x:Z

    if-eqz v0, :cond_0

    .line 302
    :goto_0
    return-void

    .line 292
    :cond_0
    iget-boolean v0, p0, Ladv;->y:Z

    .line 294
    invoke-direct {p0}, Ladv;->h()V

    .line 295
    iput-boolean v1, p0, Ladv;->y:Z

    .line 296
    invoke-direct {p0}, Ladv;->i()V

    .line 297
    invoke-virtual {p0, v1}, Ladv;->setFocusable(Z)V

    .line 298
    if-eqz v0, :cond_1

    iget-object v0, p0, Ladv;->a:Lafg;

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p0, Ladv;->a:Lafg;

    invoke-virtual {v0}, Lafg;->i()V

    .line 301
    :cond_1
    invoke-direct {p0}, Ladv;->g()V

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 217
    iput-boolean p1, p0, Ladv;->s:Z

    .line 218
    invoke-direct {p0}, Ladv;->i()V

    .line 219
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 364
    invoke-direct {p0}, Ladv;->h()V

    .line 365
    iget-boolean v0, p0, Ladv;->y:Z

    if-eqz v0, :cond_1

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    iput-boolean v2, p0, Ladv;->y:Z

    .line 369
    iget-object v0, p0, Ladv;->d:Laek;

    iget-object v0, v0, Laek;->b:Lael;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lael;->setVisibility(I)V

    .line 370
    invoke-direct {p0}, Ladv;->i()V

    .line 372
    invoke-virtual {p0, v2}, Ladv;->setFocusable(Z)V

    .line 373
    invoke-virtual {p0}, Ladv;->requestFocus()Z

    .line 374
    iget-object v0, p0, Ladv;->a:Lafg;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Ladv;->a:Lafg;

    invoke-virtual {v0}, Lafg;->j()V

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 223
    iput-boolean p1, p0, Ladv;->t:Z

    .line 224
    invoke-direct {p0}, Ladv;->i()V

    .line 225
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Ladv;->o:Ldel;

    invoke-virtual {v0}, Ldel;->a()V

    .line 609
    return-void
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Ladv;->d:Laek;

    invoke-virtual {v0, p1}, Laek;->b(Z)V

    .line 230
    return-void
.end method

.method public final g(Z)V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Ladv;->d:Laek;

    iget-object v1, v0, Laek;->d:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 235
    return-void

    .line 234
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final h(Z)V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Ladv;->d:Laek;

    iget-object v0, v0, Laek;->c:Laez;

    invoke-virtual {v0, p1}, Laez;->setEnabled(Z)V

    .line 246
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 318
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v0, :cond_0

    .line 319
    invoke-direct {p0, v1}, Ladv;->n(Z)V

    .line 322
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final i(Z)V
    .locals 0

    .prologue
    .line 251
    return-void
.end method

.method public final j(Z)V
    .locals 2

    .prologue
    .line 567
    iput-boolean p1, p0, Ladv;->r:Z

    .line 568
    iget-object v0, p0, Ladv;->d:Laek;

    iget-object v0, v0, Laek;->b:Lael;

    iget-object v1, v0, Lael;->b:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 569
    return-void

    .line 568
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final k(Z)V
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Ladv;->d:Laek;

    iget-object v0, v0, Laek;->b:Lael;

    iget-object v0, v0, Lael;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 574
    return-void
.end method

.method public final l(Z)V
    .locals 0

    .prologue
    .line 599
    return-void
.end method

.method public final m(Z)V
    .locals 0

    .prologue
    .line 604
    return-void
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 358
    const/4 v0, 0x0

    iput-boolean v0, p0, Ladv;->z:Z

    .line 359
    invoke-virtual {p0}, Ladv;->e()V

    .line 360
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 619
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 614
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Ladv;->a:Lafg;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 390
    invoke-virtual {p0}, Ladv;->e()V

    .line 391
    iget-object v0, p0, Ladv;->a:Lafg;

    invoke-virtual {v0}, Lafg;->d()V

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 392
    :cond_1
    iget-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 393
    invoke-virtual {p0}, Ladv;->e()V

    .line 394
    iget-object v0, p0, Ladv;->a:Lafg;

    invoke-virtual {v0}, Lafg;->e()V

    goto :goto_0

    .line 395
    :cond_2
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 396
    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v1, Ldbs;->g:Ldbs;

    if-ne v0, v1, :cond_3

    .line 397
    iget-object v0, p0, Ladv;->a:Lafg;

    invoke-virtual {v0}, Lafg;->l()V

    goto :goto_0

    .line 398
    :cond_3
    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v1, Ldbs;->b:Ldbs;

    if-ne v0, v1, :cond_4

    .line 399
    iget-object v0, p0, Ladv;->a:Lafg;

    invoke-virtual {v0}, Lafg;->b()V

    goto :goto_0

    .line 400
    :cond_4
    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v1, Ldbs;->c:Ldbs;

    if-ne v0, v1, :cond_0

    .line 401
    iget-object v0, p0, Ladv;->a:Lafg;

    invoke-virtual {v0}, Lafg;->a()V

    goto :goto_0
.end method

.method public final onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 382
    iget-object v1, p0, Ladv;->a:Lafg;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lafg;->a:Z

    .line 383
    invoke-super {p0, p1}, Ldee;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 382
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 409
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lddv;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v0

    .line 410
    :goto_0
    if-eqz v2, :cond_1

    .line 411
    invoke-virtual {p0}, Ladv;->d()V

    .line 413
    :cond_1
    if-eqz v2, :cond_4

    iget-object v2, p0, Ladv;->q:Ldbs;

    sget-object v3, Ldbs;->e:Ldbs;

    if-ne v2, v3, :cond_4

    .line 414
    iget-object v1, p0, Ladv;->a:Lafg;

    invoke-virtual {v1}, Lafg;->k()V

    .line 417
    :cond_2
    :goto_1
    return v0

    :cond_3
    move v2, v1

    .line 409
    goto :goto_0

    .line 417
    :cond_4
    iget-object v2, p0, Ladv;->l:Lddv;

    invoke-virtual {v2, p1, p2}, Lddv;->a(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-super {p0, p1, p2}, Ldee;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_1
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Ladv;->l:Lddv;

    invoke-virtual {v0, p1}, Lddv;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Ldee;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 482
    sub-int v0, p5, p3

    .line 483
    sub-int v1, p4, p2

    .line 485
    invoke-virtual {p0}, Ladv;->getPaddingLeft()I

    move-result v2

    .line 486
    invoke-virtual {p0}, Ladv;->getPaddingBottom()I

    move-result v3

    sub-int v3, v0, v3

    .line 489
    div-int/lit8 v1, v1, 0x2

    .line 490
    div-int/lit8 v4, v0, 0x2

    .line 492
    iget-boolean v0, p0, Ladv;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladv;->e:Laex;

    .line 493
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v3, v5

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v0, v2, v5, v6, v3}, Landroid/view/View;->layout(IIII)V

    .line 495
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    invoke-static {v0, v1, v4}, Ladv;->a(Landroid/view/View;II)V

    .line 496
    iget-object v0, p0, Ladv;->j:Landroid/widget/ImageView;

    invoke-static {v0, v1, v4}, Ladv;->a(Landroid/view/View;II)V

    .line 497
    iget-object v0, p0, Ladv;->f:Landroid/widget/ProgressBar;

    invoke-static {v0, v1, v4}, Ladv;->a(Landroid/view/View;II)V

    .line 498
    iget-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    iget-object v1, p0, Ladv;->g:Landroid/widget/ImageButton;

    .line 499
    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLeft()I

    move-result v1

    iget-object v5, p0, Ladv;->i:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v1, v5

    .line 498
    invoke-static {v0, v1, v4}, Ladv;->a(Landroid/view/View;II)V

    .line 500
    iget-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    iget-object v1, p0, Ladv;->g:Landroid/widget/ImageButton;

    .line 501
    invoke-virtual {v1}, Landroid/widget/ImageButton;->getRight()I

    move-result v1

    iget-object v5, p0, Ladv;->h:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v1, v5

    .line 500
    invoke-static {v0, v1, v4}, Ladv;->a(Landroid/view/View;II)V

    .line 503
    iget-object v0, p0, Ladv;->k:Landroid/widget/TextView;

    iget-object v1, p0, Ladv;->k:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v3, v1

    iget-object v4, p0, Ladv;->k:Landroid/widget/TextView;

    .line 504
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    .line 503
    invoke-virtual {v0, v2, v1, v4, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 505
    return-void

    .line 492
    :cond_0
    iget-object v0, p0, Ladv;->d:Laek;

    goto :goto_0
.end method

.method protected final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 461
    invoke-static {v1, p1}, Ladv;->getDefaultSize(II)I

    move-result v0

    .line 462
    invoke-static {v1, p2}, Ladv;->getDefaultSize(II)I

    move-result v1

    .line 463
    invoke-virtual {p0, v0, v1}, Ladv;->setMeasuredDimension(II)V

    .line 465
    mul-int/lit8 v0, v0, 0xa

    div-int/lit8 v0, v0, 0x64

    .line 466
    iget-object v1, p0, Ladv;->k:Landroid/widget/TextView;

    iget-object v2, p0, Ladv;->k:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Ladv;->k:Landroid/widget/TextView;

    .line 467
    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v3

    .line 466
    invoke-virtual {v1, v0, v2, v0, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 469
    iget-object v0, p0, Ladv;->f:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0, p1, p2}, Ladv;->measureChild(Landroid/view/View;II)V

    .line 470
    iget-object v0, p0, Ladv;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, p1, p2}, Ladv;->measureChild(Landroid/view/View;II)V

    .line 471
    iget-object v0, p0, Ladv;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1, p2}, Ladv;->measureChild(Landroid/view/View;II)V

    .line 472
    iget-object v0, p0, Ladv;->k:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1, p2}, Ladv;->measureChild(Landroid/view/View;II)V

    .line 473
    iget-object v0, p0, Ladv;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, p1, p2}, Ladv;->measureChild(Landroid/view/View;II)V

    .line 474
    iget-object v0, p0, Ladv;->i:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, p1, p2}, Ladv;->measureChild(Landroid/view/View;II)V

    .line 476
    iget-boolean v0, p0, Ladv;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladv;->e:Laex;

    .line 477
    :goto_0
    invoke-virtual {p0, v0, p1, p2}, Ladv;->measureChild(Landroid/view/View;II)V

    .line 478
    return-void

    .line 476
    :cond_0
    iget-object v0, p0, Ladv;->d:Laek;

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 428
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 447
    :goto_0
    :pswitch_0
    return v3

    .line 430
    :pswitch_1
    iget-boolean v0, p0, Ladv;->y:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-boolean v0, p0, Ladv;->u:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ladv;->e:Laex;

    :goto_1
    iget-boolean v1, p0, Ladv;->u:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Ladv;->v:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Ladv;->d:Laek;

    invoke-virtual {v1}, Laek;->getBottom()I

    move-result v1

    iget-object v6, p0, Ladv;->d:Laek;

    iget v6, v6, Laek;->a:I

    sub-int/2addr v1, v6

    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v6, v4

    if-gtz v6, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v4, v4, v6

    if-gtz v4, :cond_3

    int-to-float v1, v1

    cmpg-float v1, v1, v5

    if-gtz v1, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v5, v0

    if-gtz v0, :cond_3

    move v0, v3

    :goto_3
    if-nez v0, :cond_0

    iget-boolean v0, p0, Ladv;->z:Z

    if-eqz v0, :cond_4

    :cond_0
    move v0, v3

    :goto_4
    iput-boolean v0, p0, Ladv;->w:Z

    .line 432
    invoke-virtual {p0}, Ladv;->d()V

    goto :goto_0

    .line 430
    :cond_1
    iget-object v0, p0, Ladv;->d:Laek;

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    .line 435
    :pswitch_2
    iget-object v0, p0, Ladv;->q:Ldbs;

    sget-object v1, Ldbs;->e:Ldbs;

    if-ne v0, v1, :cond_6

    .line 436
    iget-object v0, p0, Ladv;->a:Lafg;

    invoke-virtual {v0}, Lafg;->k()V

    .line 440
    :cond_5
    :goto_5
    iput-boolean v2, p0, Ladv;->w:Z

    goto :goto_0

    .line 437
    :cond_6
    iget-boolean v0, p0, Ladv;->y:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Ladv;->w:Z

    if-nez v0, :cond_5

    .line 438
    invoke-direct {p0, v3}, Ladv;->n(Z)V

    goto :goto_5

    .line 443
    :pswitch_3
    iput-boolean v2, p0, Ladv;->w:Z

    goto :goto_0

    .line 428
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final r_()Ldeg;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 154
    new-instance v0, Ldeg;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Ldeg;-><init>(IIZ)V

    return-object v0
.end method
