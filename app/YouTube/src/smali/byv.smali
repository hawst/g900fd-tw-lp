.class final Lbyv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lbyt;


# direct methods
.method constructor <init>(Lbyt;)V
    .locals 0

    .prologue
    .line 588
    iput-object p1, p0, Lbyv;->a:Lbyt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 591
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->e:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 594
    :cond_0
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->c:Lawl;

    invoke-virtual {v0, p3}, Lawl;->a(I)Lgbu;

    move-result-object v0

    .line 595
    if-nez v0, :cond_5

    .line 596
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->c:Lawl;

    invoke-virtual {v0, p3}, Lawl;->d(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 597
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->f:Lbym;

    iget-object v0, v0, Lbym;->d:Lfxe;

    iget-object v1, p0, Lbyv;->a:Lbyt;

    .line 598
    iget-object v1, v1, Lbyt;->a:Ljava/lang/String;

    iget-object v2, p0, Lbyv;->a:Lbyt;

    iget-object v2, v2, Lbyt;->f:Lbym;

    iget-object v2, v2, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lbzc;

    iget-object v4, p0, Lbyv;->a:Lbyt;

    iget-object v4, v4, Lbyt;->f:Lbym;

    iget-object v4, v4, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v5, p0, Lbyv;->a:Lbyt;

    iget-object v5, v5, Lbyt;->f:Lbym;

    .line 600
    iget-object v5, v5, Lbym;->i:Leyt;

    invoke-direct {v3, v4, v5}, Lbzc;-><init>(Landroid/app/Activity;Leyt;)V

    .line 599
    invoke-static {v2, v3}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v2

    .line 597
    invoke-interface {v0, v1, v2}, Lfxe;->h(Ljava/lang/String;Leuc;)V

    .line 620
    :cond_1
    :goto_0
    return-void

    .line 601
    :cond_2
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->c:Lawl;

    invoke-virtual {v0, p3}, Lawl;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 602
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->f:Lbym;

    iget-object v0, v0, Lbym;->h:Lctk;

    iget-object v1, p0, Lbyv;->a:Lbyt;

    iget-object v1, v1, Lbyt;->f:Lbym;

    iget-object v1, v1, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lbyq;

    iget-object v3, p0, Lbyv;->a:Lbyt;

    iget-object v3, v3, Lbyt;->f:Lbym;

    iget-object v4, p0, Lbyv;->a:Lbyt;

    .line 604
    iget-object v4, v4, Lbyt;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lbyq;-><init>(Lbym;Ljava/lang/String;)V

    .line 602
    invoke-virtual {v0, v1, v2}, Lctk;->a(Landroid/app/Activity;Lctr;)V

    goto :goto_0

    .line 605
    :cond_3
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->c:Lawl;

    invoke-virtual {v0, p3}, Lawl;->e(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 606
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->f:Lbym;

    iget-object v0, v0, Lbym;->j:Lbyr;

    iget-object v1, p0, Lbyv;->a:Lbyt;

    iget-object v1, v1, Lbyt;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbyr;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 607
    :cond_4
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->c:Lawl;

    invoke-virtual {v0, p3}, Lawl;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 608
    iget-object v0, p0, Lbyv;->a:Lbyt;

    iget-object v0, v0, Lbyt;->f:Lbym;

    iget-object v0, v0, Lbym;->h:Lctk;

    iget-object v1, p0, Lbyv;->a:Lbyt;

    iget-object v1, v1, Lbyt;->f:Lbym;

    iget-object v1, v1, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lbyw;

    invoke-direct {v2, p0}, Lbyw;-><init>(Lbyv;)V

    invoke-virtual {v0, v1, v2}, Lctk;->a(Landroid/app/Activity;Lctr;)V

    goto :goto_0

    .line 618
    :cond_5
    iget-object v1, p0, Lbyv;->a:Lbyt;

    iget-object v1, v1, Lbyt;->f:Lbym;

    iget-object v2, p0, Lbyv;->a:Lbyt;

    iget-object v2, v2, Lbyt;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lbym;->a(Ljava/lang/String;Lgbu;)V

    goto :goto_0
.end method
