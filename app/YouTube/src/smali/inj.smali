.class public final Linj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lilj;

.field final b:Ljava/net/URI;

.field public final c:Lilw;

.field final d:Ljava/net/ProxySelector;

.field final e:Limn;

.field public final f:Limb;

.field g:Ljava/net/Proxy;

.field h:Ljava/net/InetSocketAddress;

.field i:Z

.field private final j:Lilu;

.field private final k:Lilo;

.field private l:Ljava/net/Proxy;

.field private m:Ljava/util/Iterator;

.field private n:[Ljava/net/InetAddress;

.field private o:I

.field private p:I

.field private q:Ljava/lang/String;

.field private final r:Ljava/util/List;


# direct methods
.method public constructor <init>(Lilj;Ljava/net/URI;Lilw;Limb;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Linj;->r:Ljava/util/List;

    .line 85
    iput-object p1, p0, Linj;->a:Lilj;

    .line 86
    iput-object p2, p0, Linj;->b:Ljava/net/URI;

    .line 87
    iput-object p3, p0, Linj;->c:Lilw;

    .line 88
    iget-object v0, p3, Lilw;->d:Ljava/net/ProxySelector;

    iput-object v0, p0, Linj;->d:Ljava/net/ProxySelector;

    .line 89
    iget-object v0, p3, Lilw;->l:Lilo;

    iput-object v0, p0, Linj;->k:Lilo;

    .line 90
    sget-object v0, Limh;->a:Limh;

    invoke-virtual {v0, p3}, Limh;->b(Lilw;)Limn;

    move-result-object v0

    iput-object v0, p0, Linj;->e:Limn;

    .line 91
    iget-object v0, p3, Lilw;->m:Lilu;

    iput-object v0, p0, Linj;->j:Lilu;

    .line 92
    iput-object p4, p0, Linj;->f:Limb;

    .line 94
    iget-object v0, p1, Lilj;->a:Ljava/net/Proxy;

    const/4 v1, 0x1

    iput-boolean v1, p0, Linj;->i:Z

    if-eqz v0, :cond_1

    iput-object v0, p0, Linj;->l:Ljava/net/Proxy;

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Linj;->d:Ljava/net/ProxySelector;

    invoke-virtual {v0, p2}, Ljava/net/ProxySelector;->select(Ljava/net/URI;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Linj;->m:Ljava/util/Iterator;

    goto :goto_0
.end method

.method private f()Ljava/net/Proxy;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 223
    iget-object v0, p0, Linj;->l:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    .line 224
    iput-boolean v3, p0, Linj;->i:Z

    .line 225
    iget-object v0, p0, Linj;->l:Ljava/net/Proxy;

    .line 241
    :goto_0
    return-object v0

    .line 230
    :cond_0
    iget-object v0, p0, Linj;->m:Ljava/util/Iterator;

    if-eqz v0, :cond_2

    .line 231
    :cond_1
    iget-object v0, p0, Linj;->m:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    iget-object v0, p0, Linj;->m:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Proxy;

    .line 233
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v1, v2, :cond_1

    goto :goto_0

    .line 240
    :cond_2
    iput-boolean v3, p0, Linj;->i:Z

    .line 241
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    goto :goto_0
.end method


# virtual methods
.method public final a()Liln;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 142
    :goto_0
    iget-object v0, p0, Linj;->k:Lilo;

    iget-object v1, p0, Linj;->a:Lilj;

    invoke-virtual {v0, v1}, Lilo;->a(Lilj;)Liln;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 143
    iget-object v1, p0, Linj;->f:Limb;

    iget-object v1, v1, Limb;->b:Ljava/lang/String;

    const-string v2, "GET"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Limh;->a:Limh;

    invoke-virtual {v1, v0}, Limh;->c(Liln;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    :cond_0
    :goto_1
    return-object v0

    .line 144
    :cond_1
    iget-object v0, v0, Liln;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {p0}, Linj;->c()Z

    move-result v0

    if-nez v0, :cond_7

    .line 149
    invoke-virtual {p0}, Linj;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 150
    iget-boolean v0, p0, Linj;->i:Z

    if-nez v0, :cond_4

    .line 151
    invoke-virtual {p0}, Linj;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 152
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 154
    :cond_3
    new-instance v1, Liln;

    iget-object v2, p0, Linj;->k:Lilo;

    iget-object v0, p0, Linj;->r:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limg;

    invoke-direct {v1, v2, v0}, Liln;-><init>(Lilo;Limg;)V

    move-object v0, v1

    goto :goto_1

    .line 156
    :cond_4
    invoke-direct {p0}, Linj;->f()Ljava/net/Proxy;

    move-result-object v0

    iput-object v0, p0, Linj;->g:Ljava/net/Proxy;

    .line 157
    iget-object v0, p0, Linj;->g:Ljava/net/Proxy;

    iput-object v6, p0, Linj;->n:[Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v1, v2, :cond_8

    iget-object v0, p0, Linj;->b:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Linj;->b:Ljava/net/URI;

    invoke-static {v1}, Limo;->a(Ljava/net/URI;)I

    move-result v1

    iput v1, p0, Linj;->p:I

    :goto_2
    iget-object v1, p0, Linj;->j:Lilu;

    invoke-interface {v1, v0}, Lilu;->a(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Linj;->n:[Ljava/net/InetAddress;

    iput v5, p0, Linj;->o:I

    .line 159
    :cond_5
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Linj;->n:[Ljava/net/InetAddress;

    iget v2, p0, Linj;->o:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Linj;->o:I

    aget-object v1, v1, v2

    iget v2, p0, Linj;->p:I

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iget v1, p0, Linj;->o:I

    iget-object v2, p0, Linj;->n:[Ljava/net/InetAddress;

    array-length v2, v2

    if-ne v1, v2, :cond_6

    iput-object v6, p0, Linj;->n:[Ljava/net/InetAddress;

    iput v5, p0, Linj;->o:I

    :cond_6
    iput-object v0, p0, Linj;->h:Ljava/net/InetSocketAddress;

    .line 160
    iget-object v0, p0, Linj;->a:Lilj;

    iget-object v0, v0, Lilj;->e:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_a

    const-string v0, "TLSv1"

    :goto_3
    iput-object v0, p0, Linj;->q:Ljava/lang/String;

    .line 163
    :cond_7
    invoke-virtual {p0}, Linj;->d()Ljava/lang/String;

    move-result-object v0

    .line 164
    new-instance v1, Limg;

    iget-object v2, p0, Linj;->a:Lilj;

    iget-object v3, p0, Linj;->g:Ljava/net/Proxy;

    iget-object v4, p0, Linj;->h:Ljava/net/InetSocketAddress;

    invoke-direct {v1, v2, v3, v4, v0}, Limg;-><init>(Lilj;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Linj;->e:Limn;

    invoke-virtual {v0, v1}, Limn;->c(Limg;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 166
    iget-object v0, p0, Linj;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 157
    :cond_8
    invoke-virtual {v0}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    instance-of v1, v0, Ljava/net/InetSocketAddress;

    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Proxy.address() is not an InetSocketAddress: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    check-cast v0, Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    iput v0, p0, Linj;->p:I

    move-object v0, v1

    goto/16 :goto_2

    .line 160
    :cond_a
    const-string v0, "SSLv3"

    goto :goto_3

    .line 172
    :cond_b
    new-instance v0, Liln;

    iget-object v2, p0, Linj;->k:Lilo;

    invoke-direct {v0, v2, v1}, Liln;-><init>(Lilo;Limg;)V

    goto/16 :goto_1
.end method

.method b()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Linj;->n:[Ljava/net/InetAddress;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Linj;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Linj;->q:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 301
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No next TLS version"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_0
    iget-object v0, p0, Linj;->q:Ljava/lang/String;

    const-string v1, "TLSv1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    const-string v0, "SSLv3"

    iput-object v0, p0, Linj;->q:Ljava/lang/String;

    .line 304
    const-string v0, "TLSv1"

    .line 307
    :goto_0
    return-object v0

    .line 305
    :cond_1
    iget-object v0, p0, Linj;->q:Ljava/lang/String;

    const-string v1, "SSLv3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 306
    const/4 v0, 0x0

    iput-object v0, p0, Linj;->q:Ljava/lang/String;

    .line 307
    const-string v0, "SSLv3"

    goto :goto_0

    .line 309
    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method e()Z
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Linj;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
