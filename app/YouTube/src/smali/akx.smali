.class public final Lakx;
.super Lajp;
.source "SourceFile"


# instance fields
.field public final a:Lala;

.field public b:Lakz;

.field private final c:Landroid/content/Context;

.field private final d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lala;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lajp;-><init>()V

    .line 39
    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lala;

    iput-object v0, p0, Lakx;->a:Lala;

    .line 40
    const-string v0, "context cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lakx;->c:Landroid/content/Context;

    .line 41
    const-string v0, "uiHandler cannot be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lakx;->d:Landroid/os/Handler;

    .line 42
    return-void
.end method

.method static synthetic a(Lakx;Lakz;)Lakz;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lakx;->b:Lakz;

    return-object p1
.end method

.method static synthetic a(Lakx;)Lala;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lakx;->a:Lala;

    return-object v0
.end method

.method static synthetic b(Lakx;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lakx;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lakx;)Lakz;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lakx;->b:Lakz;

    return-object v0
.end method


# virtual methods
.method public final a(Laob;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lakx;->d:Landroid/os/Handler;

    new-instance v1, Laky;

    invoke-direct {v1, p0, p1}, Laky;-><init>(Lakx;Laob;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 63
    return-void
.end method
