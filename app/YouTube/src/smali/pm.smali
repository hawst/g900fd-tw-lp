.class public final Lpm;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field public a:Lpc;

.field b:Z

.field public final synthetic c:Lpi;

.field private d:I

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>(Lpi;)V
    .locals 1

    .prologue
    .line 618
    iput-object p1, p0, Lpm;->c:Lpi;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 632
    const/4 v0, 0x4

    iput v0, p0, Lpm;->d:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 739
    iget v4, p0, Lpm;->d:I

    .line 740
    const v1, 0x7fffffff

    iput v1, p0, Lpm;->d:I

    .line 745
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 746
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 747
    invoke-virtual {p0}, Lpm;->getCount()I

    move-result v7

    move-object v1, v2

    move v3, v0

    .line 749
    :goto_0
    if-ge v0, v7, :cond_0

    .line 750
    invoke-virtual {p0, v0, v1, v2}, Lpm;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 751
    invoke-virtual {v1, v5, v6}, Landroid/view/View;->measure(II)V

    .line 752
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 749
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 755
    :cond_0
    iput v4, p0, Lpm;->d:I

    .line 757
    return v3
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 761
    iget v0, p0, Lpm;->d:I

    if-eq v0, p1, :cond_0

    .line 762
    iput p1, p0, Lpm;->d:I

    .line 763
    invoke-virtual {p0}, Lpm;->notifyDataSetChanged()V

    .line 765
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 772
    iget-boolean v0, p0, Lpm;->f:Z

    if-eq v0, p1, :cond_0

    .line 773
    iput-boolean p1, p0, Lpm;->f:Z

    .line 774
    invoke-virtual {p0}, Lpm;->notifyDataSetChanged()V

    .line 776
    :cond_0
    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 796
    iget-boolean v0, p0, Lpm;->b:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lpm;->e:Z

    if-eq v0, p2, :cond_1

    .line 798
    :cond_0
    iput-boolean p1, p0, Lpm;->b:Z

    .line 799
    iput-boolean p2, p0, Lpm;->e:Z

    .line 800
    invoke-virtual {p0}, Lpm;->notifyDataSetChanged()V

    .line 802
    :cond_1
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 667
    iget-object v0, p0, Lpm;->a:Lpc;

    invoke-virtual {v0}, Lpc;->a()I

    move-result v0

    .line 669
    iget-boolean v1, p0, Lpm;->b:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lpm;->a:Lpc;

    invoke-virtual {v1}, Lpc;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 670
    add-int/lit8 v0, v0, -0x1

    .line 672
    :cond_0
    iget v1, p0, Lpm;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 673
    iget-boolean v1, p0, Lpm;->f:Z

    if-eqz v1, :cond_1

    .line 674
    add-int/lit8 v0, v0, 0x1

    .line 676
    :cond_1
    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 680
    invoke-virtual {p0, p1}, Lpm;->getItemViewType(I)I

    move-result v0

    .line 681
    packed-switch v0, :pswitch_data_0

    .line 690
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 683
    :pswitch_0
    const/4 v0, 0x0

    .line 688
    :goto_0
    return-object v0

    .line 685
    :pswitch_1
    iget-boolean v0, p0, Lpm;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lpm;->a:Lpc;

    invoke-virtual {v0}, Lpc;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 686
    add-int/lit8 p1, p1, 0x1

    .line 688
    :cond_0
    iget-object v0, p0, Lpm;->a:Lpc;

    invoke-virtual {v0, p1}, Lpc;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    goto :goto_0

    .line 681
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 695
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 654
    iget-boolean v0, p0, Lpm;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lpm;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 655
    const/4 v0, 0x1

    .line 657
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const v4, 0x7f08008b

    const v3, 0x7f040009

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 699
    invoke-virtual {p0, p1}, Lpm;->getItemViewType(I)I

    move-result v0

    .line 700
    packed-switch v0, :pswitch_data_0

    .line 732
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 702
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 703
    :cond_0
    iget-object v0, p0, Lpm;->c:Lpi;

    invoke-virtual {v0}, Lpi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v3, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 705
    invoke-virtual {p2, v1}, Landroid/view/View;->setId(I)V

    .line 706
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 707
    iget-object v1, p0, Lpm;->c:Lpi;

    invoke-virtual {v1}, Lpi;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090087

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 730
    :cond_1
    :goto_0
    return-object p2

    .line 712
    :pswitch_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f080089

    if-eq v0, v1, :cond_3

    .line 713
    :cond_2
    iget-object v0, p0, Lpm;->c:Lpi;

    invoke-virtual {v0}, Lpi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v3, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 716
    :cond_3
    iget-object v0, p0, Lpm;->c:Lpi;

    invoke-virtual {v0}, Lpi;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 718
    const v0, 0x7f08008a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 719
    invoke-virtual {p0, p1}, Lpm;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 720
    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 722
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 723
    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 725
    iget-boolean v0, p0, Lpm;->b:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lpm;->e:Z

    goto :goto_0

    .line 700
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 663
    const/4 v0, 0x3

    return v0
.end method
