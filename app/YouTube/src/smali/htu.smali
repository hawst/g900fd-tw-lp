.class public final Lhtu;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Z

.field private b:Z

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:[Lhld;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-boolean v1, p0, Lhtu;->b:Z

    iput-boolean v1, p0, Lhtu;->c:Z

    const-string v0, ""

    iput-object v0, p0, Lhtu;->d:Ljava/lang/String;

    iput-boolean v1, p0, Lhtu;->e:Z

    iput-boolean v1, p0, Lhtu;->a:Z

    sget-object v0, Lhld;->a:[Lhld;

    iput-object v0, p0, Lhtu;->f:[Lhld;

    iput-boolean v1, p0, Lhtu;->g:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Lhtu;->b:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    iget-boolean v2, p0, Lhtu;->b:Z

    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-boolean v2, p0, Lhtu;->c:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x9

    iget-boolean v3, p0, Lhtu;->c:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhtu;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0xc

    iget-object v3, p0, Lhtu;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-boolean v2, p0, Lhtu;->e:Z

    if-eqz v2, :cond_2

    const/16 v2, 0xd

    iget-boolean v3, p0, Lhtu;->e:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_2
    iget-boolean v2, p0, Lhtu;->a:Z

    if-eqz v2, :cond_3

    const/16 v2, 0xe

    iget-boolean v3, p0, Lhtu;->a:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhtu;->f:[Lhld;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lhtu;->f:[Lhld;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    if-eqz v4, :cond_4

    const/16 v5, 0xf

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-boolean v1, p0, Lhtu;->g:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x10

    iget-boolean v2, p0, Lhtu;->g:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhtu;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhtu;->J:I

    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhtu;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhtu;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhtu;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhtu;->b:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhtu;->c:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhtu;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhtu;->e:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhtu;->a:Z

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhtu;->f:[Lhld;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhld;

    iget-object v3, p0, Lhtu;->f:[Lhld;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhtu;->f:[Lhld;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhtu;->f:[Lhld;

    :goto_2
    iget-object v2, p0, Lhtu;->f:[Lhld;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhtu;->f:[Lhld;

    new-instance v3, Lhld;

    invoke-direct {v3}, Lhld;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhtu;->f:[Lhld;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhtu;->f:[Lhld;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhtu;->f:[Lhld;

    new-instance v3, Lhld;

    invoke-direct {v3}, Lhld;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhtu;->f:[Lhld;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhtu;->g:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x38 -> :sswitch_1
        0x48 -> :sswitch_2
        0x62 -> :sswitch_3
        0x68 -> :sswitch_4
        0x70 -> :sswitch_5
        0x7a -> :sswitch_6
        0x80 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-boolean v0, p0, Lhtu;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    iget-boolean v1, p0, Lhtu;->b:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_0
    iget-boolean v0, p0, Lhtu;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x9

    iget-boolean v1, p0, Lhtu;->c:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_1
    iget-object v0, p0, Lhtu;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0xc

    iget-object v1, p0, Lhtu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Lhtu;->e:Z

    if-eqz v0, :cond_3

    const/16 v0, 0xd

    iget-boolean v1, p0, Lhtu;->e:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_3
    iget-boolean v0, p0, Lhtu;->a:Z

    if-eqz v0, :cond_4

    const/16 v0, 0xe

    iget-boolean v1, p0, Lhtu;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_4
    iget-object v0, p0, Lhtu;->f:[Lhld;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lhtu;->f:[Lhld;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    if-eqz v3, :cond_5

    const/16 v4, 0xf

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lhtu;->g:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x10

    iget-boolean v1, p0, Lhtu;->g:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_7
    iget-object v0, p0, Lhtu;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhtu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhtu;

    iget-boolean v2, p0, Lhtu;->b:Z

    iget-boolean v3, p1, Lhtu;->b:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhtu;->c:Z

    iget-boolean v3, p1, Lhtu;->c:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhtu;->d:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhtu;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-boolean v2, p0, Lhtu;->e:Z

    iget-boolean v3, p1, Lhtu;->e:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhtu;->a:Z

    iget-boolean v3, p1, Lhtu;->a:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhtu;->f:[Lhld;

    iget-object v3, p1, Lhtu;->f:[Lhld;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lhtu;->g:Z

    iget-boolean v3, p1, Lhtu;->g:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhtu;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhtu;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhtu;->d:Ljava/lang/String;

    iget-object v3, p1, Lhtu;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhtu;->I:Ljava/util/List;

    iget-object v3, p1, Lhtu;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhtu;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhtu;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhtu;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v3

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhtu;->e:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhtu;->a:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    iget-object v4, p0, Lhtu;->f:[Lhld;

    if-nez v4, :cond_6

    mul-int/lit8 v4, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v4, 0x1f

    iget-boolean v4, p0, Lhtu;->g:Z

    if-eqz v4, :cond_8

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhtu;->I:Ljava/util/List;

    if-nez v1, :cond_9

    :goto_6
    add-int/2addr v0, v3

    return v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhtu;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    move v4, v0

    move v0, v3

    :goto_7
    iget-object v5, p0, Lhtu;->f:[Lhld;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v5, v4, 0x1f

    iget-object v4, p0, Lhtu;->f:[Lhld;

    aget-object v4, v4, v0

    if-nez v4, :cond_7

    move v4, v3

    :goto_8
    add-int/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_7
    iget-object v4, p0, Lhtu;->f:[Lhld;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lhld;->hashCode()I

    move-result v4

    goto :goto_8

    :cond_8
    move v1, v2

    goto :goto_5

    :cond_9
    iget-object v1, p0, Lhtu;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v3

    goto :goto_6
.end method
