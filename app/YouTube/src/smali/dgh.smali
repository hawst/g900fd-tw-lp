.class public final Ldgh;
.super Levb;
.source "SourceFile"


# instance fields
.field final a:Levc;

.field private final d:Levn;

.field private final e:Lcws;

.field private final f:Lhog;

.field private g:Ldgi;


# direct methods
.method public constructor <init>(Levn;Lcws;Lhog;Levc;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Levb;-><init>()V

    .line 38
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldgh;->d:Levn;

    .line 39
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcws;

    iput-object v0, p0, Ldgh;->e:Lcws;

    .line 40
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhog;

    iput-object v0, p0, Ldgh;->f:Lhog;

    .line 41
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levc;

    iput-object v0, p0, Ldgh;->a:Levc;

    .line 42
    return-void
.end method

.method private c()V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 77
    iget-object v0, p0, Ldgh;->g:Ldgi;

    if-eqz v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v0, p0, Ldgh;->e:Lcws;

    invoke-virtual {v0}, Lcws;->h()I

    move-result v0

    int-to-long v4, v0

    .line 83
    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    .line 89
    iget-object v0, p0, Ldgh;->f:Lhog;

    .line 90
    invoke-static {v0}, La;->b(Lhog;)Lhqt;

    move-result-object v0

    .line 95
    iget v1, v0, Lhqt;->c:I

    if-ltz v1, :cond_2

    .line 96
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget v0, v0, Lhqt;->c:I

    int-to-long v2, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 98
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 109
    :goto_1
    iget-object v0, p0, Ldgh;->e:Lcws;

    invoke-virtual {v0}, Lcws;->y()Lcvl;

    move-result-object v6

    .line 110
    if-eqz v6, :cond_0

    .line 111
    new-instance v0, Ldgi;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldgi;-><init>(Ldgh;JJ)V

    iput-object v0, p0, Ldgh;->g:Ldgi;

    .line 112
    const/4 v0, 0x1

    new-array v7, v0, [Lcvh;

    const/4 v8, 0x0

    new-instance v0, Ldgi;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldgi;-><init>(Ldgh;JJ)V

    aput-object v0, v7, v8

    invoke-interface {v6, v7}, Lcvl;->a([Lcvh;)V

    goto :goto_0

    .line 103
    :cond_2
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget v0, v0, Lhqt;->c:I

    int-to-long v2, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    add-long/2addr v0, v4

    .line 106
    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ldgh;->d:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Ldgh;->e:Lcws;

    invoke-virtual {v0}, Lcws;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-direct {p0}, Ldgh;->c()V

    .line 52
    :cond_0
    return-void
.end method

.method public final handleVideoStageEvent(Ldac;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->b:Lgol;

    invoke-virtual {v0, v1}, Lgol;->a(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-direct {p0}, Ldgh;->c()V

    .line 73
    :cond_0
    return-void
.end method
