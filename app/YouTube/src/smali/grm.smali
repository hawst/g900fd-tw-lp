.class public abstract Lgrm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field final a:I

.field b:Z

.field private c:Lgrb;


# direct methods
.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lgrm;->c:Lgrb;

    .line 64
    iput p2, p0, Lgrm;->a:I

    .line 66
    return-void
.end method


# virtual methods
.method public abstract a(JZ)Lgrb;
.end method

.method public a()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgrm;->b:Z

    .line 96
    return-void
.end method

.method public abstract a(JLandroid/graphics/Bitmap;)V
.end method

.method public abstract a(Ljava/lang/Exception;)V
.end method

.method public abstract a(J)Z
.end method

.method public b()Lgrb;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lgrm;->c:Lgrb;

    .line 110
    const/4 v1, 0x0

    iput-object v1, p0, Lgrm;->c:Lgrb;

    .line 111
    return-object v0
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lgrf;->a:Lgrf;

    invoke-static {p1, p2, v0}, Lgrb;->a(JLgrf;)Lgrb;

    move-result-object v0

    iput-object v0, p0, Lgrm;->c:Lgrb;

    .line 154
    return-void
.end method

.method public abstract c()V
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 36
    check-cast p1, Lgrm;

    iget v0, p1, Lgrm;->a:I

    iget v1, p0, Lgrm;->a:I

    sub-int/2addr v0, v1

    return v0
.end method
