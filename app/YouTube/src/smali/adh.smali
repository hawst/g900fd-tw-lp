.class public final Ladh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lggz;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/telephony/TelephonyManager;

.field private final c:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Landroid/content/pm/PackageManager;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ladh;->a:Landroid/content/Context;

    .line 38
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    iput-object v0, p0, Ladh;->c:Landroid/content/pm/PackageManager;

    .line 39
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Ladh;->b:Landroid/telephony/TelephonyManager;

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Lhjx;Lgkp;)V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p1, Lhjx;->a:Lhcv;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p1, Lhjx;->a:Lhcv;

    .line 54
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhcv;->k:Ljava/lang/String;

    .line 55
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhcv;->l:Ljava/lang/String;

    .line 56
    const/4 v1, 0x3

    iput v1, v0, Lhcv;->g:I

    .line 57
    iget-object v1, p0, Ladh;->a:Landroid/content/Context;

    iget-object v2, p0, Ladh;->c:Landroid/content/pm/PackageManager;

    .line 58
    invoke-static {v1, v2}, La;->a(Landroid/content/Context;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhcv;->h:Ljava/lang/String;

    .line 59
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v1, v0, Lhcv;->j:Ljava/lang/String;

    .line 60
    const-string v1, "Android"

    iput-object v1, v0, Lhcv;->i:Ljava/lang/String;

    .line 61
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v1, v0, Lhcv;->e:Ljava/lang/String;

    .line 62
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, v0, Lhcv;->f:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Ladh;->b:Landroid/telephony/TelephonyManager;

    invoke-static {v1}, Lfaq;->a(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhcv;->c:Ljava/lang/String;

    .line 65
    iput-object v0, p1, Lhjx;->a:Lhcv;

    .line 66
    return-void

    .line 51
    :cond_0
    new-instance v0, Lhcv;

    invoke-direct {v0}, Lhcv;-><init>()V

    goto :goto_0
.end method
