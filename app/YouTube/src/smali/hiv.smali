.class public final Lhiv;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhiv;


# instance fields
.field public b:I

.field public c:Lhiw;

.field private d:Lhiu;

.field private e:Lhiy;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhiv;

    sput-object v0, Lhiv;->a:[Lhiv;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput v1, p0, Lhiv;->b:I

    iput-object v0, p0, Lhiv;->c:Lhiw;

    iput-object v0, p0, Lhiv;->d:Lhiu;

    iput-object v0, p0, Lhiv;->e:Lhiy;

    const-string v0, ""

    iput-object v0, p0, Lhiv;->f:Ljava/lang/String;

    iput v1, p0, Lhiv;->g:I

    const-string v0, ""

    iput-object v0, p0, Lhiv;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhiv;->i:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lhiv;->b:I

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lhiv;->b:I

    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhiv;->c:Lhiw;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhiv;->c:Lhiw;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhiv;->d:Lhiu;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhiv;->d:Lhiu;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhiv;->e:Lhiy;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lhiv;->e:Lhiy;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lhiv;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lhiv;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lhiv;->g:I

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Lhiv;->g:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhiv;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lhiv;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhiv;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lhiv;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lhiv;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhiv;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhiv;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhiv;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhiv;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lhiv;->b:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lhiv;->b:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhiv;->c:Lhiw;

    if-nez v0, :cond_4

    new-instance v0, Lhiw;

    invoke-direct {v0}, Lhiw;-><init>()V

    iput-object v0, p0, Lhiv;->c:Lhiw;

    :cond_4
    iget-object v0, p0, Lhiv;->c:Lhiw;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhiv;->d:Lhiu;

    if-nez v0, :cond_5

    new-instance v0, Lhiu;

    invoke-direct {v0}, Lhiu;-><init>()V

    iput-object v0, p0, Lhiv;->d:Lhiu;

    :cond_5
    iget-object v0, p0, Lhiv;->d:Lhiu;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhiv;->e:Lhiy;

    if-nez v0, :cond_6

    new-instance v0, Lhiy;

    invoke-direct {v0}, Lhiy;-><init>()V

    iput-object v0, p0, Lhiv;->e:Lhiy;

    :cond_6
    iget-object v0, p0, Lhiv;->e:Lhiy;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhiv;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhiv;->g:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhiv;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhiv;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget v0, p0, Lhiv;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lhiv;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_0
    iget-object v0, p0, Lhiv;->c:Lhiw;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhiv;->c:Lhiw;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhiv;->d:Lhiu;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhiv;->d:Lhiu;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhiv;->e:Lhiy;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lhiv;->e:Lhiy;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhiv;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lhiv;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_4
    iget v0, p0, Lhiv;->g:I

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Lhiv;->g:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_5
    iget-object v0, p0, Lhiv;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lhiv;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lhiv;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Lhiv;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lhiv;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhiv;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhiv;

    iget v2, p0, Lhiv;->b:I

    iget v3, p1, Lhiv;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhiv;->c:Lhiw;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhiv;->c:Lhiw;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhiv;->d:Lhiu;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhiv;->d:Lhiu;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhiv;->e:Lhiy;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhiv;->e:Lhiy;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhiv;->f:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhiv;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget v2, p0, Lhiv;->g:I

    iget v3, p1, Lhiv;->g:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhiv;->h:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhiv;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhiv;->i:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhiv;->i:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhiv;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhiv;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhiv;->c:Lhiw;

    iget-object v3, p1, Lhiv;->c:Lhiw;

    invoke-virtual {v2, v3}, Lhiw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhiv;->d:Lhiu;

    iget-object v3, p1, Lhiv;->d:Lhiu;

    invoke-virtual {v2, v3}, Lhiu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhiv;->e:Lhiy;

    iget-object v3, p1, Lhiv;->e:Lhiy;

    invoke-virtual {v2, v3}, Lhiy;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhiv;->f:Ljava/lang/String;

    iget-object v3, p1, Lhiv;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhiv;->h:Ljava/lang/String;

    iget-object v3, p1, Lhiv;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhiv;->i:Ljava/lang/String;

    iget-object v3, p1, Lhiv;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhiv;->I:Ljava/util/List;

    iget-object v3, p1, Lhiv;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhiv;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhiv;->c:Lhiw;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhiv;->d:Lhiu;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhiv;->e:Lhiy;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhiv;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhiv;->g:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhiv;->h:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhiv;->i:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhiv;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lhiv;->c:Lhiw;

    invoke-virtual {v0}, Lhiw;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhiv;->d:Lhiu;

    invoke-virtual {v0}, Lhiu;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhiv;->e:Lhiy;

    invoke-virtual {v0}, Lhiy;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhiv;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lhiv;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lhiv;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    iget-object v1, p0, Lhiv;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_6
.end method
