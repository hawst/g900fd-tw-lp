.class public final Lbto;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lfhz;

.field final c:Leyt;

.field d:Landroid/app/AlertDialog;

.field e:Landroid/view/View;

.field private final f:Lfln;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfhz;Leyt;Lfln;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbto;->a:Landroid/content/Context;

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lbto;->b:Lfhz;

    .line 48
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbto;->c:Leyt;

    .line 49
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfln;

    iput-object v0, p0, Lbto;->f:Lfln;

    .line 50
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 53
    iget-object v0, p0, Lbto;->d:Landroid/app/AlertDialog;

    if-nez v0, :cond_7

    .line 54
    iget-object v0, p0, Lbto;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400a5

    invoke-virtual {v0, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbto;->e:Landroid/view/View;

    iget-object v0, p0, Lbto;->e:Landroid/view/View;

    const v1, 0x7f08024c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lbto;->e:Landroid/view/View;

    const v1, 0x7f08024b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iget-object v2, p0, Lbto;->f:Lfln;

    iget-object v1, v2, Lfln;->b:Ljava/util/List;

    if-nez v1, :cond_3

    iget-object v1, v2, Lfln;->a:Lhqb;

    iget-object v1, v1, Lhqb;->a:[Lhpy;

    if-eqz v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v2, Lfln;->b:Ljava/util/List;

    iget-object v1, v2, Lfln;->a:Lhqb;

    iget-object v5, v1, Lhqb;->a:[Lhpy;

    array-length v6, v5

    move v1, v4

    :goto_0
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    iget-object v8, v7, Lhpy;->c:Lhpz;

    if-eqz v8, :cond_1

    iget-object v8, v2, Lfln;->b:Ljava/util/List;

    new-instance v9, Lfll;

    iget-object v7, v7, Lhpy;->c:Lhpz;

    invoke-direct {v9, v7}, Lfll;-><init>(Lhpz;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v8, v7, Lhpy;->b:Lhqa;

    if-eqz v8, :cond_2

    iget-object v8, v2, Lfln;->b:Ljava/util/List;

    new-instance v9, Lflm;

    iget-object v7, v7, Lhpy;->b:Lhqa;

    invoke-direct {v9, v7}, Lflm;-><init>(Lhqa;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v8, v7, Lhpy;->d:Lhqb;

    if-eqz v8, :cond_0

    iget-object v8, v2, Lfln;->b:Ljava/util/List;

    new-instance v9, Lfln;

    iget-object v7, v7, Lhpy;->d:Lhqb;

    invoke-direct {v9, v7}, Lfln;-><init>(Lhqb;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v1, v2, Lfln;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbt;

    new-instance v6, Landroid/widget/RadioButton;

    iget-object v2, p0, Lbto;->a:Landroid/content/Context;

    invoke-direct {v6, v2}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    instance-of v2, v1, Lfln;

    if-eqz v2, :cond_4

    move-object v2, v1

    check-cast v2, Lfln;

    invoke-virtual {v2}, Lfln;->b()Ljava/lang/CharSequence;

    move-result-object v2

    :goto_3
    invoke-virtual {v6, v2}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lbto;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f070082

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v6, v2}, Landroid/widget/RadioButton;->setTextColor(I)V

    invoke-virtual {v6, v1}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_4
    instance-of v2, v1, Lflk;

    if-eqz v2, :cond_8

    move-object v2, v1

    check-cast v2, Lflk;

    iget-object v7, v2, Lflk;->b:Ljava/lang/CharSequence;

    if-nez v7, :cond_5

    iget-object v7, v2, Lflk;->a:Lhgz;

    if-eqz v7, :cond_5

    iget-object v7, v2, Lflk;->a:Lhgz;

    invoke-static {v7}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v7

    iput-object v7, v2, Lflk;->b:Ljava/lang/CharSequence;

    :cond_5
    iget-object v2, v2, Lflk;->b:Ljava/lang/CharSequence;

    goto :goto_3

    :cond_6
    new-instance v1, Leyv;

    iget-object v2, p0, Lbto;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Leyv;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lbto;->f:Lfln;

    invoke-virtual {v2}, Lfln;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Leyv;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lbto;->e:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0902a6

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900a2

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    new-instance v2, Lbtq;

    invoke-direct {v2, p0, v1}, Lbtq;-><init>(Lbto;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iput-object v1, p0, Lbto;->d:Landroid/app/AlertDialog;

    .line 56
    :cond_7
    iget-object v0, p0, Lbto;->d:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 61
    iget-object v0, p0, Lbto;->d:Landroid/app/AlertDialog;

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 63
    new-instance v0, Lbtp;

    invoke-direct {v0, p0}, Lbtp;-><init>(Lbto;)V

    .line 92
    iget-object v1, p0, Lbto;->d:Landroid/app/AlertDialog;

    invoke-virtual {v1, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-void

    :cond_8
    move-object v2, v3

    goto/16 :goto_3
.end method
