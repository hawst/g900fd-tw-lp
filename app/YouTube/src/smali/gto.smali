.class public final Lgto;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgtn;


# instance fields
.field private final a:Lfgj;


# direct methods
.method public constructor <init>(Lfgj;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgj;

    iput-object v0, p0, Lgto;->a:Lfgj;

    .line 25
    return-void
.end method

.method private a(Lgvd;)Lguc;
    .locals 5

    .prologue
    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v0, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v0}, Lgvi;->b()Lgvi;

    move-result-object v1

    .line 52
    iget-object v0, v1, Lgvi;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 54
    iget-object v0, p0, Lgto;->a:Lfgj;

    iget-object v2, v1, Lgvi;->a:Ljava/lang/String;

    new-instance v3, Lfsv;

    iget-object v4, v0, Lfgj;->b:Lfsz;

    iget-object v0, v0, Lfgj;->c:Lgix;

    invoke-interface {v0, v2}, Lgix;->a(Ljava/lang/String;)Lgit;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lfsv;-><init>(Lfsz;Lgit;)V

    .line 55
    iget-object v0, v1, Lgvi;->j:Ljava/lang/String;

    iput-object v0, v3, Lfsv;->a:Ljava/lang/String;

    .line 57
    :try_start_0
    iget-object v0, p0, Lgto;->a:Lfgj;

    iget-object v0, v0, Lfgj;->e:Lfco;

    invoke-virtual {v0, v3}, Lfco;->a(Lfsp;)Lidh;

    move-result-object v0

    check-cast v0, Lhft;

    .line 58
    iget-boolean v0, v0, Lhft;->a:Z

    if-nez v0, :cond_1

    .line 59
    const/4 v0, 0x2

    invoke-static {v0}, La;->e(I)Lguc;

    move-result-object v0

    .line 73
    :goto_1
    return-object v0

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 61
    :cond_1
    new-instance v0, Lgtp;

    invoke-direct {v0, p0}, Lgtp;-><init>(Lgto;)V
    :try_end_0
    .catch Lfdv; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 72
    :catch_0
    move-exception v0

    .line 73
    invoke-static {v0}, La;->a(Lfdv;)Lguc;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)J
    .locals 2

    .prologue
    .line 19
    check-cast p1, Lgvd;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v0}, Lgvi;->b()Lgvi;

    move-result-object v0

    iget v1, v0, Lgvi;->d:I

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lgvi;->m:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lgvi;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v0, v0, Lgvi;->n:Z

    if-nez v0, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lguc;
    .locals 1

    .prologue
    .line 19
    check-cast p2, Lgvd;

    invoke-direct {p0, p2}, Lgto;->a(Lgvd;)Lguc;

    move-result-object v0

    return-object v0
.end method
