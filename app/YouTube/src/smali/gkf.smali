.class public Lgkf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgku;


# instance fields
.field final a:Lgic;

.field final b:Ljava/util/concurrent/Executor;

.field private final c:Lgku;

.field private final d:Lgib;


# direct methods
.method protected constructor <init>(Lgib;Lgic;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v0, p0, Lgkf;->c:Lgku;

    .line 56
    iput-object p1, p0, Lgkf;->d:Lgib;

    .line 57
    iput-object p2, p0, Lgkf;->a:Lgic;

    .line 58
    iput-object v0, p0, Lgkf;->b:Ljava/util/concurrent/Executor;

    .line 59
    return-void
.end method

.method public constructor <init>(Lgku;Lgib;Lgic;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgku;

    iput-object v0, p0, Lgkf;->c:Lgku;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lgkf;->d:Lgib;

    .line 43
    iput-object p3, p0, Lgkf;->a:Lgic;

    .line 44
    iput-object p4, p0, Lgkf;->b:Ljava/util/concurrent/Executor;

    .line 45
    return-void
.end method

.method public static a(Lgku;Lgic;Ljava/util/concurrent/Executor;)Lgku;
    .locals 2

    .prologue
    .line 248
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    new-instance v0, Lgkf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Lgkf;-><init>(Lgku;Lgib;Lgic;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Leuc;)V
    .locals 2

    .prologue
    .line 81
    :try_start_0
    iget-object v0, p0, Lgkf;->d:Lgib;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lgkf;->d:Lgib;

    invoke-interface {v0, p1}, Lgib;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 90
    :goto_0
    new-instance v1, Lgkg;

    invoke-direct {v1, p0, p1, v0, p2}, Lgkg;-><init>(Lgkf;Ljava/lang/Object;Ljava/lang/Object;Leuc;)V

    .line 92
    invoke-virtual {p0, v0, v1}, Lgkf;->b(Ljava/lang/Object;Leuc;)V
    :try_end_0
    .catch Lfax; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_1
    return-void

    :cond_0
    move-object v0, p1

    .line 87
    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p2, v0}, Lgkf;->a(Ljava/lang/Object;Ljava/lang/Object;Leuc;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;Leuc;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 112
    invoke-interface {p3, p1, p4}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 113
    return-void
.end method

.method protected b(Ljava/lang/Object;Leuc;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lgkf;->c:Lgku;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v0, p0, Lgkf;->c:Lgku;

    invoke-interface {v0, p1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 101
    return-void
.end method
