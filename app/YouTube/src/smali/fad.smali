.class public abstract Lfad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Landroid/os/Binder;

.field public final c:Ljava/lang/Class;

.field public final d:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lfad;->c:Ljava/lang/Class;

    .line 42
    new-instance v0, Lfae;

    invoke-direct {v0, p0}, Lfae;-><init>(Lfad;)V

    iput-object v0, p0, Lfad;->d:Landroid/content/ServiceConnection;

    .line 63
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lfad;->a:Z

    if-eqz v0, :cond_1

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfad;->a:Z

    .line 81
    iget-object v0, p0, Lfad;->b:Landroid/os/Binder;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lfad;->b:Landroid/os/Binder;

    invoke-virtual {p0, v0}, Lfad;->a(Landroid/os/Binder;)V

    .line 84
    :cond_0
    iget-object v0, p0, Lfad;->d:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 86
    :cond_1
    return-void
.end method

.method public abstract a(Landroid/os/Binder;)V
.end method

.method public abstract b(Landroid/os/Binder;)V
.end method
