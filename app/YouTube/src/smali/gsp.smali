.class public abstract Lgsp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Landroid/view/ViewGroup;

.field private d:Landroid/view/ViewGroup;

.field private e:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

.field private f:Landroid/view/ViewGroup;

.field private g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

.field private h:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

.field private i:I

.field private j:Lgqo;

.field private k:Lgrw;

.field private l:I

.field private m:Landroid/widget/Scroller;

.field private n:J

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lgsp;->l:I

    .line 72
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgsp;->n:J

    .line 75
    new-instance v0, Lgsq;

    invoke-direct {v0, p0}, Lgsq;-><init>(Lgsp;)V

    iput-object v0, p0, Lgsp;->p:Ljava/lang/Runnable;

    .line 83
    iput-object p1, p0, Lgsp;->a:Landroid/content/Context;

    .line 84
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgsp;->c:Landroid/view/ViewGroup;

    .line 85
    iput-object p2, p0, Lgsp;->b:Landroid/content/SharedPreferences;

    .line 86
    const-string v0, "upload_video_edit_tutorial_views_remaining"

    const/4 v1, 0x3

    invoke-interface {p2, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgsp;->i:I

    .line 88
    new-instance v0, Landroid/widget/Scroller;

    iget-object v1, p0, Lgsp;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    .line 89
    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 220
    iget v0, p0, Lgsp;->l:I

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lgsp;->f:Landroid/view/ViewGroup;

    iget-object v3, p0, Lgsp;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 222
    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v3

    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    iget-wide v6, p0, Lgsp;->n:J

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    invoke-direct {p0, v1, v3, v4}, Lgsp;->a(III)V

    iget-object v0, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    const-string v3, "alpha"

    new-array v1, v1, [I

    aput v2, v1, v2

    invoke-static {v0, v3, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Lgsu;

    iget-object v3, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    invoke-direct {v1, p0, v3}, Lgsu;-><init>(Lgsp;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    const/4 v0, 0x0

    iput-object v0, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    iput-wide v8, p0, Lgsp;->n:J

    .line 223
    iput v2, p0, Lgsp;->l:I

    .line 225
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 222
    goto :goto_0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 195
    iput p1, p0, Lgsp;->i:I

    .line 196
    iget-object v0, p0, Lgsp;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "upload_video_edit_tutorial_views_remaining"

    iget v2, p0, Lgsp;->i:I

    .line 197
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 198
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 199
    iget v0, p0, Lgsp;->i:I

    if-nez v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lgsp;->e()V

    .line 202
    :cond_0
    return-void
.end method

.method private a(II)V
    .locals 4

    .prologue
    .line 289
    iget-wide v0, p0, Lgsp;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 290
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2}, Lgsp;->a(III)V

    .line 291
    return-void

    .line 289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(III)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 323
    iget-wide v0, p0, Lgsp;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 324
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 325
    iget-object v8, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    iget-wide v0, p0, Lgsp;->n:J

    int-to-float v5, p2

    int-to-float v6, p3

    move v4, p1

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 326
    invoke-direct {p0, p2, p3}, Lgsp;->c(II)V

    .line 327
    return-void

    :cond_0
    move v0, v7

    .line 323
    goto :goto_0
.end method

.method static synthetic a(Lgsp;)V
    .locals 14

    .prologue
    const/4 v8, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 35
    const/16 v7, 0xa

    iget v0, p0, Lgsp;->l:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lgsp;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getLeft()I

    move-result v1

    const v2, 0x7f0a0051

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getBottom()I

    move-result v2

    const v3, 0x7f0a0052

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getWidth()I

    move-result v3

    const v5, 0x7f0c0002

    invoke-virtual {v0, v5, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    float-to-int v3, v0

    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    const/16 v5, 0xbb8

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    iget-wide v8, p0, Lgsp;->n:J

    const-wide/16 v10, -0x1

    cmp-long v0, v8, v10

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lgsp;->n:J

    invoke-direct {p0, v4, v1, v2}, Lgsp;->a(III)V

    invoke-direct {p0, v1, v2}, Lgsp;->b(II)V

    iget-object v0, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    const-string v1, "alpha"

    new-array v2, v12, [I

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iput v6, p0, Lgsp;->l:I

    move v0, v7

    :goto_1
    iget-object v1, p0, Lgsp;->f:Landroid/view/ViewGroup;

    iget-object v2, p0, Lgsp;->p:Ljava/lang/Runnable;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_2
    return-void

    :cond_0
    move v0, v4

    goto :goto_0

    :cond_1
    iget v0, p0, Lgsp;->l:I

    if-ne v0, v6, :cond_3

    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lgsp;->a(II)V

    move v0, v7

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lgsp;->h:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setVisibility(I)V

    iget-object v0, p0, Lgsp;->h:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setAlpha(F)V

    iget-object v0, p0, Lgsp;->h:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    iput v12, p0, Lgsp;->l:I

    const/16 v0, 0x3e8

    goto :goto_1

    :cond_3
    iget v0, p0, Lgsp;->l:I

    if-ne v0, v12, :cond_5

    iget-object v0, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(I)V

    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    iget-wide v8, p0, Lgsp;->n:J

    const-wide/16 v10, -0x1

    cmp-long v0, v8, v10

    if-eqz v0, :cond_4

    move v0, v6

    :goto_3
    invoke-static {v0}, Lb;->c(Z)V

    invoke-direct {p0, v1, v2}, Lgsp;->a(II)V

    iget-object v0, p0, Lgsp;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a0053

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v3, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v3

    neg-int v5, v0

    neg-int v0, v0

    invoke-virtual {v3, v5, v0}, Landroid/graphics/Rect;->inset(II)V

    iget-object v0, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    const-string v5, "bounds"

    new-instance v7, Lgst;

    invoke-direct {v7}, Lgst;-><init>()V

    new-array v8, v6, [Ljava/lang/Object;

    aput-object v3, v8, v4

    invoke-static {v0, v5, v7, v8}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v0, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    const-string v3, "alpha"

    new-array v5, v6, [I

    aput v4, v5, v4

    invoke-static {v0, v3, v5}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v3, Lgsu;

    iget-object v4, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    invoke-direct {v3, p0, v4}, Lgsu;-><init>(Lgsp;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    invoke-direct {p0, v1, v2}, Lgsp;->b(II)V

    iput v13, p0, Lgsp;->l:I

    const/16 v0, 0x3e8

    goto/16 :goto_1

    :cond_4
    move v0, v4

    goto :goto_3

    :cond_5
    iget v0, p0, Lgsp;->l:I

    if-ne v0, v13, :cond_6

    iget-object v0, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(I)V

    iget-object v0, p0, Lgsp;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getWidth()I

    move-result v1

    const v2, 0x7f0c0003

    invoke-virtual {v0, v2, v1, v1}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    float-to-int v3, v0

    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    iget-object v1, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    iget-object v2, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    const/16 v5, 0x7d0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    iput v8, p0, Lgsp;->l:I

    move v0, v7

    goto/16 :goto_1

    :cond_6
    iget v0, p0, Lgsp;->l:I

    if-ne v0, v8, :cond_8

    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lgsp;->m:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lgsp;->a(II)V

    move v0, v7

    goto/16 :goto_1

    :cond_7
    invoke-direct {p0}, Lgsp;->a()V

    goto/16 :goto_2

    :cond_8
    move v0, v7

    goto/16 :goto_1

    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method private b(II)V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lgsp;->a:Landroid/content/Context;

    const v1, 0x7f02029c

    .line 331
    invoke-static {v0, v1}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    .line 333
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 334
    iget-object v0, p0, Lgsp;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v0

    iget-object v1, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroupOverlay;->add(Landroid/graphics/drawable/Drawable;)V

    .line 336
    :cond_0
    invoke-direct {p0, p1, p2}, Lgsp;->c(II)V

    .line 337
    return-void
.end method

.method static synthetic b(Lgsp;)V
    .locals 4

    .prologue
    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lgsp;->l:I

    iget-object v0, p0, Lgsp;->f:Landroid/view/ViewGroup;

    iget-object v1, p0, Lgsp;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lgsp;->f:Landroid/view/ViewGroup;

    iget-object v1, p0, Lgsp;->p:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(I)V

    iget-object v0, p0, Lgsp;->h:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setVisibility(I)V

    return-void
.end method

.method static synthetic c(Lgsp;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private c(II)V
    .locals 7

    .prologue
    .line 343
    iget-object v0, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 345
    iget-object v1, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 346
    iget-object v2, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getLeft()I

    move-result v2

    add-int/2addr v2, p1

    .line 347
    iget-object v3, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->getTop()I

    move-result v3

    add-int/2addr v3, p2

    .line 348
    iget-object v4, p0, Lgsp;->o:Landroid/graphics/drawable/Drawable;

    sub-int v5, v2, v0

    sub-int v6, v3, v1

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 350
    :cond_0
    return-void
.end method

.method static synthetic d(Lgsp;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lgsp;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic e(Lgsp;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lgsp;->f:Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method public final a(Lgqo;Lgrw;)V
    .locals 1

    .prologue
    .line 97
    iput-object p1, p0, Lgsp;->j:Lgqo;

    .line 98
    iput-object p2, p0, Lgsp;->k:Lgrw;

    .line 99
    iget-object v0, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgqo;Lgrw;)V

    .line 102
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 108
    iget-object v2, p0, Lgsp;->j:Lgqo;

    if-eqz v2, :cond_1

    iget v2, p0, Lgsp;->i:I

    if-lez v2, :cond_1

    iget-object v2, p0, Lgsp;->b:Landroid/content/SharedPreferences;

    const-string v3, "upload_policy"

    .line 110
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    iget-object v2, p0, Lgsp;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 123
    iget-object v0, p0, Lgsp;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 124
    const v2, 0x7f040120

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    .line 126
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    const v2, 0x7f08020a

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iput-object v0, p0, Lgsp;->e:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 128
    iget-object v0, p0, Lgsp;->e:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    const v2, 0x7f080311

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgsp;->f:Landroid/view/ViewGroup;

    .line 130
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    const v2, 0x7f080312

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    iput-object v0, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    .line 131
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    const v2, 0x7f0800c5

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    .line 132
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    const v2, 0x7f080313

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iput-object v0, p0, Lgsp;->h:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 134
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 135
    iget-object v0, p0, Lgsp;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 137
    iget-object v0, p0, Lgsp;->g:Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;

    iget-object v2, p0, Lgsp;->j:Lgqo;

    iget-object v3, p0, Lgsp;->k:Lgrw;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/youtube/upload/edit/ui/VideoTrimView;->a(Lgqo;Lgrw;)V

    .line 140
    :cond_1
    iget-object v0, p0, Lgsp;->c:Landroid/view/ViewGroup;

    iget-object v2, p0, Lgsp;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lb;->c(Z)V

    .line 141
    iget-object v0, p0, Lgsp;->c:Landroid/view/ViewGroup;

    iget-object v2, p0, Lgsp;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 143
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 145
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 146
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    .line 147
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lgsr;

    invoke-direct {v1, p0}, Lgsr;-><init>(Lgsp;)V

    .line 148
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 140
    goto :goto_1
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 160
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    invoke-direct {p0}, Lgsp;->a()V

    .line 165
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 166
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    .line 167
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lgss;

    invoke-direct {v1, p0}, Lgss;-><init>(Lgsp;)V

    .line 168
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public abstract e()V
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lgsp;->e:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    if-ne p1, v0, :cond_1

    .line 183
    invoke-virtual {p0}, Lgsp;->d()V

    .line 184
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgsp;->a(I)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget-object v0, p0, Lgsp;->d:Landroid/view/ViewGroup;

    if-ne p1, v0, :cond_0

    .line 187
    invoke-virtual {p0}, Lgsp;->d()V

    .line 188
    iget v0, p0, Lgsp;->i:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lgsp;->a(I)V

    goto :goto_0
.end method
