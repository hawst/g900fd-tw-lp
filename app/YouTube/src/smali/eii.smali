.class public final Leii;
.super Ljava/lang/Object;

# interfaces
.implements Lehx;


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Leoa;

.field final c:Leip;

.field d:Leio;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Leii;->a:Ljava/lang/Object;

    new-instance v0, Leip;

    invoke-direct {v0, p0}, Leip;-><init>(Leii;)V

    iput-object v0, p0, Leii;->c:Leip;

    new-instance v0, Leij;

    invoke-direct {v0, p0}, Leij;-><init>(Leii;)V

    iput-object v0, p0, Leii;->b:Leoa;

    iget-object v0, p0, Leii;->b:Leoa;

    iget-object v1, p0, Leii;->c:Leip;

    iput-object v1, v0, Lenk;->c:Leoc;

    iget-object v1, v0, Lenk;->c:Leoc;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lenk;->d()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lejr;)Leju;
    .locals 2

    new-instance v0, Leil;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Leil;-><init>(Leii;Lejr;Lorg/json/JSONObject;)V

    invoke-interface {p1, v0}, Lejr;->b(Lejp;)Lejp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lejr;J)Leju;
    .locals 8

    new-instance v1, Lein;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v7}, Lein;-><init>(Leii;Lejr;JILorg/json/JSONObject;)V

    invoke-interface {p1, v1}, Lejr;->b(Lejp;)Lejp;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Leii;->b:Leoa;

    iget-object v0, v0, Lenk;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Leii;->b:Leoa;

    invoke-virtual {v0, p3}, Leoa;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Leio;)V
    .locals 0

    iput-object p1, p0, Leii;->d:Leio;

    return-void
.end method

.method public final b(Lejr;)Leju;
    .locals 2

    new-instance v0, Leim;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Leim;-><init>(Leii;Lejr;Lorg/json/JSONObject;)V

    invoke-interface {p1, v0}, Lejr;->b(Lejp;)Lejp;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lejr;)Leju;
    .locals 1

    new-instance v0, Leik;

    invoke-direct {v0, p0, p1}, Leik;-><init>(Leii;Lejr;)V

    invoke-interface {p1, v0}, Lejr;->b(Lejp;)Lejp;

    move-result-object v0

    return-object v0
.end method
