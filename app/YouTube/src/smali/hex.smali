.class public final Lhex;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhex;


# instance fields
.field public b:Lhxj;

.field private c:Lhoj;

.field private d:Lhkg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42304
    const/4 v0, 0x0

    new-array v0, v0, [Lhex;

    sput-object v0, Lhex;->a:[Lhex;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42305
    invoke-direct {p0}, Lidf;-><init>()V

    .line 42308
    iput-object v0, p0, Lhex;->c:Lhoj;

    .line 42311
    iput-object v0, p0, Lhex;->b:Lhxj;

    .line 42314
    iput-object v0, p0, Lhex;->d:Lhkg;

    .line 42305
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 42363
    const/4 v0, 0x0

    .line 42364
    iget-object v1, p0, Lhex;->c:Lhoj;

    if-eqz v1, :cond_0

    .line 42365
    const v0, 0x31a2ee9

    iget-object v1, p0, Lhex;->c:Lhoj;

    .line 42366
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42368
    :cond_0
    iget-object v1, p0, Lhex;->b:Lhxj;

    if-eqz v1, :cond_1

    .line 42369
    const v1, 0x4314c98

    iget-object v2, p0, Lhex;->b:Lhxj;

    .line 42370
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42372
    :cond_1
    iget-object v1, p0, Lhex;->d:Lhkg;

    if-eqz v1, :cond_2

    .line 42373
    const v1, 0x4a49488

    iget-object v2, p0, Lhex;->d:Lhkg;

    .line 42374
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42376
    :cond_2
    iget-object v1, p0, Lhex;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42377
    iput v0, p0, Lhex;->J:I

    .line 42378
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 42301
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhex;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhex;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhex;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhex;->c:Lhoj;

    if-nez v0, :cond_2

    new-instance v0, Lhoj;

    invoke-direct {v0}, Lhoj;-><init>()V

    iput-object v0, p0, Lhex;->c:Lhoj;

    :cond_2
    iget-object v0, p0, Lhex;->c:Lhoj;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhex;->b:Lhxj;

    if-nez v0, :cond_3

    new-instance v0, Lhxj;

    invoke-direct {v0}, Lhxj;-><init>()V

    iput-object v0, p0, Lhex;->b:Lhxj;

    :cond_3
    iget-object v0, p0, Lhex;->b:Lhxj;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhex;->d:Lhkg;

    if-nez v0, :cond_4

    new-instance v0, Lhkg;

    invoke-direct {v0}, Lhkg;-><init>()V

    iput-object v0, p0, Lhex;->d:Lhkg;

    :cond_4
    iget-object v0, p0, Lhex;->d:Lhkg;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18d1774a -> :sswitch_1
        0x218a64c2 -> :sswitch_2
        0x2524a442 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 42348
    iget-object v0, p0, Lhex;->c:Lhoj;

    if-eqz v0, :cond_0

    .line 42349
    const v0, 0x31a2ee9

    iget-object v1, p0, Lhex;->c:Lhoj;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 42351
    :cond_0
    iget-object v0, p0, Lhex;->b:Lhxj;

    if-eqz v0, :cond_1

    .line 42352
    const v0, 0x4314c98

    iget-object v1, p0, Lhex;->b:Lhxj;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 42354
    :cond_1
    iget-object v0, p0, Lhex;->d:Lhkg;

    if-eqz v0, :cond_2

    .line 42355
    const v0, 0x4a49488

    iget-object v1, p0, Lhex;->d:Lhkg;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 42357
    :cond_2
    iget-object v0, p0, Lhex;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 42359
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 42327
    if-ne p1, p0, :cond_1

    .line 42333
    :cond_0
    :goto_0
    return v0

    .line 42328
    :cond_1
    instance-of v2, p1, Lhex;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 42329
    :cond_2
    check-cast p1, Lhex;

    .line 42330
    iget-object v2, p0, Lhex;->c:Lhoj;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhex;->c:Lhoj;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhex;->b:Lhxj;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhex;->b:Lhxj;

    if-nez v2, :cond_3

    .line 42331
    :goto_2
    iget-object v2, p0, Lhex;->d:Lhkg;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhex;->d:Lhkg;

    if-nez v2, :cond_3

    .line 42332
    :goto_3
    iget-object v2, p0, Lhex;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhex;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 42333
    goto :goto_0

    .line 42330
    :cond_4
    iget-object v2, p0, Lhex;->c:Lhoj;

    iget-object v3, p1, Lhex;->c:Lhoj;

    invoke-virtual {v2, v3}, Lhoj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhex;->b:Lhxj;

    iget-object v3, p1, Lhex;->b:Lhxj;

    .line 42331
    invoke-virtual {v2, v3}, Lhxj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhex;->d:Lhkg;

    iget-object v3, p1, Lhex;->d:Lhkg;

    .line 42332
    invoke-virtual {v2, v3}, Lhkg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhex;->I:Ljava/util/List;

    iget-object v3, p1, Lhex;->I:Ljava/util/List;

    .line 42333
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 42337
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 42339
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhex;->c:Lhoj;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 42340
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhex;->b:Lhxj;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 42341
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhex;->d:Lhkg;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 42342
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhex;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 42343
    return v0

    .line 42339
    :cond_0
    iget-object v0, p0, Lhex;->c:Lhoj;

    invoke-virtual {v0}, Lhoj;->hashCode()I

    move-result v0

    goto :goto_0

    .line 42340
    :cond_1
    iget-object v0, p0, Lhex;->b:Lhxj;

    invoke-virtual {v0}, Lhxj;->hashCode()I

    move-result v0

    goto :goto_1

    .line 42341
    :cond_2
    iget-object v0, p0, Lhex;->d:Lhkg;

    invoke-virtual {v0}, Lhkg;->hashCode()I

    move-result v0

    goto :goto_2

    .line 42342
    :cond_3
    iget-object v1, p0, Lhex;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
