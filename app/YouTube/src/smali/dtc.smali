.class public final enum Ldtc;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum A:Ldtc;

.field public static final enum B:Ldtc;

.field private static enum C:Ldtc;

.field private static enum D:Ldtc;

.field private static enum E:Ldtc;

.field private static enum F:Ldtc;

.field private static enum G:Ldtc;

.field private static enum H:Ldtc;

.field private static I:Ljava/util/Map;

.field private static final synthetic K:[Ldtc;

.field public static final enum a:Ldtc;

.field public static final enum b:Ldtc;

.field public static final enum c:Ldtc;

.field public static final enum d:Ldtc;

.field public static final enum e:Ldtc;

.field public static final enum f:Ldtc;

.field public static final enum g:Ldtc;

.field public static final enum h:Ldtc;

.field public static final enum i:Ldtc;

.field public static final enum j:Ldtc;

.field public static final enum k:Ldtc;

.field public static final enum l:Ldtc;

.field public static final enum m:Ldtc;

.field public static final enum n:Ldtc;

.field public static final enum o:Ldtc;

.field public static final enum p:Ldtc;

.field public static final enum q:Ldtc;

.field public static final enum r:Ldtc;

.field public static final enum s:Ldtc;

.field public static final enum t:Ldtc;

.field public static final enum u:Ldtc;

.field public static final enum v:Ldtc;

.field public static final enum w:Ldtc;

.field public static final enum x:Ldtc;

.field public static final enum y:Ldtc;

.field public static final enum z:Ldtc;


# instance fields
.field private final J:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 21
    new-instance v1, Ldtc;

    const-string v2, "ADD_VIDEO"

    const-string v3, "addVideo"

    invoke-direct {v1, v2, v0, v3}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->a:Ldtc;

    .line 22
    new-instance v1, Ldtc;

    const-string v2, "ADD_VIDEOS"

    const-string v3, "addVideos"

    invoke-direct {v1, v2, v5, v3}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->b:Ldtc;

    .line 23
    new-instance v1, Ldtc;

    const-string v2, "AD_PLAYING"

    const-string v3, "adPlaying"

    invoke-direct {v1, v2, v6, v3}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->c:Ldtc;

    .line 24
    new-instance v1, Ldtc;

    const-string v2, "ON_AD_STATE_CHANGED"

    const-string v3, "onAdStateChange"

    invoke-direct {v1, v2, v7, v3}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->d:Ldtc;

    .line 25
    new-instance v1, Ldtc;

    const-string v2, "CLEAR_PLAYLIST"

    const-string v3, "clearPlaylist"

    invoke-direct {v1, v2, v8, v3}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->C:Ldtc;

    .line 26
    new-instance v1, Ldtc;

    const-string v2, "CONFIRM_PLAYLIST_UPDATE"

    const/4 v3, 0x5

    const-string v4, "confirmPlaylistUpdate"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->D:Ldtc;

    .line 27
    new-instance v1, Ldtc;

    const-string v2, "ON_ERROR"

    const/4 v3, 0x6

    const-string v4, "onError"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->e:Ldtc;

    .line 28
    new-instance v1, Ldtc;

    const-string v2, "LOUNGE_STATUS"

    const/4 v3, 0x7

    const-string v4, "loungeStatus"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->f:Ldtc;

    .line 29
    new-instance v1, Ldtc;

    const-string v2, "MOVE_VIDEO"

    const/16 v3, 0x8

    const-string v4, "moveVideo"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->E:Ldtc;

    .line 31
    new-instance v1, Ldtc;

    const-string v2, "NEXT"

    const/16 v3, 0x9

    const-string v4, "next"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->g:Ldtc;

    .line 32
    new-instance v1, Ldtc;

    const-string v2, "NOOP"

    const/16 v3, 0xa

    const-string v4, "noop"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->F:Ldtc;

    .line 33
    new-instance v1, Ldtc;

    const-string v2, "NOW_PLAYING"

    const/16 v3, 0xb

    const-string v4, "nowPlaying"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->h:Ldtc;

    .line 34
    new-instance v1, Ldtc;

    const-string v2, "NOW_PLAYING_PLAYLIST"

    const/16 v3, 0xc

    const-string v4, "nowPlayingPlaylist"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->i:Ldtc;

    .line 35
    new-instance v1, Ldtc;

    const-string v2, "ON_SUBTITLES_TRACK_CHANGED"

    const/16 v3, 0xd

    const-string v4, "onSubtitlesTrackChanged"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->j:Ldtc;

    .line 36
    new-instance v1, Ldtc;

    const-string v2, "PAUSE"

    const/16 v3, 0xe

    const-string v4, "pause"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->k:Ldtc;

    .line 37
    new-instance v1, Ldtc;

    const-string v2, "PLAY"

    const/16 v3, 0xf

    const-string v4, "play"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->l:Ldtc;

    .line 38
    new-instance v1, Ldtc;

    const-string v2, "PLAYLIST_MODIFIED"

    const/16 v3, 0x10

    const-string v4, "playlistModified"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->m:Ldtc;

    .line 39
    new-instance v1, Ldtc;

    const-string v2, "PREVIOUS"

    const/16 v3, 0x11

    const-string v4, "previous"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->n:Ldtc;

    .line 40
    new-instance v1, Ldtc;

    const-string v2, "REMOTE_CONNECTED"

    const/16 v3, 0x12

    const-string v4, "remoteConnected"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->o:Ldtc;

    .line 41
    new-instance v1, Ldtc;

    const-string v2, "REMOTE_DISCONNECTED"

    const/16 v3, 0x13

    const-string v4, "remoteDisconnected"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->p:Ldtc;

    .line 42
    new-instance v1, Ldtc;

    const-string v2, "REMOVE_VIDEO"

    const/16 v3, 0x14

    const-string v4, "removeVideo"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->q:Ldtc;

    .line 43
    new-instance v1, Ldtc;

    const-string v2, "SCREEN_CONNECTED"

    const/16 v3, 0x15

    const-string v4, "loungeScreenConnected"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->r:Ldtc;

    .line 44
    new-instance v1, Ldtc;

    const-string v2, "SCREEN_DISCONNECTED"

    const/16 v3, 0x16

    const-string v4, "loungeScreenDisconnected"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->s:Ldtc;

    .line 45
    new-instance v1, Ldtc;

    const-string v2, "SEEK_TO"

    const/16 v3, 0x17

    const-string v4, "seekTo"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->t:Ldtc;

    .line 46
    new-instance v1, Ldtc;

    const-string v2, "SET_PLAYLIST"

    const/16 v3, 0x18

    const-string v4, "setPlaylist"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->u:Ldtc;

    .line 47
    new-instance v1, Ldtc;

    const-string v2, "SET_SUBTITLES_TRACK"

    const/16 v3, 0x19

    const-string v4, "setSubtitlesTrack"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->v:Ldtc;

    .line 48
    new-instance v1, Ldtc;

    const-string v2, "SET_VIDEO"

    const/16 v3, 0x1a

    const-string v4, "setVideo"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->G:Ldtc;

    .line 49
    new-instance v1, Ldtc;

    const-string v2, "SET_VOLUME"

    const/16 v3, 0x1b

    const-string v4, "setVolume"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->w:Ldtc;

    .line 50
    new-instance v1, Ldtc;

    const-string v2, "ON_VOLUME_CHANGED"

    const/16 v3, 0x1c

    const-string v4, "onVolumeChanged"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->x:Ldtc;

    .line 51
    new-instance v1, Ldtc;

    const-string v2, "SHOW_QR_CODE"

    const/16 v3, 0x1d

    const-string v4, "showQrCode"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->H:Ldtc;

    .line 52
    new-instance v1, Ldtc;

    const-string v2, "SKIP_AD"

    const/16 v3, 0x1e

    const-string v4, "skipAd"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->y:Ldtc;

    .line 53
    new-instance v1, Ldtc;

    const-string v2, "ON_STATE_CHANGED"

    const/16 v3, 0x1f

    const-string v4, "onStateChange"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->z:Ldtc;

    .line 54
    new-instance v1, Ldtc;

    const-string v2, "STOP"

    const/16 v3, 0x20

    const-string v4, "stopVideo"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->A:Ldtc;

    .line 55
    new-instance v1, Ldtc;

    const-string v2, "UPDATE_USER"

    const/16 v3, 0x21

    const-string v4, "updateUser"

    invoke-direct {v1, v2, v3, v4}, Ldtc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Ldtc;->B:Ldtc;

    .line 19
    const/16 v1, 0x22

    new-array v1, v1, [Ldtc;

    sget-object v2, Ldtc;->a:Ldtc;

    aput-object v2, v1, v0

    sget-object v2, Ldtc;->b:Ldtc;

    aput-object v2, v1, v5

    sget-object v2, Ldtc;->c:Ldtc;

    aput-object v2, v1, v6

    sget-object v2, Ldtc;->d:Ldtc;

    aput-object v2, v1, v7

    sget-object v2, Ldtc;->C:Ldtc;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Ldtc;->D:Ldtc;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Ldtc;->e:Ldtc;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Ldtc;->f:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Ldtc;->E:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Ldtc;->g:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Ldtc;->F:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Ldtc;->h:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Ldtc;->i:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, Ldtc;->j:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    sget-object v3, Ldtc;->k:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    sget-object v3, Ldtc;->l:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    sget-object v3, Ldtc;->m:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    sget-object v3, Ldtc;->n:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    sget-object v3, Ldtc;->o:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    sget-object v3, Ldtc;->p:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    sget-object v3, Ldtc;->q:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x15

    sget-object v3, Ldtc;->r:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x16

    sget-object v3, Ldtc;->s:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    sget-object v3, Ldtc;->t:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x18

    sget-object v3, Ldtc;->u:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x19

    sget-object v3, Ldtc;->v:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    sget-object v3, Ldtc;->G:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    sget-object v3, Ldtc;->w:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    sget-object v3, Ldtc;->x:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    sget-object v3, Ldtc;->H:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    sget-object v3, Ldtc;->y:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    sget-object v3, Ldtc;->z:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x20

    sget-object v3, Ldtc;->A:Ldtc;

    aput-object v3, v1, v2

    const/16 v2, 0x21

    sget-object v3, Ldtc;->B:Ldtc;

    aput-object v3, v1, v2

    sput-object v1, Ldtc;->K:[Ldtc;

    .line 58
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Ldtc;->I:Ljava/util/Map;

    .line 61
    invoke-static {}, Ldtc;->values()[Ldtc;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 62
    sget-object v4, Ldtc;->I:Ljava/util/Map;

    iget-object v5, v3, Ldtc;->J:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldtc;->J:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public static a(Ljava/lang/String;)Ldtc;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Ldtc;->I:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldtc;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldtc;
    .locals 1

    .prologue
    .line 19
    const-class v0, Ldtc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldtc;

    return-object v0
.end method

.method public static values()[Ldtc;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Ldtc;->K:[Ldtc;

    invoke-virtual {v0}, [Ldtc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldtc;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ldtc;->J:Ljava/lang/String;

    return-object v0
.end method
