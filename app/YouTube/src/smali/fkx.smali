.class public Lfkx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfqh;


# instance fields
.field public final a:Lhnz;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:[Ljava/lang/CharSequence;

.field private final f:Lfqh;

.field private g:Lfnc;

.field private h:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhnz;Lfqh;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnz;

    iput-object v0, p0, Lfkx;->a:Lhnz;

    .line 30
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqh;

    iput-object v0, p0, Lfkx;->f:Lfqh;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Lfnc;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lfkx;->g:Lfnc;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfkx;->a:Lhnz;

    iget-object v1, v1, Lhnz;->a:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfkx;->g:Lfnc;

    .line 40
    :cond_0
    iget-object v0, p0, Lfkx;->g:Lfnc;

    return-object v0
.end method

.method public final a(Lfhz;)[Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lfkx;->h:[Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfkx;->a:Lhnz;

    iget-object v0, v0, Lhnz;->g:[Lhgz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfkx;->a:Lhnz;

    iget-object v0, v0, Lhnz;->g:[Lhgz;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 87
    iget-object v0, p0, Lfkx;->a:Lhnz;

    iget-object v0, v0, Lhnz;->g:[Lhgz;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lfkx;->h:[Ljava/lang/CharSequence;

    .line 88
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lfkx;->a:Lhnz;

    iget-object v1, v1, Lhnz;->g:[Lhgz;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 89
    iget-object v1, p0, Lfkx;->h:[Ljava/lang/CharSequence;

    iget-object v2, p0, Lfkx;->a:Lhnz;

    iget-object v2, v2, Lhnz;->g:[Lhgz;

    aget-object v2, v2, v0

    const/4 v3, 0x1

    .line 90
    invoke-static {v2, p1, v3}, Lfvo;->a(Lhgz;Lfhz;Z)Landroid/text/Spanned;

    move-result-object v2

    aput-object v2, v1, v0

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_0
    iget-object v0, p0, Lfkx;->h:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lfkx;->a:Lhnz;

    iget-object v0, v0, Lhnz;->e:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lfkx;->f:Lfqh;

    return-object v0
.end method
