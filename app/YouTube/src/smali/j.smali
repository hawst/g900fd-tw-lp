.class public Lj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/view/View$OnCreateContextMenuListener;


# static fields
.field private static final a:Ldx;

.field static final c:Ljava/lang/Object;


# instance fields
.field A:I

.field B:Ljava/lang/String;

.field C:Z

.field D:Z

.field E:Z

.field F:Z

.field G:Z

.field H:Z

.field I:I

.field J:Landroid/view/ViewGroup;

.field K:Landroid/view/View;

.field L:Landroid/view/View;

.field M:Z

.field N:Z

.field O:Lao;

.field P:Z

.field Q:Z

.field R:Ljava/lang/Object;

.field S:Ljava/lang/Object;

.field T:Ljava/lang/Object;

.field U:Lt;

.field V:Lt;

.field private W:Ljava/lang/Object;

.field private X:Ljava/lang/Object;

.field private b:Ljava/lang/Object;

.field d:I

.field e:Landroid/view/View;

.field f:I

.field g:Landroid/os/Bundle;

.field h:Landroid/util/SparseArray;

.field i:I

.field j:Ljava/lang/String;

.field public k:Landroid/os/Bundle;

.field l:Lj;

.field m:I

.field n:I

.field o:Z

.field public p:Z

.field q:Z

.field r:Z

.field s:Z

.field t:Z

.field u:I

.field v:Lv;

.field public w:Lo;

.field x:Lv;

.field y:Lj;

.field z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 168
    new-instance v0, Ldx;

    invoke-direct {v0}, Ldx;-><init>()V

    sput-object v0, Lj;->a:Ldx;

    .line 171
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lj;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    const/4 v0, 0x0

    iput v0, p0, Lj;->d:I

    .line 196
    iput v2, p0, Lj;->i:I

    .line 208
    iput v2, p0, Lj;->m:I

    .line 279
    iput-boolean v3, p0, Lj;->G:Z

    .line 301
    iput-boolean v3, p0, Lj;->N:Z

    .line 307
    iput-object v1, p0, Lj;->b:Ljava/lang/Object;

    .line 308
    sget-object v0, Lj;->c:Ljava/lang/Object;

    iput-object v0, p0, Lj;->R:Ljava/lang/Object;

    .line 309
    iput-object v1, p0, Lj;->W:Ljava/lang/Object;

    .line 310
    sget-object v0, Lj;->c:Ljava/lang/Object;

    iput-object v0, p0, Lj;->S:Ljava/lang/Object;

    .line 311
    iput-object v1, p0, Lj;->X:Ljava/lang/Object;

    .line 312
    sget-object v0, Lj;->c:Ljava/lang/Object;

    iput-object v0, p0, Lj;->T:Ljava/lang/Object;

    .line 316
    iput-object v1, p0, Lj;->U:Lt;

    .line 317
    iput-object v1, p0, Lj;->V:Lt;

    .line 388
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lj;
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lj;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lj;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lj;
    .locals 4

    .prologue
    .line 414
    :try_start_0
    sget-object v0, Lj;->a:Ldx;

    invoke-virtual {v0, p1}, Ldx;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 415
    if-nez v0, :cond_0

    .line 417
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 418
    sget-object v1, Lj;->a:Ldx;

    invoke-virtual {v1, p1, v0}, Ldx;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 421
    if-eqz p2, :cond_1

    .line 422
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 423
    iput-object p2, v0, Lj;->k:Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 425
    :cond_1
    return-object v0

    .line 426
    :catch_0
    move-exception v0

    .line 427
    new-instance v1, Ll;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ll;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 430
    :catch_1
    move-exception v0

    .line 431
    new-instance v1, Ll;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ll;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 434
    :catch_2
    move-exception v0

    .line 435
    new-instance v1, Ll;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ll;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 451
    :try_start_0
    sget-object v0, Lj;->a:Ldx;

    invoke-virtual {v0, p1}, Ldx;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 452
    if-nez v0, :cond_0

    .line 454
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 455
    sget-object v1, Lj;->a:Ldx;

    invoke-virtual {v1, p1, v0}, Ldx;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    :cond_0
    const-class v1, Lj;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 459
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o()V
    .locals 0

    .prologue
    .line 782
    return-void
.end method

.method public static q()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 997
    const/4 v0, 0x0

    return-object v0
.end method

.method public static r()V
    .locals 0

    .prologue
    .line 1053
    return-void
.end method

.method public static w()V
    .locals 0

    .prologue
    .line 1285
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1039
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 641
    invoke-virtual {p0}, Lj;->k()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 654
    invoke-virtual {p0}, Lj;->k()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final a(ILj;)V
    .locals 2

    .prologue
    .line 477
    iput p1, p0, Lj;->i:I

    .line 478
    if-eqz p2, :cond_0

    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Lj;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lj;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lj;->j:Ljava/lang/String;

    .line 483
    :goto_0
    return-void

    .line 481
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:fragment:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lj;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lj;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 990
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 991
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 893
    iget-object v0, p0, Lj;->w:Lo;

    if-nez v0, :cond_0

    .line 894
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 896
    :cond_0
    iget-object v0, p0, Lj;->w:Lo;

    const/4 v1, -0x1

    invoke-virtual {v0, p0, p1, v1}, Lo;->a(Lj;Landroid/content/Intent;I)V

    .line 897
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1015
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 1016
    return-void
.end method

.method public final a(Lj;I)V
    .locals 1

    .prologue
    .line 589
    iput-object p1, p0, Lj;->l:Lj;

    .line 590
    const/4 v0, 0x0

    iput v0, p0, Lj;->n:I

    .line 591
    return-void
.end method

.method public final a(Lm;)V
    .locals 2

    .prologue
    .line 570
    iget v0, p0, Lj;->i:I

    if-ltz v0, :cond_0

    .line 571
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 573
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lm;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lm;->a:Landroid/os/Bundle;

    :goto_0
    iput-object v0, p0, Lj;->g:Landroid/os/Bundle;

    .line 575
    return-void

    .line 573
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 3

    .prologue
    .line 933
    iget-object v0, p0, Lj;->w:Lo;

    invoke-virtual {v0}, Lo;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lj;->w:Lo;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 934
    iget-object v1, p0, Lj;->x:Lv;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lj;->x()V

    iget v1, p0, Lj;->d:I

    const/4 v2, 0x5

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lj;->x:Lv;

    invoke-virtual {v1}, Lv;->k()V

    :cond_0
    :goto_0
    iget-object v1, p0, Lj;->x:Lv;

    .line 935
    iget-object v1, p0, Lj;->x:Lv;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->setFactory(Landroid/view/LayoutInflater$Factory;)V

    .line 936
    return-object v0

    .line 934
    :cond_1
    iget v1, p0, Lj;->d:I

    const/4 v2, 0x4

    if-lt v1, v2, :cond_2

    iget-object v1, p0, Lj;->x:Lv;

    invoke-virtual {v1}, Lv;->j()V

    goto :goto_0

    :cond_2
    iget v1, p0, Lj;->d:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_3

    iget-object v1, p0, Lj;->x:Lv;

    invoke-virtual {v1}, Lv;->i()V

    goto :goto_0

    :cond_3
    iget v1, p0, Lj;->d:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lj;->x:Lv;

    invoke-virtual {v1}, Lv;->h()V

    goto :goto_0
.end method

.method final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1783
    iget-object v0, p0, Lj;->x:Lv;

    if-eqz v0, :cond_0

    .line 1784
    iget-object v0, p0, Lj;->x:Lv;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lv;->j:Z

    .line 1786
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lj;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b(II)V
    .locals 0

    .prologue
    .line 925
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 799
    iget-object v0, p0, Lj;->y:Lj;

    if-eqz v0, :cond_0

    .line 800
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t retain fragements that are nested in other fragments"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 803
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->E:Z

    .line 804
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 836
    iget-boolean v0, p0, Lj;->G:Z

    if-eq v0, p1, :cond_0

    .line 837
    iput-boolean p1, p0, Lj;->G:Z

    .line 838
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 1241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 1242
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1080
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 1081
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 858
    iget-boolean v0, p0, Lj;->N:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lj;->d:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 859
    iget-object v0, p0, Lj;->v:Lv;

    invoke-virtual {v0, p0}, Lv;->b(Lj;)V

    .line 861
    :cond_0
    iput-boolean p1, p0, Lj;->N:Z

    .line 862
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lj;->M:Z

    .line 863
    return-void

    .line 862
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1104
    iput-boolean v1, p0, Lj;->H:Z

    .line 1106
    iget-boolean v0, p0, Lj;->P:Z

    if-nez v0, :cond_1

    .line 1107
    iput-boolean v1, p0, Lj;->P:Z

    .line 1108
    iget-boolean v0, p0, Lj;->Q:Z

    if-nez v0, :cond_0

    .line 1109
    iput-boolean v1, p0, Lj;->Q:Z

    .line 1110
    iget-object v0, p0, Lj;->w:Lo;

    iget-object v1, p0, Lj;->j:Ljava/lang/String;

    iget-boolean v2, p0, Lj;->P:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lo;->a(Ljava/lang/String;ZZ)Lao;

    move-result-object v0

    iput-object v0, p0, Lj;->O:Lao;

    .line 1112
    :cond_0
    iget-object v0, p0, Lj;->O:Lao;

    if-eqz v0, :cond_1

    .line 1113
    iget-object v0, p0, Lj;->O:Lao;

    invoke-virtual {v0}, Lao;->b()V

    .line 1116
    :cond_1
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1148
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 493
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 1169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 1170
    return-void
.end method

.method public final f(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 547
    iget v0, p0, Lj;->i:I

    if-ltz v0, :cond_0

    .line 548
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 550
    :cond_0
    iput-object p1, p0, Lj;->k:Landroid/os/Bundle;

    .line 551
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 1186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 1187
    return-void
.end method

.method final g(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1933
    invoke-virtual {p0, p1}, Lj;->e(Landroid/os/Bundle;)V

    .line 1934
    iget-object v0, p0, Lj;->x:Lv;

    if-eqz v0, :cond_0

    .line 1935
    iget-object v0, p0, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->g()Landroid/os/Parcelable;

    move-result-object v0

    .line 1936
    if-eqz v0, :cond_0

    .line 1937
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1940
    :cond_0
    return-void
.end method

.method public final h()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lj;->k:Landroid/os/Bundle;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 500
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Lj;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lj;->l:Lj;

    return-object v0
.end method

.method public final j()Lo;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lj;->w:Lo;

    return-object v0
.end method

.method public final k()Landroid/content/res/Resources;
    .locals 3

    .prologue
    .line 618
    iget-object v0, p0, Lj;->w:Lo;

    if-nez v0, :cond_0

    .line 619
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 621
    :cond_0
    iget-object v0, p0, Lj;->w:Lo;

    invoke-virtual {v0}, Lo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lt;
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lj;->v:Lv;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lj;->w:Lo;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lj;->o:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 740
    iget-boolean v0, p0, Lj;->q:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 1152
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 1337
    iget-object v0, p0, Lj;->w:Lo;

    invoke-virtual {v0, p1, p2, p3}, Lo;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1338
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 1173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 1174
    return-void
.end method

.method public final p()Lam;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 877
    iget-object v0, p0, Lj;->O:Lao;

    if-eqz v0, :cond_0

    .line 878
    iget-object v0, p0, Lj;->O:Lao;

    .line 885
    :goto_0
    return-object v0

    .line 880
    :cond_0
    iget-object v0, p0, Lj;->w:Lo;

    if-nez v0, :cond_1

    .line 881
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 883
    :cond_1
    iput-boolean v3, p0, Lj;->Q:Z

    .line 884
    iget-object v0, p0, Lj;->w:Lo;

    iget-object v1, p0, Lj;->j:Ljava/lang/String;

    iget-boolean v2, p0, Lj;->P:Z

    invoke-virtual {v0, v1, v2, v3}, Lo;->a(Ljava/lang/String;ZZ)Lao;

    move-result-object v0

    iput-object v0, p0, Lj;->O:Lao;

    .line 885
    iget-object v0, p0, Lj;->O:Lao;

    goto :goto_0
.end method

.method public final s()Landroid/view/View;
    .locals 1

    .prologue
    .line 1063
    iget-object v0, p0, Lj;->K:Landroid/view/View;

    return-object v0
.end method

.method public t()V
    .locals 1

    .prologue
    .line 1125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 1126
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 505
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 506
    invoke-static {p0, v0}, La;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 507
    iget v1, p0, Lj;->i:I

    if-ltz v1, :cond_0

    .line 508
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    iget v1, p0, Lj;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 511
    :cond_0
    iget v1, p0, Lj;->z:I

    if-eqz v1, :cond_1

    .line 512
    const-string v1, " id=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    iget v1, p0, Lj;->z:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    :cond_1
    iget-object v1, p0, Lj;->B:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 516
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    iget-object v1, p0, Lj;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 519
    :cond_2
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 520
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()V
    .locals 1

    .prologue
    .line 1160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->H:Z

    .line 1161
    return-void
.end method

.method public v()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1194
    iput-boolean v1, p0, Lj;->H:Z

    .line 1197
    iget-boolean v0, p0, Lj;->Q:Z

    if-nez v0, :cond_0

    .line 1198
    iput-boolean v1, p0, Lj;->Q:Z

    .line 1199
    iget-object v0, p0, Lj;->w:Lo;

    iget-object v1, p0, Lj;->j:Ljava/lang/String;

    iget-boolean v2, p0, Lj;->P:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lo;->a(Ljava/lang/String;ZZ)Lao;

    move-result-object v0

    iput-object v0, p0, Lj;->O:Lao;

    .line 1201
    :cond_0
    iget-object v0, p0, Lj;->O:Lao;

    if-eqz v0, :cond_1

    .line 1202
    iget-object v0, p0, Lj;->O:Lao;

    invoke-virtual {v0}, Lao;->g()V

    .line 1204
    :cond_1
    return-void
.end method

.method final x()V
    .locals 3

    .prologue
    .line 1741
    new-instance v0, Lv;

    invoke-direct {v0}, Lv;-><init>()V

    iput-object v0, p0, Lj;->x:Lv;

    .line 1742
    iget-object v0, p0, Lj;->x:Lv;

    iget-object v1, p0, Lj;->w:Lo;

    new-instance v2, Lk;

    invoke-direct {v2, p0}, Lk;-><init>(Lj;)V

    invoke-virtual {v0, v1, v2, p0}, Lv;->a(Lo;Ls;Lj;)V

    .line 1756
    return-void
.end method

.method final y()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1967
    iget-object v0, p0, Lj;->x:Lv;

    if-eqz v0, :cond_0

    .line 1968
    iget-object v0, p0, Lj;->x:Lv;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Lv;->a(IZ)V

    .line 1970
    :cond_0
    iget-boolean v0, p0, Lj;->P:Z

    if-eqz v0, :cond_2

    .line 1971
    iput-boolean v3, p0, Lj;->P:Z

    .line 1972
    iget-boolean v0, p0, Lj;->Q:Z

    if-nez v0, :cond_1

    .line 1973
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj;->Q:Z

    .line 1974
    iget-object v0, p0, Lj;->w:Lo;

    iget-object v1, p0, Lj;->j:Ljava/lang/String;

    iget-boolean v2, p0, Lj;->P:Z

    invoke-virtual {v0, v1, v2, v3}, Lo;->a(Ljava/lang/String;ZZ)Lao;

    move-result-object v0

    iput-object v0, p0, Lj;->O:Lao;

    .line 1976
    :cond_1
    iget-object v0, p0, Lj;->O:Lao;

    if-eqz v0, :cond_2

    .line 1977
    iget-object v0, p0, Lj;->w:Lo;

    iget-boolean v0, v0, Lo;->d:Z

    if-nez v0, :cond_3

    .line 1978
    iget-object v0, p0, Lj;->O:Lao;

    invoke-virtual {v0}, Lao;->c()V

    .line 1984
    :cond_2
    :goto_0
    return-void

    .line 1980
    :cond_3
    iget-object v0, p0, Lj;->O:Lao;

    invoke-virtual {v0}, Lao;->d()V

    goto :goto_0
.end method
