.class public final Lcgo;
.super Lfsa;
.source "SourceFile"


# instance fields
.field final a:Lbty;

.field final b:Lbue;

.field final c:Landroid/view/View;

.field final d:Lgnd;

.field e:Lgbu;

.field final f:Lcft;

.field final g:Lbjq;

.field h:Lflv;

.field i:Lhog;

.field private final j:Lbhz;

.field private final m:Landroid/view/View;

.field private final n:Lfvi;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/view/View;

.field private final s:Landroid/widget/ImageView;

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/widget/ImageView;

.field private final v:Lcgw;

.field private final w:Lfxe;

.field private final x:Lbrh;


# direct methods
.method public constructor <init>(Lbhz;ILgix;Lcub;Lfeb;Leyt;Levn;Lfxe;Leyp;Lfhz;Lgnd;Lbjq;Lglm;Lfdw;Lfrz;)V
    .locals 10

    .prologue
    .line 114
    move-object/from16 v0, p14

    move-object/from16 v1, p15

    invoke-direct {p0, v0, v1}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 115
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbhz;

    iput-object v2, p0, Lcgo;->j:Lbhz;

    .line 116
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfxe;

    iput-object v2, p0, Lcgo;->w:Lfxe;

    .line 118
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgnd;

    iput-object v2, p0, Lcgo;->d:Lgnd;

    .line 119
    move-object/from16 v0, p12

    iput-object v0, p0, Lcgo;->g:Lbjq;

    .line 121
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 122
    const/4 v3, 0x0

    invoke-virtual {v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcgo;->m:Landroid/view/View;

    .line 124
    iget-object v2, p0, Lcgo;->m:Landroid/view/View;

    const v3, 0x7f08026a

    .line 125
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 126
    if-eqz v2, :cond_0

    new-instance v3, Lbue;

    invoke-direct {v3, v2}, Lbue;-><init>(Landroid/widget/ImageView;)V

    move-object v2, v3

    :goto_0
    iput-object v2, p0, Lcgo;->b:Lbue;

    .line 129
    iget-object v2, p0, Lcgo;->m:Landroid/view/View;

    const v3, 0x7f08012c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcgo;->c:Landroid/view/View;

    .line 130
    iget-object v2, p0, Lcgo;->c:Landroid/view/View;

    if-eqz v2, :cond_1

    new-instance v3, Lfvi;

    iget-object v2, p0, Lcgo;->c:Landroid/view/View;

    const v4, 0x7f0800a4

    .line 133
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, p9

    invoke-direct {v3, v0, v2}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    move-object v2, v3

    :goto_1
    iput-object v2, p0, Lcgo;->n:Lfvi;

    .line 135
    iget-object v2, p0, Lcgo;->m:Landroid/view/View;

    const v3, 0x7f08026e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcgo;->o:Landroid/widget/TextView;

    .line 136
    iget-object v2, p0, Lcgo;->m:Landroid/view/View;

    const v3, 0x7f080270

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcgo;->p:Landroid/widget/TextView;

    .line 137
    iget-object v2, p0, Lcgo;->m:Landroid/view/View;

    const v3, 0x7f080271

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcgo;->q:Landroid/widget/TextView;

    .line 138
    iget-object v2, p0, Lcgo;->m:Landroid/view/View;

    const v3, 0x7f08026f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcgo;->r:Landroid/view/View;

    .line 139
    iget-object v2, p0, Lcgo;->m:Landroid/view/View;

    const v3, 0x7f080272

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcgo;->s:Landroid/widget/ImageView;

    .line 140
    iget-object v2, p0, Lcgo;->m:Landroid/view/View;

    const v3, 0x7f080273

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcgo;->t:Landroid/widget/ImageView;

    .line 141
    iget-object v2, p0, Lcgo;->m:Landroid/view/View;

    const v3, 0x7f08026d

    .line 142
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcgo;->u:Landroid/widget/ImageView;

    .line 144
    new-instance v2, Lcgw;

    invoke-direct {v2, p0}, Lcgw;-><init>(Lcgo;)V

    iput-object v2, p0, Lcgo;->v:Lcgw;

    .line 146
    new-instance v2, Lbrh;

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p13

    invoke-direct/range {v2 .. v9}, Lbrh;-><init>(Landroid/app/Activity;Lgix;Lcub;Lfeb;Leyt;Levn;Lglm;)V

    iput-object v2, p0, Lcgo;->x:Lbrh;

    .line 154
    iget-object v2, p0, Lcgo;->x:Lbrh;

    iget-object v3, p0, Lcgo;->m:Landroid/view/View;

    invoke-virtual {v2, v3}, Lbrh;->b(Landroid/view/View;)V

    .line 156
    new-instance v2, Lbty;

    .line 158
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lgix;

    .line 159
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcub;

    .line 160
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lfeb;

    .line 161
    invoke-static/range {p6 .. p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Leyt;

    .line 162
    invoke-static/range {p7 .. p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Levn;

    move-object v3, p1

    invoke-direct/range {v2 .. v8}, Lbty;-><init>(Landroid/app/Activity;Lgix;Lcub;Lfeb;Leyt;Levn;)V

    iput-object v2, p0, Lcgo;->a:Lbty;

    .line 164
    iget-object v2, p0, Lcgo;->r:Landroid/view/View;

    new-instance v3, Lcgp;

    move-object/from16 v0, p10

    invoke-direct {v3, p0, v0}, Lcgp;-><init>(Lcgo;Lfhz;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v2, p0, Lcgo;->s:Landroid/widget/ImageView;

    new-instance v3, Lcgq;

    invoke-direct {v3, p0, p1}, Lcgq;-><init>(Lcgo;Lbhz;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v2, p0, Lcgo;->t:Landroid/widget/ImageView;

    new-instance v3, Lcgr;

    invoke-direct {v3, p0}, Lcgr;-><init>(Lcgo;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v2, p0, Lcgo;->g:Lbjq;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    new-instance v2, Lcgs;

    invoke-direct {v2, p0, p1}, Lcgs;-><init>(Lcgo;Lbhz;)V

    .line 237
    new-instance v3, Lcft;

    iget-object v4, p0, Lcgo;->m:Landroid/view/View;

    new-instance v5, Lcgt;

    move-object/from16 v0, p11

    invoke-direct {v5, p0, v0, v2}, Lcgt;-><init>(Lcgo;Lgnd;Lbjw;)V

    invoke-direct {v3, v4, v5}, Lcft;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v3, p0, Lcgo;->f:Lcft;

    .line 259
    move-object/from16 v0, p7

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 260
    return-void

    .line 126
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 133
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Lflv;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 264
    iget-object v0, p0, Lcgo;->h:Lflv;

    .line 265
    iput-object p1, p0, Lcgo;->h:Lflv;

    .line 267
    iget-object v4, p1, Lflv;->a:Lhsb;

    iget-object v4, v4, Lhsb;->a:Ljava/lang/String;

    .line 268
    if-eqz v0, :cond_0

    .line 269
    iget-object v0, v0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 270
    :cond_0
    iput-object v3, p0, Lcgo;->e:Lgbu;

    .line 271
    iget-object v0, p0, Lcgo;->d:Lgnd;

    invoke-interface {v0, v4}, Lgnd;->c(Ljava/lang/String;)Lglw;

    move-result-object v0

    .line 272
    if-eqz v0, :cond_4

    .line 273
    iget-object v0, p0, Lcgo;->f:Lcft;

    invoke-virtual {v0}, Lcft;->e()V

    .line 274
    iget-object v0, p0, Lcgo;->j:Lbhz;

    new-instance v5, Lcgv;

    invoke-direct {v5, p0, v4}, Lcgv;-><init>(Lcgo;Ljava/lang/String;)V

    .line 275
    invoke-static {v0, v5}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    .line 276
    iget-object v5, p0, Lcgo;->w:Lfxe;

    invoke-interface {v5, v4, v0}, Lfxe;->b(Ljava/lang/String;Leuc;)V

    .line 282
    :cond_1
    :goto_0
    iget-object v0, p0, Lcgo;->o:Landroid/widget/TextView;

    iget-object v4, p0, Lcgo;->h:Lflv;

    invoke-virtual {v4}, Lflv;->c()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v0, p0, Lcgo;->p:Landroid/widget/TextView;

    iget-object v4, p0, Lcgo;->h:Lflv;

    invoke-virtual {v4}, Lflv;->h()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v0, p0, Lcgo;->q:Landroid/widget/TextView;

    iget-object v4, p0, Lcgo;->h:Lflv;

    invoke-virtual {v4}, Lflv;->f()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    iget-object v0, p0, Lcgo;->h:Lflv;

    invoke-virtual {v0}, Lflv;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 287
    :goto_1
    iget-object v4, p0, Lcgo;->t:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcgo;->x:Lbrh;

    iget-object v4, p1, Lflv;->e:Lfkn;

    if-nez v4, :cond_2

    iget-object v4, p1, Lflv;->a:Lhsb;

    iget-object v4, v4, Lhsb;->k:Lhll;

    if-eqz v4, :cond_2

    iget-object v4, p1, Lflv;->a:Lhsb;

    iget-object v4, v4, Lhsb;->k:Lhll;

    iget-object v4, v4, Lhll;->a:Lhlk;

    if-eqz v4, :cond_2

    new-instance v4, Lfkn;

    iget-object v5, p1, Lflv;->a:Lhsb;

    iget-object v5, v5, Lhsb;->k:Lhll;

    iget-object v5, v5, Lhll;->a:Lhlk;

    invoke-direct {v4, v5}, Lfkn;-><init>(Lhlk;)V

    iput-object v4, p1, Lflv;->e:Lfkn;

    :cond_2
    iget-object v4, p1, Lflv;->e:Lfkn;

    invoke-virtual {v0, v4}, Lbrh;->a(Lfkn;)V

    .line 292
    iget-object v0, p0, Lcgo;->u:Landroid/widget/ImageView;

    .line 293
    iget-object v4, p1, Lflv;->a:Lhsb;

    iget v4, v4, Lhsb;->l:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    .line 292
    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcgo;->h:Lflv;

    iget-object v0, v0, Lflv;->f:Lflz;

    invoke-virtual {v0}, Lflz;->b()Lfma;

    move-result-object v0

    .line 297
    if-eqz v0, :cond_8

    .line 298
    invoke-virtual {v0}, Lfma;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 299
    invoke-virtual {v0}, Lfma;->d()Lfnc;

    move-result-object v0

    .line 309
    :goto_3
    iget-object v1, p0, Lcgo;->n:Lfvi;

    if-eqz v1, :cond_3

    .line 310
    iget-object v1, p0, Lcgo;->n:Lfvi;

    iget-object v2, p0, Lcgo;->v:Lcgw;

    invoke-virtual {v1, v0, v2}, Lfvi;->a(Lfnc;Leyo;)V

    .line 313
    :cond_3
    iget-object v0, p1, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->b:Lhog;

    iput-object v0, p0, Lcgo;->i:Lhog;

    .line 315
    iget-object v0, p0, Lcgo;->m:Landroid/view/View;

    return-object v0

    .line 278
    :cond_4
    invoke-virtual {p0, v0}, Lcgo;->a(Lglw;)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 286
    goto :goto_1

    :cond_6
    move v2, v1

    .line 293
    goto :goto_2

    .line 300
    :cond_7
    invoke-virtual {v0}, Lfma;->a()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 301
    invoke-virtual {v0}, Lfma;->c()Lfnc;

    move-result-object v0

    goto :goto_3

    .line 304
    :cond_8
    iget-object v0, p0, Lcgo;->h:Lflv;

    iget-object v0, v0, Lflv;->f:Lflz;

    invoke-virtual {v0}, Lflz;->a()Lfnc;

    move-result-object v0

    goto :goto_3

    :cond_9
    move-object v0, v3

    goto :goto_3
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 70
    check-cast p2, Lflv;

    invoke-virtual {p0, p2}, Lcgo;->a(Lflv;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 70
    check-cast p2, Lflv;

    invoke-virtual {p0, p2}, Lcgo;->a(Lflv;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method a(Lglw;)V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcgo;->f:Lcft;

    .line 380
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcgo;->e:Lgbu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgo;->e:Lgbu;

    .line 385
    invoke-virtual {p1, v0}, Lglw;->a(Lgbu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 386
    :goto_0
    if-eqz v0, :cond_1

    .line 387
    iget-object v0, p0, Lcgo;->f:Lcft;

    invoke-virtual {v0}, Lcft;->f()V

    .line 397
    :goto_1
    return-void

    .line 385
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 388
    :cond_1
    if-nez p1, :cond_3

    iget-object v0, p0, Lcgo;->h:Lflv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcgo;->h:Lflv;

    .line 390
    invoke-virtual {v0}, Lflv;->j()Lflh;

    move-result-object v0

    if-nez v0, :cond_3

    .line 393
    :cond_2
    iget-object v0, p0, Lcgo;->f:Lcft;

    invoke-virtual {v0}, Lcft;->a()V

    goto :goto_1

    .line 395
    :cond_3
    iget-object v0, p0, Lcgo;->f:Lcft;

    invoke-virtual {v0, p1}, Lcft;->a(Lglw;)Landroid/view/View;

    goto :goto_1
.end method

.method public final handleOfflinePlaylistAddEvent(Lblq;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 320
    iget-object v0, p1, Lblq;->a:Lglw;

    .line 321
    iget-object v1, p0, Lcgo;->h:Lflv;

    iget-object v1, v1, Lflv;->a:Lhsb;

    iget-object v1, v1, Lhsb;->a:Ljava/lang/String;

    .line 322
    iget-object v2, v0, Lglw;->a:Lgbu;

    iget-object v2, v2, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    invoke-virtual {p0, v0}, Lcgo;->a(Lglw;)V

    .line 325
    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistAddFailedEvent(Lblo;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 329
    iget-object v0, p0, Lcgo;->h:Lflv;

    iget-object v0, v0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->a:Ljava/lang/String;

    .line 330
    iget-object v1, p1, Lblo;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcgo;->a(Lglw;)V

    .line 333
    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistDeleteEvent(Lblp;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lcgo;->h:Lflv;

    iget-object v0, v0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->a:Ljava/lang/String;

    .line 347
    iget-object v1, p1, Lblp;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcgo;->a(Lglw;)V

    .line 350
    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistProgressEvent(Lblr;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 337
    iget-object v0, p1, Lblr;->a:Lglw;

    .line 338
    iget-object v1, p0, Lcgo;->h:Lflv;

    iget-object v1, v1, Lflv;->a:Lhsb;

    iget-object v1, v1, Lhsb;->a:Ljava/lang/String;

    .line 339
    iget-object v2, v0, Lglw;->a:Lgbu;

    iget-object v2, v2, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 340
    invoke-virtual {p0, v0}, Lcgo;->a(Lglw;)V

    .line 342
    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistSyncEvent(Lbls;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 354
    iget-object v0, p1, Lbls;->a:Lglw;

    .line 355
    iget-object v1, p0, Lcgo;->h:Lflv;

    iget-object v1, v1, Lflv;->a:Lhsb;

    iget-object v1, v1, Lhsb;->a:Ljava/lang/String;

    .line 356
    iget-object v2, v0, Lglw;->a:Lgbu;

    iget-object v2, v2, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 357
    invoke-virtual {p0, v0}, Lcgo;->a(Lglw;)V

    .line 359
    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistSyncFailedEvent(Lblt;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 363
    iget-object v0, p0, Lcgo;->h:Lflv;

    iget-object v0, v0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->a:Ljava/lang/String;

    .line 364
    iget-object v1, p1, Lblt;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, p0, Lcgo;->d:Lgnd;

    invoke-interface {v1, v0}, Lgnd;->c(Ljava/lang/String;)Lglw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcgo;->a(Lglw;)V

    .line 367
    :cond_0
    return-void
.end method

.method public final handlePlaylistLikeActionEvent(Lbui;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 371
    iget-object v0, p0, Lcgo;->h:Lflv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgo;->h:Lflv;

    .line 372
    iget-object v0, v0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->a:Ljava/lang/String;

    iget-object v1, p1, Lbui;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgo;->x:Lbrh;

    .line 374
    iget-object v0, p0, Lcgo;->x:Lbrh;

    iget-object v1, p1, Lbui;->b:Lbrg;

    invoke-virtual {v0, v1}, Lbrh;->a(Lbrg;)V

    .line 376
    :cond_0
    return-void
.end method
