.class public final Lcqy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;

.field private static final b:Ljava/util/Map;

.field private static final c:Lfba;


# instance fields
.field private final d:Lfbc;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    new-instance v0, Lcqz;

    invoke-direct {v0}, Lcqz;-><init>()V

    sput-object v0, Lcqy;->a:Ljava/util/Map;

    .line 39
    new-instance v0, Lcra;

    invoke-direct {v0}, Lcra;-><init>()V

    sput-object v0, Lcqy;->b:Ljava/util/Map;

    .line 45
    new-instance v0, Lfbb;

    invoke-direct {v0}, Lfbb;-><init>()V

    const-string v1, "/document"

    new-instance v2, Lcre;

    invoke-direct {v2}, Lcre;-><init>()V

    .line 46
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/document/question"

    new-instance v2, Lcrd;

    invoke-direct {v2}, Lcrd;-><init>()V

    .line 54
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/document/question/options"

    new-instance v2, Lcrc;

    invoke-direct {v2}, Lcrc;-><init>()V

    .line 93
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/document/question/additional_beacon_urls"

    new-instance v2, Lcrb;

    invoke-direct {v2}, Lcrb;-><init>()V

    .line 101
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lfbb;->a()Lfba;

    move-result-object v0

    sput-object v0, Lcqy;->c:Lfba;

    .line 45
    return-void
.end method

.method public constructor <init>(Lfbc;)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbc;

    iput-object v0, p0, Lcqy;->d:Lfbc;

    .line 115
    return-void
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcqy;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/Map;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcqy;->b:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lfoo;
    .locals 3

    .prologue
    .line 118
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 121
    :try_start_0
    iget-object v1, p0, Lcqy;->d:Lfbc;

    sget-object v2, Lcqy;->c:Lfba;

    invoke-virtual {v1, v0, v2}, Lfbc;->a(Ljava/io/InputStream;Lfba;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoq;

    .line 122
    invoke-virtual {v0}, Lfoq;->a()Lfoo;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    return-object v0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    new-instance v1, Lfax;

    invoke-direct {v1, v0}, Lfax;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 125
    :catch_1
    move-exception v0

    .line 126
    new-instance v1, Lfax;

    invoke-direct {v1, v0}, Lfax;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 127
    :catch_2
    move-exception v0

    .line 128
    new-instance v1, Lfax;

    invoke-direct {v1, v0}, Lfax;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
