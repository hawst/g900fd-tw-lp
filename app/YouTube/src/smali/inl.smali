.class final Linl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liql;


# instance fields
.field private final a:Lipe;

.field private final b:Liql;

.field private final c:Lu;

.field private final d:Liqk;

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>(Lipe;Lu;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 252
    iput-object p1, p0, Linl;->a:Lipe;

    .line 253
    iget-object v1, p1, Lipe;->f:Lipg;

    iput-object v1, p0, Linl;->b:Liql;

    .line 256
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lu;->j()Liqk;

    move-result-object v1

    .line 257
    :goto_0
    if-nez v1, :cond_0

    move-object p2, v0

    .line 261
    :cond_0
    iput-object v1, p0, Linl;->d:Liqk;

    .line 262
    iput-object p2, p0, Linl;->c:Lu;

    .line 263
    return-void

    :cond_1
    move-object v1, v0

    .line 256
    goto :goto_0
.end method

.method private b()Z
    .locals 6

    .prologue
    .line 310
    iget-object v0, p0, Linl;->a:Lipe;

    iget-object v0, v0, Lipe;->h:Liph;

    iget-wide v0, v0, Liqm;->b:J

    .line 311
    iget-object v2, p0, Linl;->a:Lipe;

    iget-object v2, v2, Lipe;->h:Liph;

    const-wide/16 v4, 0x64

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Liqm;->a(JLjava/util/concurrent/TimeUnit;)Liqm;

    .line 313
    const/16 v2, 0x64

    :try_start_0
    invoke-static {p0, v2}, Limo;->a(Liql;I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    iget-object v2, p0, Linl;->a:Lipe;

    iget-object v2, v2, Lipe;->h:Liph;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, v3}, Liqm;->a(JLjava/util/concurrent/TimeUnit;)Liqm;

    const/4 v0, 0x1

    .line 316
    :goto_0
    return v0

    :catch_0
    move-exception v2

    iget-object v2, p0, Linl;->a:Lipe;

    iget-object v2, v2, Lipe;->h:Liph;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, v3}, Liqm;->a(JLjava/util/concurrent/TimeUnit;)Liqm;

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Linl;->a:Lipe;

    iget-object v3, v3, Lipe;->h:Liph;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v0, v1, v4}, Liqm;->a(JLjava/util/concurrent/TimeUnit;)Liqm;

    throw v2
.end method


# virtual methods
.method public final a()Liqm;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Linl;->b:Liql;

    invoke-interface {v0}, Liql;->a()Liqm;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lipq;J)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 267
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_0
    iget-boolean v2, p0, Linl;->f:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_1
    iget-boolean v2, p0, Linl;->e:Z

    if-eqz v2, :cond_3

    .line 285
    :cond_2
    :goto_0
    return-wide v0

    .line 271
    :cond_3
    iget-object v2, p0, Linl;->b:Liql;

    invoke-interface {v2, p1, p2, p3}, Liql;->b(Lipq;J)J

    move-result-wide v2

    .line 272
    cmp-long v4, v2, v0

    if-nez v4, :cond_4

    .line 273
    const/4 v2, 0x1

    iput-boolean v2, p0, Linl;->e:Z

    .line 274
    iget-object v2, p0, Linl;->c:Lu;

    if-eqz v2, :cond_2

    .line 275
    iget-object v2, p0, Linl;->d:Liqk;

    invoke-interface {v2}, Liqk;->close()V

    goto :goto_0

    .line 280
    :cond_4
    iget-object v0, p0, Linl;->d:Liqk;

    if-eqz v0, :cond_5

    .line 282
    iget-object v0, p0, Linl;->d:Liqk;

    invoke-virtual {p1}, Lipq;->o()Lipq;

    move-result-object v1

    invoke-interface {v0, v1, v2, v3}, Liqk;->a(Lipq;J)V

    :cond_5
    move-wide v0, v2

    .line 285
    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 293
    iget-boolean v0, p0, Linl;->f:Z

    if-eqz v0, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    iget-boolean v0, p0, Linl;->e:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Linl;->d:Liqk;

    if-eqz v0, :cond_2

    .line 296
    invoke-direct {p0}, Linl;->b()Z

    .line 299
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Linl;->f:Z

    .line 301
    iget-boolean v0, p0, Linl;->e:Z

    if-nez v0, :cond_0

    .line 302
    iget-object v0, p0, Linl;->a:Lipe;

    sget-object v1, Linr;->h:Linr;

    invoke-virtual {v0, v1}, Lipe;->b(Linr;)V

    .line 303
    iget-object v0, p0, Linl;->c:Lu;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Linl;->c:Lu;

    goto :goto_0
.end method
