.class public final Lgyu;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5038
    invoke-direct {p0}, Lidf;-><init>()V

    .line 5041
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgyu;->a:Z

    .line 5038
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 5078
    const/4 v0, 0x0

    .line 5079
    iget-boolean v1, p0, Lgyu;->a:Z

    if-eqz v1, :cond_0

    .line 5080
    const/4 v0, 0x1

    iget-boolean v1, p0, Lgyu;->a:Z

    .line 5081
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 5083
    :cond_0
    iget-object v1, p0, Lgyu;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5084
    iput v0, p0, Lgyu;->J:I

    .line 5085
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 5034
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyu;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyu;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyu;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgyu;->a:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 5069
    iget-boolean v0, p0, Lgyu;->a:Z

    if-eqz v0, :cond_0

    .line 5070
    const/4 v0, 0x1

    iget-boolean v1, p0, Lgyu;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 5072
    :cond_0
    iget-object v0, p0, Lgyu;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 5074
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5052
    if-ne p1, p0, :cond_1

    .line 5056
    :cond_0
    :goto_0
    return v0

    .line 5053
    :cond_1
    instance-of v2, p1, Lgyu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 5054
    :cond_2
    check-cast p1, Lgyu;

    .line 5055
    iget-boolean v2, p0, Lgyu;->a:Z

    iget-boolean v3, p1, Lgyu;->a:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyu;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyu;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 5056
    goto :goto_0

    .line 5055
    :cond_4
    iget-object v2, p0, Lgyu;->I:Ljava/util/List;

    iget-object v3, p1, Lgyu;->I:Ljava/util/List;

    .line 5056
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5060
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5062
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lgyu;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 5063
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lgyu;->I:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 5064
    return v0

    .line 5062
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 5063
    :cond_1
    iget-object v0, p0, Lgyu;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_1
.end method
