.class public Lbdp;
.super Lbdx;
.source "SourceFile"

# interfaces
.implements Lfrz;


# instance fields
.field private X:Letc;

.field private Y:Lgnd;

.field private Z:Levn;

.field private a:Lari;

.field private aa:Leyp;

.field private ab:Lexd;

.field private ac:Lbjx;

.field private ad:Lbth;

.field private ae:Lbtc;

.field private af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

.field private ag:Landroid/widget/ProgressBar;

.field private ah:I

.field private ai:I

.field private aj:Z

.field private ak:Lbds;

.field private al:Lfqg;

.field private am:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lbdx;-><init>()V

    .line 385
    return-void
.end method

.method static synthetic a(Lbdp;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lbdp;->ah:I

    return v0
.end method

.method static synthetic a(Lbdp;I)I
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lbdp;->ah:I

    return p1
.end method

.method static synthetic b(Lbdp;I)I
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lbdp;->ai:I

    return p1
.end method

.method static synthetic b(Lbdp;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 51
    iget v0, p0, Lbdp;->ah:I

    if-eq v0, v2, :cond_1

    iget v0, p0, Lbdp;->ai:I

    if-eq v0, v2, :cond_1

    iget-boolean v0, p0, Lbdp;->aj:Z

    if-nez v0, :cond_0

    iget v0, p0, Lbdp;->ah:I

    if-nez v0, :cond_2

    iget v0, p0, Lbdp;->ai:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(IZ)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setVisibility(I)V

    iget-object v0, p0, Lbdp;->ag:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    iget v0, p0, Lbdp;->ah:I

    if-lez v0, :cond_0

    iget v0, p0, Lbdp;->ai:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(IZ)V

    goto :goto_0
.end method

.method static synthetic c(Lbdp;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lbdp;->ai:I

    return v0
.end method

.method static synthetic d(Lbdp;)Lbds;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lbdp;->ak:Lbds;

    return-object v0
.end method

.method static synthetic e(Lbdp;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lbdp;->Y:Lgnd;

    invoke-interface {v0}, Lgnd;->h()V

    return-void
.end method


# virtual methods
.method public final B()Lfqg;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lbdp;->al:Lfqg;

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 24

    .prologue
    .line 105
    const/16 v21, 0x0

    .line 106
    invoke-virtual/range {p0 .. p0}, Lbdp;->F()Lhog;

    move-result-object v2

    iget-object v2, v2, Lhog;->s:Lhpa;

    .line 107
    if-eqz v2, :cond_0

    iget-object v3, v2, Lhpa;->a:Lhpb;

    if-eqz v3, :cond_0

    .line 108
    new-instance v21, Lfjx;

    iget-object v2, v2, Lhpa;->a:Lhpb;

    iget-object v2, v2, Lhpb;->a:Lhhb;

    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-direct {v0, v2, v3}, Lfjx;-><init>(Lhhb;Lfqh;)V

    .line 111
    :cond_0
    const v2, 0x7f04009e

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v22

    .line 112
    const v2, 0x7f0800fe

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    .line 113
    const v2, 0x7f0800ed

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iput-object v2, v0, Lbdp;->ag:Landroid/widget/ProgressBar;

    .line 115
    move-object/from16 v0, p0

    iget-object v2, v0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    const v3, 0x7f090184

    const v4, 0x7f0400a3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(II)Landroid/view/View;

    move-result-object v23

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lbdp;->a:Lari;

    invoke-virtual {v2}, Lari;->aD()Lcst;

    move-result-object v4

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lbdp;->a:Lari;

    invoke-virtual {v2}, Lari;->O()Lgng;

    move-result-object v5

    .line 121
    invoke-interface {v4}, Lgix;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 123
    invoke-interface {v4}, Lgix;->d()Lgit;

    move-result-object v2

    invoke-virtual {v5, v2}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbdp;->Y:Lgnd;

    .line 129
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbdp;->a:Lari;

    invoke-virtual {v2}, Lari;->aR()Lcwg;

    move-result-object v16

    .line 130
    new-instance v2, Lcwj;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const/4 v6, 0x0

    invoke-direct {v2, v3, v6}, Lcwj;-><init>(Landroid/app/Activity;Lcwk;)V

    move-object/from16 v0, v16

    iput-object v2, v0, Lcwg;->d:Lcwj;

    .line 136
    new-instance v2, Lbjq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbdp;->a:Lari;

    .line 141
    invoke-virtual {v6}, Lari;->aO()Lcub;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lbdp;->a:Lari;

    .line 142
    invoke-virtual {v7}, Lari;->ay()Leyt;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lbdp;->ab:Lexd;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbdp;->ac:Lbjx;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 145
    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f()Lbrz;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 146
    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v11

    move-object/from16 v12, p0

    invoke-direct/range {v2 .. v12}, Lbjq;-><init>(Landroid/app/Activity;Lgix;Lgng;Lcub;Leyt;Lexd;Lbjx;Lbrz;Lbyg;Lfrz;)V

    .line 149
    new-instance v3, Lbth;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbdp;->a:Lari;

    .line 151
    invoke-virtual {v5}, Lari;->f()Larh;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 152
    iget-object v6, v6, Lbhz;->m:Lfus;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbdp;->Y:Lgnd;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 154
    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h()Lbka;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 155
    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 156
    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f()Lbrz;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lbdp;->Z:Levn;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbdp;->aa:Leyp;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbdp;->ab:Lexd;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbdp;->a:Lari;

    .line 160
    invoke-virtual {v14}, Lari;->o()Lglm;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lbdp;->ac:Lbjx;

    new-instance v17, Lbdq;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lbdq;-><init>(Lbdp;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v18, v0

    .line 174
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbdp;->a:Lari;

    move-object/from16 v19, v0

    .line 175
    invoke-virtual/range {v19 .. v19}, Lari;->ae()Lfdw;

    move-result-object v19

    move-object/from16 v20, p0

    invoke-direct/range {v3 .. v21}, Lbth;-><init>(Landroid/app/Activity;Lcyc;Lfus;Lgnd;Lbka;Lbyg;Lbrz;Levn;Leyp;Lexd;Lglm;Lbjx;Lcwg;Lbtj;Lfhz;Lfdw;Lfrz;Lfjx;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lbdp;->ad:Lbth;

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lbdp;->ad:Lbth;

    move-object/from16 v19, v0

    const v3, 0x7f0800fd

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, v19

    iput-object v3, v0, Lbth;->q:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const v3, 0x7f080175

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lbth;->r:Landroid/widget/ListView;

    new-instance v3, Lcgb;

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->a:Landroid/app/Activity;

    move-object/from16 v0, v19

    iget-object v5, v0, Lbth;->b:Lcyc;

    move-object/from16 v0, v19

    iget-object v6, v0, Lbth;->e:Levn;

    move-object/from16 v0, v19

    iget-object v7, v0, Lbth;->h:Lexd;

    move-object/from16 v0, v19

    iget-object v8, v0, Lbth;->i:Lglm;

    move-object/from16 v0, v19

    iget-object v9, v0, Lbth;->f:Lfus;

    move-object/from16 v0, v19

    iget-object v10, v0, Lbth;->k:Lcwg;

    move-object/from16 v0, v19

    iget-object v11, v0, Lbth;->o:Lbyg;

    move-object/from16 v0, v19

    iget-object v12, v0, Lbth;->p:Lbrz;

    move-object/from16 v0, v19

    iget-object v13, v0, Lbth;->d:Lbka;

    move-object/from16 v0, v19

    iget-object v14, v0, Lbth;->c:Lgnd;

    move-object/from16 v0, v19

    iget-object v15, v0, Lbth;->g:Leyp;

    move-object/from16 v0, v19

    iget-object v0, v0, Lbth;->j:Lbjx;

    move-object/from16 v16, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lbth;->m:Lfrz;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-direct/range {v3 .. v18}, Lcgb;-><init>(Landroid/content/Context;Lcyc;Levn;Lexd;Lglm;Lfus;Lcwg;Lbyg;Lbrz;Lbka;Lgnd;Leyp;Lbjx;Lfrz;Ljava/lang/String;)V

    new-instance v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, v19

    iget-object v5, v0, Lbth;->a:Landroid/app/Activity;

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v19

    iput-object v4, v0, Lbth;->t:Landroid/widget/LinearLayout;

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->t:Landroid/widget/LinearLayout;

    new-instance v5, Landroid/widget/AbsListView$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->t:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->r:Landroid/widget/ListView;

    move-object/from16 v0, v19

    iget-object v5, v0, Lbth;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    const v4, 0x7f04009d

    move-object/from16 v0, v19

    iget-object v5, v0, Lbth;->t:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    move-object/from16 v0, v19

    iput-object v4, v0, Lbth;->u:Landroid/view/View;

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->u:Landroid/view/View;

    const v5, 0x7f080225

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0901a4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->t:Landroid/widget/LinearLayout;

    move-object/from16 v0, v19

    iget-object v5, v0, Lbth;->u:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual/range {v19 .. v19}, Lbth;->b()V

    new-instance v4, Lfsi;

    invoke-direct {v4}, Lfsi;-><init>()V

    move-object/from16 v0, v19

    iput-object v4, v0, Lbth;->s:Lfsi;

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->s:Lfsi;

    const-class v5, Lgcd;

    invoke-virtual {v4, v5, v3}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    move-object/from16 v0, v19

    iget-object v3, v0, Lbth;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->a:Landroid/app/Activity;

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->s:Lfsi;

    goto :goto_1

    .line 125
    :cond_1
    invoke-virtual {v5}, Lgng;->c()Lgnd;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbdp;->Y:Lgnd;

    goto/16 :goto_0

    .line 179
    :cond_2
    move-object/from16 v0, v19

    iget-object v3, v0, Lbth;->s:Lfsi;

    new-instance v4, Lbti;

    move-object/from16 v0, v19

    invoke-direct {v4, v0}, Lbti;-><init>(Lbth;)V

    invoke-virtual {v3, v4}, Lfsi;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    move-object/from16 v0, v19

    iget-object v3, v0, Lbth;->r:Landroid/widget/ListView;

    move-object/from16 v0, v19

    iget-object v4, v0, Lbth;->s:Lfsi;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 181
    move-object/from16 v0, p0

    iget-object v3, v0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    const v4, 0x7f090185

    const v5, 0x7f0400a3

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(II)Landroid/view/View;

    move-result-object v3

    .line 185
    new-instance v7, Lbtc;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbdp;->a:Lari;

    .line 187
    invoke-virtual {v4}, Lari;->f()Larh;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lbdp;->Y:Lgnd;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbdp;->aa:Leyp;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbdp;->Z:Levn;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 192
    iget-object v14, v4, Lbhz;->m:Lfus;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbdp;->ab:Lexd;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbdp;->ac:Lbjx;

    move-object/from16 v16, v0

    new-instance v17, Lbdr;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lbdr;-><init>(Lbdp;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 206
    iget-object v0, v4, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lbdp;->a:Lari;

    .line 207
    invoke-virtual {v4}, Lari;->ae()Lfdw;

    move-result-object v19

    move-object v11, v2

    move-object/from16 v20, p0

    invoke-direct/range {v7 .. v21}, Lbtc;-><init>(Landroid/app/Activity;Lcyc;Lgnd;Lbjq;Leyp;Levn;Lfus;Lexd;Lbjx;Lbtf;Lfhz;Lfdw;Lfrz;Lfjx;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lbdp;->ae:Lbtc;

    .line 211
    move-object/from16 v0, p0

    iget-object v12, v0, Lbdp;->ae:Lbtc;

    new-instance v11, Lboi;

    iget-object v2, v12, Lbtc;->a:Landroid/app/Activity;

    invoke-direct {v11, v2}, Lboi;-><init>(Landroid/app/Activity;)V

    const v2, 0x7f09019a

    new-instance v4, Lbtd;

    invoke-direct {v4, v12}, Lbtd;-><init>(Lbtc;)V

    invoke-virtual {v11, v2, v4}, Lboi;->a(ILbop;)I

    const v2, 0x7f0800fd

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iput-object v2, v12, Lbtc;->k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const v2, 0x7f080175

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, v12, Lbtc;->l:Landroid/widget/ListView;

    new-instance v2, Lcfx;

    iget-object v3, v12, Lbtc;->a:Landroid/app/Activity;

    iget-object v4, v12, Lbtc;->b:Lcyc;

    iget-object v5, v12, Lbtc;->e:Leyp;

    iget-object v6, v12, Lbtc;->f:Levn;

    iget-object v7, v12, Lbtc;->c:Lgnd;

    iget-object v8, v12, Lbtc;->h:Lexd;

    iget-object v9, v12, Lbtc;->g:Lfus;

    iget-object v10, v12, Lbtc;->i:Lbjx;

    invoke-direct/range {v2 .. v11}, Lcfx;-><init>(Landroid/content/Context;Lcyc;Leyp;Levn;Lgnd;Lexd;Lfus;Lbjx;Lboi;)V

    new-instance v3, Landroid/widget/LinearLayout;

    iget-object v4, v12, Lbtc;->a:Landroid/app/Activity;

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, v12, Lbtc;->n:Landroid/widget/LinearLayout;

    iget-object v3, v12, Lbtc;->n:Landroid/widget/LinearLayout;

    new-instance v4, Landroid/widget/AbsListView$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, v12, Lbtc;->n:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v3, v12, Lbtc;->l:Landroid/widget/ListView;

    iget-object v4, v12, Lbtc;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    const v3, 0x7f04009d

    iget-object v4, v12, Lbtc;->n:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, v12, Lbtc;->o:Landroid/view/View;

    iget-object v3, v12, Lbtc;->o:Landroid/view/View;

    const v4, 0x7f080225

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0901a5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v3, v12, Lbtc;->n:Landroid/widget/LinearLayout;

    iget-object v4, v12, Lbtc;->o:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v12}, Lbtc;->b()V

    new-instance v3, Lfsi;

    invoke-direct {v3}, Lfsi;-><init>()V

    iput-object v3, v12, Lbtc;->m:Lfsi;

    iget-object v3, v12, Lbtc;->m:Lfsi;

    const-class v4, Lgbu;

    invoke-virtual {v3, v4, v2}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    iget-object v2, v12, Lbtc;->l:Landroid/widget/ListView;

    iget-object v3, v12, Lbtc;->m:Lfsi;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 213
    if-eqz p3, :cond_3

    .line 214
    const-string v2, "tab_index"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 215
    move-object/from16 v0, p0

    iget-object v3, v0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(I)V

    .line 218
    :cond_3
    return-object v22
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x7f090183

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 86
    invoke-super {p0, p1}, Lbdx;->a(Landroid/os/Bundle;)V

    .line 88
    invoke-virtual {p0}, Lbdx;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    iput-object v0, p0, Lbdp;->a:Lari;

    .line 89
    invoke-virtual {p0}, Lbdx;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    iget-object v0, v0, Lckz;->a:Letc;

    iput-object v0, p0, Lbdp;->X:Letc;

    .line 90
    iget-object v0, p0, Lbdp;->X:Letc;

    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    iput-object v0, p0, Lbdp;->Z:Levn;

    .line 91
    iget-object v0, p0, Lbdp;->a:Lari;

    invoke-virtual {v0}, Lari;->c()Leyp;

    move-result-object v0

    iput-object v0, p0, Lbdp;->aa:Leyp;

    .line 92
    iget-object v0, p0, Lbdp;->X:Letc;

    invoke-virtual {v0}, Letc;->b()Lexd;

    move-result-object v0

    iput-object v0, p0, Lbdp;->ab:Lexd;

    .line 93
    iget-object v0, p0, Lbdp;->a:Lari;

    invoke-virtual {v0}, Lari;->P()Lbjx;

    move-result-object v0

    iput-object v0, p0, Lbdp;->ac:Lbjx;

    .line 95
    new-instance v0, Lfqg;

    iget-object v1, p0, Lbdp;->X:Letc;

    .line 96
    invoke-virtual {v1}, Letc;->k()Lfac;

    move-result-object v1

    invoke-virtual {p0}, Lbdp;->F()Lhog;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lfqg;-><init>(Lfac;Lhog;)V

    iput-object v0, p0, Lbdp;->al:Lfqg;

    .line 97
    return-void
.end method

.method public final b()Lcan;
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lbdp;->W:Lcan;

    if-nez v0, :cond_0

    .line 305
    iget-object v0, p0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    invoke-virtual {v0}, Lcaq;->h()Lcar;

    move-result-object v0

    .line 306
    invoke-virtual {p0}, Lbdp;->a()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lcar;->a:Ljava/lang/CharSequence;

    new-instance v1, Lbdu;

    invoke-direct {v1, p0}, Lbdu;-><init>(Lbdp;)V

    .line 307
    invoke-virtual {v0, v1}, Lcar;->a(Lcam;)Lcar;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Lcar;->a()Lcaq;

    move-result-object v0

    iput-object v0, p0, Lbdp;->W:Lcan;

    .line 310
    :cond_0
    iget-object v0, p0, Lbdp;->W:Lcan;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 325
    const-string v0, "yt_android_offline"

    return-object v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 231
    invoke-super {p0}, Lbdx;->e()V

    .line 233
    iget-object v0, p0, Lbdp;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Lt;

    move-result-object v0

    .line 234
    const-string v1, "ClearOfflineDialogFragment"

    invoke-virtual {v0, v1}, Lt;->a(Ljava/lang/String;)Lj;

    move-result-object v0

    check-cast v0, Lbds;

    iput-object v0, p0, Lbdp;->ak:Lbds;

    .line 236
    iget-object v0, p0, Lbdp;->ak:Lbds;

    if-nez v0, :cond_0

    .line 237
    new-instance v0, Lbds;

    invoke-direct {v0}, Lbds;-><init>()V

    iput-object v0, p0, Lbdp;->ak:Lbds;

    .line 239
    :cond_0
    iget-object v0, p0, Lbdp;->ak:Lbds;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lbds;->a(Lbds;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;

    .line 241
    iget-object v0, p0, Lbdp;->Z:Levn;

    iget-object v1, p0, Lbdp;->ad:Lbth;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 242
    iget-object v0, p0, Lbdp;->Z:Levn;

    iget-object v1, p0, Lbdp;->ae:Lbtc;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 243
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 223
    invoke-super {p0, p1}, Lbdx;->e(Landroid/os/Bundle;)V

    .line 224
    iget-object v0, p0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    if-eqz v0, :cond_0

    .line 225
    const-string v0, "tab_index"

    iget-object v1, p0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget v1, v1, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 227
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 291
    invoke-super {p0}, Lbdx;->f()V

    .line 292
    iget-object v0, p0, Lbdp;->Z:Levn;

    iget-object v1, p0, Lbdp;->ad:Lbth;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 293
    iget-object v0, p0, Lbdp;->Z:Levn;

    iget-object v1, p0, Lbdp;->ae:Lbtc;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 294
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 298
    invoke-super {p0}, Lbdx;->g()V

    .line 299
    iget-object v0, p0, Lbdp;->ad:Lbth;

    iget-object v0, v0, Lbth;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 300
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 315
    invoke-super {p0, p1}, Lbdx;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 316
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lbdp;->am:I

    if-eq v0, v1, :cond_0

    .line 317
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lbdp;->am:I

    .line 318
    iget-object v0, p0, Lbdp;->ad:Lbth;

    invoke-virtual {v0}, Lbth;->b()V

    .line 319
    iget-object v0, p0, Lbdp;->ae:Lbtc;

    invoke-virtual {v0}, Lbtc;->b()V

    .line 321
    :cond_0
    return-void
.end method

.method public final t()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 247
    invoke-super {p0}, Lbdx;->t()V

    .line 248
    iget-object v0, p0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lbdp;->ag:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 250
    iput v2, p0, Lbdp;->ah:I

    .line 251
    iput v2, p0, Lbdp;->ai:I

    .line 253
    invoke-virtual {p0}, Lbdp;->h()Landroid/os/Bundle;

    move-result-object v0

    .line 254
    if-eqz v0, :cond_0

    const-string v1, "tab_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 255
    const-string v1, "tab_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 256
    iget-object v2, p0, Lbdp;->af:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v2, v1, v4}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(IZ)V

    .line 258
    const-string v1, "tab_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 259
    iput-boolean v4, p0, Lbdp;->aj:Z

    .line 264
    :goto_0
    iget-object v0, p0, Lbdp;->ad:Lbth;

    invoke-virtual {v0}, Lbth;->a()V

    .line 265
    iget-object v0, p0, Lbdp;->ae:Lbtc;

    invoke-virtual {v0}, Lbtc;->a()V

    .line 267
    invoke-virtual {p0}, Lbdp;->k()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lbdp;->am:I

    .line 269
    iget-object v0, p0, Lbdp;->a:Lari;

    .line 270
    iget-object v0, v0, Lari;->a:Lbca;

    const-class v1, Lbdv;

    .line 271
    invoke-virtual {v0, v1}, Levy;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 272
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 261
    :cond_0
    iput-boolean v3, p0, Lbdp;->aj:Z

    goto :goto_0

    .line 275
    :cond_1
    return-void
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 279
    invoke-super {p0}, Lbdx;->u()V

    .line 281
    iget-object v0, p0, Lbdp;->a:Lari;

    .line 282
    iget-object v0, v0, Lari;->a:Lbca;

    const-class v1, Lbdv;

    .line 283
    invoke-virtual {v0, v1}, Levy;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 284
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 287
    :cond_0
    return-void
.end method
