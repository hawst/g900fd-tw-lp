.class final Lbul;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Lbuk;


# direct methods
.method constructor <init>(Lbuk;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lbul;->a:Lbuk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 197
    iget-object v2, p0, Lbul;->a:Lbuk;

    invoke-static {v2}, Lbuk;->a(Lbuk;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 198
    iget-object v2, p0, Lbul;->a:Lbuk;

    invoke-static {v2}, Lbuk;->b(Lbuk;)Lcws;

    move-result-object v2

    iget-object v3, p0, Lbul;->a:Lbuk;

    invoke-static {v3}, Lbuk;->a(Lbuk;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->isSelected()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    iget-object v1, v2, Lcws;->g:Ldkf;

    invoke-interface {v1, v0}, Ldkf;->b(Z)V

    .line 221
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 198
    goto :goto_0

    .line 199
    :cond_2
    iget-object v2, p0, Lbul;->a:Lbuk;

    invoke-static {v2}, Lbuk;->c(Lbuk;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 200
    iget-object v2, p0, Lbul;->a:Lbuk;

    invoke-static {v2}, Lbuk;->b(Lbuk;)Lcws;

    move-result-object v2

    iget-object v3, p0, Lbul;->a:Lbuk;

    invoke-static {v3}, Lbuk;->c(Lbuk;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->isSelected()Z

    move-result v3

    if-nez v3, :cond_3

    :goto_2
    iget-object v1, v2, Lcws;->g:Ldkf;

    invoke-interface {v1, v0}, Ldkf;->a(Z)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 201
    :cond_4
    iget-object v0, p0, Lbul;->a:Lbuk;

    invoke-static {v0}, Lbuk;->d(Lbuk;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lbul;->a:Lbuk;

    invoke-static {v0}, Lbuk;->e(Lbuk;)Lflw;

    move-result-object v0

    if-nez v0, :cond_5

    .line 203
    const-string v0, "Share playlist error: null playlist panel"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 207
    :cond_5
    iget-object v0, p0, Lbul;->a:Lbuk;

    invoke-static {v0}, Lbuk;->e(Lbuk;)Lflw;

    move-result-object v0

    iget-object v0, v0, Lflw;->a:Lhsd;

    iget-object v1, v0, Lhsd;->a:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lbul;->a:Lbuk;

    invoke-static {v0}, Lbuk;->e(Lbuk;)Lflw;

    move-result-object v0

    iget-object v2, v0, Lflw;->a:Lhsd;

    iget-object v2, v2, Lhsd;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v0, 0x0

    .line 209
    :goto_3
    if-nez v0, :cond_8

    .line 211
    iget-object v0, p0, Lbul;->a:Lbuk;

    invoke-static {v0}, Lbuk;->e(Lbuk;)Lflw;

    move-result-object v0

    iget-object v0, v0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->d:Ljava/lang/String;

    .line 212
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 213
    const-string v0, "Share playlist error: no share url or playlist id"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 208
    :cond_6
    iget-object v0, v0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    .line 216
    :cond_7
    invoke-static {v0}, La;->J(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 219
    :cond_8
    iget-object v2, p0, Lbul;->a:Lbuk;

    invoke-static {v2}, Lbuk;->f(Lbuk;)Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-result-object v2

    invoke-static {v2, v1, v0}, Ldol;->b(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_1
.end method
