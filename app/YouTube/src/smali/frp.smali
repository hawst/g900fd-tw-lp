.class public final Lfrp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Leme;

.field final b:Ljava/lang/String;

.field public final c:Landroid/content/SharedPreferences;

.field final d:Lfem;

.field final e:Lfrr;

.field private final f:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Leme;Ljava/lang/String;Landroid/content/SharedPreferences;Lfem;Ljava/util/concurrent/Executor;Lfrr;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leme;

    iput-object v0, p0, Lfrp;->a:Leme;

    .line 40
    invoke-static {p2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfrp;->b:Ljava/lang/String;

    .line 41
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lfrp;->c:Landroid/content/SharedPreferences;

    .line 42
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfem;

    iput-object v0, p0, Lfrp;->d:Lfem;

    .line 43
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lfrp;->f:Ljava/util/concurrent/Executor;

    .line 44
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrr;

    iput-object v0, p0, Lfrp;->e:Lfrr;

    .line 45
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lfrp;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lfrq;

    invoke-direct {v1, p0}, Lfrq;-><init>(Lfrp;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 60
    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lfrp;->c:Landroid/content/SharedPreferences;

    const-string v1, "gcm_registration_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    const/4 v0, 0x0

    .line 175
    :cond_0
    return-object v0
.end method
