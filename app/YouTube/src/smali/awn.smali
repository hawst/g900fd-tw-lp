.class public Lawn;
.super Lcba;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:I

.field private final b:Lawo;

.field private final c:Landroid/content/Context;

.field private final e:Landroid/view/LayoutInflater;

.field private final f:Ljava/util/List;

.field private final g:Ljava/util/List;

.field private final h:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILawo;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 49
    invoke-direct {p0}, Lcba;-><init>()V

    .line 50
    iput p2, p0, Lawn;->a:I

    .line 51
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawo;

    iput-object v0, p0, Lawn;->b:Lawo;

    .line 52
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lawn;->c:Landroid/content/Context;

    .line 53
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lawn;->e:Landroid/view/LayoutInflater;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lawn;->f:Ljava/util/List;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lawn;->g:Ljava/util/List;

    .line 56
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lawn;->h:Landroid/util/SparseArray;

    .line 57
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Iterable;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcba;->a(Ljava/lang/Iterable;)V

    .line 62
    iget-object v0, p0, Lawn;->b:Lawo;

    invoke-interface {v0, p1}, Lawo;->a(Ljava/lang/Iterable;)V

    .line 63
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 199
    invoke-super {p0}, Lcba;->getCount()I

    move-result v0

    iget-object v1, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lawn;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 187
    iget-object v1, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-object v0

    .line 190
    :cond_1
    iget-object v1, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-super {p0}, Lcba;->getCount()I

    move-result v2

    sub-int/2addr v1, v2

    .line 191
    if-gez v1, :cond_0

    .line 194
    iget-object v0, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lcba;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 209
    iget-object v1, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 212
    :cond_1
    iget-object v1, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-super {p0}, Lcba;->getCount()I

    move-result v2

    sub-int/2addr v1, v2

    .line 213
    if-gez v1, :cond_0

    .line 216
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 135
    iget-object v0, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 136
    iget-object v0, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    invoke-interface {v0}, Lu;->c()Landroid/view/View;

    move-result-object v1

    .line 168
    :goto_0
    return-object v1

    .line 138
    :cond_0
    iget-object v0, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0}, Lcba;->getCount()I

    move-result v1

    sub-int/2addr v0, v1

    .line 139
    if-ltz v0, :cond_1

    .line 140
    iget-object v1, p0, Lawn;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    invoke-interface {v0}, Lu;->c()Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 149
    :cond_1
    iget-object v0, p0, Lawn;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 150
    if-eqz p2, :cond_3

    .line 151
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 152
    iget-object v1, p0, Lawn;->h:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    if-ltz v1, :cond_3

    .line 153
    iget-object v1, p0, Lawn;->h:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, v2, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_3

    .line 154
    const/4 p2, 0x0

    move-object v1, p2

    .line 159
    :goto_1
    if-nez v1, :cond_2

    .line 160
    iget-object v0, p0, Lawn;->e:Landroid/view/LayoutInflater;

    iget v1, p0, Lawn;->a:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 161
    iget-object v0, p0, Lawn;->b:Lawo;

    invoke-interface {v0, v1, p3}, Lawo;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lawm;

    move-result-object v0

    .line 162
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 163
    iget-object v3, p0, Lawn;->h:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v4

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 167
    :goto_2
    invoke-virtual {p0, p1}, Lawn;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Lawm;->a(ILjava/lang/Object;)Landroid/view/View;

    goto :goto_0

    .line 165
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawm;

    goto :goto_2

    :cond_3
    move-object v1, p2

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 123
    iget-object v0, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    invoke-interface {v0}, Lu;->d()Z

    move-result v0

    .line 129
    :goto_0
    return v0

    .line 125
    :cond_0
    iget-object v0, p0, Lawn;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0}, Lcba;->getCount()I

    move-result v1

    sub-int/2addr v0, v1

    .line 126
    if-ltz v0, :cond_1

    .line 127
    iget-object v1, p0, Lawn;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    invoke-interface {v0}, Lu;->d()Z

    move-result v0

    goto :goto_0

    .line 129
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
