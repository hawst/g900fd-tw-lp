.class public final Ldfn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ldfm;

.field b:Landroid/graphics/Bitmap;

.field private final c:Leyp;

.field private final d:Leuc;

.field private final e:Z

.field private f:Leue;

.field private g:Landroid/net/Uri;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Ldfm;Leyp;Z)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfm;

    iput-object v0, p0, Ldfn;->a:Ldfm;

    .line 50
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Ldfn;->c:Leyp;

    .line 51
    iput-boolean p3, p0, Ldfn;->e:Z

    .line 55
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Ldfo;

    invoke-direct {v1, p0}, Ldfo;-><init>(Ldfn;)V

    invoke-static {v0, v1}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v0

    iput-object v0, p0, Ldfn;->d:Leuc;

    .line 67
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    iput-object v2, p0, Ldfn;->g:Landroid/net/Uri;

    .line 78
    iput-object v2, p0, Ldfn;->b:Landroid/graphics/Bitmap;

    .line 79
    iget-object v0, p0, Ldfn;->f:Leue;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Ldfn;->f:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 81
    iput-object v2, p0, Ldfn;->f:Leue;

    .line 83
    :cond_0
    iget-object v0, p0, Ldfn;->a:Ldfm;

    invoke-interface {v0}, Ldfm;->c()V

    .line 84
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 132
    iget-boolean v0, p0, Ldfn;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldfn;->j:Z

    if-nez v0, :cond_2

    :cond_0
    iget-boolean v0, p0, Ldfn;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ldfn;->k:Z

    if-nez v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Ldfn;->i:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Ldfn;->e:Z

    if-eqz v0, :cond_4

    .line 135
    :cond_2
    iget-object v0, p0, Ldfn;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_3

    iget-object v0, p0, Ldfn;->g:Landroid/net/Uri;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldfn;->f:Leue;

    if-nez v0, :cond_3

    iget-object v0, p0, Ldfn;->d:Leuc;

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldfn;->f:Leue;

    iget-object v0, p0, Ldfn;->c:Leyp;

    iget-object v1, p0, Ldfn;->g:Landroid/net/Uri;

    iget-object v2, p0, Ldfn;->f:Leue;

    invoke-interface {v0, v1, v2}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    :cond_3
    iget-object v0, p0, Ldfn;->a:Ldfm;

    invoke-interface {v0}, Ldfm;->a()V

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_4
    iget-object v0, p0, Ldfn;->a:Ldfm;

    invoke-interface {v0}, Ldfm;->b()V

    goto :goto_0
.end method


# virtual methods
.method public final handleAudioOnlyEvent(Lcyw;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 127
    iget-boolean v0, p1, Lcyw;->a:Z

    iput-boolean v0, p0, Ldfn;->j:Z

    .line 128
    invoke-direct {p0}, Ldfn;->b()V

    .line 129
    return-void
.end method

.method public final handleVideoStageEvent(Ldac;)V
    .locals 7
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 93
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v4, Lgol;->a:Lgol;

    if-ne v0, v4, :cond_1

    .line 94
    invoke-direct {p0}, Ldfn;->a()V

    .line 116
    :cond_0
    :goto_0
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v3, Lgol;->b:Lgol;

    invoke-virtual {v0, v3}, Lgol;->a(Lgol;)Z

    move-result v0

    iput-boolean v0, p0, Ldfn;->h:Z

    .line 117
    iget-object v0, p1, Ldac;->a:Lgol;

    const/4 v3, 0x3

    new-array v3, v3, [Lgol;

    sget-object v4, Lgol;->b:Lgol;

    aput-object v4, v3, v2

    sget-object v4, Lgol;->g:Lgol;

    aput-object v4, v3, v1

    const/4 v4, 0x2

    sget-object v5, Lgol;->j:Lgol;

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Lgol;->a([Lgol;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Ldfn;->i:Z

    .line 121
    iget-boolean v0, p1, Ldac;->f:Z

    if-nez v0, :cond_9

    :goto_2
    iput-boolean v1, p0, Ldfn;->k:Z

    .line 122
    invoke-direct {p0}, Ldfn;->b()V

    .line 123
    return-void

    .line 95
    :cond_1
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v4, Lgol;->b:Lgol;

    invoke-virtual {v0, v4}, Lgol;->a(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Ldfn;->a:Ldfm;

    instance-of v0, v0, Ldec;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldfn;->a:Ldfm;

    check-cast v0, Ldec;

    .line 100
    invoke-interface {v0}, Ldec;->f_()Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    .line 101
    :goto_3
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v0

    move v4, v0

    .line 102
    :goto_4
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 103
    :goto_5
    iget-object v5, p1, Ldac;->b:Lfrl;

    if-eqz v5, :cond_0

    .line 106
    iget-object v5, p1, Ldac;->b:Lfrl;

    invoke-virtual {v5}, Lfrl;->b()Lfnc;

    move-result-object v5

    .line 107
    invoke-virtual {v5}, Lfnc;->a()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 108
    invoke-virtual {v5, v4, v0}, Lfnc;->a(II)Lfnb;

    move-result-object v0

    iget-object v0, v0, Lfnb;->a:Landroid/net/Uri;

    .line 110
    :goto_6
    if-eqz v0, :cond_2

    iget-object v3, p0, Ldfn;->g:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 111
    :cond_2
    invoke-direct {p0}, Ldfn;->a()V

    .line 113
    :cond_3
    iput-object v0, p0, Ldfn;->g:Landroid/net/Uri;

    goto :goto_0

    :cond_4
    move-object v5, v3

    .line 100
    goto :goto_3

    .line 101
    :cond_5
    const/16 v0, 0x1e0

    move v4, v0

    goto :goto_4

    .line 102
    :cond_6
    const/16 v0, 0x140

    goto :goto_5

    :cond_7
    move-object v0, v3

    .line 108
    goto :goto_6

    :cond_8
    move v0, v2

    .line 117
    goto :goto_1

    :cond_9
    move v1, v2

    .line 121
    goto :goto_2
.end method
