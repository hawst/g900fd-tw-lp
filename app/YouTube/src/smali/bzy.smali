.class public final Lbzy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/view/View;

.field public final c:Landroid/view/View;

.field public final d:Landroid/widget/TextView;

.field final e:Landroid/view/View;

.field public final f:Lfvi;

.field public final g:Landroid/widget/TextView;

.field final h:Landroid/widget/TextView;

.field public final i:Landroid/widget/TextView;

.field public final j:Landroid/widget/TextView;

.field public final k:Landroid/widget/TextView;

.field public l:Lfom;

.field m:Landroid/widget/EditText;

.field n:Landroid/app/AlertDialog;

.field o:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbzy;->a:Landroid/content/Context;

    .line 51
    iput-object p3, p0, Lbzy;->b:Landroid/view/View;

    .line 52
    iget-object v0, p0, Lbzy;->b:Landroid/view/View;

    const v1, 0x7f08035e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbzy;->c:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lbzy;->c:Landroid/view/View;

    const v1, 0x7f08035f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzy;->d:Landroid/widget/TextView;

    .line 54
    iget-object v0, p0, Lbzy;->d:Landroid/widget/TextView;

    new-instance v1, Lbzz;

    invoke-direct {v1, p0}, Lbzz;-><init>(Lbzy;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    iget-object v0, p0, Lbzy;->c:Landroid/view/View;

    const v1, 0x7f080360

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbzy;->e:Landroid/view/View;

    .line 61
    new-instance v1, Lfvi;

    iget-object v0, p0, Lbzy;->e:Landroid/view/View;

    const v2, 0x7f080361

    .line 62
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v0, v2}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;Z)V

    iput-object v1, p0, Lbzy;->f:Lfvi;

    .line 63
    iget-object v0, p0, Lbzy;->e:Landroid/view/View;

    const v1, 0x7f080362

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzy;->g:Landroid/widget/TextView;

    .line 64
    iget-object v0, p0, Lbzy;->e:Landroid/view/View;

    const v1, 0x7f080363

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzy;->h:Landroid/widget/TextView;

    .line 65
    iget-object v0, p0, Lbzy;->e:Landroid/view/View;

    const v1, 0x7f080285

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzy;->i:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lbzy;->e:Landroid/view/View;

    const v1, 0x7f080365

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzy;->j:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lbzy;->c:Landroid/view/View;

    new-instance v1, Lcaa;

    invoke-direct {v1, p0}, Lcaa;-><init>(Lbzy;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lbzy;->c:Landroid/view/View;

    const v1, 0x7f080366

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzy;->k:Landroid/widget/TextView;

    .line 74
    return-void
.end method

.method static synthetic a(Lbzy;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 27
    iget-object v0, p0, Lbzy;->l:Lfom;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbzy;->l:Lfom;

    invoke-virtual {v0, v2}, Lfom;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbzy;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbzy;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbzy;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lbzy;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lbzy;->o:Landroid/app/AlertDialog;

    if-nez v0, :cond_4

    iget-object v0, p0, Lbzy;->l:Lfom;

    if-eqz v0, :cond_4

    .line 78
    iget-object v0, p0, Lbzy;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 79
    const v1, 0x7f040149

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 80
    const v0, 0x7f080352

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbzy;->m:Landroid/widget/EditText;

    .line 81
    iget-object v0, p0, Lbzy;->m:Landroid/widget/EditText;

    iget-object v2, p0, Lbzy;->l:Lfom;

    invoke-virtual {v2}, Lfom;->d()Lfoj;

    move-result-object v2

    iget-object v3, v2, Lfoj;->c:Ljava/lang/CharSequence;

    if-nez v3, :cond_0

    iget-object v3, v2, Lfoj;->a:Lhxl;

    iget-object v3, v3, Lhxl;->b:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfoj;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v2, v2, Lfoj;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 82
    const v0, 0x7f080353

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 83
    new-instance v2, Lcab;

    invoke-direct {v2, p0}, Lcab;-><init>(Lbzy;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lbzy;->m:Landroid/widget/EditText;

    new-instance v2, Lcac;

    invoke-direct {v2, p0}, Lcac;-><init>(Lbzy;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 100
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lbzy;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lbzy;->l:Lfom;

    .line 101
    invoke-virtual {v2}, Lfom;->d()Lfoj;

    move-result-object v2

    iget-object v3, v2, Lfoj;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_1

    iget-object v3, v2, Lfoj;->a:Lhxl;

    iget-object v3, v3, Lhxl;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfoj;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v2, v2, Lfoj;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lbzy;->l:Lfom;

    .line 102
    invoke-virtual {v2}, Lfom;->d()Lfoj;

    move-result-object v2

    iget-object v3, v2, Lfoj;->d:Ljava/lang/CharSequence;

    if-nez v3, :cond_2

    iget-object v3, v2, Lfoj;->a:Lhxl;

    iget-object v3, v3, Lhxl;->d:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfoj;->d:Ljava/lang/CharSequence;

    :cond_2
    iget-object v2, v2, Lfoj;->d:Ljava/lang/CharSequence;

    new-instance v3, Lcae;

    invoke-direct {v3, p0}, Lcae;-><init>(Lbzy;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lbzy;->l:Lfom;

    .line 114
    invoke-virtual {v2}, Lfom;->d()Lfoj;

    move-result-object v2

    iget-object v3, v2, Lfoj;->e:Ljava/lang/CharSequence;

    if-nez v3, :cond_3

    iget-object v3, v2, Lfoj;->a:Lhxl;

    iget-object v3, v3, Lhxl;->c:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfoj;->e:Ljava/lang/CharSequence;

    :cond_3
    iget-object v2, v2, Lfoj;->e:Ljava/lang/CharSequence;

    new-instance v3, Lcad;

    invoke-direct {v3, p0}, Lcad;-><init>(Lbzy;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 121
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbzy;->o:Landroid/app/AlertDialog;

    .line 124
    :cond_4
    iget-object v0, p0, Lbzy;->o:Landroid/app/AlertDialog;

    if-eqz v0, :cond_5

    .line 125
    iget-object v0, p0, Lbzy;->l:Lfom;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbzy;->l:Lfom;

    invoke-virtual {v0}, Lfom;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 126
    iget-object v0, p0, Lbzy;->m:Landroid/widget/EditText;

    iget-object v1, p0, Lbzy;->l:Lfom;

    iget-object v1, v1, Lfom;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lbzy;->m:Landroid/widget/EditText;

    iget-object v1, p0, Lbzy;->l:Lfom;

    iget-object v1, v1, Lfom;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 131
    :goto_0
    iget-object v0, p0, Lbzy;->o:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 133
    :cond_5
    return-void

    .line 129
    :cond_6
    iget-object v0, p0, Lbzy;->m:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    goto :goto_0
.end method
