.class public final Lhwp;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Lhwt;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Lhxu;

.field public g:Lhgz;

.field public h:Lhgz;

.field public i:[B

.field public j:Lhiq;

.field public k:Lhwq;

.field private l:Lhgz;

.field private m:Lhgz;

.field private n:Z

.field private o:Lhgz;

.field private p:Lhgz;

.field private q:Lhgz;

.field private r:[Lhut;

.field private s:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Lhwp;->l:Lhgz;

    iput-object v1, p0, Lhwp;->m:Lhgz;

    iput-boolean v2, p0, Lhwp;->a:Z

    iput-boolean v2, p0, Lhwp;->b:Z

    iput-object v1, p0, Lhwp;->c:Lhwt;

    iput v2, p0, Lhwp;->d:I

    const-string v0, ""

    iput-object v0, p0, Lhwp;->e:Ljava/lang/String;

    iput-boolean v2, p0, Lhwp;->n:Z

    iput-object v1, p0, Lhwp;->o:Lhgz;

    iput-object v1, p0, Lhwp;->p:Lhgz;

    iput-object v1, p0, Lhwp;->f:Lhxu;

    iput-object v1, p0, Lhwp;->g:Lhgz;

    iput-object v1, p0, Lhwp;->h:Lhgz;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhwp;->i:[B

    iput-object v1, p0, Lhwp;->q:Lhgz;

    iput-object v1, p0, Lhwp;->j:Lhiq;

    sget-object v0, Lhut;->a:[Lhut;

    iput-object v0, p0, Lhwp;->r:[Lhut;

    iput-object v1, p0, Lhwp;->k:Lhwq;

    iput-object v1, p0, Lhwp;->s:Lhgz;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhwp;->l:Lhgz;

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    iget-object v2, p0, Lhwp;->l:Lhgz;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhwp;->m:Lhgz;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhwp;->m:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-boolean v2, p0, Lhwp;->a:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-boolean v3, p0, Lhwp;->a:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_1
    iget-boolean v2, p0, Lhwp;->b:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lhwp;->b:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhwp;->c:Lhwt;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lhwp;->c:Lhwt;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lhwp;->d:I

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget v3, p0, Lhwp;->d:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhwp;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lhwp;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-boolean v2, p0, Lhwp;->n:Z

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    iget-boolean v3, p0, Lhwp;->n:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lhwp;->o:Lhgz;

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    iget-object v3, p0, Lhwp;->o:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lhwp;->p:Lhgz;

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    iget-object v3, p0, Lhwp;->p:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lhwp;->f:Lhxu;

    if-eqz v2, :cond_9

    const/16 v2, 0xb

    iget-object v3, p0, Lhwp;->f:Lhxu;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Lhwp;->g:Lhgz;

    if-eqz v2, :cond_a

    const/16 v2, 0xc

    iget-object v3, p0, Lhwp;->g:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Lhwp;->h:Lhgz;

    if-eqz v2, :cond_b

    const/16 v2, 0xd

    iget-object v3, p0, Lhwp;->h:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget-object v2, p0, Lhwp;->i:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_c

    const/16 v2, 0xf

    iget-object v3, p0, Lhwp;->i:[B

    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_c
    iget-object v2, p0, Lhwp;->q:Lhgz;

    if-eqz v2, :cond_d

    const/16 v2, 0x10

    iget-object v3, p0, Lhwp;->q:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget-object v2, p0, Lhwp;->j:Lhiq;

    if-eqz v2, :cond_e

    const/16 v2, 0x11

    iget-object v3, p0, Lhwp;->j:Lhiq;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_e
    iget-object v2, p0, Lhwp;->r:[Lhut;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lhwp;->r:[Lhut;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    if-eqz v4, :cond_f

    const/16 v5, 0x12

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_10
    iget-object v1, p0, Lhwp;->k:Lhwq;

    if-eqz v1, :cond_11

    const/16 v1, 0x13

    iget-object v2, p0, Lhwp;->k:Lhwq;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget-object v1, p0, Lhwp;->s:Lhgz;

    if-eqz v1, :cond_12

    const/16 v1, 0x14

    iget-object v2, p0, Lhwp;->s:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget-object v1, p0, Lhwp;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhwp;->J:I

    return v0

    :cond_13
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhwp;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhwp;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhwp;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhwp;->l:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhwp;->l:Lhgz;

    :cond_2
    iget-object v0, p0, Lhwp;->l:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhwp;->m:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhwp;->m:Lhgz;

    :cond_3
    iget-object v0, p0, Lhwp;->m:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhwp;->a:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhwp;->b:Z

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhwp;->c:Lhwt;

    if-nez v0, :cond_4

    new-instance v0, Lhwt;

    invoke-direct {v0}, Lhwt;-><init>()V

    iput-object v0, p0, Lhwp;->c:Lhwt;

    :cond_4
    iget-object v0, p0, Lhwp;->c:Lhwt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-ne v0, v2, :cond_6

    :cond_5
    iput v0, p0, Lhwp;->d:I

    goto :goto_0

    :cond_6
    iput v1, p0, Lhwp;->d:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwp;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhwp;->n:Z

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lhwp;->o:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhwp;->o:Lhgz;

    :cond_7
    iget-object v0, p0, Lhwp;->o:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhwp;->p:Lhgz;

    if-nez v0, :cond_8

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhwp;->p:Lhgz;

    :cond_8
    iget-object v0, p0, Lhwp;->p:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lhwp;->f:Lhxu;

    if-nez v0, :cond_9

    new-instance v0, Lhxu;

    invoke-direct {v0}, Lhxu;-><init>()V

    iput-object v0, p0, Lhwp;->f:Lhxu;

    :cond_9
    iget-object v0, p0, Lhwp;->f:Lhxu;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lhwp;->g:Lhgz;

    if-nez v0, :cond_a

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhwp;->g:Lhgz;

    :cond_a
    iget-object v0, p0, Lhwp;->g:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lhwp;->h:Lhgz;

    if-nez v0, :cond_b

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhwp;->h:Lhgz;

    :cond_b
    iget-object v0, p0, Lhwp;->h:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhwp;->i:[B

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lhwp;->q:Lhgz;

    if-nez v0, :cond_c

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhwp;->q:Lhgz;

    :cond_c
    iget-object v0, p0, Lhwp;->q:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lhwp;->j:Lhiq;

    if-nez v0, :cond_d

    new-instance v0, Lhiq;

    invoke-direct {v0}, Lhiq;-><init>()V

    iput-object v0, p0, Lhwp;->j:Lhiq;

    :cond_d
    iget-object v0, p0, Lhwp;->j:Lhiq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_11
    const/16 v0, 0x92

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhwp;->r:[Lhut;

    if-nez v0, :cond_f

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhut;

    iget-object v3, p0, Lhwp;->r:[Lhut;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lhwp;->r:[Lhut;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    iput-object v2, p0, Lhwp;->r:[Lhut;

    :goto_2
    iget-object v2, p0, Lhwp;->r:[Lhut;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    iget-object v2, p0, Lhwp;->r:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwp;->r:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_f
    iget-object v0, p0, Lhwp;->r:[Lhut;

    array-length v0, v0

    goto :goto_1

    :cond_10
    iget-object v2, p0, Lhwp;->r:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhwp;->r:[Lhut;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lhwp;->k:Lhwq;

    if-nez v0, :cond_11

    new-instance v0, Lhwq;

    invoke-direct {v0}, Lhwq;-><init>()V

    iput-object v0, p0, Lhwp;->k:Lhwq;

    :cond_11
    iget-object v0, p0, Lhwp;->k:Lhwq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Lhwp;->s:Lhgz;

    if-nez v0, :cond_12

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhwp;->s:Lhgz;

    :cond_12
    iget-object v0, p0, Lhwp;->s:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhwp;->l:Lhgz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhwp;->l:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhwp;->m:Lhgz;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhwp;->m:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-boolean v0, p0, Lhwp;->a:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lhwp;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_2
    iget-boolean v0, p0, Lhwp;->b:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lhwp;->b:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_3
    iget-object v0, p0, Lhwp;->c:Lhwt;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lhwp;->c:Lhwt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget v0, p0, Lhwp;->d:I

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Lhwp;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_5
    iget-object v0, p0, Lhwp;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lhwp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_6
    iget-boolean v0, p0, Lhwp;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-boolean v1, p0, Lhwp;->n:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_7
    iget-object v0, p0, Lhwp;->o:Lhgz;

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Lhwp;->o:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_8
    iget-object v0, p0, Lhwp;->p:Lhgz;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lhwp;->p:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_9
    iget-object v0, p0, Lhwp;->f:Lhxu;

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Lhwp;->f:Lhxu;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_a
    iget-object v0, p0, Lhwp;->g:Lhgz;

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Lhwp;->g:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_b
    iget-object v0, p0, Lhwp;->h:Lhgz;

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lhwp;->h:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_c
    iget-object v0, p0, Lhwp;->i:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_d

    const/16 v0, 0xf

    iget-object v1, p0, Lhwp;->i:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_d
    iget-object v0, p0, Lhwp;->q:Lhgz;

    if-eqz v0, :cond_e

    const/16 v0, 0x10

    iget-object v1, p0, Lhwp;->q:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_e
    iget-object v0, p0, Lhwp;->j:Lhiq;

    if-eqz v0, :cond_f

    const/16 v0, 0x11

    iget-object v1, p0, Lhwp;->j:Lhiq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_f
    iget-object v0, p0, Lhwp;->r:[Lhut;

    if-eqz v0, :cond_11

    iget-object v1, p0, Lhwp;->r:[Lhut;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_11

    aget-object v3, v1, v0

    if-eqz v3, :cond_10

    const/16 v4, 0x12

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_11
    iget-object v0, p0, Lhwp;->k:Lhwq;

    if-eqz v0, :cond_12

    const/16 v0, 0x13

    iget-object v1, p0, Lhwp;->k:Lhwq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_12
    iget-object v0, p0, Lhwp;->s:Lhgz;

    if-eqz v0, :cond_13

    const/16 v0, 0x14

    iget-object v1, p0, Lhwp;->s:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_13
    iget-object v0, p0, Lhwp;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhwp;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhwp;

    iget-object v2, p0, Lhwp;->l:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhwp;->l:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhwp;->m:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhwp;->m:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-boolean v2, p0, Lhwp;->a:Z

    iget-boolean v3, p1, Lhwp;->a:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhwp;->b:Z

    iget-boolean v3, p1, Lhwp;->b:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhwp;->c:Lhwt;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhwp;->c:Lhwt;

    if-nez v2, :cond_3

    :goto_3
    iget v2, p0, Lhwp;->d:I

    iget v3, p1, Lhwp;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhwp;->e:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhwp;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget-boolean v2, p0, Lhwp;->n:Z

    iget-boolean v3, p1, Lhwp;->n:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhwp;->o:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhwp;->o:Lhgz;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhwp;->p:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhwp;->p:Lhgz;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhwp;->f:Lhxu;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhwp;->f:Lhxu;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Lhwp;->g:Lhgz;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhwp;->g:Lhgz;

    if-nez v2, :cond_3

    :goto_8
    iget-object v2, p0, Lhwp;->h:Lhgz;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhwp;->h:Lhgz;

    if-nez v2, :cond_3

    :goto_9
    iget-object v2, p0, Lhwp;->i:[B

    iget-object v3, p1, Lhwp;->i:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhwp;->q:Lhgz;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhwp;->q:Lhgz;

    if-nez v2, :cond_3

    :goto_a
    iget-object v2, p0, Lhwp;->j:Lhiq;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhwp;->j:Lhiq;

    if-nez v2, :cond_3

    :goto_b
    iget-object v2, p0, Lhwp;->r:[Lhut;

    iget-object v3, p1, Lhwp;->r:[Lhut;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhwp;->k:Lhwq;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhwp;->k:Lhwq;

    if-nez v2, :cond_3

    :goto_c
    iget-object v2, p0, Lhwp;->s:Lhgz;

    if-nez v2, :cond_10

    iget-object v2, p1, Lhwp;->s:Lhgz;

    if-nez v2, :cond_3

    :goto_d
    iget-object v2, p0, Lhwp;->I:Ljava/util/List;

    if-nez v2, :cond_11

    iget-object v2, p1, Lhwp;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhwp;->l:Lhgz;

    iget-object v3, p1, Lhwp;->l:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhwp;->m:Lhgz;

    iget-object v3, p1, Lhwp;->m:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhwp;->c:Lhwt;

    iget-object v3, p1, Lhwp;->c:Lhwt;

    invoke-virtual {v2, v3}, Lhwt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhwp;->e:Ljava/lang/String;

    iget-object v3, p1, Lhwp;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhwp;->o:Lhgz;

    iget-object v3, p1, Lhwp;->o:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhwp;->p:Lhgz;

    iget-object v3, p1, Lhwp;->p:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhwp;->f:Lhxu;

    iget-object v3, p1, Lhwp;->f:Lhxu;

    invoke-virtual {v2, v3}, Lhxu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhwp;->g:Lhgz;

    iget-object v3, p1, Lhwp;->g:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhwp;->h:Lhgz;

    iget-object v3, p1, Lhwp;->h:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhwp;->q:Lhgz;

    iget-object v3, p1, Lhwp;->q:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v2, p0, Lhwp;->j:Lhiq;

    iget-object v3, p1, Lhwp;->j:Lhiq;

    invoke-virtual {v2, v3}, Lhiq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v2, p0, Lhwp;->k:Lhwq;

    iget-object v3, p1, Lhwp;->k:Lhwq;

    invoke-virtual {v2, v3}, Lhwq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v2, p0, Lhwp;->s:Lhgz;

    iget-object v3, p1, Lhwp;->s:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v2, p0, Lhwp;->I:Ljava/util/List;

    iget-object v3, p1, Lhwp;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhwp;->l:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhwp;->m:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhwp;->a:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhwp;->b:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_3
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhwp;->c:Lhwt;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhwp;->d:I

    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhwp;->e:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lhwp;->n:Z

    if-eqz v4, :cond_8

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwp;->o:Lhgz;

    if-nez v0, :cond_9

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwp;->p:Lhgz;

    if-nez v0, :cond_a

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwp;->f:Lhxu;

    if-nez v0, :cond_b

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwp;->g:Lhgz;

    if-nez v0, :cond_c

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwp;->h:Lhgz;

    if-nez v0, :cond_d

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    iget-object v2, p0, Lhwp;->i:[B

    if-nez v2, :cond_e

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhwp;->q:Lhgz;

    if-nez v0, :cond_f

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwp;->j:Lhiq;

    if-nez v0, :cond_10

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    iget-object v2, p0, Lhwp;->r:[Lhut;

    if-nez v2, :cond_11

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhwp;->k:Lhwq;

    if-nez v0, :cond_13

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwp;->s:Lhgz;

    if-nez v0, :cond_14

    move v0, v1

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhwp;->I:Ljava/util/List;

    if-nez v2, :cond_15

    :goto_10
    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v0, p0, Lhwp;->l:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lhwp;->m:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_4
    move v0, v3

    goto/16 :goto_2

    :cond_5
    move v0, v3

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lhwp;->c:Lhwt;

    invoke-virtual {v0}, Lhwt;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_7
    iget-object v0, p0, Lhwp;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_8
    move v2, v3

    goto/16 :goto_6

    :cond_9
    iget-object v0, p0, Lhwp;->o:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_7

    :cond_a
    iget-object v0, p0, Lhwp;->p:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_8

    :cond_b
    iget-object v0, p0, Lhwp;->f:Lhxu;

    invoke-virtual {v0}, Lhxu;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_c
    iget-object v0, p0, Lhwp;->g:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_a

    :cond_d
    iget-object v0, p0, Lhwp;->h:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_b

    :cond_e
    move v2, v0

    move v0, v1

    :goto_11
    iget-object v3, p0, Lhwp;->i:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhwp;->i:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_f
    iget-object v0, p0, Lhwp;->q:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_c

    :cond_10
    iget-object v0, p0, Lhwp;->j:Lhiq;

    invoke-virtual {v0}, Lhiq;->hashCode()I

    move-result v0

    goto/16 :goto_d

    :cond_11
    move v2, v0

    move v0, v1

    :goto_12
    iget-object v3, p0, Lhwp;->r:[Lhut;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhwp;->r:[Lhut;

    aget-object v2, v2, v0

    if-nez v2, :cond_12

    move v2, v1

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    :cond_12
    iget-object v2, p0, Lhwp;->r:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhut;->hashCode()I

    move-result v2

    goto :goto_13

    :cond_13
    iget-object v0, p0, Lhwp;->k:Lhwq;

    invoke-virtual {v0}, Lhwq;->hashCode()I

    move-result v0

    goto/16 :goto_e

    :cond_14
    iget-object v0, p0, Lhwp;->s:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_f

    :cond_15
    iget-object v1, p0, Lhwp;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_10
.end method
