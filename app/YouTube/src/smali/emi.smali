.class final Lemi;
.super Ljava/lang/Thread;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lemk;

.field private synthetic c:Lemh;


# direct methods
.method constructor <init>(Lemh;Ljava/lang/String;Landroid/os/IBinder;)V
    .locals 1

    iput-object p1, p0, Lemi;->c:Lemh;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lemi;->a:Ljava/lang/String;

    invoke-static {p3}, Leml;->a(Landroid/os/IBinder;)Lemk;

    move-result-object v0

    iput-object v0, p0, Lemi;->b:Lemk;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lemi;->c:Lemh;

    iget-object v1, p0, Lemi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lemh;->a(Ljava/lang/String;)I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lemi;->b:Lemk;

    invoke-interface {v1, v0}, Lemk;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lemi;->c:Lemh;

    iget-object v1, p0, Lemi;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lemh;->a(Lemh;Ljava/lang/String;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "GcmTaskService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error reporting result of operation to scheduler for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lemi;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lemi;->c:Lemh;

    iget-object v1, p0, Lemi;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lemh;->a(Lemh;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lemi;->c:Lemh;

    iget-object v2, p0, Lemi;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lemh;->a(Lemh;Ljava/lang/String;)V

    throw v0
.end method
