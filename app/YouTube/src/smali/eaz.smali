.class public final Leaz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lebz;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/net/Uri;

.field private final c:Ljava/util/Map;

.field private d:Landroid/media/MediaExtractor;

.field private e:[Lecb;

.field private f:Z

.field private g:I

.field private h:[I

.field private i:[Z

.field private j:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;I)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    sget v0, Legz;->a:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 45
    iput-object p1, p0, Leaz;->a:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Leaz;->b:Landroid/net/Uri;

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Leaz;->c:Ljava/util/Map;

    .line 48
    const/4 v0, 0x2

    iput v0, p0, Leaz;->g:I

    .line 49
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(IJLebw;Leby;Z)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v3, -0x2

    const/4 v1, 0x0

    .line 104
    iget-boolean v0, p0, Leaz;->f:Z

    invoke-static {v0}, La;->c(Z)V

    .line 105
    iget-object v0, p0, Leaz;->h:[I

    aget v0, v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 106
    iget-object v0, p0, Leaz;->i:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Leaz;->i:[Z

    aput-boolean v1, v0, p1

    .line 108
    const/4 v0, -0x5

    .line 138
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 105
    goto :goto_0

    .line 110
    :cond_1
    if-eqz p6, :cond_2

    move v0, v3

    .line 111
    goto :goto_1

    .line 113
    :cond_2
    iget-object v0, p0, Leaz;->h:[I

    aget v0, v0, p1

    if-eq v0, v4, :cond_6

    .line 114
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    .line 115
    invoke-virtual {v0, p1}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v0

    .line 114
    new-instance v1, Lebv;

    invoke-direct {v1, v0}, Lebv;-><init>(Landroid/media/MediaFormat;)V

    iput-object v1, p4, Lebw;->a:Lebv;

    .line 116
    sget v0, Legz;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getPsshInfo()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    move-object v0, v2

    :cond_4
    :goto_2
    iput-object v0, p4, Lebw;->b:Ljava/util/Map;

    .line 117
    iget-object v0, p0, Leaz;->h:[I

    aput v4, v0, p1

    .line 118
    const/4 v0, -0x4

    goto :goto_1

    :cond_5
    move-object v0, v2

    .line 116
    goto :goto_2

    .line 120
    :cond_6
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleTrackIndex()I

    move-result v0

    .line 121
    if-ne v0, p1, :cond_9

    .line 122
    iget-object v0, p5, Leby;->b:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_8

    .line 123
    iget-object v0, p5, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 124
    iget-object v1, p0, Leaz;->d:Landroid/media/MediaExtractor;

    iget-object v2, p5, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v1

    iput v1, p5, Leby;->c:I

    .line 125
    iget-object v1, p5, Leby;->b:Ljava/nio/ByteBuffer;

    iget v2, p5, Leby;->c:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 129
    :goto_3
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v0

    iput-wide v0, p5, Leby;->e:J

    .line 130
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleFlags()I

    move-result v0

    iput v0, p5, Leby;->d:I

    .line 131
    iget v0, p5, Leby;->d:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7

    .line 132
    iget-object v0, p5, Leby;->a:Leao;

    iget-object v1, p0, Leaz;->d:Landroid/media/MediaExtractor;

    iget-object v2, v0, Leao;->g:Landroid/media/MediaCodec$CryptoInfo;

    invoke-virtual {v1, v2}, Landroid/media/MediaExtractor;->getSampleCryptoInfo(Landroid/media/MediaCodec$CryptoInfo;)Z

    iget-object v1, v0, Leao;->g:Landroid/media/MediaCodec$CryptoInfo;

    iget v1, v1, Landroid/media/MediaCodec$CryptoInfo;->numSubSamples:I

    iput v1, v0, Leao;->f:I

    iget-object v1, v0, Leao;->g:Landroid/media/MediaCodec$CryptoInfo;

    iget-object v1, v1, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    iput-object v1, v0, Leao;->d:[I

    iget-object v1, v0, Leao;->g:Landroid/media/MediaCodec$CryptoInfo;

    iget-object v1, v1, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfEncryptedData:[I

    iput-object v1, v0, Leao;->e:[I

    iget-object v1, v0, Leao;->g:Landroid/media/MediaCodec$CryptoInfo;

    iget-object v1, v1, Landroid/media/MediaCodec$CryptoInfo;->key:[B

    iput-object v1, v0, Leao;->b:[B

    iget-object v1, v0, Leao;->g:Landroid/media/MediaCodec$CryptoInfo;

    iget-object v1, v1, Landroid/media/MediaCodec$CryptoInfo;->iv:[B

    iput-object v1, v0, Leao;->a:[B

    iget-object v1, v0, Leao;->g:Landroid/media/MediaCodec$CryptoInfo;

    iget v1, v1, Landroid/media/MediaCodec$CryptoInfo;->mode:I

    iput v1, v0, Leao;->c:I

    .line 134
    :cond_7
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Leaz;->j:J

    .line 135
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->advance()Z

    .line 136
    const/4 v0, -0x3

    goto/16 :goto_1

    .line 127
    :cond_8
    iput v1, p5, Leby;->c:I

    goto :goto_3

    .line 138
    :cond_9
    if-gez v0, :cond_a

    const/4 v0, -0x1

    goto/16 :goto_1

    :cond_a
    move v0, v3

    goto/16 :goto_1
.end method

.method public final a(I)Lecb;
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Leaz;->f:Z

    invoke-static {v0}, La;->c(Z)V

    .line 80
    iget-object v0, p0, Leaz;->e:[Lecb;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 85
    iget-boolean v0, p0, Leaz;->f:Z

    invoke-static {v0}, La;->c(Z)V

    .line 86
    iget-object v0, p0, Leaz;->h:[I

    aget v0, v0, p1

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 87
    iget-object v0, p0, Leaz;->h:[I

    aput v1, v0, p1

    .line 88
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0, p1}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 89
    invoke-virtual {p0, p2, p3}, Leaz;->b(J)V

    .line 90
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 53
    iget-boolean v0, p0, Leaz;->f:Z

    if-nez v0, :cond_2

    .line 54
    new-instance v0, Landroid/media/MediaExtractor;

    invoke-direct {v0}, Landroid/media/MediaExtractor;-><init>()V

    iput-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    .line 55
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    iget-object v1, p0, Leaz;->a:Landroid/content/Context;

    iget-object v2, p0, Leaz;->b:Landroid/net/Uri;

    iget-object v3, p0, Leaz;->c:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaExtractor;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 56
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Leaz;->h:[I

    .line 57
    iget-object v0, p0, Leaz;->h:[I

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Leaz;->i:[Z

    .line 58
    iget-object v0, p0, Leaz;->h:[I

    array-length v0, v0

    new-array v0, v0, [Lecb;

    iput-object v0, p0, Leaz;->e:[Lecb;

    .line 59
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Leaz;->h:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 60
    iget-object v1, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v1, v0}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v1

    .line 61
    const-string v2, "durationUs"

    invoke-virtual {v1, v2}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "durationUs"

    .line 62
    invoke-virtual {v1, v2}, Landroid/media/MediaFormat;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 63
    :goto_1
    const-string v4, "mime"

    invoke-virtual {v1, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    iget-object v4, p0, Leaz;->e:[Lecb;

    new-instance v5, Lecb;

    invoke-direct {v5, v1, v2, v3}, Lecb;-><init>(Ljava/lang/String;J)V

    aput-object v5, v4, v0

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_1

    .line 66
    :cond_1
    iput-boolean v6, p0, Leaz;->f:Z

    .line 68
    :cond_2
    return v6
.end method

.method public final a(J)Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Leaz;->f:Z

    invoke-static {v0}, La;->c(Z)V

    .line 74
    iget-object v0, p0, Leaz;->h:[I

    array-length v0, v0

    return v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    iget-boolean v0, p0, Leaz;->f:Z

    invoke-static {v0}, La;->c(Z)V

    .line 151
    iget-object v0, p0, Leaz;->h:[I

    aget v0, v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 152
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0, p1}, Landroid/media/MediaExtractor;->unselectTrack(I)V

    .line 153
    iget-object v0, p0, Leaz;->i:[Z

    aput-boolean v1, v0, p1

    .line 154
    iget-object v0, p0, Leaz;->h:[I

    aput v1, v0, p1

    .line 155
    return-void

    :cond_0
    move v0, v1

    .line 151
    goto :goto_0
.end method

.method public final b(J)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 159
    iget-boolean v1, p0, Leaz;->f:Z

    invoke-static {v1}, La;->c(Z)V

    .line 160
    iget-wide v2, p0, Leaz;->j:J

    cmp-long v1, v2, p1

    if-eqz v1, :cond_1

    .line 163
    iput-wide p1, p0, Leaz;->j:J

    .line 164
    iget-object v1, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v1, p1, p2, v0}, Landroid/media/MediaExtractor;->seekTo(JI)V

    .line 165
    :goto_0
    iget-object v1, p0, Leaz;->h:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 166
    iget-object v1, p0, Leaz;->h:[I

    aget v1, v1, v0

    if-eqz v1, :cond_0

    .line 167
    iget-object v1, p0, Leaz;->i:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 165
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_1
    return-void
.end method

.method public final c()J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 175
    iget-boolean v2, p0, Leaz;->f:Z

    invoke-static {v2}, La;->c(Z)V

    .line 176
    iget-object v2, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v2}, Landroid/media/MediaExtractor;->getCachedDuration()J

    move-result-wide v2

    .line 177
    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    .line 180
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v0

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Leaz;->g:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 187
    iget v0, p0, Leaz;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Leaz;->g:I

    if-nez v0, :cond_0

    .line 188
    iget-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->release()V

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Leaz;->d:Landroid/media/MediaExtractor;

    .line 191
    :cond_0
    return-void

    .line 186
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
