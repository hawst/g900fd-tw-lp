.class public final Liix;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liex;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lifb;Lifb;)I
    .locals 2

    .prologue
    .line 106
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 107
    :cond_0
    const/4 v0, 0x0

    .line 109
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lifb;->a:F

    iget v1, p1, Lifb;->a:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method private static b(Lifb;Lifb;)I
    .locals 2

    .prologue
    .line 113
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 114
    :cond_0
    const v0, 0x7fffffff

    .line 116
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lifb;->a:F

    iget v1, p1, Lifb;->a:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lieq;Ljava/util/Map;)Liez;
    .locals 14

    .prologue
    .line 61
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lijo;->a(Lieq;Z)Lijp;

    move-result-object v9

    iget-object v0, v9, Lijp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, [Lifb;

    iget-object v0, v9, Lijp;->a:Lifx;

    const/4 v1, 0x4

    aget-object v1, v7, v1

    const/4 v2, 0x5

    aget-object v2, v7, v2

    const/4 v3, 0x6

    aget-object v3, v7, v3

    const/4 v4, 0x7

    aget-object v4, v7, v4

    const/4 v5, 0x0

    aget-object v5, v7, v5

    const/4 v6, 0x4

    aget-object v6, v7, v6

    invoke-static {v5, v6}, Liix;->b(Lifb;Lifb;)I

    move-result v5

    const/4 v6, 0x6

    aget-object v6, v7, v6

    const/4 v11, 0x2

    aget-object v11, v7, v11

    invoke-static {v6, v11}, Liix;->b(Lifb;Lifb;)I

    move-result v6

    mul-int/lit8 v6, v6, 0x11

    div-int/lit8 v6, v6, 0x12

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    const/4 v6, 0x1

    aget-object v6, v7, v6

    const/4 v11, 0x5

    aget-object v11, v7, v11

    invoke-static {v6, v11}, Liix;->b(Lifb;Lifb;)I

    move-result v6

    const/4 v11, 0x7

    aget-object v11, v7, v11

    const/4 v12, 0x3

    aget-object v12, v7, v12

    invoke-static {v11, v12}, Liix;->b(Lifb;Lifb;)I

    move-result v11

    mul-int/lit8 v11, v11, 0x11

    div-int/lit8 v11, v11, 0x12

    invoke-static {v6, v11}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    const/4 v6, 0x0

    aget-object v6, v7, v6

    const/4 v11, 0x4

    aget-object v11, v7, v11

    invoke-static {v6, v11}, Liix;->a(Lifb;Lifb;)I

    move-result v6

    const/4 v11, 0x6

    aget-object v11, v7, v11

    const/4 v12, 0x2

    aget-object v12, v7, v12

    invoke-static {v11, v12}, Liix;->a(Lifb;Lifb;)I

    move-result v11

    mul-int/lit8 v11, v11, 0x11

    div-int/lit8 v11, v11, 0x12

    invoke-static {v6, v11}, Ljava/lang/Math;->max(II)I

    move-result v6

    const/4 v11, 0x1

    aget-object v11, v7, v11

    const/4 v12, 0x5

    aget-object v12, v7, v12

    invoke-static {v11, v12}, Liix;->a(Lifb;Lifb;)I

    move-result v11

    const/4 v12, 0x7

    aget-object v12, v7, v12

    const/4 v13, 0x3

    aget-object v13, v7, v13

    invoke-static {v12, v13}, Liix;->a(Lifb;Lifb;)I

    move-result v12

    mul-int/lit8 v12, v12, 0x11

    div-int/lit8 v12, v12, 0x12

    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-static {v6, v11}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static/range {v0 .. v6}, Lijk;->a(Lifx;Lifb;Lifb;Lifb;Lifb;II)Liga;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_0
    new-instance v1, Liez;

    iget-object v2, v0, Liga;->b:Ljava/lang/String;

    iget-object v3, v0, Liga;->a:[B

    sget-object v4, Lieo;->k:Lieo;

    invoke-direct {v1, v2, v3, v7, v4}, Liez;-><init>(Ljava/lang/String;[B[Lifb;Lieo;)V

    sget-object v2, Lifa;->c:Lifa;

    iget-object v3, v0, Liga;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Liez;->a(Lifa;Ljava/lang/Object;)V

    iget-object v0, v0, Liga;->e:Ljava/lang/Object;

    check-cast v0, Liiy;

    if-eqz v0, :cond_1

    sget-object v2, Lifa;->h:Lifa;

    invoke-virtual {v1, v2, v0}, Liez;->a(Lifa;Ljava/lang/Object;)V

    :cond_1
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Liez;

    invoke-interface {v8, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Liez;

    .line 62
    if-eqz v0, :cond_3

    array-length v1, v0

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    aget-object v1, v0, v1

    if-nez v1, :cond_4

    .line 63
    :cond_3
    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    .line 65
    :cond_4
    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 138
    return-void
.end method
