.class public Ledz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgft;


# instance fields
.field public final a:Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;

.field public b:Z

.field public c:I

.field public d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Ledz;->c:I

    .line 61
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;

    iput-object v0, p0, Ledz;->a:Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;

    .line 62
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Lgfl;Landroid/os/Looper;Landroid/os/Handler;Ljava/lang/String;)Ledv;
    .locals 7

    .prologue
    .line 111
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 118
    invoke-virtual {p0}, Ledz;->b()I

    move-result v5

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p0

    move-object v6, p5

    .line 112
    invoke-static/range {v0 .. v6}, Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$V18CompatibilityLayer;->createWidevineDrmSessionManager18$1c66caf5(Landroid/net/Uri;Lgfl;Landroid/os/Looper;Landroid/os/Handler;Ledz;ILjava/lang/String;)Ledv;

    move-result-object v0

    return-object v0

    .line 121
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 71
    invoke-virtual {p0}, Ledz;->b()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Ledz;->d:Z

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "HD Entitlement received"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 74
    iput-boolean v1, p0, Ledz;->b:Z

    .line 75
    iget-object v0, p0, Ledz;->a:Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;->onHdEntitlementReceived()V

    .line 77
    :cond_0
    return-void
.end method

.method public a(Lfqy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 130
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v0, p1, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget-boolean v0, v0, Lhgg;->z:Z

    :goto_0
    iput-boolean v0, p0, Ledz;->d:Z

    .line 132
    iput-boolean v1, p0, Ledz;->b:Z

    .line 133
    return-void

    :cond_0
    move v0, v1

    .line 131
    goto :goto_0
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ledz;->a:Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$Listener;->onDrmError(Ljava/lang/Exception;)V

    .line 67
    return-void
.end method

.method public b()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 85
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_0

    .line 91
    :goto_0
    return v0

    .line 88
    :cond_0
    iget v1, p0, Ledz;->c:I

    if-ne v1, v0, :cond_1

    .line 89
    invoke-static {}, Lcom/google/android/libraries/youtube/media/player/drm/WidevineHelper$V18CompatibilityLayer;->getWidevineSecurityLevel()I

    move-result v0

    iput v0, p0, Ledz;->c:I

    .line 91
    :cond_1
    iget v0, p0, Ledz;->c:I

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Ledz;->b:Z

    return v0
.end method
