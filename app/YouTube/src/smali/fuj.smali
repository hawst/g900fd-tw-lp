.class public final Lfuj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfuv;


# instance fields
.field private final a:Lfsi;

.field private final b:Levn;

.field private final c:Lfkd;

.field private final d:Lfuh;

.field private e:Z

.field private final f:Lful;

.field private g:I

.field private final h:Ljava/util/List;


# direct methods
.method public constructor <init>(Lfuu;Levn;Lfkd;Lful;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lfuj;->b:Levn;

    .line 80
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    iput-object v0, p0, Lfuj;->c:Lfkd;

    .line 81
    const-class v0, Lfkd;

    invoke-virtual {p1, v0}, Lfuu;->a(Ljava/lang/Class;)V

    .line 82
    new-instance v1, Lfsi;

    invoke-virtual {p1}, Lfuu;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsl;

    invoke-direct {v1, v0}, Lfsi;-><init>(Lfsl;)V

    iput-object v1, p0, Lfuj;->a:Lfsi;

    .line 83
    new-instance v0, Lfuh;

    invoke-direct {v0}, Lfuh;-><init>()V

    iput-object v0, p0, Lfuj;->d:Lfuh;

    .line 85
    if-nez p4, :cond_0

    .line 86
    new-instance v0, Lfum;

    invoke-direct {v0}, Lfum;-><init>()V

    iput-object v0, p0, Lfuj;->f:Lful;

    .line 91
    :goto_0
    invoke-virtual {p2, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 93
    invoke-direct {p0}, Lfuj;->e()Z

    .line 95
    invoke-virtual {p3}, Lfkd;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfuj;->h:Ljava/util/List;

    .line 96
    iget-object v0, p0, Lfuj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p3}, Lfkd;->c()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 100
    invoke-virtual {p0}, Lfuj;->c()V

    .line 104
    :goto_1
    return-void

    .line 88
    :cond_0
    iput-object p4, p0, Lfuj;->f:Lful;

    goto :goto_0

    .line 102
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfuj;->e:Z

    iget-object v0, p0, Lfuj;->d:Lfuh;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lfuh;->b:Z

    iget-object v0, p0, Lfuj;->d:Lfuh;

    new-instance v1, Lfuk;

    invoke-direct {v1, p0}, Lfuk;-><init>(Lfuj;)V

    iput-object v1, v0, Lfuh;->c:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lfuj;->d:Lfuh;

    const/4 v1, 0x0

    iput-object v1, v0, Lfuh;->d:Lhog;

    invoke-direct {p0}, Lfuj;->g()V

    goto :goto_1
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 146
    iget v0, p0, Lfuj;->g:I

    .line 147
    iget-object v1, p0, Lfuj;->f:Lful;

    invoke-interface {v1}, Lful;->a()I

    move-result v1

    iput v1, p0, Lfuj;->g:I

    .line 148
    iget v1, p0, Lfuj;->g:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lfuj;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfuj;->c:Lfkd;

    .line 190
    iget-object v0, v0, Lfmr;->a:Lhvp;

    iget-object v0, v0, Lhvp;->c:Lhog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfuj;->c:Lfkd;

    invoke-virtual {v0}, Lfkd;->f()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 194
    iget-object v1, p0, Lfuj;->a:Lfsi;

    invoke-virtual {v1}, Lfsi;->b()V

    .line 195
    iget-object v1, p0, Lfuj;->a:Lfsi;

    iget-object v2, p0, Lfuj;->c:Lfkd;

    invoke-virtual {v1, v2}, Lfsi;->b(Ljava/lang/Object;)V

    .line 197
    iget-object v1, p0, Lfuj;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 198
    iget-boolean v1, p0, Lfuj;->e:Z

    if-eqz v1, :cond_2

    move v1, v2

    .line 200
    :goto_0
    if-ne v1, v3, :cond_3

    .line 203
    iget-object v1, p0, Lfuj;->a:Lfsi;

    iget-object v2, p0, Lfuj;->h:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfsi;->b(Ljava/lang/Object;)V

    .line 217
    :cond_0
    invoke-direct {p0}, Lfuj;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    iget-object v0, p0, Lfuj;->a:Lfsi;

    iget-object v1, p0, Lfuj;->d:Lfuh;

    invoke-virtual {v0, v1}, Lfsi;->b(Ljava/lang/Object;)V

    .line 220
    :cond_1
    return-void

    .line 198
    :cond_2
    iget-object v1, p0, Lfuj;->c:Lfkd;

    invoke-virtual {v1}, Lfkd;->c()I

    move-result v1

    goto :goto_0

    .line 204
    :cond_3
    if-le v1, v3, :cond_0

    .line 209
    iget v3, p0, Lfuj;->g:I

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    iget v3, p0, Lfuj;->g:I

    div-int/2addr v1, v3

    .line 210
    :goto_1
    if-ge v0, v1, :cond_0

    .line 211
    iget v3, p0, Lfuj;->g:I

    mul-int/2addr v3, v0

    .line 212
    add-int/lit8 v4, v0, 0x1

    iget v5, p0, Lfuj;->g:I

    mul-int/2addr v4, v5

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 213
    iget-object v5, p0, Lfuj;->a:Lfsi;

    new-instance v6, Lfui;

    iget v7, p0, Lfuj;->g:I

    iget-object v8, p0, Lfuj;->h:Ljava/util/List;

    invoke-interface {v8, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-direct {v6, v7, v3}, Lfui;-><init>(ILjava/util/List;)V

    invoke-virtual {v5, v6}, Lfsi;->b(Ljava/lang/Object;)V

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Lfuj;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0}, Lfuj;->g()V

    .line 111
    :cond_0
    return-void
.end method

.method public final b()Lfsi;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lfuj;->a:Lfsi;

    return-object v0
.end method

.method c()V
    .locals 2

    .prologue
    .line 173
    iget-boolean v0, p0, Lfuj;->e:Z

    if-eqz v0, :cond_0

    .line 186
    :goto_0
    return-void

    .line 177
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfuj;->e:Z

    .line 178
    invoke-direct {p0}, Lfuj;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lfuj;->d:Lfuh;

    iget-object v1, p0, Lfuj;->c:Lfkd;

    invoke-virtual {v1}, Lfkd;->f()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lfuh;->a:Ljava/lang/CharSequence;

    .line 180
    iget-object v0, p0, Lfuj;->d:Lfuh;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lfuh;->b:Z

    .line 181
    iget-object v0, p0, Lfuj;->d:Lfuh;

    const/4 v1, 0x0

    iput-object v1, v0, Lfuh;->c:Landroid/view/View$OnClickListener;

    .line 182
    iget-object v0, p0, Lfuj;->d:Lfuh;

    iget-object v1, p0, Lfuj;->c:Lfkd;

    iget-object v1, v1, Lfmr;->a:Lhvp;

    iget-object v1, v1, Lhvp;->c:Lhog;

    iput-object v1, v0, Lfuh;->d:Lhog;

    .line 185
    :cond_1
    invoke-direct {p0}, Lfuj;->g()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lfuj;->b:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 121
    return-void
.end method

.method public final handleHideEnclosingActionEvent(Lfhg;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 137
    iget-object v0, p1, Lfgz;->a:Ljava/lang/Object;

    iget-object v1, p0, Lfuj;->c:Lfkd;

    if-ne v0, v1, :cond_0

    .line 138
    iget-object v0, p0, Lfuj;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 143
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lfuj;->h:Ljava/util/List;

    iget-object v1, p1, Lfgz;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 141
    invoke-direct {p0}, Lfuj;->g()V

    goto :goto_0
.end method

.method public final handleServiceResponseRemoveEvent(Lfml;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 126
    iget-object v0, p1, Lfmk;->b:Ljava/lang/Object;

    iget-object v1, p0, Lfuj;->c:Lfkd;

    if-ne v0, v1, :cond_0

    .line 127
    iget-object v0, p0, Lfuj;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 132
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lfuj;->h:Ljava/util/List;

    iget-object v1, p1, Lfmk;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 130
    invoke-direct {p0}, Lfuj;->g()V

    goto :goto_0
.end method
