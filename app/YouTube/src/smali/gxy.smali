.class public final Lgxy;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgxy;


# instance fields
.field public b:Lgxt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1070
    const/4 v0, 0x0

    new-array v0, v0, [Lgxy;

    sput-object v0, Lgxy;->a:[Lgxy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1071
    invoke-direct {p0}, Lidf;-><init>()V

    .line 1074
    const/4 v0, 0x0

    iput-object v0, p0, Lgxy;->b:Lgxt;

    .line 1071
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1111
    const/4 v0, 0x0

    .line 1112
    iget-object v1, p0, Lgxy;->b:Lgxt;

    if-eqz v1, :cond_0

    .line 1113
    const v0, 0x3b7df28

    iget-object v1, p0, Lgxy;->b:Lgxt;

    .line 1114
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1116
    :cond_0
    iget-object v1, p0, Lgxy;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1117
    iput v0, p0, Lgxy;->J:I

    .line 1118
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 1067
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgxy;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgxy;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgxy;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgxy;->b:Lgxt;

    if-nez v0, :cond_2

    new-instance v0, Lgxt;

    invoke-direct {v0}, Lgxt;-><init>()V

    iput-object v0, p0, Lgxy;->b:Lgxt;

    :cond_2
    iget-object v0, p0, Lgxy;->b:Lgxt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1dbef942 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 1102
    iget-object v0, p0, Lgxy;->b:Lgxt;

    if-eqz v0, :cond_0

    .line 1103
    const v0, 0x3b7df28

    iget-object v1, p0, Lgxy;->b:Lgxt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 1105
    :cond_0
    iget-object v0, p0, Lgxy;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 1107
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1085
    if-ne p1, p0, :cond_1

    .line 1089
    :cond_0
    :goto_0
    return v0

    .line 1086
    :cond_1
    instance-of v2, p1, Lgxy;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1087
    :cond_2
    check-cast p1, Lgxy;

    .line 1088
    iget-object v2, p0, Lgxy;->b:Lgxt;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgxy;->b:Lgxt;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgxy;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgxy;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 1089
    goto :goto_0

    .line 1088
    :cond_4
    iget-object v2, p0, Lgxy;->b:Lgxt;

    iget-object v3, p1, Lgxy;->b:Lgxt;

    invoke-virtual {v2, v3}, Lgxt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgxy;->I:Ljava/util/List;

    iget-object v3, p1, Lgxy;->I:Ljava/util/List;

    .line 1089
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1093
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1095
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxy;->b:Lgxt;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1096
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgxy;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1097
    return v0

    .line 1095
    :cond_0
    iget-object v0, p0, Lgxy;->b:Lgxt;

    invoke-virtual {v0}, Lgxt;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1096
    :cond_1
    iget-object v1, p0, Lgxy;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
