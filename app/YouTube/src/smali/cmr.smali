.class public final Lcmr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leyt;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lexd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lexd;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcmr;->a:Landroid/content/Context;

    .line 43
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lcmr;->b:Lexd;

    .line 44
    return-void
.end method

.method public static a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    instance-of v0, p0, Lfxw;

    if-eqz v0, :cond_0

    check-cast p0, Lfxw;

    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0, p1, p2, v0, p4}, Lfxw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcmr;->b(Ljava/lang/Throwable;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcmr;->a(Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Leze;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    .line 183
    return-void
.end method

.method public final b(Ljava/lang/Throwable;)Landroid/util/Pair;
    .locals 9

    .prologue
    const/16 v8, 0x15

    const v6, 0x7f090118

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 71
    move-object v1, p1

    :goto_0
    if-nez v1, :cond_0

    .line 72
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f09001f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "genericError"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 162
    :goto_1
    return-object v0

    .line 74
    :cond_0
    const-class v4, Lgis;

    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v2

    :goto_3
    if-eqz v0, :cond_3

    .line 76
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900f5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "deviceRegistration"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 74
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_2

    :cond_2
    move v0, v3

    goto :goto_3

    .line 78
    :cond_3
    instance-of v0, v1, Landroid/accounts/AuthenticatorException;

    if-eqz v0, :cond_4

    .line 80
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900f6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "authenticator"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 82
    :cond_4
    instance-of v0, v1, Ljava/net/SocketException;

    if-eqz v0, :cond_6

    .line 84
    iget-object v0, p0, Lcmr;->b:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 85
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900ef

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "connection"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 87
    :cond_5
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900eb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "noNetwork"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 89
    :cond_6
    instance-of v0, v1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_c

    .line 90
    instance-of v0, v1, Lfxw;

    if-eqz v0, :cond_a

    move-object v0, v1

    .line 91
    check-cast v0, Lfxw;

    .line 94
    const-string v4, "yt:service"

    const-string v5, "disabled_in_maintenance_mode"

    invoke-virtual {v0, v4, v5, v7, v7}, Lfxw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 95
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900f1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "youtubeServerDown"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 98
    :cond_7
    invoke-virtual {v0}, Lfxw;->a()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 99
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900f7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "noLinkedYoutubeAccount"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 102
    :cond_8
    const-string v4, "GData"

    const-string v5, "InvalidRequestUriException"

    invoke-virtual {v0, v4, v5, v7, v7}, Lfxw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 103
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900f2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "invalidRequest"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 105
    :cond_9
    const-string v4, "GData"

    const-string v5, "ServiceForbiddenException"

    const-string v6, "Private video"

    invoke-virtual {v0, v4, v5, v7, v6}, Lfxw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 106
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900b3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "videoIsPrivate"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 110
    :cond_a
    check-cast v1, Lorg/apache/http/client/HttpResponseException;

    .line 111
    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "httpError "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v4

    const/16 v5, 0x193

    if-ne v4, v5, :cond_b

    .line 113
    iget-object v1, p0, Lcmr;->a:Landroid/content/Context;

    const v2, 0x7f0900f8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 115
    :cond_b
    iget-object v4, p0, Lcmr;->a:Landroid/content/Context;

    const v5, 0x7f0900f0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-virtual {v4, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 118
    :cond_c
    instance-of v0, v1, Lxa;

    if-eqz v0, :cond_e

    move-object v0, v1

    .line 119
    check-cast v0, Lxa;

    .line 120
    iget-object v4, v0, Lxa;->a:Lwm;

    .line 122
    if-eqz v4, :cond_e

    iget v5, v4, Lwm;->a:I

    if-lez v5, :cond_e

    .line 123
    iget v1, v4, Lwm;->a:I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "httpError "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 124
    iget-object v0, v0, Lxa;->a:Lwm;

    iget v0, v0, Lwm;->a:I

    const/16 v5, 0x193

    if-ne v0, v5, :cond_d

    .line 125
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v2, 0x7f0900f8

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 127
    :cond_d
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v5, 0x7f0900f0

    new-array v2, v2, [Ljava/lang/Object;

    iget v4, v4, Lwm;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 132
    :cond_e
    instance-of v0, v1, Lfay;

    if-eqz v0, :cond_f

    .line 133
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900f4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "invalidResponse"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 135
    :cond_f
    instance-of v0, v1, Lfax;

    if-eqz v0, :cond_10

    .line 137
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900f3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "genericResponseError"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 139
    :cond_10
    instance-of v0, v1, Ljava/io/IOException;

    if-eqz v0, :cond_12

    .line 141
    iget-object v0, p0, Lcmr;->b:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 142
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f09001e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "genericNetworkError"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 144
    :cond_11
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f0900eb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "noNetwork"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 146
    :cond_12
    instance-of v0, v1, Lgls;

    if-eqz v0, :cond_14

    .line 147
    instance-of v0, v1, Lglu;

    if-eqz v0, :cond_13

    .line 148
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    .line 149
    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlinePolicyExpired"

    .line 148
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 151
    :cond_13
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    .line 152
    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlinePolicyError"

    .line 151
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 154
    :cond_14
    instance-of v0, v1, Lglo;

    if-eqz v0, :cond_17

    .line 155
    instance-of v0, v1, Lglq;

    if-eqz v0, :cond_15

    .line 156
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    .line 157
    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlineMediaUnplayable"

    .line 156
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 158
    :cond_15
    instance-of v0, v1, Lglp;

    if-eqz v0, :cond_16

    .line 159
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f090119

    .line 160
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlineMediaIncomplete"

    .line 159
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 161
    :cond_16
    instance-of v0, v1, Lglr;

    if-eqz v0, :cond_17

    .line 162
    iget-object v0, p0, Lcmr;->a:Landroid/content/Context;

    const v1, 0x7f090119

    .line 163
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlineNoMedia"

    .line 162
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    .line 167
    :cond_17
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public final c(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Lcmr;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcmr;->a(Ljava/lang/String;)V

    .line 173
    return-void
.end method
