.class public Lbdh;
.super Lbdx;
.source "SourceFile"


# instance fields
.field private X:Lgku;

.field private Y:Lfxe;

.field private Z:Leyp;

.field private a:Lari;

.field private aa:Lgix;

.field private ab:Leyt;

.field private ac:Landroid/content/res/Resources;

.field private ad:Lbyc;

.field private ae:Lawv;

.field private af:Lbwd;

.field private ag:Lboi;

.field private ah:Lboi;

.field private ai:Lbdm;

.field private aj:Ldsn;

.field private ak:Lbvc;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lbdx;-><init>()V

    .line 283
    return-void
.end method

.method private A()V
    .locals 4

    .prologue
    .line 254
    iget-object v0, p0, Lbdh;->af:Lbwd;

    if-eqz v0, :cond_0

    .line 255
    iget-object v1, p0, Lbdh;->af:Lbwd;

    iget-object v0, p0, Lbdh;->ac:Landroid/content/res/Resources;

    const v2, 0x7f0b001c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    if-lez v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v3, "numColumns must be > 0"

    invoke-static {v0, v3}, Lb;->c(ZLjava/lang/Object;)V

    iget v0, v1, Lbwd;->b:I

    if-eq v0, v2, :cond_0

    iput v2, v1, Lbwd;->b:I

    invoke-virtual {v1}, Lbwd;->notifyDataSetChanged()V

    .line 257
    :cond_0
    return-void

    .line 255
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lbdh;)Lbdm;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lbdh;->ai:Lbdm;

    return-object v0
.end method

.method static synthetic a(Lbdh;Lgcd;)V
    .locals 5

    .prologue
    .line 53
    new-instance v0, Lbdo;

    invoke-direct {v0, p1}, Lbdo;-><init>(Lgcd;)V

    iget-object v1, p0, Lbdh;->Y:Lfxe;

    iget-object v2, p1, Lgcd;->h:Landroid/net/Uri;

    iget-object v3, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v4, Lbdl;

    invoke-direct {v4, p0, v0}, Lbdl;-><init>(Lbdh;Lbdo;)V

    invoke-static {v3, v4}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lfxe;->a(Landroid/net/Uri;Leuc;)V

    return-void
.end method

.method static synthetic b(Lbdh;)Lbyc;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lbdh;->ad:Lbyc;

    return-object v0
.end method

.method static synthetic c(Lbdh;)Lawv;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lbdh;->ae:Lawv;

    return-object v0
.end method

.method static synthetic d(Lbdh;)Leyt;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lbdh;->ab:Leyt;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    .line 98
    const v0, 0x7f040098

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 102
    iget-object v0, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lbdh;->a:Lari;

    .line 104
    iget-object v1, v1, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    .line 102
    invoke-static {v0, v1, v2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;

    move-result-object v0

    iput-object v0, p0, Lbdh;->ag:Lboi;

    .line 106
    new-instance v0, Lbdi;

    invoke-direct {v0, p0}, Lbdi;-><init>(Lbdh;)V

    .line 112
    iget-object v1, p0, Lbdh;->ag:Lboi;

    const v2, 0x7f0902d5

    invoke-virtual {v1, v2, v0}, Lboi;->a(ILbop;)I

    .line 113
    new-instance v0, Lbdj;

    invoke-direct {v0, p0}, Lbdj;-><init>(Lbdh;)V

    .line 122
    iget-object v1, p0, Lbdh;->ag:Lboi;

    const v2, 0x7f0902cd

    invoke-virtual {v1, v2, v0}, Lboi;->a(ILbop;)I

    .line 124
    new-instance v0, Lboi;

    iget-object v1, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1}, Lboi;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lbdh;->ah:Lboi;

    .line 125
    new-instance v0, Lbdk;

    invoke-direct {v0, p0}, Lbdk;-><init>(Lbdh;)V

    .line 132
    iget-object v1, p0, Lbdh;->ah:Lboi;

    const v2, 0x7f0902d0

    invoke-virtual {v1, v2, v0}, Lboi;->a(ILbop;)I

    .line 134
    iget-object v0, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lbdh;->Z:Leyp;

    iget-object v2, p0, Lbdh;->ag:Lboi;

    iget-object v3, p0, Lbdh;->ah:Lboi;

    invoke-static {v0, v1, v2, v3}, La;->a(Landroid/content/Context;Leyp;Lboi;Lboi;)Lawv;

    move-result-object v0

    iput-object v0, p0, Lbdh;->ae:Lawv;

    .line 137
    iget-object v0, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lbdh;->ae:Lawv;

    invoke-static {v0, v1}, Lbwd;->a(Landroid/content/Context;Lcba;)Lbwd;

    move-result-object v0

    iput-object v0, p0, Lbdh;->af:Lbwd;

    .line 139
    const v0, 0x7f08022f

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    .line 141
    new-instance v0, Lbyc;

    iget-object v1, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lbdh;->aa:Lgix;

    iget-object v4, p0, Lbdh;->ae:Lawv;

    iget-object v5, p0, Lbdh;->af:Lbwd;

    iget-object v6, p0, Lbdh;->X:Lgku;

    iget-object v7, p0, Lbdh;->Y:Lfxe;

    iget-object v8, p0, Lbdh;->ab:Leyt;

    iget-object v9, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 150
    iget-object v9, v9, Lbhz;->m:Lfus;

    invoke-direct/range {v0 .. v9}, Lbyc;-><init>(Landroid/app/Activity;Lgix;Ldob;Lawv;Lbwd;Lgku;Lfxe;Leyt;Lfus;)V

    iput-object v0, p0, Lbdh;->ad:Lbyc;

    .line 152
    iget-object v0, p0, Lbdh;->aj:Ldsn;

    iget-object v1, p0, Lbdh;->ag:Lboi;

    iget-object v2, p0, Lbdh;->ab:Leyt;

    iget-object v3, p0, Lbdh;->a:Lari;

    .line 156
    iget-object v3, v3, Lari;->b:Ldov;

    invoke-virtual {v3}, Ldov;->g()Ldaq;

    move-result-object v3

    .line 152
    new-instance v4, Lbvc;

    new-instance v5, Lbvd;

    invoke-direct {v5, v2, v3}, Lbvd;-><init>(Leyt;Ldaq;)V

    const/4 v2, 0x1

    invoke-direct {v4, v0, v1, v5, v2}, Lbvc;-><init>(Ldsn;Lboi;Lbvm;I)V

    iput-object v4, p0, Lbdh;->ak:Lbvc;

    .line 158
    if-eqz p3, :cond_0

    .line 159
    iget-object v0, p0, Lbdh;->ad:Lbyc;

    const-string v1, "uploads_helper"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbyc;->a(Landroid/os/Bundle;)V

    .line 162
    :cond_0
    return-object v10
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 277
    const v0, 0x7f090268

    invoke-virtual {p0, v0}, Lbdh;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0, p1}, Lbdx;->a(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lbdh;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    iput-object v0, p0, Lbdh;->a:Lari;

    .line 85
    iget-object v0, p0, Lbdh;->a:Lari;

    invoke-virtual {v0}, Lari;->aE()Lcuo;

    .line 86
    iget-object v0, p0, Lbdh;->a:Lari;

    invoke-virtual {v0}, Lari;->aD()Lcst;

    move-result-object v0

    iput-object v0, p0, Lbdh;->aa:Lgix;

    .line 87
    iget-object v0, p0, Lbdh;->a:Lari;

    invoke-virtual {v0}, Lari;->b()Lfxe;

    move-result-object v0

    iput-object v0, p0, Lbdh;->Y:Lfxe;

    .line 88
    iget-object v0, p0, Lbdh;->a:Lari;

    invoke-virtual {v0}, Lari;->c()Leyp;

    move-result-object v0

    iput-object v0, p0, Lbdh;->Z:Leyp;

    .line 89
    iget-object v0, p0, Lbdh;->a:Lari;

    invoke-virtual {v0}, Lari;->ay()Leyt;

    move-result-object v0

    iput-object v0, p0, Lbdh;->ab:Leyt;

    .line 90
    iget-object v0, p0, Lbdh;->Y:Lfxe;

    invoke-interface {v0}, Lfxe;->f()Lgku;

    move-result-object v0

    iput-object v0, p0, Lbdh;->X:Lgku;

    .line 91
    invoke-virtual {p0}, Lbdh;->k()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbdh;->ac:Landroid/content/res/Resources;

    .line 92
    iget-object v0, p0, Lbdh;->a:Lari;

    invoke-virtual {v0}, Lari;->K()Ldsn;

    move-result-object v0

    iput-object v0, p0, Lbdh;->aj:Ldsn;

    .line 93
    return-void
.end method

.method public final b()Lcan;
    .locals 3

    .prologue
    .line 211
    iget-object v0, p0, Lbdh;->W:Lcan;

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    invoke-virtual {v0}, Lcaq;->h()Lcar;

    move-result-object v0

    .line 213
    const v1, 0x7f090268

    invoke-virtual {p0, v1}, Lbdh;->a(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcar;->a:Ljava/lang/CharSequence;

    new-instance v1, Lcaz;

    iget-object v2, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v1, v2}, Lcaz;-><init>(Landroid/app/Activity;)V

    .line 214
    invoke-virtual {v0, v1}, Lcar;->a(Lcam;)Lcar;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Lcar;->a()Lcaq;

    move-result-object v0

    iput-object v0, p0, Lbdh;->W:Lcan;

    .line 217
    :cond_0
    iget-object v0, p0, Lbdh;->W:Lcan;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    const-string v0, "yt_android_uploads"

    return-object v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0}, Lbdx;->e()V

    .line 168
    invoke-direct {p0}, Lbdh;->A()V

    .line 170
    iget-object v0, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Lt;

    move-result-object v0

    .line 171
    const-string v1, "DeleteUploadDialogFragment"

    invoke-virtual {v0, v1}, Lt;->a(Ljava/lang/String;)Lj;

    move-result-object v0

    check-cast v0, Lbdm;

    iput-object v0, p0, Lbdh;->ai:Lbdm;

    .line 173
    iget-object v0, p0, Lbdh;->ai:Lbdm;

    if-nez v0, :cond_0

    .line 174
    new-instance v0, Lbdm;

    invoke-direct {v0}, Lbdm;-><init>()V

    iput-object v0, p0, Lbdh;->ai:Lbdm;

    .line 176
    :cond_0
    iget-object v0, p0, Lbdh;->ai:Lbdm;

    invoke-static {v0, p0}, Lbdm;->a(Lbdm;Lbdh;)Lbdh;

    .line 177
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 203
    invoke-super {p0, p1}, Lbdx;->e(Landroid/os/Bundle;)V

    .line 204
    iget-object v0, p0, Lbdh;->ad:Lbyc;

    if-eqz v0, :cond_0

    .line 205
    const-string v0, "uploads_helper"

    iget-object v1, p0, Lbdh;->ad:Lbyc;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "required_items_count"

    iget-object v1, v1, Lbtr;->b:Ldob;

    invoke-interface {v1}, Ldob;->g()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 207
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Lbdx;->g()V

    .line 198
    iget-object v0, p0, Lbdh;->ak:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 199
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 222
    invoke-super {p0, p1}, Lbdx;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 223
    invoke-direct {p0}, Lbdh;->A()V

    .line 224
    return-void
.end method

.method public final t()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 181
    invoke-super {p0}, Lbdx;->t()V

    .line 182
    iget-object v0, p0, Lbdh;->aa:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lbdh;->ad:Lbyc;

    const/4 v1, 0x1

    new-array v1, v1, [Lfxg;

    iget-object v2, p0, Lbdh;->Y:Lfxe;

    invoke-interface {v2}, Lfxe;->a()Lfxj;

    move-result-object v2

    iget v3, v2, Lfxj;->c:I

    invoke-virtual {v2, v3}, Lfxj;->a(I)Lfxg;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lbyc;->a([Lfxg;)V

    .line 184
    iget-object v0, p0, Lbdh;->ak:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lbdh;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c(Z)V

    goto :goto_0
.end method

.method public final u()V
    .locals 0

    .prologue
    .line 192
    invoke-super {p0}, Lbdx;->u()V

    .line 193
    return-void
.end method
