.class public Lezn;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:Z


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lezn;->b:Landroid/content/Context;

    .line 32
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lezn;->c:Ljava/util/concurrent/Executor;

    .line 33
    return-void
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 23
    sget-boolean v0, Lezn;->a:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 84
    sget-boolean v0, Lezn;->a:Z

    if-eqz v0, :cond_0

    .line 113
    :goto_0
    return-void

    .line 88
    :cond_0
    const-class v1, Lezn;

    monitor-enter v1

    .line 89
    :try_start_0
    sget-boolean v0, Lezn;->a:Z

    if-eqz v0, :cond_1

    .line 90
    monitor-exit v1

    goto :goto_0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 93
    :cond_1
    :try_start_1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v0, v2, :cond_2

    .line 97
    const-string v0, "Blocking main thread on ProviderInstaller."

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 99
    :cond_2
    iget-object v0, p0, Lezn;->b:Landroid/content/Context;

    invoke-static {v0}, Leqw;->a(Landroid/content/Context;)V

    .line 100
    const/4 v0, 0x1

    sput-boolean v0, Lezn;->a:Z
    :try_end_1
    .catch Leje; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lejd; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    :goto_1
    :try_start_2
    monitor-exit v1

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 104
    const-string v2, "ProviderInstaller failed."

    invoke-static {v2, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 105
    const/4 v0, 0x1

    sput-boolean v0, Lezn;->a:Z

    goto :goto_1

    .line 106
    :catch_1
    move-exception v0

    .line 110
    const-string v2, "ProviderInstaller failed."

    invoke-static {v2, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 111
    const/4 v0, 0x1

    sput-boolean v0, Lezn;->a:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 53
    sget-boolean v0, Lezn;->a:Z

    if-eqz v0, :cond_1

    .line 54
    if-eqz p1, :cond_0

    .line 55
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    iget-object v0, p0, Lezn;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lezo;

    invoke-direct {v1, p0, p1}, Lezo;-><init>(Lezn;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
