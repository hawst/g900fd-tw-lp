.class public final Lbyc;
.super Lbtr;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Ldmw;


# instance fields
.field public c:Lfad;

.field private final d:Lbtx;

.field private final e:Lawv;

.field private final f:Lbwd;

.field private final g:Lgix;

.field private final h:Lfxe;

.field private final i:Lfus;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lgix;Ldob;Lawv;Lbwd;Lgku;Lfxe;Leyt;Lfus;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 74
    new-instance v0, Lbyd;

    invoke-direct {v0}, Lbyd;-><init>()V

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lgkf;

    invoke-direct {v4, p6, v1, v0, v1}, Lgkf;-><init>(Lgku;Lgib;Lgic;Ljava/util/concurrent/Executor;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p5

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lbtr;-><init>(Landroid/app/Activity;Ldob;Lcba;Lgku;Leyt;)V

    .line 75
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lbyc;->h:Lfxe;

    .line 76
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbyc;->g:Lgix;

    .line 77
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfus;

    iput-object v0, p0, Lbyc;->i:Lfus;

    .line 78
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwd;

    iput-object v0, p0, Lbyc;->f:Lbwd;

    .line 79
    iget-object v0, p0, Lbyc;->f:Lbwd;

    iput-object p0, v0, Lbwd;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 80
    iput-object p4, p0, Lbyc;->e:Lawv;

    .line 81
    new-instance v0, Lbtx;

    invoke-direct {v0}, Lbtx;-><init>()V

    iput-object v0, p0, Lbyc;->d:Lbtx;

    .line 82
    return-void
.end method

.method private a(Lbxz;)Lbxz;
    .locals 3

    .prologue
    .line 262
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbyc;->e:Lawv;

    invoke-virtual {v0}, Lawv;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 263
    iget-object v0, p0, Lbyc;->e:Lawv;

    invoke-virtual {v0, v1}, Lawv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxz;

    .line 264
    invoke-virtual {v0, p1}, Lbxz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268
    :goto_1
    return-object v0

    .line 262
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 268
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lbyc;Lgcd;Lgjm;)Lgcd;
    .locals 6

    .prologue
    .line 52
    iget-object v0, p2, Lgjm;->g:Lgje;

    const-string v1, "upload_file_duration"

    iget v2, p1, Lgcd;->k:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lgje;->b(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    if-nez v0, :cond_0

    iget v0, p1, Lgcd;->k:I

    :cond_0
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    sub-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p1}, Lgcd;->c()Lgcf;

    move-result-object v2

    iput v0, v2, Lgcf;->k:I

    iput-object v1, v2, Lgcf;->q:Ljava/util/Date;

    invoke-virtual {v2}, Lgcf;->a()Lgcd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbyc;Lgjm;Lgcd;)V
    .locals 3

    .prologue
    .line 52
    invoke-static {p1}, Lbxz;->a(Lgjm;)Lbxz;

    move-result-object v0

    invoke-direct {p0, v0}, Lbyc;->a(Lbxz;)Lbxz;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbyc;->e:Lawv;

    invoke-static {p2}, Lbxz;->a(Lgcd;)Lbxz;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lawv;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private f(Lgjm;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 235
    iget-object v0, p1, Lgjm;->c:Lgjn;

    sget-object v1, Lgjn;->c:Lgjn;

    if-eq v0, v1, :cond_1

    .line 236
    iget-object v0, p1, Lgjm;->g:Lgje;

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, v4}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    iget-object v1, p1, Lgjm;->g:Lgje;

    const-string v2, "tracking_account_id"

    invoke-virtual {v1, v2, v5}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v1

    .line 240
    iget-object v2, p1, Lgjm;->g:Lgje;

    const-string v3, "account_id"

    invoke-virtual {v2, v3, v4}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 241
    iget-object v3, p0, Lbyc;->g:Lgix;

    invoke-interface {v3}, Lgix;->d()Lgit;

    move-result-object v3

    .line 242
    invoke-virtual {v3}, Lgit;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 243
    iget-object v4, v3, Lgit;->b:Lgiv;

    invoke-virtual {v4}, Lgiv;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    .line 244
    iget-object v0, v3, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    :cond_0
    invoke-static {p1}, Lbxz;->a(Lgjm;)Lbxz;

    move-result-object v0

    invoke-direct {p0, v0}, Lbyc;->a(Lbxz;)Lbxz;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lbyc;->e:Lawv;

    invoke-virtual {v2, v1, v0}, Lawv;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lbyc;->e:Lawv;

    new-instance v1, Lbyf;

    invoke-direct {v1, p1}, Lbyf;-><init>(Lgjm;)V

    invoke-virtual {v0, v1}, Lawv;->a(Lewh;)V

    .line 248
    :cond_1
    return-void

    .line 245
    :cond_2
    iget-object v1, p0, Lbyc;->e:Lawv;

    invoke-virtual {v1, v5, v0}, Lawv;->b(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lbyc;->c:Lfad;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lbyc;->c:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    invoke-virtual {v0}, Ldmz;->a()Ljava/util/Map;

    move-result-object v0

    .line 184
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjm;

    .line 185
    invoke-direct {p0, v0}, Lbyc;->f(Lgjm;)V

    goto :goto_0

    .line 188
    :cond_0
    return-void
.end method

.method protected final a(Lfxg;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0, p1, p2}, Lbtr;->a(Lfxg;Ljava/util/List;)V

    .line 121
    iget-object v0, p0, Lbyc;->c:Lfad;

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Lbyc;->a:Landroid/app/Activity;

    .line 123
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->a(Landroid/content/Context;Ldmw;)Lfad;

    move-result-object v0

    iput-object v0, p0, Lbyc;->c:Lfad;

    .line 125
    :cond_0
    return-void
.end method

.method public final a(Lgjm;)V
    .locals 0

    .prologue
    .line 212
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 52
    check-cast p1, Lbxz;

    invoke-virtual {p1}, Lbxz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyc;->d:Lbtx;

    iget-object v0, p1, Lbxz;->a:Lgcd;

    invoke-static {v0}, Lbtx;->a(Lgcd;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method public final b(Lgjm;)V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0, p1}, Lbyc;->f(Lgjm;)V

    .line 193
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0}, Lbtr;->c()V

    .line 139
    iget-object v0, p0, Lbyc;->c:Lfad;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lbyc;->c:Lfad;

    iget-object v1, p0, Lbyc;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfad;->a(Landroid/content/Context;)V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lbyc;->c:Lfad;

    .line 143
    :cond_0
    return-void
.end method

.method public final c(Lgjm;)V
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p1}, Lgjm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lbyc;->e:Lawv;

    invoke-virtual {v0, p1}, Lawv;->a(Lgjm;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    invoke-direct {p0, p1}, Lbyc;->f(Lgjm;)V

    .line 169
    :cond_0
    return-void
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0}, Lbtr;->d()V

    .line 130
    iget-object v0, p0, Lbyc;->c:Lfad;

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lbyc;->a:Landroid/app/Activity;

    .line 132
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->a(Landroid/content/Context;Ldmw;)Lfad;

    move-result-object v0

    iput-object v0, p0, Lbyc;->c:Lfad;

    .line 134
    :cond_0
    return-void
.end method

.method public final d(Lgjm;)V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p1, Lgjm;->c:Lgjn;

    sget-object v1, Lgjn;->c:Lgjn;

    if-ne v0, v1, :cond_0

    .line 198
    iget-object v0, p1, Lgjm;->h:Lgje;

    const-string v1, "video_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199
    if-eqz v0, :cond_1

    .line 200
    iget-object v1, p0, Lbyc;->a:Landroid/app/Activity;

    new-instance v2, Lbye;

    invoke-direct {v2, p0, p1}, Lbye;-><init>(Lbyc;Lgjm;)V

    .line 201
    invoke-static {v1, v2}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v1

    .line 202
    iget-object v2, p0, Lbyc;->h:Lfxe;

    invoke-interface {v2, v0, v1}, Lfxe;->e(Ljava/lang/String;Leuc;)V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    invoke-direct {p0, p1}, Lbyc;->f(Lgjm;)V

    goto :goto_0
.end method

.method public final e(Lgjm;)V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lbyc;->e:Lawv;

    invoke-static {p1}, Lbxz;->a(Lgjm;)Lbxz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lawv;->b(Ljava/lang/Object;)V

    .line 174
    iget-object v0, p1, Lgjm;->c:Lgjn;

    sget-object v1, Lgjn;->c:Lgjn;

    if-eq v0, v1, :cond_0

    iget v0, p1, Lgjm;->d:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lbyc;->a:Landroid/app/Activity;

    const v1, 0x7f0902d1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    .line 178
    :cond_0
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 154
    iget-object v0, p0, Lbyc;->e:Lawv;

    invoke-virtual {v0}, Lawv;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_0

    .line 155
    iget-object v0, p0, Lbyc;->e:Lawv;

    invoke-virtual {v0, p3}, Lawv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxz;

    .line 156
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbxz;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    iget-object v1, p0, Lbyc;->i:Lfus;

    iget-object v0, v0, Lbxz;->a:Lgcd;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    sget-object v2, Lgog;->c:Lgog;

    invoke-virtual {v1, v0, v2}, Lfus;->a(Ljava/lang/String;Lgog;)V

    .line 160
    :cond_0
    return-void
.end method
