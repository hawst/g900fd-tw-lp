.class public final Lhih;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhih;


# instance fields
.field public b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65450
    const/4 v0, 0x0

    new-array v0, v0, [Lhih;

    sput-object v0, Lhih;->a:[Lhih;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65451
    invoke-direct {p0}, Lidf;-><init>()V

    .line 65460
    const/4 v0, 0x0

    iput v0, p0, Lhih;->b:I

    .line 65451
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 65497
    const/4 v0, 0x0

    .line 65498
    iget v1, p0, Lhih;->b:I

    if-eqz v1, :cond_0

    .line 65499
    const/4 v0, 0x1

    iget v1, p0, Lhih;->b:I

    .line 65500
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 65502
    :cond_0
    iget-object v1, p0, Lhih;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65503
    iput v0, p0, Lhih;->J:I

    .line 65504
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 65447
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhih;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhih;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhih;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lhih;->b:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lhih;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 65488
    iget v0, p0, Lhih;->b:I

    if-eqz v0, :cond_0

    .line 65489
    const/4 v0, 0x1

    iget v1, p0, Lhih;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 65491
    :cond_0
    iget-object v0, p0, Lhih;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 65493
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 65471
    if-ne p1, p0, :cond_1

    .line 65475
    :cond_0
    :goto_0
    return v0

    .line 65472
    :cond_1
    instance-of v2, p1, Lhih;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 65473
    :cond_2
    check-cast p1, Lhih;

    .line 65474
    iget v2, p0, Lhih;->b:I

    iget v3, p1, Lhih;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhih;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhih;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 65475
    goto :goto_0

    .line 65474
    :cond_4
    iget-object v2, p0, Lhih;->I:Ljava/util/List;

    iget-object v3, p1, Lhih;->I:Ljava/util/List;

    .line 65475
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 65479
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 65481
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhih;->b:I

    add-int/2addr v0, v1

    .line 65482
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhih;->I:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 65483
    return v0

    .line 65482
    :cond_0
    iget-object v0, p0, Lhih;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_0
.end method
