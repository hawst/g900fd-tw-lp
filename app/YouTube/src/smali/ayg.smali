.class public final Layg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Laxw;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcws;

.field c:I

.field d:Ldwq;

.field private final e:Levn;

.field private final f:Ldsn;

.field private final g:Ldwl;

.field private final h:Ldwn;

.field private final i:Ldso;

.field private final j:Landroid/os/Handler;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Levn;Lcws;Ldsn;Ldwl;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 61
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Layg;->a:Landroid/content/Context;

    .line 62
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Layg;->e:Levn;

    .line 63
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcws;

    iput-object v0, p0, Layg;->b:Lcws;

    .line 64
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsn;

    iput-object v0, p0, Layg;->f:Ldsn;

    .line 65
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwl;

    iput-object v0, p0, Layg;->g:Ldwl;

    .line 66
    new-instance v0, Layh;

    invoke-direct {v0, p0}, Layh;-><init>(Layg;)V

    iput-object v0, p0, Layg;->h:Ldwn;

    .line 67
    new-instance v0, Layi;

    invoke-direct {v0, p0}, Layi;-><init>(Layg;)V

    iput-object v0, p0, Layg;->i:Ldso;

    .line 68
    new-instance v0, Layj;

    invoke-direct {v0, p0}, Layj;-><init>(Layg;)V

    iput-object v0, p0, Layg;->j:Landroid/os/Handler;

    .line 69
    return-void
.end method

.method private handleMdxPlaybackChangedEvent(Ldwi;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 121
    iget-object v3, p1, Ldwi;->a:Ldwj;

    invoke-virtual {v3}, Ldwj;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Ldwj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Lgoh;

    iget-object v1, v3, Ldwj;->a:Ljava/lang/String;

    iget-object v2, v3, Ldwj;->d:Ljava/lang/String;

    iget v3, v3, Ldwj;->e:I

    const/4 v4, 0x0

    sget-object v5, Lgog;->d:Lgog;

    invoke-direct/range {v0 .. v5}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;IILgog;)V

    iget-object v1, p0, Layg;->b:Lcws;

    invoke-virtual {v1, v0}, Lcws;->a(Lgoh;)V

    .line 122
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 79
    iget-boolean v0, p0, Layg;->k:Z

    if-nez v0, :cond_1

    .line 80
    iget-object v0, p0, Layg;->e:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Layg;->g:Ldwl;

    iget-object v1, p0, Layg;->h:Ldwn;

    invoke-virtual {v0, v1}, Ldwl;->a(Ldwn;)V

    .line 82
    iget-object v0, p0, Layg;->f:Ldsn;

    iget-object v1, p0, Layg;->i:Ldso;

    invoke-virtual {v0, v1}, Ldsn;->a(Ldso;)V

    .line 83
    iget-object v0, p0, Layg;->f:Ldsn;

    iget-object v0, v0, Ldsn;->d:Ldwq;

    iput-object v0, p0, Layg;->d:Ldwq;

    .line 86
    iget-object v0, p0, Layg;->d:Ldwq;

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Layg;->c()V

    .line 89
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Layg;->k:Z

    .line 91
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 100
    iget-boolean v0, p0, Layg;->k:Z

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Layg;->e:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Layg;->g:Ldwl;

    iget-object v1, p0, Layg;->h:Ldwn;

    invoke-virtual {v0, v1}, Ldwl;->b(Ldwn;)V

    .line 103
    iget-object v0, p0, Layg;->f:Ldsn;

    iget-object v1, p0, Layg;->i:Ldso;

    invoke-virtual {v0, v1}, Ldsn;->b(Ldso;)V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Layg;->d:Ldwq;

    .line 106
    invoke-virtual {p0}, Layg;->d()V

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Layg;->k:Z

    .line 109
    :cond_0
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Layg;->j:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 113
    return-void
.end method

.method d()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Layg;->j:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 117
    return-void
.end method
