.class public final Ldcq;
.super Ldee;
.source "SourceFile"

# interfaces
.implements Ldeo;


# instance fields
.field private final a:Landroid/util/SparseArray;

.field private final b:Landroid/util/SparseArray;

.field private c:F

.field private d:F

.field private e:Lgpo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 47
    invoke-direct {p0, p1}, Ldee;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Ldcq;->a:Landroid/util/SparseArray;

    .line 49
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Ldcq;->b:Landroid/util/SparseArray;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Ldcq;->d:F

    .line 51
    new-instance v0, Lgpo;

    .line 52
    invoke-static {}, Ldeu;->c()I

    move-result v1

    .line 53
    invoke-static {}, Ldeu;->d()I

    move-result v2

    .line 54
    invoke-static {}, Ldeu;->h()I

    move-result v3

    .line 55
    invoke-static {}, Ldev;->b()I

    move-result v4

    .line 56
    invoke-static {}, Ldeu;->g()I

    move-result v5

    .line 57
    invoke-static {}, Ldew;->b()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lgpo;-><init>(IIIIII)V

    iput-object v0, p0, Ldcq;->e:Lgpo;

    .line 58
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ldcq;->setVisibility(I)V

    .line 59
    return-void
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 148
    .line 149
    invoke-virtual {p0}, Ldcq;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldcq;->c:F

    invoke-static {v0, v1, p1, p2}, Ldes;->a(Landroid/content/Context;FII)F

    move-result v0

    iput v0, p0, Ldcq;->d:F

    .line 150
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ldcq;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 151
    iget-object v0, p0, Ldcq;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    .line 152
    invoke-direct {p0, v0}, Ldcq;->a(Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;)V

    .line 150
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 154
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 294
    iget v0, p0, Ldcq;->d:F

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->a(F)V

    .line 295
    iget-object v0, p0, Ldcq;->e:Lgpo;

    iget v0, v0, Lgpo;->a:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->d(I)V

    .line 296
    iget-object v0, p0, Ldcq;->e:Lgpo;

    iget v0, v0, Lgpo;->b:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setBackgroundColor(I)V

    .line 297
    iget-object v0, p0, Ldcq;->e:Lgpo;

    iget v0, v0, Lgpo;->e:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->a(I)V

    .line 299
    invoke-virtual {p0}, Ldcq;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 300
    iget-object v1, p0, Ldcq;->e:Lgpo;

    invoke-static {v0, v1}, Ldew;->a(Landroid/content/Context;Lgpo;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->a(Landroid/graphics/Typeface;)V

    .line 303
    iget-object v0, p0, Ldcq;->e:Lgpo;

    iget v0, v0, Lgpo;->c:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->b(I)V

    .line 304
    invoke-virtual {p1, v2, v2, v2, v2}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setPadding(IIII)V

    .line 305
    iget-object v0, p0, Ldcq;->e:Lgpo;

    iget v0, v0, Lgpo;->d:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->c(I)V

    .line 306
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Ldcq;->removeAllViews()V

    .line 117
    iget-object v0, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 118
    iget-object v0, p0, Ldcq;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 119
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 134
    iput p1, p0, Ldcq;->c:F

    .line 135
    invoke-virtual {p0}, Ldcq;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Ldcq;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Ldcq;->a(II)V

    .line 136
    return-void
.end method

.method public final a(Lgpo;)V
    .locals 2

    .prologue
    .line 143
    iput-object p1, p0, Ldcq;->e:Lgpo;

    .line 144
    invoke-virtual {p0}, Ldcq;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Ldcq;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Ldcq;->a(II)V

    .line 145
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 72
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move v0, v2

    .line 73
    :goto_0
    iget-object v1, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 74
    iget-object v1, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v2

    .line 76
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 77
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgpi;

    .line 78
    iget v1, v0, Lgpi;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 79
    iget-object v1, p0, Ldcq;->b:Landroid/util/SparseArray;

    iget v5, v0, Lgpi;->a:I

    invoke-virtual {v1, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    .line 80
    iget-object v5, v0, Lgpi;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, v0, Lgpi;->b:Lgpe;

    iget-boolean v5, v5, Lgpe;->e:Z

    if-nez v5, :cond_3

    .line 81
    :cond_1
    if-eqz v1, :cond_2

    .line 82
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setVisibility(I)V

    .line 76
    :cond_2
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 85
    :cond_3
    iget-object v5, p0, Ldcq;->a:Landroid/util/SparseArray;

    iget v6, v0, Lgpi;->a:I

    invoke-virtual {v5, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 86
    if-nez v1, :cond_4

    .line 87
    iget-object v1, v0, Lgpi;->c:Ljava/lang/String;

    new-instance v5, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    invoke-virtual {p0}, Ldcq;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v5}, Ldcq;->a(Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;)V

    invoke-virtual {v5, v1}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v5, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->a(Lgpi;)V

    .line 88
    invoke-virtual {p0, v5}, Ldcq;->addView(Landroid/view/View;)V

    .line 89
    iget-object v1, p0, Ldcq;->b:Landroid/util/SparseArray;

    iget v0, v0, Lgpi;->a:I

    invoke-virtual {v1, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 93
    :cond_4
    iget-object v5, v0, Lgpi;->c:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getTag()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 94
    iget-object v5, v0, Lgpi;->c:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setTag(Ljava/lang/Object;)V

    .line 95
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->a(Lgpi;)V

    .line 97
    :cond_5
    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setVisibility(I)V

    goto :goto_2

    .line 103
    :cond_6
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 104
    iget-object v0, p0, Ldcq;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Ldcq;->removeView(Landroid/view/View;)V

    .line 105
    iget-object v0, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 106
    iget-object v0, p0, Ldcq;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_3

    .line 108
    :cond_7
    invoke-virtual {p0, v2}, Ldcq;->setVisibility(I)V

    .line 109
    return-void
.end method

.method protected final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 158
    invoke-super {p0, p1}, Ldee;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 159
    invoke-virtual {p0}, Ldcq;->requestLayout()V

    .line 160
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 218
    sub-int v0, p4, p2

    .line 219
    sub-int v1, p5, p3

    .line 220
    mul-int/lit8 v2, v0, 0xf

    div-int/lit8 v2, v2, 0x64

    div-int/lit8 v4, v2, 0x2

    .line 221
    mul-int/lit8 v2, v1, 0xf

    div-int/lit8 v2, v2, 0x64

    div-int/lit8 v5, v2, 0x2

    .line 223
    mul-int/lit8 v0, v0, 0x55

    div-int/lit8 v6, v0, 0x64

    .line 224
    mul-int/lit8 v0, v1, 0x55

    div-int/lit8 v7, v0, 0x64

    .line 226
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 227
    iget-object v0, p0, Ldcq;->b:Landroid/util/SparseArray;

    iget-object v1, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    .line 228
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 229
    iget-object v1, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgpi;

    .line 230
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getMeasuredWidth()I

    move-result v8

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getMeasuredHeight()I

    move-result v9

    iget-object v10, v1, Lgpi;->b:Lgpe;

    iget v11, v10, Lgpe;->b:I

    iget v1, v10, Lgpe;->c:I

    mul-int/2addr v1, v6

    div-int/lit8 v3, v1, 0x64

    iget v1, v10, Lgpe;->d:I

    mul-int/2addr v1, v7

    div-int/lit8 v1, v1, 0x64

    iget-boolean v10, v10, Lgpe;->f:Z

    if-nez v10, :cond_3

    and-int/lit8 v10, v11, 0x1

    if-eqz v10, :cond_1

    :goto_1
    and-int/lit8 v10, v11, 0x8

    if-eqz v10, :cond_4

    :goto_2
    add-int/2addr v3, v4

    add-int/2addr v1, v5

    add-int/2addr v8, v3

    add-int/2addr v9, v1

    invoke-virtual {v0, v3, v1, v8, v9}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->layout(IIII)V

    .line 226
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 230
    :cond_1
    and-int/lit8 v10, v11, 0x2

    if-eqz v10, :cond_2

    div-int/lit8 v10, v8, 0x2

    sub-int/2addr v3, v10

    goto :goto_1

    :cond_2
    and-int/lit8 v10, v11, 0x4

    if-eqz v10, :cond_3

    sub-int/2addr v3, v8

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    and-int/lit8 v10, v11, 0x10

    if-eqz v10, :cond_5

    div-int/lit8 v10, v9, 0x2

    sub-int/2addr v1, v10

    goto :goto_2

    :cond_5
    and-int/lit8 v10, v11, 0x20

    if-eqz v10, :cond_6

    sub-int/2addr v1, v9

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    .line 233
    :cond_7
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 164
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 165
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 166
    invoke-virtual {p0, v5, v6}, Ldcq;->setMeasuredDimension(II)V

    .line 168
    invoke-direct {p0, v5, v6}, Ldcq;->a(II)V

    move v2, v3

    .line 170
    :goto_0
    iget-object v0, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 171
    iget-object v0, p0, Ldcq;->b:Landroid/util/SparseArray;

    iget-object v1, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    .line 172
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 173
    iget-object v1, p0, Ldcq;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgpi;

    iget-object v1, v1, Lgpi;->b:Lgpe;

    iget v7, v1, Lgpe;->b:I

    iget v4, v1, Lgpe;->c:I

    mul-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x64

    iget v1, v1, Lgpe;->d:I

    mul-int/2addr v1, v6

    div-int/lit8 v1, v1, 0x64

    and-int/lit8 v8, v7, 0x1

    if-eqz v8, :cond_3

    sub-int v4, v5, v4

    :cond_0
    :goto_1
    and-int/lit8 v8, v7, 0x8

    if-eqz v8, :cond_5

    sub-int v1, v6, v1

    :cond_1
    :goto_2
    invoke-static {v4, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->measure(II)V

    .line 170
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 173
    :cond_3
    and-int/lit8 v8, v7, 0x2

    if-eqz v8, :cond_4

    sub-int v8, v5, v4

    invoke-static {v4, v8}, Ljava/lang/Math;->min(II)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    goto :goto_1

    :cond_4
    and-int/lit8 v8, v7, 0x4

    if-nez v8, :cond_0

    move v4, v3

    goto :goto_1

    :cond_5
    and-int/lit8 v8, v7, 0x10

    if-eqz v8, :cond_6

    sub-int v7, v6, v1

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    goto :goto_2

    :cond_6
    and-int/lit8 v7, v7, 0x20

    if-nez v7, :cond_1

    move v1, v3

    goto :goto_2

    .line 176
    :cond_7
    return-void
.end method

.method public final q_()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ldcq;->setVisibility(I)V

    .line 127
    return-void
.end method

.method public final r_()Ldeg;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 63
    new-instance v0, Ldeg;

    invoke-direct {v0, v1, v1}, Ldeg;-><init>(II)V

    return-object v0
.end method
