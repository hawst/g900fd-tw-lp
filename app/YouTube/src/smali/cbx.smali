.class public Lcbx;
.super Lfsb;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/view/View;

.field final c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

.field final d:Landroid/view/View;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Lfvi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;ILfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p3, p5, p6}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcbx;->a:Landroid/content/Context;

    .line 50
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 53
    const/4 v1, 0x0

    invoke-virtual {v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbx;->b:Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcbx;->b:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbx;->e:Landroid/widget/TextView;

    .line 56
    iget-object v0, p0, Lcbx;->b:Landroid/view/View;

    const v1, 0x7f080136

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbx;->f:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lcbx;->b:Landroid/view/View;

    const v1, 0x7f080126

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbx;->g:Landroid/widget/TextView;

    .line 58
    iget-object v0, p0, Lcbx;->b:Landroid/view/View;

    const v1, 0x7f080135

    .line 59
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    iput-object v0, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    .line 60
    new-instance v0, Lfvi;

    iget-object v1, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    .line 62
    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->a:Landroid/widget/ImageView;

    invoke-direct {v0, p2, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcbx;->h:Lfvi;

    .line 63
    iget-object v0, p0, Lcbx;->b:Landroid/view/View;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbx;->d:Landroid/view/View;

    .line 64
    return-void
.end method


# virtual methods
.method protected final a(Lfma;Lfnc;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 98
    iget-object v0, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-static {v0, p3}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 101
    if-eqz p1, :cond_1

    .line 103
    invoke-virtual {p1}, Lfma;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b(Z)V

    .line 105
    iget-object v0, p0, Lcbx;->h:Lfvi;

    invoke-virtual {p1}, Lfma;->d()Lfnc;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    .line 115
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b(Z)V

    .line 108
    iget-object v0, p0, Lcbx;->h:Lfvi;

    invoke-virtual {p1}, Lfma;->c()Lfnc;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b(Z)V

    .line 113
    iget-object v0, p0, Lcbx;->h:Lfvi;

    invoke-virtual {v0, p2, v2}, Lfvi;->a(Lfnc;Leyo;)V

    goto :goto_0
.end method

.method protected final a(Lfnc;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    invoke-virtual {p1}, Lfnc;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b(Z)V

    .line 119
    iget-object v0, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcbx;->h:Lfvi;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lfvi;->a(Lfnc;Leyo;)V

    .line 121
    return-void
.end method

.method protected final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcbx;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    return-void
.end method

.method protected final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcbx;->f:Landroid/widget/TextView;

    invoke-static {v0, p1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 88
    return-void
.end method

.method protected final c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcbx;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    return-void
.end method
