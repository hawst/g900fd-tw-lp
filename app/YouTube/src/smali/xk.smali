.class public final Lxk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lws;

.field public final b:Lxp;

.field public final c:Ljava/util/HashMap;

.field final d:Ljava/util/HashMap;

.field e:Ljava/lang/Runnable;

.field private f:I

.field private final g:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lws;Lxp;)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/16 v0, 0x64

    iput v0, p0, Lxk;->f:I

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lxk;->c:Ljava/util/HashMap;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lxk;->d:Ljava/util/HashMap;

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lxk;->g:Landroid/os/Handler;

    .line 86
    iput-object p1, p0, Lxk;->a:Lws;

    .line 87
    iput-object p2, p0, Lxk;->b:Lxp;

    .line 88
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;Lxo;)V
    .locals 4

    .prologue
    .line 432
    iget-object v0, p0, Lxk;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    iget-object v0, p0, Lxk;->e:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 436
    new-instance v0, Lxn;

    invoke-direct {v0, p0}, Lxn;-><init>(Lxk;)V

    iput-object v0, p0, Lxk;->e:Ljava/lang/Runnable;

    .line 461
    iget-object v0, p0, Lxk;->g:Landroid/os/Handler;

    iget-object v1, p0, Lxk;->e:Ljava/lang/Runnable;

    iget v2, p0, Lxk;->f:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 463
    :cond_0
    return-void
.end method
