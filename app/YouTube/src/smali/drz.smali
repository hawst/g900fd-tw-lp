.class final Ldrz;
.super Ldrv;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ldri;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Levn;

.field private final e:Ldtd;

.field private final f:Ldwr;

.field private final g:Ldwq;

.field private final h:Ldwv;

.field private i:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Ldwr;Ldtd;Landroid/content/Context;Ldwq;Ldwv;Ldxe;Ljava/util/concurrent/Executor;Levn;Ldri;Ldyg;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0, p6, p10}, Ldrv;-><init>(Ldxe;Ldyg;)V

    .line 103
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    iput-object v0, p0, Ldrz;->f:Ldwr;

    .line 104
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Ldrz;->g:Ldwq;

    .line 105
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwv;

    iput-object v0, p0, Ldrz;->h:Ldwv;

    .line 106
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldtd;

    iput-object v0, p0, Ldrz;->e:Ldtd;

    .line 107
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldrz;->a:Landroid/content/Context;

    .line 108
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Ldrz;->c:Ljava/util/concurrent/Executor;

    .line 109
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldrz;->d:Levn;

    .line 110
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldri;

    iput-object v0, p0, Ldrz;->b:Ldri;

    .line 111
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    return-void
.end method

.method private c(I)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 188
    new-instance v0, Lri;

    invoke-direct {v0, p1}, Lri;-><init>(I)V

    iget-object v1, p0, Ldrz;->g:Ldwq;

    .line 189
    invoke-interface {v1}, Ldwq;->r()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lri;->b(J)Lri;

    move-result-object v0

    .line 190
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lri;->a(J)Lri;

    move-result-object v0

    invoke-virtual {v0}, Lri;->a()Lrh;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Lrh;->a()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 164
    iget-object v0, p0, Ldrz;->i:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 165
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 166
    const-string v1, "android.media.intent.extra.ITEM_ID"

    iget-object v2, p0, Ldrz;->g:Ldwq;

    invoke-interface {v2}, Ldwq;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    .line 168
    invoke-direct {p0}, Ldrz;->e()I

    move-result v2

    invoke-direct {p0, v2}, Ldrz;->c(I)Landroid/os/Bundle;

    move-result-object v2

    .line 167
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 170
    :try_start_0
    iget-object v1, p0, Ldrz;->i:Landroid/app/PendingIntent;

    iget-object v2, p0, Ldrz;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Ldrz;->i:Landroid/app/PendingIntent;
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 172
    :catch_0
    move-exception v0

    .line 173
    const-string v1, "Could not send status update"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private e()I
    .locals 3

    .prologue
    const/4 v0, 0x3

    .line 195
    sget-object v1, Ldsb;->a:[I

    iget-object v2, p0, Ldrz;->g:Ldwq;

    invoke-interface {v2}, Ldwq;->l()Ldwo;

    move-result-object v2

    invoke-virtual {v2}, Ldwo;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 216
    :goto_0
    :pswitch_0
    return v0

    .line 199
    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    .line 201
    :pswitch_2
    const/4 v0, 0x7

    goto :goto_0

    .line 203
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 205
    :pswitch_4
    const/4 v0, 0x1

    goto :goto_0

    .line 210
    :pswitch_5
    const/4 v0, 0x0

    goto :goto_0

    .line 212
    :pswitch_6
    const/16 v0, 0x439

    goto :goto_0

    .line 195
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Lse;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 116
    const-string v0, "android.media.intent.action.GET_STATUS"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-direct {p0}, Ldrz;->e()I

    move-result v0

    invoke-direct {p0, v0}, Ldrz;->c(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "android.media.intent.extra.ITEM_ID"

    iget-object v3, p0, Ldrz;->g:Ldwq;

    invoke-interface {v3}, Ldwq;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lse;->a(Landroid/os/Bundle;)V

    move v0, v1

    .line 147
    :goto_0
    return v0

    .line 120
    :cond_0
    const-string v0, "com.google.android.apps.youtube.app.remote.action.WATCH_STATUS"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    const-string v0, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Ldrz;->i:Landroid/app/PendingIntent;

    move v0, v1

    .line 123
    goto :goto_0

    .line 128
    :cond_1
    iget-object v0, p0, Ldrz;->g:Ldwq;

    invoke-interface {v0}, Ldwq;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ldrz;->g:Ldwq;

    .line 129
    invoke-interface {v0}, Ldwq;->w()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 131
    iget-object v2, p0, Ldrz;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09001f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2, v0}, Lse;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    move v0, v1

    .line 132
    goto :goto_0

    .line 134
    :cond_3
    const-string v0, "android.media.intent.action.RESUME"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135
    iget-object v0, p0, Ldrz;->g:Ldwq;

    invoke-interface {v0}, Ldwq;->a()V

    move v0, v1

    .line 136
    goto :goto_0

    .line 138
    :cond_4
    const-string v0, "android.media.intent.action.PAUSE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 139
    iget-object v0, p0, Ldrz;->g:Ldwq;

    invoke-interface {v0}, Ldwq;->b()V

    move v0, v1

    .line 140
    goto :goto_0

    .line 142
    :cond_5
    const-string v0, "android.media.intent.action.SEEK"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 143
    const-string v0, "android.media.intent.extra.ITEM_POSITION"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    long-to-int v0, v2

    .line 144
    iget-object v2, p0, Ldrz;->g:Ldwq;

    invoke-interface {v2, v0}, Ldwq;->a(I)V

    move v0, v1

    .line 145
    goto/16 :goto_0

    .line 147
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Ldrz;->d:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 153
    iget-object v0, p0, Ldrz;->g:Ldwq;

    iget-object v1, p0, Ldrz;->f:Ldwr;

    sget-object v2, Ldwj;->f:Ldwj;

    invoke-interface {v0, v1, v2}, Ldwq;->a(Ldwr;Ldwj;)V

    .line 154
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Ldrz;->h:Ldwv;

    invoke-interface {v0}, Ldwv;->a()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Ldrz;->f:Ldwr;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 159
    iget-object v1, p0, Ldrz;->g:Ldwq;

    invoke-interface {v1, v0}, Ldwq;->a(Z)V

    .line 160
    iget-object v0, p0, Ldrz;->d:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 161
    return-void
.end method

.method public final onMdxPlaybackChangedEvent(Ldwi;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 283
    invoke-direct {p0}, Ldrz;->d()V

    .line 284
    return-void
.end method

.method public final onMdxScreenDisconnecting(Ldwu;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 262
    iget-object v0, p1, Ldwu;->a:Ldwr;

    iget-object v1, p0, Ldrz;->f:Ldwr;

    invoke-virtual {v0, v1}, Ldwr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.youtube.action.mrp_screen_disconnected"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 264
    const-string v1, "pairingCode"

    iget-object v2, p0, Ldrz;->e:Ldtd;

    invoke-virtual {v2}, Ldtd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    iget-object v1, p0, Ldrz;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 267
    :cond_0
    return-void
.end method

.method public final onMdxStateChangedEvent(Ldwx;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 245
    sget-object v0, Ldsb;->b:[I

    iget-object v1, p1, Ldwx;->a:Ldww;

    invoke-virtual {v1}, Ldww;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 254
    :goto_0
    return-void

    .line 248
    :pswitch_0
    iget-object v0, p0, Ldrz;->f:Ldwr;

    invoke-virtual {v0}, Ldwr;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Ldrz;->g:Ldwq;

    iget-object v1, p0, Ldrz;->f:Ldwr;

    invoke-virtual {v1}, Ldwr;->f()Ldwf;

    move-result-object v1

    iget-object v1, v1, Ldwf;->a:Ldtm;

    invoke-interface {v0, v1}, Ldwq;->a(Ldtm;)Ldst;

    move-result-object v0

    .line 253
    :goto_1
    iget-object v1, p0, Ldrz;->e:Ldtd;

    :try_start_0
    iget-object v2, p0, Ldrz;->c:Ljava/util/concurrent/Executor;

    new-instance v3, Ldsa;

    invoke-direct {v3, p0, v1, v0}, Ldsa;-><init>(Ldrz;Ldtd;Ldst;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not register pairing code"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 251
    :cond_0
    iget-object v0, p0, Ldrz;->f:Ldwr;

    invoke-virtual {v0}, Ldwr;->e()Ldwd;

    move-result-object v0

    iget-object v0, v0, Ldwd;->a:Ldst;

    goto :goto_1

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onMdxVideoPlayerStateChangedEvent(Ldxa;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 274
    invoke-direct {p0}, Ldrz;->d()V

    .line 275
    return-void
.end method
