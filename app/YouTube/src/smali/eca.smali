.class public final Leca;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;
.implements Lebu;


# instance fields
.field private final a:Z

.field private final b:J

.field private final c:J

.field private d:Landroid/view/Choreographer;

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:Z

.field private j:J

.field private k:J

.field private l:I


# direct methods
.method public constructor <init>(FZ)V
    .locals 4

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Leca;->a:Z

    .line 47
    const-wide v0, 0x41cdcd6500000000L    # 1.0E9

    float-to-double v2, p1

    div-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Leca;->b:J

    .line 49
    iget-wide v0, p0, Leca;->b:J

    const-wide/16 v2, 0x50

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x64

    div-long/2addr v0, v2

    iput-wide v0, p0, Leca;->c:J

    .line 51
    return-void
.end method

.method private b(JJ)Z
    .locals 5

    .prologue
    .line 145
    iget-wide v0, p0, Leca;->k:J

    sub-long v0, p1, v0

    .line 146
    iget-wide v2, p0, Leca;->j:J

    sub-long v2, p3, v2

    .line 147
    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/32 v2, 0x1312d00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)J
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 82
    const-wide/16 v0, 0x3e8

    mul-long v4, p1, v0

    .line 88
    iget-boolean v0, p0, Leca;->i:Z

    if-eqz v0, :cond_4

    .line 90
    iget-wide v0, p0, Leca;->f:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 91
    iget v0, p0, Leca;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Leca;->l:I

    .line 92
    iget-wide v0, p0, Leca;->h:J

    iput-wide v0, p0, Leca;->g:J

    .line 94
    :cond_0
    iget v0, p0, Leca;->l:I

    const/4 v1, 0x6

    if-lt v0, v1, :cond_3

    .line 99
    iget-wide v0, p0, Leca;->k:J

    sub-long v0, v4, v0

    iget v2, p0, Leca;->l:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    .line 101
    iget-wide v2, p0, Leca;->g:J

    add-long/2addr v2, v0

    .line 103
    invoke-direct {p0, v2, v3, p3, p4}, Leca;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104
    iput-boolean v8, p0, Leca;->i:Z

    move-wide v0, p3

    move-wide v2, v4

    .line 119
    :goto_0
    iget-boolean v6, p0, Leca;->i:Z

    if-nez v6, :cond_1

    .line 120
    iput-wide v4, p0, Leca;->k:J

    .line 121
    iput-wide p3, p0, Leca;->j:J

    .line 122
    iput v8, p0, Leca;->l:I

    .line 123
    const/4 v4, 0x1

    iput-boolean v4, p0, Leca;->i:Z

    .line 124
    :cond_1
    iput-wide p1, p0, Leca;->f:J

    .line 128
    iput-wide v2, p0, Leca;->h:J

    .line 130
    iget-wide v2, p0, Leca;->e:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    .line 137
    :goto_1
    return-wide v0

    .line 107
    :cond_2
    iget-wide v0, p0, Leca;->j:J

    add-long/2addr v0, v2

    iget-wide v6, p0, Leca;->k:J

    sub-long/2addr v0, v6

    .line 109
    goto :goto_0

    .line 112
    :cond_3
    invoke-direct {p0, v4, v5, p3, p4}, Leca;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 113
    iput-boolean v8, p0, Leca;->i:Z

    :cond_4
    move-wide v0, p3

    move-wide v2, v4

    goto :goto_0

    .line 135
    :cond_5
    iget-wide v2, p0, Leca;->e:J

    iget-wide v4, p0, Leca;->b:J

    sub-long v6, v0, v2

    div-long/2addr v6, v4

    mul-long/2addr v6, v4

    add-long/2addr v2, v6

    cmp-long v6, v0, v2

    if-gtz v6, :cond_6

    sub-long v4, v2, v4

    :goto_2
    sub-long v6, v2, v0

    sub-long/2addr v0, v4

    cmp-long v0, v6, v0

    if-gez v0, :cond_7

    move-wide v0, v2

    .line 137
    :goto_3
    iget-wide v2, p0, Leca;->c:J

    sub-long/2addr v0, v2

    goto :goto_1

    .line 135
    :cond_6
    add-long/2addr v4, v2

    move-wide v9, v4

    move-wide v4, v2

    move-wide v2, v9

    goto :goto_2

    :cond_7
    move-wide v0, v4

    goto :goto_3
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Leca;->i:Z

    .line 59
    iget-boolean v0, p0, Leca;->a:Z

    if-eqz v0, :cond_0

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Leca;->e:J

    .line 61
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, Leca;->d:Landroid/view/Choreographer;

    .line 62
    iget-object v0, p0, Leca;->d:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 64
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Leca;->a:Z

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Leca;->d:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Leca;->d:Landroid/view/Choreographer;

    .line 72
    :cond_0
    return-void
.end method

.method public final doFrame(J)V
    .locals 5

    .prologue
    .line 76
    iput-wide p1, p0, Leca;->e:J

    .line 77
    iget-object v0, p0, Leca;->d:Landroid/view/Choreographer;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, p0, v2, v3}, Landroid/view/Choreographer;->postFrameCallbackDelayed(Landroid/view/Choreographer$FrameCallback;J)V

    .line 78
    return-void
.end method
