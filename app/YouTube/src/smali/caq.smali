.class public final Lcaq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcan;


# instance fields
.field private final a:Ljava/lang/CharSequence;

.field private final b:Landroid/view/View;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Ljava/util/Collection;


# direct methods
.method constructor <init>(Ljava/lang/CharSequence;Landroid/view/View;IIIILjava/util/Collection;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcaq;->a:Ljava/lang/CharSequence;

    .line 34
    iput-object p2, p0, Lcaq;->b:Landroid/view/View;

    .line 35
    iput p3, p0, Lcaq;->c:I

    .line 36
    iput p4, p0, Lcaq;->d:I

    .line 37
    iput p5, p0, Lcaq;->e:I

    .line 38
    iput p6, p0, Lcaq;->f:I

    .line 39
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcaq;->g:Ljava/util/Collection;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcaq;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcaq;->b:Landroid/view/View;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcaq;->c:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcaq;->d:I

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcaq;->e:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcaq;->f:I

    return v0
.end method

.method public final g()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcaq;->g:Ljava/util/Collection;

    return-object v0
.end method

.method public final h()Lcar;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcar;

    invoke-direct {v0}, Lcar;-><init>()V

    iget-object v1, p0, Lcaq;->a:Ljava/lang/CharSequence;

    .line 79
    iput-object v1, v0, Lcar;->a:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcaq;->b:Landroid/view/View;

    .line 80
    iput-object v1, v0, Lcar;->b:Landroid/view/View;

    iget v1, p0, Lcaq;->c:I

    .line 81
    iput v1, v0, Lcar;->c:I

    iget v1, p0, Lcaq;->d:I

    .line 82
    iput v1, v0, Lcar;->d:I

    iget v1, p0, Lcaq;->e:I

    .line 83
    iput v1, v0, Lcar;->e:I

    iget v1, p0, Lcaq;->f:I

    .line 84
    iput v1, v0, Lcar;->f:I

    iget-object v1, p0, Lcaq;->g:Ljava/util/Collection;

    .line 85
    invoke-virtual {v0, v1}, Lcar;->a(Ljava/util/Collection;)Lcar;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 90
    iget v0, p0, Lcaq;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcaq;->d:I

    .line 91
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcaq;->a:Ljava/lang/CharSequence;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x12

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "ab 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " sb 0x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " title "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
