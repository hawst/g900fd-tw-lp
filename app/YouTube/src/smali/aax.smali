.class public final Laax;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Ljava/util/List;

.field c:Landroid/content/Context;

.field d:Z

.field e:Z

.field f:Labb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const-wide/32 v4, 0x493e0

    const-wide/16 v6, 0x0

    const/4 v2, 0x1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Laax;->a:Ljava/lang/Object;

    .line 37
    iput-boolean v2, p0, Laax;->d:Z

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Laax;->e:Z

    .line 43
    iput-object p1, p0, Laax;->c:Landroid/content/Context;

    .line 44
    new-instance v0, Labb;

    invoke-direct {v0, p1}, Labb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laax;->f:Labb;

    .line 45
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Laax;->b:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Laba;

    invoke-direct {v1, p0}, Laba;-><init>(Laax;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 47
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v0, v2}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    .line 48
    invoke-static {p1}, Labd;->b(Landroid/content/Context;)J

    move-result-wide v2

    .line 49
    invoke-static {}, Labd;->a()J

    move-result-wide v8

    .line 50
    add-long/2addr v2, v4

    sub-long/2addr v2, v8

    .line 51
    new-instance v1, Laaz;

    invoke-direct {v1, p0}, Laaz;-><init>(Laax;)V

    cmp-long v8, v2, v6

    if-lez v8, :cond_0

    :goto_0
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 55
    return-void

    :cond_0
    move-wide v2, v6

    .line 51
    goto :goto_0
.end method


# virtual methods
.method protected final a(Laaw;)I
    .locals 7

    .prologue
    const/4 v3, 0x2

    .line 210
    const/4 v1, 0x0

    .line 211
    const-string v0, "http.agent"

    .line 212
    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Laax;->c:Landroid/content/Context;

    .line 211
    invoke-static {v0, v2}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;Landroid/content/Context;)Landroid/net/http/AndroidHttpClient;

    move-result-object v4

    .line 215
    :try_start_0
    const-string v0, "Pinging: "

    iget-object v2, p1, Laaw;->g:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 216
    :goto_0
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    iget-object v0, p1, Laaw;->g:Ljava/lang/String;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    :try_start_1
    invoke-virtual {v4, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 218
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 219
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x21

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Receive response code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 221
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    .line 223
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 229
    :cond_0
    const/16 v0, 0xc8

    if-ne v1, v0, :cond_4

    move v0, v3

    .line 230
    :goto_1
    if-ne v0, v3, :cond_1

    .line 231
    iget-boolean v1, p1, Laaw;->b:Z

    if-nez v1, :cond_1

    iget-boolean v1, p1, Laaw;->a:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Laax;->c:Landroid/content/Context;

    iget-object v3, p1, Laaw;->e:Ljava/lang/String;

    iget-object v5, p1, Laaw;->f:Ljava/lang/String;

    invoke-static {v1, v3, v5}, Labd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245
    :cond_1
    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    :goto_2
    return v0

    .line 215
    :cond_2
    :try_start_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 234
    :catch_0
    move-exception v0

    .line 235
    :goto_3
    :try_start_3
    const-string v2, "GoogleConversionReporter"

    const-string v3, "Error sending ping"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 236
    if-eqz v1, :cond_3

    .line 238
    :try_start_4
    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V
    :try_end_4
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 243
    :cond_3
    :goto_4
    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    const/4 v0, 0x0

    goto :goto_2

    .line 229
    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    .line 243
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/net/http/AndroidHttpClient;->close()V

    throw v0

    :catch_1
    move-exception v0

    goto :goto_4

    .line 234
    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Labf;ZZZ)V
    .locals 10

    .prologue
    .line 85
    new-instance v1, Laaw;

    invoke-direct {v1, p1, p2, p3, p4}, Laaw;-><init>(Ljava/lang/String;Labf;ZZ)V

    .line 87
    iget-object v2, p0, Laax;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 88
    if-nez p5, :cond_0

    .line 89
    :try_start_0
    new-instance v0, Laay;

    invoke-direct {v0, p0, v1}, Laay;-><init>(Laax;Laaw;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 95
    monitor-exit v2

    .line 104
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v3, p0, Laax;->f:Labb;

    iget-object v4, v3, Labb;->a:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Labb;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    if-nez v5, :cond_2

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 99
    :goto_1
    :try_start_2
    iget-boolean v0, p0, Laax;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Laax;->c:Landroid/content/Context;

    invoke-static {v0}, Labd;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Laax;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Laax;->d:Z

    .line 102
    iget-object v0, p0, Laax;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 104
    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 98
    :cond_2
    :try_start_3
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "string_url"

    iget-object v7, v1, Laaw;->g:Ljava/lang/String;

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "preference_key"

    iget-object v7, v1, Laaw;->f:Ljava/lang/String;

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "is_repeatable"

    iget-boolean v0, v1, Laaw;->b:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "parameter_is_null"

    iget-boolean v0, v1, Laaw;->a:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "preference_name"

    iget-object v7, v1, Laaw;->e:Ljava/lang/String;

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "record_time"

    iget-wide v8, v1, Laaw;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "retry_count"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "last_retry_time"

    iget-wide v8, v1, Laaw;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "conversiontracking"

    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    iput-wide v6, v1, Laaw;->h:J

    invoke-virtual {v3}, Labb;->b()V

    invoke-virtual {v3}, Labb;->c()I

    move-result v0

    int-to-long v6, v0

    const-wide/16 v8, 0x4e20

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    invoke-virtual {v3}, Labb;->d()V

    :cond_3
    monitor-exit v4

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method
