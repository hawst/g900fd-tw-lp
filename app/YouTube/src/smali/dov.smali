.class public final Ldov;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Ljava/util/Random;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Letc;

.field public final c:Lezs;

.field public d:Lezs;

.field private final f:Lcla;

.field private final g:Ljava/lang/String;

.field private final h:Lcyc;

.field private final i:Ljava/lang/String;

.field private final j:Lezs;

.field private final k:Lezs;

.field private final l:Lezs;

.field private final m:Lezs;

.field private final n:Lezs;

.field private final o:Lezs;

.field private final p:Lezs;

.field private final q:Lezs;

.field private final r:Lezs;

.field private final s:Lezs;

.field private final t:Lezs;

.field private final u:Lezs;

.field private final v:Lezs;

.field private final w:Lezs;

.field private final x:Lezs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Ldov;->e:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Letc;Lcla;Lcyc;)V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Ldow;

    invoke-direct {v0, p0}, Ldow;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->j:Lezs;

    .line 125
    new-instance v0, Ldpg;

    invoke-direct {v0, p0}, Ldpg;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->k:Lezs;

    .line 144
    new-instance v0, Ldph;

    invoke-direct {v0, p0}, Ldph;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->l:Lezs;

    .line 166
    new-instance v0, Ldpi;

    invoke-direct {v0, p0}, Ldpi;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->m:Lezs;

    .line 183
    new-instance v0, Ldpj;

    invoke-direct {v0, p0}, Ldpj;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->n:Lezs;

    .line 202
    new-instance v0, Ldpk;

    invoke-direct {v0, p0}, Ldpk;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->o:Lezs;

    .line 226
    new-instance v0, Ldpl;

    invoke-direct {v0, p0}, Ldpl;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->p:Lezs;

    .line 249
    new-instance v0, Ldpm;

    invoke-direct {v0, p0}, Ldpm;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->c:Lezs;

    .line 267
    new-instance v0, Ldpn;

    invoke-direct {v0, p0}, Ldpn;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->q:Lezs;

    .line 286
    new-instance v0, Ldox;

    invoke-direct {v0, p0}, Ldox;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->r:Lezs;

    .line 301
    new-instance v0, Ldoy;

    invoke-direct {v0, p0}, Ldoy;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->s:Lezs;

    .line 321
    new-instance v0, Ldoz;

    invoke-direct {v0, p0}, Ldoz;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->t:Lezs;

    .line 344
    new-instance v0, Ldpa;

    invoke-direct {v0, p0}, Ldpa;-><init>(Ldov;)V

    .line 357
    new-instance v0, Ldpb;

    invoke-direct {v0, p0}, Ldpb;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->u:Lezs;

    .line 374
    new-instance v0, Ldpc;

    invoke-direct {v0, p0}, Ldpc;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->v:Lezs;

    .line 385
    new-instance v0, Ldpd;

    invoke-direct {v0, p0}, Ldpd;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->w:Lezs;

    .line 399
    new-instance v0, Ldpe;

    invoke-direct {v0, p0}, Ldpe;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->x:Lezs;

    .line 486
    new-instance v0, Ldpf;

    invoke-direct {v0, p0}, Ldpf;-><init>(Ldov;)V

    iput-object v0, p0, Ldov;->d:Lezs;

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldov;->a:Landroid/content/Context;

    .line 95
    iput-object p2, p0, Ldov;->b:Letc;

    .line 96
    iput-object p3, p0, Ldov;->f:Lcla;

    .line 97
    invoke-interface {p4}, Lcyc;->X()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldov;->g:Ljava/lang/String;

    .line 98
    iput-object p4, p0, Ldov;->h:Lcyc;

    .line 99
    invoke-virtual {p2}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "remote_id"

    invoke-static {v0, v1}, Ldov;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldov;->i:Ljava/lang/String;

    .line 100
    return-void
.end method

.method static synthetic a(Ldov;)Letc;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldov;->b:Letc;

    return-object v0
.end method

.method public static a(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 73
    invoke-interface {p0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const-string v0, ""

    invoke-interface {p0, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 76
    :cond_0
    new-instance v0, Ljava/math/BigInteger;

    const/16 v1, 0x82

    sget-object v2, Ldov;->e:Ljava/util/Random;

    invoke-direct {v0, v1, v2}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method static synthetic b(Ldov;)Lcla;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldov;->f:Lcla;

    return-object v0
.end method

.method static synthetic c(Ldov;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldov;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Ldov;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldov;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Ldov;)Ldyd;
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ldov;->m()Ldyd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ldov;)Lcyc;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldov;->h:Lcyc;

    return-object v0
.end method

.method static synthetic g(Ldov;)Lezs;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldov;->l:Lezs;

    return-object v0
.end method

.method static synthetic h(Ldov;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldov;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Ldov;)Ldsc;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldov;->q:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsc;

    return-object v0
.end method

.method private final m()Ldyd;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Ldov;->k:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyd;

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Ldpo;
    .locals 10

    .prologue
    .line 422
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 424
    const-string v2, "android-%s-%s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Ldov;->a:Landroid/content/Context;

    .line 426
    invoke-static {v0}, La;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "tablet"

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p0, Ldov;->a:Landroid/content/Context;

    .line 427
    invoke-static {v4}, La;->r(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 424
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 428
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 429
    const-string v3, "device"

    sget-object v4, Ldsz;->a:Ldsz;

    invoke-virtual {v4}, Ldsz;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    const-string v3, "id"

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    const-string v3, "name"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    const-string v1, "app"

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    const-string v0, "mdx-version"

    const-string v1, "3"

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    iget-object v0, p0, Ldov;->h:Lcyc;

    invoke-interface {v0}, Lcyc;->ac()Lfko;

    move-result-object v0

    iget-object v0, v0, Lfko;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    const-string v0, "capabilities"

    const-string v1, ","

    iget-object v3, p0, Ldov;->h:Lcyc;

    .line 437
    invoke-interface {v3}, Lcyc;->ac()Lfko;

    move-result-object v3

    iget-object v3, v3, Lfko;->a:Ljava/util/Set;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 435
    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    :cond_0
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v9

    .line 441
    iget-object v0, p0, Ldov;->h:Lcyc;

    invoke-interface {v0}, Lcyc;->Z()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ldqx;

    iget-object v1, p0, Ldov;->a:Landroid/content/Context;

    .line 444
    invoke-static {}, La;->l()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    .line 445
    invoke-virtual {p0}, Ldov;->h()Ldqz;

    move-result-object v3

    .line 446
    invoke-direct {p0}, Ldov;->m()Ldyd;

    move-result-object v4

    const-string v5, "www.youtube.com"

    const/16 v6, 0x50

    .line 449
    invoke-virtual {p0}, Ldov;->h()Ldqz;

    move-result-object v7

    iget-object v7, v7, Ldqz;->b:Ljava/lang/String;

    .line 450
    invoke-virtual {p0}, Ldov;->j()Ldyg;

    move-result-object v8

    invoke-direct/range {v0 .. v9}, Ldqx;-><init>(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Ldqz;Ldyd;Ljava/lang/String;ILjava/lang/String;Ldyg;Ljava/util/Map;)V

    .line 458
    :goto_1
    return-object v0

    .line 426
    :cond_1
    const-string v0, "phone"

    goto/16 :goto_0

    .line 450
    :cond_2
    new-instance v2, Ldqu;

    iget-object v3, p0, Ldov;->a:Landroid/content/Context;

    const-string v4, "www.youtube.com"

    const/16 v5, 0x50

    .line 456
    invoke-virtual {p0}, Ldov;->h()Ldqz;

    move-result-object v0

    iget-object v6, v0, Ldqz;->b:Ljava/lang/String;

    .line 457
    invoke-direct {p0}, Ldov;->m()Ldyd;

    move-result-object v7

    .line 458
    invoke-virtual {p0}, Ldov;->j()Ldyg;

    move-result-object v8

    invoke-direct/range {v2 .. v9}, Ldqu;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ldyd;Ldyg;Ljava/util/Map;)V

    move-object v0, v2

    goto :goto_1
.end method

.method public final a()Lduy;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Ldov;->j:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lduy;

    return-object v0
.end method

.method public final b()Ldwq;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Ldov;->m:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    return-object v0
.end method

.method public final c()Ldxe;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Ldov;->n:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxe;

    return-object v0
.end method

.method public final d()Ldwv;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Ldov;->o:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwv;

    return-object v0
.end method

.method public final e()Ldxf;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Ldov;->p:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxf;

    return-object v0
.end method

.method public final f()Ldsl;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Ldov;->r:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsl;

    return-object v0
.end method

.method public final g()Ldaq;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Ldov;->s:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaq;

    return-object v0
.end method

.method public final h()Ldqz;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Ldov;->t:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqz;

    return-object v0
.end method

.method public final i()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 371
    iget-object v0, p0, Ldov;->u:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final j()Ldyg;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Ldov;->v:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    return-object v0
.end method

.method public final k()Ldwl;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Ldov;->w:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwl;

    return-object v0
.end method

.method public final l()Ldrw;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Ldov;->x:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldrw;

    return-object v0
.end method
