.class public Lecw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIIIILjava/lang/String;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-static {p1}, La;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lecw;->a:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lecw;->b:Ljava/lang/String;

    .line 105
    iput p3, p0, Lecw;->c:I

    .line 106
    iput p4, p0, Lecw;->d:I

    .line 107
    iput p7, p0, Lecw;->e:I

    .line 110
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 124
    if-ne p0, p1, :cond_0

    .line 125
    const/4 v0, 0x1

    .line 131
    :goto_0
    return v0

    .line 127
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 130
    :cond_2
    check-cast p1, Lecw;

    .line 131
    iget-object v0, p1, Lecw;->a:Ljava/lang/String;

    iget-object v1, p0, Lecw;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lecw;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
