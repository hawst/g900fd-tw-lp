.class public final Lesw;
.super Lgia;
.source "SourceFile"


# instance fields
.field private a:Less;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 524
    invoke-direct {p0}, Lgia;-><init>()V

    return-void
.end method

.method public constructor <init>(Less;)V
    .locals 0

    .prologue
    .line 526
    invoke-direct {p0}, Lgia;-><init>()V

    .line 527
    iput-object p1, p0, Lesw;->a:Less;

    .line 528
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 532
    const/4 v0, 0x1

    return v0
.end method

.method protected final synthetic a(Lorg/json/JSONObject;I)Ljava/lang/Object;
    .locals 19

    .prologue
    .line 503
    const/4 v2, 0x1

    move/from16 v0, p2

    if-eq v0, v2, :cond_0

    new-instance v2, Lorg/json/JSONException;

    const-string v3, "Unsupported version"

    invoke-direct {v2, v3}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v2, Less;

    new-instance v3, Lesh;

    const-string v4, "offsetType"

    const-class v5, Lesl;

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v4

    check-cast v4, Lesl;

    const-string v5, "offsetValue"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lesh;-><init>(Lesl;J)V

    const-string v4, "isLinearAdAllowed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string v5, "isNonlinearAdAllowed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    const-string v6, "isDisplayAdAllowed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    const-string v7, "adBreakId"

    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "originalVideoId"

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "requestTrackingParams"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v9

    sget-object v10, Lfoy;->al:Lfpd;

    const-string v11, "ads"

    move-object/from16 v0, p1

    invoke-virtual {v10, v0, v11}, Lfpd;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    const-string v11, "startEvents"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lesw;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    const-string v12, "endEvents"

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lesw;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    const-string v13, "errorEvents"

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lesw;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v13

    sget-object v14, Lesh;->d:Lesk;

    const-string v15, "repeatedOffsets"

    move-object/from16 v0, p1

    invoke-virtual {v14, v0, v15}, Lesk;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v14

    sget-object v15, Lesn;->e:Lesp;

    const-string v16, "trackingDecoration"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v15, v0, v1}, Lesp;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lesn;

    const-string v16, "isForOffline"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v16

    const-string v17, "allowIdfaUrlRegex"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-direct/range {v2 .. v18}, Less;-><init>(Lesh;ZZZLjava/lang/String;Ljava/lang/String;[BLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lesn;ZLjava/lang/String;B)V

    return-object v2
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 537
    const-string v0, "offsetType"

    iget-object v1, p0, Lesw;->a:Less;

    invoke-virtual {v1}, Less;->c()Lesl;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Enum;)V

    .line 538
    const-string v0, "offsetValue"

    iget-object v1, p0, Lesw;->a:Less;

    invoke-virtual {v1}, Less;->d()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 539
    const-string v0, "isLinearAdAllowed"

    iget-object v1, p0, Lesw;->a:Less;

    iget-boolean v1, v1, Less;->b:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 540
    const-string v0, "isNonlinearAdAllowed"

    iget-object v1, p0, Lesw;->a:Less;

    iget-boolean v1, v1, Less;->c:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 541
    const-string v0, "isDisplayAdAllowed"

    iget-object v1, p0, Lesw;->a:Less;

    iget-boolean v1, v1, Less;->d:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 542
    const-string v0, "adBreakId"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->e:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 543
    const-string v0, "originalVideoId"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->f:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 544
    const-string v0, "requestTrackingParams"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->g:[B

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 545
    const-string v0, "ads"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->h:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 546
    const-string v0, "startEvents"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->i:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lesw;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 547
    const-string v0, "endEvents"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->j:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lesw;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 548
    const-string v0, "errorEvents"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->k:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lesw;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 549
    const-string v0, "repeatedOffsets"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->l:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 550
    const-string v0, "trackingDecoration"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->m:Lesn;

    invoke-static {p1, v0, v1}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;Lghz;)V

    .line 551
    const-string v0, "isForOffline"

    iget-object v1, p0, Lesw;->a:Less;

    iget-boolean v1, v1, Less;->n:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 552
    const-string v0, "allowIdfaUrlRegex"

    iget-object v1, p0, Lesw;->a:Less;

    iget-object v1, v1, Less;->o:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lesw;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 553
    return-void
.end method
