.class public abstract Lejp;
.super Lejn;

# interfaces
.implements Lekg;


# instance fields
.field private final a:Lejm;

.field private c:Leke;


# direct methods
.method public constructor <init>(Lejm;)V
    .locals 1

    invoke-direct {p0}, Lejn;-><init>()V

    invoke-static {p1}, Lb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejm;

    iput-object v0, p0, Lejp;->a:Lejm;

    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lejp;->c(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method


# virtual methods
.method public abstract a(Lejk;)V
.end method

.method public final a(Leke;)V
    .locals 0

    iput-object p1, p0, Lejp;->c:Leke;

    return-void
.end method

.method protected final b()V
    .locals 1

    invoke-super {p0}, Lejn;->b()V

    iget-object v0, p0, Lejp;->c:Leke;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejp;->c:Leke;

    invoke-interface {v0, p0}, Leke;->a(Lekg;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lejp;->c:Leke;

    :cond_0
    return-void
.end method

.method public final b(Lejk;)V
    .locals 2

    iget-object v0, p0, Lejp;->b:Lejo;

    if-nez v0, :cond_0

    new-instance v0, Lejo;

    invoke-interface {p1}, Lejk;->d()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lejo;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lejn;->b:Lejo;

    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lejp;->a(Lejk;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lejp;->a(Landroid/os/RemoteException;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lejp;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method public final c()Lejm;
    .locals 1

    iget-object v0, p0, Lejp;->a:Lejm;

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Failed result must not be success"

    invoke-static {v0, v1}, Lb;->b(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lejp;->a(Lcom/google/android/gms/common/api/Status;)Lejw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lejp;->a(Lejw;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
