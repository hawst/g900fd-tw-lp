.class public final Lios;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final j:Ljava/util/concurrent/ExecutorService;

.field private static synthetic w:Z


# instance fields
.field public final a:Lima;

.field final b:Z

.field c:J

.field d:J

.field public final e:Lioo;

.field final f:Lioo;

.field final g:Lipi;

.field final h:Ljava/net/Socket;

.field public final i:Linu;

.field private final k:Lioh;

.field private final l:Ljava/util/Map;

.field private final m:Ljava/lang/String;

.field private n:I

.field private o:I

.field private p:Z

.field private q:J

.field private final r:Ljava/util/concurrent/ExecutorService;

.field private final s:Liom;

.field private t:Z

.field private u:Lipb;

.field private final v:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 53
    const-class v0, Lios;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    sput-boolean v0, Lios;->w:Z

    .line 67
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, 0x7fffffff

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    const-string v0, "OkHttp SpdyConnection"

    invoke-static {v0, v8}, Limo;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v1, Lios;->j:Ljava/util/concurrent/ExecutorService;

    return-void

    :cond_0
    move v0, v2

    .line 53
    goto :goto_0
.end method

.method public constructor <init>(Lipa;)V
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v10, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lios;->l:Ljava/util/Map;

    .line 87
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lios;->q:J

    .line 103
    iput-wide v4, p0, Lios;->c:J

    .line 114
    new-instance v0, Lioo;

    invoke-direct {v0}, Lioo;-><init>()V

    iput-object v0, p0, Lios;->e:Lioo;

    .line 120
    new-instance v0, Lioo;

    invoke-direct {v0}, Lioo;-><init>()V

    iput-object v0, p0, Lios;->f:Lioo;

    .line 122
    iput-boolean v2, p0, Lios;->t:Z

    .line 792
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lios;->v:Ljava/util/Set;

    .line 131
    iget-object v0, p1, Lipa;->d:Lima;

    iput-object v0, p0, Lios;->a:Lima;

    .line 132
    iget-object v0, p1, Lipa;->e:Liom;

    iput-object v0, p0, Lios;->s:Liom;

    .line 133
    iget-boolean v0, p1, Lipa;->f:Z

    iput-boolean v0, p0, Lios;->b:Z

    .line 134
    iget-object v0, p1, Lipa;->c:Lioh;

    iput-object v0, p0, Lios;->k:Lioh;

    .line 136
    iget-boolean v0, p1, Lipa;->f:Z

    if-eqz v0, :cond_2

    move v0, v3

    :goto_0
    iput v0, p0, Lios;->o:I

    .line 137
    iget-boolean v0, p1, Lipa;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lios;->a:Lima;

    sget-object v1, Lima;->d:Lima;

    if-ne v0, v1, :cond_0

    .line 138
    iget v0, p0, Lios;->o:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lios;->o:I

    .line 141
    :cond_0
    iget-boolean v0, p1, Lipa;->f:Z

    .line 147
    iget-boolean v0, p1, Lipa;->f:Z

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lios;->e:Lioo;

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v10, v2, v1}, Lioo;->a(III)Lioo;

    .line 151
    :cond_1
    iget-object v0, p1, Lipa;->a:Ljava/lang/String;

    iput-object v0, p0, Lios;->m:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lios;->a:Lima;

    sget-object v1, Lima;->d:Lima;

    if-ne v0, v1, :cond_3

    .line 154
    new-instance v0, Lioa;

    invoke-direct {v0}, Lioa;-><init>()V

    iput-object v0, p0, Lios;->g:Lipi;

    .line 156
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const-string v0, "OkHttp %s Push Observer"

    new-array v8, v3, [Ljava/lang/Object;

    iget-object v9, p0, Lios;->m:Ljava/lang/String;

    aput-object v9, v8, v2

    invoke-static {v0, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Limo;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lios;->r:Ljava/util/concurrent/ExecutorService;

    .line 161
    iget-object v0, p0, Lios;->f:Lioo;

    const v1, 0xffff

    invoke-virtual {v0, v10, v2, v1}, Lioo;->a(III)Lioo;

    .line 162
    iget-object v0, p0, Lios;->f:Lioo;

    const/4 v1, 0x5

    const/16 v3, 0x4000

    invoke-virtual {v0, v1, v2, v3}, Lioo;->a(III)Lioo;

    .line 169
    :goto_1
    iget-object v0, p0, Lios;->f:Lioo;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Lioo;->c(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lios;->d:J

    .line 170
    iget-object v0, p1, Lipa;->b:Ljava/net/Socket;

    iput-object v0, p0, Lios;->h:Ljava/net/Socket;

    .line 171
    iget-object v0, p0, Lios;->g:Lipi;

    iget-object v1, p1, Lipa;->b:Ljava/net/Socket;

    invoke-static {v1}, Liqa;->a(Ljava/net/Socket;)Liqk;

    move-result-object v1

    invoke-static {v1}, Liqa;->a(Liqk;)Lipt;

    move-result-object v1

    iget-boolean v2, p0, Lios;->b:Z

    invoke-interface {v0, v1, v2}, Lipi;->a(Lipt;Z)Linu;

    move-result-object v0

    iput-object v0, p0, Lios;->i:Linu;

    .line 173
    new-instance v0, Lipb;

    invoke-direct {v0, p0}, Lipb;-><init>(Lios;)V

    iput-object v0, p0, Lios;->u:Lipb;

    .line 174
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lios;->u:Lipb;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 175
    return-void

    .line 136
    :cond_2
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 163
    :cond_3
    iget-object v0, p0, Lios;->a:Lima;

    sget-object v1, Lima;->c:Lima;

    if-ne v0, v1, :cond_4

    .line 164
    new-instance v0, Liop;

    invoke-direct {v0}, Liop;-><init>()V

    iput-object v0, p0, Lios;->g:Lipi;

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lios;->r:Ljava/util/concurrent/ExecutorService;

    goto :goto_1

    .line 167
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lios;->a:Lima;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method static synthetic a(Lios;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lios;->m:Ljava/lang/String;

    return-object v0
.end method

.method private a(Linr;Linr;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 447
    sget-boolean v1, Lios;->w:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 450
    :cond_0
    :try_start_0
    iget-object v3, p0, Lios;->i:Linu;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-boolean v1, p0, Lios;->p:Z

    if-eqz v1, :cond_2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v1, v0

    .line 456
    :goto_0
    monitor-enter p0

    .line 458
    :try_start_4
    iget-object v3, p0, Lios;->l:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 459
    iget-object v0, p0, Lios;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v3, p0, Lios;->l:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    new-array v3, v3, [Lipe;

    invoke-interface {v0, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lipe;

    .line 460
    iget-object v3, p0, Lios;->l:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 461
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lios;->a(Z)V

    move-object v3, v0

    .line 463
    :goto_1
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 469
    if-eqz v3, :cond_4

    .line 470
    array-length v4, v3

    move-object v0, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v1, v3, v2

    .line 472
    :try_start_5
    invoke-virtual {v1, p2}, Lipe;->a(Linr;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 470
    :cond_1
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 450
    :cond_2
    const/4 v1, 0x1

    :try_start_6
    iput-boolean v1, p0, Lios;->p:Z

    iget v1, p0, Lios;->n:I

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    iget-object v4, p0, Lios;->i:Linu;

    sget-object v5, Limo;->a:[B

    invoke-interface {v4, v1, p1, v5}, Linu;->a(ILinr;[B)V

    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v1, v0

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    throw v1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    .line 453
    :catch_0
    move-exception v1

    goto :goto_0

    .line 463
    :catchall_2
    move-exception v0

    :try_start_b
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v0

    .line 473
    :catch_1
    move-exception v1

    .line 474
    if-eqz v0, :cond_1

    move-object v0, v1

    goto :goto_3

    :cond_3
    move-object v1, v0

    .line 479
    :cond_4
    :try_start_c
    iget-object v0, p0, Lios;->i:Linu;

    invoke-interface {v0}, Linu;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    move-object v0, v1

    .line 494
    :cond_5
    :goto_4
    :try_start_d
    iget-object v1, p0, Lios;->h:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3

    .line 499
    :goto_5
    if-eqz v0, :cond_6

    throw v0

    .line 488
    :catch_2
    move-exception v0

    .line 489
    if-eqz v1, :cond_5

    move-object v0, v1

    goto :goto_4

    .line 500
    :cond_6
    return-void

    .line 495
    :catch_3
    move-exception v0

    goto :goto_5

    :cond_7
    move-object v3, v0

    goto :goto_1
.end method

.method static synthetic a(Lios;ILinr;)V
    .locals 7

    .prologue
    .line 53
    iget-object v6, p0, Lios;->r:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lioz;

    const-string v2, "OkHttp %s Push Reset[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lios;->m:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lioz;-><init>(Lios;Ljava/lang/String;[Ljava/lang/Object;ILinr;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method static synthetic a(Lios;ILipu;IZ)V
    .locals 9

    .prologue
    .line 53
    new-instance v5, Lipq;

    invoke-direct {v5}, Lipq;-><init>()V

    int-to-long v0, p3

    invoke-interface {p2, v0, v1}, Lipu;->a(J)V

    int-to-long v0, p3

    invoke-interface {p2, v5, v0, v1}, Lipu;->b(Lipq;J)J

    iget-wide v0, v5, Lipq;->b:J

    int-to-long v2, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    iget-wide v2, v5, Lipq;->b:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x23

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v8, p0, Lios;->r:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lioy;

    const-string v2, "OkHttp %s Push Data[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lios;->m:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lioy;-><init>(Lios;Ljava/lang/String;[Ljava/lang/Object;ILipq;IZ)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method static synthetic a(Lios;ILjava/util/List;)V
    .locals 7

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lios;->v:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Linr;->b:Linr;

    invoke-virtual {p0, p1, v0}, Lios;->a(ILinr;)V

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lios;->v:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v6, p0, Lios;->r:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Liow;

    const-string v2, "OkHttp %s Push Request[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lios;->m:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Liow;-><init>(Lios;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lios;ILjava/util/List;Z)V
    .locals 8

    .prologue
    .line 53
    iget-object v7, p0, Lios;->r:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Liox;

    const-string v2, "OkHttp %s Push Headers[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lios;->m:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Liox;-><init>(Lios;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;Z)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method static synthetic a(Lios;Linr;Linr;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lios;->a(Linr;Linr;)V

    return-void
.end method

.method static synthetic a(Lios;ZIILcw;)V
    .locals 2

    .prologue
    .line 53
    iget-object v1, p0, Lios;->i:Linu;

    monitor-enter v1

    if-eqz p4, :cond_0

    :try_start_0
    invoke-virtual {p4}, Lcw;->s()V

    :cond_0
    iget-object v0, p0, Lios;->i:Linu;

    invoke-interface {v0, p1, p2, p3}, Linu;->a(ZII)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 203
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lios;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    monitor-exit p0

    return-void

    .line 203
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lios;I)Z
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lios;->a:Lima;

    sget-object v1, Lima;->d:Lima;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lios;Z)Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lios;->t:Z

    return v0
.end method

.method static synthetic b(Lios;I)I
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lios;->n:I

    return p1
.end method

.method static synthetic b(Lios;ZIILcw;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    .line 53
    const/4 v7, 0x0

    sget-object v8, Lios;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Liov;

    const-string v2, "OkHttp %s ping %08x%08x"

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v5, p0, Lios;->m:Ljava/lang/String;

    aput-object v5, v3, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v1

    move-object v1, p0

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v7}, Liov;-><init>(Lios;Ljava/lang/String;[Ljava/lang/Object;ZIILcw;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method static synthetic b(Lios;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lios;->p:Z

    return v0
.end method

.method static synthetic b(Lios;Z)Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lios;->p:Z

    return v0
.end method

.method static synthetic c(Lios;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lios;->n:I

    return v0
.end method

.method private declared-synchronized c(I)Lcw;
    .locals 1

    .prologue
    .line 409
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0
.end method

.method static synthetic c(Lios;I)Lcw;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lios;->c(I)Lcw;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lios;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lios;->o:I

    return v0
.end method

.method static synthetic d()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lios;->j:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic e(Lios;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lios;->l:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lios;)Lioh;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lios;->k:Lioh;

    return-object v0
.end method

.method static synthetic g(Lios;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lios;->t:Z

    return v0
.end method

.method static synthetic h(Lios;)Liom;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lios;->s:Liom;

    return-object v0
.end method

.method static synthetic i(Lios;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lios;->v:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method final declared-synchronized a(I)Lipe;
    .locals 2

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lios;->l:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lipe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(ILjava/util/List;ZZ)Lipe;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 249
    if-nez p3, :cond_0

    move v3, v4

    .line 250
    :goto_0
    if-nez p4, :cond_1

    .line 254
    :goto_1
    iget-object v8, p0, Lios;->i:Linu;

    monitor-enter v8

    .line 255
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 256
    :try_start_1
    iget-boolean v0, p0, Lios;->p:Z

    if-eqz v0, :cond_2

    .line 257
    new-instance v0, Ljava/io/IOException;

    const-string v1, "shutdown"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 270
    :catchall_1
    move-exception v0

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_0
    move v3, v0

    .line 249
    goto :goto_0

    :cond_1
    move v4, v0

    .line 250
    goto :goto_1

    .line 259
    :cond_2
    :try_start_3
    iget v1, p0, Lios;->o:I

    .line 260
    iget v0, p0, Lios;->o:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lios;->o:I

    .line 261
    new-instance v0, Lipe;

    move-object v2, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lipe;-><init>(ILios;ZZLjava/util/List;)V

    .line 262
    invoke-virtual {v0}, Lipe;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 263
    iget-object v2, p0, Lios;->l:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lios;->a(Z)V

    .line 266
    :cond_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 267
    :try_start_4
    iget-object v2, p0, Lios;->i:Linu;

    const/4 v6, 0x0

    move v5, v1

    move-object v7, p2

    invoke-interface/range {v2 .. v7}, Linu;->a(ZZIILjava/util/List;)V

    .line 270
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 277
    if-nez p3, :cond_4

    .line 278
    iget-object v1, p0, Lios;->i:Linu;

    invoke-interface {v1}, Linu;->b()V

    .line 281
    :cond_4
    return-object v0
.end method

.method final a(IJ)V
    .locals 8

    .prologue
    .line 357
    sget-object v0, Lios;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Liou;

    const-string v3, "OkHttp Window Update %s stream %d"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v5, p0, Lios;->m:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    move-object v2, p0

    move v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Liou;-><init>(Lios;Ljava/lang/String;[Ljava/lang/Object;IJ)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 365
    return-void
.end method

.method final a(ILinr;)V
    .locals 7

    .prologue
    .line 342
    sget-object v6, Lios;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Liot;

    const-string v2, "OkHttp %s stream %d"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lios;->m:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Liot;-><init>(Lios;Ljava/lang/String;[Ljava/lang/Object;ILinr;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 350
    return-void
.end method

.method public final a(IZLipq;J)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const-wide/16 v8, 0x0

    .line 306
    cmp-long v0, p4, v8

    if-nez v0, :cond_2

    .line 307
    iget-object v0, p0, Lios;->i:Linu;

    invoke-interface {v0, p2, p1, p3, v1}, Linu;->a(ZILipq;I)V

    .line 330
    :cond_0
    return-void

    .line 322
    :cond_1
    :try_start_0
    iget-wide v2, p0, Lios;->d:J

    invoke-static {p4, p5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v0, v2

    .line 323
    iget-object v2, p0, Lios;->i:Linu;

    invoke-interface {v2}, Linu;->c()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 324
    iget-wide v4, p0, Lios;->d:J

    int-to-long v6, v2

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lios;->d:J

    .line 325
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    int-to-long v4, v2

    sub-long/2addr p4, v4

    .line 328
    iget-object v3, p0, Lios;->i:Linu;

    if-eqz p2, :cond_3

    cmp-long v0, p4, v8

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v3, v0, p1, p3, v2}, Linu;->a(ZILipq;I)V

    .line 311
    :cond_2
    cmp-long v0, p4, v8

    if-lez v0, :cond_0

    .line 313
    monitor-enter p0

    .line 315
    :goto_1
    :try_start_1
    iget-wide v2, p0, Lios;->d:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_1

    .line 316
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 319
    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 325
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    .line 328
    goto :goto_0
.end method

.method public final declared-synchronized a()Z
    .locals 4

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lios;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()J
    .locals 2

    .prologue
    .line 216
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lios;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(I)Lipe;
    .locals 2

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lios;->l:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lipe;

    .line 196
    if-eqz v0, :cond_0

    iget-object v1, p0, Lios;->l:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lios;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :cond_0
    monitor-exit p0

    return-object v0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final b(ILinr;)V
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lios;->i:Linu;

    invoke-interface {v0, p1, p2}, Linu;->a(ILinr;)V

    .line 354
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lios;->i:Linu;

    invoke-interface {v0}, Linu;->b()V

    .line 414
    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 443
    sget-object v0, Linr;->a:Linr;

    sget-object v1, Linr;->h:Linr;

    invoke-direct {p0, v0, v1}, Lios;->a(Linr;Linr;)V

    .line 444
    return-void
.end method
