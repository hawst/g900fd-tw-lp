.class public final Ldra;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldrc;


# instance fields
.field public volatile a:Ldrb;

.field private final b:Ldrc;


# direct methods
.method public constructor <init>(Ldrc;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Ldra;->b:Ldrc;

    .line 26
    new-instance v0, Ldrb;

    invoke-direct {v0}, Ldrb;-><init>()V

    iput-object v0, p0, Ldra;->a:Ldrb;

    .line 27
    return-void
.end method

.method private declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldra;->a:Ldrb;

    invoke-virtual {v0, p1}, Ldrb;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Ldra;->a:Ldrb;

    iget-object v1, p0, Ldra;->b:Ldrc;

    invoke-interface {v1, p1, p2}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iget-object v2, v0, Ldrb;->b:Ldq;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, v0, Ldrb;->b:Ldq;

    invoke-virtual {v0, p1, v1}, Ldq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    :cond_0
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 57
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ldth;)Ldst;
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ldra;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Ldra;->b:Ldrc;

    invoke-interface {v0, p1, p2, p3}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldth;)Ldst;

    .line 53
    iget-object v0, p0, Ldra;->a:Ldrb;

    invoke-virtual {v0, p1, p3}, Ldrb;->a(Ljava/lang/String;Ldth;)Ldst;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldth;Ljava/lang/String;)Ldst;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ldra;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Ldra;->b:Ldrc;

    invoke-interface {v0, p1, p2, p3, p4}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldth;Ljava/lang/String;)Ldst;

    .line 46
    iget-object v0, p0, Ldra;->a:Ldrb;

    invoke-virtual {v0, p1, p3, p4}, Ldrb;->a(Ljava/lang/String;Ldth;Ljava/lang/String;)Ldst;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ldra;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Ldra;->a:Ldrb;

    invoke-virtual {v0, p1}, Ldrb;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldst;)V
    .locals 3

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ldra;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Ldra;->b:Ldrc;

    invoke-interface {v0, p1, p2, p3}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldst;)V

    .line 39
    iget-object v0, p0, Ldra;->a:Ldrb;

    iget-object v1, v0, Ldrb;->b:Ldq;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Ldrb;->b:Ldq;

    invoke-virtual {v0, p1}, Ldq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    sget-object v0, Ldrb;->a:Ljava/lang/String;

    const-string v2, "Cache is not initialized. Add operation has been refused."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    invoke-interface {v0, v2, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
