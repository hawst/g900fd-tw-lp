.class final Lgug;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private synthetic a:Ljava/lang/String;

.field private synthetic b:Lgty;

.field private synthetic c:Lguf;


# direct methods
.method constructor <init>(Lguf;Ljava/lang/String;Lgty;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lgug;->c:Lguf;

    iput-object p2, p0, Lgug;->a:Ljava/lang/String;

    iput-object p3, p0, Lgug;->b:Lgty;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lgtz;
    .locals 5

    .prologue
    .line 42
    new-instance v2, Lgtz;

    iget-object v0, p0, Lgug;->c:Lguf;

    iget-object v1, p0, Lgug;->a:Ljava/lang/String;

    iget-object v3, p0, Lgug;->b:Lgty;

    invoke-direct {v2, v0, v1, v3}, Lgtz;-><init>(Landroid/content/Context;Ljava/lang/String;Lgty;)V

    .line 46
    iget-object v0, v2, Lgtz;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    :try_start_0
    new-instance v0, Lgua;

    iget-object v1, v2, Lgtz;->a:Landroid/content/Context;

    iget-object v3, v2, Lgtz;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lgua;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lgua;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, v2, Lgtz;->c:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    invoke-virtual {v2}, Lgtz;->a()Ljava/util/Map;

    move-result-object v0

    .line 48
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 49
    iget-object v4, p0, Lgug;->c:Lguf;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v4, v1, v0}, Lguf;->a(Lguf;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lgub;

    const-string v2, "Could not open the database"

    invoke-direct {v1, v2, v0}, Lgub;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 51
    :cond_1
    return-object v2
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lgug;->a()Lgtz;

    move-result-object v0

    return-object v0
.end method
