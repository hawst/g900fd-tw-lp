.class public final Lbxq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbog;


# instance fields
.field final a:Lfhz;

.field final b:Levn;

.field c:Lfjh;

.field private final d:Landroid/view/ViewStub;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private final h:Leyp;

.field private i:Z

.field private j:Lfvi;

.field private k:Lfnc;

.field private l:Z

.field private m:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Levn;Leyp;Lfhz;Landroid/view/ViewStub;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p3, p0, Lbxq;->a:Lfhz;

    .line 56
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lbxq;->d:Landroid/view/ViewStub;

    .line 57
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbxq;->h:Leyp;

    .line 58
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lbxq;->b:Levn;

    .line 59
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lbxq;->j:Lfvi;

    invoke-virtual {v0}, Lfvi;->a()V

    .line 171
    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 173
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lfoy;Lfnx;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 119
    iget-object v2, p1, Lfoy;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 122
    :cond_1
    iget-object v2, p2, Lfnx;->j:Lfjh;

    if-nez v2, :cond_2

    iget-object v2, p2, Lfnx;->a:Libi;

    iget-object v2, v2, Libi;->f:Lhee;

    if-eqz v2, :cond_2

    iget-object v2, p2, Lfnx;->a:Libi;

    iget-object v2, v2, Libi;->f:Lhee;

    iget-object v2, v2, Lhee;->a:Lhed;

    if-eqz v2, :cond_2

    new-instance v2, Lfjh;

    iget-object v3, p2, Lfnx;->a:Libi;

    iget-object v3, v3, Libi;->f:Lhee;

    iget-object v3, v3, Lhee;->a:Lhed;

    invoke-direct {v2, v3}, Lfjh;-><init>(Lhed;)V

    iput-object v2, p2, Lfnx;->j:Lfjh;

    :cond_2
    iget-object v2, p2, Lfnx;->j:Lfjh;

    iput-object v2, p0, Lbxq;->c:Lfjh;

    .line 123
    iget-object v2, p0, Lbxq;->c:Lfjh;

    if-eqz v2, :cond_0

    .line 127
    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    if-nez v0, :cond_3

    iget-object v0, p0, Lbxq;->d:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbxq;->e:Landroid/view/View;

    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    const v2, 0x7f0800c0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbxq;->m:Landroid/widget/ImageView;

    iget-object v0, p0, Lbxq;->m:Landroid/widget/ImageView;

    new-instance v2, Lbxr;

    invoke-direct {v2, p0}, Lbxr;-><init>(Lbxq;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    new-instance v2, Lfvi;

    iget-object v0, p0, Lbxq;->h:Leyp;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iget-object v3, p0, Lbxq;->m:Landroid/widget/ImageView;

    invoke-direct {v2, v0, v3, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;Z)V

    iput-object v2, p0, Lbxq;->j:Lfvi;

    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    const v2, 0x7f08008b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbxq;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    const v2, 0x7f0800c5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbxq;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    new-instance v2, Lbxs;

    invoke-direct {v2, p0}, Lbxs;-><init>(Lbxq;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lbxq;->h()V

    .line 128
    :cond_3
    iget-object v0, p0, Lbxq;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lbxq;->c:Lfjh;

    iget-object v3, v2, Lfjh;->c:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v2, Lfjh;->a:Lhed;

    iget-object v3, v3, Lhed;->a:Lhgz;

    if-eqz v3, :cond_4

    iget-object v3, v2, Lfjh;->a:Lhed;

    iget-object v3, v3, Lhed;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lfjh;->c:Ljava/lang/String;

    :cond_4
    iget-object v2, v2, Lfjh;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v0, p0, Lbxq;->g:Landroid/widget/TextView;

    iget-object v2, p0, Lbxq;->c:Lfjh;

    iget-object v3, v2, Lfjh;->d:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v2, Lfjh;->a:Lhed;

    iget-object v3, v3, Lhed;->b:Lhgz;

    if-eqz v3, :cond_5

    iget-object v3, v2, Lfjh;->a:Lhed;

    iget-object v3, v3, Lhed;->b:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lfjh;->d:Ljava/lang/String;

    :cond_5
    iget-object v2, v2, Lfjh;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lbxq;->c:Lfjh;

    iget-object v2, v0, Lfjh;->b:Lfnc;

    if-nez v2, :cond_6

    new-instance v2, Lfnc;

    iget-object v3, v0, Lfjh;->a:Lhed;

    iget-object v3, v3, Lhed;->c:Lhxf;

    invoke-direct {v2, v3}, Lfnc;-><init>(Lhxf;)V

    iput-object v2, v0, Lfjh;->b:Lfnc;

    :cond_6
    iget-object v0, v0, Lfjh;->b:Lfnc;

    iput-object v0, p0, Lbxq;->k:Lfnc;

    .line 131
    iput-boolean v1, p0, Lbxq;->l:Z

    move v0, v1

    .line 132
    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxq;->i:Z

    .line 141
    invoke-virtual {p0}, Lbxq;->g()V

    .line 142
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 154
    iput-boolean v0, p0, Lbxq;->i:Z

    .line 155
    iput-object v1, p0, Lbxq;->c:Lfjh;

    .line 156
    iput-object v1, p0, Lbxq;->k:Lfnc;

    .line 157
    iput-boolean v0, p0, Lbxq;->l:Z

    .line 158
    invoke-direct {p0}, Lbxq;->h()V

    .line 159
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 146
    invoke-virtual {p0}, Lbxq;->g()V

    .line 147
    return-void
.end method

.method g()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 186
    iget-boolean v0, p0, Lbxq;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxq;->c:Lfjh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lbxq;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 189
    :cond_0
    iget-boolean v0, p0, Lbxq;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbxq;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbxq;->m:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lbxq;->m:Landroid/widget/ImageView;

    .line 190
    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lbxq;->k:Lfnc;

    if-eqz v0, :cond_1

    .line 191
    iput-boolean v1, p0, Lbxq;->l:Z

    .line 196
    iget-object v0, p0, Lbxq;->j:Lfvi;

    iget-object v1, p0, Lbxq;->k:Lfnc;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    .line 198
    :cond_1
    return-void
.end method
