.class final Ldhz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldiy;


# instance fields
.field private final a:Ldix;

.field private b:J

.field private final c:Levn;

.field private d:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method constructor <init>(Ldix;Levn;)V
    .locals 2

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldix;

    iput-object v0, p0, Ldhz;->a:Ldix;

    .line 177
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldhz;->b:J

    .line 178
    iput-object p2, p0, Ldhz;->c:Levn;

    .line 179
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ldhz;->d:Ljava/util/concurrent/atomic/AtomicReference;

    .line 180
    return-void
.end method

.method private handleAdCompleteEvent(Lcyu;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Ldhz;->d:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Ldhz;->c:Levn;

    new-instance v1, Ldii;

    sget-object v2, Ldih;->a:Ldih;

    invoke-direct {v1, v2}, Ldii;-><init>(Ldih;)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Ldhz;->a:Ldix;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldix;->a(Z)V

    goto :goto_0
.end method

.method private handleSequencerEndedEvent(Lczs;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Ldhz;->d:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Ldhz;->c:Levn;

    new-instance v1, Ldii;

    sget-object v2, Ldih;->b:Ldih;

    invoke-direct {v1, v2}, Ldii;-><init>(Ldih;)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 220
    iget-object v0, p0, Ldhz;->a:Ldix;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldix;->a(Z)V

    goto :goto_0
.end method

.method private handleVideoStageEvent(Ldac;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 225
    iget-object v0, p1, Ldac;->a:Lgol;

    invoke-virtual {v0}, Lgol;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    iget-object v0, p0, Ldhz;->d:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Ldhz;->c:Levn;

    new-instance v1, Ldii;

    sget-object v2, Ldih;->b:Ldih;

    invoke-direct {v1, v2}, Ldii;-><init>(Ldih;)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 234
    iget-object v0, p0, Ldhz;->a:Ldix;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldix;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfqj;J)V
    .locals 6

    .prologue
    .line 185
    const/4 v0, 0x3

    shl-long v0, p2, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-object v2, p1, Lfqj;->a:Lhgy;

    iget v2, v2, Lhgy;->e:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    .line 187
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p0, Ldhz;->b:J

    sub-long v4, v0, v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 188
    iget-object v2, p0, Ldhz;->c:Levn;

    new-instance v3, Ldil;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 190
    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    invoke-direct {v3, p1, v4, v5}, Ldil;-><init>(Lfqj;J)V

    .line 188
    invoke-virtual {v2, v3}, Levn;->c(Ljava/lang/Object;)V

    .line 191
    iput-wide v0, p0, Ldhz;->b:J

    .line 193
    :cond_0
    return-void
.end method
