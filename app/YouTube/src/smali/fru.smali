.class public final Lfru;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgha;


# instance fields
.field private final a:Levn;


# direct methods
.method public constructor <init>(Levn;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lfru;->a:Levn;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lhtx;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 29
    iget-object v3, p1, Lhtx;->b:[Lhuu;

    .line 31
    if-eqz v3, :cond_3

    array-length v0, v3

    if-lez v0, :cond_3

    .line 32
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 33
    iget v5, v0, Lhuu;->b:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 34
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 35
    iget-object v6, v0, Lhuu;->c:[Lhld;

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    .line 36
    iget-object v9, v8, Lhld;->b:Ljava/lang/String;

    iget-object v8, v8, Lhld;->c:Ljava/lang/String;

    invoke-interface {v5, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 38
    :cond_0
    iget-object v0, p0, Lfru;->a:Levn;

    new-instance v6, Lfrs;

    invoke-direct {v6, v5}, Lfrs;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v6}, Levn;->d(Ljava/lang/Object;)V

    .line 32
    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 39
    :cond_2
    iget v5, v0, Lhuu;->b:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 41
    iget-object v5, p0, Lfru;->a:Levn;

    new-instance v6, Lfrt;

    iget-object v0, v0, Lhuu;->c:[Lhld;

    invoke-direct {v6, v0}, Lfrt;-><init>([Lhld;)V

    invoke-virtual {v5, v6}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_2

    .line 45
    :cond_3
    return-void
.end method
