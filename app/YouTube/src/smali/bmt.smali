.class public final Lbmt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ldwq;

.field private final b:Lbmx;

.field private final c:Lbms;

.field private final d:Lbmw;

.field private e:I


# direct methods
.method public constructor <init>(Ldwq;Lbmx;Lbms;Lbmw;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Lbmt;->a:Ldwq;

    .line 42
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmx;

    iput-object v0, p0, Lbmt;->b:Lbmx;

    .line 43
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbms;

    iput-object v0, p0, Lbmt;->c:Lbms;

    .line 44
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmw;

    iput-object v0, p0, Lbmt;->d:Lbmw;

    .line 46
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbmt;->a(I)V

    .line 47
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lbmt;->a:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    invoke-virtual {v0}, Ldwr;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 153
    iget v0, p0, Lbmt;->e:I

    if-ne v0, p1, :cond_0

    .line 158
    :goto_0
    return-void

    .line 156
    :cond_0
    iput p1, p0, Lbmt;->e:I

    .line 157
    iget-object v3, p0, Lbmt;->c:Lbms;

    iget v0, p0, Lbmt;->e:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lbms;->setVisibility(I)V

    iget-object v3, p0, Lbmt;->d:Lbmw;

    iget v0, p0, Lbmt;->e:I

    const/4 v4, 0x1

    if-eq v0, v4, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lbmw;->setVisibility(I)V

    iget-object v0, p0, Lbmt;->b:Lbmx;

    iget v3, p0, Lbmt;->e:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Lbmx;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method


# virtual methods
.method public final handleMdxStateChangedEvent(Ldwx;)V
    .locals 7
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v6, 0x3

    .line 51
    iget-object v0, p1, Ldwx;->a:Ldww;

    sget-object v1, Lbmv;->a:[I

    invoke-virtual {v0}, Ldww;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 52
    :goto_0
    return-void

    .line 51
    :pswitch_0
    iget-object v0, p0, Lbmt;->b:Lbmx;

    iget-object v1, v0, Lbmx;->a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-virtual {v0}, Lbmx;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0902f4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Lbmx;->a(I)V

    invoke-direct {p0, v6}, Lbmt;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lbmt;->c:Lbms;

    invoke-direct {p0}, Lbmt;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbms;->a(Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lbmt;->a(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lbmt;->a:Ldwq;

    invoke-interface {v0}, Ldwq;->j()Ldwh;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Ldwh;->k:Z

    iget-object v2, p0, Lbmt;->b:Lbmx;

    iget v3, v0, Ldwh;->j:I

    new-instance v4, Lbmu;

    invoke-direct {v4, p0, v0, v1}, Lbmu;-><init>(Lbmt;Ldwh;Z)V

    iget-object v0, v2, Lbmx;->a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-virtual {v2}, Lbmx;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v1, :cond_1

    iget-object v0, v2, Lbmx;->b:Landroid/widget/Button;

    invoke-virtual {v2}, Lbmx;->getContext()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f0900a4

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, v2, Lbmx;->b:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v1, :cond_2

    const/4 v0, 0x5

    :goto_2
    invoke-virtual {v2, v0}, Lbmx;->a(I)V

    :cond_0
    invoke-direct {p0, v6}, Lbmt;->a(I)V

    goto :goto_0

    :cond_1
    iget-object v0, v2, Lbmx;->b:Landroid/widget/Button;

    invoke-virtual {v2}, Lbmx;->getContext()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f0900a2

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x4

    goto :goto_2

    :pswitch_3
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbmt;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final handleVideoStageEvent(Ldac;)V
    .locals 5
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 113
    iget-object v0, p0, Lbmt;->a:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    invoke-virtual {v0}, Ldww;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    sget-object v0, Lbmv;->b:[I

    iget-object v1, p1, Ldac;->a:Lgol;

    invoke-virtual {v1}, Lgol;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 118
    :pswitch_0
    iget-object v0, p1, Ldac;->d:Lfoy;

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lbmt;->b:Lbmx;

    iget-object v1, v0, Lbmx;->a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-virtual {v0}, Lbmx;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090302

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Lbmx;->a(I)V

    .line 121
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lbmt;->a(I)V

    goto :goto_0

    .line 125
    :pswitch_1
    iget-object v0, p0, Lbmt;->c:Lbms;

    invoke-direct {p0}, Lbmt;->a()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f090309

    invoke-virtual {v0, v2, v1}, Lbms;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbms;->setText(Ljava/lang/CharSequence;)V

    .line 126
    invoke-direct {p0, v4}, Lbmt;->a(I)V

    goto :goto_0

    .line 129
    :pswitch_2
    iget-object v0, p0, Lbmt;->c:Lbms;

    invoke-direct {p0}, Lbmt;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbms;->a(Ljava/lang/String;)V

    .line 130
    invoke-direct {p0, v4}, Lbmt;->a(I)V

    goto :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
