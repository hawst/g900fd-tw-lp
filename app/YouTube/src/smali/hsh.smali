.class public final Lhsh;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhxf;

.field public c:Lhgz;

.field public d:Lhgz;

.field public e:Z

.field public f:Lhog;

.field public g:Lhgz;

.field public h:Ljava/lang/String;

.field public i:Lhgz;

.field public j:Lhsg;

.field public k:[B

.field public l:[Lhxe;

.field private m:Lhgz;

.field private n:Lhgz;

.field private o:[Lhbi;

.field private p:[Lhut;

.field private q:Lhgz;

.field private r:Lhnh;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Lhsh;->a:Lhgz;

    iput-object v1, p0, Lhsh;->m:Lhgz;

    iput-object v1, p0, Lhsh;->b:Lhxf;

    iput-object v1, p0, Lhsh;->c:Lhgz;

    iput-object v1, p0, Lhsh;->d:Lhgz;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhsh;->e:Z

    iput-object v1, p0, Lhsh;->f:Lhog;

    iput-object v1, p0, Lhsh;->n:Lhgz;

    iput-object v1, p0, Lhsh;->g:Lhgz;

    const-string v0, ""

    iput-object v0, p0, Lhsh;->h:Ljava/lang/String;

    iput-object v1, p0, Lhsh;->i:Lhgz;

    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lhsh;->o:[Lhbi;

    iput-object v1, p0, Lhsh;->j:Lhsg;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhsh;->k:[B

    sget-object v0, Lhxe;->a:[Lhxe;

    iput-object v0, p0, Lhsh;->l:[Lhxe;

    sget-object v0, Lhut;->a:[Lhut;

    iput-object v0, p0, Lhsh;->p:[Lhut;

    iput-object v1, p0, Lhsh;->q:Lhgz;

    iput-object v1, p0, Lhsh;->r:Lhnh;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lhsh;->a:Lhgz;

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    iget-object v2, p0, Lhsh;->a:Lhgz;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhsh;->m:Lhgz;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhsh;->m:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhsh;->b:Lhxf;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lhsh;->b:Lhxf;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhsh;->c:Lhgz;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lhsh;->c:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhsh;->d:Lhgz;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lhsh;->d:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-boolean v2, p0, Lhsh;->e:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-boolean v3, p0, Lhsh;->e:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhsh;->f:Lhog;

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lhsh;->f:Lhog;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lhsh;->n:Lhgz;

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    iget-object v3, p0, Lhsh;->n:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lhsh;->g:Lhgz;

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    iget-object v3, p0, Lhsh;->g:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lhsh;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const/16 v2, 0xa

    iget-object v3, p0, Lhsh;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lhsh;->i:Lhgz;

    if-eqz v2, :cond_9

    const/16 v2, 0xc

    iget-object v3, p0, Lhsh;->i:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Lhsh;->o:[Lhbi;

    if-eqz v2, :cond_b

    iget-object v3, p0, Lhsh;->o:[Lhbi;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_b

    aget-object v5, v3, v2

    if-eqz v5, :cond_a

    const/16 v6, 0xd

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_b
    iget-object v2, p0, Lhsh;->j:Lhsg;

    if-eqz v2, :cond_c

    const/16 v2, 0xe

    iget-object v3, p0, Lhsh;->j:Lhsg;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_c
    iget-object v2, p0, Lhsh;->k:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_d

    const/16 v2, 0xf

    iget-object v3, p0, Lhsh;->k:[B

    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget-object v2, p0, Lhsh;->l:[Lhxe;

    if-eqz v2, :cond_f

    iget-object v3, p0, Lhsh;->l:[Lhxe;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_f

    aget-object v5, v3, v2

    if-eqz v5, :cond_e

    const/16 v6, 0x10

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_f
    iget-object v2, p0, Lhsh;->p:[Lhut;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lhsh;->p:[Lhut;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_11

    aget-object v4, v2, v1

    if-eqz v4, :cond_10

    const/16 v5, 0x11

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_11
    iget-object v1, p0, Lhsh;->q:Lhgz;

    if-eqz v1, :cond_12

    const/16 v1, 0x12

    iget-object v2, p0, Lhsh;->q:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget-object v1, p0, Lhsh;->r:Lhnh;

    if-eqz v1, :cond_13

    const/16 v1, 0x13

    iget-object v2, p0, Lhsh;->r:Lhnh;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-object v1, p0, Lhsh;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhsh;->J:I

    return v0

    :cond_14
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhsh;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhsh;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhsh;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhsh;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsh;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhsh;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhsh;->m:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsh;->m:Lhgz;

    :cond_3
    iget-object v0, p0, Lhsh;->m:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhsh;->b:Lhxf;

    if-nez v0, :cond_4

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhsh;->b:Lhxf;

    :cond_4
    iget-object v0, p0, Lhsh;->b:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhsh;->c:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsh;->c:Lhgz;

    :cond_5
    iget-object v0, p0, Lhsh;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhsh;->d:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsh;->d:Lhgz;

    :cond_6
    iget-object v0, p0, Lhsh;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhsh;->e:Z

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lhsh;->f:Lhog;

    if-nez v0, :cond_7

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhsh;->f:Lhog;

    :cond_7
    iget-object v0, p0, Lhsh;->f:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhsh;->n:Lhgz;

    if-nez v0, :cond_8

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsh;->n:Lhgz;

    :cond_8
    iget-object v0, p0, Lhsh;->n:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhsh;->g:Lhgz;

    if-nez v0, :cond_9

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsh;->g:Lhgz;

    :cond_9
    iget-object v0, p0, Lhsh;->g:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsh;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lhsh;->i:Lhgz;

    if-nez v0, :cond_a

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsh;->i:Lhgz;

    :cond_a
    iget-object v0, p0, Lhsh;->i:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsh;->o:[Lhbi;

    if-nez v0, :cond_c

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lhsh;->o:[Lhbi;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lhsh;->o:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Lhsh;->o:[Lhbi;

    :goto_2
    iget-object v2, p0, Lhsh;->o:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Lhsh;->o:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsh;->o:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_c
    iget-object v0, p0, Lhsh;->o:[Lhbi;

    array-length v0, v0

    goto :goto_1

    :cond_d
    iget-object v2, p0, Lhsh;->o:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsh;->o:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lhsh;->j:Lhsg;

    if-nez v0, :cond_e

    new-instance v0, Lhsg;

    invoke-direct {v0}, Lhsg;-><init>()V

    iput-object v0, p0, Lhsh;->j:Lhsg;

    :cond_e
    iget-object v0, p0, Lhsh;->j:Lhsg;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhsh;->k:[B

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x82

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsh;->l:[Lhxe;

    if-nez v0, :cond_10

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhxe;

    iget-object v3, p0, Lhsh;->l:[Lhxe;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lhsh;->l:[Lhxe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    iput-object v2, p0, Lhsh;->l:[Lhxe;

    :goto_4
    iget-object v2, p0, Lhsh;->l:[Lhxe;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    iget-object v2, p0, Lhsh;->l:[Lhxe;

    new-instance v3, Lhxe;

    invoke-direct {v3}, Lhxe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsh;->l:[Lhxe;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_10
    iget-object v0, p0, Lhsh;->l:[Lhxe;

    array-length v0, v0

    goto :goto_3

    :cond_11
    iget-object v2, p0, Lhsh;->l:[Lhxe;

    new-instance v3, Lhxe;

    invoke-direct {v3}, Lhxe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsh;->l:[Lhxe;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_10
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsh;->p:[Lhut;

    if-nez v0, :cond_13

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhut;

    iget-object v3, p0, Lhsh;->p:[Lhut;

    if-eqz v3, :cond_12

    iget-object v3, p0, Lhsh;->p:[Lhut;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    iput-object v2, p0, Lhsh;->p:[Lhut;

    :goto_6
    iget-object v2, p0, Lhsh;->p:[Lhut;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_14

    iget-object v2, p0, Lhsh;->p:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsh;->p:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_13
    iget-object v0, p0, Lhsh;->p:[Lhut;

    array-length v0, v0

    goto :goto_5

    :cond_14
    iget-object v2, p0, Lhsh;->p:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsh;->p:[Lhut;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lhsh;->q:Lhgz;

    if-nez v0, :cond_15

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsh;->q:Lhgz;

    :cond_15
    iget-object v0, p0, Lhsh;->q:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lhsh;->r:Lhnh;

    if-nez v0, :cond_16

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Lhsh;->r:Lhnh;

    :cond_16
    iget-object v0, p0, Lhsh;->r:Lhnh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhsh;->a:Lhgz;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lhsh;->a:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v1, p0, Lhsh;->m:Lhgz;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhsh;->m:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v1, p0, Lhsh;->b:Lhxf;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhsh;->b:Lhxf;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v1, p0, Lhsh;->c:Lhgz;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lhsh;->c:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v1, p0, Lhsh;->d:Lhgz;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lhsh;->d:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_4
    iget-boolean v1, p0, Lhsh;->e:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Lhsh;->e:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    :cond_5
    iget-object v1, p0, Lhsh;->f:Lhog;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lhsh;->f:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_6
    iget-object v1, p0, Lhsh;->n:Lhgz;

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lhsh;->n:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_7
    iget-object v1, p0, Lhsh;->g:Lhgz;

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Lhsh;->g:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_8
    iget-object v1, p0, Lhsh;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Lhsh;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_9
    iget-object v1, p0, Lhsh;->i:Lhgz;

    if-eqz v1, :cond_a

    const/16 v1, 0xc

    iget-object v2, p0, Lhsh;->i:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_a
    iget-object v1, p0, Lhsh;->o:[Lhbi;

    if-eqz v1, :cond_c

    iget-object v2, p0, Lhsh;->o:[Lhbi;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    if-eqz v4, :cond_b

    const/16 v5, 0xd

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_c
    iget-object v1, p0, Lhsh;->j:Lhsg;

    if-eqz v1, :cond_d

    const/16 v1, 0xe

    iget-object v2, p0, Lhsh;->j:Lhsg;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_d
    iget-object v1, p0, Lhsh;->k:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_e

    const/16 v1, 0xf

    iget-object v2, p0, Lhsh;->k:[B

    invoke-virtual {p1, v1, v2}, Lidd;->a(I[B)V

    :cond_e
    iget-object v1, p0, Lhsh;->l:[Lhxe;

    if-eqz v1, :cond_10

    iget-object v2, p0, Lhsh;->l:[Lhxe;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    if-eqz v4, :cond_f

    const/16 v5, 0x10

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_10
    iget-object v1, p0, Lhsh;->p:[Lhut;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lhsh;->p:[Lhut;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_12

    aget-object v3, v1, v0

    if-eqz v3, :cond_11

    const/16 v4, 0x11

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_12
    iget-object v0, p0, Lhsh;->q:Lhgz;

    if-eqz v0, :cond_13

    const/16 v0, 0x12

    iget-object v1, p0, Lhsh;->q:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_13
    iget-object v0, p0, Lhsh;->r:Lhnh;

    if-eqz v0, :cond_14

    const/16 v0, 0x13

    iget-object v1, p0, Lhsh;->r:Lhnh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_14
    iget-object v0, p0, Lhsh;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhsh;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhsh;

    iget-object v2, p0, Lhsh;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhsh;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhsh;->m:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhsh;->m:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhsh;->b:Lhxf;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhsh;->b:Lhxf;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhsh;->c:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhsh;->c:Lhgz;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhsh;->d:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhsh;->d:Lhgz;

    if-nez v2, :cond_3

    :goto_5
    iget-boolean v2, p0, Lhsh;->e:Z

    iget-boolean v3, p1, Lhsh;->e:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsh;->f:Lhog;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhsh;->f:Lhog;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhsh;->n:Lhgz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhsh;->n:Lhgz;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Lhsh;->g:Lhgz;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhsh;->g:Lhgz;

    if-nez v2, :cond_3

    :goto_8
    iget-object v2, p0, Lhsh;->h:Ljava/lang/String;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhsh;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_9
    iget-object v2, p0, Lhsh;->i:Lhgz;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhsh;->i:Lhgz;

    if-nez v2, :cond_3

    :goto_a
    iget-object v2, p0, Lhsh;->o:[Lhbi;

    iget-object v3, p1, Lhsh;->o:[Lhbi;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsh;->j:Lhsg;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhsh;->j:Lhsg;

    if-nez v2, :cond_3

    :goto_b
    iget-object v2, p0, Lhsh;->k:[B

    iget-object v3, p1, Lhsh;->k:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsh;->l:[Lhxe;

    iget-object v3, p1, Lhsh;->l:[Lhxe;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsh;->p:[Lhut;

    iget-object v3, p1, Lhsh;->p:[Lhut;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsh;->q:Lhgz;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhsh;->q:Lhgz;

    if-nez v2, :cond_3

    :goto_c
    iget-object v2, p0, Lhsh;->r:Lhnh;

    if-nez v2, :cond_10

    iget-object v2, p1, Lhsh;->r:Lhnh;

    if-nez v2, :cond_3

    :goto_d
    iget-object v2, p0, Lhsh;->I:Ljava/util/List;

    if-nez v2, :cond_11

    iget-object v2, p1, Lhsh;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhsh;->a:Lhgz;

    iget-object v3, p1, Lhsh;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhsh;->m:Lhgz;

    iget-object v3, p1, Lhsh;->m:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhsh;->b:Lhxf;

    iget-object v3, p1, Lhsh;->b:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhsh;->c:Lhgz;

    iget-object v3, p1, Lhsh;->c:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhsh;->d:Lhgz;

    iget-object v3, p1, Lhsh;->d:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhsh;->f:Lhog;

    iget-object v3, p1, Lhsh;->f:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhsh;->n:Lhgz;

    iget-object v3, p1, Lhsh;->n:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhsh;->g:Lhgz;

    iget-object v3, p1, Lhsh;->g:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhsh;->h:Ljava/lang/String;

    iget-object v3, p1, Lhsh;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhsh;->i:Lhgz;

    iget-object v3, p1, Lhsh;->i:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v2, p0, Lhsh;->j:Lhsg;

    iget-object v3, p1, Lhsh;->j:Lhsg;

    invoke-virtual {v2, v3}, Lhsg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v2, p0, Lhsh;->q:Lhgz;

    iget-object v3, p1, Lhsh;->q:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v2, p0, Lhsh;->r:Lhnh;

    iget-object v3, p1, Lhsh;->r:Lhnh;

    invoke-virtual {v2, v3}, Lhnh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v2, p0, Lhsh;->I:Ljava/util/List;

    iget-object v3, p1, Lhsh;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->a:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->m:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->b:Lhxf;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->c:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->d:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhsh;->e:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->f:Lhog;

    if-nez v0, :cond_a

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->n:Lhgz;

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->g:Lhgz;

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->h:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->i:Lhgz;

    if-nez v0, :cond_e

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsh;->o:[Lhbi;

    if-nez v2, :cond_f

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsh;->j:Lhsg;

    if-nez v0, :cond_11

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsh;->k:[B

    if-nez v2, :cond_12

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    iget-object v0, p0, Lhsh;->l:[Lhxe;

    if-nez v0, :cond_13

    mul-int/lit8 v2, v2, 0x1f

    :cond_2
    iget-object v0, p0, Lhsh;->p:[Lhut;

    if-nez v0, :cond_15

    mul-int/lit8 v2, v2, 0x1f

    :cond_3
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsh;->q:Lhgz;

    if-nez v0, :cond_17

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsh;->r:Lhnh;

    if-nez v0, :cond_18

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhsh;->I:Ljava/util/List;

    if-nez v2, :cond_19

    :goto_e
    add-int/2addr v0, v1

    return v0

    :cond_4
    iget-object v0, p0, Lhsh;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lhsh;->m:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lhsh;->b:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lhsh;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_8
    iget-object v0, p0, Lhsh;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_9
    const/4 v0, 0x2

    goto/16 :goto_5

    :cond_a
    iget-object v0, p0, Lhsh;->f:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_6

    :cond_b
    iget-object v0, p0, Lhsh;->n:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_7

    :cond_c
    iget-object v0, p0, Lhsh;->g:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_8

    :cond_d
    iget-object v0, p0, Lhsh;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_e
    iget-object v0, p0, Lhsh;->i:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_a

    :cond_f
    move v2, v0

    move v0, v1

    :goto_f
    iget-object v3, p0, Lhsh;->o:[Lhbi;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhsh;->o:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_10

    move v2, v1

    :goto_10
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_10
    iget-object v2, p0, Lhsh;->o:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_10

    :cond_11
    iget-object v0, p0, Lhsh;->j:Lhsg;

    invoke-virtual {v0}, Lhsg;->hashCode()I

    move-result v0

    goto/16 :goto_b

    :cond_12
    move v2, v0

    move v0, v1

    :goto_11
    iget-object v3, p0, Lhsh;->k:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhsh;->k:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_13
    move v0, v1

    :goto_12
    iget-object v3, p0, Lhsh;->l:[Lhxe;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhsh;->l:[Lhxe;

    aget-object v2, v2, v0

    if-nez v2, :cond_14

    move v2, v1

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    :cond_14
    iget-object v2, p0, Lhsh;->l:[Lhxe;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhxe;->hashCode()I

    move-result v2

    goto :goto_13

    :cond_15
    move v0, v1

    :goto_14
    iget-object v3, p0, Lhsh;->p:[Lhut;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhsh;->p:[Lhut;

    aget-object v2, v2, v0

    if-nez v2, :cond_16

    move v2, v1

    :goto_15
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    :cond_16
    iget-object v2, p0, Lhsh;->p:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhut;->hashCode()I

    move-result v2

    goto :goto_15

    :cond_17
    iget-object v0, p0, Lhsh;->q:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_c

    :cond_18
    iget-object v0, p0, Lhsh;->r:Lhnh;

    invoke-virtual {v0}, Lhnh;->hashCode()I

    move-result v0

    goto/16 :goto_d

    :cond_19
    iget-object v1, p0, Lhsh;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_e
.end method
