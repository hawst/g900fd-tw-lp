.class public final Laon;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldfu;


# instance fields
.field private final a:Laoo;

.field private b:Laiq;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Laiq;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiq;

    iput-object v0, p0, Laon;->b:Laiq;

    .line 24
    new-instance v0, Laoo;

    invoke-direct {v0, p1}, Laoo;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Laon;->a:Laoo;

    .line 26
    :try_start_0
    iget-object v0, p0, Laon;->a:Laoo;

    invoke-interface {p2, v0}, Laiq;->a(Lang;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 45
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0}, Laiq;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 67
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ldfv;)V
    .locals 2

    .prologue
    .line 38
    iget-object v1, p0, Laon;->a:Laoo;

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfv;

    iput-object v0, v1, Laoo;->a:Ldfv;

    .line 39
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 100
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->a(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 155
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1, p2}, Laiq;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 56
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 177
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1, p2}, Laiq;->a(ZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 122
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->b(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 111
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->b(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 188
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 89
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0}, Laiq;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 166
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->c(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 221
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->c(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 243
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 133
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0}, Laiq;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 232
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0, p1}, Laiq;->d(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 144
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0}, Laiq;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 199
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0}, Laiq;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 210
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0}, Laiq;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final p_()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Laon;->b:Laiq;

    if-eqz v0, :cond_0

    .line 78
    :try_start_0
    iget-object v0, p0, Laon;->b:Laiq;

    invoke-interface {v0}, Laiq;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
