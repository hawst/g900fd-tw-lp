.class public abstract Lfvz;
.super Lfwb;
.source "SourceFile"


# instance fields
.field private a:Lfwe;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lfwb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lfwe;)V
    .locals 1

    .prologue
    .line 22
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    iput-object v0, p0, Lfvz;->a:Lfwe;

    .line 23
    return-void
.end method

.method protected final c()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfvz;->a:Lfwe;

    instance-of v0, v0, Lfwf;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfvz;->a:Lfwe;

    instance-of v0, v0, Lfwd;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 37
    return-void

    .line 35
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 26
    iget-object v0, p0, Lfvz;->a:Lfwe;

    instance-of v0, v0, Lfwf;

    if-eqz v0, :cond_1

    .line 27
    const-string v1, "activities/"

    iget-object v0, p0, Lfvz;->a:Lfwe;

    check-cast v0, Lfwf;

    iget-object v0, v0, Lfwf;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 29
    :cond_1
    const-string v1, "comments/"

    iget-object v0, p0, Lfvz;->a:Lfwe;

    check-cast v0, Lfwd;

    iget-object v0, v0, Lfwd;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
