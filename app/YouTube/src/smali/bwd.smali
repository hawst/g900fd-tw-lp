.class public final Lbwd;
.super Lcba;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/widget/AdapterView$OnItemClickListener;

.field public b:I

.field private c:Landroid/content/Context;

.field private e:Landroid/content/res/Resources;

.field private final f:Lcba;

.field private g:F

.field private h:F

.field private i:F

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field private o:I


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcba;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 63
    invoke-direct {p0}, Lcba;-><init>()V

    .line 47
    iput v3, p0, Lbwd;->b:I

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lbwd;->g:F

    .line 64
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbwd;->c:Landroid/content/Context;

    .line 65
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcba;

    iput-object v0, p0, Lbwd;->f:Lcba;

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbwd;->e:Landroid/content/res/Resources;

    .line 68
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010112

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 70
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    iput v0, p0, Lbwd;->o:I

    .line 72
    new-instance v0, Lbwe;

    invoke-direct {v0, p0}, Lbwe;-><init>(Lbwd;)V

    invoke-virtual {p2, v0}, Lcba;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 73
    return-void
.end method

.method public static a(Landroid/content/Context;Lcba;)Lbwd;
    .locals 6

    .prologue
    .line 132
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    new-instance v0, Lbwd;

    invoke-direct {v0, p0, p1}, Lbwd;-><init>(Landroid/content/Context;Lcba;)V

    .line 133
    const v1, 0x7f0a00b7

    iget-object v2, v0, Lbwd;->e:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, v0, Lbwd;->i:F

    .line 134
    const v1, 0x7f0a00b6

    iget-object v2, v0, Lbwd;->e:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, v0, Lbwd;->h:F

    .line 135
    const v1, 0x7f0a00bd

    const v2, 0x7f0a00be

    const v3, 0x7f0a00bf

    const v4, 0x7f0a00c0

    iget-object v5, v0, Lbwd;->e:Landroid/content/res/Resources;

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Lbwd;->k:I

    iget-object v1, v0, Lbwd;->e:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Lbwd;->j:I

    iget-object v1, v0, Lbwd;->e:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Lbwd;->m:I

    iget-object v1, v0, Lbwd;->e:Landroid/content/res/Resources;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Lbwd;->l:I

    .line 140
    const/4 v1, 0x1

    iput-boolean v1, v0, Lbwd;->n:Z

    .line 141
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lbwd;->f:Lcba;

    invoke-virtual {v0, p1, p2}, Lcba;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 403
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lbwd;->f:Lcba;

    invoke-virtual {v0}, Lcba;->b()V

    .line 408
    return-void
.end method

.method public final b(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lbwd;->f:Lcba;

    invoke-virtual {v0, p1, p2}, Lcba;->b(ILjava/lang/Object;)V

    .line 383
    return-void
.end method

.method public final b(Ljava/lang/Iterable;)V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lbwd;->f:Lcba;

    invoke-virtual {v0, p1}, Lcba;->b(Ljava/lang/Iterable;)V

    .line 151
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lbwd;->f:Lcba;

    invoke-virtual {v0, p1}, Lcba;->b(Ljava/lang/Object;)V

    .line 388
    return-void
.end method

.method public final getCount()I
    .locals 4

    .prologue
    .line 342
    iget-object v0, p0, Lbwd;->f:Lcba;

    invoke-virtual {v0}, Lcba;->getCount()I

    move-result v0

    int-to-double v0, v0

    iget v2, p0, Lbwd;->b:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lbwd;->f:Lcba;

    invoke-virtual {v0, p1}, Lcba;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 367
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x2

    const/4 v9, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 416
    .line 418
    if-nez p2, :cond_4

    .line 419
    new-instance p2, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lbwd;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 420
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 421
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, v2, v10}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v2, v4

    .line 431
    :goto_0
    if-nez p1, :cond_5

    .line 432
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 438
    :goto_1
    invoke-virtual {p0}, Lbwd;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 439
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 444
    iget v5, p0, Lbwd;->h:F

    .line 445
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iget v6, p0, Lbwd;->i:F

    .line 447
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 444
    invoke-virtual {p2, v5, v0, v6, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 450
    if-eqz v2, :cond_0

    array-length v0, v2

    iget v3, p0, Lbwd;->b:I

    if-eq v0, v3, :cond_1

    .line 451
    :cond_0
    iget v0, p0, Lbwd;->b:I

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    .line 452
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 453
    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 454
    iget v2, p0, Lbwd;->b:I

    int-to-float v2, v2

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    move-object v2, v0

    :cond_1
    move v0, v1

    .line 457
    :goto_2
    iget v3, p0, Lbwd;->b:I

    if-ge v0, v3, :cond_7

    .line 459
    aget-object v3, v2, v0

    if-nez v3, :cond_6

    .line 460
    new-instance v3, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iget-object v5, p0, Lbwd;->c:Landroid/content/Context;

    invoke-direct {v3, v5}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;-><init>(Landroid/content/Context;)V

    aput-object v3, v2, v0

    .line 461
    aget-object v3, v2, v0

    .line 462
    iget-boolean v5, p0, Lbwd;->n:Z

    if-eqz v5, :cond_2

    .line 463
    iget v5, p0, Lbwd;->k:I

    iget v6, p0, Lbwd;->j:I

    iget v7, p0, Lbwd;->m:I

    iget v8, p0, Lbwd;->l:I

    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a(IIII)V

    .line 465
    iget v5, p0, Lbwd;->o:I

    invoke-virtual {v3, v5}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a(I)V

    .line 467
    :cond_2
    invoke-virtual {v3, p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 468
    invoke-virtual {v3, v11}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setClickable(Z)V

    .line 469
    invoke-virtual {v3, v11}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setEnabled(Z)V

    .line 470
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 471
    invoke-virtual {v3, v5, v5, v5, v5}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setPadding(IIII)V

    .line 476
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v1, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 478
    const/high16 v6, 0x3f800000    # 1.0f

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 479
    if-lez v0, :cond_3

    .line 480
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 482
    :cond_3
    invoke-virtual {p2, v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 486
    :goto_3
    iget v5, p0, Lbwd;->b:I

    mul-int/2addr v5, p1

    add-int/2addr v5, v0

    .line 487
    const v6, 0x7f080021

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v6, v5}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setTag(ILjava/lang/Object;)V

    .line 457
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 424
    :cond_4
    check-cast p2, Landroid/widget/LinearLayout;

    .line 425
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    move-object v2, v0

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 434
    goto/16 :goto_1

    .line 484
    :cond_6
    aget-object v3, v2, v0

    goto :goto_3

    :cond_7
    move v3, v1

    .line 490
    :goto_4
    iget v0, p0, Lbwd;->b:I

    if-ge v3, v0, :cond_b

    .line 491
    aget-object v0, v2, v3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 492
    iget v5, p0, Lbwd;->b:I

    mul-int/2addr v5, p1

    add-int/2addr v5, v3

    .line 493
    iget-object v6, p0, Lbwd;->f:Lcba;

    invoke-virtual {v6}, Lcba;->getCount()I

    move-result v6

    if-ge v5, v6, :cond_a

    .line 495
    iget-object v6, p0, Lbwd;->f:Lcba;

    invoke-virtual {v6, v5, v0, p2}, Lcba;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 496
    if-eq v0, v5, :cond_9

    .line 499
    aget-object v0, v2, v3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 500
    aget-object v0, v2, v3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->removeAllViews()V

    .line 502
    :cond_8
    aget-object v0, v2, v3

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setTag(Ljava/lang/Object;)V

    .line 503
    aget-object v0, v2, v3

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 504
    aget-object v0, v2, v3

    invoke-virtual {v0, v11}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setClickable(Z)V

    .line 505
    aget-object v0, v2, v3

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v7, -0x1

    invoke-direct {v6, v7, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 490
    :cond_9
    :goto_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 510
    :cond_a
    aget-object v0, v2, v3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->removeAllViews()V

    .line 511
    aget-object v0, v2, v3

    const v5, 0x7f080021

    invoke-virtual {v0, v5, v4}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setTag(ILjava/lang/Object;)V

    .line 512
    aget-object v0, v2, v3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setTag(Ljava/lang/Object;)V

    .line 513
    aget-object v0, v2, v3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 514
    aget-object v0, v2, v3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 515
    aget-object v0, v2, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setClickable(Z)V

    goto :goto_5

    .line 519
    :cond_b
    return-object p2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x1

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 524
    iget-object v0, p0, Lbwd;->a:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 525
    const v0, 0x7f080021

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 526
    iget-object v0, p0, Lbwd;->a:Landroid/widget/AdapterView$OnItemClickListener;

    const/4 v1, 0x0

    invoke-virtual {p0, v3}, Lbwd;->getItemId(I)J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 528
    :cond_0
    return-void
.end method
