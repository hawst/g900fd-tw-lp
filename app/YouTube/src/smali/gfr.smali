.class public final Lgfr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ledw;


# instance fields
.field final a:Lgft;

.field private final b:Ljava/lang/String;

.field private final c:Lgfl;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/os/Handler;

.field private f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lgfl;Ljava/lang/String;Landroid/os/Handler;Lgft;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgfr;->b:Ljava/lang/String;

    .line 54
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfl;

    iput-object v0, p0, Lgfr;->c:Lgfl;

    .line 55
    iput-object p3, p0, Lgfr;->d:Ljava/lang/String;

    .line 56
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lgfr;->e:Landroid/os/Handler;

    .line 57
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgft;

    iput-object v0, p0, Lgfr;->a:Lgft;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgfr;->f:Z

    .line 59
    return-void
.end method


# virtual methods
.method public final a(Landroid/media/MediaDrm$KeyRequest;)[B
    .locals 4

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/media/MediaDrm$KeyRequest;->getDefaultUrl()Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    iget-object v0, p0, Lgfr;->b:Ljava/lang/String;

    .line 89
    :cond_0
    iget-object v1, p0, Lgfr;->c:Lgfl;

    .line 90
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Landroid/media/MediaDrm$KeyRequest;->getData()[B

    move-result-object v2

    iget-object v3, p0, Lgfr;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lgfl;->a(Landroid/net/Uri;[BLjava/lang/String;)Lgfp;

    move-result-object v1

    .line 91
    iget-object v0, v1, Lgfp;->c:Ljava/util/Map;

    const-string v2, "Authorized-Format-Types"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, v1, Lgfp;->c:Ljava/util/Map;

    const-string v2, "Authorized-Format-Types"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 94
    const-string v2, "HD"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    iget-boolean v0, p0, Lgfr;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lgfr;->e:Landroid/os/Handler;

    new-instance v2, Lgfs;

    invoke-direct {v2, p0}, Lgfs;-><init>(Lgfr;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgfr;->f:Z

    .line 98
    :cond_1
    iget-object v0, v1, Lgfp;->b:[B

    return-object v0
.end method

.method public final a(Landroid/media/MediaDrm$ProvisionRequest;)[B
    .locals 3

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/media/MediaDrm$ProvisionRequest;->getDefaultUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 74
    new-instance v1, Ljava/lang/String;

    invoke-virtual {p1}, Landroid/media/MediaDrm$ProvisionRequest;->getData()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 75
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "signedRequest"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 76
    iget-object v1, p0, Lgfr;->c:Lgfl;

    invoke-virtual {v1, v0}, Lgfl;->a(Landroid/net/Uri;)[B

    move-result-object v0

    return-object v0
.end method
