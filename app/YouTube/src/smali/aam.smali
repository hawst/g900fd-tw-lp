.class public Laam;
.super Laah;
.source "SourceFile"

# interfaces
.implements Lyn;


# static fields
.field private static synthetic j:Z


# instance fields
.field private b:I

.field private c:I

.field private d:D

.field private e:D

.field private f:I

.field private g:Ljava/lang/String;

.field private h:I

.field private i:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Laam;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Laam;->j:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-wide/high16 v0, 0x4052000000000000L    # 72.0

    .line 75
    invoke-direct {p0, p1}, Laah;-><init>(Ljava/lang/String;)V

    .line 66
    iput-wide v0, p0, Laam;->d:D

    .line 67
    iput-wide v0, p0, Laam;->e:D

    .line 68
    const/4 v0, 0x1

    iput v0, p0, Laam;->f:I

    .line 70
    const/16 v0, 0x18

    iput v0, p0, Laam;->h:I

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [J

    iput-object v0, p0, Laam;->i:[J

    .line 76
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v1, 0x1f

    .line 136
    invoke-virtual {p0, p1}, Laam;->c(Ljava/nio/ByteBuffer;)V

    .line 137
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    int-to-long v2, v0

    .line 138
    sget-boolean v0, Laam;->j:Z

    if-nez v0, :cond_0

    cmp-long v0, v6, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "reserved byte not 0"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 139
    :cond_0
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    int-to-long v2, v0

    .line 140
    sget-boolean v0, Laam;->j:Z

    if-nez v0, :cond_1

    cmp-long v0, v6, v2

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "reserved byte not 0"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 141
    :cond_1
    iget-object v0, p0, Laam;->i:[J

    const/4 v2, 0x0

    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    aput-wide v4, v0, v2

    .line 142
    iget-object v0, p0, Laam;->i:[J

    const/4 v2, 0x1

    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    aput-wide v4, v0, v2

    .line 143
    iget-object v0, p0, Laam;->i:[J

    const/4 v2, 0x2

    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    aput-wide v4, v0, v2

    .line 144
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laam;->b:I

    .line 145
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laam;->c:I

    .line 146
    invoke-static {p1}, La;->g(Ljava/nio/ByteBuffer;)D

    move-result-wide v2

    iput-wide v2, p0, Laam;->d:D

    .line 147
    invoke-static {p1}, La;->g(Ljava/nio/ByteBuffer;)D

    move-result-wide v2

    iput-wide v2, p0, Laam;->e:D

    .line 148
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    .line 149
    sget-boolean v0, Laam;->j:Z

    if-nez v0, :cond_2

    cmp-long v0, v6, v2

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "reserved byte not 0"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 150
    :cond_2
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laam;->f:I

    .line 151
    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 152
    if-le v0, v1, :cond_3

    .line 153
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x35

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "invalid compressor name displayable data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v0, v1

    .line 156
    :cond_3
    new-array v2, v0, [B

    .line 157
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 158
    invoke-static {v2}, La;->a([B)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Laam;->g:Ljava/lang/String;

    .line 159
    if-ge v0, v1, :cond_4

    .line 160
    rsub-int/lit8 v0, v0, 0x1f

    new-array v0, v0, [B

    .line 161
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 164
    :cond_4
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laam;->h:I

    .line 165
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    int-to-long v0, v0

    .line 166
    sget-boolean v2, Laam;->j:Z

    if-nez v2, :cond_5

    const-wide/32 v2, 0xffff

    cmp-long v0, v2, v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 168
    :cond_5
    invoke-virtual {p0, p1}, Laam;->d(Ljava/nio/ByteBuffer;)V

    .line 170
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 183
    invoke-virtual {p0, p1}, Laam;->e(Ljava/nio/ByteBuffer;)V

    .line 184
    invoke-static {p1, v2}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 185
    invoke-static {p1, v2}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 186
    iget-object v0, p0, Laam;->i:[J

    aget-wide v0, v0, v2

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 187
    iget-object v0, p0, Laam;->i:[J

    const/4 v1, 0x1

    aget-wide v0, v0, v1

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 188
    iget-object v0, p0, Laam;->i:[J

    const/4 v1, 0x2

    aget-wide v0, v0, v1

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 190
    iget v0, p0, Laam;->b:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 191
    iget v0, p0, Laam;->c:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 193
    iget-wide v0, p0, Laam;->d:D

    invoke-static {p1, v0, v1}, Lyf;->a(Ljava/nio/ByteBuffer;D)V

    .line 194
    iget-wide v0, p0, Laam;->e:D

    invoke-static {p1, v0, v1}, Lyf;->a(Ljava/nio/ByteBuffer;D)V

    .line 197
    const-wide/16 v0, 0x0

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 198
    iget v0, p0, Laam;->f:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 199
    iget-object v0, p0, Laam;->g:Ljava/lang/String;

    invoke-static {v0}, La;->c(Ljava/lang/String;)I

    move-result v0

    invoke-static {p1, v0}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    .line 200
    iget-object v0, p0, Laam;->g:Ljava/lang/String;

    invoke-static {v0}, La;->b(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 201
    iget-object v0, p0, Laam;->g:Ljava/lang/String;

    invoke-static {v0}, La;->c(Ljava/lang/String;)I

    move-result v0

    .line 202
    :goto_0
    const/16 v1, 0x1f

    if-ge v0, v1, :cond_0

    .line 203
    add-int/lit8 v0, v0, 0x1

    .line 204
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 206
    :cond_0
    iget v0, p0, Laam;->h:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 207
    const v0, 0xffff

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 209
    invoke-virtual {p0, p1}, Laam;->f(Ljava/nio/ByteBuffer;)V

    .line 211
    return-void
.end method

.method protected final d_()J
    .locals 5

    .prologue
    .line 174
    const-wide/16 v0, 0x4e

    .line 175
    iget-object v2, p0, Laam;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 176
    invoke-interface {v0}, Lyj;->l_()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 177
    goto :goto_0

    .line 178
    :cond_0
    return-wide v2
.end method
