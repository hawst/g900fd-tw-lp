.class public final Lgqh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field private final b:Ljava/util/PriorityQueue;

.field private final c:Ljava/util/PriorityQueue;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x1

    iput v0, p0, Lgqh;->a:I

    .line 60
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0, v2}, Ljava/util/PriorityQueue;-><init>(I)V

    iput-object v0, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    .line 61
    new-instance v0, Ljava/util/PriorityQueue;

    .line 62
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    .line 63
    return-void
.end method

.method private declared-synchronized a()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 177
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->size()I

    move-result v2

    iget v3, p0, Lgqh;->a:I

    if-ge v2, v3, :cond_1

    .line 178
    iget-object v0, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqj;

    .line 179
    if-eqz v0, :cond_0

    .line 180
    iget-object v1, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 181
    const/4 v1, 0x0

    iput-boolean v1, v0, Lgqj;->c:Z

    .line 182
    iget-object v0, v0, Lgqj;->b:Lgqi;

    invoke-interface {v0}, Lgqi;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 185
    :cond_1
    :try_start_1
    iget-object v2, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Lb;->c(Z)V

    .line 186
    iget-object v0, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqj;

    .line 187
    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgqj;

    .line 189
    iget v0, v0, Lgqj;->a:I

    iget v2, v1, Lgqj;->a:I

    if-le v0, v2, :cond_0

    .line 193
    iget-boolean v0, v1, Lgqj;->c:Z

    if-nez v0, :cond_0

    .line 194
    const/4 v0, 0x1

    iput-boolean v0, v1, Lgqj;->c:Z

    .line 195
    iget-object v0, v1, Lgqj;->b:Lgqi;

    invoke-interface {v0}, Lgqi;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v1

    .line 185
    goto :goto_1
.end method

.method private d(Lgqi;)Lgqj;
    .locals 3

    .prologue
    .line 208
    iget-object v0, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqj;

    .line 209
    iget-object v2, v0, Lgqj;->b:Lgqi;

    if-ne v2, p1, :cond_0

    .line 219
    :goto_0
    return-object v0

    .line 214
    :cond_1
    iget-object v0, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqj;

    .line 215
    iget-object v2, v0, Lgqj;->b:Lgqi;

    if-ne v2, p1, :cond_2

    goto :goto_0

    .line 219
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 2

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lgqh;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt p1, v0, :cond_1

    .line 85
    :cond_0
    monitor-exit p0

    return-void

    .line 78
    :cond_1
    :try_start_1
    iput p1, p0, Lgqh;->a:I

    .line 80
    :goto_0
    iget-object v0, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    iget v1, p0, Lgqh;->a:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqj;

    .line 82
    iget-object v1, v0, Lgqj;->b:Lgqi;

    invoke-interface {v1}, Lgqi;->a()V

    .line 83
    iget-object v1, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lgqi;I)V
    .locals 2

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    invoke-direct {p0, p1}, Lgqh;->d(Lgqi;)Lgqj;

    move-result-object v0

    .line 109
    if-nez v0, :cond_1

    .line 110
    iget-object v0, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    new-instance v1, Lgqj;

    invoke-direct {v1, p0, p2, p1}, Lgqj;-><init>(Lgqh;ILgqi;)V

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 124
    :goto_0
    invoke-direct {p0}, Lgqh;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    :cond_0
    monitor-exit p0

    return-void

    .line 112
    :cond_1
    :try_start_1
    iget v1, v0, Lgqj;->a:I

    if-eq v1, p2, :cond_0

    .line 116
    iget-object v1, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    iget-object v1, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 118
    iget-object v0, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    new-instance v1, Lgqj;

    invoke-direct {v1, p0, p2, p1}, Lgqj;-><init>(Lgqh;ILgqi;)V

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 120
    :cond_2
    :try_start_2
    iget-object v1, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 121
    iget-object v0, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    new-instance v1, Lgqj;

    invoke-direct {v1, p0, p2, p1}, Lgqj;-><init>(Lgqh;ILgqi;)V

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Lgqi;)Z
    .locals 2

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lgqh;->d(Lgqi;)Lgqj;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_0

    iget-object v1, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lgqi;)V
    .locals 2

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    invoke-direct {p0, p1}, Lgqh;->d(Lgqi;)Lgqj;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    iget-object v1, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    iget-object v1, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 151
    invoke-direct {p0}, Lgqh;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :cond_0
    monitor-exit p0

    return-void

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lgqi;)V
    .locals 2

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    invoke-direct {p0, p1}, Lgqh;->d(Lgqi;)Lgqj;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_0

    .line 165
    iget-object v1, p0, Lgqh;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 166
    iget-object v1, p0, Lgqh;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 167
    invoke-direct {p0}, Lgqh;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    :cond_0
    monitor-exit p0

    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
