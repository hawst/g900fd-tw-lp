.class public final Lbya;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/DialogInterface$OnClickListener;

.field public final b:Landroid/app/AlertDialog;

.field public c:Z

.field private final d:Landroid/content/Context;

.field private final e:Landroid/content/SharedPreferences;

.field private final f:Landroid/view/LayoutInflater;

.field private final g:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbya;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbya;->d:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lbya;->a:Landroid/content/DialogInterface$OnClickListener;

    .line 48
    const-string v0, "youtube"

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lbya;->e:Landroid/content/SharedPreferences;

    .line 49
    iget-object v0, p0, Lbya;->e:Landroid/content/SharedPreferences;

    const-string v1, "upload_policy"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lbya;->c:Z

    .line 51
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lbya;->f:Landroid/view/LayoutInflater;

    .line 52
    iget-object v0, p0, Lbya;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f040123

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbya;->g:Landroid/view/View;

    .line 56
    iget-boolean v0, p0, Lbya;->c:Z

    if-nez v0, :cond_0

    .line 57
    const-string v0, "connectivity"

    .line 58
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 59
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 60
    if-nez v0, :cond_0

    .line 61
    const v0, 0x7f09010b

    invoke-virtual {p0, v0}, Lbya;->a(I)V

    .line 65
    :cond_0
    iget-object v0, p0, Lbya;->g:Landroid/view/View;

    const v1, 0x7f080320

    .line 66
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 68
    new-instance v1, Lbyb;

    invoke-direct {v1, p0, v0}, Lbyb;-><init>(Lbya;Landroid/widget/RadioButton;)V

    .line 78
    new-instance v0, Leyv;

    invoke-direct {v0, p1}, Leyv;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0901f0

    .line 79
    invoke-virtual {v0, v2}, Leyv;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lbya;->g:Landroid/view/View;

    .line 80
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x104000a

    .line 81
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 82
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbya;->b:Landroid/app/AlertDialog;

    .line 84
    return-void
.end method


# virtual methods
.method a(I)V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lbya;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "upload_policy"

    iget-object v2, p0, Lbya;->d:Landroid/content/Context;

    .line 96
    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 97
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbya;->c:Z

    .line 99
    return-void
.end method
