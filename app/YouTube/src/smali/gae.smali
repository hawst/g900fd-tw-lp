.class public final Lgae;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;

.field private static final b:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 43
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 44
    const/4 v1, 0x0

    sget-object v2, Lgch;->a:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const-string v1, "processing"

    sget-object v2, Lgch;->b:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const-string v1, "deleted"

    sget-object v2, Lgch;->c:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v1, "requesterRegion"

    sget-object v2, Lgch;->e:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v1, "limitedSyndication"

    sget-object v2, Lgch;->f:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "private"

    sget-object v2, Lgch;->g:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    const-string v1, "copyright"

    sget-object v2, Lgch;->i:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v1, "inappropriate"

    sget-object v2, Lgch;->j:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v1, "duplicate"

    sget-object v2, Lgch;->k:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string v1, "termsOfUse"

    sget-object v2, Lgch;->l:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    const-string v1, "suspended"

    sget-object v2, Lgch;->m:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v1, "tooLong"

    sget-object v2, Lgch;->n:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const-string v1, "blocked"

    sget-object v2, Lgch;->o:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v1, "clientRestrict"

    sget-object v2, Lgch;->h:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v1, "cantProcess"

    sget-object v2, Lgch;->p:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const-string v1, "invalidFormat"

    sget-object v2, Lgch;->q:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v1, "unsupportedCodec"

    sget-object v2, Lgch;->r:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const-string v1, "empty"

    sget-object v2, Lgch;->s:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string v1, "tooSmall"

    sget-object v2, Lgch;->t:Lgch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lgae;->a:Ljava/util/Map;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "edit"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "http://gdata.youtube.com/schemas/2007#video.captionTracks"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "http://gdata.youtube.com/schemas/2007#live.event"

    aput-object v3, v1, v2

    .line 75
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lgae;->b:Ljava/util/Set;

    .line 74
    return-void
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lgae;->a:Ljava/util/Map;

    return-object v0
.end method

.method public static a(Lfbb;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 92
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-static {p0, v0}, Lgae;->d(Lfbb;Ljava/lang/String;)V

    .line 95
    new-instance v1, Lgaf;

    invoke-direct {v1}, Lgaf;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 107
    return-void
.end method

.method static synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lgae;->b:Ljava/util/Set;

    return-object v0
.end method

.method public static b(Lfbb;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 116
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    invoke-static {p0, v0}, Lgae;->d(Lfbb;Ljava/lang/String;)V

    .line 119
    new-instance v1, Lgaq;

    invoke-direct {v1}, Lgaq;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 125
    return-void
.end method

.method public static c(Lfbb;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 136
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-static {p0, v0}, Lgae;->d(Lfbb;Ljava/lang/String;)V

    .line 139
    new-instance v1, Lgbb;

    invoke-direct {v1}, Lgbb;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 160
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/batch:status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbc;

    invoke-direct {v1}, Lgbc;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 161
    return-void
.end method

.method public static d(Lfbb;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 207
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/media:group/media:content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgba;

    invoke-direct {v1}, Lgba;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/media:thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgaz;

    invoke-direct {v2}, Lgaz;-><init>()V

    .line 219
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/media:player"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgay;

    invoke-direct {v2}, Lgay;-><init>()V

    .line 236
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/yt:duration"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgax;

    invoke-direct {v2}, Lgax;-><init>()V

    .line 243
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/media:rating"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgaw;

    invoke-direct {v2}, Lgaw;-><init>()V

    .line 250
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/yt:videoid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgav;

    invoke-direct {v2}, Lgav;-><init>()V

    .line 259
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/media:credit"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgau;

    invoke-direct {v2}, Lgau;-><init>()V

    .line 266
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/author/uri"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgat;

    invoke-direct {v2}, Lgat;-><init>()V

    .line 280
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/media:description"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgas;

    invoke-direct {v2}, Lgas;-><init>()V

    .line 287
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/media:keywords"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgar;

    invoke-direct {v2}, Lgar;-><init>()V

    .line 294
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/yt:statistics"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgap;

    invoke-direct {v2}, Lgap;-><init>()V

    .line 301
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/link"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgao;

    invoke-direct {v2}, Lgao;-><init>()V

    .line 308
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/category"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgan;

    invoke-direct {v2}, Lgan;-><init>()V

    .line 327
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/yt:rating"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgam;

    invoke-direct {v2}, Lgam;-><init>()V

    .line 337
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/yt:accessControl"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgal;

    invoke-direct {v2}, Lgal;-><init>()V

    .line 345
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/yt:private"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgak;

    invoke-direct {v2}, Lgak;-><init>()V

    .line 367
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/yt:location"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgaj;

    invoke-direct {v2}, Lgaj;-><init>()V

    .line 374
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/georss:where/gml:Point/gml:pos"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgai;

    invoke-direct {v2}, Lgai;-><init>()V

    .line 381
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/app:control/yt:state"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgah;

    invoke-direct {v2}, Lgah;-><init>()V

    .line 388
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/published"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgag;

    invoke-direct {v2}, Lgag;-><init>()V

    .line 399
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/yt:uploaded"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbg;

    invoke-direct {v2}, Lgbg;-><init>()V

    .line 410
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/title"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbf;

    invoke-direct {v2}, Lgbf;-><init>()V

    .line 417
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/yt:threed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbe;

    invoke-direct {v2}, Lgbe;-><init>()V

    .line 424
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/yt:paidContent"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbd;

    invoke-direct {v2}, Lgbd;-><init>()V

    .line 431
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 438
    return-void
.end method
