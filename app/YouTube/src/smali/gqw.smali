.class public final Lgqw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgcn;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/net/Uri;

.field private final c:Landroid/net/Uri;

.field private final d:Lgqy;

.field private e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;JJ)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgqw;->e:J

    .line 70
    iput-object p1, p0, Lgqw;->a:Landroid/content/Context;

    .line 71
    iput-object p2, p0, Lgqw;->b:Landroid/net/Uri;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lgqw;->c:Landroid/net/Uri;

    .line 73
    new-instance v0, Lgqy;

    invoke-direct {v0, p4, p5, p6, p7}, Lgqy;-><init>(JJ)V

    iput-object v0, p0, Lgqw;->d:Lgqy;

    .line 74
    return-void
.end method

.method private static a(JLjava/util/List;)J
    .locals 10

    .prologue
    .line 310
    const-wide/16 v0, 0x0

    .line 311
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzq;

    .line 312
    iget-wide v6, v0, Lzq;->a:J

    .line 313
    iget-wide v0, v0, Lzq;->b:J

    const-wide/32 v8, 0xf4240

    mul-long/2addr v0, v8

    div-long/2addr v0, p0

    .line 315
    mul-long/2addr v0, v6

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 316
    goto :goto_0

    .line 317
    :cond_0
    return-wide v2
.end method

.method private static a([JJJZ)J
    .locals 3

    .prologue
    .line 327
    invoke-static {p0, p3, p4}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v0

    .line 328
    if-gez v0, :cond_0

    .line 331
    const/4 v1, 0x0

    xor-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 334
    :cond_0
    if-nez p5, :cond_1

    .line 336
    add-int/lit8 v0, v0, 0x1

    .line 339
    :cond_1
    array-length v1, p0

    if-ge v0, v1, :cond_2

    aget-wide p1, p0, v0

    :cond_2
    return-wide p1
.end method

.method private static a(Lgqx;Landroid/content/Context;Landroid/net/Uri;)Lgqx;
    .locals 3

    .prologue
    .line 162
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "r"

    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 163
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/io/Closeable;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {p0, v1}, Lgqx;->a([Ljava/io/Closeable;)V

    .line 165
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 166
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lgqx;->a([Ljava/io/Closeable;)V

    .line 168
    new-instance v0, Lye;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    new-instance v2, Lgqv;

    invoke-direct {v2}, Lgqv;-><init>()V

    invoke-direct {v0, v1, v2}, Lye;-><init>(Ljava/nio/channels/ReadableByteChannel;Lyb;)V

    .line 169
    invoke-virtual {p0, v0}, Lgqx;->a(Lye;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    return-object p0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    invoke-virtual {p0}, Lgqx;->close()V

    .line 173
    throw v0
.end method

.method private static a(Likz;Lgqy;Lgqy;)Lilf;
    .locals 27

    .prologue
    .line 354
    invoke-virtual/range {p0 .. p0}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v0, v2, Lila;->b:J

    move-wide/from16 v16, v0

    .line 356
    const-wide/16 v10, 0x0

    .line 357
    const-wide/16 v8, 0x0

    .line 358
    const-wide/16 v4, -0x1

    .line 359
    const-wide/16 v2, -0x1

    .line 360
    const-wide/16 v6, -0x1

    .line 362
    invoke-virtual/range {p0 .. p0}, Likz;->c()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    move-wide v12, v10

    move-wide v10, v8

    move-wide v8, v2

    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzq;

    .line 363
    iget-wide v0, v2, Lzq;->a:J

    move-wide/from16 v20, v0

    .line 364
    iget-wide v2, v2, Lzq;->b:J

    const-wide/32 v14, 0xf4240

    mul-long/2addr v2, v14

    div-long v22, v2, v16

    .line 366
    const/4 v2, 0x0

    move/from16 v24, v2

    move-wide v2, v8

    move-wide v8, v12

    move/from16 v12, v24

    move-wide/from16 v25, v4

    move-wide v4, v10

    move-wide v10, v6

    move-wide/from16 v6, v25

    :goto_1
    int-to-long v14, v12

    cmp-long v13, v14, v20

    if-gez v13, :cond_1

    .line 367
    move-object/from16 v0, p1

    iget-wide v14, v0, Lgqy;->a:J

    cmp-long v13, v4, v14

    if-gtz v13, :cond_0

    move-wide v2, v4

    move-wide v6, v8

    .line 371
    :cond_0
    move-object/from16 v0, p1

    iget-wide v14, v0, Lgqy;->b:J

    cmp-long v13, v4, v14

    if-gtz v13, :cond_1

    .line 376
    add-long v10, v4, v22

    .line 377
    const-wide/16 v4, 0x1

    add-long v14, v8, v4

    .line 366
    add-int/lit8 v4, v12, 0x1

    move v12, v4

    move-wide v4, v10

    move-wide v10, v8

    move-wide v8, v14

    goto :goto_1

    :cond_1
    move-wide v12, v8

    move-wide v8, v2

    move-wide/from16 v24, v6

    move-wide v6, v10

    move-wide v10, v4

    move-wide/from16 v4, v24

    .line 379
    goto :goto_0

    .line 381
    :cond_2
    new-instance v2, Lilf;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lilf;-><init>(Likz;JJ)V

    .line 382
    invoke-virtual {v2}, Lilf;->g()Lila;

    move-result-object v3

    move-object/from16 v0, p2

    iget-wide v4, v0, Lgqy;->a:J

    sub-long/2addr v4, v8

    long-to-double v4, v4

    const-wide v6, 0x412e848000000000L    # 1000000.0

    div-double/2addr v4, v6

    iput-wide v4, v3, Lila;->i:D

    .line 383
    invoke-virtual {v2}, Lilf;->g()Lila;

    move-result-object v3

    move-object/from16 v0, p2

    iget-wide v4, v0, Lgqy;->b:J

    move-object/from16 v0, p2

    iget-wide v6, v0, Lgqy;->a:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    const-wide v6, 0x412e848000000000L    # 1000000.0

    div-double/2addr v4, v6

    iput-wide v4, v3, Lila;->j:D

    .line 384
    return-object v2
.end method

.method private static a(Lzr;)Z
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0}, Lzr;->i()Lyz;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lyz;->g()Lzb;

    move-result-object v0

    invoke-virtual {v0}, Lzb;->h()Lyi;

    move-result-object v0

    .line 180
    instance-of v0, v0, Lzl;

    return v0
.end method

.method private static a([JJLjava/util/List;)[J
    .locals 19

    .prologue
    .line 276
    move-object/from16 v0, p0

    array-length v2, v0

    new-array v8, v2, [J

    .line 278
    const-wide/16 v6, 0x1

    .line 279
    const-wide/16 v4, 0x0

    .line 281
    const/4 v2, 0x0

    .line 283
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v3, v2

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzq;

    .line 284
    iget-wide v10, v2, Lzq;->a:J

    .line 285
    iget-wide v12, v2, Lzq;->b:J

    const-wide/32 v14, 0xf4240

    mul-long/2addr v12, v14

    div-long v12, v12, p1

    .line 288
    add-long v14, v6, v10

    move v2, v3

    .line 290
    :goto_1
    move-object/from16 v0, p0

    array-length v3, v0

    if-ge v2, v3, :cond_0

    aget-wide v16, p0, v2

    cmp-long v3, v6, v16

    if-gtz v3, :cond_0

    aget-wide v16, p0, v2

    cmp-long v3, v16, v14

    if-gez v3, :cond_0

    .line 293
    aget-wide v16, p0, v2

    sub-long v16, v16, v6

    .line 294
    mul-long v16, v16, v12

    add-long v16, v16, v4

    aput-wide v16, v8, v2

    .line 296
    add-int/lit8 v2, v2, 0x1

    .line 297
    goto :goto_1

    .line 298
    :cond_0
    add-long/2addr v6, v10

    .line 299
    mul-long/2addr v10, v12

    add-long/2addr v4, v10

    .line 301
    move-object/from16 v0, p0

    array-length v3, v0

    if-eq v2, v3, :cond_1

    move v3, v2

    .line 302
    goto :goto_0

    .line 305
    :cond_1
    return-object v8
.end method

.method private c()Lgqx;
    .locals 18

    .prologue
    .line 185
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqw;->d:Lgqy;

    iget-wide v2, v2, Lgqy;->a:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lgqw;->d:Lgqy;

    iget-wide v4, v4, Lgqy;->b:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lb;->b(Z)V

    .line 187
    new-instance v9, Lgqx;

    invoke-direct {v9}, Lgqx;-><init>()V

    .line 188
    new-instance v10, Lgqx;

    invoke-direct {v10}, Lgqx;-><init>()V

    .line 190
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqw;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lgqw;->b:Landroid/net/Uri;

    invoke-static {v9, v2, v3}, Lgqw;->a(Lgqx;Landroid/content/Context;Landroid/net/Uri;)Lgqx;

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqw;->c:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 193
    move-object/from16 v0, p0

    iget-object v2, v0, Lgqw;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lgqw;->c:Landroid/net/Uri;

    invoke-static {v10, v2, v3}, Lgqw;->a(Lgqx;Landroid/content/Context;Landroid/net/Uri;)Lgqx;

    .line 197
    :cond_0
    invoke-static {}, La;->J()Ljava/util/ArrayList;

    move-result-object v11

    .line 198
    invoke-virtual {v9}, Lgqx;->a()Lye;

    move-result-object v2

    invoke-virtual {v2}, Lye;->b_()Lzc;

    move-result-object v2

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzc;

    .line 199
    const-class v3, Lzr;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Liku;->a(Ljava/lang/Class;Z)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lzr;

    .line 200
    invoke-virtual {v10}, Lgqx;->b()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {v3}, Lgqw;->a(Lzr;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 201
    :cond_2
    new-instance v5, Liky;

    invoke-direct {v5, v3}, Liky;-><init>(Lzr;)V

    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 232
    :catch_0
    move-exception v2

    .line 233
    invoke-virtual {v9}, Lgqx;->close()V

    .line 234
    invoke-virtual {v10}, Lgqx;->close()V

    .line 235
    throw v2

    .line 185
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 206
    :cond_4
    :try_start_1
    invoke-virtual {v10}, Lgqx;->b()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 207
    invoke-virtual {v10}, Lgqx;->a()Lye;

    move-result-object v3

    invoke-virtual {v3}, Lye;->b_()Lzc;

    move-result-object v3

    invoke-static {v3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lzc;

    .line 208
    const-class v4, Lzr;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Liku;->a(Ljava/lang/Class;Z)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lzr;

    .line 209
    invoke-static {v3}, Lgqw;->a(Lzr;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 210
    new-instance v5, Liky;

    invoke-direct {v5, v3}, Liky;-><init>(Lzr;)V

    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 215
    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lgqw;->d:Lgqy;

    new-instance v13, Lgqy;

    const-wide/16 v4, -0x1

    const-wide/16 v6, -0x1

    invoke-direct {v13, v4, v5, v6, v7}, Lgqy;-><init>(JJ)V

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_7
    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Likz;

    move-object v4, v0

    invoke-virtual {v4}, Likz;->e()[J

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v4}, Likz;->e()[J

    move-result-object v3

    array-length v3, v3

    if-lez v3, :cond_7

    iget-wide v6, v13, Lgqy;->a:J

    const-wide/16 v16, -0x1

    cmp-long v3, v6, v16

    if-eqz v3, :cond_8

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Only one track with sync samples is supported"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_8
    invoke-virtual {v4}, Likz;->e()[J

    move-result-object v3

    invoke-virtual {v4}, Likz;->g()Lila;

    move-result-object v5

    iget-wide v6, v5, Lila;->b:J

    invoke-virtual {v4}, Likz;->c()Ljava/util/List;

    move-result-object v5

    invoke-static {v3, v6, v7, v5}, Lgqw;->a([JJLjava/util/List;)[J

    move-result-object v3

    invoke-virtual {v4}, Likz;->g()Lila;

    move-result-object v5

    iget-wide v6, v5, Lila;->b:J

    invoke-virtual {v4}, Likz;->c()Ljava/util/List;

    move-result-object v4

    invoke-static {v6, v7, v4}, Lgqw;->a(JLjava/util/List;)J

    move-result-wide v4

    iget-wide v6, v12, Lgqy;->a:J

    const/4 v8, 0x1

    invoke-static/range {v3 .. v8}, Lgqw;->a([JJJZ)J

    move-result-wide v6

    iput-wide v6, v13, Lgqy;->a:J

    iget-wide v6, v12, Lgqy;->b:J

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lgqw;->a([JJJZ)J

    move-result-wide v4

    iput-wide v4, v13, Lgqy;->b:J

    goto :goto_3

    :cond_9
    iget-wide v4, v13, Lgqy;->a:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_a

    iget-wide v4, v13, Lgqy;->b:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_b

    :cond_a
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Unable to find keyframes to cut at"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 217
    :cond_b
    new-instance v4, Likx;

    invoke-direct {v4}, Likx;-><init>()V

    .line 218
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Likz;

    .line 219
    move-object/from16 v0, p0

    iget-object v6, v0, Lgqw;->d:Lgqy;

    invoke-static {v3, v13, v6}, Lgqw;->a(Likz;Lgqy;Lgqy;)Lilf;

    move-result-object v3

    invoke-virtual {v3}, Likz;->g()Lila;

    move-result-object v6

    iget-wide v6, v6, Lila;->g:J

    invoke-virtual {v4, v6, v7}, Likx;->a(J)Likz;

    move-result-object v6

    if-eqz v6, :cond_c

    invoke-virtual {v3}, Likz;->g()Lila;

    move-result-object v6

    invoke-virtual {v4}, Likx;->a()J

    move-result-wide v14

    iput-wide v14, v6, Lila;->g:J

    :cond_c
    iget-object v6, v4, Likx;->a:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 221
    :cond_d
    const-class v3, Lzd;

    invoke-virtual {v2, v3}, Lzc;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 222
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_e

    .line 225
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzd;

    .line 226
    iget-wide v6, v2, Lzd;->a:J

    invoke-static {v6, v7}, La;->c(J)Ljava/util/Date;

    move-result-object v3

    iput-object v3, v4, Likx;->c:Ljava/util/Date;

    .line 227
    iget-wide v2, v2, Lzd;->b:J

    invoke-static {v2, v3}, La;->c(J)Ljava/util/Date;

    move-result-object v2

    iput-object v2, v4, Likx;->b:Ljava/util/Date;

    .line 230
    :cond_e
    new-instance v2, Lilb;

    invoke-direct {v2}, Lilb;-><init>()V

    invoke-virtual {v2, v4}, Lilb;->a(Likx;)Lye;

    move-result-object v2

    .line 231
    new-instance v3, Lgqx;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v5, 0x0

    aput-object v9, v4, v5

    const/4 v5, 0x1

    aput-object v10, v4, v5

    invoke-direct {v3, v2, v4}, Lgqx;-><init>(Lye;[Ljava/io/Closeable;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v3
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 122
    iget-wide v0, p0, Lgqw;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Lgra;

    invoke-direct {v0, p0}, Lgra;-><init>(Lgqw;)V

    .line 124
    invoke-direct {p0}, Lgqw;->c()Lgqx;

    move-result-object v1

    .line 126
    :try_start_0
    invoke-virtual {v1}, Lgqx;->a()Lye;

    move-result-object v2

    invoke-virtual {v2, v0}, Lye;->a(Ljava/nio/channels/WritableByteChannel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    invoke-virtual {v1}, Lgqx;->close()V

    .line 130
    invoke-virtual {v0}, Lgra;->close()V

    .line 131
    iget-boolean v1, v0, Lgra;->b:Z

    invoke-static {v1}, Lb;->c(Z)V

    iget-wide v0, v0, Lgra;->a:J

    iput-wide v0, p0, Lgqw;->e:J

    .line 133
    :cond_0
    iget-wide v0, p0, Lgqw;->e:J

    return-wide v0

    .line 128
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lgqx;->close()V

    throw v0
.end method

.method public final b()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 142
    invoke-direct {p0}, Lgqw;->c()Lgqx;

    move-result-object v1

    .line 144
    :try_start_0
    invoke-static {}, Ljava/nio/channels/Pipe;->open()Ljava/nio/channels/Pipe;

    move-result-object v0

    .line 145
    new-instance v2, Lgqz;

    invoke-virtual {v0}, Ljava/nio/channels/Pipe;->sink()Ljava/nio/channels/Pipe$SinkChannel;

    move-result-object v3

    invoke-direct {v2, p0, v1, v3}, Lgqz;-><init>(Lgqw;Lgqx;Ljava/nio/channels/WritableByteChannel;)V

    invoke-virtual {v2}, Lgqz;->start()V

    .line 146
    const/4 v1, 0x0

    .line 147
    invoke-virtual {v0}, Ljava/nio/channels/Pipe;->source()Ljava/nio/channels/Pipe$SourceChannel;

    move-result-object v0

    invoke-static {v0}, Ljava/nio/channels/Channels;->newInputStream(Ljava/nio/channels/ReadableByteChannel;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 149
    return-object v0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 150
    invoke-virtual {v1}, Lgqx;->close()V

    :cond_0
    throw v0
.end method
