.class public final Lcvf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbr;


# instance fields
.field private synthetic a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcvf;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 0

    .prologue
    .line 387
    return-void
.end method

.method public final a(Ldbs;)V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcvf;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbe;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a()Ljava/util/EnumMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbl;

    invoke-virtual {v1, v0}, Ldbe;->a(Ldbl;)V

    .line 367
    return-void
.end method

.method public final a(Ldbt;)V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcvf;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;Ldbt;)Ldbt;

    .line 336
    return-void
.end method

.method public final a(Ldbu;)V
    .locals 0

    .prologue
    .line 392
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcvf;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbe;

    move-result-object v0

    sget-object v1, Ldbl;->f:Ldbl;

    invoke-virtual {v0, v1}, Ldbe;->a(Ldbl;)V

    .line 372
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 406
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 380
    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 400
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 385
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 398
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 394
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 395
    return-void
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 348
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 357
    iget-object v0, p0, Lcvf;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;J)J

    .line 358
    iget-object v0, p0, Lcvf;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbe;

    move-result-object v0

    sget-object v1, Ldbl;->d:Ldbl;

    invoke-virtual {v0, v1}, Ldbe;->a(Ldbl;)V

    .line 359
    return-void
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 401
    return-void
.end method

.method public final g(Z)V
    .locals 0

    .prologue
    .line 402
    return-void
.end method

.method public final h(Z)V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcvf;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Ldbe;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbe;->b(Z)V

    .line 353
    return-void
.end method

.method public final i(Z)V
    .locals 0

    .prologue
    .line 412
    return-void
.end method

.method public final j(Z)V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method public final k(Z)V
    .locals 0

    .prologue
    .line 405
    return-void
.end method

.method public final l(Z)V
    .locals 0

    .prologue
    .line 410
    return-void
.end method

.method public final m(Z)V
    .locals 0

    .prologue
    .line 414
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x0

    return v0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 424
    const/4 v0, 0x0

    return v0
.end method
