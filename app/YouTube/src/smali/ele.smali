.class public final Lele;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic a:Leld;


# direct methods
.method public constructor <init>(Leld;)V
    .locals 0

    iput-object p1, p0, Lele;->a:Leld;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    iget-object v0, p0, Lele;->a:Leld;

    iget-object v0, v0, Leld;->h:Lelc;

    invoke-static {v0}, Lelc;->a(Lelc;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lele;->a:Leld;

    iput-object p2, v0, Leld;->f:Landroid/os/IBinder;

    iget-object v0, p0, Lele;->a:Leld;

    iput-object p1, v0, Leld;->g:Landroid/content/ComponentName;

    iget-object v0, p0, Lele;->a:Leld;

    iget-object v0, v0, Leld;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekw;

    invoke-virtual {v0, p1, p2}, Lekw;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lele;->a:Leld;

    const/4 v2, 0x1

    iput v2, v0, Leld;->d:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    iget-object v0, p0, Lele;->a:Leld;

    iget-object v0, v0, Leld;->h:Lelc;

    invoke-static {v0}, Lelc;->a(Lelc;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lele;->a:Leld;

    const/4 v2, 0x0

    iput-object v2, v0, Leld;->f:Landroid/os/IBinder;

    iget-object v0, p0, Lele;->a:Leld;

    iput-object p1, v0, Leld;->g:Landroid/content/ComponentName;

    iget-object v0, p0, Lele;->a:Leld;

    iget-object v0, v0, Leld;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekw;

    invoke-virtual {v0, p1}, Lekw;->onServiceDisconnected(Landroid/content/ComponentName;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lele;->a:Leld;

    const/4 v2, 0x2

    iput v2, v0, Leld;->d:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
