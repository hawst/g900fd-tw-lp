.class public final Lgxs;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;

.field private b:Lhgz;

.field private c:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 361
    invoke-direct {p0}, Lidf;-><init>()V

    .line 364
    iput-object v0, p0, Lgxs;->a:Lhgz;

    .line 367
    iput-object v0, p0, Lgxs;->b:Lhgz;

    .line 370
    iput-object v0, p0, Lgxs;->c:Lhgz;

    .line 361
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 419
    const/4 v0, 0x0

    .line 420
    iget-object v1, p0, Lgxs;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 421
    const/4 v0, 0x1

    iget-object v1, p0, Lgxs;->a:Lhgz;

    .line 422
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 424
    :cond_0
    iget-object v1, p0, Lgxs;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 425
    const/4 v1, 0x2

    iget-object v2, p0, Lgxs;->b:Lhgz;

    .line 426
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 428
    :cond_1
    iget-object v1, p0, Lgxs;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 429
    const/4 v1, 0x3

    iget-object v2, p0, Lgxs;->c:Lhgz;

    .line 430
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 432
    :cond_2
    iget-object v1, p0, Lgxs;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 433
    iput v0, p0, Lgxs;->J:I

    .line 434
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 357
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgxs;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgxs;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgxs;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgxs;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgxs;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lgxs;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgxs;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgxs;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lgxs;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgxs;->c:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgxs;->c:Lhgz;

    :cond_4
    iget-object v0, p0, Lgxs;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lgxs;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 405
    const/4 v0, 0x1

    iget-object v1, p0, Lgxs;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 407
    :cond_0
    iget-object v0, p0, Lgxs;->b:Lhgz;

    if-eqz v0, :cond_1

    .line 408
    const/4 v0, 0x2

    iget-object v1, p0, Lgxs;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 410
    :cond_1
    iget-object v0, p0, Lgxs;->c:Lhgz;

    if-eqz v0, :cond_2

    .line 411
    const/4 v0, 0x3

    iget-object v1, p0, Lgxs;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 413
    :cond_2
    iget-object v0, p0, Lgxs;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 415
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 383
    if-ne p1, p0, :cond_1

    .line 389
    :cond_0
    :goto_0
    return v0

    .line 384
    :cond_1
    instance-of v2, p1, Lgxs;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 385
    :cond_2
    check-cast p1, Lgxs;

    .line 386
    iget-object v2, p0, Lgxs;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgxs;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgxs;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgxs;->b:Lhgz;

    if-nez v2, :cond_3

    .line 387
    :goto_2
    iget-object v2, p0, Lgxs;->c:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgxs;->c:Lhgz;

    if-nez v2, :cond_3

    .line 388
    :goto_3
    iget-object v2, p0, Lgxs;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgxs;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 389
    goto :goto_0

    .line 386
    :cond_4
    iget-object v2, p0, Lgxs;->a:Lhgz;

    iget-object v3, p1, Lgxs;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgxs;->b:Lhgz;

    iget-object v3, p1, Lgxs;->b:Lhgz;

    .line 387
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgxs;->c:Lhgz;

    iget-object v3, p1, Lgxs;->c:Lhgz;

    .line 388
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgxs;->I:Ljava/util/List;

    iget-object v3, p1, Lgxs;->I:Ljava/util/List;

    .line 389
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 393
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 395
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxs;->a:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 396
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxs;->b:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 397
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxs;->c:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 398
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgxs;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 399
    return v0

    .line 395
    :cond_0
    iget-object v0, p0, Lgxs;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 396
    :cond_1
    iget-object v0, p0, Lgxs;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 397
    :cond_2
    iget-object v0, p0, Lgxs;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 398
    :cond_3
    iget-object v1, p0, Lgxs;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
