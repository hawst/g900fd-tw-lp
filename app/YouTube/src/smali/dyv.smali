.class public final Ldyv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbt;
.implements Ldyy;


# static fields
.field private static synthetic m:Z


# instance fields
.field public final a:Ldzr;

.field public b:Ldzs;

.field public c:Ldyz;

.field public final d:Ldzu;

.field public final e:Ldzv;

.field public f:Ldyp;

.field public g:Z

.field public h:Z

.field public i:Z

.field private j:Ldyx;

.field private k:I

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Ldyv;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ldyv;->m:Z

    .line 492
    const-class v0, Ldyv;

    .line 493
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    .line 492
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ldzr;Ldyp;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 506
    iput-object v0, p0, Ldyv;->b:Ldzs;

    .line 510
    iput-object v0, p0, Ldyv;->c:Ldyz;

    .line 513
    iput-object v0, p0, Ldyv;->j:Ldyx;

    .line 519
    new-instance v0, Ldzu;

    invoke-direct {v0}, Ldzu;-><init>()V

    iput-object v0, p0, Ldyv;->d:Ldzu;

    .line 526
    new-instance v0, Ldzv;

    invoke-direct {v0}, Ldzv;-><init>()V

    iput-object v0, p0, Ldyv;->e:Ldzv;

    .line 536
    iput v2, p0, Ldyv;->k:I

    .line 541
    iput v2, p0, Ldyv;->l:I

    .line 545
    iput-boolean v1, p0, Ldyv;->g:Z

    .line 549
    iput-boolean v1, p0, Ldyv;->h:Z

    .line 556
    iput-boolean v1, p0, Ldyv;->i:Z

    .line 66
    if-nez p1, :cond_0

    .line 67
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "eventRegistry cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    iput-object p1, p0, Ldyv;->a:Ldzr;

    .line 73
    iput-object p2, p0, Ldyv;->f:Ldyp;

    .line 74
    return-void
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Ldyv;->b:Ldzs;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Ldyv;->c:Ldyz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/net/InetSocketAddress;Ldyx;)V
    .locals 3

    .prologue
    .line 163
    .line 164
    :try_start_0
    invoke-static {}, Ldzs;->a()Ldzs;

    move-result-object v0

    .line 163
    invoke-direct {p0}, Ldyv;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "alreading connecting"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    iget-object v1, p0, Ldyv;->f:Ldyp;

    iget-object v2, p0, Ldyv;->a:Ldzr;

    invoke-static {v1, v0, v2}, Ldyq;->a(Ldyp;Ljava/lang/Exception;Ldzr;)V

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    :try_start_1
    invoke-direct {p0}, Ldyv;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "alreading connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v1, p0, Ldyv;->i:Z

    if-eqz v1, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput-object p2, p0, Ldyv;->j:Ldyx;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iput-object v0, p0, Ldyv;->b:Ldzs;

    iget-object v1, p0, Ldyv;->b:Ldzs;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ldzs;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    iget-object v1, p0, Ldyv;->b:Ldzs;

    iget-object v1, v1, Ldzs;->a:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1, p1}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    iget-object v1, p0, Ldyv;->a:Ldzr;

    iget-object v2, p0, Ldyv;->b:Ldzs;

    invoke-interface {v1, v2, p0}, Ldzr;->a(Ljava/nio/channels/SelectableChannel;Ldyy;)V

    iget-object v0, v0, Ldzs;->a:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyv;->a:Ldzr;

    new-instance v1, Ldyw;

    invoke-direct {v1, p0}, Ldyw;-><init>(Ldyv;)V

    invoke-interface {v0, v1}, Ldzr;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    iget-object v1, p0, Ldyv;->f:Ldyp;

    iget-object v2, p0, Ldyv;->a:Ldzr;

    invoke-static {v1, v0, v2}, Ldyq;->a(Ldyp;Ljava/lang/Exception;Ldzr;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 252
    :try_start_0
    invoke-direct {p0}, Ldyv;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-boolean v0, p0, Ldyv;->i:Z

    if-eqz v0, :cond_3

    .line 262
    sget-boolean v0, Ldyv;->m:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Ldyv;->b:Ldzs;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :catch_0
    move-exception v0

    .line 322
    iget-object v1, p0, Ldyv;->f:Ldyp;

    invoke-interface {v1, v0}, Ldyp;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 263
    :cond_2
    :try_start_1
    iget-object v0, p0, Ldyv;->b:Ldzs;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Ldyv;->b:Ldzs;

    invoke-virtual {v0}, Ldzs;->close()V

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Ldyv;->b:Ldzs;

    goto :goto_0

    .line 270
    :cond_3
    iget-object v0, p0, Ldyv;->b:Ldzs;

    iget-object v0, v0, Ldzs;->a:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    move-result v0

    if-nez v0, :cond_4

    .line 271
    new-instance v0, Ldzf;

    const-string v1, "Seems like a bug: handleConnectEvent() invoked when connection is still in progress"

    invoke-direct {v0, v1}, Ldzf;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_4
    iget-object v0, p0, Ldyv;->a:Ldzr;

    iget-object v1, p0, Ldyv;->b:Ldzs;

    invoke-interface {v0, v1}, Ldzr;->a(Ljava/nio/channels/SelectableChannel;)V

    .line 280
    new-instance v0, Ldyz;

    iget-object v1, p0, Ldyv;->b:Ldzs;

    iget-object v2, p0, Ldyv;->a:Ldzr;

    iget-object v3, p0, Ldyv;->f:Ldyp;

    const/4 v4, 0x2

    invoke-direct {v0, v1, v2, v3, v4}, Ldyz;-><init>(Ldzs;Ldzr;Ldyp;I)V

    iput-object v0, p0, Ldyv;->c:Ldyz;

    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Ldyv;->b:Ldzs;

    .line 291
    iget-object v0, p0, Ldyv;->d:Ldzu;

    iget-object v1, p0, Ldyv;->c:Ldyz;

    iget-object v1, v1, Ldyz;->b:Ljava/io/InputStream;

    iput-object v1, v0, Ldzu;->a:Ljava/io/InputStream;

    .line 296
    iget-object v0, p0, Ldyv;->e:Ldzv;

    iget-object v1, p0, Ldyv;->c:Ldyz;

    iget-object v1, v1, Ldyz;->c:Ljava/io/OutputStream;

    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "newOut cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v2, v0, Ldzv;->a:Ljava/io/OutputStream;

    if-eqz v2, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setUnderlyingOutputStream() cannot be called more than once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget-object v2, v0, Ldzv;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-virtual {v2}, Lorg/apache/http/util/ByteArrayBuffer;->buffer()[B

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, v0, Ldzv;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-virtual {v4}, Lorg/apache/http/util/ByteArrayBuffer;->length()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    const/4 v2, 0x0

    iput-object v2, v0, Ldzv;->b:Lorg/apache/http/util/ByteArrayBuffer;

    iput-object v1, v0, Ldzv;->a:Ljava/io/OutputStream;

    .line 300
    iget v0, p0, Ldyv;->k:I

    if-lez v0, :cond_8

    .line 301
    iget-object v0, p0, Ldyv;->c:Ldyz;

    iget v1, p0, Ldyv;->k:I

    if-gtz v1, :cond_7

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxNumByte must be positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iput v1, v0, Ldyz;->d:I

    .line 306
    :cond_8
    iget v0, p0, Ldyv;->l:I

    if-lez v0, :cond_9

    .line 307
    iget-object v0, p0, Ldyv;->c:Ldyz;

    iget v1, p0, Ldyv;->l:I

    if-lez v1, :cond_c

    iput v1, v0, Ldyz;->e:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldyz;->f:Z

    .line 310
    :cond_9
    :goto_1
    iget-boolean v0, p0, Ldyv;->g:Z

    if-eqz v0, :cond_a

    .line 311
    iget-object v0, p0, Ldyv;->c:Ldyz;

    invoke-virtual {v0}, Ldyz;->b()V

    .line 314
    :cond_a
    iget-boolean v0, p0, Ldyv;->h:Z

    if-eqz v0, :cond_b

    .line 315
    iget-object v0, p0, Ldyv;->c:Ldyz;

    invoke-virtual {v0}, Ldyz;->c()V

    .line 318
    :cond_b
    iget-object v0, p0, Ldyv;->j:Ldyx;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Ldyv;->j:Ldyx;

    iget-object v0, p0, Ldyv;->c:Ldyz;

    goto/16 :goto_0

    .line 307
    :cond_c
    const/4 v2, -0x1

    if-ne v1, v2, :cond_d

    const v1, 0x7fffffff

    iput v1, v0, Ldyz;->e:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Ldyz;->f:Z

    goto :goto_1

    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid maxNumByte "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 223
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 224
    invoke-direct {p0}, Ldyv;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    const-string v1, "Status = connecting; Connecting SocketChannel = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 226
    iget-object v1, p0, Ldyv;->b:Ldzs;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 233
    :goto_0
    const-string v1, ";maxSizePerReadToSet_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 234
    iget v1, p0, Ldyv;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 235
    const-string v1, ";maxSizePerFlushToSet_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 236
    iget v1, p0, Ldyv;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 237
    const-string v1, ";asyncReadAfterConnect_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 238
    iget-boolean v1, p0, Ldyv;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 239
    const-string v1, ";asyncFlushAfterConnect_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 240
    iget-boolean v1, p0, Ldyv;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 241
    const-string v1, ";isClosed_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 242
    iget-boolean v1, p0, Ldyv;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 243
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 227
    :cond_0
    invoke-direct {p0}, Ldyv;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 228
    const-string v1, "Status = connected; Connection = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 229
    iget-object v1, p0, Ldyv;->c:Ldyz;

    invoke-virtual {v1}, Ldyz;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 231
    :cond_1
    const-string v1, "Status = idle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
