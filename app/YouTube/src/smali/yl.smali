.class public Lyl;
.super Likv;
.source "SourceFile"


# static fields
.field private static synthetic b:Z


# instance fields
.field public a:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lyl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lyl;->b:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    const-string v0, "ctts"

    invoke-direct {p0, v0}, Likv;-><init>(Ljava/lang/String;)V

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lyl;->a:Ljava/util/List;

    .line 52
    return-void
.end method

.method public static a(Ljava/util/List;)[I
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 131
    const-wide/16 v0, 0x0

    .line 132
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lym;

    .line 133
    iget v0, v0, Lym;->a:I

    int-to-long v0, v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 134
    goto :goto_0

    .line 135
    :cond_0
    sget-boolean v0, Lyl;->b:Z

    if-nez v0, :cond_1

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 136
    :cond_1
    long-to-int v0, v2

    new-array v5, v0, [I

    .line 141
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v4

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lym;

    move v2, v4

    .line 142
    :goto_1
    iget v3, v0, Lym;->a:I

    if-ge v2, v3, :cond_2

    .line 143
    add-int/lit8 v3, v1, 0x1

    iget v7, v0, Lym;->b:I

    aput v7, v5, v1

    .line 142
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    .line 147
    :cond_3
    return-object v5
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lyl;->c(Ljava/nio/ByteBuffer;)J

    .line 69
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    invoke-static {v0, v1}, La;->d(J)I

    move-result v1

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lyl;->a:Ljava/util/List;

    .line 71
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 72
    new-instance v2, Lym;

    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    invoke-static {v4, v5}, La;->d(J)I

    move-result v3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lym;-><init>(II)V

    .line 73
    iget-object v3, p0, Lyl;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lyl;->d(Ljava/nio/ByteBuffer;)V

    .line 80
    iget-object v0, p0, Lyl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 82
    iget-object v0, p0, Lyl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lym;

    .line 83
    iget v2, v0, Lym;->a:I

    int-to-long v2, v2

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 84
    iget v0, v0, Lym;->b:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 87
    :cond_0
    return-void
.end method

.method protected final d_()J
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lyl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x8

    int-to-long v0, v0

    return-wide v0
.end method
