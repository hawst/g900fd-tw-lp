.class public final Lgza;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5634
    invoke-direct {p0}, Lidf;-><init>()V

    .line 5637
    const/4 v0, 0x0

    iput-object v0, p0, Lgza;->a:Lhgz;

    .line 5634
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 5674
    const/4 v0, 0x0

    .line 5675
    iget-object v1, p0, Lgza;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 5676
    const/4 v0, 0x1

    iget-object v1, p0, Lgza;->a:Lhgz;

    .line 5677
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5679
    :cond_0
    iget-object v1, p0, Lgza;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5680
    iput v0, p0, Lgza;->J:I

    .line 5681
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 5630
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgza;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgza;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgza;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgza;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgza;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lgza;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 5665
    iget-object v0, p0, Lgza;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 5666
    const/4 v0, 0x1

    iget-object v1, p0, Lgza;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 5668
    :cond_0
    iget-object v0, p0, Lgza;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 5670
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5648
    if-ne p1, p0, :cond_1

    .line 5652
    :cond_0
    :goto_0
    return v0

    .line 5649
    :cond_1
    instance-of v2, p1, Lgza;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 5650
    :cond_2
    check-cast p1, Lgza;

    .line 5651
    iget-object v2, p0, Lgza;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgza;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgza;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgza;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 5652
    goto :goto_0

    .line 5651
    :cond_4
    iget-object v2, p0, Lgza;->a:Lhgz;

    iget-object v3, p1, Lgza;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgza;->I:Ljava/util/List;

    iget-object v3, p1, Lgza;->I:Ljava/util/List;

    .line 5652
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5656
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5658
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgza;->a:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 5659
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgza;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 5660
    return v0

    .line 5658
    :cond_0
    iget-object v0, p0, Lgza;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5659
    :cond_1
    iget-object v1, p0, Lgza;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
