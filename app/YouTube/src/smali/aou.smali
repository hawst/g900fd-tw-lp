.class public final Laou;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbr;


# instance fields
.field a:Laiz;

.field private final b:Laov;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Laiz;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const-string v0, "client cannot be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiz;

    iput-object v0, p0, Laou;->a:Laiz;

    .line 36
    new-instance v0, Laov;

    invoke-direct {v0, p2}, Laov;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Laou;->b:Laov;

    .line 39
    :try_start_0
    iget-object v0, p0, Laou;->b:Laov;

    invoke-interface {p3, v0}, Laiz;->a(Lanp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(III)V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 215
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1, p2, p3}, Laiz;->a(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ldbs;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 157
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-virtual {p1}, Ldbs;->ordinal()I

    move-result v1

    invoke-interface {v0, v1}, Laiz;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ldbt;)V
    .locals 2

    .prologue
    .line 51
    iget-object v1, p0, Laou;->b:Laov;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbt;

    iput-object v0, v1, Laov;->a:Ldbt;

    .line 52
    return-void
.end method

.method public final a(Ldbu;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 58
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-virtual {p1}, Ldbu;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Laiz;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 168
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1, p2}, Laiz;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 294
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->a(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 80
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1, p2}, Laiz;->a([Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 226
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0}, Laiz;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 69
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 135
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0}, Laiz;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 113
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->d(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 146
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0}, Laiz;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 124
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->e(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 237
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0}, Laiz;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 91
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final g(Z)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 102
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final h(Z)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 190
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->f(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final i(Z)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 201
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->g(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final j(Z)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 272
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->h(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final k(Z)V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 283
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1}, Laiz;->i(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final l(Z)V
    .locals 0

    .prologue
    .line 317
    return-void
.end method

.method public final m(Z)V
    .locals 0

    .prologue
    .line 323
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 248
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1, p2}, Laiz;->a(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Laou;->a:Laiz;

    if-eqz v0, :cond_0

    .line 260
    :try_start_0
    iget-object v0, p0, Laou;->a:Laiz;

    invoke-interface {v0, p1, p2}, Laiz;->b(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
