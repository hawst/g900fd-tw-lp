.class final Ldjc;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/File;

.field private final b:J

.field private final c:J

.field private final d:Ljava/security/Key;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;JJLjava/security/Key;)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    .line 41
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Ldjc;->a:Ljava/io/File;

    .line 42
    cmp-long v0, p3, p5

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "begin must be less than or equal to end"

    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    .line 43
    iput-wide p3, p0, Ldjc;->b:J

    .line 44
    iput-wide p5, p0, Ldjc;->c:J

    .line 45
    iput-object p7, p0, Ldjc;->d:Ljava/security/Key;

    .line 46
    const-string v0, "contentType cannot be empty"

    invoke-static {p2, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldjc;->setContentType(Ljava/lang/String;)V

    .line 47
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 7

    .prologue
    .line 76
    iget-object v0, p0, Ldjc;->d:Ljava/security/Key;

    if-eqz v0, :cond_0

    .line 77
    new-instance v0, Ldiw;

    iget-object v1, p0, Ldjc;->a:Ljava/io/File;

    iget-wide v2, p0, Ldjc;->b:J

    iget-wide v4, p0, Ldjc;->c:J

    iget-object v6, p0, Ldjc;->d:Ljava/security/Key;

    invoke-direct/range {v0 .. v6}, Ldiw;-><init>(Ljava/io/File;JJLjava/security/Key;)V

    .line 79
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldjd;

    iget-object v1, p0, Ldjc;->a:Ljava/io/File;

    iget-wide v2, p0, Ldjc;->b:J

    iget-wide v4, p0, Ldjc;->c:J

    invoke-direct/range {v0 .. v5}, Ldjd;-><init>(Ljava/io/File;JJ)V

    goto :goto_0
.end method

.method public final getContentLength()J
    .locals 4

    .prologue
    .line 85
    iget-wide v0, p0, Ldjc;->c:J

    iget-wide v2, p0, Ldjc;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Ldjc;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 68
    :try_start_0
    invoke-static {v0, p1}, Lfaq;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 71
    return-void

    .line 70
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method
