.class public final Lhav;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:F

.field public b:Z

.field private c:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12777
    invoke-direct {p0}, Lidf;-><init>()V

    .line 12780
    iput v1, p0, Lhav;->a:F

    .line 12783
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhav;->b:Z

    .line 12786
    iput v1, p0, Lhav;->c:F

    .line 12777
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 12835
    const/4 v0, 0x0

    .line 12836
    iget v1, p0, Lhav;->a:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    .line 12837
    const/4 v0, 0x1

    iget v1, p0, Lhav;->a:F

    .line 12838
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 12840
    :cond_0
    iget-boolean v1, p0, Lhav;->b:Z

    if-eqz v1, :cond_1

    .line 12841
    const/4 v1, 0x2

    iget-boolean v2, p0, Lhav;->b:Z

    .line 12842
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12844
    :cond_1
    iget v1, p0, Lhav;->c:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_2

    .line 12845
    const/4 v1, 0x3

    iget v2, p0, Lhav;->c:F

    .line 12846
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 12848
    :cond_2
    iget-object v1, p0, Lhav;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12849
    iput v0, p0, Lhav;->J:I

    .line 12850
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 12773
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhav;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhav;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhav;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhav;->a:F

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhav;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhav;->c:F

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12820
    iget v0, p0, Lhav;->a:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    .line 12821
    const/4 v0, 0x1

    iget v1, p0, Lhav;->a:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    .line 12823
    :cond_0
    iget-boolean v0, p0, Lhav;->b:Z

    if-eqz v0, :cond_1

    .line 12824
    const/4 v0, 0x2

    iget-boolean v1, p0, Lhav;->b:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 12826
    :cond_1
    iget v0, p0, Lhav;->c:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 12827
    const/4 v0, 0x3

    iget v1, p0, Lhav;->c:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    .line 12829
    :cond_2
    iget-object v0, p0, Lhav;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 12831
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12799
    if-ne p1, p0, :cond_1

    .line 12805
    :cond_0
    :goto_0
    return v0

    .line 12800
    :cond_1
    instance-of v2, p1, Lhav;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 12801
    :cond_2
    check-cast p1, Lhav;

    .line 12802
    iget v2, p0, Lhav;->a:F

    iget v3, p1, Lhav;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lhav;->b:Z

    iget-boolean v3, p1, Lhav;->b:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhav;->c:F

    iget v3, p1, Lhav;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-object v2, p0, Lhav;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhav;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 12805
    goto :goto_0

    .line 12802
    :cond_4
    iget-object v2, p0, Lhav;->I:Ljava/util/List;

    iget-object v3, p1, Lhav;->I:Ljava/util/List;

    .line 12805
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 12809
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 12811
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhav;->a:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 12812
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lhav;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 12813
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhav;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 12814
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhav;->I:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 12815
    return v0

    .line 12812
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 12814
    :cond_1
    iget-object v0, p0, Lhav;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_1
.end method
