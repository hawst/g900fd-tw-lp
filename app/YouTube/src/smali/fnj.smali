.class public final Lfnj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;
.implements Lfrx;


# instance fields
.field private final a:Lhzm;

.field private final b:Lfqh;

.field private c:Lhog;

.field private d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lhzm;Lfqh;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzm;

    iput-object v0, p0, Lfnj;->a:Lhzm;

    .line 43
    iput-object p2, p0, Lfnj;->b:Lfqh;

    .line 44
    return-void
.end method

.method private b()Lhog;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lfnj;->c:Lhog;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnj;->a:Lhzm;

    iget-object v0, v0, Lhzm;->b:Lhep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnj;->a:Lhzm;

    iget-object v0, v0, Lhzm;->b:Lhep;

    iget-object v0, v0, Lhep;->a:Lheo;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lfnj;->a:Lhzm;

    iget-object v0, v0, Lhzm;->b:Lhep;

    iget-object v0, v0, Lhep;->a:Lheo;

    iget-object v0, v0, Lheo;->a:Lhog;

    iput-object v0, p0, Lfnj;->c:Lhog;

    .line 103
    :cond_0
    iget-object v0, p0, Lfnj;->c:Lhog;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lfnj;->d:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 129
    invoke-direct {p0}, Lfnj;->b()Lhog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 130
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfnj;->d:Ljava/util/Set;

    .line 135
    :cond_0
    :goto_0
    iget-object v0, p0, Lfnj;->d:Ljava/util/Set;

    return-object v0

    .line 132
    :cond_1
    invoke-direct {p0}, Lfnj;->b()Lhog;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfnj;->d:Ljava/util/Set;

    goto :goto_0
.end method

.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 140
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 141
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lfnj;->a:Lhzm;

    iget-object v0, v0, Lhzm;->a:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lfnj;->b:Lfqh;

    return-object v0
.end method
