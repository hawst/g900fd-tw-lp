.class public final Laer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldoa;
.implements Ldok;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Laes;

.field public final c:Ldoj;

.field public final d:Z

.field public e:I

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:I

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public final o:Ldnz;

.field public p:Z

.field private q:Ladq;

.field private r:I

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ladq;Laes;Ldef;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Laer;->a:Landroid/content/Context;

    .line 94
    const-string v0, "activityProxy cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladq;

    iput-object v0, p0, Laer;->q:Ladq;

    .line 95
    const-string v0, "listener cannot be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laes;

    iput-object v0, p0, Laer;->b:Laes;

    .line 97
    iget-object v0, p2, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    iput v0, p0, Laer;->i:I

    .line 98
    iget-object v0, p2, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    iput v0, p0, Laer;->r:I

    .line 100
    new-instance v0, Ldoj;

    .line 101
    iget-object v3, p2, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-direct {v0, p1, v3, p0}, Ldoj;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Ldok;)V

    iput-object v0, p0, Laer;->c:Ldoj;

    .line 102
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Laer;->e:I

    .line 106
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 107
    iget-object v3, p2, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ActivityInfo;->configChanges:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    move v0, v1

    .line 118
    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Laer;->d:Z

    .line 120
    iput-boolean v1, p0, Laer;->j:Z

    .line 121
    iput-boolean v1, p0, Laer;->n:Z

    .line 122
    iput-boolean v2, p0, Laer;->k:Z

    .line 123
    iput-boolean v2, p0, Laer;->l:Z

    .line 125
    iput-boolean v1, p0, Laer;->f:Z

    .line 127
    const-string v0, "playerOverlaysLayout cannot be null"

    invoke-static {p4, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    new-instance v0, Ldnz;

    iget-object v1, p2, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 129
    iget-object v2, p2, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-direct {v0, v1, v2, p4, p0}, Ldnz;-><init>(Landroid/view/Window;Landroid/app/ActionBar;Ldef;Ldoa;)V

    iput-object v0, p0, Laer;->o:Ldnz;

    .line 130
    return-void

    :cond_0
    move v0, v2

    .line 112
    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    .line 118
    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Laer;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laer;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laer;->n:Z

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Laer;->b:Laes;

    invoke-interface {v0}, Laes;->a()V

    .line 279
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 142
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    .line 144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can not set FULLSCREEN_FLAG_FULLSCREEN_WHEN_DEVICE_LANDSCAPE without setting FULLSCREEN_FLAG_CONTROL_ORIENTATION"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_0
    iget-boolean v3, p0, Laer;->k:Z

    .line 151
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Laer;->j:Z

    .line 152
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Laer;->n:Z

    .line 153
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Laer;->k:Z

    .line 154
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_1

    move v2, v1

    :cond_1
    iput-boolean v2, p0, Laer;->l:Z

    .line 156
    iget-boolean v0, p0, Laer;->k:Z

    if-eqz v0, :cond_6

    .line 157
    iget-object v0, p0, Laer;->c:Ldoj;

    invoke-virtual {v0}, Ldoj;->enable()V

    .line 167
    :cond_2
    :goto_3
    iget-object v0, p0, Laer;->o:Ldnz;

    iget-boolean v1, p0, Laer;->l:Z

    iput-boolean v1, v0, Ldnz;->b:Z

    .line 168
    return-void

    :cond_3
    move v0, v2

    .line 151
    goto :goto_0

    :cond_4
    move v0, v2

    .line 152
    goto :goto_1

    :cond_5
    move v0, v2

    .line 153
    goto :goto_2

    .line 158
    :cond_6
    if-eqz v3, :cond_2

    iget v0, p0, Laer;->e:I

    if-ne v0, v1, :cond_2

    .line 160
    iget-object v0, p0, Laer;->c:Ldoj;

    invoke-virtual {v0}, Ldoj;->disable()V

    goto :goto_3
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 306
    iget v0, p0, Laer;->i:I

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    .line 307
    :goto_0
    iget-object v1, p0, Laer;->q:Ladq;

    iget-object v1, v1, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iget v2, p0, Laer;->r:I

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Laer;->q:Ladq;

    iget-object v1, v1, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Laer;->i:I

    .line 308
    :cond_0
    if-eqz v0, :cond_1

    iget p1, p0, Laer;->i:I

    :cond_1
    iput p1, p0, Laer;->r:I

    .line 309
    iget-object v0, p0, Laer;->q:Ladq;

    iget v1, p0, Laer;->r:I

    iget-object v0, v0, Ladq;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 310
    return-void

    .line 306
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 254
    iput-boolean v1, p0, Laer;->h:Z

    .line 255
    iget-boolean v0, p0, Laer;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laer;->j:Z

    if-eqz v0, :cond_0

    .line 256
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Laer;->k:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Laer;->g:Z

    if-nez v0, :cond_1

    .line 257
    iget-object v0, p0, Laer;->b:Laes;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Laes;->a(Z)V

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    if-nez p1, :cond_0

    invoke-virtual {p0}, Laer;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-boolean v0, p0, Laer;->d:Z

    if-eqz v0, :cond_2

    .line 267
    iget-object v0, p0, Laer;->b:Laes;

    invoke-interface {v0, v1}, Laes;->a(Z)V

    .line 269
    :cond_2
    iget v0, p0, Laer;->i:I

    invoke-virtual {p0, v0}, Laer;->b(I)V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 295
    iget v0, p0, Laer;->r:I

    iget v1, p0, Laer;->i:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b_(Z)V
    .locals 2

    .prologue
    .line 240
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Laer;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laer;->g:Z

    if-nez v0, :cond_0

    .line 246
    iget-boolean v0, p0, Laer;->h:Z

    if-nez v0, :cond_0

    .line 247
    iget-object v0, p0, Laer;->b:Laes;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Laes;->a(Z)V

    .line 250
    :cond_0
    return-void
.end method

.method public final c()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 332
    iget-boolean v0, p0, Laer;->j:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v2, p0, Laer;->n:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    :goto_1
    or-int/2addr v2, v0

    iget-boolean v0, p0, Laer;->k:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    :goto_2
    or-int/2addr v0, v2

    iget-boolean v2, p0, Laer;->l:Z

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    :cond_0
    or-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 355
    iput-boolean p1, p0, Laer;->s:Z

    .line 356
    invoke-virtual {p0}, Laer;->d()V

    .line 357
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 339
    iget-boolean v0, p0, Laer;->n:Z

    if-eqz v0, :cond_0

    .line 340
    iget-object v1, p0, Laer;->o:Ldnz;

    iget-boolean v0, p0, Laer;->p:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Laer;->s:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ldnz;->c(Z)V

    .line 343
    :cond_0
    return-void

    .line 340
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Laer;->o:Ldnz;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldnz;->b(Z)V

    .line 347
    invoke-virtual {p0}, Laer;->d()V

    .line 348
    return-void
.end method
