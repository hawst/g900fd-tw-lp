.class public final Lhcn;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[B

.field public b:Ljava/lang/String;

.field public c:Liaj;

.field private d:[Liaj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22275
    invoke-direct {p0}, Lidf;-><init>()V

    .line 22278
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhcn;->a:[B

    .line 22281
    sget-object v0, Liaj;->a:[Liaj;

    iput-object v0, p0, Lhcn;->d:[Liaj;

    .line 22284
    const-string v0, ""

    iput-object v0, p0, Lhcn;->b:Ljava/lang/String;

    .line 22287
    const/4 v0, 0x0

    iput-object v0, p0, Lhcn;->c:Liaj;

    .line 22275
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 22356
    .line 22357
    iget-object v0, p0, Lhcn;->a:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 22358
    const/4 v0, 0x1

    iget-object v2, p0, Lhcn;->a:[B

    .line 22359
    invoke-static {v0, v2}, Lidd;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22361
    :goto_0
    iget-object v2, p0, Lhcn;->d:[Liaj;

    if-eqz v2, :cond_1

    .line 22362
    iget-object v2, p0, Lhcn;->d:[Liaj;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 22363
    if-eqz v4, :cond_0

    .line 22364
    const/4 v5, 0x2

    .line 22365
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 22362
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 22369
    :cond_1
    iget-object v1, p0, Lhcn;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 22370
    const/4 v1, 0x3

    iget-object v2, p0, Lhcn;->b:Ljava/lang/String;

    .line 22371
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22373
    :cond_2
    iget-object v1, p0, Lhcn;->c:Liaj;

    if-eqz v1, :cond_3

    .line 22374
    const/4 v1, 0x4

    iget-object v2, p0, Lhcn;->c:Liaj;

    .line 22375
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22377
    :cond_3
    iget-object v1, p0, Lhcn;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22378
    iput v0, p0, Lhcn;->J:I

    .line 22379
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 22271
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhcn;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhcn;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhcn;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhcn;->a:[B

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhcn;->d:[Liaj;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liaj;

    iget-object v3, p0, Lhcn;->d:[Liaj;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhcn;->d:[Liaj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhcn;->d:[Liaj;

    :goto_2
    iget-object v2, p0, Lhcn;->d:[Liaj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhcn;->d:[Liaj;

    new-instance v3, Liaj;

    invoke-direct {v3}, Liaj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcn;->d:[Liaj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhcn;->d:[Liaj;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhcn;->d:[Liaj;

    new-instance v3, Liaj;

    invoke-direct {v3}, Liaj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcn;->d:[Liaj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhcn;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhcn;->c:Liaj;

    if-nez v0, :cond_5

    new-instance v0, Liaj;

    invoke-direct {v0}, Liaj;-><init>()V

    iput-object v0, p0, Lhcn;->c:Liaj;

    :cond_5
    iget-object v0, p0, Lhcn;->c:Liaj;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 22334
    iget-object v0, p0, Lhcn;->a:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22335
    const/4 v0, 0x1

    iget-object v1, p0, Lhcn;->a:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 22337
    :cond_0
    iget-object v0, p0, Lhcn;->d:[Liaj;

    if-eqz v0, :cond_2

    .line 22338
    iget-object v1, p0, Lhcn;->d:[Liaj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 22339
    if-eqz v3, :cond_1

    .line 22340
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 22338
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22344
    :cond_2
    iget-object v0, p0, Lhcn;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 22345
    const/4 v0, 0x3

    iget-object v1, p0, Lhcn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 22347
    :cond_3
    iget-object v0, p0, Lhcn;->c:Liaj;

    if-eqz v0, :cond_4

    .line 22348
    const/4 v0, 0x4

    iget-object v1, p0, Lhcn;->c:Liaj;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 22350
    :cond_4
    iget-object v0, p0, Lhcn;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 22352
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22301
    if-ne p1, p0, :cond_1

    .line 22308
    :cond_0
    :goto_0
    return v0

    .line 22302
    :cond_1
    instance-of v2, p1, Lhcn;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 22303
    :cond_2
    check-cast p1, Lhcn;

    .line 22304
    iget-object v2, p0, Lhcn;->a:[B

    iget-object v3, p1, Lhcn;->a:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhcn;->d:[Liaj;

    iget-object v3, p1, Lhcn;->d:[Liaj;

    .line 22305
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhcn;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhcn;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 22306
    :goto_1
    iget-object v2, p0, Lhcn;->c:Liaj;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhcn;->c:Liaj;

    if-nez v2, :cond_3

    .line 22307
    :goto_2
    iget-object v2, p0, Lhcn;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhcn;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 22308
    goto :goto_0

    .line 22305
    :cond_4
    iget-object v2, p0, Lhcn;->b:Ljava/lang/String;

    iget-object v3, p1, Lhcn;->b:Ljava/lang/String;

    .line 22306
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhcn;->c:Liaj;

    iget-object v3, p1, Lhcn;->c:Liaj;

    .line 22307
    invoke-virtual {v2, v3}, Liaj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhcn;->I:Ljava/util/List;

    iget-object v3, p1, Lhcn;->I:Ljava/util/List;

    .line 22308
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 22312
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 22314
    iget-object v2, p0, Lhcn;->a:[B

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 22320
    :cond_0
    iget-object v0, p0, Lhcn;->d:[Liaj;

    if-nez v0, :cond_3

    mul-int/lit8 v2, v2, 0x1f

    .line 22326
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhcn;->b:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 22327
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcn;->c:Liaj;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 22328
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhcn;->I:Ljava/util/List;

    if-nez v2, :cond_7

    :goto_2
    add-int/2addr v0, v1

    .line 22329
    return v0

    :cond_2
    move v2, v0

    move v0, v1

    .line 22316
    :goto_3
    iget-object v3, p0, Lhcn;->a:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 22317
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhcn;->a:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 22316
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v0, v1

    .line 22322
    :goto_4
    iget-object v3, p0, Lhcn;->d:[Liaj;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 22323
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhcn;->d:[Liaj;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    .line 22322
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 22323
    :cond_4
    iget-object v2, p0, Lhcn;->d:[Liaj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Liaj;->hashCode()I

    move-result v2

    goto :goto_5

    .line 22326
    :cond_5
    iget-object v0, p0, Lhcn;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 22327
    :cond_6
    iget-object v0, p0, Lhcn;->c:Liaj;

    invoke-virtual {v0}, Liaj;->hashCode()I

    move-result v0

    goto :goto_1

    .line 22328
    :cond_7
    iget-object v1, p0, Lhcn;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
