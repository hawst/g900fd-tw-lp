.class public Lfld;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhos;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field private final d:Lfqh;

.field private e:Lfnc;

.field private f:Lfnc;

.field private g:Lhut;


# direct methods
.method public constructor <init>(Lhos;Lfqh;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lfic;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhos;

    iput-object v0, p0, Lfld;->a:Lhos;

    .line 32
    iput-object p2, p0, Lfld;->d:Lfqh;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 91
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 92
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lfld;->a:Lhos;

    iget-object v0, v0, Lhos;->e:Lhog;

    return-object v0
.end method

.method public final c()Lfnc;
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lfld;->e:Lfnc;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfld;->a:Lhos;

    iget-object v1, v1, Lhos;->a:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfld;->e:Lfnc;

    .line 53
    :cond_0
    iget-object v0, p0, Lfld;->e:Lfnc;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lfld;->d:Lfqh;

    return-object v0
.end method

.method public final f()Lfnc;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lfld;->f:Lfnc;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfld;->a:Lhos;

    iget-object v1, v1, Lhos;->b:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfld;->f:Lfnc;

    .line 60
    :cond_0
    iget-object v0, p0, Lfld;->f:Lfnc;

    return-object v0
.end method

.method public final g()Lhut;
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lfld;->g:Lhut;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfld;->a:Lhos;

    iget-object v0, v0, Lhos;->f:[Lhut;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfld;->a:Lhos;

    iget-object v0, v0, Lhos;->f:[Lhut;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 72
    iget-object v0, p0, Lfld;->a:Lhos;

    iget-object v0, v0, Lhos;->f:[Lhut;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lfld;->g:Lhut;

    .line 74
    :cond_0
    iget-object v0, p0, Lfld;->g:Lhut;

    return-object v0
.end method
