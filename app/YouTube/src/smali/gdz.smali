.class final Lgdz;
.super Landroid/os/HandlerThread;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field a:Landroid/os/Handler;

.field private b:Lgdu;


# direct methods
.method public constructor <init>(Lgdu;)V
    .locals 1

    .prologue
    .line 733
    const-string v0, "YouTubePlayer.MediaPlayerThread"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 734
    iput-object p1, p0, Lgdz;->b:Lgdu;

    .line 735
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 761
    iget-object v0, p0, Lgdz;->a:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 762
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 765
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgdz;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 767
    iget-object v0, p0, Lgdz;->b:Lgdu;

    if-eqz v0, :cond_0

    .line 768
    iget-object v0, p0, Lgdz;->b:Lgdu;

    invoke-static {v0}, Lgdu;->a(Lgdu;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 770
    :cond_0
    monitor-exit p0

    return-void

    .line 765
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized handleMessage(Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 784
    monitor-enter p0

    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 808
    :goto_0
    monitor-exit p0

    return v0

    .line 786
    :pswitch_0
    :try_start_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 787
    iget-object v3, p0, Lgdz;->b:Lgdu;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Lgds;

    const/4 v4, 0x1

    aget-object v0, v0, v4

    check-cast v0, Landroid/net/Uri;

    invoke-static {v3, v1, v0}, Lgdu;->a(Lgdu;Lgds;Landroid/net/Uri;)V

    move v0, v2

    .line 788
    goto :goto_0

    .line 790
    :pswitch_1
    iget-object v0, p0, Lgdz;->b:Lgdu;

    invoke-static {v0}, Lgdu;->b(Lgdu;)V

    move v0, v2

    .line 791
    goto :goto_0

    .line 793
    :pswitch_2
    iget-object v0, p0, Lgdz;->b:Lgdu;

    invoke-static {v0}, Lgdu;->c(Lgdu;)V

    move v0, v2

    .line 794
    goto :goto_0

    .line 796
    :pswitch_3
    iget-object v0, p0, Lgdz;->b:Lgdu;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lgdu;->a(Lgdu;I)V

    move v0, v2

    .line 797
    goto :goto_0

    .line 799
    :pswitch_4
    iget-object v0, p0, Lgdz;->b:Lgdu;

    invoke-static {v0}, Lgdu;->a(Lgdu;)V

    move v0, v2

    .line 800
    goto :goto_0

    .line 802
    :pswitch_5
    iget-object v0, p0, Lgdz;->b:Lgdu;

    invoke-static {v0}, Lgdu;->a(Lgdu;)V

    .line 803
    invoke-virtual {p0}, Lgdz;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 804
    iget-object v0, p0, Lgdz;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 805
    const/4 v0, 0x0

    iput-object v0, p0, Lgdz;->b:Lgdu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v2

    .line 806
    goto :goto_0

    .line 784
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final quit()Z
    .locals 2

    .prologue
    .line 774
    invoke-virtual {p0}, Lgdz;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lgdz;->a:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 776
    const/4 v0, 0x1

    .line 778
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final start()V
    .locals 2

    .prologue
    .line 739
    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    .line 740
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lgdz;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lgdz;->a:Landroid/os/Handler;

    .line 741
    return-void
.end method
