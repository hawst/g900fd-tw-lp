.class public final Lgzd;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6442
    invoke-direct {p0}, Lidf;-><init>()V

    .line 6445
    iput v1, p0, Lgzd;->a:I

    .line 6448
    const-string v0, ""

    iput-object v0, p0, Lgzd;->b:Ljava/lang/String;

    .line 6451
    iput v1, p0, Lgzd;->c:I

    .line 6454
    iput v1, p0, Lgzd;->d:I

    .line 6457
    iput v1, p0, Lgzd;->e:I

    .line 6442
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 6518
    const/4 v0, 0x0

    .line 6519
    iget v1, p0, Lgzd;->a:I

    if-eqz v1, :cond_0

    .line 6520
    const/4 v0, 0x2

    iget v1, p0, Lgzd;->a:I

    .line 6521
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6523
    :cond_0
    iget-object v1, p0, Lgzd;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 6524
    const/4 v1, 0x3

    iget-object v2, p0, Lgzd;->b:Ljava/lang/String;

    .line 6525
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6527
    :cond_1
    iget v1, p0, Lgzd;->c:I

    if-eqz v1, :cond_2

    .line 6528
    const/4 v1, 0x4

    iget v2, p0, Lgzd;->c:I

    .line 6529
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6531
    :cond_2
    iget v1, p0, Lgzd;->d:I

    if-eqz v1, :cond_3

    .line 6532
    const/4 v1, 0x5

    iget v2, p0, Lgzd;->d:I

    .line 6533
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6535
    :cond_3
    iget v1, p0, Lgzd;->e:I

    if-eqz v1, :cond_4

    .line 6536
    const/4 v1, 0x6

    iget v2, p0, Lgzd;->e:I

    .line 6537
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6539
    :cond_4
    iget-object v1, p0, Lgzd;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6540
    iput v0, p0, Lgzd;->J:I

    .line 6541
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6438
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzd;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzd;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzd;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    iput v0, p0, Lgzd;->a:I

    goto :goto_0

    :cond_3
    iput v2, p0, Lgzd;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgzd;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    :cond_4
    iput v0, p0, Lgzd;->c:I

    goto :goto_0

    :cond_5
    iput v2, p0, Lgzd;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-eq v0, v6, :cond_6

    const/4 v1, 0x5

    if-eq v0, v1, :cond_6

    const/4 v1, 0x6

    if-eq v0, v1, :cond_6

    const/4 v1, 0x7

    if-ne v0, v1, :cond_7

    :cond_6
    iput v0, p0, Lgzd;->d:I

    goto :goto_0

    :cond_7
    iput v2, p0, Lgzd;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_8

    if-ne v0, v3, :cond_9

    :cond_8
    iput v0, p0, Lgzd;->e:I

    goto :goto_0

    :cond_9
    iput v2, p0, Lgzd;->e:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 6497
    iget v0, p0, Lgzd;->a:I

    if-eqz v0, :cond_0

    .line 6498
    const/4 v0, 0x2

    iget v1, p0, Lgzd;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 6500
    :cond_0
    iget-object v0, p0, Lgzd;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 6501
    const/4 v0, 0x3

    iget-object v1, p0, Lgzd;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 6503
    :cond_1
    iget v0, p0, Lgzd;->c:I

    if-eqz v0, :cond_2

    .line 6504
    const/4 v0, 0x4

    iget v1, p0, Lgzd;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 6506
    :cond_2
    iget v0, p0, Lgzd;->d:I

    if-eqz v0, :cond_3

    .line 6507
    const/4 v0, 0x5

    iget v1, p0, Lgzd;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 6509
    :cond_3
    iget v0, p0, Lgzd;->e:I

    if-eqz v0, :cond_4

    .line 6510
    const/4 v0, 0x6

    iget v1, p0, Lgzd;->e:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 6512
    :cond_4
    iget-object v0, p0, Lgzd;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 6514
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6472
    if-ne p1, p0, :cond_1

    .line 6480
    :cond_0
    :goto_0
    return v0

    .line 6473
    :cond_1
    instance-of v2, p1, Lgzd;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 6474
    :cond_2
    check-cast p1, Lgzd;

    .line 6475
    iget v2, p0, Lgzd;->a:I

    iget v3, p1, Lgzd;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzd;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzd;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 6476
    :goto_1
    iget v2, p0, Lgzd;->c:I

    iget v3, p1, Lgzd;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgzd;->d:I

    iget v3, p1, Lgzd;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgzd;->e:I

    iget v3, p1, Lgzd;->e:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzd;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzd;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 6480
    goto :goto_0

    .line 6475
    :cond_4
    iget-object v2, p0, Lgzd;->b:Ljava/lang/String;

    iget-object v3, p1, Lgzd;->b:Ljava/lang/String;

    .line 6476
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzd;->I:Ljava/util/List;

    iget-object v3, p1, Lgzd;->I:Ljava/util/List;

    .line 6480
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6484
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 6486
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgzd;->a:I

    add-int/2addr v0, v2

    .line 6487
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzd;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 6488
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgzd;->c:I

    add-int/2addr v0, v2

    .line 6489
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgzd;->d:I

    add-int/2addr v0, v2

    .line 6490
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgzd;->e:I

    add-int/2addr v0, v2

    .line 6491
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzd;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 6492
    return v0

    .line 6487
    :cond_0
    iget-object v0, p0, Lgzd;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 6491
    :cond_1
    iget-object v1, p0, Lgzd;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
