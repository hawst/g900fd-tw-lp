.class final Lgnq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/database/Cursor;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private synthetic j:Lgnp;


# direct methods
.method constructor <init>(Lgnp;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 840
    iput-object p1, p0, Lgnq;->j:Lgnp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 841
    iput-object p2, p0, Lgnq;->a:Landroid/database/Cursor;

    .line 843
    const-string v0, "id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgnq;->b:I

    .line 844
    const-string v0, "title"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgnq;->c:I

    .line 845
    const-string v0, "summary"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgnq;->d:I

    .line 846
    const-string v0, "author"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgnq;->e:I

    .line 847
    const-string v0, "updated_date"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgnq;->f:I

    .line 848
    const-string v0, "content_uri"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgnq;->g:I

    .line 849
    const-string v0, "size"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgnq;->h:I

    .line 850
    const-string v0, "placeholder"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgnq;->i:I

    .line 851
    return-void
.end method


# virtual methods
.method a()Lgbu;
    .locals 4

    .prologue
    .line 854
    iget-object v0, p0, Lgnq;->a:Landroid/database/Cursor;

    iget v1, p0, Lgnq;->b:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 855
    iget-object v1, p0, Lgnq;->j:Lgnp;

    iget-object v1, v1, Lgnp;->b:Lgmt;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgmt;->f(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 857
    new-instance v2, Lgbv;

    invoke-direct {v2}, Lgbv;-><init>()V

    .line 858
    iput-object v0, v2, Lgbv;->a:Ljava/lang/String;

    iget-object v0, p0, Lgnq;->a:Landroid/database/Cursor;

    iget v3, p0, Lgnq;->c:I

    .line 859
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lgbv;->b:Ljava/lang/String;

    iget-object v0, p0, Lgnq;->a:Landroid/database/Cursor;

    iget v3, p0, Lgnq;->d:I

    .line 860
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lgbv;->c:Ljava/lang/String;

    iget-object v0, p0, Lgnq;->a:Landroid/database/Cursor;

    iget v3, p0, Lgnq;->e:I

    .line 861
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lgbv;->d:Ljava/lang/String;

    iget-object v0, p0, Lgnq;->a:Landroid/database/Cursor;

    iget v3, p0, Lgnq;->f:I

    .line 862
    invoke-static {v0, v3}, Levj;->a(Landroid/database/Cursor;I)Ljava/util/Date;

    move-result-object v0

    iput-object v0, v2, Lgbv;->e:Ljava/util/Date;

    iget-object v0, p0, Lgnq;->a:Landroid/database/Cursor;

    iget v3, p0, Lgnq;->g:I

    .line 863
    invoke-static {v0, v3}, Levj;->b(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v2, Lgbv;->f:Landroid/net/Uri;

    iget-object v0, p0, Lgnq;->a:Landroid/database/Cursor;

    iget v3, p0, Lgnq;->h:I

    .line 864
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lgbv;->l:I

    .line 865
    iput-object v1, v2, Lgbv;->i:Landroid/net/Uri;

    .line 866
    iput-object v1, v2, Lgbv;->j:Landroid/net/Uri;

    .line 867
    iput-object v1, v2, Lgbv;->k:Landroid/net/Uri;

    iget-object v0, p0, Lgnq;->a:Landroid/database/Cursor;

    iget v1, p0, Lgnq;->i:I

    const/4 v3, 0x0

    .line 868
    invoke-static {v0, v1, v3}, Levj;->a(Landroid/database/Cursor;IZ)Z

    move-result v0

    iput-boolean v0, v2, Lgbv;->n:Z

    .line 869
    invoke-virtual {v2}, Lgbv;->a()Lgbu;

    move-result-object v0

    return-object v0
.end method

.method b()Ljava/util/List;
    .locals 2

    .prologue
    .line 873
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lgnq;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 874
    :goto_0
    iget-object v1, p0, Lgnq;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 875
    invoke-virtual {p0}, Lgnq;->a()Lgbu;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 877
    :cond_0
    return-object v0
.end method
