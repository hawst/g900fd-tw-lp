.class public final Lbfb;
.super Li;
.source "SourceFile"


# instance fields
.field private W:Landroid/widget/Spinner;

.field private X:Landroid/widget/Spinner;

.field private Y:Landroid/widget/Spinner;

.field private Z:Landroid/widget/CheckBox;

.field private aa:Landroid/widget/CheckBox;

.field private ab:Landroid/widget/CheckBox;

.field private ac:Landroid/widget/CheckBox;

.field private ad:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 497
    invoke-direct {p0}, Li;-><init>()V

    return-void
.end method

.method private a(Landroid/widget/CheckBox;Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 615
    invoke-virtual {p1, p3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 616
    new-instance v0, Lbfd;

    invoke-direct {v0, p0, p1}, Lbfd;-><init>(Lbfb;Landroid/widget/CheckBox;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 622
    return-void
.end method

.method private static a(Landroid/widget/Spinner;[Lbne;I)V
    .locals 5

    .prologue
    .line 603
    invoke-virtual {p0}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 604
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v0, 0x1090008

    invoke-direct {v2, v1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 606
    const v0, 0x1090009

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 607
    array-length v3, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 608
    invoke-interface {v4}, Lbne;->a()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 607
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 610
    :cond_0
    invoke-virtual {p0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 611
    invoke-virtual {p0, p2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 612
    return-void
.end method

.method static synthetic a(Lbfb;)V
    .locals 2

    .prologue
    .line 497
    invoke-virtual {p0}, Lbfb;->i()Lj;

    move-result-object v0

    instance-of v1, v0, Lbev;

    if-eqz v1, :cond_0

    check-cast v0, Lbev;

    invoke-direct {p0}, Lbfb;->z()Lbnf;

    move-result-object v1

    invoke-static {v0, v1}, Lbev;->a(Lbev;Lbnf;)V

    :cond_0
    return-void
.end method

.method private z()Lbnf;
    .locals 9

    .prologue
    .line 591
    new-instance v0, Lbnf;

    iget-object v1, p0, Lbfb;->W:Landroid/widget/Spinner;

    .line 592
    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    invoke-static {v1}, Lbnc;->a(I)Lbnc;

    move-result-object v1

    iget-object v2, p0, Lbfb;->X:Landroid/widget/Spinner;

    .line 593
    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    invoke-static {v2}, Lbnh;->a(I)Lbnh;

    move-result-object v2

    iget-object v3, p0, Lbfb;->Y:Landroid/widget/Spinner;

    .line 594
    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    invoke-static {v3}, Lbnd;->a(I)Lbnd;

    move-result-object v3

    iget-object v4, p0, Lbfb;->Z:Landroid/widget/CheckBox;

    .line 595
    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    iget-object v5, p0, Lbfb;->aa:Landroid/widget/CheckBox;

    .line 596
    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    iget-object v6, p0, Lbfb;->ab:Landroid/widget/CheckBox;

    .line 597
    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    iget-object v7, p0, Lbfb;->ac:Landroid/widget/CheckBox;

    .line 598
    invoke-virtual {v7}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v7

    iget-object v8, p0, Lbfb;->ad:Landroid/widget/CheckBox;

    .line 599
    invoke-virtual {v8}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v8

    invoke-direct/range {v0 .. v8}, Lbnf;-><init>(Lbnc;Lbnh;Lbnd;ZZZZZ)V

    return-object v0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 513
    if-eqz p1, :cond_0

    .line 514
    const-string v0, "search_filters"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbnf;

    move-object v1, v0

    .line 524
    :goto_0
    invoke-virtual {p0}, Lbfb;->j()Lo;

    move-result-object v2

    .line 525
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 526
    const v3, 0x7f0400f5

    invoke-virtual {v0, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 528
    const v0, 0x7f0802c9

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lbfb;->W:Landroid/widget/Spinner;

    .line 529
    iget-object v0, p0, Lbfb;->W:Landroid/widget/Spinner;

    .line 531
    invoke-static {}, Lbnc;->values()[Lbnc;

    move-result-object v4

    .line 532
    iget-object v5, v1, Lbnf;->b:Lbnc;

    invoke-virtual {v5}, Lbnc;->ordinal()I

    move-result v5

    .line 529
    invoke-static {v0, v4, v5}, Lbfb;->a(Landroid/widget/Spinner;[Lbne;I)V

    .line 534
    const v0, 0x7f0802ca

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lbfb;->X:Landroid/widget/Spinner;

    .line 535
    iget-object v0, p0, Lbfb;->X:Landroid/widget/Spinner;

    .line 537
    invoke-static {}, Lbnh;->values()[Lbnh;

    move-result-object v4

    .line 538
    iget-object v5, v1, Lbnf;->c:Lbnh;

    invoke-virtual {v5}, Lbnh;->ordinal()I

    move-result v5

    .line 535
    invoke-static {v0, v4, v5}, Lbfb;->a(Landroid/widget/Spinner;[Lbne;I)V

    .line 540
    const v0, 0x7f0802cb

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lbfb;->Y:Landroid/widget/Spinner;

    .line 541
    iget-object v0, p0, Lbfb;->Y:Landroid/widget/Spinner;

    .line 543
    invoke-static {}, Lbnd;->values()[Lbnd;

    move-result-object v4

    .line 544
    iget-object v5, v1, Lbnf;->d:Lbnd;

    invoke-virtual {v5}, Lbnd;->ordinal()I

    move-result v5

    .line 541
    invoke-static {v0, v4, v5}, Lbfb;->a(Landroid/widget/Spinner;[Lbne;I)V

    .line 546
    const v0, 0x7f0802cd

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbfb;->Z:Landroid/widget/CheckBox;

    .line 547
    iget-object v0, p0, Lbfb;->Z:Landroid/widget/CheckBox;

    const v4, 0x7f0802cc

    .line 549
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 550
    iget-boolean v5, v1, Lbnf;->e:Z

    .line 547
    invoke-direct {p0, v0, v4, v5}, Lbfb;->a(Landroid/widget/CheckBox;Landroid/view/View;Z)V

    .line 552
    const v0, 0x7f0802cf

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbfb;->aa:Landroid/widget/CheckBox;

    .line 553
    iget-object v0, p0, Lbfb;->aa:Landroid/widget/CheckBox;

    const v4, 0x7f0802ce

    .line 555
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 556
    iget-boolean v5, v1, Lbnf;->f:Z

    .line 553
    invoke-direct {p0, v0, v4, v5}, Lbfb;->a(Landroid/widget/CheckBox;Landroid/view/View;Z)V

    .line 558
    const v0, 0x7f0802d1

    .line 559
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbfb;->ab:Landroid/widget/CheckBox;

    .line 560
    iget-object v0, p0, Lbfb;->ab:Landroid/widget/CheckBox;

    const v4, 0x7f0802d0

    .line 562
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 563
    iget-boolean v5, v1, Lbnf;->g:Z

    .line 560
    invoke-direct {p0, v0, v4, v5}, Lbfb;->a(Landroid/widget/CheckBox;Landroid/view/View;Z)V

    .line 565
    const v0, 0x7f0802d3

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbfb;->ac:Landroid/widget/CheckBox;

    .line 566
    iget-object v0, p0, Lbfb;->ac:Landroid/widget/CheckBox;

    const v4, 0x7f0802d2

    .line 568
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 569
    iget-boolean v5, v1, Lbnf;->h:Z

    .line 566
    invoke-direct {p0, v0, v4, v5}, Lbfb;->a(Landroid/widget/CheckBox;Landroid/view/View;Z)V

    .line 571
    const v0, 0x7f0802d5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbfb;->ad:Landroid/widget/CheckBox;

    .line 572
    iget-object v0, p0, Lbfb;->ad:Landroid/widget/CheckBox;

    const v4, 0x7f0802d4

    .line 574
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 575
    iget-boolean v1, v1, Lbnf;->i:Z

    .line 572
    invoke-direct {p0, v0, v4, v1}, Lbfb;->a(Landroid/widget/CheckBox;Landroid/view/View;Z)V

    .line 577
    new-instance v0, Leyv;

    invoke-direct {v0, v2}, Leyv;-><init>(Landroid/content/Context;)V

    .line 578
    invoke-virtual {v0, v3}, Leyv;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900a1

    new-instance v2, Lbfc;

    invoke-direct {v2, p0}, Lbfc;-><init>(Lbfb;)V

    .line 579
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900a2

    .line 585
    invoke-virtual {v0, v1, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 586
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 587
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 516
    :cond_0
    invoke-virtual {p0}, Lbfb;->h()Landroid/os/Bundle;

    move-result-object v0

    .line 517
    if-eqz v0, :cond_1

    const-string v1, "search_filters"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 518
    const-string v1, "search_filters"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbnf;

    move-object v1, v0

    goto/16 :goto_0

    .line 520
    :cond_1
    sget-object v0, Lbnf;->a:Lbnf;

    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 626
    const-string v0, "search_filters"

    invoke-direct {p0}, Lbfb;->z()Lbnf;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 627
    return-void
.end method
