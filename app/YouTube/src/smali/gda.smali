.class public final Lgda;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leaw;
.implements Lgeh;


# instance fields
.field private final a:Lewi;

.field private final b:Lges;

.field private final c:Lgei;

.field private d:Lfqj;

.field private e:Ledm;

.field private f:Leau;

.field private g:Lecc;

.field private h:Z

.field private i:F

.field private j:Lfqy;

.field private k:Legc;

.field private l:Lewi;


# direct methods
.method public constructor <init>(Lewi;Lges;Lezj;Legc;Lewi;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Lgda;->a:Lewi;

    .line 73
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lges;

    iput-object v0, p0, Lgda;->b:Lges;

    .line 74
    new-instance v0, Lgdf;

    invoke-direct {v0, p3}, Lgdf;-><init>(Lezj;)V

    iput-object v0, p0, Lgda;->c:Lgei;

    .line 75
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lgda;->i:F

    .line 76
    iput-object p4, p0, Lgda;->k:Legc;

    .line 77
    iput-object p5, p0, Lgda;->l:Lewi;

    .line 78
    return-void
.end method

.method private a(Lfrf;Lfqy;)Lger;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 101
    if-eqz p1, :cond_1

    .line 102
    invoke-static {}, Lfqm;->k()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lfrf;->a(I)Lfqj;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v7

    :goto_0
    if-eqz v0, :cond_0

    move v0, v7

    :goto_1
    if-eqz v0, :cond_1

    .line 103
    iget-boolean v0, p1, Lfrf;->h:Z

    if-eqz v0, :cond_4

    .line 104
    :cond_1
    new-instance v0, Lgej;

    const-string v1, "No streaming data available for background playback."

    invoke-direct {v0, v1}, Lgej;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v5

    .line 102
    goto :goto_0

    :cond_3
    move v0, v5

    goto :goto_1

    .line 106
    :cond_4
    iget-object v0, p0, Lgda;->b:Lges;

    .line 108
    iget-object v2, p1, Lfrf;->c:Ljava/util/List;

    .line 109
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    .line 110
    invoke-static {}, Lfqm;->i()Ljava/util/Set;

    move-result-object v4

    move-object v1, p2

    move v6, v5

    move v8, v5

    .line 106
    invoke-interface/range {v0 .. v8}, Lges;->a(Lfqy;Ljava/util/Collection;Ljava/util/Set;Ljava/util/Set;ZZZZ)Lger;

    move-result-object v0

    return-object v0
.end method

.method private a(Ledm;I)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 193
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lgda;->f:Leau;

    invoke-virtual {v0}, Leau;->f()V

    .line 196
    :cond_0
    iget-object v0, p0, Lgda;->j:Lfqy;

    .line 197
    invoke-virtual {v0}, Lfqy;->j()I

    move-result v0

    const/16 v1, 0x7d0

    .line 196
    invoke-static {v12, v0, v1}, La;->a(III)Leau;

    move-result-object v0

    iput-object v0, p0, Lgda;->f:Leau;

    .line 198
    iget-object v0, p0, Lgda;->f:Leau;

    invoke-virtual {v0, p0}, Leau;->a(Leaw;)V

    .line 199
    iget-object v0, p0, Lgda;->f:Leau;

    int-to-long v4, p2

    invoke-virtual {v0, v4, v5}, Leau;->a(J)V

    .line 200
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 201
    iget-object v0, p0, Lgda;->l:Lewi;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lgda;->l:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v9, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 204
    :cond_1
    iget-object v0, p0, Lgda;->k:Legc;

    if-eqz v0, :cond_2

    .line 205
    iget-object v0, p0, Lgda;->k:Legc;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    :cond_2
    iget-object v8, p0, Lgda;->a:Lewi;

    iget-object v10, p0, Lgda;->j:Lfqy;

    new-instance v0, Leaq;

    new-instance v1, Leez;

    const v3, 0xa000

    invoke-direct {v1, v3}, Leez;-><init>(I)V

    invoke-virtual {v10}, Lfqy;->c()I

    move-result v4

    invoke-virtual {v10}, Lfqy;->d()I

    move-result v5

    invoke-virtual {v10}, Lfqy;->e()F

    move-result v6

    invoke-virtual {v10}, Lfqy;->f()F

    move-result v7

    move-object v3, v2

    invoke-direct/range {v0 .. v7}, Leaq;-><init>(Leex;Landroid/os/Handler;Lu;IIFF)V

    invoke-interface {v8}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lefc;

    new-instance v1, Lggc;

    new-instance v3, Ledb;

    invoke-direct {v3}, Ledb;-><init>()V

    new-instance v5, Legq;

    invoke-direct {v5}, Legq;-><init>()V

    invoke-virtual {v10}, Lfqy;->k()Z

    move-result v6

    new-array v8, v12, [Ledm;

    aput-object p1, v8, v11

    move-object v4, v9

    move v7, v11

    invoke-direct/range {v1 .. v8}, Lggc;-><init>(Lefc;Lecy;Ljava/util/List;Legq;ZZ[Ledm;)V

    new-instance v2, Lecl;

    const/high16 v3, 0x500000

    invoke-direct {v2, v1, v0, v3, v11}, Lecl;-><init>(Lecv;Leba;IZ)V

    new-instance v0, Lgdd;

    invoke-direct {v0, v2}, Lgdd;-><init>(Lebz;)V

    iput-object v0, p0, Lgda;->g:Lecc;

    .line 212
    iget-object v0, p0, Lgda;->f:Leau;

    new-array v1, v12, [Lecc;

    iget-object v2, p0, Lgda;->g:Lecc;

    aput-object v2, v1, v11

    invoke-virtual {v0, v1}, Leau;->a([Lecc;)V

    .line 213
    iget v0, p0, Lgda;->i:F

    invoke-virtual {p0, v0}, Lgda;->a(F)V

    .line 214
    iget-object v0, p0, Lgda;->c:Lgei;

    invoke-interface {v0, v12}, Lgei;->a(I)V

    .line 215
    invoke-virtual {p0}, Lgda;->e()V

    .line 216
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 266
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lgda;->f:Leau;

    invoke-virtual {v0}, Leau;->f()V

    .line 268
    iput-object v1, p0, Lgda;->f:Leau;

    .line 269
    iput-object v1, p0, Lgda;->g:Lecc;

    .line 271
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method public final a(F)V
    .locals 4

    .prologue
    .line 295
    iput p1, p0, Lgda;->i:F

    .line 296
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lgda;->f:Leau;

    iget-object v1, p0, Lgda;->g:Lecc;

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Leau;->a(Leav;ILjava/lang/Object;)V

    .line 299
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 317
    packed-switch p1, :pswitch_data_0

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 319
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgda;->h:Z

    .line 320
    iget-object v0, p0, Lgda;->c:Lgei;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Lgei;->a(I)V

    goto :goto_0

    .line 323
    :pswitch_1
    iget-boolean v0, p0, Lgda;->h:Z

    if-eqz v0, :cond_0

    .line 324
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgda;->h:Z

    .line 325
    iget-object v0, p0, Lgda;->c:Lgei;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lgei;->a(I)V

    goto :goto_0

    .line 329
    :pswitch_2
    iget-object v0, p0, Lgda;->c:Lgei;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lgei;->a(I)V

    goto :goto_0

    .line 317
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lgda;->c:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Landroid/os/Handler;)V

    .line 83
    return-void
.end method

.method public final a(Leat;)V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lgda;->c:Lgei;

    .line 337
    invoke-virtual {p0}, Lgda;->i()I

    move-result v1

    .line 336
    invoke-static {p1, v1}, Lgdj;->a(Leat;I)Lggp;

    move-result-object v1

    invoke-interface {v0, v1}, Lgei;->a(Lggp;)V

    .line 338
    return-void
.end method

.method public final a(Lfrf;ILjava/lang/String;Lfqy;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 125
    :try_start_0
    invoke-direct {p0, p1, p4}, Lgda;->a(Lfrf;Lfqy;)Lger;
    :try_end_0
    .catch Lgej; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 134
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqy;

    iput-object v0, p0, Lgda;->j:Lfqy;

    .line 135
    iget-object v0, v3, Lger;->b:[Lfqj;

    aget-object v2, v0, v6

    .line 136
    invoke-virtual {v2, p3}, Lfqj;->b(Ljava/lang/String;)Ledm;

    move-result-object v0

    .line 137
    iget-object v4, p0, Lgda;->f:Leau;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lgda;->e:Ledm;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lgda;->e:Ledm;

    iget-object v4, v4, Ledm;->e:Landroid/net/Uri;

    iget-object v5, v0, Ledm;->e:Landroid/net/Uri;

    .line 139
    invoke-virtual {v4, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 140
    :cond_0
    iget-object v4, p0, Lgda;->c:Lgei;

    invoke-interface {v4}, Lgei;->a()V

    .line 141
    iput-object v2, p0, Lgda;->d:Lfqj;

    .line 142
    iput-object v0, p0, Lgda;->e:Ledm;

    .line 143
    iget-object v0, p0, Lgda;->c:Lgei;

    invoke-interface {v0, v6}, Lgei;->a(I)V

    .line 144
    iget-object v0, p0, Lgda;->c:Lgei;

    .line 148
    iget-object v4, v3, Lger;->d:[Lfre;

    .line 149
    iget-object v5, v3, Lger;->e:[Ljava/lang/String;

    move-object v3, v1

    .line 144
    invoke-interface/range {v0 .. v6}, Lgei;->a(Lfqj;Lfqj;Lfqj;[Lfre;[Ljava/lang/String;I)V

    .line 151
    iget-object v0, p0, Lgda;->e:Ledm;

    invoke-direct {p0, v0, p2}, Lgda;->a(Ledm;I)V

    .line 155
    :goto_0
    invoke-virtual {p0}, Lgda;->e()V

    .line 156
    :goto_1
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    invoke-virtual {v0}, Lgej;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 128
    iput-object v1, p0, Lgda;->d:Lfqj;

    .line 129
    iput-object v1, p0, Lgda;->e:Ledm;

    .line 130
    iget-object v0, p0, Lgda;->c:Lgei;

    new-instance v1, Lggp;

    const-string v2, "fmt.noneavailable"

    invoke-direct {v1, v2, v6}, Lggp;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Lgei;->a(Lggp;)V

    goto :goto_1

    .line 153
    :cond_1
    invoke-virtual {p0, p2}, Lgda;->b(I)V

    goto :goto_0
.end method

.method public final a(Lgec;)V
    .locals 0

    .prologue
    .line 349
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0}, Lgda;->p()V

    .line 276
    return-void
.end method

.method public final a(Lfrf;Lfqy;Z)[Lfqj;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 168
    :try_start_0
    invoke-direct {p0, p1, p2}, Lgda;->a(Lfrf;Lfqy;)Lger;
    :try_end_0
    .catch Lgej; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 172
    const/4 v0, 0x1

    new-array v0, v0, [Lfqj;

    iget-object v1, v1, Lger;->b:[Lfqj;

    aget-object v1, v1, v2

    aput-object v1, v0, v2

    :goto_0
    return-object v0

    .line 170
    :catch_0
    move-exception v0

    sget-object v0, Lges;->a:[Lfqj;

    goto :goto_0
.end method

.method public final b(Lfrf;Lfqy;Z)Lger;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0, p1, p2}, Lgda;->a(Lfrf;Lfqy;)Lger;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 245
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lgda;->c:Lgei;

    invoke-interface {v0, p1}, Lgei;->b(I)V

    .line 247
    iget-object v0, p0, Lgda;->f:Leau;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Leau;->a(J)V

    .line 248
    iget-object v0, p0, Lgda;->c:Lgei;

    invoke-interface {v0, p1}, Lgei;->c(I)V

    .line 250
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lgda;->c:Lgei;

    invoke-interface {v0, p1}, Lgei;->b(Landroid/os/Handler;)V

    .line 88
    return-void
.end method

.method public final c()Lfqj;
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lfqj;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lgda;->d:Lfqj;

    return-object v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 220
    iget-object v0, p0, Lgda;->e:Ledm;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lgda;->f:Leau;

    if-nez v0, :cond_1

    .line 223
    iget-object v0, p0, Lgda;->e:Ledm;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lgda;->a(Ledm;I)V

    .line 231
    :goto_0
    iget-object v0, p0, Lgda;->c:Lgei;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lgei;->a(I)V

    .line 233
    :cond_0
    return-void

    .line 225
    :cond_1
    iget-object v0, p0, Lgda;->f:Leau;

    invoke-virtual {v0}, Leau;->b()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 227
    iget-object v0, p0, Lgda;->f:Leau;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Leau;->a(J)V

    .line 229
    :cond_2
    iget-object v0, p0, Lgda;->f:Leau;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leau;->a(Z)V

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lgda;->f:Leau;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leau;->a(Z)V

    .line 239
    iget-object v0, p0, Lgda;->c:Lgei;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lgei;->a(I)V

    .line 241
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 254
    invoke-virtual {p0}, Lgda;->h()V

    .line 255
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    .line 260
    invoke-direct {p0}, Lgda;->p()V

    .line 261
    iget-object v0, p0, Lgda;->c:Lgei;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lgei;->a(I)V

    .line 263
    :cond_0
    return-void
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgda;->f:Leau;

    invoke-virtual {v0}, Leau;->h()J

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgda;->f:Leau;

    invoke-virtual {v0}, Leau;->g()J

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgda;->f:Leau;

    invoke-virtual {v0}, Leau;->i()J

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgda;->f:Leau;

    invoke-virtual {v0}, Leau;->b()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lgda;->f:Leau;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgda;->f:Leau;

    .line 309
    invoke-virtual {v0}, Leau;->b()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgda;->f:Leau;

    .line 310
    invoke-virtual {v0}, Leau;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 346
    return-void
.end method

.method public final o()V
    .locals 0

    .prologue
    .line 352
    return-void
.end method
