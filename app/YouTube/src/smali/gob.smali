.class public final Lgob;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lexy;


# instance fields
.field private final a:Lgjp;

.field private final b:Lgll;

.field private final c:Lexz;

.field private final d:Lezj;

.field private final e:Lght;


# direct methods
.method public constructor <init>(Lgjp;Lgll;Lexz;Lezj;Lght;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lgob;->a:Lgjp;

    .line 110
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgll;

    iput-object v0, p0, Lgob;->b:Lgll;

    .line 111
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexz;

    iput-object v0, p0, Lgob;->c:Lexz;

    .line 112
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgob;->d:Lezj;

    .line 113
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lght;

    iput-object v0, p0, Lgob;->e:Lght;

    .line 114
    return-void
.end method

.method public static a(JLght;)Lead;
    .locals 4

    .prologue
    .line 137
    new-instance v0, Lead;

    invoke-direct {v0}, Lead;-><init>()V

    sget-object v1, Lgnz;->b:Ljava/lang/String;

    .line 138
    invoke-virtual {v0, v1}, Lead;->a(Ljava/lang/String;)Lead;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    .line 139
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v2, p0

    invoke-virtual {v0, v2, v3}, Lead;->a(J)Lead;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    .line 141
    invoke-virtual {p2}, Lght;->e()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 140
    invoke-virtual {v0, v2, v3}, Lead;->b(J)Lead;

    move-result-object v0

    .line 142
    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lead;)Lexx;
    .locals 7

    .prologue
    .line 94
    new-instance v0, Lgnz;

    iget-object v2, p0, Lgob;->a:Lgjp;

    iget-object v3, p0, Lgob;->b:Lgll;

    iget-object v4, p0, Lgob;->c:Lexz;

    iget-object v5, p0, Lgob;->d:Lezj;

    iget-object v6, p0, Lgob;->e:Lght;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lgnz;-><init>(Lead;Lgjp;Lgll;Lexz;Lezj;Lght;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lgnz;->b:Ljava/lang/String;

    return-object v0
.end method
