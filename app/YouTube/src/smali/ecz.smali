.class public Lecz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lecy;


# instance fields
.field private final a:Leey;

.field private final b:I

.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:F


# direct methods
.method public constructor <init>(Leey;IIIIF)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x3e8

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    iput-object p1, p0, Lecz;->a:Leey;

    .line 202
    iput p2, p0, Lecz;->b:I

    .line 203
    int-to-long v0, p3

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lecz;->c:J

    .line 204
    int-to-long v0, p4

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lecz;->d:J

    .line 205
    int-to-long v0, p5

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lecz;->e:J

    .line 206
    iput p6, p0, Lecz;->f:F

    .line 207
    return-void
.end method


# virtual methods
.method public a(J)J
    .locals 3

    .prologue
    .line 282
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, Lecz;->b:I

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    long-to-float v0, p1

    iget v1, p0, Lecz;->f:F

    mul-float/2addr v0, v1

    float-to-long v0, v0

    goto :goto_0
.end method

.method public a([Lecw;J)Lecw;
    .locals 6

    .prologue
    .line 267
    invoke-virtual {p0, p2, p3}, Lecz;->a(J)J

    move-result-wide v2

    .line 268
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 269
    aget-object v1, p1, v0

    .line 270
    iget v4, v1, Lecw;->e:I

    int-to-long v4, v4

    cmp-long v4, v4, v2

    if-gtz v4, :cond_0

    move-object v0, v1

    .line 275
    :goto_1
    return-object v0

    .line 268
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275
    :cond_1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    goto :goto_1
.end method

.method public a(Ljava/util/List;J[Lecw;Leda;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 222
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x0

    .line 224
    :goto_0
    iget-object v3, p5, Leda;->c:Lecw;

    .line 225
    iget-object v4, p0, Lecz;->a:Leey;

    invoke-interface {v4}, Leey;->a()J

    move-result-wide v6

    invoke-virtual {p0, p4, v6, v7}, Lecz;->a([Lecw;J)Lecw;

    move-result-object v4

    .line 226
    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    iget v6, v4, Lecw;->e:I

    iget v7, v3, Lecw;->e:I

    if-le v6, v7, :cond_3

    move v6, v2

    .line 227
    :goto_1
    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    iget v7, v4, Lecw;->e:I

    iget v8, v3, Lecw;->e:I

    if-ge v7, v8, :cond_0

    move v5, v2

    .line 228
    :cond_0
    if-eqz v6, :cond_7

    .line 229
    iget-wide v6, p0, Lecz;->c:J

    cmp-long v5, v0, v6

    if-gez v5, :cond_4

    move-object v0, v3

    .line 257
    :goto_2
    if-eqz v3, :cond_1

    if-eq v0, v3, :cond_1

    .line 258
    const/4 v1, 0x2

    iput v1, p5, Leda;->b:I

    .line 260
    :cond_1
    iput-object v0, p5, Leda;->c:Lecw;

    .line 261
    return-void

    .line 223
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    iget-wide v0, v0, Ledc;->g:J

    sub-long/2addr v0, p2

    goto :goto_0

    :cond_3
    move v6, v5

    .line 226
    goto :goto_1

    .line 233
    :cond_4
    iget-wide v6, p0, Lecz;->e:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_8

    move v1, v2

    .line 237
    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 238
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    .line 239
    iget-wide v6, v0, Ledc;->f:J

    sub-long/2addr v6, p2

    .line 240
    iget-wide v8, p0, Lecz;->e:J

    cmp-long v2, v6, v8

    if-ltz v2, :cond_5

    iget-object v2, v0, Ledc;->a:Lecw;

    iget v2, v2, Lecw;->e:I

    iget v5, v4, Lecw;->e:I

    if-ge v2, v5, :cond_5

    iget-object v2, v0, Ledc;->a:Lecw;

    iget v2, v2, Lecw;->d:I

    iget v5, v4, Lecw;->d:I

    if-ge v2, v5, :cond_5

    iget-object v2, v0, Ledc;->a:Lecw;

    iget v2, v2, Lecw;->d:I

    const/16 v5, 0x2d0

    if-ge v2, v5, :cond_5

    iget-object v0, v0, Ledc;->a:Lecw;

    iget v0, v0, Lecw;->c:I

    const/16 v2, 0x500

    if-ge v0, v2, :cond_5

    .line 246
    iput v1, p5, Leda;->a:I

    move-object v0, v4

    .line 247
    goto :goto_2

    .line 237
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_6
    move-object v0, v4

    goto :goto_2

    .line 251
    :cond_7
    if-eqz v5, :cond_8

    if-eqz v3, :cond_8

    iget-wide v6, p0, Lecz;->d:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_8

    move-object v0, v3

    .line 255
    goto :goto_2

    :cond_8
    move-object v0, v4

    goto :goto_2
.end method
