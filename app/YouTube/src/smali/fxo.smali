.class public abstract Lfxo;
.super Lgil;
.source "SourceFile"


# static fields
.field private static final b:Lfba;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lfbb;

    invoke-direct {v0}, Lfbb;-><init>()V

    const-string v1, "/errors"

    new-instance v2, Lfxu;

    invoke-direct {v2}, Lfxu;-><init>()V

    .line 33
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/errors/error"

    new-instance v2, Lfxt;

    invoke-direct {v2}, Lfxt;-><init>()V

    .line 39
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/errors/error/domain"

    new-instance v2, Lfxs;

    invoke-direct {v2}, Lfxs;-><init>()V

    .line 51
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/errors/error/code"

    new-instance v2, Lfxr;

    invoke-direct {v2}, Lfxr;-><init>()V

    .line 57
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/errors/error/location"

    new-instance v2, Lfxq;

    invoke-direct {v2}, Lfxq;-><init>()V

    .line 63
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/errors/error/internalReason"

    new-instance v2, Lfxp;

    invoke-direct {v2}, Lfxp;-><init>()V

    .line 69
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lfbb;->a()Lfba;

    move-result-object v0

    sput-object v0, Lfxo;->b:Lfba;

    .line 32
    return-void
.end method

.method public constructor <init>(Lfbc;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lgil;-><init>(Lfbc;)V

    .line 79
    return-void
.end method

.method public static a(Lfbc;)Lfxo;
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lfxv;

    invoke-direct {v0, p0}, Lfxv;-><init>(Lfbc;)V

    return-object v0
.end method


# virtual methods
.method protected final a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;
    .locals 6

    .prologue
    const/16 v5, 0x191

    .line 83
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 84
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 85
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v3

    .line 88
    const-string v0, "Content-Type"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    const/16 v1, 0x190

    if-eq v2, v1, :cond_0

    if-eq v2, v5, :cond_0

    const/16 v1, 0x193

    if-eq v2, v1, :cond_0

    const/16 v1, 0x1f7

    if-ne v2, v1, :cond_1

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, "xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 90
    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lfxo;->a:Lfbc;

    sget-object v4, Lfxo;->b:Lfba;

    invoke-virtual {v1, v0, v4}, Lfbc;->a(Ljava/io/InputStream;Lfba;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 93
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_2

    .line 94
    new-instance v1, Lfxw;

    invoke-direct {v1, v2, v3, v0}, Lfxw;-><init>(ILjava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 108
    :goto_1
    return-object v0

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 103
    :cond_2
    if-ne v2, v5, :cond_3

    const-string v0, "NoLinkedYouTubeAccount"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104
    invoke-static {v2, v3}, Lfxw;->a(ILjava/lang/String;)Lfxw;

    move-result-object v0

    goto :goto_1

    .line 108
    :cond_3
    invoke-super {p0, p1}, Lgil;->a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;

    move-result-object v0

    goto :goto_1
.end method
