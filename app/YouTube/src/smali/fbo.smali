.class public final Lfbo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lfbo;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lfbo;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->b(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lfbo;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-static {v1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 94
    iget-object v0, p0, Lfbo;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iget-object v1, p0, Lfbo;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-static {v1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)I

    move-result v1

    iget-object v2, p0, Lfbo;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-static {v2}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;I)I

    .line 95
    iget-object v1, p0, Lfbo;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iget-object v0, p0, Lfbo;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->c(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)I

    move-result v0

    iget-object v2, p0, Lfbo;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-static {v2}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;Z)Z

    .line 96
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
