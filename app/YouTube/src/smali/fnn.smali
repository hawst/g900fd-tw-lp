.class public final Lfnn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/graphics/PointF;

.field private b:Landroid/graphics/PointF;

.field private c:Ljava/lang/String;

.field private final d:Lial;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 14
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 15
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lfnn;->a:Landroid/graphics/PointF;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lfnn;->d:Lial;

    iget-object v1, v1, Lial;->c:Liam;

    iget v1, v1, Liam;->a:F

    iget-object v2, p0, Lfnn;->d:Lial;

    iget-object v2, v2, Lial;->c:Liam;

    iget v2, v2, Liam;->b:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lfnn;->a:Landroid/graphics/PointF;

    .line 48
    :cond_0
    iget-object v0, p0, Lfnn;->a:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final b()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lfnn;->b:Landroid/graphics/PointF;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lfnn;->d:Lial;

    iget-object v1, v1, Lial;->c:Liam;

    iget v1, v1, Liam;->c:F

    iget-object v2, p0, Lfnn;->d:Lial;

    iget-object v2, v2, Lial;->c:Liam;

    iget v2, v2, Liam;->d:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lfnn;->b:Landroid/graphics/PointF;

    .line 55
    :cond_0
    iget-object v0, p0, Lfnn;->b:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lfnn;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lfnn;->d:Lial;

    iget-object v0, v0, Lial;->b:Ljava/lang/String;

    iput-object v0, p0, Lfnn;->c:Ljava/lang/String;

    .line 62
    :cond_0
    iget-object v0, p0, Lfnn;->c:Ljava/lang/String;

    return-object v0
.end method
