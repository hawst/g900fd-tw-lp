.class public final enum Ldev;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static enum a:Ldev;

.field private static enum b:Ldev;

.field private static enum c:Ldev;

.field private static enum d:Ldev;

.field private static enum e:Ldev;

.field private static h:[Ljava/lang/String;

.field private static i:[Ljava/lang/String;

.field private static final synthetic j:[Ldev;


# instance fields
.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 375
    new-instance v0, Ldev;

    const-string v1, "NONE"

    const v2, 0x7f090159

    invoke-direct {v0, v1, v3, v2, v3}, Ldev;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldev;->a:Ldev;

    .line 376
    new-instance v0, Ldev;

    const-string v1, "UNIFORM"

    const v2, 0x7f090174

    invoke-direct {v0, v1, v4, v2, v4}, Ldev;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldev;->b:Ldev;

    .line 377
    new-instance v0, Ldev;

    const-string v1, "DROP_SHADOW"

    const v2, 0x7f090175

    invoke-direct {v0, v1, v5, v2, v5}, Ldev;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldev;->c:Ldev;

    .line 378
    new-instance v0, Ldev;

    const-string v1, "RAISED"

    const v2, 0x7f090172

    invoke-direct {v0, v1, v6, v2, v6}, Ldev;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldev;->d:Ldev;

    .line 379
    new-instance v0, Ldev;

    const-string v1, "DEPRESSED"

    const v2, 0x7f090173

    invoke-direct {v0, v1, v7, v2, v7}, Ldev;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldev;->e:Ldev;

    .line 374
    const/4 v0, 0x5

    new-array v0, v0, [Ldev;

    sget-object v1, Ldev;->a:Ldev;

    aput-object v1, v0, v3

    sget-object v1, Ldev;->b:Ldev;

    aput-object v1, v0, v4

    sget-object v1, Ldev;->c:Ldev;

    aput-object v1, v0, v5

    sget-object v1, Ldev;->d:Ldev;

    aput-object v1, v0, v6

    sget-object v1, Ldev;->e:Ldev;

    aput-object v1, v0, v7

    sput-object v0, Ldev;->j:[Ldev;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0

    .prologue
    .line 386
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 387
    iput p3, p0, Ldev;->f:I

    .line 388
    iput p4, p0, Ldev;->g:I

    .line 389
    return-void
.end method

.method public static a()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 405
    sget-object v0, Ldev;->i:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 406
    invoke-static {}, Ldev;->values()[Ldev;

    move-result-object v1

    .line 407
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Ldev;->i:[Ljava/lang/String;

    .line 408
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 409
    sget-object v2, Ldev;->i:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Ldev;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 408
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 412
    :cond_0
    sget-object v0, Ldev;->i:[Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 394
    sget-object v0, Ldev;->h:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 395
    invoke-static {}, Ldev;->values()[Ldev;

    move-result-object v1

    .line 396
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Ldev;->h:[Ljava/lang/String;

    .line 397
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 398
    sget-object v2, Ldev;->h:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Ldev;->f:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 397
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 401
    :cond_0
    sget-object v0, Ldev;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public static b()I
    .locals 2

    .prologue
    .line 416
    invoke-static {}, Ldev;->values()[Ldev;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v0, v0, Ldev;->g:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldev;
    .locals 1

    .prologue
    .line 374
    const-class v0, Ldev;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldev;

    return-object v0
.end method

.method public static values()[Ldev;
    .locals 1

    .prologue
    .line 374
    sget-object v0, Ldev;->j:[Ldev;

    invoke-virtual {v0}, [Ldev;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldev;

    return-object v0
.end method
