.class final Ldug;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lejx;


# instance fields
.field private synthetic a:Z

.field private synthetic b:Ldub;


# direct methods
.method constructor <init>(Ldub;Z)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Ldug;->b:Ldub;

    iput-boolean p2, p0, Ldug;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lejw;)V
    .locals 10

    .prologue
    .line 417
    check-cast p1, Leho;

    iget-object v0, p0, Ldug;->b:Ldub;

    iget-object v0, v0, Ldub;->i:Ldum;

    sget-object v1, Ldum;->d:Ldum;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldug;->b:Ldub;

    const-string v1, "connectionCallback: no longer connected"

    invoke-virtual {v0, v1}, Ldub;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldug;->b:Ldub;

    iget-object v0, v0, Ldub;->f:Lejr;

    if-nez v0, :cond_1

    iget-object v0, p0, Ldug;->b:Ldub;

    const-string v1, "connectionCallback: no connected client"

    invoke-virtual {v0, v1}, Ldub;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Leho;->c_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iget-object v1, p0, Ldug;->b:Ldub;

    invoke-interface {p1}, Leho;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Ldub;->g:Ljava/lang/String;

    iget-object v1, p0, Ldug;->b:Ldub;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->a()Z

    move-result v3

    invoke-interface {p1}, Leho;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Ldug;->b:Ldub;

    iget-object v5, v5, Ldub;->g:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1f

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "onConnectionResult: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ": "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldub;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ldug;->b:Ldub;

    invoke-virtual {v0}, Ldub;->c()V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Ldug;->b:Ldub;

    iget-object v0, v0, Ldub;->c:Ldwj;

    invoke-virtual {v0}, Ldwj;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Ldug;->b:Ldub;

    const-string v1, "No video to fling locally"

    invoke-virtual {v0, v1}, Ldub;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    iget-boolean v0, p0, Ldug;->a:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Ldug;->b:Ldub;

    const-string v1, "Not flinging video locally because of reconnection"

    invoke-virtual {v0, v1}, Ldub;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Ldug;->b:Ldub;

    :try_start_0
    iget-object v0, v1, Ldub;->c:Ldwj;

    iget-object v0, v0, Ldwj;->a:Ljava/lang/String;

    iget-object v2, v1, Ldub;->c:Ldwj;

    iget v2, v2, Ldwj;->b:I

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v4, "{\"type\":\"flingVideo\",\"data\":{\"videoId\":\"%s\",\"currentTime\":%.2f}}"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    int-to-double v6, v2

    const-wide v8, 0x408f400000000000L    # 1000.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "for the performance purposes sending message: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ldub;->a(Ljava/lang/String;)V

    sget-object v0, Lehm;->c:Lehp;

    iget-object v3, v1, Ldub;->f:Lejr;

    const-string v4, "urn:x-cast:com.google.youtube.mdx"

    invoke-virtual {v0, v3, v4, v2}, Lehp;->a(Lejr;Ljava/lang/String;Ljava/lang/String;)Leju;

    move-result-object v0

    new-instance v3, Lduh;

    invoke-direct {v3, v1, v2}, Lduh;-><init>(Ldub;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Leju;->a(Lejx;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v0, "can\'t send message over Cast"

    invoke-virtual {v1, v0}, Ldub;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
