.class public Lcw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbnn;
.implements Lcao;
.implements Lcuk;
.implements Lfsk;
.implements Lle;


# instance fields
.field public final A:Leyp;

.field public final B:Levn;

.field public final C:Leyt;

.field public final D:Lfdw;

.field public final E:Lfrz;

.field public final F:Ljava/lang/String;

.field public volatile G:Ljava/lang/Object;

.field public final H:Lu;

.field public final I:Ljava/util/concurrent/CountDownLatch;

.field public J:J

.field public K:J

.field public final a:Landroid/support/v7/widget/Toolbar;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Landroid/view/View;

.field public final e:Landroid/view/View;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/widget/TextView;

.field public final h:Lfvi;

.field public final i:Lbqn;

.field public final synthetic j:Ljava/lang/String;

.field public final synthetic k:Lbrg;

.field public final synthetic l:Lbym;

.field public final m:Lfus;

.field public final n:Landroid/app/SearchableInfo;

.field public o:Landroid/view/MenuItem;

.field public p:Landroid/support/v7/widget/SearchView;

.field public q:Ljava/lang/Boolean;

.field public final r:Lcau;

.field public final s:Ljava/util/List;

.field public t:Ljava/lang/CharSequence;

.field public u:Ljava/lang/CharSequence;

.field public final v:Lbhz;

.field public final w:Lffs;

.field public final x:Lfhz;

.field public final y:Lgix;

.field public final z:Lcub;


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcw;->I:Ljava/util/concurrent/CountDownLatch;

    .line 26
    iput-wide v2, p0, Lcw;->J:J

    .line 27
    iput-wide v2, p0, Lcw;->K:J

    .line 30
    return-void
.end method

.method public static synthetic a(Lcw;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcw;->q:Ljava/lang/Boolean;

    return-object p1
.end method

.method public static synthetic a(Lcw;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 794
    iget-object v0, p0, Lcw;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcw;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcw;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcw;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 794
    iget-object v0, p0, Lcw;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic c(Lcw;)Lfvi;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcw;->h:Lfvi;

    return-object v0
.end method

.method public static synthetic d(Lcw;)Landroid/support/v7/widget/SearchView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method public static synthetic e(Lcw;)Ljava/util/List;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcw;->s:Ljava/util/List;

    return-object v0
.end method

.method public static synthetic f(Lcw;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcw;->u:Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 621
    iget-object v0, p0, Lcw;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x102002c

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 623
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 624
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 625
    return-object v1
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Lcw;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->b(I)V

    .line 617
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcw;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 611
    iget-object v0, p0, Lcw;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/Toolbar;->b(I)V

    .line 612
    return-void
.end method

.method public a(Landroid/view/MenuItem;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 62
    iput-object p1, p0, Lcw;->o:Landroid/view/MenuItem;

    .line 64
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    invoke-static {v0}, Lez;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    iput-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    .line 65
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    iget-object v1, p0, Lcw;->n:Landroid/app/SearchableInfo;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 66
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView;->setQueryRefinementEnabled(Z)V

    .line 68
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    new-instance v1, Lcas;

    invoke-direct {v1, p0}, Lcas;-><init>(Lcw;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Lvo;)V

    .line 81
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    new-instance v1, Lcat;

    invoke-direct {v1, p0}, Lcat;-><init>(Lcw;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setOnSuggestionListener(Lvp;)V

    .line 104
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 105
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lez;->a(Landroid/view/MenuItem;I)V

    .line 109
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    iget-object v1, p0, Lcw;->r:Lcau;

    invoke-static {v0, v1}, Lez;->a(Landroid/view/MenuItem;Lff;)Landroid/view/MenuItem;

    .line 114
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    iget-object v1, p0, Lcw;->t:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 117
    :cond_0
    return-void
.end method

.method public a(Lcap;)V
    .locals 1

    .prologue
    .line 180
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    iget-object v0, p0, Lcw;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    return-void
.end method

.method public a(Lcua;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 75
    iget-object v0, p0, Lcw;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcw;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 78
    iget-object v0, p1, Lcua;->d:Landroid/text/Spanned;

    .line 79
    if-eqz v0, :cond_0

    .line 80
    iget-object v1, p0, Lcw;->f:Landroid/widget/TextView;

    invoke-static {v1, v0}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcw;->g:Landroid/widget/TextView;

    iget-object v1, p1, Lcua;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 86
    :goto_0
    iget-object v0, p1, Lcua;->e:Lfnc;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcw;->h:Lfvi;

    .line 88
    iget-object v1, p1, Lcua;->e:Lfnc;

    iget-object v2, p0, Lcw;->i:Lbqn;

    .line 87
    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    .line 93
    :goto_1
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcw;->f:Landroid/widget/TextView;

    iget-object v1, p1, Lcua;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcw;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 91
    :cond_1
    iget-object v0, p0, Lcw;->h:Lfvi;

    const v1, 0x7f0201e9

    invoke-virtual {v0, v1}, Lfvi;->c(I)V

    goto :goto_1
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcw;->l:Lbym;

    iget-object v0, v0, Lbym;->i:Leyt;

    invoke-interface {v0, p1}, Leyt;->c(Ljava/lang/Throwable;)V

    .line 210
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    invoke-static {v0}, Lez;->d(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 167
    :cond_0
    iput-object p1, p0, Lcw;->u:Ljava/lang/CharSequence;

    .line 168
    return-void
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcw;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcap;)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcw;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 187
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcw;->m:Lfus;

    invoke-virtual {v0, p1}, Lfus;->b(Ljava/lang/String;)V

    .line 200
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcw;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcw;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 99
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 103
    iget-object v0, p0, Lcw;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcw;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 105
    return-void
.end method

.method public e()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 199
    iget-object v4, p0, Lcw;->l:Lbym;

    iget-object v5, p0, Lcw;->j:Ljava/lang/String;

    iget-object v6, p0, Lcw;->k:Lbrg;

    invoke-virtual {v4, v5}, Lbym;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lbrg;->a:Lbrg;

    if-ne v6, v2, :cond_0

    move v2, v0

    :goto_0
    sget-object v3, Lbrg;->b:Lbrg;

    if-ne v6, v3, :cond_1

    move v3, v0

    :goto_1
    iget-object v0, v4, Lbym;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    invoke-static {v1, v2}, Lbym;->a(Landroid/view/View;Z)V

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v3}, Lbym;->a(Landroid/view/View;Z)V

    goto :goto_2

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v3, v1

    goto :goto_1

    :cond_2
    new-instance v0, Lbyy;

    invoke-direct {v0, v4, v6, v5}, Lbyy;-><init>(Lbym;Lbrg;Ljava/lang/String;)V

    sget-object v1, Lbyp;->a:[I

    invoke-virtual {v6}, Lbrg;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 200
    :goto_3
    return-void

    .line 199
    :pswitch_0
    iget-object v1, v4, Lbym;->f:Lfeb;

    invoke-virtual {v1}, Lfeb;->a()Lfef;

    move-result-object v1

    invoke-virtual {v1, v5}, Lfef;->a(Ljava/lang/String;)Lfec;

    sget-object v2, Lfhy;->a:[B

    invoke-virtual {v1, v2}, Lfef;->a([B)V

    iget-object v2, v4, Lbym;->f:Lfeb;

    invoke-virtual {v2, v1, v0}, Lfeb;->a(Lfef;Lwv;)V

    goto :goto_3

    :pswitch_1
    iget-object v1, v4, Lbym;->f:Lfeb;

    invoke-virtual {v1}, Lfeb;->b()Lfed;

    move-result-object v1

    invoke-virtual {v1, v5}, Lfed;->a(Ljava/lang/String;)Lfec;

    sget-object v2, Lfhy;->a:[B

    invoke-virtual {v1, v2}, Lfed;->a([B)V

    iget-object v2, v4, Lbym;->f:Lfeb;

    invoke-virtual {v2, v1, v0}, Lfeb;->a(Lfed;Lwv;)V

    goto :goto_3

    :pswitch_2
    iget-object v1, v4, Lbym;->f:Lfeb;

    invoke-virtual {v1}, Lfeb;->c()Lfeh;

    move-result-object v1

    invoke-virtual {v1, v5}, Lfeh;->a(Ljava/lang/String;)Lfec;

    sget-object v2, Lfhy;->a:[B

    invoke-virtual {v1, v2}, Lfeh;->a([B)V

    iget-object v2, v4, Lbym;->f:Lfeb;

    invoke-virtual {v2, v1, v0}, Lfeb;->a(Lfeh;Lwv;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public f()V
    .locals 0

    .prologue
    .line 205
    return-void
.end method

.method public g()I
    .locals 1

    .prologue
    .line 52
    const v0, 0x7f08036c

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 57
    const v0, 0x7f110004

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcw;->q:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->isIconified()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcw;->q:Ljava/lang/Boolean;

    .line 130
    :cond_0
    iget-object v0, p0, Lcw;->q:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 135
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    if-nez v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setSubmitButtonEnabled(Z)V

    .line 139
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setIconified(Z)V

    .line 141
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    invoke-static {v0}, Lez;->b(Landroid/view/MenuItem;)Z

    goto :goto_0
.end method

.method public l()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    if-nez v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setIconified(Z)V

    .line 154
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    invoke-static {v0}, Lez;->d(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcw;->o:Landroid/view/MenuItem;

    invoke-static {v0}, Lez;->c(Landroid/view/MenuItem;)Z

    goto :goto_0
.end method

.method public m()V
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 174
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 176
    :cond_0
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    if-nez v0, :cond_0

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lcw;->t:Ljava/lang/CharSequence;

    .line 196
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcw;->p:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcw;->t:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public o()Lfsb;
    .locals 12

    .prologue
    .line 113
    new-instance v0, Lcdk;

    iget-object v1, p0, Lcw;->v:Lbhz;

    new-instance v2, Lccb;

    iget-object v3, p0, Lcw;->v:Lbhz;

    invoke-direct {v2, v3}, Lccb;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcw;->w:Lffs;

    iget-object v4, p0, Lcw;->y:Lgix;

    iget-object v5, p0, Lcw;->z:Lcub;

    iget-object v6, p0, Lcw;->A:Leyp;

    iget-object v7, p0, Lcw;->B:Levn;

    iget-object v8, p0, Lcw;->C:Leyt;

    iget-object v9, p0, Lcw;->x:Lfhz;

    iget-object v10, p0, Lcw;->D:Lfdw;

    iget-object v11, p0, Lcw;->E:Lfrz;

    invoke-direct/range {v0 .. v11}, Lcdk;-><init>(Landroid/app/Activity;Lfsj;Lffs;Lgix;Lcub;Leyp;Levn;Leyt;Lfhz;Lfdw;Lfrz;)V

    return-object v0
.end method

.method public synthetic p()Lfsh;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcw;->o()Lfsb;

    move-result-object v0

    return-object v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcw;->F:Ljava/lang/String;

    return-object v0
.end method

.method public r()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcw;->G:Ljava/lang/Object;

    return-void
.end method

.method public s()V
    .locals 4

    .prologue
    .line 33
    iget-wide v0, p0, Lcw;->J:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 34
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcw;->J:J

    .line 35
    return-void
.end method

.method public t()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 38
    iget-wide v0, p0, Lcw;->K:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcw;->J:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 39
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcw;->K:J

    .line 40
    iget-object v0, p0, Lcw;->I:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 41
    return-void
.end method
