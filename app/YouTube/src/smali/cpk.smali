.class public final Lcpk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lgjp;

.field private final b:Lexd;

.field private final c:Lggr;

.field private final d:Levn;

.field private final e:Lery;

.field private final f:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lgjp;Lexd;Lggr;Levn;Lery;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lcpk;->a:Lgjp;

    .line 225
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lcpk;->b:Lexd;

    .line 226
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggr;

    iput-object v0, p0, Lcpk;->c:Lggr;

    .line 227
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcpk;->d:Levn;

    .line 228
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lery;

    iput-object v0, p0, Lcpk;->e:Lery;

    .line 229
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcpk;->f:Ljava/util/concurrent/Executor;

    .line 230
    return-void
.end method


# virtual methods
.method public final a(Lcpl;)Lcph;
    .locals 2

    .prologue
    .line 249
    invoke-static {p1}, Lcpl;->a(Lcpl;)[Lfnd;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcpl;->b(Lcpl;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpk;->a(Ljava/util/List;Ljava/lang/String;)Lcph;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)Lcph;
    .locals 9

    .prologue
    .line 233
    new-instance v0, Lcph;

    iget-object v1, p0, Lcpk;->a:Lgjp;

    iget-object v2, p0, Lcpk;->b:Lexd;

    iget-object v3, p0, Lcpk;->c:Lggr;

    iget-object v4, p0, Lcpk;->d:Levn;

    iget-object v5, p0, Lcpk;->e:Lery;

    iget-object v8, p0, Lcpk;->f:Ljava/util/concurrent/Executor;

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Lcph;-><init>(Lgjp;Lexd;Lggr;Levn;Lery;Ljava/util/List;Ljava/lang/String;Ljava/util/concurrent/Executor;)V

    .line 243
    iget-object v1, v0, Lcph;->a:Levn;

    invoke-virtual {v1, v0}, Levn;->a(Ljava/lang/Object;)V

    .line 244
    return-object v0
.end method
