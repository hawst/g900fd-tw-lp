.class public final Lijo;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 41
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lijo;->a:[I

    .line 42
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lijo;->b:[I

    .line 50
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lijo;->c:[I

    .line 52
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lijo;->d:[I

    return-void

    .line 41
    :array_0
    .array-data 4
        0x0
        0x4
        0x1
        0x5
    .end array-data

    .line 42
    :array_1
    .array-data 4
        0x6
        0x2
        0x7
        0x3
    .end array-data

    .line 50
    :array_2
    .array-data 4
        0x8
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x3
    .end array-data

    .line 52
    :array_3
    .array-data 4
        0x7
        0x1
        0x1
        0x3
        0x1
        0x1
        0x1
        0x2
        0x1
    .end array-data
.end method

.method private static a([I[II)I
    .locals 9

    .prologue
    const v0, 0x7fffffff

    const/4 v1, 0x0

    .line 352
    array-length v5, p0

    move v2, v1

    move v3, v1

    move v4, v1

    .line 355
    :goto_0
    if-ge v2, v5, :cond_0

    .line 356
    aget v6, p0, v2

    add-int/2addr v4, v6

    .line 357
    aget v6, p1, v2

    add-int/2addr v3, v6

    .line 355
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 359
    :cond_0
    if-ge v4, v3, :cond_2

    .line 380
    :cond_1
    :goto_1
    return v0

    .line 367
    :cond_2
    shl-int/lit8 v2, v4, 0x8

    div-int v6, v2, v3

    .line 368
    mul-int/lit16 v2, v6, 0xcc

    shr-int/lit8 v7, v2, 0x8

    move v2, v1

    move v3, v1

    .line 371
    :goto_2
    if-ge v2, v5, :cond_4

    .line 372
    aget v1, p0, v2

    shl-int/lit8 v1, v1, 0x8

    .line 373
    aget v8, p1, v2

    mul-int/2addr v8, v6

    .line 374
    if-le v1, v8, :cond_3

    sub-int/2addr v1, v8

    .line 375
    :goto_3
    if-gt v1, v7, :cond_1

    .line 378
    add-int/2addr v3, v1

    .line 371
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 374
    :cond_3
    sub-int v1, v8, v1

    goto :goto_3

    .line 380
    :cond_4
    div-int v0, v3, v4

    goto :goto_1
.end method

.method private static a(Lifw;Lifw;)Lifw;
    .locals 3

    .prologue
    .line 167
    invoke-virtual {p1}, Lifw;->a()V

    .line 168
    iget v1, p0, Lifw;->b:I

    .line 169
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 170
    invoke-virtual {p0, v0}, Lifw;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 171
    add-int/lit8 v2, v1, -0x1

    sub-int/2addr v2, v0

    invoke-virtual {p1, v2}, Lifw;->b(I)V

    .line 169
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    :cond_1
    return-object p1
.end method

.method public static a(Lieq;Z)Lijp;
    .locals 8

    .prologue
    .line 81
    invoke-virtual {p0}, Lieq;->b()Lifx;

    move-result-object v2

    .line 83
    invoke-static {p1, v2}, Lijo;->a(ZLifx;)Ljava/util/List;

    move-result-object v0

    .line 84
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    iget v0, v2, Lifx;->a:I

    iget v3, v2, Lifx;->b:I

    new-instance v1, Lifw;

    invoke-direct {v1, v0}, Lifw;-><init>(I)V

    new-instance v4, Lifw;

    invoke-direct {v4, v0}, Lifw;-><init>(I)V

    new-instance v5, Lifw;

    invoke-direct {v5, v0}, Lifw;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v6, v3, 0x1

    shr-int/lit8 v6, v6, 0x1

    if-ge v0, v6, :cond_0

    invoke-virtual {v2, v0, v1}, Lifx;->a(ILifw;)Lifw;

    move-result-object v1

    add-int/lit8 v6, v3, -0x1

    sub-int/2addr v6, v0

    invoke-virtual {v2, v6, v4}, Lifx;->a(ILifw;)Lifw;

    move-result-object v6

    invoke-static {v6, v5}, Lijo;->a(Lifw;Lifw;)Lifw;

    move-result-object v6

    invoke-virtual {v2, v0, v6}, Lifx;->b(ILifw;)V

    add-int/lit8 v6, v3, -0x1

    sub-int/2addr v6, v0

    invoke-static {v1, v5}, Lijo;->a(Lifw;Lifw;)Lifw;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lifx;->b(ILifw;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_0
    invoke-static {p1, v2}, Lijo;->a(ZLifx;)Ljava/util/List;

    move-result-object v0

    .line 88
    :cond_1
    new-instance v1, Lijp;

    invoke-direct {v1, v2, v0}, Lijp;-><init>(Lifx;Ljava/util/List;)V

    return-object v1
.end method

.method private static a(ZLifx;)Ljava/util/List;
    .locals 13

    .prologue
    .line 99
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 100
    const/4 v3, 0x0

    .line 101
    const/4 v4, 0x0

    .line 102
    const/4 v0, 0x0

    move v10, v0

    .line 103
    :goto_0
    iget v0, p1, Lifx;->b:I

    if-ge v3, v0, :cond_5

    .line 104
    iget v1, p1, Lifx;->b:I

    iget v2, p1, Lifx;->a:I

    const/16 v0, 0x8

    new-array v12, v0, [Lifb;

    sget-object v5, Lijo;->c:[I

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lijo;->a(Lifx;IIII[I)[Lifb;

    move-result-object v0

    sget-object v5, Lijo;->a:[I

    invoke-static {v12, v0, v5}, Lijo;->a([Lifb;[Lifb;[I)V

    const/4 v0, 0x4

    aget-object v0, v12, v0

    if-eqz v0, :cond_6

    const/4 v0, 0x4

    aget-object v0, v12, v0

    iget v0, v0, Lifb;->a:F

    float-to-int v8, v0

    const/4 v0, 0x4

    aget-object v0, v12, v0

    iget v0, v0, Lifb;->b:F

    float-to-int v7, v0

    :goto_1
    sget-object v9, Lijo;->d:[I

    move-object v4, p1

    move v5, v1

    move v6, v2

    invoke-static/range {v4 .. v9}, Lijo;->a(Lifx;IIII[I)[Lifb;

    move-result-object v0

    sget-object v1, Lijo;->b:[I

    invoke-static {v12, v0, v1}, Lijo;->a([Lifb;[Lifb;[I)V

    .line 106
    const/4 v0, 0x0

    aget-object v0, v12, v0

    if-nez v0, :cond_3

    const/4 v0, 0x3

    aget-object v0, v12, v0

    if-nez v0, :cond_3

    .line 107
    if-eqz v10, :cond_5

    .line 109
    const/4 v1, 0x0

    .line 114
    const/4 v4, 0x0

    .line 115
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lifb;

    .line 116
    const/4 v5, 0x1

    aget-object v5, v0, v5

    if-eqz v5, :cond_1

    .line 117
    int-to-float v3, v3

    const/4 v5, 0x1

    aget-object v5, v0, v5

    iget v5, v5, Lifb;->b:F

    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    float-to-int v3, v3

    .line 119
    :cond_1
    const/4 v5, 0x3

    aget-object v5, v0, v5

    if-eqz v5, :cond_0

    .line 120
    const/4 v5, 0x3

    aget-object v0, v0, v5

    iget v0, v0, Lifb;->b:F

    float-to-int v0, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_2

    .line 123
    :cond_2
    add-int/lit8 v3, v3, 0x5

    move v10, v1

    .line 124
    goto :goto_0

    .line 126
    :cond_3
    const/4 v0, 0x1

    .line 127
    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    if-eqz p0, :cond_5

    .line 129
    const/4 v1, 0x2

    aget-object v1, v12, v1

    if-eqz v1, :cond_4

    .line 134
    const/4 v1, 0x2

    aget-object v1, v12, v1

    iget v1, v1, Lifb;->a:F

    float-to-int v4, v1

    .line 135
    const/4 v1, 0x2

    aget-object v1, v12, v1

    iget v1, v1, Lifb;->b:F

    float-to-int v3, v1

    move v10, v0

    goto/16 :goto_0

    .line 137
    :cond_4
    const/4 v1, 0x4

    aget-object v1, v12, v1

    iget v1, v1, Lifb;->a:F

    float-to-int v4, v1

    .line 138
    const/4 v1, 0x4

    aget-object v1, v12, v1

    iget v1, v1, Lifb;->b:F

    float-to-int v3, v1

    move v10, v0

    .line 140
    goto/16 :goto_0

    .line 141
    :cond_5
    return-object v11

    :cond_6
    move v8, v4

    move v7, v3

    goto :goto_1
.end method

.method private static a([Lifb;[Lifb;[I)V
    .locals 3

    .prologue
    .line 210
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 211
    aget v1, p2, v0

    aget-object v2, p1, v0

    aput-object v2, p0, v1

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 213
    :cond_0
    return-void
.end method

.method private static a(Lifx;IIIZ[I[I)[I
    .locals 7

    .prologue
    .line 295
    const/4 v0, 0x0

    array-length v1, p6

    const/4 v2, 0x0

    invoke-static {p6, v0, v1, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 296
    array-length v3, p5

    .line 297
    const/4 v2, 0x0

    .line 299
    const/4 v0, 0x0

    .line 302
    :goto_0
    invoke-virtual {p0, p1, p2}, Lifx;->a(II)Z

    move-result v1

    if-eqz v1, :cond_0

    if-lez p1, :cond_0

    add-int/lit8 v1, v0, 0x1

    const/4 v4, 0x3

    if-ge v0, v4, :cond_0

    .line 303
    add-int/lit8 p1, p1, -0x1

    move v0, v1

    goto :goto_0

    .line 306
    :cond_0
    const/4 v0, 0x0

    move v1, p1

    .line 307
    :goto_1
    if-ge p1, p3, :cond_5

    .line 308
    invoke-virtual {p0, p1, p2}, Lifx;->a(II)Z

    move-result v4

    .line 309
    xor-int/2addr v4, v2

    if-eqz v4, :cond_1

    .line 310
    aget v4, p6, v0

    add-int/lit8 v4, v4, 0x1

    aput v4, p6, v0

    .line 307
    :goto_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 312
    :cond_1
    add-int/lit8 v4, v3, -0x1

    if-ne v0, v4, :cond_3

    .line 313
    const/16 v4, 0xcc

    invoke-static {p6, p5, v4}, Lijo;->a([I[II)I

    move-result v4

    const/16 v5, 0x6b

    if-ge v4, v5, :cond_2

    .line 314
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v2, 0x0

    aput v1, v0, v2

    const/4 v1, 0x1

    aput p1, v0, v1

    .line 333
    :goto_3
    return-object v0

    .line 316
    :cond_2
    const/4 v4, 0x0

    aget v4, p6, v4

    const/4 v5, 0x1

    aget v5, p6, v5

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 317
    const/4 v4, 0x2

    const/4 v5, 0x0

    add-int/lit8 v6, v3, -0x2

    invoke-static {p6, v4, p6, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 318
    add-int/lit8 v4, v3, -0x2

    const/4 v5, 0x0

    aput v5, p6, v4

    .line 319
    add-int/lit8 v4, v3, -0x1

    const/4 v5, 0x0

    aput v5, p6, v4

    .line 320
    add-int/lit8 v0, v0, -0x1

    .line 324
    :goto_4
    const/4 v4, 0x1

    aput v4, p6, v0

    .line 325
    if-nez v2, :cond_4

    const/4 v2, 0x1

    goto :goto_2

    .line 322
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 325
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 328
    :cond_5
    add-int/lit8 v2, v3, -0x1

    if-ne v0, v2, :cond_6

    .line 329
    const/16 v0, 0xcc

    invoke-static {p6, p5, v0}, Lijo;->a([I[II)I

    move-result v0

    const/16 v2, 0x6b

    if-ge v0, v2, :cond_6

    .line 330
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v2, 0x0

    aput v1, v0, v2

    const/4 v1, 0x1

    add-int/lit8 v2, p1, -0x1

    aput v2, v0, v1

    goto :goto_3

    .line 333
    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static a(Lifx;IIII[I)[Lifb;
    .locals 12

    .prologue
    .line 221
    const/4 v1, 0x4

    new-array v11, v1, [Lifb;

    .line 222
    const/4 v8, 0x0

    .line 223
    move-object/from16 v0, p5

    array-length v1, v0

    new-array v7, v1, [I

    move v3, p3

    .line 224
    :goto_0
    if-ge v3, p1, :cond_7

    .line 225
    const/4 v5, 0x0

    move-object v1, p0

    move/from16 v2, p4

    move v4, p2

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v7}, Lijo;->a(Lifx;IIIZ[I[I)[I

    move-result-object v1

    .line 226
    if-eqz v1, :cond_2

    move-object v8, v1

    move v1, v3

    .line 227
    :goto_1
    if-lez v1, :cond_1

    .line 228
    add-int/lit8 v3, v1, -0x1

    const/4 v5, 0x0

    move-object v1, p0

    move/from16 v2, p4

    move v4, p2

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v7}, Lijo;->a(Lifx;IIIZ[I[I)[I

    move-result-object v1

    .line 229
    if-eqz v1, :cond_0

    move-object v8, v1

    move v1, v3

    .line 230
    goto :goto_1

    .line 232
    :cond_0
    add-int/lit8 v1, v3, 0x1

    .line 235
    :cond_1
    const/4 v2, 0x0

    new-instance v3, Lifb;

    const/4 v4, 0x0

    aget v4, v8, v4

    int-to-float v4, v4

    int-to-float v5, v1

    invoke-direct {v3, v4, v5}, Lifb;-><init>(FF)V

    aput-object v3, v11, v2

    .line 237
    const/4 v2, 0x1

    new-instance v3, Lifb;

    const/4 v4, 0x1

    aget v4, v8, v4

    int-to-float v4, v4

    int-to-float v5, v1

    invoke-direct {v3, v4, v5}, Lifb;-><init>(FF)V

    aput-object v3, v11, v2

    .line 238
    const/4 v2, 0x1

    move v10, v1

    .line 242
    :goto_2
    add-int/lit8 v1, v10, 0x1

    .line 244
    if-eqz v2, :cond_5

    .line 245
    const/4 v2, 0x0

    .line 246
    const/4 v3, 0x2

    new-array v8, v3, [I

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget-object v4, v11, v4

    iget v4, v4, Lifb;->a:F

    float-to-int v4, v4

    aput v4, v8, v3

    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-object v4, v11, v4

    iget v4, v4, Lifb;->a:F

    float-to-int v4, v4

    aput v4, v8, v3

    move v9, v2

    move v3, v1

    .line 247
    :goto_3
    if-ge v3, p1, :cond_4

    .line 248
    const/4 v1, 0x0

    aget v2, v8, v1

    const/4 v5, 0x0

    move-object v1, p0

    move v4, p2

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v7}, Lijo;->a(Lifx;IIIZ[I[I)[I

    move-result-object v1

    .line 253
    if-eqz v1, :cond_3

    const/4 v2, 0x0

    aget v2, v8, v2

    const/4 v4, 0x0

    aget v4, v1, v4

    sub-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v4, 0x5

    if-ge v2, v4, :cond_3

    const/4 v2, 0x1

    aget v2, v8, v2

    const/4 v4, 0x1

    aget v4, v1, v4

    sub-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v4, 0x5

    if-ge v2, v4, :cond_3

    .line 257
    const/4 v2, 0x0

    .line 247
    :goto_4
    add-int/lit8 v3, v3, 0x1

    move-object v8, v1

    move v9, v2

    goto :goto_3

    .line 224
    :cond_2
    add-int/lit8 v3, v3, 0x5

    goto/16 :goto_0

    .line 259
    :cond_3
    const/16 v1, 0x19

    if-gt v9, v1, :cond_4

    .line 260
    add-int/lit8 v1, v9, 0x1

    move v2, v1

    move-object v1, v8

    goto :goto_4

    .line 266
    :cond_4
    add-int/lit8 v1, v9, 0x1

    sub-int v1, v3, v1

    .line 267
    const/4 v2, 0x2

    new-instance v3, Lifb;

    const/4 v4, 0x0

    aget v4, v8, v4

    int-to-float v4, v4

    int-to-float v5, v1

    invoke-direct {v3, v4, v5}, Lifb;-><init>(FF)V

    aput-object v3, v11, v2

    .line 268
    const/4 v2, 0x3

    new-instance v3, Lifb;

    const/4 v4, 0x1

    aget v4, v8, v4

    int-to-float v4, v4

    int-to-float v5, v1

    invoke-direct {v3, v4, v5}, Lifb;-><init>(FF)V

    aput-object v3, v11, v2

    .line 270
    :cond_5
    sub-int/2addr v1, v10

    const/16 v2, 0xa

    if-ge v1, v2, :cond_6

    .line 271
    const/4 v1, 0x0

    :goto_5
    const/4 v2, 0x4

    if-ge v1, v2, :cond_6

    .line 272
    const/4 v2, 0x0

    aput-object v2, v11, v1

    .line 271
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 275
    :cond_6
    return-object v11

    :cond_7
    move v2, v8

    move v10, v3

    goto/16 :goto_2
.end method
