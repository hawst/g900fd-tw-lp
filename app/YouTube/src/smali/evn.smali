.class public final Levn;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/Object;

.field private static final b:Ljava/util/Map;


# instance fields
.field private final c:Ljava/util/concurrent/Executor;

.field private d:Lezj;

.field private final e:Ljava/util/Map;

.field private final f:Ljava/util/Map;

.field private final g:Ljava/util/concurrent/locks/ReadWriteLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Levn;->a:Ljava/lang/Object;

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Levn;->b:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Levn;->c:Ljava/util/concurrent/Executor;

    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Levn;->e:Ljava/util/Map;

    .line 134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Levn;->f:Ljava/util/Map;

    .line 135
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 136
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lezj;)V
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0, p1}, Levn;-><init>(Ljava/util/concurrent/Executor;)V

    .line 147
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Levn;->d:Lezj;

    .line 148
    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Levq;)Levr;
    .locals 3

    .prologue
    .line 264
    new-instance v0, Levr;

    invoke-direct {v0, p1, p2, p3, p4}, Levr;-><init>(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Levq;)V

    .line 266
    iget-object v1, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 268
    :try_start_0
    iget-object v1, p0, Levn;->e:Ljava/util/Map;

    invoke-static {v1, p2, v0}, La;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 269
    iget-object v1, p0, Levn;->f:Ljava/util/Map;

    new-instance v2, Lfag;

    invoke-direct {v2, p1}, Lfag;-><init>(Ljava/lang/Object;)V

    invoke-static {v1, v2, v0}, La;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    iget-object v1, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 274
    new-instance v1, Levu;

    invoke-direct {v1, v0}, Levu;-><init>(Levr;)V

    const/4 v2, 0x1

    invoke-direct {p0, p2, v1, v2}, Levn;->a(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 275
    return-object v0

    .line 272
    :catchall_0
    move-exception v0

    iget-object v1, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/Class;)Ljava/util/Set;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 499
    monitor-enter p0

    :try_start_0
    sget-object v0, Levn;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 500
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v4

    .line 501
    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    .line 502
    const-class v0, Levv;

    invoke-virtual {v6, v0}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    invoke-virtual {v6}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    const-string v7, "Event handler methods can only take a single event argument."

    invoke-static {v0, v7}, Lb;->c(ZLjava/lang/Object;)V

    invoke-virtual {v6}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    const/4 v7, 0x0

    aget-object v0, v0, v7

    .line 504
    sget-object v7, Levn;->b:Ljava/util/Map;

    new-instance v8, Levp;

    invoke-direct {v8, v0, v6}, Levp;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    invoke-static {v7, p1, v8}, La;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 501
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 503
    goto :goto_1

    .line 509
    :cond_2
    sget-object v0, Levn;->b:Ljava/util/Map;

    invoke-static {v0, p1}, La;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 499
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Levn;)Ljava/util/concurrent/locks/ReadWriteLock;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    .line 391
    iget-object v0, p0, Levn;->d:Lezj;

    if-eqz v0, :cond_0

    instance-of v0, p2, Levw;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 392
    check-cast v0, Levw;

    iget-object v1, p0, Levn;->d:Lezj;

    invoke-virtual {v1}, Lezj;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Levw;->a(J)V

    .line 394
    :cond_0
    new-instance v1, Levo;

    invoke-direct {v1, p0, p2, p1}, Levo;-><init>(Levn;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 426
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    .line 427
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 431
    :goto_1
    return-void

    .line 426
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 429
    :cond_2
    iget-object v0, p0, Levn;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method private a(Ljava/util/Collection;)V
    .locals 8

    .prologue
    .line 321
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 323
    iget-object v0, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 325
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levr;

    .line 326
    iget-object v3, v0, Levr;->b:Ljava/lang/Class;

    .line 327
    iget-object v4, p0, Levn;->e:Ljava/util/Map;

    .line 328
    invoke-static {v4, v3, v0}, La;->b(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    .line 334
    iget-object v5, v0, Levr;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    .line 335
    if-eqz v5, :cond_1

    .line 336
    iget-object v6, p0, Levn;->f:Ljava/util/Map;

    new-instance v7, Lfag;

    invoke-direct {v7, v5}, Lfag;-><init>(Ljava/lang/Object;)V

    invoke-static {v6, v7, v0}, La;->b(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 343
    :cond_1
    if-eqz v4, :cond_0

    iget-object v0, p0, Levn;->e:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 348
    :catchall_0
    move-exception v0

    iget-object v1, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 351
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 352
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    const/4 v3, 0x1

    invoke-direct {p0, v2, v0, v3}, Levn;->a(Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto :goto_1

    .line 354
    :cond_3
    return-void
.end method

.method static synthetic b(Levn;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Levn;->e:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Class;Levq;)Levr;
    .locals 1

    .prologue
    .line 235
    sget-object v0, Levn;->a:Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v0, p3}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Levq;)Levr;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 158
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Class;)V
    .locals 1

    .prologue
    .line 170
    sget-object v0, Levn;->a:Ljava/lang/Object;

    invoke-virtual {p0, p1, p2, v0}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 171
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 199
    .line 200
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    const-string v3, "clazz must be a superclass of target"

    .line 199
    invoke-static {v2, v3}, Lb;->c(ZLjava/lang/Object;)V

    .line 201
    iget-object v2, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 203
    :try_start_0
    invoke-direct {p0, p2}, Levn;->a(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v2

    .line 205
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    const-string v1, "Class %s does not contain any methods annotated with @Subscribe"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 208
    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 206
    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 204
    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    .line 209
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levp;

    .line 212
    iget-object v2, v0, Levp;->a:Ljava/lang/Class;

    new-instance v3, Levs;

    .line 214
    iget-object v0, v0, Levp;->b:Ljava/lang/reflect/Method;

    invoke-direct {v3, p1, v0}, Levs;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    .line 210
    invoke-direct {p0, p1, v2, p3, v3}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Levq;)Levr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 217
    :catchall_0
    move-exception v0

    iget-object v1, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_0
    move v0, v1

    .line 205
    goto :goto_0

    .line 217
    :cond_1
    iget-object v0, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 218
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 385
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Levn;->a(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 386
    return-void
.end method

.method public final varargs a([Levr;)V
    .locals 1

    .prologue
    .line 308
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 309
    invoke-direct {p0, v0}, Levn;->a(Ljava/util/Collection;)V

    .line 310
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 288
    iget-object v0, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 290
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Levn;->f:Ljava/util/Map;

    new-instance v2, Lfag;

    invoke-direct {v2, p1}, Lfag;-><init>(Ljava/lang/Object;)V

    .line 291
    invoke-static {v1, v2}, La;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 290
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 293
    invoke-direct {p0, v0}, Levn;->a(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    iget-object v0, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 296
    return-void

    .line 295
    :catchall_0
    move-exception v0

    iget-object v1, p0, Levn;->g:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 364
    sget-object v0, Levn;->a:Ljava/lang/Object;

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Levn;->a(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 365
    return-void
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 374
    sget-object v0, Levn;->a:Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Levn;->a(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 375
    return-void
.end method
