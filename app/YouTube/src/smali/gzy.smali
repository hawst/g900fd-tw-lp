.class public final Lgzy;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field public c:Z

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10057
    invoke-direct {p0}, Lidf;-><init>()V

    .line 10060
    iput-boolean v0, p0, Lgzy;->a:Z

    .line 10063
    iput v0, p0, Lgzy;->b:I

    .line 10066
    iput-boolean v0, p0, Lgzy;->c:Z

    .line 10069
    iput v0, p0, Lgzy;->d:I

    .line 10057
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 10124
    const/4 v0, 0x0

    .line 10125
    iget-boolean v1, p0, Lgzy;->a:Z

    if-eqz v1, :cond_0

    .line 10126
    const/4 v0, 0x1

    iget-boolean v1, p0, Lgzy;->a:Z

    .line 10127
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 10129
    :cond_0
    iget v1, p0, Lgzy;->b:I

    if-eqz v1, :cond_1

    .line 10130
    const/4 v1, 0x2

    iget v2, p0, Lgzy;->b:I

    .line 10131
    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10133
    :cond_1
    iget-boolean v1, p0, Lgzy;->c:Z

    if-eqz v1, :cond_2

    .line 10134
    const/4 v1, 0x3

    iget-boolean v2, p0, Lgzy;->c:Z

    .line 10135
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10137
    :cond_2
    iget v1, p0, Lgzy;->d:I

    if-eqz v1, :cond_3

    .line 10138
    const/4 v1, 0x4

    iget v2, p0, Lgzy;->d:I

    .line 10139
    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10141
    :cond_3
    iget-object v1, p0, Lgzy;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10142
    iput v0, p0, Lgzy;->J:I

    .line 10143
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 10053
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzy;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzy;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzy;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgzy;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lgzy;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgzy;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lgzy;->d:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 10106
    iget-boolean v0, p0, Lgzy;->a:Z

    if-eqz v0, :cond_0

    .line 10107
    const/4 v0, 0x1

    iget-boolean v1, p0, Lgzy;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 10109
    :cond_0
    iget v0, p0, Lgzy;->b:I

    if-eqz v0, :cond_1

    .line 10110
    const/4 v0, 0x2

    iget v1, p0, Lgzy;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    .line 10112
    :cond_1
    iget-boolean v0, p0, Lgzy;->c:Z

    if-eqz v0, :cond_2

    .line 10113
    const/4 v0, 0x3

    iget-boolean v1, p0, Lgzy;->c:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 10115
    :cond_2
    iget v0, p0, Lgzy;->d:I

    if-eqz v0, :cond_3

    .line 10116
    const/4 v0, 0x4

    iget v1, p0, Lgzy;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    .line 10118
    :cond_3
    iget-object v0, p0, Lgzy;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 10120
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 10083
    if-ne p1, p0, :cond_1

    .line 10090
    :cond_0
    :goto_0
    return v0

    .line 10084
    :cond_1
    instance-of v2, p1, Lgzy;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 10085
    :cond_2
    check-cast p1, Lgzy;

    .line 10086
    iget-boolean v2, p0, Lgzy;->a:Z

    iget-boolean v3, p1, Lgzy;->a:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgzy;->b:I

    iget v3, p1, Lgzy;->b:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lgzy;->c:Z

    iget-boolean v3, p1, Lgzy;->c:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgzy;->d:I

    iget v3, p1, Lgzy;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzy;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzy;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 10090
    goto :goto_0

    .line 10086
    :cond_4
    iget-object v2, p0, Lgzy;->I:Ljava/util/List;

    iget-object v3, p1, Lgzy;->I:Ljava/util/List;

    .line 10090
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 10094
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 10096
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lgzy;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 10097
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lgzy;->b:I

    add-int/2addr v0, v3

    .line 10098
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lgzy;->c:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 10099
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lgzy;->d:I

    add-int/2addr v0, v1

    .line 10100
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lgzy;->I:Ljava/util/List;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    add-int/2addr v0, v1

    .line 10101
    return v0

    :cond_0
    move v0, v2

    .line 10096
    goto :goto_0

    :cond_1
    move v1, v2

    .line 10098
    goto :goto_1

    .line 10100
    :cond_2
    iget-object v0, p0, Lgzy;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_2
.end method
