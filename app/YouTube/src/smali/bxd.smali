.class public final Lbxd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lffs;

.field private final c:Lgix;

.field private final d:Lcub;

.field private final e:Levn;

.field private final f:Lfhz;

.field private final g:Leyt;

.field private final h:Lfdw;

.field private final i:Lfrz;

.field private j:Lfmy;

.field private k:Landroid/app/AlertDialog;

.field private final l:Lbxi;


# direct methods
.method public constructor <init>(Lbxi;Landroid/app/Activity;Lffs;Lgix;Lcub;Leyt;Levn;Lfhz;Lfdw;Lfrz;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxi;

    iput-object v0, p0, Lbxd;->l:Lbxi;

    .line 69
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbxd;->a:Landroid/app/Activity;

    .line 70
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffs;

    iput-object v0, p0, Lbxd;->b:Lffs;

    .line 71
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbxd;->c:Lgix;

    .line 72
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcub;

    iput-object v0, p0, Lbxd;->d:Lcub;

    .line 73
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lbxd;->e:Levn;

    .line 74
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbxd;->g:Leyt;

    .line 75
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lbxd;->f:Lfhz;

    .line 76
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lbxd;->h:Lfdw;

    .line 78
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lbxd;->i:Lfrz;

    .line 80
    invoke-virtual {p7, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 81
    invoke-interface {p1, p0}, Lbxi;->a(Landroid/view/View$OnClickListener;)V

    .line 82
    return-void
.end method

.method static synthetic a(Lbxd;)Leyt;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbxd;->g:Leyt;

    return-object v0
.end method

.method static synthetic a(Lbxd;Lfmy;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lbxd;->b(Lfmy;)V

    return-void
.end method

.method private a(ZLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lbxd;->j:Lfmy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxd;->j:Lfmy;

    .line 147
    iget-boolean v0, v0, Lfmy;->d:Z

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lbxd;->j:Lfmy;

    .line 148
    iget-object v0, v0, Lfmy;->b:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Lbxd;->j:Lfmy;

    iput-boolean p1, v0, Lfmy;->d:Z

    .line 153
    iget-object v0, p0, Lbxd;->l:Lbxi;

    iget-object v1, p0, Lbxd;->j:Lfmy;

    invoke-interface {v0, v1}, Lbxi;->a(Lfmy;)V

    .line 154
    iget-object v0, p0, Lbxd;->l:Lbxi;

    invoke-interface {v0, p3}, Lbxi;->a(I)V

    goto :goto_0
.end method

.method static synthetic b(Lbxd;)Levn;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbxd;->e:Levn;

    return-object v0
.end method

.method static synthetic b(Lbxd;Lfmy;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lbxd;->c(Lfmy;)V

    return-void
.end method

.method private b(Lfmy;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 186
    iget-boolean v0, p1, Lfmy;->d:Z

    if-eqz v0, :cond_7

    .line 187
    invoke-virtual {p1}, Lfmy;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 189
    invoke-virtual {p1}, Lfmy;->i()Lfiy;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 190
    invoke-virtual {p1}, Lfmy;->i()Lfiy;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lbxd;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, v0, Lfiy;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_0

    iget-object v2, v0, Lfiy;->a:Lhqc;

    iget-object v2, v2, Lhqc;->a:Lhgz;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lfiy;->a:Lhqc;

    iget-object v2, v2, Lhqc;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v0, Lfiy;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v2, v0, Lfiy;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, v0, Lfiy;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_1

    iget-object v2, v0, Lfiy;->a:Lhqc;

    iget-object v2, v2, Lhqc;->b:Lhgz;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lfiy;->a:Lhqc;

    iget-object v2, v2, Lhqc;->b:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v0, Lfiy;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v2, v0, Lfiy;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, v0, Lfiy;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_2

    iget-object v2, v0, Lfiy;->a:Lhqc;

    iget-object v2, v2, Lhqc;->c:Lhgz;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lfiy;->a:Lhqc;

    iget-object v2, v2, Lhqc;->c:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v0, Lfiy;->d:Ljava/lang/CharSequence;

    :cond_2
    iget-object v2, v0, Lfiy;->d:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iget-object v2, v0, Lfiy;->a:Lhqc;

    iget-boolean v2, v2, Lhqc;->d:Z

    if-eqz v2, :cond_4

    const/4 v2, -0x1

    iget-object v3, v0, Lfiy;->e:Ljava/lang/CharSequence;

    if-nez v3, :cond_3

    iget-object v3, v0, Lfiy;->a:Lhqc;

    iget-object v3, v3, Lhqc;->e:Lhgz;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lfiy;->a:Lhqc;

    iget-object v3, v3, Lhqc;->e:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v0, Lfiy;->e:Ljava/lang/CharSequence;

    :cond_3
    iget-object v0, v0, Lfiy;->e:Ljava/lang/CharSequence;

    new-instance v3, Lbxf;

    invoke-direct {v3, p0, p1}, Lbxf;-><init>(Lbxd;Lfmy;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    :cond_4
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 213
    :cond_5
    :goto_0
    return-void

    .line 194
    :cond_6
    iget-object v0, p1, Lfmy;->a:Lhwp;

    iget-boolean v0, v0, Lhwp;->b:Z

    if-eqz v0, :cond_5

    .line 195
    invoke-direct {p0, p1}, Lbxd;->c(Lfmy;)V

    goto :goto_0

    .line 198
    :cond_7
    invoke-virtual {p1}, Lfmy;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 201
    invoke-virtual {p1}, Lfmy;->j()Lhog;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 202
    iget-object v0, p0, Lbxd;->f:Lfhz;

    invoke-virtual {p1}, Lfmy;->j()Lhog;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    goto :goto_0

    .line 205
    :cond_8
    invoke-virtual {p1}, Lfmy;->h()Lhfz;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 206
    invoke-virtual {p1}, Lfmy;->h()Lhfz;

    move-result-object v0

    iget-object v1, p0, Lbxd;->k:Landroid/app/AlertDialog;

    if-nez v1, :cond_9

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lbxd;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lbxd;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lbxd;->k:Landroid/app/AlertDialog;

    :cond_9
    iget-object v1, p0, Lbxd;->k:Landroid/app/AlertDialog;

    iget-object v2, v0, Lhfz;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lbxd;->k:Landroid/app/AlertDialog;

    iget-object v0, v0, Lhfz;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbxd;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 209
    :cond_a
    iget-object v0, p1, Lfmy;->a:Lhwp;

    iget-boolean v0, v0, Lhwp;->b:Z

    if-eqz v0, :cond_5

    .line 210
    iget-object v0, p0, Lbxd;->l:Lbxi;

    invoke-interface {v0, p1}, Lbxi;->b(Lfmy;)V

    iget-object v0, p0, Lbxd;->b:Lffs;

    new-instance v1, Lftn;

    iget-object v2, v0, Lffs;->b:Lfsz;

    iget-object v0, v0, Lffs;->c:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lftn;-><init>(Lfsz;Lgit;)V

    iget-object v0, p1, Lfmy;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, v1, Lftn;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_b
    sget-object v0, Lfhy;->a:[B

    invoke-virtual {v1, v0}, Lftn;->a([B)V

    iget-object v0, p0, Lbxd;->b:Lffs;

    new-instance v2, Lbxg;

    invoke-direct {v2, p0, p1}, Lbxg;-><init>(Lbxd;Lfmy;)V

    iget-object v3, v0, Lffs;->e:Lfco;

    new-instance v4, Lfft;

    invoke-direct {v4, v0, v2}, Lfft;-><init>(Lffs;Lwv;)V

    invoke-virtual {v3, v1, v4}, Lfco;->a(Lfsp;Lwv;)V

    goto/16 :goto_0
.end method

.method static synthetic c(Lbxd;)Lfmy;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbxd;->j:Lfmy;

    return-object v0
.end method

.method private c(Lfmy;)V
    .locals 5

    .prologue
    .line 280
    iget-object v0, p0, Lbxd;->l:Lbxi;

    invoke-interface {v0, p1}, Lbxi;->b(Lfmy;)V

    .line 281
    iget-object v0, p0, Lbxd;->b:Lffs;

    new-instance v1, Lfto;

    iget-object v2, v0, Lffs;->b:Lfsz;

    iget-object v0, v0, Lffs;->c:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lfto;-><init>(Lfsz;Lgit;)V

    .line 282
    iget-object v0, p1, Lfmy;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lfto;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 284
    :cond_0
    sget-object v0, Lfhy;->a:[B

    invoke-virtual {v1, v0}, Lfto;->a([B)V

    .line 285
    iget-object v0, p0, Lbxd;->b:Lffs;

    new-instance v2, Lbxh;

    invoke-direct {v2, p0, p1}, Lbxh;-><init>(Lbxd;Lfmy;)V

    iget-object v3, v0, Lffs;->f:Lfco;

    new-instance v4, Lffu;

    invoke-direct {v4, v0, v2}, Lffu;-><init>(Lffs;Lwv;)V

    invoke-virtual {v3, v1, v4}, Lfco;->a(Lfsp;Lwv;)V

    .line 304
    return-void
.end method

.method static synthetic d(Lbxd;)Lbxi;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbxd;->l:Lbxi;

    return-object v0
.end method


# virtual methods
.method public final a(Lfmy;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 85
    iput-object p1, p0, Lbxd;->j:Lfmy;

    .line 86
    if-eqz p1, :cond_3

    iget-object v1, p1, Lfmy;->a:Lhwp;

    iget-boolean v1, v1, Lhwp;->b:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    .line 87
    iget-object v0, p0, Lbxd;->l:Lbxi;

    invoke-interface {v0, p1}, Lbxi;->a(Lfmy;)V

    .line 89
    iget-object v0, p0, Lbxd;->h:Lfdw;

    iget-object v1, p0, Lbxd;->i:Lfrz;

    .line 90
    invoke-interface {v1}, Lfrz;->B()Lfqg;

    move-result-object v1

    iget-object v2, p0, Lbxd;->j:Lfmy;

    const/4 v3, 0x0

    .line 89
    invoke-virtual {v0, v1, v2, v3}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    .line 96
    :goto_1
    return-void

    .line 86
    :cond_1
    iget-boolean v1, p1, Lfmy;->d:Z

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lfmy;->g()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    iget-boolean v1, p1, Lfmy;->d:Z

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lfmy;->f()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 94
    :cond_4
    iget-object v0, p0, Lbxd;->l:Lbxi;

    invoke-interface {v0}, Lbxi;->a()V

    goto :goto_1
.end method

.method public final handleChannelSubscribedEvent(Lbnz;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 128
    const/4 v0, 0x1

    .line 130
    iget-object v1, p1, Lbnz;->a:Ljava/lang/String;

    const v2, 0x7f09031c

    .line 128
    invoke-direct {p0, v0, v1, v2}, Lbxd;->a(ZLjava/lang/String;I)V

    .line 132
    return-void
.end method

.method public final handleChannelUnsubscribedEvent(Lboa;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 138
    iget-object v1, p1, Lboa;->a:Ljava/lang/String;

    const v2, 0x7f09031e

    .line 136
    invoke-direct {p0, v0, v1, v2}, Lbxd;->a(ZLjava/lang/String;I)V

    .line 140
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lbxd;->j:Lfmy;

    if-nez v0, :cond_0

    .line 124
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lbxd;->j:Lfmy;

    .line 105
    iget-object v1, p0, Lbxd;->c:Lgix;

    invoke-interface {v1}, Lgix;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    invoke-direct {p0, v0}, Lbxd;->b(Lfmy;)V

    goto :goto_0

    .line 108
    :cond_1
    iget-object v1, p0, Lbxd;->d:Lcub;

    iget-object v2, p0, Lbxd;->a:Landroid/app/Activity;

    new-instance v3, Lbxe;

    invoke-direct {v3, p0, v0}, Lbxe;-><init>(Lbxd;Lfmy;)V

    invoke-virtual {v1, v2, v3}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    goto :goto_0
.end method
