.class public final Lcik;
.super Lfsb;
.source "SourceFile"

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field private final a:Landroid/view/ViewStub;

.field private final b:Lexd;

.field private final c:Lezj;

.field private final d:Landroid/content/SharedPreferences;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ImageButton;

.field private h:Lfro;


# direct methods
.method public constructor <init>(Lfhz;Lfdw;Lfrz;Landroid/view/ViewStub;Lexd;Lezj;Landroid/content/SharedPreferences;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 57
    iput-object p4, p0, Lcik;->a:Landroid/view/ViewStub;

    .line 58
    iput-object p5, p0, Lcik;->b:Lexd;

    .line 59
    iput-object p6, p0, Lcik;->c:Lezj;

    .line 60
    iput-object p7, p0, Lcik;->d:Landroid/content/SharedPreferences;

    .line 61
    return-void
.end method

.method private a(Lfrn;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 75
    const/4 v0, 0x0

    invoke-super {p0, v0, p1}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcik;->e:Landroid/view/View;

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcik;->a:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcik;->e:Landroid/view/View;

    iget-object v0, p0, Lcik;->e:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcik;->e:Landroid/view/View;

    const v1, 0x7f0800c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcik;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcik;->e:Landroid/view/View;

    const v1, 0x7f0800c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcik;->g:Landroid/widget/ImageButton;

    .line 81
    :cond_0
    iget-object v0, p0, Lcik;->f:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfrn;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcik;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcik;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "video_quality_promo_last_displayed"

    iget-object v2, p0, Lcik;->c:Lezj;

    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 85
    iget-object v0, p1, Lfrn;->b:Lfro;

    if-nez v0, :cond_1

    iget-object v0, p1, Lfrn;->a:Lhzy;

    iget-object v0, v0, Lhzy;->e:Lhzw;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lfrn;->a:Lhzy;

    iget-object v0, v0, Lhzy;->e:Lhzw;

    iget-object v0, v0, Lhzw;->a:Lhzx;

    if-eqz v0, :cond_1

    new-instance v0, Lfro;

    iget-object v1, p1, Lfrn;->a:Lhzy;

    iget-object v1, v1, Lhzy;->e:Lhzw;

    iget-object v1, v1, Lhzw;->a:Lhzx;

    invoke-direct {v0, v1, p1}, Lfro;-><init>(Lhzx;Lfqh;)V

    iput-object v0, p1, Lfrn;->b:Lfro;

    :cond_1
    iget-object v0, p1, Lfrn;->b:Lfro;

    iput-object v0, p0, Lcik;->h:Lfro;

    .line 86
    iget-object v0, p0, Lcik;->h:Lfro;

    if-eqz v0, :cond_2

    .line 87
    iget-object v0, p0, Lcik;->h:Lfro;

    invoke-super {p0, v0}, Lfsb;->a(Lfqh;)V

    .line 88
    iget-object v0, p0, Lcik;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcik;->g:Landroid/widget/ImageButton;

    new-instance v1, Lcil;

    invoke-direct {v1, p0}, Lcil;-><init>(Lcik;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    :cond_2
    iget-object v0, p0, Lcik;->e:Landroid/view/View;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 115
    iget-object v0, p0, Lcik;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcik;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    return-void
.end method

.method static synthetic a(Lcik;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcik;->a()V

    return-void
.end method

.method static synthetic a(Lcik;Lfqh;)V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lfsa;->l:Lfrz;

    invoke-interface {v0}, Lfrz;->B()Lfqg;

    move-result-object v0

    iget-object v1, p0, Lfsa;->k:Lfdw;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Lfdw;->a(Lfqg;Lfqh;Lhcq;)V

    return-void
.end method

.method static synthetic b(Lcik;)Lfro;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcik;->h:Lfro;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    check-cast p2, Lfrn;

    invoke-direct {p0, p2}, Lcik;->a(Lfrn;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    check-cast p2, Lfrn;

    invoke-direct {p0, p2}, Lcik;->a(Lfrn;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    check-cast p2, Lfrn;

    invoke-direct {p0, p2}, Lcik;->a(Lfrn;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcik;->a()V

    .line 111
    invoke-super {p0, p1}, Lfsb;->onClick(Landroid/view/View;)V

    .line 112
    return-void
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 65
    instance-of v2, p1, Ldmc;

    if-eqz v2, :cond_2

    instance-of v2, p2, Lfrn;

    if-eqz v2, :cond_2

    .line 66
    check-cast p2, Lfrn;

    .line 67
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfrn;->a()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p2, Lfrn;->a:Lhzy;

    iget-object v2, v2, Lhzy;->c:Lhog;

    if-nez v2, :cond_3

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 68
    invoke-direct {p0, p2}, Lcik;->a(Lfrn;)Landroid/view/View;

    .line 71
    :cond_2
    return-void

    .line 67
    :cond_3
    iget-object v2, p2, Lfrn;->a:Lhzy;

    iget-object v2, v2, Lhzy;->a:Liaa;

    if-eqz v2, :cond_5

    iget-object v3, p0, Lcik;->c:Lezj;

    invoke-virtual {v3}, Lezj;->a()J

    move-result-wide v4

    iget-object v3, p0, Lcik;->d:Landroid/content/SharedPreferences;

    const-string v6, "video_quality_promo_last_displayed"

    const-wide/16 v8, 0x0

    invoke-interface {v3, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget v2, v2, Liaa;->e:I

    int-to-long v2, v2

    cmp-long v2, v4, v2

    if-gez v2, :cond_5

    move v2, v0

    :goto_1
    if-nez v2, :cond_4

    iget-object v2, p0, Lcik;->b:Lexd;

    invoke-interface {v2}, Lexd;->e()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x2

    invoke-virtual {p2, v2}, Lfrn;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v0

    :goto_2
    if-nez v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcik;->b:Lexd;

    invoke-interface {v2}, Lexd;->c()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p2, v0}, Lfrn;->a(I)Z

    move-result v2

    if-eqz v2, :cond_7

    move v2, v0

    goto :goto_2

    :cond_7
    move v2, v1

    goto :goto_2
.end method
