.class public final Lgca;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private A:I

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Landroid/net/Uri;

.field public final f:Z

.field public final g:Z

.field private h:Landroid/net/Uri;

.field private i:Landroid/net/Uri;

.field private j:Landroid/net/Uri;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Lgcc;

.field private n:Ljava/lang/String;

.field private o:Landroid/net/Uri;

.field private p:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private q:Landroid/net/Uri;

.field private r:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private s:Landroid/net/Uri;

.field private t:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private u:Landroid/net/Uri;

.field private v:Landroid/net/Uri;

.field private w:Landroid/net/Uri;

.field private x:Landroid/net/Uri;

.field private y:J

.field private z:J


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILgcc;Ljava/lang/String;Landroid/net/Uri;ZZLandroid/net/Uri;ILandroid/net/Uri;ILandroid/net/Uri;ILandroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JJI)V
    .locals 3

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Lgca;->h:Landroid/net/Uri;

    .line 112
    iput-object p2, p0, Lgca;->i:Landroid/net/Uri;

    .line 113
    iput-object p3, p0, Lgca;->j:Landroid/net/Uri;

    .line 115
    if-eqz p4, :cond_0

    move-object v2, p4

    :goto_0
    iput-object v2, p0, Lgca;->a:Ljava/lang/String;

    .line 116
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    iput-object p4, p0, Lgca;->b:Ljava/lang/String;

    .line 117
    iput-object p6, p0, Lgca;->c:Ljava/lang/String;

    .line 118
    iput-object p7, p0, Lgca;->d:Ljava/lang/String;

    .line 119
    iput-object p8, p0, Lgca;->k:Ljava/lang/String;

    .line 120
    iput p9, p0, Lgca;->l:I

    .line 121
    iput-object p10, p0, Lgca;->m:Lgcc;

    .line 122
    iput-object p11, p0, Lgca;->n:Ljava/lang/String;

    .line 123
    iput-object p12, p0, Lgca;->e:Landroid/net/Uri;

    .line 124
    move/from16 v0, p13

    iput-boolean v0, p0, Lgca;->f:Z

    .line 125
    move/from16 v0, p14

    iput-boolean v0, p0, Lgca;->g:Z

    .line 126
    move-object/from16 v0, p15

    iput-object v0, p0, Lgca;->o:Landroid/net/Uri;

    .line 127
    move/from16 v0, p16

    iput v0, p0, Lgca;->p:I

    .line 128
    move-object/from16 v0, p17

    iput-object v0, p0, Lgca;->q:Landroid/net/Uri;

    .line 129
    move/from16 v0, p18

    iput v0, p0, Lgca;->r:I

    .line 130
    move-object/from16 v0, p19

    iput-object v0, p0, Lgca;->s:Landroid/net/Uri;

    .line 131
    move/from16 v0, p20

    iput v0, p0, Lgca;->t:I

    .line 132
    move-object/from16 v0, p21

    iput-object v0, p0, Lgca;->u:Landroid/net/Uri;

    .line 133
    move-object/from16 v0, p22

    iput-object v0, p0, Lgca;->v:Landroid/net/Uri;

    .line 134
    move-object/from16 v0, p23

    iput-object v0, p0, Lgca;->w:Landroid/net/Uri;

    .line 135
    move-object/from16 v0, p24

    iput-object v0, p0, Lgca;->x:Landroid/net/Uri;

    .line 136
    move-wide/from16 v0, p25

    iput-wide v0, p0, Lgca;->y:J

    .line 137
    move-wide/from16 v0, p27

    iput-wide v0, p0, Lgca;->z:J

    .line 138
    move/from16 v0, p29

    iput v0, p0, Lgca;->A:I

    .line 139
    return-void

    :cond_0
    move-object v2, p8

    .line 115
    goto :goto_0

    :cond_1
    move-object p4, p5

    .line 116
    goto :goto_1
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 159
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 155
    new-instance v0, Lgcb;

    invoke-direct {v0}, Lgcb;-><init>()V

    iget-object v1, p0, Lgca;->h:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->a:Landroid/net/Uri;

    iget-object v1, p0, Lgca;->i:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->b:Landroid/net/Uri;

    iget-object v1, p0, Lgca;->j:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->c:Landroid/net/Uri;

    iget-object v1, p0, Lgca;->a:Ljava/lang/String;

    iput-object v1, v0, Lgcb;->d:Ljava/lang/String;

    iget-object v1, p0, Lgca;->b:Ljava/lang/String;

    iput-object v1, v0, Lgcb;->e:Ljava/lang/String;

    iget-object v1, p0, Lgca;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgcb;->a(Ljava/lang/String;)Lgcb;

    move-result-object v0

    iget-object v1, p0, Lgca;->d:Ljava/lang/String;

    iput-object v1, v0, Lgcb;->f:Ljava/lang/String;

    iget-object v1, p0, Lgca;->k:Ljava/lang/String;

    iput-object v1, v0, Lgcb;->g:Ljava/lang/String;

    iget v1, p0, Lgca;->l:I

    iput v1, v0, Lgcb;->h:I

    iget-object v1, p0, Lgca;->m:Lgcc;

    iput-object v1, v0, Lgcb;->i:Lgcc;

    iget-object v1, p0, Lgca;->n:Ljava/lang/String;

    iput-object v1, v0, Lgcb;->j:Ljava/lang/String;

    iget-object v1, p0, Lgca;->e:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->k:Landroid/net/Uri;

    iget-boolean v1, p0, Lgca;->f:Z

    iput-boolean v1, v0, Lgcb;->l:Z

    iget-boolean v1, p0, Lgca;->g:Z

    iput-boolean v1, v0, Lgcb;->m:Z

    iget-object v1, p0, Lgca;->o:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->n:Landroid/net/Uri;

    iget v1, p0, Lgca;->p:I

    iput v1, v0, Lgcb;->o:I

    iget-object v1, p0, Lgca;->q:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->p:Landroid/net/Uri;

    iget v1, p0, Lgca;->r:I

    iput v1, v0, Lgcb;->q:I

    iget-object v1, p0, Lgca;->s:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->r:Landroid/net/Uri;

    iget v1, p0, Lgca;->t:I

    iput v1, v0, Lgcb;->s:I

    iget-object v1, p0, Lgca;->u:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->t:Landroid/net/Uri;

    iget-object v1, p0, Lgca;->v:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->u:Landroid/net/Uri;

    iget-object v1, p0, Lgca;->w:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->v:Landroid/net/Uri;

    iget-object v1, p0, Lgca;->x:Landroid/net/Uri;

    iput-object v1, v0, Lgcb;->w:Landroid/net/Uri;

    iget-wide v2, p0, Lgca;->y:J

    iput-wide v2, v0, Lgcb;->x:J

    iget-wide v2, p0, Lgca;->z:J

    iput-wide v2, v0, Lgcb;->y:J

    iget v1, p0, Lgca;->A:I

    iput v1, v0, Lgcb;->z:I

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lgca;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgca;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgca;->k:Ljava/lang/String;

    goto :goto_0
.end method
