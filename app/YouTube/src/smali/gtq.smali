.class public final Lgtq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgtn;


# instance fields
.field private final a:Lfgi;


# direct methods
.method public constructor <init>(Lfgi;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgi;

    iput-object v0, p0, Lgtq;->a:Lfgi;

    .line 24
    return-void
.end method

.method private a(Lgvd;)Lguc;
    .locals 6

    .prologue
    .line 51
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v0}, Lgvi;->b()Lgvi;

    move-result-object v1

    .line 54
    iget-object v0, v1, Lgvi;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 56
    new-instance v0, Lhdq;

    invoke-direct {v0}, Lhdq;-><init>()V

    .line 57
    iget-object v2, v1, Lgvi;->j:Ljava/lang/String;

    iput-object v2, v0, Lhdq;->b:Ljava/lang/String;

    .line 60
    :try_start_0
    iget-object v2, p0, Lgtq;->a:Lfgi;

    iget-object v1, v1, Lgvi;->a:Ljava/lang/String;

    iget-object v3, v2, Lfgi;->f:Lfco;

    new-instance v4, Lfss;

    iget-object v5, v2, Lfgi;->b:Lfsz;

    iget-object v2, v2, Lfgi;->c:Lgix;

    invoke-interface {v2, v1}, Lgix;->a(Ljava/lang/String;)Lgit;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lfss;-><init>(Lfsz;Lgit;)V

    invoke-virtual {v4, v0}, Lfss;->a(Lidh;)V

    invoke-virtual {v3, v4}, Lfco;->a(Lfsp;)Lidh;

    move-result-object v0

    check-cast v0, Lhdr;

    .line 62
    new-instance v0, Lgtr;

    invoke-direct {v0, p0}, Lgtr;-><init>(Lgtq;)V
    :try_end_0
    .catch Lfdv; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_1
    return-object v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    invoke-static {v0}, La;->a(Lfdv;)Lguc;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)J
    .locals 4

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 18
    check-cast p1, Lgvd;

    if-eqz p1, :cond_0

    iget-object v2, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v2}, Lgvi;->b()Lgvi;

    move-result-object v2

    iget v3, v2, Lgvi;->d:I

    if-nez v3, :cond_0

    iget-boolean v3, v2, Lgvi;->m:Z

    if-nez v3, :cond_0

    iget-boolean v3, v2, Lgvi;->n:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-boolean v3, v2, Lgvi;->l:Z

    if-nez v3, :cond_0

    iget-object v3, v2, Lgvi;->j:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v2, v2, Lgvi;->k:Z

    if-eqz v2, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lguc;
    .locals 1

    .prologue
    .line 18
    check-cast p2, Lgvd;

    invoke-direct {p0, p2}, Lgtq;->a(Lgvd;)Lguc;

    move-result-object v0

    return-object v0
.end method
