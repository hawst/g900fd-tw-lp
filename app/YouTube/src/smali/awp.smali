.class public Lawp;
.super Lcba;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private b:Landroid/content/res/Resources;

.field private final c:I

.field private final e:Ljava/util/Map;

.field private final f:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Lcba;-><init>()V

    .line 41
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lawp;->a:Landroid/view/LayoutInflater;

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lawp;->b:Landroid/content/res/Resources;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lawp;->e:Ljava/util/Map;

    .line 44
    iput p2, p0, Lawp;->c:I

    .line 45
    iget-object v0, p0, Lawp;->b:Landroid/content/res/Resources;

    new-instance v1, Lbwt;

    invoke-direct {v1, v0}, Lbwt;-><init>(Landroid/content/res/Resources;)V

    iget-object v0, v1, Lbwt;->a:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lawp;->f:Landroid/graphics/Bitmap;

    .line 46
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 73
    if-nez p1, :cond_0

    .line 74
    iget-object v0, p0, Lawp;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lawp;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 75
    new-instance v0, Lawq;

    invoke-direct {v0, p1}, Lawq;-><init>(Landroid/view/View;)V

    .line 76
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 78
    :cond_0
    return-object p1
.end method

.method protected final a(Lgbu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 82
    invoke-virtual {p0, p2, p3}, Lawp;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawq;

    .line 84
    iget-object v2, v0, Lawq;->a:Landroid/widget/TextView;

    iget-object v3, p1, Lgbu;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, v0, Lawq;->b:Landroid/widget/ImageView;

    const v2, 0x7f020132

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 87
    return-object v1
.end method

.method protected final synthetic a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 28
    check-cast p2, Lgbu;

    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lawp;->e:Ljava/util/Map;

    iget-object v1, p2, Lgbu;->f:Landroid/net/Uri;

    iget-object v2, p0, Lawp;->f:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1, p2}, Lcba;->a(ILjava/lang/Object;)V

    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 28
    check-cast p1, Lgbu;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lawp;->e:Ljava/util/Map;

    iget-object v1, p1, Lgbu;->f:Landroid/net/Uri;

    iget-object v2, p0, Lawp;->f:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1}, Lcba;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lawp;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 69
    invoke-super {p0}, Lcba;->b()V

    .line 70
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lawp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    invoke-virtual {p0, v0, p2, p3}, Lawp;->a(Lgbu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
