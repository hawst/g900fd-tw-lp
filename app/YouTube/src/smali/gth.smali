.class public final Lgth;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lgth;


# instance fields
.field public final b:F

.field public final c:F

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1331
    new-instance v0, Lgth;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lgth;-><init>(FFI)V

    sput-object v0, Lgth;->a:Lgth;

    return-void
.end method

.method public constructor <init>(FFI)V
    .locals 0

    .prologue
    .line 1337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338
    iput p1, p0, Lgth;->b:F

    .line 1339
    iput p2, p0, Lgth;->c:F

    .line 1340
    iput p3, p0, Lgth;->d:I

    .line 1341
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1345
    if-ne p0, p1, :cond_1

    .line 1353
    :cond_0
    :goto_0
    return v0

    .line 1348
    :cond_1
    instance-of v2, p1, Lgth;

    if-nez v2, :cond_2

    move v0, v1

    .line 1349
    goto :goto_0

    .line 1352
    :cond_2
    check-cast p1, Lgth;

    .line 1353
    iget v2, p0, Lgth;->d:I

    iget v3, p1, Lgth;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgth;->c:F

    iget v3, p1, Lgth;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lgth;->b:F

    iget v3, p1, Lgth;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
