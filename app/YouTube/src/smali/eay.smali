.class final Leay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final a:Landroid/os/Handler;

.field final b:Landroid/os/HandlerThread;

.field c:I

.field volatile d:J

.field volatile e:J

.field volatile f:J

.field private final g:Landroid/os/Handler;

.field private final h:Lebb;

.field private final i:[Z

.field private final j:J

.field private final k:J

.field private final l:Ljava/util/List;

.field private m:[Lecc;

.field private n:Lecc;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:I

.field private s:I

.field private t:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;Z[ZII)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput v0, p0, Leay;->c:I

    .line 66
    iput v0, p0, Leay;->s:I

    .line 75
    iput-object p1, p0, Leay;->g:Landroid/os/Handler;

    .line 76
    iput-boolean p2, p0, Leay;->p:Z

    .line 77
    array-length v1, p3

    new-array v1, v1, [Z

    iput-object v1, p0, Leay;->i:[Z

    .line 78
    int-to-long v2, p4

    mul-long/2addr v2, v6

    iput-wide v2, p0, Leay;->j:J

    .line 79
    int-to-long v2, p5

    mul-long/2addr v2, v6

    iput-wide v2, p0, Leay;->k:J

    .line 80
    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    .line 81
    iget-object v1, p0, Leay;->i:[Z

    aget-boolean v2, p3, v0

    aput-boolean v2, v1, v0

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Leay;->r:I

    .line 85
    iput-wide v4, p0, Leay;->d:J

    .line 86
    iput-wide v4, p0, Leay;->f:J

    .line 88
    new-instance v0, Lebb;

    invoke-direct {v0}, Lebb;-><init>()V

    iput-object v0, p0, Leay;->h:Lebb;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Leay;->l:Ljava/util/List;

    .line 92
    new-instance v0, Legu;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ":Handler"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, -0x10

    invoke-direct {v0, v1, v2}, Legu;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Leay;->b:Landroid/os/HandlerThread;

    .line 94
    iget-object v0, p0, Leay;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 95
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Leay;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Leay;->a:Landroid/os/Handler;

    .line 96
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 230
    iget v0, p0, Leay;->r:I

    if-eq v0, p1, :cond_0

    .line 231
    iput p1, p0, Leay;->r:I

    .line 232
    iget-object v0, p0, Leay;->g:Landroid/os/Handler;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 234
    :cond_0
    return-void
.end method

.method private a(IJJ)V
    .locals 4

    .prologue
    .line 435
    add-long v0, p2, p4

    .line 436
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 437
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 438
    iget-object v0, p0, Leay;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 442
    :goto_0
    return-void

    .line 440
    :cond_0
    iget-object v2, p0, Leay;->a:Landroid/os/Handler;

    invoke-virtual {v2, p1, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private a(Lecc;)Z
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 308
    invoke-virtual {p1}, Lecc;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 320
    :cond_0
    :goto_0
    return v0

    .line 311
    :cond_1
    invoke-virtual {p1}, Lecc;->e()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 312
    goto :goto_0

    .line 314
    :cond_2
    iget v2, p0, Leay;->r:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 317
    invoke-virtual {p1}, Lecc;->m()J

    move-result-wide v4

    .line 318
    invoke-virtual {p1}, Lecc;->n()J

    move-result-wide v6

    .line 319
    iget-boolean v2, p0, Leay;->q:Z

    if-eqz v2, :cond_4

    iget-wide v2, p0, Leay;->k:J

    .line 320
    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-lez v8, :cond_0

    cmp-long v8, v6, v10

    if-eqz v8, :cond_0

    const-wide/16 v8, -0x3

    cmp-long v8, v6, v8

    if-eqz v8, :cond_0

    iget-wide v8, p0, Leay;->e:J

    add-long/2addr v2, v8

    cmp-long v2, v6, v2

    if-gez v2, :cond_0

    cmp-long v2, v4, v10

    if-eqz v2, :cond_3

    const-wide/16 v2, -0x2

    cmp-long v2, v4, v2

    if-eqz v2, :cond_3

    cmp-long v2, v6, v4

    if-gez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 319
    :cond_4
    iget-wide v2, p0, Leay;->j:J

    goto :goto_1
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 350
    iput-boolean v0, p0, Leay;->q:Z

    .line 351
    iget-object v1, p0, Leay;->h:Lebb;

    iget-boolean v2, v1, Lebb;->a:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v1, Lebb;->a:Z

    iget-wide v2, v1, Lebb;->b:J

    invoke-static {v2, v3}, Lebb;->b(J)J

    move-result-wide v2

    iput-wide v2, v1, Lebb;->c:J

    :cond_0
    move v1, v0

    .line 352
    :goto_0
    iget-object v0, p0, Leay;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 353
    iget-object v0, p0, Leay;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecc;

    invoke-virtual {v0}, Lecc;->p()V

    .line 352
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 355
    :cond_1
    return-void
.end method

.method private static b(Lecc;)V
    .locals 2

    .prologue
    .line 577
    iget v0, p0, Lecc;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 578
    invoke-virtual {p0}, Lecc;->q()V

    .line 580
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Leay;->h:Lebb;

    invoke-virtual {v0}, Lebb;->a()V

    .line 359
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Leay;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 360
    iget-object v0, p0, Leay;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecc;

    invoke-static {v0}, Leay;->b(Lecc;)V

    .line 359
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 362
    :cond_0
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 365
    iget-object v0, p0, Leay;->n:Lecc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leay;->l:Ljava/util/List;

    iget-object v1, p0, Leay;->n:Lecc;

    .line 366
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leay;->n:Lecc;

    .line 367
    invoke-virtual {v0}, Lecc;->f()J

    move-result-wide v0

    .line 368
    :goto_0
    iput-wide v0, p0, Leay;->e:J

    .line 369
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Leay;->t:J

    .line 370
    return-void

    .line 367
    :cond_0
    iget-object v0, p0, Leay;->h:Lebb;

    .line 368
    iget-boolean v1, v0, Lebb;->a:Z

    if-eqz v1, :cond_1

    iget-wide v0, v0, Lebb;->c:J

    invoke-static {v0, v1}, Lebb;->b(J)J

    move-result-wide v0

    goto :goto_0

    :cond_1
    iget-wide v0, v0, Lebb;->b:J

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 462
    const/4 v0, 0x0

    iput-boolean v0, p0, Leay;->q:Z

    .line 463
    invoke-direct {p0}, Leay;->f()V

    .line 464
    return-void
.end method

.method private f()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 475
    iget-object v0, p0, Leay;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 476
    iget-object v0, p0, Leay;->a:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 477
    iget-object v0, p0, Leay;->h:Lebb;

    invoke-virtual {v0}, Lebb;->a()V

    .line 478
    iget-object v0, p0, Leay;->m:[Lecc;

    if-nez v0, :cond_0

    .line 490
    :goto_0
    return-void

    .line 481
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Leay;->m:[Lecc;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 482
    iget-object v1, p0, Leay;->m:[Lecc;

    aget-object v2, v1, v0

    .line 483
    :try_start_0
    invoke-static {v2}, Leay;->b(Lecc;)V

    iget v1, v2, Lecc;->g:I

    if-ne v1, v5, :cond_1

    invoke-virtual {v2}, Lecc;->r()V
    :try_end_0
    .catch Leat; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 484
    :cond_1
    :goto_2
    :try_start_1
    invoke-virtual {v2}, Lecc;->s()V
    :try_end_1
    .catch Leat; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3

    .line 481
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 483
    :catch_0
    move-exception v1

    const-string v3, "ExoPlayerImplInternal"

    const-string v4, "Stop failed."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_1
    move-exception v1

    const-string v3, "ExoPlayerImplInternal"

    const-string v4, "Stop failed."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 484
    :catch_2
    move-exception v1

    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Release failed."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catch_3
    move-exception v1

    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Release failed."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 486
    :cond_2
    iput-object v6, p0, Leay;->m:[Lecc;

    .line 487
    iput-object v6, p0, Leay;->n:Lecc;

    .line 488
    iget-object v0, p0, Leay;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 489
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Leay;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Leay;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 171
    :goto_0
    monitor-exit p0

    return-void

    .line 162
    :cond_0
    :try_start_1
    iget-object v0, p0, Leay;->a:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 163
    :goto_1
    iget-boolean v0, p0, Leay;->o:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 165
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 167
    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170
    :cond_1
    :try_start_4
    iget-object v0, p0, Leay;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Leav;ILjava/lang/Object;)V
    .locals 5

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Leay;->o:Z

    if-eqz v0, :cond_1

    .line 144
    const-string v0, "ExoPlayerImplInternal"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x39

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Sent message("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") after release. Message ignored."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :cond_0
    monitor-exit p0

    return-void

    .line 147
    :cond_1
    :try_start_1
    iget v0, p0, Leay;->c:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Leay;->c:I

    .line 148
    iget-object v1, p0, Leay;->a:Landroid/os/Handler;

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 149
    :goto_0
    iget v1, p0, Leay;->s:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gt v1, v0, :cond_0

    .line 151
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 153
    :catch_0
    move-exception v1

    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 17

    .prologue
    .line 176
    :try_start_0
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 214
    const/4 v2, 0x0

    .line 225
    :goto_0
    return v2

    .line 178
    :pswitch_0
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [Lecc;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Leay;->q:Z

    move-object/from16 v0, p0

    iput-object v2, v0, Leay;->m:[Lecc;

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    array-length v3, v2

    if-ge v4, v3, :cond_2

    aget-object v3, v2, v4

    invoke-virtual {v3}, Lecc;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Leay;->n:Lecc;

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, La;->c(Z)V

    aget-object v3, v2, v4

    move-object/from16 v0, p0

    iput-object v3, v0, Leay;->n:Lecc;

    :cond_0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Leay;->a(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->a:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 179
    const/4 v2, 0x1

    goto :goto_0

    .line 182
    :pswitch_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const/4 v3, 0x1

    const/4 v2, 0x0

    move/from16 v16, v2

    move v2, v3

    move/from16 v3, v16

    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Leay;->m:[Lecc;

    array-length v6, v6

    if-ge v3, v6, :cond_4

    move-object/from16 v0, p0

    iget-object v6, v0, Leay;->m:[Lecc;

    aget-object v6, v6, v3

    iget v6, v6, Lecc;->g:I

    if-nez v6, :cond_3

    move-object/from16 v0, p0

    iget-object v6, v0, Leay;->m:[Lecc;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Lecc;->o()I

    move-result v6

    if-nez v6, :cond_3

    const/4 v2, 0x0

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    if-nez v2, :cond_5

    const/4 v3, 0x2

    const-wide/16 v6, 0xa

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Leay;->a(IJJ)V

    .line 183
    :goto_4
    const/4 v2, 0x1

    goto :goto_0

    .line 182
    :cond_5
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    move/from16 v16, v2

    move v2, v3

    move v3, v4

    move-wide v4, v6

    move/from16 v6, v16

    :goto_5
    move-object/from16 v0, p0

    iget-object v7, v0, Leay;->m:[Lecc;

    array-length v7, v7

    if-ge v6, v7, :cond_a

    move-object/from16 v0, p0

    iget-object v7, v0, Leay;->m:[Lecc;

    aget-object v7, v7, v6

    move-object/from16 v0, p0

    iget-object v8, v0, Leay;->i:[Z

    aget-boolean v8, v8, v6

    if-eqz v8, :cond_6

    iget v8, v7, Lecc;->g:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    move-object/from16 v0, p0

    iget-wide v8, v0, Leay;->e:J

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Lecc;->b(JZ)V

    move-object/from16 v0, p0

    iget-object v8, v0, Leay;->l:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_7

    invoke-virtual {v7}, Lecc;->d()Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    :goto_6
    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Leay;->a(Lecc;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_7
    const-wide/16 v8, -0x1

    cmp-long v8, v4, v8

    if-eqz v8, :cond_6

    invoke-virtual {v7}, Lecc;->m()J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-nez v7, :cond_9

    const-wide/16 v4, -0x1

    :cond_6
    :goto_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    goto :goto_6

    :cond_8
    const/4 v2, 0x0

    goto :goto_7

    :cond_9
    const-wide/16 v10, -0x2

    cmp-long v7, v8, v10

    if-eqz v7, :cond_6

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    goto :goto_8

    :cond_a
    move-object/from16 v0, p0

    iput-wide v4, v0, Leay;->d:J

    if-eqz v3, :cond_c

    const/4 v2, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Leay;->a(I)V

    :cond_b
    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->a:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Leat; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_4

    .line 216
    :catch_0
    move-exception v2

    .line 217
    const-string v3, "ExoPlayerImplInternal"

    const-string v4, "Internal track renderer error."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 218
    move-object/from16 v0, p0

    iget-object v3, v0, Leay;->g:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 219
    invoke-direct/range {p0 .. p0}, Leay;->e()V

    .line 220
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 182
    :cond_c
    if-eqz v2, :cond_d

    const/4 v2, 0x4

    :goto_a
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Leay;->a(I)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Leay;->p:Z

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    invoke-direct/range {p0 .. p0}, Leay;->b()V
    :try_end_1
    .catch Leat; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_9

    .line 221
    :catch_1
    move-exception v2

    .line 222
    const-string v3, "ExoPlayerImplInternal"

    const-string v4, "Internal runtime error."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 223
    move-object/from16 v0, p0

    iget-object v3, v0, Leay;->g:Landroid/os/Handler;

    const/4 v4, 0x3

    new-instance v5, Leat;

    invoke-direct {v5, v2}, Leat;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v3, v4, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 224
    invoke-direct/range {p0 .. p0}, Leay;->e()V

    .line 225
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 182
    :cond_d
    const/4 v2, 0x3

    goto :goto_a

    .line 186
    :pswitch_2
    :try_start_2
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I
    :try_end_2
    .catch Leat; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_b
    const/4 v3, 0x0

    :try_start_3
    move-object/from16 v0, p0

    iput-boolean v3, v0, Leay;->q:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Leay;->p:Z

    if-nez v2, :cond_10

    invoke-direct/range {p0 .. p0}, Leay;->c()V

    invoke-direct/range {p0 .. p0}, Leay;->d()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_e
    :goto_c
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->g:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
    :try_end_4
    .catch Leat; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    .line 187
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 186
    :cond_f
    const/4 v2, 0x0

    goto :goto_b

    :cond_10
    :try_start_5
    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_11

    invoke-direct/range {p0 .. p0}, Leay;->b()V

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->a:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_c

    :catchall_0
    move-exception v2

    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, Leay;->g:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    throw v2
    :try_end_6
    .catch Leat; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1

    :cond_11
    :try_start_7
    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->a:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_c

    .line 190
    :pswitch_3
    :try_start_8
    const-string v2, "doSomeWork"

    invoke-static {v2}, La;->s(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v2, v0, Leay;->d:J

    const-wide/16 v6, -0x1

    cmp-long v2, v2, v6

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-wide v6, v0, Leay;->d:J

    :goto_d
    const/4 v9, 0x1

    const/4 v8, 0x1

    invoke-direct/range {p0 .. p0}, Leay;->d()V

    const/4 v2, 0x0

    move v10, v2

    :goto_e
    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v10, v2, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->l:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lecc;

    move-object/from16 v0, p0

    iget-wide v12, v0, Leay;->e:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Leay;->t:J

    invoke-virtual {v2, v12, v13, v14, v15}, Lecc;->a(JJ)V

    if-eqz v9, :cond_13

    invoke-virtual {v2}, Lecc;->d()Z

    move-result v3

    if-eqz v3, :cond_13

    const/4 v3, 0x1

    move v9, v3

    :goto_f
    if-eqz v8, :cond_14

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Leay;->a(Lecc;)Z

    move-result v3

    if-eqz v3, :cond_14

    const/4 v3, 0x1

    move v8, v3

    :goto_10
    const-wide/16 v12, -0x1

    cmp-long v3, v6, v12

    if-eqz v3, :cond_2b

    invoke-virtual {v2}, Lecc;->m()J

    move-result-wide v12

    invoke-virtual {v2}, Lecc;->n()J

    move-result-wide v2

    const-wide/16 v14, -0x1

    cmp-long v11, v2, v14

    if-nez v11, :cond_15

    const-wide/16 v2, -0x1

    :goto_11
    add-int/lit8 v6, v10, 0x1

    move v10, v6

    move-wide v6, v2

    goto :goto_e

    :cond_12
    const-wide v6, 0x7fffffffffffffffL

    goto :goto_d

    :cond_13
    const/4 v3, 0x0

    move v9, v3

    goto :goto_f

    :cond_14
    const/4 v3, 0x0

    move v8, v3

    goto :goto_10

    :cond_15
    const-wide/16 v14, -0x3

    cmp-long v11, v2, v14

    if-eqz v11, :cond_2b

    const-wide/16 v14, -0x1

    cmp-long v11, v12, v14

    if-eqz v11, :cond_16

    const-wide/16 v14, -0x2

    cmp-long v11, v12, v14

    if-eqz v11, :cond_16

    cmp-long v11, v2, v12

    if-gez v11, :cond_2b

    :cond_16
    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    goto :goto_11

    :cond_17
    move-object/from16 v0, p0

    iput-wide v6, v0, Leay;->f:J

    if-eqz v9, :cond_1c

    const/4 v2, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Leay;->a(I)V

    invoke-direct/range {p0 .. p0}, Leay;->c()V

    :cond_18
    :goto_12
    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->a:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Leay;->p:Z

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1a

    :cond_19
    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1e

    :cond_1a
    const/4 v3, 0x7

    const-wide/16 v6, 0xa

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Leay;->a(IJJ)V

    :cond_1b
    :goto_13
    invoke-static {}, La;->o()V

    .line 191
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 190
    :cond_1c
    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1d

    if-eqz v8, :cond_1d

    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Leay;->a(I)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Leay;->p:Z

    if-eqz v2, :cond_18

    invoke-direct/range {p0 .. p0}, Leay;->b()V

    goto :goto_12

    :cond_1d
    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_18

    if-nez v8, :cond_18

    move-object/from16 v0, p0

    iget-boolean v2, v0, Leay;->p:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Leay;->q:Z

    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Leay;->a(I)V

    invoke-direct/range {p0 .. p0}, Leay;->c()V

    goto :goto_12

    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1b

    const/4 v3, 0x7

    const-wide/16 v6, 0x3e8

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Leay;->a(IJJ)V

    goto :goto_13

    .line 194
    :pswitch_4
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Leay;->q:Z

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Leay;->e:J

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->h:Lebb;

    invoke-virtual {v2}, Lebb;->a()V

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->h:Lebb;

    move-object/from16 v0, p0

    iget-wide v4, v0, Leay;->e:J

    invoke-virtual {v2, v4, v5}, Lebb;->a(J)V

    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1f

    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_20

    .line 195
    :cond_1f
    :goto_14
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 194
    :cond_20
    const/4 v2, 0x0

    move v3, v2

    :goto_15
    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_21

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->l:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lecc;

    invoke-static {v2}, Leay;->b(Lecc;)V

    move-object/from16 v0, p0

    iget-wide v4, v0, Leay;->e:J

    invoke-virtual {v2, v4, v5}, Lecc;->a(J)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_15

    :cond_21
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Leay;->a(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->a:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_14

    .line 198
    :pswitch_5
    invoke-direct/range {p0 .. p0}, Leay;->e()V

    .line 199
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 202
    :pswitch_6
    invoke-direct/range {p0 .. p0}, Leay;->f()V

    monitor-enter p0
    :try_end_8
    .catch Leat; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_1

    const/4 v2, 0x1

    :try_start_9
    move-object/from16 v0, p0

    iput-boolean v2, v0, Leay;->o:Z

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    .line 203
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 202
    :catchall_1
    move-exception v2

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    throw v2

    .line 206
    :pswitch_7
    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;
    :try_end_a
    .catch Leat; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_1

    :try_start_b
    check-cast v2, Landroid/util/Pair;

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Leav;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v3, v4, v2}, Leav;->a(ILjava/lang/Object;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :try_start_c
    monitor-enter p0
    :try_end_c
    .catch Leat; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_1

    :try_start_d
    move-object/from16 v0, p0

    iget v2, v0, Leay;->s:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Leay;->s:I

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :try_start_e
    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_22

    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_22

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->a:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_e
    .catch Leat; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_1

    .line 207
    :cond_22
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 206
    :catchall_2
    move-exception v2

    :try_start_f
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    :try_start_10
    throw v2

    :catchall_3
    move-exception v2

    monitor-enter p0
    :try_end_10
    .catch Leat; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_1

    :try_start_11
    move-object/from16 v0, p0

    iget v3, v0, Leay;->s:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Leay;->s:I

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    :try_start_12
    throw v2
    :try_end_12
    .catch Leat; {:try_start_12 .. :try_end_12} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_1

    :catchall_4
    move-exception v2

    :try_start_13
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    :try_start_14
    throw v2

    .line 210
    :pswitch_8
    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg2:I

    if-eqz v2, :cond_24

    const/4 v2, 0x1

    :goto_16
    move-object/from16 v0, p0

    iget-object v4, v0, Leay;->i:[Z

    aget-boolean v4, v4, v3

    if-eq v4, v2, :cond_23

    move-object/from16 v0, p0

    iget-object v4, v0, Leay;->i:[Z

    aput-boolean v2, v4, v3

    move-object/from16 v0, p0

    iget v4, v0, Leay;->r:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_23

    move-object/from16 v0, p0

    iget v4, v0, Leay;->r:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_25

    .line 211
    :cond_23
    :goto_17
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 210
    :cond_24
    const/4 v2, 0x0

    goto :goto_16

    :cond_25
    move-object/from16 v0, p0

    iget-object v4, v0, Leay;->m:[Lecc;

    aget-object v3, v4, v3

    iget v4, v3, Lecc;->g:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_26

    const/4 v5, 0x2

    if-eq v4, v5, :cond_26

    const/4 v5, 0x3

    if-ne v4, v5, :cond_23

    :cond_26
    if-eqz v2, :cond_29

    move-object/from16 v0, p0

    iget-boolean v2, v0, Leay;->p:Z

    if-eqz v2, :cond_28

    move-object/from16 v0, p0

    iget v2, v0, Leay;->r:I

    const/4 v4, 0x4

    if-ne v2, v4, :cond_28

    const/4 v2, 0x1

    :goto_18
    move-object/from16 v0, p0

    iget-wide v4, v0, Leay;->e:J

    invoke-virtual {v3, v4, v5, v2}, Lecc;->b(JZ)V

    move-object/from16 v0, p0

    iget-object v4, v0, Leay;->l:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v2, :cond_27

    invoke-virtual {v3}, Lecc;->p()V

    :cond_27
    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->a:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_17

    :cond_28
    const/4 v2, 0x0

    goto :goto_18

    :cond_29
    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->n:Lecc;

    if-ne v3, v2, :cond_2a

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->h:Lebb;

    invoke-virtual {v3}, Lecc;->f()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lebb;->a(J)V

    :cond_2a
    invoke-static {v3}, Leay;->b(Lecc;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Leay;->l:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Lecc;->r()V
    :try_end_14
    .catch Leat; {:try_start_14 .. :try_end_14} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_1

    goto :goto_17

    :cond_2b
    move-wide v2, v6

    goto/16 :goto_11

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method
