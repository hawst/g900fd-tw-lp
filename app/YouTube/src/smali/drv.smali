.class public Ldrv;
.super Lrq;
.source "SourceFile"


# instance fields
.field private final a:Ldxe;

.field private final b:Ldyg;


# direct methods
.method public constructor <init>(Ldxe;Ldyg;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lrq;-><init>()V

    .line 23
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxe;

    iput-object v0, p0, Ldrv;->a:Ldxe;

    .line 24
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldrv;->b:Ldyg;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    .line 30
    iget-object v0, p0, Ldrv;->b:Ldyg;

    const-string v1, "set volume on route: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iget-object v0, p0, Ldrv;->a:Ldxe;

    invoke-interface {v0, p1}, Ldxe;->a(I)V

    .line 33
    invoke-super {p0, p1}, Lrq;->a(I)V

    .line 34
    return-void
.end method

.method public final b(I)V
    .locals 5

    .prologue
    .line 43
    iget-object v0, p0, Ldrv;->b:Ldyg;

    const-string v1, "update volume on route: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    if-lez p1, :cond_0

    .line 45
    iget-object v0, p0, Ldrv;->a:Ldxe;

    invoke-interface {v0}, Ldxe;->a()V

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Ldrv;->a:Ldxe;

    invoke-interface {v0}, Ldxe;->b()V

    goto :goto_0
.end method
