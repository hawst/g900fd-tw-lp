.class public final Lget;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgei;


# instance fields
.field private final a:Lgei;

.field private b:Z


# direct methods
.method public constructor <init>(Lgei;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgei;

    iput-object v0, p0, Lget;->a:Lgei;

    .line 26
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0}, Lgei;->a()V

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lget;->b:Z

    .line 127
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lget;->b:Z

    if-eqz v0, :cond_0

    .line 41
    packed-switch p1, :pswitch_data_0

    .line 55
    :goto_0
    :pswitch_0
    return-void

    .line 44
    :pswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lget;->b:Z

    goto :goto_0

    .line 54
    :cond_0
    :pswitch_2
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(I)V

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Landroid/os/Handler;)V

    .line 31
    return-void
.end method

.method public final a(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Landroid/os/Message;)V

    .line 121
    return-void
.end method

.method public final a(Lfqj;Lfqj;Lfqj;[Lfre;[Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 99
    iget-object v0, p0, Lget;->a:Lgei;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lgei;->a(Lfqj;Lfqj;Lfqj;[Lfre;[Ljava/lang/String;I)V

    .line 106
    return-void
.end method

.method public final a(Lgcs;)V
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lget;->b:Z

    if-eqz v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Lgcs;)V

    goto :goto_0
.end method

.method public final a(Lggp;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Lggp;)V

    .line 60
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Z)V

    .line 89
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0}, Lgei;->b()V

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lget;->b:Z

    .line 133
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lget;->b:Z

    if-eqz v0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->b(I)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->b(Landroid/os/Handler;)V

    .line 36
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lget;->b:Z

    if-eqz v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->c(I)V

    goto :goto_0
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->d(I)V

    .line 111
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lget;->a:Lgei;

    invoke-interface {v0, p1}, Lgei;->e(I)V

    .line 116
    return-void
.end method
