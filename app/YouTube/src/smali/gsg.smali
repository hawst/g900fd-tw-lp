.class public final Lgsg;
.super Lecc;
.source "SourceFile"


# instance fields
.field private a:J

.field private synthetic b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lgsg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-direct {p0}, Lecc;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(J)V
    .locals 3

    .prologue
    .line 322
    iput-wide p1, p0, Lgsg;->a:J

    .line 323
    iget-object v0, p0, Lgsg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    iget-object v1, p0, Lgsg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-static {v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->f(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->post(Ljava/lang/Runnable;)Z

    .line 324
    return-void
.end method

.method protected final a(JJ)V
    .locals 5

    .prologue
    .line 293
    iput-wide p1, p0, Lgsg;->a:J

    .line 294
    iget-object v0, p0, Lgsg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->d(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)Leau;

    move-result-object v0

    if-nez v0, :cond_1

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    iget v0, p0, Lecc;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lgsg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->e(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_2

    .line 298
    iget-object v0, p0, Lgsg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->d(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)Leau;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leau;->a(Z)V

    .line 300
    :cond_2
    iget-object v0, p0, Lgsg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-static {v0}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->d(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)Leau;

    move-result-object v0

    invoke-virtual {v0}, Leau;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lgsg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    iget-object v1, p0, Lgsg;->b:Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;

    invoke-static {v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->f(Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/upload/edit/ui/EditableVideoControllerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected final a(JZ)V
    .locals 1

    .prologue
    .line 288
    iput-wide p1, p0, Lgsg;->a:J

    .line 289
    return-void
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    return v0
.end method

.method protected final e()Z
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x1

    return v0
.end method

.method protected final f()J
    .locals 2

    .prologue
    .line 312
    iget-wide v0, p0, Lgsg;->a:J

    return-wide v0
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    return v0
.end method

.method protected final m()J
    .locals 2

    .prologue
    .line 307
    const-wide/16 v0, -0x2

    return-wide v0
.end method

.method protected final n()J
    .locals 2

    .prologue
    .line 317
    const-wide/16 v0, -0x3

    return-wide v0
.end method
