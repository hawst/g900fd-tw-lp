.class public final Lbkl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgmw;


# static fields
.field private static final a:J


# instance fields
.field private final b:Lbjx;

.field private final c:Lgit;

.field private final d:Lbkm;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 36
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lbkl;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lgit;Lbjx;)V
    .locals 2

    .prologue
    .line 51
    new-instance v1, Lbkm;

    .line 52
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lemg;->a(Landroid/content/Context;)Lemg;

    move-result-object v0

    invoke-direct {v1, v0}, Lbkm;-><init>(Lemg;)V

    .line 51
    invoke-direct {p0, v1, p2, p3}, Lbkl;-><init>(Lbkm;Lgit;Lbjx;)V

    .line 55
    return-void
.end method

.method private constructor <init>(Lbkm;Lgit;Lbjx;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgit;

    iput-object v0, p0, Lbkl;->c:Lgit;

    .line 63
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjx;

    iput-object v0, p0, Lbkl;->b:Lbjx;

    .line 64
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkm;

    iput-object v0, p0, Lbkl;->d:Lbkm;

    .line 66
    iget-object v0, p2, Lgit;->c:Ljava/lang/String;

    .line 67
    invoke-static {v0}, La;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbkl;->e:Ljava/lang/String;

    .line 69
    invoke-static {v0}, La;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbkl;->f:Ljava/lang/String;

    .line 70
    return-void
.end method

.method private c(J)V
    .locals 7

    .prologue
    .line 122
    iget-object v1, p0, Lbkl;->d:Lbkm;

    sget-wide v4, Lbkl;->a:J

    iget-object v0, p0, Lbkl;->c:Lgit;

    .line 125
    iget-object v0, v0, Lgit;->c:Ljava/lang/String;

    invoke-static {v0}, La;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-wide v2, p1

    .line 122
    invoke-virtual/range {v1 .. v6}, Lbkm;->a(JJLjava/lang/String;)V

    .line 126
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 92
    iget-object v0, p0, Lbkl;->b:Lbjx;

    iget-object v1, p0, Lbkl;->c:Lgit;

    invoke-virtual {v0, v1}, Lbjx;->a(Lgit;)J

    move-result-wide v0

    .line 93
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 94
    invoke-direct {p0, v0, v1}, Lbkl;->c(J)V

    .line 96
    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 74
    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    .line 75
    iget-object v1, p0, Lbkl;->d:Lbkm;

    const-wide/16 v4, 0x1

    iget-object v6, p0, Lbkl;->e:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lbkm;->b(JJLjava/lang/String;)V

    .line 80
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-direct {p0, p1, p2}, Lbkl;->c(J)V

    .line 78
    iget-object v0, p0, Lbkl;->b:Lbjx;

    iget-object v1, p0, Lbkl;->c:Lgit;

    invoke-virtual {v0, v1, p1, p2}, Lbjx;->a(Lgit;J)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lbkl;->d:Lbkm;

    iget-object v1, p0, Lbkl;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbkm;->a(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lbkl;->d:Lbkm;

    iget-object v1, p0, Lbkl;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbkm;->a(Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public final b(J)V
    .locals 7

    .prologue
    .line 84
    iget-object v1, p0, Lbkl;->d:Lbkm;

    sget-wide v2, Lbkl;->a:J

    add-long v4, p1, v2

    iget-object v6, p0, Lbkl;->f:Ljava/lang/String;

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lbkm;->b(JJLjava/lang/String;)V

    .line 88
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 106
    invoke-virtual {p0}, Lbkl;->b()V

    .line 107
    iget-object v0, p0, Lbkl;->b:Lbjx;

    iget-object v1, p0, Lbkl;->c:Lgit;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lbjx;->a(Lgit;J)V

    .line 108
    return-void
.end method
