.class public final Lhbd;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:F

.field private c:F

.field private d:F

.field private e:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14706
    invoke-direct {p0}, Lidf;-><init>()V

    .line 14709
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhbd;->a:Z

    .line 14712
    iput v1, p0, Lhbd;->b:F

    .line 14715
    iput v1, p0, Lhbd;->c:F

    .line 14718
    iput v1, p0, Lhbd;->d:F

    .line 14721
    iput v1, p0, Lhbd;->e:F

    .line 14706
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 14782
    const/4 v0, 0x0

    .line 14783
    iget-boolean v1, p0, Lhbd;->a:Z

    if-eqz v1, :cond_0

    .line 14784
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhbd;->a:Z

    .line 14785
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 14787
    :cond_0
    iget v1, p0, Lhbd;->b:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_1

    .line 14788
    const/4 v1, 0x2

    iget v2, p0, Lhbd;->b:F

    .line 14789
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 14791
    :cond_1
    iget v1, p0, Lhbd;->c:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_2

    .line 14792
    const/4 v1, 0x3

    iget v2, p0, Lhbd;->c:F

    .line 14793
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 14795
    :cond_2
    iget v1, p0, Lhbd;->d:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_3

    .line 14796
    const/4 v1, 0x4

    iget v2, p0, Lhbd;->d:F

    .line 14797
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 14799
    :cond_3
    iget v1, p0, Lhbd;->e:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_4

    .line 14800
    const/4 v1, 0x5

    iget v2, p0, Lhbd;->e:F

    .line 14801
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 14803
    :cond_4
    iget-object v1, p0, Lhbd;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14804
    iput v0, p0, Lhbd;->J:I

    .line 14805
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 14702
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbd;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbd;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbd;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhbd;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhbd;->b:F

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhbd;->c:F

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhbd;->d:F

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhbd;->e:F

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14761
    iget-boolean v0, p0, Lhbd;->a:Z

    if-eqz v0, :cond_0

    .line 14762
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhbd;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 14764
    :cond_0
    iget v0, p0, Lhbd;->b:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    .line 14765
    const/4 v0, 0x2

    iget v1, p0, Lhbd;->b:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    .line 14767
    :cond_1
    iget v0, p0, Lhbd;->c:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 14768
    const/4 v0, 0x3

    iget v1, p0, Lhbd;->c:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    .line 14770
    :cond_2
    iget v0, p0, Lhbd;->d:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_3

    .line 14771
    const/4 v0, 0x4

    iget v1, p0, Lhbd;->d:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    .line 14773
    :cond_3
    iget v0, p0, Lhbd;->e:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_4

    .line 14774
    const/4 v0, 0x5

    iget v1, p0, Lhbd;->e:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    .line 14776
    :cond_4
    iget-object v0, p0, Lhbd;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 14778
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 14736
    if-ne p1, p0, :cond_1

    .line 14744
    :cond_0
    :goto_0
    return v0

    .line 14737
    :cond_1
    instance-of v2, p1, Lhbd;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 14738
    :cond_2
    check-cast p1, Lhbd;

    .line 14739
    iget-boolean v2, p0, Lhbd;->a:Z

    iget-boolean v3, p1, Lhbd;->a:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhbd;->b:F

    iget v3, p1, Lhbd;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lhbd;->c:F

    iget v3, p1, Lhbd;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lhbd;->d:F

    iget v3, p1, Lhbd;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lhbd;->e:F

    iget v3, p1, Lhbd;->e:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-object v2, p0, Lhbd;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbd;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 14744
    goto :goto_0

    .line 14739
    :cond_4
    iget-object v2, p0, Lhbd;->I:Ljava/util/List;

    iget-object v3, p1, Lhbd;->I:Ljava/util/List;

    .line 14744
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 14748
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 14750
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lhbd;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 14751
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhbd;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 14752
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhbd;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 14753
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhbd;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 14754
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhbd;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 14755
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhbd;->I:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 14756
    return v0

    .line 14750
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 14755
    :cond_1
    iget-object v0, p0, Lhbd;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_1
.end method
