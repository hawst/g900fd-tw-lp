.class public final Ldub;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ldul;

.field final c:Ldwj;

.field final d:Ljava/lang/String;

.field final e:Ldwb;

.field f:Lejr;

.field g:Ljava/lang/String;

.field h:Leii;

.field i:Ldum;

.field private final j:Ldyg;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ldwb;Ldul;Ldwj;Ldyg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Ldub;->a:Landroid/content/Context;

    .line 96
    iput-object p2, p0, Ldub;->d:Ljava/lang/String;

    .line 97
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwb;

    iput-object v0, p0, Ldub;->e:Ldwb;

    .line 98
    iget-object v0, p0, Ldub;->e:Ldwb;

    invoke-virtual {v0}, Ldwb;->i()Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    .line 99
    iput-object p4, p0, Ldub;->b:Ldul;

    .line 100
    iput-object p5, p0, Ldub;->c:Ldwj;

    .line 101
    iput-object p6, p0, Ldub;->j:Ldyg;

    .line 103
    iput-object v1, p0, Ldub;->f:Lejr;

    .line 104
    iput-object v1, p0, Ldub;->g:Ljava/lang/String;

    .line 105
    iput-object v1, p0, Ldub;->h:Leii;

    .line 106
    sget-object v0, Ldum;->a:Ldum;

    iput-object v0, p0, Ldub;->i:Ldum;

    .line 107
    return-void
.end method


# virtual methods
.method public final a(D)V
    .locals 5

    .prologue
    .line 178
    const-string v0, "SET VOLUME "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 179
    iget-object v0, p0, Ldub;->i:Ldum;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cast command "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". State: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldub;->a(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Ldub;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    const-string v0, "does not support local volume controls"

    invoke-virtual {p0, v0}, Ldub;->b(Ljava/lang/String;)V

    .line 195
    :goto_0
    return-void

    .line 187
    :cond_0
    :try_start_0
    sget-object v0, Lehm;->c:Lehp;

    iget-object v2, p0, Ldub;->f:Lejr;

    invoke-virtual {v0, v2, p1, p2}, Lehp;->a(Lejr;D)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Command "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldub;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :catch_1
    move-exception v0

    .line 191
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Command "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldub;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 192
    :catch_2
    move-exception v0

    .line 193
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Command "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldub;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 202
    iget-object v0, p0, Ldub;->j:Ldyg;

    const-string v1, " CastRouteSession: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "disconnect, should stop the application: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldub;->a(Ljava/lang/String;)V

    .line 121
    sget-object v0, Ldum;->d:Ldum;

    iput-object v0, p0, Ldub;->i:Ldum;

    .line 123
    if-eqz p1, :cond_0

    .line 124
    iget-object v0, p0, Ldub;->f:Lejr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldub;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v1, "Stop currently running application, sessionId: "

    iget-object v0, p0, Ldub;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Ldub;->a(Ljava/lang/String;)V

    sget-object v0, Lehm;->c:Lehp;

    iget-object v1, p0, Ldub;->f:Lejr;

    iget-object v2, p0, Ldub;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lehp;->a(Lejr;Ljava/lang/String;)Leju;

    .line 126
    :cond_0
    :goto_1
    const-string v0, "remove remote media player"

    invoke-virtual {p0, v0}, Ldub;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldub;->h:Leii;

    if-eqz v0, :cond_1

    iput-object v3, p0, Ldub;->h:Leii;

    .line 127
    :cond_1
    const-string v0, "Disconnect client"

    invoke-virtual {p0, v0}, Ldub;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldub;->f:Lejr;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldub;->f:Lejr;

    invoke-interface {v0}, Lejr;->b()V

    iput-object v3, p0, Ldub;->f:Lejr;

    .line 128
    :cond_2
    return-void

    .line 124
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, "Stop currently running application, session id is not known"

    invoke-virtual {p0, v0}, Ldub;->a(Ljava/lang/String;)V

    sget-object v0, Lehm;->c:Lehp;

    iget-object v1, p0, Ldub;->f:Lejr;

    invoke-virtual {v0, v1}, Lehp;->a(Lejr;)Leju;

    goto :goto_1
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Ldub;->i:Ldum;

    sget-object v1, Ldum;->c:Ldum;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldub;->f:Lejr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldub;->h:Leii;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 206
    iget-object v0, p0, Ldub;->j:Ldyg;

    const-string v1, " CastRouteSession Error: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ldyg;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Ldub;->i:Ldum;

    sget-object v1, Ldum;->c:Ldum;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldub;->f:Lejr;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c(Ljava/lang/String;)Lejx;
    .locals 1

    .prologue
    .line 279
    new-instance v0, Ldud;

    invoke-direct {v0, p0, p1}, Ldud;-><init>(Ldub;Ljava/lang/String;)V

    return-object v0
.end method

.method c()V
    .locals 3

    .prologue
    .line 210
    sget-object v0, Ldum;->d:Ldum;

    iput-object v0, p0, Ldub;->i:Ldum;

    .line 211
    const-string v0, "cannot connect"

    invoke-virtual {p0, v0}, Ldub;->b(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Ldub;->b:Ldul;

    sget-object v1, Ldwh;->a:Ldwh;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ldul;->a(Ldwh;Z)V

    .line 213
    return-void
.end method
