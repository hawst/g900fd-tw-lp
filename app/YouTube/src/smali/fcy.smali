.class public final Lfcy;
.super Lfsp;
.source "SourceFile"


# instance fields
.field private final a:Lfth;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lfsz;Lgit;Lfth;)V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p2, Lgit;->b:Lgiv;

    sget-object v1, Lfsq;->c:Lfsq;

    invoke-direct {p0, p1, v0, v1}, Lfsp;-><init>(Lfsz;Lgiv;Lfsq;)V

    .line 136
    const-string v0, ""

    iput-object v0, p0, Lfcy;->b:Ljava/lang/String;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lfcy;->c:Ljava/lang/String;

    .line 138
    const-string v0, ""

    iput-object v0, p0, Lfcy;->d:Ljava/lang/String;

    .line 145
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfth;

    iput-object v0, p0, Lfcy;->a:Lfth;

    .line 146
    return-void
.end method


# virtual methods
.method public final a(Lfjk;)Lfcy;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p1, Lfjk;->a:Ljava/lang/String;

    invoke-static {v0}, Lfcy;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfcy;->c:Ljava/lang/String;

    .line 155
    iget-object v0, p1, Lfjk;->b:[B

    invoke-virtual {p0, v0}, Lfcy;->a([B)V

    .line 156
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lfcy;
    .locals 1

    .prologue
    .line 149
    invoke-static {p1}, Lfcy;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfcy;->b:Ljava/lang/String;

    .line 150
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    const-string v0, "browse"

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lfcy;
    .locals 1

    .prologue
    .line 160
    invoke-static {p1}, Lfcy;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfcy;->d:Ljava/lang/String;

    .line 161
    return-object p0
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 173
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lfcy;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lfcy;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lfcy;->a([Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public final synthetic c()Lidh;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lhbo;

    invoke-direct {v0}, Lhbo;-><init>()V

    invoke-virtual {p0}, Lfcy;->i()Lhjx;

    move-result-object v1

    iput-object v1, v0, Lhbo;->a:Lhjx;

    iget-object v1, p0, Lfcy;->b:Ljava/lang/String;

    iput-object v1, v0, Lhbo;->b:Ljava/lang/String;

    iget-object v1, p0, Lfcy;->c:Ljava/lang/String;

    iput-object v1, v0, Lhbo;->d:Ljava/lang/String;

    iget-object v1, p0, Lfcy;->d:Ljava/lang/String;

    iput-object v1, v0, Lhbo;->c:Ljava/lang/String;

    iget-object v1, p0, Lfcy;->a:Lfth;

    invoke-interface {v1}, Lfth;->a()Lhrn;

    move-result-object v1

    iput-object v1, v0, Lhbo;->e:Lhrn;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 198
    invoke-virtual {p0}, Lfcy;->j()Lggq;

    move-result-object v0

    .line 199
    const-string v1, "browseId"

    iget-object v2, p0, Lfcy;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 200
    const-string v1, "continuation"

    iget-object v2, p0, Lfcy;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 201
    const-string v1, "params"

    iget-object v2, p0, Lfcy;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 202
    invoke-virtual {v0}, Lggq;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
