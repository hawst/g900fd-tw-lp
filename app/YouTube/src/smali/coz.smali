.class public final Lcoz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcvm;


# static fields
.field private static final a:J

.field private static final e:Ljava/util/HashSet;

.field private static final f:Ljava/util/HashSet;

.field private static final g:Ljava/util/HashSet;


# instance fields
.field private final b:Lcnu;

.field private final c:Levn;

.field private final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcoz;->a:J

    .line 32
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Lcvj;

    sget-object v2, Lcvj;->d:Lcvj;

    aput-object v2, v1, v4

    sget-object v2, Lcvj;->e:Lcvj;

    aput-object v2, v1, v5

    .line 33
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcoz;->e:Ljava/util/HashSet;

    .line 35
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Lcvj;

    sget-object v2, Lcvj;->f:Lcvj;

    aput-object v2, v1, v4

    sget-object v2, Lcvj;->g:Lcvj;

    aput-object v2, v1, v5

    .line 36
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcoz;->f:Ljava/util/HashSet;

    .line 38
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Lcvj;

    sget-object v2, Lcvj;->b:Lcvj;

    aput-object v2, v1, v4

    sget-object v2, Lcvj;->c:Lcvj;

    aput-object v2, v1, v5

    .line 39
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcoz;->g:Ljava/util/HashSet;

    .line 38
    return-void
.end method

.method public constructor <init>(Lcnu;Levn;J)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnu;

    iput-object v0, p0, Lcoz;->b:Lcnu;

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcoz;->c:Levn;

    .line 48
    iput-wide p3, p0, Lcoz;->d:J

    .line 49
    return-void
.end method

.method private static a(JJJJ)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 111
    cmp-long v1, p0, p2

    if-gez v1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p4, p5, p2, p3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    add-long/2addr v2, p6

    cmp-long v1, p0, v2

    if-gez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JLjava/lang/Iterable;)V
    .locals 15

    .prologue
    .line 57
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 58
    const-wide/16 v4, 0x0

    .line 59
    iget-object v0, p0, Lcoz;->b:Lcnu;

    invoke-virtual {v0}, Lcnu;->c()I

    move-result v0

    if-gtz v0, :cond_2

    const-wide/16 v0, 0x0

    .line 61
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 62
    const-wide/16 v0, 0x0

    .line 64
    :cond_0
    invoke-interface/range {p3 .. p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v6, v0

    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcvh;

    .line 65
    sget-object v0, Lcoz;->e:Ljava/util/HashSet;

    iget-object v1, v8, Lcvh;->d:Lcvj;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 67
    iget-object v0, v8, Lcvv;->e:Lcvw;

    iget-wide v0, v0, Lcvw;->b:J

    move-wide/from16 v2, p1

    .line 66
    invoke-static/range {v0 .. v7}, Lcoz;->a(JJJJ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 71
    sget-object v0, Lcvj;->d:Lcvj;

    invoke-virtual {v8, v0}, Lcvh;->a(Lcvj;)V

    .line 72
    new-instance v0, Ldfq;

    .line 73
    iget-object v1, v8, Lcvv;->e:Lcvw;

    iget-wide v2, v1, Lcvw;->b:J

    iget-object v1, v8, Lcvv;->f:Lcvw;

    iget-wide v12, v1, Lcvw;->b:J

    invoke-direct {v0, v2, v3, v12, v13}, Ldfq;-><init>(JJ)V

    .line 72
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v0, v8, Lcvv;->e:Lcvw;

    iget-wide v0, v0, Lcvw;->b:J

    cmp-long v0, v0, p1

    if-ltz v0, :cond_1

    .line 75
    iget-object v0, v8, Lcvv;->e:Lcvw;

    iget-wide v4, v0, Lcvw;->b:J

    .line 76
    sget-wide v6, Lcoz;->a:J

    goto :goto_1

    .line 59
    :cond_2
    sget-wide v0, Lcoz;->a:J

    iget-object v2, p0, Lcoz;->b:Lcnu;

    .line 60
    invoke-virtual {v2}, Lcnu;->c()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0

    .line 79
    :cond_3
    sget-object v0, Lcvj;->e:Lcvj;

    invoke-virtual {v8, v0}, Lcvh;->a(Lcvj;)V

    goto :goto_1

    .line 81
    :cond_4
    sget-object v0, Lcoz;->g:Ljava/util/HashSet;

    iget-object v1, v8, Lcvh;->d:Lcvj;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 83
    iget-object v0, v8, Lcvv;->e:Lcvw;

    iget-wide v0, v0, Lcvw;->b:J

    iget-wide v2, p0, Lcoz;->d:J

    add-long/2addr v0, v2

    move-wide/from16 v2, p1

    .line 82
    invoke-static/range {v0 .. v7}, Lcoz;->a(JJJJ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 87
    sget-object v0, Lcvj;->b:Lcvj;

    invoke-virtual {v8, v0}, Lcvh;->a(Lcvj;)V

    goto :goto_1

    .line 89
    :cond_5
    sget-object v0, Lcvj;->c:Lcvj;

    invoke-virtual {v8, v0}, Lcvh;->a(Lcvj;)V

    goto :goto_1

    .line 91
    :cond_6
    sget-object v0, Lcoz;->f:Ljava/util/HashSet;

    iget-object v1, v8, Lcvh;->d:Lcvj;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, v8, Lcvv;->f:Lcvw;

    iget-wide v0, v0, Lcvw;->b:J

    move-wide/from16 v2, p1

    .line 92
    invoke-static/range {v0 .. v7}, Lcoz;->a(JJJJ)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 97
    sget-object v0, Lcvj;->f:Lcvj;

    invoke-virtual {v8, v0}, Lcvh;->a(Lcvj;)V

    goto/16 :goto_1

    .line 99
    :cond_7
    sget-object v0, Lcvj;->g:Lcvj;

    invoke-virtual {v8, v0}, Lcvh;->a(Lcvj;)V

    goto/16 :goto_1

    .line 103
    :cond_8
    iget-object v0, p0, Lcoz;->c:Levn;

    new-instance v1, Ldft;

    sget-object v2, Ldfs;->a:Ldfs;

    invoke-direct {v1, v2, v9}, Ldft;-><init>(Ldfs;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 104
    return-void
.end method
