.class public final Lhkd;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhkd;


# instance fields
.field public b:Liae;

.field public c:Lhuf;

.field public d:Lhcn;

.field public e:J

.field public f:Lhud;

.field public g:Lhcq;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhkd;

    sput-object v0, Lhkd;->a:[Lhkd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v2, p0, Lhkd;->b:Liae;

    iput-object v2, p0, Lhkd;->c:Lhuf;

    iput-object v2, p0, Lhkd;->d:Lhcn;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhkd;->e:J

    iput-object v2, p0, Lhkd;->f:Lhud;

    iput-object v2, p0, Lhkd;->g:Lhcq;

    const/4 v0, 0x0

    iput v0, p0, Lhkd;->h:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhkd;->b:Liae;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhkd;->b:Liae;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhkd;->c:Lhuf;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhkd;->c:Lhuf;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhkd;->d:Lhcn;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhkd;->d:Lhcn;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-wide v2, p0, Lhkd;->e:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-wide v2, p0, Lhkd;->e:J

    invoke-static {v1, v2, v3}, Lidd;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lhkd;->f:Lhud;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lhkd;->f:Lhud;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhkd;->g:Lhcq;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lhkd;->g:Lhcq;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lhkd;->h:I

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Lhkd;->h:I

    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhkd;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhkd;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhkd;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhkd;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhkd;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhkd;->b:Liae;

    if-nez v0, :cond_2

    new-instance v0, Liae;

    invoke-direct {v0}, Liae;-><init>()V

    iput-object v0, p0, Lhkd;->b:Liae;

    :cond_2
    iget-object v0, p0, Lhkd;->b:Liae;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhkd;->c:Lhuf;

    if-nez v0, :cond_3

    new-instance v0, Lhuf;

    invoke-direct {v0}, Lhuf;-><init>()V

    iput-object v0, p0, Lhkd;->c:Lhuf;

    :cond_3
    iget-object v0, p0, Lhkd;->c:Lhuf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhkd;->d:Lhcn;

    if-nez v0, :cond_4

    new-instance v0, Lhcn;

    invoke-direct {v0}, Lhcn;-><init>()V

    iput-object v0, p0, Lhkd;->d:Lhcn;

    :cond_4
    iget-object v0, p0, Lhkd;->d:Lhcn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lhkd;->e:J

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhkd;->f:Lhud;

    if-nez v0, :cond_5

    new-instance v0, Lhud;

    invoke-direct {v0}, Lhud;-><init>()V

    iput-object v0, p0, Lhkd;->f:Lhud;

    :cond_5
    iget-object v0, p0, Lhkd;->f:Lhud;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhkd;->g:Lhcq;

    if-nez v0, :cond_6

    new-instance v0, Lhcq;

    invoke-direct {v0}, Lhcq;-><init>()V

    iput-object v0, p0, Lhkd;->g:Lhcq;

    :cond_6
    iget-object v0, p0, Lhkd;->g:Lhcq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhkd;->h:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 4

    iget-object v0, p0, Lhkd;->b:Liae;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhkd;->b:Liae;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhkd;->c:Lhuf;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhkd;->c:Lhuf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhkd;->d:Lhcn;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhkd;->d:Lhcn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-wide v0, p0, Lhkd;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-wide v2, p0, Lhkd;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(IJ)V

    :cond_3
    iget-object v0, p0, Lhkd;->f:Lhud;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lhkd;->f:Lhud;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Lhkd;->g:Lhcq;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lhkd;->g:Lhcq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_5
    iget v0, p0, Lhkd;->h:I

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Lhkd;->h:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    :cond_6
    iget-object v0, p0, Lhkd;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhkd;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhkd;

    iget-object v2, p0, Lhkd;->b:Liae;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhkd;->b:Liae;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhkd;->c:Lhuf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhkd;->c:Lhuf;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhkd;->d:Lhcn;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhkd;->d:Lhcn;

    if-nez v2, :cond_3

    :goto_3
    iget-wide v2, p0, Lhkd;->e:J

    iget-wide v4, p1, Lhkd;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhkd;->f:Lhud;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhkd;->f:Lhud;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhkd;->g:Lhcq;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhkd;->g:Lhcq;

    if-nez v2, :cond_3

    :goto_5
    iget v2, p0, Lhkd;->h:I

    iget v3, p1, Lhkd;->h:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhkd;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhkd;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhkd;->b:Liae;

    iget-object v3, p1, Lhkd;->b:Liae;

    invoke-virtual {v2, v3}, Liae;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhkd;->c:Lhuf;

    iget-object v3, p1, Lhkd;->c:Lhuf;

    invoke-virtual {v2, v3}, Lhuf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhkd;->d:Lhcn;

    iget-object v3, p1, Lhkd;->d:Lhcn;

    invoke-virtual {v2, v3}, Lhcn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhkd;->f:Lhud;

    iget-object v3, p1, Lhkd;->f:Lhud;

    invoke-virtual {v2, v3}, Lhud;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhkd;->g:Lhcq;

    iget-object v3, p1, Lhkd;->g:Lhcq;

    invoke-virtual {v2, v3}, Lhcq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhkd;->I:Ljava/util/List;

    iget-object v3, p1, Lhkd;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkd;->b:Liae;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkd;->c:Lhuf;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkd;->d:Lhcn;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhkd;->e:J

    iget-wide v4, p0, Lhkd;->e:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkd;->f:Lhud;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkd;->g:Lhcq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhkd;->h:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhkd;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lhkd;->b:Liae;

    invoke-virtual {v0}, Liae;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhkd;->c:Lhuf;

    invoke-virtual {v0}, Lhuf;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhkd;->d:Lhcn;

    invoke-virtual {v0}, Lhcn;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhkd;->f:Lhud;

    invoke-virtual {v0}, Lhud;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lhkd;->g:Lhcq;

    invoke-virtual {v0}, Lhcq;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lhkd;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
