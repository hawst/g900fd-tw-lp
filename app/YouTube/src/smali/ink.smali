.class public final Link;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Linn;


# static fields
.field private static final a:Ljava/util/List;

.field private static final b:Ljava/util/List;


# instance fields
.field private final c:Linc;

.field private final d:Lios;

.field private e:Lipe;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52
    const/4 v0, 0x5

    new-array v0, v0, [Lipv;

    const-string v1, "connection"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "host"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "keep-alive"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "proxy-connection"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "transfer-encoding"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Limo;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Link;->a:Ljava/util/List;

    .line 60
    const/16 v0, 0x8

    new-array v0, v0, [Lipv;

    const-string v1, "connection"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "host"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "keep-alive"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "proxy-connection"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "te"

    invoke-static {v1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "transfer-encoding"

    invoke-static {v2}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "encoding"

    invoke-static {v2}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "upgrade"

    invoke-static {v2}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Limo;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Link;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Linc;Lios;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Link;->c:Linc;

    .line 76
    iput-object p2, p0, Link;->d:Lios;

    .line 77
    return-void
.end method

.method private static a(Lima;Lipv;)Z
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lima;->c:Lima;

    if-ne p0, v0, :cond_0

    .line 233
    sget-object v0, Link;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 235
    :goto_0
    return v0

    .line 234
    :cond_0
    sget-object v0, Lima;->d:Lima;

    if-ne p0, v0, :cond_1

    .line 235
    sget-object v0, Link;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 237
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method public final a(Limb;)Liqk;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Link;->b(Limb;)V

    .line 82
    iget-object v0, p0, Link;->e:Lipe;

    invoke-virtual {v0}, Lipe;->d()Liqk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lu;)Liql;
    .locals 2

    .prologue
    .line 216
    new-instance v0, Linl;

    iget-object v1, p0, Link;->e:Lipe;

    invoke-direct {v0, v1, p1}, Linl;-><init>(Lipe;Lu;)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Link;->e:Lipe;

    invoke-virtual {v0}, Lipe;->d()Liqk;

    move-result-object v0

    invoke-interface {v0}, Liqk;->close()V

    .line 104
    return-void
.end method

.method public final a(Linc;)V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Link;->e:Lipe;

    sget-object v1, Linr;->h:Linr;

    invoke-virtual {v0, v1}, Lipe;->a(Linr;)V

    .line 224
    return-void
.end method

.method public final a(Lini;)V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Lime;
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 107
    iget-object v0, p0, Link;->e:Lipe;

    invoke-virtual {v0}, Lipe;->c()Ljava/util/List;

    move-result-object v6

    iget-object v0, p0, Link;->d:Lios;

    iget-object v7, v0, Lios;->a:Lima;

    const/4 v2, 0x0

    const-string v1, "HTTP/1.1"

    new-instance v8, Lilt;

    invoke-direct {v8}, Lilt;-><init>()V

    sget-object v0, Ling;->c:Ljava/lang/String;

    invoke-virtual {v7}, Lima;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v0, v4}, Lilt;->c(Ljava/lang/String;Ljava/lang/String;)Lilt;

    move v5, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_5

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linv;

    iget-object v9, v0, Linv;->h:Lipv;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linv;

    iget-object v0, v0, Linv;->i:Lipv;

    invoke-virtual {v0}, Lipv;->a()Ljava/lang/String;

    move-result-object v10

    move-object v0, v1

    move v1, v3

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_4

    invoke-virtual {v10, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    const/4 v11, -0x1

    if-ne v4, v11, :cond_0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v4

    :cond_0
    invoke-virtual {v10, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    sget-object v11, Linv;->a:Lipv;

    invoke-virtual {v9, v11}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v12, v2

    move-object v2, v1

    move v1, v12

    goto :goto_1

    :cond_1
    sget-object v11, Linv;->g:Lipv;

    invoke-virtual {v9, v11}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    move-object v0, v1

    move-object v1, v2

    goto :goto_2

    :cond_2
    invoke-static {v7, v9}, Link;->a(Lima;Lipv;)Z

    move-result v11

    if-nez v11, :cond_3

    invoke-virtual {v9}, Lipv;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11, v1}, Lilt;->a(Ljava/lang/String;Ljava/lang/String;)Lilt;

    :cond_3
    move-object v1, v2

    goto :goto_2

    :cond_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v1, v0

    goto :goto_0

    :cond_5
    if-nez v2, :cond_6

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Expected \':status\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    if-nez v1, :cond_7

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Expected \':version\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Linm;->a(Ljava/lang/String;)Linm;

    move-result-object v0

    new-instance v1, Lime;

    invoke-direct {v1}, Lime;-><init>()V

    iput-object v7, v1, Lime;->b:Lima;

    iget v2, v0, Linm;->b:I

    iput v2, v1, Lime;->c:I

    iget-object v0, v0, Linm;->c:Ljava/lang/String;

    iput-object v0, v1, Lime;->d:Ljava/lang/String;

    invoke-virtual {v8}, Lilt;->a()Lils;

    move-result-object v0

    invoke-virtual {v1, v0}, Lime;->a(Lils;)Lime;

    move-result-object v0

    return-object v0
.end method

.method public final b(Limb;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 86
    iget-object v0, p0, Link;->e:Lipe;

    if-eqz v0, :cond_0

    .line 96
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Link;->c:Linc;

    invoke-virtual {v0}, Linc;->a()V

    .line 89
    iget-object v0, p0, Link;->c:Linc;

    invoke-virtual {v0}, Linc;->b()Z

    move-result v4

    .line 90
    iget-object v0, p0, Link;->c:Linc;

    iget-object v0, v0, Linc;->c:Liln;

    iget-object v0, v0, Liln;->g:Lima;

    invoke-static {v0}, La;->a(Lima;)Ljava/lang/String;

    move-result-object v0

    .line 92
    iget-object v5, p0, Link;->d:Lios;

    iget-object v1, p0, Link;->d:Lios;

    iget-object v6, v1, Lios;->a:Lima;

    iget-object v7, p1, Limb;->c:Lils;

    new-instance v8, Ljava/util/ArrayList;

    iget-object v1, v7, Lils;->a:[Ljava/lang/String;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0xa

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Linv;

    sget-object v3, Linv;->b:Lipv;

    iget-object v9, p1, Limb;->b:Ljava/lang/String;

    invoke-direct {v1, v3, v9}, Linv;-><init>(Lipv;Ljava/lang/String;)V

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Linv;

    sget-object v3, Linv;->c:Lipv;

    invoke-virtual {p1}, Limb;->a()Ljava/net/URL;

    move-result-object v9

    invoke-static {v9}, La;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v3, v9}, Linv;-><init>(Lipv;Ljava/lang/String;)V

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Limb;->a()Ljava/net/URL;

    move-result-object v1

    invoke-static {v1}, Linc;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lima;->c:Lima;

    if-ne v3, v6, :cond_2

    new-instance v3, Linv;

    sget-object v9, Linv;->g:Lipv;

    invoke-direct {v3, v9, v0}, Linv;-><init>(Lipv;Ljava/lang/String;)V

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Linv;

    sget-object v3, Linv;->f:Lipv;

    invoke-direct {v0, v3, v1}, Linv;-><init>(Lipv;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance v0, Linv;

    sget-object v1, Linv;->d:Lipv;

    invoke-virtual {p1}, Limb;->a()Ljava/net/URL;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Linv;-><init>(Lipv;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v9, Ljava/util/LinkedHashSet;

    invoke-direct {v9}, Ljava/util/LinkedHashSet;-><init>()V

    move v3, v2

    :goto_2
    iget-object v0, v7, Lils;->a:[Ljava/lang/String;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    if-ge v3, v0, :cond_6

    invoke-virtual {v7, v3}, Lils;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v10

    invoke-virtual {v7, v3}, Lils;->b(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v10}, Link;->a(Lima;Lipv;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Linv;->b:Lipv;

    invoke-virtual {v10, v0}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Linv;->c:Lipv;

    invoke-virtual {v10, v0}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Linv;->d:Lipv;

    invoke-virtual {v10, v0}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Linv;->e:Lipv;

    invoke-virtual {v10, v0}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Linv;->f:Lipv;

    invoke-virtual {v10, v0}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Linv;->g:Lipv;

    invoke-virtual {v10, v0}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Linv;

    invoke-direct {v0, v10, v11}, Linv;-><init>(Lipv;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_2
    sget-object v0, Lima;->d:Lima;

    if-ne v0, v6, :cond_3

    new-instance v0, Linv;

    sget-object v3, Linv;->e:Lipv;

    invoke-direct {v0, v3, v1}, Linv;-><init>(Lipv;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_3
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    move v1, v2

    :goto_4
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linv;

    iget-object v0, v0, Linv;->h:Lipv;

    invoke-virtual {v0, v10}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linv;

    iget-object v0, v0, Linv;->i:Lipv;

    invoke-virtual {v0}, Lipv;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v11, Linv;

    invoke-direct {v11, v10, v0}, Linv;-><init>(Lipv;Ljava/lang/String;)V

    invoke-interface {v8, v1, v11}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_6
    const/4 v0, 0x1

    invoke-virtual {v5, v2, v8, v4, v0}, Lios;->a(ILjava/util/List;ZZ)Lipe;

    move-result-object v0

    iput-object v0, p0, Link;->e:Lipe;

    .line 95
    iget-object v0, p0, Link;->e:Lipe;

    iget-object v0, v0, Lipe;->h:Liph;

    iget-object v1, p0, Link;->c:Linc;

    iget-object v1, v1, Linc;->b:Lilw;

    iget v1, v1, Lilw;->q:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Liqm;->a(JLjava/util/concurrent/TimeUnit;)Liqm;

    goto/16 :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 220
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method
