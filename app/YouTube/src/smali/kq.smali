.class abstract Lkq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lkp;

.field b:Lkm;

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field final g:Lmt;

.field h:Lmt;

.field i:Z

.field private j:Landroid/view/MenuInflater;


# direct methods
.method constructor <init>(Lkp;)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Lkr;

    invoke-direct {v0, p0}, Lkr;-><init>(Lkq;)V

    iput-object v0, p0, Lkq;->g:Lmt;

    .line 112
    iput-object p1, p0, Lkq;->a:Lkp;

    .line 113
    iget-object v0, p0, Lkq;->g:Lmt;

    iput-object v0, p0, Lkq;->h:Lmt;

    .line 114
    return-void
.end method


# virtual methods
.method abstract a(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
.end method

.method abstract a()Lkm;
.end method

.method abstract a(Ltz;)Lty;
.end method

.method abstract a(I)V
.end method

.method abstract a(ILandroid/view/Menu;)V
.end method

.method abstract a(Landroid/content/res/Configuration;)V
.end method

.method a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 147
    iget-object v0, p0, Lkq;->a:Lkp;

    sget-object v1, Lmd;->q:[I

    invoke-virtual {v0, v1}, Lkp;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 149
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 151
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_0
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    iput-boolean v3, p0, Lkq;->c:Z

    .line 158
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159
    iput-boolean v3, p0, Lkq;->d:Z

    .line 161
    :cond_2
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 162
    iput-boolean v3, p0, Lkq;->e:Z

    .line 164
    :cond_3
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lkq;->f:Z

    .line 165
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 166
    return-void
.end method

.method abstract a(Landroid/support/v7/widget/Toolbar;)V
.end method

.method abstract a(Landroid/view/View;)V
.end method

.method abstract a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method abstract a(Ljava/lang/CharSequence;)V
.end method

.method a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method

.method abstract a(ILandroid/view/View;Landroid/view/Menu;)Z
.end method

.method abstract a(Landroid/view/KeyEvent;)Z
.end method

.method final b()Lkm;
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lkq;->c:Z

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lkq;->b:Lkm;

    if-nez v0, :cond_0

    .line 123
    invoke-virtual {p0}, Lkq;->a()Lkm;

    move-result-object v0

    iput-object v0, p0, Lkq;->b:Lkm;

    .line 126
    :cond_0
    iget-object v0, p0, Lkq;->b:Lkm;

    return-object v0
.end method

.method abstract b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method abstract b(I)Z
.end method

.method abstract b(ILandroid/view/Menu;)Z
.end method

.method final c()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lkq;->j:Landroid/view/MenuInflater;

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Lne;

    invoke-virtual {p0}, Lkq;->i()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lne;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkq;->j:Landroid/view/MenuInflater;

    .line 143
    :cond_0
    iget-object v0, p0, Lkq;->j:Landroid/view/MenuInflater;

    return-object v0
.end method

.method abstract c(I)Landroid/view/View;
.end method

.method abstract c(ILandroid/view/Menu;)Z
.end method

.method abstract d()V
.end method

.method abstract e()V
.end method

.method abstract f()V
.end method

.method abstract g()Z
.end method

.method abstract h()I
.end method

.method protected final i()Landroid/content/Context;
    .locals 2

    .prologue
    .line 258
    const/4 v0, 0x0

    .line 261
    invoke-virtual {p0}, Lkq;->b()Lkm;

    move-result-object v1

    .line 262
    if-eqz v1, :cond_0

    .line 263
    invoke-virtual {v1}, Lkm;->b()Landroid/content/Context;

    move-result-object v0

    .line 266
    :cond_0
    if-nez v0, :cond_1

    .line 267
    iget-object v0, p0, Lkq;->a:Lkp;

    .line 269
    :cond_1
    return-object v0
.end method
