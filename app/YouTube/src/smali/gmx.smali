.class public final Lgmx;
.super Leve;
.source "SourceFile"


# static fields
.field private static b:Ljava/util/List;


# instance fields
.field private final c:Lgmy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 32
    new-instance v1, Lgmz;

    invoke-direct {v1}, Lgmz;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    new-instance v1, Lgna;

    invoke-direct {v1}, Lgna;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    new-instance v1, Lgnb;

    invoke-direct {v1}, Lgnb;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    new-instance v1, Lgnc;

    invoke-direct {v1}, Lgnc;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lgmx;->b:Ljava/util/List;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lgmy;)V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lgmx;->b:Ljava/util/List;

    invoke-direct {p0, p1, p2, v0}, Leve;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    .line 46
    iput-object p3, p0, Lgmx;->c:Lgmy;

    .line 47
    return-void
.end method


# virtual methods
.method protected final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1}, Leve;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 59
    iget-object v0, p0, Lgmx;->c:Lgmy;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lgmx;->c:Lgmy;

    invoke-interface {v0}, Lgmy;->a()V

    .line 62
    :cond_0
    return-void
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lgmx;->c:Lgmy;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lgmx;->c:Lgmy;

    invoke-interface {v0, p1}, Lgmy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 54
    :cond_0
    return-void
.end method
