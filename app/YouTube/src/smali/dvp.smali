.class final Ldvp;
.super Ldvk;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private synthetic b:Ldvb;


# direct methods
.method public constructor <init>(Ldvb;Ljava/util/Set;)V
    .locals 5

    .prologue
    .line 770
    iput-object p1, p0, Ldvp;->b:Ldvb;

    .line 771
    invoke-direct {p0, p1, p2}, Ldvk;-><init>(Ldvb;Ljava/util/Set;)V

    .line 773
    const/4 v1, 0x0

    .line 774
    iget-object v0, p1, Ldvb;->k:Lexd;

    invoke-interface {v0}, Lexd;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 775
    iget-object v0, p1, Ldvb;->d:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 776
    if-eqz v0, :cond_3

    .line 777
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 778
    if-eqz v0, :cond_3

    .line 779
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    .line 783
    :goto_0
    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    iput-object v0, p0, Ldvp;->a:Ljava/lang/String;

    .line 785
    invoke-virtual {p1}, Ldvb;->d()V

    .line 787
    iget-object v0, p1, Ldvb;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ldtj;

    .line 788
    iget-object v0, p1, Ldvb;->g:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvj;

    .line 789
    if-eqz v0, :cond_1

    .line 790
    iget-object v3, v0, Ldvj;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Ldvp;->a:Ljava/lang/String;

    iget-object v4, v0, Ldvj;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 793
    new-instance v3, Ldto;

    invoke-direct {v3}, Ldto;-><init>()V

    iput-object v1, v3, Ldto;->g:Ldtj;

    .line 795
    iget-object v1, v0, Ldvj;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v4, " (WoL)"

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ldto;->c:Ljava/lang/String;

    .line 796
    iget-object v1, v0, Ldvj;->b:Ljava/lang/String;

    iput-object v1, v3, Ldto;->h:Ljava/lang/String;

    .line 797
    iget-object v0, v0, Ldvj;->c:Ljava/lang/Integer;

    iput-object v0, v3, Ldto;->i:Ljava/lang/Integer;

    .line 798
    invoke-virtual {v3}, Ldto;->a()Ldtm;

    move-result-object v0

    invoke-super {p0, v0}, Ldvk;->a(Ldtm;)V

    goto :goto_1

    .line 801
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldtm;)V
    .locals 6

    .prologue
    .line 805
    iget-object v0, p0, Ldvp;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Ldtm;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 806
    iget-object v0, p0, Ldvp;->b:Ldvb;

    iget-object v0, v0, Ldvb;->g:Ljava/util/Map;

    iget-object v1, p1, Ldtm;->e:Ldtj;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvj;

    .line 807
    if-eqz v0, :cond_0

    iget-object v1, p0, Ldvp;->a:Ljava/lang/String;

    iget-object v2, v0, Ldvj;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 808
    :cond_0
    new-instance v2, Ldvj;

    if-nez v0, :cond_3

    .line 811
    iget-object v1, p1, Ldtm;->c:Ljava/lang/String;

    .line 813
    :goto_0
    iget-object v3, p1, Ldtm;->f:Ljava/lang/String;

    .line 814
    iget-object v4, p1, Ldtm;->g:Ljava/lang/Integer;

    iget-object v5, p0, Ldvp;->a:Ljava/lang/String;

    invoke-direct {v2, v1, v3, v4, v5}, Ldvj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 816
    iget-object v1, p0, Ldvp;->b:Ldvb;

    iget-object v1, v1, Ldvb;->g:Ljava/util/Map;

    iget-object v3, p1, Ldtm;->e:Ldtj;

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    if-nez v0, :cond_1

    .line 818
    iget-object v0, p0, Ldvp;->b:Ldvb;

    iget-object v0, v0, Ldvb;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p1, Ldtm;->e:Ldtj;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 820
    :cond_1
    iget-object v0, p0, Ldvp;->b:Ldvb;

    invoke-virtual {v0}, Ldvb;->e()V

    .line 823
    :cond_2
    iget-object v0, p0, Ldvp;->b:Ldvb;

    invoke-virtual {v0, p1}, Ldvb;->a(Ldtm;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldtm;->a(Ljava/lang/String;)Ldtm;

    move-result-object v0

    invoke-super {p0, v0}, Ldvk;->a(Ldtm;)V

    .line 824
    return-void

    .line 812
    :cond_3
    iget-object v1, v0, Ldvj;->a:Ljava/lang/String;

    goto :goto_0
.end method
