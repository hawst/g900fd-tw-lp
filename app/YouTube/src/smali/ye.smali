.class public Lye;
.super Liku;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static synthetic d:Z


# instance fields
.field private b:Lyb;

.field private c:Ljava/nio/channels/ReadableByteChannel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lye;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lye;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    const-string v0, ""

    invoke-direct {p0, v0}, Liku;-><init>(Ljava/lang/String;)V

    .line 36
    new-instance v0, Lyg;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, v1}, Lyg;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lye;->b:Lyb;

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/nio/channels/ReadableByteChannel;Lyb;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 58
    const-string v2, ""

    invoke-direct {p0, v2}, Liku;-><init>(Ljava/lang/String;)V

    .line 36
    new-instance v2, Lyg;

    new-array v3, v0, [Ljava/lang/String;

    invoke-direct {v2, v3}, Lyg;-><init>([Ljava/lang/String;)V

    iput-object v2, p0, Lye;->b:Lyb;

    .line 59
    iput-object p1, p0, Lye;->c:Ljava/nio/channels/ReadableByteChannel;

    .line 60
    iput-object p2, p0, Lye;->b:Lyb;

    .line 61
    :goto_0
    if-nez v0, :cond_1

    :try_start_0
    iget-object v2, p0, Lye;->b:Lyb;

    iget-object v3, p0, Lye;->c:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v2, v3, p0}, Lyb;->a(Ljava/nio/channels/ReadableByteChannel;Lyn;)Lyj;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lye;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 64
    :cond_1
    return-void
.end method

.method public static a([B)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 129
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    .line 130
    invoke-static {v1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 134
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string v2, "ISO-8859-1"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 135
    :catch_0
    move-exception v0

    .line 136
    new-instance v1, Ljava/lang/Error;

    const-string v2, "Required character encoding is missing"

    invoke-direct {v1, v2, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 129
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 118
    new-array v1, v3, [B

    .line 119
    if-eqz p0, :cond_0

    .line 120
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 121
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public final a(Ljava/nio/channels/WritableByteChannel;)V
    .locals 8

    .prologue
    .line 178
    iget-object v0, p0, Lye;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 180
    instance-of v1, p1, Ljava/nio/channels/FileChannel;

    if-eqz v1, :cond_1

    move-object v1, p1

    .line 181
    check-cast v1, Ljava/nio/channels/FileChannel;

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 182
    invoke-interface {v0, p1}, Lyj;->a(Ljava/nio/channels/WritableByteChannel;)V

    move-object v1, p1

    .line 183
    check-cast v1, Ljava/nio/channels/FileChannel;

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 184
    sget-boolean v1, Lye;->d:Z

    if-nez v1, :cond_0

    invoke-interface {v0}, Lyj;->l_()J

    move-result-wide v0

    cmp-long v0, v4, v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 186
    :cond_1
    invoke-interface {v0, p1}, Lyj;->a(Ljava/nio/channels/WritableByteChannel;)V

    goto :goto_0

    .line 190
    :cond_2
    return-void
.end method

.method public final b()Lye;
    .locals 0

    .prologue
    .line 157
    return-object p0
.end method

.method public final b_()Lzc;
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lye;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 170
    instance-of v2, v0, Lzc;

    if-eqz v2, :cond_0

    .line 171
    check-cast v0, Lzc;

    .line 174
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lye;->c:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V

    .line 194
    return-void
.end method

.method public final l_()J
    .locals 5

    .prologue
    .line 148
    const-wide/16 v0, 0x0

    .line 149
    iget-object v2, p0, Lye;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 150
    invoke-interface {v0}, Lyj;->l_()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 151
    goto :goto_0

    .line 152
    :cond_0
    return-wide v2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 100
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    const-string v0, "IsoFile["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    iget-object v0, p0, Lye;->a:Ljava/util/List;

    if-nez v0, :cond_1

    .line 103
    const-string v0, "unparsed"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 105
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lye;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 106
    if-lez v1, :cond_2

    .line 107
    const-string v0, ";"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    :cond_2
    iget-object v0, p0, Lye;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
