.class public final Lgjp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Lgir;

.field private final c:Lws;

.field private final d:Lgju;

.field private final e:Lezj;

.field private final f:Lght;

.field private final g:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/List;Lgir;Lws;Lgju;Lezj;Lght;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgjp;->a:Ljava/util/List;

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgir;

    iput-object v0, p0, Lgjp;->b:Lgir;

    .line 48
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    iput-object v0, p0, Lgjp;->c:Lws;

    .line 49
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgju;

    iput-object v0, p0, Lgjp;->d:Lgju;

    .line 50
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgjp;->e:Lezj;

    .line 51
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lght;

    iput-object v0, p0, Lgjp;->f:Lght;

    .line 52
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lgjp;->g:Ljava/util/concurrent/Executor;

    .line 53
    return-void
.end method

.method public static a(Ljava/lang/String;I)Lgjt;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Lgjt;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1}, Lgjt;-><init>(ILjava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public final a(Lgjt;Lwu;)V
    .locals 18

    .prologue
    .line 63
    move-object/from16 v0, p1

    iget-object v3, v0, Lgjt;->c:Landroid/net/Uri;

    .line 64
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    .line 65
    move-object/from16 v0, p0

    iget-object v2, v0, Lgjp;->g:Ljava/util/concurrent/Executor;

    new-instance v4, Lgjq;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v4, v0, v1, v3}, Lgjq;-><init>(Lgjp;Lwu;Landroid/net/Uri;)V

    invoke-interface {v2, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 105
    :goto_1
    return-void

    .line 64
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 76
    :cond_1
    new-instance v2, Lgjo;

    .line 77
    move-object/from16 v0, p1

    iget v3, v0, Lgjt;->a:I

    .line 78
    move-object/from16 v0, p1

    iget-object v4, v0, Lgjt;->c:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 79
    move-object/from16 v0, p1

    iget-boolean v5, v0, Lgjt;->d:Z

    .line 80
    move-object/from16 v0, p1

    iget-object v6, v0, Lgjt;->b:Ljava/lang/String;

    .line 81
    move-object/from16 v0, p1

    iget v7, v0, Lgjt;->f:I

    .line 82
    move-object/from16 v0, p1

    iget-wide v8, v0, Lgjt;->e:J

    .line 83
    move-object/from16 v0, p1

    iget-object v10, v0, Lgjt;->g:Ljava/util/Map;

    .line 84
    move-object/from16 v0, p1

    iget-object v11, v0, Lgjt;->h:Lgjr;

    move-object/from16 v0, p0

    iget-object v13, v0, Lgjp;->a:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lgjp;->b:Lgir;

    move-object/from16 v0, p0

    iget-object v15, v0, Lgjp;->e:Lezj;

    move-object/from16 v0, p0

    iget-object v0, v0, Lgjp;->f:Lght;

    move-object/from16 v16, v0

    .line 90
    move-object/from16 v0, p1

    iget-object v0, v0, Lgjt;->i:Lgkm;

    move-object/from16 v17, v0

    move-object/from16 v12, p2

    invoke-direct/range {v2 .. v17}, Lgjo;-><init>(ILjava/lang/String;ZLjava/lang/String;IJLjava/util/Map;Lgjr;Lwu;Ljava/util/List;Lgir;Lezj;Lght;Lgkm;)V

    .line 92
    const-string v3, "Sending from HttpPingService"

    invoke-static {v3}, Lezp;->e(Ljava/lang/String;)V

    .line 93
    move-object/from16 v0, p0

    iget-object v3, v0, Lgjp;->f:Lght;

    invoke-virtual {v3}, Lght;->i()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 94
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lgjt;->d:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lgjp;->d:Lgju;

    sget-object v4, Lgju;->a:Lgju;

    if-ne v3, v4, :cond_3

    .line 98
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lgjp;->c:Lws;

    invoke-virtual {v3, v2}, Lws;->a(Lwp;)Lwp;

    goto :goto_1

    .line 103
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lgjp;->d:Lgju;

    invoke-interface {v3, v2}, Lgju;->a(Lgjw;)V

    goto :goto_1
.end method
