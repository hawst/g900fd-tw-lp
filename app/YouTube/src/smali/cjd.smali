.class public final Lcjd;
.super Lfsb;
.source "SourceFile"


# instance fields
.field private final a:Lcyc;

.field private final b:Landroid/content/res/Resources;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Lfvi;

.field private final g:Leyo;

.field private final h:Landroid/graphics/drawable/Drawable;

.field private final i:Landroid/widget/ImageView;

.field private final j:Landroid/content/res/ColorStateList;

.field private final m:Landroid/content/res/ColorStateList;

.field private final n:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Lfhz;Landroid/app/Activity;Leyp;Landroid/view/ViewGroup;Lfdw;Lfrz;Lcyc;)V
    .locals 4

    .prologue
    .line 76
    invoke-direct {p0, p1, p5, p6}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 77
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyc;

    iput-object v0, p0, Lcjd;->a:Lcyc;

    .line 81
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcjd;->b:Landroid/content/res/Resources;

    .line 83
    invoke-virtual {p2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400ce

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcjd;->c:Landroid/view/View;

    .line 86
    iget-object v0, p0, Lcjd;->c:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcjd;->d:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcjd;->c:Landroid/view/View;

    const v1, 0x7f08011d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcjd;->e:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcjd;->c:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 89
    new-instance v1, Lfvi;

    invoke-direct {v1, p3, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcjd;->f:Lfvi;

    .line 90
    new-instance v0, Lcje;

    invoke-direct {v0, p0}, Lcje;-><init>(Lcjd;)V

    iput-object v0, p0, Lcjd;->g:Leyo;

    .line 92
    iget-object v0, p0, Lcjd;->c:Landroid/view/View;

    const v1, 0x7f0801f1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcjd;->i:Landroid/widget/ImageView;

    .line 94
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 95
    invoke-virtual {p2}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010112

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 96
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-static {p2, v0}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcjd;->h:Landroid/graphics/drawable/Drawable;

    .line 97
    iget-object v0, p0, Lcjd;->c:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v0, p0, Lcjd;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcjd;->j:Landroid/content/res/ColorStateList;

    .line 100
    iget-object v0, p0, Lcjd;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcjd;->m:Landroid/content/res/ColorStateList;

    .line 101
    iget-object v0, p0, Lcjd;->b:Landroid/content/res/Resources;

    const v1, 0x7f0700b5

    .line 103
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcjd;->n:Landroid/content/res/ColorStateList;

    .line 105
    return-void
.end method

.method private a(Lfsg;Lfpz;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 111
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 113
    invoke-virtual {p2}, Lfpz;->c()Ljava/lang/String;

    move-result-object v0

    .line 114
    iget-object v2, p0, Lcjd;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p2, Lfpz;->e:Ljava/lang/String;

    .line 117
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 118
    iget-object v2, p0, Lcjd;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcjd;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    :goto_0
    iget-object v0, p2, Lfpz;->f:Lhiq;

    if-eqz v0, :cond_8

    .line 126
    iget-object v0, p2, Lfpz;->f:Lhiq;

    iget v0, v0, Lhiq;->a:I

    .line 127
    iget-object v2, p0, Lcjd;->a:Lcyc;

    .line 128
    iget-boolean v2, p2, Lfpz;->h:Z

    if-eqz v2, :cond_3

    .line 129
    invoke-static {v0}, Lbrf;->c(I)I

    move-result v0

    .line 134
    :goto_1
    iget-object v2, p0, Lcjd;->f:Lfvi;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lfvi;->a(Landroid/graphics/drawable/Drawable;)V

    .line 138
    if-nez v0, :cond_4

    .line 139
    iget-object v0, p0, Lcjd;->f:Lfvi;

    iget-object v2, p2, Lfpz;->d:Lfnc;

    iget-object v3, p0, Lcjd;->g:Leyo;

    invoke-virtual {v0, v2, v3}, Lfvi;->a(Lfnc;Leyo;)V

    .line 140
    iget-object v0, p2, Lfpz;->d:Lfnc;

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcjd;->f:Lfvi;

    const v2, 0x7f070088

    invoke-virtual {v0, v2}, Lfvi;->b(I)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcjd;->f:Lfvi;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Lfvi;->a(Landroid/widget/ImageView$ScaleType;)V

    .line 150
    :goto_2
    iget-object v0, p0, Lcjd;->a:Lcyc;

    .line 151
    iget-boolean v0, p2, Lfpz;->h:Z

    if-eqz v0, :cond_5

    .line 152
    iget-object v0, p0, Lcjd;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcjd;->n:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 153
    iget-object v0, p0, Lcjd;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcjd;->n:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 167
    :goto_3
    iget-object v0, p2, Lfpz;->g:Lhig;

    .line 168
    if-nez v0, :cond_6

    .line 169
    iget-object v0, p0, Lcjd;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 181
    :cond_1
    :goto_4
    iget-object v0, p0, Lcjd;->c:Landroid/view/View;

    return-object v0

    .line 121
    :cond_2
    iget-object v0, p0, Lcjd;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 131
    :cond_3
    invoke-static {v0}, Lbrf;->b(I)I

    move-result v0

    goto :goto_1

    .line 145
    :cond_4
    iget-object v2, p0, Lcjd;->f:Lfvi;

    invoke-virtual {v2, v0}, Lfvi;->c(I)V

    .line 146
    iget-object v0, p0, Lcjd;->f:Lfvi;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Lfvi;->a(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_2

    .line 155
    :cond_5
    iget-object v0, p0, Lcjd;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcjd;->j:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 156
    iget-object v0, p0, Lcjd;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcjd;->m:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_3

    .line 170
    :cond_6
    iget-object v2, v0, Lhig;->a:Lhnx;

    if-eqz v2, :cond_1

    .line 171
    iget-object v2, p0, Lcjd;->i:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 172
    iget-object v0, v0, Lhig;->a:Lhnx;

    iget-boolean v0, v0, Lhnx;->a:Z

    if-eqz v0, :cond_7

    .line 173
    iget-object v0, p0, Lcjd;->i:Landroid/widget/ImageView;

    const v1, 0x7f020128

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 174
    iget-object v0, p0, Lcjd;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcjd;->b:Landroid/content/res/Resources;

    const v2, 0x7f090060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 176
    :cond_7
    iget-object v0, p0, Lcjd;->i:Landroid/widget/ImageView;

    const v1, 0x7f020127

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 177
    iget-object v0, p0, Lcjd;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcjd;->b:Landroid/content/res/Resources;

    const v2, 0x7f090061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method static synthetic a(Lcjd;)Lfvi;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcjd;->f:Lfvi;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    check-cast p2, Lfpz;

    invoke-direct {p0, p1, p2}, Lcjd;->a(Lfsg;Lfpz;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    check-cast p2, Lfpz;

    invoke-direct {p0, p1, p2}, Lcjd;->a(Lfsg;Lfpz;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    check-cast p2, Lfpz;

    invoke-direct {p0, p1, p2}, Lcjd;->a(Lfsg;Lfpz;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
