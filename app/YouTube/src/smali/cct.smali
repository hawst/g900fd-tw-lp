.class public final Lcct;
.super Lcbv;
.source "SourceFile"


# instance fields
.field private final c:Lfsj;

.field private final d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;ILfhz;Lfdw;Lfrz;)V
    .locals 7

    .prologue
    .line 39
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    move v4, p4

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcbv;-><init>(Landroid/content/Context;Leyp;Lfhz;ILfdw;Lfrz;)V

    .line 46
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcct;->c:Lfsj;

    .line 47
    iget-object v0, p0, Lcbv;->b:Landroid/view/View;

    const v1, 0x7f08028a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcct;->d:Landroid/view/View;

    .line 49
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Lfsj;->a(Z)V

    .line 50
    iget-object v0, p0, Lcbv;->b:Landroid/view/View;

    invoke-interface {p3, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 51
    invoke-interface {p3, p0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 52
    return-void
.end method

.method private a(Lfsg;Lfja;)Landroid/view/View;
    .locals 3

    .prologue
    .line 56
    invoke-super {p0, p1, p2}, Lcbv;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 58
    iget-object v0, p0, Lcct;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcbv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 59
    :cond_0
    iget-object v0, p2, Lfja;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    iget-object v0, p2, Lfja;->a:Lhds;

    iget-object v0, v0, Lhds;->e:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfja;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v0, p2, Lfja;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcct;->a(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v0, p2, Lfja;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    iget-object v0, p2, Lfja;->a:Lhds;

    iget-object v0, v0, Lhds;->b:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfja;->d:Ljava/lang/CharSequence;

    :cond_2
    iget-object v0, p2, Lfja;->d:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcct;->b(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v0, p2, Lfja;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_3

    iget-object v0, p2, Lfja;->a:Lhds;

    iget-object v0, v0, Lhds;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfja;->c:Ljava/lang/CharSequence;

    :cond_3
    iget-object v0, p2, Lfja;->c:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcct;->c(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v0, p2, Lfja;->e:Lfnc;

    if-nez v0, :cond_4

    new-instance v0, Lfnc;

    iget-object v1, p2, Lfja;->a:Lhds;

    iget-object v1, v1, Lhds;->a:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p2, Lfja;->e:Lfnc;

    :cond_4
    iget-object v0, p2, Lfja;->e:Lfnc;

    invoke-virtual {p0, v0}, Lcct;->a(Lfnc;)V

    .line 64
    iget-object v0, p0, Lcct;->c:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 26
    check-cast p2, Lfja;

    invoke-direct {p0, p1, p2}, Lcct;->a(Lfsg;Lfja;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 26
    check-cast p2, Lfja;

    invoke-direct {p0, p1, p2}, Lcct;->a(Lfsg;Lfja;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 26
    check-cast p2, Lfja;

    invoke-direct {p0, p1, p2}, Lcct;->a(Lfsg;Lfja;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
