.class public final Lcdu;
.super Lfsa;
.source "SourceFile"


# instance fields
.field final a:Leyp;

.field final b:Levn;

.field final c:Lfhz;

.field d:Lfjx;

.field private final e:Landroid/view/LayoutInflater;

.field private final f:Landroid/content/res/Resources;

.field private g:Landroid/view/ViewGroup;

.field private h:Lcdw;

.field private i:Lcdw;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Levn;Lfhz;Lfdw;Lfrz;)V
    .locals 4

    .prologue
    .line 61
    invoke-direct {p0, p5, p6}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 62
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcdu;->a:Leyp;

    .line 64
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcdu;->b:Levn;

    .line 65
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcdu;->c:Lfhz;

    .line 66
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcdu;->e:Landroid/view/LayoutInflater;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcdu;->f:Landroid/content/res/Resources;

    .line 68
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcdu;->g:Landroid/view/ViewGroup;

    .line 69
    iget-object v0, p0, Lcdu;->g:Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 71
    return-void
.end method

.method private a(Lcdw;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 95
    iget-object v0, p1, Lcdw;->b:Landroid/widget/TextView;

    iget-object v3, p0, Lcdu;->d:Lfjx;

    iget-object v4, v3, Lfjx;->b:Ljava/lang/CharSequence;

    if-nez v4, :cond_0

    iget-object v4, v3, Lfjx;->a:Lhhb;

    iget-object v4, v4, Lhhb;->a:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, v3, Lfjx;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v3, v3, Lfjx;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p1, Lcdw;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcdu;->d:Lfjx;

    iget-object v4, v3, Lfjx;->c:Ljava/lang/CharSequence;

    if-nez v4, :cond_1

    iget-object v4, v3, Lfjx;->a:Lhhb;

    iget-object v4, v4, Lhhb;->b:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, v3, Lfjx;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v3, v3, Lfjx;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcdu;->d:Lfjx;

    iget-object v0, v0, Lfjx;->a:Lhhb;

    iget-object v0, v0, Lhhb;->g:Lhut;

    if-eqz v0, :cond_4

    .line 99
    iget-object v0, p1, Lcdw;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 104
    :goto_0
    iget-object v0, p0, Lcdu;->d:Lfjx;

    iget-object v3, v0, Lfjx;->e:Lfnc;

    if-nez v3, :cond_2

    new-instance v3, Lfnc;

    iget-object v4, v0, Lfjx;->a:Lhhb;

    iget-object v4, v4, Lhhb;->f:Lhxf;

    invoke-direct {v3, v4}, Lfnc;-><init>(Lhxf;)V

    iput-object v3, v0, Lfjx;->e:Lfnc;

    :cond_2
    iget-object v3, v0, Lfjx;->e:Lfnc;

    .line 105
    iget-object v0, p1, Lcdw;->e:Landroid/widget/ImageView;

    .line 106
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvi;

    .line 107
    if-nez v0, :cond_3

    .line 108
    new-instance v0, Lfvi;

    iget-object v4, p0, Lcdu;->a:Leyp;

    iget-object v5, p1, Lcdw;->e:Landroid/widget/ImageView;

    invoke-direct {v0, v4, v5}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    .line 110
    iget-object v4, p1, Lcdw;->e:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 112
    :cond_3
    invoke-virtual {v0, v3, v6}, Lfvi;->a(Lfnc;Leyo;)V

    .line 116
    iget-object v0, p0, Lcdu;->d:Lfjx;

    invoke-virtual {v0}, Lfjx;->a()Lfit;

    move-result-object v0

    if-nez v0, :cond_5

    .line 117
    iget-object v0, p1, Lcdw;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 125
    :goto_1
    iget-object v0, p0, Lcdu;->d:Lfjx;

    iget-object v0, v0, Lfjx;->a:Lhhb;

    iget-object v0, v0, Lhhb;->e:Lhiq;

    if-eqz v0, :cond_9

    .line 126
    iget-object v0, p0, Lcdu;->d:Lfjx;

    .line 127
    iget-object v0, v0, Lfjx;->a:Lhhb;

    iget-object v0, v0, Lhhb;->e:Lhiq;

    iget v0, v0, Lhiq;->a:I

    invoke-static {v0}, Lbrf;->a(I)I

    move-result v0

    .line 129
    :goto_2
    if-eqz v0, :cond_6

    .line 130
    iget-object v1, p1, Lcdw;->f:Lfvi;

    invoke-virtual {v1, v0}, Lfvi;->b(I)V

    .line 138
    :goto_3
    iget-object v0, p0, Lcdu;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 139
    iget-object v0, p0, Lcdu;->g:Landroid/view/ViewGroup;

    iget-object v1, p1, Lcdw;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 140
    iget-object v0, p0, Lcdu;->g:Landroid/view/ViewGroup;

    return-object v0

    .line 101
    :cond_4
    iget-object v0, p1, Lcdw;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 119
    :cond_5
    iget-object v0, p1, Lcdw;->g:Landroid/widget/Button;

    iget-object v3, p0, Lcdu;->d:Lfjx;

    invoke-virtual {v3}, Lfjx;->a()Lfit;

    move-result-object v3

    invoke-virtual {v3}, Lfit;->a()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 132
    :cond_6
    iget-object v0, p0, Lcdu;->d:Lfjx;

    iget-object v3, v0, Lfjx;->d:Lfnc;

    if-nez v3, :cond_7

    new-instance v3, Lfnc;

    iget-object v4, v0, Lfjx;->a:Lhhb;

    iget-object v4, v4, Lhhb;->d:Lhxf;

    invoke-direct {v3, v4}, Lfnc;-><init>(Lhxf;)V

    iput-object v3, v0, Lfjx;->d:Lfnc;

    :cond_7
    iget-object v0, v0, Lfjx;->d:Lfnc;

    .line 133
    iget-object v3, p1, Lcdw;->f:Lfvi;

    invoke-virtual {v3, v0, v6}, Lfvi;->a(Lfnc;Leyo;)V

    .line 134
    iget-object v3, p1, Lcdw;->f:Lfvi;

    .line 135
    invoke-virtual {v0}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    .line 134
    :goto_4
    invoke-virtual {v3, v0}, Lfvi;->a(I)V

    goto :goto_3

    :cond_8
    move v0, v2

    .line 135
    goto :goto_4

    :cond_9
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Lfsg;Lfjx;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 75
    invoke-super {p0, p1, p2}, Lfsa;->a(Lfsg;Lfqh;)Landroid/view/View;

    .line 76
    iput-object p2, p0, Lcdu;->d:Lfjx;

    .line 77
    iget-object v0, p0, Lcdu;->f:Landroid/content/res/Resources;

    const v1, 0x7f0e000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcdu;->i:Lcdw;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcdw;

    iget-object v1, p0, Lcdu;->e:Landroid/view/LayoutInflater;

    const v2, 0x7f04006f

    iget-object v3, p0, Lcdu;->g:Landroid/view/ViewGroup;

    .line 81
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcdw;-><init>(Lcdu;Landroid/view/View;)V

    iput-object v0, p0, Lcdu;->i:Lcdw;

    .line 83
    :cond_0
    iget-object v0, p0, Lcdu;->i:Lcdw;

    invoke-direct {p0, v0}, Lcdu;->a(Lcdw;)Landroid/view/View;

    move-result-object v0

    .line 90
    :goto_0
    return-object v0

    .line 85
    :cond_1
    iget-object v0, p0, Lcdu;->h:Lcdw;

    if-nez v0, :cond_2

    .line 86
    new-instance v0, Lcdw;

    iget-object v1, p0, Lcdu;->e:Landroid/view/LayoutInflater;

    const v2, 0x7f040070

    iget-object v3, p0, Lcdu;->g:Landroid/view/ViewGroup;

    .line 88
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcdw;-><init>(Lcdu;Landroid/view/View;)V

    iput-object v0, p0, Lcdu;->h:Lcdw;

    .line 90
    :cond_2
    iget-object v0, p0, Lcdu;->h:Lcdw;

    invoke-direct {p0, v0}, Lcdu;->a(Lcdw;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lfjx;

    invoke-virtual {p0, p1, p2}, Lcdu;->a(Lfsg;Lfjx;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lfjx;

    invoke-virtual {p0, p1, p2}, Lcdu;->a(Lfsg;Lfjx;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
