.class public final Lfet;
.super Lfcn;
.source "SourceFile"


# instance fields
.field final e:Lfri;

.field private final f:Lezj;

.field private final g:Lfac;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lftb;Lfsz;Lgix;Lws;Lezj;Lfac;Ljava/lang/String;Lfri;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lfcn;-><init>(Lftb;Lfsz;Lgix;Lws;)V

    .line 54
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lfet;->f:Lezj;

    .line 55
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfac;

    iput-object v0, p0, Lfet;->g:Lfac;

    .line 56
    invoke-static {p7}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfet;->h:Ljava/lang/String;

    .line 57
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfri;

    iput-object v0, p0, Lfet;->e:Lfri;

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Lftj;)Lfrl;
    .locals 2

    .prologue
    .line 114
    invoke-static {}, Lb;->b()V

    .line 115
    invoke-static {}, Lgky;->a()Lgky;

    move-result-object v0

    .line 116
    invoke-virtual {p0, p1, v0}, Lfet;->a(Lftj;Lwv;)V

    .line 118
    :try_start_0
    invoke-virtual {v0}, Lgky;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrl;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    new-instance v1, Lfdv;

    invoke-direct {v1, v0}, Lfdv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 121
    :catch_1
    move-exception v0

    .line 122
    new-instance v1, Lfdv;

    invoke-direct {v1, v0}, Lfdv;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()Lftj;
    .locals 4

    .prologue
    .line 130
    new-instance v0, Lftj;

    iget-object v1, p0, Lfet;->b:Lfsz;

    iget-object v2, p0, Lfet;->c:Lgix;

    .line 131
    invoke-interface {v2}, Lgix;->d()Lgit;

    move-result-object v2

    iget-object v3, p0, Lfet;->g:Lfac;

    invoke-direct {v0, v1, v2, v3}, Lftj;-><init>(Lfsz;Lgit;Lfac;)V

    iget-object v1, p0, Lfet;->h:Ljava/lang/String;

    .line 132
    iput-object v1, v0, Lftj;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lftj;Lwv;)V
    .locals 6

    .prologue
    .line 71
    iget-object v0, p0, Lfet;->f:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v2

    .line 74
    new-instance v0, Lfeu;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lfeu;-><init>(Lfet;JLftj;Lwv;)V

    .line 96
    iget-object v1, p0, Lfet;->d:Lws;

    iget-object v2, p0, Lfet;->a:Lftb;

    const-class v3, Lhro;

    .line 97
    invoke-virtual {v2, p1, v3, v0}, Lftb;->a(Lftd;Ljava/lang/Class;Lwv;)Lfta;

    move-result-object v0

    .line 96
    invoke-virtual {v1, v0}, Lws;->a(Lwp;)Lwp;

    .line 101
    return-void
.end method
