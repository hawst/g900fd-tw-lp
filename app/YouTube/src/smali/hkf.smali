.class public final Lhkf;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhkf;


# instance fields
.field private b:Liaf;

.field private c:Lhug;

.field private d:Lhco;

.field private e:Lhue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhkf;

    sput-object v0, Lhkf;->a:[Lhkf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v0, p0, Lhkf;->b:Liaf;

    iput-object v0, p0, Lhkf;->c:Lhug;

    iput-object v0, p0, Lhkf;->d:Lhco;

    iput-object v0, p0, Lhkf;->e:Lhue;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhkf;->b:Liaf;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhkf;->b:Liaf;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhkf;->c:Lhug;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhkf;->c:Lhug;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhkf;->d:Lhco;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhkf;->d:Lhco;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhkf;->e:Lhue;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lhkf;->e:Lhue;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lhkf;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhkf;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhkf;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhkf;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhkf;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhkf;->b:Liaf;

    if-nez v0, :cond_2

    new-instance v0, Liaf;

    invoke-direct {v0}, Liaf;-><init>()V

    iput-object v0, p0, Lhkf;->b:Liaf;

    :cond_2
    iget-object v0, p0, Lhkf;->b:Liaf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhkf;->c:Lhug;

    if-nez v0, :cond_3

    new-instance v0, Lhug;

    invoke-direct {v0}, Lhug;-><init>()V

    iput-object v0, p0, Lhkf;->c:Lhug;

    :cond_3
    iget-object v0, p0, Lhkf;->c:Lhug;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhkf;->d:Lhco;

    if-nez v0, :cond_4

    new-instance v0, Lhco;

    invoke-direct {v0}, Lhco;-><init>()V

    iput-object v0, p0, Lhkf;->d:Lhco;

    :cond_4
    iget-object v0, p0, Lhkf;->d:Lhco;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhkf;->e:Lhue;

    if-nez v0, :cond_5

    new-instance v0, Lhue;

    invoke-direct {v0}, Lhue;-><init>()V

    iput-object v0, p0, Lhkf;->e:Lhue;

    :cond_5
    iget-object v0, p0, Lhkf;->e:Lhue;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget-object v0, p0, Lhkf;->b:Liaf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhkf;->b:Liaf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhkf;->c:Lhug;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhkf;->c:Lhug;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhkf;->d:Lhco;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhkf;->d:Lhco;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhkf;->e:Lhue;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lhkf;->e:Lhue;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhkf;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhkf;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhkf;

    iget-object v2, p0, Lhkf;->b:Liaf;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhkf;->b:Liaf;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhkf;->c:Lhug;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhkf;->c:Lhug;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhkf;->d:Lhco;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhkf;->d:Lhco;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhkf;->e:Lhue;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhkf;->e:Lhue;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhkf;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhkf;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhkf;->b:Liaf;

    iget-object v3, p1, Lhkf;->b:Liaf;

    invoke-virtual {v2, v3}, Liaf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhkf;->c:Lhug;

    iget-object v3, p1, Lhkf;->c:Lhug;

    invoke-virtual {v2, v3}, Lhug;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhkf;->d:Lhco;

    iget-object v3, p1, Lhkf;->d:Lhco;

    invoke-virtual {v2, v3}, Lhco;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhkf;->e:Lhue;

    iget-object v3, p1, Lhkf;->e:Lhue;

    invoke-virtual {v2, v3}, Lhue;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhkf;->I:Ljava/util/List;

    iget-object v3, p1, Lhkf;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkf;->b:Liaf;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkf;->c:Lhug;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkf;->d:Lhco;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkf;->e:Lhue;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhkf;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lhkf;->b:Liaf;

    invoke-virtual {v0}, Liaf;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhkf;->c:Lhug;

    invoke-virtual {v0}, Lhug;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhkf;->d:Lhco;

    invoke-virtual {v0}, Lhco;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhkf;->e:Lhue;

    invoke-virtual {v0}, Lhue;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lhkf;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
