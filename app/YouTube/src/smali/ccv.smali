.class public Lccv;
.super Lfsb;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:Landroid/widget/FrameLayout;

.field private c:Landroid/widget/RelativeLayout;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Lfvi;

.field private final j:Landroid/view/View;

.field private final m:Lboi;

.field private final n:Lfsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;ILfhz;Lboi;Lfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0, p5, p3, p7, p8}, Lfsb;-><init>(Lfhz;Lfsj;Lfdw;Lfrz;)V

    .line 65
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lccv;->a:Landroid/content/res/Resources;

    .line 67
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lccv;->n:Lfsj;

    .line 68
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lccv;->m:Lboi;

    .line 70
    const/4 v0, 0x0

    invoke-static {p1, p4, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    .line 71
    iget-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccv;->d:Landroid/widget/TextView;

    .line 72
    iget-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    const v1, 0x7f08012f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccv;->e:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    const v1, 0x7f080131

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccv;->f:Landroid/widget/TextView;

    .line 74
    iget-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    const v1, 0x7f080132

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccv;->g:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    const v1, 0x7f080133

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccv;->h:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 77
    new-instance v1, Lfvi;

    invoke-direct {v1, p2, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lccv;->i:Lfvi;

    .line 78
    iget-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    const v1, 0x7f08012c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lccv;->b:Landroid/widget/FrameLayout;

    .line 79
    iget-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lccv;->j:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lccv;->c:Landroid/widget/RelativeLayout;

    invoke-interface {p3, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 82
    return-void
.end method


# virtual methods
.method public final a(Lfsg;Lfjb;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 86
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 88
    invoke-virtual {p0}, Lccv;->a()V

    .line 89
    iget-object v0, p0, Lccv;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lfjb;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lccv;->f:Landroid/widget/TextView;

    iget-object v1, p2, Lfjb;->e:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    iget-object v1, p2, Lfjb;->a:Lhdt;

    iget-object v1, v1, Lhdt;->d:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjb;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v1, p2, Lfjb;->e:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lccv;->g:Landroid/widget/TextView;

    iget-object v1, p2, Lfjb;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p2, Lfjb;->a:Lhdt;

    iget-object v1, v1, Lhdt;->e:Lhgz;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lfjb;->a:Lhdt;

    iget-object v1, v1, Lhdt;->e:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjb;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfjb;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    iget-object v1, p2, Lfjb;->a:Lhdt;

    iget-object v1, v1, Lhdt;->l:Lhgz;

    if-eqz v1, :cond_6

    iget-object v1, p2, Lfjb;->a:Lhdt;

    iget-object v1, v1, Lhdt;->l:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjb;->b:Ljava/lang/CharSequence;

    :cond_2
    :goto_0
    iget-object v1, p2, Lfjb;->b:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p2, Lfjb;->c:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    iget-object v3, p2, Lfjb;->c:Ljava/lang/CharSequence;

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, " \u00b7 "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p2, Lfjb;->c:Ljava/lang/CharSequence;

    :cond_3
    :goto_1
    iget-object v1, p2, Lfjb;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lfjb;->f()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lccv;->h:Landroid/widget/TextView;

    invoke-virtual {p2}, Lfjb;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    :goto_2
    iget-object v0, p0, Lccv;->e:Landroid/widget/TextView;

    iget-object v1, p2, Lfjb;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_4

    iget-object v1, p2, Lfjb;->a:Lhdt;

    iget-object v1, v1, Lhdt;->g:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjb;->d:Ljava/lang/CharSequence;

    :cond_4
    iget-object v1, p2, Lfjb;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lccv;->i:Lfvi;

    iget-object v1, p2, Lfjb;->f:Lfnc;

    if-nez v1, :cond_5

    new-instance v1, Lfnc;

    iget-object v2, p2, Lfjb;->a:Lhdt;

    iget-object v2, v2, Lhdt;->b:Lhxf;

    invoke-direct {v1, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, p2, Lfjb;->f:Lfnc;

    :cond_5
    iget-object v1, p2, Lfjb;->f:Lfnc;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    .line 92
    iget-object v0, p0, Lccv;->j:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lccv;->n:Lfsj;

    .line 94
    invoke-interface {v0}, Lfsj;->a()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lccv;->m:Lboi;

    iget-object v2, p0, Lccv;->j:Landroid/view/View;

    .line 93
    invoke-static {v0, v1, v2, p2}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 99
    iget-object v0, p0, Lccv;->n:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 89
    :cond_6
    iget-object v1, p2, Lfjb;->a:Lhdt;

    iget-object v1, v1, Lhdt;->f:Lhgz;

    if-eqz v1, :cond_2

    iget-object v1, p2, Lfjb;->a:Lhdt;

    iget-object v1, v1, Lhdt;->f:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjb;->b:Ljava/lang/CharSequence;

    goto/16 :goto_0

    :cond_7
    iput-object v1, p2, Lfjb;->c:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lccv;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    check-cast p2, Lfjb;

    invoke-virtual {p0, p1, p2}, Lccv;->a(Lfsg;Lfjb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    check-cast p2, Lfjb;

    invoke-virtual {p0, p1, p2}, Lccv;->a(Lfsg;Lfjb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    check-cast p2, Lfjb;

    invoke-virtual {p0, p1, p2}, Lccv;->a(Lfsg;Lfjb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method
