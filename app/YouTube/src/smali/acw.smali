.class public final Lacw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lacy;

.field public final b:Lcws;

.field public c:Lada;

.field public d:Z

.field private final e:Laco;

.field private final f:Levn;

.field private final g:Lgeh;

.field private final h:Ldbr;

.field private final i:Ldbo;

.field private final j:Ldfh;

.field private final k:Ldfw;

.field private final l:Ldbv;

.field private final m:Lddd;

.field private final n:Ldfn;

.field private final o:Lacz;

.field private final p:Ldaq;

.field private final q:Ldep;

.field private final r:Landroid/os/Handler;

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lacy;Laco;Lcwz;Lgec;Ldbm;Ldff;Ldfu;Ldbr;Lddb;Ldeo;Ldfm;)V
    .locals 29

    .prologue
    .line 143
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 144
    invoke-static/range {p5 .. p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    invoke-static/range {p2 .. p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lacy;

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->a:Lacy;

    .line 146
    invoke-static/range {p3 .. p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laco;

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->e:Laco;

    .line 147
    invoke-static/range {p4 .. p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    invoke-static/range {p6 .. p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldbr;

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->h:Ldbr;

    .line 150
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    move-object/from16 v0, p3

    iget-object v2, v0, Laco;->I:Levn;

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->f:Levn;

    .line 155
    move-object/from16 v0, p3

    iget-object v3, v0, Laco;->w:Lgco;

    new-instance v2, Lgdu;

    iget-object v4, v3, Lgco;->j:Lezj;

    iget-object v5, v3, Lgco;->g:Lexd;

    iget-object v6, v3, Lgco;->k:Lges;

    iget-object v7, v3, Lgco;->f:Ljava/lang/String;

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lgdu;-><init>(Landroid/content/Context;Lezj;Lexd;Lges;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->g:Lgeh;

    .line 156
    move-object/from16 v0, p0

    iget-object v2, v0, Lacw;->g:Lgeh;

    move-object/from16 v0, p5

    invoke-interface {v2, v0}, Lgeh;->a(Lgec;)V

    .line 157
    new-instance v2, Landroid/os/Handler;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->r:Landroid/os/Handler;

    .line 158
    sget-object v2, Lada;->a:Lada;

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->c:Lada;

    .line 159
    new-instance v2, Lacz;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lacz;-><init>(Lacw;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->o:Lacz;

    .line 160
    new-instance v2, Ldaq;

    move-object/from16 v0, p3

    iget-object v3, v0, Laco;->I:Levn;

    move-object/from16 v0, p3

    iget-object v4, v0, Laco;->h:Lfet;

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->k:Lcwg;

    move-object/from16 v0, p3

    iget-object v6, v0, Laco;->L:Lcst;

    new-instance v7, Lgng;

    invoke-direct {v7}, Lgng;-><init>()V

    move-object/from16 v0, p3

    iget-object v8, v0, Laco;->f:Ljava/util/concurrent/Executor;

    new-instance v9, Lcnz;

    move-object/from16 v0, p3

    iget-object v10, v0, Laco;->n:Lcnu;

    move-object/from16 v0, p3

    iget-object v11, v0, Laco;->b:Landroid/telephony/TelephonyManager;

    move-object/from16 v0, p3

    iget-object v12, v0, Laco;->c:Landroid/content/pm/PackageManager;

    invoke-direct {v9, v10, v11, v12}, Lcnz;-><init>(Lcnu;Landroid/telephony/TelephonyManager;Landroid/content/pm/PackageManager;)V

    invoke-direct/range {v2 .. v9}, Ldaq;-><init>(Levn;Lfet;Lcwg;Lgix;Lgng;Ljava/util/concurrent/Executor;Lftg;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->p:Ldaq;

    .line 162
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->l:Lfwk;

    move-object/from16 v25, v0

    .line 163
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->t:Leyp;

    move-object/from16 v26, v0

    .line 165
    move-object/from16 v0, p3

    iget-object v2, v0, Laco;->a:Landroid/content/Context;

    invoke-static {v2}, La;->q(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lggs;->b:Lggs;

    :goto_0
    new-instance v12, Lggr;

    move-object/from16 v0, p3

    iget-object v3, v0, Laco;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ".api"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    iget-object v4, v0, Laco;->a:Landroid/content/Context;

    invoke-static {v4}, Lgxp;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lggt;->b:Lggt;

    invoke-direct {v12, v3, v4, v2, v5}, Lggr;-><init>(Ljava/lang/String;Ljava/lang/String;Lggs;Lggt;)V

    new-instance v2, Lcoq;

    move-object/from16 v0, p3

    iget-object v3, v0, Laco;->d:Lezj;

    move-object/from16 v0, p3

    iget-object v4, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->I:Levn;

    move-object/from16 v0, p3

    iget-object v6, v0, Laco;->e:Landroid/os/Handler;

    move-object/from16 v0, p3

    iget-object v7, v0, Laco;->i:Lfdo;

    move-object/from16 v0, p3

    iget-object v8, v0, Laco;->a:Landroid/content/Context;

    invoke-static {v8}, La;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcoq;-><init>(Lezj;Ljava/util/concurrent/Executor;Levn;Landroid/os/Handler;Lfdo;Ljava/lang/String;)V

    new-instance v3, Lcpb;

    move-object/from16 v0, p3

    iget-object v4, v0, Laco;->a:Landroid/content/Context;

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->d:Lezj;

    move-object/from16 v0, p3

    iget-object v6, v0, Laco;->w:Lgco;

    iget-object v6, v6, Lgco;->c:Lefj;

    move-object/from16 v0, p3

    iget-object v7, v0, Laco;->w:Lgco;

    iget-object v7, v7, Lgco;->d:Lefj;

    new-instance v8, Ldoo;

    invoke-direct {v8}, Ldoo;-><init>()V

    invoke-direct/range {v3 .. v8}, Lcpb;-><init>(Landroid/content/Context;Lezj;Lefj;Lefj;Ldoo;)V

    new-instance v4, Lcpk;

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->G:Lgjp;

    move-object/from16 v0, p3

    iget-object v6, v0, Laco;->A:Lexd;

    move-object/from16 v0, p3

    iget-object v8, v0, Laco;->I:Levn;

    move-object/from16 v0, p3

    iget-object v9, v0, Laco;->m:Lery;

    move-object/from16 v0, p3

    iget-object v10, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object v7, v12

    invoke-direct/range {v4 .. v10}, Lcpk;-><init>(Lgjp;Lexd;Lggr;Levn;Lery;Ljava/util/concurrent/Executor;)V

    new-instance v6, Lcps;

    move-object/from16 v0, p3

    iget-object v7, v0, Laco;->I:Levn;

    move-object/from16 v0, p3

    iget-object v8, v0, Laco;->F:Lgjp;

    move-object/from16 v0, p3

    iget-object v9, v0, Laco;->d:Lezj;

    move-object/from16 v0, p3

    iget-object v10, v0, Laco;->A:Lexd;

    move-object/from16 v0, p3

    iget-object v11, v0, Laco;->B:Lezi;

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->w:Lgco;

    iget-object v13, v5, Lgco;->c:Lefj;

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->w:Lgco;

    iget-object v14, v5, Lgco;->d:Lefj;

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->w:Lgco;

    iget-object v15, v5, Lgco;->l:Lggm;

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->w:Lgco;

    iget-object v0, v5, Lgco;->m:Lgfx;

    move-object/from16 v16, v0

    new-instance v17, Lacs;

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lacs;-><init>(Laco;)V

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->y:Lcyc;

    invoke-interface {v5}, Lcyc;->P()J

    move-result-wide v18

    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->x:Ldmc;

    move-object/from16 v20, v0

    invoke-direct/range {v6 .. v20}, Lcps;-><init>(Levn;Lgjp;Lezj;Lexd;Lezi;Lggr;Lefj;Lefj;Lggm;Lgfx;Lewi;JLdmc;)V

    new-instance v7, Lcqc;

    move-object/from16 v0, p3

    iget-object v8, v0, Laco;->H:Lgjp;

    move-object/from16 v0, p3

    iget-object v9, v0, Laco;->d:Lezj;

    move-object/from16 v0, p3

    iget-object v10, v0, Laco;->A:Lexd;

    move-object/from16 v0, p3

    iget-object v11, v0, Laco;->I:Levn;

    move-object/from16 v0, p3

    iget-object v13, v0, Laco;->N:Lgoc;

    invoke-direct/range {v7 .. v13}, Lcqc;-><init>(Lgjp;Lezj;Lexd;Levn;Lggr;Lgoc;)V

    new-instance v13, Lcpg;

    invoke-direct {v13}, Lcpg;-><init>()V

    new-instance v17, Ldlq;

    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->F:Lgjp;

    move-object/from16 v0, p3

    iget-object v8, v0, Laco;->I:Levn;

    move-object/from16 v0, v17

    invoke-direct {v0, v5, v8}, Ldlq;-><init>(Lgjp;Levn;)V

    new-instance v8, Lcwn;

    move-object/from16 v0, p3

    iget-object v9, v0, Laco;->r:Lcnm;

    move-object/from16 v0, p3

    iget-object v10, v0, Laco;->s:Lcpd;

    move-object v11, v2

    move-object v12, v3

    move-object v14, v4

    move-object v15, v6

    move-object/from16 v16, v7

    invoke-direct/range {v8 .. v17}, Lcwn;-><init>(Lcnm;Lcpd;Lcoq;Lcpb;Lcpg;Lcpk;Lcps;Lcqc;Ldlq;)V

    .line 166
    new-instance v27, Lcso;

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcso;-><init>(Landroid/content/Context;)V

    .line 167
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->z:Landroid/content/SharedPreferences;

    move-object/from16 v28, v0

    .line 169
    new-instance v9, Lcvz;

    .line 170
    move-object/from16 v0, p3

    iget-object v10, v0, Laco;->d:Lezj;

    move-object/from16 v0, p0

    iget-object v11, v0, Lacw;->g:Lgeh;

    move-object/from16 v0, p0

    iget-object v13, v0, Lacw;->f:Levn;

    .line 174
    move-object/from16 v0, p3

    iget-object v2, v0, Laco;->w:Lgco;

    iget-object v14, v2, Lgco;->i:Lggn;

    .line 175
    move-object/from16 v0, p3

    iget-object v15, v0, Laco;->q:Lcnq;

    .line 177
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->o:Lcwq;

    move-object/from16 v17, v0

    .line 178
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->k:Lcwg;

    move-object/from16 v18, v0

    .line 179
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->v:Lgeq;

    move-object/from16 v19, v0

    .line 180
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->J:Lfrd;

    move-object/from16 v20, v0

    .line 181
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->g:Lfac;

    move-object/from16 v21, v0

    move-object/from16 v12, p1

    move-object/from16 v16, v8

    invoke-direct/range {v9 .. v21}, Lcvz;-><init>(Lezj;Lgeh;Landroid/content/Context;Levn;Lggn;Lcnq;Lcwn;Lcwq;Lcwg;Lgeq;Lfrd;Lfac;)V

    .line 182
    new-instance v23, Lcxc;

    move-object/from16 v0, p0

    iget-object v2, v0, Lacw;->p:Ldaq;

    move-object/from16 v0, v23

    invoke-direct {v0, v9, v2}, Lcxc;-><init>(Lcvq;Ldaq;)V

    .line 183
    new-instance v9, Lcws;

    move-object/from16 v0, p0

    iget-object v11, v0, Lacw;->f:Levn;

    move-object/from16 v0, p0

    iget-object v12, v0, Lacw;->g:Lgeh;

    const/4 v13, 0x0

    .line 189
    move-object/from16 v0, p3

    iget-object v15, v0, Laco;->E:Leyt;

    .line 190
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->g:Lfac;

    move-object/from16 v16, v0

    .line 191
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->y:Lcyc;

    move-object/from16 v17, v0

    .line 192
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v18, v0

    .line 193
    invoke-static {}, Lcst;->a()Lcst;

    move-result-object v19

    .line 194
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->C:Lglm;

    move-object/from16 v20, v0

    .line 195
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->D:Lgng;

    move-object/from16 v21, v0

    .line 196
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->o:Lcwq;

    move-object/from16 v22, v0

    .line 198
    move-object/from16 v0, p3

    iget-object v0, v0, Laco;->p:Lcxi;

    move-object/from16 v24, v0

    move-object/from16 v10, p1

    move-object/from16 v14, v25

    invoke-direct/range {v9 .. v24}, Lcws;-><init>(Landroid/content/Context;Levn;Lgeh;Ldaw;Lfxe;Leyt;Lfac;Lcyc;Ljava/util/concurrent/Executor;Lgix;Lglm;Lgng;Lcwq;Lcwr;Lcxi;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lacw;->b:Lcws;

    .line 200
    move-object/from16 v0, p3

    iget-object v2, v0, Laco;->q:Lcnq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lacw;->b:Lcws;

    .line 201
    invoke-virtual {v3}, Lcws;->y()Lcvl;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lacw;->b:Lcws;

    .line 202
    invoke-virtual {v4}, Lcws;->c()Lcxl;

    move-result-object v4

    .line 203
    move-object/from16 v0, p3

    iget-object v5, v0, Laco;->n:Lcnu;

    move-object/from16 v0, p0

    iget-object v6, v0, Lacw;->f:Levn;

    .line 200
    invoke-virtual {v2, v3, v4, v5, v6}, Lcnq;->a(Lcvl;Lcxl;Lcnu;Levn;)V

    .line 206
    new-instance v9, Ldbo;

    move-object/from16 v0, p0

    iget-object v11, v0, Lacw;->f:Levn;

    move-object/from16 v0, p0

    iget-object v12, v0, Lacw;->b:Lcws;

    .line 210
    move-object/from16 v0, p3

    iget-object v13, v0, Laco;->j:Lfgk;

    move-object/from16 v10, p6

    move-object/from16 v14, v26

    move-object/from16 v15, v27

    invoke-direct/range {v9 .. v15}, Ldbo;-><init>(Ldbm;Levn;Lcws;Lfgk;Leyp;Lfhz;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lacw;->i:Ldbo;

    .line 213
    new-instance v7, Ldfh;

    move-object/from16 v0, p0

    iget-object v9, v0, Lacw;->f:Levn;

    .line 217
    move-object/from16 v0, p3

    iget-object v11, v0, Laco;->d:Lezj;

    move-object/from16 v0, p0

    iget-object v2, v0, Lacw;->b:Lcws;

    .line 218
    invoke-virtual {v2}, Lcws;->c()Lcxl;

    move-result-object v12

    move-object/from16 v10, p7

    move-object/from16 v13, p4

    invoke-direct/range {v7 .. v13}, Ldfh;-><init>(Lcwn;Levn;Ldff;Lezj;Lcxl;Lcwz;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lacw;->j:Ldfh;

    .line 220
    new-instance v2, Ldfw;

    .line 222
    move-object/from16 v0, p3

    iget-object v4, v0, Laco;->I:Levn;

    const/4 v9, 0x0

    move-object/from16 v3, p1

    move-object/from16 v5, p8

    move-object/from16 v6, v26

    move-object/from16 v7, v27

    move-object/from16 v8, p4

    invoke-direct/range {v2 .. v9}, Ldfw;-><init>(Landroid/content/Context;Levn;Ldfu;Leyp;Lfhz;Lcwz;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->k:Ldfw;

    .line 228
    new-instance v2, Ldbv;

    .line 229
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lacw;->b:Lcws;

    move-object/from16 v0, p0

    iget-object v5, v0, Lacw;->f:Levn;

    move-object/from16 v0, p9

    invoke-direct {v2, v3, v4, v5, v0}, Ldbv;-><init>(Landroid/content/res/Resources;Lcws;Levn;Ldbr;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->l:Ldbv;

    .line 233
    new-instance v2, Lddd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lacw;->b:Lcws;

    move-object/from16 v0, p10

    invoke-direct {v2, v0, v3}, Lddd;-><init>(Lddb;Lcws;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->m:Lddd;

    .line 234
    new-instance v2, Ldep;

    .line 236
    move-object/from16 v0, p3

    iget-object v4, v0, Laco;->u:Lgot;

    move-object/from16 v0, p0

    iget-object v5, v0, Lacw;->l:Ldbv;

    const/4 v7, 0x0

    .line 241
    move-object/from16 v0, p3

    iget-object v9, v0, Laco;->I:Levn;

    move-object/from16 v3, p11

    move-object/from16 v6, v28

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v9}, Ldep;-><init>(Ldeo;Lgot;Ldbv;Landroid/content/SharedPreferences;ZLandroid/content/Context;Levn;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->q:Ldep;

    .line 242
    new-instance v2, Ldei;

    move-object/from16 v0, p4

    move-object/from16 v1, p9

    invoke-direct {v2, v0, v1}, Ldei;-><init>(Lcwz;Ldbr;)V

    .line 243
    new-instance v2, Ldfn;

    const/4 v3, 0x1

    move-object/from16 v0, p12

    move-object/from16 v1, v26

    invoke-direct {v2, v0, v1, v3}, Ldfn;-><init>(Ldfm;Leyp;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lacw;->n:Ldfn;

    .line 247
    move-object/from16 v0, p0

    iget-object v2, v0, Lacw;->g:Lgeh;

    move-object/from16 v0, p0

    iget-object v3, v0, Lacw;->o:Lacz;

    invoke-interface {v2, v3}, Lgeh;->a(Landroid/os/Handler;)V

    .line 254
    invoke-direct/range {p0 .. p0}, Lacw;->n()V

    .line 255
    return-void

    .line 165
    :cond_0
    move-object/from16 v0, p3

    iget-object v2, v0, Laco;->a:Landroid/content/Context;

    invoke-static {v2}, La;->f(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lggs;->c:Lggs;

    goto/16 :goto_0

    :cond_1
    sget-object v2, Lggs;->a:Lggs;

    goto/16 :goto_0
.end method

.method private a(Lada;)V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->d:Lada;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can not leave the DESTROYED state"

    invoke-static {v0, v1}, Lb;->d(ZLjava/lang/Object;)V

    .line 259
    iput-object p1, p0, Lacw;->c:Lada;

    .line 260
    return-void

    .line 258
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handlePlaybackServiceException(Lczb;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 656
    sget-object v0, Lada;->a:Lada;

    invoke-direct {p0, v0}, Lacw;->a(Lada;)V

    .line 658
    sget-object v0, Lacx;->a:[I

    iget-object v1, p1, Lczb;->a:Lczc;

    invoke-virtual {v1}, Lczc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 679
    const-string v0, "Unhandled ErrorReason in onError"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 680
    iget-object v0, p0, Lacw;->a:Lacy;

    sget-object v1, Lgwh;->j:Lgwh;

    invoke-interface {v0, v1}, Lacy;->a(Lgwh;)V

    .line 683
    :goto_0
    return-void

    .line 662
    :pswitch_0
    iget-object v0, p0, Lacw;->a:Lacy;

    sget-object v1, Lgwh;->a:Lgwh;

    invoke-interface {v0, v1}, Lacy;->a(Lgwh;)V

    goto :goto_0

    .line 669
    :pswitch_1
    iget-object v0, p0, Lacw;->e:Laco;

    iget-object v0, v0, Laco;->A:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lacw;->a:Lacy;

    sget-object v1, Lgwh;->i:Lgwh;

    invoke-interface {v0, v1}, Lacy;->a(Lgwh;)V

    goto :goto_0

    .line 672
    :cond_0
    iget-object v0, p0, Lacw;->a:Lacy;

    sget-object v1, Lgwh;->b:Lgwh;

    invoke-interface {v0, v1}, Lacy;->a(Lgwh;)V

    goto :goto_0

    .line 676
    :pswitch_2
    iget-object v0, p0, Lacw;->a:Lacy;

    sget-object v1, Lgwh;->g:Lgwh;

    invoke-interface {v0, v1}, Lacy;->a(Lgwh;)V

    goto :goto_0

    .line 658
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleSequencerEndedEvent(Lczs;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 625
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->c()V

    .line 626
    const/4 v0, 0x0

    iput-boolean v0, p0, Lacw;->d:Z

    .line 627
    return-void
.end method

.method private handleSequencerNavigationRequestEvent(Ldam;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 692
    sget-object v0, Lacx;->b:[I

    iget-object v1, p1, Ldam;->a:Ldan;

    invoke-virtual {v1}, Ldan;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 699
    :goto_0
    return-void

    .line 695
    :pswitch_0
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->b()V

    goto :goto_0

    .line 698
    :pswitch_1
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->a()V

    goto :goto_0

    .line 692
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleSequencerStageEvent(Lczu;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 631
    iget-object v0, p1, Lczu;->a:Lgok;

    sget-object v1, Lgok;->f:Lgok;

    if-ne v0, v1, :cond_0

    .line 632
    iget-object v0, p0, Lacw;->a:Lacy;

    sget-object v1, Lgwh;->f:Lgwh;

    invoke-interface {v0, v1}, Lacy;->a(Lgwh;)V

    .line 633
    const/4 v0, 0x0

    iput-boolean v0, p0, Lacw;->d:Z

    .line 635
    :cond_0
    return-void
.end method

.method private handleVideoControlsVisibilityEvent(Ldaa;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 639
    iget-boolean v0, p1, Ldaa;->a:Z

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->j()V

    .line 644
    :goto_0
    return-void

    .line 642
    :cond_0
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->k()V

    goto :goto_0
.end method

.method private handleVideoFullscreenEvent(Ldab;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 687
    iget-object v0, p0, Lacw;->a:Lacy;

    iget-boolean v1, p1, Ldab;->a:Z

    invoke-interface {v0, v1}, Lacy;->a(Z)V

    .line 688
    return-void
.end method

.method private handleVideoStageEvent(Ldac;)V
    .locals 7
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 595
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->a:Lgol;

    if-ne v0, v1, :cond_1

    .line 596
    sget-object v0, Lada;->b:Lada;

    invoke-direct {p0, v0}, Lacw;->a(Lada;)V

    .line 597
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->d()V

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 598
    :cond_1
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->g:Lgol;

    if-ne v0, v1, :cond_2

    .line 599
    sget-object v0, Lada;->c:Lada;

    invoke-direct {p0, v0}, Lacw;->a(Lada;)V

    .line 600
    iget-object v0, p0, Lacw;->a:Lacy;

    .line 601
    iget-object v1, p1, Ldac;->b:Lfrl;

    iget-object v1, v1, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v1

    .line 602
    iget-object v2, p1, Ldac;->b:Lfrl;

    invoke-virtual {v2}, Lfrl;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lacw;->b:Lcws;

    .line 603
    invoke-virtual {v3}, Lcws;->g()I

    move-result v3

    iget-object v4, p0, Lacw;->b:Lcws;

    .line 604
    invoke-virtual {v4}, Lcws;->h()I

    move-result v4

    iget-object v5, p0, Lacw;->b:Lcws;

    .line 605
    invoke-virtual {v5}, Lcws;->p()Z

    move-result v5

    iget-object v6, p0, Lacw;->b:Lcws;

    .line 606
    invoke-virtual {v6}, Lcws;->q()Z

    move-result v6

    .line 600
    invoke-interface/range {v0 .. v6}, Lacy;->a(Ljava/lang/String;Ljava/lang/String;IIZZ)V

    goto :goto_0

    .line 607
    :cond_2
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->d:Lgol;

    if-ne v0, v1, :cond_3

    .line 608
    sget-object v0, Lada;->c:Lada;

    invoke-direct {p0, v0}, Lacw;->a(Lada;)V

    goto :goto_0

    .line 609
    :cond_3
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->e:Lgol;

    if-ne v0, v1, :cond_4

    .line 610
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->e()V

    goto :goto_0

    .line 611
    :cond_4
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->f:Lgol;

    if-ne v0, v1, :cond_5

    .line 612
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->i()V

    goto :goto_0

    .line 613
    :cond_5
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->h:Lgol;

    if-ne v0, v1, :cond_6

    .line 614
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->f()V

    goto :goto_0

    .line 615
    :cond_6
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->j:Lgol;

    if-ne v0, v1, :cond_0

    .line 616
    iget-object v0, p0, Lacw;->a:Lacy;

    invoke-interface {v0}, Lacy;->g()V

    goto :goto_0
.end method

.method private handleVideoTimeEvent(Ldad;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 648
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0}, Lcws;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 650
    iget-object v0, p0, Lacw;->a:Lacy;

    iget-wide v2, p1, Ldad;->a:J

    long-to-int v1, v2

    iget-wide v2, p1, Ldad;->b:J

    long-to-int v2, v2

    invoke-interface {v0, v1, v2}, Lacy;->b(II)V

    .line 652
    :cond_0
    return-void
.end method

.method private m()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 263
    iget-object v1, p0, Lacw;->c:Lada;

    sget-object v2, Lada;->d:Lada;

    invoke-virtual {v1, v2}, Lada;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 264
    const-string v1, "This YouTubePlayer has been released - ignoring command."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, La;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 265
    const/4 v0, 0x1

    .line 267
    :cond_0
    return v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 511
    iget-boolean v0, p0, Lacw;->s:Z

    if-nez v0, :cond_0

    .line 512
    iget-object v0, p0, Lacw;->e:Laco;

    iget-object v0, v0, Laco;->I:Levn;

    .line 513
    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 514
    iget-object v1, p0, Lacw;->e:Laco;

    iget-object v1, v1, Laco;->o:Lcwq;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 515
    iget-object v1, p0, Lacw;->i:Ldbo;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 516
    iget-object v1, p0, Lacw;->k:Ldfw;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 517
    iget-object v1, p0, Lacw;->l:Ldbv;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 518
    iget-object v1, p0, Lacw;->m:Lddd;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 519
    iget-object v1, p0, Lacw;->q:Ldep;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 520
    iget-object v1, p0, Lacw;->j:Ldfh;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 521
    iget-object v1, p0, Lacw;->n:Ldfn;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 522
    iget-object v1, p0, Lacw;->e:Laco;

    iget-object v1, v1, Laco;->x:Ldmc;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 523
    const/4 v0, 0x1

    iput-boolean v0, p0, Lacw;->s:Z

    .line 525
    :cond_0
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 528
    iget-object v0, p0, Lacw;->e:Laco;

    iget-object v0, v0, Laco;->I:Levn;

    .line 529
    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 530
    iget-object v1, p0, Lacw;->e:Laco;

    iget-object v1, v1, Laco;->o:Lcwq;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 531
    iget-object v1, p0, Lacw;->i:Ldbo;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 532
    iget-object v1, p0, Lacw;->k:Ldfw;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 533
    iget-object v1, p0, Lacw;->l:Ldbv;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 534
    iget-object v1, p0, Lacw;->m:Lddd;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 535
    iget-object v1, p0, Lacw;->q:Ldep;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 536
    iget-object v1, p0, Lacw;->j:Ldfh;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 537
    iget-object v1, p0, Lacw;->n:Ldfn;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 538
    iget-object v1, p0, Lacw;->e:Laco;

    iget-object v1, v1, Laco;->x:Ldmc;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 539
    const/4 v0, 0x0

    iput-boolean v0, p0, Lacw;->s:Z

    .line 540
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 350
    invoke-direct {p0}, Lacw;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->c:Lada;

    if-ne v0, v1, :cond_0

    .line 354
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0}, Lcws;->l()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 428
    invoke-direct {p0}, Lacw;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 431
    :cond_1
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->c:Lada;

    if-ne v0, v1, :cond_0

    .line 432
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0, p1}, Lcws;->a(I)V

    goto :goto_0
.end method

.method public final a(Ldlf;)V
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0, p1}, Lcws;->a(Ldlf;)V

    .line 552
    return-void
.end method

.method public final a(Lgoh;)V
    .locals 2

    .prologue
    .line 280
    invoke-direct {p0}, Lacw;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p1, Lgoh;->a:Leaa;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leaa;->e(Z)Leaa;

    .line 284
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0, p1}, Lcws;->a(Lgoh;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 290
    new-instance v0, Lgoh;

    .line 291
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2, p2}, Lgoh;-><init>(Ljava/util/List;II)V

    .line 294
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgoh;->b(Z)V

    .line 295
    invoke-virtual {p0, v0}, Lacw;->a(Lgoh;)V

    .line 296
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 308
    new-instance v0, Lgoh;

    const-string v1, ""

    invoke-direct {v0, v1, p1, p2, p3}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 313
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgoh;->b(Z)V

    .line 314
    invoke-virtual {p0, v0}, Lacw;->a(Lgoh;)V

    .line 315
    return-void
.end method

.method public final a(Ljava/util/List;II)V
    .locals 2

    .prologue
    .line 328
    new-instance v0, Lgoh;

    invoke-direct {v0, p1, p2, p3}, Lgoh;-><init>(Ljava/util/List;II)V

    .line 332
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgoh;->b(Z)V

    .line 333
    invoke-virtual {p0, v0}, Lacw;->a(Lgoh;)V

    .line 334
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0, p1}, Lcws;->c(Z)V

    .line 447
    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lacw;->h:Ldbr;

    invoke-interface {v0, p1, p2}, Ldbr;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 359
    invoke-direct {p0}, Lacw;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 362
    :cond_1
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->c:Lada;

    if-ne v0, v1, :cond_0

    .line 363
    iget-object v0, p0, Lacw;->f:Levn;

    sget-object v1, Ldak;->b:Ldak;

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 364
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0}, Lcws;->l()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 437
    invoke-direct {p0}, Lacw;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 440
    :cond_1
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->c:Lada;

    if-ne v0, v1, :cond_0

    .line 441
    iget-object v0, p0, Lacw;->b:Lcws;

    iget-object v0, v0, Lcws;->f:Lcvq;

    invoke-interface {v0, p1}, Lcvq;->b(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 299
    new-instance v0, Lgoh;

    .line 300
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2, p2}, Lgoh;-><init>(Ljava/util/List;II)V

    .line 303
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgoh;->c(Z)V

    .line 304
    invoke-virtual {p0, v0}, Lacw;->a(Lgoh;)V

    .line 305
    return-void
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 318
    new-instance v0, Lgoh;

    const-string v1, ""

    invoke-direct {v0, v1, p1, p2, p3}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 323
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgoh;->c(Z)V

    .line 324
    invoke-virtual {p0, v0}, Lacw;->a(Lgoh;)V

    .line 325
    return-void
.end method

.method public final b(Ljava/util/List;II)V
    .locals 2

    .prologue
    .line 337
    new-instance v0, Lgoh;

    invoke-direct {v0, p1, p2, p3}, Lgoh;-><init>(Ljava/util/List;II)V

    .line 341
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgoh;->c(Z)V

    .line 342
    invoke-virtual {p0, v0}, Lacw;->a(Lgoh;)V

    .line 343
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0, p1}, Lcws;->d(Z)V

    .line 451
    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lacw;->h:Ldbr;

    invoke-interface {v0, p1, p2}, Ldbr;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 369
    invoke-direct {p0}, Lacw;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->c:Lada;

    if-ne v0, v1, :cond_0

    .line 373
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0}, Lcws;->m()V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0, p1}, Lcws;->e(Z)V

    .line 455
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->d:Lada;

    if-eq v0, v1, :cond_0

    .line 504
    iget-object v0, p0, Lacw;->a:Lacy;

    iget-object v1, p0, Lacw;->b:Lcws;

    invoke-virtual {v1}, Lcws;->g()I

    move-result v1

    invoke-interface {v0, v1}, Lacy;->a(I)V

    .line 505
    invoke-direct {p0}, Lacw;->o()V

    .line 506
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0, p1}, Lcws;->b(Z)V

    .line 508
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->c:Lada;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->b:Lada;

    if-ne v0, v1, :cond_1

    .line 383
    :cond_0
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0}, Lcws;->q()Z

    move-result v0

    .line 385
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->c:Lada;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->b:Lada;

    if-ne v0, v1, :cond_1

    .line 390
    :cond_0
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0}, Lcws;->p()Z

    move-result v0

    .line 392
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 396
    invoke-direct {p0}, Lacw;->m()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lacw;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 397
    :cond_0
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->d:Lada;

    invoke-virtual {v0, v1}, Lada;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 398
    const-string v0, "Ignoring call to next() on YouTubePlayer as already at end of playlist."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, La;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 405
    :cond_1
    :goto_0
    return-void

    .line 402
    :cond_2
    sget-object v0, Lada;->b:Lada;

    invoke-direct {p0, v0}, Lacw;->a(Lada;)V

    .line 403
    iget-object v0, p0, Lacw;->f:Levn;

    sget-object v1, Ldak;->a:Ldak;

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 404
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0}, Lcws;->s()V

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 408
    invoke-direct {p0}, Lacw;->m()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lacw;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 409
    :cond_0
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->d:Lada;

    invoke-virtual {v0, v1}, Lada;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 410
    const-string v0, "Ignoring call to next() on YouTubePlayer as already at end of playlist."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, La;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 417
    :cond_1
    :goto_0
    return-void

    .line 414
    :cond_2
    sget-object v0, Lada;->b:Lada;

    invoke-direct {p0, v0}, Lacw;->a(Lada;)V

    .line 415
    iget-object v0, p0, Lacw;->f:Levn;

    sget-object v1, Ldak;->a:Ldak;

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 416
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0}, Lcws;->r()V

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 460
    invoke-direct {p0}, Lacw;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->c:Lada;

    if-ne v0, v1, :cond_0

    .line 464
    iget-object v0, p0, Lacw;->b:Lcws;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcws;->a(Z)V

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lacw;->h:Ldbr;

    invoke-interface {v0}, Ldbr;->d()V

    .line 474
    return-void
.end method

.method public final j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 480
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->d:Lada;

    if-eq v0, v1, :cond_3

    .line 481
    iput-boolean v4, p0, Lacw;->d:Z

    .line 482
    iget-object v0, p0, Lacw;->b:Lcws;

    iget-object v1, v0, Lcws;->g:Ldkf;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcws;->g:Ldkf;

    invoke-interface {v1}, Ldkf;->j()V

    iput-object v3, v0, Lcws;->g:Ldkf;

    :cond_0
    iget-object v1, v0, Lcws;->i:Lcvd;

    iget-boolean v2, v1, Lcvd;->b:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcvd;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v4, v1, Lcvd;->b:Z

    :cond_1
    iget-object v1, v0, Lcws;->f:Lcvq;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcvq;->b(Z)V

    iget-object v1, v0, Lcws;->b:Levn;

    iget-object v2, v0, Lcws;->d:Lcwn;

    invoke-virtual {v1, v2}, Levn;->b(Ljava/lang/Object;)V

    iget-object v1, v0, Lcws;->e:Lcxi;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcws;->b:Levn;

    iget-object v0, v0, Lcws;->e:Lcxi;

    invoke-virtual {v1, v0}, Levn;->b(Ljava/lang/Object;)V

    .line 483
    :cond_2
    invoke-direct {p0}, Lacw;->o()V

    .line 484
    iget-object v0, p0, Lacw;->g:Lgeh;

    iget-object v1, p0, Lacw;->o:Lacz;

    invoke-interface {v0, v1}, Lgeh;->b(Landroid/os/Handler;)V

    .line 485
    iget-object v0, p0, Lacw;->o:Lacz;

    invoke-virtual {v0, v3}, Lacz;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 486
    iget-object v0, p0, Lacw;->r:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 487
    iget-object v0, p0, Lacw;->q:Ldep;

    invoke-virtual {v0}, Ldep;->a()V

    .line 488
    sget-object v0, Lada;->d:Lada;

    invoke-direct {p0, v0}, Lacw;->a(Lada;)V

    .line 490
    :cond_3
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lacw;->c:Lada;

    sget-object v1, Lada;->d:Lada;

    if-eq v0, v1, :cond_0

    .line 494
    invoke-direct {p0}, Lacw;->n()V

    .line 495
    iget-object v0, p0, Lacw;->b:Lcws;

    invoke-virtual {v0}, Lcws;->t()V

    .line 497
    :cond_0
    return-void
.end method

.method public final l()Ldlf;
    .locals 2

    .prologue
    .line 555
    iget-object v0, p0, Lacw;->b:Lcws;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcws;->f(Z)Ldlf;

    move-result-object v0

    return-object v0
.end method
