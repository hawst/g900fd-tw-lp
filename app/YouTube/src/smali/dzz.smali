.class public final Ldzz;
.super Lida;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Licw;

.field private c:Z

.field private d:Z

.field private e:J

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2141
    invoke-direct {p0}, Lida;-><init>()V

    .line 2146
    const-string v0, ""

    iput-object v0, p0, Ldzz;->a:Ljava/lang/String;

    .line 2163
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldzz;->e:J

    .line 2180
    sget-object v0, Licw;->b:Licw;

    iput-object v0, p0, Ldzz;->b:Licw;

    .line 2219
    const/4 v0, -0x1

    iput v0, p0, Ldzz;->g:I

    .line 2141
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2221
    iget v0, p0, Ldzz;->g:I

    if-gez v0, :cond_0

    .line 2223
    invoke-virtual {p0}, Ldzz;->b()I

    .line 2225
    :cond_0
    iget v0, p0, Ldzz;->g:I

    return v0
.end method

.method public final a(J)Ldzz;
    .locals 1

    .prologue
    .line 2167
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzz;->d:Z

    .line 2168
    iput-wide p1, p0, Ldzz;->e:J

    .line 2169
    return-object p0
.end method

.method public final a(Licw;)Ldzz;
    .locals 1

    .prologue
    .line 2184
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzz;->f:Z

    .line 2185
    iput-object p1, p0, Ldzz;->b:Licw;

    .line 2186
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Ldzz;
    .locals 1

    .prologue
    .line 2150
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzz;->c:Z

    .line 2151
    iput-object p1, p0, Ldzz;->a:Ljava/lang/String;

    .line 2152
    return-object p0
.end method

.method public final synthetic a(Licx;)Lida;
    .locals 2

    .prologue
    .line 2139
    :cond_0
    :goto_0
    invoke-virtual {p1}, Licx;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Licx;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Licx;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldzz;->a(Ljava/lang/String;)Ldzz;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Licx;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldzz;->a(J)Ldzz;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Licx;->e()Licw;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldzz;->a(Licw;)Ldzz;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Licy;)V
    .locals 4

    .prologue
    .line 2208
    iget-boolean v0, p0, Ldzz;->c:Z

    if-eqz v0, :cond_0

    .line 2209
    const/4 v0, 0x1

    iget-object v1, p0, Ldzz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILjava/lang/String;)V

    .line 2211
    :cond_0
    iget-boolean v0, p0, Ldzz;->d:Z

    if-eqz v0, :cond_1

    .line 2212
    const/4 v0, 0x2

    iget-wide v2, p0, Ldzz;->e:J

    invoke-virtual {p1, v0, v2, v3}, Licy;->a(IJ)V

    .line 2214
    :cond_1
    iget-boolean v0, p0, Ldzz;->f:Z

    if-eqz v0, :cond_2

    .line 2215
    const/4 v0, 0x3

    iget-object v1, p0, Ldzz;->b:Licw;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILicw;)V

    .line 2217
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 2229
    const/4 v0, 0x0

    .line 2230
    iget-boolean v1, p0, Ldzz;->c:Z

    if-eqz v1, :cond_0

    .line 2231
    const/4 v0, 0x1

    .line 2232
    iget-object v1, p0, Ldzz;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Licy;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2234
    :cond_0
    iget-boolean v1, p0, Ldzz;->d:Z

    if-eqz v1, :cond_1

    .line 2235
    const/4 v1, 0x2

    .line 2236
    iget-wide v2, p0, Ldzz;->e:J

    invoke-static {v1, v2, v3}, Licy;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2238
    :cond_1
    iget-boolean v1, p0, Ldzz;->f:Z

    if-eqz v1, :cond_2

    .line 2239
    const/4 v1, 0x3

    .line 2240
    iget-object v2, p0, Ldzz;->b:Licw;

    invoke-static {v1, v2}, Licy;->b(ILicw;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2242
    :cond_2
    iput v0, p0, Ldzz;->g:I

    .line 2243
    return v0
.end method
