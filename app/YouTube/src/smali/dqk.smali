.class final Ldqk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private synthetic a:Ldqg;


# direct methods
.method constructor <init>(Ldqg;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Ldqk;->a:Ldqg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 501
    :try_start_0
    iget-object v1, p0, Ldqk;->a:Ldqg;

    invoke-static {v1}, Ldqg;->m(Ldqg;)Ldqn;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, v1, Ldqn;->e:J

    sub-long/2addr v2, v4

    sget v1, Ldqn;->a:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    .line 502
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldqk;->a:Ldqg;

    .line 503
    invoke-static {v1}, Ldqg;->m(Ldqg;)Ldqn;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget v2, Ldqn;->a:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x31

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is older than "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms. Dropping."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 502
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    iget-object v0, p0, Ldqk;->a:Ldqg;

    invoke-static {v0}, Ldqg;->n(Ldqg;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 506
    iget-object v0, p0, Ldqk;->a:Ldqg;

    iget-object v1, p0, Ldqk;->a:Ldqg;

    invoke-static {v1}, Ldqg;->m(Ldqg;)Ldqn;

    move-result-object v1

    iget-object v1, v1, Ldqn;->b:Ljava/util/List;

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Ldqg;->a(Ldqg;Ljava/util/List;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    :cond_0
    :goto_1
    iget-object v0, p0, Ldqk;->a:Ldqg;

    invoke-static {v0}, Ldqg;->g(Ldqg;)V

    .line 516
    const/4 v0, 0x0

    return-object v0

    .line 501
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 508
    :cond_2
    :try_start_1
    iget-object v0, p0, Ldqk;->a:Ldqg;

    iget-object v1, p0, Ldqk;->a:Ldqg;

    invoke-static {v1}, Ldqg;->m(Ldqg;)Ldqn;

    move-result-object v1

    iget-object v1, v1, Ldqn;->c:Ldtc;

    iget-object v2, p0, Ldqk;->a:Ldqg;

    invoke-static {v2}, Ldqg;->m(Ldqg;)Ldqn;

    move-result-object v2

    iget-object v2, v2, Ldqn;->d:Ldte;

    iget-object v3, p0, Ldqk;->a:Ldqg;

    invoke-static {v3}, Ldqg;->m(Ldqg;)Ldqn;

    move-result-object v3

    iget-object v3, v3, Ldqn;->b:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Ldqg;->a(Ldqg;Ldtc;Ldte;Ljava/util/List;)V

    .line 509
    iget-object v0, p0, Ldqk;->a:Ldqg;

    invoke-static {v0}, Ldqg;->o(Ldqg;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Ldqk;->a:Ldqg;

    invoke-static {v0}, Ldqg;->o(Ldqg;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 514
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ldqk;->a:Ldqg;

    invoke-static {v1}, Ldqg;->g(Ldqg;)V

    throw v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 497
    invoke-direct {p0}, Ldqk;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
