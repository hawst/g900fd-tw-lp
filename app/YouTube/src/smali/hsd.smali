.class public final Lhsd;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lhsf;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Lhgz;

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:Lhll;

.field public j:[Lhsj;

.field public k:Lhgz;

.field public l:I

.field public m:[B

.field public n:Lhog;

.field public o:Lhse;

.field private p:Z

.field private q:I

.field private r:Lhgz;

.field private s:Lhgz;

.field private t:[Lhbi;

.field private u:Lhgz;

.field private v:Z

.field private w:Lhnh;

.field private x:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lhsd;->a:Ljava/lang/String;

    sget-object v0, Lhsf;->a:[Lhsf;

    iput-object v0, p0, Lhsd;->b:[Lhsf;

    iput v2, p0, Lhsd;->c:I

    const-string v0, ""

    iput-object v0, p0, Lhsd;->d:Ljava/lang/String;

    iput v2, p0, Lhsd;->e:I

    iput-object v1, p0, Lhsd;->f:Lhgz;

    iput-boolean v2, p0, Lhsd;->g:Z

    iput-boolean v2, p0, Lhsd;->p:Z

    iput v2, p0, Lhsd;->q:I

    const-string v0, ""

    iput-object v0, p0, Lhsd;->h:Ljava/lang/String;

    iput-object v1, p0, Lhsd;->i:Lhll;

    sget-object v0, Lhsj;->a:[Lhsj;

    iput-object v0, p0, Lhsd;->j:[Lhsj;

    iput-object v1, p0, Lhsd;->r:Lhgz;

    iput-object v1, p0, Lhsd;->s:Lhgz;

    iput-object v1, p0, Lhsd;->k:Lhgz;

    iput v2, p0, Lhsd;->l:I

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhsd;->m:[B

    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lhsd;->t:[Lhbi;

    iput-object v1, p0, Lhsd;->u:Lhgz;

    iput-boolean v2, p0, Lhsd;->v:Z

    iput-object v1, p0, Lhsd;->n:Lhog;

    iput-object v1, p0, Lhsd;->o:Lhse;

    iput-object v1, p0, Lhsd;->w:Lhnh;

    iput v2, p0, Lhsd;->x:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lhsd;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    iget-object v2, p0, Lhsd;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhsd;->b:[Lhsf;

    if-eqz v2, :cond_1

    iget-object v3, p0, Lhsd;->b:[Lhsf;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    if-eqz v5, :cond_0

    const/4 v6, 0x2

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget v2, p0, Lhsd;->c:I

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    iget v3, p0, Lhsd;->c:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhsd;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lhsd;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lhsd;->e:I

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget v3, p0, Lhsd;->e:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhsd;->f:Lhgz;

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lhsd;->f:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-boolean v2, p0, Lhsd;->g:Z

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    iget-boolean v3, p0, Lhsd;->g:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_6
    iget-boolean v2, p0, Lhsd;->p:Z

    if-eqz v2, :cond_7

    const/16 v2, 0xb

    iget-boolean v3, p0, Lhsd;->p:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_7
    iget v2, p0, Lhsd;->q:I

    if-eqz v2, :cond_8

    const/16 v2, 0xc

    iget v3, p0, Lhsd;->q:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lhsd;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const/16 v2, 0xd

    iget-object v3, p0, Lhsd;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Lhsd;->i:Lhll;

    if-eqz v2, :cond_a

    const/16 v2, 0xe

    iget-object v3, p0, Lhsd;->i:Lhll;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Lhsd;->j:[Lhsj;

    if-eqz v2, :cond_c

    iget-object v3, p0, Lhsd;->j:[Lhsj;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_c

    aget-object v5, v3, v2

    if-eqz v5, :cond_b

    const/16 v6, 0xf

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_c
    iget-object v2, p0, Lhsd;->r:Lhgz;

    if-eqz v2, :cond_d

    const/16 v2, 0x10

    iget-object v3, p0, Lhsd;->r:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget-object v2, p0, Lhsd;->s:Lhgz;

    if-eqz v2, :cond_e

    const/16 v2, 0x11

    iget-object v3, p0, Lhsd;->s:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_e
    iget-object v2, p0, Lhsd;->k:Lhgz;

    if-eqz v2, :cond_f

    const/16 v2, 0x12

    iget-object v3, p0, Lhsd;->k:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_f
    iget v2, p0, Lhsd;->l:I

    if-eqz v2, :cond_10

    const/16 v2, 0x13

    iget v3, p0, Lhsd;->l:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_10
    iget-object v2, p0, Lhsd;->m:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_11

    const/16 v2, 0x14

    iget-object v3, p0, Lhsd;->m:[B

    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_11
    iget-object v2, p0, Lhsd;->t:[Lhbi;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lhsd;->t:[Lhbi;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_13

    aget-object v4, v2, v1

    if-eqz v4, :cond_12

    const/16 v5, 0x15

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_13
    iget-object v1, p0, Lhsd;->u:Lhgz;

    if-eqz v1, :cond_14

    const/16 v1, 0x16

    iget-object v2, p0, Lhsd;->u:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget-boolean v1, p0, Lhsd;->v:Z

    if-eqz v1, :cond_15

    const/16 v1, 0x17

    iget-boolean v2, p0, Lhsd;->v:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_15
    iget-object v1, p0, Lhsd;->n:Lhog;

    if-eqz v1, :cond_16

    const/16 v1, 0x18

    iget-object v2, p0, Lhsd;->n:Lhog;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    iget-object v1, p0, Lhsd;->o:Lhse;

    if-eqz v1, :cond_17

    const/16 v1, 0x19

    iget-object v2, p0, Lhsd;->o:Lhse;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_17
    iget-object v1, p0, Lhsd;->w:Lhnh;

    if-eqz v1, :cond_18

    const/16 v1, 0x1a

    iget-object v2, p0, Lhsd;->w:Lhnh;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_18
    iget v1, p0, Lhsd;->x:I

    if-eqz v1, :cond_19

    const/16 v1, 0x1b

    iget v2, p0, Lhsd;->x:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_19
    iget-object v1, p0, Lhsd;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhsd;->J:I

    return v0

    :cond_1a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhsd;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhsd;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhsd;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsd;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsd;->b:[Lhsf;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhsf;

    iget-object v3, p0, Lhsd;->b:[Lhsf;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhsd;->b:[Lhsf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhsd;->b:[Lhsf;

    :goto_2
    iget-object v2, p0, Lhsd;->b:[Lhsf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhsd;->b:[Lhsf;

    new-instance v3, Lhsf;

    invoke-direct {v3}, Lhsf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsd;->b:[Lhsf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhsd;->b:[Lhsf;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhsd;->b:[Lhsf;

    new-instance v3, Lhsf;

    invoke-direct {v3}, Lhsf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsd;->b:[Lhsf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhsd;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsd;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhsd;->e:I

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lhsd;->f:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsd;->f:Lhgz;

    :cond_5
    iget-object v0, p0, Lhsd;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhsd;->g:Z

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhsd;->p:Z

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_6

    if-ne v0, v5, :cond_7

    :cond_6
    iput v0, p0, Lhsd;->q:I

    goto/16 :goto_0

    :cond_7
    iput v1, p0, Lhsd;->q:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsd;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lhsd;->i:Lhll;

    if-nez v0, :cond_8

    new-instance v0, Lhll;

    invoke-direct {v0}, Lhll;-><init>()V

    iput-object v0, p0, Lhsd;->i:Lhll;

    :cond_8
    iget-object v0, p0, Lhsd;->i:Lhll;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsd;->j:[Lhsj;

    if-nez v0, :cond_a

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhsj;

    iget-object v3, p0, Lhsd;->j:[Lhsj;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lhsd;->j:[Lhsj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    iput-object v2, p0, Lhsd;->j:[Lhsj;

    :goto_4
    iget-object v2, p0, Lhsd;->j:[Lhsj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    iget-object v2, p0, Lhsd;->j:[Lhsj;

    new-instance v3, Lhsj;

    invoke-direct {v3}, Lhsj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsd;->j:[Lhsj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lhsd;->j:[Lhsj;

    array-length v0, v0

    goto :goto_3

    :cond_b
    iget-object v2, p0, Lhsd;->j:[Lhsj;

    new-instance v3, Lhsj;

    invoke-direct {v3}, Lhsj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsd;->j:[Lhsj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lhsd;->r:Lhgz;

    if-nez v0, :cond_c

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsd;->r:Lhgz;

    :cond_c
    iget-object v0, p0, Lhsd;->r:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lhsd;->s:Lhgz;

    if-nez v0, :cond_d

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsd;->s:Lhgz;

    :cond_d
    iget-object v0, p0, Lhsd;->s:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lhsd;->k:Lhgz;

    if-nez v0, :cond_e

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsd;->k:Lhgz;

    :cond_e
    iget-object v0, p0, Lhsd;->k:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_f

    if-eq v0, v4, :cond_f

    if-eq v0, v5, :cond_f

    const/4 v2, 0x3

    if-ne v0, v2, :cond_10

    :cond_f
    iput v0, p0, Lhsd;->l:I

    goto/16 :goto_0

    :cond_10
    iput v1, p0, Lhsd;->l:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhsd;->m:[B

    goto/16 :goto_0

    :sswitch_12
    const/16 v0, 0xaa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsd;->t:[Lhbi;

    if-nez v0, :cond_12

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lhsd;->t:[Lhbi;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lhsd;->t:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    iput-object v2, p0, Lhsd;->t:[Lhbi;

    :goto_6
    iget-object v2, p0, Lhsd;->t:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    iget-object v2, p0, Lhsd;->t:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsd;->t:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_12
    iget-object v0, p0, Lhsd;->t:[Lhbi;

    array-length v0, v0

    goto :goto_5

    :cond_13
    iget-object v2, p0, Lhsd;->t:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsd;->t:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Lhsd;->u:Lhgz;

    if-nez v0, :cond_14

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsd;->u:Lhgz;

    :cond_14
    iget-object v0, p0, Lhsd;->u:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhsd;->v:Z

    goto/16 :goto_0

    :sswitch_15
    iget-object v0, p0, Lhsd;->n:Lhog;

    if-nez v0, :cond_15

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhsd;->n:Lhog;

    :cond_15
    iget-object v0, p0, Lhsd;->n:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Lhsd;->o:Lhse;

    if-nez v0, :cond_16

    new-instance v0, Lhse;

    invoke-direct {v0}, Lhse;-><init>()V

    iput-object v0, p0, Lhsd;->o:Lhse;

    :cond_16
    iget-object v0, p0, Lhsd;->o:Lhse;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Lhsd;->w:Lhnh;

    if-nez v0, :cond_17

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Lhsd;->w:Lhnh;

    :cond_17
    iget-object v0, p0, Lhsd;->w:Lhnh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhsd;->x:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x58 -> :sswitch_8
        0x60 -> :sswitch_9
        0x6a -> :sswitch_a
        0x72 -> :sswitch_b
        0x7a -> :sswitch_c
        0x82 -> :sswitch_d
        0x8a -> :sswitch_e
        0x92 -> :sswitch_f
        0x98 -> :sswitch_10
        0xa2 -> :sswitch_11
        0xaa -> :sswitch_12
        0xb2 -> :sswitch_13
        0xb8 -> :sswitch_14
        0xc2 -> :sswitch_15
        0xca -> :sswitch_16
        0xd2 -> :sswitch_17
        0xd8 -> :sswitch_18
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhsd;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lhsd;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lhsd;->b:[Lhsf;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lhsd;->b:[Lhsf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    if-eqz v4, :cond_1

    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget v1, p0, Lhsd;->c:I

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    iget v2, p0, Lhsd;->c:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    :cond_3
    iget-object v1, p0, Lhsd;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lhsd;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_4
    iget v1, p0, Lhsd;->e:I

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Lhsd;->e:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    :cond_5
    iget-object v1, p0, Lhsd;->f:Lhgz;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lhsd;->f:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_6
    iget-boolean v1, p0, Lhsd;->g:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-boolean v2, p0, Lhsd;->g:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    :cond_7
    iget-boolean v1, p0, Lhsd;->p:Z

    if-eqz v1, :cond_8

    const/16 v1, 0xb

    iget-boolean v2, p0, Lhsd;->p:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    :cond_8
    iget v1, p0, Lhsd;->q:I

    if-eqz v1, :cond_9

    const/16 v1, 0xc

    iget v2, p0, Lhsd;->q:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    :cond_9
    iget-object v1, p0, Lhsd;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const/16 v1, 0xd

    iget-object v2, p0, Lhsd;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_a
    iget-object v1, p0, Lhsd;->i:Lhll;

    if-eqz v1, :cond_b

    const/16 v1, 0xe

    iget-object v2, p0, Lhsd;->i:Lhll;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_b
    iget-object v1, p0, Lhsd;->j:[Lhsj;

    if-eqz v1, :cond_d

    iget-object v2, p0, Lhsd;->j:[Lhsj;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    if-eqz v4, :cond_c

    const/16 v5, 0xf

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_d
    iget-object v1, p0, Lhsd;->r:Lhgz;

    if-eqz v1, :cond_e

    const/16 v1, 0x10

    iget-object v2, p0, Lhsd;->r:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_e
    iget-object v1, p0, Lhsd;->s:Lhgz;

    if-eqz v1, :cond_f

    const/16 v1, 0x11

    iget-object v2, p0, Lhsd;->s:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_f
    iget-object v1, p0, Lhsd;->k:Lhgz;

    if-eqz v1, :cond_10

    const/16 v1, 0x12

    iget-object v2, p0, Lhsd;->k:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_10
    iget v1, p0, Lhsd;->l:I

    if-eqz v1, :cond_11

    const/16 v1, 0x13

    iget v2, p0, Lhsd;->l:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    :cond_11
    iget-object v1, p0, Lhsd;->m:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_12

    const/16 v1, 0x14

    iget-object v2, p0, Lhsd;->m:[B

    invoke-virtual {p1, v1, v2}, Lidd;->a(I[B)V

    :cond_12
    iget-object v1, p0, Lhsd;->t:[Lhbi;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lhsd;->t:[Lhbi;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_14

    aget-object v3, v1, v0

    if-eqz v3, :cond_13

    const/16 v4, 0x15

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_14
    iget-object v0, p0, Lhsd;->u:Lhgz;

    if-eqz v0, :cond_15

    const/16 v0, 0x16

    iget-object v1, p0, Lhsd;->u:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_15
    iget-boolean v0, p0, Lhsd;->v:Z

    if-eqz v0, :cond_16

    const/16 v0, 0x17

    iget-boolean v1, p0, Lhsd;->v:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_16
    iget-object v0, p0, Lhsd;->n:Lhog;

    if-eqz v0, :cond_17

    const/16 v0, 0x18

    iget-object v1, p0, Lhsd;->n:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_17
    iget-object v0, p0, Lhsd;->o:Lhse;

    if-eqz v0, :cond_18

    const/16 v0, 0x19

    iget-object v1, p0, Lhsd;->o:Lhse;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_18
    iget-object v0, p0, Lhsd;->w:Lhnh;

    if-eqz v0, :cond_19

    const/16 v0, 0x1a

    iget-object v1, p0, Lhsd;->w:Lhnh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_19
    iget v0, p0, Lhsd;->x:I

    if-eqz v0, :cond_1a

    const/16 v0, 0x1b

    iget v1, p0, Lhsd;->x:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_1a
    iget-object v0, p0, Lhsd;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhsd;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhsd;

    iget-object v2, p0, Lhsd;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhsd;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhsd;->b:[Lhsf;

    iget-object v3, p1, Lhsd;->b:[Lhsf;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lhsd;->c:I

    iget v3, p1, Lhsd;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsd;->d:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhsd;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget v2, p0, Lhsd;->e:I

    iget v3, p1, Lhsd;->e:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsd;->f:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhsd;->f:Lhgz;

    if-nez v2, :cond_3

    :goto_3
    iget-boolean v2, p0, Lhsd;->g:Z

    iget-boolean v3, p1, Lhsd;->g:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhsd;->p:Z

    iget-boolean v3, p1, Lhsd;->p:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhsd;->q:I

    iget v3, p1, Lhsd;->q:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsd;->h:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhsd;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhsd;->i:Lhll;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhsd;->i:Lhll;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhsd;->j:[Lhsj;

    iget-object v3, p1, Lhsd;->j:[Lhsj;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsd;->r:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhsd;->r:Lhgz;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhsd;->s:Lhgz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhsd;->s:Lhgz;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Lhsd;->k:Lhgz;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhsd;->k:Lhgz;

    if-nez v2, :cond_3

    :goto_8
    iget v2, p0, Lhsd;->l:I

    iget v3, p1, Lhsd;->l:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsd;->m:[B

    iget-object v3, p1, Lhsd;->m:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsd;->t:[Lhbi;

    iget-object v3, p1, Lhsd;->t:[Lhbi;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsd;->u:Lhgz;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhsd;->u:Lhgz;

    if-nez v2, :cond_3

    :goto_9
    iget-boolean v2, p0, Lhsd;->v:Z

    iget-boolean v3, p1, Lhsd;->v:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsd;->n:Lhog;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhsd;->n:Lhog;

    if-nez v2, :cond_3

    :goto_a
    iget-object v2, p0, Lhsd;->o:Lhse;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhsd;->o:Lhse;

    if-nez v2, :cond_3

    :goto_b
    iget-object v2, p0, Lhsd;->w:Lhnh;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhsd;->w:Lhnh;

    if-nez v2, :cond_3

    :goto_c
    iget v2, p0, Lhsd;->x:I

    iget v3, p1, Lhsd;->x:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsd;->I:Ljava/util/List;

    if-nez v2, :cond_10

    iget-object v2, p1, Lhsd;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhsd;->a:Ljava/lang/String;

    iget-object v3, p1, Lhsd;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhsd;->d:Ljava/lang/String;

    iget-object v3, p1, Lhsd;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhsd;->f:Lhgz;

    iget-object v3, p1, Lhsd;->f:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhsd;->h:Ljava/lang/String;

    iget-object v3, p1, Lhsd;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhsd;->i:Lhll;

    iget-object v3, p1, Lhsd;->i:Lhll;

    invoke-virtual {v2, v3}, Lhll;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhsd;->r:Lhgz;

    iget-object v3, p1, Lhsd;->r:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhsd;->s:Lhgz;

    iget-object v3, p1, Lhsd;->s:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhsd;->k:Lhgz;

    iget-object v3, p1, Lhsd;->k:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhsd;->u:Lhgz;

    iget-object v3, p1, Lhsd;->u:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhsd;->n:Lhog;

    iget-object v3, p1, Lhsd;->n:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v2, p0, Lhsd;->o:Lhse;

    iget-object v3, p1, Lhsd;->o:Lhse;

    invoke-virtual {v2, v3}, Lhse;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v2, p0, Lhsd;->w:Lhnh;

    iget-object v3, p1, Lhsd;->w:Lhnh;

    invoke-virtual {v2, v3}, Lhnh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v2, p0, Lhsd;->I:Ljava/util/List;

    iget-object v3, p1, Lhsd;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->a:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsd;->b:[Lhsf;

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget v2, p0, Lhsd;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->d:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhsd;->e:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->f:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhsd;->g:Z

    if-eqz v0, :cond_9

    move v0, v3

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhsd;->p:Z

    if-eqz v0, :cond_a

    move v0, v3

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhsd;->q:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->h:Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->i:Lhll;

    if-nez v0, :cond_c

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsd;->j:[Lhsj;

    if-nez v2, :cond_d

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsd;->r:Lhgz;

    if-nez v0, :cond_f

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->s:Lhgz;

    if-nez v0, :cond_10

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->k:Lhgz;

    if-nez v0, :cond_11

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhsd;->l:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lhsd;->m:[B

    if-nez v2, :cond_12

    mul-int/lit8 v2, v0, 0x1f

    :cond_2
    iget-object v0, p0, Lhsd;->t:[Lhbi;

    if-nez v0, :cond_13

    mul-int/lit8 v2, v2, 0x1f

    :cond_3
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsd;->u:Lhgz;

    if-nez v0, :cond_15

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lhsd;->v:Z

    if-eqz v2, :cond_16

    :goto_b
    add-int/2addr v0, v3

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->n:Lhog;

    if-nez v0, :cond_17

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->o:Lhse;

    if-nez v0, :cond_18

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsd;->w:Lhnh;

    if-nez v0, :cond_19

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhsd;->x:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhsd;->I:Ljava/util/List;

    if-nez v2, :cond_1a

    :goto_f
    add-int/2addr v0, v1

    return v0

    :cond_4
    iget-object v0, p0, Lhsd;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_5
    move v2, v0

    move v0, v1

    :goto_10
    iget-object v5, p0, Lhsd;->b:[Lhsf;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhsd;->b:[Lhsf;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    move v2, v1

    :goto_11
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_6
    iget-object v2, p0, Lhsd;->b:[Lhsf;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhsf;->hashCode()I

    move-result v2

    goto :goto_11

    :cond_7
    iget-object v0, p0, Lhsd;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lhsd;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_9
    move v0, v4

    goto/16 :goto_3

    :cond_a
    move v0, v4

    goto/16 :goto_4

    :cond_b
    iget-object v0, p0, Lhsd;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_c
    iget-object v0, p0, Lhsd;->i:Lhll;

    invoke-virtual {v0}, Lhll;->hashCode()I

    move-result v0

    goto/16 :goto_6

    :cond_d
    move v2, v0

    move v0, v1

    :goto_12
    iget-object v5, p0, Lhsd;->j:[Lhsj;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhsd;->j:[Lhsj;

    aget-object v2, v2, v0

    if-nez v2, :cond_e

    move v2, v1

    :goto_13
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    :cond_e
    iget-object v2, p0, Lhsd;->j:[Lhsj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhsj;->hashCode()I

    move-result v2

    goto :goto_13

    :cond_f
    iget-object v0, p0, Lhsd;->r:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_7

    :cond_10
    iget-object v0, p0, Lhsd;->s:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_8

    :cond_11
    iget-object v0, p0, Lhsd;->k:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_12
    move v2, v0

    move v0, v1

    :goto_14
    iget-object v5, p0, Lhsd;->m:[B

    array-length v5, v5

    if-ge v0, v5, :cond_2

    mul-int/lit8 v2, v2, 0x1f

    iget-object v5, p0, Lhsd;->m:[B

    aget-byte v5, v5, v0

    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    :cond_13
    move v0, v1

    :goto_15
    iget-object v5, p0, Lhsd;->t:[Lhbi;

    array-length v5, v5

    if-ge v0, v5, :cond_3

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhsd;->t:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_14

    move v2, v1

    :goto_16
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    :cond_14
    iget-object v2, p0, Lhsd;->t:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_16

    :cond_15
    iget-object v0, p0, Lhsd;->u:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_a

    :cond_16
    move v3, v4

    goto/16 :goto_b

    :cond_17
    iget-object v0, p0, Lhsd;->n:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_c

    :cond_18
    iget-object v0, p0, Lhsd;->o:Lhse;

    invoke-virtual {v0}, Lhse;->hashCode()I

    move-result v0

    goto/16 :goto_d

    :cond_19
    iget-object v0, p0, Lhsd;->w:Lhnh;

    invoke-virtual {v0}, Lhnh;->hashCode()I

    move-result v0

    goto/16 :goto_e

    :cond_1a
    iget-object v1, p0, Lhsd;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_f
.end method
