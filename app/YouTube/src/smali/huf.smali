.class public final Lhuf;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[B

.field public c:Ljava/lang/String;

.field private d:[[B

.field private e:[Liaj;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lidf;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lhuf;->a:I

    sget-object v0, Lidj;->e:[[B

    iput-object v0, p0, Lhuf;->d:[[B

    sget-object v0, Liaj;->a:[Liaj;

    iput-object v0, p0, Lhuf;->e:[Liaj;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhuf;->b:[B

    const-string v0, ""

    iput-object v0, p0, Lhuf;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget v0, p0, Lhuf;->a:I

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    iget v2, p0, Lhuf;->a:I

    invoke-static {v0, v2}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhuf;->d:[[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhuf;->d:[[B

    array-length v2, v2

    if-lez v2, :cond_1

    iget-object v4, p0, Lhuf;->d:[[B

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    invoke-static {v6}, Lidd;->a([B)I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/2addr v0, v3

    iget-object v2, p0, Lhuf;->d:[[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhuf;->e:[Liaj;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhuf;->e:[Liaj;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    if-eqz v4, :cond_2

    const/4 v5, 0x3

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lhuf;->b:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x4

    iget-object v2, p0, Lhuf;->b:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhuf;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lhuf;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhuf;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhuf;->J:I

    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhuf;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhuf;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhuf;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhuf;->a:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhuf;->d:[[B

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [[B

    iget-object v3, p0, Lhuf;->d:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lhuf;->d:[[B

    :goto_1
    iget-object v2, p0, Lhuf;->d:[[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lhuf;->d:[[B

    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lhuf;->d:[[B

    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhuf;->e:[Liaj;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Liaj;

    iget-object v3, p0, Lhuf;->e:[Liaj;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhuf;->e:[Liaj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhuf;->e:[Liaj;

    :goto_3
    iget-object v2, p0, Lhuf;->e:[Liaj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhuf;->e:[Liaj;

    new-instance v3, Liaj;

    invoke-direct {v3}, Liaj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhuf;->e:[Liaj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lhuf;->e:[Liaj;

    array-length v0, v0

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lhuf;->e:[Liaj;

    new-instance v3, Liaj;

    invoke-direct {v3}, Liaj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhuf;->e:[Liaj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhuf;->b:[B

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhuf;->c:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget v1, p0, Lhuf;->a:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Lhuf;->a:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    :cond_0
    iget-object v1, p0, Lhuf;->d:[[B

    if-eqz v1, :cond_1

    iget-object v2, p0, Lhuf;->d:[[B

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lidd;->a(I[B)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lhuf;->e:[Liaj;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhuf;->e:[Liaj;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhuf;->b:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x4

    iget-object v1, p0, Lhuf;->b:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_4
    iget-object v0, p0, Lhuf;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x5

    iget-object v1, p0, Lhuf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lhuf;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhuf;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhuf;

    iget v2, p0, Lhuf;->a:I

    iget v3, p1, Lhuf;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhuf;->d:[[B

    iget-object v3, p1, Lhuf;->d:[[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhuf;->e:[Liaj;

    iget-object v3, p1, Lhuf;->e:[Liaj;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhuf;->b:[B

    iget-object v3, p1, Lhuf;->b:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhuf;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhuf;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhuf;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhuf;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhuf;->c:Ljava/lang/String;

    iget-object v3, p1, Lhuf;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhuf;->I:Ljava/util/List;

    iget-object v3, p1, Lhuf;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhuf;->a:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lhuf;->d:[[B

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lhuf;->e:[Liaj;

    if-nez v0, :cond_5

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    iget-object v0, p0, Lhuf;->b:[B

    if-nez v0, :cond_7

    mul-int/lit8 v2, v2, 0x1f

    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhuf;->c:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhuf;->I:Ljava/util/List;

    if-nez v2, :cond_9

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_3
    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Lhuf;->d:[[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    move v3, v1

    :goto_3
    iget-object v4, p0, Lhuf;->d:[[B

    aget-object v4, v4, v0

    array-length v4, v4

    if-ge v3, v4, :cond_4

    mul-int/lit8 v2, v2, 0x1f

    iget-object v4, p0, Lhuf;->d:[[B

    aget-object v4, v4, v0

    aget-byte v4, v4, v3

    add-int/2addr v4, v2

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_3

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    :goto_4
    iget-object v3, p0, Lhuf;->e:[Liaj;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhuf;->e:[Liaj;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v2, p0, Lhuf;->e:[Liaj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Liaj;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    move v0, v1

    :goto_6
    iget-object v3, p0, Lhuf;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhuf;->b:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lhuf;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_9
    iget-object v1, p0, Lhuf;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
