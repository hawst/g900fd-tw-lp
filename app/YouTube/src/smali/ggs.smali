.class public final enum Lggs;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lggs;

.field public static final enum b:Lggs;

.field public static final enum c:Lggs;

.field private static enum e:Lggs;

.field private static enum f:Lggs;

.field private static enum g:Lggs;

.field private static enum h:Lggs;

.field private static enum i:Lggs;

.field private static final synthetic j:[Lggs;


# instance fields
.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Lggs;

    const-string v1, "DESKTOP"

    const-string v2, "desktop"

    invoke-direct {v0, v1, v4, v2}, Lggs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggs;->e:Lggs;

    .line 19
    new-instance v0, Lggs;

    const-string v1, "MOBILE"

    const-string v2, "mobile"

    invoke-direct {v0, v1, v5, v2}, Lggs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggs;->a:Lggs;

    .line 20
    new-instance v0, Lggs;

    const-string v1, "TV"

    const-string v2, "tv"

    invoke-direct {v0, v1, v6, v2}, Lggs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggs;->b:Lggs;

    .line 21
    new-instance v0, Lggs;

    const-string v1, "TABLET"

    const-string v2, "tablet"

    invoke-direct {v0, v1, v7, v2}, Lggs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggs;->c:Lggs;

    .line 22
    new-instance v0, Lggs;

    const-string v1, "BLURAY"

    const-string v2, "bluray"

    invoke-direct {v0, v1, v8, v2}, Lggs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggs;->f:Lggs;

    .line 23
    new-instance v0, Lggs;

    const-string v1, "STB"

    const/4 v2, 0x5

    const-string v3, "stb"

    invoke-direct {v0, v1, v2, v3}, Lggs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggs;->g:Lggs;

    .line 24
    new-instance v0, Lggs;

    const-string v1, "GAME_CONSOLE"

    const/4 v2, 0x6

    const-string v3, "game_console"

    invoke-direct {v0, v1, v2, v3}, Lggs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggs;->h:Lggs;

    .line 25
    new-instance v0, Lggs;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x7

    const-string v3, "unknown_platform"

    invoke-direct {v0, v1, v2, v3}, Lggs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lggs;->i:Lggs;

    .line 17
    const/16 v0, 0x8

    new-array v0, v0, [Lggs;

    sget-object v1, Lggs;->e:Lggs;

    aput-object v1, v0, v4

    sget-object v1, Lggs;->a:Lggs;

    aput-object v1, v0, v5

    sget-object v1, Lggs;->b:Lggs;

    aput-object v1, v0, v6

    sget-object v1, Lggs;->c:Lggs;

    aput-object v1, v0, v7

    sget-object v1, Lggs;->f:Lggs;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lggs;->g:Lggs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lggs;->h:Lggs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lggs;->i:Lggs;

    aput-object v2, v0, v1

    sput-object v0, Lggs;->j:[Lggs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput-object p3, p0, Lggs;->d:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lggs;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lggs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lggs;

    return-object v0
.end method

.method public static values()[Lggs;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lggs;->j:[Lggs;

    invoke-virtual {v0}, [Lggs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lggs;

    return-object v0
.end method
