.class public Ldrb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field public final b:Ldq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Ldrb;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldrb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ldq;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ldq;-><init>(I)V

    iput-object v0, p0, Ldrb;->b:Ldq;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ldth;)Ldst;
    .locals 3

    .prologue
    .line 68
    iget-object v2, p0, Ldrb;->b:Ldq;

    monitor-enter v2

    .line 69
    :try_start_0
    iget-object v0, p0, Ldrb;->b:Ldq;

    invoke-virtual {v0, p1}, Ldq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 70
    if-nez v0, :cond_0

    .line 71
    sget-object v0, Ldrb;->a:Ljava/lang/String;

    const-string v1, "Cache is not initialized. Remove operation has been refused."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v0, 0x0

    monitor-exit v2

    .line 78
    :goto_0
    return-object v0

    .line 74
    :cond_0
    invoke-static {v0, p2}, La;->a(Ljava/util/List;Ldth;)Ldst;

    move-result-object v1

    .line 75
    if-eqz v1, :cond_1

    .line 76
    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 78
    :cond_1
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ldth;Ljava/lang/String;)Ldst;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-object v2, p0, Ldrb;->b:Ldq;

    monitor-enter v2

    .line 50
    :try_start_0
    iget-object v0, p0, Ldrb;->b:Ldq;

    invoke-virtual {v0, p1}, Ldq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 51
    if-nez v0, :cond_0

    .line 52
    sget-object v0, Ldrb;->a:Ljava/lang/String;

    const-string v3, "Cache is not initialized. Update operation has been refused."

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    monitor-exit v2

    move-object v0, v1

    .line 63
    :goto_0
    return-object v0

    .line 55
    :cond_0
    invoke-static {v0, p2}, La;->a(Ljava/util/List;Ldth;)Ldst;

    move-result-object v3

    .line 57
    invoke-interface {v0, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 58
    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 59
    invoke-interface {v0, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 60
    invoke-virtual {v3, p3}, Ldst;->a(Ljava/lang/String;)Ldst;

    move-result-object v1

    .line 61
    invoke-interface {v0, v4, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move-object v0, v1

    .line 63
    :goto_1
    monitor-exit v2

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Ldrb;->b:Ldq;

    invoke-virtual {v0, p1}, Ldq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
