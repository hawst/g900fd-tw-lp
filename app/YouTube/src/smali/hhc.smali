.class public final Lhhc;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhjx;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56311
    invoke-direct {p0}, Lidf;-><init>()V

    .line 56314
    const/4 v0, 0x0

    iput-object v0, p0, Lhhc;->a:Lhjx;

    .line 56317
    const-string v0, ""

    iput-object v0, p0, Lhhc;->b:Ljava/lang/String;

    .line 56320
    const-string v0, ""

    iput-object v0, p0, Lhhc;->c:Ljava/lang/String;

    .line 56311
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 56369
    const/4 v0, 0x0

    .line 56370
    iget-object v1, p0, Lhhc;->a:Lhjx;

    if-eqz v1, :cond_0

    .line 56371
    const/4 v0, 0x1

    iget-object v1, p0, Lhhc;->a:Lhjx;

    .line 56372
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 56374
    :cond_0
    iget-object v1, p0, Lhhc;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 56375
    const/4 v1, 0x2

    iget-object v2, p0, Lhhc;->b:Ljava/lang/String;

    .line 56376
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56378
    :cond_1
    iget-object v1, p0, Lhhc;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 56379
    const/4 v1, 0x3

    iget-object v2, p0, Lhhc;->c:Ljava/lang/String;

    .line 56380
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56382
    :cond_2
    iget-object v1, p0, Lhhc;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56383
    iput v0, p0, Lhhc;->J:I

    .line 56384
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 56307
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhhc;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhhc;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhhc;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhhc;->a:Lhjx;

    if-nez v0, :cond_2

    new-instance v0, Lhjx;

    invoke-direct {v0}, Lhjx;-><init>()V

    iput-object v0, p0, Lhhc;->a:Lhjx;

    :cond_2
    iget-object v0, p0, Lhhc;->a:Lhjx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhhc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhhc;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 56354
    iget-object v0, p0, Lhhc;->a:Lhjx;

    if-eqz v0, :cond_0

    .line 56355
    const/4 v0, 0x1

    iget-object v1, p0, Lhhc;->a:Lhjx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 56357
    :cond_0
    iget-object v0, p0, Lhhc;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 56358
    const/4 v0, 0x2

    iget-object v1, p0, Lhhc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 56360
    :cond_1
    iget-object v0, p0, Lhhc;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 56361
    const/4 v0, 0x3

    iget-object v1, p0, Lhhc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 56363
    :cond_2
    iget-object v0, p0, Lhhc;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 56365
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56333
    if-ne p1, p0, :cond_1

    .line 56339
    :cond_0
    :goto_0
    return v0

    .line 56334
    :cond_1
    instance-of v2, p1, Lhhc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 56335
    :cond_2
    check-cast p1, Lhhc;

    .line 56336
    iget-object v2, p0, Lhhc;->a:Lhjx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhhc;->a:Lhjx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhhc;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhhc;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 56337
    :goto_2
    iget-object v2, p0, Lhhc;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhhc;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 56338
    :goto_3
    iget-object v2, p0, Lhhc;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhhc;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 56339
    goto :goto_0

    .line 56336
    :cond_4
    iget-object v2, p0, Lhhc;->a:Lhjx;

    iget-object v3, p1, Lhhc;->a:Lhjx;

    invoke-virtual {v2, v3}, Lhjx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhhc;->b:Ljava/lang/String;

    iget-object v3, p1, Lhhc;->b:Ljava/lang/String;

    .line 56337
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhhc;->c:Ljava/lang/String;

    iget-object v3, p1, Lhhc;->c:Ljava/lang/String;

    .line 56338
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhhc;->I:Ljava/util/List;

    iget-object v3, p1, Lhhc;->I:Ljava/util/List;

    .line 56339
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 56343
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 56345
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhc;->a:Lhjx;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 56346
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhc;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 56347
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhc;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 56348
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhhc;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 56349
    return v0

    .line 56345
    :cond_0
    iget-object v0, p0, Lhhc;->a:Lhjx;

    invoke-virtual {v0}, Lhjx;->hashCode()I

    move-result v0

    goto :goto_0

    .line 56346
    :cond_1
    iget-object v0, p0, Lhhc;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 56347
    :cond_2
    iget-object v0, p0, Lhhc;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 56348
    :cond_3
    iget-object v1, p0, Lhhc;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
