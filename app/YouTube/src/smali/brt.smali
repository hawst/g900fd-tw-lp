.class public final Lbrt;
.super Lbrw;
.source "SourceFile"


# instance fields
.field public a:Lfve;

.field private b:Landroid/widget/Button;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;III)V
    .locals 6

    .prologue
    .line 289
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lbrw;-><init>(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;IIIB)V

    .line 291
    return-void
.end method


# virtual methods
.method protected final a()Landroid/view/View;
    .locals 3

    .prologue
    .line 295
    invoke-super {p0}, Lbrw;->a()Landroid/view/View;

    move-result-object v1

    .line 296
    const v0, 0x7f080228

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbrt;->b:Landroid/widget/Button;

    .line 297
    iget-object v0, p0, Lbrt;->b:Landroid/widget/Button;

    new-instance v2, Lbru;

    invoke-direct {v2, p0}, Lbru;-><init>(Lbrt;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    iget-boolean v0, p0, Lbrt;->c:Z

    invoke-virtual {p0, v0}, Lbrt;->a(Z)V

    .line 306
    return-object v1
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lbrt;->b:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 315
    iget-object v1, p0, Lbrt;->b:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 319
    :goto_1
    return-void

    .line 315
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 317
    :cond_1
    iput-boolean p1, p0, Lbrt;->c:Z

    goto :goto_1
.end method
