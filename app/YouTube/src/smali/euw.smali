.class public final Leuw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuk;


# instance fields
.field private final a:Leuk;

.field private final b:Leuk;

.field private final c:Leux;


# direct methods
.method public constructor <init>(Leuk;Leuk;Leux;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leuk;

    iput-object v0, p0, Leuw;->a:Leuk;

    .line 56
    const-string v0, "singleElementsCache may not be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leuk;

    iput-object v0, p0, Leuw;->b:Leuk;

    .line 58
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leux;

    iput-object v0, p0, Leuw;->c:Leux;

    .line 59
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Leuw;->a:Leuk;

    invoke-interface {v0, p1}, Leuk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Leuw;->a:Leuk;

    invoke-interface {v0}, Leuk;->a()V

    .line 80
    return-void
.end method

.method public final a(Lewh;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Leuw;->a:Leuk;

    invoke-interface {v0, p1}, Leuk;->a(Lewh;)V

    .line 90
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Leuw;->c:Leux;

    iget-object v1, p0, Leuw;->b:Leuk;

    invoke-interface {v0, p1, p2, v1}, Leux;->a(Ljava/lang/Object;Ljava/lang/Object;Leuk;)V

    .line 69
    iget-object v0, p0, Leuw;->a:Leuk;

    invoke-interface {v0, p1, p2}, Leuk;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 70
    return-void
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Leuw;->a:Leuk;

    invoke-interface {v0, p1}, Leuk;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
