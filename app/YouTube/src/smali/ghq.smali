.class public final enum Lghq;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static enum b:Lghq;

.field private static enum c:Lghq;

.field private static enum d:Lghq;

.field private static final synthetic e:[Lghq;


# instance fields
.field final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lghq;

    const-string v1, "V1"

    const-string v2, "youtubei/v1"

    invoke-direct {v0, v1, v3, v2}, Lghq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lghq;->b:Lghq;

    .line 21
    new-instance v0, Lghq;

    const-string v1, "V1RELEASE_ONLY_WORKS_IN_STAGING"

    const-string v2, "youtubei/v1release"

    invoke-direct {v0, v1, v4, v2}, Lghq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lghq;->c:Lghq;

    .line 22
    new-instance v0, Lghq;

    const-string v1, "PRERELEASE"

    const-string v2, "youtubei/vi"

    invoke-direct {v0, v1, v5, v2}, Lghq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lghq;->d:Lghq;

    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Lghq;

    sget-object v1, Lghq;->b:Lghq;

    aput-object v1, v0, v3

    sget-object v1, Lghq;->c:Lghq;

    aput-object v1, v0, v4

    sget-object v1, Lghq;->d:Lghq;

    aput-object v1, v0, v5

    sput-object v0, Lghq;->e:[Lghq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput-object p3, p0, Lghq;->a:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public static a(Landroid/content/SharedPreferences;)Z
    .locals 2

    .prologue
    .line 53
    invoke-static {p0}, Lghq;->b(Landroid/content/SharedPreferences;)Lghq;

    move-result-object v0

    sget-object v1, Lghq;->c:Lghq;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Landroid/content/SharedPreferences;)Lghq;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 60
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const-string v0, "InnerTubeApiSelection"

    .line 64
    invoke-static {}, Lghq;->values()[Lghq;

    move-result-object v1

    aget-object v1, v1, v5

    invoke-virtual {v1}, Lghq;->toString()Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    :try_start_0
    invoke-static {v0}, Lghq;->valueOf(Ljava/lang/String;)Lghq;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    .line 70
    :catch_0
    move-exception v1

    const-string v1, "Bogus value in shared preferences for key InnerTubeApiSelection value "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " returning default value."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lghq;->values()[Lghq;

    move-result-object v0

    aget-object v0, v0, v5

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lghq;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lghq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lghq;

    return-object v0
.end method

.method public static values()[Lghq;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lghq;->e:[Lghq;

    invoke-virtual {v0}, [Lghq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lghq;

    return-object v0
.end method
