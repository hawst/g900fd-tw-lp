.class public final Ldrq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldrc;


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private b:Ljava/util/List;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "preferences can not be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Ldrq;->a:Landroid/content/SharedPreferences;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldrq;->c:Z

    .line 32
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 99
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    .line 103
    iget-object v4, v0, Ldst;->b:Ldth;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    iget-object v0, v0, Ldst;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 106
    :cond_0
    iget-object v0, p0, Ldrq;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "screenIds"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "screenNames"

    .line 107
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 108
    return-void
.end method

.method private declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ldrq;->c:Z

    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p0, p1, p2}, Ldrq;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    :cond_0
    monitor-exit p0

    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ldth;)Ldst;
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ldrq;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Ldrq;->b:Ljava/util/List;

    invoke-static {v0, p3}, La;->a(Ljava/util/List;Ldth;)Ldst;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    .line 92
    iget-object v1, p0, Ldrq;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 94
    :cond_0
    iget-object v1, p0, Ldrq;->b:Ljava/util/List;

    invoke-direct {p0, v1}, Ldrq;->a(Ljava/util/List;)V

    .line 95
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldth;Ljava/lang/String;)Ldst;
    .locals 4

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Ldrq;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Ldrq;->b:Ljava/util/List;

    invoke-static {v0, p3}, La;->a(Ljava/util/List;Ldth;)Ldst;

    move-result-object v1

    .line 76
    const/4 v0, 0x0

    .line 77
    if-eqz v1, :cond_0

    .line 78
    iget-object v0, p0, Ldrq;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 79
    invoke-virtual {v1, p4}, Ldst;->a(Ljava/lang/String;)Ldst;

    move-result-object v0

    .line 80
    iget-object v3, p0, Ldrq;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 81
    iget-object v1, p0, Ldrq;->b:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 83
    :cond_0
    iget-object v1, p0, Ldrq;->b:Ljava/util/List;

    invoke-direct {p0, v1}, Ldrq;->a(Ljava/util/List;)V

    .line 84
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 36
    iget-boolean v0, p0, Ldrq;->c:Z

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Ldrq;->b:Ljava/util/List;

    .line 57
    :goto_0
    return-object v0

    .line 40
    :cond_0
    iget-object v0, p0, Ldrq;->a:Landroid/content/SharedPreferences;

    const-string v1, "screenIds"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldrq;->b:Ljava/util/List;

    .line 56
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldrq;->c:Z

    .line 57
    iget-object v0, p0, Ldrq;->b:Ljava/util/List;

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Ldrq;->a:Landroid/content/SharedPreferences;

    const-string v1, "screenIds"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 44
    iget-object v0, p0, Ldrq;->a:Landroid/content/SharedPreferences;

    const-string v1, "screenNames"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 45
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 46
    const/4 v0, 0x0

    :goto_2
    array-length v1, v2

    if-ge v0, v1, :cond_4

    .line 47
    aget-object v1, v2, v0

    .line 48
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 49
    new-instance v5, Ldst;

    new-instance v6, Ldth;

    invoke-direct {v6, v1}, Ldth;-><init>(Ljava/lang/String;)V

    array-length v1, v3

    if-ge v0, v1, :cond_3

    aget-object v1, v3, v0

    :goto_3
    invoke-direct {v5, v6, v1, v7, v7}, Ldst;-><init>(Ldth;Ljava/lang/String;Ldss;Ldtb;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 49
    :cond_3
    const-string v1, ""

    goto :goto_3

    .line 54
    :cond_4
    iput-object v4, p0, Ldrq;->b:Ljava/util/List;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldst;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 62
    invoke-direct {p0, p1, p2}, Ldrq;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Ldrq;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 66
    iget-object v0, p0, Ldrq;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 67
    iget-object v0, p0, Ldrq;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    iget-object v0, v0, Ldst;->b:Ldth;

    invoke-virtual {p0, p1, p2, v0}, Ldrq;->a(Ljava/lang/String;Ljava/lang/String;Ldth;)Ldst;

    .line 69
    :cond_0
    iget-object v0, p0, Ldrq;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Ldrq;->a(Ljava/util/List;)V

    .line 70
    return-void
.end method
