.class public final Lepw;
.super Lepz;


# instance fields
.field private final a:Lenf;

.field private final b:Leoi;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Lepz;-><init>()V

    invoke-static {p1, p2}, Leog;->a(Ljava/lang/String;Landroid/content/Context;)Leog;

    move-result-object v0

    iput-object v0, p0, Lepw;->a:Lenf;

    new-instance v0, Leoi;

    iget-object v1, p0, Lepw;->a:Lenf;

    invoke-direct {v0, v1}, Leoi;-><init>(Lenf;)V

    iput-object v0, p0, Lepw;->b:Leoi;

    return-void
.end method


# virtual methods
.method public final a(Lelr;Lelr;)Lelr;
    .locals 6

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p1}, Lelu;->a(Lelr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {p2}, Lelu;->a(Lelr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v3, p0, Lepw;->b:Leoi;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v1, v4, v5}, Leoi;->a(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lelu;->a(Ljava/lang/Object;)Lelr;
    :try_end_0
    .catch Leon; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v2

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "ms"

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lepw;->b:Leoi;

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Leoi;->c:[Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lepw;->b:Leoi;

    iput-object p1, v0, Leoi;->a:Ljava/lang/String;

    iput-object p2, v0, Leoi;->b:Ljava/lang/String;

    return-void
.end method

.method public final a(Lelr;)Z
    .locals 2

    invoke-static {p1}, Lelu;->a(Lelr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p0, Lepw;->b:Leoi;

    invoke-virtual {v1, v0}, Leoi;->a(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final b(Lelr;)Z
    .locals 2

    invoke-static {p1}, Lelu;->a(Lelr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p0, Lepw;->b:Leoi;

    invoke-virtual {v1, v0}, Leoi;->b(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final c(Lelr;)Ljava/lang/String;
    .locals 2

    invoke-static {p1}, Lelu;->a(Lelr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lepw;->a:Lenf;

    invoke-interface {v1, v0}, Lenf;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
