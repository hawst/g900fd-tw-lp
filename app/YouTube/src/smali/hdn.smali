.class public final Lhdn;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhdm;

.field private b:Lhdj;

.field private c:Lhgz;

.field private d:Lhgz;

.field private e:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31647
    invoke-direct {p0}, Lidf;-><init>()V

    .line 31650
    iput-object v0, p0, Lhdn;->a:Lhdm;

    .line 31653
    iput-object v0, p0, Lhdn;->b:Lhdj;

    .line 31656
    iput-object v0, p0, Lhdn;->c:Lhgz;

    .line 31659
    iput-object v0, p0, Lhdn;->d:Lhgz;

    .line 31662
    iput-object v0, p0, Lhdn;->e:Lhgz;

    .line 31647
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 31723
    const/4 v0, 0x0

    .line 31724
    iget-object v1, p0, Lhdn;->a:Lhdm;

    if-eqz v1, :cond_0

    .line 31725
    const/4 v0, 0x1

    iget-object v1, p0, Lhdn;->a:Lhdm;

    .line 31726
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 31728
    :cond_0
    iget-object v1, p0, Lhdn;->b:Lhdj;

    if-eqz v1, :cond_1

    .line 31729
    const/4 v1, 0x2

    iget-object v2, p0, Lhdn;->b:Lhdj;

    .line 31730
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31732
    :cond_1
    iget-object v1, p0, Lhdn;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 31733
    const/4 v1, 0x3

    iget-object v2, p0, Lhdn;->c:Lhgz;

    .line 31734
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31736
    :cond_2
    iget-object v1, p0, Lhdn;->d:Lhgz;

    if-eqz v1, :cond_3

    .line 31737
    const/4 v1, 0x4

    iget-object v2, p0, Lhdn;->d:Lhgz;

    .line 31738
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31740
    :cond_3
    iget-object v1, p0, Lhdn;->e:Lhgz;

    if-eqz v1, :cond_4

    .line 31741
    const/4 v1, 0x5

    iget-object v2, p0, Lhdn;->e:Lhgz;

    .line 31742
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31744
    :cond_4
    iget-object v1, p0, Lhdn;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31745
    iput v0, p0, Lhdn;->J:I

    .line 31746
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 31643
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhdn;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhdn;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhdn;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhdn;->a:Lhdm;

    if-nez v0, :cond_2

    new-instance v0, Lhdm;

    invoke-direct {v0}, Lhdm;-><init>()V

    iput-object v0, p0, Lhdn;->a:Lhdm;

    :cond_2
    iget-object v0, p0, Lhdn;->a:Lhdm;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhdn;->b:Lhdj;

    if-nez v0, :cond_3

    new-instance v0, Lhdj;

    invoke-direct {v0}, Lhdj;-><init>()V

    iput-object v0, p0, Lhdn;->b:Lhdj;

    :cond_3
    iget-object v0, p0, Lhdn;->b:Lhdj;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhdn;->c:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdn;->c:Lhgz;

    :cond_4
    iget-object v0, p0, Lhdn;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhdn;->d:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdn;->d:Lhgz;

    :cond_5
    iget-object v0, p0, Lhdn;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhdn;->e:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdn;->e:Lhgz;

    :cond_6
    iget-object v0, p0, Lhdn;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 31702
    iget-object v0, p0, Lhdn;->a:Lhdm;

    if-eqz v0, :cond_0

    .line 31703
    const/4 v0, 0x1

    iget-object v1, p0, Lhdn;->a:Lhdm;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 31705
    :cond_0
    iget-object v0, p0, Lhdn;->b:Lhdj;

    if-eqz v0, :cond_1

    .line 31706
    const/4 v0, 0x2

    iget-object v1, p0, Lhdn;->b:Lhdj;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 31708
    :cond_1
    iget-object v0, p0, Lhdn;->c:Lhgz;

    if-eqz v0, :cond_2

    .line 31709
    const/4 v0, 0x3

    iget-object v1, p0, Lhdn;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 31711
    :cond_2
    iget-object v0, p0, Lhdn;->d:Lhgz;

    if-eqz v0, :cond_3

    .line 31712
    const/4 v0, 0x4

    iget-object v1, p0, Lhdn;->d:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 31714
    :cond_3
    iget-object v0, p0, Lhdn;->e:Lhgz;

    if-eqz v0, :cond_4

    .line 31715
    const/4 v0, 0x5

    iget-object v1, p0, Lhdn;->e:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 31717
    :cond_4
    iget-object v0, p0, Lhdn;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 31719
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 31677
    if-ne p1, p0, :cond_1

    .line 31685
    :cond_0
    :goto_0
    return v0

    .line 31678
    :cond_1
    instance-of v2, p1, Lhdn;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 31679
    :cond_2
    check-cast p1, Lhdn;

    .line 31680
    iget-object v2, p0, Lhdn;->a:Lhdm;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhdn;->a:Lhdm;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhdn;->b:Lhdj;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhdn;->b:Lhdj;

    if-nez v2, :cond_3

    .line 31681
    :goto_2
    iget-object v2, p0, Lhdn;->c:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhdn;->c:Lhgz;

    if-nez v2, :cond_3

    .line 31682
    :goto_3
    iget-object v2, p0, Lhdn;->d:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhdn;->d:Lhgz;

    if-nez v2, :cond_3

    .line 31683
    :goto_4
    iget-object v2, p0, Lhdn;->e:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhdn;->e:Lhgz;

    if-nez v2, :cond_3

    .line 31684
    :goto_5
    iget-object v2, p0, Lhdn;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhdn;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 31685
    goto :goto_0

    .line 31680
    :cond_4
    iget-object v2, p0, Lhdn;->a:Lhdm;

    iget-object v3, p1, Lhdn;->a:Lhdm;

    invoke-virtual {v2, v3}, Lhdm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhdn;->b:Lhdj;

    iget-object v3, p1, Lhdn;->b:Lhdj;

    .line 31681
    invoke-virtual {v2, v3}, Lhdj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhdn;->c:Lhgz;

    iget-object v3, p1, Lhdn;->c:Lhgz;

    .line 31682
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhdn;->d:Lhgz;

    iget-object v3, p1, Lhdn;->d:Lhgz;

    .line 31683
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhdn;->e:Lhgz;

    iget-object v3, p1, Lhdn;->e:Lhgz;

    .line 31684
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhdn;->I:Ljava/util/List;

    iget-object v3, p1, Lhdn;->I:Ljava/util/List;

    .line 31685
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 31689
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 31691
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdn;->a:Lhdm;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 31692
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdn;->b:Lhdj;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 31693
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdn;->c:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 31694
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdn;->d:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 31695
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdn;->e:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 31696
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhdn;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 31697
    return v0

    .line 31691
    :cond_0
    iget-object v0, p0, Lhdn;->a:Lhdm;

    invoke-virtual {v0}, Lhdm;->hashCode()I

    move-result v0

    goto :goto_0

    .line 31692
    :cond_1
    iget-object v0, p0, Lhdn;->b:Lhdj;

    invoke-virtual {v0}, Lhdj;->hashCode()I

    move-result v0

    goto :goto_1

    .line 31693
    :cond_2
    iget-object v0, p0, Lhdn;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 31694
    :cond_3
    iget-object v0, p0, Lhdn;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    .line 31695
    :cond_4
    iget-object v0, p0, Lhdn;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_4

    .line 31696
    :cond_5
    iget-object v1, p0, Lhdn;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
