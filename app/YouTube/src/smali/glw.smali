.class public final Lglw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lgbu;

.field public final b:I

.field public final c:Z

.field private final d:I


# direct methods
.method public constructor <init>(Lgbu;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, v0, v0, v0}, Lglw;-><init>(Lgbu;IIZ)V

    .line 31
    return-void
.end method

.method public constructor <init>(Lgbu;IIZ)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    iput-object v0, p0, Lglw;->a:Lgbu;

    .line 24
    iput p2, p0, Lglw;->d:I

    .line 25
    iput p3, p0, Lglw;->b:I

    .line 26
    iput-boolean p4, p0, Lglw;->c:Z

    .line 27
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lglw;->a:Lgbu;

    iget v0, v0, Lgbu;->j:I

    iget v1, p0, Lglw;->d:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(Lgbu;)Z
    .locals 2

    .prologue
    .line 75
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p0, Lglw;->a:Lgbu;

    iget-object v0, v0, Lgbu;->a:Ljava/lang/String;

    iget-object v1, p1, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lglw;->a:Lgbu;

    iget-object v0, v0, Lgbu;->e:Ljava/util/Date;

    iget-object v1, p1, Lgbu;->e:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lglw;->a:Lgbu;

    iget-boolean v0, v0, Lgbu;->l:Z

    if-nez v0, :cond_0

    iget v0, p0, Lglw;->d:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
