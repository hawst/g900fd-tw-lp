.class public final Lfva;
.super Lfud;
.source "SourceFile"


# instance fields
.field private final c:Lfus;

.field private final d:Lfrz;


# direct methods
.method public constructor <init>(Lfdg;Lfuu;Lfus;Levn;Leyt;Lfrz;Lfkl;)V
    .locals 3

    .prologue
    .line 57
    .line 59
    invoke-virtual {p2}, Lfuu;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsl;

    .line 57
    invoke-direct {p0, p1, v0, p4, p5}, Lfud;-><init>(Lfdg;Lfsl;Levn;Leyt;)V

    .line 62
    const-class v0, Lfkl;

    invoke-virtual {p2, v0}, Lfuu;->a(Ljava/lang/Class;)V

    .line 63
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfus;

    iput-object v0, p0, Lfva;->c:Lfus;

    .line 65
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lfva;->d:Lfrz;

    .line 67
    new-instance v1, Lfuz;

    invoke-direct {v1}, Lfuz;-><init>()V

    .line 68
    iget-object v0, p7, Lfkl;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p7, Lfkl;->a:Lhkv;

    iget-object v0, v0, Lhkv;->d:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p7, Lfkl;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p7, Lfkl;->c:Ljava/lang/CharSequence;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, v1, Lfuz;->a:Ljava/lang/CharSequence;

    .line 69
    invoke-virtual {p0, v1}, Lfva;->a(Ljava/lang/Object;)V

    .line 70
    if-eqz p7, :cond_2

    invoke-virtual {p7}, Lfkl;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfva;->a(Ljava/util/Collection;)V

    iget-object v0, p7, Lfkl;->a:Lhkv;

    iget-object v0, v0, Lhkv;->c:Lhkw;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    iget-object v1, p7, Lfkl;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p7, Lfkl;->a:Lhkv;

    iget-object v1, v1, Lhkv;->b:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p7, Lfkl;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p7, Lfkl;->b:Ljava/lang/CharSequence;

    iget-object v2, p0, Lfud;->b:Lfvc;

    iput-object v1, v2, Lfvc;->c:Ljava/lang/CharSequence;

    iget-object v1, p0, Lfud;->a:Lfsi;

    invoke-virtual {v1}, Lfsi;->notifyDataSetChanged()V

    new-instance v1, Lfvb;

    invoke-direct {v1, p0, v0}, Lfvb;-><init>(Lfva;Lhuo;)V

    iget-object v0, p0, Lfud;->b:Lfvc;

    iput-object v1, v0, Lfvc;->a:Landroid/view/View$OnClickListener;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfva;->a(Z)V

    .line 71
    :cond_2
    :goto_1
    return-void

    .line 70
    :cond_3
    iget-object v0, p7, Lfkl;->a:Lhkv;

    iget-object v0, v0, Lhkv;->c:Lhkw;

    iget-object v0, v0, Lhkw;->a:Lhuo;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfva;->a(Z)V

    goto :goto_1
.end method

.method static synthetic a(Lfva;)Lfrz;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lfva;->d:Lfrz;

    return-object v0
.end method

.method static synthetic b(Lfva;)Lfus;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lfva;->c:Lfus;

    return-object v0
.end method
