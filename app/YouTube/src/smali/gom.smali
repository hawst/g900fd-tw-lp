.class public Lgom;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lgoh;

.field public final b:Leaf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210
    new-instance v0, Lgon;

    invoke-direct {v0}, Lgon;-><init>()V

    sput-object v0, Lgom;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Leaf;)V
    .locals 2

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    new-instance v0, Lgoh;

    .line 232
    iget-object v1, p1, Leaf;->a:Leaa;

    invoke-direct {v0, v1}, Lgoh;-><init>(Leaa;)V

    iput-object v0, p0, Lgom;->a:Lgoh;

    .line 233
    iput-object p1, p0, Lgom;->b:Leaf;

    .line 234
    return-void
.end method

.method public constructor <init>(Lgoh;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Leaf;

    invoke-direct {v0}, Leaf;-><init>()V

    iput-object v0, p0, Lgom;->b:Leaf;

    .line 102
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoh;

    iput-object v0, p0, Lgom;->a:Lgoh;

    .line 103
    return-void
.end method

.method public static final a(Landroid/content/Intent;)Lgom;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 41
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 44
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 46
    :try_start_0
    invoke-static {v3}, Lglb;->a(Landroid/net/Uri;)Lglb;

    move-result-object v4

    .line 47
    invoke-static {v3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/net/Uri;->isHierarchical()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "playnext"

    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    const/4 v8, 0x0

    invoke-static {v5, v8}, La;->b(Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_0

    move v2, v1

    :cond_0
    const-string v1, "index"

    invoke-virtual {v3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, -0x1

    invoke-static {v1, v0}, La;->b(Ljava/lang/String;I)I

    move-result v0

    move v1, v0

    :goto_0
    const-string v0, "list"

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lgla;

    invoke-direct {v0, v5, v2, v1}, Lgla;-><init>(Ljava/lang/String;ZI)V

    move-object v3, v0

    .line 49
    :goto_1
    if-eqz v3, :cond_5

    iget-object v0, v3, Lgla;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 50
    new-instance v0, Lgoh;

    const-string v1, ""

    iget-object v2, v3, Lgla;->a:Ljava/lang/String;

    iget v3, v3, Lgla;->b:I

    iget v4, v4, Lglb;->b:I

    sget-object v5, Lgog;->b:Lgog;

    invoke-direct/range {v0 .. v5}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;IILgog;)V

    move-object v1, v0

    .line 64
    :goto_2
    new-instance v0, Lgom;

    invoke-direct {v0, v1}, Lgom;-><init>(Lgoh;)V

    .line 65
    invoke-virtual {v0, v7}, Lgom;->a(Landroid/os/Bundle;)V

    .line 70
    :goto_3
    return-object v0

    :cond_1
    move v1, v0

    .line 47
    goto :goto_0

    :cond_2
    const-string v0, "p"

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lgla;

    invoke-direct {v0, v5, v2, v1}, Lgla;-><init>(Ljava/lang/String;ZI)V

    move-object v3, v0

    goto :goto_1

    :cond_3
    const-string v0, "https://gdata.youtube.com/feeds/api/playlists/(.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lgla;

    invoke-direct {v0, v3, v2, v1}, Lgla;-><init>(Ljava/lang/String;ZI)V

    move-object v3, v0

    goto :goto_1

    :cond_4
    move-object v3, v6

    goto :goto_1

    .line 57
    :cond_5
    new-instance v0, Lgoh;

    iget-object v1, v4, Lglb;->a:Ljava/util/List;

    const/4 v2, -0x1

    iget v3, v4, Lglb;->b:I

    sget-object v4, Lgog;->b:Lgog;

    invoke-direct {v0, v1, v2, v3, v4}, Lgoh;-><init>(Ljava/util/List;IILgog;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_2

    .line 70
    :catch_0
    move-exception v0

    move-object v0, v6

    goto :goto_3
.end method

.method public static final b(Landroid/content/Intent;)Lgom;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    :try_start_0
    const-string v2, "android.intent.extra.inventory_identifier"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_0

    array-length v2, v0

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    :cond_0
    move-object v0, v1

    .line 96
    :goto_0
    return-object v0

    .line 86
    :cond_1
    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lglb;->a(Landroid/net/Uri;)Lglb;

    move-result-object v0

    .line 87
    new-instance v2, Lgoh;

    iget-object v3, v0, Lglb;->a:Ljava/util/List;

    const/4 v4, -0x1

    iget v0, v0, Lglb;->b:I

    sget-object v5, Lgog;->b:Lgog;

    invoke-direct {v2, v3, v4, v0, v5}, Lgoh;-><init>(Ljava/util/List;IILgog;)V

    .line 93
    new-instance v0, Lgom;

    invoke-direct {v0, v2}, Lgom;-><init>(Lgoh;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 107
    const-string v1, "finish_on_ended"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lgom;->b:Leaf;

    invoke-virtual {v2, v1}, Leaf;->a(Z)Leaf;

    .line 108
    const-string v1, "force_fullscreen"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lgom;->b:Leaf;

    invoke-virtual {v2, v1}, Leaf;->b(Z)Leaf;

    .line 109
    const-string v1, "no_animation"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v1}, Lgom;->b(Z)V

    .line 110
    const-string v1, "skip_remote_route_dialog"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v1}, Lgom;->c(Z)V

    .line 111
    const-string v1, "youtube_tv_uid"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgom;->b:Leaf;

    invoke-virtual {v2, v1}, Leaf;->a(Ljava/lang/String;)Leaf;

    .line 113
    const-string v1, "keep_history"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 114
    invoke-virtual {p0, v1}, Lgom;->a(Z)V

    .line 117
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lgom;->b:Leaf;

    invoke-virtual {v1, v0}, Leaf;->f(Z)Leaf;

    .line 118
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lgom;->b:Leaf;

    invoke-virtual {v0, p1}, Leaf;->e(Z)Leaf;

    .line 177
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lgom;->b:Leaf;

    invoke-virtual {v0, p1}, Leaf;->c(Z)Leaf;

    .line 193
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lgom;->b:Leaf;

    invoke-virtual {v0, p1}, Leaf;->d(Z)Leaf;

    .line 197
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lgom;->b:Leaf;

    iget-object v1, p0, Lgom;->a:Lgoh;

    .line 239
    iget-object v1, v1, Lgoh;->a:Leaa;

    .line 238
    invoke-virtual {v0, v1}, Leaf;->a(Leaa;)Leaf;

    .line 240
    iget-object v0, p0, Lgom;->b:Leaf;

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lida;)V

    .line 241
    return-void
.end method
