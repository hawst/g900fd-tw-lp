.class public final Lbtt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfuv;


# instance fields
.field final a:Lfsi;

.field final b:Ljava/util/Map;

.field private final c:Lgix;

.field private final d:Lckt;

.field private final e:Lbtw;

.field private final f:Lffw;

.field private final g:Lfgg;

.field private final h:Lhgz;

.field private final i:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgix;Lfsl;Lckt;Lffw;)V
    .locals 4

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbtt;->c:Lgix;

    .line 106
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckt;

    iput-object v0, p0, Lbtt;->d:Lckt;

    .line 107
    new-instance v0, Lfsi;

    invoke-direct {v0, p3}, Lfsi;-><init>(Lfsl;)V

    iput-object v0, p0, Lbtt;->a:Lfsi;

    .line 108
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbtt;->i:Ljava/util/Map;

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbtt;->b:Ljava/util/Map;

    .line 110
    new-instance v0, Lbtw;

    invoke-direct {v0, p0}, Lbtw;-><init>(Lbtt;)V

    iput-object v0, p0, Lbtt;->e:Lbtw;

    .line 111
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffw;

    iput-object v0, p0, Lbtt;->f:Lffw;

    .line 112
    new-instance v0, Lbtv;

    invoke-direct {v0, p0}, Lbtv;-><init>(Lbtt;)V

    iput-object v0, p0, Lbtt;->g:Lfgg;

    .line 113
    iget-object v0, p0, Lbtt;->g:Lfgg;

    invoke-virtual {p5, v0}, Lffw;->a(Lfgg;)V

    .line 116
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 117
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902d0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 116
    invoke-static {v0}, Lfvo;->a([Ljava/lang/String;)Lhgz;

    move-result-object v0

    iput-object v0, p0, Lbtt;->h:Lhgz;

    .line 119
    iget-object v0, p0, Lbtt;->e:Lbtw;

    iget-object v1, p4, Lckt;->a:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p4, Lckt;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Lckt;->c()V

    .line 122
    :cond_0
    invoke-virtual {p0}, Lbtt;->c()V

    .line 123
    return-void
.end method

.method private a(Lgjm;)Lcku;
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 213
    iget-object v0, p0, Lbtt;->i:Ljava/util/Map;

    iget-object v0, p0, Lbtt;->c:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 251
    :cond_0
    :goto_0
    return-object v0

    .line 218
    :cond_1
    iget-object v0, p0, Lbtt;->c:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    .line 219
    iget-object v2, p1, Lgjm;->g:Lgje;

    const-string v3, "authAccount"

    invoke-virtual {v2, v3, v1}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 220
    iget-object v3, p1, Lgjm;->g:Lgje;

    const-string v4, "account_id"

    invoke-virtual {v3, v4, v1}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 223
    iget-object v4, p1, Lgjm;->g:Lgje;

    const-string v5, "tracking_account_id"

    invoke-virtual {v4, v5, v8}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v4

    .line 227
    iget-object v5, v0, Lgit;->b:Lgiv;

    invoke-virtual {v5}, Lgiv;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v4, :cond_3

    .line 228
    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move-object v0, v1

    .line 230
    goto :goto_0

    .line 235
    :cond_3
    iget-object v0, p0, Lbtt;->i:Ljava/util/Map;

    iget-object v2, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 236
    iget-object v0, p0, Lbtt;->i:Ljava/util/Map;

    iget-object v2, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcku;

    .line 243
    :goto_1
    iget-object v2, p1, Lgjm;->g:Lgje;

    const-string v3, "upload_title"

    invoke-virtual {v2, v3, v1}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcku;->a:Ljava/lang/CharSequence;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p1, Lgjm;->e:J

    long-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p1, Lgjm;->f:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    iput-wide v2, v0, Lcku;->f:D

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p1, Lgjm;->g:Lgje;

    const-string v5, "upload_start_time_millis"

    invoke-virtual {v4, v5, v6, v7}, Lgje;->b(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-wide v4, p1, Lgjm;->e:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    iget-wide v4, p1, Lgjm;->f:J

    iget-wide v6, p1, Lgjm;->e:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    long-to-double v2, v2

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v6

    mul-double/2addr v2, v4

    iget-wide v4, p1, Lgjm;->e:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v0, Lcku;->g:I

    :cond_4
    iget-object v2, p1, Lgjm;->h:Lgje;

    const-string v3, "video_id"

    invoke-virtual {v2, v3}, Lgje;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p1, Lgjm;->h:Lgje;

    const-string v3, "video_id"

    invoke-virtual {v2, v3, v1}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcku;->k:Ljava/lang/String;

    if-nez v2, :cond_8

    iput-object v1, v0, Lcku;->k:Ljava/lang/String;

    :cond_5
    :goto_2
    iget-object v1, v0, Lcku;->e:Landroid/graphics/Bitmap;

    if-nez v1, :cond_6

    iget-object v1, p1, Lgjm;->g:Lgje;

    const-string v2, "upload_file_thumbnail"

    invoke-virtual {v1, v2}, Lgje;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p1, Lgjm;->g:Lgje;

    const-string v2, "upload_file_thumbnail"

    invoke-virtual {v1, v2}, Lgje;->b(Ljava/lang/String;)[B

    move-result-object v1

    array-length v2, v1

    invoke-static {v1, v8, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcku;->e:Landroid/graphics/Bitmap;

    .line 245
    :cond_6
    iget-object v1, v0, Lcku;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbtt;->b:Ljava/util/Map;

    .line 246
    iget-object v2, v0, Lcku;->k:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 247
    iget-object v1, p0, Lbtt;->b:Ljava/util/Map;

    iget-object v2, v0, Lcku;->k:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    iget-object v1, p0, Lbtt;->f:Lffw;

    iget-object v2, v0, Lcku;->k:Ljava/lang/String;

    iget-object v3, v1, Lffw;->d:Landroid/os/Handler;

    new-instance v4, Lfgb;

    const-wide/16 v6, 0x32

    invoke-direct {v4, v1, v6, v7, v2}, Lfgb;-><init>(Lffw;JLjava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 238
    :cond_7
    new-instance v0, Lcku;

    invoke-direct {v0}, Lcku;-><init>()V

    .line 239
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lhnf;

    invoke-direct {v3}, Lhnf;-><init>()V

    iget-object v4, p0, Lbtt;->h:Lhgz;

    iput-object v4, v3, Lhnf;->a:Lhgz;

    new-instance v4, Lhut;

    invoke-direct {v4}, Lhut;-><init>()V

    iput-object v4, v3, Lhnf;->c:Lhut;

    iget-object v4, v3, Lhnf;->c:Lhut;

    new-instance v5, Lhfm;

    invoke-direct {v5}, Lhfm;-><init>()V

    iput-object v5, v4, Lhut;->h:Lhfm;

    iget-object v4, v3, Lhnf;->c:Lhut;

    iget-object v4, v4, Lhut;->h:Lhfm;

    iget-object v5, p1, Lgjm;->a:Ljava/lang/String;

    iput-object v5, v4, Lhfm;->a:Ljava/lang/String;

    new-instance v4, Lfkq;

    invoke-direct {v4, v3, v1}, Lfkq;-><init>(Lhnf;Lfqh;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcku;->a()Lfkp;

    move-result-object v3

    iput-object v2, v3, Lfkp;->a:Ljava/util/List;

    .line 240
    iget-object v2, p0, Lbtt;->i:Ljava/util/Map;

    iget-object v3, p1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 243
    :cond_8
    iget-object v2, v0, Lcku;->k:Ljava/lang/String;

    invoke-static {v2, v1}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Lb;->c(Z)V

    goto/16 :goto_2
.end method

.method static synthetic a(Lbtt;Lgjm;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lbtt;->a(Lgjm;)Lcku;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lbtt;->a:Lfsi;

    iget-object v0, p0, Lbtt;->a:Lfsi;

    iget-object v0, v0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lbtt;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->notifyDataSetChanged()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbtt;->a:Lfsi;

    invoke-virtual {v0, v1}, Lfsi;->b(Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public final b()Lfsi;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lbtt;->a:Lfsi;

    return-object v0
.end method

.method c()V
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Lbtt;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 155
    iget-object v0, p0, Lbtt;->d:Lckt;

    iget-object v1, v0, Lckt;->b:Lfad;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lckt;->b:Lfad;

    iget-object v1, v1, Lfad;->b:Landroid/os/Binder;

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    .line 156
    :goto_0
    if-nez v0, :cond_3

    .line 187
    :cond_1
    return-void

    .line 155
    :cond_2
    iget-object v0, v0, Lckt;->b:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    invoke-virtual {v0}, Ldmz;->a()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 162
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 163
    new-instance v0, Lbtu;

    invoke-direct {v0, p0}, Lbtu;-><init>(Lbtt;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 176
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjm;

    .line 178
    iget-object v2, v0, Lgjm;->c:Lgjn;

    sget-object v3, Lgjn;->c:Lgjn;

    if-eq v2, v3, :cond_4

    .line 179
    invoke-direct {p0, v0}, Lbtt;->a(Lgjm;)Lcku;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_4

    .line 183
    iget-object v2, p0, Lbtt;->a:Lfsi;

    invoke-virtual {v2, v0}, Lfsi;->b(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lbtt;->d:Lckt;

    iget-object v1, p0, Lbtt;->e:Lbtw;

    iget-object v2, v0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lckt;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->size()I

    move-result v1

    if-gtz v1, :cond_0

    invoke-virtual {v0}, Lckt;->d()V

    .line 137
    :cond_0
    iget-object v0, p0, Lbtt;->f:Lffw;

    invoke-virtual {v0}, Lffw;->a()V

    .line 138
    iget-object v0, p0, Lbtt;->f:Lffw;

    iget-object v1, p0, Lbtt;->g:Lfgg;

    iget-object v0, v0, Lffw;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 139
    return-void
.end method

.method public final handleServiceResponseRemoveEvent(Lfml;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 143
    iget-object v0, p1, Lfmk;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lbtt;->a:Lfsi;

    iget-object v1, p1, Lfmk;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/lang/Object;)Z

    .line 146
    :cond_0
    return-void
.end method
