.class final Lsg;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private synthetic b:Lsf;


# direct methods
.method constructor <init>(Lsf;)V
    .locals 1

    .prologue
    .line 2211
    iput-object p1, p0, Lsg;->b:Lsf;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsg;->a:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2232
    invoke-virtual {p0, p1, p2}, Lsg;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2233
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 2237
    iget v6, p1, Landroid/os/Message;->what:I

    .line 2238
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2241
    packed-switch v6, :pswitch_data_0

    .line 2247
    :goto_0
    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lsg;->b:Lsf;

    iget-object v1, v1, Lsf;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    add-int/lit8 v3, v1, -0x1

    if-ltz v3, :cond_1

    .line 2248
    iget-object v1, p0, Lsg;->b:Lsf;

    iget-object v1, v1, Lsf;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsb;

    .line 2249
    if-nez v1, :cond_0

    .line 2250
    iget-object v1, p0, Lsg;->b:Lsf;

    iget-object v1, v1, Lsf;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v3

    goto :goto_1

    .line 2241
    :pswitch_1
    iget-object v1, p0, Lsg;->b:Lsf;

    iget-object v3, v1, Lsf;->g:Ltm;

    move-object v1, v2

    check-cast v1, Lsk;

    invoke-virtual {v3, v1}, Ltm;->a(Lsk;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lsg;->b:Lsf;

    iget-object v3, v1, Lsf;->g:Ltm;

    move-object v1, v2

    check-cast v1, Lsk;

    invoke-virtual {v3, v1}, Ltm;->b(Lsk;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lsg;->b:Lsf;

    iget-object v3, v1, Lsf;->g:Ltm;

    move-object v1, v2

    check-cast v1, Lsk;

    invoke-virtual {v3, v1}, Ltm;->c(Lsk;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lsg;->b:Lsf;

    iget-object v3, v1, Lsf;->g:Ltm;

    move-object v1, v2

    check-cast v1, Lsk;

    invoke-virtual {v3, v1}, Ltm;->d(Lsk;)V

    goto :goto_0

    .line 2252
    :cond_0
    :try_start_1
    iget-object v4, p0, Lsg;->a:Ljava/util/ArrayList;

    iget-object v1, v1, Lsb;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move v1, v3

    .line 2254
    goto :goto_1

    .line 2256
    :cond_1
    iget-object v1, p0, Lsg;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v5

    .line 2257
    :goto_2
    if-ge v4, v7, :cond_5

    .line 2258
    iget-object v1, p0, Lsg;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lsd;

    move-object v3, v0

    iget-object v8, v3, Lsd;->a:Lsb;

    iget-object v9, v3, Lsd;->b:Lsc;

    const v1, 0xff00

    and-int/2addr v1, v6

    sparse-switch v1, :sswitch_data_0

    .line 2257
    :cond_2
    :goto_3
    :pswitch_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 2258
    :sswitch_0
    move-object v0, v2

    check-cast v0, Lsk;

    move-object v1, v0

    iget v10, v3, Lsd;->d:I

    and-int/lit8 v10, v10, 0x2

    if-nez v10, :cond_3

    iget-object v3, v3, Lsd;->c:Lrz;

    invoke-virtual {v1, v3}, Lsk;->a(Lrz;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_2

    packed-switch v6, :pswitch_data_1

    goto :goto_3

    :pswitch_6
    invoke-virtual {v9, v8, v1}, Lsc;->a(Lsb;Lsk;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 2261
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lsg;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    throw v1

    :cond_4
    move v3, v5

    .line 2258
    goto :goto_4

    :pswitch_7
    :try_start_2
    invoke-virtual {v9, v8, v1}, Lsc;->b(Lsb;Lsk;)V

    goto :goto_3

    :pswitch_8
    invoke-virtual {v9, v8}, Lsc;->a(Lsb;)V

    goto :goto_3

    :pswitch_9
    invoke-virtual {v9, v1}, Lsc;->c(Lsk;)V

    goto :goto_3

    :pswitch_a
    invoke-virtual {v9, v1}, Lsc;->a(Lsk;)V

    goto :goto_3

    :pswitch_b
    invoke-virtual {v9, v1}, Lsc;->b(Lsk;)V

    goto :goto_3

    :sswitch_1
    packed-switch v6, :pswitch_data_2

    goto :goto_3

    :pswitch_c
    invoke-virtual {v9, v8}, Lsc;->b(Lsb;)V

    goto :goto_3

    :pswitch_d
    invoke-virtual {v9, v8}, Lsc;->c(Lsb;)V

    goto :goto_3

    :pswitch_e
    invoke-virtual {v9, v8}, Lsc;->d(Lsb;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 2261
    :cond_5
    iget-object v1, p0, Lsg;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2262
    return-void

    .line 2241
    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 2258
    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x101
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_5
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x201
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
