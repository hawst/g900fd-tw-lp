.class public final Lgfg;
.super Lgee;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lgec;


# instance fields
.field private a:Lgfk;

.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/Runnable;

.field private d:Lged;

.field private e:Landroid/view/Surface;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lgee;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-direct {p0}, Lgfg;->k()V

    .line 37
    new-instance v0, Lgfh;

    invoke-direct {v0, p0}, Lgfh;-><init>(Lgfg;)V

    iput-object v0, p0, Lgfg;->b:Ljava/lang/Runnable;

    .line 45
    new-instance v0, Lgfi;

    invoke-direct {v0, p0}, Lgfi;-><init>(Lgfg;)V

    iput-object v0, p0, Lgfg;->c:Ljava/lang/Runnable;

    .line 56
    iget-object v0, p0, Lgfg;->a:Lgfk;

    invoke-virtual {p0, v0}, Lgfg;->addView(Landroid/view/View;)V

    .line 57
    return-void
.end method

.method static synthetic a(Lgfg;)Lgfk;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lgfg;->a:Lgfk;

    return-object v0
.end method

.method static synthetic b(Lgfg;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lgfg;->k()V

    return-void
.end method

.method static synthetic c(Lgfg;)I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lgfg;->f:I

    return v0
.end method

.method static synthetic d(Lgfg;)I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lgfg;->g:I

    return v0
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 60
    new-instance v0, Lgfk;

    invoke-virtual {p0}, Lgfg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lgfk;-><init>(Lgfg;Landroid/content/Context;)V

    iput-object v0, p0, Lgfg;->a:Lgfk;

    .line 61
    iget-object v0, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v0, p0}, Lgfk;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 62
    iget-object v0, p0, Lgfg;->a:Lgfk;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lgfk;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 64
    iget-object v0, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v0, v2}, Lgfk;->setPivotX(F)V

    .line 65
    iget-object v0, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v0, v2}, Lgfk;->setPivotY(F)V

    .line 69
    iget-object v0, p0, Lgfg;->a:Lgfk;

    const v1, 0x3f800054    # 1.00001f

    invoke-virtual {v0, v1}, Lgfk;->setScaleX(F)V

    .line 70
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v0}, Lgfk;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lgfg;->c:Ljava/lang/Runnable;

    iget-object v1, p0, Lgfg;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, v1, p1}, Lgfg;->a(Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 147
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 96
    iput p1, p0, Lgfg;->f:I

    .line 97
    iput p2, p0, Lgfg;->g:I

    .line 98
    iget-object v0, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v0}, Lgfk;->requestLayout()V

    .line 99
    return-void
.end method

.method public final a(Lged;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lgfg;->d:Lged;

    .line 92
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v0}, Lgfk;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 203
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 204
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TextureMediaView does not support requested type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lgfg;->b:Ljava/lang/Runnable;

    iget-object v1, p0, Lgfg;->c:Ljava/lang/Runnable;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lgfg;->a(Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 152
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lgfj;

    invoke-direct {v0, p0}, Lgfj;-><init>(Lgfg;)V

    invoke-virtual {p0, v0}, Lgfg;->post(Ljava/lang/Runnable;)Z

    .line 165
    return-void
.end method

.method public final e()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lgfg;->e:Landroid/view/Surface;

    return-object v0
.end method

.method public final f()Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lgfg;->e:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lgfg;->e:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 172
    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lgfg;->e:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x2

    return v0
.end method

.method public final j()Landroid/view/View;
    .locals 0

    .prologue
    .line 118
    return-object p0
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 82
    iget-object v0, p0, Lgfg;->a:Lgfk;

    iget-object v1, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v1}, Lgfk;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v2}, Lgfk;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lgfk;->layout(IIII)V

    .line 83
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v0, p1, p2}, Lgfk;->measure(II)V

    .line 75
    iget-object v0, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v0}, Lgfk;->getMeasuredWidth()I

    move-result v0

    .line 76
    iget-object v1, p0, Lgfg;->a:Lgfk;

    invoke-virtual {v1}, Lgfk;->getMeasuredHeight()I

    move-result v1

    .line 77
    invoke-virtual {p0, v0, v1}, Lgfg;->setMeasuredDimension(II)V

    .line 78
    return-void
.end method

.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 195
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lgfg;->e:Landroid/view/Surface;

    .line 196
    iget-object v0, p0, Lgfg;->d:Lged;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lgfg;->d:Lged;

    invoke-interface {v0}, Lged;->a()V

    .line 199
    :cond_0
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lgfg;->e:Landroid/view/Surface;

    .line 216
    iget-object v0, p0, Lgfg;->d:Lged;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lgfg;->d:Lged;

    invoke-interface {v0}, Lged;->c()V

    .line 219
    :cond_0
    iget-object v0, p0, Lgfg;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lgfg;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 220
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lgfg;->d:Lged;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lgfg;->d:Lged;

    invoke-interface {v0}, Lged;->b()V

    .line 184
    :cond_0
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lgfg;->d:Lged;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lgfg;->d:Lged;

    invoke-interface {v0}, Lged;->b()V

    .line 191
    :cond_0
    return-void
.end method
