.class public final Lflx;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhsh;

.field public final b:F

.field public c:Lflh;

.field private final d:Lfqh;

.field private e:Lfnc;


# direct methods
.method public constructor <init>(Lhsh;FLfqh;)V
    .locals 5

    .prologue
    .line 30
    invoke-direct {p0}, Lfic;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsh;

    iput-object v0, p0, Lflx;->a:Lhsh;

    .line 32
    iput p2, p0, Lflx;->b:F

    .line 33
    iput-object p3, p0, Lflx;->d:Lfqh;

    .line 37
    iget-object v1, p1, Lhsh;->l:[Lhxe;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 40
    iget-object v4, v3, Lhxe;->b:Lhnx;

    if-eqz v4, :cond_1

    .line 41
    new-instance v0, Lfkv;

    iget-object v1, v3, Lhxe;->b:Lhnx;

    invoke-direct {v0, v1, p0}, Lfkv;-><init>(Lhnx;Lfqh;)V

    .line 45
    :cond_0
    return-void

    .line 39
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 126
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 127
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflx;->a:Lhsh;

    iget-object v0, v0, Lhsh;->f:Lhog;

    return-object v0
.end method

.method public final c()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflx;->a:Lhsh;

    iget-object v0, v0, Lhsh;->a:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lflx;->a:Lhsh;

    iget-object v0, v0, Lhsh;->k:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lflx;->d:Lfqh;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflx;->a:Lhsh;

    iget-object v0, v0, Lhsh;->g:Lhgz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lfnc;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lflx;->e:Lfnc;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lfnc;

    iget-object v1, p0, Lflx;->a:Lhsh;

    iget-object v1, v1, Lhsh;->b:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lflx;->e:Lfnc;

    .line 94
    :cond_0
    iget-object v0, p0, Lflx;->e:Lfnc;

    return-object v0
.end method
