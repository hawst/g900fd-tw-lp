.class public final Lcfr;
.super Lfsb;
.source "SourceFile"


# instance fields
.field private final a:Landroid/widget/RelativeLayout;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Lfvi;

.field private final e:Lfvi;

.field private final f:Landroid/view/View;

.field private final g:Lboi;

.field private final h:Lfsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;Lboi;Lfdw;Lfrz;)V
    .locals 4

    .prologue
    .line 57
    invoke-direct {p0, p3, p4, p6, p7}, Lfsb;-><init>(Lfhz;Lfsj;Lfdw;Lfrz;)V

    .line 58
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcfr;->h:Lfsj;

    .line 60
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcfr;->g:Lboi;

    .line 62
    const v0, 0x7f04009a

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcfr;->a:Landroid/widget/RelativeLayout;

    .line 64
    iget-object v0, p0, Lcfr;->a:Landroid/widget/RelativeLayout;

    const v1, 0x7f080239

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcfr;->b:Landroid/widget/TextView;

    .line 65
    iget-object v0, p0, Lcfr;->a:Landroid/widget/RelativeLayout;

    const v1, 0x7f08023a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcfr;->c:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcfr;->a:Landroid/widget/RelativeLayout;

    const v1, 0x7f080237

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 67
    iget-object v1, p0, Lcfr;->a:Landroid/widget/RelativeLayout;

    const v2, 0x7f08023b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 69
    iget-object v2, p0, Lcfr;->a:Landroid/widget/RelativeLayout;

    const v3, 0x7f080238

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    .line 71
    new-instance v2, Lfvi;

    invoke-direct {v2, p2, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v2, p0, Lcfr;->d:Lfvi;

    .line 72
    new-instance v0, Lfvi;

    invoke-direct {v0, p2, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcfr;->e:Lfvi;

    .line 74
    iget-object v0, p0, Lcfr;->a:Landroid/widget/RelativeLayout;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcfr;->f:Landroid/view/View;

    .line 76
    iget-object v0, p0, Lcfr;->a:Landroid/widget/RelativeLayout;

    invoke-interface {p4, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 77
    return-void
.end method

.method private a(Lfsg;Lfld;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 81
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 83
    iget-object v0, p0, Lcfr;->b:Landroid/widget/TextView;

    iget-object v3, p2, Lfld;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_0

    iget-object v3, p2, Lfld;->a:Lhos;

    iget-object v3, v3, Lhos;->c:Lhgz;

    if-eqz v3, :cond_0

    iget-object v3, p2, Lfld;->a:Lhos;

    iget-object v3, v3, Lhos;->c:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, p2, Lfld;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v3, p2, Lfld;->b:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcfr;->c:Landroid/widget/TextView;

    iget-object v3, p2, Lfld;->c:Ljava/lang/CharSequence;

    if-nez v3, :cond_1

    iget-object v3, p2, Lfld;->a:Lhos;

    iget-object v3, v3, Lhos;->d:Lhgz;

    if-eqz v3, :cond_1

    iget-object v3, p2, Lfld;->a:Lhos;

    iget-object v3, v3, Lhos;->d:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, p2, Lfld;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v3, p2, Lfld;->c:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcfr;->e:Lfvi;

    invoke-virtual {p2}, Lfld;->f()Lfnc;

    move-result-object v3

    invoke-virtual {v0, v3, v4}, Lfvi;->a(Lfnc;Leyo;)V

    .line 87
    iget-object v3, p0, Lcfr;->e:Lfvi;

    .line 88
    invoke-virtual {p2}, Lfld;->f()Lfnc;

    move-result-object v0

    invoke-virtual {v0}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 87
    :goto_0
    invoke-virtual {v3, v0}, Lfvi;->a(I)V

    .line 90
    iget-object v0, p0, Lcfr;->d:Lfvi;

    invoke-virtual {p2}, Lfld;->c()Lfnc;

    move-result-object v3

    invoke-virtual {v0, v3, v4}, Lfvi;->a(Lfnc;Leyo;)V

    .line 91
    iget-object v0, p0, Lcfr;->d:Lfvi;

    .line 92
    invoke-virtual {p2}, Lfld;->c()Lfnc;

    move-result-object v3

    invoke-virtual {v3}, Lfnc;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 91
    :goto_1
    invoke-virtual {v0, v1}, Lfvi;->a(I)V

    .line 94
    iget-object v0, p0, Lcfr;->g:Lboi;

    iget-object v1, p0, Lcfr;->f:Landroid/view/View;

    invoke-static {v0, v1, p2}, La;->a(Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lcfr;->h:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    .line 88
    goto :goto_0

    :cond_3
    move v1, v2

    .line 92
    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    check-cast p2, Lfld;

    invoke-direct {p0, p1, p2}, Lcfr;->a(Lfsg;Lfld;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    check-cast p2, Lfld;

    invoke-direct {p0, p1, p2}, Lcfr;->a(Lfsg;Lfld;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    check-cast p2, Lfld;

    invoke-direct {p0, p1, p2}, Lcfr;->a(Lfsg;Lfld;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
