.class public final Lajx;
.super Lajd;
.source "SourceFile"


# instance fields
.field public a:Lakb;

.field private final b:Lddb;

.field private final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lddb;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lajd;-><init>()V

    .line 24
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddb;

    iput-object v0, p0, Lajx;->b:Lddb;

    .line 25
    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lajx;->c:Landroid/os/Handler;

    .line 26
    return-void
.end method

.method static synthetic a(Lajx;)Lakb;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lajx;->a:Lakb;

    return-object v0
.end method

.method static synthetic a(Lajx;Lakb;)Lakb;
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Lajx;->a:Lakb;

    return-object p1
.end method

.method static synthetic b(Lajx;)Lddb;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lajx;->b:Lddb;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lajx;->c:Landroid/os/Handler;

    new-instance v1, Laka;

    invoke-direct {v1, p0}, Laka;-><init>(Lajx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 64
    return-void
.end method

.method public final a(JZZ)V
    .locals 7

    .prologue
    .line 48
    iget-object v6, p0, Lajx;->c:Landroid/os/Handler;

    new-instance v0, Lajz;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lajz;-><init>(Lajx;JZZ)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 54
    return-void
.end method

.method public final a(Lans;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lajx;->c:Landroid/os/Handler;

    new-instance v1, Lajy;

    invoke-direct {v1, p0, p1}, Lajy;-><init>(Lajx;Lans;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 44
    return-void
.end method
