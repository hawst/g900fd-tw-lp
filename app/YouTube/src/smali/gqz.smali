.class final Lgqz;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final a:Lgqx;

.field private final b:Ljava/nio/channels/WritableByteChannel;


# direct methods
.method public constructor <init>(Lgqw;Lgqx;Ljava/nio/channels/WritableByteChannel;)V
    .locals 1

    .prologue
    .line 398
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 399
    const-string v0, "Trimmed ISO file writer"

    invoke-virtual {p0, v0}, Lgqz;->setName(Ljava/lang/String;)V

    .line 400
    iput-object p2, p0, Lgqz;->a:Lgqx;

    .line 401
    iput-object p3, p0, Lgqz;->b:Ljava/nio/channels/WritableByteChannel;

    .line 402
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 407
    :try_start_0
    iget-object v0, p0, Lgqz;->a:Lgqx;

    invoke-virtual {v0}, Lgqx;->a()Lye;

    move-result-object v0

    iget-object v1, p0, Lgqz;->b:Ljava/nio/channels/WritableByteChannel;

    invoke-virtual {v0, v1}, Lye;->a(Ljava/nio/channels/WritableByteChannel;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    :try_start_1
    iget-object v0, p0, Lgqz;->b:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 416
    :goto_0
    iget-object v0, p0, Lgqz;->a:Lgqx;

    invoke-virtual {v0}, Lgqx;->close()V

    .line 417
    :goto_1
    return-void

    .line 413
    :catch_0
    move-exception v0

    .line 414
    const-string v1, "Failed to close output stream"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 408
    :catch_1
    move-exception v0

    .line 409
    :try_start_2
    const-string v1, "Failed to render video"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 412
    :try_start_3
    iget-object v0, p0, Lgqz;->b:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 416
    :goto_2
    iget-object v0, p0, Lgqz;->a:Lgqx;

    invoke-virtual {v0}, Lgqx;->close()V

    goto :goto_1

    .line 413
    :catch_2
    move-exception v0

    .line 414
    const-string v1, "Failed to close output stream"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 411
    :catchall_0
    move-exception v0

    .line 412
    :try_start_4
    iget-object v1, p0, Lgqz;->b:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v1}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 416
    :goto_3
    iget-object v1, p0, Lgqz;->a:Lgqx;

    invoke-virtual {v1}, Lgqx;->close()V

    throw v0

    .line 413
    :catch_3
    move-exception v1

    .line 414
    const-string v2, "Failed to close output stream"

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method
