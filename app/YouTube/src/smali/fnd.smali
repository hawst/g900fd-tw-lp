.class public final Lfnd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final a:Ljava/util/Set;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/Set;

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lfnd;->a:Ljava/util/Set;

    .line 59
    new-instance v0, Lfne;

    invoke-direct {v0}, Lfne;-><init>()V

    sput-object v0, Lfnd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Leae;)V
    .locals 3

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iget-boolean v0, p1, Leae;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Leae;->b:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lfnd;->b:Ljava/lang/String;

    .line 100
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfnd;->c:Ljava/util/Set;

    .line 101
    iget-object v0, p1, Leae;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 102
    iget-object v2, p0, Lfnd;->c:Ljava/util/Set;

    invoke-static {v0}, Lfnf;->a(I)Lfnf;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 99
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 104
    :cond_1
    iget-boolean v0, p1, Leae;->d:Z

    if-eqz v0, :cond_2

    .line 105
    iget v0, p1, Leae;->e:I

    :goto_2
    iput v0, p0, Lfnd;->d:I

    .line 106
    return-void

    .line 105
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public constructor <init>(Lhxq;Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iget-object v0, p1, Lhxq;->b:Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfnd;->b:Ljava/lang/String;

    .line 82
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lfnd;->c:Ljava/util/Set;

    .line 83
    iget v0, p1, Lhxq;->c:I

    if-eqz v0, :cond_0

    iget v0, p1, Lhxq;->c:I

    :goto_0
    iput v0, p0, Lfnd;->d:I

    .line 86
    return-void

    .line 83
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 95
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;I)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Set;I)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfnd;->b:Ljava/lang/String;

    .line 90
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lfnd;->c:Ljava/util/Set;

    .line 91
    iput p3, p0, Lfnd;->d:I

    .line 92
    return-void
.end method

.method private a(Lfnd;)I
    .locals 2

    .prologue
    .line 164
    iget v0, p0, Lfnd;->d:I

    iget v1, p1, Lfnd;->d:I

    if-eq v0, v1, :cond_1

    .line 165
    iget v0, p0, Lfnd;->d:I

    iget v1, p1, Lfnd;->d:I

    if-ge v0, v1, :cond_0

    const/4 v0, -0x1

    .line 167
    :goto_0
    return v0

    .line 165
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 167
    :cond_1
    iget-object v0, p0, Lfnd;->b:Ljava/lang/String;

    iget-object v1, p1, Lfnd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Lfnd;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    :goto_0
    return p1

    :cond_0
    iget p1, p0, Lfnd;->d:I

    goto :goto_0
.end method

.method public final a()Leae;
    .locals 3

    .prologue
    .line 144
    new-instance v0, Leae;

    invoke-direct {v0}, Leae;-><init>()V

    iget-object v1, p0, Lfnd;->b:Ljava/lang/String;

    .line 145
    invoke-virtual {v0, v1}, Leae;->a(Ljava/lang/String;)Leae;

    move-result-object v0

    iget v1, p0, Lfnd;->d:I

    .line 146
    invoke-virtual {v0, v1}, Leae;->b(I)Leae;

    move-result-object v1

    .line 147
    iget-object v0, p0, Lfnd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnf;

    .line 148
    invoke-static {v0}, Lfnf;->a(Lfnf;)I

    move-result v0

    invoke-virtual {v1, v0}, Leae;->a(I)Leae;

    goto :goto_0

    .line 150
    :cond_0
    return-object v1
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lfnd;

    invoke-direct {p0, p1}, Lfnd;->a(Lfnd;)I

    move-result v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 181
    instance-of v1, p1, Lfnd;

    if-eqz v1, :cond_1

    .line 182
    check-cast p1, Lfnd;

    .line 183
    if-eq p0, p1, :cond_0

    .line 184
    invoke-direct {p1, p0}, Lfnd;->a(Lfnd;)I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lfnd;->hashCode()I

    move-result v1

    invoke-virtual {p1}, Lfnd;->hashCode()I

    move-result v2

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 186
    :cond_1
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lfnd;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 194
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lfnd;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfnd;->d:I

    add-int/2addr v0, v1

    .line 196
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "@"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 202
    iget v1, p0, Lfnd;->d:I

    .line 203
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s->"

    .line 204
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lfnd;->b:Ljava/lang/String;

    .line 205
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lfnd;->c:Ljava/util/Set;

    .line 206
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lfnd;->a()Leae;

    move-result-object v0

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lida;)V

    .line 116
    return-void
.end method
