.class public final Laea;
.super Lded;
.source "SourceFile"

# interfaces
.implements Ladr;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Laet;

.field private final c:Landroid/view/View;

.field private final d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/Button;

.field private final i:Landroid/widget/Button;

.field private final j:Ldel;

.field private final k:I

.field private final l:Landroid/os/Handler;

.field private m:Ldbt;

.field private n:Ldbu;

.field private o:Ldbs;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:I

.field private t:Z

.field private final u:Lddf;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 52
    const/16 v1, 0x5a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lddk;->a:Lddk;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const/16 v1, 0x59

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lddk;->b:Lddk;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const/16 v1, 0x56

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lddk;->c:Lddk;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const/16 v1, 0x7f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lddk;->c:Lddk;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    const/16 v1, 0x7e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lddk;->d:Lddk;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const/16 v1, 0x58

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lddk;->f:Lddk;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const/16 v1, 0x57

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lddk;->e:Lddk;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Laea;->a:Ljava/util/Map;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Laet;)V
    .locals 4

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lded;-><init>(Landroid/content/Context;)V

    .line 91
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Laea;->c:Landroid/view/View;

    .line 92
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laet;

    iput-object v0, p0, Laea;->b:Laet;

    .line 94
    sget-object v0, Ldbs;->a:Ldbs;

    iput-object v0, p0, Laea;->o:Ldbs;

    .line 96
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 97
    const v1, 0x7f040028

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 100
    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Laea;->k:I

    .line 101
    new-instance v0, Laeb;

    invoke-direct {v0, p0}, Laeb;-><init>(Laea;)V

    iput-object v0, p0, Laea;->l:Landroid/os/Handler;

    .line 110
    const v0, 0x7f0800ee

    invoke-virtual {p0, v0}, Laea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iput-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    .line 111
    const v0, 0x7f0800ed

    invoke-virtual {p0, v0}, Laea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laea;->e:Landroid/view/View;

    .line 112
    const v0, 0x7f0800ea

    invoke-virtual {p0, v0}, Laea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laea;->f:Landroid/view/View;

    .line 113
    const v0, 0x7f0800eb

    invoke-virtual {p0, v0}, Laea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laea;->g:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f0800ec

    invoke-virtual {p0, v0}, Laea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Laea;->h:Landroid/widget/Button;

    .line 116
    iget-object v0, p0, Laea;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    new-instance v1, Lddf;

    invoke-virtual {p0}, Laea;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Laed;

    invoke-direct {v3, p0}, Laed;-><init>(Laea;)V

    const v0, 0x7f0800f0

    .line 119
    invoke-virtual {p0, v0}, Laea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, v2, v3, v0}, Lddf;-><init>(Landroid/content/Context;Lddu;Landroid/widget/ImageView;)V

    iput-object v1, p0, Laea;->u:Lddf;

    .line 120
    iget-object v1, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v2, p0, Laea;->u:Lddf;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddf;

    iput-object v0, v1, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->g:Lddf;

    iget-object v3, v1, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->f:Laeh;

    new-instance v0, Laee;

    invoke-direct {v0, v1, v2}, Laee;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;Lddf;)V

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laej;

    iput-object v0, v3, Laeh;->e:Laej;

    .line 121
    iget-object v1, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    const v0, 0x7f08000b

    sget-object v2, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddk;

    iget-object v2, v1, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->d:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(Lddk;)V

    .line 122
    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    const v1, 0x7f0800ef

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Laea;->i:Landroid/widget/Button;

    .line 123
    iget-object v0, p0, Laea;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    new-instance v0, Ldel;

    invoke-direct {v0, p1}, Ldel;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laea;->j:Ldel;

    .line 127
    invoke-virtual {p0}, Laea;->e()V

    .line 128
    return-void
.end method

.method static synthetic a(Laea;)Ldbs;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Laea;->o:Ldbs;

    return-object v0
.end method

.method private a(I)Lddk;
    .locals 2

    .prologue
    .line 380
    sget-object v0, Laea;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    sget-object v0, Laea;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddk;

    .line 386
    :goto_0
    return-object v0

    .line 383
    :cond_0
    const/16 v0, 0x55

    if-eq p1, v0, :cond_1

    const/16 v0, 0x3e

    if-ne p1, v0, :cond_3

    .line 384
    :cond_1
    iget-object v0, p0, Laea;->o:Ldbs;

    sget-object v1, Ldbs;->b:Ldbs;

    if-ne v0, v1, :cond_2

    sget-object v0, Lddk;->c:Lddk;

    goto :goto_0

    :cond_2
    sget-object v0, Lddk;->d:Lddk;

    goto :goto_0

    .line 386
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Laea;)Ldbt;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Laea;->m:Ldbt;

    return-object v0
.end method

.method static synthetic c(Laea;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Laea;->s:I

    return v0
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 147
    iget-object v0, p0, Laea;->l:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 148
    iget-object v0, p0, Laea;->o:Ldbs;

    sget-object v1, Ldbs;->b:Ldbs;

    if-ne v0, v1, :cond_0

    .line 149
    iget-object v0, p0, Laea;->l:Landroid/os/Handler;

    iget v1, p0, Laea;->k:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 151
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return v0
.end method

.method public final a(III)V
    .locals 4

    .prologue
    .line 256
    iput p1, p0, Laea;->s:I

    .line 257
    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v0, v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->f:Laeh;

    div-int/lit16 v1, p1, 0x3e8

    div-int/lit16 v2, p2, 0x3e8

    div-int/lit16 v3, p3, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Laeh;->a(III)V

    .line 258
    return-void
.end method

.method public final a(Lads;)V
    .locals 0

    .prologue
    .line 333
    return-void
.end method

.method public final a(Ldbs;)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Laea;->o:Ldbs;

    if-eq v0, p1, :cond_0

    .line 190
    iput-object p1, p0, Laea;->o:Ldbs;

    .line 191
    invoke-virtual {p0}, Laea;->d()V

    .line 193
    :cond_0
    sget-object v0, Ldbs;->c:Ldbs;

    if-ne p1, v0, :cond_1

    .line 194
    iget-object v0, p0, Laea;->l:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 196
    :cond_1
    return-void
.end method

.method public final a(Ldbt;)V
    .locals 2

    .prologue
    .line 137
    new-instance v0, Lafg;

    iget-object v1, p0, Laea;->b:Laet;

    invoke-direct {v0, p1, v1}, Lafg;-><init>(Ldbt;Laet;)V

    iput-object v0, p0, Laea;->m:Ldbt;

    .line 138
    return-void
.end method

.method public final a(Ldbu;)V
    .locals 4

    .prologue
    .line 142
    iput-object p1, p0, Laea;->n:Ldbu;

    .line 143
    iget-object v1, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iput-object p1, v1, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->h:Ldbu;

    iget-object v2, v1, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->f:Laeh;

    iget-boolean v0, p1, Ldbu;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v3, v2, Laeh;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, v2, Laeh;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v2, Laeh;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v1, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddk;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(Lddk;)V

    goto :goto_1

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 144
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 272
    if-eqz p2, :cond_0

    .line 273
    sget-object v0, Ldbs;->e:Ldbs;

    iput-object v0, p0, Laea;->o:Ldbs;

    iget-object v0, p0, Laea;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Laea;->h:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Laea;->d()V

    .line 277
    :goto_0
    iput-boolean p2, p0, Laea;->r:Z

    .line 278
    return-void

    .line 275
    :cond_0
    sget-object v0, Ldbs;->f:Ldbs;

    iput-object v0, p0, Laea;->o:Ldbs;

    iget-object v0, p0, Laea;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Laea;->h:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Laea;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Laea;->j:Ldel;

    new-instance v1, Laec;

    invoke-direct {v1, p0}, Laec;-><init>(Laea;)V

    invoke-virtual {v0, p1, v1}, Ldel;->a(Ljava/util/List;Lden;)V

    .line 302
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 534
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 214
    if-eqz p1, :cond_0

    .line 216
    const-string v0, "MINIMAL mode is not supported for Google TV controls"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, La;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    :cond_0
    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 531
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 222
    iput-boolean p1, p0, Laea;->p:Z

    .line 223
    if-eqz p1, :cond_0

    .line 224
    invoke-virtual {p0}, Laea;->e()V

    .line 226
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 240
    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v0, v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->f:Laeh;

    invoke-virtual {v0, v1, v1, v1}, Laeh;->a(III)V

    .line 241
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 528
    return-void
.end method

.method public final d()V
    .locals 8

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 159
    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Laea;->q:Z

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    .line 161
    :goto_0
    iget-object v5, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v3, p0, Laea;->o:Ldbs;

    sget-object v6, Ldbs;->b:Ldbs;

    if-ne v3, v6, :cond_4

    move v3, v2

    :goto_1
    invoke-virtual {v5, v3}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(I)V

    .line 164
    iget-object v5, p0, Laea;->e:Landroid/view/View;

    iget-object v3, p0, Laea;->o:Ldbs;

    sget-object v6, Ldbs;->d:Ldbs;

    if-ne v3, v6, :cond_5

    move v3, v1

    :goto_2
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 165
    iget-object v5, p0, Laea;->f:Landroid/view/View;

    iget-object v3, p0, Laea;->o:Ldbs;

    invoke-virtual {v3}, Ldbs;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v1

    :goto_3
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 166
    iget-object v5, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v3, p0, Laea;->o:Ldbs;

    invoke-virtual {v3}, Ldbs;->a()Z

    move-result v6

    if-nez v6, :cond_7

    move v3, v2

    :goto_4
    sget-object v7, Lddk;->d:Lddk;

    invoke-virtual {v5, v3, v7}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    if-nez v6, :cond_8

    move v3, v2

    :goto_5
    sget-object v7, Lddk;->g:Lddk;

    invoke-virtual {v5, v3, v7}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    if-nez v6, :cond_9

    move v3, v2

    :goto_6
    sget-object v7, Lddk;->a:Lddk;

    invoke-virtual {v5, v3, v7}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    if-nez v6, :cond_a

    move v3, v2

    :goto_7
    sget-object v7, Lddk;->b:Lddk;

    invoke-virtual {v5, v3, v7}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    if-nez v6, :cond_b

    :goto_8
    sget-object v3, Lddk;->i:Lddk;

    invoke-virtual {v5, v2, v3}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    .line 167
    iget-object v2, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v3, p0, Laea;->o:Ldbs;

    sget-object v5, Ldbs;->d:Ldbs;

    if-eq v3, v5, :cond_1

    iget-object v3, p0, Laea;->o:Ldbs;

    sget-object v5, Ldbs;->a:Ldbs;

    if-eq v3, v5, :cond_1

    move v4, v1

    :cond_1
    invoke-virtual {v2, v4}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setVisibility(I)V

    .line 170
    iget-boolean v2, p0, Laea;->p:Z

    if-eqz v2, :cond_c

    iget-object v2, p0, Laea;->o:Ldbs;

    invoke-virtual {v2}, Ldbs;->a()Z

    move-result v2

    if-nez v2, :cond_c

    iget-object v2, p0, Laea;->o:Ldbs;

    invoke-virtual {v2}, Ldbs;->a()Z

    move-result v2

    if-nez v2, :cond_c

    .line 171
    invoke-virtual {p0}, Laea;->e()V

    .line 181
    :goto_9
    invoke-direct {p0}, Laea;->g()V

    .line 182
    iget-object v0, p0, Laea;->m:Ldbt;

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p0, Laea;->m:Ldbt;

    invoke-interface {v0}, Ldbt;->i()V

    .line 185
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 159
    goto/16 :goto_0

    .line 161
    :cond_4
    const/4 v3, 0x2

    goto/16 :goto_1

    :cond_5
    move v3, v4

    .line 164
    goto :goto_2

    :cond_6
    move v3, v4

    .line 165
    goto :goto_3

    :cond_7
    move v3, v1

    .line 166
    goto :goto_4

    :cond_8
    move v3, v1

    goto :goto_5

    :cond_9
    move v3, v1

    goto :goto_6

    :cond_a
    move v3, v1

    goto :goto_7

    :cond_b
    move v2, v1

    goto :goto_8

    .line 173
    :cond_c
    iget-object v2, p0, Laea;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_d

    if-eqz v0, :cond_d

    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    .line 174
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_d

    .line 175
    iget-object v2, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    sget-object v0, Lddk;->d:Lddk;

    sget-object v3, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 177
    :cond_d
    iput-boolean v1, p0, Laea;->q:Z

    .line 178
    invoke-virtual {p0, v1}, Laea;->setVisibility(I)V

    goto :goto_9
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    sget-object v1, Lddk;->e:Lddk;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    .line 263
    return-void
.end method

.method public final dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 375
    invoke-direct {p0}, Laea;->g()V

    .line 376
    invoke-super {p0, p1}, Lded;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Laea;->q:Z

    .line 231
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Laea;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Laea;->m:Ldbt;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Laea;->m:Ldbt;

    invoke-interface {v0}, Ldbt;->j()V

    .line 236
    :cond_0
    return-void
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    sget-object v1, Lddk;->f:Lddk;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    .line 268
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Laea;->j:Ldel;

    invoke-virtual {v0}, Ldel;->a()V

    .line 343
    return-void
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 516
    iput-boolean p1, p0, Laea;->t:Z

    .line 517
    iget-object v0, p0, Laea;->i:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setSelected(Z)V

    .line 518
    return-void
.end method

.method public final g(Z)V
    .locals 2

    .prologue
    .line 511
    iget-object v1, p0, Laea;->i:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 512
    return-void

    .line 511
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final h(Z)V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    sget-object v1, Lddk;->a:Lddk;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    sget-object v1, Lddk;->b:Lddk;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    sget-object v1, Lddk;->i:Lddk;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(ZLddk;)V

    .line 246
    iget-object v0, p0, Laea;->u:Lddf;

    iput-boolean p1, v0, Lddf;->c:Z

    iget-object v1, v0, Lddf;->d:Lddk;

    sget-object v2, Lddk;->b:Lddk;

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lddf;->d:Lddk;

    sget-object v2, Lddk;->a:Lddk;

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, v0, Lddf;->d:Lddk;

    invoke-virtual {v0, v1}, Lddf;->a(Lddk;)V

    .line 247
    :cond_1
    return-void
.end method

.method public final i(Z)V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method public final j(Z)V
    .locals 3

    .prologue
    .line 522
    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    if-eqz p1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->d:Ljava/util/Set;

    sget-object v2, Lddk;->g:Lddk;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    sget-object v1, Lddk;->g:Lddk;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(Lddk;)V

    .line 523
    return-void

    .line 522
    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->d:Ljava/util/Set;

    sget-object v2, Lddk;->g:Lddk;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final k(Z)V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Laea;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v0, v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setSelected(Z)V

    .line 292
    return-void
.end method

.method public final l(Z)V
    .locals 0

    .prologue
    .line 317
    return-void
.end method

.method public final m(Z)V
    .locals 0

    .prologue
    .line 322
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 364
    iget-object v1, p0, Laea;->h:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    iget-boolean v1, p0, Laea;->r:Z

    if-eqz v1, :cond_1

    .line 365
    iput-boolean v0, p0, Laea;->r:Z

    .line 366
    iget-object v0, p0, Laea;->m:Ldbt;

    invoke-interface {v0}, Ldbt;->k()V

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    iget-object v1, p0, Laea;->i:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    .line 368
    iget-boolean v1, p0, Laea;->t:Z

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-virtual {p0, v0}, Laea;->f(Z)V

    .line 369
    iget-object v0, p0, Laea;->m:Ldbt;

    iget-boolean v1, p0, Laea;->t:Z

    invoke-interface {v0, v1}, Ldbt;->a(Z)V

    goto :goto_0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Laea;->d()V

    .line 348
    invoke-super {p0, p1}, Lded;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 391
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    .line 392
    :goto_0
    invoke-direct {p0, p1}, Laea;->a(I)Lddk;

    move-result-object v4

    .line 393
    if-eqz v4, :cond_5

    .line 394
    iget-object v3, p0, Laea;->u:Lddf;

    iget-object v5, p0, Laea;->n:Ldbu;

    invoke-virtual {v3, v4, v5}, Lddf;->a(Lddk;Ldbu;)I

    move-result v3

    if-ne v3, v1, :cond_2

    move v3, v1

    :goto_1
    if-nez v3, :cond_3

    .line 424
    :cond_0
    :goto_2
    return v1

    :cond_1
    move v0, v2

    .line 391
    goto :goto_0

    :cond_2
    move v3, v2

    .line 394
    goto :goto_1

    .line 397
    :cond_3
    if-nez v0, :cond_0

    .line 398
    iget-object v0, p0, Laea;->u:Lddf;

    iget-object v3, v0, Lddf;->d:Lddk;

    if-nez v3, :cond_0

    invoke-virtual {v4}, Lddk;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    iput-object v4, v0, Lddf;->d:Lddk;

    invoke-virtual {v0, v4, v2}, Lddf;->a(Lddk;Z)V

    invoke-virtual {v4}, Lddk;->a()Z

    move-result v2

    invoke-static {v2}, Lb;->b(Z)V

    iput-object v4, v0, Lddf;->d:Lddk;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, v0, Lddf;->b:D

    invoke-virtual {v0, v4}, Lddf;->b(Lddk;)V

    invoke-virtual {v0, v4}, Lddf;->c(Lddk;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v0, v4, v1}, Lddf;->a(Lddk;Z)V

    invoke-virtual {v4, v0}, Lddk;->a(Lddf;)V

    goto :goto_2

    .line 403
    :cond_5
    sparse-switch p1, :sswitch_data_0

    .line 424
    invoke-super {p0, p1, p2}, Lded;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_2

    .line 410
    :sswitch_0
    iget-boolean v0, p0, Laea;->q:Z

    if-eqz v0, :cond_6

    .line 411
    invoke-virtual {p0}, Laea;->d()V

    goto :goto_2

    :cond_6
    move v1, v2

    .line 414
    goto :goto_2

    .line 417
    :sswitch_1
    iget-boolean v0, p0, Laea;->q:Z

    if-nez v0, :cond_7

    .line 418
    invoke-virtual {p0}, Laea;->e()V

    goto :goto_2

    :cond_7
    move v1, v2

    .line 421
    goto :goto_2

    .line 403
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 430
    invoke-direct {p0, p1}, Laea;->a(I)Lddk;

    move-result-object v0

    .line 431
    if-eqz v0, :cond_0

    .line 432
    iget-object v1, p0, Laea;->u:Lddf;

    invoke-virtual {v1, v0}, Lddf;->a(Lddk;)V

    .line 433
    const/4 v0, 0x1

    .line 436
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lded;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 353
    invoke-super {p0, p1}, Lded;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 359
    :goto_0
    return v0

    .line 355
    :cond_0
    iget-boolean v1, p0, Laea;->q:Z

    if-eqz v1, :cond_1

    .line 356
    invoke-virtual {p0}, Laea;->d()V

    goto :goto_0

    .line 359
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r_()Ldeg;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 132
    new-instance v0, Ldeg;

    invoke-direct {v0, v1, v1}, Ldeg;-><init>(II)V

    return-object v0
.end method
