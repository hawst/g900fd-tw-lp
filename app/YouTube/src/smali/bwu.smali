.class public abstract Lbwu;
.super Lbzl;
.source "SourceFile"


# instance fields
.field public final a:Lbfl;

.field final b:Lbxu;

.field final c:Landroid/content/SharedPreferences;

.field final d:Landroid/view/ViewGroup;

.field final e:Ljava/lang/String;

.field f:Ljava/lang/Boolean;

.field g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

.field private h:Lbfm;

.field private final i:Landroid/app/Activity;

.field private final j:I

.field private final k:I

.field private l:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lbxu;Landroid/content/SharedPreferences;Lbfm;Lbfl;ILjava/lang/String;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lbzl;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lbwu;->f:Ljava/lang/Boolean;

    .line 53
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbwu;->i:Landroid/app/Activity;

    .line 54
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxu;

    iput-object v0, p0, Lbwu;->b:Lbxu;

    .line 55
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lbwu;->c:Landroid/content/SharedPreferences;

    .line 56
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfm;

    iput-object v0, p0, Lbwu;->h:Lbfm;

    .line 57
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfl;

    iput-object v0, p0, Lbwu;->a:Lbfl;

    .line 58
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lbwu;->j:I

    .line 59
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbwu;->e:Ljava/lang/String;

    .line 60
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lbwu;->k:I

    .line 61
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lbwu;->d:Landroid/view/ViewGroup;

    .line 62
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lbwu;->j:I

    return v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 138
    iput-object p1, p0, Lbwu;->l:Landroid/view/View;

    .line 139
    iget-object v0, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    iget-object v1, p0, Lbwu;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->a(Landroid/view/View;Landroid/view/View;)V

    .line 142
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 71
    iget-object v2, p0, Lbwu;->a:Lbfl;

    iget-boolean v2, v2, Lbfl;->a:Z

    if-eqz v2, :cond_8

    .line 72
    iget-object v2, p0, Lbwu;->l:Landroid/view/View;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lbwu;->h:Lbfm;

    iget v2, v2, Lbfm;->h:I

    if-ne v2, v4, :cond_3

    iget-object v2, p0, Lbwu;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_8

    iget-object v2, p0, Lbwu;->h:Lbfm;

    .line 73
    iget-boolean v3, v2, Lbfm;->b:Z

    if-eqz v3, :cond_7

    iget-boolean v3, v2, Lbfm;->a:Z

    if-nez v3, :cond_7

    iget-boolean v3, v2, Lbfm;->c:Z

    if-nez v3, :cond_7

    iget-boolean v3, v2, Lbfm;->d:Z

    if-eqz v3, :cond_7

    iget-boolean v3, v2, Lbfm;->e:Z

    if-eqz v3, :cond_0

    iget v3, v2, Lbfm;->h:I

    if-ne v3, v4, :cond_7

    :cond_0
    iget v3, v2, Lbfm;->h:I

    if-eq v3, v0, :cond_1

    iget-boolean v2, v2, Lbfm;->f:Z

    if-eqz v2, :cond_7

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_8

    :goto_2
    return v0

    :cond_2
    move v2, v1

    .line 72
    goto :goto_0

    :cond_3
    iget-object v2, p0, Lbwu;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lbwu;->h:Lbfm;

    iget-boolean v2, v2, Lbfm;->g:Z

    if-eqz v2, :cond_5

    :cond_4
    iget-object v2, p0, Lbwu;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    move v2, v0

    goto :goto_0

    :cond_6
    move v2, v1

    goto :goto_0

    :cond_7
    move v2, v1

    .line 73
    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_2
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lbwu;->i:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 95
    iget v1, p0, Lbwu;->k:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    iput-object v0, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    .line 96
    iget-object v0, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    new-instance v1, Lbwv;

    invoke-direct {v1, p0}, Lbwv;-><init>(Lbwu;)V

    iget-object v0, v0, Lbqr;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v0, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    iget-object v1, p0, Lbwu;->d:Landroid/view/ViewGroup;

    iget-object v2, p0, Lbwu;->l:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->a(Landroid/view/View;Landroid/view/View;)V

    .line 118
    :cond_0
    iget-object v0, p0, Lbwu;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-gez v0, :cond_1

    .line 119
    iget-object v0, p0, Lbwu;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 121
    :cond_1
    iget-object v0, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->a()V

    .line 122
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lbwu;->d:Landroid/view/ViewGroup;

    new-instance v1, Lbww;

    invoke-direct {v1, p0}, Lbww;-><init>(Lbwu;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 133
    iget-object v0, p0, Lbwu;->g:Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ClingTutorialLayout;->b()V

    .line 135
    :cond_0
    return-void
.end method
