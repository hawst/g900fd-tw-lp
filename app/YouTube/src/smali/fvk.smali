.class public final Lfvk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfuv;


# instance fields
.field private final a:Lfsi;

.field private final b:Levn;

.field private final c:Lfnh;

.field private final d:Lfuh;

.field private e:Z


# direct methods
.method public constructor <init>(Lfuu;Levn;Lfnh;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lfvk;->b:Levn;

    .line 58
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnh;

    iput-object v0, p0, Lfvk;->c:Lfnh;

    .line 59
    const-class v0, Lfnh;

    invoke-virtual {p1, v0}, Lfuu;->a(Ljava/lang/Class;)V

    .line 60
    new-instance v1, Lfsi;

    invoke-virtual {p1}, Lfuu;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsl;

    invoke-direct {v1, v0}, Lfsi;-><init>(Lfsl;)V

    iput-object v1, p0, Lfvk;->a:Lfsi;

    .line 61
    iput-boolean v3, p0, Lfvk;->e:Z

    .line 62
    new-instance v0, Lfuh;

    invoke-direct {v0}, Lfuh;-><init>()V

    iput-object v0, p0, Lfvk;->d:Lfuh;

    .line 64
    invoke-virtual {p2, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 66
    invoke-virtual {p3}, Lfnh;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p3}, Lfnh;->c()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 70
    invoke-virtual {p0}, Lfvk;->c()V

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iput-boolean v3, p0, Lfvk;->e:Z

    iget-object v0, p0, Lfvk;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    iget-object v0, p0, Lfvk;->a:Lfsi;

    iget-object v1, p0, Lfvk;->c:Lfnh;

    invoke-virtual {v0, v1}, Lfsi;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lfvk;->a:Lfsi;

    iget-object v1, p0, Lfvk;->c:Lfnh;

    invoke-virtual {v1}, Lfnh;->a()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lfvk;->c:Lfnh;

    invoke-virtual {v2}, Lfnh;->c()I

    move-result v2

    invoke-interface {v1, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lfvk;->c:Lfnh;

    invoke-virtual {v0}, Lfnh;->f()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfvk;->d:Lfuh;

    iget-object v1, p0, Lfvk;->c:Lfnh;

    invoke-virtual {v1}, Lfnh;->f()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lfuh;->a:Ljava/lang/CharSequence;

    iget-object v0, p0, Lfvk;->d:Lfuh;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lfuh;->b:Z

    iget-object v0, p0, Lfvk;->d:Lfuh;

    new-instance v1, Lfvl;

    invoke-direct {v1, p0}, Lfvl;-><init>(Lfvk;)V

    iput-object v1, v0, Lfuh;->c:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lfvk;->d:Lfuh;

    const/4 v1, 0x0

    iput-object v1, v0, Lfuh;->d:Lhog;

    iget-object v0, p0, Lfvk;->a:Lfsi;

    iget-object v1, p0, Lfvk;->d:Lfuh;

    invoke-virtual {v0, v1}, Lfsi;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public final b()Lfsi;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lfvk;->a:Lfsi;

    return-object v0
.end method

.method c()V
    .locals 2

    .prologue
    .line 137
    iget-boolean v0, p0, Lfvk;->e:Z

    if-eqz v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfvk;->e:Z

    .line 142
    iget-object v0, p0, Lfvk;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 143
    iget-object v0, p0, Lfvk;->a:Lfsi;

    iget-object v1, p0, Lfvk;->c:Lfnh;

    invoke-virtual {v0, v1}, Lfsi;->b(Ljava/lang/Object;)V

    .line 144
    iget-object v0, p0, Lfvk;->a:Lfsi;

    iget-object v1, p0, Lfvk;->c:Lfnh;

    invoke-virtual {v1}, Lfnh;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/util/Collection;)V

    .line 145
    iget-object v0, p0, Lfvk;->c:Lfnh;

    iget-object v0, v0, Lfmr;->a:Lhvp;

    iget-object v0, v0, Lhvp;->c:Lhog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfvk;->c:Lfnh;

    .line 146
    invoke-virtual {v0}, Lfnh;->i()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    iget-object v0, p0, Lfvk;->d:Lfuh;

    iget-object v1, p0, Lfvk;->c:Lfnh;

    invoke-virtual {v1}, Lfnh;->i()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lfuh;->a:Ljava/lang/CharSequence;

    .line 148
    iget-object v0, p0, Lfvk;->d:Lfuh;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lfuh;->b:Z

    .line 149
    iget-object v0, p0, Lfvk;->d:Lfuh;

    const/4 v1, 0x0

    iput-object v1, v0, Lfuh;->c:Landroid/view/View$OnClickListener;

    .line 150
    iget-object v0, p0, Lfvk;->d:Lfuh;

    iget-object v1, p0, Lfvk;->c:Lfnh;

    iget-object v1, v1, Lfmr;->a:Lhvp;

    iget-object v1, v1, Lhvp;->c:Lhog;

    iput-object v1, v0, Lfuh;->d:Lhog;

    .line 151
    iget-object v0, p0, Lfvk;->a:Lfsi;

    iget-object v1, p0, Lfvk;->d:Lfuh;

    invoke-virtual {v0, v1}, Lfsi;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lfvk;->b:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 88
    return-void
.end method

.method public final handleHideEnclosingActionEvent(Lfhg;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 103
    iget-object v0, p1, Lfgz;->a:Ljava/lang/Object;

    iget-object v1, p0, Lfvk;->c:Lfnh;

    if-ne v0, v1, :cond_0

    .line 104
    iget-object v0, p0, Lfvk;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lfvk;->a:Lfsi;

    iget-object v1, p1, Lfgz;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final handleServiceResponseRemoveEvent(Lfml;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 93
    iget-object v0, p1, Lfmk;->b:Ljava/lang/Object;

    iget-object v1, p0, Lfvk;->c:Lfnh;

    if-ne v0, v1, :cond_0

    .line 94
    iget-object v0, p0, Lfvk;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lfvk;->a:Lfsi;

    iget-object v1, p1, Lfmk;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/lang/Object;)Z

    goto :goto_0
.end method
