.class public Lcnu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lery;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lexd;

.field public final e:Ljava/lang/String;

.field public final f:Lezf;

.field final g:Lcwq;

.field private final h:Lezj;

.field private final i:Landroid/content/SharedPreferences;

.field private final j:Lgix;

.field private k:J


# direct methods
.method protected constructor <init>(Lcnv;)V
    .locals 6

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iget-object v0, p1, Lcnv;->a:Lery;

    iput-object v0, p0, Lcnu;->a:Lery;

    .line 47
    iget-object v0, p1, Lcnv;->b:Lezj;

    iput-object v0, p0, Lcnu;->h:Lezj;

    .line 48
    iget-object v0, p1, Lcnv;->c:Ljava/lang/String;

    iput-object v0, p0, Lcnu;->b:Ljava/lang/String;

    .line 49
    iget-object v0, p1, Lcnv;->d:Ljava/lang/String;

    iput-object v0, p0, Lcnu;->c:Ljava/lang/String;

    .line 50
    iget-object v0, p1, Lcnv;->e:Lexd;

    iput-object v0, p0, Lcnu;->d:Lexd;

    .line 51
    iget-object v0, p1, Lcnv;->f:Lglm;

    .line 52
    iget-object v0, p1, Lcnv;->g:Lcwg;

    .line 53
    iget-object v0, p1, Lcnv;->h:Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcnu;->i:Landroid/content/SharedPreferences;

    .line 54
    iget-object v0, p1, Lcnv;->i:Lgix;

    iput-object v0, p0, Lcnu;->j:Lgix;

    .line 55
    iget-object v0, p1, Lcnv;->j:Ljava/lang/String;

    iput-object v0, p0, Lcnu;->e:Ljava/lang/String;

    .line 56
    iget-object v0, p1, Lcnv;->k:Lezf;

    iput-object v0, p0, Lcnu;->f:Lezf;

    .line 57
    iget-object v0, p1, Lcnv;->l:Lcwq;

    iput-object v0, p0, Lcnu;->g:Lcwq;

    .line 58
    iget-object v0, p0, Lcnu;->h:Lezj;

    .line 59
    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcnu;->i:Landroid/content/SharedPreferences;

    const-string v3, "last_ad_time"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 58
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcnu;->k:J

    .line 60
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcnu;->j:Lgix;

    invoke-interface {v0}, Lgix;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 121
    iput-wide p1, p0, Lcnu;->k:J

    .line 122
    iget-object v0, p0, Lcnu;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_ad_time"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 123
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcnu;->k:J

    return-wide v0
.end method

.method public c()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 87
    iget-wide v2, p0, Lcnu;->k:J

    cmp-long v1, v2, v6

    if-gtz v1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    iget-object v1, p0, Lcnu;->h:Lezj;

    invoke-virtual {v1}, Lezj;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcnu;->k:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 92
    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    cmp-long v1, v2, v6

    if-lez v1, :cond_0

    long-to-int v0, v2

    goto :goto_0
.end method
