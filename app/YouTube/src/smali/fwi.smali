.class public final Lfwi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;
.implements Lgir;


# instance fields
.field private final a:Lfws;

.field private final b:Landroid/content/SharedPreferences;

.field private volatile c:Z

.field private final d:Landroid/os/ConditionVariable;

.field private volatile e:Lgbl;

.field private volatile f:Lgis;


# direct methods
.method public constructor <init>(Lfws;Landroid/content/SharedPreferences;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, "deviceRegistrationClient cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfws;

    iput-object v0, p0, Lfwi;->a:Lfws;

    .line 41
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lfwi;->b:Landroid/content/SharedPreferences;

    .line 43
    const-string v0, ""

    invoke-static {p2, v0}, Lgbl;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lgbl;

    move-result-object v0

    iput-object v0, p0, Lfwi;->e:Lgbl;

    .line 44
    iget-object v0, p0, Lfwi;->e:Lgbl;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    :goto_0
    iput-object v0, p0, Lfwi;->d:Landroid/os/ConditionVariable;

    .line 45
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 57
    iget-object v1, p0, Lfwi;->e:Lgbl;

    if-eqz v1, :cond_0

    .line 58
    iget-object v0, p0, Lfwi;->e:Lgbl;

    invoke-virtual {v0, p1}, Lgbl;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    .line 61
    :cond_0
    const/4 v1, 0x0

    .line 63
    monitor-enter p0

    .line 64
    :try_start_0
    iget-boolean v2, p0, Lfwi;->c:Z

    if-nez v2, :cond_3

    .line 65
    const/4 v1, 0x1

    iput-boolean v1, p0, Lfwi;->c:Z

    .line 67
    iget-object v1, p0, Lfwi;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 69
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lfwi;->a:Lfws;

    invoke-interface {v0, p0}, Lfws;->a(Leuc;)V

    .line 77
    :goto_2
    iget-object v0, p0, Lfwi;->f:Lgis;

    if-eqz v0, :cond_2

    .line 78
    iget-object v0, p0, Lfwi;->f:Lgis;

    throw v0

    .line 69
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 74
    :cond_1
    iget-object v0, p0, Lfwi;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    goto :goto_2

    .line 80
    :cond_2
    iget-object v0, p0, Lfwi;->e:Lgbl;

    invoke-virtual {v0, p1}, Lgbl;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lfwi;->e:Lgbl;

    new-instance v0, Lgis;

    invoke-direct {v0, p2}, Lgis;-><init>(Ljava/lang/Throwable;)V

    iput-object v0, p0, Lfwi;->f:Lgis;

    iget-object v0, p0, Lfwi;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfwi;->c:Z

    const-string v0, "device registration failed"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 25
    check-cast p2, Lgbl;

    iput-object p2, p0, Lfwi;->e:Lgbl;

    const/4 v0, 0x0

    iput-object v0, p0, Lfwi;->f:Lgis;

    iget-object v0, p0, Lfwi;->e:Lgbl;

    iget-object v1, p0, Lfwi;->b:Landroid/content/SharedPreferences;

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lgbl;->a(Lgbl;Landroid/content/SharedPreferences;Ljava/lang/String;)V

    iget-object v0, p0, Lfwi;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfwi;->c:Z

    const-string v0, "device registered"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    return-void
.end method
