.class final Lbme;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lgbu;

.field private final b:Lbmd;

.field private final c:Ljava/util/HashSet;

.field private volatile d:Lglw;

.field private volatile e:I

.field private volatile f:I

.field private volatile g:Z

.field private volatile h:Z

.field private synthetic i:Lbla;


# direct methods
.method constructor <init>(Lbla;Lbmd;Lgbu;)V
    .locals 2

    .prologue
    .line 1625
    iput-object p1, p0, Lbme;->i:Lbla;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1626
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iput-object v0, p0, Lbme;->b:Lbmd;

    .line 1627
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    iput-object v0, p0, Lbme;->a:Lgbu;

    .line 1628
    new-instance v0, Ljava/util/HashSet;

    iget v1, p3, Lgbu;->j:I

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lbme;->c:Ljava/util/HashSet;

    .line 1629
    return-void
.end method


# virtual methods
.method declared-synchronized a()I
    .locals 1

    .prologue
    .line 1719
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbme;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lgjm;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 1638
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1639
    invoke-static {p1}, La;->c(Lgjm;)Ljava/lang/String;

    move-result-object v3

    .line 1640
    const/4 v0, 0x0

    .line 1641
    iget-object v6, p0, Lbme;->c:Ljava/util/HashSet;

    invoke-virtual {v6, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1643
    invoke-virtual {p1}, Lgjm;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1646
    iget-object v0, p0, Lbme;->c:Ljava/util/HashSet;

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1647
    iget-object v0, p0, Lbme;->b:Lbmd;

    iget-object v6, p0, Lbme;->a:Lgbu;

    iget-object v6, v6, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v6}, Lbmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1648
    iget-object v0, p1, Lgjm;->c:Lgjn;

    sget-object v3, Lgjn;->d:Lgjn;

    if-ne v0, v3, :cond_0

    .line 1649
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbme;->h:Z

    .line 1652
    :cond_0
    invoke-static {p1}, La;->b(Lgjm;)Ljava/lang/String;

    move-result-object v0

    .line 1653
    iget-object v3, p0, Lbme;->a:Lgbu;

    iget-object v3, v3, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lgjm;->c:Lgjn;

    sget-object v3, Lgjn;->b:Lgjn;

    if-ne v0, v3, :cond_1

    .line 1656
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbme;->g:Z

    .line 1659
    :cond_1
    iget-object v0, p0, Lbme;->a:Lgbu;

    iget v0, v0, Lgbu;->j:I

    if-lez v0, :cond_8

    .line 1660
    iget-object v0, p0, Lbme;->a:Lgbu;

    iget v0, v0, Lgbu;->j:I

    iget-object v3, p0, Lbme;->c:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    sub-int v3, v0, v3

    .line 1661
    iget-object v0, p0, Lbme;->a:Lgbu;

    iget v0, v0, Lgbu;->j:I

    if-ne v3, v0, :cond_4

    .line 1662
    const/16 v0, 0x64

    iput v0, p0, Lbme;->e:I

    .line 1663
    iput v3, p0, Lbme;->f:I

    move v0, v1

    .line 1684
    :goto_0
    iget-boolean v2, p0, Lbme;->g:Z

    if-eqz v2, :cond_2

    iget-object v2, p1, Lgjm;->c:Lgjn;

    sget-object v3, Lgjn;->a:Lgjn;

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 1690
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lbme;->d:Lglw;

    .line 1692
    :cond_3
    iget-object v1, p0, Lbme;->i:Lbla;

    invoke-virtual {p0}, Lbme;->b()Lglw;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lbla;->a(Lglw;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1693
    monitor-exit p0

    return-void

    .line 1666
    :cond_4
    mul-int/lit8 v0, v3, 0x64

    :try_start_1
    iget-object v6, p0, Lbme;->a:Lgbu;

    iget v6, v6, Lgbu;->j:I

    div-int/2addr v0, v6

    .line 1667
    invoke-virtual {p1}, Lgjm;->a()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1668
    iget-wide v6, p1, Lgjm;->f:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_5

    iget-wide v4, p1, Lgjm;->e:J

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    iget-wide v6, p1, Lgjm;->f:J

    div-long/2addr v4, v6

    :cond_5
    long-to-int v4, v4

    iget-object v5, p0, Lbme;->a:Lgbu;

    iget v5, v5, Lgbu;->j:I

    div-int/2addr v4, v5

    add-int/2addr v0, v4

    .line 1670
    :cond_6
    const/16 v4, 0x63

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1671
    iget v4, p0, Lbme;->e:I

    if-gt v0, v4, :cond_7

    iget v4, p0, Lbme;->f:I

    if-le v3, v4, :cond_8

    .line 1676
    :cond_7
    iput v0, p0, Lbme;->e:I

    .line 1677
    iput v3, p0, Lbme;->f:I

    .line 1678
    iget-boolean v0, p0, Lbme;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_8

    move v0, v1

    .line 1679
    goto :goto_0

    .line 1638
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_8
    move v0, v2

    goto :goto_0
.end method

.method declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1632
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 1633
    iget-object v0, p0, Lbme;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1634
    iget-object v0, p0, Lbme;->b:Lbmd;

    iget-object v1, p0, Lbme;->a:Lgbu;

    iget-object v1, v1, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lbmd;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1635
    monitor-exit p0

    return-void

    .line 1632
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method b()Lglw;
    .locals 5

    .prologue
    .line 1723
    iget-object v0, p0, Lbme;->d:Lglw;

    if-nez v0, :cond_0

    .line 1724
    new-instance v0, Lglw;

    iget-object v1, p0, Lbme;->a:Lgbu;

    .line 1725
    invoke-virtual {p0}, Lbme;->a()I

    move-result v2

    iget v3, p0, Lbme;->e:I

    iget-boolean v4, p0, Lbme;->h:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lglw;-><init>(Lgbu;IIZ)V

    iput-object v0, p0, Lbme;->d:Lglw;

    .line 1727
    :cond_0
    iget-object v0, p0, Lbme;->d:Lglw;

    return-object v0
.end method

.method declared-synchronized b(Lgjm;)V
    .locals 3

    .prologue
    .line 1696
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1697
    const/4 v0, 0x0

    .line 1698
    invoke-static {p1}, La;->c(Lgjm;)Ljava/lang/String;

    move-result-object v1

    .line 1699
    iget-object v2, p0, Lbme;->c:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1700
    iget-object v0, p0, Lbme;->b:Lbmd;

    iget-object v2, p0, Lbme;->a:Lgbu;

    iget-object v2, v2, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1703
    invoke-static {p1}, La;->b(Lgjm;)Ljava/lang/String;

    move-result-object v0

    .line 1704
    iget-object v1, p0, Lbme;->a:Lgbu;

    iget-object v1, v1, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbme;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 1705
    :cond_0
    const/4 v0, 0x2

    .line 1709
    :goto_0
    iget-object v1, p0, Lbme;->a:Lgbu;

    iget v1, v1, Lgbu;->j:I

    if-lez v1, :cond_1

    .line 1710
    iget-object v1, p0, Lbme;->a:Lgbu;

    iget v1, v1, Lgbu;->j:I

    iget-object v2, p0, Lbme;->c:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lbme;->f:I

    .line 1711
    iget v1, p0, Lbme;->f:I

    mul-int/lit8 v1, v1, 0x64

    iget-object v2, p0, Lbme;->a:Lgbu;

    iget v2, v2, Lgbu;->j:I

    div-int/2addr v1, v2

    iput v1, p0, Lbme;->e:I

    .line 1713
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lbme;->d:Lglw;

    .line 1715
    :cond_2
    iget-object v1, p0, Lbme;->i:Lbla;

    invoke-virtual {p0}, Lbme;->b()Lglw;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lbla;->a(Lglw;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1716
    monitor-exit p0

    return-void

    .line 1707
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 1696
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
