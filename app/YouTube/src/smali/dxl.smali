.class final Ldxl;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private final a:Ldre;


# direct methods
.method constructor <init>(Landroid/os/Looper;Ldre;)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 218
    iput-object p2, p0, Ldxl;->a:Ldre;

    .line 219
    return-void
.end method

.method private static a(Ldxy;Ldst;)V
    .locals 1

    .prologue
    .line 265
    if-nez p1, :cond_0

    .line 266
    const/4 v0, 0x3

    invoke-interface {p0, v0}, Ldxy;->a(I)V

    .line 270
    :goto_0
    return-void

    .line 268
    :cond_0
    invoke-interface {p0, p1}, Ldxy;->a(Ldst;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldxm;)V
    .locals 4

    .prologue
    .line 255
    iget v0, p1, Ldxm;->e:I

    sget-object v1, Ldxm;->a:[J

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    new-instance v0, Ldxm;

    iget v1, p1, Ldxm;->e:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p1, Ldxm;->c:Ldtd;

    iget-object v3, p1, Ldxm;->d:Ldxy;

    invoke-direct {v0, v1, v2, v3}, Ldxm;-><init>(ILdtd;Ldxy;)V

    .line 256
    :goto_0
    sget-object v1, Ldxm;->b:Ldxm;

    if-ne v0, v1, :cond_1

    .line 257
    iget-object v0, p1, Ldxm;->d:Ldxy;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ldxl;->a(Ldxy;Ldst;)V

    .line 261
    :goto_1
    return-void

    .line 255
    :cond_0
    sget-object v0, Ldxm;->b:Ldxm;

    goto :goto_0

    .line 260
    :cond_1
    const/4 v1, 0x1

    invoke-static {p0, v1, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    sget-object v2, Ldxm;->a:[J

    iget v0, v0, Ldxm;->e:I

    aget-wide v2, v2, v0

    invoke-virtual {p0, v1, v2, v3}, Ldxl;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 227
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 229
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldxm;

    .line 230
    iget-object v1, v0, Ldxm;->c:Ldtd;

    iget-object v2, v0, Ldxm;->d:Ldxy;

    iget-object v3, p0, Ldxl;->a:Ldre;

    invoke-interface {v3, v1}, Ldre;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldst;

    if-eqz v1, :cond_2

    invoke-static {}, Ldxj;->b()Ljava/lang/String;

    const-string v3, "Found screen with id: "

    iget-object v4, v1, Ldst;->b:Ldth;

    invoke-virtual {v4}, Ldth;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_1
    invoke-static {v2, v1}, Ldxl;->a(Ldxy;Ldst;)V

    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_0

    .line 231
    invoke-virtual {p0, v0}, Ldxl;->a(Ldxm;)V

    goto :goto_0

    .line 230
    :cond_1
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 227
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
