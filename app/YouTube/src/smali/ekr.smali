.class public abstract Lekr;
.super Ljava/lang/Object;

# interfaces
.implements Lejk;
.implements Lelb;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/os/Handler;

.field private final c:Landroid/os/Looper;

.field private d:Landroid/os/IInterface;

.field private final e:Ljava/util/ArrayList;

.field private f:Lekw;

.field private volatile g:I

.field private h:Z

.field private final i:Lekz;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lejt;Lejc;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lekr;->e:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput v0, p0, Lekr;->g:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lekr;->h:Z

    invoke-static {p1}, Lb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lekr;->a:Landroid/content/Context;

    const-string v0, "Looper must not be null"

    invoke-static {p2, v0}, Lb;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lekr;->c:Landroid/os/Looper;

    new-instance v0, Lekz;

    invoke-direct {v0, p2, p0}, Lekz;-><init>(Landroid/os/Looper;Lelb;)V

    iput-object v0, p0, Lekr;->i:Lekz;

    new-instance v0, Leks;

    invoke-direct {v0, p0, p2}, Leks;-><init>(Lekr;Landroid/os/Looper;)V

    iput-object v0, p0, Lekr;->b:Landroid/os/Handler;

    invoke-static {p3}, Lb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejt;

    iget-object v1, p0, Lekr;->i:Lekz;

    invoke-virtual {v1, v0}, Lekz;->a(Lejt;)V

    invoke-static {p4}, Lb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejc;

    invoke-virtual {p0, v0}, Lekr;->a(Lejc;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Lejb;Lejc;[Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Leku;

    invoke-direct {v3, p2}, Leku;-><init>(Lejb;)V

    new-instance v4, Lekx;

    invoke-direct {v4, p3}, Lekx;-><init>(Lejc;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lekr;-><init>(Landroid/content/Context;Landroid/os/Looper;Lejt;Lejc;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lekr;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0

    iput-object p1, p0, Lekr;->d:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic a(Lekr;Lekw;)Lekw;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lekr;->f:Lekw;

    return-object v0
.end method

.method static synthetic a(Lekr;)Lekz;
    .locals 1

    iget-object v0, p0, Lekr;->i:Lekz;

    return-object v0
.end method

.method private a(I)V
    .locals 1

    iget v0, p0, Lekr;->g:I

    iput p1, p0, Lekr;->g:I

    return-void
.end method

.method static synthetic a(Lekr;I)V
    .locals 0

    invoke-direct {p0, p1}, Lekr;->a(I)V

    return-void
.end method

.method static synthetic b(Lekr;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lekr;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lekr;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lekr;->d:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic d(Lekr;)Lekw;
    .locals 1

    iget-object v0, p0, Lekr;->f:Lekw;

    return-object v0
.end method

.method static synthetic e(Lekr;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lekr;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/os/IBinder;)Landroid/os/IInterface;
.end method

.method public final a()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lekr;->h:Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lekr;->a(I)V

    iget-object v0, p0, Lekr;->a:Landroid/content/Context;

    invoke-static {v0}, Lejf;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lekr;->a(I)V

    iget-object v1, p0, Lekr;->b:Landroid/os/Handler;

    iget-object v2, p0, Lekr;->b:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lekr;->f:Lekw;

    if-eqz v0, :cond_2

    const-string v0, "GmsClient"

    const-string v1, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lekr;->d:Landroid/os/IInterface;

    iget-object v0, p0, Lekr;->a:Landroid/content/Context;

    invoke-static {v0}, Lelc;->a(Landroid/content/Context;)Lelc;

    move-result-object v0

    invoke-virtual {p0}, Lekr;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lekr;->f:Lekw;

    invoke-virtual {v0, v1, v2}, Lelc;->b(Ljava/lang/String;Lekw;)V

    :cond_2
    new-instance v0, Lekw;

    invoke-direct {v0, p0}, Lekw;-><init>(Lekr;)V

    iput-object v0, p0, Lekr;->f:Lekw;

    iget-object v0, p0, Lekr;->a:Landroid/content/Context;

    invoke-static {v0}, Lelc;->a(Landroid/content/Context;)Lelc;

    move-result-object v0

    invoke-virtual {p0}, Lekr;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lekr;->f:Lekw;

    invoke-virtual {v0, v1, v2}, Lelc;->a(Ljava/lang/String;Lekw;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to connect to service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lekr;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lekr;->b:Landroid/os/Handler;

    iget-object v1, p0, Lekr;->b:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lekr;->b:Landroid/os/Handler;

    iget-object v1, p0, Lekr;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Leky;

    invoke-direct {v3, p0, p1, p2, p3}, Leky;-><init>(Lekr;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Lejc;)V
    .locals 1

    iget-object v0, p0, Lekr;->i:Lekz;

    invoke-virtual {v0, p1}, Lekz;->a(Lejc;)V

    return-void
.end method

.method public abstract a(Lelk;Lekv;)V
.end method

.method public b()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lekr;->h:Z

    iget-object v2, p0, Lekr;->e:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lekr;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lekr;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekt;

    invoke-virtual {v0}, Lekt;->c()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lekr;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lekr;->a(I)V

    iput-object v4, p0, Lekr;->d:Landroid/os/IInterface;

    iget-object v0, p0, Lekr;->f:Lekw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lekr;->a:Landroid/content/Context;

    invoke-static {v0}, Lelc;->a(Landroid/content/Context;)Lelc;

    move-result-object v0

    invoke-virtual {p0}, Lekr;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lekr;->f:Lekw;

    invoke-virtual {v0, v1, v2}, Lelc;->b(Ljava/lang/String;Lekw;)V

    iput-object v4, p0, Lekr;->f:Lekw;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected final b(Landroid/os/IBinder;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Lell;->a(Landroid/os/IBinder;)Lelk;

    move-result-object v0

    new-instance v1, Lekv;

    invoke-direct {v1, p0}, Lekv;-><init>(Lekr;)V

    invoke-virtual {p0, v0, v1}, Lekr;->a(Lelk;Lekv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GmsClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    iget v0, p0, Lekr;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lekr;->c:Landroid/os/Looper;

    return-object v0
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lekr;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    invoke-virtual {p0}, Lekr;->c()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final i()Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0}, Lekr;->h()V

    iget-object v0, p0, Lekr;->d:Landroid/os/IInterface;

    return-object v0
.end method

.method public x_()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final y_()Z
    .locals 1

    iget-boolean v0, p0, Lekr;->h:Z

    return v0
.end method
