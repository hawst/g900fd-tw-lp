.class public final Lflv;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhsb;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Lfkn;

.field public f:Lflz;

.field private g:Ljava/lang/CharSequence;

.field private h:Ljava/lang/CharSequence;

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Lflh;


# direct methods
.method public constructor <init>(Lhsb;)V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0}, Lfic;-><init>()V

    .line 42
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsb;

    iput-object v0, p0, Lflv;->a:Lhsb;

    .line 43
    new-instance v0, Lflz;

    iget-object v1, p1, Lhsb;->d:Lhxf;

    iget-object v2, p1, Lhsb;->q:Lhsk;

    invoke-direct {v0, v1, v2}, Lflz;-><init>(Lhxf;Lhsk;)V

    iput-object v0, p0, Lflv;->f:Lflz;

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 273
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 274
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->m:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lflv;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->c:Lhgz;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lflv;->g:Ljava/lang/CharSequence;

    .line 73
    :cond_0
    :goto_0
    iget-object v0, p0, Lflv;->g:Ljava/lang/CharSequence;

    return-object v0

    .line 70
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lflv;->g:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->p:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lflv;->h:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->e:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lflv;->h:Ljava/lang/CharSequence;

    .line 96
    :cond_0
    iget-object v0, p0, Lflv;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lflv;->i:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->f:Lhgz;

    const/4 v1, 0x1

    .line 105
    invoke-static {v0, v1}, Lfvo;->a(Lhgz;I)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lflv;->i:Ljava/lang/CharSequence;

    .line 107
    :cond_0
    iget-object v0, p0, Lflv;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lflv;->j:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->g:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lflv;->j:Ljava/lang/CharSequence;

    .line 141
    :cond_0
    iget-object v0, p0, Lflv;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->j:Lhvf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->j:Lhvf;

    iget-boolean v0, v0, Lhvf;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lflh;
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lflv;->k:Lflh;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->o:Lhsc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->o:Lhsc;

    iget-object v0, v0, Lhsc;->a:Lhpn;

    if-eqz v0, :cond_0

    .line 248
    new-instance v0, Lflh;

    iget-object v1, p0, Lflv;->a:Lhsb;

    iget-object v1, v1, Lhsb;->o:Lhsc;

    iget-object v1, v1, Lhsc;->a:Lhpn;

    invoke-direct {v0, v1}, Lflh;-><init>(Lhpn;)V

    iput-object v0, p0, Lflv;->k:Lflh;

    .line 250
    :cond_0
    iget-object v0, p0, Lflv;->k:Lflh;

    return-object v0
.end method
