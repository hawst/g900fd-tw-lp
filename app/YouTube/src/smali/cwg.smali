.class public Lcwg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lftg;


# instance fields
.field final a:Landroid/content/Context;

.field public b:Z

.field public c:Z

.field public d:Lcwj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcwg;->a:Landroid/content/Context;

    .line 42
    return-void
.end method

.method public static b(Lflo;)Lczb;
    .locals 4

    .prologue
    .line 136
    iget v1, p0, Lflo;->b:I

    .line 137
    sget-object v0, Lczc;->a:Lczc;

    .line 138
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 139
    sget-object v0, Lczc;->b:Lczc;

    .line 146
    :cond_0
    :goto_0
    new-instance v1, Lczb;

    const/4 v2, 0x0

    iget-object v3, p0, Lflo;->a:Lhqn;

    iget-object v3, v3, Lhqn;->b:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Lczb;-><init>(Lczc;ZLjava/lang/String;)V

    return-object v1

    .line 140
    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 141
    sget-object v0, Lczc;->c:Lczc;

    goto :goto_0

    .line 142
    :cond_2
    invoke-virtual {p0}, Lflo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    sget-object v0, Lczc;->e:Lczc;

    goto :goto_0
.end method


# virtual methods
.method protected a(Lflo;)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method protected a(Lflo;Lcwi;)V
    .locals 1

    .prologue
    .line 112
    invoke-static {p1}, Lcwg;->b(Lflo;)Lczb;

    move-result-object v0

    invoke-interface {p2, v0}, Lcwi;->a(Lczb;)V

    .line 113
    return-void
.end method

.method public final a(Lflo;Lcwi;Lcwx;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 63
    if-nez p1, :cond_0

    .line 66
    new-instance v0, Lczb;

    sget-object v1, Lczc;->a:Lczc;

    invoke-direct {v0, v1, v4, v6}, Lczb;-><init>(Lczc;ZLjava/lang/String;)V

    invoke-interface {p2, v0}, Lcwi;->a(Lczb;)V

    .line 85
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-virtual {p1}, Lflo;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    iget v0, p3, Lcwx;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 72
    iget-boolean v0, p1, Lflo;->c:Z

    if-nez v0, :cond_1

    .line 73
    new-instance v0, Lczb;

    sget-object v1, Lczc;->k:Lczc;

    iget-object v2, p0, Lcwg;->a:Landroid/content/Context;

    const v3, 0x7f090182

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Lczb;-><init>(Lczc;ZLjava/lang/String;)V

    invoke-interface {p2, v0}, Lcwi;->a(Lczb;)V

    goto :goto_0

    .line 75
    :cond_1
    invoke-interface {p2}, Lcwi;->a()V

    goto :goto_0

    .line 77
    :cond_2
    invoke-virtual {p1}, Lflo;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 78
    iget-object v0, p0, Lcwg;->d:Lcwj;

    if-nez v0, :cond_3

    invoke-static {p1}, Lcwg;->b(Lflo;)Lczb;

    move-result-object v0

    invoke-interface {p2, v0}, Lcwi;->a(Lczb;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcwg;->d:Lcwj;

    iget-object v0, p1, Lflo;->a:Lhqn;

    iget-object v2, v0, Lhqn;->b:Ljava/lang/String;

    new-instance v3, Lcwh;

    invoke-direct {v3, p0, p1, p2}, Lcwh;-><init>(Lcwg;Lflo;Lcwi;)V

    invoke-virtual {p1}, Lflo;->e()Lfij;

    move-result-object v4

    iget-object v0, v1, Lcwj;->a:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v5, 0x7f040045

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v0, 0x7f080142

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcwl;

    invoke-direct {v0, v1, v3, v4}, Lcwl;-><init>(Lcwj;Lcwm;Lfij;)V

    new-instance v2, Leyv;

    iget-object v3, v1, Lcwj;->a:Landroid/app/Activity;

    invoke-direct {v2, v3}, Leyv;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Leyv;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0900a3

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0900a2

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, v1, Lcwj;->c:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 82
    :cond_4
    invoke-static {p1}, Lcwg;->b(Lflo;)Lczb;

    move-result-object v0

    .line 81
    invoke-interface {p2, v0}, Lcwi;->a(Lczb;)V

    goto/16 :goto_0
.end method

.method public final a(Lftj;)V
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcwg;->b:Z

    iput-boolean v0, p1, Lftj;->l:Z

    .line 131
    iget-boolean v0, p0, Lcwg;->c:Z

    iput-boolean v0, p1, Lftj;->k:Z

    .line 132
    return-void
.end method
