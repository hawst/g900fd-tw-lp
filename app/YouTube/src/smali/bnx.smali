.class final Lbnx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Lbnw;


# direct methods
.method constructor <init>(Lbnw;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lbnx;->a:Lbnw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 66
    iget-object v0, p0, Lbnx;->a:Lbnw;

    iget-object v0, v0, Lbnw;->d:Lfiw;

    if-eqz v0, :cond_4

    .line 67
    iget-object v1, p0, Lbnx;->a:Lbnw;

    iget-object v0, p0, Lbnx;->a:Lbnw;

    iget-object v0, v0, Lbnw;->d:Lfiw;

    invoke-virtual {v0}, Lfiw;->a()Lfiz;

    move-result-object v2

    iget-object v0, v1, Lbnw;->f:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    iget-object v0, v1, Lbnw;->a:Landroid/app/Activity;

    const v3, 0x7f040038

    invoke-static {v0, v3, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbnw;->g:Landroid/view/View;

    iget-object v0, v1, Lbnw;->g:Landroid/view/View;

    const v3, 0x7f0800a4

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v3, Lfvi;

    iget-object v4, v1, Lbnw;->b:Leyp;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v0, v5}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;Z)V

    iput-object v3, v1, Lbnw;->h:Lfvi;

    iget-object v0, v1, Lbnw;->g:Landroid/view/View;

    const v3, 0x7f08011c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbnw;->i:Landroid/widget/TextView;

    iget-object v0, v1, Lbnw;->g:Landroid/view/View;

    const v3, 0x7f08008b

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbnw;->j:Landroid/widget/TextView;

    iget-object v0, v1, Lbnw;->g:Landroid/view/View;

    const v3, 0x7f08011d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbnw;->k:Landroid/widget/TextView;

    iget-object v0, v1, Lbnw;->g:Landroid/view/View;

    const v3, 0x7f080119

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbnw;->l:Landroid/widget/TextView;

    iget-object v0, v1, Lbnw;->g:Landroid/view/View;

    const v3, 0x7f08011a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbnw;->m:Landroid/widget/TextView;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, v1, Lbnw;->a:Landroid/app/Activity;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, v1, Lbnw;->a:Landroid/app/Activity;

    const v4, 0x7f090273

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v3, v1, Lbnw;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f0901d7

    invoke-virtual {v0, v3, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f090274

    new-instance v4, Lbny;

    invoke-direct {v4, v1}, Lbny;-><init>(Lbnw;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, v1, Lbnw;->f:Landroid/app/AlertDialog;

    :cond_0
    iput-object v2, v1, Lbnw;->e:Lfiz;

    iget-object v0, v1, Lbnw;->i:Landroid/widget/TextView;

    iget-object v3, v2, Lfiz;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_1

    iget-object v3, v2, Lfiz;->a:Lhch;

    iget-object v3, v3, Lhch;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfiz;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v3, v2, Lfiz;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lbnw;->j:Landroid/widget/TextView;

    iget-object v3, v2, Lfiz;->c:Ljava/lang/CharSequence;

    if-nez v3, :cond_2

    iget-object v3, v2, Lfiz;->a:Lhch;

    iget-object v3, v3, Lhch;->c:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfiz;->c:Ljava/lang/CharSequence;

    :cond_2
    iget-object v3, v2, Lfiz;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lbnw;->h:Lfvi;

    iget-object v3, v2, Lfiz;->d:Lfnc;

    if-nez v3, :cond_3

    new-instance v3, Lfnc;

    iget-object v4, v2, Lfiz;->a:Lhch;

    iget-object v4, v4, Lhch;->b:Lhxf;

    invoke-direct {v3, v4}, Lfnc;-><init>(Lhxf;)V

    iput-object v3, v2, Lfiz;->d:Lfnc;

    :cond_3
    iget-object v3, v2, Lfiz;->d:Lfnc;

    invoke-virtual {v0, v3, v8}, Lfvi;->a(Lfnc;Leyo;)V

    invoke-virtual {v2}, Lfiz;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lbnw;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-virtual {v2}, Lfiz;->b()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v1, Lbnw;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    invoke-virtual {v2}, Lfiz;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v1, Lbnw;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    iget-object v0, v1, Lbnw;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 69
    :cond_4
    return-void

    .line 67
    :cond_5
    iget-object v0, v1, Lbnw;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, v1, Lbnw;->k:Landroid/widget/TextView;

    invoke-virtual {v2}, Lfiz;->a()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_6
    iget-object v0, v1, Lbnw;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, v1, Lbnw;->l:Landroid/widget/TextView;

    invoke-virtual {v2}, Lfiz;->b()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    iget-object v0, v1, Lbnw;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, v1, Lbnw;->m:Landroid/widget/TextView;

    invoke-virtual {v2}, Lfiz;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
