.class final Lacl;
.super Lghv;
.source "SourceFile"

# interfaces
.implements Lgib;


# instance fields
.field private synthetic a:Lacj;


# direct methods
.method constructor <init>(Lacj;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lacl;->a:Lacj;

    invoke-direct {p0}, Lghv;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 233
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v1, "DeviceId"

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbl;

    iget-object v2, p0, Lacl;->a:Lacj;

    invoke-virtual {v2}, Lacj;->a()[B

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lgbl;-><init>(Ljava/lang/String;[B)V

    return-object v1
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 233
    check-cast p1, Landroid/util/Pair;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lgbl;

    sget-object v2, Lewv;->c:Lewv;

    invoke-virtual {v2, v0}, Lewv;->a(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    const-string v3, "X-GData-Device"

    invoke-virtual {v1, v0}, Lgbl;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method protected final a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;
    .locals 3

    .prologue
    .line 258
    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 259
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    .line 260
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 261
    const-string v2, "Error"

    invoke-virtual {v0, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 262
    const/16 v2, 0x190

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_0

    const-string v2, "InvalidDeveloper"

    .line 263
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    new-instance v0, Lacn;

    const-string v2, "Invalid Developer Key"

    invoke-direct {v0, v1, v2}, Lacn;-><init>(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    invoke-super {p0, p1}, Lghv;->a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;

    move-result-object v0

    goto :goto_0
.end method
