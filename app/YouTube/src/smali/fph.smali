.class public final Lfph;
.super Lgia;
.source "SourceFile"


# instance fields
.field private a:Lfpf;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1919
    invoke-direct {p0}, Lgia;-><init>()V

    return-void
.end method

.method public constructor <init>(Lfpf;)V
    .locals 0

    .prologue
    .line 1921
    invoke-direct {p0}, Lgia;-><init>()V

    .line 1922
    iput-object p1, p0, Lfph;->a:Lfpf;

    .line 1923
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1927
    const/4 v0, 0x1

    return v0
.end method

.method protected final synthetic a(Lorg/json/JSONObject;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1911
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lfpf;

    const-string v1, "offset"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "isPercentageOffset"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "pingUri"

    invoke-static {p1, v3}, Lfph;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lfpf;-><init>(IZLandroid/net/Uri;)V

    return-object v0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 1932
    const-string v0, "offset"

    iget-object v1, p0, Lfph;->a:Lfpf;

    iget v1, v1, Lfpf;->a:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1933
    const-string v0, "isPercentageOffset"

    iget-object v1, p0, Lfph;->a:Lfpf;

    iget-boolean v1, v1, Lfpf;->b:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1934
    const-string v0, "pingUri"

    iget-object v1, p0, Lfph;->a:Lfpf;

    iget-object v1, v1, Lfpf;->c:Landroid/net/Uri;

    invoke-static {p1, v0, v1}, Lfph;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1935
    return-void
.end method
