.class public final enum Lbnc;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lbne;


# static fields
.field public static final enum a:Lbnc;

.field private static enum c:Lbnc;

.field private static enum d:Lbnc;

.field private static e:Lbnc;

.field private static final synthetic g:[Lbnc;


# instance fields
.field public final b:Lffl;

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lbnc;

    const-string v1, "ANY"

    sget-object v2, Lffl;->a:Lffl;

    const v3, 0x7f090287

    invoke-direct {v0, v1, v4, v2, v3}, Lbnc;-><init>(Ljava/lang/String;ILffl;I)V

    sput-object v0, Lbnc;->a:Lbnc;

    .line 15
    new-instance v0, Lbnc;

    const-string v1, "CHANNEL"

    sget-object v2, Lffl;->b:Lffl;

    const v3, 0x7f090289

    invoke-direct {v0, v1, v5, v2, v3}, Lbnc;-><init>(Ljava/lang/String;ILffl;I)V

    sput-object v0, Lbnc;->c:Lbnc;

    .line 16
    new-instance v0, Lbnc;

    const-string v1, "PLAYLISTS"

    sget-object v2, Lffl;->c:Lffl;

    const v3, 0x7f09028a

    invoke-direct {v0, v1, v6, v2, v3}, Lbnc;-><init>(Ljava/lang/String;ILffl;I)V

    sput-object v0, Lbnc;->d:Lbnc;

    .line 12
    const/4 v0, 0x3

    new-array v0, v0, [Lbnc;

    sget-object v1, Lbnc;->a:Lbnc;

    aput-object v1, v0, v4

    sget-object v1, Lbnc;->c:Lbnc;

    aput-object v1, v0, v5

    sget-object v1, Lbnc;->d:Lbnc;

    aput-object v1, v0, v6

    sput-object v0, Lbnc;->g:[Lbnc;

    .line 18
    sget-object v0, Lbnc;->a:Lbnc;

    sput-object v0, Lbnc;->e:Lbnc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILffl;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput-object p3, p0, Lbnc;->b:Lffl;

    .line 27
    iput p4, p0, Lbnc;->f:I

    .line 28
    return-void
.end method

.method public static a(I)Lbnc;
    .locals 2

    .prologue
    .line 57
    invoke-static {}, Lbnc;->values()[Lbnc;

    move-result-object v0

    .line 58
    if-ltz p0, :cond_0

    array-length v1, v0

    if-ge p0, v1, :cond_0

    .line 59
    aget-object v0, v0, p0

    .line 62
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lbnc;->e:Lbnc;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lbnc;
    .locals 3

    .prologue
    .line 44
    if-nez p0, :cond_0

    .line 45
    sget-object v0, Lbnc;->e:Lbnc;

    .line 52
    :goto_0
    return-object v0

    .line 49
    :cond_0
    :try_start_0
    invoke-static {p0}, Lbnc;->valueOf(Ljava/lang/String;)Lbnc;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    const-string v1, "Attempted to search with unsupported SEARCH_TYPE: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 52
    sget-object v0, Lbnc;->e:Lbnc;

    goto :goto_0

    .line 51
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;)Lbnc;
    .locals 1

    .prologue
    .line 72
    if-eqz p0, :cond_1

    .line 78
    const-string v0, "is:channel"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lbnc;->c:Lbnc;

    .line 84
    :goto_0
    return-object v0

    .line 80
    :cond_0
    const-string v0, "is:playlists"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    sget-object v0, Lbnc;->d:Lbnc;

    goto :goto_0

    .line 84
    :cond_1
    sget-object v0, Lbnc;->e:Lbnc;

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 91
    const-string v0, "is:channel"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 92
    const-string v1, "is:playlists"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 94
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbnc;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lbnc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbnc;

    return-object v0
.end method

.method public static values()[Lbnc;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lbnc;->g:[Lbnc;

    invoke-virtual {v0}, [Lbnc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbnc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lbnc;->f:I

    return v0
.end method
