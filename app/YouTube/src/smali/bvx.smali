.class public final Lbvx;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field a:Ldcd;

.field b:Ldbz;

.field c:Lbxa;

.field d:Landroid/widget/ImageView;

.field e:Lfvi;

.field f:Z

.field g:Z

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/ImageView;

.field j:Z

.field private final k:Landroid/view/LayoutInflater;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldcd;Ldbz;Lbxb;Landroid/view/View$OnClickListener;)V
    .locals 5

    .prologue
    const/4 v4, -0x2

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 60
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 61
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lbvx;->k:Landroid/view/LayoutInflater;

    .line 63
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldcd;

    iput-object v0, p0, Lbvx;->a:Ldcd;

    .line 64
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbz;

    iput-object v0, p0, Lbvx;->b:Ldbz;

    .line 65
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbvx;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbvx;->d:Landroid/widget/ImageView;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lbvx;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lbvx;->d:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lbvx;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lbvx;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lbvx;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lbvx;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p0, v0}, Lbvx;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lbvx;->k:Landroid/view/LayoutInflater;

    const v1, 0x7f0400b0

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbvx;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lbvx;->h:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lbvx;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lbvx;->h:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p2, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c()I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lbvx;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object p2, p0, Lbvx;->a:Ldcd;

    invoke-virtual {p0, p2}, Lbvx;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ldcd;->g(Z)V

    iget-object v0, p0, Lbvx;->k:Landroid/view/LayoutInflater;

    const v1, 0x7f0400ed

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbvx;->l:Landroid/view/View;

    invoke-virtual {p0}, Lbvx;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lbvx;->l:Landroid/view/View;

    invoke-static {v0, v1, p4}, Lbxa;->a(Landroid/content/Context;Landroid/view/View;Lbxb;)Lbxa;

    move-result-object v0

    iput-object v0, p0, Lbvx;->c:Lbxa;

    iget-object v0, p0, Lbvx;->c:Lbxa;

    invoke-virtual {v0}, Lbxa;->a()V

    iget-object v0, p0, Lbvx;->l:Landroid/view/View;

    const v1, 0x7f0802b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbvx;->m:Landroid/widget/Button;

    iget-object v0, p0, Lbvx;->l:Landroid/view/View;

    invoke-virtual {p0, v0}, Lbvx;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbvx;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbvx;->i:Landroid/widget/ImageView;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lbvx;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lbvx;->i:Landroid/widget/ImageView;

    const v1, 0x7f0200ba

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lbvx;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lbvx;->i:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lbvx;->addView(Landroid/view/View;)V

    .line 68
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 161
    iget-object v0, p0, Lbvx;->c:Lbxa;

    invoke-virtual {v0}, Lbxa;->b()V

    .line 162
    iget-object v0, p0, Lbvx;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lbvx;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lbvx;->a:Ldcd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldcd;->c_(Z)V

    .line 165
    iget-object v0, p0, Lbvx;->b:Ldbz;

    invoke-virtual {v0, v2}, Ldbz;->setVisibility(I)V

    .line 168
    invoke-virtual {p0}, Lbvx;->b()V

    .line 169
    return-void
.end method

.method public final a(Ldwo;)V
    .locals 2

    .prologue
    .line 187
    sget-object v0, Lbvy;->a:[I

    invoke-virtual {p1}, Ldwo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 201
    :goto_0
    return-void

    .line 189
    :pswitch_0
    iget-object v0, p0, Lbvx;->a:Ldcd;

    sget-object v1, Ldbs;->b:Ldbs;

    invoke-virtual {v0, v1}, Ldcd;->a(Ldbs;)V

    goto :goto_0

    .line 193
    :pswitch_1
    iget-object v0, p0, Lbvx;->a:Ldcd;

    sget-object v1, Ldbs;->c:Ldbs;

    invoke-virtual {v0, v1}, Ldcd;->a(Ldbs;)V

    goto :goto_0

    .line 196
    :pswitch_2
    iget-object v0, p0, Lbvx;->a:Ldcd;

    sget-object v1, Ldbs;->g:Ldbs;

    invoke-virtual {v0, v1}, Ldcd;->a(Ldbs;)V

    goto :goto_0

    .line 200
    :pswitch_3
    iget-object v0, p0, Lbvx;->a:Ldcd;

    sget-object v1, Ldbs;->d:Ldbs;

    invoke-virtual {v0, v1}, Ldcd;->a(Ldbs;)V

    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 148
    iget-boolean v0, p0, Lbvx;->j:Z

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {p0}, Lbvx;->a()V

    .line 158
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lbvx;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lbvx;->m:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(I)V

    .line 154
    iget-object v0, p0, Lbvx;->c:Lbxa;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbxa;->a(Ljava/lang/CharSequence;Z)V

    .line 156
    invoke-virtual {p0}, Lbvx;->b()V

    .line 157
    iget-object v0, p0, Lbvx;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 143
    if-eqz p2, :cond_0

    const v0, 0x7f0900a4

    .line 144
    :goto_0
    invoke-virtual {p0, p1, v0}, Lbvx;->a(Ljava/lang/String;I)V

    .line 145
    return-void

    .line 143
    :cond_0
    const v0, 0x7f0900a2

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lbvx;->a:Ldcd;

    invoke-virtual {v0, p1}, Ldcd;->h(Z)V

    .line 99
    invoke-virtual {p0}, Lbvx;->e()V

    .line 100
    return-void
.end method

.method b()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lbvx;->a:Ldcd;

    invoke-virtual {v0}, Ldcd;->e()V

    .line 266
    iget-object v0, p0, Lbvx;->a:Ldcd;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 267
    return-void
.end method

.method c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 270
    iget-object v0, p0, Lbvx;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lbvx;->a:Ldcd;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbvx;->a:Ldcd;

    invoke-virtual {v0, v2}, Ldcd;->c_(Z)V

    iget-object v0, p0, Lbvx;->a:Ldcd;

    invoke-virtual {v0}, Ldcd;->d()V

    .line 272
    iget-object v0, p0, Lbvx;->c:Lbxa;

    invoke-virtual {v0}, Lbxa;->b()V

    .line 273
    iget-object v0, p0, Lbvx;->a:Ldcd;

    iget-boolean v1, p0, Lbvx;->f:Z

    invoke-virtual {v0, v1}, Ldcd;->j(Z)V

    .line 274
    iget-object v0, p0, Lbvx;->a:Ldcd;

    iget-boolean v1, p0, Lbvx;->g:Z

    invoke-virtual {v0, v1}, Ldcd;->k(Z)V

    .line 275
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lbvx;->e:Lfvi;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lbvx;->e:Lfvi;

    invoke-virtual {v0}, Lfvi;->a()V

    .line 293
    :goto_0
    sget-object v0, Ldwo;->i:Ldwo;

    invoke-virtual {p0, v0}, Lbvx;->a(Ldwo;)V

    .line 294
    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Lbvx;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method e()V
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lbvx;->j:Z

    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {p0}, Lbvx;->b()V

    .line 300
    :cond_0
    return-void
.end method
