.class public final Lbth;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Landroid/view/View;

.field public final a:Landroid/app/Activity;

.field public final b:Lcyc;

.field public final c:Lgnd;

.field public final d:Lbka;

.field public final e:Levn;

.field public final f:Lfus;

.field public final g:Leyp;

.field public final h:Lexd;

.field public final i:Lglm;

.field public final j:Lbjx;

.field public final k:Lcwg;

.field final l:Lbtj;

.field public final m:Lfrz;

.field public final n:Ljava/util/List;

.field public final o:Lbyg;

.field public final p:Lbrz;

.field public q:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field public r:Landroid/widget/ListView;

.field public s:Lfsi;

.field public t:Landroid/widget/LinearLayout;

.field public u:Landroid/view/View;

.field private final v:Lfhz;

.field private final w:Lfdw;

.field private final x:Lfjx;

.field private y:Leue;

.field private z:Lcdu;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcyc;Lfus;Lgnd;Lbka;Lbyg;Lbrz;Levn;Leyp;Lexd;Lglm;Lbjx;Lcwg;Lbtj;Lfhz;Lfdw;Lfrz;Lfjx;)V
    .locals 4

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lbth;->a:Landroid/app/Activity;

    .line 153
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcyc;

    iput-object v1, p0, Lbth;->b:Lcyc;

    .line 154
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfus;

    iput-object v1, p0, Lbth;->f:Lfus;

    .line 155
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgnd;

    iput-object v1, p0, Lbth;->c:Lgnd;

    .line 156
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbka;

    iput-object v1, p0, Lbth;->d:Lbka;

    .line 157
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbyg;

    iput-object v1, p0, Lbth;->o:Lbyg;

    .line 158
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbrz;

    iput-object v1, p0, Lbth;->p:Lbrz;

    .line 159
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Levn;

    iput-object v1, p0, Lbth;->e:Levn;

    .line 160
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leyp;

    iput-object v1, p0, Lbth;->g:Leyp;

    .line 161
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexd;

    iput-object v1, p0, Lbth;->h:Lexd;

    .line 162
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lglm;

    iput-object v1, p0, Lbth;->i:Lglm;

    .line 163
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjx;

    iput-object v1, p0, Lbth;->j:Lbjx;

    .line 164
    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwg;

    iput-object v1, p0, Lbth;->k:Lcwg;

    .line 165
    invoke-static/range {p14 .. p14}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbtj;

    iput-object v1, p0, Lbth;->l:Lbtj;

    .line 166
    invoke-static/range {p15 .. p15}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfhz;

    iput-object v1, p0, Lbth;->v:Lfhz;

    .line 167
    invoke-static/range {p16 .. p16}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfdw;

    iput-object v1, p0, Lbth;->w:Lfdw;

    .line 169
    invoke-static/range {p17 .. p17}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfrz;

    iput-object v1, p0, Lbth;->m:Lfrz;

    .line 170
    move-object/from16 v0, p18

    iput-object v0, p0, Lbth;->x:Lfjx;

    .line 171
    const/4 v1, 0x0

    new-array v1, v1, [Lu;

    invoke-static {v1}, La;->d([Ljava/lang/Object;)Ljava/util/LinkedList;

    move-result-object v1

    iput-object v1, p0, Lbth;->n:Ljava/util/List;

    .line 173
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 174
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v1

    .line 175
    iget-object v1, v1, Lari;->a:Lbca;

    const-class v2, Lbtn;

    .line 176
    invoke-virtual {v1, v2}, Levy;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    .line 173
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbtn;

    .line 177
    invoke-interface {v1}, Lbtn;->a()Lu;

    move-result-object v1

    .line 178
    if-eqz v1, :cond_0

    .line 179
    iget-object v3, p0, Lbth;->n:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 182
    :cond_1
    return-void
.end method

.method private handleOfflineVideoAddEvent(Lbly;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lbth;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 275
    invoke-interface {v0}, Lu;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    :goto_0
    return-void

    .line 280
    :cond_1
    invoke-virtual {p0}, Lbth;->a()V

    goto :goto_0
.end method

.method private handleOfflineVideoDeleteEvent(Lbmb;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 267
    invoke-virtual {p0}, Lbth;->a()V

    .line 268
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lbth;->y:Leue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbth;->y:Leue;

    iget-boolean v0, v0, Leue;->a:Z

    if-nez v0, :cond_0

    .line 285
    iget-object v0, p0, Lbth;->y:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 287
    :cond_0
    iget-object v0, p0, Lbth;->q:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 288
    new-instance v0, Lbtk;

    invoke-direct {v0, p0}, Lbtk;-><init>(Lbth;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbth;->y:Leue;

    .line 289
    iget-object v0, p0, Lbth;->c:Lgnd;

    iget-object v1, p0, Lbth;->a:Landroid/app/Activity;

    iget-object v2, p0, Lbth;->y:Leue;

    invoke-static {v1, v2}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v1

    invoke-interface {v0, v1}, Lgnd;->a(Leuc;)V

    .line 290
    return-void
.end method

.method public b()V
    .locals 7

    .prologue
    .line 297
    iget-object v0, p0, Lbth;->x:Lfjx;

    if-eqz v0, :cond_2

    .line 298
    iget-object v0, p0, Lbth;->z:Lcdu;

    if-nez v0, :cond_0

    .line 299
    new-instance v0, Lcdu;

    iget-object v1, p0, Lbth;->a:Landroid/app/Activity;

    iget-object v2, p0, Lbth;->g:Leyp;

    iget-object v3, p0, Lbth;->e:Levn;

    iget-object v4, p0, Lbth;->v:Lfhz;

    iget-object v5, p0, Lbth;->w:Lfdw;

    iget-object v6, p0, Lbth;->m:Lfrz;

    invoke-direct/range {v0 .. v6}, Lcdu;-><init>(Landroid/content/Context;Leyp;Levn;Lfhz;Lfdw;Lfrz;)V

    iput-object v0, p0, Lbth;->z:Lcdu;

    .line 307
    :cond_0
    iget-object v0, p0, Lbth;->A:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Lbth;->t:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbth;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 310
    :cond_1
    iget-object v0, p0, Lbth;->z:Lcdu;

    new-instance v1, Lfsg;

    invoke-direct {v1}, Lfsg;-><init>()V

    iget-object v2, p0, Lbth;->x:Lfjx;

    invoke-virtual {v0, v1, v2}, Lcdu;->a(Lfsg;Lfjx;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbth;->A:Landroid/view/View;

    .line 311
    iget-object v0, p0, Lbth;->t:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbth;->A:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 313
    :cond_2
    return-void
.end method
