.class final Levo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Ljava/lang/Object;

.field private synthetic b:Ljava/lang/Object;

.field private synthetic c:Levn;


# direct methods
.method constructor <init>(Levn;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Levo;->c:Levn;

    iput-object p2, p0, Levo;->a:Ljava/lang/Object;

    iput-object p3, p0, Levo;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 399
    iget-object v0, p0, Levo;->c:Levn;

    invoke-static {v0}, Levn;->a(Levn;)Ljava/util/concurrent/locks/ReadWriteLock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 401
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Levo;->c:Levn;

    .line 402
    invoke-static {v1}, Levn;->b(Levn;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Levo;->a:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, La;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 401
    invoke-static {v0}, La;->a(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 407
    iget-object v1, p0, Levo;->c:Levn;

    invoke-static {v1}, Levn;->a(Levn;)Ljava/util/concurrent/locks/ReadWriteLock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 410
    :goto_0
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 411
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levr;

    .line 412
    iget v2, v0, Levr;->d:I

    .line 413
    iget-object v3, p0, Levo;->b:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_1

    sget-object v3, Levn;->a:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 415
    :cond_1
    iget-object v2, v0, Levr;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 416
    iget-object v0, v0, Levr;->c:Levq;

    iget-object v2, p0, Levo;->a:Ljava/lang/Object;

    invoke-interface {v0, v2}, Levq;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 403
    :catch_0
    move-exception v0

    .line 404
    :try_start_1
    const-string v1, "exception "

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405
    const/4 v0, 0x0

    .line 407
    iget-object v1, p0, Levo;->c:Levn;

    invoke-static {v1}, Levn;->a(Levn;)Ljava/util/concurrent/locks/ReadWriteLock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Levo;->c:Levn;

    invoke-static {v1}, Levn;->a(Levn;)Ljava/util/concurrent/locks/ReadWriteLock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 419
    :cond_2
    iget-object v2, p0, Levo;->c:Levn;

    const/4 v3, 0x1

    new-array v3, v3, [Levr;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Levn;->a([Levr;)V

    goto :goto_1

    .line 424
    :cond_3
    return-void
.end method
