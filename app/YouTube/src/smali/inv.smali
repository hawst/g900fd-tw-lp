.class public final Linv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lipv;

.field public static final b:Lipv;

.field public static final c:Lipv;

.field public static final d:Lipv;

.field public static final e:Lipv;

.field public static final f:Lipv;

.field public static final g:Lipv;


# instance fields
.field public final h:Lipv;

.field public final i:Lipv;

.field final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, ":status"

    invoke-static {v0}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v0

    sput-object v0, Linv;->a:Lipv;

    .line 9
    const-string v0, ":method"

    invoke-static {v0}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v0

    sput-object v0, Linv;->b:Lipv;

    .line 10
    const-string v0, ":path"

    invoke-static {v0}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v0

    sput-object v0, Linv;->c:Lipv;

    .line 11
    const-string v0, ":scheme"

    invoke-static {v0}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v0

    sput-object v0, Linv;->d:Lipv;

    .line 12
    const-string v0, ":authority"

    invoke-static {v0}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v0

    sput-object v0, Linv;->e:Lipv;

    .line 13
    const-string v0, ":host"

    invoke-static {v0}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v0

    sput-object v0, Linv;->f:Lipv;

    .line 14
    const-string v0, ":version"

    invoke-static {v0}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v0

    sput-object v0, Linv;->g:Lipv;

    return-void
.end method

.method public constructor <init>(Lipv;Lipv;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Linv;->h:Lipv;

    .line 33
    iput-object p2, p0, Linv;->i:Lipv;

    .line 34
    iget-object v0, p1, Lipv;->b:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x20

    iget-object v1, p2, Lipv;->b:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Linv;->j:I

    .line 35
    return-void
.end method

.method public constructor <init>(Lipv;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    invoke-static {p2}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Linv;-><init>(Lipv;Lipv;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 24
    invoke-static {p1}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v0

    invoke-static {p2}, Lipv;->a(Ljava/lang/String;)Lipv;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Linv;-><init>(Lipv;Lipv;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 38
    instance-of v1, p1, Linv;

    if-eqz v1, :cond_0

    .line 39
    check-cast p1, Linv;

    .line 40
    iget-object v1, p0, Linv;->h:Lipv;

    iget-object v2, p1, Linv;->h:Lipv;

    invoke-virtual {v1, v2}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Linv;->i:Lipv;

    iget-object v2, p1, Linv;->i:Lipv;

    invoke-virtual {v1, v2}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 43
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Linv;->h:Lipv;

    invoke-virtual {v0}, Lipv;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 49
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Linv;->i:Lipv;

    invoke-virtual {v1}, Lipv;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    const-string v0, "%s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Linv;->h:Lipv;

    invoke-virtual {v3}, Lipv;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Linv;->i:Lipv;

    invoke-virtual {v3}, Lipv;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
