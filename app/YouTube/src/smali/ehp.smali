.class public Lehp;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lejr;)Leju;
    .locals 1

    new-instance v0, Lehs;

    invoke-direct {v0, p0}, Lehs;-><init>(Lehp;)V

    invoke-interface {p1, v0}, Lejr;->b(Lejp;)Lejp;

    move-result-object v0

    return-object v0
.end method

.method public a(Lejr;Ljava/lang/String;)Leju;
    .locals 1

    new-instance v0, Leht;

    invoke-direct {v0, p0, p2}, Leht;-><init>(Lehp;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lejr;->b(Lejp;)Lejp;

    move-result-object v0

    return-object v0
.end method

.method public a(Lejr;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)Leju;
    .locals 1

    new-instance v0, Lehr;

    invoke-direct {v0, p0, p2, p3}, Lehr;-><init>(Lehp;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    invoke-interface {p1, v0}, Lejr;->b(Lejp;)Lejp;

    move-result-object v0

    return-object v0
.end method

.method public a(Lejr;Ljava/lang/String;Ljava/lang/String;)Leju;
    .locals 1

    new-instance v0, Lehq;

    invoke-direct {v0, p0, p2, p3}, Lehq;-><init>(Lehp;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lejr;->b(Lejp;)Lejp;

    move-result-object v0

    return-object v0
.end method

.method public a(Lejr;Ljava/lang/String;Z)Leju;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Leic;

    invoke-direct {v0}, Leic;-><init>()V

    const/4 v1, 0x0

    iget-object v2, v0, Leic;->a:Lcom/google/android/gms/cast/LaunchOptions;

    iput-boolean v1, v2, Lcom/google/android/gms/cast/LaunchOptions;->b:Z

    iget-object v0, v0, Leic;->a:Lcom/google/android/gms/cast/LaunchOptions;

    invoke-virtual {p0, p1, p2, v0}, Lehp;->a(Lejr;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)Leju;

    move-result-object v0

    return-object v0
.end method

.method public a(Lejr;D)V
    .locals 8

    :try_start_0
    sget-object v0, Lehm;->a:Lejm;

    invoke-interface {p1, v0}, Lejr;->a(Lejm;)Lejk;

    move-result-object v0

    check-cast v0, Lenl;

    invoke-static {p2, p3}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Volume cannot be "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lenl;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lenu;

    iget-wide v4, v0, Lenl;->g:D

    iget-boolean v6, v0, Lenl;->e:Z

    move-wide v2, p2

    invoke-interface/range {v1 .. v6}, Lenu;->a(DDZ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    return-void
.end method

.method public a(Lejr;Ljava/lang/String;Lehx;)V
    .locals 3

    :try_start_0
    sget-object v0, Lehm;->a:Lejm;

    invoke-interface {p1, v0}, Lejr;->a(Lejm;)Lejk;

    move-result-object v0

    check-cast v0, Lenl;

    invoke-static {p2}, La;->u(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lenl;->a(Ljava/lang/String;)V

    if-eqz p3, :cond_0

    iget-object v1, v0, Lenl;->c:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v2, v0, Lenl;->c:Ljava/util/Map;

    invoke-interface {v2, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v0}, Lenl;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lenu;

    invoke-interface {v0, p2}, Lenu;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
