.class public final Lcuy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgod;


# instance fields
.field public a:Lfoy;

.field public b:Less;

.field public c:Ljava/util/regex/Pattern;

.field public d:Lcwx;

.field public e:Lerr;

.field public f:Z

.field public g:J

.field private final i:Ljava/lang/String;

.field private final j:Ljava/util/Random;

.field private final k:Lcva;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/Random;Lcva;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v0, p0, Lcuy;->d:Lcwx;

    .line 59
    iput-object v0, p0, Lcuy;->e:Lerr;

    .line 97
    iput-object p2, p0, Lcuy;->j:Ljava/util/Random;

    .line 98
    const-string v0, "a."

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcuy;->i:Ljava/lang/String;

    .line 99
    iput-object p3, p0, Lcuy;->k:Lcva;

    .line 100
    invoke-virtual {p3}, Lcva;->a()V

    .line 101
    return-void

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcuy;->c:Ljava/util/regex/Pattern;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcuy;->c:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;I)Ljava/lang/String;
    .locals 8

    .prologue
    .line 143
    packed-switch p2, :pswitch_data_0

    .line 231
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 145
    :pswitch_1
    iget-object v0, p0, Lcuy;->e:Lerr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcuy;->e:Lerr;

    iget-object v0, v0, Lerr;->a:Leri;

    invoke-virtual {v0}, Leri;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 147
    :pswitch_2
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcuy;->a:Lfoy;

    invoke-virtual {v0}, Lfoy;->g()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcuy;->a:Lfoy;

    invoke-virtual {v0}, Lfoy;->g()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 149
    :pswitch_3
    iget-object v0, p0, Lcuy;->j:Ljava/util/Random;

    const v1, 0x55d4a7f

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    const v1, 0x989680

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 152
    :pswitch_4
    const-string v0, "00:00:00.000"

    goto :goto_0

    .line 154
    :pswitch_5
    const-string v0, ","

    iget-object v1, p0, Lcuy;->a:Lfoy;

    iget-object v1, v1, Lfoy;->ah:Ljava/util/List;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 156
    :pswitch_6
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget-object v0, v0, Lfoy;->ad:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 158
    :pswitch_7
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget v0, v0, Lfoy;->o:I

    mul-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v0, "0"

    goto :goto_0

    .line 160
    :pswitch_8
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget-object v0, v0, Lfoy;->ac:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, ""

    goto :goto_0

    .line 162
    :pswitch_9
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget-object v0, v0, Lfoy;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget-object v0, v0, Lfoy;->c:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    const-string v0, "0"

    goto/16 :goto_0

    .line 164
    :pswitch_a
    iget-object v0, p0, Lcuy;->b:Less;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcuy;->b:Less;

    iget-object v0, v0, Less;->a:Lesh;

    iget-object v0, v0, Lesh;->c:Lesj;

    iget v0, v0, Lesj;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    const-string v0, "0"

    goto/16 :goto_0

    .line 167
    :pswitch_b
    const-string v0, "0"

    goto/16 :goto_0

    .line 169
    :pswitch_c
    iget-object v0, p0, Lcuy;->m:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcuy;->m:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    const-string v0, ""

    goto/16 :goto_0

    .line 171
    :pswitch_d
    iget-object v0, p0, Lcuy;->l:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcuy;->l:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    const-string v0, ""

    goto/16 :goto_0

    .line 173
    :pswitch_e
    const-string v0, "detailpage"

    goto/16 :goto_0

    .line 175
    :pswitch_f
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget-object v0, v0, Lfoy;->m:Lfpb;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcuy;->a:Lfoy;

    .line 176
    iget-object v0, v0, Lfoy;->m:Lfpb;

    iget-object v0, v0, Lfpb;->e:Ljava/lang/String;

    goto/16 :goto_0

    :cond_9
    const-string v0, "0"

    goto/16 :goto_0

    .line 178
    :pswitch_10
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget-object v1, v0, Lfoy;->aj:Lfoo;

    if-eqz v1, :cond_a

    sget-object v0, Lfpa;->c:Lfpa;

    :goto_1
    iget-object v0, v0, Lfpa;->d:Ljava/lang/String;

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v0}, Lfoy;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lfpa;->b:Lfpa;

    goto :goto_1

    :cond_b
    sget-object v0, Lfpa;->a:Lfpa;

    goto :goto_1

    :cond_c
    const-string v0, "0"

    goto/16 :goto_0

    .line 180
    :pswitch_11
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_d

    const-string v0, "2"

    goto/16 :goto_0

    :cond_d
    const-string v0, "0"

    goto/16 :goto_0

    .line 182
    :pswitch_12
    iget-object v0, p0, Lcuy;->b:Less;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcuy;->b:Less;

    iget-object v0, v0, Less;->a:Lesh;

    iget-object v0, v0, Lesh;->c:Lesj;

    sget-object v1, Lesj;->b:Lesj;

    if-ne v0, v1, :cond_e

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcuy;->b:Less;

    .line 183
    iget-object v1, v1, Less;->a:Lesh;

    iget-wide v2, v1, Lesh;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_e
    const-string v0, "0"

    goto/16 :goto_0

    .line 186
    :pswitch_13
    const-string v0, "0"

    goto/16 :goto_0

    .line 188
    :pswitch_14
    iget-object v0, p0, Lcuy;->d:Lcwx;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcuy;->d:Lcwx;

    iget v0, v0, Lcwx;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_f
    const-string v0, "0"

    goto/16 :goto_0

    .line 191
    :pswitch_15
    const-string v0, "0"

    goto/16 :goto_0

    .line 193
    :pswitch_16
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget-object v0, v0, Lfoy;->ae:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget-object v0, v0, Lfoy;->ae:Ljava/lang/String;

    goto/16 :goto_0

    :cond_10
    const-string v0, ""

    goto/16 :goto_0

    .line 196
    :pswitch_17
    const-string v0, "0"

    goto/16 :goto_0

    .line 198
    :pswitch_18
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcuy;->a:Lfoy;

    invoke-virtual {v0}, Lfoy;->d()Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "1"

    goto/16 :goto_0

    :cond_11
    const-string v0, "0"

    goto/16 :goto_0

    .line 200
    :pswitch_19
    const-string v0, "DROID"

    goto/16 :goto_0

    .line 202
    :pswitch_1a
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    goto/16 :goto_0

    .line 204
    :pswitch_1b
    const-string v0, "UNWN"

    goto/16 :goto_0

    .line 206
    :pswitch_1c
    invoke-direct {p0, p1}, Lcuy;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcuy;->k:Lcva;

    .line 207
    iget-object v0, v0, Lcva;->b:Ljava/lang/String;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcuy;->k:Lcva;

    .line 208
    iget-object v0, v0, Lcva;->b:Ljava/lang/String;

    goto/16 :goto_0

    :cond_12
    const-string v0, ""

    goto/16 :goto_0

    .line 210
    :pswitch_1d
    const-string v0, "MBL"

    goto/16 :goto_0

    .line 212
    :pswitch_1e
    iget-object v0, p0, Lcuy;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 214
    :pswitch_1f
    const-string v0, "a"

    goto/16 :goto_0

    .line 216
    :pswitch_20
    const-string v0, "android"

    goto/16 :goto_0

    .line 218
    :pswitch_21
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    goto/16 :goto_0

    .line 220
    :pswitch_22
    invoke-direct {p0, p1}, Lcuy;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcuy;->k:Lcva;

    .line 221
    iget-object v0, v0, Lcva;->b:Ljava/lang/String;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcuy;->k:Lcva;

    .line 222
    iget-object v0, v0, Lcva;->b:Ljava/lang/String;

    goto/16 :goto_0

    :cond_13
    const-string v0, "none"

    goto/16 :goto_0

    .line 224
    :pswitch_23
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_14

    iget-wide v0, p0, Lcuy;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcuy;->g:J

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    .line 225
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_14
    const-string v0, "0.0"

    goto/16 :goto_0

    .line 227
    :pswitch_24
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcuy;->a:Lfoy;

    iget-object v0, v0, Lfoy;->i:Ljava/lang/String;

    goto/16 :goto_0

    :cond_15
    const-string v0, ""

    goto/16 :goto_0

    .line 229
    :pswitch_25
    iget-object v0, p0, Lcuy;->a:Lfoy;

    if-eqz v0, :cond_17

    iget-boolean v0, p0, Lcuy;->f:Z

    if-eqz v0, :cond_16

    const-string v0, "playing"

    goto/16 :goto_0

    :cond_16
    const-string v0, "pause"

    goto/16 :goto_0

    :cond_17
    const-string v0, ""

    goto/16 :goto_0

    .line 143
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_0
        :pswitch_0
        :pswitch_18
        :pswitch_1c
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_23
        :pswitch_24
        :pswitch_25
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 104
    iput-object p2, p0, Lcuy;->l:Ljava/lang/String;

    .line 105
    iput-object p1, p0, Lcuy;->m:Ljava/lang/String;

    .line 106
    return-void
.end method
