.class public final Lhpo;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhpo;


# instance fields
.field public b:Lhgz;

.field public c:Lhgz;

.field public d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhpo;

    sput-object v0, Lhpo;->a:[Lhpo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v0, p0, Lhpo;->b:Lhgz;

    iput-object v0, p0, Lhpo;->c:Lhgz;

    const/4 v0, 0x1

    iput v0, p0, Lhpo;->d:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lhpo;->b:Lhgz;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lhpo;->b:Lhgz;

    invoke-static {v3, v0}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhpo;->c:Lhgz;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhpo;->c:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lhpo;->d:I

    if-eq v1, v3, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lhpo;->d:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhpo;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhpo;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhpo;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhpo;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhpo;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhpo;->b:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhpo;->b:Lhgz;

    :cond_2
    iget-object v0, p0, Lhpo;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhpo;->c:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhpo;->c:Lhgz;

    :cond_3
    iget-object v0, p0, Lhpo;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    :cond_4
    iput v0, p0, Lhpo;->d:I

    goto :goto_0

    :cond_5
    iput v2, p0, Lhpo;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lhpo;->b:Lhgz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpo;->b:Lhgz;

    invoke-virtual {p1, v2, v0}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhpo;->c:Lhgz;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhpo;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget v0, p0, Lhpo;->d:I

    if-eq v0, v2, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lhpo;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_2
    iget-object v0, p0, Lhpo;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhpo;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhpo;

    iget-object v2, p0, Lhpo;->b:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhpo;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhpo;->c:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhpo;->c:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget v2, p0, Lhpo;->d:I

    iget v3, p1, Lhpo;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhpo;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhpo;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhpo;->b:Lhgz;

    iget-object v3, p1, Lhpo;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhpo;->c:Lhgz;

    iget-object v3, p1, Lhpo;->c:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhpo;->I:Ljava/util/List;

    iget-object v3, p1, Lhpo;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhpo;->b:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhpo;->c:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhpo;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhpo;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lhpo;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhpo;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lhpo;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
