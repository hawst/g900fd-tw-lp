.class public final Lhfl;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:[Lhld;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46025
    invoke-direct {p0}, Lidf;-><init>()V

    .line 46028
    sget-object v0, Lhld;->a:[Lhld;

    iput-object v0, p0, Lhfl;->a:[Lhld;

    .line 46025
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 46074
    .line 46075
    iget-object v1, p0, Lhfl;->a:[Lhld;

    if-eqz v1, :cond_1

    .line 46076
    iget-object v2, p0, Lhfl;->a:[Lhld;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 46077
    if-eqz v4, :cond_0

    .line 46078
    const/4 v5, 0x1

    .line 46079
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 46076
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46083
    :cond_1
    iget-object v1, p0, Lhfl;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46084
    iput v0, p0, Lhfl;->J:I

    .line 46085
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 46021
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhfl;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhfl;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhfl;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhfl;->a:[Lhld;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhld;

    iget-object v3, p0, Lhfl;->a:[Lhld;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhfl;->a:[Lhld;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhfl;->a:[Lhld;

    :goto_2
    iget-object v2, p0, Lhfl;->a:[Lhld;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhfl;->a:[Lhld;

    new-instance v3, Lhld;

    invoke-direct {v3}, Lhld;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhfl;->a:[Lhld;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhfl;->a:[Lhld;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhfl;->a:[Lhld;

    new-instance v3, Lhld;

    invoke-direct {v3}, Lhld;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhfl;->a:[Lhld;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 46061
    iget-object v0, p0, Lhfl;->a:[Lhld;

    if-eqz v0, :cond_1

    .line 46062
    iget-object v1, p0, Lhfl;->a:[Lhld;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 46063
    if-eqz v3, :cond_0

    .line 46064
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 46062
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46068
    :cond_1
    iget-object v0, p0, Lhfl;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 46070
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46039
    if-ne p1, p0, :cond_1

    .line 46043
    :cond_0
    :goto_0
    return v0

    .line 46040
    :cond_1
    instance-of v2, p1, Lhfl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 46041
    :cond_2
    check-cast p1, Lhfl;

    .line 46042
    iget-object v2, p0, Lhfl;->a:[Lhld;

    iget-object v3, p1, Lhfl;->a:[Lhld;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhfl;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfl;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 46043
    goto :goto_0

    .line 46042
    :cond_4
    iget-object v2, p0, Lhfl;->I:Ljava/util/List;

    iget-object v3, p1, Lhfl;->I:Ljava/util/List;

    .line 46043
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 46047
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 46049
    iget-object v2, p0, Lhfl;->a:[Lhld;

    if-nez v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    .line 46055
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhfl;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_0
    add-int/2addr v0, v1

    .line 46056
    return v0

    :cond_1
    move v2, v0

    move v0, v1

    .line 46051
    :goto_1
    iget-object v3, p0, Lhfl;->a:[Lhld;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 46052
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhfl;->a:[Lhld;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    move v2, v1

    :goto_2
    add-int/2addr v2, v3

    .line 46051
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 46052
    :cond_2
    iget-object v2, p0, Lhfl;->a:[Lhld;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhld;->hashCode()I

    move-result v2

    goto :goto_2

    .line 46055
    :cond_3
    iget-object v1, p0, Lhfl;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_0
.end method
