.class public final Lesy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lffv;


# instance fields
.field private final a:Lgjp;

.field private final b:Letb;

.field private final c:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lgjp;Letb;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lesy;->a:Lgjp;

    .line 35
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Letb;

    iput-object v0, p0, Lesy;->b:Letb;

    .line 36
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lesy;->c:Ljava/util/concurrent/Executor;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 45
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 47
    iget-object v0, p0, Lesy;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lesz;

    invoke-direct {v1, p0, p1}, Lesz;-><init>(Lesy;Landroid/net/Uri;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-virtual {p0, p1}, Lesy;->b(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lesy;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method b(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 74
    invoke-static {p1}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v2

    iget-object v0, p0, Lesy;->b:Letb;

    invoke-interface {v0}, Letb;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    goto :goto_0

    :cond_0
    iget-object v0, v2, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lesy;->a:Lgjp;

    const-string v1, "remarketing"

    const v2, 0x323467f

    invoke-static {v1, v2}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v1

    .line 78
    invoke-virtual {v1, v0}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    .line 79
    iget-object v0, p0, Lesy;->a:Lgjp;

    sget-object v2, Lggu;->a:Lwu;

    invoke-virtual {v0, v1, v2}, Lgjp;->a(Lgjt;Lwu;)V

    .line 80
    return-void
.end method
