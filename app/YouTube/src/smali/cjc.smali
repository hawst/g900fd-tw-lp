.class public final Lcjc;
.super Lfsa;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lfdw;Lfrz;)V
    .locals 4

    .prologue
    .line 53
    invoke-direct {p0, p3, p4}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 54
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040077

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcjc;->a:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcjc;->a:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcjc;->b:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lcjc;->a:Landroid/view/View;

    const v1, 0x7f08011d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcjc;->c:Landroid/widget/TextView;

    .line 63
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 64
    invoke-virtual {p1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010112

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 65
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-static {p1, v0}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcjc;->d:Landroid/graphics/drawable/Drawable;

    .line 66
    return-void
.end method

.method private a(Lfpy;)Landroid/view/View;
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcjc;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfpy;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p1, Lfpz;->e:Ljava/lang/String;

    .line 73
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 74
    iget-object v1, p0, Lcjc;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcjc;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 80
    :goto_0
    iget-object v0, p0, Lcjc;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 81
    iget-object v0, p0, Lcjc;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 85
    :cond_0
    iget-boolean v0, p1, Lfpz;->h:Z

    if-eqz v0, :cond_2

    .line 86
    iget-object v0, p0, Lcjc;->a:Landroid/view/View;

    const v1, 0x7f0700a1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 91
    :goto_1
    iget-object v0, p0, Lcjc;->a:Landroid/view/View;

    return-object v0

    .line 77
    :cond_1
    iget-object v0, p0, Lcjc;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 88
    :cond_2
    iget-object v0, p0, Lcjc;->a:Landroid/view/View;

    iget-object v1, p0, Lcjc;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    check-cast p2, Lfpy;

    invoke-direct {p0, p2}, Lcjc;->a(Lfpy;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    check-cast p2, Lfpy;

    invoke-direct {p0, p2}, Lcjc;->a(Lfpy;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
