.class public final Lhdb;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhbt;

.field private b:Lhbt;

.field private c:Lhbt;

.field private d:Lhbt;

.field private e:Lhbt;

.field private f:Lhbt;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28768
    invoke-direct {p0}, Lidf;-><init>()V

    .line 28771
    iput-object v0, p0, Lhdb;->a:Lhbt;

    .line 28774
    iput-object v0, p0, Lhdb;->b:Lhbt;

    .line 28777
    iput-object v0, p0, Lhdb;->c:Lhbt;

    .line 28780
    iput-object v0, p0, Lhdb;->d:Lhbt;

    .line 28783
    iput-object v0, p0, Lhdb;->e:Lhbt;

    .line 28786
    iput-object v0, p0, Lhdb;->f:Lhbt;

    .line 28768
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 28853
    const/4 v0, 0x0

    .line 28854
    iget-object v1, p0, Lhdb;->a:Lhbt;

    if-eqz v1, :cond_0

    .line 28855
    const/4 v0, 0x1

    iget-object v1, p0, Lhdb;->a:Lhbt;

    .line 28856
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 28858
    :cond_0
    iget-object v1, p0, Lhdb;->b:Lhbt;

    if-eqz v1, :cond_1

    .line 28859
    const/4 v1, 0x2

    iget-object v2, p0, Lhdb;->b:Lhbt;

    .line 28860
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28862
    :cond_1
    iget-object v1, p0, Lhdb;->c:Lhbt;

    if-eqz v1, :cond_2

    .line 28863
    const/4 v1, 0x3

    iget-object v2, p0, Lhdb;->c:Lhbt;

    .line 28864
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28866
    :cond_2
    iget-object v1, p0, Lhdb;->d:Lhbt;

    if-eqz v1, :cond_3

    .line 28867
    const/4 v1, 0x4

    iget-object v2, p0, Lhdb;->d:Lhbt;

    .line 28868
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28870
    :cond_3
    iget-object v1, p0, Lhdb;->e:Lhbt;

    if-eqz v1, :cond_4

    .line 28871
    const/4 v1, 0x5

    iget-object v2, p0, Lhdb;->e:Lhbt;

    .line 28872
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28874
    :cond_4
    iget-object v1, p0, Lhdb;->f:Lhbt;

    if-eqz v1, :cond_5

    .line 28875
    const/4 v1, 0x6

    iget-object v2, p0, Lhdb;->f:Lhbt;

    .line 28876
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28878
    :cond_5
    iget-object v1, p0, Lhdb;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28879
    iput v0, p0, Lhdb;->J:I

    .line 28880
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 28764
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhdb;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhdb;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhdb;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhdb;->a:Lhbt;

    if-nez v0, :cond_2

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhdb;->a:Lhbt;

    :cond_2
    iget-object v0, p0, Lhdb;->a:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhdb;->b:Lhbt;

    if-nez v0, :cond_3

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhdb;->b:Lhbt;

    :cond_3
    iget-object v0, p0, Lhdb;->b:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhdb;->c:Lhbt;

    if-nez v0, :cond_4

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhdb;->c:Lhbt;

    :cond_4
    iget-object v0, p0, Lhdb;->c:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhdb;->d:Lhbt;

    if-nez v0, :cond_5

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhdb;->d:Lhbt;

    :cond_5
    iget-object v0, p0, Lhdb;->d:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhdb;->e:Lhbt;

    if-nez v0, :cond_6

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhdb;->e:Lhbt;

    :cond_6
    iget-object v0, p0, Lhdb;->e:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhdb;->f:Lhbt;

    if-nez v0, :cond_7

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhdb;->f:Lhbt;

    :cond_7
    iget-object v0, p0, Lhdb;->f:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 28829
    iget-object v0, p0, Lhdb;->a:Lhbt;

    if-eqz v0, :cond_0

    .line 28830
    const/4 v0, 0x1

    iget-object v1, p0, Lhdb;->a:Lhbt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 28832
    :cond_0
    iget-object v0, p0, Lhdb;->b:Lhbt;

    if-eqz v0, :cond_1

    .line 28833
    const/4 v0, 0x2

    iget-object v1, p0, Lhdb;->b:Lhbt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 28835
    :cond_1
    iget-object v0, p0, Lhdb;->c:Lhbt;

    if-eqz v0, :cond_2

    .line 28836
    const/4 v0, 0x3

    iget-object v1, p0, Lhdb;->c:Lhbt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 28838
    :cond_2
    iget-object v0, p0, Lhdb;->d:Lhbt;

    if-eqz v0, :cond_3

    .line 28839
    const/4 v0, 0x4

    iget-object v1, p0, Lhdb;->d:Lhbt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 28841
    :cond_3
    iget-object v0, p0, Lhdb;->e:Lhbt;

    if-eqz v0, :cond_4

    .line 28842
    const/4 v0, 0x5

    iget-object v1, p0, Lhdb;->e:Lhbt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 28844
    :cond_4
    iget-object v0, p0, Lhdb;->f:Lhbt;

    if-eqz v0, :cond_5

    .line 28845
    const/4 v0, 0x6

    iget-object v1, p0, Lhdb;->f:Lhbt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 28847
    :cond_5
    iget-object v0, p0, Lhdb;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 28849
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 28802
    if-ne p1, p0, :cond_1

    .line 28811
    :cond_0
    :goto_0
    return v0

    .line 28803
    :cond_1
    instance-of v2, p1, Lhdb;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 28804
    :cond_2
    check-cast p1, Lhdb;

    .line 28805
    iget-object v2, p0, Lhdb;->a:Lhbt;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhdb;->a:Lhbt;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhdb;->b:Lhbt;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhdb;->b:Lhbt;

    if-nez v2, :cond_3

    .line 28806
    :goto_2
    iget-object v2, p0, Lhdb;->c:Lhbt;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhdb;->c:Lhbt;

    if-nez v2, :cond_3

    .line 28807
    :goto_3
    iget-object v2, p0, Lhdb;->d:Lhbt;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhdb;->d:Lhbt;

    if-nez v2, :cond_3

    .line 28808
    :goto_4
    iget-object v2, p0, Lhdb;->e:Lhbt;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhdb;->e:Lhbt;

    if-nez v2, :cond_3

    .line 28809
    :goto_5
    iget-object v2, p0, Lhdb;->f:Lhbt;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhdb;->f:Lhbt;

    if-nez v2, :cond_3

    .line 28810
    :goto_6
    iget-object v2, p0, Lhdb;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhdb;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 28811
    goto :goto_0

    .line 28805
    :cond_4
    iget-object v2, p0, Lhdb;->a:Lhbt;

    iget-object v3, p1, Lhdb;->a:Lhbt;

    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhdb;->b:Lhbt;

    iget-object v3, p1, Lhdb;->b:Lhbt;

    .line 28806
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhdb;->c:Lhbt;

    iget-object v3, p1, Lhdb;->c:Lhbt;

    .line 28807
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhdb;->d:Lhbt;

    iget-object v3, p1, Lhdb;->d:Lhbt;

    .line 28808
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhdb;->e:Lhbt;

    iget-object v3, p1, Lhdb;->e:Lhbt;

    .line 28809
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhdb;->f:Lhbt;

    iget-object v3, p1, Lhdb;->f:Lhbt;

    .line 28810
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhdb;->I:Ljava/util/List;

    iget-object v3, p1, Lhdb;->I:Ljava/util/List;

    .line 28811
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 28815
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 28817
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdb;->a:Lhbt;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 28818
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdb;->b:Lhbt;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 28819
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdb;->c:Lhbt;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 28820
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdb;->d:Lhbt;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 28821
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdb;->e:Lhbt;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 28822
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdb;->f:Lhbt;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 28823
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhdb;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 28824
    return v0

    .line 28817
    :cond_0
    iget-object v0, p0, Lhdb;->a:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_0

    .line 28818
    :cond_1
    iget-object v0, p0, Lhdb;->b:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_1

    .line 28819
    :cond_2
    iget-object v0, p0, Lhdb;->c:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_2

    .line 28820
    :cond_3
    iget-object v0, p0, Lhdb;->d:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_3

    .line 28821
    :cond_4
    iget-object v0, p0, Lhdb;->e:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_4

    .line 28822
    :cond_5
    iget-object v0, p0, Lhdb;->f:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_5

    .line 28823
    :cond_6
    iget-object v1, p0, Lhdb;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_6
.end method
