.class public Lfmf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhti;

.field public b:Landroid/text/Spanned;

.field private final c:Lfqh;

.field private d:Ljava/util/List;


# direct methods
.method public constructor <init>(Lhti;Lfqh;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhti;

    iput-object v0, p0, Lfmf;->a:Lhti;

    .line 34
    iput-object p2, p0, Lfmf;->c:Lfqh;

    .line 35
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 45
    iget-object v0, p0, Lfmf;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lfmf;->d:Ljava/util/List;

    .line 62
    :goto_0
    return-object v0

    .line 49
    :cond_0
    iget-object v0, p0, Lfmf;->a:Lhti;

    iget-object v0, v0, Lhti;->b:[Lhtj;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfmf;->d:Ljava/util/List;

    .line 51
    iget-object v0, p0, Lfmf;->d:Ljava/util/List;

    goto :goto_0

    .line 54
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfmf;->d:Ljava/util/List;

    .line 55
    iget-object v0, p0, Lfmf;->a:Lhti;

    iget-object v1, v0, Lhti;->b:[Lhtj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 56
    iget-object v3, v3, Lhtj;->b:Lhth;

    .line 57
    if-eqz v3, :cond_2

    iget-object v4, v3, Lhth;->a:Lhgz;

    if-eqz v4, :cond_2

    iget-object v4, v3, Lhth;->b:Lhog;

    if-eqz v4, :cond_2

    .line 58
    iget-object v4, p0, Lfmf;->d:Ljava/util/List;

    new-instance v5, Lfme;

    invoke-direct {v5, v3}, Lfme;-><init>(Lhth;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 62
    :cond_3
    iget-object v0, p0, Lfmf;->d:Ljava/util/List;

    goto :goto_0
.end method

.method public final a(Lfau;)V
    .locals 2

    .prologue
    .line 77
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 78
    invoke-virtual {p0}, Lfmf;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfme;

    .line 79
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    goto :goto_0

    .line 81
    :cond_0
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfmf;->a:Lhti;

    iget-object v0, v0, Lhti;->c:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lfmf;->c:Lfqh;

    return-object v0
.end method
