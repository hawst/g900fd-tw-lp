.class public Lgmu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgmk;


# instance fields
.field public final a:Lgmt;

.field public final b:Lbjx;

.field public final c:Lexn;

.field public volatile d:Legc;

.field public volatile e:Legc;

.field public volatile f:Ljava/util/List;


# direct methods
.method public constructor <init>(Lgmt;Lbjx;Lexn;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmt;

    iput-object v0, p0, Lgmu;->a:Lgmt;

    .line 37
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjx;

    iput-object v0, p0, Lgmu;->b:Lbjx;

    .line 38
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexn;

    iput-object v0, p0, Lgmu;->c:Lexn;

    .line 40
    iput-object p0, p1, Lgmt;->e:Lgmu;

    .line 41
    invoke-virtual {p0}, Lgmu;->h()V

    .line 42
    return-void
.end method


# virtual methods
.method public b()Legc;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lgmu;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "Using SD card for offline content."

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lgmu;->e()Legc;

    move-result-object v0

    .line 55
    :goto_0
    return-object v0

    .line 54
    :cond_0
    const-string v0, "Using Primary Stroage for offline content."

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lgmu;->d()Legc;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Ljava/io/File;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lgmu;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lgmu;->a:Lgmt;

    invoke-virtual {v0}, Lgmt;->b()Ljava/io/File;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgmu;->a:Lgmt;

    invoke-virtual {v0}, Lgmt;->a()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Legc;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lgmu;->d:Legc;

    return-object v0
.end method

.method public e()Legc;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lgmu;->e:Legc;

    return-object v0
.end method

.method public synthetic e_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lgmu;->i()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0}, Lgmu;->h()V

    .line 117
    return-void
.end method

.method public g()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 45
    iget-object v1, p0, Lgmu;->c:Lexn;

    invoke-virtual {v1}, Lexn;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgmu;->b:Lbjx;

    iget-object v1, v1, Lbjx;->a:Landroid/content/SharedPreferences;

    const-string v2, "offline_use_sd_card"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 84
    iput-object v0, p0, Lgmu;->d:Legc;

    .line 85
    iput-object v0, p0, Lgmu;->e:Legc;

    .line 86
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 89
    iget-object v0, p0, Lgmu;->a:Lgmt;

    invoke-virtual {v0}, Lgmt;->a()Ljava/io/File;

    move-result-object v2

    .line 90
    if-eqz v2, :cond_0

    .line 91
    const-string v3, "offline primary cache dir: "

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 92
    new-instance v0, Legl;

    new-instance v3, Legk;

    invoke-direct {v3}, Legk;-><init>()V

    invoke-direct {v0, v2, v3}, Legl;-><init>(Ljava/io/File;Legd;)V

    iput-object v0, p0, Lgmu;->d:Legc;

    .line 93
    iget-object v0, p0, Lgmu;->d:Legc;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 96
    :cond_0
    iget-object v0, p0, Lgmu;->c:Lexn;

    invoke-virtual {v0}, Lexn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lgmu;->a:Lgmt;

    invoke-virtual {v0}, Lgmt;->b()Ljava/io/File;

    move-result-object v2

    .line 98
    if-eqz v2, :cond_1

    .line 99
    const-string v3, "offline sd card cache dir: "

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 100
    new-instance v0, Legl;

    new-instance v3, Legk;

    invoke-direct {v3}, Legk;-><init>()V

    invoke-direct {v0, v2, v3}, Legl;-><init>(Ljava/io/File;Legd;)V

    iput-object v0, p0, Lgmu;->e:Legc;

    .line 101
    iget-object v0, p0, Lgmu;->e:Legc;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgmu;->f:Ljava/util/List;

    .line 106
    return-void

    .line 91
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public i()Ljava/util/List;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lgmu;->f:Ljava/util/List;

    return-object v0
.end method
