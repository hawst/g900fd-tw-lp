.class public final Laco;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldau;
.implements Lfev;
.implements Lfxf;
.implements Lgjd;


# static fields
.field private static final O:Ljava/util/Map;

.field private static final P:Ljava/util/concurrent/atomic/AtomicReference;

.field private static final Q:Ljava/util/List;

.field private static final R:Ljava/util/List;

.field private static final S:Ljava/util/List;


# instance fields
.field public final A:Lexd;

.field final B:Lezi;

.field final C:Lglm;

.field final D:Lgng;

.field final E:Leyt;

.field final F:Lgjp;

.field final G:Lgjp;

.field final H:Lgjp;

.field public final I:Levn;

.field final J:Lfrd;

.field public final K:Z

.field final L:Lcst;

.field public final M:Lcub;

.field final N:Lgoc;

.field private final T:Lacv;

.field private final U:Lorg/apache/http/client/HttpClient;

.field private final V:Lorg/apache/http/client/HttpClient;

.field private final W:Lfbc;

.field private final X:Lfcs;

.field private final Y:Lert;

.field private final Z:Lacd;

.field final a:Landroid/content/Context;

.field private final aa:Ljava/util/concurrent/Executor;

.field private final ab:Lctd;

.field private final ac:Lglc;

.field private final ad:Lctw;

.field private ae:I

.field final b:Landroid/telephony/TelephonyManager;

.field final c:Landroid/content/pm/PackageManager;

.field final d:Lezj;

.field final e:Landroid/os/Handler;

.field final f:Ljava/util/concurrent/Executor;

.field final g:Lfac;

.field final h:Lfet;

.field final i:Lfdo;

.field final j:Lfgk;

.field final k:Lcwg;

.field public final l:Lfwk;

.field final m:Lery;

.field final n:Lcnu;

.field final o:Lcwq;

.field final p:Lcxi;

.field final q:Lcnq;

.field final r:Lcnm;

.field final s:Lcpd;

.field public final t:Leyp;

.field final u:Lgot;

.field final v:Lgeq;

.field final w:Lgco;

.field final x:Ldmc;

.field final y:Lcyc;

.field final z:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 215
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Laco;->O:Ljava/util/Map;

    .line 222
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Laco;->P:Ljava/util/concurrent/atomic/AtomicReference;

    .line 226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 227
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Laco;->Q:Ljava/util/List;

    .line 229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 230
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Laco;->R:Ljava/util/List;

    .line 232
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 233
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Laco;->S:Ljava/util/List;

    .line 290
    const-string v0, "YouTubeAndroidPlayerAPI"

    invoke-static {v0}, Lezp;->a(Ljava/lang/String;)V

    .line 291
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lacv;Z)V
    .locals 24

    .prologue
    .line 376
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 377
    invoke-static/range {p1 .. p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->a:Landroid/content/Context;

    .line 378
    invoke-static/range {p3 .. p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lacv;

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->T:Lacv;

    .line 379
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput-boolean v0, v1, Laco;->K:Z

    .line 380
    invoke-static {}, Lb;->a()V

    .line 382
    const-string v4, "youtube"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->z:Landroid/content/SharedPreferences;

    .line 384
    const-string v4, "phone"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->b:Landroid/telephony/TelephonyManager;

    .line 386
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->c:Landroid/content/pm/PackageManager;

    .line 388
    new-instance v4, Lezj;

    invoke-direct {v4}, Lezj;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->d:Lezj;

    .line 389
    new-instance v4, Landroid/os/Handler;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->e:Landroid/os/Handler;

    .line 390
    new-instance v4, Levn;

    new-instance v5, Leva;

    invoke-direct {v5}, Leva;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->d:Lezj;

    invoke-direct {v4, v5, v6}, Levn;-><init>(Ljava/util/concurrent/Executor;Lezj;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->I:Levn;

    .line 391
    new-instance v5, Ljava/util/concurrent/ThreadPoolExecutor;

    const/16 v6, 0x10

    const/16 v7, 0x10

    const-wide/16 v8, 0x3c

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v11, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v11}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v12, Lfaa;

    const/4 v4, 0x1

    invoke-direct {v12, v4}, Lfaa;-><init>(I)V

    invoke-direct/range {v5 .. v12}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Laco;->f:Ljava/util/concurrent/Executor;

    .line 392
    move-object/from16 v0, p3

    iget-object v4, v0, Lacv;->a:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v5, v0, Lacv;->b:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "com.google.android.youtube.player"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v7, 0x2f

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v7, 0x2f

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Lgxp;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "(Linux; U; Android "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "; "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    const-string v5, "; "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    sget-object v4, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    const-string v5, " Build/"

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/16 v4, 0x29

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 397
    new-instance v19, Lezn;

    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lezn;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V

    .line 399
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lezn;->a(Ljava/lang/Runnable;)V

    .line 400
    const/4 v4, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, La;->a(Ljava/lang/String;Lezn;Z)Lorg/apache/http/client/HttpClient;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->U:Lorg/apache/http/client/HttpClient;

    .line 404
    const/4 v4, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, La;->b(Ljava/lang/String;Lezn;Z)Lorg/apache/http/client/HttpClient;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->V:Lorg/apache/http/client/HttpClient;

    .line 408
    new-instance v4, Lfac;

    move-object/from16 v0, v19

    invoke-direct {v4, v0}, Lfac;-><init>(Lezn;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->g:Lfac;

    .line 409
    invoke-static {}, Lfbc;->a()Lfbc;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->W:Lfbc;

    .line 410
    new-instance v4, Lfrd;

    invoke-direct {v4}, Lfrd;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->J:Lfrd;

    .line 412
    new-instance v6, Lewo;

    const-string v4, "connectivity"

    .line 413
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    const-string v5, "wifi"

    .line 414
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/WifiManager;

    sget-object v7, Lewo;->a:Lewq;

    invoke-direct {v6, v4, v5, v7}, Lewo;-><init>(Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lewq;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Laco;->A:Lexd;

    .line 416
    new-instance v4, Lezi;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Lezi;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->B:Lezi;

    .line 417
    new-instance v4, Lglj;

    invoke-direct {v4}, Lglj;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->C:Lglm;

    .line 418
    new-instance v4, Lgng;

    invoke-direct {v4}, Lgng;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->D:Lgng;

    .line 419
    new-instance v4, Lcmr;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->A:Lexd;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v5}, Lcmr;-><init>(Landroid/content/Context;Lexd;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->E:Leyt;

    .line 420
    new-instance v4, Leva;

    invoke-direct {v4}, Leva;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->aa:Ljava/util/concurrent/Executor;

    .line 422
    new-instance v11, Lgjc;

    const/16 v4, 0x78

    const/16 v5, 0x1e0

    const/16 v6, 0x53

    invoke-direct {v11, v4, v5, v6}, Lgjc;-><init>(III)V

    .line 428
    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->f:Ljava/util/concurrent/Executor;

    .line 429
    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->f:Ljava/util/concurrent/Executor;

    .line 431
    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->U:Lorg/apache/http/client/HttpClient;

    .line 432
    move-object/from16 v0, p0

    iget-object v10, v0, Laco;->d:Lezj;

    .line 427
    new-instance v4, Lgjb;

    const/4 v9, 0x0

    const/16 v12, 0x46

    const/16 v13, 0x1e

    move-object/from16 v7, p1

    invoke-direct/range {v4 .. v13}, Lgjb;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Landroid/content/Context;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lezj;Lgjc;II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->t:Leyp;

    .line 436
    new-instance v4, Lgop;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->U:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->W:Lfbc;

    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->d:Lezj;

    invoke-direct {v4, v5, v6, v7, v8}, Lgop;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Lezj;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->u:Lgot;

    .line 438
    new-instance v4, Lcmp;

    .line 439
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "youtube"

    invoke-direct {v4, v5, v6}, Lcmp;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->y:Lcyc;

    .line 441
    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->y:Lcyc;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->z:Landroid/content/SharedPreferences;

    invoke-interface {v4, v5}, Lcyc;->a(Landroid/content/SharedPreferences;)V

    .line 443
    new-instance v4, Lgeq;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->A:Lexd;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->y:Lcyc;

    .line 445
    move-object/from16 v0, p1

    invoke-static {v0, v6}, La;->a(Landroid/content/Context;Lezk;)Z

    move-result v6

    .line 446
    invoke-static/range {p1 .. p1}, La;->k(Landroid/content/Context;)Z

    move-result v7

    .line 447
    invoke-static/range {p1 .. p1}, La;->l(Landroid/content/Context;)Z

    move-result v8

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lgeq;-><init>(Lexd;ZZZZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->v:Lgeq;

    .line 450
    new-instance v4, Lgco;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->e:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->A:Lexd;

    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->B:Lezi;

    new-instance v10, Lggn;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->z:Landroid/content/SharedPreferences;

    .line 456
    invoke-static/range {p1 .. p1}, La;->m(Landroid/content/Context;)Z

    move-result v7

    invoke-direct {v10, v5, v7}, Lggn;-><init>(Landroid/content/SharedPreferences;Z)V

    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->J:Lfrd;

    move-object/from16 v0, p0

    iget-object v12, v0, Laco;->d:Lezj;

    move-object/from16 v0, p0

    iget-object v13, v0, Laco;->v:Lgeq;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->y:Lcyc;

    .line 460
    move-object/from16 v0, p1

    invoke-static {v0, v5}, La;->a(Landroid/content/Context;Lezk;)Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->y:Lcyc;

    .line 461
    move-object/from16 v0, p1

    invoke-static {v0, v5}, La;->b(Landroid/content/Context;Lezk;)Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->y:Lcyc;

    .line 462
    move-object/from16 v0, p1

    invoke-static {v0, v5}, La;->c(Landroid/content/Context;Lezk;)Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->y:Lcyc;

    .line 463
    move-object/from16 v0, p1

    invoke-static {v0, v5}, La;->d(Landroid/content/Context;Lezk;)Z

    move-result v17

    move-object/from16 v5, p1

    move-object/from16 v7, v18

    invoke-direct/range {v4 .. v17}, Lgco;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Lexd;Lezi;Lggn;Lewi;Lezj;Lgeq;ZZZZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->w:Lgco;

    .line 465
    new-instance v4, Ldmc;

    invoke-direct {v4}, Ldmc;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->x:Ldmc;

    .line 468
    new-instance v10, Lghf;

    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->z:Landroid/content/SharedPreferences;

    invoke-direct {v10, v4}, Lghf;-><init>(Landroid/content/SharedPreferences;)V

    .line 470
    new-instance v20, Lghm;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->z:Landroid/content/SharedPreferences;

    .line 474
    move-object/from16 v0, p0

    iget-boolean v4, v0, Laco;->K:Z

    if-eqz v4, :cond_3

    sget-object v4, Ladm;->a:Landroid/util/SparseArray;

    :goto_0
    move-object/from16 v0, v20

    invoke-direct {v0, v10, v5, v4}, Lghm;-><init>(Lghr;Landroid/content/SharedPreferences;Landroid/util/SparseArray;)V

    .line 476
    new-instance v21, Lexf;

    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->aa:Ljava/util/concurrent/Executor;

    new-instance v4, Lacq;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, v18

    move-object/from16 v8, v19

    move-object/from16 v9, p3

    invoke-direct/range {v4 .. v10}, Lacq;-><init>(Laco;Landroid/content/Context;Ljava/lang/String;Lezn;Lacv;Lghr;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v11, v4}, Lexf;-><init>(Lezn;Ljava/util/concurrent/Executor;Lewi;)V

    .line 502
    invoke-virtual/range {v21 .. v21}, Lws;->a()V

    .line 504
    new-instance v5, Lery;

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->v:Lgeq;

    .line 507
    invoke-static/range {p1 .. p1}, Lgxp;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 508
    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->y:Lcyc;

    invoke-interface {v4}, Lcyc;->v()Ljava/lang/String;

    move-result-object v9

    .line 509
    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->y:Lcyc;

    invoke-interface {v4}, Lcyc;->w()Ljava/lang/String;

    move-result-object v10

    .line 510
    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->d:Lezj;

    .line 511
    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->y:Lcyc;

    invoke-interface {v4}, Lcyc;->Y()J

    move-result-wide v12

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v13}, Lery;-><init>(Landroid/content/Context;Lgeq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lezj;J)V

    move-object/from16 v0, p0

    iput-object v5, v0, Laco;->m:Lery;

    .line 513
    new-instance v18, Ladb;

    move-object/from16 v0, p3

    iget-object v4, v0, Lacv;->c:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v5, v0, Lacv;->a:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Ladb;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 526
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527
    new-instance v4, Leta;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->m:Lery;

    invoke-direct {v4, v5}, Leta;-><init>(Letb;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    new-instance v4, Ladh;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->b:Landroid/telephony/TelephonyManager;

    .line 531
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v5, v6}, Ladh;-><init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Landroid/content/pm/PackageManager;)V

    .line 528
    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 533
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->g:Lfac;

    invoke-static {v4, v5}, La;->a(Landroid/content/ContentResolver;Lfac;)Ljava/lang/String;

    move-result-object v9

    .line 536
    if-eqz p4, :cond_4

    .line 537
    const-string v10, "YouTubeApplication"

    .line 545
    :goto_1
    new-instance v4, Lgiq;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->z:Landroid/content/SharedPreferences;

    .line 549
    invoke-direct/range {p0 .. p0}, Laco;->d()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v5, v21

    move-object/from16 v7, v20

    invoke-direct/range {v4 .. v10}, Lgiq;-><init>(Lws;Landroid/content/SharedPreferences;Lghm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    new-instance v5, Ljava/util/HashSet;

    sget-object v6, Lfcd;->a:Ljava/util/Set;

    invoke-direct {v5, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 555
    const-string v6, "https://www.googleapis.com/auth/identity.plus.page.impersonation"

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 556
    new-instance v22, Lfcd;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lfcd;-><init>(Landroid/content/Context;Ljava/util/Set;)V

    .line 557
    new-instance v23, Lfbu;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v5}, Lfbu;-><init>(Landroid/content/Context;Lfcd;Ljava/util/concurrent/Executor;)V

    .line 562
    if-eqz p4, :cond_5

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->z:Landroid/content/SharedPreferences;

    move-object/from16 v0, p3

    iget-object v7, v0, Lacv;->a:Ljava/lang/String;

    .line 563
    new-instance v5, Lcst;

    new-instance v8, Lctx;

    invoke-direct {v8, v6, v7}, Lctx;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    invoke-direct {v5, v8}, Lcst;-><init>(Lctj;)V

    .line 564
    :goto_2
    move-object/from16 v0, p0

    iput-object v5, v0, Laco;->L:Lcst;

    .line 566
    new-instance v5, Lctd;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->L:Lcst;

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-direct {v5, v0, v6, v1, v2}, Lctd;-><init>(Lfbu;Lgix;Landroid/content/Context;Lghp;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Laco;->ab:Lctd;

    .line 571
    new-instance v5, Lglc;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->z:Landroid/content/SharedPreferences;

    invoke-direct {v5, v6}, Lglc;-><init>(Landroid/content/SharedPreferences;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Laco;->ac:Lglc;

    .line 572
    new-instance v5, Lctw;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->L:Lcst;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-direct {v5, v6, v0, v1}, Lctw;-><init>(Lgix;Landroid/content/Context;Lghp;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Laco;->ad:Lctw;

    .line 577
    new-instance v5, Lfyy;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->z:Landroid/content/SharedPreferences;

    invoke-direct {v5, v6}, Lfyy;-><init>(Landroid/content/SharedPreferences;)V

    .line 579
    new-instance v6, Lfxj;

    const/16 v7, 0xf

    const/4 v8, 0x0

    const/4 v10, 0x0

    invoke-direct {v6, v5, v7, v8, v10}, Lfxj;-><init>(Lfxl;ILgjk;Ljava/lang/String;)V

    .line 582
    new-instance v10, Lacj;

    .line 584
    move-object/from16 v0, p0

    iget-object v12, v0, Laco;->f:Ljava/util/concurrent/Executor;

    .line 585
    move-object/from16 v0, p0

    iget-object v13, v0, Laco;->U:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p3

    iget-object v14, v0, Lacv;->c:Ljava/lang/String;

    sget-object v16, Ladl;->a:[B

    sget-object v17, Ladl;->b:[B

    move-object/from16 v11, p1

    move-object v15, v9

    invoke-direct/range {v10 .. v17}, Lacj;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;[B[B)V

    .line 591
    new-instance v9, Lacd;

    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v12, v0, Laco;->e:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v13, v0, Laco;->z:Landroid/content/SharedPreferences;

    move-object/from16 v0, p3

    iget-object v14, v0, Lacv;->a:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v15, v0, Lacv;->b:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v0, v0, Lacv;->c:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-direct/range {v9 .. v16}, Lacd;-><init>(Lfws;Ljava/util/concurrent/Executor;Landroid/os/Handler;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Laco;->Z:Lacd;

    .line 600
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 602
    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->ac:Lglc;

    invoke-interface {v10, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 603
    new-instance v12, Lfap;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-direct {v12, v5}, Lfap;-><init>(Landroid/content/ContentResolver;)V

    .line 604
    new-instance v5, Lctb;

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->U:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->Z:Lacd;

    .line 612
    move-object/from16 v0, p0

    iget-object v13, v0, Laco;->W:Lfbc;

    move-object/from16 v11, v23

    invoke-direct/range {v5 .. v13}, Lctb;-><init>(Lfxj;Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lgir;Ljava/util/List;Lfbu;Lfap;Lfbc;)V

    .line 614
    new-instance v7, Lftb;

    new-instance v9, Lcsr;

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Lcsr;-><init>(Lfbu;)V

    .line 617
    invoke-direct/range {p0 .. p0}, Laco;->e()Ljava/util/List;

    move-result-object v10

    .line 618
    move-object/from16 v0, p0

    iget-boolean v8, v0, Laco;->K:Z

    if-nez v8, :cond_6

    sget-object v11, Laco;->R:Ljava/util/List;

    .line 620
    :goto_3
    invoke-direct/range {p0 .. p0}, Laco;->d()Ljava/lang/String;

    move-result-object v13

    .line 621
    invoke-virtual/range {v18 .. v18}, Ladb;->a()Lhxc;

    move-result-object v8

    invoke-static {v8}, Lidh;->a(Lidh;)[B

    move-result-object v8

    invoke-static {v8}, Lfaq;->b([B)[B

    move-result-object v8

    const/16 v12, 0xb

    invoke-static {v8, v12}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v14

    move-object v8, v4

    move-object/from16 v12, v20

    invoke-direct/range {v7 .. v14}, Lftb;-><init>(Lgiq;Lewi;Ljava/util/List;Ljava/util/List;Lghm;Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    new-instance v4, Lcwg;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Lcwg;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->k:Lcwg;

    .line 627
    new-instance v4, Lfcs;

    new-instance v8, Lfsz;

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Lfsz;-><init>(Ljava/util/List;)V

    move-object/from16 v0, v21

    invoke-direct {v4, v7, v8, v0}, Lfcs;-><init>(Lftb;Lfsz;Lws;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->X:Lfcs;

    .line 631
    new-instance v8, Lfet;

    new-instance v10, Lfsz;

    move-object/from16 v0, v19

    invoke-direct {v10, v0}, Lfsz;-><init>(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->L:Lcst;

    .line 636
    move-object/from16 v0, p0

    iget-object v13, v0, Laco;->d:Lezj;

    move-object/from16 v0, p0

    iget-object v14, v0, Laco;->g:Lfac;

    .line 638
    invoke-static/range {p1 .. p1}, La;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    new-instance v16, Lfri;

    const/4 v4, 0x0

    new-array v4, v4, [Lfrj;

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Lfri;-><init>([Lfrj;)V

    move-object v9, v7

    move-object/from16 v12, v21

    invoke-direct/range {v8 .. v16}, Lfet;-><init>(Lftb;Lfsz;Lgix;Lws;Lezj;Lfac;Ljava/lang/String;Lfri;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Laco;->h:Lfet;

    .line 640
    new-instance v4, Lfdo;

    new-instance v8, Lfsz;

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Lfsz;-><init>(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->L:Lcst;

    move-object/from16 v0, v21

    invoke-direct {v4, v7, v8, v9, v0}, Lfdo;-><init>(Lftb;Lfsz;Lgix;Lws;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->i:Lfdo;

    .line 645
    new-instance v4, Lfgk;

    new-instance v8, Lfsz;

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Lfsz;-><init>(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->L:Lcst;

    move-object/from16 v0, v21

    invoke-direct {v4, v7, v8, v9, v0}, Lfgk;-><init>(Lftb;Lfsz;Lgix;Lws;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->j:Lfgk;

    .line 651
    new-instance v9, Lcup;

    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->X:Lfcs;

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->t:Leyp;

    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->L:Lcst;

    invoke-direct {v9, v4, v7, v8}, Lcup;-><init>(Lfcs;Leyp;Lcst;)V

    .line 655
    new-instance v7, Lcub;

    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->L:Lcst;

    new-instance v13, Lcss;

    invoke-direct {v13}, Lcss;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Laco;->aa:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v15, v0, Laco;->f:Ljava/util/concurrent/Executor;

    .line 664
    move-object/from16 v0, p0

    iget-object v0, v0, Laco;->I:Levn;

    move-object/from16 v16, v0

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v5

    invoke-direct/range {v7 .. v16}, Lcub;-><init>(Lcst;Lcup;Lfcd;Lfbu;Lctb;Lcss;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Levn;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Laco;->M:Lcub;

    .line 666
    new-instance v16, Lgiz;

    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->L:Lcst;

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Lgiz;-><init>(Lgix;)V

    .line 667
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    .line 668
    if-nez v4, :cond_2

    .line 669
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v4

    .line 672
    :cond_2
    new-instance v7, Lfwk;

    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v10, v0, Laco;->U:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->d:Lezj;

    move-object/from16 v0, p0

    iget-object v12, v0, Laco;->W:Lfbc;

    move-object/from16 v0, p0

    iget-object v14, v0, Laco;->L:Lcst;

    .line 680
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v8, p1

    move-object v13, v6

    invoke-direct/range {v7 .. v15}, Lfwk;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lezj;Lfbc;Lfxj;Lgix;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Laco;->l:Lfwk;

    .line 681
    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->l:Lfwk;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->Z:Lacd;

    .line 683
    move-object/from16 v0, p0

    iget-boolean v6, v0, Laco;->K:Z

    if-nez v6, :cond_7

    sget-object v6, Laco;->Q:Ljava/util/List;

    :goto_4
    sget-object v9, Lfxh;->a:Lfxh;

    move-object/from16 v7, v23

    move-object/from16 v8, v16

    .line 681
    invoke-virtual/range {v4 .. v9}, Lfwk;->a(Lgir;Ljava/util/List;Lgkb;Lgiz;Lfxh;)V

    .line 688
    new-instance v4, Lcwq;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->I:Levn;

    invoke-direct {v4, v5}, Lcwq;-><init>(Levn;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->o:Lcwq;

    .line 689
    new-instance v4, Lcxi;

    .line 690
    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->I:Levn;

    .line 691
    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->y:Lcyc;

    invoke-interface {v6}, Lcyc;->N()I

    move-result v6

    invoke-direct {v4, v5, v6}, Lcxi;-><init>(Levn;I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->p:Lcxi;

    .line 693
    new-instance v4, Lcnv;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->d:Lezj;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->z:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->L:Lcst;

    .line 694
    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->A:Lexd;

    invoke-direct {v4, v5, v6, v7, v8}, Lcnv;-><init>(Lezj;Landroid/content/SharedPreferences;Lgix;Lexd;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->m:Lery;

    .line 695
    invoke-virtual {v4, v5}, Lcnv;->a(Lery;)Lcnv;

    move-result-object v4

    move-object/from16 v0, p3

    iget-object v5, v0, Lacv;->a:Ljava/lang/String;

    .line 696
    iput-object v5, v4, Lcnv;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->b:Landroid/telephony/TelephonyManager;

    .line 697
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-static {v5, v6}, Lfaq;->a(Landroid/telephony/TelephonyManager;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcnv;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->o:Lcwq;

    .line 698
    iput-object v5, v4, Lcnv;->l:Lcwq;

    .line 699
    invoke-virtual {v4}, Lcnv;->a()Lcnu;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->n:Lcnu;

    .line 701
    new-instance v4, Lgjp;

    .line 702
    invoke-direct/range {p0 .. p0}, Laco;->e()Ljava/util/List;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->Z:Lacd;

    sget-object v8, Lgju;->a:Lgju;

    .line 706
    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->d:Lezj;

    new-instance v10, Lght;

    .line 707
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-direct {v10, v7}, Lght;-><init>(Landroid/content/ContentResolver;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->aa:Ljava/util/concurrent/Executor;

    move-object/from16 v7, v21

    invoke-direct/range {v4 .. v11}, Lgjp;-><init>(Ljava/util/List;Lgir;Lws;Lgju;Lezj;Lght;Ljava/util/concurrent/Executor;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->F:Lgjp;

    .line 710
    new-instance v4, Lgjp;

    .line 711
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->Z:Lacd;

    sget-object v8, Lgju;->a:Lgju;

    .line 715
    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->d:Lezj;

    new-instance v10, Lght;

    .line 716
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-direct {v10, v7}, Lght;-><init>(Landroid/content/ContentResolver;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->aa:Ljava/util/concurrent/Executor;

    move-object/from16 v7, v21

    invoke-direct/range {v4 .. v11}, Lgjp;-><init>(Ljava/util/List;Lgir;Lws;Lgju;Lezj;Lght;Ljava/util/concurrent/Executor;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->G:Lgjp;

    .line 719
    new-instance v4, Lgjp;

    .line 720
    move-object/from16 v0, p0

    iget-boolean v5, v0, Laco;->K:Z

    if-nez v5, :cond_8

    sget-object v5, Laco;->S:Ljava/util/List;

    :goto_5
    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->Z:Lacd;

    sget-object v8, Lgju;->a:Lgju;

    .line 724
    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->d:Lezj;

    new-instance v10, Lght;

    .line 725
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-direct {v10, v7}, Lght;-><init>(Landroid/content/ContentResolver;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->aa:Ljava/util/concurrent/Executor;

    move-object/from16 v7, v21

    invoke-direct/range {v4 .. v11}, Lgjp;-><init>(Ljava/util/List;Lgir;Lws;Lgju;Lezj;Lght;Ljava/util/concurrent/Executor;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->H:Lgjp;

    .line 728
    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->a:Landroid/content/Context;

    invoke-static {v4}, Lgxp;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x8

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/player."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 729
    new-instance v10, Lgoc;

    new-instance v4, Lcvc;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->d:Lezj;

    const/4 v7, 0x0

    .line 733
    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->A:Lexd;

    new-instance v9, Lfaw;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Lfaw;-><init>(Landroid/content/Context;)V

    invoke-direct/range {v4 .. v9}, Lcvc;-><init>(Ljava/lang/String;Lezj;Lezf;Lexd;Lfaw;)V

    invoke-direct {v10, v4}, Lgoc;-><init>(Lgod;)V

    move-object/from16 v0, p0

    iput-object v10, v0, Laco;->N:Lgoc;

    .line 735
    new-instance v4, Lert;

    invoke-direct {v4}, Lert;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->Y:Lert;

    .line 736
    new-instance v11, Lcuz;

    move-object/from16 v0, p0

    iget-object v4, v0, Laco;->d:Lezj;

    new-instance v6, Lcva;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Lcva;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->N:Lgoc;

    invoke-direct {v11, v5, v4, v6, v7}, Lcuz;-><init>(Ljava/lang/String;Lezj;Lcva;Lgoc;)V

    .line 742
    new-instance v4, Lcnm;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->G:Lgjp;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->G:Lgjp;

    .line 745
    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->d:Lezj;

    .line 746
    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->I:Levn;

    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->o:Lcwq;

    move-object/from16 v0, p0

    iget-object v10, v0, Laco;->Y:Lert;

    move-object/from16 v0, p0

    iget-object v12, v0, Laco;->N:Lgoc;

    invoke-direct/range {v4 .. v12}, Lcnm;-><init>(Lgjp;Lgjp;Lezj;Levn;Lcwq;Lert;Lcuz;Lgoc;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->r:Lcnm;

    .line 752
    new-instance v4, Lcpd;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->G:Lgjp;

    .line 754
    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->I:Levn;

    invoke-direct {v4, v5, v6}, Lcpd;-><init>(Lgjp;Levn;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->s:Lcpd;

    .line 756
    new-instance v10, Lacr;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lacr;-><init>(Laco;)V

    .line 763
    new-instance v12, Lcnx;

    invoke-direct {v12}, Lcnx;-><init>()V

    .line 765
    new-instance v4, Lcoe;

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->V:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->W:Lfbc;

    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->d:Lezj;

    .line 770
    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->I:Levn;

    invoke-direct/range {v4 .. v10}, Lcoe;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Lezj;Levn;Lewi;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->n:Lcnu;

    .line 772
    iput-object v5, v4, Lcoe;->i:Lcnu;

    .line 773
    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcoe;->a(Ldau;)Lcoe;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Laco;->r:Lcnm;

    .line 774
    iput-object v5, v4, Lcoe;->j:Lcnm;

    .line 775
    iput-object v12, v4, Lcoe;->l:Lcnx;

    .line 776
    invoke-virtual {v4}, Lcoe;->a()Lcod;

    move-result-object v5

    .line 777
    new-instance v4, Lcnq;

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->f:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->d:Lezj;

    move-object/from16 v0, p0

    iget-object v9, v0, Laco;->r:Lcnm;

    move-object/from16 v0, p0

    iget-object v10, v0, Laco;->N:Lgoc;

    move-object/from16 v0, p0

    iget-object v11, v0, Laco;->g:Lfac;

    move-object v6, v12

    invoke-direct/range {v4 .. v11}, Lcnq;-><init>(Lcnn;Lcnx;Ljava/util/concurrent/Executor;Lezj;Lcnm;Lgoc;Lfac;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laco;->q:Lcnq;

    .line 785
    return-void

    .line 474
    :cond_3
    sget-object v4, Ladl;->c:Landroid/util/SparseArray;

    goto/16 :goto_0

    .line 539
    :cond_4
    const-string v4, "%s_%s_%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p3

    iget-object v7, v0, Lacv;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p3

    iget-object v7, v0, Lacv;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    move-object/from16 v0, p3

    iget-object v7, v0, Lacv;->c:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1

    .line 564
    :cond_5
    invoke-static {}, Lcst;->a()Lcst;

    move-result-object v5

    goto/16 :goto_2

    .line 618
    :cond_6
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Laco;->ac:Lglc;

    invoke-interface {v11, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 683
    :cond_7
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->ac:Lglc;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v7, v0, Laco;->ab:Lctd;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 720
    :cond_8
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->ac:Lglc;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->ab:Lctd;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v6, v0, Laco;->ad:Lctw;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5
.end method

.method static synthetic a(ZLandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Laco;
    .locals 4

    .prologue
    .line 172
    new-instance v1, Lacv;

    invoke-direct {v1, p3, p4, p2}, Lacv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p0, :cond_1

    new-instance v0, Laco;

    invoke-direct {v0, p1, p5, v1, p6}, Laco;-><init>(Landroid/content/Context;Ljava/lang/String;Lacv;Z)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Laco;->O:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laco;

    if-nez v0, :cond_0

    sget-object v0, Laco;->P:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laco;

    if-eqz v0, :cond_2

    iget-object v2, v0, Laco;->T:Lacv;

    invoke-virtual {v2, v1}, Lacv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Laco;->P:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :goto_1
    sget-object v2, Laco;->O:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    new-instance v0, Laco;

    invoke-direct {v0, p1, p5, v1, p6}, Laco;-><init>(Landroid/content/Context;Ljava/lang/String;Lacv;Z)V

    goto :goto_1
.end method

.method static synthetic a(Laco;)Lcyc;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Laco;->y:Lcyc;

    return-object v0
.end method

.method public static a(Ljava/lang/Exception;)Lgwg;
    .locals 1

    .prologue
    .line 1112
    instance-of v0, p0, Lezu;

    if-eqz v0, :cond_0

    .line 1113
    sget-object v0, Lgwg;->f:Lgwg;

    .line 1122
    :goto_0
    return-object v0

    .line 1114
    :cond_0
    instance-of v0, p0, Lacn;

    if-eqz v0, :cond_1

    .line 1116
    sget-object v0, Lgwg;->e:Lgwg;

    goto :goto_0

    .line 1117
    :cond_1
    instance-of v0, p0, Ljava/net/UnknownHostException;

    if-nez v0, :cond_2

    instance-of v0, p0, Ljava/net/SocketException;

    if-nez v0, :cond_2

    instance-of v0, p0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_3

    .line 1120
    :cond_2
    sget-object v0, Lgwg;->d:Lgwg;

    goto :goto_0

    .line 1122
    :cond_3
    sget-object v0, Lgwg;->b:Lgwg;

    goto :goto_0
.end method

.method static synthetic a(Laco;Lacu;)V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Laco;->Z:Lacd;

    new-instance v1, Lact;

    invoke-direct {v1, p0, p1}, Lact;-><init>(Laco;Lacu;)V

    iget-object v2, v0, Lacd;->f:Lgbl;

    if-eqz v2, :cond_0

    invoke-interface {v1}, Laci;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, v0, Lacd;->c:Landroid/content/SharedPreferences;

    iget-object v3, v0, Lacd;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lgbl;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lgbl;

    move-result-object v2

    iput-object v2, v0, Lacd;->f:Lgbl;

    iget-object v2, v0, Lacd;->f:Lgbl;

    if-eqz v2, :cond_1

    invoke-interface {v1}, Laci;->a()V

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lacd;->b:Ljava/util/concurrent/Executor;

    new-instance v3, Lace;

    invoke-direct {v3, v0, v1}, Lace;-><init>(Lacd;Laci;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 910
    sget-object v0, Laco;->P:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laco;

    .line 911
    if-eqz v0, :cond_0

    iget-object v1, v0, Laco;->T:Lacv;

    iget-object v1, v1, Lacv;->a:Ljava/lang/String;

    .line 912
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 913
    sget-object v1, Laco;->P:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 915
    :cond_0
    return-void
.end method

.method public static a(ZLacu;Landroid/os/Handler;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9

    .prologue
    .line 309
    new-instance v0, Lacp;

    move v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move/from16 v7, p8

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lacp;-><init>(ZLandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLacu;)V

    invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 327
    return-void
.end method

.method static synthetic b(Laco;)Lfbc;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Laco;->W:Lfbc;

    return-object v0
.end method

.method static synthetic c(Laco;)I
    .locals 2

    .prologue
    .line 172
    iget v0, p0, Laco;->ae:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Laco;->ae:I

    return v0
.end method

.method private d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1170
    iget-boolean v0, p0, Laco;->K:Z

    if-eqz v0, :cond_0

    const-string v0, "AIzaSyA8eiZmM1FaDVjRy-df2KTyQ_vz_yYM39w"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "AIzaSyCjc_pVEDi4qsv5MtC2dMXzpIaDoRFLsxw"

    goto :goto_0
.end method

.method private e()Ljava/util/List;
    .locals 2

    .prologue
    .line 1174
    iget-boolean v0, p0, Laco;->K:Z

    if-nez v0, :cond_0

    .line 1175
    sget-object v0, Laco;->S:Ljava/util/List;

    .line 1181
    :goto_0
    return-object v0

    .line 1178
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1179
    iget-object v1, p0, Laco;->ac:Lglc;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1180
    iget-object v1, p0, Laco;->ab:Lctd;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfoy;)Ldaq;
    .locals 8

    .prologue
    .line 1153
    new-instance v0, Ldaq;

    .line 1154
    iget-object v1, p0, Laco;->I:Levn;

    .line 1155
    iget-object v2, p0, Laco;->h:Lfet;

    .line 1156
    iget-object v3, p0, Laco;->k:Lcwg;

    iget-object v4, p0, Laco;->L:Lcst;

    new-instance v5, Lgng;

    invoke-direct {v5}, Lgng;-><init>()V

    .line 1159
    iget-object v6, p0, Laco;->f:Ljava/util/concurrent/Executor;

    new-instance v7, Lcmw;

    invoke-direct {v7, p1}, Lcmw;-><init>(Lfoy;)V

    invoke-direct/range {v0 .. v7}, Ldaq;-><init>(Levn;Lfet;Lcwg;Lgix;Lgng;Ljava/util/concurrent/Executor;Lftg;)V

    return-object v0
.end method

.method public final a()Lfet;
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Laco;->h:Lfet;

    return-object v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 899
    iget v0, p0, Laco;->ae:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 900
    iget v0, p0, Laco;->ae:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Laco;->ae:I

    .line 901
    iget v0, p0, Laco;->ae:I

    if-gtz v0, :cond_0

    .line 902
    sget-object v0, Laco;->O:Ljava/util/Map;

    iget-object v1, p0, Laco;->T:Lacv;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 903
    if-eqz p1, :cond_0

    .line 904
    sget-object v0, Laco;->P:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 907
    :cond_0
    return-void

    .line 899
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lfxe;
    .locals 1

    .prologue
    .line 958
    iget-object v0, p0, Laco;->l:Lfwk;

    return-object v0
.end method

.method public final c()Leyp;
    .locals 1

    .prologue
    .line 968
    iget-object v0, p0, Laco;->t:Leyp;

    return-object v0
.end method
