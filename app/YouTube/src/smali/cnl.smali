.class public final enum Lcnl;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcnl;

.field public static final enum b:Lcnl;

.field private static final synthetic d:[Lcnl;


# instance fields
.field final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 321
    new-instance v0, Lcnl;

    const-string v1, "INSTREAM"

    const-string v2, "1"

    invoke-direct {v0, v1, v3, v2}, Lcnl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcnl;->a:Lcnl;

    .line 322
    new-instance v0, Lcnl;

    const-string v1, "TRUEVIEW_INDISPLAY"

    const-string v2, "2"

    invoke-direct {v0, v1, v4, v2}, Lcnl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcnl;->b:Lcnl;

    .line 320
    const/4 v0, 0x2

    new-array v0, v0, [Lcnl;

    sget-object v1, Lcnl;->a:Lcnl;

    aput-object v1, v0, v3

    sget-object v1, Lcnl;->b:Lcnl;

    aput-object v1, v0, v4

    sput-object v0, Lcnl;->d:[Lcnl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 327
    iput-object p3, p0, Lcnl;->c:Ljava/lang/String;

    .line 328
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcnl;
    .locals 1

    .prologue
    .line 331
    sget-object v0, Lcnl;->a:Lcnl;

    iget-object v0, v0, Lcnl;->c:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    sget-object v0, Lcnl;->a:Lcnl;

    .line 337
    :goto_0
    return-object v0

    .line 333
    :cond_0
    sget-object v0, Lcnl;->b:Lcnl;

    iget-object v0, v0, Lcnl;->c:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 334
    sget-object v0, Lcnl;->b:Lcnl;

    goto :goto_0

    .line 337
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcnl;
    .locals 1

    .prologue
    .line 320
    const-class v0, Lcnl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcnl;

    return-object v0
.end method

.method public static values()[Lcnl;
    .locals 1

    .prologue
    .line 320
    sget-object v0, Lcnl;->d:[Lcnl;

    invoke-virtual {v0}, [Lcnl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcnl;

    return-object v0
.end method
