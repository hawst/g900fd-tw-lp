.class public final Lics;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lict;

.field public c:Lhgz;

.field public d:Licr;

.field public e:[B

.field public f:[Lhgz;

.field public g:[Lhgz;

.field private h:[Lhgz;

.field private i:[Lhgz;

.field private j:Lhog;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Lics;->a:Lhgz;

    iput-object v1, p0, Lics;->b:Lict;

    sget-object v0, Lhgz;->a:[Lhgz;

    iput-object v0, p0, Lics;->h:[Lhgz;

    sget-object v0, Lhgz;->a:[Lhgz;

    iput-object v0, p0, Lics;->i:[Lhgz;

    iput-object v1, p0, Lics;->j:Lhog;

    iput-object v1, p0, Lics;->c:Lhgz;

    iput-object v1, p0, Lics;->d:Licr;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lics;->e:[B

    sget-object v0, Lhgz;->a:[Lhgz;

    iput-object v0, p0, Lics;->f:[Lhgz;

    sget-object v0, Lhgz;->a:[Lhgz;

    iput-object v0, p0, Lics;->g:[Lhgz;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lics;->a:Lhgz;

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    iget-object v2, p0, Lics;->a:Lhgz;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lics;->b:Lict;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lics;->b:Lict;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lics;->h:[Lhgz;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lics;->h:[Lhgz;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    if-eqz v5, :cond_1

    const/4 v6, 0x4

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lics;->i:[Lhgz;

    if-eqz v2, :cond_4

    iget-object v3, p0, Lics;->i:[Lhgz;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    if-eqz v5, :cond_3

    const/4 v6, 0x5

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lics;->j:Lhog;

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    iget-object v3, p0, Lics;->j:Lhog;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lics;->c:Lhgz;

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    iget-object v3, p0, Lics;->c:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lics;->d:Licr;

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    iget-object v3, p0, Lics;->d:Licr;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lics;->e:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_8

    const/16 v2, 0xa

    iget-object v3, p0, Lics;->e:[B

    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lics;->f:[Lhgz;

    if-eqz v2, :cond_a

    iget-object v3, p0, Lics;->f:[Lhgz;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_a

    aget-object v5, v3, v2

    if-eqz v5, :cond_9

    const/16 v6, 0xb

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_a
    iget-object v2, p0, Lics;->g:[Lhgz;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lics;->g:[Lhgz;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    if-eqz v4, :cond_b

    const/16 v5, 0xc

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_c
    iget-object v1, p0, Lics;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lics;->J:I

    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lics;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lics;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lics;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lics;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lics;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lics;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lics;->b:Lict;

    if-nez v0, :cond_3

    new-instance v0, Lict;

    invoke-direct {v0}, Lict;-><init>()V

    iput-object v0, p0, Lics;->b:Lict;

    :cond_3
    iget-object v0, p0, Lics;->b:Lict;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lics;->h:[Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgz;

    iget-object v3, p0, Lics;->h:[Lhgz;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lics;->h:[Lhgz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lics;->h:[Lhgz;

    :goto_2
    iget-object v2, p0, Lics;->h:[Lhgz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lics;->h:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lics;->h:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lics;->h:[Lhgz;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lics;->h:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lics;->h:[Lhgz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lics;->i:[Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgz;

    iget-object v3, p0, Lics;->i:[Lhgz;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lics;->i:[Lhgz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lics;->i:[Lhgz;

    :goto_4
    iget-object v2, p0, Lics;->i:[Lhgz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lics;->i:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lics;->i:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lics;->i:[Lhgz;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lics;->i:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lics;->i:[Lhgz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lics;->j:Lhog;

    if-nez v0, :cond_a

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lics;->j:Lhog;

    :cond_a
    iget-object v0, p0, Lics;->j:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lics;->c:Lhgz;

    if-nez v0, :cond_b

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lics;->c:Lhgz;

    :cond_b
    iget-object v0, p0, Lics;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lics;->d:Licr;

    if-nez v0, :cond_c

    new-instance v0, Licr;

    invoke-direct {v0}, Licr;-><init>()V

    iput-object v0, p0, Lics;->d:Licr;

    :cond_c
    iget-object v0, p0, Lics;->d:Licr;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lics;->e:[B

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lics;->f:[Lhgz;

    if-nez v0, :cond_e

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgz;

    iget-object v3, p0, Lics;->f:[Lhgz;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lics;->f:[Lhgz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    iput-object v2, p0, Lics;->f:[Lhgz;

    :goto_6
    iget-object v2, p0, Lics;->f:[Lhgz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    iget-object v2, p0, Lics;->f:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lics;->f:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_e
    iget-object v0, p0, Lics;->f:[Lhgz;

    array-length v0, v0

    goto :goto_5

    :cond_f
    iget-object v2, p0, Lics;->f:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lics;->f:[Lhgz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lics;->g:[Lhgz;

    if-nez v0, :cond_11

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgz;

    iget-object v3, p0, Lics;->g:[Lhgz;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lics;->g:[Lhgz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_10
    iput-object v2, p0, Lics;->g:[Lhgz;

    :goto_8
    iget-object v2, p0, Lics;->g:[Lhgz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_12

    iget-object v2, p0, Lics;->g:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lics;->g:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_11
    iget-object v0, p0, Lics;->g:[Lhgz;

    array-length v0, v0

    goto :goto_7

    :cond_12
    iget-object v2, p0, Lics;->g:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lics;->g:[Lhgz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x62 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lics;->a:Lhgz;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lics;->a:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v1, p0, Lics;->b:Lict;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lics;->b:Lict;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v1, p0, Lics;->h:[Lhgz;

    if-eqz v1, :cond_3

    iget-object v2, p0, Lics;->h:[Lhgz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    if-eqz v4, :cond_2

    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lics;->i:[Lhgz;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lics;->i:[Lhgz;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    if-eqz v4, :cond_4

    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lics;->j:Lhog;

    if-eqz v1, :cond_6

    const/4 v1, 0x6

    iget-object v2, p0, Lics;->j:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_6
    iget-object v1, p0, Lics;->c:Lhgz;

    if-eqz v1, :cond_7

    const/4 v1, 0x7

    iget-object v2, p0, Lics;->c:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_7
    iget-object v1, p0, Lics;->d:Licr;

    if-eqz v1, :cond_8

    const/16 v1, 0x8

    iget-object v2, p0, Lics;->d:Licr;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_8
    iget-object v1, p0, Lics;->e:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Lics;->e:[B

    invoke-virtual {p1, v1, v2}, Lidd;->a(I[B)V

    :cond_9
    iget-object v1, p0, Lics;->f:[Lhgz;

    if-eqz v1, :cond_b

    iget-object v2, p0, Lics;->f:[Lhgz;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    if-eqz v4, :cond_a

    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_b
    iget-object v1, p0, Lics;->g:[Lhgz;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lics;->g:[Lhgz;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_d

    aget-object v3, v1, v0

    if-eqz v3, :cond_c

    const/16 v4, 0xc

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_d
    iget-object v0, p0, Lics;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lics;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lics;

    iget-object v2, p0, Lics;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lics;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lics;->b:Lict;

    if-nez v2, :cond_5

    iget-object v2, p1, Lics;->b:Lict;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lics;->h:[Lhgz;

    iget-object v3, p1, Lics;->h:[Lhgz;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lics;->i:[Lhgz;

    iget-object v3, p1, Lics;->i:[Lhgz;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lics;->j:Lhog;

    if-nez v2, :cond_6

    iget-object v2, p1, Lics;->j:Lhog;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lics;->c:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lics;->c:Lhgz;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lics;->d:Licr;

    if-nez v2, :cond_8

    iget-object v2, p1, Lics;->d:Licr;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lics;->e:[B

    iget-object v3, p1, Lics;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lics;->f:[Lhgz;

    iget-object v3, p1, Lics;->f:[Lhgz;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lics;->g:[Lhgz;

    iget-object v3, p1, Lics;->g:[Lhgz;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lics;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lics;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lics;->a:Lhgz;

    iget-object v3, p1, Lics;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lics;->b:Lict;

    iget-object v3, p1, Lics;->b:Lict;

    invoke-virtual {v2, v3}, Lict;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lics;->j:Lhog;

    iget-object v3, p1, Lics;->j:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lics;->c:Lhgz;

    iget-object v3, p1, Lics;->c:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lics;->d:Licr;

    iget-object v3, p1, Lics;->d:Licr;

    invoke-virtual {v2, v3}, Licr;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lics;->I:Ljava/util/List;

    iget-object v3, p1, Lics;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lics;->a:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lics;->b:Lict;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Lics;->h:[Lhgz;

    if-nez v2, :cond_7

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lics;->i:[Lhgz;

    if-nez v0, :cond_9

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lics;->j:Lhog;

    if-nez v0, :cond_b

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lics;->c:Lhgz;

    if-nez v0, :cond_c

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lics;->d:Licr;

    if-nez v0, :cond_d

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    iget-object v2, p0, Lics;->e:[B

    if-nez v2, :cond_e

    mul-int/lit8 v2, v0, 0x1f

    :cond_2
    iget-object v0, p0, Lics;->f:[Lhgz;

    if-nez v0, :cond_f

    mul-int/lit8 v2, v2, 0x1f

    :cond_3
    iget-object v0, p0, Lics;->g:[Lhgz;

    if-nez v0, :cond_11

    mul-int/lit8 v2, v2, 0x1f

    :cond_4
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lics;->I:Ljava/util/List;

    if-nez v2, :cond_13

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_5
    iget-object v0, p0, Lics;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lics;->b:Lict;

    invoke-virtual {v0}, Lict;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_7
    move v2, v0

    move v0, v1

    :goto_6
    iget-object v3, p0, Lics;->h:[Lhgz;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lics;->h:[Lhgz;

    aget-object v2, v2, v0

    if-nez v2, :cond_8

    move v2, v1

    :goto_7
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v2, p0, Lics;->h:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgz;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_9
    move v0, v1

    :goto_8
    iget-object v3, p0, Lics;->i:[Lhgz;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lics;->i:[Lhgz;

    aget-object v2, v2, v0

    if-nez v2, :cond_a

    move v2, v1

    :goto_9
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_a
    iget-object v2, p0, Lics;->i:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgz;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_b
    iget-object v0, p0, Lics;->j:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_c
    iget-object v0, p0, Lics;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_d
    iget-object v0, p0, Lics;->d:Licr;

    invoke-virtual {v0}, Licr;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_e
    move v2, v0

    move v0, v1

    :goto_a
    iget-object v3, p0, Lics;->e:[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lics;->e:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_f
    move v0, v1

    :goto_b
    iget-object v3, p0, Lics;->f:[Lhgz;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lics;->f:[Lhgz;

    aget-object v2, v2, v0

    if-nez v2, :cond_10

    move v2, v1

    :goto_c
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_10
    iget-object v2, p0, Lics;->f:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgz;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_11
    move v0, v1

    :goto_d
    iget-object v3, p0, Lics;->g:[Lhgz;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lics;->g:[Lhgz;

    aget-object v2, v2, v0

    if-nez v2, :cond_12

    move v2, v1

    :goto_e
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_12
    iget-object v2, p0, Lics;->g:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgz;->hashCode()I

    move-result v2

    goto :goto_e

    :cond_13
    iget-object v1, p0, Lics;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_5
.end method
