.class public Lfvq;
.super Landroid/text/style/ClickableSpan;
.source "SourceFile"


# static fields
.field private static a:Lfvs;

.field private static b:Lfvs;


# instance fields
.field private final c:Lhog;

.field private final d:Lfhz;

.field private final e:Z


# direct methods
.method public constructor <init>(Lfhz;Lhog;Z)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 29
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lfvq;->d:Lfhz;

    .line 30
    iput-object p2, p0, Lfvq;->c:Lhog;

    .line 31
    iput-boolean p3, p0, Lfvq;->e:Z

    .line 32
    return-void
.end method

.method public static a(Z)Lfvs;
    .locals 1

    .prologue
    .line 62
    if-eqz p0, :cond_1

    .line 63
    sget-object v0, Lfvq;->a:Lfvs;

    if-nez v0, :cond_0

    .line 64
    invoke-static {p0}, Lfvq;->b(Z)Lfvs;

    move-result-object v0

    sput-object v0, Lfvq;->a:Lfvs;

    .line 66
    :cond_0
    sget-object v0, Lfvq;->a:Lfvs;

    .line 71
    :goto_0
    return-object v0

    .line 68
    :cond_1
    sget-object v0, Lfvq;->b:Lfvs;

    if-nez v0, :cond_2

    .line 69
    invoke-static {p0}, Lfvq;->b(Z)Lfvs;

    move-result-object v0

    sput-object v0, Lfvq;->b:Lfvs;

    .line 71
    :cond_2
    sget-object v0, Lfvq;->b:Lfvs;

    goto :goto_0
.end method

.method private static b(Z)Lfvs;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lfvr;

    invoke-direct {v0, p0}, Lfvr;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lfvq;->c:Lhog;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lfvq;->d:Lfhz;

    iget-object v1, p0, Lfvq;->c:Lhog;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    .line 39
    :cond_0
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 44
    iget-boolean v0, p0, Lfvq;->e:Z

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 45
    return-void
.end method
