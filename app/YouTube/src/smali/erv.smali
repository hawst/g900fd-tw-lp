.class public final Lerv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/Set;

.field private final b:Lgjp;


# direct methods
.method public constructor <init>(Lgjp;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lerv;->b:Lgjp;

    .line 31
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lerv;->a:Ljava/util/Set;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lerv;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-virtual {p0, p2}, Lerv;->a(Ljava/util/List;)V

    .line 55
    iget-object v0, p0, Lerv;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 74
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 75
    if-eqz v0, :cond_0

    .line 76
    iget-object v2, p0, Lerv;->b:Lgjp;

    const-string v2, "adtracking"

    const v3, 0x323467f

    invoke-static {v2, v3}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v2

    invoke-virtual {v2, v0}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    const/4 v0, 0x0

    iput-boolean v0, v2, Lgjt;->d:Z

    iget-object v0, p0, Lerv;->b:Lgjp;

    sget-object v3, Lggu;->b:Lwu;

    invoke-virtual {v0, v2, v3}, Lgjp;->a(Lgjt;Lwu;)V

    goto :goto_0

    .line 79
    :cond_1
    return-void
.end method
