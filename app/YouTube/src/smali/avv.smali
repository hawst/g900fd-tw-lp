.class public Lavv;
.super Lbhz;
.source "SourceFile"

# interfaces
.implements Ldso;


# instance fields
.field private e:Levn;

.field private f:Lbvz;

.field private g:Ldsn;

.field private h:Lbid;

.field public l:Lbgt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lbhz;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ldwq;Z)V
    .locals 2

    .prologue
    .line 108
    if-eqz p1, :cond_0

    .line 109
    iget-object v0, p0, Lavv;->f:Lbvz;

    invoke-virtual {v0, p1}, Lbvz;->a(Ldwq;)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lavv;->f:Lbvz;

    const/4 v1, 0x0

    iput-object v1, v0, Lbvz;->g:Ldwq;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 36
    invoke-super {p0, p1}, Lbhz;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lavv;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 39
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v1

    .line 40
    iget-object v0, v0, Lckz;->a:Letc;

    .line 41
    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 43
    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v2

    iput-object v2, p0, Lavv;->e:Levn;

    .line 45
    new-instance v2, Lbvz;

    .line 47
    iget-object v4, v1, Lari;->b:Ldov;

    invoke-virtual {v4}, Ldov;->c()Ldxe;

    move-result-object v4

    .line 48
    invoke-virtual {v0}, Letc;->i()Levn;

    move-result-object v0

    invoke-direct {v2, p0, v4, v0}, Lbvz;-><init>(Landroid/content/Context;Ldxe;Levn;)V

    iput-object v2, p0, Lavv;->f:Lbvz;

    .line 50
    invoke-virtual {v1}, Lari;->K()Ldsn;

    move-result-object v0

    iput-object v0, p0, Lavv;->g:Ldsn;

    .line 51
    new-instance v0, Lbgt;

    iget-object v2, p0, Lavv;->g:Ldsn;

    .line 53
    iget-object v2, v2, Ldsn;->a:Ldrw;

    iget-object v2, v2, Ldrw;->b:Lrz;

    .line 54
    iget-object v4, v1, Lari;->b:Ldov;

    invoke-virtual {v4}, Ldov;->l()Ldrw;

    move-result-object v4

    .line 55
    iget-object v5, v1, Lari;->b:Ldov;

    invoke-virtual {v5}, Ldov;->f()Ldsl;

    move-result-object v5

    invoke-direct {v0, p0, v2, v4, v5}, Lbgt;-><init>(Landroid/content/Context;Lrz;Ldrw;Ldsl;)V

    iput-object v0, p0, Lavv;->l:Lbgt;

    .line 56
    iget-object v0, p0, Lavv;->l:Lbgt;

    .line 57
    invoke-virtual {p0}, Lavv;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0e000c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 56
    iput-boolean v2, v0, Lbgt;->a:Z

    invoke-virtual {v0}, Lbgt;->b()V

    .line 59
    const-string v0, "show_dial_screen_tutorial"

    const/4 v2, 0x1

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Lbid;

    .line 62
    invoke-virtual {p0}, Lavv;->B()Lbxu;

    move-result-object v2

    .line 64
    iget-object v1, v1, Lari;->b:Ldov;

    invoke-virtual {v1}, Ldov;->d()Ldwv;

    move-result-object v4

    iget-object v5, p0, Lavv;->l:Lbgt;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbid;-><init>(Lbhz;Lbxu;Landroid/content/SharedPreferences;Ldwv;Lbgt;)V

    iput-object v0, p0, Lavv;->h:Lbid;

    .line 66
    invoke-virtual {p0}, Lavv;->B()Lbxu;

    move-result-object v0

    iget-object v1, p0, Lavv;->h:Lbid;

    invoke-virtual {v0, v1}, Lbxu;->a(Lbxy;)V

    .line 69
    :cond_0
    invoke-virtual {p0}, Lavv;->C()Lcal;

    move-result-object v0

    iget-object v1, p0, Lavv;->l:Lbgt;

    invoke-virtual {v0, v1}, Lcal;->a(Lcam;)V

    .line 70
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lavv;->l:Lbgt;

    invoke-virtual {v0}, Lbgt;->a()V

    .line 87
    invoke-super {p0}, Lbhz;->onDestroy()V

    .line 88
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9

    .prologue
    const/16 v8, 0x18

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    iget-object v3, p0, Lavv;->f:Lbvz;

    iget-object v0, v3, Lbvz;->g:Ldwq;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lbvz;->g:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v4, Ldww;->b:Ldww;

    if-eq v0, v4, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_5

    move v0, v1

    .line 95
    :goto_1
    return v0

    .line 92
    :cond_1
    const/16 v0, 0x19

    if-eq p1, v0, :cond_2

    if-eq p1, v8, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, v3, Lbvz;->c:Landroid/widget/Toast;

    if-nez v0, :cond_3

    new-instance v0, Landroid/widget/Toast;

    iget-object v4, v3, Lbvz;->a:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v0, v3, Lbvz;->c:Landroid/widget/Toast;

    iget-object v0, v3, Lbvz;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f04012c

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iget-object v0, v3, Lbvz;->c:Landroid/widget/Toast;

    invoke-virtual {v0, v4}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    iget-object v0, v3, Lbvz;->c:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    iget-object v0, v3, Lbvz;->c:Landroid/widget/Toast;

    const/16 v5, 0x30

    iget-object v6, v3, Lbvz;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0099

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v0, v5, v2, v6}, Landroid/widget/Toast;->setGravity(III)V

    const v0, 0x7f080332

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v3, Lbvz;->e:Landroid/widget/ImageView;

    iget-object v0, v3, Lbvz;->e:Landroid/widget/ImageView;

    const v5, 0x7f0200b8

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, v3, Lbvz;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f080333

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v3, Lbvz;->d:Landroid/widget/ProgressBar;

    iget-object v0, v3, Lbvz;->d:Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, v3, Lbvz;->d:Landroid/widget/ProgressBar;

    iget v2, v3, Lbvz;->f:I

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_3
    iget-object v0, v3, Lbvz;->c:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    if-ne p1, v8, :cond_4

    iget-object v0, v3, Lbvz;->b:Ldxe;

    invoke-interface {v0}, Ldxe;->a()V

    :goto_2
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v0, v3, Lbvz;->b:Ldxe;

    invoke-interface {v0}, Ldxe;->b()V

    goto :goto_2

    .line 95
    :cond_5
    invoke-super {p0, p1, p2}, Lbhz;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_1
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 74
    invoke-super {p0}, Lbhz;->onStart()V

    .line 75
    iget-object v0, p0, Lavv;->g:Ldsn;

    invoke-virtual {v0}, Ldsn;->a()V

    iget-object v0, p0, Lavv;->g:Ldsn;

    invoke-virtual {v0, p0}, Ldsn;->a(Ldso;)V

    iget-object v0, p0, Lavv;->g:Ldsn;

    iget-object v0, v0, Ldsn;->d:Ldwq;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lavv;->f:Lbvz;

    invoke-virtual {v1, v0}, Lbvz;->a(Ldwq;)V

    :goto_0
    iget-object v0, p0, Lavv;->l:Lbgt;

    iget-object v1, v0, Lbgt;->c:Lsb;

    iget-object v2, v0, Lbgt;->b:Lrz;

    iget-object v0, v0, Lbgt;->d:Lbgu;

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v0, v3}, Lsb;->a(Lrz;Lsc;I)V

    iget-object v0, p0, Lavv;->h:Lbid;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lavv;->e:Levn;

    iget-object v1, p0, Lavv;->h:Lbid;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    .line 76
    :cond_0
    return-void

    .line 75
    :cond_1
    iget-object v0, p0, Lavv;->f:Lbvz;

    const/4 v1, 0x0

    iput-object v1, v0, Lbvz;->g:Ldwq;

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lavv;->g:Ldsn;

    iget v1, v0, Ldsn;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Ldsn;->c:I

    iget v1, v0, Ldsn;->c:I

    if-nez v1, :cond_0

    iget-object v1, v0, Ldsn;->b:Lsb;

    invoke-virtual {v1, v0}, Lsb;->a(Lsc;)V

    :cond_0
    iget-object v0, p0, Lavv;->g:Ldsn;

    invoke-virtual {v0, p0}, Ldsn;->b(Ldso;)V

    iget-object v0, p0, Lavv;->h:Lbid;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lavv;->e:Levn;

    iget-object v1, p0, Lavv;->h:Lbid;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lavv;->l:Lbgt;

    invoke-virtual {v0}, Lbgt;->a()V

    .line 81
    invoke-super {p0}, Lbhz;->onStop()V

    .line 82
    return-void
.end method
