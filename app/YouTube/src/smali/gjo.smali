.class public final Lgjo;
.super Lgkx;
.source "SourceFile"

# interfaces
.implements Lgjw;


# instance fields
.field private final j:Ljava/util/List;

.field private final k:Lgir;

.field private final l:Ljava/lang/String;

.field private final m:Lezj;

.field private final n:Ljava/lang/String;

.field private final o:I

.field private final p:J

.field private final q:Ljava/util/Map;

.field private final r:Lgjr;

.field private final s:Lgkm;


# direct methods
.method public constructor <init>(ILjava/lang/String;ZLjava/lang/String;IJLjava/util/Map;Lgjr;Lwu;Ljava/util/List;Lgir;Lezj;Lght;Lgkm;)V
    .locals 4

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p10}, Lgkx;-><init>(ILjava/lang/String;Lwu;)V

    .line 76
    new-instance v0, Lwg;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 77
    invoke-virtual/range {p14 .. p14}, Lght;->f()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lwg;-><init>(IIF)V

    .line 76
    iput-object v0, p0, Lwp;->h:Lwx;

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lwp;->e:Z

    .line 80
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgjo;->l:Ljava/lang/String;

    .line 81
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgjo;->n:Ljava/lang/String;

    .line 82
    iput p5, p0, Lgjo;->o:I

    .line 83
    iput-wide p6, p0, Lgjo;->p:J

    .line 84
    iput-object p8, p0, Lgjo;->q:Ljava/util/Map;

    .line 85
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjr;

    iput-object v0, p0, Lgjo;->r:Lgjr;

    .line 86
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgjo;->j:Ljava/util/List;

    .line 87
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgir;

    iput-object v0, p0, Lgjo;->k:Lgir;

    .line 88
    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgjo;->m:Lezj;

    .line 89
    invoke-static/range {p15 .. p15}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkm;

    iput-object v0, p0, Lgjo;->s:Lgkm;

    .line 91
    return-void
.end method


# virtual methods
.method protected final a(Lwm;)Lwt;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 154
    invoke-static {v0, v0}, Lwt;->a(Ljava/lang/Object;Lwd;)Lwt;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgjo;->r:Lgjr;

    invoke-interface {v0}, Lgjr;->a()V

    return-void
.end method

.method public final b(Lxa;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lgjo;->r:Lgjr;

    invoke-static {p1}, Lggu;->a(Lxa;)I

    invoke-interface {v0}, Lgjr;->b()V

    .line 165
    return-void
.end method

.method public final c()Ljava/util/Map;
    .locals 5

    .prologue
    .line 95
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 98
    iget-object v0, p0, Lgjo;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkl;

    .line 99
    iget-object v3, p0, Lgjo;->s:Lgkm;

    invoke-interface {v0}, Lgkl;->a()I

    move-result v4

    invoke-interface {v3, v4}, Lgkm;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 100
    invoke-interface {v0, v1, p0}, Lgkl;->a(Ljava/util/Map;Lgkt;)V

    goto :goto_0

    .line 105
    :cond_1
    :try_start_0
    const-string v0, "X-GData-Device"

    iget-object v2, p0, Lgjo;->k:Lgir;

    iget-object v3, p0, Lgjo;->l:Ljava/lang/String;

    .line 107
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v2, v3}, Lgir;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lgis; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected final f()Ljava/util/Map;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lgjo;->q:Ljava/util/Map;

    return-object v0
.end method

.method public final m()Ldzy;
    .locals 5

    .prologue
    .line 127
    new-instance v2, Ldzy;

    invoke-direct {v2}, Ldzy;-><init>()V

    .line 128
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldzy;->a(Ljava/lang/String;)Ldzy;

    .line 129
    iget-object v0, p0, Lgjo;->n:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ldzy;->c(Ljava/lang/String;)Ldzy;

    .line 130
    iget-wide v0, p0, Lgjo;->p:J

    invoke-virtual {v2, v0, v1}, Ldzy;->b(J)Ldzy;

    .line 131
    iget-object v0, p0, Lgjo;->m:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ldzy;->a(J)Ldzy;

    .line 132
    iget v0, p0, Lgjo;->o:I

    invoke-virtual {v2, v0}, Ldzy;->a(I)Ldzy;

    .line 133
    invoke-virtual {p0}, Lgjo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldzy;->b(Ljava/lang/String;)Ldzy;

    .line 134
    iget v0, p0, Lwp;->a:I

    invoke-virtual {v2, v0}, Ldzy;->b(I)Ldzy;

    .line 136
    :try_start_0
    invoke-virtual {p0}, Lgjo;->h()[B

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    invoke-static {v0}, Licw;->a([B)Licw;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldzy;->a(Licw;)Ldzy;
    :try_end_0
    .catch Lwb; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lgjo;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 144
    new-instance v4, Ldzx;

    invoke-direct {v4}, Ldzx;-><init>()V

    .line 145
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ldzx;->a(Ljava/lang/String;)Ldzx;

    move-result-object v1

    .line 146
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ldzx;->b(Ljava/lang/String;)Ldzx;

    move-result-object v0

    .line 144
    invoke-virtual {v2, v0}, Ldzy;->a(Ldzx;)Ldzy;

    goto :goto_1

    .line 140
    :catch_0
    move-exception v0

    .line 141
    const-string v1, "Auth failure: "

    invoke-virtual {v0}, Lwb;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 148
    :cond_2
    return-object v2
.end method
