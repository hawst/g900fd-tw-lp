.class public final Laba;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:J

.field private synthetic b:Laax;


# direct methods
.method public constructor <init>(Laax;)V
    .locals 2

    .prologue
    .line 142
    iput-object p1, p0, Laba;->b:Laax;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Laba;->a:J

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 152
    :try_start_0
    iget-object v0, p0, Laba;->b:Laax;

    const/4 v1, 0x1

    iput-boolean v1, v0, Laax;->e:Z

    .line 154
    :cond_0
    :goto_0
    iget-object v0, p0, Laba;->b:Laax;

    iget-object v1, v0, Laax;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_1
    :try_start_1
    iget-object v0, p0, Laba;->b:Laax;

    iget-object v0, v0, Laax;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Laba;->b:Laax;

    const/4 v2, 0x0

    iput-boolean v2, v0, Laax;->d:Z

    .line 158
    iget-object v0, p0, Laba;->b:Laax;

    iget-object v0, v0, Laax;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    goto :goto_1

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 188
    :catch_0
    move-exception v0

    const-string v0, "GoogleConversionReporter"

    const-string v1, "Dispatch thread is interrupted."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v0, p0, Laba;->b:Laax;

    iput-boolean v5, v0, Laax;->e:Z

    .line 191
    return-void

    .line 160
    :cond_1
    :try_start_3
    iget-object v0, p0, Laba;->b:Laax;

    const/4 v2, 0x1

    iput-boolean v2, v0, Laax;->d:Z

    .line 161
    iget-object v0, p0, Laba;->b:Laax;

    iget-object v0, v0, Laax;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaw;

    .line 162
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 163
    if-eqz v0, :cond_0

    .line 164
    :try_start_4
    iget-object v1, p0, Laba;->b:Laax;

    .line 168
    iget-object v1, v1, Laax;->c:Landroid/content/Context;

    iget-object v2, v0, Laaw;->e:Ljava/lang/String;

    iget-object v3, v0, Laaw;->f:Ljava/lang/String;

    iget-boolean v4, v0, Laaw;->b:Z

    .line 167
    invoke-static {v1, v2, v3, v4}, Labd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 170
    iget-object v1, p0, Laba;->b:Laax;

    iget-object v1, v1, Laax;->f:Labb;

    invoke-virtual {v1, v0}, Labb;->a(Laaw;)V

    goto :goto_0

    .line 173
    :cond_2
    iget-object v1, p0, Laba;->b:Laax;

    invoke-virtual {v1, v0}, Laax;->a(Laaw;)I

    move-result v1

    .line 174
    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 175
    iget-object v1, p0, Laba;->b:Laax;

    iget-object v1, v1, Laax;->f:Labb;

    invoke-virtual {v1, v0}, Labb;->a(Laaw;)V

    .line 176
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Laba;->a:J

    goto :goto_0

    .line 177
    :cond_3
    if-nez v1, :cond_5

    .line 178
    iget-object v1, p0, Laba;->b:Laax;

    iget-object v1, v1, Laax;->f:Labb;

    invoke-virtual {v1, v0}, Labb;->b(Laaw;)V

    .line 180
    iget-wide v0, p0, Laba;->a:J

    cmp-long v0, v0, v8

    if-nez v0, :cond_4

    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Laba;->a:J

    .line 181
    :goto_2
    iget-wide v0, p0, Laba;->a:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    goto/16 :goto_0

    .line 180
    :cond_4
    iget-wide v0, p0, Laba;->a:J

    shl-long/2addr v0, v6

    const-wide/32 v2, 0xea60

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Laba;->a:J

    goto :goto_2

    .line 183
    :cond_5
    iget-object v1, p0, Laba;->b:Laax;

    iget-object v1, v1, Laax;->f:Labb;

    invoke-virtual {v1, v0}, Labb;->b(Laaw;)V

    .line 184
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Laba;->a:J
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0
.end method
