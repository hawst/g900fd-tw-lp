.class public Lws;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Ljava/util/Set;

.field final c:Ljava/util/concurrent/PriorityBlockingQueue;

.field private d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final e:Ljava/util/concurrent/PriorityBlockingQueue;

.field private final f:Lwc;

.field private final g:Lwj;

.field private final h:Lww;

.field private i:[Lwk;

.field private j:Lwe;


# direct methods
.method public constructor <init>(Lwc;Lwj;)V
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lws;-><init>(Lwc;Lwj;I)V

    .line 126
    return-void
.end method

.method private constructor <init>(Lwc;Lwj;I)V
    .locals 4

    .prologue
    .line 114
    const/4 v0, 0x4

    new-instance v1, Lww;

    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v1, v2}, Lww;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lws;-><init>(Lwc;Lwj;ILww;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Lwc;Lwj;ILww;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lws;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lws;->a:Ljava/util/Map;

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lws;->b:Ljava/util/Set;

    .line 65
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lws;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 69
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lws;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 100
    iput-object p1, p0, Lws;->f:Lwc;

    .line 101
    iput-object p2, p0, Lws;->g:Lwj;

    .line 102
    new-array v0, p3, [Lwk;

    iput-object v0, p0, Lws;->i:[Lwk;

    .line 103
    iput-object p4, p0, Lws;->h:Lww;

    .line 104
    return-void
.end method


# virtual methods
.method public a(Lwp;)Lwp;
    .locals 5

    .prologue
    .line 219
    iput-object p0, p1, Lwp;->d:Lws;

    .line 220
    iget-object v1, p0, Lws;->b:Ljava/util/Set;

    monitor-enter v1

    .line 221
    :try_start_0
    iget-object v0, p0, Lws;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 222
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    invoke-virtual {p0}, Lws;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lwp;->c:Ljava/lang/Integer;

    .line 226
    const-string v0, "add-to-queue"

    invoke-virtual {p1, v0}, Lwp;->a(Ljava/lang/String;)V

    .line 229
    iget-boolean v0, p1, Lwp;->e:Z

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lws;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 254
    :goto_0
    return-object p1

    .line 222
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 235
    :cond_0
    iget-object v1, p0, Lws;->a:Ljava/util/Map;

    monitor-enter v1

    .line 236
    :try_start_2
    invoke-virtual {p1}, Lwp;->b()Ljava/lang/String;

    move-result-object v2

    .line 237
    iget-object v0, p0, Lws;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    iget-object v0, p0, Lws;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 240
    if-nez v0, :cond_1

    .line 241
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 243
    :cond_1
    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 244
    iget-object v3, p0, Lws;->a:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-boolean v0, Lxb;->a:Z

    if-eqz v0, :cond_2

    .line 246
    const-string v0, "Request for cacheKey=%s is in flight, putting on hold."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v3}, Lxb;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    :cond_2
    :goto_1
    monitor-exit v1

    goto :goto_0

    .line 255
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 251
    :cond_3
    :try_start_3
    iget-object v0, p0, Lws;->a:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object v0, p0, Lws;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1
.end method

.method public a()V
    .locals 6

    .prologue
    .line 132
    invoke-virtual {p0}, Lws;->b()V

    .line 134
    new-instance v0, Lwe;

    iget-object v1, p0, Lws;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v2, p0, Lws;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lws;->f:Lwc;

    iget-object v4, p0, Lws;->h:Lww;

    invoke-direct {v0, v1, v2, v3, v4}, Lwe;-><init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lwc;Lww;)V

    iput-object v0, p0, Lws;->j:Lwe;

    .line 135
    iget-object v0, p0, Lws;->j:Lwe;

    invoke-virtual {v0}, Lwe;->start()V

    .line 138
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lws;->i:[Lwk;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 139
    new-instance v1, Lwk;

    iget-object v2, p0, Lws;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lws;->g:Lwj;

    iget-object v4, p0, Lws;->f:Lwc;

    iget-object v5, p0, Lws;->h:Lww;

    invoke-direct {v1, v2, v3, v4, v5}, Lwk;-><init>(Ljava/util/concurrent/BlockingQueue;Lwj;Lwc;Lww;)V

    .line 141
    iget-object v2, p0, Lws;->i:[Lwk;

    aput-object v1, v2, v0

    .line 142
    invoke-virtual {v1}, Lwk;->start()V

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_0
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 150
    iget-object v0, p0, Lws;->j:Lwe;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lws;->j:Lwe;

    iput-boolean v2, v0, Lwe;->a:Z

    invoke-virtual {v0}, Lwe;->interrupt()V

    .line 153
    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lws;->i:[Lwk;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 154
    iget-object v1, p0, Lws;->i:[Lwk;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 155
    iget-object v1, p0, Lws;->i:[Lwk;

    aget-object v1, v1, v0

    iput-boolean v2, v1, Lwk;->a:Z

    invoke-virtual {v1}, Lwk;->interrupt()V

    .line 153
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 158
    :cond_2
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lws;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    return v0
.end method

.method public d()Lwc;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lws;->f:Lwc;

    return-object v0
.end method
