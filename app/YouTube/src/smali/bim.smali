.class public final Lbim;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lggz;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/telephony/TelephonyManager;

.field private final c:Landroid/content/pm/PackageManager;

.field private final d:Lbin;

.field private final e:Lfhw;

.field private final f:Landroid/content/SharedPreferences;

.field private final g:Lghm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Landroid/content/pm/PackageManager;Lbin;Lfhw;Landroid/content/SharedPreferences;Lghm;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbim;->a:Landroid/content/Context;

    .line 75
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lbim;->b:Landroid/telephony/TelephonyManager;

    .line 76
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    iput-object v0, p0, Lbim;->c:Landroid/content/pm/PackageManager;

    .line 77
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbin;

    iput-object v0, p0, Lbim;->d:Lbin;

    .line 78
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhw;

    iput-object v0, p0, Lbim;->e:Lfhw;

    .line 79
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lbim;->f:Landroid/content/SharedPreferences;

    .line 80
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghm;

    iput-object v0, p0, Lbim;->g:Lghm;

    .line 81
    return-void
.end method


# virtual methods
.method public final a(Lhjx;Lgkp;)V
    .locals 5

    .prologue
    .line 90
    iget-object v0, p1, Lhjx;->a:Lhcv;

    if-eqz v0, :cond_5

    .line 91
    iget-object v0, p1, Lhjx;->a:Lhcv;

    .line 96
    :goto_0
    iget-object v1, p0, Lbim;->f:Landroid/content/SharedPreferences;

    const-string v2, "country"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    iget-object v2, p0, Lbim;->f:Landroid/content/SharedPreferences;

    const-string v3, "internal_geo"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 99
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lhcv;->k:Ljava/lang/String;

    .line 100
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 101
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    :cond_0
    iput-object v1, v0, Lhcv;->l:Ljava/lang/String;

    .line 103
    iget-object v1, p0, Lbim;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0901ed

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "\u200e\u200f\u200e\u200e"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    const/4 v1, 0x1

    iput-boolean v1, v0, Lhcv;->d:Z

    .line 107
    :cond_1
    const/4 v1, 0x3

    iput v1, v0, Lhcv;->g:I

    .line 108
    iget-object v1, p0, Lbim;->a:Landroid/content/Context;

    iget-object v3, p0, Lbim;->c:Landroid/content/pm/PackageManager;

    .line 109
    invoke-static {v1, v3}, La;->a(Landroid/content/Context;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhcv;->h:Ljava/lang/String;

    .line 110
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v1, v0, Lhcv;->j:Ljava/lang/String;

    .line 111
    const-string v1, "Android"

    iput-object v1, v0, Lhcv;->i:Ljava/lang/String;

    .line 112
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v1, v0, Lhcv;->e:Ljava/lang/String;

    .line 113
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, v0, Lhcv;->f:Ljava/lang/String;

    .line 114
    iget-object v1, p0, Lbim;->b:Landroid/telephony/TelephonyManager;

    invoke-static {v1}, Lfaq;->a(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhcv;->c:Ljava/lang/String;

    .line 116
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 117
    iput-object v2, v0, Lhcv;->p:Ljava/lang/String;

    .line 120
    :cond_2
    iget-object v1, p0, Lbim;->e:Lfhw;

    invoke-virtual {v1}, Lfhw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 121
    iget-object v1, p0, Lbim;->e:Lfhw;

    invoke-virtual {v1}, Lfhw;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lhcv;->n:Ljava/lang/String;

    .line 124
    :cond_3
    iget-object v1, p0, Lbim;->a:Landroid/content/Context;

    invoke-static {v1}, La;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 125
    iget-object v1, v0, Lhcv;->b:[I

    iget-object v2, p0, Lbim;->f:Landroid/content/SharedPreferences;

    .line 126
    invoke-static {v2}, Lfvn;->a(Landroid/content/SharedPreferences;)[I

    move-result-object v2

    .line 125
    invoke-static {v1, v2}, La;->a([I[I)[I

    move-result-object v1

    iput-object v1, v0, Lhcv;->b:[I

    .line 131
    iget-object v1, p0, Lbim;->g:Lghm;

    invoke-virtual {v1}, Lghm;->g()[B

    move-result-object v1

    if-nez v1, :cond_4

    .line 132
    const-string v1, "123"

    iput-object v1, v0, Lhcv;->a:Ljava/lang/String;

    .line 139
    :cond_4
    iget-object v1, p0, Lbim;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, v0, Lhcv;->o:I

    .line 141
    iget-object v1, p0, Lbim;->d:Lbin;

    iget-object v2, p0, Lbim;->a:Landroid/content/Context;

    invoke-interface {v1, v2}, Lbin;->a(Landroid/content/Context;)Lhbx;

    move-result-object v1

    iput-object v1, p1, Lhjx;->c:Lhbx;

    .line 142
    iput-object v0, p1, Lhjx;->a:Lhcv;

    .line 143
    return-void

    .line 93
    :cond_5
    new-instance v0, Lhcv;

    invoke-direct {v0}, Lhcv;-><init>()V

    goto/16 :goto_0
.end method
