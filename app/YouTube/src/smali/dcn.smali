.class public final Ldcn;
.super Lded;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lddy;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/CompoundButton;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/widget/TextView;

.field private l:Lddz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lded;-><init>(Landroid/content/Context;)V

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;F)V
    .locals 6

    .prologue
    .line 108
    iget-object v0, p0, Ldcn;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 109
    iget-object v0, p0, Ldcn;->i:Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, " %.2f KiB/s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/high16 v5, 0x46000000    # 8192.0f

    div-float v5, p2, v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;J)V
    .locals 8

    .prologue
    .line 114
    iget-object v0, p0, Ldcn;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 115
    iget-object v0, p0, Ldcn;->k:Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, " %.2f s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    long-to-float v5, p2

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    return-void
.end method

.method public final a(Lddz;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Ldcn;->l:Lddz;

    .line 71
    return-void
.end method

.method public final a(Lfqj;)V
    .locals 6

    .prologue
    .line 80
    if-eqz p1, :cond_0

    .line 81
    iget-object v0, p0, Ldcn;->f:Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%d %s %dx%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 84
    iget-object v5, p1, Lfqj;->a:Lhgy;

    iget v5, v5, Lhgy;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 85
    iget-object v5, p1, Lfqj;->a:Lhgy;

    iget-object v5, v5, Lhgy;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 86
    iget-object v5, p1, Lfqj;->a:Lhgy;

    iget v5, v5, Lhgy;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 87
    iget-object v5, p1, Lfqj;->a:Lhgy;

    iget v5, v5, Lhgy;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 81
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Ldcn;->f:Landroid/widget/TextView;

    const-string v1, "N/A"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ldcn;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    return-void
.end method

.method public final b(Lfqj;)V
    .locals 6

    .prologue
    .line 95
    if-eqz p1, :cond_0

    .line 96
    iget-object v0, p0, Ldcn;->g:Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%d %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 99
    iget-object v5, p1, Lfqj;->a:Lhgy;

    iget v5, v5, Lhgy;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 100
    iget-object v5, p1, Lfqj;->a:Lhgy;

    iget-object v5, v5, Lhgy;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 96
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Ldcn;->g:Landroid/widget/TextView;

    const-string v1, "N/A"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Ldcn;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 155
    invoke-virtual {p0}, Ldcn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04005a

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0801a2

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldcn;->a:Landroid/view/View;

    const v0, 0x7f0801a4

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldcn;->b:Landroid/view/View;

    iget-object v0, p0, Ldcn;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0801a3

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldcn;->c:Landroid/view/View;

    iget-object v0, p0, Ldcn;->c:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0801a5

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Ldcn;->d:Landroid/widget/CompoundButton;

    iget-object v0, p0, Ldcn;->d:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0801a7

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcn;->e:Landroid/widget/TextView;

    const v0, 0x7f0801a9

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcn;->f:Landroid/widget/TextView;

    const v0, 0x7f0801ab

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcn;->g:Landroid/widget/TextView;

    const v0, 0x7f0801ad

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldcn;->h:Landroid/widget/ImageView;

    const v0, 0x7f0801ae

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcn;->i:Landroid/widget/TextView;

    const v0, 0x7f0801b0

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldcn;->j:Landroid/widget/ImageView;

    const v0, 0x7f0801b1

    invoke-virtual {p0, v0}, Ldcn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcn;->k:Landroid/widget/TextView;

    .line 157
    :cond_0
    iget-object v0, p0, Ldcn;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 158
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Ldcn;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Ldcn;->d:Landroid/widget/CompoundButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 164
    return-void
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Ldcn;->d:Landroid/widget/CompoundButton;

    if-ne p1, v0, :cond_0

    .line 141
    iget-object v0, p0, Ldcn;->l:Lddz;

    invoke-interface {v0, p2}, Lddz;->a(Z)V

    .line 143
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Ldcn;->b:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 130
    iget-object v0, p0, Ldcn;->l:Lddz;

    invoke-interface {v0}, Lddz;->e()V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Ldcn;->c:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 132
    iget-object v0, p0, Ldcn;->l:Lddz;

    invoke-interface {v0}, Lddz;->f()V

    goto :goto_0
.end method

.method public final r_()Ldeg;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 149
    new-instance v0, Ldeg;

    invoke-direct {v0, v1, v1}, Ldeg;-><init>(II)V

    return-object v0
.end method
