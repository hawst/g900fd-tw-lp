.class public abstract Laqq;
.super Lkp;
.source "SourceFile"


# instance fields
.field public e:Landroid/view/View;

.field public f:Landroid/view/View;

.field public g:Landroid/widget/TextView;

.field public h:Landroid/widget/TextView;

.field private i:Ljava/util/List;

.field private j:Landroid/view/View;

.field private k:Landroid/support/v4/view/ViewPager;

.field private l:I

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lkp;-><init>()V

    .line 146
    return-void
.end method

.method static synthetic a(Laqq;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Laqq;->k:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic b(Laqq;)Ljava/util/List;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Laqq;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Laqq;)Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Laqq;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Laqq;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Laqq;->l:I

    return v0
.end method

.method static synthetic e(Laqq;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Laqq;->m:I

    return v0
.end method

.method static synthetic f(Laqq;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Laqq;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Laqq;)Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Laqq;->j:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(Laqq;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Laqq;->h:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public abstract f()Ljava/util/List;
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 102
    invoke-virtual {p0}, Laqq;->finish()V

    .line 103
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lkp;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Laqq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Laqq;->setRequestedOrientation(I)V

    .line 47
    :cond_0
    const v0, 0x7f04012b

    invoke-virtual {p0, v0}, Laqq;->setContentView(I)V

    .line 48
    const v0, 0x7f08032d

    invoke-virtual {p0, v0}, Laqq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqq;->e:Landroid/view/View;

    .line 49
    const v0, 0x7f08032f

    invoke-virtual {p0, v0}, Laqq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laqq;->g:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f080303

    invoke-virtual {p0, v0}, Laqq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laqq;->h:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f08032e

    invoke-virtual {p0, v0}, Laqq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqq;->j:Landroid/view/View;

    .line 53
    const v0, 0x7f0801f9

    invoke-virtual {p0, v0}, Laqq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Laqq;->k:Landroid/support/v4/view/ViewPager;

    .line 54
    invoke-virtual {p0}, Laqq;->f()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laqq;->i:Ljava/util/List;

    .line 55
    iget-object v0, p0, Laqq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Laqq;->m:I

    .line 56
    iget-object v0, p0, Laqq;->k:Landroid/support/v4/view/ViewPager;

    new-instance v1, Laqt;

    invoke-direct {v1, p0}, Laqt;-><init>(Laqq;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lfm;)V

    .line 57
    iget-object v0, p0, Laqq;->k:Landroid/support/v4/view/ViewPager;

    new-instance v1, Laqu;

    invoke-direct {v1, p0}, Laqu;-><init>(Laqq;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lhe;)V

    .line 58
    const v0, 0x7f080331

    invoke-virtual {p0, v0}, Laqq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqq;->f:Landroid/view/View;

    invoke-static {p0}, La;->h(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Laqq;->l:I

    iget-object v0, p0, Laqq;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Laqq;->l:I

    iget v2, p0, Laqq;->m:I

    div-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object v1, p0, Laqq;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 60
    iget-object v0, p0, Laqq;->h:Landroid/widget/TextView;

    new-instance v1, Laqr;

    invoke-direct {v1, p0}, Laqr;-><init>(Laqq;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v0, p0, Laqq;->g:Landroid/widget/TextView;

    new-instance v1, Laqs;

    invoke-direct {v1, p0}, Laqs;-><init>(Laqq;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    return-void
.end method
