.class public final Ladi;
.super Ljavax/crypto/Cipher;
.source "SourceFile"


# static fields
.field private static final a:Ljava/security/Provider;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 22
    new-instance v0, Ladj;

    const-string v1, "FroyoRC4Provider"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-string v4, "Based on SpongyCastle"

    invoke-direct {v0, v1, v2, v3, v4}, Ladj;-><init>(Ljava/lang/String;DLjava/lang/String;)V

    sput-object v0, Ladi;->a:Ljava/security/Provider;

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .prologue
    .line 26
    new-instance v0, Ladk;

    invoke-direct {v0}, Ladk;-><init>()V

    sget-object v1, Ladi;->a:Ljava/security/Provider;

    const-string v2, "RC4"

    invoke-direct {p0, v0, v1, v2}, Ljavax/crypto/Cipher;-><init>(Ljavax/crypto/CipherSpi;Ljava/security/Provider;Ljava/lang/String;)V

    .line 27
    return-void
.end method
