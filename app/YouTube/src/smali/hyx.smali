.class public final Lhyx;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lhyz;

.field public b:I

.field public c:Lhgz;

.field public d:Lhgz;

.field private e:[Lhyy;

.field private f:[B

.field private g:Lhog;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    sget-object v0, Lhyz;->a:[Lhyz;

    iput-object v0, p0, Lhyx;->a:[Lhyz;

    const/4 v0, 0x0

    iput v0, p0, Lhyx;->b:I

    iput-object v1, p0, Lhyx;->c:Lhgz;

    iput-object v1, p0, Lhyx;->d:Lhgz;

    sget-object v0, Lhyy;->a:[Lhyy;

    iput-object v0, p0, Lhyx;->e:[Lhyy;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhyx;->f:[B

    iput-object v1, p0, Lhyx;->g:Lhog;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lhyx;->a:[Lhyz;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lhyx;->a:[Lhyz;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    if-eqz v5, :cond_0

    const/4 v6, 0x1

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :cond_2
    iget v2, p0, Lhyx;->b:I

    if-eqz v2, :cond_3

    const/4 v2, 0x2

    iget v3, p0, Lhyx;->b:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhyx;->c:Lhgz;

    if-eqz v2, :cond_4

    const/4 v2, 0x3

    iget-object v3, p0, Lhyx;->c:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhyx;->d:Lhgz;

    if-eqz v2, :cond_5

    const/4 v2, 0x4

    iget-object v3, p0, Lhyx;->d:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lhyx;->e:[Lhyy;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lhyx;->e:[Lhyy;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    if-eqz v4, :cond_6

    const/4 v5, 0x5

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lhyx;->f:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x7

    iget-object v2, p0, Lhyx;->f:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lhyx;->g:Lhog;

    if-eqz v1, :cond_9

    const/16 v1, 0x8

    iget-object v2, p0, Lhyx;->g:Lhog;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lhyx;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhyx;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhyx;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhyx;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhyx;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhyx;->a:[Lhyz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhyz;

    iget-object v3, p0, Lhyx;->a:[Lhyz;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhyx;->a:[Lhyz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhyx;->a:[Lhyz;

    :goto_2
    iget-object v2, p0, Lhyx;->a:[Lhyz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhyx;->a:[Lhyz;

    new-instance v3, Lhyz;

    invoke-direct {v3}, Lhyz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhyx;->a:[Lhyz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhyx;->a:[Lhyz;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhyx;->a:[Lhyz;

    new-instance v3, Lhyz;

    invoke-direct {v3}, Lhyz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhyx;->a:[Lhyz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhyx;->b:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhyx;->c:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhyx;->c:Lhgz;

    :cond_5
    iget-object v0, p0, Lhyx;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhyx;->d:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhyx;->d:Lhgz;

    :cond_6
    iget-object v0, p0, Lhyx;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhyx;->e:[Lhyy;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhyy;

    iget-object v3, p0, Lhyx;->e:[Lhyy;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lhyx;->e:[Lhyy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lhyx;->e:[Lhyy;

    :goto_4
    iget-object v2, p0, Lhyx;->e:[Lhyy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lhyx;->e:[Lhyy;

    new-instance v3, Lhyy;

    invoke-direct {v3}, Lhyy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhyx;->e:[Lhyy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lhyx;->e:[Lhyy;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lhyx;->e:[Lhyy;

    new-instance v3, Lhyy;

    invoke-direct {v3}, Lhyy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhyx;->e:[Lhyy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhyx;->f:[B

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lhyx;->g:Lhog;

    if-nez v0, :cond_a

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhyx;->g:Lhog;

    :cond_a
    iget-object v0, p0, Lhyx;->g:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhyx;->a:[Lhyz;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lhyx;->a:[Lhyz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget v1, p0, Lhyx;->b:I

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    iget v2, p0, Lhyx;->b:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    :cond_2
    iget-object v1, p0, Lhyx;->c:Lhgz;

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lhyx;->c:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v1, p0, Lhyx;->d:Lhgz;

    if-eqz v1, :cond_4

    const/4 v1, 0x4

    iget-object v2, p0, Lhyx;->d:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v1, p0, Lhyx;->e:[Lhyy;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lhyx;->e:[Lhyy;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    if-eqz v3, :cond_5

    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lhyx;->f:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x7

    iget-object v1, p0, Lhyx;->f:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_7
    iget-object v0, p0, Lhyx;->g:Lhog;

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    iget-object v1, p0, Lhyx;->g:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_8
    iget-object v0, p0, Lhyx;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhyx;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhyx;

    iget-object v2, p0, Lhyx;->a:[Lhyz;

    iget-object v3, p1, Lhyx;->a:[Lhyz;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lhyx;->b:I

    iget v3, p1, Lhyx;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhyx;->c:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhyx;->c:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhyx;->d:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhyx;->d:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhyx;->e:[Lhyy;

    iget-object v3, p1, Lhyx;->e:[Lhyy;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhyx;->f:[B

    iget-object v3, p1, Lhyx;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhyx;->g:Lhog;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhyx;->g:Lhog;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhyx;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhyx;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhyx;->c:Lhgz;

    iget-object v3, p1, Lhyx;->c:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhyx;->d:Lhgz;

    iget-object v3, p1, Lhyx;->d:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhyx;->g:Lhog;

    iget-object v3, p1, Lhyx;->g:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhyx;->I:Ljava/util/List;

    iget-object v3, p1, Lhyx;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    iget-object v2, p0, Lhyx;->a:[Lhyz;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget v2, p0, Lhyx;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhyx;->c:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhyx;->d:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Lhyx;->e:[Lhyy;

    if-nez v2, :cond_7

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    iget-object v0, p0, Lhyx;->f:[B

    if-nez v0, :cond_9

    mul-int/lit8 v2, v2, 0x1f

    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhyx;->g:Lhog;

    if-nez v0, :cond_a

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhyx;->I:Ljava/util/List;

    if-nez v2, :cond_b

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_3
    move v2, v0

    move v0, v1

    :goto_4
    iget-object v3, p0, Lhyx;->a:[Lhyz;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhyx;->a:[Lhyz;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    iget-object v2, p0, Lhyx;->a:[Lhyz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhyz;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    iget-object v0, p0, Lhyx;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lhyx;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_7
    move v2, v0

    move v0, v1

    :goto_6
    iget-object v3, p0, Lhyx;->e:[Lhyy;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhyx;->e:[Lhyy;

    aget-object v2, v2, v0

    if-nez v2, :cond_8

    move v2, v1

    :goto_7
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v2, p0, Lhyx;->e:[Lhyy;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhyy;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_9
    move v0, v1

    :goto_8
    iget-object v3, p0, Lhyx;->f:[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhyx;->f:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_a
    iget-object v0, p0, Lhyx;->g:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_b
    iget-object v1, p0, Lhyx;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
