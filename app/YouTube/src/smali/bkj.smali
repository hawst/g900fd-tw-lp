.class public final Lbkj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgmv;


# instance fields
.field private final a:Lgit;

.field private final b:Lgnd;

.field private final c:Lgmw;

.field private final d:Lfep;

.field private final e:Lezj;

.field private final f:Lcyc;

.field private final g:Lckv;

.field private h:Lbkk;

.field private final i:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lgit;Lgnd;Lfep;Lezj;Lcyc;Lckv;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 69
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgit;

    iput-object v0, p0, Lbkj;->a:Lgit;

    .line 70
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnd;

    iput-object v0, p0, Lbkj;->b:Lgnd;

    .line 71
    invoke-interface {p2}, Lgnd;->k()Lgmw;

    move-result-object v0

    iput-object v0, p0, Lbkj;->c:Lgmw;

    .line 72
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfep;

    iput-object v0, p0, Lbkj;->d:Lfep;

    .line 73
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lbkj;->e:Lezj;

    .line 74
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyc;

    iput-object v0, p0, Lbkj;->f:Lcyc;

    .line 75
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckv;

    iput-object v0, p0, Lbkj;->g:Lckv;

    .line 77
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbkj;->i:Ljava/util/Set;

    .line 78
    return-void
.end method

.method private a(J)Ljava/util/Collection;
    .locals 15

    .prologue
    .line 206
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 210
    iget-object v0, p0, Lbkj;->b:Lgnd;

    .line 211
    invoke-interface {v0}, Lgnd;->i()Ljava/util/Map;

    move-result-object v3

    .line 214
    iget-object v0, p0, Lbkj;->b:Lgnd;

    invoke-interface {v0}, Lgnd;->g()Ljava/util/Collection;

    move-result-object v0

    .line 215
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmb;

    .line 217
    iget-object v1, v0, Lgmb;->d:Lglz;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lgmb;->d:Lglz;

    iget-object v1, v1, Lglz;->b:Lflg;

    iget-object v1, v1, Lflg;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lgmb;->f:Lglv;

    sget-object v5, Lglv;->a:Lglv;

    if-eq v1, v5, :cond_2

    iget-object v1, v0, Lgmb;->f:Lglv;

    sget-object v5, Lglv;->h:Lglv;

    if-eq v1, v5, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 218
    iget-object v1, v0, Lgmb;->d:Lglz;

    .line 222
    iget-object v5, v0, Lgmb;->a:Lgcd;

    iget-object v5, v5, Lgcd;->b:Ljava/lang/String;

    .line 226
    iget-wide v6, v1, Lglz;->c:J

    .line 227
    iget-wide v8, v0, Lgmb;->c:J

    .line 228
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v6, p1, v6

    .line 229
    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    .line 228
    invoke-static {v6, v7}, Lbkj;->b(J)I

    move-result v0

    .line 230
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v8, p1, v8

    .line 231
    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    .line 230
    invoke-static {v6, v7}, Lbkj;->b(J)I

    move-result v6

    .line 233
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "Refreshing video %s: Time since last refreshed: %d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    const/4 v10, 0x1

    .line 234
    iget-wide v12, v1, Lglz;->d:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    .line 233
    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lezp;->e(Ljava/lang/String;)V

    .line 237
    new-instance v7, Lhpr;

    invoke-direct {v7}, Lhpr;-><init>()V

    .line 238
    iput-object v5, v7, Lhpr;->b:Ljava/lang/String;

    .line 239
    iput v0, v7, Lhpr;->c:I

    .line 240
    iput v6, v7, Lhpr;->d:I

    .line 245
    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v5, v7, Lhpr;->e:[Lhpt;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhpt;

    iput-object v0, v7, Lhpr;->e:[Lhpt;

    .line 252
    :cond_1
    iget-object v0, v1, Lglz;->b:Lflg;

    iget-object v1, v0, Lflg;->c:Ljava/lang/String;

    .line 254
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 255
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    move-object v1, v0

    .line 263
    :goto_2
    iget-object v0, v1, Lhpu;->c:[Lhpr;

    const/4 v5, 0x1

    new-array v5, v5, [Lhpr;

    const/4 v6, 0x0

    aput-object v7, v5, v6

    invoke-static {v0, v5}, La;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhpr;

    iput-object v0, v1, Lhpu;->c:[Lhpr;

    goto/16 :goto_0

    .line 217
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 257
    :cond_3
    new-instance v0, Lhpu;

    invoke-direct {v0}, Lhpu;-><init>()V

    .line 258
    iput-object v1, v0, Lhpu;->b:Ljava/lang/String;

    .line 259
    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    goto :goto_2

    .line 266
    :cond_4
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private a(JLhpl;Lglz;)V
    .locals 3

    .prologue
    .line 470
    iget-object v0, p4, Lglz;->a:Ljava/lang/String;

    .line 472
    iget v1, p3, Lhpl;->d:I

    packed-switch v1, :pswitch_data_0

    .line 511
    :goto_0
    iget v1, p3, Lhpl;->d:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    .line 514
    iget-object v1, p0, Lbkj;->i:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 516
    :cond_0
    return-void

    .line 476
    :pswitch_0
    invoke-direct {p0, p1, p2, p3, p4}, Lbkj;->b(JLhpl;Lglz;)V

    goto :goto_0

    .line 479
    :pswitch_1
    iget-object v1, p0, Lbkj;->b:Lgnd;

    invoke-interface {v1, v0}, Lgnd;->j(Ljava/lang/String;)V

    goto :goto_0

    .line 482
    :pswitch_2
    iget-object v1, p0, Lbkj;->b:Lgnd;

    invoke-interface {v1, v0}, Lgnd;->f(Ljava/lang/String;)V

    .line 488
    :pswitch_3
    :try_start_0
    new-instance v1, Lhpl;

    invoke-direct {v1}, Lhpl;-><init>()V

    .line 489
    invoke-static {p3}, Lidh;->a(Lidh;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lidh;->a(Lidh;[B)Lidh;

    .line 490
    const/4 v2, 0x1

    iput v2, v1, Lhpl;->d:I

    .line 491
    invoke-direct {p0, p1, p2, v1, p4}, Lbkj;->b(JLhpl;Lglz;)V

    .line 492
    iget-object v1, p0, Lbkj;->i:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 494
    :catch_0
    move-exception v1

    const-string v1, "Error parsing the original OfflineState"

    invoke-static {v1}, Lezp;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 501
    :pswitch_4
    :try_start_1
    new-instance v1, Lhpl;

    invoke-direct {v1}, Lhpl;-><init>()V

    .line 502
    invoke-static {p3}, Lidh;->a(Lidh;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lidh;->a(Lidh;[B)Lidh;

    .line 503
    const/4 v2, 0x1

    iput v2, v1, Lhpl;->d:I

    .line 504
    invoke-direct {p0, p1, p2, v1, p4}, Lbkj;->b(JLhpl;Lglz;)V

    .line 505
    iget-object v1, p0, Lbkj;->b:Lgnd;

    invoke-interface {v1, v0}, Lgnd;->p(Ljava/lang/String;)V
    :try_end_1
    .catch Lidg; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 507
    :catch_1
    move-exception v1

    const-string v1, "Error parsing the original OfflineState"

    invoke-static {v1}, Lezp;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lhpe;J)V
    .locals 4

    .prologue
    .line 321
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    iget v0, p1, Lhpe;->b:I

    .line 325
    iget-object v1, p0, Lbkj;->f:Lcyc;

    invoke-interface {v1}, Lcyc;->L()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 327
    new-instance v1, Lbkk;

    invoke-direct {v1, p0, p1, p2, p3}, Lbkk;-><init>(Lbkj;Lhpe;J)V

    iput-object v1, p0, Lbkj;->h:Lbkk;

    .line 328
    iget-object v1, p0, Lbkj;->c:Lgmw;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, Lgmw;->b(J)V

    .line 340
    :goto_0
    return-void

    .line 330
    :cond_0
    if-lez v0, :cond_1

    .line 335
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 339
    :cond_1
    iget-object v0, p1, Lhpe;->a:Ljava/lang/String;

    invoke-direct {p0, v0, p2, p3}, Lbkj;->a(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 350
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 352
    iget-object v0, p0, Lbkj;->d:Lfep;

    iget-object v1, p0, Lbkj;->a:Lgit;

    invoke-virtual {v0, v1}, Lfep;->a(Lgit;)Lfer;

    move-result-object v1

    .line 353
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lfer;->b:Ljava/lang/String;

    .line 355
    iget-object v0, p0, Lbkj;->d:Lfep;

    invoke-virtual {v0, v1}, Lfep;->a(Lfer;)Lhph;

    move-result-object v0

    .line 356
    const-string v1, "Offline refresh continuation response returned!"

    invoke-static {v1}, Lezp;->e(Ljava/lang/String;)V

    .line 357
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Offlined video set update count: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, Lhph;->a:[Lhpv;

    array-length v5, v5

    .line 358
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 357
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->e(Ljava/lang/String;)V

    .line 361
    iget-object v1, v0, Lhph;->a:[Lhpv;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 362
    iget-object v1, v0, Lhph;->a:[Lhpv;

    iget v2, v0, Lhph;->c:I

    invoke-direct {p0, v1, v2, p2, p3}, Lbkj;->a([Lhpv;IJ)V

    .line 370
    :cond_0
    :try_start_0
    iget-object v1, v0, Lhph;->b:Lhpi;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lhph;->b:Lhpi;

    iget-object v1, v1, Lhpi;->a:Lhpe;

    if-eqz v1, :cond_1

    .line 372
    iget-object v0, v0, Lhph;->b:Lhpi;

    iget-object v0, v0, Lhpi;->a:Lhpe;

    invoke-direct {p0, v0, p2, p3}, Lbkj;->a(Lhpe;J)V

    .line 382
    :goto_0
    return-void

    .line 377
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lbkj;->h:Lbkk;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 379
    :catch_0
    move-exception v0

    .line 380
    const-string v1, "OfflineSyncController: Thread.sleep interrupted: "

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;Lhpl;)V
    .locals 3

    .prologue
    .line 437
    iget v0, p2, Lhpl;->d:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 438
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 439
    iget v1, p2, Lhpl;->d:I

    packed-switch v1, :pswitch_data_0

    .line 456
    const-string v1, "UNEXPECTED_ACTION_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lhpl;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 459
    :goto_0
    const-string v1, " video "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 460
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 462
    :cond_0
    return-void

    .line 441
    :pswitch_0
    const-string v1, "DISABLE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 444
    :pswitch_1
    const-string v1, "DELETE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 447
    :pswitch_2
    const-string v1, "REFRESH"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 450
    :pswitch_3
    const-string v1, "REFRESH_AD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 453
    :pswitch_4
    const-string v1, "DELETE_AD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 439
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a([Lhpv;IJ)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 392
    invoke-static {}, Lb;->b()V

    .line 395
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 396
    array-length v4, p1

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, p1, v2

    .line 397
    iget-object v5, v0, Lhpv;->b:Lhpl;

    .line 399
    iget-object v6, v0, Lhpv;->c:[Lhpw;

    array-length v6, v6

    if-nez v6, :cond_0

    .line 402
    iget-object v0, p0, Lbkj;->b:Lgnd;

    iget-object v6, v5, Lhpl;->a:Ljava/lang/String;

    .line 403
    invoke-interface {v0, v6}, Lgnd;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 404
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglz;

    .line 405
    iget-object v7, v0, Lglz;->a:Ljava/lang/String;

    invoke-static {v3, v7, v5}, Lbkj;->a(Ljava/util/List;Ljava/lang/String;Lhpl;)V

    .line 406
    invoke-direct {p0, p3, p4, v5, v0}, Lbkj;->a(JLhpl;Lglz;)V

    goto :goto_1

    .line 410
    :cond_0
    iget-object v6, v0, Lhpv;->c:[Lhpw;

    array-length v7, v6

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_1

    aget-object v8, v6, v0

    .line 411
    iget-object v8, v8, Lhpw;->b:Ljava/lang/String;

    .line 412
    invoke-static {v3, v8, v5}, Lbkj;->a(Ljava/util/List;Ljava/lang/String;Lhpl;)V

    .line 413
    iget-object v9, p0, Lbkj;->b:Lgnd;

    .line 414
    invoke-interface {v9, v8}, Lgnd;->d(Ljava/lang/String;)Lglz;

    move-result-object v8

    .line 413
    invoke-direct {p0, p3, p4, v5, v8}, Lbkj;->a(JLhpl;Lglz;)V

    .line 410
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 396
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 418
    :cond_2
    iget-object v1, p0, Lbkj;->g:Lckv;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lckv;->a([Ljava/lang/String;)V

    .line 422
    :cond_3
    if-lez p2, :cond_4

    .line 423
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x37

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Setting offline refresh interval to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 424
    iget-object v0, p0, Lbkj;->c:Lgmw;

    int-to-long v2, p2

    invoke-interface {v0, v2, v3}, Lgmw;->a(J)V

    .line 430
    :goto_3
    iget-object v0, p0, Lbkj;->b:Lgnd;

    iget-object v1, p0, Lbkj;->i:Ljava/util/Set;

    invoke-interface {v0, v1}, Lgnd;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lbkj;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 431
    return-void

    .line 426
    :cond_4
    const-string v0, "0 specified as offline refresh interval; ignoring."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private static b(J)I
    .locals 2

    .prologue
    .line 279
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 282
    const v0, 0x7fffffff

    .line 284
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p0

    goto :goto_0
.end method

.method private b(JLhpl;Lglz;)V
    .locals 3

    .prologue
    .line 523
    iget-object v0, p0, Lbkj;->b:Lgnd;

    .line 524
    invoke-virtual {p4}, Lglz;->d()Lgma;

    move-result-object v1

    new-instance v2, Lflg;

    invoke-direct {v2, p3}, Lflg;-><init>(Lhpl;)V

    .line 525
    iput-object v2, v1, Lgma;->b:Lflg;

    .line 526
    iput-wide p1, v1, Lgma;->d:J

    .line 527
    invoke-virtual {v1}, Lgma;->a()Lglz;

    move-result-object v1

    .line 523
    invoke-interface {v0, v1}, Lgnd;->a(Lglz;)Z

    .line 528
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 90
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lb;->b()V

    .line 91
    iget-object v2, p0, Lbkj;->i:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 93
    iget-object v3, p0, Lbkj;->g:Lckv;

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Run refresh for identity "

    iget-object v2, p0, Lbkj;->a:Lgit;

    iget-object v2, v2, Lgit;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lckv;->a([Ljava/lang/String;)V

    .line 96
    iget-object v2, p0, Lbkj;->e:Lezj;

    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    .line 99
    invoke-direct {p0, v2, v3}, Lbkj;->a(J)Ljava/util/Collection;

    move-result-object v4

    .line 102
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_2

    .line 127
    :cond_0
    :goto_1
    monitor-exit p0

    return v0

    .line 93
    :cond_1
    :try_start_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 107
    :cond_2
    :try_start_2
    invoke-static {}, Lb;->b()V

    iget-object v5, p0, Lbkj;->d:Lfep;

    iget-object v6, p0, Lbkj;->a:Lgit;

    invoke-virtual {v5, v6}, Lfep;->a(Lgit;)Lfer;

    move-result-object v5

    invoke-static {v4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v4, v5, Lfer;->a:Ljava/util/Collection;

    iget-object v4, p0, Lbkj;->d:Lfep;

    invoke-virtual {v4, v5}, Lfep;->a(Lfer;)Lhph;

    move-result-object v4

    const-string v5, "Offline refresh response returned!"

    invoke-static {v5}, Lezp;->e(Ljava/lang/String;)V

    iget-object v5, v4, Lhph;->a:[Lhpv;

    array-length v5, v5

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x2c

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Offlined video set update count: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lezp;->e(Ljava/lang/String;)V

    iget-object v5, v4, Lhph;->b:Lhpi;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x18

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Contains continuation?: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lezp;->e(Ljava/lang/String;)V

    .line 110
    iget-object v5, v4, Lhph;->a:[Lhpv;

    iget v6, v4, Lhph;->c:I

    invoke-direct {p0, v5, v6, v2, v3}, Lbkj;->a([Lhpv;IJ)V

    .line 116
    iget-object v5, v4, Lhph;->b:Lhpi;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lhph;->b:Lhpi;

    iget-object v5, v5, Lhpi;->a:Lhpe;

    if-eqz v5, :cond_0

    .line 118
    iget-object v4, v4, Lhph;->b:Lhpi;

    iget-object v4, v4, Lhpi;->a:Lhpe;

    invoke-direct {p0, v4, v2, v3}, Lbkj;->a(Lhpe;J)V
    :try_end_2
    .catch Lfdv; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 122
    :catch_0
    move-exception v0

    .line 123
    :try_start_3
    const-string v2, "Failed to reach offline refresh service: "

    invoke-static {v2, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 124
    goto/16 :goto_1

    .line 125
    :catch_1
    move-exception v0

    .line 126
    const-string v2, "OfflineSyncController: Thread.sleep interrupted: "

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 127
    goto/16 :goto_1
.end method

.method public final declared-synchronized b()I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 145
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lb;->b()V

    .line 146
    iget-object v2, p0, Lbkj;->i:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 148
    iget-object v3, p0, Lbkj;->g:Lckv;

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Run refresh continuation for identity "

    iget-object v2, p0, Lbkj;->a:Lgit;

    iget-object v2, v2, Lgit;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lckv;->a([Ljava/lang/String;)V

    .line 151
    iget-object v2, p0, Lbkj;->h:Lbkk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    .line 178
    :cond_0
    :goto_1
    monitor-exit p0

    return v0

    .line 148
    :cond_1
    :try_start_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 155
    :cond_2
    :try_start_2
    iget-object v2, p0, Lbkj;->e:Lezj;

    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    .line 158
    iget-object v4, p0, Lbkj;->h:Lbkk;

    iget-wide v4, v4, Lbkk;->b:J

    cmp-long v4, v2, v4

    if-lez v4, :cond_3

    .line 159
    const/4 v1, 0x0

    iput-object v1, p0, Lbkj;->h:Lbkk;

    .line 163
    iget-object v1, p0, Lbkj;->f:Lcyc;

    invoke-interface {v1}, Lcyc;->M()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lbkj;->c:Lgmw;

    const-wide/16 v2, 0x0

    invoke-interface {v1, v2, v3}, Lgmw;->a(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 172
    :cond_3
    :try_start_3
    iget-object v4, p0, Lbkj;->h:Lbkk;

    iget-object v4, v4, Lbkk;->a:Ljava/lang/String;

    invoke-direct {p0, v4, v2, v3}, Lbkj;->a(Ljava/lang/String;J)V
    :try_end_3
    .catch Lfdv; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 175
    :catch_0
    move-exception v0

    .line 176
    :try_start_4
    const-string v2, "Failed to reach offline refresh service: "

    invoke-static {v2, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    const/4 v0, 0x0

    iput-object v0, p0, Lbkj;->h:Lbkk;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v1

    .line 178
    goto :goto_1
.end method
