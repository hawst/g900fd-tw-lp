.class final Lbys;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field private final a:Lgbu;

.field private synthetic b:Lbym;


# direct methods
.method public constructor <init>(Lbym;Lgbu;)V
    .locals 0

    .prologue
    .line 667
    iput-object p1, p0, Lbys;->b:Lbym;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668
    iput-object p2, p0, Lbys;->a:Lgbu;

    .line 669
    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 3

    .prologue
    .line 686
    iget-object v0, p0, Lbys;->b:Lbym;

    iget-object v0, v0, Lbym;->m:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    .line 687
    if-eqz v0, :cond_0

    iget-object v1, v0, Lgbu;->a:Ljava/lang/String;

    iget-object v2, p0, Lbys;->a:Lgbu;

    iget-object v2, v2, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 688
    iget-object v1, p0, Lbys;->b:Lbym;

    iget-object v1, v1, Lbym;->m:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 690
    :cond_0
    const-string v0, "Error adding video to playlist"

    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 691
    iget-object v0, p0, Lbys;->b:Lbym;

    iget-object v0, v0, Lbym;->i:Leyt;

    invoke-interface {v0, p1}, Leyt;->c(Ljava/lang/Throwable;)V

    .line 692
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 663
    iget-object v0, p0, Lbys;->b:Lbym;

    iget-object v0, v0, Lbym;->m:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, Lbys;->a:Lgbu;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lbys;->b:Lbym;

    iget-object v0, v0, Lbym;->d:Lfxe;

    invoke-interface {v0}, Lfxe;->c()V

    iget-object v0, p0, Lbys;->b:Lbym;

    iget-object v0, v0, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lbys;->b:Lbym;

    iget-object v1, v1, Lbym;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v2, 0x7f0902ca

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lbys;->a:Lgbu;

    iget-object v5, v5, Lgbu;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Leze;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method
