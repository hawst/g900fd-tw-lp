.class public final enum Lgrf;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgrf;

.field public static final enum b:Lgrf;

.field private static enum d:Lgrf;

.field private static final synthetic e:[Lgrf;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lgrf;

    const-string v1, "Previous"

    invoke-direct {v0, v1, v2, v2}, Lgrf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgrf;->a:Lgrf;

    .line 36
    new-instance v0, Lgrf;

    const-string v1, "Next"

    invoke-direct {v0, v1, v3, v3}, Lgrf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgrf;->d:Lgrf;

    .line 42
    new-instance v0, Lgrf;

    const-string v1, "Closest"

    invoke-direct {v0, v1, v4, v4}, Lgrf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgrf;->b:Lgrf;

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [Lgrf;

    sget-object v1, Lgrf;->a:Lgrf;

    aput-object v1, v0, v2

    sget-object v1, Lgrf;->d:Lgrf;

    aput-object v1, v0, v3

    sget-object v1, Lgrf;->b:Lgrf;

    aput-object v1, v0, v4

    sput-object v0, Lgrf;->e:[Lgrf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Lgrf;->c:I

    .line 48
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgrf;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lgrf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgrf;

    return-object v0
.end method

.method public static values()[Lgrf;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lgrf;->e:[Lgrf;

    invoke-virtual {v0}, [Lgrf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgrf;

    return-object v0
.end method
