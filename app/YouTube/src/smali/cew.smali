.class public final Lcew;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private final b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

.field private final c:Lfsj;

.field private final d:Levn;

.field private e:Lfvc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Levn;)V
    .locals 6

    .prologue
    .line 39
    const v4, 0x7f040091

    const v5, 0x7f040093

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcew;-><init>(Landroid/content/Context;Lfsj;Levn;II)V

    .line 45
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lfsj;Levn;II)V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcew;->c:Lfsj;

    .line 55
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcew;->d:Levn;

    .line 56
    new-instance v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-direct {v0, p1, p4, p5}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcew;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    .line 61
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 62
    const v1, 0x7f040118

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iput-object v0, p0, Lcew;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 63
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iget-object v1, p0, Lcew;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->addView(Landroid/view/View;)V

    .line 64
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 65
    return-void
.end method


# virtual methods
.method public final a(Lfsg;Lfvc;)Landroid/view/View;
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lcew;->e:Lfvc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcew;->e:Lfvc;

    iget-object v0, v0, Lfvc;->e:Ljava/lang/Object;

    iget-object v1, p2, Lfvc;->e:Ljava/lang/Object;

    if-eq v0, v1, :cond_1

    .line 71
    :cond_0
    iget-object v0, p0, Lcew;->d:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lcew;->d:Levn;

    iget-object v1, p2, Lfvc;->e:Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, p0, v2, v1}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 74
    :cond_1
    iput-object p2, p0, Lcew;->e:Lfvc;

    .line 76
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iget-object v1, p2, Lfvc;->b:Lfve;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lfve;)V

    .line 77
    iget-object v0, p0, Lcew;->c:Lfsj;

    iget-object v1, p2, Lfvc;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, v1}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p2, Lfvc;->c:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    .line 80
    iget-object v0, p0, Lcew;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget-object v1, p2, Lfvc;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    :goto_0
    iget-object v0, p2, Lfvc;->d:Lbt;

    instance-of v0, v0, Lfua;

    if-eqz v0, :cond_4

    .line 88
    iget-object v0, p2, Lfvc;->d:Lbt;

    check-cast v0, Lfua;

    invoke-virtual {p0, v0}, Lcew;->onContentEvent(Lfua;)V

    .line 95
    :cond_2
    :goto_1
    iget-object v0, p0, Lcew;->c:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 82
    :cond_3
    iget-object v0, p0, Lcew;->b:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    const v1, 0x7f0902a9

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(I)V

    goto :goto_0

    .line 89
    :cond_4
    iget-object v0, p2, Lfvc;->d:Lbt;

    instance-of v0, v0, Lfuc;

    if-eqz v0, :cond_5

    .line 90
    iget-object v0, p2, Lfvc;->d:Lbt;

    check-cast v0, Lfuc;

    invoke-virtual {p0, v0}, Lcew;->onLoadingEvent(Lfuc;)V

    goto :goto_1

    .line 91
    :cond_5
    iget-object v0, p2, Lfvc;->d:Lbt;

    instance-of v0, v0, Lfub;

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p2, Lfvc;->d:Lbt;

    check-cast v0, Lfub;

    invoke-virtual {p0, v0}, Lcew;->onErrorEvent(Lfub;)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    check-cast p2, Lfvc;

    invoke-virtual {p0, p1, p2}, Lcew;->a(Lfsg;Lfvc;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onContentEvent(Lfua;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 112
    return-void
.end method

.method public final onErrorEvent(Lfub;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iget-object v1, p1, Lfub;->a:Ljava/lang/CharSequence;

    iget-boolean v2, p1, Lfub;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;Z)V

    .line 117
    return-void
.end method

.method public final onLoadingEvent(Lfuc;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 104
    return-void
.end method
