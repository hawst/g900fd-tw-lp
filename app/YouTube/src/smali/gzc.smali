.class public final Lgzc;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgzc;


# instance fields
.field private b:Lhgz;

.field private c:Lhgz;

.field private d:Lhgz;

.field private e:Lhgz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5726
    const/4 v0, 0x0

    new-array v0, v0, [Lgzc;

    sput-object v0, Lgzc;->a:[Lgzc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5727
    invoke-direct {p0}, Lidf;-><init>()V

    .line 5730
    iput-object v0, p0, Lgzc;->b:Lhgz;

    .line 5733
    iput-object v0, p0, Lgzc;->c:Lhgz;

    .line 5736
    iput-object v0, p0, Lgzc;->d:Lhgz;

    .line 5739
    iput-object v0, p0, Lgzc;->e:Lhgz;

    .line 5727
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 5794
    const/4 v0, 0x0

    .line 5795
    iget-object v1, p0, Lgzc;->b:Lhgz;

    if-eqz v1, :cond_0

    .line 5796
    const/4 v0, 0x1

    iget-object v1, p0, Lgzc;->b:Lhgz;

    .line 5797
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5799
    :cond_0
    iget-object v1, p0, Lgzc;->c:Lhgz;

    if-eqz v1, :cond_1

    .line 5800
    const/4 v1, 0x2

    iget-object v2, p0, Lgzc;->c:Lhgz;

    .line 5801
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5803
    :cond_1
    iget-object v1, p0, Lgzc;->d:Lhgz;

    if-eqz v1, :cond_2

    .line 5804
    const/4 v1, 0x3

    iget-object v2, p0, Lgzc;->d:Lhgz;

    .line 5805
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5807
    :cond_2
    iget-object v1, p0, Lgzc;->e:Lhgz;

    if-eqz v1, :cond_3

    .line 5808
    const/4 v1, 0x4

    iget-object v2, p0, Lgzc;->e:Lhgz;

    .line 5809
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5811
    :cond_3
    iget-object v1, p0, Lgzc;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5812
    iput v0, p0, Lgzc;->J:I

    .line 5813
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 5723
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzc;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzc;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzc;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzc;->b:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzc;->b:Lhgz;

    :cond_2
    iget-object v0, p0, Lgzc;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgzc;->c:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzc;->c:Lhgz;

    :cond_3
    iget-object v0, p0, Lgzc;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgzc;->d:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzc;->d:Lhgz;

    :cond_4
    iget-object v0, p0, Lgzc;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lgzc;->e:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzc;->e:Lhgz;

    :cond_5
    iget-object v0, p0, Lgzc;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 5776
    iget-object v0, p0, Lgzc;->b:Lhgz;

    if-eqz v0, :cond_0

    .line 5777
    const/4 v0, 0x1

    iget-object v1, p0, Lgzc;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 5779
    :cond_0
    iget-object v0, p0, Lgzc;->c:Lhgz;

    if-eqz v0, :cond_1

    .line 5780
    const/4 v0, 0x2

    iget-object v1, p0, Lgzc;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 5782
    :cond_1
    iget-object v0, p0, Lgzc;->d:Lhgz;

    if-eqz v0, :cond_2

    .line 5783
    const/4 v0, 0x3

    iget-object v1, p0, Lgzc;->d:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 5785
    :cond_2
    iget-object v0, p0, Lgzc;->e:Lhgz;

    if-eqz v0, :cond_3

    .line 5786
    const/4 v0, 0x4

    iget-object v1, p0, Lgzc;->e:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 5788
    :cond_3
    iget-object v0, p0, Lgzc;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 5790
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5753
    if-ne p1, p0, :cond_1

    .line 5760
    :cond_0
    :goto_0
    return v0

    .line 5754
    :cond_1
    instance-of v2, p1, Lgzc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 5755
    :cond_2
    check-cast p1, Lgzc;

    .line 5756
    iget-object v2, p0, Lgzc;->b:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzc;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzc;->c:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzc;->c:Lhgz;

    if-nez v2, :cond_3

    .line 5757
    :goto_2
    iget-object v2, p0, Lgzc;->d:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzc;->d:Lhgz;

    if-nez v2, :cond_3

    .line 5758
    :goto_3
    iget-object v2, p0, Lgzc;->e:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgzc;->e:Lhgz;

    if-nez v2, :cond_3

    .line 5759
    :goto_4
    iget-object v2, p0, Lgzc;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Lgzc;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 5760
    goto :goto_0

    .line 5756
    :cond_4
    iget-object v2, p0, Lgzc;->b:Lhgz;

    iget-object v3, p1, Lgzc;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzc;->c:Lhgz;

    iget-object v3, p1, Lgzc;->c:Lhgz;

    .line 5757
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgzc;->d:Lhgz;

    iget-object v3, p1, Lgzc;->d:Lhgz;

    .line 5758
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgzc;->e:Lhgz;

    iget-object v3, p1, Lgzc;->e:Lhgz;

    .line 5759
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lgzc;->I:Ljava/util/List;

    iget-object v3, p1, Lgzc;->I:Ljava/util/List;

    .line 5760
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5764
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5766
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzc;->b:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 5767
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzc;->c:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5768
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzc;->d:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 5769
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzc;->e:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 5770
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzc;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 5771
    return v0

    .line 5766
    :cond_0
    iget-object v0, p0, Lgzc;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5767
    :cond_1
    iget-object v0, p0, Lgzc;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5768
    :cond_2
    iget-object v0, p0, Lgzc;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 5769
    :cond_3
    iget-object v0, p0, Lgzc;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    .line 5770
    :cond_4
    iget-object v1, p0, Lgzc;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
