.class public Lcbv;
.super Lfsb;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Lfvi;

.field private final h:Leyo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;ILfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p3, p5, p6}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcbv;->a:Landroid/content/Context;

    .line 51
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 52
    const/4 v1, 0x0

    invoke-virtual {v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbv;->b:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lcbv;->b:Landroid/view/View;

    const v1, 0x7f080125

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbv;->c:Landroid/widget/TextView;

    .line 54
    iget-object v0, p0, Lcbv;->b:Landroid/view/View;

    const v1, 0x7f080126

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbv;->d:Landroid/widget/TextView;

    .line 55
    iget-object v0, p0, Lcbv;->b:Landroid/view/View;

    const v1, 0x7f080127

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbv;->e:Landroid/widget/TextView;

    .line 56
    iget-object v0, p0, Lcbv;->b:Landroid/view/View;

    const v1, 0x7f0800c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcbv;->f:Landroid/widget/ImageView;

    .line 57
    new-instance v0, Lfvi;

    iget-object v1, p0, Lcbv;->f:Landroid/widget/ImageView;

    invoke-direct {v0, p2, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcbv;->g:Lfvi;

    .line 58
    new-instance v0, Lcbw;

    invoke-direct {v0, p0}, Lcbw;-><init>(Lcbv;)V

    iput-object v0, p0, Lcbv;->h:Leyo;

    .line 59
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcbv;->g:Lfvi;

    const v1, 0x7f0201e9

    invoke-virtual {v0, v1}, Lfvi;->c(I)V

    .line 96
    return-void
.end method

.method static synthetic a(Lcbv;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcbv;->a()V

    return-void
.end method


# virtual methods
.method protected final a(Lfnc;)V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p1}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcbv;->g:Lfvi;

    iget-object v1, p0, Lcbv;->h:Leyo;

    invoke-virtual {v0, p1, v1}, Lfvi;->a(Lfnc;Leyo;)V

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-direct {p0}, Lcbv;->a()V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcbv;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcbv;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 76
    return-void
.end method

.method protected final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcbv;->d:Landroid/widget/TextView;

    invoke-static {v0, p1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method protected final c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcbv;->e:Landroid/widget/TextView;

    invoke-static {v0, p1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 84
    return-void
.end method
