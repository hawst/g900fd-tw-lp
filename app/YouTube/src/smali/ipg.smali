.class final Lipg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liql;


# static fields
.field private static synthetic f:Z


# instance fields
.field private final a:Lipq;

.field private final b:Lipq;

.field private final c:J

.field private d:Z

.field private e:Z

.field private synthetic g:Lipe;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 314
    const-class v0, Lipe;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lipg;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lipe;J)V
    .locals 2

    .prologue
    .line 333
    iput-object p1, p0, Lipg;->g:Lipe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316
    new-instance v0, Lipq;

    invoke-direct {v0}, Lipq;-><init>()V

    iput-object v0, p0, Lipg;->a:Lipq;

    .line 319
    new-instance v0, Lipq;

    invoke-direct {v0}, Lipq;-><init>()V

    iput-object v0, p0, Lipg;->b:Lipq;

    .line 334
    iput-wide p2, p0, Lipg;->c:J

    .line 335
    return-void
.end method

.method static synthetic a(Lipg;)Z
    .locals 1

    .prologue
    .line 314
    iget-boolean v0, p0, Lipg;->e:Z

    return v0
.end method

.method static synthetic a(Lipg;Z)Z
    .locals 0

    .prologue
    .line 314
    iput-boolean p1, p0, Lipg;->e:Z

    return p1
.end method

.method private b()V
    .locals 4

    .prologue
    .line 374
    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-static {v0}, Lipe;->c(Lipe;)Liph;

    move-result-object v0

    invoke-virtual {v0}, Liph;->c()V

    .line 376
    :goto_0
    :try_start_0
    iget-object v0, p0, Lipg;->b:Lipq;

    iget-wide v0, v0, Lipq;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lipg;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lipg;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-static {v0}, Lipe;->d(Lipe;)Linr;

    move-result-object v0

    if-nez v0, :cond_0

    .line 377
    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-static {v0}, Lipe;->e(Lipe;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 380
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lipg;->g:Lipe;

    invoke-static {v1}, Lipe;->c(Lipe;)Liph;

    move-result-object v1

    invoke-virtual {v1}, Liph;->b()V

    throw v0

    :cond_0
    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-static {v0}, Lipe;->c(Lipe;)Liph;

    move-result-object v0

    invoke-virtual {v0}, Liph;->b()V

    .line 381
    return-void
.end method

.method static synthetic b(Lipg;)Z
    .locals 1

    .prologue
    .line 314
    iget-boolean v0, p0, Lipg;->d:Z

    return v0
.end method


# virtual methods
.method public final a()Liqm;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-static {v0}, Lipe;->c(Lipe;)Liph;

    move-result-object v0

    return-object v0
.end method

.method final a(Lipu;J)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 385
    sget-boolean v0, Lipg;->f:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 411
    :cond_0
    sub-long/2addr p2, v4

    .line 414
    iget-object v3, p0, Lipg;->g:Lipe;

    monitor-enter v3

    .line 415
    :try_start_0
    iget-object v0, p0, Lipg;->b:Lipq;

    iget-wide v4, v0, Lipq;->b:J

    cmp-long v0, v4, v10

    if-nez v0, :cond_7

    move v0, v1

    .line 416
    :goto_0
    iget-object v4, p0, Lipg;->b:Lipq;

    iget-object v5, p0, Lipg;->a:Lipq;

    invoke-virtual {v4, v5}, Lipq;->a(Liql;)J

    .line 417
    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 420
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 387
    :cond_2
    cmp-long v0, p2, v10

    if-lez v0, :cond_3

    .line 390
    iget-object v3, p0, Lipg;->g:Lipe;

    monitor-enter v3

    .line 391
    :try_start_1
    iget-boolean v4, p0, Lipg;->e:Z

    .line 392
    iget-object v0, p0, Lipg;->b:Lipq;

    iget-wide v6, v0, Lipq;->b:J

    add-long/2addr v6, p2

    iget-wide v8, p0, Lipg;->c:J

    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    move v0, v1

    .line 393
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396
    if-eqz v0, :cond_5

    .line 397
    invoke-interface {p1, p2, p3}, Lipu;->f(J)V

    .line 398
    iget-object v0, p0, Lipg;->g:Lipe;

    sget-object v1, Linr;->f:Linr;

    invoke-virtual {v0, v1}, Lipe;->b(Linr;)V

    .line 421
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v2

    .line 392
    goto :goto_1

    .line 393
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 403
    :cond_5
    if-eqz v4, :cond_6

    .line 404
    invoke-interface {p1, p2, p3}, Lipu;->f(J)V

    goto :goto_2

    .line 409
    :cond_6
    iget-object v0, p0, Lipg;->a:Lipq;

    invoke-interface {p1, v0, p2, p3}, Lipu;->b(Lipq;J)J

    move-result-wide v4

    .line 410
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_7
    move v0, v2

    .line 415
    goto :goto_0

    .line 420
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final b(Lipq;J)J
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 339
    cmp-long v0, p2, v4

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :cond_0
    iget-object v2, p0, Lipg;->g:Lipe;

    monitor-enter v2

    .line 343
    :try_start_0
    invoke-direct {p0}, Lipg;->b()V

    .line 344
    iget-boolean v0, p0, Lipg;->d:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 344
    :cond_1
    :try_start_1
    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-static {v0}, Lipe;->d(Lipe;)Linr;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lipg;->g:Lipe;

    invoke-static {v1}, Lipe;->d(Lipe;)Linr;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x12

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "stream was reset: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_2
    iget-object v0, p0, Lipg;->b:Lipq;

    iget-wide v0, v0, Lipq;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    const-wide/16 v0, -0x1

    monitor-exit v2

    .line 369
    :goto_0
    return-wide v0

    .line 348
    :cond_3
    iget-object v0, p0, Lipg;->b:Lipq;

    iget-object v1, p0, Lipg;->b:Lipq;

    iget-wide v4, v1, Lipq;->b:J

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-virtual {v0, p1, v4, v5}, Lipq;->b(Lipq;J)J

    move-result-wide v0

    .line 351
    iget-object v3, p0, Lipg;->g:Lipe;

    iget-wide v4, v3, Lipe;->a:J

    add-long/2addr v4, v0

    iput-wide v4, v3, Lipe;->a:J

    .line 352
    iget-object v3, p0, Lipg;->g:Lipe;

    iget-wide v4, v3, Lipe;->a:J

    iget-object v3, p0, Lipg;->g:Lipe;

    invoke-static {v3}, Lipe;->a(Lipe;)Lios;

    move-result-object v3

    iget-object v3, v3, Lios;->e:Lioo;

    const/high16 v6, 0x10000

    invoke-virtual {v3, v6}, Lioo;->c(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_4

    .line 354
    iget-object v3, p0, Lipg;->g:Lipe;

    invoke-static {v3}, Lipe;->a(Lipe;)Lios;

    move-result-object v3

    iget-object v4, p0, Lipg;->g:Lipe;

    invoke-static {v4}, Lipe;->b(Lipe;)I

    move-result v4

    iget-object v5, p0, Lipg;->g:Lipe;

    iget-wide v6, v5, Lipe;->a:J

    invoke-virtual {v3, v4, v6, v7}, Lios;->a(IJ)V

    .line 355
    iget-object v3, p0, Lipg;->g:Lipe;

    const-wide/16 v4, 0x0

    iput-wide v4, v3, Lipe;->a:J

    .line 357
    :cond_4
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 360
    iget-object v2, p0, Lipg;->g:Lipe;

    invoke-static {v2}, Lipe;->a(Lipe;)Lios;

    move-result-object v2

    monitor-enter v2

    .line 361
    :try_start_2
    iget-object v3, p0, Lipg;->g:Lipe;

    invoke-static {v3}, Lipe;->a(Lipe;)Lios;

    move-result-object v3

    iget-wide v4, v3, Lios;->c:J

    add-long/2addr v4, v0

    iput-wide v4, v3, Lios;->c:J

    .line 362
    iget-object v3, p0, Lipg;->g:Lipe;

    invoke-static {v3}, Lipe;->a(Lipe;)Lios;

    move-result-object v3

    iget-wide v4, v3, Lios;->c:J

    iget-object v3, p0, Lipg;->g:Lipe;

    invoke-static {v3}, Lipe;->a(Lipe;)Lios;

    move-result-object v3

    iget-object v3, v3, Lios;->e:Lioo;

    const/high16 v6, 0x10000

    invoke-virtual {v3, v6}, Lioo;->c(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_5

    .line 364
    iget-object v3, p0, Lipg;->g:Lipe;

    invoke-static {v3}, Lipe;->a(Lipe;)Lios;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lipg;->g:Lipe;

    invoke-static {v5}, Lipe;->a(Lipe;)Lios;

    move-result-object v5

    iget-wide v6, v5, Lios;->c:J

    invoke-virtual {v3, v4, v6, v7}, Lios;->a(IJ)V

    .line 365
    iget-object v3, p0, Lipg;->g:Lipe;

    invoke-static {v3}, Lipe;->a(Lipe;)Lios;

    move-result-object v3

    const-wide/16 v4, 0x0

    iput-wide v4, v3, Lios;->c:J

    .line 367
    :cond_5
    monitor-exit v2

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 429
    iget-object v1, p0, Lipg;->g:Lipe;

    monitor-enter v1

    .line 430
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lipg;->d:Z

    .line 431
    iget-object v0, p0, Lipg;->b:Lipq;

    invoke-virtual {v0}, Lipq;->n()V

    .line 432
    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 433
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 434
    iget-object v0, p0, Lipg;->g:Lipe;

    invoke-static {v0}, Lipe;->f(Lipe;)V

    .line 435
    return-void

    .line 433
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
