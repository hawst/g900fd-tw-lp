.class public final Limc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/net/URL;

.field c:Ljava/lang/String;

.field d:Lilt;

.field e:Lt;

.field f:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    const-string v0, "GET"

    iput-object v0, p0, Limc;->c:Ljava/lang/String;

    .line 134
    new-instance v0, Lilt;

    invoke-direct {v0}, Lilt;-><init>()V

    iput-object v0, p0, Limc;->d:Lilt;

    .line 135
    return-void
.end method

.method constructor <init>(Limb;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iget-object v0, p1, Limb;->a:Ljava/lang/String;

    iput-object v0, p0, Limc;->a:Ljava/lang/String;

    .line 139
    iget-object v0, p1, Limb;->f:Ljava/net/URL;

    iput-object v0, p0, Limc;->b:Ljava/net/URL;

    .line 140
    iget-object v0, p1, Limb;->b:Ljava/lang/String;

    iput-object v0, p0, Limc;->c:Ljava/lang/String;

    .line 141
    iget-object v0, p1, Limb;->d:Lt;

    iput-object v0, p0, Limc;->e:Lt;

    .line 142
    iget-object v0, p1, Limb;->e:Ljava/lang/Object;

    iput-object v0, p0, Limc;->f:Ljava/lang/Object;

    .line 143
    iget-object v0, p1, Limb;->c:Lils;

    invoke-virtual {v0}, Lils;->a()Lilt;

    move-result-object v0

    iput-object v0, p0, Limc;->d:Lilt;

    .line 144
    return-void
.end method


# virtual methods
.method public final a()Limb;
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Limc;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :cond_0
    new-instance v0, Limb;

    invoke-direct {v0, p0}, Limb;-><init>(Limc;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Limc;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Limc;->d:Lilt;

    invoke-virtual {v0, p1}, Lilt;->a(Ljava/lang/String;)Lilt;

    .line 179
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Limc;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Limc;->d:Lilt;

    invoke-virtual {v0, p1, p2}, Lilt;->c(Ljava/lang/String;Ljava/lang/String;)Lilt;

    .line 165
    return-object p0
.end method

.method public final a(Ljava/lang/String;Lt;)Limc;
    .locals 2

    .prologue
    .line 213
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 214
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "method == null || method.length() == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_1
    iput-object p1, p0, Limc;->c:Ljava/lang/String;

    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Limc;->e:Lt;

    .line 221
    return-object p0
.end method

.method public final a(Ljava/net/URL;)Limc;
    .locals 2

    .prologue
    .line 153
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    iput-object p1, p0, Limc;->b:Ljava/net/URL;

    .line 155
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Limc;->a:Ljava/lang/String;

    .line 156
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Limc;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Limc;->d:Lilt;

    invoke-virtual {v0, p1, p2}, Lilt;->a(Ljava/lang/String;Ljava/lang/String;)Lilt;

    .line 174
    return-object p0
.end method
