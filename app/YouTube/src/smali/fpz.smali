.class public Lfpz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lflb;
.implements Lfqh;
.implements Lfrx;


# instance fields
.field private a:Lfqh;

.field private b:[B

.field public final c:Lhog;

.field public d:Lfnc;

.field public final e:Ljava/lang/String;

.field public final f:Lhiq;

.field public final g:Lhig;

.field public h:Z

.field public final i:Lfqa;

.field private j:Ljava/lang/String;

.field private final k:Ljava/util/Set;


# direct methods
.method constructor <init>(Lfqh;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object v0, p0, Lfpz;->c:Lhog;

    .line 79
    iput-object p1, p0, Lfpz;->a:Lfqh;

    .line 80
    iput-object v0, p0, Lfpz;->b:[B

    .line 81
    iput-object v0, p0, Lfpz;->j:Ljava/lang/String;

    .line 82
    iput-object v0, p0, Lfpz;->e:Ljava/lang/String;

    .line 84
    iput-object v0, p0, Lfpz;->i:Lfqa;

    .line 86
    iput-object v0, p0, Lfpz;->f:Lhiq;

    .line 87
    iput-object v0, p0, Lfpz;->g:Lhig;

    .line 88
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfpz;->k:Ljava/util/Set;

    .line 89
    return-void
.end method

.method public constructor <init>(Lhhu;Lfqa;Lfqh;)V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v0, p1, Lhhu;->b:Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v0, p1, Lhhu;->a:Lhog;

    iput-object v0, p0, Lfpz;->c:Lhog;

    .line 105
    iput-object p3, p0, Lfpz;->a:Lfqh;

    .line 106
    iget-object v0, p1, Lhhu;->g:[B

    iput-object v0, p0, Lfpz;->b:[B

    .line 109
    new-instance v0, Lfnc;

    iget-object v1, p1, Lhhu;->c:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfpz;->d:Lfnc;

    .line 110
    iget-object v0, p1, Lhhu;->b:Ljava/lang/String;

    iput-object v0, p0, Lfpz;->j:Ljava/lang/String;

    .line 111
    iget-object v0, p1, Lhhu;->e:Ljava/lang/String;

    iput-object v0, p0, Lfpz;->e:Ljava/lang/String;

    .line 112
    iget v0, p1, Lhhu;->d:I

    .line 113
    iget-object v0, p1, Lhhu;->f:Lhiq;

    iput-object v0, p0, Lfpz;->f:Lhiq;

    .line 114
    iget-object v0, p1, Lhhu;->h:Lhig;

    iput-object v0, p0, Lfpz;->g:Lhig;

    .line 115
    iput-object p2, p0, Lfpz;->i:Lfqa;

    .line 116
    iget-object v0, p0, Lfpz;->c:Lhog;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfpz;->k:Ljava/util/Set;

    .line 117
    return-void
.end method

.method public constructor <init>(Lhhu;Lfqh;)V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lfpz;-><init>(Lhhu;Lfqa;Lfqh;)V

    .line 95
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lfpz;->k:Ljava/util/Set;

    return-object v0
.end method

.method public a(Lfau;)V
    .locals 0

    .prologue
    .line 206
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 207
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lfpz;->c:Lhog;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lfpz;->j:Ljava/lang/String;

    return-object v0
.end method

.method public d()[B
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lfpz;->b:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lfpz;->a:Lfqh;

    return-object v0
.end method
