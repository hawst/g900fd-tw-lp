.class Lbrm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I


# instance fields
.field public final a:Z

.field public final b:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 63
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbrm;->c:[I

    .line 69
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lbrm;->d:[I

    .line 75
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lbrm;->e:[I

    .line 81
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lbrm;->f:[I

    return-void

    .line 63
    :array_0
    .array-data 4
        0x7f09031f
        0x7f090320
        0x7f100017
        0x7f100018
    .end array-data

    .line 69
    :array_1
    .array-data 4
        0x7f090321
        0x7f090322
        0x7f100019
        0x7f10001a
    .end array-data

    .line 75
    :array_2
    .array-data 4
        0x7f090323
        0x7f090324
        0x7f10001b
        0x7f10001c
    .end array-data

    .line 81
    :array_3
    .array-data 4
        0x7f090325
        0x7f090326
        0x7f10001d
        0x7f10001e
    .end array-data
.end method

.method public constructor <init>(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    if-eqz p2, :cond_0

    const v0, 0x7f080216

    .line 100
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbrm;->b:Landroid/view/View;

    .line 101
    iput-boolean p2, p0, Lbrm;->a:Z

    .line 102
    return-void

    .line 99
    :cond_0
    const v0, 0x7f080215

    goto :goto_0
.end method

.method private a(ILfkn;[I)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 183
    iget-object v0, p0, Lbrm;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 184
    if-nez p2, :cond_1

    move v0, v1

    .line 186
    :goto_0
    iget-boolean v2, p0, Lbrm;->a:Z

    if-nez v2, :cond_3

    if-nez p1, :cond_3

    move v2, v3

    :goto_1
    iget-boolean v4, p0, Lbrm;->a:Z

    if-eqz v4, :cond_4

    if-ne p1, v3, :cond_4

    move v4, v3

    :goto_2
    if-nez v2, :cond_0

    if-eqz v4, :cond_5

    :cond_0
    move v2, v3

    :goto_3
    if-eqz v2, :cond_7

    .line 188
    if-lez v0, :cond_6

    const/4 v2, 0x3

    aget v2, p3, v2

    new-array v3, v3, [Ljava/lang/Object;

    .line 190
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v5, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 199
    :goto_4
    return-object v0

    .line 184
    :cond_1
    iget-boolean v0, p0, Lbrm;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p2, Lfkn;->a:Lhlk;

    iget v0, v0, Lhlk;->c:I

    goto :goto_0

    :cond_2
    iget-object v0, p2, Lfkn;->a:Lhlk;

    iget v0, v0, Lhlk;->g:I

    goto :goto_0

    :cond_3
    move v2, v1

    .line 186
    goto :goto_1

    :cond_4
    move v4, v1

    goto :goto_2

    :cond_5
    move v2, v1

    goto :goto_3

    .line 190
    :cond_6
    aget v0, p3, v3

    .line 192
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 195
    :cond_7
    if-lez v0, :cond_8

    const/4 v2, 0x2

    aget v2, p3, v2

    new-array v3, v3, [Ljava/lang/Object;

    .line 197
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v5, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_8
    aget v0, p3, v1

    .line 199
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lbrm;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 154
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lbrm;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    return-void
.end method

.method public final a(ILfkn;)V
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2}, Lbrm;->c(ILfkn;)V

    .line 123
    iget-boolean v0, p0, Lbrm;->a:Z

    if-nez v0, :cond_0

    sget-object v0, Lbrm;->c:[I

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lbrm;->a(ILfkn;[I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lbrm;->a(Ljava/lang/CharSequence;)V

    .line 124
    return-void

    .line 123
    :cond_0
    sget-object v0, Lbrm;->d:[I

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lbrm;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 106
    iget-object v1, p0, Lbrm;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 107
    return-void

    .line 106
    :cond_0
    const v0, 0x3e4ccccd    # 0.2f

    goto :goto_0
.end method

.method public final b(ILfkn;)V
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0, p1, p2}, Lbrm;->c(ILfkn;)V

    .line 128
    iget-boolean v0, p0, Lbrm;->a:Z

    if-nez v0, :cond_0

    sget-object v0, Lbrm;->e:[I

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lbrm;->a(ILfkn;[I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lbrm;->a(Ljava/lang/CharSequence;)V

    .line 129
    return-void

    .line 128
    :cond_0
    sget-object v0, Lbrm;->f:[I

    goto :goto_0
.end method

.method protected c(ILfkn;)V
    .locals 3

    .prologue
    .line 138
    packed-switch p1, :pswitch_data_0

    .line 141
    iget-object v0, p0, Lbrm;->b:Landroid/view/View;

    .line 147
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    move-object v1, v0

    move v0, v2

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 150
    return-void

    .line 144
    :pswitch_0
    iget-object v1, p0, Lbrm;->b:Landroid/view/View;

    iget-boolean v0, p0, Lbrm;->a:Z

    goto :goto_0

    .line 147
    :pswitch_1
    iget-object v0, p0, Lbrm;->b:Landroid/view/View;

    iget-boolean v1, p0, Lbrm;->a:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    move v2, v1

    move-object v1, v0

    move v0, v2

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
