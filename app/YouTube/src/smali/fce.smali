.class public final Lfce;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Lfvi;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Leyp;)V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const v0, 0x7f040019

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfce;->a:Landroid/view/View;

    .line 35
    iget-object v0, p0, Lfce;->a:Landroid/view/View;

    const v1, 0x7f0800a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfce;->b:Landroid/widget/TextView;

    .line 36
    new-instance v1, Lfvi;

    iget-object v0, p0, Lfce;->a:Landroid/view/View;

    const v2, 0x7f0800a4

    .line 37
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p3, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lfce;->c:Lfvi;

    .line 38
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    .line 23
    check-cast p2, Lfid;

    iget-object v0, p0, Lfce;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Lfid;->a()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lfce;->c:Lfvi;

    invoke-virtual {p2}, Lfid;->c()Lfnc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    iget-object v0, p0, Lfce;->a:Landroid/view/View;

    return-object v0
.end method
