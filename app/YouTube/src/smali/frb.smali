.class public final enum Lfrb;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfrb;

.field public static final enum b:Lfrb;

.field public static final enum c:Lfrb;

.field public static final enum d:Lfrb;

.field private static final synthetic e:[Lfrb;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lfrb;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v2}, Lfrb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfrb;->a:Lfrb;

    .line 58
    new-instance v0, Lfrb;

    const-string v1, "NON_ADAPTIVE"

    invoke-direct {v0, v1, v3}, Lfrb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfrb;->b:Lfrb;

    .line 59
    new-instance v0, Lfrb;

    const-string v1, "ADAPTIVE"

    invoke-direct {v0, v1, v4}, Lfrb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfrb;->c:Lfrb;

    .line 60
    new-instance v0, Lfrb;

    const-string v1, "SERVER_EXPERIMENT"

    invoke-direct {v0, v1, v5}, Lfrb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfrb;->d:Lfrb;

    .line 56
    const/4 v0, 0x4

    new-array v0, v0, [Lfrb;

    sget-object v1, Lfrb;->a:Lfrb;

    aput-object v1, v0, v2

    sget-object v1, Lfrb;->b:Lfrb;

    aput-object v1, v0, v3

    sget-object v1, Lfrb;->c:Lfrb;

    aput-object v1, v0, v4

    sget-object v1, Lfrb;->d:Lfrb;

    aput-object v1, v0, v5

    sput-object v0, Lfrb;->e:[Lfrb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfrb;
    .locals 1

    .prologue
    .line 56
    const-class v0, Lfrb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfrb;

    return-object v0
.end method

.method public static values()[Lfrb;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lfrb;->e:[Lfrb;

    invoke-virtual {v0}, [Lfrb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfrb;

    return-object v0
.end method
