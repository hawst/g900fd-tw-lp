.class public final Lerp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/text/DecimalFormat;


# instance fields
.field b:J

.field c:D

.field d:Z

.field e:Z

.field final f:[Ljava/lang/Long;

.field final g:[Ljava/lang/Long;

.field final h:[Ljava/lang/Long;

.field final i:Ljava/util/List;

.field j:Landroid/graphics/Rect;

.field k:I

.field l:I

.field m:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lerp;->a:Ljava/text/DecimalFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Long;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v8

    const/4 v1, 0x4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lerp;->f:[Ljava/lang/Long;

    .line 122
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Long;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v8

    const/4 v1, 0x4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lerp;->g:[Ljava/lang/Long;

    .line 123
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Long;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v8

    const/4 v1, 0x4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lerp;->h:[Ljava/lang/Long;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lerp;->i:Ljava/util/List;

    .line 125
    return-void
.end method

.method static a(D)Lerq;
    .locals 2

    .prologue
    .line 285
    sget-object v0, Lerq;->a:Lerq;

    iget-wide v0, v0, Lerq;->f:D

    cmpl-double v0, p0, v0

    if-ltz v0, :cond_0

    .line 286
    sget-object v0, Lerq;->a:Lerq;

    .line 296
    :goto_0
    return-object v0

    .line 287
    :cond_0
    sget-object v0, Lerq;->b:Lerq;

    iget-wide v0, v0, Lerq;->f:D

    cmpl-double v0, p0, v0

    if-ltz v0, :cond_1

    .line 288
    sget-object v0, Lerq;->b:Lerq;

    goto :goto_0

    .line 289
    :cond_1
    sget-object v0, Lerq;->c:Lerq;

    iget-wide v0, v0, Lerq;->f:D

    cmpl-double v0, p0, v0

    if-ltz v0, :cond_2

    .line 290
    sget-object v0, Lerq;->c:Lerq;

    goto :goto_0

    .line 291
    :cond_2
    sget-object v0, Lerq;->d:Lerq;

    iget-wide v0, v0, Lerq;->f:D

    cmpl-double v0, p0, v0

    if-ltz v0, :cond_3

    .line 292
    sget-object v0, Lerq;->d:Lerq;

    goto :goto_0

    .line 293
    :cond_3
    sget-object v0, Lerq;->e:Lerq;

    iget-wide v0, v0, Lerq;->f:D

    cmpl-double v0, p0, v0

    if-lez v0, :cond_4

    .line 294
    sget-object v0, Lerq;->e:Lerq;

    goto :goto_0

    .line 296
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(DLero;)V
    .locals 5

    .prologue
    .line 133
    iget v0, p3, Lero;->k:I

    if-gez v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lerp;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    iget v1, p3, Lero;->k:I

    if-gt v0, v1, :cond_1

    .line 140
    iget-object v1, p0, Lerp;->i:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 143
    :cond_1
    iget-object v0, p0, Lerp;->i:Ljava/util/List;

    iget v1, p3, Lero;->k:I

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, p1

    double-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
