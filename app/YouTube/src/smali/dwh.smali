.class public final enum Ldwh;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldwh;

.field public static final enum b:Ldwh;

.field public static final enum c:Ldwh;

.field public static final enum d:Ldwh;

.field public static final enum e:Ldwh;

.field public static final enum f:Ldwh;

.field public static final enum g:Ldwh;

.field public static final enum h:Ldwh;

.field public static final enum i:Ldwh;

.field private static final synthetic l:[Ldwh;


# instance fields
.field public final j:I

.field public final k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const v3, 0x7f09001a

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 13
    new-instance v0, Ldwh;

    const-string v1, "LAUNCH_CAST_FAIL_TIMEOUT"

    invoke-direct {v0, v1, v5, v3, v5}, Ldwh;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwh;->a:Ldwh;

    .line 14
    new-instance v0, Ldwh;

    const-string v1, "LAUNCH_FAIL_DEVICE_BUSY"

    const v2, 0x7f09001b

    invoke-direct {v0, v1, v4, v2, v4}, Ldwh;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwh;->b:Ldwh;

    .line 15
    new-instance v0, Ldwh;

    const-string v1, "LAUNCH_FAIL_NEEDS_INSTALL"

    const v2, 0x7f09001d

    invoke-direct {v0, v1, v6, v2, v5}, Ldwh;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwh;->c:Ldwh;

    .line 16
    new-instance v0, Ldwh;

    const-string v1, "LAUNCH_FAIL_TIMEOUT"

    invoke-direct {v0, v1, v7, v3, v4}, Ldwh;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwh;->d:Ldwh;

    .line 17
    new-instance v0, Ldwh;

    const-string v1, "LOUNGE_SERVER_CONNECTION_ERROR"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v3, v4}, Ldwh;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwh;->e:Ldwh;

    .line 18
    new-instance v0, Ldwh;

    const-string v1, "NETWORK"

    const/4 v2, 0x5

    const v3, 0x7f09001e

    invoke-direct {v0, v1, v2, v3, v4}, Ldwh;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwh;->f:Ldwh;

    .line 19
    new-instance v0, Ldwh;

    const-string v1, "SCREEN_NOT_FOUND"

    const/4 v2, 0x6

    const v3, 0x7f09001c

    invoke-direct {v0, v1, v2, v3, v5}, Ldwh;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwh;->g:Ldwh;

    .line 20
    new-instance v0, Ldwh;

    const-string v1, "UNPLAYABLE"

    const/4 v2, 0x7

    const v3, 0x7f090020

    invoke-direct {v0, v1, v2, v3, v4}, Ldwh;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwh;->h:Ldwh;

    .line 21
    new-instance v0, Ldwh;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x8

    const v3, 0x7f09001f

    invoke-direct {v0, v1, v2, v3, v5}, Ldwh;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwh;->i:Ldwh;

    .line 10
    const/16 v0, 0x9

    new-array v0, v0, [Ldwh;

    sget-object v1, Ldwh;->a:Ldwh;

    aput-object v1, v0, v5

    sget-object v1, Ldwh;->b:Ldwh;

    aput-object v1, v0, v4

    sget-object v1, Ldwh;->c:Ldwh;

    aput-object v1, v0, v6

    sget-object v1, Ldwh;->d:Ldwh;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, Ldwh;->e:Ldwh;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Ldwh;->f:Ldwh;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldwh;->g:Ldwh;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldwh;->h:Ldwh;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldwh;->i:Ldwh;

    aput-object v2, v0, v1

    sput-object v0, Ldwh;->l:[Ldwh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Ldwh;->j:I

    .line 29
    iput-boolean p4, p0, Ldwh;->k:Z

    .line 30
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldwh;
    .locals 1

    .prologue
    .line 10
    const-class v0, Ldwh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldwh;

    return-object v0
.end method

.method public static values()[Ldwh;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Ldwh;->l:[Ldwh;

    invoke-virtual {v0}, [Ldwh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldwh;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    invoke-virtual {p0}, Ldwh;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Ldwh;->k:Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "RemoteError [name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", canRetry="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
