.class public final Ldyj;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private a:Ldyi;


# direct methods
.method public constructor <init>(Ldyi;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 39
    iput-object p1, p0, Ldyj;->a:Ldyi;

    .line 40
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldyj;->a:Ldyi;

    invoke-virtual {v0}, Ldyi;->b()I

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public final mark(I)V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 3

    .prologue
    .line 58
    iget-object v1, p0, Ldyj;->a:Ldyi;

    invoke-virtual {v1}, Ldyi;->d()Ljava/nio/ByteBuffer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {v1}, Ldyi;->e()V

    goto :goto_0
.end method

.method public final read([B)I
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Ldyj;->a:Ldyi;

    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Ldyi;->a([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ldyj;->a:Ldyi;

    invoke-virtual {v0, p1, p2, p3}, Ldyi;->a([BII)I

    move-result v0

    return v0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final skip(J)J
    .locals 9

    .prologue
    .line 116
    iget-object v4, p0, Ldyj;->a:Ldyi;

    move-wide v2, p1

    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    invoke-virtual {v4}, Ldyi;->d()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    int-to-long v6, v0

    cmp-long v5, v6, v2

    if-lez v5, :cond_0

    long-to-int v0, v2

    :cond_0
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    int-to-long v0, v0

    sub-long v0, v2, v0

    invoke-virtual {v4}, Ldyi;->e()V

    move-wide v2, v0

    goto :goto_0

    :cond_1
    sub-long v0, p1, v2

    return-wide v0
.end method
