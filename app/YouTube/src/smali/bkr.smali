.class public final Lbkr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lgml;

.field final b:Lezj;

.field private final c:Layz;

.field private final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 36
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    return-void
.end method

.method public constructor <init>(Layz;Lgml;Lezj;J)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgml;

    iput-object v0, p0, Lbkr;->a:Lgml;

    .line 56
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layz;

    iput-object v0, p0, Lbkr;->c:Layz;

    .line 57
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lbkr;->b:Lezj;

    .line 58
    iput-wide p4, p0, Lbkr;->d:J

    .line 59
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;Lbjy;)Lesq;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 99
    :try_start_0
    new-instance v0, Lfai;

    iget-object v2, p0, Lbkr;->b:Lezj;

    iget-wide v4, p0, Lbkr;->d:J

    invoke-direct {v0, v2, v4, v5}, Lfai;-><init>(Lezj;J)V

    .line 100
    iget-object v2, p0, Lbkr;->c:Layz;

    const/4 v3, 0x0

    .line 101
    invoke-interface {v2, p1, v3, p2, v0}, Layz;->a(Ljava/lang/String;ZLbjy;Lfai;)Lesq;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_0

    iget-object v2, v0, Lesq;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 106
    :cond_0
    iget-object v0, p0, Lbkr;->a:Lgml;

    new-instance v2, Lesr;

    invoke-direct {v2}, Lesr;-><init>()V

    invoke-virtual {v2}, Lesr;->a()Lesq;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lgml;->a(Ljava/lang/String;Lesq;)Z

    move-object v0, v1

    .line 122
    :cond_1
    :goto_0
    return-object v0

    .line 109
    :cond_2
    iget-object v2, p0, Lbkr;->a:Lgml;

    invoke-virtual {v2, p1, v0}, Lgml;->a(Ljava/lang/String;Lesq;)Z
    :try_end_0
    .catch Lcnp; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 112
    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    .line 114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error loading ad breaks for ad [originalVideoId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 115
    goto :goto_0

    .line 116
    :catch_1
    move-exception v0

    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x30

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error saving ad breaks for ad [originalVideoId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 118
    goto :goto_0

    .line 120
    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3c

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Timeout error loading non-YouTube-hosted ad video [request="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    move-object v0, v1

    .line 122
    goto/16 :goto_0
.end method

.method a(Less;)Lfoy;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 131
    :try_start_0
    new-instance v1, Lfai;

    iget-object v2, p0, Lbkr;->b:Lezj;

    iget-wide v4, p0, Lbkr;->d:J

    invoke-direct {v1, v2, v4, v5}, Lfai;-><init>(Lezj;J)V

    .line 132
    iget-object v2, p0, Lbkr;->c:Layz;

    invoke-interface {v2, p1, v1}, Layz;->a(Less;Lfai;)Lfoy;

    move-result-object v1

    .line 133
    if-nez v1, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-object v0

    .line 136
    :cond_1
    invoke-virtual {v1}, Lfoy;->e()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lfoy;->d()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, Lfoy;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 138
    iget-object v1, p1, Less;->f:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x34

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error loading non-YouTube-hosted ad video [request="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-static {v1}, Lezp;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Lcno; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 161
    :catch_0
    move-exception v1

    .line 162
    iget-object v2, p1, Less;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x28

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Error loading vast ad [originalVideoId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 143
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lfoy;->b()Lfpc;

    move-result-object v1

    const/4 v2, 0x0

    .line 144
    iput-object v2, v1, Lfpc;->r:Lfrf;

    .line 145
    invoke-virtual {v1}, Lfpc;->a()Lfoy;
    :try_end_1
    .catch Lcno; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    .line 147
    :try_start_2
    iget-object v2, p0, Lbkr;->a:Lgml;

    .line 148
    iget-object v3, p1, Less;->f:Ljava/lang/String;

    iget-object v4, p1, Less;->e:Ljava/lang/String;

    .line 147
    invoke-virtual {v2, v3, v4, v1}, Lgml;->a(Ljava/lang/String;Ljava/lang/String;Lfoy;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    iget-object v2, v1, Lfoy;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 152
    iget-object v2, p0, Lbkr;->a:Lgml;

    iget-object v3, v1, Lfoy;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lgml;->j(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcno; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    move-object v0, v1

    .line 154
    goto/16 :goto_0

    .line 157
    :catch_1
    move-exception v1

    .line 158
    :try_start_3
    iget-object v2, p1, Less;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x27

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Error saving vast ad [originalVideoId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Lcno; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 164
    :catch_2
    move-exception v1

    .line 166
    iget-object v2, p1, Less;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x30

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Timeout error loading vast ad [originalVideoId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 165
    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method
