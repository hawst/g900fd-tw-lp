.class public final Lhmd;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;

.field private b:Z

.field private c:I

.field private d:[Lhmc;

.field private e:Z

.field private f:Lhgz;

.field private g:Z

.field private h:Lhgz;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v2, p0, Lhmd;->a:Lhgz;

    iput-boolean v1, p0, Lhmd;->b:Z

    iput v1, p0, Lhmd;->c:I

    sget-object v0, Lhmc;->a:[Lhmc;

    iput-object v0, p0, Lhmd;->d:[Lhmc;

    iput-boolean v1, p0, Lhmd;->e:Z

    iput-object v2, p0, Lhmd;->f:Lhgz;

    iput-boolean v1, p0, Lhmd;->g:Z

    iput-object v2, p0, Lhmd;->h:Lhgz;

    iput-boolean v1, p0, Lhmd;->i:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhmd;->a:Lhgz;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    iget-object v2, p0, Lhmd;->a:Lhgz;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-boolean v2, p0, Lhmd;->b:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-boolean v3, p0, Lhmd;->b:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Lhmd;->c:I

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget v3, p0, Lhmd;->c:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhmd;->d:[Lhmc;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhmd;->d:[Lhmc;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    if-eqz v4, :cond_2

    const/4 v5, 0x4

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-boolean v1, p0, Lhmd;->e:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-boolean v2, p0, Lhmd;->e:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhmd;->f:Lhgz;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lhmd;->f:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lhmd;->g:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-boolean v2, p0, Lhmd;->g:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhmd;->h:Lhgz;

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lhmd;->h:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Lhmd;->i:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-boolean v2, p0, Lhmd;->i:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lhmd;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhmd;->J:I

    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhmd;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhmd;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhmd;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhmd;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhmd;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhmd;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhmd;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_3

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4

    :cond_3
    iput v0, p0, Lhmd;->c:I

    goto :goto_0

    :cond_4
    iput v1, p0, Lhmd;->c:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhmd;->d:[Lhmc;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhmc;

    iget-object v3, p0, Lhmd;->d:[Lhmc;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lhmd;->d:[Lhmc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lhmd;->d:[Lhmc;

    :goto_2
    iget-object v2, p0, Lhmd;->d:[Lhmc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lhmd;->d:[Lhmc;

    new-instance v3, Lhmc;

    invoke-direct {v3}, Lhmc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhmd;->d:[Lhmc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lhmd;->d:[Lhmc;

    array-length v0, v0

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lhmd;->d:[Lhmc;

    new-instance v3, Lhmc;

    invoke-direct {v3}, Lhmc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhmd;->d:[Lhmc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhmd;->e:Z

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lhmd;->f:Lhgz;

    if-nez v0, :cond_8

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhmd;->f:Lhgz;

    :cond_8
    iget-object v0, p0, Lhmd;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhmd;->g:Z

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhmd;->h:Lhgz;

    if-nez v0, :cond_9

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhmd;->h:Lhgz;

    :cond_9
    iget-object v0, p0, Lhmd;->h:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhmd;->i:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhmd;->a:Lhgz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhmd;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-boolean v0, p0, Lhmd;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Lhmd;->b:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_1
    iget v0, p0, Lhmd;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lhmd;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_2
    iget-object v0, p0, Lhmd;->d:[Lhmc;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lhmd;->d:[Lhmc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    if-eqz v3, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lhmd;->e:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    iget-boolean v1, p0, Lhmd;->e:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_5
    iget-object v0, p0, Lhmd;->f:Lhgz;

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget-object v1, p0, Lhmd;->f:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_6
    iget-boolean v0, p0, Lhmd;->g:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    iget-boolean v1, p0, Lhmd;->g:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_7
    iget-object v0, p0, Lhmd;->h:Lhgz;

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    iget-object v1, p0, Lhmd;->h:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_8
    iget-boolean v0, p0, Lhmd;->i:Z

    if-eqz v0, :cond_9

    const/16 v0, 0x9

    iget-boolean v1, p0, Lhmd;->i:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_9
    iget-object v0, p0, Lhmd;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhmd;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhmd;

    iget-object v2, p0, Lhmd;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhmd;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-boolean v2, p0, Lhmd;->b:Z

    iget-boolean v3, p1, Lhmd;->b:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhmd;->c:I

    iget v3, p1, Lhmd;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhmd;->d:[Lhmc;

    iget-object v3, p1, Lhmd;->d:[Lhmc;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lhmd;->e:Z

    iget-boolean v3, p1, Lhmd;->e:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhmd;->f:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhmd;->f:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-boolean v2, p0, Lhmd;->g:Z

    iget-boolean v3, p1, Lhmd;->g:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhmd;->h:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhmd;->h:Lhgz;

    if-nez v2, :cond_3

    :goto_3
    iget-boolean v2, p0, Lhmd;->i:Z

    iget-boolean v3, p1, Lhmd;->i:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhmd;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhmd;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhmd;->a:Lhgz;

    iget-object v3, p1, Lhmd;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhmd;->f:Lhgz;

    iget-object v3, p1, Lhmd;->f:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhmd;->h:Lhgz;

    iget-object v3, p1, Lhmd;->h:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhmd;->I:Ljava/util/List;

    iget-object v3, p1, Lhmd;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhmd;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhmd;->b:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhmd;->c:I

    add-int/2addr v0, v4

    iget-object v4, p0, Lhmd;->d:[Lhmc;

    if-nez v4, :cond_3

    mul-int/lit8 v4, v0, 0x1f

    :cond_0
    mul-int/lit8 v4, v4, 0x1f

    iget-boolean v0, p0, Lhmd;->e:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhmd;->f:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhmd;->g:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhmd;->h:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lhmd;->i:Z

    if-eqz v4, :cond_9

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhmd;->I:Ljava/util/List;

    if-nez v2, :cond_a

    :goto_7
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lhmd;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    move v4, v0

    move v0, v1

    :goto_8
    iget-object v5, p0, Lhmd;->d:[Lhmc;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v5, v4, 0x1f

    iget-object v4, p0, Lhmd;->d:[Lhmc;

    aget-object v4, v4, v0

    if-nez v4, :cond_4

    move v4, v1

    :goto_9
    add-int/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_4
    iget-object v4, p0, Lhmd;->d:[Lhmc;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lhmc;->hashCode()I

    move-result v4

    goto :goto_9

    :cond_5
    move v0, v3

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lhmd;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lhmd;->h:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_9
    move v2, v3

    goto :goto_6

    :cond_a
    iget-object v1, p0, Lhmd;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_7
.end method
