.class public final Lhrv;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhrv;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field private e:Ljava/lang/String;

.field private f:F

.field private g:F

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:Ljava/lang/String;

.field private o:[B

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhrv;

    sput-object v0, Lhrv;->a:[Lhrv;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lhrv;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhrv;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhrv;->e:Ljava/lang/String;

    iput v1, p0, Lhrv;->d:I

    iput v2, p0, Lhrv;->f:F

    iput v2, p0, Lhrv;->g:F

    const-string v0, ""

    iput-object v0, p0, Lhrv;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhrv;->i:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhrv;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhrv;->k:Ljava/lang/String;

    iput v1, p0, Lhrv;->l:I

    iput v1, p0, Lhrv;->m:I

    const-string v0, ""

    iput-object v0, p0, Lhrv;->n:Ljava/lang/String;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhrv;->o:[B

    const-string v0, ""

    iput-object v0, p0, Lhrv;->p:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhrv;->q:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lhrv;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lhrv;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhrv;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Lhrv;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhrv;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x5

    iget-object v2, p0, Lhrv;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lhrv;->d:I

    if-eqz v1, :cond_3

    const/4 v1, 0x6

    iget v2, p0, Lhrv;->d:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lhrv;->f:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_4

    const/4 v1, 0x7

    iget v2, p0, Lhrv;->f:F

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lhrv;->g:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_5

    const/16 v1, 0x8

    iget v2, p0, Lhrv;->g:F

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhrv;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const/16 v1, 0x9

    iget-object v2, p0, Lhrv;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhrv;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const/16 v1, 0xa

    iget-object v2, p0, Lhrv;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lhrv;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const/16 v1, 0xb

    iget-object v2, p0, Lhrv;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lhrv;->k:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/16 v1, 0xc

    iget-object v2, p0, Lhrv;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lhrv;->l:I

    if-eqz v1, :cond_a

    const/16 v1, 0xd

    iget v2, p0, Lhrv;->l:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lhrv;->m:I

    if-eqz v1, :cond_b

    const/16 v1, 0xe

    iget v2, p0, Lhrv;->m:I

    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lhrv;->n:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const/16 v1, 0xf

    iget-object v2, p0, Lhrv;->n:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lhrv;->o:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_d

    const/16 v1, 0x10

    iget-object v2, p0, Lhrv;->o:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-object v1, p0, Lhrv;->p:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const/16 v1, 0x11

    iget-object v2, p0, Lhrv;->p:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget-object v1, p0, Lhrv;->q:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    const/16 v1, 0x12

    iget-object v2, p0, Lhrv;->q:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Lhrv;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhrv;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhrv;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhrv;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhrv;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lhrv;->d:I

    goto :goto_0

    :cond_3
    iput v2, p0, Lhrv;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhrv;->f:F

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhrv;->g:F

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->j:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-ne v0, v4, :cond_5

    :cond_4
    iput v0, p0, Lhrv;->l:I

    goto/16 :goto_0

    :cond_5
    iput v2, p0, Lhrv;->l:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhrv;->m:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhrv;->o:[B

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->p:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhrv;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x2a -> :sswitch_3
        0x30 -> :sswitch_4
        0x3d -> :sswitch_5
        0x45 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x62 -> :sswitch_a
        0x68 -> :sswitch_b
        0x70 -> :sswitch_c
        0x7a -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
        0x92 -> :sswitch_10
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lhrv;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lhrv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhrv;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    iget-object v1, p0, Lhrv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhrv;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x5

    iget-object v1, p0, Lhrv;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_2
    iget v0, p0, Lhrv;->d:I

    if-eqz v0, :cond_3

    const/4 v0, 0x6

    iget v1, p0, Lhrv;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_3
    iget v0, p0, Lhrv;->f:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_4

    const/4 v0, 0x7

    iget v1, p0, Lhrv;->f:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    :cond_4
    iget v0, p0, Lhrv;->g:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_5

    const/16 v0, 0x8

    iget v1, p0, Lhrv;->g:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    :cond_5
    iget-object v0, p0, Lhrv;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0x9

    iget-object v1, p0, Lhrv;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lhrv;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0xa

    iget-object v1, p0, Lhrv;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lhrv;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const/16 v0, 0xb

    iget-object v1, p0, Lhrv;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lhrv;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const/16 v0, 0xc

    iget-object v1, p0, Lhrv;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_9
    iget v0, p0, Lhrv;->l:I

    if-eqz v0, :cond_a

    const/16 v0, 0xd

    iget v1, p0, Lhrv;->l:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_a
    iget v0, p0, Lhrv;->m:I

    if-eqz v0, :cond_b

    const/16 v0, 0xe

    iget v1, p0, Lhrv;->m:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    :cond_b
    iget-object v0, p0, Lhrv;->n:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const/16 v0, 0xf

    iget-object v1, p0, Lhrv;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_c
    iget-object v0, p0, Lhrv;->o:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_d

    const/16 v0, 0x10

    iget-object v1, p0, Lhrv;->o:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_d
    iget-object v0, p0, Lhrv;->p:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    const/16 v0, 0x11

    iget-object v1, p0, Lhrv;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_e
    iget-object v0, p0, Lhrv;->q:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    const/16 v0, 0x12

    iget-object v1, p0, Lhrv;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_f
    iget-object v0, p0, Lhrv;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhrv;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhrv;

    iget-object v2, p0, Lhrv;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhrv;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhrv;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhrv;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhrv;->e:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhrv;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    iget v2, p0, Lhrv;->d:I

    iget v3, p1, Lhrv;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhrv;->f:F

    iget v3, p1, Lhrv;->f:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lhrv;->g:F

    iget v3, p1, Lhrv;->g:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-object v2, p0, Lhrv;->h:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhrv;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhrv;->i:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhrv;->i:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhrv;->j:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhrv;->j:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhrv;->k:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhrv;->k:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_7
    iget v2, p0, Lhrv;->l:I

    iget v3, p1, Lhrv;->l:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhrv;->m:I

    iget v3, p1, Lhrv;->m:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhrv;->n:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhrv;->n:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_8
    iget-object v2, p0, Lhrv;->o:[B

    iget-object v3, p1, Lhrv;->o:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhrv;->p:Ljava/lang/String;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhrv;->p:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_9
    iget-object v2, p0, Lhrv;->q:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhrv;->q:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_a
    iget-object v2, p0, Lhrv;->I:Ljava/util/List;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhrv;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhrv;->b:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhrv;->c:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhrv;->e:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhrv;->h:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhrv;->i:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhrv;->j:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhrv;->k:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhrv;->n:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhrv;->p:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_9

    :cond_d
    iget-object v2, p0, Lhrv;->q:Ljava/lang/String;

    iget-object v3, p1, Lhrv;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_a

    :cond_e
    iget-object v2, p0, Lhrv;->I:Ljava/util/List;

    iget-object v3, p1, Lhrv;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhrv;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhrv;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhrv;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhrv;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhrv;->f:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhrv;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhrv;->h:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhrv;->i:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhrv;->j:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhrv;->k:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhrv;->l:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhrv;->m:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhrv;->n:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    iget-object v2, p0, Lhrv;->o:[B

    if-nez v2, :cond_9

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhrv;->p:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhrv;->q:Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhrv;->I:Ljava/util/List;

    if-nez v2, :cond_c

    :goto_a
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lhrv;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhrv;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhrv;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhrv;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lhrv;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lhrv;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_7
    iget-object v0, p0, Lhrv;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lhrv;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_7

    :cond_9
    move v2, v0

    move v0, v1

    :goto_b
    iget-object v3, p0, Lhrv;->o:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhrv;->o:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_a
    iget-object v0, p0, Lhrv;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    :cond_b
    iget-object v0, p0, Lhrv;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    :cond_c
    iget-object v1, p0, Lhrv;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_a
.end method
