.class public Ldtm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ldsp;

.field public final b:Landroid/net/Uri;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Ldtj;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/Integer;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    new-instance v0, Ldtn;

    invoke-direct {v0}, Ldtn;-><init>()V

    sput-object v0, Ldtm;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ldto;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iget-object v2, p1, Ldto;->c:Ljava/lang/String;

    iput-object v2, p0, Ldtm;->c:Ljava/lang/String;

    .line 130
    iget-object v2, p1, Ldto;->e:Ljava/lang/String;

    iput-object v2, p0, Ldtm;->h:Ljava/lang/String;

    .line 131
    iget-object v2, p1, Ldto;->f:Ljava/lang/String;

    iput-object v2, p0, Ldtm;->i:Ljava/lang/String;

    .line 132
    iget-object v2, p1, Ldto;->g:Ldtj;

    iput-object v2, p0, Ldtm;->e:Ldtj;

    .line 133
    iget-object v2, p1, Ldto;->b:Landroid/net/Uri;

    iput-object v2, p0, Ldtm;->b:Landroid/net/Uri;

    .line 134
    iget-boolean v2, p1, Ldto;->d:Z

    iput-boolean v2, p0, Ldtm;->d:Z

    .line 135
    iget-object v2, p1, Ldto;->a:Ldsp;

    iput-object v2, p0, Ldtm;->a:Ldsp;

    .line 136
    iget-object v2, p1, Ldto;->h:Ljava/lang/String;

    iput-object v2, p0, Ldtm;->f:Ljava/lang/String;

    .line 137
    iget-object v2, p1, Ldto;->i:Ljava/lang/Integer;

    iput-object v2, p0, Ldtm;->g:Ljava/lang/Integer;

    .line 139
    iget-boolean v2, p0, Ldtm;->d:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldtm;->b:Landroid/net/Uri;

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 141
    return-void

    :cond_1
    move v0, v1

    .line 139
    goto :goto_0

    :cond_2
    iget-object v2, p0, Ldtm;->b:Landroid/net/Uri;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Ldtm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldtm;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Ldtm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldtm;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Ldtm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldtm;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Ldtm;)Ldtj;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldtm;->e:Ldtj;

    return-object v0
.end method

.method static synthetic e(Ldtm;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldtm;->b:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic f(Ldtm;)Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Ldtm;->d:Z

    return v0
.end method

.method static synthetic g(Ldtm;)Ldsp;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldtm;->a:Ldsp;

    return-object v0
.end method

.method static synthetic h(Ldtm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldtm;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Ldtm;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldtm;->g:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ldtm;
    .locals 1

    .prologue
    .line 290
    if-nez p1, :cond_0

    .line 294
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ldto;

    invoke-direct {v0, p0}, Ldto;-><init>(Ldtm;)V

    iput-object p1, v0, Ldto;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ldto;->a()Ldtm;

    move-result-object p0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Ldtm;->a:Ldsp;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 220
    if-ne p0, p1, :cond_1

    .line 282
    :cond_0
    :goto_0
    return v0

    .line 223
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 224
    goto :goto_0

    .line 226
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 227
    goto :goto_0

    .line 229
    :cond_3
    check-cast p1, Ldtm;

    .line 230
    iget-object v2, p0, Ldtm;->b:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 231
    iget-object v2, p1, Ldtm;->b:Landroid/net/Uri;

    if-eqz v2, :cond_5

    move v0, v1

    .line 232
    goto :goto_0

    .line 234
    :cond_4
    iget-object v2, p0, Ldtm;->b:Landroid/net/Uri;

    iget-object v3, p1, Ldtm;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 235
    goto :goto_0

    .line 237
    :cond_5
    iget-object v2, p0, Ldtm;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 238
    iget-object v2, p1, Ldtm;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 239
    goto :goto_0

    .line 241
    :cond_6
    iget-object v2, p0, Ldtm;->c:Ljava/lang/String;

    iget-object v3, p1, Ldtm;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 242
    goto :goto_0

    .line 244
    :cond_7
    iget-boolean v2, p0, Ldtm;->d:Z

    iget-boolean v3, p1, Ldtm;->d:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 245
    goto :goto_0

    .line 247
    :cond_8
    iget-object v2, p0, Ldtm;->h:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 248
    iget-object v2, p1, Ldtm;->h:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 249
    goto :goto_0

    .line 251
    :cond_9
    iget-object v2, p0, Ldtm;->h:Ljava/lang/String;

    iget-object v3, p1, Ldtm;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 252
    goto :goto_0

    .line 254
    :cond_a
    iget-object v2, p0, Ldtm;->i:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 255
    iget-object v2, p1, Ldtm;->i:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 256
    goto :goto_0

    .line 258
    :cond_b
    iget-object v2, p0, Ldtm;->i:Ljava/lang/String;

    iget-object v3, p1, Ldtm;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 259
    goto :goto_0

    .line 261
    :cond_c
    iget-object v2, p0, Ldtm;->e:Ldtj;

    if-nez v2, :cond_d

    .line 262
    iget-object v2, p1, Ldtm;->e:Ldtj;

    if-eqz v2, :cond_e

    move v0, v1

    .line 263
    goto :goto_0

    .line 265
    :cond_d
    iget-object v2, p0, Ldtm;->e:Ldtj;

    iget-object v3, p1, Ldtm;->e:Ldtj;

    invoke-virtual {v2, v3}, Ldtj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 266
    goto/16 :goto_0

    .line 268
    :cond_e
    iget-object v2, p0, Ldtm;->f:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 269
    iget-object v2, p1, Ldtm;->f:Ljava/lang/String;

    if-eqz v2, :cond_10

    move v0, v1

    .line 270
    goto/16 :goto_0

    .line 272
    :cond_f
    iget-object v2, p0, Ldtm;->f:Ljava/lang/String;

    iget-object v3, p1, Ldtm;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 273
    goto/16 :goto_0

    .line 275
    :cond_10
    iget-object v2, p0, Ldtm;->g:Ljava/lang/Integer;

    if-nez v2, :cond_11

    .line 276
    iget-object v2, p1, Ldtm;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    move v0, v1

    .line 277
    goto/16 :goto_0

    .line 279
    :cond_11
    iget-object v2, p0, Ldtm;->g:Ljava/lang/Integer;

    iget-object v3, p1, Ldtm;->g:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 280
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 205
    iget-object v0, p0, Ldtm;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 208
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldtm;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 209
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Ldtm;->d:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    add-int/2addr v0, v2

    .line 210
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldtm;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 211
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldtm;->i:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 212
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldtm;->e:Ldtj;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 213
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldtm;->f:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 214
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ldtm;->g:Ljava/lang/Integer;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 215
    return v0

    .line 205
    :cond_0
    iget-object v0, p0, Ldtm;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_0

    .line 208
    :cond_1
    iget-object v0, p0, Ldtm;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 209
    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2

    .line 210
    :cond_3
    iget-object v0, p0, Ldtm;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 211
    :cond_4
    iget-object v0, p0, Ldtm;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 212
    :cond_5
    iget-object v0, p0, Ldtm;->e:Ldtj;

    invoke-virtual {v0}, Ldtj;->hashCode()I

    move-result v0

    goto :goto_5

    .line 213
    :cond_6
    iget-object v0, p0, Ldtm;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 214
    :cond_7
    iget-object v1, p0, Ldtm;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 299
    iget-object v0, p0, Ldtm;->c:Ljava/lang/String;

    iget-object v1, p0, Ldtm;->e:Ldtj;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "YouTubeDevice [deviceName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ssdpId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Ldtm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Ldtm;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Ldtm;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Ldtm;->e:Ldtj;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 313
    iget-object v0, p0, Ldtm;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 314
    iget-boolean v0, p0, Ldtm;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 315
    iget-object v0, p0, Ldtm;->a:Ldsp;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 316
    iget-object v0, p0, Ldtm;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Ldtm;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 318
    return-void

    .line 314
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
