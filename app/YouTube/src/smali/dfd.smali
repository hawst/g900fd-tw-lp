.class public final enum Ldfd;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static enum a:Ldfd;

.field private static enum b:Ldfd;

.field private static enum c:Ldfd;

.field private static enum d:Ldfd;

.field private static enum e:Ldfd;

.field private static h:[Ljava/lang/String;

.field private static i:[Ljava/lang/String;

.field private static final synthetic j:[Ldfd;


# instance fields
.field private f:I

.field private g:F


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 154
    new-instance v0, Ldfd;

    const-string v1, "VERY_SMALL"

    const v2, 0x7f09014c

    const/high16 v3, 0x3e800000    # 0.25f

    invoke-direct {v0, v1, v4, v2, v3}, Ldfd;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Ldfd;->a:Ldfd;

    .line 155
    new-instance v0, Ldfd;

    const-string v1, "SMALL"

    const v2, 0x7f09014d

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {v0, v1, v5, v2, v3}, Ldfd;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Ldfd;->b:Ldfd;

    .line 156
    new-instance v0, Ldfd;

    const-string v1, "NORMAL"

    const v2, 0x7f09014e

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v6, v2, v3}, Ldfd;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Ldfd;->c:Ldfd;

    .line 157
    new-instance v0, Ldfd;

    const-string v1, "LARGE"

    const v2, 0x7f09014f

    const/high16 v3, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1, v7, v2, v3}, Ldfd;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Ldfd;->d:Ldfd;

    .line 158
    new-instance v0, Ldfd;

    const-string v1, "VERY_LARGE"

    const v2, 0x7f090150

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v0, v1, v8, v2, v3}, Ldfd;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Ldfd;->e:Ldfd;

    .line 153
    const/4 v0, 0x5

    new-array v0, v0, [Ldfd;

    sget-object v1, Ldfd;->a:Ldfd;

    aput-object v1, v0, v4

    sget-object v1, Ldfd;->b:Ldfd;

    aput-object v1, v0, v5

    sget-object v1, Ldfd;->c:Ldfd;

    aput-object v1, v0, v6

    sget-object v1, Ldfd;->d:Ldfd;

    aput-object v1, v0, v7

    sget-object v1, Ldfd;->e:Ldfd;

    aput-object v1, v0, v8

    sput-object v0, Ldfd;->j:[Ldfd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIF)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 166
    iput p3, p0, Ldfd;->f:I

    .line 167
    iput p4, p0, Ldfd;->g:F

    .line 168
    return-void
.end method

.method public static a()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 184
    sget-object v0, Ldfd;->i:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 185
    invoke-static {}, Ldfd;->values()[Ldfd;

    move-result-object v1

    .line 186
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Ldfd;->i:[Ljava/lang/String;

    .line 187
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 188
    sget-object v2, Ldfd;->i:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Ldfd;->g:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 191
    :cond_0
    sget-object v0, Ldfd;->i:[Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 173
    sget-object v0, Ldfd;->h:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 174
    invoke-static {}, Ldfd;->values()[Ldfd;

    move-result-object v1

    .line 175
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Ldfd;->h:[Ljava/lang/String;

    .line 176
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 177
    sget-object v2, Ldfd;->h:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Ldfd;->f:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_0
    sget-object v0, Ldfd;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public static b()F
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Ldfd;->values()[Ldfd;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iget v0, v0, Ldfd;->g:F

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldfd;
    .locals 1

    .prologue
    .line 153
    const-class v0, Ldfd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldfd;

    return-object v0
.end method

.method public static values()[Ldfd;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Ldfd;->j:[Ldfd;

    invoke-virtual {v0}, [Ldfd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldfd;

    return-object v0
.end method
