.class public Lchc;
.super Lfsa;
.source "SourceFile"


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/view/View;

.field public final c:Landroid/view/View;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/LinearLayout;

.field public final f:Landroid/view/View;

.field public final g:Landroid/content/res/Resources;

.field public final h:Lboi;

.field public i:Lfmd;

.field private j:Landroid/view/View;

.field private final m:Landroid/widget/TextView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/TextView;

.field private final p:Lfvi;

.field private final q:Lfvi;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/view/View;

.field private final t:Landroid/content/Context;

.field private final u:Landroid/text/SpannableStringBuilder;

.field private final v:Landroid/text/style/StyleSpan;

.field private final w:Lerv;

.field private final x:Landroid/view/View$OnClickListener;

.field private final y:Landroid/view/View$OnClickListener;

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyc;ILeyp;Lfhz;Lboi;Lerv;Lfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0, p8, p9}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 84
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lchc;->t:Landroid/content/Context;

    .line 85
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lchc;->h:Lboi;

    .line 87
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lerv;

    iput-object v0, p0, Lchc;->w:Lerv;

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lchc;->g:Landroid/content/res/Resources;

    .line 90
    const/4 v0, 0x0

    invoke-static {p1, p3, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lchc;->a:Landroid/view/View;

    .line 91
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchc;->m:Landroid/widget/TextView;

    .line 92
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f0800ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchc;->o:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f08012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchc;->n:Landroid/widget/TextView;

    .line 94
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 95
    new-instance v1, Lfvi;

    invoke-direct {v1, p4, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lchc;->p:Lfvi;

    .line 96
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f08012c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lchc;->b:Landroid/view/View;

    .line 97
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f080106

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lchc;->j:Landroid/view/View;

    .line 98
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f080269

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lchc;->c:Landroid/view/View;

    .line 99
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lchc;->f:Landroid/view/View;

    .line 100
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f0800c0

    .line 101
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 102
    new-instance v1, Lfvi;

    invoke-direct {v1, p4, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lchc;->q:Lfvi;

    .line 103
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f0800c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchc;->r:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f0800c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    .line 105
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f080268

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lchc;->e:Landroid/widget/LinearLayout;

    .line 106
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    const v1, 0x7f080267

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lchc;->s:Landroid/view/View;

    .line 108
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lchc;->u:Landroid/text/SpannableStringBuilder;

    .line 109
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object v0, p0, Lchc;->v:Landroid/text/style/StyleSpan;

    .line 111
    new-instance v0, Lchd;

    invoke-direct {v0, p0, p7, p5}, Lchd;-><init>(Lchc;Lerv;Lfhz;)V

    iput-object v0, p0, Lchc;->x:Landroid/view/View$OnClickListener;

    .line 121
    new-instance v0, Lche;

    invoke-direct {v0, p0, p5}, Lche;-><init>(Lchc;Lfhz;)V

    iput-object v0, p0, Lchc;->y:Landroid/view/View$OnClickListener;

    .line 130
    iget-object v0, p0, Lchc;->s:Landroid/view/View;

    new-instance v1, Lchf;

    invoke-direct {v1, p0, p5}, Lchf;-><init>(Lchc;Lfhz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    return-void
.end method

.method private a(Lfmd;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 142
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmd;

    iput-object v0, p0, Lchc;->i:Lfmd;

    .line 143
    iget-object v0, p0, Lchc;->w:Lerv;

    iget-object v1, p1, Lfmd;->a:Lhtd;

    iget-object v1, v1, Lhtd;->a:Ljava/lang/String;

    iget-object v2, p1, Lfmd;->f:Ljava/util/List;

    if-nez v2, :cond_0

    iget-object v2, p1, Lfmd;->a:Lhtd;

    iget-object v2, v2, Lhtd;->k:[Ljava/lang/String;

    invoke-static {v2}, La;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p1, Lfmd;->f:Ljava/util/List;

    :cond_0
    iget-object v2, p1, Lfmd;->f:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lerv;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 145
    invoke-direct {p0}, Lchc;->b()I

    move-result v0

    iget v1, p0, Lchc;->z:I

    if-eq v0, v1, :cond_1

    .line 146
    invoke-direct {p0}, Lchc;->b()I

    move-result v0

    iput v0, p0, Lchc;->z:I

    .line 147
    invoke-direct {p0}, Lchc;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lchc;->a(I)V

    .line 150
    :cond_1
    iget-object v0, p0, Lchc;->m:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfmd;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lchc;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 153
    invoke-virtual {p1}, Lfmd;->f()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 154
    iget-object v0, p0, Lchc;->r:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfmd;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lchc;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    :goto_0
    invoke-virtual {p1}, Lfmd;->g()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 161
    iget-object v0, p0, Lchc;->n:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfmd;->g()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lchc;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 167
    :goto_1
    iget-object v0, p1, Lfmd;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    iget-object v0, p1, Lfmd;->a:Lhtd;

    iget-object v0, v0, Lhtd;->e:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p1, Lfmd;->c:Ljava/lang/CharSequence;

    :cond_2
    iget-object v0, p1, Lfmd;->c:Ljava/lang/CharSequence;

    if-eqz v0, :cond_9

    .line 168
    iget-object v0, p0, Lchc;->u:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 169
    iget-object v0, p0, Lchc;->u:Landroid/text/SpannableStringBuilder;

    iget-object v1, p1, Lfmd;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p1, Lfmd;->a:Lhtd;

    iget-object v1, v1, Lhtd;->f:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p1, Lfmd;->d:Ljava/lang/CharSequence;

    :cond_3
    iget-object v1, p1, Lfmd;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 170
    iget-object v0, p0, Lchc;->u:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lchc;->v:Landroid/text/style/StyleSpan;

    iget-object v2, p0, Lchc;->u:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 171
    iget-object v0, p0, Lchc;->o:Landroid/widget/TextView;

    iget-object v1, p0, Lchc;->u:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v0, p0, Lchc;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    :goto_2
    iget-object v0, p0, Lchc;->i:Lfmd;

    invoke-virtual {v0}, Lfmd;->h()Lhog;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lchc;->i:Lfmd;

    invoke-virtual {v0}, Lfmd;->i()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lchc;->i:Lfmd;

    invoke-virtual {v1}, Lfmd;->i()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :cond_4
    :goto_3
    iget-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lchc;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    invoke-static {v0, v4}, Lchc;->a(Landroid/view/View;I)V

    .line 178
    :goto_4
    iget-object v0, p0, Lchc;->p:Lfvi;

    iget-object v1, p1, Lfmd;->b:Lfnc;

    if-nez v1, :cond_5

    new-instance v1, Lfnc;

    iget-object v2, p1, Lfmd;->a:Lhtd;

    iget-object v2, v2, Lhtd;->b:Lhxf;

    invoke-direct {v1, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, p1, Lfmd;->b:Lfnc;

    :cond_5
    iget-object v1, p1, Lfmd;->b:Lfnc;

    invoke-virtual {v0, v1, v6}, Lfvi;->a(Lfnc;Leyo;)V

    .line 179
    iget-object v0, p0, Lchc;->q:Lfvi;

    iget-object v1, p1, Lfmd;->e:Lfnc;

    if-nez v1, :cond_6

    new-instance v1, Lfnc;

    iget-object v2, p1, Lfmd;->a:Lhtd;

    iget-object v2, v2, Lhtd;->g:Lhxf;

    invoke-direct {v1, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, p1, Lfmd;->e:Lfnc;

    :cond_6
    iget-object v1, p1, Lfmd;->e:Lfnc;

    invoke-virtual {v0, v1, v6}, Lfvi;->a(Lfnc;Leyo;)V

    .line 181
    iget-object v0, p0, Lchc;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 183
    invoke-virtual {p0}, Lchc;->a()V

    .line 185
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    iget-object v1, p0, Lchc;->a:Landroid/view/View;

    .line 186
    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lchc;->g:Landroid/content/res/Resources;

    const v3, 0x7f0a0096

    .line 187
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lchc;->a:Landroid/view/View;

    .line 188
    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lchc;->a:Landroid/view/View;

    .line 189
    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 185
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 191
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    iget-object v1, p0, Lchc;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    iget-object v0, p0, Lchc;->a:Landroid/view/View;

    return-object v0

    .line 157
    :cond_7
    iget-object v0, p0, Lchc;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 164
    :cond_8
    iget-object v0, p0, Lchc;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 174
    :cond_9
    iget-object v0, p0, Lchc;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 177
    :cond_a
    iget-object v0, p0, Lchc;->i:Lfmd;

    iget-object v0, v0, Lfmd;->h:Liag;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    const v1, 0x7f020037

    invoke-virtual {v0, v4, v4, v1, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_3

    :cond_b
    iget-object v0, p0, Lchc;->i:Lfmd;

    iget-object v0, v0, Lfmd;->i:Lham;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    const v1, 0x7f090306

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_3

    :cond_c
    iget-object v0, p0, Lchc;->d:Landroid/widget/TextView;

    invoke-static {v0, v5}, Lchc;->a(Landroid/view/View;I)V

    goto/16 :goto_4
.end method

.method private static a(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 271
    if-eqz p0, :cond_0

    .line 272
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 274
    :cond_0
    return-void
.end method

.method private b()I
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lchc;->t:Landroid/content/Context;

    invoke-static {v0}, La;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const/4 v0, 0x2

    .line 206
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lchc;->g:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lfmd;

    invoke-direct {p0, p2}, Lchc;->a(Lfmd;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lfmd;

    invoke-direct {p0, p2}, Lchc;->a(Lfmd;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lchc;->h:Lboi;

    iget-object v1, p0, Lchc;->f:Landroid/view/View;

    iget-object v2, p0, Lchc;->i:Lfmd;

    invoke-static {v0, v1, v2}, La;->a(Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 200
    return-void
.end method

.method protected a(I)V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 210
    iget-object v0, p0, Lchc;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 211
    iget-object v1, p0, Lchc;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 213
    iget-object v2, p0, Lchc;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lchc;->g:Landroid/content/res/Resources;

    const v4, 0x7f0a00b1

    .line 214
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 213
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 216
    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    .line 217
    iget-object v2, p0, Lchc;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 218
    iget-object v2, p0, Lchc;->b:Landroid/view/View;

    iget-object v3, p0, Lchc;->g:Landroid/content/res/Resources;

    const v4, 0x7f0a00dc

    .line 219
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lchc;->b:Landroid/view/View;

    .line 220
    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lchc;->g:Landroid/content/res/Resources;

    const v6, 0x7f0a00dd

    .line 221
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iget-object v6, p0, Lchc;->g:Landroid/content/res/Resources;

    const v7, 0x7f0a00db

    .line 222
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    .line 218
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 223
    iget-object v2, p0, Lchc;->c:Landroid/view/View;

    iget-object v3, p0, Lchc;->c:Landroid/view/View;

    .line 224
    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lchc;->c:Landroid/view/View;

    .line 226
    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lchc;->c:Landroid/view/View;

    .line 227
    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    .line 223
    invoke-virtual {v2, v3, v8, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 228
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 229
    iget-object v2, p0, Lchc;->g:Landroid/content/res/Resources;

    const v3, 0x7f0b0020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 230
    iput v8, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 231
    iget-object v0, p0, Lchc;->g:Landroid/content/res/Resources;

    const v2, 0x7f0b0021

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 232
    iget-object v0, p0, Lchc;->j:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 248
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v2, p0, Lchc;->e:Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 235
    iget-object v2, p0, Lchc;->b:Landroid/view/View;

    iget-object v3, p0, Lchc;->b:Landroid/view/View;

    .line 236
    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    .line 235
    invoke-virtual {v2, v8, v3, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 237
    iget-object v2, p0, Lchc;->c:Landroid/view/View;

    iget-object v3, p0, Lchc;->c:Landroid/view/View;

    .line 238
    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lchc;->g:Landroid/content/res/Resources;

    const v5, 0x7f0a00da

    .line 239
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iget-object v5, p0, Lchc;->c:Landroid/view/View;

    .line 240
    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lchc;->c:Landroid/view/View;

    .line 241
    invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    .line 237
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 242
    iput v9, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 243
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 244
    iput v9, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 245
    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 246
    iget-object v0, p0, Lchc;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
