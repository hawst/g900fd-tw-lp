.class public final Lbnv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lfhz;

.field private final b:Landroid/widget/TextView;

.field private c:Lfit;


# direct methods
.method public constructor <init>(Lfhz;Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lbnv;->a:Lfhz;

    .line 27
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnv;->b:Landroid/widget/TextView;

    .line 29
    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Lfit;)V
    .locals 2

    .prologue
    .line 33
    iput-object p1, p0, Lbnv;->c:Lfit;

    .line 34
    if-nez p1, :cond_0

    .line 35
    iget-object v0, p0, Lbnv;->b:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 37
    :cond_0
    iget-object v0, p0, Lbnv;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lbnv;->c:Lfit;

    invoke-virtual {v1}, Lfit;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lbnv;->c:Lfit;

    if-nez v0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    iget-object v0, p0, Lbnv;->c:Lfit;

    iget-object v0, v0, Lfit;->a:Lhbs;

    iget-object v0, v0, Lhbs;->c:Lhog;

    if-eqz v0, :cond_2

    .line 46
    iget-object v0, p0, Lbnv;->a:Lfhz;

    iget-object v1, p0, Lbnv;->c:Lfit;

    iget-object v1, v1, Lfit;->a:Lhbs;

    iget-object v1, v1, Lhbs;->c:Lhog;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :cond_2
    iget-object v0, p0, Lbnv;->c:Lfit;

    iget-object v0, v0, Lfit;->a:Lhbs;

    iget-object v0, v0, Lhbs;->b:Lhut;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lbnv;->a:Lfhz;

    iget-object v1, p0, Lbnv;->c:Lfit;

    iget-object v1, v1, Lfit;->a:Lhbs;

    iget-object v1, v1, Lhbs;->b:Lhut;

    iget-object v2, p0, Lbnv;->c:Lfit;

    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhut;Ljava/lang/Object;)V

    goto :goto_0
.end method
