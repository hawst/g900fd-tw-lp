.class public final Lcjl;
.super Lcjh;
.source "SourceFile"


# instance fields
.field final c:Lfhz;

.field d:Lhut;

.field private final e:Levn;

.field private final f:Lcjm;


# direct methods
.method public constructor <init>(Lfhz;Levn;Lfis;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Lcjh;-><init>()V

    .line 40
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcjl;->c:Lfhz;

    .line 41
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcjl;->e:Levn;

    .line 43
    invoke-virtual {p3}, Lfis;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmz;

    invoke-virtual {v0}, Lfmz;->c()Lfmi;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lfmz;->c()Lfmi;

    move-result-object v0

    invoke-virtual {v0}, Lfmi;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, Lfkj;

    if-eqz v4, :cond_1

    check-cast v0, Lfkj;

    invoke-virtual {v0}, Lfkj;->b()Lfkk;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v0, v4, Lfkk;->a:Lhkt;

    iget-object v0, v0, Lhkt;->b:[Lhut;

    if-eqz v0, :cond_3

    iget-object v0, v4, Lfkk;->a:Lhkt;

    iget-object v0, v0, Lhkt;->b:[Lhut;

    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, v4, Lfkk;->a:Lhkt;

    iget-object v4, v0, Lhkt;->b:[Lhut;

    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    iget-object v7, v6, Lhut;->d:Lhck;

    if-eqz v7, :cond_4

    iput-object v6, p0, Lcjl;->d:Lhut;

    .line 45
    :cond_2
    iget-object v0, p0, Lcjl;->d:Lhut;

    if-eqz v0, :cond_5

    new-instance v0, Lcjm;

    invoke-direct {v0, p0}, Lcjm;-><init>(Lcjl;)V

    :goto_2
    iput-object v0, p0, Lcjl;->f:Lcjm;

    .line 47
    return-void

    :cond_3
    move v0, v1

    .line 43
    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 45
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcjl;->e:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 74
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcjl;->e:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method public final c()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcjl;->f:Lcjm;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcjl;->f:Lcjm;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final handleServiceResponseClearTabEvent(Lfmj;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 91
    iget-object v0, p1, Lfmk;->a:Lhut;

    iget-object v1, p0, Lcjl;->d:Lhut;

    if-ne v0, v1, :cond_0

    .line 92
    iget-object v0, p0, Lcjh;->b:Lcji;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcjh;->b:Lcji;

    invoke-interface {v0}, Lcji;->A()V

    .line 95
    :cond_0
    return-void
.end method
