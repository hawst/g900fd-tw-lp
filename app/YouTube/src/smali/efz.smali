.class public final Lefz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefc;


# instance fields
.field private final a:Lefc;

.field private final b:Lefb;


# direct methods
.method public constructor <init>(Lefc;Lefb;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, La;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefc;

    iput-object v0, p0, Lefz;->a:Lefc;

    .line 24
    invoke-static {p2}, La;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefb;

    iput-object v0, p0, Lefz;->b:Lefb;

    .line 25
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lefz;->a:Lefc;

    invoke-interface {v0, p1, p2, p3}, Lefc;->a([BII)I

    move-result v0

    .line 42
    if-lez v0, :cond_0

    .line 44
    iget-object v1, p0, Lefz;->b:Lefb;

    invoke-interface {v1, p1, p2, v0}, Lefb;->a([BII)V

    .line 46
    :cond_0
    return v0
.end method

.method public final a(Lefg;)J
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    .line 29
    iget-object v0, p0, Lefz;->a:Lefc;

    invoke-interface {v0, p1}, Lefc;->a(Lefg;)J

    move-result-wide v4

    .line 30
    iget-wide v0, p1, Lefg;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    cmp-long v0, v4, v2

    if-eqz v0, :cond_0

    .line 32
    new-instance v0, Lefg;

    iget-object v1, p1, Lefg;->a:Landroid/net/Uri;

    iget-wide v2, p1, Lefg;->c:J

    iget-object v6, p1, Lefg;->f:Ljava/lang/String;

    iget-wide v7, p1, Lefg;->d:J

    iget-boolean v9, p1, Lefg;->b:Z

    invoke-direct/range {v0 .. v9}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;JZ)V

    move-object p1, v0

    .line 35
    :cond_0
    iget-object v0, p0, Lefz;->b:Lefb;

    invoke-interface {v0, p1}, Lefb;->a(Lefg;)Lefb;

    .line 36
    return-wide v4
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lefz;->a:Lefc;

    invoke-interface {v0}, Lefc;->a()V

    .line 52
    iget-object v0, p0, Lefz;->b:Lefb;

    invoke-interface {v0}, Lefb;->a()V

    .line 53
    return-void
.end method
