.class public final Lfom;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Licn;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Lfnc;

.field public e:Lfnc;

.field public f:Lfok;

.field public g:Ljava/util/Currency;

.field public h:Ljava/lang/String;

.field public i:J

.field public j:Ljava/lang/CharSequence;

.field public k:Ljava/lang/CharSequence;

.field public l:Ljava/lang/CharSequence;

.field public m:Ljava/lang/CharSequence;

.field public n:Ljava/lang/CharSequence;

.field private o:Lfnc;

.field private p:Lfoj;

.field private q:Lfoi;


# direct methods
.method public constructor <init>(Licn;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lfom;->a:Licn;

    .line 40
    iget-object v0, p1, Licn;->e:Lhxm;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p1, Licn;->e:Lhxm;

    iget-wide v0, v0, Lhxm;->a:J

    invoke-virtual {p0, v0, v1}, Lfom;->a(J)V

    .line 43
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lfnc;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lfom;->o:Lfnc;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfom;->a:Licn;

    iget-object v1, v1, Licn;->c:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfom;->o:Lfnc;

    .line 81
    :cond_0
    iget-object v0, p0, Lfom;->o:Lfnc;

    return-object v0
.end method

.method public a(J)V
    .locals 7

    .prologue
    .line 156
    iget-object v0, p0, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->e:Lhxm;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->e:Lhxm;

    iget-wide v0, v0, Lhxm;->c:J

    .line 159
    div-long v2, p1, v0

    long-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    long-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 160
    iget-object v2, p0, Lfom;->a:Licn;

    iget-object v2, v2, Licn;->e:Lhxm;

    iget-wide v2, v2, Lhxm;->d:J

    iget-object v4, p0, Lfom;->a:Licn;

    iget-object v4, v4, Licn;->e:Lhxm;

    iget-wide v4, v4, Lhxm;->e:J

    .line 162
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 160
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lfom;->i:J

    .line 164
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 226
    if-eqz p1, :cond_0

    .line 227
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfom;->h:Ljava/lang/String;

    .line 231
    :goto_0
    return-void

    .line 229
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfom;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b()D
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->e:Lhxm;

    if-nez v0, :cond_0

    .line 86
    const-wide/16 v0, 0x0

    .line 88
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->e:Lhxm;

    iget-wide v0, v0, Lhxm;->c:J

    long-to-double v0, v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method public final c()D
    .locals 4

    .prologue
    .line 95
    iget-wide v0, p0, Lfom;->i:J

    long-to-double v0, v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public final d()Lfoj;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lfom;->p:Lfoj;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->g:Lhdd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->g:Lhdd;

    iget-object v0, v0, Lhdd;->f:Lhxl;

    if-eqz v0, :cond_0

    .line 124
    new-instance v0, Lfoj;

    iget-object v1, p0, Lfom;->a:Licn;

    iget-object v1, v1, Licn;->g:Lhdd;

    iget-object v1, v1, Lhdd;->f:Lhxl;

    invoke-direct {v0, v1}, Lfoj;-><init>(Lhxl;)V

    iput-object v0, p0, Lfom;->p:Lfoj;

    .line 126
    :cond_0
    iget-object v0, p0, Lfom;->p:Lfoj;

    return-object v0
.end method

.method public final e()Lfoi;
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lfom;->q:Lfoi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->g:Lhdd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->g:Lhdd;

    iget-object v0, v0, Lhdd;->g:Lhxk;

    if-eqz v0, :cond_0

    .line 132
    new-instance v0, Lfoi;

    iget-object v1, p0, Lfom;->a:Licn;

    iget-object v1, v1, Licn;->g:Lhdd;

    iget-object v1, v1, Lhdd;->g:Lhxk;

    invoke-direct {v0, v1}, Lfoi;-><init>(Lhxk;)V

    iput-object v0, p0, Lfom;->q:Lfoi;

    .line 135
    :cond_0
    iget-object v0, p0, Lfom;->q:Lfoi;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lfom;->a:Licn;

    iget-object v0, v0, Licn;->g:Lhdd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lfom;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
