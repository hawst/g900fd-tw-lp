.class public final Lhzs;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhgz;

.field public c:Lhgz;

.field public d:Z

.field public e:Z

.field public f:Lhgz;

.field public g:Lhll;

.field public h:[Lhbi;

.field public i:[B

.field public j:Lhgz;

.field public k:Lhzt;

.field private l:Lhgz;

.field private m:Lhgz;

.field private n:Lhgz;

.field private o:Lhgz;

.field private p:I

.field private q:Ljava/lang/String;

.field private r:Lhgz;

.field private s:Ljava/lang/String;

.field private t:I

.field private u:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Lhzs;->a:Lhgz;

    iput-object v1, p0, Lhzs;->b:Lhgz;

    iput-object v1, p0, Lhzs;->l:Lhgz;

    iput-object v1, p0, Lhzs;->m:Lhgz;

    iput-object v1, p0, Lhzs;->c:Lhgz;

    iput-object v1, p0, Lhzs;->n:Lhgz;

    iput-object v1, p0, Lhzs;->o:Lhgz;

    iput-boolean v2, p0, Lhzs;->d:Z

    iput-boolean v2, p0, Lhzs;->e:Z

    iput-object v1, p0, Lhzs;->f:Lhgz;

    iput v2, p0, Lhzs;->p:I

    const-string v0, ""

    iput-object v0, p0, Lhzs;->q:Ljava/lang/String;

    iput-object v1, p0, Lhzs;->g:Lhll;

    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lhzs;->h:[Lhbi;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhzs;->i:[B

    iput-object v1, p0, Lhzs;->j:Lhgz;

    iput-object v1, p0, Lhzs;->r:Lhgz;

    const-string v0, ""

    iput-object v0, p0, Lhzs;->s:Ljava/lang/String;

    iput v2, p0, Lhzs;->t:I

    iput-object v1, p0, Lhzs;->k:Lhzt;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhzs;->u:J

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhzs;->a:Lhgz;

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    iget-object v2, p0, Lhzs;->a:Lhgz;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhzs;->b:Lhgz;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhzs;->b:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhzs;->l:Lhgz;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lhzs;->l:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhzs;->m:Lhgz;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lhzs;->m:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhzs;->c:Lhgz;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lhzs;->c:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhzs;->n:Lhgz;

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Lhzs;->n:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhzs;->o:Lhgz;

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lhzs;->o:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-boolean v2, p0, Lhzs;->d:Z

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    iget-boolean v3, p0, Lhzs;->d:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_6
    iget-boolean v2, p0, Lhzs;->e:Z

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    iget-boolean v3, p0, Lhzs;->e:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lhzs;->f:Lhgz;

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    iget-object v3, p0, Lhzs;->f:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget v2, p0, Lhzs;->p:I

    if-eqz v2, :cond_9

    const/16 v2, 0xb

    iget v3, p0, Lhzs;->p:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Lhzs;->q:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const/16 v2, 0xc

    iget-object v3, p0, Lhzs;->q:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Lhzs;->g:Lhll;

    if-eqz v2, :cond_b

    const/16 v2, 0xd

    iget-object v3, p0, Lhzs;->g:Lhll;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget-object v2, p0, Lhzs;->h:[Lhbi;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lhzs;->h:[Lhbi;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    if-eqz v4, :cond_c

    const/16 v5, 0xe

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_d
    iget-object v1, p0, Lhzs;->i:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_e

    const/16 v1, 0x10

    iget-object v2, p0, Lhzs;->i:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget-object v1, p0, Lhzs;->j:Lhgz;

    if-eqz v1, :cond_f

    const/16 v1, 0x11

    iget-object v2, p0, Lhzs;->j:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Lhzs;->r:Lhgz;

    if-eqz v1, :cond_10

    const/16 v1, 0x12

    iget-object v2, p0, Lhzs;->r:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-object v1, p0, Lhzs;->s:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    const/16 v1, 0x17

    iget-object v2, p0, Lhzs;->s:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget v1, p0, Lhzs;->t:I

    if-eqz v1, :cond_12

    const/16 v1, 0x18

    iget v2, p0, Lhzs;->t:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget-object v1, p0, Lhzs;->k:Lhzt;

    if-eqz v1, :cond_13

    const/16 v1, 0x19

    iget-object v2, p0, Lhzs;->k:Lhzt;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-wide v2, p0, Lhzs;->u:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_14

    const/16 v1, 0x1b

    iget-wide v2, p0, Lhzs;->u:J

    invoke-static {v1, v2, v3}, Lidd;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget-object v1, p0, Lhzs;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhzs;->J:I

    return v0

    :cond_15
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhzs;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhzs;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhzs;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhzs;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhzs;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhzs;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lhzs;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhzs;->l:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->l:Lhgz;

    :cond_4
    iget-object v0, p0, Lhzs;->l:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhzs;->m:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->m:Lhgz;

    :cond_5
    iget-object v0, p0, Lhzs;->m:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhzs;->c:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->c:Lhgz;

    :cond_6
    iget-object v0, p0, Lhzs;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhzs;->n:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->n:Lhgz;

    :cond_7
    iget-object v0, p0, Lhzs;->n:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lhzs;->o:Lhgz;

    if-nez v0, :cond_8

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->o:Lhgz;

    :cond_8
    iget-object v0, p0, Lhzs;->o:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzs;->d:Z

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzs;->e:Z

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhzs;->f:Lhgz;

    if-nez v0, :cond_9

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->f:Lhgz;

    :cond_9
    iget-object v0, p0, Lhzs;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_a

    if-eq v0, v4, :cond_a

    if-ne v0, v5, :cond_b

    :cond_a
    iput v0, p0, Lhzs;->p:I

    goto/16 :goto_0

    :cond_b
    iput v1, p0, Lhzs;->p:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzs;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lhzs;->g:Lhll;

    if-nez v0, :cond_c

    new-instance v0, Lhll;

    invoke-direct {v0}, Lhll;-><init>()V

    iput-object v0, p0, Lhzs;->g:Lhll;

    :cond_c
    iget-object v0, p0, Lhzs;->g:Lhll;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x72

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhzs;->h:[Lhbi;

    if-nez v0, :cond_e

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lhzs;->h:[Lhbi;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lhzs;->h:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    iput-object v2, p0, Lhzs;->h:[Lhbi;

    :goto_2
    iget-object v2, p0, Lhzs;->h:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    iget-object v2, p0, Lhzs;->h:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhzs;->h:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_e
    iget-object v0, p0, Lhzs;->h:[Lhbi;

    array-length v0, v0

    goto :goto_1

    :cond_f
    iget-object v2, p0, Lhzs;->h:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhzs;->h:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhzs;->i:[B

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lhzs;->j:Lhgz;

    if-nez v0, :cond_10

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->j:Lhgz;

    :cond_10
    iget-object v0, p0, Lhzs;->j:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lhzs;->r:Lhgz;

    if-nez v0, :cond_11

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzs;->r:Lhgz;

    :cond_11
    iget-object v0, p0, Lhzs;->r:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzs;->s:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_12

    if-eq v0, v4, :cond_12

    if-eq v0, v5, :cond_12

    const/4 v2, 0x3

    if-ne v0, v2, :cond_13

    :cond_12
    iput v0, p0, Lhzs;->t:I

    goto/16 :goto_0

    :cond_13
    iput v1, p0, Lhzs;->t:I

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lhzs;->k:Lhzt;

    if-nez v0, :cond_14

    new-instance v0, Lhzt;

    invoke-direct {v0}, Lhzt;-><init>()V

    iput-object v0, p0, Lhzs;->k:Lhzt;

    :cond_14
    iget-object v0, p0, Lhzs;->k:Lhzt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lhzs;->u:J

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0xba -> :sswitch_12
        0xc0 -> :sswitch_13
        0xca -> :sswitch_14
        0xd8 -> :sswitch_15
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhzs;->a:Lhgz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhzs;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhzs;->b:Lhgz;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhzs;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhzs;->l:Lhgz;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhzs;->l:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhzs;->m:Lhgz;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lhzs;->m:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhzs;->c:Lhgz;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lhzs;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Lhzs;->n:Lhgz;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lhzs;->n:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v0, p0, Lhzs;->o:Lhgz;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lhzs;->o:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_6
    iget-boolean v0, p0, Lhzs;->d:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-boolean v1, p0, Lhzs;->d:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_7
    iget-boolean v0, p0, Lhzs;->e:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Lhzs;->e:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_8
    iget-object v0, p0, Lhzs;->f:Lhgz;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lhzs;->f:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_9
    iget v0, p0, Lhzs;->p:I

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget v1, p0, Lhzs;->p:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_a
    iget-object v0, p0, Lhzs;->q:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Lhzs;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_b
    iget-object v0, p0, Lhzs;->g:Lhll;

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lhzs;->g:Lhll;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_c
    iget-object v0, p0, Lhzs;->h:[Lhbi;

    if-eqz v0, :cond_e

    iget-object v1, p0, Lhzs;->h:[Lhbi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    if-eqz v3, :cond_d

    const/16 v4, 0xe

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_e
    iget-object v0, p0, Lhzs;->i:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_f

    const/16 v0, 0x10

    iget-object v1, p0, Lhzs;->i:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_f
    iget-object v0, p0, Lhzs;->j:Lhgz;

    if-eqz v0, :cond_10

    const/16 v0, 0x11

    iget-object v1, p0, Lhzs;->j:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_10
    iget-object v0, p0, Lhzs;->r:Lhgz;

    if-eqz v0, :cond_11

    const/16 v0, 0x12

    iget-object v1, p0, Lhzs;->r:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_11
    iget-object v0, p0, Lhzs;->s:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const/16 v0, 0x17

    iget-object v1, p0, Lhzs;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_12
    iget v0, p0, Lhzs;->t:I

    if-eqz v0, :cond_13

    const/16 v0, 0x18

    iget v1, p0, Lhzs;->t:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_13
    iget-object v0, p0, Lhzs;->k:Lhzt;

    if-eqz v0, :cond_14

    const/16 v0, 0x19

    iget-object v1, p0, Lhzs;->k:Lhzt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_14
    iget-wide v0, p0, Lhzs;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_15

    const/16 v0, 0x1b

    iget-wide v2, p0, Lhzs;->u:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    :cond_15
    iget-object v0, p0, Lhzs;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhzs;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhzs;

    iget-object v2, p0, Lhzs;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhzs;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhzs;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhzs;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhzs;->l:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhzs;->l:Lhgz;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhzs;->m:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhzs;->m:Lhgz;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhzs;->c:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhzs;->c:Lhgz;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhzs;->n:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhzs;->n:Lhgz;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhzs;->o:Lhgz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhzs;->o:Lhgz;

    if-nez v2, :cond_3

    :goto_7
    iget-boolean v2, p0, Lhzs;->d:Z

    iget-boolean v3, p1, Lhzs;->d:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhzs;->e:Z

    iget-boolean v3, p1, Lhzs;->e:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhzs;->f:Lhgz;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhzs;->f:Lhgz;

    if-nez v2, :cond_3

    :goto_8
    iget v2, p0, Lhzs;->p:I

    iget v3, p1, Lhzs;->p:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhzs;->q:Ljava/lang/String;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhzs;->q:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_9
    iget-object v2, p0, Lhzs;->g:Lhll;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhzs;->g:Lhll;

    if-nez v2, :cond_3

    :goto_a
    iget-object v2, p0, Lhzs;->h:[Lhbi;

    iget-object v3, p1, Lhzs;->h:[Lhbi;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhzs;->i:[B

    iget-object v3, p1, Lhzs;->i:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhzs;->j:Lhgz;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhzs;->j:Lhgz;

    if-nez v2, :cond_3

    :goto_b
    iget-object v2, p0, Lhzs;->r:Lhgz;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhzs;->r:Lhgz;

    if-nez v2, :cond_3

    :goto_c
    iget-object v2, p0, Lhzs;->s:Ljava/lang/String;

    if-nez v2, :cond_10

    iget-object v2, p1, Lhzs;->s:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_d
    iget v2, p0, Lhzs;->t:I

    iget v3, p1, Lhzs;->t:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhzs;->k:Lhzt;

    if-nez v2, :cond_11

    iget-object v2, p1, Lhzs;->k:Lhzt;

    if-nez v2, :cond_3

    :goto_e
    iget-wide v2, p0, Lhzs;->u:J

    iget-wide v4, p1, Lhzs;->u:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhzs;->I:Ljava/util/List;

    if-nez v2, :cond_12

    iget-object v2, p1, Lhzs;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhzs;->a:Lhgz;

    iget-object v3, p1, Lhzs;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhzs;->b:Lhgz;

    iget-object v3, p1, Lhzs;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhzs;->l:Lhgz;

    iget-object v3, p1, Lhzs;->l:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhzs;->m:Lhgz;

    iget-object v3, p1, Lhzs;->m:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhzs;->c:Lhgz;

    iget-object v3, p1, Lhzs;->c:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhzs;->n:Lhgz;

    iget-object v3, p1, Lhzs;->n:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhzs;->o:Lhgz;

    iget-object v3, p1, Lhzs;->o:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhzs;->f:Lhgz;

    iget-object v3, p1, Lhzs;->f:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhzs;->q:Ljava/lang/String;

    iget-object v3, p1, Lhzs;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhzs;->g:Lhll;

    iget-object v3, p1, Lhzs;->g:Lhll;

    invoke-virtual {v2, v3}, Lhll;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v2, p0, Lhzs;->j:Lhgz;

    iget-object v3, p1, Lhzs;->j:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v2, p0, Lhzs;->r:Lhgz;

    iget-object v3, p1, Lhzs;->r:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v2, p0, Lhzs;->s:Ljava/lang/String;

    iget-object v3, p1, Lhzs;->s:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v2, p0, Lhzs;->k:Lhzt;

    iget-object v3, p1, Lhzs;->k:Lhzt;

    invoke-virtual {v2, v3}, Lhzt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v2, p0, Lhzs;->I:Ljava/util/List;

    iget-object v3, p1, Lhzs;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzs;->a:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzs;->b:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzs;->l:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzs;->m:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzs;->c:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzs;->n:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzs;->o:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_6
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhzs;->d:Z

    if-eqz v0, :cond_9

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lhzs;->e:Z

    if-eqz v4, :cond_a

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzs;->f:Lhgz;

    if-nez v0, :cond_b

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhzs;->p:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzs;->q:Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzs;->g:Lhll;

    if-nez v0, :cond_d

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    iget-object v2, p0, Lhzs;->h:[Lhbi;

    if-nez v2, :cond_e

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lhzs;->i:[B

    if-nez v0, :cond_10

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhzs;->j:Lhgz;

    if-nez v0, :cond_11

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzs;->r:Lhgz;

    if-nez v0, :cond_12

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzs;->s:Ljava/lang/String;

    if-nez v0, :cond_13

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhzs;->t:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzs;->k:Lhzt;

    if-nez v0, :cond_14

    move v0, v1

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhzs;->u:J

    iget-wide v4, p0, Lhzs;->u:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhzs;->I:Ljava/util/List;

    if-nez v2, :cond_15

    :goto_10
    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v0, p0, Lhzs;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lhzs;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lhzs;->l:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_5
    iget-object v0, p0, Lhzs;->m:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lhzs;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_7
    iget-object v0, p0, Lhzs;->n:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_8
    iget-object v0, p0, Lhzs;->o:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_6

    :cond_9
    move v0, v3

    goto/16 :goto_7

    :cond_a
    move v2, v3

    goto/16 :goto_8

    :cond_b
    iget-object v0, p0, Lhzs;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_c
    iget-object v0, p0, Lhzs;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_a

    :cond_d
    iget-object v0, p0, Lhzs;->g:Lhll;

    invoke-virtual {v0}, Lhll;->hashCode()I

    move-result v0

    goto/16 :goto_b

    :cond_e
    move v2, v0

    move v0, v1

    :goto_11
    iget-object v3, p0, Lhzs;->h:[Lhbi;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhzs;->h:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_f

    move v2, v1

    :goto_12
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_f
    iget-object v2, p0, Lhzs;->h:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_12

    :cond_10
    move v0, v1

    :goto_13
    iget-object v3, p0, Lhzs;->i:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhzs;->i:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    :cond_11
    iget-object v0, p0, Lhzs;->j:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_c

    :cond_12
    iget-object v0, p0, Lhzs;->r:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_d

    :cond_13
    iget-object v0, p0, Lhzs;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_e

    :cond_14
    iget-object v0, p0, Lhzs;->k:Lhzt;

    invoke-virtual {v0}, Lhzt;->hashCode()I

    move-result v0

    goto/16 :goto_f

    :cond_15
    iget-object v1, p0, Lhzs;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_10
.end method
