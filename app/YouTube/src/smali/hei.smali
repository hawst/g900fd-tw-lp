.class public final Lhei;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhgz;

.field public c:Lhgz;

.field public d:[B

.field public e:[Lhgz;

.field private f:Lhgz;

.field private g:Lhut;

.field private h:Lhut;

.field private i:Lhog;

.field private j:[Lgyj;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38595
    invoke-direct {p0}, Lidf;-><init>()V

    .line 38598
    iput-object v1, p0, Lhei;->a:Lhgz;

    .line 38601
    iput-object v1, p0, Lhei;->f:Lhgz;

    .line 38604
    iput-object v1, p0, Lhei;->b:Lhgz;

    .line 38607
    iput-object v1, p0, Lhei;->c:Lhgz;

    .line 38610
    iput-object v1, p0, Lhei;->g:Lhut;

    .line 38613
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhei;->d:[B

    .line 38616
    sget-object v0, Lhgz;->a:[Lhgz;

    iput-object v0, p0, Lhei;->e:[Lhgz;

    .line 38619
    iput-object v1, p0, Lhei;->h:Lhut;

    .line 38622
    iput-object v1, p0, Lhei;->i:Lhog;

    .line 38625
    sget-object v0, Lgyj;->a:[Lgyj;

    iput-object v0, p0, Lhei;->j:[Lgyj;

    .line 38595
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 38739
    .line 38740
    iget-object v0, p0, Lhei;->a:Lhgz;

    if-eqz v0, :cond_b

    .line 38741
    const/4 v0, 0x1

    iget-object v2, p0, Lhei;->a:Lhgz;

    .line 38742
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 38744
    :goto_0
    iget-object v2, p0, Lhei;->f:Lhgz;

    if-eqz v2, :cond_0

    .line 38745
    const/4 v2, 0x2

    iget-object v3, p0, Lhei;->f:Lhgz;

    .line 38746
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 38748
    :cond_0
    iget-object v2, p0, Lhei;->b:Lhgz;

    if-eqz v2, :cond_1

    .line 38749
    const/4 v2, 0x3

    iget-object v3, p0, Lhei;->b:Lhgz;

    .line 38750
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 38752
    :cond_1
    iget-object v2, p0, Lhei;->c:Lhgz;

    if-eqz v2, :cond_2

    .line 38753
    const/4 v2, 0x4

    iget-object v3, p0, Lhei;->c:Lhgz;

    .line 38754
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 38756
    :cond_2
    iget-object v2, p0, Lhei;->g:Lhut;

    if-eqz v2, :cond_3

    .line 38757
    const/4 v2, 0x5

    iget-object v3, p0, Lhei;->g:Lhut;

    .line 38758
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 38760
    :cond_3
    iget-object v2, p0, Lhei;->d:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_4

    .line 38761
    const/4 v2, 0x7

    iget-object v3, p0, Lhei;->d:[B

    .line 38762
    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 38764
    :cond_4
    iget-object v2, p0, Lhei;->e:[Lhgz;

    if-eqz v2, :cond_6

    .line 38765
    iget-object v3, p0, Lhei;->e:[Lhgz;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 38766
    if-eqz v5, :cond_5

    .line 38767
    const/16 v6, 0x8

    .line 38768
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 38765
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 38772
    :cond_6
    iget-object v2, p0, Lhei;->h:Lhut;

    if-eqz v2, :cond_7

    .line 38773
    const/16 v2, 0x9

    iget-object v3, p0, Lhei;->h:Lhut;

    .line 38774
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 38776
    :cond_7
    iget-object v2, p0, Lhei;->i:Lhog;

    if-eqz v2, :cond_8

    .line 38777
    const/16 v2, 0xa

    iget-object v3, p0, Lhei;->i:Lhog;

    .line 38778
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 38780
    :cond_8
    iget-object v2, p0, Lhei;->j:[Lgyj;

    if-eqz v2, :cond_a

    .line 38781
    iget-object v2, p0, Lhei;->j:[Lgyj;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 38782
    if-eqz v4, :cond_9

    .line 38783
    const/16 v5, 0xb

    .line 38784
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 38781
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 38788
    :cond_a
    iget-object v1, p0, Lhei;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38789
    iput v0, p0, Lhei;->J:I

    .line 38790
    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 38591
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhei;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhei;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhei;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhei;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhei;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhei;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhei;->f:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhei;->f:Lhgz;

    :cond_3
    iget-object v0, p0, Lhei;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhei;->b:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhei;->b:Lhgz;

    :cond_4
    iget-object v0, p0, Lhei;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhei;->c:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhei;->c:Lhgz;

    :cond_5
    iget-object v0, p0, Lhei;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhei;->g:Lhut;

    if-nez v0, :cond_6

    new-instance v0, Lhut;

    invoke-direct {v0}, Lhut;-><init>()V

    iput-object v0, p0, Lhei;->g:Lhut;

    :cond_6
    iget-object v0, p0, Lhei;->g:Lhut;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhei;->d:[B

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhei;->e:[Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgz;

    iget-object v3, p0, Lhei;->e:[Lhgz;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lhei;->e:[Lhgz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lhei;->e:[Lhgz;

    :goto_2
    iget-object v2, p0, Lhei;->e:[Lhgz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lhei;->e:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhei;->e:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lhei;->e:[Lhgz;

    array-length v0, v0

    goto :goto_1

    :cond_9
    iget-object v2, p0, Lhei;->e:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhei;->e:[Lhgz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhei;->h:Lhut;

    if-nez v0, :cond_a

    new-instance v0, Lhut;

    invoke-direct {v0}, Lhut;-><init>()V

    iput-object v0, p0, Lhei;->h:Lhut;

    :cond_a
    iget-object v0, p0, Lhei;->h:Lhut;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhei;->i:Lhog;

    if-nez v0, :cond_b

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhei;->i:Lhog;

    :cond_b
    iget-object v0, p0, Lhei;->i:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhei;->j:[Lgyj;

    if-nez v0, :cond_d

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgyj;

    iget-object v3, p0, Lhei;->j:[Lgyj;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lhei;->j:[Lgyj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    iput-object v2, p0, Lhei;->j:[Lgyj;

    :goto_4
    iget-object v2, p0, Lhei;->j:[Lgyj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    iget-object v2, p0, Lhei;->j:[Lgyj;

    new-instance v3, Lgyj;

    invoke-direct {v3}, Lgyj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhei;->j:[Lgyj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_d
    iget-object v0, p0, Lhei;->j:[Lgyj;

    array-length v0, v0

    goto :goto_3

    :cond_e
    iget-object v2, p0, Lhei;->j:[Lgyj;

    new-instance v3, Lgyj;

    invoke-direct {v3}, Lgyj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhei;->j:[Lgyj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 38695
    iget-object v1, p0, Lhei;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 38696
    const/4 v1, 0x1

    iget-object v2, p0, Lhei;->a:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 38698
    :cond_0
    iget-object v1, p0, Lhei;->f:Lhgz;

    if-eqz v1, :cond_1

    .line 38699
    const/4 v1, 0x2

    iget-object v2, p0, Lhei;->f:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 38701
    :cond_1
    iget-object v1, p0, Lhei;->b:Lhgz;

    if-eqz v1, :cond_2

    .line 38702
    const/4 v1, 0x3

    iget-object v2, p0, Lhei;->b:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 38704
    :cond_2
    iget-object v1, p0, Lhei;->c:Lhgz;

    if-eqz v1, :cond_3

    .line 38705
    const/4 v1, 0x4

    iget-object v2, p0, Lhei;->c:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 38707
    :cond_3
    iget-object v1, p0, Lhei;->g:Lhut;

    if-eqz v1, :cond_4

    .line 38708
    const/4 v1, 0x5

    iget-object v2, p0, Lhei;->g:Lhut;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 38710
    :cond_4
    iget-object v1, p0, Lhei;->d:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 38711
    const/4 v1, 0x7

    iget-object v2, p0, Lhei;->d:[B

    invoke-virtual {p1, v1, v2}, Lidd;->a(I[B)V

    .line 38713
    :cond_5
    iget-object v1, p0, Lhei;->e:[Lhgz;

    if-eqz v1, :cond_7

    .line 38714
    iget-object v2, p0, Lhei;->e:[Lhgz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 38715
    if-eqz v4, :cond_6

    .line 38716
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 38714
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38720
    :cond_7
    iget-object v1, p0, Lhei;->h:Lhut;

    if-eqz v1, :cond_8

    .line 38721
    const/16 v1, 0x9

    iget-object v2, p0, Lhei;->h:Lhut;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 38723
    :cond_8
    iget-object v1, p0, Lhei;->i:Lhog;

    if-eqz v1, :cond_9

    .line 38724
    const/16 v1, 0xa

    iget-object v2, p0, Lhei;->i:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 38726
    :cond_9
    iget-object v1, p0, Lhei;->j:[Lgyj;

    if-eqz v1, :cond_b

    .line 38727
    iget-object v1, p0, Lhei;->j:[Lgyj;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 38728
    if-eqz v3, :cond_a

    .line 38729
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 38727
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 38733
    :cond_b
    iget-object v0, p0, Lhei;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 38735
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 38645
    if-ne p1, p0, :cond_1

    .line 38658
    :cond_0
    :goto_0
    return v0

    .line 38646
    :cond_1
    instance-of v2, p1, Lhei;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 38647
    :cond_2
    check-cast p1, Lhei;

    .line 38648
    iget-object v2, p0, Lhei;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhei;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhei;->f:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhei;->f:Lhgz;

    if-nez v2, :cond_3

    .line 38649
    :goto_2
    iget-object v2, p0, Lhei;->b:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhei;->b:Lhgz;

    if-nez v2, :cond_3

    .line 38650
    :goto_3
    iget-object v2, p0, Lhei;->c:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhei;->c:Lhgz;

    if-nez v2, :cond_3

    .line 38651
    :goto_4
    iget-object v2, p0, Lhei;->g:Lhut;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhei;->g:Lhut;

    if-nez v2, :cond_3

    .line 38652
    :goto_5
    iget-object v2, p0, Lhei;->d:[B

    iget-object v3, p1, Lhei;->d:[B

    .line 38653
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhei;->e:[Lhgz;

    iget-object v3, p1, Lhei;->e:[Lhgz;

    .line 38654
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhei;->h:Lhut;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhei;->h:Lhut;

    if-nez v2, :cond_3

    .line 38655
    :goto_6
    iget-object v2, p0, Lhei;->i:Lhog;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhei;->i:Lhog;

    if-nez v2, :cond_3

    .line 38656
    :goto_7
    iget-object v2, p0, Lhei;->j:[Lgyj;

    iget-object v3, p1, Lhei;->j:[Lgyj;

    .line 38657
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhei;->I:Ljava/util/List;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhei;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 38658
    goto :goto_0

    .line 38648
    :cond_4
    iget-object v2, p0, Lhei;->a:Lhgz;

    iget-object v3, p1, Lhei;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhei;->f:Lhgz;

    iget-object v3, p1, Lhei;->f:Lhgz;

    .line 38649
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhei;->b:Lhgz;

    iget-object v3, p1, Lhei;->b:Lhgz;

    .line 38650
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhei;->c:Lhgz;

    iget-object v3, p1, Lhei;->c:Lhgz;

    .line 38651
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhei;->g:Lhut;

    iget-object v3, p1, Lhei;->g:Lhut;

    .line 38652
    invoke-virtual {v2, v3}, Lhut;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    .line 38654
    :cond_9
    iget-object v2, p0, Lhei;->h:Lhut;

    iget-object v3, p1, Lhei;->h:Lhut;

    .line 38655
    invoke-virtual {v2, v3}, Lhut;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhei;->i:Lhog;

    iget-object v3, p1, Lhei;->i:Lhog;

    .line 38656
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    .line 38657
    :cond_b
    iget-object v2, p0, Lhei;->I:Ljava/util/List;

    iget-object v3, p1, Lhei;->I:Ljava/util/List;

    .line 38658
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 38662
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 38664
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhei;->a:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 38665
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhei;->f:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 38666
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhei;->b:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 38667
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhei;->c:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 38668
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhei;->g:Lhut;

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 38669
    iget-object v2, p0, Lhei;->d:[B

    if-nez v2, :cond_8

    mul-int/lit8 v2, v0, 0x1f

    .line 38675
    :cond_0
    iget-object v0, p0, Lhei;->e:[Lhgz;

    if-nez v0, :cond_9

    mul-int/lit8 v2, v2, 0x1f

    .line 38681
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhei;->h:Lhut;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 38682
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhei;->i:Lhog;

    if-nez v0, :cond_c

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 38683
    iget-object v2, p0, Lhei;->j:[Lgyj;

    if-nez v2, :cond_d

    mul-int/lit8 v2, v0, 0x1f

    .line 38689
    :cond_2
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhei;->I:Ljava/util/List;

    if-nez v2, :cond_f

    :goto_7
    add-int/2addr v0, v1

    .line 38690
    return v0

    .line 38664
    :cond_3
    iget-object v0, p0, Lhei;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 38665
    :cond_4
    iget-object v0, p0, Lhei;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 38666
    :cond_5
    iget-object v0, p0, Lhei;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 38667
    :cond_6
    iget-object v0, p0, Lhei;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    .line 38668
    :cond_7
    iget-object v0, p0, Lhei;->g:Lhut;

    invoke-virtual {v0}, Lhut;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_8
    move v2, v0

    move v0, v1

    .line 38671
    :goto_8
    iget-object v3, p0, Lhei;->d:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 38672
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhei;->d:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 38671
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    move v0, v1

    .line 38677
    :goto_9
    iget-object v3, p0, Lhei;->e:[Lhgz;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 38678
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhei;->e:[Lhgz;

    aget-object v2, v2, v0

    if-nez v2, :cond_a

    move v2, v1

    :goto_a
    add-int/2addr v2, v3

    .line 38677
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 38678
    :cond_a
    iget-object v2, p0, Lhei;->e:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgz;->hashCode()I

    move-result v2

    goto :goto_a

    .line 38681
    :cond_b
    iget-object v0, p0, Lhei;->h:Lhut;

    invoke-virtual {v0}, Lhut;->hashCode()I

    move-result v0

    goto :goto_5

    .line 38682
    :cond_c
    iget-object v0, p0, Lhei;->i:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_d
    move v2, v0

    move v0, v1

    .line 38685
    :goto_b
    iget-object v3, p0, Lhei;->j:[Lgyj;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 38686
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhei;->j:[Lgyj;

    aget-object v2, v2, v0

    if-nez v2, :cond_e

    move v2, v1

    :goto_c
    add-int/2addr v2, v3

    .line 38685
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 38686
    :cond_e
    iget-object v2, p0, Lhei;->j:[Lgyj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgyj;->hashCode()I

    move-result v2

    goto :goto_c

    .line 38689
    :cond_f
    iget-object v1, p0, Lhei;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_7
.end method
