.class final Lcrx;
.super Lfbd;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lfbd;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/net/Uri;)Lfpf;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const-string v0, "Badly formed progress tracking event (missing offset attribute) - ignoring"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 133
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 118
    :cond_0
    const-string v0, "%"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, La;->b(Ljava/lang/String;I)I

    move-result v1

    .line 120
    if-ltz v1, :cond_1

    const/16 v0, 0x64

    if-gt v1, v0, :cond_1

    .line 121
    new-instance v0, Lfpf;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, p1}, Lfpf;-><init>(IZLandroid/net/Uri;)V

    goto :goto_1

    .line 123
    :cond_1
    const-string v0, "Badly formed progress tracking event (invalid offset percentage) - ignoring"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :cond_2
    :try_start_0
    new-instance v0, Lfpf;

    .line 128
    invoke-static {p0}, La;->C(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p1}, Lfpf;-><init>(IZLandroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 130
    :catch_0
    move-exception v0

    const-string v0, "Badly formed progress tracking event (invalid offset format) - ignoring"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfah;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 70
    const-class v0, Lfpc;

    invoke-virtual {p1, v0}, Lfah;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpc;

    .line 71
    const-string v1, "event"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    if-nez v1, :cond_1

    .line 73
    const-string v0, "Badly formed tracking event - ignoring"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, La;->E(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 78
    const-string v3, "start"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    invoke-virtual {v0, v2}, Lfpc;->b(Landroid/net/Uri;)Lfpc;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    const-string v0, "Badly formed tracking uri - ignoring"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :cond_2
    :try_start_1
    const-string v3, "creativeView"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 82
    invoke-virtual {v0, v2}, Lfpc;->b(Landroid/net/Uri;)Lfpc;

    goto :goto_0

    .line 83
    :cond_3
    const-string v3, "firstQuartile"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 84
    invoke-virtual {v0, v2}, Lfpc;->c(Landroid/net/Uri;)Lfpc;

    goto :goto_0

    .line 85
    :cond_4
    const-string v3, "midpoint"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 86
    invoke-virtual {v0, v2}, Lfpc;->d(Landroid/net/Uri;)Lfpc;

    goto :goto_0

    .line 87
    :cond_5
    const-string v3, "thirdQuartile"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 88
    invoke-virtual {v0, v2}, Lfpc;->e(Landroid/net/Uri;)Lfpc;

    goto :goto_0

    .line 89
    :cond_6
    const-string v3, "complete"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 90
    invoke-virtual {v0, v2}, Lfpc;->h(Landroid/net/Uri;)Lfpc;

    goto :goto_0

    .line 91
    :cond_7
    const-string v3, "pause"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 92
    invoke-virtual {v0, v2}, Lfpc;->j(Landroid/net/Uri;)Lfpc;

    goto :goto_0

    .line 93
    :cond_8
    const-string v3, "resume"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 94
    invoke-virtual {v0, v2}, Lfpc;->k(Landroid/net/Uri;)Lfpc;

    goto :goto_0

    .line 95
    :cond_9
    const-string v3, "mute"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 96
    invoke-virtual {v0, v2}, Lfpc;->l(Landroid/net/Uri;)Lfpc;

    goto :goto_0

    .line 97
    :cond_a
    const-string v3, "fullscreen"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 98
    invoke-virtual {v0, v2}, Lfpc;->m(Landroid/net/Uri;)Lfpc;

    goto/16 :goto_0

    .line 99
    :cond_b
    const-string v3, "endFullscreen"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 100
    iget-object v1, v0, Lfpc;->K:Ljava/util/List;

    if-nez v1, :cond_c

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lfpc;->K:Ljava/util/List;

    :cond_c
    iget-object v0, v0, Lfpc;->K:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 101
    :cond_d
    const-string v3, "close"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 102
    invoke-virtual {v0, v2}, Lfpc;->i(Landroid/net/Uri;)Lfpc;

    goto/16 :goto_0

    .line 103
    :cond_e
    const-string v3, "skip"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    iget v3, v0, Lfpc;->a:I

    if-lt v3, v4, :cond_f

    .line 104
    invoke-virtual {v0, v2}, Lfpc;->f(Landroid/net/Uri;)Lfpc;

    goto/16 :goto_0

    .line 105
    :cond_f
    const-string v3, "progress"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Lfpc;->a:I

    if-lt v1, v4, :cond_0

    .line 106
    const-string v1, "offset"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lcrx;->a(Ljava/lang/String;Landroid/net/Uri;)Lfpf;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_0

    .line 108
    invoke-virtual {v0, v1}, Lfpc;->a(Lfpf;)Lfpc;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
