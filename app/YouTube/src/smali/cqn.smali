.class public final Lcqn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcqj;


# instance fields
.field final a:Labu;

.field b:Ljava/util/List;

.field private final c:Lgix;

.field private final d:Ljava/lang/String;

.field private final e:Lexd;

.field private final f:Lcwq;

.field private final g:Levn;

.field private h:Ljava/util/Map;


# direct methods
.method public constructor <init>(Levn;Lgix;Labu;Ljava/lang/String;Lexd;Lcwq;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcqn;->g:Levn;

    .line 64
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lcqn;->c:Lgix;

    .line 65
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labu;

    iput-object v0, p0, Lcqn;->a:Labu;

    .line 66
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcqn;->d:Ljava/lang/String;

    .line 67
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lcqn;->e:Lexd;

    .line 68
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwq;

    iput-object v0, p0, Lcqn;->f:Lcwq;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcqn;->h:Ljava/util/Map;

    .line 70
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcqn;->b:Ljava/util/List;

    .line 71
    return-void
.end method

.method private b(Ljava/lang/Class;)Lcqo;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcqn;->h:Ljava/util/Map;

    .line 121
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqo;

    .line 122
    if-eqz v0, :cond_0

    .line 130
    :goto_0
    return-object v0

    .line 127
    :cond_0
    new-instance v0, Lcqo;

    invoke-direct {v0, p0}, Lcqo;-><init>(Lcqn;)V

    .line 128
    iget-object v1, p0, Lcqn;->g:Levn;

    invoke-virtual {v1, p0, p1, v0}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;Levq;)Levr;

    .line 129
    iget-object v1, p0, Lcqn;->h:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Lcqi;)Lcql;
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcqn;->a(Ljava/lang/Class;Lcqi;Lewh;)Lcql;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Lcqi;Lewh;)Lcql;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Lcqq;

    invoke-direct {v0, p0, p2, p3}, Lcqq;-><init>(Lcqn;Lcqi;Lewh;)V

    .line 151
    invoke-direct {p0, p1}, Lcqn;->b(Ljava/lang/Class;)Lcqo;

    move-result-object v1

    .line 152
    iget-object v1, v1, Lcqo;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    return-object v0
.end method

.method public final a()Lgix;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcqn;->c:Lgix;

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Lcqk;)V
    .locals 2

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcqn;->b(Ljava/lang/Class;)Lcqo;

    move-result-object v0

    .line 184
    iget-object v0, v0, Lcqo;->b:Ljava/util/List;

    new-instance v1, Lcqp;

    invoke-direct {v1, p0, p2}, Lcqp;-><init>(Lcqn;Lcqk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    return-void
.end method

.method public final a(Ljava/lang/Class;Lcqm;)V
    .locals 2

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcqn;->b(Ljava/lang/Class;)Lcqo;

    move-result-object v0

    .line 174
    iget-object v0, v0, Lcqo;->b:Ljava/util/List;

    new-instance v1, Lcqs;

    invoke-direct {v1, p0, p2}, Lcqs;-><init>(Lcqn;Lcqm;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcqn;->b(Ljava/lang/Class;)Lcqo;

    move-result-object v0

    .line 164
    iget-object v0, v0, Lcqo;->b:Ljava/util/List;

    new-instance v1, Lcqr;

    invoke-direct {v1, p0, p2}, Lcqr;-><init>(Lcqn;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    return-void
.end method

.method public final a(Ljava/lang/Class;)Z
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcqn;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqh;

    .line 215
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const/4 v0, 0x1

    .line 219
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcqn;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lexd;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcqn;->e:Lexd;

    return-object v0
.end method

.method public final d()Lcwx;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcqn;->f:Lcwq;

    invoke-virtual {v0}, Lcwq;->g()Lcwx;

    move-result-object v0

    return-object v0
.end method
