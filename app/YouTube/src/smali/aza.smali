.class public final Laza;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Layz;
.implements Lcnn;


# instance fields
.field private final a:Lcod;

.field private final b:Lcnu;

.field private final c:Lgix;

.field private final d:Lgnh;

.field private final e:Lezj;

.field private final f:J

.field private final g:I

.field private final h:J

.field private final i:J

.field private final j:J


# direct methods
.method public constructor <init>(Lcod;Lgix;Lgnh;Lcnu;Lezj;Lcyc;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x3e8

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcod;

    iput-object v0, p0, Laza;->a:Lcod;

    .line 64
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnu;

    iput-object v0, p0, Laza;->b:Lcnu;

    .line 65
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Laza;->c:Lgix;

    .line 66
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnh;

    iput-object v0, p0, Laza;->d:Lgnh;

    .line 67
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Laza;->e:Lezj;

    .line 69
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-interface {p6}, Lcyc;->F()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Laza;->f:J

    .line 71
    invoke-interface {p6}, Lcyc;->G()I

    move-result v0

    iput v0, p0, Laza;->g:I

    .line 72
    invoke-interface {p6}, Lcyc;->H()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Laza;->h:J

    .line 73
    invoke-interface {p6}, Lcyc;->I()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Laza;->i:J

    .line 74
    invoke-interface {p6}, Lcyc;->J()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Laza;->j:J

    .line 75
    return-void
.end method

.method private a()Lgnd;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Laza;->d:Lgnh;

    invoke-interface {v0}, Lgnh;->O()Lgng;

    move-result-object v0

    .line 79
    iget-object v1, p0, Laza;->c:Lgix;

    invoke-interface {v1}, Lgix;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    invoke-virtual {v0}, Lgng;->c()Lgnd;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Laza;->c:Lgix;

    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfrl;Ljava/lang/String;)Lesq;
    .locals 1

    .prologue
    .line 121
    invoke-static {}, Lb;->b()V

    .line 122
    iget-object v0, p0, Laza;->a:Lcod;

    invoke-virtual {v0, p1, p2}, Lcod;->a(Lfrl;Ljava/lang/String;)Lesq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lfrl;Ljava/lang/String;Lfai;)Lesq;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 98
    invoke-static {}, Lb;->b()V

    .line 99
    invoke-direct {p0}, Laza;->a()Lgnd;

    move-result-object v1

    .line 100
    iget-object v2, p0, Laza;->b:Lcnu;

    iget-object v2, v2, Lcnu;->d:Lexd;

    .line 101
    iget-object v3, p1, Lfrl;->a:Lhro;

    invoke-static {v3}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v3

    .line 104
    invoke-virtual {p1}, Lfrl;->f()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 105
    invoke-interface {v1, v3}, Lgnd;->o(Ljava/lang/String;)Lesq;

    move-result-object v1

    if-nez v1, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-object v0

    .line 105
    :cond_1
    new-instance v2, Lesr;

    invoke-direct {v2}, Lesr;-><init>()V

    iget-object v0, v1, Lesq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Less;

    invoke-virtual {v0}, Less;->b()Lesv;

    move-result-object v0

    const/4 v3, 0x1

    iput-boolean v3, v0, Lesv;->m:Z

    invoke-virtual {v0}, Lesv;->a()Less;

    move-result-object v0

    invoke-virtual {v2, v0}, Lesr;->a(Less;)Lesr;

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lesr;->a()Lesq;

    move-result-object v0

    goto :goto_0

    .line 108
    :cond_3
    invoke-interface {v2}, Lexd;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v0, p0, Laza;->a:Lcod;

    invoke-virtual {v0, p1, p2, p3}, Lcod;->a(Lfrl;Ljava/lang/String;Lfai;)Lesq;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZLbjy;Lfai;)Lesq;
    .locals 9

    .prologue
    .line 141
    iget-object v0, p0, Laza;->a:Lcod;

    const-string v2, ""

    const-string v3, ""

    sget-object v4, Lfhy;->a:[B

    .line 146
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const-string v6, "pin"

    const-string v1, "1"

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lbjy;->a:Lbjy;

    if-ne p3, v1, :cond_0

    const-string v1, "vis"

    new-instance v6, Lcwx;

    const/4 v7, 0x3

    invoke-direct {v6, v7}, Lcwx;-><init>(I)V

    iget v6, v6, Lcwx;->a:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-wide v6, p0, Laza;->h:J

    move-object v1, p1

    move-object v8, p4

    .line 141
    invoke-virtual/range {v0 .. v8}, Lcod;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/util/Map;JLfai;)Lesq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Less;Lfai;)Lfoy;
    .locals 10

    .prologue
    const/4 v8, -0x1

    .line 280
    invoke-static {}, Lb;->b()V

    .line 284
    iget-object v1, p0, Laza;->a:Lcod;

    const-string v3, ""

    iget-wide v4, p0, Laza;->h:J

    .line 289
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v7

    move-object v2, p1

    move-object v6, p2

    .line 284
    invoke-virtual/range {v1 .. v7}, Lcod;->a(Less;Ljava/lang/String;JLfai;Ljava/util/Map;)Lfoy;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lfoy;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 293
    :goto_0
    return-object v0

    :cond_1
    iget v0, v1, Lfoy;->S:I

    if-eq v0, v8, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    iget v2, v1, Lfoy;->S:I

    iget-object v0, v1, Lfoy;->aa:Lfoy;

    move-object v9, v0

    move v0, v2

    move-object v2, v9

    :goto_1
    if-ne v0, v8, :cond_3

    if-eqz v2, :cond_3

    iget v3, v2, Lfoy;->S:I

    iget-object v0, v2, Lfoy;->aa:Lfoy;

    move-object v2, v0

    move v0, v3

    goto :goto_1

    :cond_3
    if-ne v0, v8, :cond_4

    iget v0, p0, Laza;->g:I

    :cond_4
    invoke-virtual {v1}, Lfoy;->b()Lfpc;

    move-result-object v1

    iput v0, v1, Lfpc;->S:I

    invoke-virtual {v1}, Lfpc;->a()Lfoy;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Less;Ljava/lang/String;Lfai;Ljava/util/Map;)Lfoy;
    .locals 16

    .prologue
    .line 182
    invoke-static {}, Lb;->b()V

    .line 183
    invoke-direct/range {p0 .. p0}, Laza;->a()Lgnd;

    move-result-object v8

    .line 184
    move-object/from16 v0, p0

    iget-object v4, v0, Laza;->b:Lcnu;

    iget-object v4, v4, Lcnu;->d:Lexd;

    .line 185
    move-object/from16 v0, p1

    iget-boolean v5, v0, Less;->n:Z

    if-eqz v5, :cond_d

    .line 188
    move-object/from16 v0, p1

    iget-object v4, v0, Less;->f:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Less;->e:Ljava/lang/String;

    .line 187
    invoke-interface {v8, v4, v5}, Lgnd;->b(Ljava/lang/String;Ljava/lang/String;)Lfoy;

    move-result-object v4

    .line 186
    if-nez v4, :cond_1

    const/4 v4, 0x0

    .line 196
    :cond_0
    :goto_0
    return-object v4

    .line 186
    :cond_1
    invoke-virtual {v4}, Lfoy;->b()Lfpc;

    move-result-object v4

    const/4 v5, 0x1

    iput-boolean v5, v4, Lfpc;->aa:Z

    const/4 v5, 0x1

    iput-boolean v5, v4, Lfpc;->ab:Z

    const-string v5, ""

    iput-object v5, v4, Lfpc;->d:Ljava/lang/String;

    const-string v5, ""

    iput-object v5, v4, Lfpc;->e:Ljava/lang/String;

    invoke-virtual {v4}, Lfpc;->a()Lfoy;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, Laza;->a()Lgnd;

    move-result-object v9

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Laza;->a:Lcod;

    iget-object v4, v4, Lcod;->a:Lcnu;

    invoke-virtual {v4}, Lcnu;->b()J

    move-result-wide v6

    const-wide/16 v12, 0x0

    cmp-long v4, v6, v12

    if-lez v4, :cond_2

    move-object/from16 v0, p0

    iget-wide v6, v0, Laza;->f:J

    const-wide/16 v12, 0x0

    cmp-long v4, v6, v12

    if-lez v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Laza;->a:Lcod;

    iget-object v4, v4, Lcod;->a:Lcnu;

    invoke-virtual {v4}, Lcnu;->b()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-wide v12, v0, Laza;->f:J

    add-long/2addr v6, v12

    move-object/from16 v0, p0

    iget-object v4, v0, Laza;->e:Lezj;

    invoke-virtual {v4}, Lezj;->a()J

    move-result-wide v12

    cmp-long v4, v6, v12

    if-lez v4, :cond_2

    sget-object v4, Lfpe;->a:Lfpe;

    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Laza;->b:Lcnu;

    iget-object v4, v4, Lcnu;->f:Lezf;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Laza;->b:Lcnu;

    iget-object v4, v4, Lcnu;->f:Lezf;

    invoke-virtual {v4}, Lezf;->b()J

    move-result-wide v12

    const-wide/16 v6, -0x1

    cmp-long v4, v12, v6

    if-eqz v4, :cond_3

    invoke-virtual {v5}, Lfoy;->f()Z

    move-result v4

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-wide v6, v0, Laza;->i:J

    :goto_1
    const-wide/16 v14, 0x0

    cmp-long v4, v6, v14

    if-lez v4, :cond_3

    cmp-long v4, v12, v6

    if-lez v4, :cond_3

    sget-object v4, Lfpe;->d:Lfpe;

    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget v4, v5, Lfoy;->S:I

    const/4 v6, -0x1

    if-eq v4, v6, :cond_4

    iget-object v4, v5, Lfoy;->c:Ljava/lang/String;

    if-nez v4, :cond_a

    const/4 v4, 0x0

    :goto_2
    iget-object v6, v5, Lfoy;->d:Ljava/lang/String;

    iget-object v7, v5, Lfoy;->h:Ljava/lang/String;

    invoke-interface {v9, v6, v7}, Lgnd;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v6, v5, Lfoy;->S:I

    if-lt v4, v6, :cond_4

    sget-object v4, Lfpe;->b:Lfpe;

    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Laza;->e:Lezj;

    invoke-virtual {v5, v4}, Lfoy;->a(Lezj;)Z

    move-result v4

    if-eqz v4, :cond_5

    sget-object v4, Lfpe;->c:Lfpe;

    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v4, v5, Lfoy;->c:Ljava/lang/String;

    if-eqz v4, :cond_6

    iget-object v4, v5, Lfoy;->c:Ljava/lang/String;

    invoke-interface {v9, v4}, Lgnd;->s(Ljava/lang/String;)Lglv;

    move-result-object v4

    sget-object v6, Lglv;->b:Lglv;

    if-eq v4, v6, :cond_6

    sget-object v4, Lfpe;->e:Lfpe;

    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_b

    move-object v4, v5

    :goto_3
    iget-object v5, v4, Lfoy;->c:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lfoy;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Laza;->e:Lezj;

    invoke-virtual {v6}, Lezj;->b()J

    move-result-wide v6

    iget-wide v10, v4, Lfoy;->R:J

    move-object/from16 v0, p0

    iget-object v9, v0, Laza;->e:Lezj;

    invoke-virtual {v9}, Lezj;->a()J

    move-result-wide v12

    sub-long/2addr v10, v12

    add-long/2addr v6, v10

    invoke-interface {v8, v5, v6, v7}, Lgnd;->b(Ljava/lang/String;J)Lgly;

    move-result-object v5

    invoke-virtual {v4}, Lfoy;->b()Lfpc;

    move-result-object v4

    const/4 v6, 0x0

    iput-object v6, v4, Lfpc;->r:Lfrf;

    if-eqz v5, :cond_8

    invoke-virtual {v5}, Lgly;->a()Lfqj;

    move-result-object v6

    if-eqz v6, :cond_7

    move-object/from16 v0, p0

    iget-object v6, v0, Laza;->e:Lezj;

    invoke-virtual {v6}, Lezj;->b()J

    move-result-wide v6

    iput-wide v6, v4, Lfpc;->ag:J

    invoke-virtual {v5}, Lgly;->a()Lfqj;

    move-result-object v6

    invoke-virtual {v6}, Lfqj;->a()Lhgy;

    move-result-object v6

    invoke-virtual {v4, v6}, Lfpc;->a(Lhgy;)Lfpc;

    :cond_7
    invoke-virtual {v5}, Lgly;->b()Lfqj;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-virtual {v5}, Lgly;->b()Lfqj;

    move-result-object v5

    invoke-virtual {v5}, Lfqj;->a()Lhgy;

    move-result-object v5

    invoke-virtual {v4, v5}, Lfpc;->a(Lhgy;)Lfpc;

    :cond_8
    invoke-virtual {v4}, Lfpc;->a()Lfoy;

    move-result-object v4

    iget-object v5, v4, Lfoy;->p:Lfrf;

    if-nez v5, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p0

    iget-wide v6, v0, Laza;->j:J

    goto/16 :goto_1

    :cond_a
    iget-object v4, v5, Lfoy;->c:Ljava/lang/String;

    invoke-interface {v9, v4}, Lgnd;->r(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_2

    :cond_b
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v4, v5

    :goto_4
    if-eqz v4, :cond_c

    iget-object v7, v4, Lfoy;->M:Ljava/util/List;

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v4, v4, Lfoy;->aa:Lfoy;

    goto :goto_4

    :cond_c
    new-instance v4, Lfpc;

    invoke-direct {v4}, Lfpc;-><init>()V

    iget-object v7, v5, Lfoy;->d:Ljava/lang/String;

    iput-object v7, v4, Lfpc;->c:Ljava/lang/String;

    iget-object v5, v5, Lfoy;->h:Ljava/lang/String;

    iput-object v5, v4, Lfpc;->g:Ljava/lang/String;

    iput-object v10, v4, Lfpc;->af:Ljava/util/List;

    iput-object v6, v4, Lfpc;->b:Ljava/util/List;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lfpc;->U:Z

    invoke-virtual {v4}, Lfpc;->a()Lfoy;

    move-result-object v4

    goto/16 :goto_3

    .line 192
    :cond_d
    invoke-interface {v4}, Lexd;->a()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 193
    move-object/from16 v0, p0

    iget-object v4, v0, Laza;->a:Lcod;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v4, v0, v1, v2, v3}, Lcod;->a(Less;Ljava/lang/String;Lfai;Ljava/util/Map;)Lfoy;

    move-result-object v4

    goto/16 :goto_0

    .line 196
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lfoy;)V
    .locals 3

    .prologue
    .line 301
    if-nez p1, :cond_0

    .line 312
    :goto_0
    return-void

    .line 304
    :cond_0
    invoke-direct {p0}, Laza;->a()Lgnd;

    move-result-object v0

    .line 305
    iget-boolean v1, p1, Lfoy;->af:Z

    if-eqz v1, :cond_1

    .line 306
    iget-object v1, p1, Lfoy;->d:Ljava/lang/String;

    iget-object v2, p1, Lfoy;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lgnd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v1, p1, Lfoy;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 308
    iget-object v1, p1, Lfoy;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgnd;->q(Ljava/lang/String;)V

    .line 311
    :cond_1
    iget-object v0, p0, Laza;->a:Lcod;

    invoke-virtual {v0, p1}, Lcod;->a(Lfoy;)V

    goto :goto_0
.end method

.method public final a(Lfrl;)Z
    .locals 1

    .prologue
    .line 127
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    invoke-virtual {p1}, Lfrl;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laza;->a:Lcod;

    invoke-virtual {v0, p1}, Lcod;->a(Lfrl;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
