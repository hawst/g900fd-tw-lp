.class public final enum Lbzs;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbzs;

.field public static final enum b:Lbzs;

.field public static final enum c:Lbzs;

.field private static final synthetic e:[Lbzs;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lbzs;

    const-string v1, "DISMISSED"

    invoke-direct {v0, v1, v2, v2}, Lbzs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbzs;->a:Lbzs;

    .line 46
    new-instance v0, Lbzs;

    const-string v1, "MAXIMIZED"

    invoke-direct {v0, v1, v3, v3}, Lbzs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbzs;->b:Lbzs;

    .line 47
    new-instance v0, Lbzs;

    const-string v1, "MINIMIZED"

    invoke-direct {v0, v1, v4, v4}, Lbzs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbzs;->c:Lbzs;

    .line 44
    const/4 v0, 0x3

    new-array v0, v0, [Lbzs;

    sget-object v1, Lbzs;->a:Lbzs;

    aput-object v1, v0, v2

    sget-object v1, Lbzs;->b:Lbzs;

    aput-object v1, v0, v3

    sget-object v1, Lbzs;->c:Lbzs;

    aput-object v1, v0, v4

    sput-object v0, Lbzs;->e:[Lbzs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lbzs;->d:I

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbzs;
    .locals 1

    .prologue
    .line 44
    const-class v0, Lbzs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbzs;

    return-object v0
.end method

.method public static values()[Lbzs;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lbzs;->e:[Lbzs;

    invoke-virtual {v0}, [Lbzs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbzs;

    return-object v0
.end method
