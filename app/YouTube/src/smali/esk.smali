.class public final Lesk;
.super Lgia;
.source "SourceFile"


# instance fields
.field private a:Lesh;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Lgia;-><init>()V

    return-void
.end method

.method public constructor <init>(Lesh;)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Lgia;-><init>()V

    .line 137
    iput-object p1, p0, Lesk;->a:Lesh;

    .line 138
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    return v0
.end method

.method protected final synthetic a(Lorg/json/JSONObject;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 126
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Lesh;

    const-string v0, "offsetType"

    const-class v2, Lesl;

    invoke-static {p1, v0, v2}, Lesk;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lesl;

    const-string v2, "offsetValue"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v1, v0, v2, v3}, Lesh;-><init>(Lesl;J)V

    return-object v1
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 147
    const-string v0, "offsetType"

    iget-object v1, p0, Lesk;->a:Lesh;

    iget-object v1, v1, Lesh;->a:Lesl;

    invoke-static {p1, v0, v1}, Lesk;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Enum;)V

    .line 148
    const-string v0, "offsetValue"

    iget-object v1, p0, Lesk;->a:Lesh;

    iget-wide v2, v1, Lesh;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 149
    return-void
.end method
