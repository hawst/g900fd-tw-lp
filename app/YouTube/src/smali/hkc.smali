.class public final Lhkc;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhjx;

.field public b:[Lhkd;

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lidf;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhkc;->a:Lhjx;

    sget-object v0, Lhkd;->a:[Lhkd;

    iput-object v0, p0, Lhkc;->b:[Lhkd;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhkc;->c:J

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhkc;->a:Lhjx;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iget-object v2, p0, Lhkc;->a:Lhjx;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhkc;->b:[Lhkd;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhkc;->b:[Lhkd;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    const/4 v5, 0x2

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-wide v2, p0, Lhkc;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Lhkc;->c:J

    invoke-static {v1, v2, v3}, Lidd;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhkc;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhkc;->J:I

    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhkc;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhkc;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhkc;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhkc;->a:Lhjx;

    if-nez v0, :cond_2

    new-instance v0, Lhjx;

    invoke-direct {v0}, Lhjx;-><init>()V

    iput-object v0, p0, Lhkc;->a:Lhjx;

    :cond_2
    iget-object v0, p0, Lhkc;->a:Lhjx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhkc;->b:[Lhkd;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhkd;

    iget-object v3, p0, Lhkc;->b:[Lhkd;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhkc;->b:[Lhkd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhkc;->b:[Lhkd;

    :goto_2
    iget-object v2, p0, Lhkc;->b:[Lhkd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhkc;->b:[Lhkd;

    new-instance v3, Lhkd;

    invoke-direct {v3}, Lhkd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhkc;->b:[Lhkd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhkc;->b:[Lhkd;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhkc;->b:[Lhkd;

    new-instance v3, Lhkd;

    invoke-direct {v3}, Lhkd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhkc;->b:[Lhkd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lhkc;->c:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhkc;->a:Lhjx;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhkc;->a:Lhjx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhkc;->b:[Lhkd;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lhkc;->b:[Lhkd;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lhkc;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    iget-wide v2, p0, Lhkc;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(IJ)V

    :cond_3
    iget-object v0, p0, Lhkc;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhkc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhkc;

    iget-object v2, p0, Lhkc;->a:Lhjx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhkc;->a:Lhjx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhkc;->b:[Lhkd;

    iget-object v3, p1, Lhkc;->b:[Lhkd;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lhkc;->c:J

    iget-wide v4, p1, Lhkc;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhkc;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhkc;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhkc;->a:Lhjx;

    iget-object v3, p1, Lhkc;->a:Lhjx;

    invoke-virtual {v2, v3}, Lhjx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhkc;->I:Ljava/util/List;

    iget-object v3, p1, Lhkc;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkc;->a:Lhjx;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, Lhkc;->b:[Lhkd;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-wide v2, p0, Lhkc;->c:J

    iget-wide v4, p0, Lhkc;->c:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhkc;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lhkc;->a:Lhjx;

    invoke-virtual {v0}, Lhjx;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Lhkc;->b:[Lhkd;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhkc;->b:[Lhkd;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_3
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lhkc;->b:[Lhkd;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhkd;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lhkc;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
