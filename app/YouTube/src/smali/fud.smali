.class public abstract Lfud;
.super Lftx;
.source "SourceFile"

# interfaces
.implements Lfuv;


# instance fields
.field final a:Lfsi;

.field final b:Lfvc;

.field private final c:Levn;

.field private d:Lewh;

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>(Lfdg;Lfsl;Levn;Leyt;)V
    .locals 4

    .prologue
    .line 43
    invoke-static {}, Levn;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p1, p3, v0, p4}, Lftx;-><init>(Lfdg;Levn;Ljava/lang/Object;Leyt;)V

    .line 34
    invoke-static {}, Lezw;->a()Lewh;

    move-result-object v0

    iput-object v0, p0, Lfud;->d:Lewh;

    .line 44
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lfud;->c:Levn;

    .line 45
    new-instance v0, Lfsi;

    invoke-direct {v0, p2}, Lfsi;-><init>(Lfsl;)V

    iput-object v0, p0, Lfud;->a:Lfsi;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfud;->e:Z

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfud;->f:Z

    .line 51
    const-class v0, Lfud;

    invoke-virtual {p0}, Lfud;->c()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, p0, v0, v1}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 53
    new-instance v0, Lfvc;

    .line 54
    invoke-virtual {p0}, Lfud;->c()Ljava/lang/Object;

    move-result-object v1

    new-instance v2, Lfue;

    invoke-direct {v2, p0}, Lfue;-><init>(Lfud;)V

    new-instance v3, Lfuf;

    invoke-direct {v3, p0}, Lfuf;-><init>(Lfud;)V

    invoke-direct {v0, v1, v2, v3}, Lfvc;-><init>(Ljava/lang/Object;Landroid/view/View$OnClickListener;Lfve;)V

    iput-object v0, p0, Lfud;->b:Lfvc;

    .line 67
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lfud;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfud;->e:Z

    if-nez v0, :cond_0

    .line 180
    invoke-direct {p0}, Lfud;->j()V

    .line 184
    :goto_0
    return-void

    .line 182
    :cond_0
    invoke-direct {p0}, Lfud;->k()V

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 190
    invoke-direct {p0}, Lfud;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lfud;->a:Lfsi;

    invoke-direct {p0}, Lfud;->m()I

    move-result v1

    iget-object v2, v0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, v0, Lfsi;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lfsi;->notifyDataSetChanged()V

    .line 193
    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 199
    invoke-direct {p0}, Lfud;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lfud;->a:Lfsi;

    iget-object v1, p0, Lfud;->b:Lfvc;

    invoke-virtual {v0, v1}, Lfsi;->b(Ljava/lang/Object;)V

    .line 202
    :cond_0
    return-void
.end method

.method private l()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 205
    iget-object v1, p0, Lfud;->a:Lfsi;

    invoke-virtual {v1}, Lfsi;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 206
    iget-object v1, p0, Lfud;->a:Lfsi;

    invoke-direct {p0}, Lfud;->m()I

    move-result v2

    invoke-virtual {v1, v2}, Lfsi;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lfud;->b:Lfvc;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 208
    :cond_0
    return v0
.end method

.method private m()I
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lfud;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 115
    invoke-direct {p0}, Lfud;->j()V

    .line 116
    iget-object v0, p0, Lfud;->a:Lfsi;

    iget-object v1, p0, Lfud;->d:Lewh;

    invoke-virtual {v0}, Lfsi;->getCount()I

    move-result v2

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1, p1}, Lewh;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2, p1}, Lfsi;->a(ILjava/lang/Object;)V

    .line 117
    :cond_0
    invoke-direct {p0}, Lfud;->i()V

    .line 118
    return-void
.end method

.method protected final a(Ljava/util/Collection;)V
    .locals 7

    .prologue
    .line 131
    invoke-direct {p0}, Lfud;->j()V

    .line 132
    iget-object v0, p0, Lfud;->a:Lfsi;

    iget-object v1, p0, Lfud;->d:Lewh;

    invoke-virtual {v0}, Lfsi;->getCount()I

    move-result v2

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v1, v5}, Lewh;->a(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v0, v2, v3}, Lfsi;->a(ILjava/util/Collection;)V

    .line 133
    :cond_2
    invoke-direct {p0}, Lfud;->i()V

    .line 134
    return-void
.end method

.method protected final a(Z)V
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lfud;->e:Z

    if-eq v0, p1, :cond_0

    .line 138
    iput-boolean p1, p0, Lfud;->e:Z

    .line 139
    invoke-direct {p0}, Lfud;->i()V

    .line 141
    :cond_0
    return-void
.end method

.method public final b()Lfsi;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lfud;->a:Lfsi;

    return-object v0
.end method

.method protected final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 121
    if-eqz p1, :cond_0

    .line 122
    iget-object v0, p0, Lfud;->a:Lfsi;

    invoke-virtual {v0, p1}, Lfsi;->a(Ljava/lang/Object;)Z

    .line 124
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lfud;->c:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 88
    return-void
.end method

.method protected final h()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lfud;->a:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 107
    invoke-virtual {p0}, Lfud;->e()V

    .line 108
    return-void
.end method

.method public onContentEvent(Lfua;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lfud;->b:Lfvc;

    iput-object p1, v0, Lfvc;->d:Lbt;

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfud;->f:Z

    .line 170
    iget-boolean v0, p0, Lfud;->e:Z

    if-nez v0, :cond_0

    .line 171
    invoke-direct {p0}, Lfud;->j()V

    .line 173
    :cond_0
    return-void
.end method

.method public onErrorEvent(Lfub;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lfud;->b:Lfvc;

    iput-object p1, v0, Lfvc;->d:Lbt;

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfud;->f:Z

    .line 163
    invoke-direct {p0}, Lfud;->k()V

    .line 164
    return-void
.end method

.method public onLoadingEvent(Lfuc;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lfud;->b:Lfvc;

    iput-object p1, v0, Lfvc;->d:Lbt;

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfud;->f:Z

    .line 156
    invoke-direct {p0}, Lfud;->k()V

    .line 157
    return-void
.end method
