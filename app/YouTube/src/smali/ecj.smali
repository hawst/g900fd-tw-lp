.class public abstract Lecj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefu;


# instance fields
.field public final a:Lecw;

.field public final b:I

.field final c:Lefc;

.field final d:Lefg;

.field e:Lefx;


# direct methods
.method public constructor <init>(Lefc;Lefg;Lecw;I)V
    .locals 4

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iget-wide v0, p2, Lefg;->e:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 50
    invoke-static {p1}, La;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefc;

    iput-object v0, p0, Lecj;->c:Lefc;

    .line 51
    invoke-static {p2}, La;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefg;

    iput-object v0, p0, Lecj;->d:Lefg;

    .line 52
    invoke-static {p3}, La;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecw;

    iput-object v0, p0, Lecj;->a:Lecw;

    .line 53
    iput p4, p0, Lecj;->b:I

    .line 54
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lecj;->e:Lefx;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->j()V

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lecj;->e:Lefx;

    .line 75
    :cond_0
    return-void
.end method

.method public a(Lefx;)V
    .locals 0

    .prologue
    .line 135
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lecj;->e:Lefx;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 121
    iget-object v0, p0, Lecj;->e:Lefx;

    invoke-virtual {p0, v0}, Lecj;->a(Lefx;)V

    .line 122
    return-void

    .line 120
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final c()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lecj;->e:Lefx;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->b()V

    .line 147
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->d()V

    .line 154
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->e()Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->f()V

    .line 164
    return-void
.end method
