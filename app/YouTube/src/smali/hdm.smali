.class public final Lhdm;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhdm;


# instance fields
.field private b:Lhdh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31559
    const/4 v0, 0x0

    new-array v0, v0, [Lhdm;

    sput-object v0, Lhdm;->a:[Lhdm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31560
    invoke-direct {p0}, Lidf;-><init>()V

    .line 31563
    const/4 v0, 0x0

    iput-object v0, p0, Lhdm;->b:Lhdh;

    .line 31560
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 31600
    const/4 v0, 0x0

    .line 31601
    iget-object v1, p0, Lhdm;->b:Lhdh;

    if-eqz v1, :cond_0

    .line 31602
    const v0, 0x3b6687b

    iget-object v1, p0, Lhdm;->b:Lhdh;

    .line 31603
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 31605
    :cond_0
    iget-object v1, p0, Lhdm;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31606
    iput v0, p0, Lhdm;->J:I

    .line 31607
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 31556
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhdm;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhdm;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhdm;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhdm;->b:Lhdh;

    if-nez v0, :cond_2

    new-instance v0, Lhdh;

    invoke-direct {v0}, Lhdh;-><init>()V

    iput-object v0, p0, Lhdm;->b:Lhdh;

    :cond_2
    iget-object v0, p0, Lhdm;->b:Lhdh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1db343da -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 31591
    iget-object v0, p0, Lhdm;->b:Lhdh;

    if-eqz v0, :cond_0

    .line 31592
    const v0, 0x3b6687b

    iget-object v1, p0, Lhdm;->b:Lhdh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 31594
    :cond_0
    iget-object v0, p0, Lhdm;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 31596
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 31574
    if-ne p1, p0, :cond_1

    .line 31578
    :cond_0
    :goto_0
    return v0

    .line 31575
    :cond_1
    instance-of v2, p1, Lhdm;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 31576
    :cond_2
    check-cast p1, Lhdm;

    .line 31577
    iget-object v2, p0, Lhdm;->b:Lhdh;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhdm;->b:Lhdh;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhdm;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhdm;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 31578
    goto :goto_0

    .line 31577
    :cond_4
    iget-object v2, p0, Lhdm;->b:Lhdh;

    iget-object v3, p1, Lhdm;->b:Lhdh;

    invoke-virtual {v2, v3}, Lhdh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhdm;->I:Ljava/util/List;

    iget-object v3, p1, Lhdm;->I:Ljava/util/List;

    .line 31578
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 31582
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 31584
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdm;->b:Lhdh;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 31585
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhdm;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 31586
    return v0

    .line 31584
    :cond_0
    iget-object v0, p0, Lhdm;->b:Lhdh;

    invoke-virtual {v0}, Lhdh;->hashCode()I

    move-result v0

    goto :goto_0

    .line 31585
    :cond_1
    iget-object v1, p0, Lhdm;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
