.class final Lgns;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/database/Cursor;

.field private final b:Lgmt;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:I

.field private final s:I


# direct methods
.method constructor <init>(Landroid/database/Cursor;Lgmt;)V
    .locals 1

    .prologue
    .line 732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 733
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iput-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    .line 734
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmt;

    iput-object v0, p0, Lgns;->b:Lgmt;

    .line 736
    const-string v0, "id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->c:I

    .line 737
    const-string v0, "watch_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->d:I

    .line 738
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->e:I

    .line 739
    const-string v0, "duration"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->f:I

    .line 740
    const-string v0, "view_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->g:I

    .line 741
    const-string v0, "likes_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->h:I

    .line 742
    const-string v0, "dislikes_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->i:I

    .line 743
    const-string v0, "owner"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->j:I

    .line 744
    const-string v0, "owner_display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->k:I

    .line 745
    const-string v0, "owner_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->l:I

    .line 746
    const-string v0, "upload_date"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->m:I

    .line 747
    const-string v0, "published_date"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->n:I

    .line 748
    const-string v0, "tags"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->o:I

    .line 749
    const-string v0, "description"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->p:I

    .line 750
    const-string v0, "subtitle_tracks_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->q:I

    .line 751
    const-string v0, "state"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->r:I

    .line 753
    const-string v0, "video_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgns;->s:I

    .line 754
    return-void
.end method

.method static synthetic a(Lgns;)Ljava/util/List;
    .locals 2

    .prologue
    .line 708
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lgns;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    iget-object v1, p0, Lgns;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lgns;->a()Lgcd;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method final a()Lgcd;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    .line 760
    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v1, p0, Lgns;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lgns;->s:I

    if-ltz v0, :cond_0

    .line 761
    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v1, p0, Lgns;->s:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 762
    new-instance v1, Lgcf;

    invoke-direct {v1}, Lgcf;-><init>()V

    .line 763
    iput-object v0, v1, Lgcf;->a:Ljava/lang/String;

    sget-object v0, Lgch;->d:Lgch;

    .line 764
    invoke-virtual {v1, v0}, Lgcf;->a(Lgch;)Lgcf;

    move-result-object v0

    .line 765
    invoke-virtual {v0}, Lgcf;->a()Lgcd;

    move-result-object v0

    .line 807
    :goto_0
    return-object v0

    .line 768
    :cond_0
    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v1, p0, Lgns;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 769
    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v1, p0, Lgns;->j:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 770
    iget-object v0, p0, Lgns;->b:Lgmt;

    invoke-static {v3}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0, v3}, Lgmt;->e(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 771
    iget-object v0, p0, Lgns;->b:Lgmt;

    invoke-static {v3}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0, v3}, Lgmt;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 773
    if-eqz v4, :cond_2

    .line 774
    iget-object v0, p0, Lgns;->b:Lgmt;

    invoke-static {v4}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0, v4}, Lgmt;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 777
    :goto_1
    new-instance v7, Lgcf;

    invoke-direct {v7}, Lgcf;-><init>()V

    .line 778
    iput-object v3, v7, Lgcf;->a:Ljava/lang/String;

    .line 779
    iput-object v6, v7, Lgcf;->d:Landroid/net/Uri;

    .line 780
    iput-object v6, v7, Lgcf;->e:Landroid/net/Uri;

    .line 781
    iput-object v5, v7, Lgcf;->f:Landroid/net/Uri;

    .line 782
    iput-object v5, v7, Lgcf;->g:Landroid/net/Uri;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->d:I

    .line 783
    invoke-static {v0, v3}, Levj;->b(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v7, Lgcf;->c:Landroid/net/Uri;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->e:I

    .line 784
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lgcf;->j:Ljava/lang/String;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->f:I

    .line 785
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lgcf;->k:I

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->g:I

    .line 786
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v7, Lgcf;->l:J

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->h:I

    .line 787
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v7, Lgcf;->m:J

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->i:I

    .line 788
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v7, Lgcf;->n:J

    .line 789
    iput-object v4, v7, Lgcf;->o:Ljava/lang/String;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->l:I

    .line 790
    invoke-static {v0, v3}, Levj;->b(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v7, Lgcf;->p:Landroid/net/Uri;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->m:I

    .line 791
    invoke-static {v0, v3}, Levj;->a(Landroid/database/Cursor;I)Ljava/util/Date;

    move-result-object v0

    iput-object v0, v7, Lgcf;->q:Ljava/util/Date;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->n:I

    .line 792
    invoke-static {v0, v3}, Levj;->a(Landroid/database/Cursor;I)Ljava/util/Date;

    move-result-object v0

    iput-object v0, v7, Lgcf;->r:Ljava/util/Date;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->o:I

    .line 793
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lgcf;->u:Ljava/lang/String;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->p:I

    .line 794
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lgcf;->v:Ljava/lang/String;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->k:I

    .line 795
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lgcf;->G:Ljava/lang/String;

    iget-object v0, p0, Lgns;->a:Landroid/database/Cursor;

    iget v3, p0, Lgns;->q:I

    .line 796
    invoke-static {v0, v3}, Levj;->b(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v7, Lgcf;->i:Landroid/net/Uri;

    const-class v0, Lgch;

    iget-object v3, p0, Lgns;->a:Landroid/database/Cursor;

    iget v4, p0, Lgns;->r:I

    .line 797
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgch;

    invoke-virtual {v7, v0}, Lgcf;->a(Lgch;)Lgcf;

    move-result-object v0

    .line 798
    iput-boolean v10, v0, Lgcf;->H:Z

    if-eqz v1, :cond_1

    .line 800
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 799
    :cond_1
    iput-object v2, v0, Lgcf;->I:Ljava/lang/String;

    .line 806
    iput-boolean v10, v0, Lgcf;->A:Z

    .line 807
    invoke-virtual {v0}, Lgcf;->a()Lgcd;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    move-object v1, v2

    goto/16 :goto_1
.end method
