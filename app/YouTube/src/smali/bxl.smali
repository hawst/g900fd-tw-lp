.class public final Lbxl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhe;


# instance fields
.field private synthetic a:Lcom/google/android/apps/youtube/app/ui/TabbedView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Lcom/google/android/apps/youtube/app/ui/TabbedView;IZ)V

    .line 211
    return-void
.end method

.method public final a(IF)V
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Lcom/google/android/apps/youtube/app/ui/TabbedView;I)I

    .line 193
    iget-object v0, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Lcom/google/android/apps/youtube/app/ui/TabbedView;F)F

    .line 194
    iget-object v0, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 195
    iget-object v0, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxp;

    iget-object v0, v0, Lbxp;->a:Landroid/view/View;

    .line 196
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    add-int/2addr v0, v1

    .line 197
    if-eqz p1, :cond_1

    .line 199
    iget-object v1, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b(Lcom/google/android/apps/youtube/app/ui/TabbedView;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 203
    :goto_0
    iget-object v1, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/widget/HorizontalScrollView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 204
    iget-object v0, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->e(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->invalidate(Landroid/graphics/Rect;)V

    .line 206
    :cond_0
    return-void

    .line 201
    :cond_1
    int-to-float v0, v0

    iget-object v1, p0, Lbxl;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b(Lcom/google/android/apps/youtube/app/ui/TabbedView;)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method
