.class public final Lgld;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgju;


# instance fields
.field private final b:Levk;

.field private final c:Landroid/util/SparseArray;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Lght;

.field private final f:Lgll;

.field private final g:Lezj;

.field private final h:Lexd;


# direct methods
.method public constructor <init>(Levk;Ljava/util/Set;Ljava/util/concurrent/Executor;Lght;Lgll;Lezj;Lexd;)V
    .locals 4

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levk;

    iput-object v0, p0, Lgld;->b:Levk;

    .line 58
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lgld;->d:Ljava/util/concurrent/Executor;

    .line 59
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lght;

    iput-object v0, p0, Lgld;->e:Lght;

    .line 60
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgll;

    iput-object v0, p0, Lgld;->f:Lgll;

    .line 61
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgld;->g:Lezj;

    .line 62
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lgld;->h:Lexd;

    .line 65
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lgld;->c:Landroid/util/SparseArray;

    .line 66
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgln;

    .line 67
    iget-object v2, p0, Lgld;->c:Landroid/util/SparseArray;

    invoke-interface {v0}, Lgln;->a()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 69
    :cond_0
    return-void
.end method

.method static synthetic a(Lgld;)Lgll;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgld;->f:Lgll;

    return-object v0
.end method

.method private a(Ldzy;)V
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lgld;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lglh;

    invoke-direct {v1, p0, p1}, Lglh;-><init>(Lgld;Ldzy;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 365
    return-void
.end method

.method static synthetic a(Lgld;Ldzy;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lgld;->a(Ldzy;)V

    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 322
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 324
    add-int/lit8 v0, v0, 0x1

    .line 325
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    :goto_0
    return-void

    .line 327
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic a(Lgld;Ldzy;Lxa;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 39
    iget v2, p1, Ldzy;->k:I

    iget-object v3, p0, Lgld;->e:Lght;

    invoke-virtual {v3}, Lght;->g()I

    move-result v3

    if-ge v2, v3, :cond_6

    const/4 v2, 0x0

    instance-of v3, p2, Lgjj;

    if-eqz v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_6

    :goto_1
    return v0

    :cond_0
    instance-of v3, p2, Lwl;

    if-eqz v3, :cond_3

    check-cast p2, Lwl;

    iget-object v2, p2, Lwl;->a:Lwm;

    :cond_1
    :goto_2
    if-eqz v2, :cond_5

    iget v3, v2, Lwm;->a:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x2d

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Status code of errored request is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lezp;->e(Ljava/lang/String;)V

    iget v3, v2, Lwm;->a:I

    const/16 v4, 0x190

    if-eq v3, v4, :cond_2

    iget v2, v2, Lwm;->a:I

    const/16 v3, 0x193

    if-ne v2, v3, :cond_4

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    instance-of v3, p2, Lwy;

    if-eqz v3, :cond_1

    check-cast p2, Lwy;

    iget-object v2, p2, Lwy;->a:Lwm;

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    const-string v2, "Network response was not present, request is retryable."

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    move v2, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lgld;)Levk;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgld;->b:Levk;

    return-object v0
.end method

.method private b()Ljava/util/List;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 171
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 176
    iget-object v1, p0, Lgld;->b:Levk;

    .line 177
    invoke-virtual {v1}, Levk;->d()Levl;

    move-result-object v4

    move v1, v0

    move v2, v0

    .line 178
    :goto_0
    invoke-interface {v4}, Levl;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    invoke-interface {v4}, Levl;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzy;

    .line 181
    iget-object v5, p0, Lgld;->e:Lght;

    invoke-virtual {v5}, Lght;->d()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 182
    add-int/lit8 v1, v1, 0x1

    .line 183
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move v0, v1

    .line 185
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    .line 186
    goto :goto_0

    .line 187
    :cond_1
    invoke-interface {v4}, Levl;->a()V

    .line 188
    iget-object v0, p0, Lgld;->f:Lgll;

    sub-int v1, v2, v1

    invoke-virtual {v0, v1}, Lgll;->b(I)V

    .line 191
    iget-object v0, p0, Lgld;->b:Levk;

    invoke-virtual {v0}, Levk;->a()V

    .line 193
    :try_start_0
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzy;

    .line 194
    iget-object v2, p0, Lgld;->b:Levk;

    iget-object v0, v0, Ldzy;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Levk;->a(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 198
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgld;->b:Levk;

    invoke-virtual {v1}, Levk;->b()V

    throw v0

    .line 196
    :cond_2
    :try_start_1
    iget-object v0, p0, Lgld;->b:Levk;

    invoke-virtual {v0}, Levk;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 198
    iget-object v0, p0, Lgld;->b:Levk;

    invoke-virtual {v0}, Levk;->b()V

    .line 200
    return-object v3
.end method

.method static synthetic c(Lgld;)Lexd;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgld;->h:Lexd;

    return-object v0
.end method

.method static synthetic d(Lgld;)Lezj;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgld;->g:Lezj;

    return-object v0
.end method

.method static synthetic e(Lgld;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgld;->d:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 100
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lb;->b()V

    .line 103
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lgld;->b:Levk;

    invoke-virtual {v0}, Levk;->d()Levl;

    move-result-object v9

    move v1, v3

    move v4, v2

    :goto_0
    invoke-interface {v9}, Levl;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Levl;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzy;

    add-int/lit8 v5, v4, 0x1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lgld;->f:Lgll;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v10, p0, Lgld;->g:Lezj;

    invoke-virtual {v10}, Lezj;->a()J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v10

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v12, v0, Ldzy;->h:J

    invoke-virtual {v4, v12, v13}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v12

    sub-long/2addr v10, v12

    invoke-virtual {v1, v10, v11}, Lgll;->a(J)V

    move v4, v2

    :goto_1
    iget-object v1, p0, Lgld;->c:Landroid/util/SparseArray;

    iget v10, v0, Ldzy;->b:I

    invoke-virtual {v1, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgln;

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lgln;->a(Ldzy;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, v0, Ldzy;->a:Ljava/lang/String;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Ldzy;->i:Ljava/lang/String;

    invoke-static {v8, v0}, Lgld;->a(Ljava/util/Map;Ljava/lang/String;)V

    move v1, v4

    move v4, v5

    goto :goto_0

    :cond_1
    new-instance v1, Lgli;

    iget-object v10, v0, Ldzy;->a:Ljava/lang/String;

    iget-object v0, v0, Ldzy;->i:Ljava/lang/String;

    invoke-direct {v1, v10, v0}, Lgli;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v4

    move v4, v5

    goto :goto_0

    :cond_2
    invoke-interface {v9}, Levl;->a()V

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lgld;->e:Lght;

    invoke-virtual {v1}, Lght;->b()I

    move-result v1

    if-le v0, v1, :cond_3

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lgld;->e:Lght;

    invoke-virtual {v1}, Lght;->b()I

    move-result v1

    sub-int v5, v0, v1

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_3

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgli;

    iget-object v9, v0, Lgli;->a:Ljava/lang/String;

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lgli;->b:Ljava/lang/String;

    invoke-static {v8, v0}, Lgld;->a(Ljava/util/Map;Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lgld;->f:Lgll;

    invoke-virtual {v0, v8}, Lgll;->a(Ljava/util/Map;)V

    iget-object v0, p0, Lgld;->f:Lgll;

    invoke-virtual {v0, v4}, Lgll;->a(I)V

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lgld;->b:Levk;

    invoke-virtual {v0}, Levk;->a()V

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lgld;->b:Levk;

    invoke-virtual {v4, v0}, Levk;->a(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 103
    :cond_4
    :try_start_1
    iget-object v0, p0, Lgld;->b:Levk;

    invoke-virtual {v0}, Levk;->c()V

    iget-object v0, p0, Lgld;->b:Levk;

    invoke-virtual {v0}, Levk;->b()V

    :cond_5
    move v0, v2

    .line 107
    :goto_4
    iget-object v1, p0, Lgld;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 109
    iget-object v1, p0, Lgld;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 110
    iget-object v4, p0, Lgld;->c:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 114
    :cond_6
    invoke-direct {p0}, Lgld;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzy;

    .line 116
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lgld;->e:Lght;

    invoke-virtual {v5}, Lght;->h()I

    move-result v5

    int-to-long v6, v5

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v5

    iget v6, v0, Ldzy;->k:I

    invoke-virtual {v5, v6}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    iget-wide v8, v0, Ldzy;->l:J

    add-long/2addr v6, v8

    iget-object v1, p0, Lgld;->g:Lezj;

    invoke-virtual {v1}, Lezj;->a()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-lez v1, :cond_7

    move v1, v3

    :goto_6
    if-eqz v1, :cond_8

    .line 117
    invoke-direct {p0, v0}, Lgld;->a(Ldzy;)V

    goto :goto_5

    :cond_7
    move v1, v2

    .line 116
    goto :goto_6

    .line 123
    :cond_8
    new-instance v5, Lglf;

    invoke-direct {v5, p0, v0}, Lglf;-><init>(Lgld;Ldzy;)V

    .line 152
    iget v1, v0, Ldzy;->b:I

    .line 153
    iget-object v6, p0, Lgld;->c:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgln;

    invoke-interface {v1, v0, v5}, Lgln;->a(Ldzy;Lwv;)V

    goto :goto_5

    :cond_9
    move v0, v2

    .line 158
    :goto_7
    iget-object v1, p0, Lgld;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 160
    iget-object v1, p0, Lgld;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 161
    iget-object v2, p0, Lgld;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 163
    :cond_a
    monitor-exit p0

    return-void

    :cond_b
    move v4, v1

    goto/16 :goto_1
.end method

.method public final declared-synchronized a(Lgjw;)V
    .locals 2

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgld;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lgle;

    invoke-direct {v1, p0, p1}, Lgle;-><init>(Lgld;Lgjw;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    monitor-exit p0

    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
