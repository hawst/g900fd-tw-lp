.class public final Lccf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field private final a:Lfsj;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lbnv;

.field private f:Lfix;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;ILfhz;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lccf;->a:Lfsj;

    .line 41
    const/4 v0, 0x0

    invoke-static {p1, p3, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lccf;->b:Landroid/view/View;

    .line 42
    iget-object v0, p0, Lccf;->b:Landroid/view/View;

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccf;->c:Landroid/widget/TextView;

    .line 43
    iget-object v0, p0, Lccf;->b:Landroid/view/View;

    const v1, 0x7f080120

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccf;->d:Landroid/widget/TextView;

    .line 44
    iget-object v0, p0, Lccf;->b:Landroid/view/View;

    const v1, 0x7f080121

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 45
    new-instance v1, Lbnv;

    invoke-direct {v1, p4, v0}, Lbnv;-><init>(Lfhz;Landroid/widget/TextView;)V

    iput-object v1, p0, Lccf;->e:Lbnv;

    .line 47
    iget-object v0, p0, Lccf;->b:Landroid/view/View;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 48
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 26
    check-cast p2, Lfix;

    iget-object v0, p0, Lccf;->f:Lfix;

    if-eq v0, p2, :cond_3

    iput-object p2, p0, Lccf;->f:Lfix;

    iget-object v0, p0, Lccf;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lfix;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    iget-object v1, p2, Lfix;->a:Lhcg;

    iget-object v1, v1, Lhcg;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfix;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v1, p2, Lfix;->b:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lccf;->d:Landroid/widget/TextView;

    iget-object v1, p2, Lfix;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfix;->a:Lhcg;

    iget-object v1, v1, Lhcg;->b:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfix;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfix;->c:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lccf;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lccf;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lccf;->e:Lbnv;

    iget-object v1, p2, Lfix;->d:Lfit;

    if-nez v1, :cond_2

    iget-object v1, p2, Lfix;->a:Lhcg;

    iget-object v1, v1, Lhcg;->c:Lhbt;

    if-eqz v1, :cond_2

    iget-object v1, p2, Lfix;->a:Lhcg;

    iget-object v1, v1, Lhcg;->c:Lhbt;

    iget-object v1, v1, Lhbt;->a:Lhbs;

    if-eqz v1, :cond_2

    new-instance v1, Lfit;

    iget-object v2, p2, Lfix;->a:Lhcg;

    iget-object v2, v2, Lhcg;->c:Lhbt;

    iget-object v2, v2, Lhbt;->a:Lhbs;

    invoke-direct {v1, v2}, Lfit;-><init>(Lhbs;)V

    iput-object v1, p2, Lfix;->d:Lfit;

    :cond_2
    iget-object v1, p2, Lfix;->d:Lfit;

    invoke-virtual {v0, v1}, Lbnv;->a(Lfit;)V

    :cond_3
    iget-object v0, p0, Lccf;->a:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
