.class final Lawu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lawm;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/ProgressBar;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/view/View;

.field private synthetic i:Lawt;


# direct methods
.method constructor <init>(Lawt;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 72
    iput-object p1, p0, Lawu;->i:Lawt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p2, p0, Lawu;->a:Landroid/view/View;

    .line 74
    const v0, 0x7f08008b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lawu;->b:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f08031e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lawu;->c:Landroid/widget/ImageView;

    .line 76
    const v0, 0x7f080131

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lawu;->d:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0800de

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lawu;->e:Landroid/widget/ProgressBar;

    .line 78
    const v0, 0x7f080276

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lawu;->f:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f08031f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lawu;->g:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f080130

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 81
    iput-object v0, p0, Lawu;->h:Landroid/view/View;

    .line 82
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 61
    check-cast p2, Lgjm;

    iget-object v0, p0, Lawu;->i:Lawt;

    iget-object v0, v0, Lawt;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2, p0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lawu;->i:Lawt;

    iget-object v0, v0, Lawt;->b:Lboi;

    iget-object v1, p0, Lawu;->h:Landroid/view/View;

    invoke-virtual {v0, v1, p2}, Lboi;->a(Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lawu;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lgjm;->g:Lgje;

    const-string v1, "username"

    invoke-virtual {v0, v1}, Lgje;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lawu;->d:Landroid/widget/TextView;

    iget-object v1, p2, Lgjm;->g:Lgje;

    const-string v2, "username"

    invoke-virtual {v1, v2, v4}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lawu;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lawu;->b:Landroid/widget/TextView;

    iget-object v1, p2, Lgjm;->g:Lgje;

    const-string v2, "upload_title"

    invoke-virtual {v1, v2, v4}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lawu;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lgjm;->g:Lgje;

    const-string v1, "upload_file_thumbnail"

    invoke-virtual {v0, v1}, Lgje;->b(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lawu;->c:Landroid/widget/ImageView;

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_2
    iget-object v0, p0, Lawu;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lgjm;->c:Lgjn;

    sget-object v1, Lgjn;->b:Lgjn;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lawu;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lawu;->i:Lawt;

    iget-object v1, v1, Lawt;->a:Landroid/content/Context;

    iget-wide v2, p2, Lgjm;->f:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_0
    invoke-virtual {p0, p2}, Lawu;->a(Lgjm;)V

    iget-object v0, p0, Lawu;->a:Landroid/view/View;

    return-object v0

    :cond_4
    iget-object v0, p0, Lawu;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method final a(Lgjm;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 122
    .line 123
    const/high16 v2, 0x42c80000    # 100.0f

    iget-wide v4, p1, Lgjm;->e:J

    long-to-float v3, v4

    mul-float/2addr v2, v3

    iget-wide v4, p1, Lgjm;->f:J

    long-to-float v3, v4

    div-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    move-result v2

    float-to-int v2, v2

    .line 125
    iget-object v3, p0, Lawu;->e:Landroid/widget/ProgressBar;

    if-eqz v3, :cond_0

    .line 126
    iget-object v3, p1, Lgjm;->c:Lgjn;

    sget-object v4, Lgjn;->d:Lgjn;

    if-ne v3, v4, :cond_2

    .line 127
    iget-object v3, p0, Lawu;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 133
    :cond_0
    :goto_0
    iget-object v3, p0, Lawu;->g:Landroid/widget/TextView;

    if-eqz v3, :cond_6

    .line 134
    iget-object v3, p1, Lgjm;->c:Lgjn;

    sget-object v4, Lgjn;->b:Lgjn;

    if-ne v3, v4, :cond_4

    .line 135
    iget-wide v4, p1, Lgjm;->f:J

    iget-wide v6, p1, Lgjm;->e:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 136
    iget-object v1, p0, Lawu;->g:Landroid/widget/TextView;

    const v2, 0x7f09026e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 149
    :goto_1
    iget-object v1, p0, Lawu;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 150
    iget-object v1, p0, Lawu;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 151
    if-nez v0, :cond_1

    .line 152
    iget-object v0, p0, Lawu;->i:Lawt;

    iget-object v0, v0, Lawt;->b:Lboi;

    invoke-virtual {v0}, Lboi;->b()V

    .line 154
    :cond_1
    return-void

    .line 129
    :cond_2
    iget-object v3, p0, Lawu;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 140
    :cond_3
    iget-object v3, p0, Lawu;->g:Landroid/widget/TextView;

    iget-object v4, p0, Lawu;->i:Lawt;

    iget-object v4, v4, Lawt;->a:Landroid/content/Context;

    const v5, 0x7f09027b

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    goto :goto_1

    .line 142
    :cond_4
    iget-object v0, p1, Lgjm;->c:Lgjn;

    sget-object v2, Lgjn;->d:Lgjn;

    if-ne v0, v2, :cond_5

    .line 143
    iget-object v0, p0, Lawu;->g:Landroid/widget/TextView;

    const v2, 0x7f090270

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    move v0, v1

    goto :goto_1

    .line 145
    :cond_5
    iget-object v0, p0, Lawu;->g:Landroid/widget/TextView;

    const v2, 0x7f09026f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_6
    move v0, v1

    goto :goto_1
.end method
