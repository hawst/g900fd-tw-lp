.class public final Lcec;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Leyp;

.field private final c:Lfhz;

.field private final d:Lfsj;

.field private final e:Lboi;

.field private final f:Lfdw;

.field private final g:Lfrz;

.field private h:Lcee;

.field private i:Lcee;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;Lboi;Lfdw;Lfrz;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcec;->a:Landroid/content/Context;

    .line 51
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcec;->b:Leyp;

    .line 52
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcec;->c:Lfhz;

    .line 53
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcec;->d:Lfsj;

    .line 54
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcec;->e:Lboi;

    .line 55
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lcec;->f:Lfdw;

    .line 57
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lcec;->g:Lfrz;

    .line 59
    const/4 v0, 0x1

    invoke-interface {p4, v0}, Lfsj;->a(Z)V

    .line 60
    return-void
.end method

.method private a(I)Lcee;
    .locals 7

    .prologue
    .line 101
    new-instance v0, Lcee;

    iget-object v1, p0, Lcec;->a:Landroid/content/Context;

    iget-object v2, p0, Lcec;->b:Leyp;

    iget-object v3, p0, Lcec;->c:Lfhz;

    iget-object v4, p0, Lcec;->e:Lboi;

    iget-object v5, p0, Lcec;->f:Lfdw;

    iget-object v6, p0, Lcec;->g:Lfrz;

    move v4, p1

    invoke-direct/range {v0 .. v6}, Lcee;-><init>(Landroid/content/Context;Leyp;Lfhz;ILfdw;Lfrz;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 4

    .prologue
    .line 28
    check-cast p2, Lfka;

    iget-object v0, p0, Lcec;->a:Landroid/content/Context;

    invoke-static {v0, p1}, La;->a(Landroid/content/Context;Lfsg;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcec;->i:Lcee;

    if-nez v0, :cond_0

    const v0, 0x7f040072

    invoke-direct {p0, v0}, Lcec;->a(I)Lcee;

    move-result-object v0

    iput-object v0, p0, Lcec;->i:Lcee;

    :cond_0
    iget-object v0, p0, Lcec;->i:Lcee;

    :goto_0
    invoke-virtual {p2}, Lfka;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcee;->a(Ljava/lang/CharSequence;)V

    iget-object v1, p2, Lfka;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfka;->a:Lhhk;

    iget-object v1, v1, Lhhk;->d:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfka;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfka;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcee;->b(Ljava/lang/CharSequence;)V

    iget-object v1, p2, Lfka;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    iget-object v1, p2, Lfka;->a:Lhhk;

    iget-object v1, v1, Lhhk;->e:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfka;->c:Ljava/lang/CharSequence;

    :cond_2
    iget-object v1, p2, Lfka;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcee;->c(Ljava/lang/CharSequence;)V

    iget-object v1, p2, Lfka;->e:Lflz;

    invoke-virtual {v1}, Lflz;->b()Lfma;

    move-result-object v1

    iget-object v2, p2, Lfka;->e:Lflz;

    invoke-virtual {v2}, Lflz;->a()Lfnc;

    move-result-object v2

    iget-object v3, p2, Lfka;->d:Ljava/lang/CharSequence;

    if-nez v3, :cond_3

    iget-object v3, p2, Lfka;->a:Lhhk;

    iget-object v3, v3, Lhhk;->g:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, p2, Lfka;->d:Ljava/lang/CharSequence;

    :cond_3
    iget-object v3, p2, Lfka;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2, v3}, Lcee;->a(Lfma;Lfnc;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcec;->d:Lfsj;

    invoke-virtual {v0, v1, p2}, Lcee;->a(Lfsj;Lfka;)V

    iget-object v1, p0, Lcec;->d:Lfsj;

    invoke-interface {v1}, Lfsj;->a()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcec;->e:Lboi;

    iget-object v0, v0, Lcbx;->d:Landroid/view/View;

    invoke-static {v1, v2, v0, p2}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcec;->d:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_4
    iget-object v0, p0, Lcec;->h:Lcee;

    if-nez v0, :cond_5

    const v0, 0x7f0400c7

    invoke-direct {p0, v0}, Lcec;->a(I)Lcee;

    move-result-object v0

    iput-object v0, p0, Lcec;->h:Lcee;

    :cond_5
    iget-object v0, p0, Lcec;->h:Lcee;

    invoke-virtual {v0}, Lcee;->a()V

    goto :goto_0
.end method
