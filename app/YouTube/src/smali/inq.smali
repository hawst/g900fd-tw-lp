.class public final Linq;
.super Lino;
.source "SourceFile"


# instance fields
.field private final a:Linp;


# direct methods
.method private constructor <init>(Linp;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lino;-><init>(Ljava/net/HttpURLConnection;)V

    .line 34
    iput-object p1, p0, Linq;->a:Linp;

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/net/URL;Lilw;)V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Linp;

    invoke-direct {v0, p1, p2}, Linp;-><init>(Ljava/net/URL;Lilw;)V

    invoke-direct {p0, v0}, Linq;-><init>(Linp;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected final a()Lilr;
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Linq;->a:Linp;

    iget-object v0, v0, Linp;->b:Linc;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Connection has not yet been established"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    iget-object v0, p0, Linq;->a:Linp;

    iget-object v0, v0, Linp;->b:Linc;

    invoke-virtual {v0}, Linc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Linq;->a:Linp;

    iget-object v0, v0, Linp;->b:Linc;

    invoke-virtual {v0}, Linc;->e()Limd;

    move-result-object v0

    iget-object v0, v0, Limd;->e:Lilr;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Linq;->a:Linp;

    iget-object v0, v0, Linp;->c:Lilr;

    goto :goto_0
.end method

.method public final bridge synthetic addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lino;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final bridge synthetic connect()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Lino;->connect()V

    return-void
.end method

.method public final bridge synthetic disconnect()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Lino;->disconnect()V

    return-void
.end method

.method public final bridge synthetic getAllowUserInteraction()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getAllowUserInteraction()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getCipherSuite()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getCipherSuite()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getConnectTimeout()I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getConnectTimeout()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getContent()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getContent()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getContent([Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->getContent([Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getContentEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getContentLength()I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getContentLength()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getContentType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDate()J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getDate()J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getDefaultUseCaches()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getDefaultUseCaches()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getDoInput()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getDoInput()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getDoOutput()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getDoOutput()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getErrorStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getExpiration()J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getExpiration()J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getHeaderField(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->getHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getHeaderFieldDate(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Lino;->getHeaderFieldDate(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getHeaderFieldInt(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lino;->getHeaderFieldInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getHeaderFields()Ljava/util/Map;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Linq;->a:Linp;

    iget-object v0, v0, Linp;->a:Lilw;

    iget-object v0, v0, Lilw;->j:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public final bridge synthetic getIfModifiedSince()J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getIfModifiedSince()J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getInstanceFollowRedirects()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getInstanceFollowRedirects()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getLastModified()J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getLastModified()J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getLocalCertificates()[Ljava/security/cert/Certificate;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getLocalCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getLocalPrincipal()Ljava/security/Principal;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getLocalPrincipal()Ljava/security/Principal;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getPeerPrincipal()Ljava/security/Principal;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getPeerPrincipal()Ljava/security/Principal;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getPermission()Ljava/security/Permission;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getPermission()Ljava/security/Permission;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getReadTimeout()I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getReadTimeout()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getRequestMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getRequestMethod()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getRequestProperties()Ljava/util/Map;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getRequestProperties()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getResponseCode()I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getResponseCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getResponseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Linq;->a:Linp;

    iget-object v0, v0, Linp;->a:Lilw;

    iget-object v0, v0, Lilw;->i:Ljavax/net/ssl/SSLSocketFactory;

    return-object v0
.end method

.method public final bridge synthetic getServerCertificates()[Ljava/security/cert/Certificate;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getServerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getURL()Ljava/net/URL;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getURL()Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getUseCaches()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->getUseCaches()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic setAllowUserInteraction(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setAllowUserInteraction(Z)V

    return-void
.end method

.method public final bridge synthetic setChunkedStreamingMode(I)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setChunkedStreamingMode(I)V

    return-void
.end method

.method public final bridge synthetic setConnectTimeout(I)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setConnectTimeout(I)V

    return-void
.end method

.method public final bridge synthetic setDefaultUseCaches(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setDefaultUseCaches(Z)V

    return-void
.end method

.method public final bridge synthetic setDoInput(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setDoInput(Z)V

    return-void
.end method

.method public final bridge synthetic setDoOutput(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setDoOutput(Z)V

    return-void
.end method

.method public final bridge synthetic setFixedLengthStreamingMode(I)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setFixedLengthStreamingMode(I)V

    return-void
.end method

.method public final setFixedLengthStreamingMode(J)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Linq;->a:Linp;

    invoke-virtual {v0, p1, p2}, Linp;->setFixedLengthStreamingMode(J)V

    .line 72
    return-void
.end method

.method public final setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Linq;->a:Linp;

    iget-object v0, v0, Linp;->a:Lilw;

    iput-object p1, v0, Lilw;->j:Ljavax/net/ssl/HostnameVerifier;

    .line 52
    return-void
.end method

.method public final bridge synthetic setIfModifiedSince(J)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lino;->setIfModifiedSince(J)V

    return-void
.end method

.method public final bridge synthetic setInstanceFollowRedirects(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setInstanceFollowRedirects(Z)V

    return-void
.end method

.method public final bridge synthetic setReadTimeout(I)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setReadTimeout(I)V

    return-void
.end method

.method public final bridge synthetic setRequestMethod(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setRequestMethod(Ljava/lang/String;)V

    return-void
.end method

.method public final bridge synthetic setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lino;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Linq;->a:Linp;

    iget-object v0, v0, Linp;->a:Lilw;

    iput-object p1, v0, Lilw;->i:Ljavax/net/ssl/SSLSocketFactory;

    .line 60
    return-void
.end method

.method public final bridge synthetic setUseCaches(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lino;->setUseCaches(Z)V

    return-void
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic usingProxy()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lino;->usingProxy()Z

    move-result v0

    return v0
.end method
