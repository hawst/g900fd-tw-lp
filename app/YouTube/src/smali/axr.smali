.class public abstract enum Laxr;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Laxu;


# static fields
.field private static enum a:Laxr;

.field private static enum b:Laxr;

.field private static final synthetic d:[Laxr;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Laxs;

    const-string v1, "ALWAYS_ASK"

    const v2, 0x7f09006d

    invoke-direct {v0, v1, v3, v2}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxr;->a:Laxr;

    .line 42
    new-instance v0, Laxt;

    const-string v1, "NEVER"

    const v2, 0x7f09006e

    invoke-direct {v0, v1, v4, v2}, Laxt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxr;->b:Laxr;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Laxr;

    sget-object v1, Laxr;->a:Laxr;

    aput-object v1, v0, v3

    sget-object v1, Laxr;->b:Laxr;

    aput-object v1, v0, v4

    sput-object v0, Laxr;->d:[Laxr;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Laxr;->c:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laxr;
    .locals 1

    .prologue
    .line 26
    const-class v0, Laxr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laxr;

    return-object v0
.end method

.method public static values()[Laxr;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Laxr;->d:[Laxr;

    invoke-virtual {v0}, [Laxr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laxr;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Laxr;->c:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
