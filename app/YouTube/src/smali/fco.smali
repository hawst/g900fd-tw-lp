.class public Lfco;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lftb;

.field private final b:Lws;

.field private c:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Lftb;Lws;Ljava/lang/Class;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftb;

    iput-object v0, p0, Lfco;->a:Lftb;

    .line 78
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    iput-object v0, p0, Lfco;->b:Lws;

    .line 79
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lfco;->c:Ljava/lang/Class;

    .line 80
    return-void
.end method


# virtual methods
.method public final a(Lfsp;)Lidh;
    .locals 2

    .prologue
    .line 95
    invoke-static {}, Lb;->b()V

    .line 96
    invoke-static {}, Lgky;->a()Lgky;

    move-result-object v0

    .line 97
    invoke-virtual {p0, p1, v0}, Lfco;->a(Lfsp;Lwv;)V

    .line 99
    :try_start_0
    invoke-virtual {v0}, Lgky;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidh;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    new-instance v1, Lfdv;

    invoke-direct {v1, v0}, Lfdv;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 102
    :catch_1
    move-exception v0

    .line 103
    new-instance v1, Lfdv;

    invoke-direct {v1, v0}, Lfdv;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lfsp;Lwv;)V
    .locals 3

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lfco;->b(Lfsp;)V

    .line 86
    iget-object v0, p0, Lfco;->b:Lws;

    iget-object v1, p0, Lfco;->a:Lftb;

    iget-object v2, p0, Lfco;->c:Ljava/lang/Class;

    .line 87
    invoke-virtual {v1, p1, v2, p2}, Lftb;->a(Lftd;Ljava/lang/Class;Lwv;)Lfta;

    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Lws;->a(Lwp;)Lwp;

    .line 91
    return-void
.end method

.method protected final b(Lfsp;)V
    .locals 3

    .prologue
    .line 109
    invoke-virtual {p1}, Lfsp;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lfco;->b:Lws;

    invoke-virtual {v0}, Lws;->d()Lwc;

    move-result-object v0

    invoke-virtual {p1}, Lfsp;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lwc;->a(Ljava/lang/String;Z)V

    .line 112
    :cond_0
    return-void
.end method
