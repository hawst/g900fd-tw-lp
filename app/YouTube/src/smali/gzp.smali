.class public final Lgzp;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Lhld;

.field private d:Lhog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9073
    invoke-direct {p0}, Lidf;-><init>()V

    .line 9076
    const-string v0, ""

    iput-object v0, p0, Lgzp;->a:Ljava/lang/String;

    .line 9079
    const-string v0, ""

    iput-object v0, p0, Lgzp;->b:Ljava/lang/String;

    .line 9082
    sget-object v0, Lhld;->a:[Lhld;

    iput-object v0, p0, Lgzp;->c:[Lhld;

    .line 9085
    const/4 v0, 0x0

    iput-object v0, p0, Lgzp;->d:Lhog;

    .line 9073
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 9149
    .line 9150
    iget-object v0, p0, Lgzp;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 9151
    const/4 v0, 0x1

    iget-object v2, p0, Lgzp;->a:Ljava/lang/String;

    .line 9152
    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9154
    :goto_0
    iget-object v2, p0, Lgzp;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 9155
    const/4 v2, 0x2

    iget-object v3, p0, Lgzp;->b:Ljava/lang/String;

    .line 9156
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 9158
    :cond_0
    iget-object v2, p0, Lgzp;->c:[Lhld;

    if-eqz v2, :cond_2

    .line 9159
    iget-object v2, p0, Lgzp;->c:[Lhld;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 9160
    if-eqz v4, :cond_1

    .line 9161
    const/4 v5, 0x3

    .line 9162
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 9159
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 9166
    :cond_2
    iget-object v1, p0, Lgzp;->d:Lhog;

    if-eqz v1, :cond_3

    .line 9167
    const/4 v1, 0x4

    iget-object v2, p0, Lgzp;->d:Lhog;

    .line 9168
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9170
    :cond_3
    iget-object v1, p0, Lgzp;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9171
    iput v0, p0, Lgzp;->J:I

    .line 9172
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9069
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lgzp;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lgzp;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lgzp;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgzp;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgzp;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgzp;->c:[Lhld;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhld;

    iget-object v3, p0, Lgzp;->c:[Lhld;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lgzp;->c:[Lhld;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lgzp;->c:[Lhld;

    :goto_2
    iget-object v2, p0, Lgzp;->c:[Lhld;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lgzp;->c:[Lhld;

    new-instance v3, Lhld;

    invoke-direct {v3}, Lhld;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzp;->c:[Lhld;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lgzp;->c:[Lhld;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lgzp;->c:[Lhld;

    new-instance v3, Lhld;

    invoke-direct {v3}, Lhld;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzp;->c:[Lhld;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lgzp;->d:Lhog;

    if-nez v0, :cond_5

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lgzp;->d:Lhog;

    :cond_5
    iget-object v0, p0, Lgzp;->d:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 9127
    iget-object v0, p0, Lgzp;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9128
    const/4 v0, 0x1

    iget-object v1, p0, Lgzp;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 9130
    :cond_0
    iget-object v0, p0, Lgzp;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 9131
    const/4 v0, 0x2

    iget-object v1, p0, Lgzp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 9133
    :cond_1
    iget-object v0, p0, Lgzp;->c:[Lhld;

    if-eqz v0, :cond_3

    .line 9134
    iget-object v1, p0, Lgzp;->c:[Lhld;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 9135
    if-eqz v3, :cond_2

    .line 9136
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 9134
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9140
    :cond_3
    iget-object v0, p0, Lgzp;->d:Lhog;

    if-eqz v0, :cond_4

    .line 9141
    const/4 v0, 0x4

    iget-object v1, p0, Lgzp;->d:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 9143
    :cond_4
    iget-object v0, p0, Lgzp;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 9145
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9099
    if-ne p1, p0, :cond_1

    .line 9106
    :cond_0
    :goto_0
    return v0

    .line 9100
    :cond_1
    instance-of v2, p1, Lgzp;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 9101
    :cond_2
    check-cast p1, Lgzp;

    .line 9102
    iget-object v2, p0, Lgzp;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzp;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzp;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzp;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 9103
    :goto_2
    iget-object v2, p0, Lgzp;->c:[Lhld;

    iget-object v3, p1, Lgzp;->c:[Lhld;

    .line 9104
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgzp;->d:Lhog;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzp;->d:Lhog;

    if-nez v2, :cond_3

    .line 9105
    :goto_3
    iget-object v2, p0, Lgzp;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgzp;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 9106
    goto :goto_0

    .line 9102
    :cond_4
    iget-object v2, p0, Lgzp;->a:Ljava/lang/String;

    iget-object v3, p1, Lgzp;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzp;->b:Ljava/lang/String;

    iget-object v3, p1, Lgzp;->b:Ljava/lang/String;

    .line 9103
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 9104
    :cond_6
    iget-object v2, p0, Lgzp;->d:Lhog;

    iget-object v3, p1, Lgzp;->d:Lhog;

    .line 9105
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgzp;->I:Ljava/util/List;

    iget-object v3, p1, Lgzp;->I:Ljava/util/List;

    .line 9106
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9110
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 9112
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzp;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 9113
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzp;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 9114
    iget-object v2, p0, Lgzp;->c:[Lhld;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 9120
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgzp;->d:Lhog;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 9121
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzp;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_3
    add-int/2addr v0, v1

    .line 9122
    return v0

    .line 9112
    :cond_1
    iget-object v0, p0, Lgzp;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 9113
    :cond_2
    iget-object v0, p0, Lgzp;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    move v2, v0

    move v0, v1

    .line 9116
    :goto_4
    iget-object v3, p0, Lgzp;->c:[Lhld;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 9117
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgzp;->c:[Lhld;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    .line 9116
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 9117
    :cond_4
    iget-object v2, p0, Lgzp;->c:[Lhld;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhld;->hashCode()I

    move-result v2

    goto :goto_5

    .line 9120
    :cond_5
    iget-object v0, p0, Lgzp;->d:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_2

    .line 9121
    :cond_6
    iget-object v1, p0, Lgzp;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
