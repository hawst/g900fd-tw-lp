.class public final Lhbn;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhbu;

.field public b:Lhgp;

.field public c:Lhsb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15990
    invoke-direct {p0}, Lidf;-><init>()V

    .line 15993
    iput-object v0, p0, Lhbn;->a:Lhbu;

    .line 15996
    iput-object v0, p0, Lhbn;->b:Lhgp;

    .line 15999
    iput-object v0, p0, Lhbn;->c:Lhsb;

    .line 15990
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 16048
    const/4 v0, 0x0

    .line 16049
    iget-object v1, p0, Lhbn;->a:Lhbu;

    if-eqz v1, :cond_0

    .line 16050
    const v0, 0x2c42002

    iget-object v1, p0, Lhbn;->a:Lhbu;

    .line 16051
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16053
    :cond_0
    iget-object v1, p0, Lhbn;->b:Lhgp;

    if-eqz v1, :cond_1

    .line 16054
    const v1, 0x2fe8b38

    iget-object v2, p0, Lhbn;->b:Lhgp;

    .line 16055
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16057
    :cond_1
    iget-object v1, p0, Lhbn;->c:Lhsb;

    if-eqz v1, :cond_2

    .line 16058
    const v1, 0x32ce059

    iget-object v2, p0, Lhbn;->c:Lhsb;

    .line 16059
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16061
    :cond_2
    iget-object v1, p0, Lhbn;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16062
    iput v0, p0, Lhbn;->J:I

    .line 16063
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 15986
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbn;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbn;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbn;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhbn;->a:Lhbu;

    if-nez v0, :cond_2

    new-instance v0, Lhbu;

    invoke-direct {v0}, Lhbu;-><init>()V

    iput-object v0, p0, Lhbn;->a:Lhbu;

    :cond_2
    iget-object v0, p0, Lhbn;->a:Lhbu;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhbn;->b:Lhgp;

    if-nez v0, :cond_3

    new-instance v0, Lhgp;

    invoke-direct {v0}, Lhgp;-><init>()V

    iput-object v0, p0, Lhbn;->b:Lhgp;

    :cond_3
    iget-object v0, p0, Lhbn;->b:Lhgp;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhbn;->c:Lhsb;

    if-nez v0, :cond_4

    new-instance v0, Lhsb;

    invoke-direct {v0}, Lhsb;-><init>()V

    iput-object v0, p0, Lhbn;->c:Lhsb;

    :cond_4
    iget-object v0, p0, Lhbn;->c:Lhsb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x16210012 -> :sswitch_1
        0x17f459c2 -> :sswitch_2
        0x196702ca -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 16033
    iget-object v0, p0, Lhbn;->a:Lhbu;

    if-eqz v0, :cond_0

    .line 16034
    const v0, 0x2c42002

    iget-object v1, p0, Lhbn;->a:Lhbu;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16036
    :cond_0
    iget-object v0, p0, Lhbn;->b:Lhgp;

    if-eqz v0, :cond_1

    .line 16037
    const v0, 0x2fe8b38

    iget-object v1, p0, Lhbn;->b:Lhgp;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16039
    :cond_1
    iget-object v0, p0, Lhbn;->c:Lhsb;

    if-eqz v0, :cond_2

    .line 16040
    const v0, 0x32ce059

    iget-object v1, p0, Lhbn;->c:Lhsb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16042
    :cond_2
    iget-object v0, p0, Lhbn;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 16044
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 16012
    if-ne p1, p0, :cond_1

    .line 16018
    :cond_0
    :goto_0
    return v0

    .line 16013
    :cond_1
    instance-of v2, p1, Lhbn;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 16014
    :cond_2
    check-cast p1, Lhbn;

    .line 16015
    iget-object v2, p0, Lhbn;->a:Lhbu;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbn;->a:Lhbu;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhbn;->b:Lhgp;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbn;->b:Lhgp;

    if-nez v2, :cond_3

    .line 16016
    :goto_2
    iget-object v2, p0, Lhbn;->c:Lhsb;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhbn;->c:Lhsb;

    if-nez v2, :cond_3

    .line 16017
    :goto_3
    iget-object v2, p0, Lhbn;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhbn;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 16018
    goto :goto_0

    .line 16015
    :cond_4
    iget-object v2, p0, Lhbn;->a:Lhbu;

    iget-object v3, p1, Lhbn;->a:Lhbu;

    invoke-virtual {v2, v3}, Lhbu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbn;->b:Lhgp;

    iget-object v3, p1, Lhbn;->b:Lhgp;

    .line 16016
    invoke-virtual {v2, v3}, Lhgp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhbn;->c:Lhsb;

    iget-object v3, p1, Lhbn;->c:Lhsb;

    .line 16017
    invoke-virtual {v2, v3}, Lhsb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhbn;->I:Ljava/util/List;

    iget-object v3, p1, Lhbn;->I:Ljava/util/List;

    .line 16018
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 16022
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 16024
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbn;->a:Lhbu;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 16025
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbn;->b:Lhgp;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 16026
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbn;->c:Lhsb;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 16027
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbn;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 16028
    return v0

    .line 16024
    :cond_0
    iget-object v0, p0, Lhbn;->a:Lhbu;

    invoke-virtual {v0}, Lhbu;->hashCode()I

    move-result v0

    goto :goto_0

    .line 16025
    :cond_1
    iget-object v0, p0, Lhbn;->b:Lhgp;

    invoke-virtual {v0}, Lhgp;->hashCode()I

    move-result v0

    goto :goto_1

    .line 16026
    :cond_2
    iget-object v0, p0, Lhbn;->c:Lhsb;

    invoke-virtual {v0}, Lhsb;->hashCode()I

    move-result v0

    goto :goto_2

    .line 16027
    :cond_3
    iget-object v1, p0, Lhbn;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
