.class final Lgnw;
.super Lgkx;
.source "SourceFile"


# instance fields
.field private final j:Ldzy;

.field private final k:Lezj;

.field private final l:Lgll;

.field private final m:Lwv;


# direct methods
.method public constructor <init>(Ldzy;Lwv;Lezj;Lght;Lgll;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 115
    .line 116
    iget v1, p1, Ldzy;->c:I

    .line 117
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzy;

    iget-object v0, v0, Ldzy;->e:Ljava/lang/String;

    .line 115
    invoke-direct {p0, v1, v0, p2}, Lgkx;-><init>(ILjava/lang/String;Lwu;)V

    .line 121
    new-instance v0, Lwg;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 122
    invoke-virtual {p4}, Lght;->f()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, Lwg;-><init>(IIF)V

    .line 121
    iput-object v0, p0, Lwp;->h:Lwx;

    .line 124
    iput-boolean v4, p0, Lwp;->e:Z

    .line 125
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzy;

    iput-object v0, p0, Lgnw;->j:Ldzy;

    .line 126
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwv;

    iput-object v0, p0, Lgnw;->m:Lwv;

    .line 127
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgnw;->k:Lezj;

    .line 128
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgll;

    iput-object v0, p0, Lgnw;->l:Lgll;

    .line 129
    return-void
.end method


# virtual methods
.method protected final a(Lwm;)Lwt;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 150
    invoke-static {v0, v0}, Lwt;->a(Ljava/lang/Object;Lwd;)Lwt;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lgnw;->l:Lgll;

    invoke-virtual {v0}, Lgll;->b()V

    iget-object v0, p0, Lgnw;->m:Lwv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lwv;->onResponse(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lxa;)V
    .locals 1

    .prologue
    .line 163
    invoke-static {p1}, Lggu;->a(Lxa;)I

    move-result v0

    if-lez v0, :cond_0

    .line 164
    iget-object v0, p0, Lgnw;->l:Lgll;

    invoke-virtual {v0}, Lgll;->c()V

    .line 169
    :goto_0
    invoke-super {p0, p1}, Lgkx;->b(Lxa;)V

    .line 170
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lgnw;->l:Lgll;

    invoke-virtual {v0}, Lgll;->d()V

    goto :goto_0
.end method

.method public final c()Ljava/util/Map;
    .locals 4

    .prologue
    .line 133
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 134
    iget-object v0, p0, Lgnw;->j:Ldzy;

    iget-object v0, v0, Ldzy;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzx;

    .line 135
    iget-boolean v3, v0, Ldzx;->a:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v0, Ldzx;->c:Z

    if-eqz v3, :cond_0

    .line 136
    iget-object v3, v0, Ldzx;->b:Ljava/lang/String;

    iget-object v0, v0, Ldzx;->d:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 141
    :cond_1
    const-string v0, "X-Goog-Request-Time"

    iget-object v2, p0, Lgnw;->k:Lezj;

    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    const-string v0, "X-Goog-Event-Time"

    iget-object v2, p0, Lgnw;->j:Ldzy;

    .line 143
    iget-wide v2, v2, Ldzy;->h:J

    .line 142
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    return-object v1
.end method
