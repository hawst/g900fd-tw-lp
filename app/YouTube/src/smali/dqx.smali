.class public final Ldqx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldpo;


# instance fields
.field private final a:Lorg/apache/http/client/HttpClient;

.field private final b:Ljava/lang/String;

.field private final c:Ldqz;

.field private final d:Ldyd;

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:Landroid/content/Context;

.field private final h:Ldyg;

.field private final i:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Ldqz;Ldyd;Ljava/lang/String;ILjava/lang/String;Ldyg;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Ldqx;->g:Landroid/content/Context;

    .line 52
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Ldqx;->a:Lorg/apache/http/client/HttpClient;

    .line 53
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqz;

    iput-object v0, p0, Ldqx;->c:Ldqz;

    .line 54
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyd;

    iput-object v0, p0, Ldqx;->d:Ldyd;

    .line 55
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldqx;->f:Ljava/lang/String;

    .line 56
    const/16 v0, 0x50

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Ldqx;->e:I

    .line 57
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldqx;->b:Ljava/lang/String;

    .line 58
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Ldqx;->i:Ljava/util/Map;

    .line 59
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldqx;->h:Ldyg;

    .line 60
    return-void
.end method


# virtual methods
.method public final a(Ldpt;)Ldqq;
    .locals 12

    .prologue
    .line 65
    new-instance v6, Ljava/util/HashMap;

    iget-object v0, p0, Ldqx;->i:Ljava/util/Map;

    invoke-direct {v6, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 67
    invoke-virtual {p1}, Ldpt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    const-string v0, "method"

    iget-object v1, p1, Ldpt;->a:Ldtc;

    invoke-virtual {v1}, Ldtc;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    invoke-virtual {p1}, Ldpt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-string v0, "params"

    .line 71
    iget-object v1, p1, Ldpt;->b:Ldte;

    invoke-static {v1}, Ldqp;->a(Ldte;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    :cond_0
    iget-boolean v0, p1, Ldpt;->d:Z

    if-eqz v0, :cond_1

    .line 76
    const-string v0, "ui"

    const-string v1, ""

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    :cond_1
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 81
    invoke-virtual {p1}, Ldpt;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    const-string v0, "X-YouTube-LoungeId-Token"

    .line 83
    iget-object v1, p1, Ldpt;->c:Ldtb;

    invoke-virtual {v1}, Ldtb;->toString()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    :cond_2
    invoke-static {}, Ldzh;->a()Ldza;

    move-result-object v1

    .line 87
    new-instance v0, Ldqv;

    iget-object v2, p0, Ldqx;->f:Ljava/lang/String;

    iget-object v3, p0, Ldqx;->b:Ljava/lang/String;

    iget-object v4, p0, Ldqx;->c:Ldqz;

    .line 91
    iget-object v4, v4, Ldqz;->c:Ljava/lang/String;

    iget-object v5, p0, Ldqx;->d:Ldyd;

    new-instance v8, Ldpx;

    iget-object v9, p0, Ldqx;->g:Landroid/content/Context;

    iget-object v10, p0, Ldqx;->f:Ljava/lang/String;

    iget v11, p0, Ldqx;->e:I

    invoke-direct {v8, v9, v1, v10, v11}, Ldpx;-><init>(Landroid/content/Context;Ldza;Ljava/lang/String;I)V

    iget-object v9, p0, Ldqx;->a:Lorg/apache/http/client/HttpClient;

    iget-object v10, p0, Ldqx;->h:Ldyg;

    invoke-direct/range {v0 .. v10}, Ldqv;-><init>(Ldza;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldyd;Ljava/util/Map;Ljava/util/Map;Ldpx;Lorg/apache/http/client/HttpClient;Ldyg;)V

    return-object v0
.end method
