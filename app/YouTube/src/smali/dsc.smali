.class public final Ldsc;
.super Ldrr;
.source "SourceFile"


# instance fields
.field private final o:Ldsl;

.field private final p:Ldyg;


# direct methods
.method public constructor <init>(Landroid/content/Context;Levn;Ldsl;Ldwv;Ldxe;Ldwq;Ldyg;)V
    .locals 8

    .prologue
    .line 37
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p2

    move-object v4, p4

    move-object v5, p6

    move-object v6, p5

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Ldrr;-><init>(Landroid/content/Context;Levn;Levn;Ldwv;Ldwq;Ldxe;Ldyg;)V

    .line 45
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsl;

    iput-object v0, p0, Ldsc;->o:Ldsl;

    .line 46
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldsc;->p:Ldyg;

    .line 47
    return-void
.end method

.method public static a(Lsk;)Z
    .locals 3

    .prologue
    .line 57
    invoke-virtual {p0}, Lsk;->e()Ljava/util/List;

    move-result-object v0

    .line 58
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    .line 59
    const-string v2, "MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x1

    .line 63
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ldwr;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "screen"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lrq;
    .locals 5

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Ldsc;->b(Ljava/lang/String;)Ldwr;

    move-result-object v1

    .line 69
    if-nez v1, :cond_0

    .line 70
    const/4 v0, 0x0

    .line 72
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldsd;

    iget-object v2, p0, Ldsc;->o:Ldsl;

    iget-object v3, p0, Ldsc;->l:Ldxe;

    iget-object v4, p0, Ldsc;->p:Ldyg;

    invoke-direct {v0, v2, v1, v3, v4}, Ldsd;-><init>(Ldsl;Ldwr;Ldxe;Ldyg;)V

    goto :goto_0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string v0, "MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    return-object v0
.end method

.method protected final c()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x1

    return v0
.end method
