.class public abstract Ldnw;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Ldob;


# instance fields
.field public final a:Landroid/view/View;

.field public b:Landroid/view/View;

.field public c:Landroid/widget/ListAdapter;

.field public d:Ldoc;

.field private e:Landroid/view/ViewGroup;

.field private f:Landroid/view/ViewGroup;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/ViewGroup;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/Button;

.field private final k:Ljava/util/List;

.field private final l:Z

.field private final m:Landroid/os/Handler;

.field private n:Z

.field private o:Ldod;

.field private p:Ljava/lang/String;

.field private final q:I

.field private final r:I

.field private s:Landroid/widget/FrameLayout;

.field private t:Ldnx;

.field private u:I


# direct methods
.method public constructor <init>(ILandroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    .line 75
    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Ldnw;-><init>(ILandroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V

    .line 76
    return-void
.end method

.method public constructor <init>(ILandroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    invoke-direct {p0, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    iput v1, p0, Ldnw;->u:I

    .line 82
    sget-object v0, Lgvk;->a:[I

    invoke-virtual {p2, p3, v0, v2, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 84
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ldnw;->m:Landroid/os/Handler;

    .line 86
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ldnw;->k:Ljava/util/List;

    .line 87
    iput-boolean v1, p0, Ldnw;->l:Z

    .line 89
    if-nez p6, :cond_1

    .line 90
    invoke-virtual {v3, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnw;->p:Ljava/lang/String;

    .line 95
    :goto_0
    invoke-virtual {p0, v1}, Ldnw;->setOrientation(I)V

    .line 96
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldnw;->s:Landroid/widget/FrameLayout;

    .line 97
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 99
    iget-object v4, p0, Ldnw;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v4, v0}, Ldnw;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 101
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 102
    const/4 v0, 0x0

    invoke-virtual {v4, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ldnw;->e:Landroid/view/ViewGroup;

    .line 103
    iget-object v0, p0, Ldnw;->s:Landroid/widget/FrameLayout;

    iget-object v5, p0, Ldnw;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 105
    if-gez p5, :cond_0

    .line 106
    invoke-virtual {v3, v8, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p5

    .line 109
    :cond_0
    if-lez p5, :cond_2

    move v0, v1

    :goto_1
    const-string v5, "no statusView provided"

    invoke-static {v0, v5}, Lb;->d(ZLjava/lang/Object;)V

    .line 110
    new-instance v0, Ldod;

    const/4 v5, 0x0

    invoke-virtual {v4, p5, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-direct {v0, v4}, Ldod;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Ldnw;->o:Ldod;

    .line 112
    iget-object v0, p0, Ldnw;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ldnw;->f:Landroid/view/ViewGroup;

    .line 113
    iget-object v0, p0, Ldnw;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldnw;->g:Landroid/widget/TextView;

    .line 114
    iget-object v0, p0, Ldnw;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldnw;->a:Landroid/view/View;

    .line 116
    iget-object v0, p0, Ldnw;->e:Landroid/view/ViewGroup;

    const-string v4, "error_message_view"

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 117
    if-nez v0, :cond_3

    .line 118
    iget-object v0, p0, Ldnw;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ldnw;->h:Landroid/view/ViewGroup;

    .line 119
    iget-object v0, p0, Ldnw;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldnw;->i:Landroid/widget/TextView;

    .line 120
    iget-object v0, p0, Ldnw;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Ldnw;->j:Landroid/widget/Button;

    .line 127
    :goto_2
    iget-object v0, p0, Ldnw;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldnw;->b:Landroid/view/View;

    .line 129
    const v0, 0x7fffffff

    invoke-virtual {v3, v2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Ldnw;->q:I

    .line 130
    const v0, 0x7fffffff

    invoke-virtual {v3, v1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Ldnw;->r:I

    .line 132
    invoke-virtual {p0}, Ldnw;->getPaddingLeft()I

    move-result v0

    .line 133
    invoke-virtual {p0}, Ldnw;->getPaddingTop()I

    move-result v1

    .line 134
    invoke-virtual {p0}, Ldnw;->getPaddingRight()I

    move-result v4

    .line 135
    invoke-virtual {p0}, Ldnw;->getPaddingBottom()I

    move-result v5

    .line 136
    iget-object v6, p0, Ldnw;->f:Landroid/view/ViewGroup;

    invoke-static {v6, v0, v1, v4, v5}, Ldnw;->a(Landroid/view/View;IIII)V

    .line 137
    iget-object v6, p0, Ldnw;->a:Landroid/view/View;

    invoke-static {v6, v0, v1, v4, v5}, Ldnw;->a(Landroid/view/View;IIII)V

    .line 138
    iget-object v6, p0, Ldnw;->b:Landroid/view/View;

    invoke-static {v6, v0, v1, v4, v5}, Ldnw;->a(Landroid/view/View;IIII)V

    .line 139
    invoke-virtual {p0, v2, v2, v2, v2}, Ldnw;->setPadding(IIII)V

    .line 141
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 143
    new-instance v0, Ldnx;

    invoke-direct {v0, p0}, Ldnx;-><init>(Ldnw;)V

    iput-object v0, p0, Ldnw;->t:Ldnx;

    .line 144
    return-void

    .line 92
    :cond_1
    iput-object p6, p0, Ldnw;->p:Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 109
    goto/16 :goto_1

    .line 122
    :cond_3
    iput-object v0, p0, Ldnw;->h:Landroid/view/ViewGroup;

    .line 123
    iget-object v0, p0, Ldnw;->h:Landroid/view/ViewGroup;

    const-string v4, "error_message"

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldnw;->i:Landroid/widget/TextView;

    .line 124
    iget-object v0, p0, Ldnw;->h:Landroid/view/ViewGroup;

    const-string v4, "retry_button"

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Ldnw;->j:Landroid/widget/Button;

    goto :goto_2
.end method

.method static synthetic a(Ldnw;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Ldnw;->m:Landroid/os/Handler;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 270
    iget v0, p0, Ldnw;->u:I

    if-eq v0, p1, :cond_0

    .line 271
    iget-object v3, p0, Ldnw;->f:Landroid/view/ViewGroup;

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 272
    iget-object v3, p0, Ldnw;->a:Landroid/view/View;

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 273
    iget-object v3, p0, Ldnw;->h:Landroid/view/ViewGroup;

    const/4 v0, 0x5

    if-ne p1, v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Ldnw;->b:Landroid/view/View;

    const/4 v3, 0x4

    if-ne p1, v3, :cond_4

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 275
    iput p1, p0, Ldnw;->u:I

    .line 276
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 271
    goto :goto_0

    :cond_2
    move v0, v2

    .line 272
    goto :goto_1

    :cond_3
    move v0, v2

    .line 273
    goto :goto_2

    :cond_4
    move v1, v2

    .line 274
    goto :goto_3
.end method

.method private static a(Landroid/view/View;IIII)V
    .locals 4

    .prologue
    .line 359
    .line 360
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p1

    .line 361
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, p2

    .line 362
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, p3

    .line 363
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    add-int/2addr v3, p4

    .line 359
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 364
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    .line 293
    iget-boolean v0, p0, Ldnw;->n:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldnw;->l:Z

    if-nez v0, :cond_1

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    const/4 v0, 0x0

    .line 297
    iget-object v1, p0, Ldnw;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 298
    invoke-virtual {p0, v0}, Ldnw;->b(Landroid/view/View;)V

    .line 299
    add-int/lit8 v2, v1, 0x1

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v0, v1, v4}, Ldnw;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move v1, v2

    .line 300
    goto :goto_1

    .line 301
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldnw;->n:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Ldnw;->q:I

    return v0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Ldnw;->j:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    return-void
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Ldnw;->c:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Ldnw;->c:Landroid/widget/ListAdapter;

    iget-object v1, p0, Ldnw;->t:Ldnx;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 188
    :cond_0
    iput-object p1, p0, Ldnw;->c:Landroid/widget/ListAdapter;

    .line 189
    iget-object v0, p0, Ldnw;->c:Landroid/widget/ListAdapter;

    iget-object v1, p0, Ldnw;->t:Ldnx;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 190
    return-void
.end method

.method public final a(Ldoc;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Ldnw;->d:Ldoc;

    .line 205
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Ldnw;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v0, p0, Ldnw;->j:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 251
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Ldnw;->a(I)V

    .line 252
    invoke-direct {p0}, Ldnw;->h()V

    .line 253
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Ldnw;->r:I

    return v0
.end method

.method public abstract b(Landroid/view/View;)V
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Ldnw;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v0, p0, Ldnw;->j:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 259
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Ldnw;->a(I)V

    .line 260
    invoke-direct {p0}, Ldnw;->h()V

    .line 261
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Ldnw;->g:Landroid/widget/TextView;

    iget-object v1, p0, Ldnw;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Ldnw;->a(I)V

    .line 231
    invoke-direct {p0}, Ldnw;->h()V

    .line 232
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 243
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Ldnw;->a(I)V

    .line 244
    iget-boolean v0, p0, Ldnw;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldnw;->l:Z

    if-nez v0, :cond_1

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    iget-object v0, p0, Ldnw;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Ldnw;->removeView(Landroid/view/View;)V

    invoke-virtual {p0, v0}, Ldnw;->a(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldnw;->n:Z

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Ldnw;->a(I)V

    .line 266
    invoke-direct {p0}, Ldnw;->h()V

    .line 267
    return-void
.end method

.method public final f()Ldod;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Ldnw;->o:Ldod;

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x0

    return v0
.end method

.method public setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Ldnw;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 225
    return-void
.end method
