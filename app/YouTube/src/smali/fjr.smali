.class public final Lfjr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbt;
.implements Lflb;


# instance fields
.field private final a:Lhfa;

.field private b:Lfjf;

.field private c:Lfip;


# direct methods
.method public constructor <init>(Lhfa;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfa;

    iput-object v0, p0, Lfjr;->a:Lhfa;

    .line 29
    return-void
.end method

.method private f()Lfjf;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Lfjr;->b:Lfjf;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    iget-object v0, v0, Lhes;->a:Lheb;

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Lfjf;

    iget-object v1, p0, Lfjr;->a:Lhfa;

    iget-object v1, v1, Lhfa;->a:Lhes;

    iget-object v1, v1, Lhes;->a:Lheb;

    invoke-direct {v0, v1, v2}, Lfjf;-><init>(Lheb;Lfqh;)V

    iput-object v0, p0, Lfjr;->b:Lfjf;

    .line 72
    :cond_0
    iget-object v0, p0, Lfjr;->b:Lfjf;

    if-nez v0, :cond_1

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    iget-object v0, v0, Lhes;->c:Lian;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    iget-object v0, v0, Lhes;->c:Lian;

    iget-object v0, v0, Lian;->a:Liao;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    iget-object v0, v0, Lhes;->c:Lian;

    iget-object v0, v0, Lian;->a:Liao;

    iget-object v0, v0, Liao;->a:Lheb;

    if-eqz v0, :cond_1

    .line 77
    new-instance v0, Lfjf;

    iget-object v1, p0, Lfjr;->a:Lhfa;

    iget-object v1, v1, Lhfa;->a:Lhes;

    iget-object v1, v1, Lhes;->c:Lian;

    iget-object v1, v1, Lian;->a:Liao;

    iget-object v1, v1, Liao;->a:Lheb;

    invoke-direct {v0, v1, v2}, Lfjf;-><init>(Lheb;Lfqh;)V

    iput-object v0, p0, Lfjr;->b:Lfjf;

    .line 80
    :cond_1
    iget-object v0, p0, Lfjr;->b:Lfjf;

    return-object v0
.end method

.method private g()Lfip;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    iget-object v0, p0, Lfjr;->c:Lfip;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    iget-object v0, v0, Lhes;->b:Lhbb;

    if-eqz v0, :cond_0

    .line 88
    new-instance v0, Lfip;

    iget-object v1, p0, Lfjr;->a:Lhfa;

    iget-object v1, v1, Lhfa;->a:Lhes;

    iget-object v1, v1, Lhes;->b:Lhbb;

    invoke-direct {v0, v1, v2}, Lfip;-><init>(Lhbb;Lfqh;)V

    iput-object v0, p0, Lfjr;->c:Lfip;

    .line 90
    :cond_0
    iget-object v0, p0, Lfjr;->c:Lfip;

    if-nez v0, :cond_1

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    iget-object v0, v0, Lhes;->c:Lian;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    iget-object v0, v0, Lhes;->c:Lian;

    iget-object v0, v0, Lian;->a:Liao;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfjr;->a:Lhfa;

    iget-object v0, v0, Lhfa;->a:Lhes;

    iget-object v0, v0, Lhes;->c:Lian;

    iget-object v0, v0, Lian;->a:Liao;

    iget-object v0, v0, Liao;->b:Lhbb;

    if-eqz v0, :cond_1

    .line 95
    new-instance v0, Lfip;

    iget-object v1, p0, Lfjr;->a:Lhfa;

    iget-object v1, v1, Lhfa;->a:Lhes;

    iget-object v1, v1, Lhes;->c:Lian;

    iget-object v1, v1, Lian;->a:Liao;

    iget-object v1, v1, Liao;->b:Lhbb;

    invoke-direct {v0, v1, v2}, Lfip;-><init>(Lhbb;Lfqh;)V

    iput-object v0, p0, Lfjr;->c:Lfip;

    .line 98
    :cond_1
    iget-object v0, p0, Lfjr;->c:Lfip;

    return-object v0
.end method


# virtual methods
.method public final b()Lhog;
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Lfjr;->g()Lfip;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0}, Lfjr;->g()Lfip;

    move-result-object v0

    iget-object v0, v0, Lfip;->a:Lhbb;

    iget-object v0, v0, Lhbb;->a:Lhog;

    .line 128
    :goto_0
    return-object v0

    .line 125
    :cond_0
    invoke-direct {p0}, Lfjr;->f()Lfjf;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 126
    invoke-direct {p0}, Lfjr;->f()Lfjf;

    move-result-object v0

    iget-object v0, v0, Lfjf;->a:Lheb;

    iget-object v0, v0, Lheb;->g:Lhog;

    goto :goto_0

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
