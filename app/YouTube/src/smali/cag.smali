.class public final Lcag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/view/View;

.field final b:Landroid/view/View;

.field public final c:Landroid/widget/TextView;

.field final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;

.field final f:Landroid/widget/EditText;

.field public final g:Lfvi;

.field public final h:Lfvi;

.field public final i:Lfvi;

.field public j:Ljava/text/NumberFormat;

.field public k:Lfom;

.field public l:Z

.field public m:Lbzy;

.field private final n:Landroid/view/View;

.field private final o:Landroid/view/inputmethod/InputMethodManager;

.field private p:Ljava/text/NumberFormat;

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Landroid/view/inputmethod/InputMethodManager;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcag;->n:Landroid/view/View;

    .line 61
    iput-object p3, p0, Lcag;->o:Landroid/view/inputmethod/InputMethodManager;

    .line 63
    iget-object v0, p0, Lcag;->n:Landroid/view/View;

    const v1, 0x7f08035a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcag;->a:Landroid/view/View;

    .line 64
    iget-object v0, p0, Lcag;->n:Landroid/view/View;

    const v1, 0x7f08035d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcag;->b:Landroid/view/View;

    .line 65
    iget-object v0, p0, Lcag;->n:Landroid/view/View;

    const v1, 0x7f08011c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcag;->c:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcag;->n:Landroid/view/View;

    const v1, 0x7f08035b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcag;->d:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcag;->n:Landroid/view/View;

    const v1, 0x7f08011a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcag;->e:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcag;->n:Landroid/view/View;

    const v1, 0x7f08035c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcag;->f:Landroid/widget/EditText;

    .line 69
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcag;->p:Ljava/text/NumberFormat;

    .line 70
    invoke-static {}, Ljava/text/NumberFormat;->getCurrencyInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcag;->j:Ljava/text/NumberFormat;

    .line 71
    new-instance v1, Lfvi;

    iget-object v0, p0, Lcag;->n:Landroid/view/View;

    const v2, 0x7f080102

    .line 72
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p2, v0, v3}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;Z)V

    iput-object v1, p0, Lcag;->g:Lfvi;

    .line 73
    new-instance v1, Lfvi;

    iget-object v0, p0, Lcag;->n:Landroid/view/View;

    const v2, 0x7f080359

    .line 74
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p2, v0, v3}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;Z)V

    iput-object v1, p0, Lcag;->h:Lfvi;

    .line 75
    new-instance v1, Lfvi;

    iget-object v0, p0, Lcag;->n:Landroid/view/View;

    const v2, 0x7f08034d

    .line 76
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p2, v0, v3}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;Z)V

    iput-object v1, p0, Lcag;->i:Lfvi;

    .line 78
    new-instance v0, Lcah;

    invoke-direct {v0, p0}, Lcah;-><init>(Lcag;)V

    .line 89
    iget-object v1, p0, Lcag;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v1, p0, Lcag;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    new-instance v0, Lcai;

    invoke-direct {v0, p0}, Lcai;-><init>(Lcag;)V

    .line 97
    iget-object v1, p0, Lcag;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 98
    iget-object v1, p0, Lcag;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 99
    iget-object v1, p0, Lcag;->f:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 100
    iget-object v1, p0, Lcag;->n:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 102
    new-instance v0, Lbzy;

    iget-object v1, p0, Lcag;->n:Landroid/view/View;

    invoke-direct {v0, p1, p2, v1}, Lbzy;-><init>(Landroid/content/Context;Leyp;Landroid/view/View;)V

    iput-object v0, p0, Lcag;->m:Lbzy;

    .line 106
    return-void
.end method

.method static synthetic a(Lcag;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33
    iget-boolean v0, p0, Lcag;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcag;->q:Z

    iget-object v0, p0, Lcag;->d:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcag;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcag;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lcag;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v0, p0, Lcag;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocusFromTouch()Z

    iget-object v0, p0, Lcag;->o:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcag;->o:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcag;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lfom;
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lcag;->b()V

    .line 134
    iget-object v0, p0, Lcag;->k:Lfom;

    return-object v0
.end method

.method a(D)V
    .locals 5

    .prologue
    .line 224
    iget-object v0, p0, Lcag;->k:Lfom;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcag;->l:Z

    if-nez v0, :cond_0

    .line 225
    iget-object v0, p0, Lcag;->k:Lfom;

    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lfom;->a(J)V

    .line 227
    :cond_0
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 162
    iget-object v0, p0, Lcag;->k:Lfom;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcag;->q:Z

    if-nez v0, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iput-boolean v2, p0, Lcag;->q:Z

    .line 169
    :try_start_0
    iget-object v0, p0, Lcag;->p:Ljava/text/NumberFormat;

    iget-object v1, p0, Lcag;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 175
    :goto_1
    invoke-virtual {p0, v0, v1}, Lcag;->a(D)V

    .line 176
    iget-object v0, p0, Lcag;->f:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcag;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcag;->o:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcag;->o:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcag;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 182
    :cond_2
    invoke-virtual {p0}, Lcag;->c()V

    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    const-string v0, "Failed to parse viewer\'s tip currency input."

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcag;->k:Lfom;

    invoke-virtual {v0}, Lfom;->c()D

    move-result-wide v0

    goto :goto_1
.end method

.method public c()V
    .locals 4

    .prologue
    .line 210
    iget-object v0, p0, Lcag;->k:Lfom;

    if-nez v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcag;->k:Lfom;

    invoke-virtual {v0}, Lfom;->c()D

    move-result-wide v0

    .line 215
    iget-object v2, p0, Lcag;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcag;->j:Ljava/text/NumberFormat;

    invoke-virtual {v3, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v2, p0, Lcag;->f:Landroid/widget/EditText;

    iget-object v3, p0, Lcag;->p:Ljava/text/NumberFormat;

    invoke-virtual {v3, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
