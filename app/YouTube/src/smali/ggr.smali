.class public final Lggr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lggs;

.field private final d:Lggt;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lggs;Lggt;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lggr;->a:Ljava/lang/String;

    .line 61
    invoke-static {p2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lggr;->b:Ljava/lang/String;

    .line 62
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggs;

    iput-object v0, p0, Lggr;->c:Lggs;

    .line 63
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggt;

    iput-object v0, p0, Lggr;->d:Lggt;

    .line 64
    return-void
.end method


# virtual methods
.method public final a(Lfao;)Lfao;
    .locals 3

    .prologue
    .line 75
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    const-string v0, "cplatform"

    iget-object v1, p0, Lggr;->c:Lggs;

    iget-object v1, v1, Lggs;->d:Ljava/lang/String;

    .line 77
    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "c"

    iget-object v2, p0, Lggr;->d:Lggt;

    iget-object v2, v2, Lggt;->c:Ljava/lang/String;

    .line 78
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "cver"

    iget-object v2, p0, Lggr;->b:Ljava/lang/String;

    .line 79
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "cos"

    const-string v2, "Android"

    .line 80
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "cosver"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 81
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "cbr"

    iget-object v2, p0, Lggr;->a:Ljava/lang/String;

    .line 82
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "cbrver"

    iget-object v2, p0, Lggr;->b:Ljava/lang/String;

    .line 85
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "cbrand"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 86
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "cmodel"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 87
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 88
    return-object p1
.end method
