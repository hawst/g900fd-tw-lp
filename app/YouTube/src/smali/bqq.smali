.class public final Lbqq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbxi;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Z

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/view/View;

.field private e:Lfmy;

.field private f:Landroid/content/Context;

.field private g:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lbqq;->a:Landroid/view/View;

    .line 35
    iput-boolean p2, p0, Lbqq;->b:Z

    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbqq;->f:Landroid/content/Context;

    .line 37
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbqq;->g:Landroid/content/res/Resources;

    .line 38
    const v0, 0x7f0801f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f0801ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbqq;->d:Landroid/view/View;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 71
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lbqq;->c:Landroid/widget/TextView;

    invoke-static {v0, v1, p1}, La;->a(Landroid/content/Context;Landroid/view/View;I)V

    .line 134
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    return-void
.end method

.method public final a(Lfmy;)V
    .locals 6

    .prologue
    const v5, 0x7f070093

    const v4, 0x7f020277

    const/4 v1, 0x0

    .line 49
    iput-object p1, p0, Lbqq;->e:Lfmy;

    .line 50
    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 51
    iget-object v0, p0, Lbqq;->d:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 52
    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 53
    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 54
    iget-boolean v0, p1, Lfmy;->d:Z

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lbqq;->e:Lfmy;

    invoke-virtual {v0}, Lfmy;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    const v2, 0x7f090267

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lbqq;->f:Landroid/content/Context;

    const v3, 0x7f09031d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lbqq;->g:Landroid/content/res/Resources;

    const v3, 0x7f070095

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    const v2, 0x7f02019a

    invoke-virtual {v0, v2, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 59
    :goto_1
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lbqq;->e:Lfmy;

    invoke-virtual {v2}, Lfmy;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 57
    :cond_1
    iget-object v0, p0, Lbqq;->e:Lfmy;

    invoke-virtual {v0}, Lfmy;->b()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    const v2, 0x7f090266

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lbqq;->f:Landroid/content/Context;

    const v3, 0x7f09031b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbqq;->e:Lfmy;

    invoke-virtual {v0}, Lfmy;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lbqq;->g:Landroid/content/res/Resources;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lbqq;->e:Lfmy;

    iget-object v2, v0, Lfmy;->a:Lhwp;

    iget-object v2, v2, Lhwp;->k:Lhwq;

    if-nez v2, :cond_3

    move v0, v1

    :goto_3
    const/4 v2, 0x4

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    const v2, 0x7f0201fb

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_4
    iget-object v0, p0, Lbqq;->e:Lfmy;

    iget-object v0, v0, Lfmy;->a:Lhwp;

    iget-object v0, v0, Lhwp;->j:Lhiq;

    if-eqz v0, :cond_5

    iget v0, v0, Lhiq;->a:I

    const/16 v2, 0x1f

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    const v2, 0x7f020199

    invoke-virtual {v0, v2, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lbqq;->e:Lfmy;

    invoke-virtual {v2}, Lfmy;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_3
    iget-object v0, v0, Lfmy;->a:Lhwp;

    iget-object v0, v0, Lhwp;->k:Lhwq;

    iget v0, v0, Lhwq;->a:I

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    const v2, 0x7f020203

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_1

    :cond_6
    iget-boolean v0, p0, Lbqq;->b:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lbqq;->g:Landroid/content/res/Resources;

    const v3, 0x7f070094

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    const v2, 0x7f020198

    invoke-virtual {v0, v2, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lbqq;->g:Landroid/content/res/Resources;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    const v2, 0x7f020197

    invoke-virtual {v0, v2, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    const v1, 0x7f020274

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1
.end method

.method public final b(Lfmy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lbqq;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 64
    iget-object v0, p0, Lbqq;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lbqq;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    return-void
.end method
