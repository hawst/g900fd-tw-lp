.class public final Lsk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field g:I

.field h:I

.field public i:I

.field public j:I

.field public k:I

.field l:Lrj;

.field private final m:Lsj;

.field private final n:Ljava/util/ArrayList;

.field private o:I

.field private p:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lsj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsk;->n:Ljava/util/ArrayList;

    .line 710
    const/4 v0, -0x1

    iput v0, p0, Lsk;->o:I

    .line 764
    iput-object p1, p0, Lsk;->m:Lsj;

    .line 765
    iput-object p2, p0, Lsk;->a:Ljava/lang/String;

    .line 766
    iput-object p3, p0, Lsk;->b:Ljava/lang/String;

    .line 767
    return-void
.end method


# virtual methods
.method final a(Lrj;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1178
    const/4 v0, 0x0

    .line 1179
    iget-object v2, p0, Lsk;->l:Lrj;

    if-eq v2, p1, :cond_a

    .line 1180
    iput-object p1, p0, Lsk;->l:Lrj;

    .line 1181
    if-eqz p1, :cond_a

    .line 1182
    iget-object v2, p0, Lsk;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lrj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lsb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1183
    invoke-virtual {p1}, Lrj;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsk;->c:Ljava/lang/String;

    move v0, v1

    .line 1186
    :cond_0
    iget-object v2, p0, Lsk;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lrj;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lsb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1187
    invoke-virtual {p1}, Lrj;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsk;->d:Ljava/lang/String;

    move v0, v1

    .line 1190
    :cond_1
    iget-boolean v2, p0, Lsk;->e:Z

    invoke-virtual {p1}, Lrj;->d()Z

    move-result v3

    if-eq v2, v3, :cond_c

    .line 1191
    invoke-virtual {p1}, Lrj;->d()Z

    move-result v0

    iput-boolean v0, p0, Lsk;->e:Z

    .line 1194
    :goto_0
    iget-boolean v0, p0, Lsk;->f:Z

    invoke-virtual {p1}, Lrj;->e()Z

    move-result v2

    if-eq v0, v2, :cond_2

    .line 1195
    invoke-virtual {p1}, Lrj;->e()Z

    move-result v0

    iput-boolean v0, p0, Lsk;->f:Z

    .line 1196
    or-int/lit8 v1, v1, 0x1

    .line 1198
    :cond_2
    iget-object v0, p0, Lsk;->n:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lrj;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1199
    iget-object v0, p0, Lsk;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1200
    iget-object v0, p0, Lsk;->n:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lrj;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1201
    or-int/lit8 v1, v1, 0x1

    .line 1203
    :cond_3
    iget v0, p0, Lsk;->g:I

    invoke-virtual {p1}, Lrj;->h()I

    move-result v2

    if-eq v0, v2, :cond_4

    .line 1204
    invoke-virtual {p1}, Lrj;->h()I

    move-result v0

    iput v0, p0, Lsk;->g:I

    .line 1205
    or-int/lit8 v1, v1, 0x1

    .line 1207
    :cond_4
    iget v0, p0, Lsk;->h:I

    invoke-virtual {p1}, Lrj;->i()I

    move-result v2

    if-eq v0, v2, :cond_5

    .line 1208
    invoke-virtual {p1}, Lrj;->i()I

    move-result v0

    iput v0, p0, Lsk;->h:I

    .line 1209
    or-int/lit8 v1, v1, 0x1

    .line 1211
    :cond_5
    iget v0, p0, Lsk;->i:I

    invoke-virtual {p1}, Lrj;->l()I

    move-result v2

    if-eq v0, v2, :cond_6

    .line 1212
    invoke-virtual {p1}, Lrj;->l()I

    move-result v0

    iput v0, p0, Lsk;->i:I

    .line 1213
    or-int/lit8 v1, v1, 0x3

    .line 1215
    :cond_6
    iget v0, p0, Lsk;->j:I

    invoke-virtual {p1}, Lrj;->j()I

    move-result v2

    if-eq v0, v2, :cond_7

    .line 1216
    invoke-virtual {p1}, Lrj;->j()I

    move-result v0

    iput v0, p0, Lsk;->j:I

    .line 1217
    or-int/lit8 v1, v1, 0x3

    .line 1219
    :cond_7
    iget v0, p0, Lsk;->k:I

    invoke-virtual {p1}, Lrj;->k()I

    move-result v2

    if-eq v0, v2, :cond_8

    .line 1220
    invoke-virtual {p1}, Lrj;->k()I

    move-result v0

    iput v0, p0, Lsk;->k:I

    .line 1221
    or-int/lit8 v1, v1, 0x3

    .line 1223
    :cond_8
    iget v0, p0, Lsk;->o:I

    invoke-virtual {p1}, Lrj;->m()I

    move-result v2

    if-eq v0, v2, :cond_9

    .line 1224
    invoke-virtual {p1}, Lrj;->m()I

    move-result v0

    iput v0, p0, Lsk;->o:I

    .line 1225
    or-int/lit8 v1, v1, 0x5

    .line 1228
    :cond_9
    iget-object v0, p0, Lsk;->p:Landroid/os/Bundle;

    invoke-virtual {p1}, Lrj;->n()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Lsb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1229
    invoke-virtual {p1}, Lrj;->n()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lsk;->p:Landroid/os/Bundle;

    .line 1230
    or-int/lit8 v0, v1, 0x1

    .line 1234
    :cond_a
    :goto_1
    return v0

    :cond_b
    move v0, v1

    goto :goto_1

    :cond_c
    move v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lsk;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1084
    invoke-static {}, Lsb;->d()V

    .line 1085
    sget-object v0, Lsb;->a:Lsf;

    iget v1, p0, Lsk;->k:I

    const/4 v2, 0x0

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, v0, Lsf;->i:Lsk;

    if-ne p0, v2, :cond_0

    iget-object v2, v0, Lsf;->j:Lrq;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lsf;->j:Lrq;

    invoke-virtual {v0, v1}, Lrq;->a(I)V

    .line 1086
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 913
    invoke-static {}, Lsb;->d()V

    .line 918
    iget-object v0, p0, Lsk;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 919
    :goto_0
    if-ge v2, v3, :cond_1

    .line 920
    iget-object v0, p0, Lsk;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    invoke-virtual {v0, p1}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 921
    const/4 v0, 0x1

    .line 924
    :goto_1
    return v0

    .line 919
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 924
    goto :goto_1
.end method

.method public final a(Lrz;)Z
    .locals 2

    .prologue
    .line 887
    if-nez p1, :cond_0

    .line 888
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 890
    :cond_0
    invoke-static {}, Lsb;->d()V

    .line 891
    iget-object v0, p0, Lsk;->n:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Lrz;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lsk;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1098
    invoke-static {}, Lsb;->d()V

    .line 1099
    if-eqz p1, :cond_0

    .line 1100
    sget-object v0, Lsb;->a:Lsf;

    iget-object v1, v0, Lsf;->i:Lsk;

    if-ne p0, v1, :cond_0

    iget-object v1, v0, Lsf;->j:Lrq;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lsf;->j:Lrq;

    invoke-virtual {v0, p1}, Lrq;->b(I)V

    .line 1102
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 846
    invoke-static {}, Lsb;->d()V

    .line 847
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0}, Lsf;->b()Lsk;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 858
    invoke-static {}, Lsb;->d()V

    .line 859
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0}, Lsf;->a()Lsk;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lsk;->n:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final f()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1148
    iget-object v0, p0, Lsk;->p:Landroid/os/Bundle;

    return-object v0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1155
    invoke-static {}, Lsb;->d()V

    .line 1156
    sget-object v0, Lsb;->a:Lsf;

    iget-object v1, v0, Lsf;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring attempt to select removed route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1157
    :goto_0
    return-void

    .line 1156
    :cond_0
    iget-boolean v1, p0, Lsk;->e:Z

    if-nez v1, :cond_1

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring attempt to select disabled route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p0}, Lsf;->a(Lsk;)V

    goto :goto_0
.end method

.method final h()Lrm;
    .locals 1

    .prologue
    .line 1242
    iget-object v0, p0, Lsk;->m:Lsj;

    invoke-static {}, Lsb;->d()V

    iget-object v0, v0, Lsj;->a:Lrm;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaRouter.RouteInfo{ uniqueId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lsk;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsk;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsk;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lsk;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connecting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lsk;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsk;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsk;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeHandling="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsk;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsk;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsk;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presentationDisplayId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsk;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsk;->p:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", providerPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsk;->m:Lsj;

    invoke-virtual {v1}, Lsj;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
