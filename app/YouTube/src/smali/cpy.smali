.class public final Lcpy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Set;

.field private static final b:Ljava/util/Set;

.field private static final c:Ljava/util/Set;


# instance fields
.field private final d:Lgku;

.field private final e:Lgku;

.field private final f:Lezj;

.field private final g:Ldau;

.field private final h:Lcnm;

.field private final i:Lcnu;

.field private final j:Levn;

.field private final k:Lcnx;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 65
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "YT:ADSENSE"

    aput-object v1, v0, v3

    const-string v1, "ADSENSE"

    aput-object v1, v0, v4

    const-string v1, "ADSENSE/ADX"

    aput-object v1, v0, v5

    invoke-static {v0}, La;->e([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcpy;->a:Ljava/util/Set;

    .line 69
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "YT:DOUBLECLICK"

    aput-object v1, v0, v3

    const-string v1, "GDFP"

    aput-object v1, v0, v4

    const-string v1, "DART"

    aput-object v1, v0, v5

    const-string v1, "DART_DFA"

    aput-object v1, v0, v2

    const/4 v1, 0x4

    const-string v2, "DART_DFP"

    aput-object v2, v0, v1

    invoke-static {v0}, La;->e([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcpy;->b:Ljava/util/Set;

    .line 75
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "YT:FREEWHEEL"

    aput-object v1, v0, v3

    const-string v1, "FREEWHEEL"

    aput-object v1, v0, v4

    invoke-static {v0}, La;->e([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcpy;->c:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lezj;Levn;Lcnu;Ldau;Lcnm;Lcny;Lfbc;Lcnx;)V
    .locals 3

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcpy;->f:Lezj;

    .line 99
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcpy;->j:Levn;

    .line 100
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnu;

    iput-object v0, p0, Lcpy;->i:Lcnu;

    .line 101
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldau;

    iput-object v0, p0, Lcpy;->g:Ldau;

    .line 102
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnm;

    iput-object v0, p0, Lcpy;->h:Lcnm;

    .line 103
    new-instance v0, Lerx;

    .line 104
    iget-object v1, p3, Lcnu;->a:Lery;

    invoke-direct {v0, v1}, Lerx;-><init>(Lery;)V

    new-instance v1, Lcrf;

    new-instance v2, Lcqy;

    invoke-direct {v2, p7}, Lcqy;-><init>(Lfbc;)V

    invoke-direct {v1, p7, p1, v2}, Lcrf;-><init>(Lfbc;Lezj;Lcqy;)V

    const/4 v2, 0x0

    .line 103
    invoke-virtual {p6, v0, v1, v2}, Lcny;->a(Lerx;Lcrf;Z)Lgko;

    move-result-object v0

    iput-object v0, p0, Lcpy;->d:Lgku;

    .line 107
    new-instance v0, Lerx;

    .line 108
    iget-object v1, p3, Lcnu;->a:Lery;

    invoke-direct {v0, v1}, Lerx;-><init>(Lery;)V

    new-instance v1, Lcrf;

    new-instance v2, Lcqy;

    invoke-direct {v2, p7}, Lcqy;-><init>(Lfbc;)V

    invoke-direct {v1, p7, p1, v2}, Lcrf;-><init>(Lfbc;Lezj;Lcqy;)V

    const/4 v2, 0x1

    .line 107
    invoke-virtual {p6, v0, v1, v2}, Lcny;->a(Lerx;Lcrf;Z)Lgko;

    move-result-object v0

    iput-object v0, p0, Lcpy;->e:Lgku;

    .line 111
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnx;

    iput-object v0, p0, Lcpy;->k:Lcnx;

    .line 112
    return-void
.end method

.method private a(Landroid/net/Uri;Lfoy;Lfai;I)Lfoy;
    .locals 8

    .prologue
    .line 490
    iget-object v0, p0, Lcpy;->j:Levn;

    new-instance v1, Lcze;

    .line 492
    invoke-virtual {p2}, Lfoy;->h()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, p4, v2}, Lcze;-><init>(II)V

    .line 490
    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 493
    iget-object v0, p2, Lfoy;->Z:Lfoy;

    if-eqz v0, :cond_1

    .line 496
    iget-object v0, p2, Lfoy;->Z:Lfoy;

    invoke-virtual {v0}, Lfoy;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    const/4 v0, 0x0

    .line 537
    :goto_0
    return-object v0

    .line 499
    :cond_0
    iget-object v0, p2, Lfoy;->Z:Lfoy;

    invoke-virtual {v0}, Lfoy;->b()Lfpc;

    move-result-object v0

    .line 500
    invoke-virtual {p2}, Lfoy;->b()Lfpc;

    move-result-object v1

    const/4 v2, 0x0

    iput-object v2, v1, Lfpc;->ad:Lfoy;

    invoke-virtual {v1}, Lfpc;->a()Lfoy;

    move-result-object v1

    iput-object v1, v0, Lfpc;->ae:Lfoy;

    .line 501
    invoke-virtual {v0}, Lfpc;->a()Lfoy;

    move-result-object v0

    goto :goto_0

    .line 503
    :cond_1
    invoke-static {}, Leud;->a()Leud;

    move-result-object v1

    .line 505
    iget-object v0, p0, Lcpy;->f:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v2

    .line 506
    invoke-virtual {p3}, Lfai;->a()J

    move-result-wide v4

    .line 507
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_2

    .line 508
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0

    .line 510
    :cond_2
    iget-object v0, p0, Lcpy;->k:Lcnx;

    invoke-virtual {v0}, Lcnx;->a()Ljava/util/regex/Pattern;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcpy;->e:Lgku;

    :goto_1
    invoke-interface {v0, p1, v1}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 513
    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5, v0}, Leud;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 529
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 530
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 510
    :cond_4
    iget-object v0, p0, Lcpy;->d:Lgku;

    goto :goto_1

    .line 514
    :catch_0
    move-exception v0

    .line 516
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 517
    instance-of v0, v1, Lorg/apache/http/client/HttpResponseException;

    if-nez v0, :cond_5

    instance-of v0, v1, Ljava/lang/IllegalStateException;

    if-eqz v0, :cond_6

    :cond_5
    sget-object v0, Lcuw;->k:Lcuw;

    .line 520
    :goto_2
    instance-of v2, v1, Lfaz;

    if-eqz v2, :cond_7

    .line 522
    new-instance v2, Lcno;

    const-string v3, "BadXML u:%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 523
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 522
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1, p2, v0}, Lcno;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lfoy;Lcuw;)V

    throw v2

    .line 517
    :cond_6
    sget-object v0, Lcuw;->i:Lcuw;

    goto :goto_2

    .line 525
    :cond_7
    new-instance v2, Lcno;

    const-string v3, "BadReq u:%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 526
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 525
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1, p2, v0}, Lcno;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lfoy;Lcuw;)V

    throw v2

    .line 533
    :cond_8
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoy;

    .line 534
    invoke-virtual {v0}, Lfoy;->b()Lfpc;

    move-result-object v0

    .line 535
    iput-wide v2, v0, Lfpc;->Z:J

    .line 536
    iput-object p2, v0, Lfpc;->ae:Lfoy;

    .line 537
    invoke-virtual {v0}, Lfpc;->a()Lfoy;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private a(Lfoy;JLfai;II)Lfoy;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 447
    :goto_0
    iget-boolean v0, p1, Lfoy;->Y:Z

    if-eqz v0, :cond_4

    .line 452
    const/4 v0, 0x2

    if-ne p6, v0, :cond_0

    .line 453
    const-string v0, "ADSENSE/ADX"

    iget-object v1, p1, Lfoy;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 454
    add-int/lit8 p6, p6, -0x1

    .line 458
    :cond_0
    if-gtz p6, :cond_1

    .line 459
    new-instance v0, Lcno;

    const/4 v1, 0x0

    sget-object v2, Lcuw;->l:Lcuw;

    invoke-direct {v0, v1, p1, v2}, Lcno;-><init>(Ljava/lang/Throwable;Lfoy;Lcuw;)V

    throw v0

    .line 462
    :cond_1
    iget-object v0, p1, Lfoy;->X:Landroid/net/Uri;

    .line 461
    invoke-direct {p0, v0, p1, p4, p5}, Lcpy;->a(Landroid/net/Uri;Lfoy;Lfai;I)Lfoy;

    move-result-object v0

    .line 470
    :cond_2
    :goto_1
    if-nez v0, :cond_6

    move-object p1, v0

    .line 471
    :cond_3
    return-object p1

    .line 463
    :cond_4
    iget-object v0, p1, Lfoy;->N:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 465
    iget-object v0, p1, Lfoy;->N:Landroid/net/Uri;

    .line 464
    invoke-direct {p0, v0, p1, p4, p5}, Lcpy;->a(Landroid/net/Uri;Lfoy;Lfai;I)Lfoy;

    move-result-object v0

    .line 466
    if-eqz v0, :cond_2

    iget-object v1, v0, Lfoy;->aa:Lfoy;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lfoy;->aa:Lfoy;

    iget-object v1, v1, Lfoy;->N:Landroid/net/Uri;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lfoy;->aa:Lfoy;

    invoke-virtual {v0}, Lfoy;->b()Lfpc;

    move-result-object v0

    iget-object v2, v1, Lfoy;->N:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "dfaexp=1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, v1, Lfoy;->t:Landroid/net/Uri;

    iput-object v2, v0, Lfpc;->v:Landroid/net/Uri;

    iget-object v2, v1, Lfoy;->p:Lfrf;

    iput-object v2, v0, Lfpc;->r:Lfrf;

    iget-object v2, v1, Lfoy;->q:Lflp;

    iput-object v2, v0, Lfpc;->s:Lflp;

    iget-object v2, v1, Lfoy;->r:Lfqy;

    iput-object v2, v0, Lfpc;->t:Lfqy;

    iget-object v2, v1, Lfoy;->c:Ljava/lang/String;

    iput-object v2, v0, Lfpc;->i:Ljava/lang/String;

    invoke-virtual {v1}, Lfoy;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lfpc;->k:Ljava/lang/String;

    iget v2, v1, Lfoy;->o:I

    iput v2, v0, Lfpc;->q:I

    iget-boolean v1, v1, Lfoy;->T:Z

    iput-boolean v1, v0, Lfpc;->V:Z

    :cond_5
    invoke-virtual {v0}, Lfpc;->a()Lfoy;

    move-result-object v0

    goto :goto_1

    .line 475
    :cond_6
    invoke-virtual {v0}, Lfoy;->b()Lfpc;

    move-result-object v2

    .line 476
    invoke-direct {p0, v0}, Lcpy;->a(Lfoy;)Lfpb;

    move-result-object v1

    iput-object v1, v2, Lfpc;->o:Lfpb;

    .line 477
    iget-wide v0, v0, Lfoy;->R:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_7

    .line 478
    if-eqz p1, :cond_8

    .line 479
    iget-wide v0, p1, Lfoy;->R:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_8

    .line 480
    iget-wide v0, p1, Lfoy;->R:J

    .line 478
    :goto_2
    iput-wide v0, v2, Lfpc;->R:J

    .line 483
    :cond_7
    invoke-virtual {v2}, Lfpc;->a()Lfoy;

    move-result-object p1

    add-int/lit8 p6, p6, -0x1

    goto/16 :goto_0

    :cond_8
    move-wide v0, p2

    .line 480
    goto :goto_2
.end method

.method private a(Lfoy;Ljava/util/List;Less;)Lfoy;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 257
    iget-object v0, p1, Lfoy;->aa:Lfoy;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfoy;->aa:Lfoy;

    move-object v1, p1

    :goto_0
    iget-object v4, v0, Lfoy;->aa:Lfoy;

    if-eqz v4, :cond_1

    iget-object v1, v0, Lfoy;->aa:Lfoy;

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0

    :cond_0
    move-object v1, p1

    :cond_1
    invoke-direct {p0, v1}, Lcpy;->a(Lfoy;)Lfpb;

    move-result-object v5

    .line 258
    invoke-virtual {p1}, Lfoy;->b()Lfpc;

    move-result-object v6

    .line 259
    iget-object v0, p3, Less;->f:Ljava/lang/String;

    iput-object v0, v6, Lfpc;->c:Ljava/lang/String;

    .line 260
    iget-object v0, p3, Less;->e:Ljava/lang/String;

    iput-object v0, v6, Lfpc;->g:Ljava/lang/String;

    .line 261
    iput-object v5, v6, Lfpc;->o:Lfpb;

    .line 262
    iget-object v1, p1, Lfoy;->aa:Lfoy;

    if-eqz v1, :cond_7

    iget-object v0, v1, Lfoy;->m:Lfpb;

    :goto_1
    if-eqz v1, :cond_8

    sget-object v4, Lfpb;->b:Lfpb;

    if-ne v4, v0, :cond_8

    iget-boolean v4, v1, Lfoy;->P:Z

    if-eqz v4, :cond_8

    move v4, v2

    :goto_2
    if-eqz v1, :cond_9

    sget-object v7, Lfpb;->a:Lfpb;

    if-ne v7, v0, :cond_9

    iget-boolean v0, v1, Lfoy;->P:Z

    if-eqz v0, :cond_9

    iget-object v0, v1, Lfoy;->N:Landroid/net/Uri;

    if-eqz v0, :cond_9

    move v0, v2

    :goto_3
    iget-boolean v1, p1, Lfoy;->P:Z

    if-eqz v1, :cond_a

    sget-object v1, Lfpb;->b:Lfpb;

    iget-object v7, p1, Lfoy;->m:Lfpb;

    if-eq v1, v7, :cond_2

    sget-object v1, Lfpb;->a:Lfpb;

    iget-object v7, p1, Lfoy;->m:Lfpb;

    if-eq v1, v7, :cond_2

    sget-object v1, Lfpb;->c:Lfpb;

    iget-object v7, p1, Lfoy;->m:Lfpb;

    if-ne v1, v7, :cond_a

    :cond_2
    move v1, v2

    :goto_4
    if-nez v1, :cond_3

    if-nez v4, :cond_3

    if-eqz v0, :cond_4

    :cond_3
    move v3, v2

    :cond_4
    iput-boolean v3, v6, Lfpc;->T:Z

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v5, Lfpb;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lfoy;->f()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "_1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lfpc;->p:Ljava/lang/String;

    .line 264
    iget-object v0, p3, Less;->g:[B

    iput-object v0, v6, Lfpc;->f:[B

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lfoy;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v6, Lfpc;->b:Ljava/util/List;

    .line 268
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoy;

    .line 269
    iget-object v0, v0, Lfoy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 270
    invoke-virtual {v6, v0}, Lfpc;->a(Landroid/net/Uri;)Lfpc;

    goto :goto_5

    .line 262
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_8
    move v4, v3

    goto/16 :goto_2

    :cond_9
    move v0, v3

    goto :goto_3

    :cond_a
    move v1, v3

    goto :goto_4

    .line 273
    :cond_b
    invoke-virtual {v6}, Lfpc;->a()Lfoy;

    move-result-object v0

    return-object v0
.end method

.method private a(Lfoy;[BLfai;Ljava/util/Map;)Lfoy;
    .locals 10

    .prologue
    .line 552
    invoke-static {}, Lb;->b()V

    .line 554
    iget-object v0, p0, Lcpy;->g:Ldau;

    invoke-interface {v0, p1}, Ldau;->a(Lfoy;)Ldaq;

    move-result-object v0

    .line 558
    :try_start_0
    iget-object v1, p1, Lfoy;->c:Ljava/lang/String;

    invoke-interface {p4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 560
    iget-object v0, p1, Lfoy;->c:Ljava/lang/String;

    invoke-interface {p4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrl;

    .line 578
    :goto_0
    if-nez v0, :cond_2

    .line 579
    new-instance v0, Lcpz;

    const-string v1, "null playerResponse"

    invoke-direct {v0, p1, v1}, Lcpz;-><init>(Lfoy;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 597
    :catch_0
    move-exception v0

    .line 598
    const-string v1, "Error retrieving streams for ad video"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 599
    new-instance v1, Lcpz;

    invoke-direct {v1, p1, v0}, Lcpz;-><init>(Lfoy;Ljava/lang/Exception;)V

    throw v1

    .line 562
    :cond_0
    :try_start_1
    invoke-virtual {p3}, Lfai;->a()J

    move-result-wide v8

    .line 563
    const-wide/16 v2, 0x0

    cmp-long v1, v8, v2

    if-gtz v1, :cond_1

    .line 564
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 600
    :catch_1
    move-exception v0

    .line 601
    const-string v1, "Error retrieving streams for ad video"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 602
    new-instance v1, Lcpz;

    invoke-direct {v1, p1, v0}, Lcpz;-><init>(Lfoy;Ljava/lang/Exception;)V

    throw v1

    .line 569
    :cond_1
    :try_start_2
    iget-object v1, p1, Lfoy;->c:Ljava/lang/String;

    const-string v3, ""

    const-string v4, ""

    const/4 v5, -0x1

    const/4 v6, -0x1

    move-object v2, p2

    .line 568
    invoke-virtual/range {v0 .. v6}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;II)Lgky;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 575
    invoke-virtual {v0, v8, v9, v1}, Lgky;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrl;

    goto :goto_0

    .line 581
    :cond_2
    invoke-virtual {v0}, Lfrl;->g()Lflo;

    move-result-object v1

    invoke-virtual {v1}, Lflo;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 582
    new-instance v1, Lcpz;

    const-string v2, "unplayable. status: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 585
    invoke-virtual {v0}, Lfrl;->g()Lflo;

    move-result-object v0

    iget v0, v0, Lflo;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    .line 584
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lcpz;-><init>(Lfoy;Ljava/lang/String;)V

    throw v1

    .line 587
    :cond_3
    invoke-virtual {p1}, Lfoy;->b()Lfpc;

    move-result-object v1

    .line 588
    iget-object v2, v0, Lfrl;->d:Lfrf;

    iput-object v2, v1, Lfpc;->r:Lfrf;

    .line 589
    invoke-virtual {v0}, Lfrl;->h()Lflp;

    move-result-object v2

    iput-object v2, v1, Lfpc;->s:Lflp;

    .line 590
    invoke-virtual {v0}, Lfrl;->i()Lfqy;

    move-result-object v2

    iput-object v2, v1, Lfpc;->t:Lfqy;

    .line 591
    invoke-virtual {v0}, Lfrl;->p()Lflr;

    move-result-object v2

    iput-object v2, v1, Lfpc;->u:Lflr;

    .line 592
    invoke-virtual {v0}, Lfrl;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lfpc;->j:Ljava/lang/String;

    .line 593
    invoke-virtual {v0}, Lfrl;->c()I

    move-result v2

    iput v2, v1, Lfpc;->q:I

    .line 594
    invoke-virtual {v0}, Lfrl;->j()Lhqz;

    move-result-object v2

    iput-object v2, v1, Lfpc;->X:Lhqz;

    .line 595
    invoke-virtual {v0}, Lfrl;->k()Lfkg;

    move-result-object v0

    iput-object v0, v1, Lfpc;->Y:Lfkg;

    .line 596
    invoke-virtual {v1}, Lfpc;->a()Lfoy;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    return-object v0
.end method

.method private a(Lfoy;)Lfpb;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 383
    .line 384
    iget-object v0, p1, Lfoy;->aa:Lfoy;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 385
    :goto_0
    iget-object v3, p1, Lfoy;->l:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Lcpy;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 386
    sget-object v0, Lfpb;->a:Lfpb;

    .line 392
    :goto_1
    return-object v0

    .line 384
    :cond_0
    iget-object v0, p1, Lfoy;->aa:Lfoy;

    iget-object v0, v0, Lfoy;->X:Landroid/net/Uri;

    goto :goto_0

    .line 387
    :cond_1
    iget-object v3, p1, Lfoy;->l:Ljava/lang/String;

    if-eqz v3, :cond_2

    sget-object v4, Lcpy;->b:Ljava/util/Set;

    invoke-static {v3}, La;->A(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v3, v1

    :goto_2
    if-eqz v3, :cond_4

    .line 388
    sget-object v0, Lfpb;->b:Lfpb;

    goto :goto_1

    .line 387
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".doubleclick.net"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-direct {p0, v0, v3}, Lcpy;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    goto :goto_2

    :cond_3
    move v3, v2

    goto :goto_2

    .line 389
    :cond_4
    iget-object v3, p1, Lfoy;->l:Ljava/lang/String;

    if-eqz v3, :cond_5

    sget-object v4, Lcpy;->c:Ljava/util/Set;

    invoke-static {v3}, La;->A(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 390
    sget-object v0, Lfpb;->c:Lfpb;

    goto :goto_1

    .line 389
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v3, ".fwmrm.net"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_3

    .line 392
    :cond_7
    sget-object v0, Lfpb;->d:Lfpb;

    goto :goto_1
.end method

.method private static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 239
    :try_start_0
    invoke-static {p0}, Lglb;->a(Landroid/net/Uri;)Lglb;

    move-result-object v0

    .line 240
    iget-object v2, v0, Lglb;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_0

    .line 241
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to find video id in watch uri from VastAd "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    move-object v0, v1

    .line 247
    :goto_0
    return-object v0

    .line 244
    :cond_0
    iget-object v0, v0, Lglb;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246
    :catch_0
    move-exception v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to parse watch uri from VastAd "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    move-object v0, v1

    .line 247
    goto :goto_0
.end method

.method private a(Lcuw;Ljava/lang/String;Less;Lfoy;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 224
    iget-object v0, p0, Lcpy;->h:Lcnm;

    if-eqz p4, :cond_0

    :goto_0
    invoke-virtual {v0, p3, p4, p5}, Lcnm;->a(Less;Lfoy;Ljava/lang/String;)Lcnh;

    move-result-object v0

    .line 232
    new-instance v1, Lcuv;

    invoke-direct {v1, p1, p2}, Lcuv;-><init>(Lcuw;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcnh;->b(Lcuv;)V

    .line 235
    return-void

    .line 224
    :cond_0
    sget-object v1, Lfoy;->a:Lfoy;

    .line 228
    invoke-virtual {v1}, Lfoy;->b()Lfpc;

    move-result-object v1

    const-wide v2, 0x7fffffffffffffffL

    .line 229
    iput-wide v2, v1, Lfpc;->R:J

    .line 230
    invoke-virtual {v1}, Lfpc;->a()Lfoy;

    move-result-object p4

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 433
    if-eqz p2, :cond_1

    sget-object v1, Lcpy;->a:Ljava/util/Set;

    invoke-static {p2}, La;->A(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 437
    :cond_0
    :goto_0
    return v0

    .line 436
    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcpy;->i:Lcnu;

    .line 437
    iget-object v1, v1, Lcnu;->a:Lery;

    invoke-virtual {v1, p1}, Lery;->b(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Less;Ljava/lang/String;JLfai;Ljava/util/Map;)Lfoy;
    .locals 15

    .prologue
    .line 139
    invoke-static/range {p1 .. p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    invoke-static {}, Lb;->b()V

    .line 141
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 142
    iget-object v2, p0, Lcpy;->f:Lezj;

    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    add-long v12, v2, p3

    .line 143
    const/4 v7, 0x1

    .line 144
    const/4 v6, 0x0

    .line 145
    move-object/from16 v0, p1

    iget-object v2, v0, Less;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move-object v9, v6

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lfoy;

    .line 147
    add-int/lit8 v10, v7, 0x1

    const/4 v8, 0x3

    move-object v2, p0

    move-wide v4, v12

    move-object/from16 v6, p5

    :try_start_0
    invoke-direct/range {v2 .. v8}, Lcpy;->a(Lfoy;JLfai;II)Lfoy;
    :try_end_0
    .catch Lcno; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v6

    .line 149
    if-nez v6, :cond_0

    move-object v9, v6

    move v7, v10

    .line 150
    goto :goto_0

    .line 152
    :cond_0
    :try_start_1
    invoke-virtual {v6}, Lfoy;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 154
    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    sget-object v2, Lfpb;->b:Lfpb;

    iget-object v3, v6, Lfoy;->m:Lfpb;

    if-ne v2, v3, :cond_5

    .line 158
    iget-boolean v2, v6, Lfoy;->Q:Z
    :try_end_1
    .catch Lcno; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v2, :cond_5

    .line 209
    :cond_1
    :goto_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 212
    const/4 v6, 0x0

    .line 215
    :cond_2
    :goto_2
    return-object v6

    .line 163
    :cond_3
    :try_start_2
    iget-object v2, v6, Lfoy;->aj:Lfoo;

    sget-object v3, Lfoo;->a:Lfoo;

    if-ne v2, v3, :cond_4

    .line 164
    sget-object v3, Lcuw;->h:Lcuw;

    const-string v4, "Invalid survey XML"

    move-object v2, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    invoke-direct/range {v2 .. v7}, Lcpy;->a(Lcuw;Ljava/lang/String;Less;Lfoy;Ljava/lang/String;)V

    move-object v9, v6

    move v7, v10

    .line 170
    goto :goto_0

    .line 173
    :cond_4
    move-object/from16 v0, p1

    invoke-direct {p0, v6, v11, v0}, Lcpy;->a(Lfoy;Ljava/util/List;Less;)Lfoy;
    :try_end_2
    .catch Lcno; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v6

    .line 176
    :try_start_3
    move-object/from16 v0, p1

    iget-object v2, v0, Less;->g:[B

    .line 175
    invoke-virtual {v6}, Lfoy;->g()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lfoy;->a(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcpy;->j:Levn;

    new-instance v4, Lczf;

    invoke-direct {v4}, Lczf;-><init>()V

    invoke-virtual {v3, v4}, Levn;->d(Ljava/lang/Object;)V

    invoke-virtual {v6}, Lfoy;->g()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcpy;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v2, Lcpz;

    const-string v3, "no video-id in url"

    invoke-direct {v2, v6, v3}, Lcpz;-><init>(Lfoy;Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Lcpz; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcno; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_2

    .line 177
    :catch_0
    move-exception v2

    move-object v8, v2

    .line 178
    :try_start_4
    sget-object v3, Lcuw;->j:Lcuw;

    const/4 v2, 0x1

    .line 180
    invoke-static {v8, v2}, Lgoo;->a(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v4

    move-object v2, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    .line 178
    invoke-direct/range {v2 .. v7}, Lcpy;->a(Lcuw;Ljava/lang/String;Less;Lfoy;Ljava/lang/String;)V

    .line 184
    const-string v2, "Error retrieving ad video info"

    invoke-static {v2, v8}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Lcno; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_5
    move-object v9, v6

    move v7, v10

    .line 208
    goto/16 :goto_0

    .line 175
    :cond_6
    :try_start_5
    invoke-virtual {v6}, Lfoy;->b()Lfpc;

    move-result-object v4

    iput-object v3, v4, Lfpc;->i:Ljava/lang/String;

    invoke-virtual {v4}, Lfpc;->a()Lfoy;

    move-result-object v3

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-direct {p0, v3, v2, v0, v1}, Lcpy;->a(Lfoy;[BLfai;Ljava/util/Map;)Lfoy;
    :try_end_5
    .catch Lcpz; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcno; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_2

    move-result-object v6

    goto :goto_2

    .line 188
    :catch_1
    move-exception v2

    move-object v9, v2

    move-object v8, v6

    .line 190
    :goto_3
    iget-object v2, v9, Lcno;->b:Lcuw;

    if-eqz v2, :cond_7

    .line 191
    iget-object v3, v9, Lcno;->b:Lcuw;

    :goto_4
    const/4 v2, 0x1

    .line 192
    invoke-static {v9, v2}, Lgoo;->a(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v4

    .line 194
    iget-object v6, v9, Lcno;->a:Lfoy;

    move-object v2, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    .line 189
    invoke-direct/range {v2 .. v7}, Lcpy;->a(Lcuw;Ljava/lang/String;Less;Lfoy;Ljava/lang/String;)V

    .line 196
    const-string v2, "Error resolving VAST Wrapper"

    invoke-static {v2, v9}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v9, v8

    move v7, v10

    .line 208
    goto/16 :goto_0

    .line 191
    :cond_7
    sget-object v3, Lcuw;->i:Lcuw;

    goto :goto_4

    .line 198
    :catch_2
    move-exception v2

    move-object v8, v2

    .line 199
    :goto_5
    sget-object v3, Lcuw;->k:Lcuw;

    const/4 v2, 0x1

    .line 201
    invoke-static {v8, v2}, Lgoo;->a(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v4

    move-object v2, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    .line 199
    invoke-direct/range {v2 .. v7}, Lcpy;->a(Lcuw;Ljava/lang/String;Less;Lfoy;Ljava/lang/String;)V

    .line 205
    const-string v2, "Timeout error while retrieving ad video info"

    invoke-static {v2, v8}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 214
    :cond_8
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfoy;

    .line 215
    move-object/from16 v0, p1

    invoke-direct {p0, v2, v11, v0}, Lcpy;->a(Lfoy;Ljava/util/List;Less;)Lfoy;

    move-result-object v6

    goto/16 :goto_2

    .line 198
    :catch_3
    move-exception v2

    move-object v8, v2

    move-object v6, v9

    goto :goto_5

    .line 188
    :catch_4
    move-exception v2

    move-object v8, v9

    move-object v9, v2

    goto :goto_3
.end method
