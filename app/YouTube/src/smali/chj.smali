.class public final Lchj;
.super Lchc;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyc;ILeyp;Lfhz;Lboi;Lerv;Lfdw;Lfrz;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct/range {p0 .. p9}, Lchc;-><init>(Landroid/content/Context;Lcyc;ILeyp;Lfhz;Lboi;Lerv;Lfdw;Lfrz;)V

    .line 46
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Lchj;->a:Landroid/view/View;

    .line 51
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lchj;->h:Lboi;

    iget-object v2, p0, Lchj;->f:Landroid/view/View;

    iget-object v3, p0, Lchj;->i:Lfmd;

    .line 50
    invoke-static {v0, v1, v2, v3}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 55
    return-void
.end method

.method protected final a(I)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 59
    iget-object v0, p0, Lchj;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 60
    iget-object v1, p0, Lchj;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 62
    iget-object v2, p0, Lchj;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lchj;->g:Landroid/content/res/Resources;

    const v4, 0x7f0a00b1

    .line 63
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 62
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 65
    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    .line 66
    iget-object v2, p0, Lchj;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 67
    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 68
    iget-object v2, p0, Lchj;->g:Landroid/content/res/Resources;

    const v3, 0x7f0b0020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 69
    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 70
    iget-object v0, p0, Lchj;->g:Landroid/content/res/Resources;

    const v2, 0x7f0b0021

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 71
    iget-object v0, p0, Lchj;->c:Landroid/view/View;

    iget-object v1, p0, Lchj;->g:Landroid/content/res/Resources;

    const v2, 0x7f0a00eb

    .line 72
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 71
    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 88
    :goto_0
    return-void

    .line 77
    :cond_0
    iget-object v2, p0, Lchj;->e:Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 78
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 79
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 80
    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 81
    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 82
    iget-object v0, p0, Lchj;->c:Landroid/view/View;

    iget-object v1, p0, Lchj;->g:Landroid/content/res/Resources;

    const v2, 0x7f0a00da

    .line 84
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 82
    invoke-virtual {v0, v5, v1, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method
