.class public abstract enum Lddk;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lddk;

.field public static final enum b:Lddk;

.field public static final enum c:Lddk;

.field public static final enum d:Lddk;

.field public static final enum e:Lddk;

.field public static final enum f:Lddk;

.field public static final enum g:Lddk;

.field public static final enum h:Lddk;

.field public static final enum i:Lddk;

.field private static final synthetic j:[Lddk;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 58
    new-instance v0, Lddl;

    const-string v1, "FAST_FORWARD"

    invoke-direct {v0, v1, v3}, Lddl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddk;->a:Lddk;

    .line 70
    new-instance v0, Lddm;

    const-string v1, "REWIND"

    invoke-direct {v0, v1, v4}, Lddm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddk;->b:Lddk;

    .line 81
    new-instance v0, Lddn;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v5}, Lddn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddk;->c:Lddk;

    .line 87
    new-instance v0, Lddo;

    const-string v1, "PLAY"

    invoke-direct {v0, v1, v6}, Lddo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddk;->d:Lddk;

    .line 93
    new-instance v0, Lddp;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v7}, Lddp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddk;->e:Lddk;

    .line 99
    new-instance v0, Lddq;

    const-string v1, "PREVIOUS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lddq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddk;->f:Lddk;

    .line 105
    new-instance v0, Lddr;

    const-string v1, "CC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lddr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddk;->g:Lddk;

    .line 111
    new-instance v0, Ldds;

    const-string v1, "HOME"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ldds;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddk;->h:Lddk;

    .line 117
    new-instance v0, Lddt;

    const-string v1, "SCROLL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lddt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddk;->i:Lddk;

    .line 56
    const/16 v0, 0x9

    new-array v0, v0, [Lddk;

    sget-object v1, Lddk;->a:Lddk;

    aput-object v1, v0, v3

    sget-object v1, Lddk;->b:Lddk;

    aput-object v1, v0, v4

    sget-object v1, Lddk;->c:Lddk;

    aput-object v1, v0, v5

    sget-object v1, Lddk;->d:Lddk;

    aput-object v1, v0, v6

    sget-object v1, Lddk;->e:Lddk;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lddk;->f:Lddk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lddk;->g:Lddk;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lddk;->h:Lddk;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lddk;->i:Lddk;

    aput-object v2, v0, v1

    sput-object v0, Lddk;->j:[Lddk;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lddk;
    .locals 1

    .prologue
    .line 56
    const-class v0, Lddk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lddk;

    return-object v0
.end method

.method public static values()[Lddk;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lddk;->j:[Lddk;

    invoke-virtual {v0}, [Lddk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lddk;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lddf;)V
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method
