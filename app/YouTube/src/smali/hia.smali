.class public final Lhia;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lhib;

.field public b:[B

.field public c:[Lhpc;

.field public d:Lhhw;

.field private e:Lhtx;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64377
    invoke-direct {p0}, Lidf;-><init>()V

    .line 64380
    iput-object v1, p0, Lhia;->e:Lhtx;

    .line 64383
    const-string v0, ""

    iput-object v0, p0, Lhia;->f:Ljava/lang/String;

    .line 64386
    sget-object v0, Lhib;->a:[Lhib;

    iput-object v0, p0, Lhia;->a:[Lhib;

    .line 64389
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhia;->b:[B

    .line 64392
    sget-object v0, Lhpc;->a:[Lhpc;

    iput-object v0, p0, Lhia;->c:[Lhpc;

    .line 64395
    iput-object v1, p0, Lhia;->d:Lhhw;

    .line 64377
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 64485
    .line 64486
    iget-object v0, p0, Lhia;->e:Lhtx;

    if-eqz v0, :cond_7

    .line 64487
    const/4 v0, 0x1

    iget-object v2, p0, Lhia;->e:Lhtx;

    .line 64488
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 64490
    :goto_0
    iget-object v2, p0, Lhia;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 64491
    const/4 v2, 0x3

    iget-object v3, p0, Lhia;->f:Ljava/lang/String;

    .line 64492
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 64494
    :cond_0
    iget-object v2, p0, Lhia;->a:[Lhib;

    if-eqz v2, :cond_2

    .line 64495
    iget-object v3, p0, Lhia;->a:[Lhib;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 64496
    if-eqz v5, :cond_1

    .line 64497
    const/4 v6, 0x4

    .line 64498
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 64495
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 64502
    :cond_2
    iget-object v2, p0, Lhia;->b:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    .line 64503
    const/4 v2, 0x5

    iget-object v3, p0, Lhia;->b:[B

    .line 64504
    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 64506
    :cond_3
    iget-object v2, p0, Lhia;->c:[Lhpc;

    if-eqz v2, :cond_5

    .line 64507
    iget-object v2, p0, Lhia;->c:[Lhpc;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 64508
    if-eqz v4, :cond_4

    .line 64509
    const/4 v5, 0x6

    .line 64510
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 64507
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 64514
    :cond_5
    iget-object v1, p0, Lhia;->d:Lhhw;

    if-eqz v1, :cond_6

    .line 64515
    const/16 v1, 0x9

    iget-object v2, p0, Lhia;->d:Lhhw;

    .line 64516
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64518
    :cond_6
    iget-object v1, p0, Lhia;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64519
    iput v0, p0, Lhia;->J:I

    .line 64520
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64373
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhia;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhia;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhia;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhia;->e:Lhtx;

    if-nez v0, :cond_2

    new-instance v0, Lhtx;

    invoke-direct {v0}, Lhtx;-><init>()V

    iput-object v0, p0, Lhia;->e:Lhtx;

    :cond_2
    iget-object v0, p0, Lhia;->e:Lhtx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhia;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhia;->a:[Lhib;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhib;

    iget-object v3, p0, Lhia;->a:[Lhib;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhia;->a:[Lhib;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhia;->a:[Lhib;

    :goto_2
    iget-object v2, p0, Lhia;->a:[Lhib;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhia;->a:[Lhib;

    new-instance v3, Lhib;

    invoke-direct {v3}, Lhib;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhia;->a:[Lhib;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhia;->a:[Lhib;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhia;->a:[Lhib;

    new-instance v3, Lhib;

    invoke-direct {v3}, Lhib;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhia;->a:[Lhib;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhia;->b:[B

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhia;->c:[Lhpc;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhpc;

    iget-object v3, p0, Lhia;->c:[Lhpc;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhia;->c:[Lhpc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhia;->c:[Lhpc;

    :goto_4
    iget-object v2, p0, Lhia;->c:[Lhpc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhia;->c:[Lhpc;

    new-instance v3, Lhpc;

    invoke-direct {v3}, Lhpc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhia;->c:[Lhpc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lhia;->c:[Lhpc;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lhia;->c:[Lhpc;

    new-instance v3, Lhpc;

    invoke-direct {v3}, Lhpc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhia;->c:[Lhpc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lhia;->d:Lhhw;

    if-nez v0, :cond_9

    new-instance v0, Lhhw;

    invoke-direct {v0}, Lhhw;-><init>()V

    iput-object v0, p0, Lhia;->d:Lhhw;

    :cond_9
    iget-object v0, p0, Lhia;->d:Lhhw;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x4a -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 64453
    iget-object v1, p0, Lhia;->e:Lhtx;

    if-eqz v1, :cond_0

    .line 64454
    const/4 v1, 0x1

    iget-object v2, p0, Lhia;->e:Lhtx;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 64456
    :cond_0
    iget-object v1, p0, Lhia;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 64457
    const/4 v1, 0x3

    iget-object v2, p0, Lhia;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    .line 64459
    :cond_1
    iget-object v1, p0, Lhia;->a:[Lhib;

    if-eqz v1, :cond_3

    .line 64460
    iget-object v2, p0, Lhia;->a:[Lhib;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 64461
    if-eqz v4, :cond_2

    .line 64462
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 64460
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64466
    :cond_3
    iget-object v1, p0, Lhia;->b:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_4

    .line 64467
    const/4 v1, 0x5

    iget-object v2, p0, Lhia;->b:[B

    invoke-virtual {p1, v1, v2}, Lidd;->a(I[B)V

    .line 64469
    :cond_4
    iget-object v1, p0, Lhia;->c:[Lhpc;

    if-eqz v1, :cond_6

    .line 64470
    iget-object v1, p0, Lhia;->c:[Lhpc;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 64471
    if-eqz v3, :cond_5

    .line 64472
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 64470
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64476
    :cond_6
    iget-object v0, p0, Lhia;->d:Lhhw;

    if-eqz v0, :cond_7

    .line 64477
    const/16 v0, 0x9

    iget-object v1, p0, Lhia;->d:Lhhw;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64479
    :cond_7
    iget-object v0, p0, Lhia;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 64481
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64411
    if-ne p1, p0, :cond_1

    .line 64420
    :cond_0
    :goto_0
    return v0

    .line 64412
    :cond_1
    instance-of v2, p1, Lhia;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 64413
    :cond_2
    check-cast p1, Lhia;

    .line 64414
    iget-object v2, p0, Lhia;->e:Lhtx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhia;->e:Lhtx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhia;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhia;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 64415
    :goto_2
    iget-object v2, p0, Lhia;->a:[Lhib;

    iget-object v3, p1, Lhia;->a:[Lhib;

    .line 64416
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhia;->b:[B

    iget-object v3, p1, Lhia;->b:[B

    .line 64417
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhia;->c:[Lhpc;

    iget-object v3, p1, Lhia;->c:[Lhpc;

    .line 64418
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhia;->d:Lhhw;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhia;->d:Lhhw;

    if-nez v2, :cond_3

    .line 64419
    :goto_3
    iget-object v2, p0, Lhia;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhia;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 64420
    goto :goto_0

    .line 64414
    :cond_4
    iget-object v2, p0, Lhia;->e:Lhtx;

    iget-object v3, p1, Lhia;->e:Lhtx;

    invoke-virtual {v2, v3}, Lhtx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhia;->f:Ljava/lang/String;

    iget-object v3, p1, Lhia;->f:Ljava/lang/String;

    .line 64415
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 64418
    :cond_6
    iget-object v2, p0, Lhia;->d:Lhhw;

    iget-object v3, p1, Lhia;->d:Lhhw;

    .line 64419
    invoke-virtual {v2, v3}, Lhhw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhia;->I:Ljava/util/List;

    iget-object v3, p1, Lhia;->I:Ljava/util/List;

    .line 64420
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64424
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 64426
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhia;->e:Lhtx;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 64427
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhia;->f:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 64428
    iget-object v2, p0, Lhia;->a:[Lhib;

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    .line 64434
    :cond_0
    iget-object v0, p0, Lhia;->b:[B

    if-nez v0, :cond_7

    mul-int/lit8 v2, v2, 0x1f

    .line 64440
    :cond_1
    iget-object v0, p0, Lhia;->c:[Lhpc;

    if-nez v0, :cond_8

    mul-int/lit8 v2, v2, 0x1f

    .line 64446
    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhia;->d:Lhhw;

    if-nez v0, :cond_a

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 64447
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhia;->I:Ljava/util/List;

    if-nez v2, :cond_b

    :goto_3
    add-int/2addr v0, v1

    .line 64448
    return v0

    .line 64426
    :cond_3
    iget-object v0, p0, Lhia;->e:Lhtx;

    invoke-virtual {v0}, Lhtx;->hashCode()I

    move-result v0

    goto :goto_0

    .line 64427
    :cond_4
    iget-object v0, p0, Lhia;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_5
    move v2, v0

    move v0, v1

    .line 64430
    :goto_4
    iget-object v3, p0, Lhia;->a:[Lhib;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 64431
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhia;->a:[Lhib;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    .line 64430
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 64431
    :cond_6
    iget-object v2, p0, Lhia;->a:[Lhib;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhib;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    move v0, v1

    .line 64436
    :goto_6
    iget-object v3, p0, Lhia;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 64437
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhia;->b:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 64436
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    move v0, v1

    .line 64442
    :goto_7
    iget-object v3, p0, Lhia;->c:[Lhpc;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 64443
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhia;->c:[Lhpc;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v1

    :goto_8
    add-int/2addr v2, v3

    .line 64442
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 64443
    :cond_9
    iget-object v2, p0, Lhia;->c:[Lhpc;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhpc;->hashCode()I

    move-result v2

    goto :goto_8

    .line 64446
    :cond_a
    iget-object v0, p0, Lhia;->d:Lhhw;

    invoke-virtual {v0}, Lhhw;->hashCode()I

    move-result v0

    goto :goto_2

    .line 64447
    :cond_b
    iget-object v1, p0, Lhia;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
