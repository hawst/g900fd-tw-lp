.class public final Lcvk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcvl;


# static fields
.field private static final i:Ljava/lang/Boolean;

.field private static final j:Ljava/lang/Boolean;


# instance fields
.field private a:Lcvy;

.field private b:Lezv;

.field private c:J

.field private d:J

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Ljava/util/List;

.field private k:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Lcvk;->i:Ljava/lang/Boolean;

    .line 49
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcvk;->j:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Lcvy;

    invoke-direct {v0}, Lcvy;-><init>()V

    iput-object v0, p0, Lcvk;->a:Lcvy;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcvk;->k:Ljava/util/List;

    .line 58
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcvk;->c:J

    .line 59
    iput-boolean v2, p0, Lcvk;->e:Z

    .line 60
    iput-boolean v2, p0, Lcvk;->f:Z

    .line 61
    return-void
.end method

.method private a(JJ)Lezv;
    .locals 7

    .prologue
    .line 332
    new-instance v1, Lezv;

    iget-object v0, p0, Lcvk;->a:Lcvy;

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, p3, v2

    if-nez v2, :cond_0

    invoke-virtual {v0, p1, p2}, Lcvy;->a(J)Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Lezv;-><init>(Ljava/util/Iterator;)V

    return-object v1

    :cond_0
    invoke-static {p1, p2}, Lcvv;->b(J)Lcvw;

    move-result-object v2

    const-wide/16 v4, 0x1

    add-long/2addr v4, p3

    invoke-static {v4, v5}, Lcvv;->b(J)Lcvw;

    move-result-object v3

    iget-object v0, v0, Lcvy;->a:Ljava/util/TreeSet;

    invoke-virtual {v0, v2, v3}, Ljava/util/TreeSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method private c(J)Lezv;
    .locals 3

    .prologue
    .line 328
    new-instance v0, Lezv;

    iget-object v1, p0, Lcvk;->a:Lcvy;

    invoke-virtual {v1, p1, p2}, Lcvy;->a(J)Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lezv;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 235
    iget-boolean v0, p0, Lcvk;->g:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 236
    iget-object v0, p0, Lcvk;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvk;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 247
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 235
    goto :goto_0

    .line 239
    :cond_2
    iget-object v0, p0, Lcvk;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 240
    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    sget-object v5, Lcvk;->i:Ljava/lang/Boolean;

    if-ne v4, v5, :cond_3

    .line 241
    new-array v4, v1, [Lcvh;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcvh;

    aput-object v0, v4, v2

    invoke-virtual {p0, v4}, Lcvk;->a([Lcvh;)V

    goto :goto_2

    .line 243
    :cond_3
    new-array v4, v1, [Lcvh;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcvh;

    aput-object v0, v4, v2

    invoke-virtual {p0, v4}, Lcvk;->b([Lcvh;)V

    goto :goto_2

    .line 246
    :cond_4
    iget-object v0, p0, Lcvk;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_1
.end method

.method private d(J)V
    .locals 3

    .prologue
    .line 336
    iget-object v0, p0, Lcvk;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvm;

    .line 337
    iget-object v2, p0, Lcvk;->a:Lcvy;

    invoke-interface {v0, p1, p2, v2}, Lcvm;->a(JLjava/lang/Iterable;)V

    goto :goto_0

    .line 339
    :cond_0
    return-void
.end method

.method private declared-synchronized e()V
    .locals 4

    .prologue
    .line 263
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcvk;->e:Z

    .line 264
    iget-wide v0, p0, Lcvk;->c:J

    iget-wide v2, p0, Lcvk;->d:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 265
    iget-wide v0, p0, Lcvk;->d:J

    invoke-virtual {p0, v0, v1}, Lcvk;->b(J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    :goto_0
    monitor-exit p0

    return-void

    .line 267
    :cond_0
    :try_start_1
    iget-wide v0, p0, Lcvk;->d:J

    invoke-direct {p0, v0, v1}, Lcvk;->d(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(J)J
    .locals 7

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 73
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcvk;->e:Z

    if-eqz v0, :cond_0

    .line 74
    invoke-direct {p0}, Lcvk;->e()V

    .line 76
    :cond_0
    iget-wide v0, p0, Lcvk;->c:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    cmp-long v0, p1, v2

    if-ltz v0, :cond_2

    .line 78
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcvk;->c:J

    .line 79
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x41

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "CueRangeManger state error: currentPosition="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " lastPositionTracked="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide v0, v2

    .line 104
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 82
    :cond_2
    :try_start_1
    iget-boolean v0, p0, Lcvk;->e:Z

    if-eqz v0, :cond_3

    .line 83
    const-string v0, "CueRangeManger state error: isTrackingPaused = true"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 85
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvk;->g:Z

    .line 86
    iget-boolean v0, p0, Lcvk;->f:Z

    if-eqz v0, :cond_4

    .line 87
    iget-wide v0, p0, Lcvk;->c:J

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    invoke-direct {p0, v0, v1}, Lcvk;->c(J)Lezv;

    move-result-object v0

    iput-object v0, p0, Lcvk;->b:Lezv;

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcvk;->f:Z

    .line 89
    invoke-direct {p0, p1, p2}, Lcvk;->d(J)V

    .line 91
    :cond_4
    :goto_1
    iget-object v0, p0, Lcvk;->b:Lezv;

    invoke-virtual {v0}, Lezv;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcvk;->b:Lezv;

    invoke-virtual {v0}, Lezv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    iget-wide v0, v0, Lcvw;->b:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_6

    .line 92
    iget-object v0, p0, Lcvk;->b:Lezv;

    invoke-virtual {v0}, Lezv;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    .line 93
    iget-object v1, v0, Lcvw;->c:Lcvv;

    check-cast v1, Lcvh;

    .line 94
    iget-object v0, v0, Lcvw;->a:Lcvx;

    sget-object v4, Lcvx;->a:Lcvx;

    if-ne v0, v4, :cond_5

    .line 96
    iget-boolean v0, p0, Lcvk;->e:Z

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4}, Lcvh;->b(ZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 98
    :cond_5
    :try_start_2
    iget-boolean v0, p0, Lcvk;->e:Z

    invoke-virtual {v1, v0}, Lcvh;->a(Z)V

    goto :goto_1

    .line 101
    :cond_6
    iput-wide p1, p0, Lcvk;->c:J

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcvk;->g:Z

    .line 103
    invoke-direct {p0}, Lcvk;->d()V

    .line 104
    iget-object v0, p0, Lcvk;->b:Lezv;

    invoke-virtual {v0}, Lezv;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcvk;->b:Lezv;

    invoke-virtual {v0}, Lezv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    iget-wide v0, v0, Lcvw;->b:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sub-long/2addr v0, p1

    goto :goto_0

    :cond_7
    move-wide v0, v2

    goto :goto_0
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 254
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcvk;->e:Z

    .line 255
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    invoke-virtual {p0, v0, v1}, Lcvk;->b(J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    monitor-exit p0

    return-void

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcvm;)V
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcvk;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    iget-object v0, p0, Lcvk;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    :cond_0
    return-void
.end method

.method public final varargs declared-synchronized a([Lcvh;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 188
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcvk;->g:Z

    if-eqz v1, :cond_1

    .line 189
    iget-object v1, p0, Lcvk;->h:Ljava/util/List;

    if-nez v1, :cond_0

    .line 190
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcvk;->h:Ljava/util/List;

    .line 192
    :cond_0
    :goto_0
    if-gtz v0, :cond_3

    const/4 v1, 0x0

    aget-object v1, p1, v1

    .line 193
    iget-object v2, p0, Lcvk;->h:Ljava/util/List;

    new-instance v3, Landroid/util/Pair;

    sget-object v4, Lcvk;->i:Ljava/lang/Boolean;

    invoke-direct {v3, v4, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    :cond_1
    :goto_1
    if-gtz v0, :cond_2

    const/4 v1, 0x0

    aget-object v1, p1, v1

    .line 199
    iget-object v2, p0, Lcvk;->a:Lcvy;

    const/4 v3, 0x1

    new-array v3, v3, [Lcvh;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcvy;->a([Lcvv;)V

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 202
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvk;->f:Z

    .line 203
    iget-wide v0, p0, Lcvk;->c:J

    invoke-direct {p0, v0, v1}, Lcvk;->d(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :cond_3
    monitor-exit p0

    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(J)J
    .locals 13

    .prologue
    .line 124
    monitor-enter p0

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    .line 125
    :cond_0
    :try_start_0
    const-string v1, "CueRangeManger state error: newPosition="

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 129
    :cond_1
    invoke-direct {p0, p1, p2}, Lcvk;->d(J)V

    .line 134
    iget-boolean v0, p0, Lcvk;->e:Z

    if-eqz v0, :cond_4

    .line 135
    iget-wide v0, p0, Lcvk;->d:J

    move-wide v4, v0

    .line 140
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvk;->g:Z

    .line 141
    cmp-long v0, p1, v4

    if-lez v0, :cond_5

    .line 142
    invoke-direct {p0, v4, v5, p1, p2}, Lcvk;->a(JJ)Lezv;

    move-result-object v0

    .line 143
    :goto_2
    iput-object v0, p0, Lcvk;->b:Lezv;

    .line 144
    :cond_2
    :goto_3
    iget-object v0, p0, Lcvk;->b:Lezv;

    invoke-virtual {v0}, Lezv;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 145
    iget-object v0, p0, Lcvk;->b:Lezv;

    invoke-virtual {v0}, Lezv;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    .line 146
    iget-object v1, v0, Lcvw;->c:Lcvv;

    check-cast v1, Lcvh;

    .line 147
    invoke-virtual {v1, v4, v5}, Lcvh;->a(J)Z

    move-result v3

    .line 148
    invoke-virtual {v1, p1, p2}, Lcvh;->a(J)Z

    move-result v6

    .line 149
    iget-object v2, v1, Lcvv;->e:Lcvw;

    iget-wide v8, v2, Lcvw;->b:J

    iget-object v2, v1, Lcvv;->f:Lcvw;

    iget-wide v10, v2, Lcvw;->b:J

    cmp-long v2, v8, v10

    if-nez v2, :cond_6

    const/4 v2, 0x1

    .line 150
    :goto_4
    if-nez v3, :cond_8

    if-eqz v6, :cond_8

    .line 153
    if-eqz v2, :cond_7

    iget-object v0, v0, Lcvw;->a:Lcvx;

    sget-object v2, Lcvx;->b:Lcvx;

    if-ne v0, v2, :cond_7

    .line 154
    iget-boolean v0, p0, Lcvk;->e:Z

    invoke-virtual {v1, v0}, Lcvh;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 125
    :cond_3
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_4
    iget-wide v0, p0, Lcvk;->c:J

    move-wide v4, v0

    goto :goto_1

    .line 143
    :cond_5
    invoke-direct {p0, p1, p2, v4, v5}, Lcvk;->a(JJ)Lezv;

    move-result-object v0

    goto :goto_2

    .line 149
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 157
    :cond_7
    iget-boolean v0, p0, Lcvk;->e:Z

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcvh;->b(ZZ)V

    goto :goto_3

    .line 162
    :cond_8
    if-eqz v3, :cond_2

    if-nez v6, :cond_2

    if-nez v2, :cond_2

    .line 163
    iget-boolean v0, p0, Lcvk;->e:Z

    invoke-virtual {v1, v0}, Lcvh;->a(Z)V

    goto :goto_3

    .line 167
    :cond_9
    iget-boolean v0, p0, Lcvk;->e:Z

    if-eqz v0, :cond_a

    .line 168
    iput-wide p1, p0, Lcvk;->d:J

    .line 172
    :goto_5
    const-wide/16 v0, 0x1

    add-long/2addr v0, p1

    invoke-direct {p0, v0, v1}, Lcvk;->c(J)Lezv;

    move-result-object v0

    iput-object v0, p0, Lcvk;->b:Lezv;

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcvk;->f:Z

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcvk;->g:Z

    .line 175
    invoke-direct {p0}, Lcvk;->d()V

    .line 176
    iget-object v0, p0, Lcvk;->b:Lezv;

    invoke-virtual {v0}, Lezv;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcvk;->b:Lezv;

    invoke-virtual {v0}, Lezv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    iget-wide v0, v0, Lcvw;->b:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long/2addr v0, p1

    :goto_6
    monitor-exit p0

    return-wide v0

    .line 170
    :cond_a
    :try_start_2
    iput-wide p1, p0, Lcvk;->c:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 176
    :cond_b
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_6
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 276
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcvk;->e:Z

    .line 277
    iget-wide v0, p0, Lcvk;->c:J

    iput-wide v0, p0, Lcvk;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    monitor-exit p0

    return-void

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final varargs declared-synchronized b([Lcvh;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 213
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcvk;->g:Z

    if-eqz v1, :cond_1

    .line 214
    iget-object v1, p0, Lcvk;->h:Ljava/util/List;

    if-nez v1, :cond_0

    .line 215
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcvk;->h:Ljava/util/List;

    .line 217
    :cond_0
    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_4

    aget-object v2, p1, v0

    .line 218
    iget-object v3, p0, Lcvk;->h:Ljava/util/List;

    new-instance v4, Landroid/util/Pair;

    sget-object v5, Lcvk;->j:Ljava/lang/Boolean;

    invoke-direct {v4, v5, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_1
    array-length v1, p1

    :goto_1
    if-ge v0, v1, :cond_3

    aget-object v2, p1, v0

    .line 224
    iget-boolean v3, v2, Lcvh;->c:Z

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcvk;->c:J

    invoke-virtual {v2, v4, v5}, Lcvh;->a(J)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 225
    iget-boolean v3, p0, Lcvk;->e:Z

    invoke-virtual {v2, v3}, Lcvh;->a(Z)V

    .line 227
    :cond_2
    iget-object v3, p0, Lcvk;->a:Lcvy;

    const/4 v4, 0x1

    new-array v4, v4, [Lcvh;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcvy;->b([Lcvv;)V

    .line 228
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 230
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvk;->f:Z

    .line 231
    iget-wide v0, p0, Lcvk;->c:J

    invoke-direct {p0, v0, v1}, Lcvk;->d(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :cond_4
    monitor-exit p0

    return-void

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 294
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcvk;->c:J

    .line 295
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvk;->f:Z

    .line 296
    invoke-virtual {p0}, Lcvk;->b()V

    .line 297
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcvk;->a:Lcvy;

    invoke-virtual {v0}, Lcvy;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvh;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Lcvh;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcvh;

    invoke-virtual {p0, v0}, Lcvk;->b([Lcvh;)V

    .line 298
    return-void
.end method
