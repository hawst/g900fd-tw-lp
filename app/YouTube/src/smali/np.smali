.class public final Lnp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Log;


# instance fields
.field a:Landroid/view/LayoutInflater;

.field b:Lnr;

.field c:I

.field public d:Loh;

.field private e:Landroid/content/Context;

.field private f:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

.field private g:I

.field private h:Lnq;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Lnp;->c:I

    .line 79
    iput p2, p0, Lnp;->g:I

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lnp;-><init>(II)V

    .line 68
    iput-object p1, p0, Lnp;->e:Landroid/content/Context;

    .line 69
    iget-object v0, p0, Lnp;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lnp;->a:Landroid/view/LayoutInflater;

    .line 70
    return-void
.end method

.method static synthetic a(Lnp;)I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Loi;
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lnp;->f:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-nez v0, :cond_1

    .line 102
    iget-object v0, p0, Lnp;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f04000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iput-object v0, p0, Lnp;->f:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    .line 104
    iget-object v0, p0, Lnp;->h:Lnq;

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Lnq;

    invoke-direct {v0, p0}, Lnq;-><init>(Lnp;)V

    iput-object v0, p0, Lnp;->h:Lnq;

    .line 107
    :cond_0
    iget-object v0, p0, Lnp;->f:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iget-object v1, p0, Lnp;->h:Lnq;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    iget-object v0, p0, Lnp;->f:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 110
    :cond_1
    iget-object v0, p0, Lnp;->f:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lnr;)V
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lnp;->g:I

    if-eqz v0, :cond_2

    .line 85
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, p0, Lnp;->g:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lnp;->e:Landroid/content/Context;

    .line 86
    iget-object v0, p0, Lnp;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lnp;->a:Landroid/view/LayoutInflater;

    .line 93
    :cond_0
    :goto_0
    iput-object p2, p0, Lnp;->b:Lnr;

    .line 94
    iget-object v0, p0, Lnp;->h:Lnq;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lnp;->h:Lnq;

    invoke-virtual {v0}, Lnq;->notifyDataSetChanged()V

    .line 97
    :cond_1
    return-void

    .line 87
    :cond_2
    iget-object v0, p0, Lnp;->e:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 88
    iput-object p1, p0, Lnp;->e:Landroid/content/Context;

    .line 89
    iget-object v0, p0, Lnp;->a:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lnp;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lnp;->a:Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method public final a(Lnr;Z)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lnp;->d:Loh;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lnp;->d:Loh;

    invoke-interface {v0, p1, p2}, Loh;->a(Lnr;Z)V

    .line 154
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lol;)Z
    .locals 6

    .prologue
    .line 139
    invoke-virtual {p1}, Lol;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    .line 142
    :cond_0
    new-instance v0, Lnu;

    invoke-direct {v0, p1}, Lnu;-><init>(Lnr;)V

    iget-object v1, v0, Lnu;->a:Lnr;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, v1, Lnr;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v3, Lnp;

    const v4, 0x7f04000d

    const v5, 0x7f0d00d0

    invoke-direct {v3, v4, v5}, Lnp;-><init>(II)V

    iput-object v3, v0, Lnu;->c:Lnp;

    iget-object v3, v0, Lnu;->c:Lnp;

    iput-object v0, v3, Lnp;->d:Loh;

    iget-object v3, v0, Lnu;->a:Lnr;

    iget-object v4, v0, Lnu;->c:Lnp;

    invoke-virtual {v3, v4}, Lnr;->a(Log;)V

    iget-object v3, v0, Lnu;->c:Lnp;

    invoke-virtual {v3}, Lnp;->b()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v3, v1, Lnr;->h:Landroid/view/View;

    if-eqz v3, :cond_2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    :goto_1
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, v0, Lnu;->b:Landroid/app/AlertDialog;

    iget-object v1, v0, Lnu;->b:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v1, v0, Lnu;->b:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/16 v2, 0x3eb

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, 0x20000

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v0, v0, Lnu;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 143
    iget-object v0, p0, Lnp;->d:Loh;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lnp;->d:Loh;

    invoke-interface {v0, p1}, Loh;->a(Lnr;)Z

    .line 146
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 142
    :cond_2
    iget-object v3, v1, Lnr;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v1, v1, Lnr;->f:Ljava/lang/CharSequence;

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method public final b()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lnp;->h:Lnq;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lnq;

    invoke-direct {v0, p0}, Lnq;-><init>(Lnp;)V

    iput-object v0, p0, Lnp;->h:Lnq;

    .line 124
    :cond_0
    iget-object v0, p0, Lnp;->h:Lnq;

    return-object v0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lnp;->h:Lnq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnp;->h:Lnq;

    invoke-virtual {v0}, Lnq;->notifyDataSetChanged()V

    .line 130
    :cond_0
    return-void
.end method

.method public final b(Lnv;)Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lnv;)Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lnp;->b:Lnr;

    iget-object v1, p0, Lnp;->h:Lnq;

    invoke-virtual {v1, p3}, Lnq;->a(I)Lnv;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lnr;->a(Landroid/view/MenuItem;Log;I)Z

    .line 170
    return-void
.end method
