.class public final Lecd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Ljava/lang/reflect/Method;

.field private B:J

.field private C:F

.field final a:Landroid/os/ConditionVariable;

.field public final b:Lecf;

.field public final c:F

.field public d:Landroid/media/AudioTrack;

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:J

.field public m:Z

.field public n:J

.field public o:I

.field public p:J

.field public q:J

.field public r:[B

.field public s:I

.field public t:I

.field private final u:[J

.field private v:I

.field private w:J

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 130
    const/high16 v0, 0x40800000    # 4.0f

    invoke-direct {p0, v0}, Lecd;-><init>(F)V

    .line 131
    return-void
.end method

.method private constructor <init>(F)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    invoke-static {v1}, La;->b(Z)V

    .line 136
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lecd;->c:F

    .line 137
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lecd;->a:Landroid/os/ConditionVariable;

    .line 138
    sget v0, Legz;->a:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 139
    new-instance v0, Lecg;

    invoke-direct {v0}, Lecg;-><init>()V

    iput-object v0, p0, Lecd;->b:Lecf;

    .line 143
    :goto_0
    sget v0, Legz;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 145
    :try_start_0
    const-class v0, Landroid/media/AudioTrack;

    const-string v1, "getLatency"

    const/4 v2, 0x0

    .line 146
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lecd;->A:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :cond_0
    :goto_1
    const/16 v0, 0xa

    new-array v0, v0, [J

    iput-object v0, p0, Lecd;->u:[J

    .line 152
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lecd;->C:F

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lecd;->o:I

    .line 154
    return-void

    .line 141
    :cond_1
    new-instance v0, Leci;

    invoke-direct {v0}, Leci;-><init>()V

    iput-object v0, p0, Lecd;->b:Lecf;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(I)I
    .locals 9

    .prologue
    const/4 v1, 0x3

    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 233
    iget-object v0, p0, Lecd;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 235
    if-nez p1, :cond_0

    .line 236
    new-instance v0, Landroid/media/AudioTrack;

    iget v2, p0, Lecd;->e:I

    iget v3, p0, Lecd;->f:I

    iget v4, p0, Lecd;->g:I

    iget v5, p0, Lecd;->j:I

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    .line 243
    :goto_0
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    if-eq v0, v6, :cond_1

    :try_start_0
    iget-object v1, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v8, p0, Lecd;->d:Landroid/media/AudioTrack;

    :goto_1
    new-instance v1, Lech;

    iget v2, p0, Lecd;->e:I

    iget v3, p0, Lecd;->f:I

    iget v4, p0, Lecd;->j:I

    invoke-direct {v1, v0, v2, v3, v4}, Lech;-><init>(IIII)V

    throw v1

    .line 240
    :cond_0
    new-instance v0, Landroid/media/AudioTrack;

    iget v2, p0, Lecd;->e:I

    iget v3, p0, Lecd;->f:I

    iget v4, p0, Lecd;->g:I

    iget v5, p0, Lecd;->j:I

    move v7, p1

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    iput-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    goto :goto_0

    .line 243
    :catch_0
    move-exception v1

    iput-object v8, p0, Lecd;->d:Landroid/media/AudioTrack;

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v8, p0, Lecd;->d:Landroid/media/AudioTrack;

    throw v0

    .line 244
    :cond_1
    iget v0, p0, Lecd;->C:F

    invoke-virtual {p0, v0}, Lecd;->a(F)V

    .line 245
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public a(J)J
    .locals 5

    .prologue
    .line 613
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iget v2, p0, Lecd;->e:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 430
    iput p1, p0, Lecd;->C:F

    .line 431
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    .line 432
    sget v0, Legz;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 433
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v0, p1}, Landroid/media/AudioTrack;->setVolume(F)I

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 435
    :cond_1
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v0, p1, p1}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 310
    invoke-virtual {p0}, Lecd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lecd;->B:J

    .line 312
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 314
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 420
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lecd;->n:J

    iget v2, p0, Lecd;->h:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    invoke-virtual {p0}, Lecd;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 464
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    if-eqz v0, :cond_1

    .line 465
    iput-wide v2, p0, Lecd;->n:J

    .line 466
    const/4 v0, 0x0

    iput v0, p0, Lecd;->t:I

    .line 467
    iput-wide v2, p0, Lecd;->y:J

    .line 468
    iput-wide v2, p0, Lecd;->z:J

    .line 469
    iput-wide v2, p0, Lecd;->p:J

    .line 470
    invoke-virtual {p0}, Lecd;->h()V

    .line 471
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    .line 472
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 473
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    .line 476
    :cond_0
    iget-object v0, p0, Lecd;->d:Landroid/media/AudioTrack;

    .line 477
    const/4 v1, 0x0

    iput-object v1, p0, Lecd;->d:Landroid/media/AudioTrack;

    .line 478
    iget-object v1, p0, Lecd;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 479
    new-instance v1, Lece;

    invoke-direct {v1, p0, v0}, Lece;-><init>(Lecd;Landroid/media/AudioTrack;)V

    .line 488
    invoke-virtual {v1}, Lece;->start()V

    .line 490
    :cond_1
    return-void
.end method

.method public e()V
    .locals 10

    .prologue
    .line 499
    invoke-virtual {p0}, Lecd;->g()J

    move-result-wide v0

    .line 500
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 554
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 505
    iget-wide v4, p0, Lecd;->w:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x7530

    cmp-long v4, v4, v6

    if-ltz v4, :cond_3

    .line 507
    iget-object v4, p0, Lecd;->u:[J

    iget v5, p0, Lecd;->v:I

    sub-long/2addr v0, v2

    aput-wide v0, v4, v5

    .line 508
    iget v0, p0, Lecd;->v:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, Lecd;->v:I

    .line 509
    iget v0, p0, Lecd;->k:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 510
    iget v0, p0, Lecd;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecd;->k:I

    .line 512
    :cond_2
    iput-wide v2, p0, Lecd;->w:J

    .line 513
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lecd;->l:J

    .line 514
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lecd;->k:I

    if-ge v0, v1, :cond_3

    .line 515
    iget-wide v4, p0, Lecd;->l:J

    iget-object v1, p0, Lecd;->u:[J

    aget-wide v6, v1, v0

    iget v1, p0, Lecd;->k:I

    int-to-long v8, v1

    div-long/2addr v6, v8

    add-long/2addr v4, v6

    iput-wide v4, p0, Lecd;->l:J

    .line 514
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 519
    :cond_3
    iget-wide v0, p0, Lecd;->x:J

    sub-long v0, v2, v0

    const-wide/32 v4, 0x7a120

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    .line 520
    iget-object v0, p0, Lecd;->b:Lecf;

    iget-object v1, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-interface {v0, v1}, Lecf;->a(Landroid/media/AudioTrack;)Z

    move-result v0

    iput-boolean v0, p0, Lecd;->m:Z

    .line 521
    iget-boolean v0, p0, Lecd;->m:Z

    if-eqz v0, :cond_4

    .line 523
    iget-object v0, p0, Lecd;->b:Lecf;

    invoke-interface {v0}, Lecf;->a()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    .line 524
    iget-wide v4, p0, Lecd;->B:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_6

    .line 526
    const/4 v0, 0x0

    iput-boolean v0, p0, Lecd;->m:Z

    .line 534
    :cond_4
    :goto_2
    iget-object v0, p0, Lecd;->A:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_5

    .line 538
    :try_start_0
    iget-object v0, p0, Lecd;->A:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lecd;->d:Landroid/media/AudioTrack;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    iget v4, p0, Lecd;->j:I

    int-to-long v4, v4

    .line 539
    iget v6, p0, Lecd;->h:I

    int-to-long v6, v6

    div-long/2addr v4, v6

    invoke-virtual {p0, v4, v5}, Lecd;->a(J)J

    move-result-wide v4

    sub-long/2addr v0, v4

    iput-wide v0, p0, Lecd;->q:J

    .line 541
    iget-wide v0, p0, Lecd;->q:J

    const-wide/16 v4, 0x0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lecd;->q:J

    .line 543
    iget-wide v0, p0, Lecd;->q:J

    const-wide/32 v4, 0x989680

    cmp-long v0, v0, v4

    if-lez v0, :cond_5

    .line 544
    const-string v0, "AudioTrack"

    iget-wide v4, p0, Lecd;->q:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v6, 0x3d

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Ignoring impossibly large audio latency: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lecd;->q:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 552
    :cond_5
    :goto_3
    iput-wide v2, p0, Lecd;->x:J

    goto/16 :goto_0

    .line 527
    :cond_6
    sub-long v4, v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v6, 0x989680

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    .line 529
    const/4 v4, 0x0

    iput-boolean v4, p0, Lecd;->m:Z

    .line 530
    const-string v4, "AudioTrack"

    iget-object v5, p0, Lecd;->b:Lecf;

    invoke-interface {v5}, Lecf;->b()J

    move-result-wide v6

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v8, 0x5a

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Spurious audio timestamp: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 549
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lecd;->A:Ljava/lang/reflect/Method;

    goto :goto_3
.end method

.method public f()J
    .locals 6

    .prologue
    .line 591
    const-wide v0, 0xffffffffL

    iget-object v2, p0, Lecd;->d:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v2

    int-to-long v2, v2

    and-long/2addr v0, v2

    .line 592
    iget-wide v2, p0, Lecd;->y:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    .line 594
    iget-wide v2, p0, Lecd;->z:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lecd;->z:J

    .line 596
    :cond_0
    iput-wide v0, p0, Lecd;->y:J

    .line 597
    iget-wide v2, p0, Lecd;->z:J

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 601
    invoke-virtual {p0}, Lecd;->f()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lecd;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public h()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 621
    iput-wide v2, p0, Lecd;->l:J

    .line 622
    iput v0, p0, Lecd;->k:I

    .line 623
    iput v0, p0, Lecd;->v:I

    .line 624
    iput-wide v2, p0, Lecd;->w:J

    .line 625
    iput-boolean v0, p0, Lecd;->m:Z

    .line 626
    iput-wide v2, p0, Lecd;->x:J

    .line 627
    return-void
.end method
