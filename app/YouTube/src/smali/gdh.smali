.class final Lgdh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final a:Leey;

.field private final b:Z

.field private final c:Z

.field private d:Z


# direct methods
.method constructor <init>(Leey;ZZ)V
    .locals 0

    .prologue
    .line 444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445
    iput-object p1, p0, Lgdh;->a:Leey;

    .line 446
    iput-boolean p2, p0, Lgdh;->b:Z

    .line 447
    iput-boolean p3, p0, Lgdh;->c:Z

    .line 448
    return-void
.end method

.method static synthetic a(Lgdh;Z)V
    .locals 0

    .prologue
    .line 429
    iput-boolean p1, p0, Lgdh;->d:Z

    return-void
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 7

    .prologue
    .line 429
    check-cast p1, Lfqj;

    check-cast p2, Lfqj;

    invoke-virtual {p1}, Lfqj;->b()I

    move-result v0

    invoke-virtual {p2}, Lfqj;->b()I

    move-result v1

    iget-boolean v2, p0, Lgdh;->d:Z

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lgdh;->b:Z

    if-eqz v2, :cond_7

    iget-object v2, p1, Lfqj;->d:Landroid/net/Uri;

    invoke-static {v2}, La;->c(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    mul-int/lit16 v0, v0, 0x3e8

    :cond_0
    iget-object v2, p2, Lfqj;->d:Landroid/net/Uri;

    invoke-static {v2}, La;->c(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_7

    mul-int/lit16 v1, v1, 0x3e8

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p1, Lfqj;->a:Lhgy;

    iget-boolean v2, v2, Lhgy;->l:Z

    if-eqz v2, :cond_1

    mul-int/lit8 v1, v1, 0x64

    :cond_1
    iget-object v2, p2, Lfqj;->a:Lhgy;

    iget-boolean v2, v2, Lhgy;->l:Z

    if-eqz v2, :cond_2

    mul-int/lit8 v0, v0, 0x64

    :cond_2
    iget-boolean v2, p0, Lgdh;->c:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lgdh;->a:Leey;

    invoke-interface {v2}, Leey;->a()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_4

    iget-object v4, p1, Lfqj;->a:Lhgy;

    iget v4, v4, Lhgy;->e:I

    int-to-long v4, v4

    cmp-long v4, v4, v2

    if-gtz v4, :cond_3

    mul-int/lit8 v1, v1, 0xa

    :cond_3
    iget-object v4, p2, Lfqj;->a:Lhgy;

    iget v4, v4, Lhgy;->e:I

    int-to-long v4, v4

    cmp-long v2, v4, v2

    if-gtz v2, :cond_4

    mul-int/lit8 v0, v0, 0xa

    :cond_4
    :goto_1
    if-le v1, v0, :cond_5

    const/4 v0, -0x1

    :goto_2
    return v0

    :cond_5
    if-ne v1, v0, :cond_6

    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x1

    goto :goto_2

    :cond_7
    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_0

    :cond_8
    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_1
.end method
