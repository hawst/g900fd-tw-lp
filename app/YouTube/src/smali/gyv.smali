.class public final Lgyv;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lgyw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5122
    invoke-direct {p0}, Lidf;-><init>()V

    .line 5125
    const/4 v0, 0x0

    iput-object v0, p0, Lgyv;->a:Lgyw;

    .line 5122
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 5162
    const/4 v0, 0x0

    .line 5163
    iget-object v1, p0, Lgyv;->a:Lgyw;

    if-eqz v1, :cond_0

    .line 5164
    const/4 v0, 0x1

    iget-object v1, p0, Lgyv;->a:Lgyw;

    .line 5165
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5167
    :cond_0
    iget-object v1, p0, Lgyv;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5168
    iput v0, p0, Lgyv;->J:I

    .line 5169
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 5118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyv;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyv;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyv;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgyv;->a:Lgyw;

    if-nez v0, :cond_2

    new-instance v0, Lgyw;

    invoke-direct {v0}, Lgyw;-><init>()V

    iput-object v0, p0, Lgyv;->a:Lgyw;

    :cond_2
    iget-object v0, p0, Lgyv;->a:Lgyw;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 5153
    iget-object v0, p0, Lgyv;->a:Lgyw;

    if-eqz v0, :cond_0

    .line 5154
    const/4 v0, 0x1

    iget-object v1, p0, Lgyv;->a:Lgyw;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 5156
    :cond_0
    iget-object v0, p0, Lgyv;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 5158
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5136
    if-ne p1, p0, :cond_1

    .line 5140
    :cond_0
    :goto_0
    return v0

    .line 5137
    :cond_1
    instance-of v2, p1, Lgyv;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 5138
    :cond_2
    check-cast p1, Lgyv;

    .line 5139
    iget-object v2, p0, Lgyv;->a:Lgyw;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyv;->a:Lgyw;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgyv;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyv;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 5140
    goto :goto_0

    .line 5139
    :cond_4
    iget-object v2, p0, Lgyv;->a:Lgyw;

    iget-object v3, p1, Lgyv;->a:Lgyw;

    invoke-virtual {v2, v3}, Lgyw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyv;->I:Ljava/util/List;

    iget-object v3, p1, Lgyv;->I:Ljava/util/List;

    .line 5140
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5144
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5146
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyv;->a:Lgyw;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 5147
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgyv;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 5148
    return v0

    .line 5146
    :cond_0
    iget-object v0, p0, Lgyv;->a:Lgyw;

    invoke-virtual {v0}, Lgyw;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5147
    :cond_1
    iget-object v1, p0, Lgyv;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
