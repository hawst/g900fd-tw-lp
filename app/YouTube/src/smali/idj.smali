.class public final Lidj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[I

.field public static final b:[J

.field public static final c:[D

.field public static final d:[Ljava/lang/String;

.field public static final e:[[B

.field public static final f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    new-array v0, v1, [I

    sput-object v0, Lidj;->a:[I

    .line 94
    new-array v0, v1, [J

    sput-object v0, Lidj;->b:[J

    .line 95
    new-array v0, v1, [D

    sput-object v0, Lidj;->c:[D

    .line 97
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lidj;->d:[Ljava/lang/String;

    .line 99
    new-array v0, v1, [[B

    sput-object v0, Lidj;->e:[[B

    .line 100
    new-array v0, v1, [B

    sput-object v0, Lidj;->f:[B

    .line 102
    return-void
.end method

.method static a(I)I
    .locals 1

    .prologue
    .line 65
    and-int/lit8 v0, p0, 0x7

    return v0
.end method

.method static a(II)I
    .locals 1

    .prologue
    .line 75
    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method

.method public static a(Ljava/util/List;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 346
    if-nez p0, :cond_0

    .line 354
    :goto_0
    return v0

    .line 350
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidi;

    .line 351
    iget v3, v0, Lidi;->a:I

    invoke-static {v3}, Lidd;->c(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 352
    iget-object v0, v0, Lidi;->b:[B

    array-length v0, v0

    add-int/2addr v0, v1

    move v1, v0

    .line 353
    goto :goto_1

    :cond_1
    move v0, v1

    .line 354
    goto :goto_0
.end method

.method public static a(Ljava/util/List;Lidd;)V
    .locals 4

    .prologue
    .line 362
    if-nez p0, :cond_1

    .line 369
    :cond_0
    return-void

    .line 365
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidi;

    .line 366
    iget v2, v0, Lidi;->a:I

    ushr-int/lit8 v2, v2, 0x3

    iget v3, v0, Lidi;->a:I

    and-int/lit8 v3, v3, 0x7

    invoke-virtual {p1, v2, v3}, Lidd;->e(II)V

    .line 367
    iget-object v0, v0, Lidi;->b:[B

    invoke-virtual {p1, v0}, Lidd;->b([B)V

    goto :goto_0
.end method

.method public static a(Lidc;I)Z
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lidc;->b(I)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/List;Lidc;I)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 141
    invoke-virtual {p1}, Lidc;->l()I

    move-result v2

    .line 142
    invoke-virtual {p1, p2}, Lidc;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 148
    :goto_0
    return v0

    .line 145
    :cond_0
    invoke-virtual {p1}, Lidc;->l()I

    move-result v0

    .line 146
    sub-int v3, v0, v2

    if-nez v3, :cond_1

    sget-object v0, Lidj;->f:[B

    .line 147
    :goto_1
    new-instance v1, Lidi;

    invoke-direct {v1, p2, v0}, Lidi;-><init>(I[B)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    const/4 v0, 0x1

    goto :goto_0

    .line 146
    :cond_1
    new-array v0, v3, [B

    iget v4, p1, Lidc;->b:I

    add-int/2addr v2, v4

    iget-object v4, p1, Lidc;->a:[B

    invoke-static {v4, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 70
    ushr-int/lit8 v0, p0, 0x3

    return v0
.end method

.method public static final b(Lidc;I)I
    .locals 5

    .prologue
    .line 166
    const/4 v0, 0x1

    .line 167
    invoke-virtual {p0}, Lidc;->l()I

    move-result v2

    .line 168
    invoke-virtual {p0, p1}, Lidc;->b(I)Z

    .line 169
    :goto_0
    iget v1, p0, Lidc;->d:I

    const v3, 0x7fffffff

    if-ne v1, v3, :cond_0

    const/4 v1, -0x1

    :goto_1
    if-lez v1, :cond_1

    .line 170
    invoke-virtual {p0}, Lidc;->a()I

    move-result v1

    .line 171
    if-ne v1, p1, :cond_1

    .line 172
    invoke-virtual {p0, p1}, Lidc;->b(I)Z

    .line 175
    add-int/lit8 v0, v0, 0x1

    .line 176
    goto :goto_0

    .line 169
    :cond_0
    iget v1, p0, Lidc;->c:I

    iget v3, p0, Lidc;->d:I

    sub-int v1, v3, v1

    goto :goto_1

    .line 177
    :cond_1
    iget v1, p0, Lidc;->c:I

    iget v3, p0, Lidc;->b:I

    sub-int/2addr v1, v3

    if-le v2, v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget v1, p0, Lidc;->c:I

    iget v3, p0, Lidc;->b:I

    sub-int/2addr v1, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x32

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Position "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is beyond current "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-gez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x18

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Bad position "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget v1, p0, Lidc;->b:I

    add-int/2addr v1, v2

    iput v1, p0, Lidc;->c:I

    .line 178
    return v0
.end method
