.class public final Lejs;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/Set;

.field private b:Ljava/lang/String;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/Map;

.field private e:I

.field private f:Landroid/os/Looper;

.field private g:I

.field private final h:Ljava/util/Set;

.field private final i:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lejs;->a:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lejs;->d:Ljava/util/Map;

    const/4 v0, -0x1

    iput v0, p0, Lejs;->e:I

    const/4 v0, 0x2

    iput v0, p0, Lejs;->g:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lejs;->h:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lejs;->i:Ljava/util/Set;

    iput-object p1, p0, Lejs;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lejs;->f:Landroid/os/Looper;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lejs;->b:Ljava/lang/String;

    return-void
.end method

.method private b()Lcom/google/android/gms/common/internal/ClientSettings;
    .locals 6

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/gms/common/internal/ClientSettings;

    iget-object v2, p0, Lejs;->a:Ljava/util/Set;

    const/4 v3, 0x0

    iget-object v5, p0, Lejs;->b:Ljava/lang/String;

    move-object v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/ClientSettings;-><init>(Ljava/lang/String;Ljava/util/Collection;ILandroid/view/View;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lejr;
    .locals 13

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    iget-object v0, p0, Lejs;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v9

    :goto_0
    const-string v1, "must call addApi() to add at least one API"

    invoke-static {v0, v1}, Lb;->b(ZLjava/lang/Object;)V

    iget v0, p0, Lejs;->e:I

    if-ltz v0, :cond_5

    invoke-static {v11}, Lekh;->a(Lo;)Lekh;

    move-result-object v12

    iget v0, p0, Lejs;->e:I

    iget-object v1, v12, Lj;->w:Lo;

    if-eqz v1, :cond_3

    invoke-virtual {v12, v0}, Lekh;->b(I)Leki;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v0, Leki;->f:Lejr;

    :goto_1
    if-nez v0, :cond_0

    new-instance v0, Lejz;

    iget-object v1, p0, Lejs;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lejs;->f:Landroid/os/Looper;

    invoke-direct {p0}, Lejs;->b()Lcom/google/android/gms/common/internal/ClientSettings;

    move-result-object v3

    iget-object v4, p0, Lejs;->d:Ljava/util/Map;

    iget-object v5, p0, Lejs;->h:Ljava/util/Set;

    iget-object v6, p0, Lejs;->i:Ljava/util/Set;

    iget v7, p0, Lejs;->e:I

    iget v8, p0, Lejs;->g:I

    invoke-direct/range {v0 .. v8}, Lejz;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;II)V

    :cond_0
    iget v1, p0, Lejs;->e:I

    const-string v2, "GoogleApiClient instance cannot be null"

    invoke-static {v0, v2}, Lb;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v12, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v2

    if-gez v2, :cond_4

    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Already managing a GoogleApiClient with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Lb;->a(ZLjava/lang/Object;)V

    new-instance v2, Lekj;

    invoke-direct {v2, v0, v11}, Lekj;-><init>(Lejr;Lejc;)V

    iget-object v3, v12, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v2, v12, Lj;->w:Lo;

    if-eqz v2, :cond_1

    invoke-virtual {v12}, Lekh;->p()Lam;

    move-result-object v2

    invoke-virtual {v2, v1, v11, v12}, Lam;->a(ILandroid/os/Bundle;Lan;)Lcj;

    :cond_1
    :goto_3
    return-object v0

    :cond_2
    move v0, v10

    goto :goto_0

    :cond_3
    move-object v0, v11

    goto :goto_1

    :cond_4
    move v9, v10

    goto :goto_2

    :cond_5
    new-instance v0, Lejz;

    iget-object v1, p0, Lejs;->c:Landroid/content/Context;

    iget-object v2, p0, Lejs;->f:Landroid/os/Looper;

    invoke-direct {p0}, Lejs;->b()Lcom/google/android/gms/common/internal/ClientSettings;

    move-result-object v3

    iget-object v4, p0, Lejs;->d:Ljava/util/Map;

    iget-object v5, p0, Lejs;->h:Ljava/util/Set;

    iget-object v6, p0, Lejs;->i:Ljava/util/Set;

    const/4 v7, -0x1

    iget v8, p0, Lejs;->g:I

    invoke-direct/range {v0 .. v8}, Lejz;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;II)V

    goto :goto_3
.end method

.method public final a(Lejc;)Lejs;
    .locals 1

    iget-object v0, p0, Lejs;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lejj;)Lejs;
    .locals 5

    iget-object v0, p0, Lejs;->d:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lejj;->c:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lejs;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcw;

    invoke-virtual {v0}, Lcw;->q()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public final a(Lejj;Lbt;)Lejs;
    .locals 5

    const-string v0, "Null options are not permitted for this Api"

    invoke-static {p2, v0}, Lb;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lejs;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lejj;->c:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lejs;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcw;

    invoke-virtual {v0}, Lcw;->q()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object p0
.end method
