.class public final Lfql;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lhgy;

.field public b:Landroid/net/Uri$Builder;

.field public c:Ljava/lang/String;

.field public final d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 352
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lfql;-><init>(J)V

    .line 353
    return-void
.end method

.method private constructor <init>(J)V
    .locals 2

    .prologue
    .line 344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    new-instance v0, Lhgy;

    invoke-direct {v0}, Lhgy;-><init>()V

    iput-object v0, p0, Lfql;->a:Lhgy;

    .line 346
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    iput-object v0, p0, Lfql;->b:Landroid/net/Uri$Builder;

    .line 347
    const/4 v0, 0x0

    iput-object v0, p0, Lfql;->c:Ljava/lang/String;

    .line 348
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfql;->d:J

    .line 349
    return-void
.end method

.method public constructor <init>(Lfqj;)V
    .locals 2

    .prologue
    .line 355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356
    invoke-virtual {p1}, Lfqj;->a()Lhgy;

    move-result-object v0

    iput-object v0, p0, Lfql;->a:Lhgy;

    .line 357
    invoke-static {p1}, Lfqj;->a(Lfqj;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iput-object v0, p0, Lfql;->b:Landroid/net/Uri$Builder;

    .line 358
    invoke-static {p1}, Lfqj;->b(Lfqj;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfql;->c:Ljava/lang/String;

    .line 359
    invoke-static {p1}, Lfqj;->c(Lfqj;)J

    move-result-wide v0

    iput-wide v0, p0, Lfql;->d:J

    .line 360
    return-void
.end method
