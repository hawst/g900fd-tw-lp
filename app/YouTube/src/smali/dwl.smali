.class public final Ldwl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ldaq;

.field public final b:Z

.field public c:Ldwn;

.field public d:Leue;


# direct methods
.method public constructor <init>(Ldaq;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Ldwl;->a:Ldaq;

    .line 40
    iput-boolean p2, p0, Ldwl;->b:Z

    .line 41
    return-void
.end method

.method static synthetic a(Ldwl;Lfrl;Ldwj;Leuc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    if-nez p1, :cond_0

    const-string v0, "no local playback to fling to TV"

    iget-boolean v0, p0, Ldwl;->b:Z

    sget-object v0, Ldwj;->f:Ldwj;

    invoke-interface {p3, v1, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lfrl;->g()Lflo;

    move-result-object v0

    invoke-virtual {v0}, Lflo;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "error: can not play local playback on TV"

    iget-boolean v0, p0, Ldwl;->b:Z

    sget-object v0, Ldwj;->f:Ldwj;

    invoke-interface {p3, v1, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-interface {p3, v1, p2}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldwn;)V
    .locals 1

    .prologue
    .line 44
    const-string v0, "provider cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwn;

    iput-object v0, p0, Ldwl;->c:Ldwn;

    .line 45
    return-void
.end method

.method public final b(Ldwn;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ldwl;->c:Ldwn;

    if-ne v0, p1, :cond_0

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Ldwl;->c:Ldwn;

    .line 51
    :cond_0
    return-void
.end method
