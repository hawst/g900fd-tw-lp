.class public final Lifk;
.super Landroid/os/Handler;
.source "SourceFile"


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public final a:Lifm;

.field public b:I

.field public final c:Lifu;

.field private final e:Lcom/google/zxing/client/android/CaptureActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lifk;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lifk;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/zxing/client/android/CaptureActivity;Ljava/lang/String;Lifu;)V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 54
    iput-object p1, p0, Lifk;->e:Lcom/google/zxing/client/android/CaptureActivity;

    .line 55
    new-instance v0, Lifm;

    new-instance v1, Lifc;

    .line 56
    iget-object v2, p1, Lcom/google/zxing/client/android/CaptureActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    invoke-direct {v1, v2}, Lifc;-><init>(Lcom/google/zxing/client/android/ViewfinderView;)V

    invoke-direct {v0, p1, p2, v1}, Lifm;-><init>(Lcom/google/zxing/client/android/CaptureActivity;Ljava/lang/String;Lifc;)V

    iput-object v0, p0, Lifk;->a:Lifm;

    .line 57
    iget-object v0, p0, Lifk;->a:Lifm;

    invoke-virtual {v0}, Lifm;->start()V

    .line 58
    const/4 v0, 0x2

    iput v0, p0, Lifk;->b:I

    .line 61
    iput-object p3, p0, Lifk;->c:Lifu;

    .line 62
    iget-object v0, p3, Lifu;->c:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v1, p3, Lifu;->g:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    const/4 v0, 0x1

    iput-boolean v0, p3, Lifu;->g:Z

    .line 63
    :cond_0
    invoke-direct {p0}, Lifk;->a()V

    .line 64
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 120
    iget v0, p0, Lifk;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 121
    const/4 v0, 0x1

    iput v0, p0, Lifk;->b:I

    .line 122
    iget-object v0, p0, Lifk;->c:Lifu;

    iget-object v1, p0, Lifk;->a:Lifm;

    invoke-virtual {v1}, Lifm;->a()Landroid/os/Handler;

    move-result-object v1

    const v2, 0x7f080001

    invoke-virtual {v0, v1, v2}, Lifu;->a(Landroid/os/Handler;I)V

    .line 123
    iget-object v0, p0, Lifk;->c:Lifu;

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, p0, v1}, Lifu;->b(Landroid/os/Handler;I)V

    .line 124
    iget-object v0, p0, Lifk;->e:Lcom/google/zxing/client/android/CaptureActivity;

    iget-object v0, v0, Lcom/google/zxing/client/android/CaptureActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/zxing/client/android/ViewfinderView;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/ViewfinderView;->invalidate()V

    .line 126
    :cond_0
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/high16 v1, 0x7f080000

    const/4 v11, -0x1

    const/4 v10, 0x1

    const/4 v2, 0x0

    const/4 v9, 0x2

    .line 68
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_1

    .line 72
    iget v0, p0, Lifk;->b:I

    if-ne v0, v10, :cond_0

    .line 73
    iget-object v0, p0, Lifk;->c:Lifu;

    invoke-virtual {v0, p0, v1}, Lifu;->b(Landroid/os/Handler;I)V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x7f080006

    if-ne v0, v1, :cond_2

    .line 76
    sget-object v0, Lifk;->d:Ljava/lang/String;

    .line 77
    invoke-direct {p0}, Lifk;->a()V

    goto :goto_0

    .line 78
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x7f080003

    if-ne v0, v1, :cond_a

    .line 79
    sget-object v0, Lifk;->d:Ljava/lang/String;

    .line 80
    iput v9, p0, Lifk;->b:I

    .line 81
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    if-nez v0, :cond_3

    const/4 v0, 0x0

    move-object v1, v0

    .line 84
    :goto_1
    iget-object v3, p0, Lifk;->e:Lcom/google/zxing/client/android/CaptureActivity;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Liez;

    iget-object v4, v3, Lcom/google/zxing/client/android/CaptureActivity;->e:Lifo;

    invoke-virtual {v4}, Lifo;->a()V

    if-nez v1, :cond_4

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivity;->a:Ljava/lang/String;

    const-string v1, "Barcode not recognized"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {v3, v2, v0}, Lcom/google/zxing/client/android/CaptureActivity;->setResult(ILandroid/content/Intent;)V

    :goto_2
    invoke-virtual {v3}, Lcom/google/zxing/client/android/CaptureActivity;->finish()V

    goto :goto_0

    .line 82
    :cond_3
    const-string v1, "barcode_bitmap"

    .line 83
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    goto :goto_1

    .line 84
    :cond_4
    iget-object v4, v0, Liez;->c:[Lifb;

    if-eqz v4, :cond_5

    array-length v5, v4

    if-lez v5, :cond_5

    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v3}, Lcom/google/zxing/client/android/CaptureActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070005

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v7, 0x40400000    # 3.0f

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v7, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v7, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-direct {v7, v9, v9, v8, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, v7, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {v3}, Lcom/google/zxing/client/android/CaptureActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v7, 0x7f070007

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    array-length v1, v4

    if-ne v1, v9, :cond_6

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    aget-object v1, v4, v2

    aget-object v2, v4, v10

    invoke-static {v5, v6, v1, v2}, Lcom/google/zxing/client/android/CaptureActivity;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lifb;Lifb;)V

    :cond_5
    :goto_3
    sget-object v1, Lcom/google/zxing/client/android/CaptureActivity;->a:Ljava/lang/String;

    const-string v1, "Barcode is: "

    iget-object v2, v0, Liez;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_4
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "SCAN_RESULT"

    iget-object v0, v0, Liez;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3, v11, v1}, Lcom/google/zxing/client/android/CaptureActivity;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_2

    :cond_6
    array-length v1, v4

    const/4 v7, 0x4

    if-ne v1, v7, :cond_8

    iget-object v1, v0, Liez;->d:Lieo;

    sget-object v7, Lieo;->o:Lieo;

    if-eq v1, v7, :cond_7

    iget-object v1, v0, Liez;->d:Lieo;

    sget-object v7, Lieo;->h:Lieo;

    if-ne v1, v7, :cond_8

    :cond_7
    aget-object v1, v4, v2

    aget-object v2, v4, v10

    invoke-static {v5, v6, v1, v2}, Lcom/google/zxing/client/android/CaptureActivity;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lifb;Lifb;)V

    aget-object v1, v4, v9

    const/4 v2, 0x3

    aget-object v2, v4, v2

    invoke-static {v5, v6, v1, v2}, Lcom/google/zxing/client/android/CaptureActivity;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lifb;Lifb;)V

    goto :goto_3

    :cond_8
    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    array-length v7, v4

    move v1, v2

    :goto_5
    if-ge v1, v7, :cond_5

    aget-object v2, v4, v1

    iget v8, v2, Lifb;->a:F

    iget v2, v2, Lifb;->b:F

    invoke-virtual {v5, v8, v2, v6}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_9
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 85
    :cond_a
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x7f080002

    if-ne v0, v1, :cond_b

    .line 87
    iput v10, p0, Lifk;->b:I

    .line 88
    iget-object v0, p0, Lifk;->c:Lifu;

    iget-object v1, p0, Lifk;->a:Lifm;

    invoke-virtual {v1}, Lifm;->a()Landroid/os/Handler;

    move-result-object v1

    const v2, 0x7f080001

    invoke-virtual {v0, v1, v2}, Lifu;->a(Landroid/os/Handler;I)V

    goto/16 :goto_0

    .line 89
    :cond_b
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x7f080007

    if-ne v0, v1, :cond_c

    .line 90
    sget-object v0, Lifk;->d:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lifk;->e:Lcom/google/zxing/client/android/CaptureActivity;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v1, v11, v0}, Lcom/google/zxing/client/android/CaptureActivity;->setResult(ILandroid/content/Intent;)V

    .line 92
    iget-object v0, p0, Lifk;->e:Lcom/google/zxing/client/android/CaptureActivity;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/CaptureActivity;->finish()V

    goto/16 :goto_0

    .line 93
    :cond_c
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x7f080004

    if-ne v0, v1, :cond_0

    .line 94
    sget-object v0, Lifk;->d:Ljava/lang/String;

    .line 95
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 96
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 97
    const/high16 v0, 0x80000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 98
    iget-object v0, p0, Lifk;->e:Lcom/google/zxing/client/android/CaptureActivity;

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/CaptureActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
