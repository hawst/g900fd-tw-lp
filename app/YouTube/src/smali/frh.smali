.class final Lfrh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lfrf;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 422
    const/4 v2, 0x0

    .line 424
    :try_start_0
    new-instance v0, Lhwn;

    invoke-direct {v0}, Lhwn;-><init>()V

    invoke-static {p0, v0}, La;->b(Landroid/os/Parcel;Lidh;)Lidh;

    move-result-object v0

    check-cast v0, Lhwn;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    .line 428
    :goto_0
    new-instance v1, Lfrf;

    .line 430
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 431
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 432
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 433
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v8, :cond_0

    :goto_1
    invoke-direct/range {v1 .. v8}, Lfrf;-><init>(Lhwn;Ljava/lang/String;JJZ)V

    return-object v1

    .line 425
    :catch_0
    move-exception v0

    .line 426
    const-string v1, "Error reading streaming data"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 433
    :cond_0
    const/4 v8, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 419
    invoke-static {p1}, Lfrh;->a(Landroid/os/Parcel;)Lfrf;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 419
    new-array v0, p1, [Lfrf;

    return-object v0
.end method
