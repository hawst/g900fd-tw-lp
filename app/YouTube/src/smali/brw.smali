.class public Lbrw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private d:Landroid/view/View;

.field private e:Landroid/widget/TextView;

.field private f:Ljava/lang/CharSequence;

.field private synthetic g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;III)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lbrw;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    iput p2, p0, Lbrw;->a:I

    .line 231
    iput p3, p0, Lbrw;->b:I

    .line 232
    iput p4, p0, Lbrw;->c:I

    .line 233
    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;IIIB)V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0, p1, p2, p3, p4}, Lbrw;-><init>(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;III)V

    return-void
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .locals 4

    .prologue
    .line 236
    iget-object v0, p0, Lbrw;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lbrw;->b:I

    iget-object v2, p0, Lbrw;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 237
    iget v0, p0, Lbrw;->c:I

    if-lez v0, :cond_0

    .line 238
    iget v0, p0, Lbrw;->c:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbrw;->e:Landroid/widget/TextView;

    .line 239
    iget-object v0, p0, Lbrw;->e:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 240
    iget-object v0, p0, Lbrw;->f:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lbrw;->a(Ljava/lang/CharSequence;)V

    .line 242
    :cond_0
    return-object v1
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 246
    iget v0, p0, Lbrw;->a:I

    if-ne v0, p1, :cond_2

    .line 247
    iget-object v0, p0, Lbrw;->d:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbrw;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbrw;->d:Landroid/view/View;

    iget-object v0, p0, Lbrw;->g:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iget-object v1, p0, Lbrw;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lbrw;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 251
    :cond_1
    :goto_0
    return-void

    .line 249
    :cond_2
    iget-object v0, p0, Lbrw;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbrw;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lbrw;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lbrw;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    const/4 v0, 0x0

    iput-object v0, p0, Lbrw;->f:Ljava/lang/CharSequence;

    .line 274
    :goto_0
    return-void

    .line 272
    :cond_0
    iput-object p1, p0, Lbrw;->f:Ljava/lang/CharSequence;

    goto :goto_0
.end method
