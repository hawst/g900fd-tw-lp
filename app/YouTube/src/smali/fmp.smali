.class public Lfmp;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfqh;


# instance fields
.field public final a:Lhvg;

.field public b:Ljava/lang/String;

.field public c:Landroid/graphics/drawable/Drawable;

.field private final d:Lfqh;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lhog;)V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lhvg;

    invoke-direct {v0}, Lhvg;-><init>()V

    invoke-direct {p0, v0}, Lfmp;-><init>(Lhvg;)V

    .line 41
    invoke-virtual {p0, p1, p2, p3}, Lfmp;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lhog;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lhvg;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfmp;-><init>(Lhvg;Lfqh;)V

    .line 46
    return-void
.end method

.method private constructor <init>(Lhvg;Lfqh;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lfic;-><init>()V

    .line 51
    iput-object p1, p0, Lfmp;->a:Lhvg;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lfmp;->d:Lfqh;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lhog;)V
    .locals 2

    .prologue
    .line 93
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v0, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lfmp;->b:Ljava/lang/String;

    .line 97
    iget-object v0, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lfmp;->e:Ljava/lang/String;

    .line 98
    invoke-virtual {p2, p1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfmp;->c:Landroid/graphics/drawable/Drawable;

    .line 99
    invoke-virtual {p2, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lfmp;->f:Ljava/lang/CharSequence;

    .line 101
    iget-object v0, p0, Lfmp;->a:Lhvg;

    iget-object v0, v0, Lhvg;->b:Lhog;

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lfmp;->a:Lhvg;

    new-instance v1, Lhog;

    invoke-direct {v1}, Lhog;-><init>()V

    iput-object v1, v0, Lhvg;->b:Lhog;

    .line 104
    iget-object v0, p0, Lfmp;->a:Lhvg;

    iget-object v0, v0, Lhvg;->b:Lhog;

    new-instance v1, Lgzp;

    invoke-direct {v1}, Lgzp;-><init>()V

    iput-object v1, v0, Lhog;->x:Lgzp;

    .line 105
    iget-object v0, p0, Lfmp;->a:Lhvg;

    iget-object v0, v0, Lhvg;->b:Lhog;

    iget-object v0, v0, Lhog;->x:Lgzp;

    iget-object v1, p0, Lfmp;->b:Ljava/lang/String;

    iput-object v1, v0, Lgzp;->a:Ljava/lang/String;

    .line 108
    :cond_0
    iget-object v0, p0, Lfmp;->a:Lhvg;

    iget-object v0, v0, Lhvg;->b:Lhog;

    iget-object v0, v0, Lhog;->x:Lgzp;

    iget-object v1, p0, Lfmp;->e:Ljava/lang/String;

    iput-object v1, v0, Lgzp;->b:Ljava/lang/String;

    .line 113
    :try_start_0
    iget-object v0, p0, Lfmp;->a:Lhvg;

    iget-object v0, v0, Lhvg;->b:Lhog;

    .line 115
    invoke-static {p3}, Lidh;->a(Lidh;)[B

    move-result-object v1

    .line 113
    invoke-static {v0, v1}, Lidh;->a(Lidh;[B)Lidh;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    const-string v1, "Could not merge prototype navigation endpoint: "

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lfmp;->a:Lhvg;

    iget-object v0, v0, Lhvg;->b:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lfmp;->f:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfmp;->a:Lhvg;

    iget-object v0, v0, Lhvg;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lfmp;->a:Lhvg;

    iget-object v0, v0, Lhvg;->a:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmp;->f:Ljava/lang/CharSequence;

    .line 85
    :cond_0
    iget-object v0, p0, Lfmp;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lfmp;->a:Lhvg;

    iget-object v0, v0, Lhvg;->c:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lfmp;->d:Lfqh;

    return-object v0
.end method
