.class public final Lzu;
.super Lyi;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "vmhd"

    invoke-direct {p0, v0}, Lyi;-><init>(Ljava/lang/String;)V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lzu;->a:I

    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lzu;->b:[I

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Likv;->p:I

    .line 36
    return-void

    .line 30
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 53
    invoke-virtual {p0, p1}, Lzu;->c(Ljava/nio/ByteBuffer;)J

    .line 54
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lzu;->a:I

    .line 55
    new-array v0, v3, [I

    iput-object v0, p0, Lzu;->b:[I

    .line 56
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 57
    iget-object v1, p0, Lzu;->b:[I

    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v2

    aput v2, v1, v0

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lzu;->d(Ljava/nio/ByteBuffer;)V

    .line 64
    iget v0, p0, Lzu;->a:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 65
    iget-object v1, p0, Lzu;->b:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 66
    invoke-static {p1, v3}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method

.method protected final d_()J
    .locals 2

    .prologue
    .line 47
    const-wide/16 v0, 0xc

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 71
    iget v0, p0, Lzu;->a:I

    iget-object v1, p0, Lzu;->b:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lzu;->b:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lzu;->b:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x6c

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "VideoMediaHeaderBox[graphicsmode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ";opcolor0="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";opcolor1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";opcolor2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
