.class public final Lbhj;
.super Lbzt;
.source "SourceFile"


# instance fields
.field W:Lbhl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0}, Lbzt;-><init>()V

    .line 331
    return-void
.end method

.method static synthetic a(Lbhj;)Lbhl;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lbhj;->W:Lbhl;

    return-object v0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    .line 339
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lbhj;->j()Lo;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 341
    invoke-virtual {p0}, Lbhj;->j()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400ef

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 342
    invoke-virtual {p0}, Lbhj;->h()Landroid/os/Bundle;

    move-result-object v3

    .line 343
    const-string v1, "YouTubeScreen"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Ldwr;

    .line 344
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 345
    const v4, 0x7f09030a

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Ldwr;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-virtual {p0, v4, v5}, Lbhj;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v4, 0x7f0900a1

    new-instance v5, Lbhn;

    invoke-direct {v5, p0, v0, v3}, Lbhn;-><init>(Lbhj;Landroid/widget/EditText;Landroid/os/Bundle;)V

    .line 346
    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f0900a2

    new-instance v4, Lbhk;

    invoke-direct {v4, p0, v0}, Lbhk;-><init>(Lbhj;Landroid/widget/EditText;)V

    .line 347
    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 355
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 356
    new-instance v2, Lbho;

    invoke-direct {v2, p0}, Lbho;-><init>(Lbhj;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 357
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 358
    return-object v1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 327
    invoke-super {p0}, Lbzt;->e()V

    .line 328
    invoke-virtual {p0}, Lbhj;->c()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 329
    return-void
.end method
