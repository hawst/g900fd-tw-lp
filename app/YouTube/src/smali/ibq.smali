.class public final Libq;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Libq;


# instance fields
.field private b:[Lhgj;

.field private c:[Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Libq;

    sput-object v0, Libq;->a:[Libq;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    sget-object v0, Lhgj;->a:[Lhgj;

    iput-object v0, p0, Libq;->b:[Lhgj;

    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Libq;->c:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Libq;->d:Ljava/lang/String;

    iput v1, p0, Libq;->e:I

    iput v1, p0, Libq;->f:I

    iput v1, p0, Libq;->g:I

    iput v1, p0, Libq;->h:I

    iput-boolean v1, p0, Libq;->i:Z

    iput-boolean v1, p0, Libq;->j:Z

    const-string v0, ""

    iput-object v0, p0, Libq;->k:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Libq;->l:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Libq;->b:[Lhgj;

    if-eqz v0, :cond_1

    iget-object v3, p0, Libq;->b:[Lhgj;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    if-eqz v5, :cond_0

    const/4 v6, 0x1

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :cond_2
    iget-object v2, p0, Libq;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Libq;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    iget-object v3, p0, Libq;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    add-int/2addr v0, v2

    iget-object v1, p0, Libq;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Libq;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x3

    iget-object v2, p0, Libq;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Libq;->e:I

    if-eqz v1, :cond_6

    const/4 v1, 0x4

    iget v2, p0, Libq;->e:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Libq;->f:I

    if-eqz v1, :cond_7

    const/4 v1, 0x5

    iget v2, p0, Libq;->f:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Libq;->g:I

    if-eqz v1, :cond_8

    const/4 v1, 0x6

    iget v2, p0, Libq;->g:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Libq;->h:I

    if-eqz v1, :cond_9

    const/4 v1, 0x7

    iget v2, p0, Libq;->h:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-boolean v1, p0, Libq;->i:Z

    if-eqz v1, :cond_a

    const/16 v1, 0x8

    iget-boolean v2, p0, Libq;->i:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Libq;->j:Z

    if-eqz v1, :cond_b

    const/16 v1, 0x9

    iget-boolean v2, p0, Libq;->j:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Libq;->k:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const/16 v1, 0xa

    iget-object v2, p0, Libq;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Libq;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const/16 v1, 0xb

    iget-object v2, p0, Libq;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-object v1, p0, Libq;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Libq;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Libq;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Libq;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Libq;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libq;->b:[Lhgj;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgj;

    iget-object v3, p0, Libq;->b:[Lhgj;

    if-eqz v3, :cond_2

    iget-object v3, p0, Libq;->b:[Lhgj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Libq;->b:[Lhgj;

    :goto_2
    iget-object v2, p0, Libq;->b:[Lhgj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Libq;->b:[Lhgj;

    new-instance v3, Lhgj;

    invoke-direct {v3}, Lhgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libq;->b:[Lhgj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Libq;->b:[Lhgj;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Libq;->b:[Lhgj;

    new-instance v3, Lhgj;

    invoke-direct {v3}, Lhgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libq;->b:[Lhgj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libq;->c:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Libq;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Libq;->c:[Ljava/lang/String;

    :goto_3
    iget-object v2, p0, Libq;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Libq;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v2, p0, Libq;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libq;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libq;->e:I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libq;->f:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libq;->g:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libq;->h:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Libq;->i:Z

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Libq;->j:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libq;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libq;->l:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Libq;->b:[Lhgj;

    if-eqz v1, :cond_1

    iget-object v2, p0, Libq;->b:[Lhgj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Libq;->c:[Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Libq;->c:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Libq;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x3

    iget-object v1, p0, Libq;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_3
    iget v0, p0, Libq;->e:I

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    iget v1, p0, Libq;->e:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_4
    iget v0, p0, Libq;->f:I

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    iget v1, p0, Libq;->f:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_5
    iget v0, p0, Libq;->g:I

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget v1, p0, Libq;->g:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_6
    iget v0, p0, Libq;->h:I

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    iget v1, p0, Libq;->h:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_7
    iget-boolean v0, p0, Libq;->i:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    iget-boolean v1, p0, Libq;->i:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_8
    iget-boolean v0, p0, Libq;->j:Z

    if-eqz v0, :cond_9

    const/16 v0, 0x9

    iget-boolean v1, p0, Libq;->j:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_9
    iget-object v0, p0, Libq;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const/16 v0, 0xa

    iget-object v1, p0, Libq;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_a
    iget-object v0, p0, Libq;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const/16 v0, 0xb

    iget-object v1, p0, Libq;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_b
    iget-object v0, p0, Libq;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Libq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Libq;

    iget-object v2, p0, Libq;->b:[Lhgj;

    iget-object v3, p1, Libq;->b:[Lhgj;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libq;->c:[Ljava/lang/String;

    iget-object v3, p1, Libq;->c:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libq;->d:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Libq;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget v2, p0, Libq;->e:I

    iget v3, p1, Libq;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Libq;->f:I

    iget v3, p1, Libq;->f:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Libq;->g:I

    iget v3, p1, Libq;->g:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Libq;->h:I

    iget v3, p1, Libq;->h:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Libq;->i:Z

    iget-boolean v3, p1, Libq;->i:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Libq;->j:Z

    iget-boolean v3, p1, Libq;->j:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Libq;->k:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Libq;->k:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Libq;->l:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Libq;->l:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Libq;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Libq;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Libq;->d:Ljava/lang/String;

    iget-object v3, p1, Libq;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Libq;->k:Ljava/lang/String;

    iget-object v3, p1, Libq;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Libq;->l:Ljava/lang/String;

    iget-object v3, p1, Libq;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Libq;->I:Ljava/util/List;

    iget-object v3, p1, Libq;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    iget-object v2, p0, Libq;->b:[Lhgj;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Libq;->c:[Ljava/lang/String;

    if-nez v0, :cond_4

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Libq;->d:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libq;->e:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libq;->f:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libq;->g:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libq;->h:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Libq;->i:Z

    if-eqz v0, :cond_7

    move v0, v3

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Libq;->j:Z

    if-eqz v2, :cond_8

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libq;->k:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libq;->l:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Libq;->I:Ljava/util/List;

    if-nez v2, :cond_b

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v2, v0

    move v0, v1

    :goto_6
    iget-object v5, p0, Libq;->b:[Lhgj;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Libq;->b:[Lhgj;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_7
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_3
    iget-object v2, p0, Libq;->b:[Lhgj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgj;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_4
    move v0, v1

    :goto_8
    iget-object v5, p0, Libq;->c:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Libq;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_9
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_5
    iget-object v2, p0, Libq;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_6
    iget-object v0, p0, Libq;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_7
    move v0, v4

    goto :goto_1

    :cond_8
    move v3, v4

    goto :goto_2

    :cond_9
    iget-object v0, p0, Libq;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_a
    iget-object v0, p0, Libq;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_b
    iget-object v1, p0, Libq;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
