.class public final Lgjb;
.super Lghl;
.source "SourceFile"

# interfaces
.implements Leyp;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:I

.field private final c:I

.field private final d:Lgku;

.field private final j:Lgku;

.field private final k:Lgku;

.field private final l:Lgku;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Landroid/content/Context;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lezj;Lgjc;II)V
    .locals 6

    .prologue
    .line 162
    invoke-direct {p0, p1, p4, p5, p6}, Lghl;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lezj;)V

    .line 165
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lgjb;->a:Ljava/util/concurrent/Executor;

    .line 167
    iput p8, p0, Lgjb;->b:I

    .line 168
    iput p9, p0, Lgjb;->c:I

    .line 170
    new-instance v0, Lghx;

    invoke-direct {v0}, Lghx;-><init>()V

    iget v1, p0, Lgjb;->b:I

    invoke-static {v1}, Lgjb;->a(I)Leul;

    move-result-object v1

    iget-object v2, p0, Lgjb;->h:Lgik;

    invoke-virtual {p0, v2, v0}, Lgjb;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iget-object v2, p0, Lgjb;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lgjb;->h()Leun;

    move-result-object v2

    const-wide/32 v4, 0x240c8400

    invoke-virtual {p0, v2, v0, v4, v5}, Lgjb;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    :cond_0
    new-instance v2, Lgke;

    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lgke;-><init>(Landroid/content/ContentResolver;Lgku;)V

    new-instance v0, Lgkk;

    invoke-direct {v0, v2}, Lgkk;-><init>(Lgku;)V

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lgjb;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iput-object v0, p0, Lgjb;->d:Lgku;

    .line 171
    iget-object v0, p0, Lgjb;->d:Lgku;

    invoke-virtual {p0, v0}, Lgjb;->a(Lgku;)Lgjx;

    move-result-object v0

    iput-object v0, p0, Lgjb;->j:Lgku;

    .line 174
    new-instance v0, Lgja;

    invoke-direct {v0}, Lgja;-><init>()V

    iget v1, p0, Lgjb;->c:I

    new-instance v2, Leuu;

    invoke-direct {v2, v1}, Leuu;-><init>(I)V

    iget-object v1, p0, Lgjb;->d:Lgku;

    iget-object v3, p0, Lgjb;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v3}, Lgkf;->a(Lgku;Lgic;Ljava/util/concurrent/Executor;)Lgku;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgjb;->a(Lgku;)Lgjx;

    move-result-object v0

    const-wide/32 v4, 0x6ddd00

    invoke-virtual {p0, v2, v0, v4, v5}, Lgjb;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iput-object v0, p0, Lgjb;->k:Lgku;

    .line 175
    iget v0, p7, Lgjc;->a:I

    const/4 v1, 0x1

    iget-object v2, p7, Lgjc;->d:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2}, Lgjb;->a(IZLandroid/graphics/Bitmap$Config;)Lgku;

    .line 179
    iget v0, p7, Lgjc;->b:I

    const/4 v1, 0x1

    iget-object v2, p7, Lgjc;->e:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2}, Lgjb;->a(IZLandroid/graphics/Bitmap$Config;)Lgku;

    move-result-object v0

    iput-object v0, p0, Lgjb;->l:Lgku;

    .line 183
    iget v0, p7, Lgjc;->c:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lgjb;->a(IZLandroid/graphics/Bitmap$Config;)Lgku;

    .line 187
    return-void
.end method

.method private a(IZLandroid/graphics/Bitmap$Config;)Lgku;
    .locals 4

    .prologue
    .line 246
    new-instance v0, Lgja;

    invoke-direct {v0, p1, p2, p3}, Lgja;-><init>(IZLandroid/graphics/Bitmap$Config;)V

    .line 252
    iget-object v1, p0, Lgjb;->d:Lgku;

    iget-object v2, p0, Lgjb;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, Lgkf;->a(Lgku;Lgic;Ljava/util/concurrent/Executor;)Lgku;

    move-result-object v0

    .line 258
    invoke-virtual {p0, v0}, Lgjb;->a(Lgku;)Lgjx;

    move-result-object v0

    .line 261
    iget v1, p0, Lgjb;->c:I

    invoke-static {v1}, Lgjb;->a(I)Leul;

    move-result-object v1

    .line 262
    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lgjb;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    .line 264
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Leuc;)V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lgjb;->k:Lgku;

    invoke-interface {v0, p1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 192
    return-void
.end method

.method public final b(Landroid/net/Uri;Leuc;)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lgjb;->l:Lgku;

    invoke-interface {v0, p1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 202
    return-void
.end method

.method public final c(Landroid/net/Uri;Leuc;)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lgjb;->j:Lgku;

    invoke-interface {v0, p1, p2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 212
    return-void
.end method
