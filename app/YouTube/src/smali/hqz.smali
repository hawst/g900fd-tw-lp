.class public final Lhqz;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgm;

.field public b:Lhgl;

.field public c:[Lhbw;

.field private d:Lhrs;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Lhqz;->a:Lhgm;

    iput-object v1, p0, Lhqz;->b:Lhgl;

    sget-object v0, Lhbw;->a:[Lhbw;

    iput-object v0, p0, Lhqz;->c:[Lhbw;

    iput-object v1, p0, Lhqz;->d:Lhrs;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhqz;->a:Lhgm;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    iget-object v2, p0, Lhqz;->a:Lhgm;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhqz;->b:Lhgl;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhqz;->b:Lhgl;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhqz;->c:[Lhbw;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhqz;->c:[Lhbw;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    if-eqz v4, :cond_1

    const/4 v5, 0x3

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lhqz;->d:Lhrs;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lhqz;->d:Lhrs;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lhqz;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhqz;->J:I

    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhqz;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhqz;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhqz;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhqz;->a:Lhgm;

    if-nez v0, :cond_2

    new-instance v0, Lhgm;

    invoke-direct {v0}, Lhgm;-><init>()V

    iput-object v0, p0, Lhqz;->a:Lhgm;

    :cond_2
    iget-object v0, p0, Lhqz;->a:Lhgm;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhqz;->b:Lhgl;

    if-nez v0, :cond_3

    new-instance v0, Lhgl;

    invoke-direct {v0}, Lhgl;-><init>()V

    iput-object v0, p0, Lhqz;->b:Lhgl;

    :cond_3
    iget-object v0, p0, Lhqz;->b:Lhgl;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhqz;->c:[Lhbw;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbw;

    iget-object v3, p0, Lhqz;->c:[Lhbw;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lhqz;->c:[Lhbw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lhqz;->c:[Lhbw;

    :goto_2
    iget-object v2, p0, Lhqz;->c:[Lhbw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lhqz;->c:[Lhbw;

    new-instance v3, Lhbw;

    invoke-direct {v3}, Lhbw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhqz;->c:[Lhbw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lhqz;->c:[Lhbw;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lhqz;->c:[Lhbw;

    new-instance v3, Lhbw;

    invoke-direct {v3}, Lhbw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhqz;->c:[Lhbw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lhqz;->d:Lhrs;

    if-nez v0, :cond_7

    new-instance v0, Lhrs;

    invoke-direct {v0}, Lhrs;-><init>()V

    iput-object v0, p0, Lhqz;->d:Lhrs;

    :cond_7
    iget-object v0, p0, Lhqz;->d:Lhrs;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhqz;->a:Lhgm;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhqz;->a:Lhgm;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhqz;->b:Lhgl;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhqz;->b:Lhgl;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhqz;->c:[Lhbw;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lhqz;->c:[Lhbw;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhqz;->d:Lhrs;

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    iget-object v1, p0, Lhqz;->d:Lhrs;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Lhqz;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhqz;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhqz;

    iget-object v2, p0, Lhqz;->a:Lhgm;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhqz;->a:Lhgm;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhqz;->b:Lhgl;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhqz;->b:Lhgl;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhqz;->c:[Lhbw;

    iget-object v3, p1, Lhqz;->c:[Lhbw;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhqz;->d:Lhrs;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhqz;->d:Lhrs;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhqz;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhqz;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhqz;->a:Lhgm;

    iget-object v3, p1, Lhqz;->a:Lhgm;

    invoke-virtual {v2, v3}, Lhgm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhqz;->b:Lhgl;

    iget-object v3, p1, Lhqz;->b:Lhgl;

    invoke-virtual {v2, v3}, Lhgl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhqz;->d:Lhrs;

    iget-object v3, p1, Lhqz;->d:Lhrs;

    invoke-virtual {v2, v3}, Lhrs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhqz;->I:Ljava/util/List;

    iget-object v3, p1, Lhqz;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhqz;->a:Lhgm;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhqz;->b:Lhgl;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Lhqz;->c:[Lhbw;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhqz;->d:Lhrs;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhqz;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lhqz;->a:Lhgm;

    invoke-virtual {v0}, Lhgm;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhqz;->b:Lhgl;

    invoke-virtual {v0}, Lhgl;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    move v2, v0

    move v0, v1

    :goto_4
    iget-object v3, p0, Lhqz;->c:[Lhbw;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhqz;->c:[Lhbw;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    iget-object v2, p0, Lhqz;->c:[Lhbw;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbw;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    iget-object v0, p0, Lhqz;->d:Lhrs;

    invoke-virtual {v0}, Lhrs;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lhqz;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
