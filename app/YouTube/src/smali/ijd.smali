.class final Lijd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[C

.field private static final b:[C

.field private static final c:[Ljava/math/BigInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x10

    .line 61
    const/16 v0, 0x1d

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lijd;->a:[C

    .line 66
    const/16 v0, 0x19

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lijd;->b:[C

    .line 77
    new-array v0, v5, [Ljava/math/BigInteger;

    .line 78
    sput-object v0, Lijd;->c:[Ljava/math/BigInteger;

    const/4 v1, 0x0

    sget-object v2, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    .line 79
    const-wide/16 v0, 0x384

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    .line 80
    sget-object v0, Lijd;->c:[Ljava/math/BigInteger;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 81
    const/4 v0, 0x2

    :goto_0
    sget-object v2, Lijd;->c:[Ljava/math/BigInteger;

    if-ge v0, v5, :cond_0

    .line 82
    sget-object v2, Lijd;->c:[Ljava/math/BigInteger;

    sget-object v3, Lijd;->c:[Ljava/math/BigInteger;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v3, v1}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v3

    aput-object v3, v2, v0

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    :cond_0
    return-void

    .line 61
    nop

    :array_0
    .array-data 2
        0x3bs
        0x3cs
        0x3es
        0x40s
        0x5bs
        0x5cs
        0x7ds
        0x5fs
        0x60s
        0x7es
        0x21s
        0xds
        0x9s
        0x2cs
        0x3as
        0xas
        0x2ds
        0x2es
        0x24s
        0x2fs
        0x22s
        0x7cs
        0x2as
        0x28s
        0x29s
        0x3fs
        0x7bs
        0x7ds
        0x27s
    .end array-data

    .line 66
    nop

    :array_1
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x26s
        0xds
        0x9s
        0x2cs
        0x3as
        0x23s
        0x2ds
        0x2es
        0x24s
        0x2fs
        0x2bs
        0x25s
        0x2as
        0x3ds
        0x5es
    .end array-data
.end method

.method private static a(I[IILjava/lang/StringBuilder;)I
    .locals 12

    .prologue
    .line 437
    const/16 v0, 0x385

    if-ne p0, v0, :cond_7

    .line 440
    const/4 v4, 0x0

    .line 441
    const-wide/16 v2, 0x0

    .line 442
    const/4 v0, 0x6

    new-array v10, v0, [C

    .line 443
    const/4 v0, 0x6

    new-array v11, v0, [I

    .line 444
    const/4 v1, 0x0

    .line 445
    add-int/lit8 v5, p2, 0x1

    aget v0, p1, p2

    move v6, v5

    .line 446
    :goto_0
    const/4 v5, 0x0

    aget v5, p1, v5

    if-ge v6, v5, :cond_3

    if-nez v1, :cond_3

    .line 447
    add-int/lit8 v5, v4, 0x1

    aput v0, v11, v4

    .line 449
    const-wide/16 v8, 0x384

    mul-long/2addr v2, v8

    int-to-long v8, v0

    add-long/2addr v2, v8

    .line 450
    add-int/lit8 v7, v6, 0x1

    aget v0, p1, v6

    .line 452
    const/16 v4, 0x384

    if-eq v0, v4, :cond_0

    const/16 v4, 0x385

    if-eq v0, v4, :cond_0

    const/16 v4, 0x386

    if-eq v0, v4, :cond_0

    const/16 v4, 0x39c

    if-eq v0, v4, :cond_0

    const/16 v4, 0x3a0

    if-eq v0, v4, :cond_0

    const/16 v4, 0x39b

    if-eq v0, v4, :cond_0

    const/16 v4, 0x39a

    if-ne v0, v4, :cond_1

    .line 459
    :cond_0
    add-int/lit8 v4, v7, -0x1

    .line 460
    const/4 v1, 0x1

    move v6, v4

    move v4, v5

    goto :goto_0

    .line 462
    :cond_1
    rem-int/lit8 v4, v5, 0x5

    if-nez v4, :cond_d

    if-lez v5, :cond_d

    .line 465
    const/4 v4, 0x0

    :goto_1
    const/4 v5, 0x6

    if-ge v4, v5, :cond_2

    .line 466
    rsub-int/lit8 v5, v4, 0x5

    const-wide/16 v8, 0x100

    rem-long v8, v2, v8

    long-to-int v6, v8

    int-to-char v6, v6

    aput-char v6, v10, v5

    .line 467
    const/16 v5, 0x8

    shr-long v8, v2, v5

    .line 465
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v8

    goto :goto_1

    .line 469
    :cond_2
    invoke-virtual {p3, v10}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 470
    const/4 v4, 0x0

    move v6, v7

    goto :goto_0

    .line 476
    :cond_3
    const/4 v1, 0x0

    aget v1, p1, v1

    if-ne v6, v1, :cond_4

    const/16 v1, 0x384

    if-ge v0, v1, :cond_4

    .line 477
    add-int/lit8 v1, v4, 0x1

    aput v0, v11, v4

    move v4, v1

    .line 483
    :cond_4
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v4, :cond_5

    .line 484
    aget v1, v11, v0

    int-to-char v1, v1

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 483
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move p2, v6

    .line 524
    :cond_6
    return p2

    .line 487
    :cond_7
    const/16 v0, 0x39c

    if-ne p0, v0, :cond_6

    .line 490
    const/4 v1, 0x0

    .line 491
    const-wide/16 v2, 0x0

    .line 492
    const/4 v0, 0x0

    .line 493
    :cond_8
    :goto_3
    const/4 v4, 0x0

    aget v4, p1, v4

    if-ge p2, v4, :cond_6

    if-nez v0, :cond_6

    .line 494
    add-int/lit8 v4, p2, 0x1

    aget v5, p1, p2

    .line 495
    const/16 v6, 0x384

    if-ge v5, v6, :cond_9

    .line 496
    add-int/lit8 v1, v1, 0x1

    .line 498
    const-wide/16 v6, 0x384

    mul-long/2addr v2, v6

    int-to-long v6, v5

    add-long/2addr v2, v6

    move p2, v4

    .line 511
    :goto_4
    rem-int/lit8 v4, v1, 0x5

    if-nez v4, :cond_8

    if-lez v1, :cond_8

    .line 514
    const/4 v1, 0x6

    new-array v4, v1, [C

    .line 515
    const/4 v1, 0x0

    :goto_5
    const/4 v5, 0x6

    if-ge v1, v5, :cond_b

    .line 516
    rsub-int/lit8 v5, v1, 0x5

    const-wide/16 v6, 0xff

    and-long/2addr v6, v2

    long-to-int v6, v6

    int-to-char v6, v6

    aput-char v6, v4, v5

    .line 517
    const/16 v5, 0x8

    shr-long/2addr v2, v5

    .line 515
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 500
    :cond_9
    const/16 v6, 0x384

    if-eq v5, v6, :cond_a

    const/16 v6, 0x385

    if-eq v5, v6, :cond_a

    const/16 v6, 0x386

    if-eq v5, v6, :cond_a

    const/16 v6, 0x39c

    if-eq v5, v6, :cond_a

    const/16 v6, 0x3a0

    if-eq v5, v6, :cond_a

    const/16 v6, 0x39b

    if-eq v5, v6, :cond_a

    const/16 v6, 0x39a

    if-ne v5, v6, :cond_c

    .line 507
    :cond_a
    add-int/lit8 p2, v4, -0x1

    .line 508
    const/4 v0, 0x1

    goto :goto_4

    .line 519
    :cond_b
    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    .line 520
    const/4 v1, 0x0

    goto :goto_3

    :cond_c
    move p2, v4

    goto :goto_4

    :cond_d
    move v4, v5

    move v6, v7

    goto/16 :goto_0
.end method

.method private static a([IILiiy;)I
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 145
    add-int/lit8 v0, p1, 0x2

    aget v2, p0, v1

    if-le v0, v2, :cond_0

    .line 147
    invoke-static {}, Liet;->a()Liet;

    move-result-object v0

    throw v0

    .line 149
    :cond_0
    new-array v2, v4, [I

    move v0, v1

    .line 150
    :goto_0
    if-ge v0, v4, :cond_1

    .line 151
    aget v3, p0, p1

    aput v3, v2, v0

    .line 150
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 153
    :cond_1
    invoke-static {v2, v4}, Lijd;->a([II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    invoke-static {p0, p1, v0}, Lijd;->a([IILjava/lang/StringBuilder;)I

    move-result v3

    .line 160
    aget v0, p0, v3

    const/16 v2, 0x39b

    if-ne v0, v2, :cond_5

    .line 161
    add-int/lit8 v0, v3, 0x1

    .line 162
    aget v2, p0, v1

    sub-int/2addr v2, v0

    new-array v5, v2, [I

    move v2, v1

    move v3, v0

    move v0, v1

    .line 166
    :goto_1
    aget v4, p0, v1

    if-ge v3, v4, :cond_3

    if-nez v0, :cond_3

    .line 167
    add-int/lit8 v4, v3, 0x1

    aget v6, p0, v3

    .line 168
    const/16 v3, 0x384

    if-ge v6, v3, :cond_2

    .line 169
    add-int/lit8 v3, v2, 0x1

    aput v6, v5, v2

    move v2, v3

    move v3, v4

    goto :goto_1

    .line 171
    :cond_2
    packed-switch v6, :pswitch_data_0

    .line 178
    invoke-static {}, Liet;->a()Liet;

    move-result-object v0

    throw v0

    .line 173
    :pswitch_0
    add-int/lit8 v3, v4, 0x1

    .line 175
    const/4 v0, 0x1

    .line 176
    goto :goto_1

    .line 181
    :cond_3
    invoke-static {v5, v2}, Ljava/util/Arrays;->copyOf([II)[I

    .line 189
    :cond_4
    :goto_2
    return v3

    .line 184
    :cond_5
    aget v0, p0, v3

    const/16 v1, 0x39a

    if-ne v0, v1, :cond_4

    .line 185
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x39a
        :pswitch_0
    .end packed-switch
.end method

.method private static a([IILjava/lang/StringBuilder;)I
    .locals 11

    .prologue
    .line 204
    const/4 v0, 0x0

    aget v0, p0, v0

    sub-int/2addr v0, p1

    shl-int/lit8 v0, v0, 0x1

    new-array v5, v0, [I

    .line 206
    const/4 v0, 0x0

    aget v0, p0, v0

    sub-int/2addr v0, p1

    shl-int/lit8 v0, v0, 0x1

    new-array v6, v0, [I

    .line 208
    const/4 v1, 0x0

    .line 209
    const/4 v0, 0x0

    .line 210
    :goto_0
    const/4 v2, 0x0

    aget v2, p0, v2

    if-ge p1, v2, :cond_1

    if-nez v0, :cond_1

    .line 211
    add-int/lit8 v3, p1, 0x1

    aget v2, p0, p1

    .line 212
    const/16 v4, 0x384

    if-ge v2, v4, :cond_0

    .line 213
    div-int/lit8 v4, v2, 0x1e

    aput v4, v5, v1

    .line 214
    add-int/lit8 v4, v1, 0x1

    rem-int/lit8 v2, v2, 0x1e

    aput v2, v5, v4

    .line 215
    add-int/lit8 v1, v1, 0x2

    move p1, v3

    goto :goto_0

    .line 217
    :cond_0
    sparse-switch v2, :sswitch_data_0

    move p1, v3

    goto :goto_0

    .line 220
    :sswitch_0
    add-int/lit8 v2, v1, 0x1

    const/16 v4, 0x384

    aput v4, v5, v1

    move v1, v2

    move p1, v3

    .line 221
    goto :goto_0

    .line 223
    :sswitch_1
    add-int/lit8 p1, v3, -0x1

    .line 224
    const/4 v0, 0x1

    .line 225
    goto :goto_0

    .line 227
    :sswitch_2
    add-int/lit8 p1, v3, -0x1

    .line 228
    const/4 v0, 0x1

    .line 229
    goto :goto_0

    .line 231
    :sswitch_3
    add-int/lit8 p1, v3, -0x1

    .line 232
    const/4 v0, 0x1

    .line 233
    goto :goto_0

    .line 235
    :sswitch_4
    add-int/lit8 p1, v3, -0x1

    .line 236
    const/4 v0, 0x1

    .line 237
    goto :goto_0

    .line 239
    :sswitch_5
    add-int/lit8 p1, v3, -0x1

    .line 240
    const/4 v0, 0x1

    .line 241
    goto :goto_0

    .line 249
    :sswitch_6
    const/16 v2, 0x391

    aput v2, v5, v1

    .line 250
    add-int/lit8 p1, v3, 0x1

    aget v2, p0, v3

    .line 251
    aput v2, v6, v1

    .line 252
    add-int/lit8 v1, v1, 0x1

    .line 253
    goto :goto_0

    .line 255
    :sswitch_7
    add-int/lit8 p1, v3, -0x1

    .line 256
    const/4 v0, 0x1

    goto :goto_0

    .line 261
    :cond_1
    const/4 v3, 0x1

    const/4 v2, 0x1

    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v1, :cond_1f

    aget v7, v5, v4

    const/4 v0, 0x0

    sget-object v8, Lije;->a:[I

    add-int/lit8 v9, v3, -0x1

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :pswitch_0
    const/16 v8, 0x1a

    if-ge v7, v8, :cond_4

    add-int/lit8 v0, v7, 0x41

    int-to-char v0, v0

    goto :goto_2

    :cond_4
    const/16 v8, 0x1a

    if-ne v7, v8, :cond_5

    const/16 v0, 0x20

    goto :goto_2

    :cond_5
    const/16 v8, 0x1b

    if-ne v7, v8, :cond_6

    const/4 v3, 0x2

    goto :goto_2

    :cond_6
    const/16 v8, 0x1c

    if-ne v7, v8, :cond_7

    const/4 v3, 0x3

    goto :goto_2

    :cond_7
    const/16 v8, 0x1d

    if-ne v7, v8, :cond_8

    const/4 v2, 0x6

    move v10, v3

    move v3, v2

    move v2, v10

    goto :goto_2

    :cond_8
    const/16 v8, 0x391

    if-ne v7, v8, :cond_9

    aget v7, v6, v4

    int-to-char v7, v7

    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_9
    const/16 v8, 0x384

    if-ne v7, v8, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :pswitch_1
    const/16 v8, 0x1a

    if-ge v7, v8, :cond_a

    add-int/lit8 v0, v7, 0x61

    int-to-char v0, v0

    goto :goto_2

    :cond_a
    const/16 v8, 0x1a

    if-ne v7, v8, :cond_b

    const/16 v0, 0x20

    goto :goto_2

    :cond_b
    const/16 v8, 0x1b

    if-ne v7, v8, :cond_c

    const/4 v2, 0x5

    move v10, v3

    move v3, v2

    move v2, v10

    goto :goto_2

    :cond_c
    const/16 v8, 0x1c

    if-ne v7, v8, :cond_d

    const/4 v3, 0x3

    goto :goto_2

    :cond_d
    const/16 v8, 0x1d

    if-ne v7, v8, :cond_e

    const/4 v2, 0x6

    move v10, v3

    move v3, v2

    move v2, v10

    goto :goto_2

    :cond_e
    const/16 v8, 0x391

    if-ne v7, v8, :cond_f

    aget v7, v6, v4

    int-to-char v7, v7

    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_f
    const/16 v8, 0x384

    if-ne v7, v8, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :pswitch_2
    const/16 v8, 0x19

    if-ge v7, v8, :cond_10

    sget-object v0, Lijd;->b:[C

    aget-char v0, v0, v7

    goto :goto_2

    :cond_10
    const/16 v8, 0x19

    if-ne v7, v8, :cond_11

    const/4 v3, 0x4

    goto/16 :goto_2

    :cond_11
    const/16 v8, 0x1a

    if-ne v7, v8, :cond_12

    const/16 v0, 0x20

    goto/16 :goto_2

    :cond_12
    const/16 v8, 0x1b

    if-ne v7, v8, :cond_13

    const/4 v3, 0x2

    goto/16 :goto_2

    :cond_13
    const/16 v8, 0x1c

    if-ne v7, v8, :cond_14

    const/4 v3, 0x1

    goto/16 :goto_2

    :cond_14
    const/16 v8, 0x1d

    if-ne v7, v8, :cond_15

    const/4 v2, 0x6

    move v10, v3

    move v3, v2

    move v2, v10

    goto/16 :goto_2

    :cond_15
    const/16 v8, 0x391

    if-ne v7, v8, :cond_16

    aget v7, v6, v4

    int-to-char v7, v7

    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_16
    const/16 v8, 0x384

    if-ne v7, v8, :cond_2

    const/4 v3, 0x1

    goto/16 :goto_2

    :pswitch_3
    const/16 v8, 0x1d

    if-ge v7, v8, :cond_17

    sget-object v0, Lijd;->a:[C

    aget-char v0, v0, v7

    goto/16 :goto_2

    :cond_17
    const/16 v8, 0x1d

    if-ne v7, v8, :cond_18

    const/4 v3, 0x1

    goto/16 :goto_2

    :cond_18
    const/16 v8, 0x391

    if-ne v7, v8, :cond_19

    aget v7, v6, v4

    int-to-char v7, v7

    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_19
    const/16 v8, 0x384

    if-ne v7, v8, :cond_2

    const/4 v3, 0x1

    goto/16 :goto_2

    :pswitch_4
    const/16 v3, 0x1a

    if-ge v7, v3, :cond_1a

    add-int/lit8 v0, v7, 0x41

    int-to-char v0, v0

    move v3, v2

    goto/16 :goto_2

    :cond_1a
    const/16 v3, 0x1a

    if-ne v7, v3, :cond_1b

    const/16 v0, 0x20

    move v3, v2

    goto/16 :goto_2

    :cond_1b
    const/16 v3, 0x384

    if-ne v7, v3, :cond_20

    const/4 v3, 0x1

    goto/16 :goto_2

    :pswitch_5
    const/16 v3, 0x1d

    if-ge v7, v3, :cond_1c

    sget-object v0, Lijd;->a:[C

    aget-char v0, v0, v7

    move v3, v2

    goto/16 :goto_2

    :cond_1c
    const/16 v3, 0x1d

    if-ne v7, v3, :cond_1d

    const/4 v3, 0x1

    goto/16 :goto_2

    :cond_1d
    const/16 v3, 0x391

    if-ne v7, v3, :cond_1e

    aget v3, v6, v4

    int-to-char v3, v3

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v3, v2

    goto/16 :goto_2

    :cond_1e
    const/16 v3, 0x384

    if-ne v7, v3, :cond_20

    const/4 v3, 0x1

    goto/16 :goto_2

    .line 262
    :cond_1f
    return p1

    :cond_20
    move v3, v2

    goto/16 :goto_2

    .line 217
    nop

    :sswitch_data_0
    .sparse-switch
        0x384 -> :sswitch_0
        0x385 -> :sswitch_1
        0x386 -> :sswitch_2
        0x391 -> :sswitch_6
        0x39a -> :sswitch_5
        0x39b -> :sswitch_4
        0x39c -> :sswitch_7
        0x3a0 -> :sswitch_3
    .end sparse-switch

    .line 261
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static a([ILjava/lang/String;)Liga;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 92
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v0, p0

    shl-int/lit8 v0, v0, 0x1

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 95
    const/4 v0, 0x1

    const/4 v1, 0x2

    aget v0, p0, v0

    .line 96
    new-instance v3, Liiy;

    invoke-direct {v3}, Liiy;-><init>()V

    .line 97
    :goto_0
    const/4 v4, 0x0

    aget v4, p0, v4

    if-ge v1, v4, :cond_1

    .line 98
    sparse-switch v0, :sswitch_data_0

    .line 125
    add-int/lit8 v0, v1, -0x1

    .line 126
    invoke-static {p0, v0, v2}, Lijd;->a([IILjava/lang/StringBuilder;)I

    move-result v0

    .line 129
    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 130
    add-int/lit8 v1, v0, 0x1

    aget v0, p0, v0

    goto :goto_0

    .line 100
    :sswitch_0
    invoke-static {p0, v1, v2}, Lijd;->a([IILjava/lang/StringBuilder;)I

    move-result v0

    goto :goto_1

    .line 103
    :sswitch_1
    invoke-static {v0, p0, v1, v2}, Lijd;->a(I[IILjava/lang/StringBuilder;)I

    move-result v0

    goto :goto_1

    .line 106
    :sswitch_2
    invoke-static {p0, v1, v2}, Lijd;->b([IILjava/lang/StringBuilder;)I

    move-result v0

    goto :goto_1

    .line 109
    :sswitch_3
    invoke-static {v0, p0, v1, v2}, Lijd;->a(I[IILjava/lang/StringBuilder;)I

    move-result v0

    goto :goto_1

    .line 112
    :sswitch_4
    invoke-static {v0, p0, v1, v2}, Lijd;->a(I[IILjava/lang/StringBuilder;)I

    move-result v0

    goto :goto_1

    .line 115
    :sswitch_5
    invoke-static {p0, v1, v3}, Lijd;->a([IILiiy;)I

    move-result v0

    goto :goto_1

    .line 120
    :sswitch_6
    invoke-static {}, Liet;->a()Liet;

    move-result-object v0

    throw v0

    .line 132
    :cond_0
    invoke-static {}, Liet;->a()Liet;

    move-result-object v0

    throw v0

    .line 135
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 136
    invoke-static {}, Liet;->a()Liet;

    move-result-object v0

    throw v0

    .line 138
    :cond_2
    new-instance v0, Liga;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v5, v1, v5, p1}, Liga;-><init>([BLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 139
    iput-object v3, v0, Liga;->e:Ljava/lang/Object;

    .line 140
    return-object v0

    .line 98
    nop

    :sswitch_data_0
    .sparse-switch
        0x384 -> :sswitch_0
        0x385 -> :sswitch_1
        0x386 -> :sswitch_2
        0x391 -> :sswitch_3
        0x39a -> :sswitch_6
        0x39b -> :sswitch_6
        0x39c -> :sswitch_4
        0x3a0 -> :sswitch_5
    .end sparse-switch
.end method

.method private static a([II)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 619
    sget-object v0, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    move-object v2, v0

    move v0, v1

    .line 620
    :goto_0
    if-ge v0, p1, :cond_0

    .line 621
    sget-object v3, Lijd;->c:[Ljava/math/BigInteger;

    sub-int v4, p1, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aget v4, p0, v0

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    .line 620
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 623
    :cond_0
    invoke-virtual {v2}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v0

    .line 624
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x31

    if-eq v1, v2, :cond_1

    .line 625
    invoke-static {}, Liet;->a()Liet;

    move-result-object v0

    throw v0

    .line 627
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b([IILjava/lang/StringBuilder;)I
    .locals 9

    .prologue
    const/16 v8, 0x384

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 536
    .line 539
    const/16 v0, 0xf

    new-array v5, v0, [I

    move v0, v1

    move v2, v1

    .line 541
    :cond_0
    :goto_0
    aget v4, p0, v1

    if-ge p1, v4, :cond_5

    if-nez v0, :cond_5

    .line 542
    add-int/lit8 v4, p1, 0x1

    aget v6, p0, p1

    .line 543
    aget v7, p0, v1

    if-ne v4, v7, :cond_1

    move v0, v3

    .line 546
    :cond_1
    if-ge v6, v8, :cond_3

    .line 547
    aput v6, v5, v2

    .line 548
    add-int/lit8 v2, v2, 0x1

    move p1, v4

    .line 560
    :goto_1
    rem-int/lit8 v4, v2, 0xf

    if-eqz v4, :cond_2

    const/16 v4, 0x386

    if-eq v6, v4, :cond_2

    if-eqz v0, :cond_0

    .line 567
    :cond_2
    invoke-static {v5, v2}, Lijd;->a([II)Ljava/lang/String;

    move-result-object v2

    .line 568
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v1

    .line 569
    goto :goto_0

    .line 550
    :cond_3
    if-eq v6, v8, :cond_4

    const/16 v7, 0x385

    if-eq v6, v7, :cond_4

    const/16 v7, 0x39c

    if-eq v6, v7, :cond_4

    const/16 v7, 0x3a0

    if-eq v6, v7, :cond_4

    const/16 v7, 0x39b

    if-eq v6, v7, :cond_4

    const/16 v7, 0x39a

    if-ne v6, v7, :cond_6

    .line 556
    :cond_4
    add-int/lit8 p1, v4, -0x1

    move v0, v3

    .line 557
    goto :goto_1

    .line 572
    :cond_5
    return p1

    :cond_6
    move p1, v4

    goto :goto_1
.end method
