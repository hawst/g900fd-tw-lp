.class public final enum Lgch;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgch;

.field public static final enum b:Lgch;

.field public static final enum c:Lgch;

.field public static final enum d:Lgch;

.field public static final enum e:Lgch;

.field public static final enum f:Lgch;

.field public static final enum g:Lgch;

.field public static final enum h:Lgch;

.field public static final enum i:Lgch;

.field public static final enum j:Lgch;

.field public static final enum k:Lgch;

.field public static final enum l:Lgch;

.field public static final enum m:Lgch;

.field public static final enum n:Lgch;

.field public static final enum o:Lgch;

.field public static final enum p:Lgch;

.field public static final enum q:Lgch;

.field public static final enum r:Lgch;

.field public static final enum s:Lgch;

.field public static final enum t:Lgch;

.field private static final synthetic v:[Lgch;


# instance fields
.field public final u:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 44
    new-instance v0, Lgch;

    const-string v1, "PLAYABLE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->a:Lgch;

    .line 45
    new-instance v0, Lgch;

    const-string v1, "PROCESSING"

    const v2, 0x7f0900ae

    invoke-direct {v0, v1, v5, v2}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->b:Lgch;

    .line 46
    new-instance v0, Lgch;

    const-string v1, "DELETED"

    const v2, 0x7f0900af

    invoke-direct {v0, v1, v6, v2}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->c:Lgch;

    .line 47
    new-instance v0, Lgch;

    const-string v1, "OFFLINE_DELETED"

    const v2, 0x7f0900b0

    invoke-direct {v0, v1, v7, v2}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->d:Lgch;

    .line 50
    new-instance v0, Lgch;

    const-string v1, "COUNTRY_RESTRICTED"

    const v2, 0x7f0900b1

    invoke-direct {v0, v1, v8, v2}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->e:Lgch;

    .line 51
    new-instance v0, Lgch;

    const-string v1, "NOT_AVAILABLE_ON_MOBILE"

    const/4 v2, 0x5

    const v3, 0x7f0900b2

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->f:Lgch;

    .line 52
    new-instance v0, Lgch;

    const-string v1, "PRIVATE"

    const/4 v2, 0x6

    const v3, 0x7f0900b3

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->g:Lgch;

    .line 53
    new-instance v0, Lgch;

    const-string v1, "BLOCKED_FOR_CLIENT_APP"

    const/4 v2, 0x7

    const v3, 0x7f0900b4

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->h:Lgch;

    .line 56
    new-instance v0, Lgch;

    const-string v1, "COPYRIGHT"

    const/16 v2, 0x8

    const v3, 0x7f0900b5

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->i:Lgch;

    .line 57
    new-instance v0, Lgch;

    const-string v1, "INAPPROPRIATE"

    const/16 v2, 0x9

    const v3, 0x7f0900b6

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->j:Lgch;

    .line 58
    new-instance v0, Lgch;

    const-string v1, "DUPLICATE"

    const/16 v2, 0xa

    const v3, 0x7f0900b7

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->k:Lgch;

    .line 59
    new-instance v0, Lgch;

    const-string v1, "TERMS_OF_USE"

    const/16 v2, 0xb

    const v3, 0x7f0900b8

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->l:Lgch;

    .line 60
    new-instance v0, Lgch;

    const-string v1, "ACCOUNT_SUSPENDED"

    const/16 v2, 0xc

    const v3, 0x7f0900b9

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->m:Lgch;

    .line 61
    new-instance v0, Lgch;

    const-string v1, "VIDEO_TOO_LONG"

    const/16 v2, 0xd

    const v3, 0x7f0900ba

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->n:Lgch;

    .line 62
    new-instance v0, Lgch;

    const-string v1, "BLOCKED_BY_OWNER"

    const/16 v2, 0xe

    const v3, 0x7f0900bb

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->o:Lgch;

    .line 65
    new-instance v0, Lgch;

    const-string v1, "CANT_PROCESS"

    const/16 v2, 0xf

    const v3, 0x7f0900bc

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->p:Lgch;

    .line 66
    new-instance v0, Lgch;

    const-string v1, "INVALID_FORMAT"

    const/16 v2, 0x10

    const v3, 0x7f0900bd

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->q:Lgch;

    .line 67
    new-instance v0, Lgch;

    const-string v1, "UNSUPPORTED_CODEC"

    const/16 v2, 0x11

    const v3, 0x7f0900be

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->r:Lgch;

    .line 68
    new-instance v0, Lgch;

    const-string v1, "EMPTY"

    const/16 v2, 0x12

    const v3, 0x7f0900bf

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->s:Lgch;

    .line 69
    new-instance v0, Lgch;

    const-string v1, "TOO_SMALL"

    const/16 v2, 0x13

    const v3, 0x7f0900c0

    invoke-direct {v0, v1, v2, v3}, Lgch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgch;->t:Lgch;

    .line 42
    const/16 v0, 0x14

    new-array v0, v0, [Lgch;

    sget-object v1, Lgch;->a:Lgch;

    aput-object v1, v0, v4

    sget-object v1, Lgch;->b:Lgch;

    aput-object v1, v0, v5

    sget-object v1, Lgch;->c:Lgch;

    aput-object v1, v0, v6

    sget-object v1, Lgch;->d:Lgch;

    aput-object v1, v0, v7

    sget-object v1, Lgch;->e:Lgch;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lgch;->f:Lgch;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgch;->g:Lgch;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lgch;->h:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lgch;->i:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lgch;->j:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lgch;->k:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lgch;->l:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lgch;->m:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lgch;->n:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lgch;->o:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lgch;->p:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lgch;->q:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lgch;->r:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lgch;->s:Lgch;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lgch;->t:Lgch;

    aput-object v2, v0, v1

    sput-object v0, Lgch;->v:[Lgch;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 74
    iput p3, p0, Lgch;->u:I

    .line 75
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgch;
    .locals 1

    .prologue
    .line 42
    const-class v0, Lgch;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgch;

    return-object v0
.end method

.method public static values()[Lgch;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lgch;->v:[Lgch;

    invoke-virtual {v0}, [Lgch;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgch;

    return-object v0
.end method
