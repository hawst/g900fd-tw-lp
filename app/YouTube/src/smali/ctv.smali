.class public final Lctv;
.super Lgit;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 19
    .line 22
    invoke-static {p1, p2}, Lctv;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 19
    invoke-direct {p0, p1, p2, v0}, Lgit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public static a(Lgit;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    .line 43
    iget-object v0, p0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->c()Ljava/lang/String;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lgit;->b:Lgiv;

    invoke-virtual {v1}, Lgiv;->d()Ljava/lang/String;

    move-result-object v1

    .line 42
    invoke-static {v0, v1}, Lctv;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    if-eqz p1, :cond_0

    .line 53
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 27
    if-ne p0, p1, :cond_0

    .line 28
    const/4 v0, 0x1

    .line 34
    :goto_0
    return v0

    .line 30
    :cond_0
    instance-of v0, p1, Lctv;

    if-nez v0, :cond_1

    .line 31
    const/4 v0, 0x0

    goto :goto_0

    .line 33
    :cond_1
    check-cast p1, Lctv;

    .line 34
    iget-object v0, p0, Lctv;->b:Lgiv;

    iget-object v1, p1, Lctv;->b:Lgiv;

    invoke-virtual {v0, v1}, Lgiv;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
