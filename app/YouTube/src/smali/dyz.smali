.class public final Ldyz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbt;
.implements Ldzo;
.implements Ldzw;


# static fields
.field private static synthetic n:Z


# instance fields
.field public final a:Ldzs;

.field final b:Ljava/io/InputStream;

.field final c:Ljava/io/OutputStream;

.field volatile d:I

.field volatile e:I

.field volatile f:Z

.field private final g:Ldzr;

.field private volatile h:Ldyp;

.field private final i:Ldyi;

.field private final j:Ldyi;

.field private k:Z

.field private final l:Ljava/nio/ByteBuffer;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const-class v0, Ldyz;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ldyz;->n:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ldzs;Ldzr;Ldyp;I)V
    .locals 6

    .prologue
    .line 138
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Ldyz;-><init>(Ldzs;Ldzr;Ldyp;I[B)V

    .line 139
    return-void
.end method

.method private constructor <init>(Ldzs;Ldzr;Ldyp;I[B)V
    .locals 4

    .prologue
    const v3, 0x7fffffff

    const/16 v2, 0x400

    const/4 v1, 0x0

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 689
    new-instance v0, Ldyi;

    invoke-direct {v0}, Ldyi;-><init>()V

    iput-object v0, p0, Ldyz;->i:Ldyi;

    .line 692
    new-instance v0, Ldyi;

    invoke-direct {v0}, Ldyi;-><init>()V

    iput-object v0, p0, Ldyz;->j:Ldyi;

    .line 705
    iput-boolean v1, p0, Ldyz;->k:Z

    .line 713
    iput v3, p0, Ldyz;->d:I

    .line 717
    iput v3, p0, Ldyz;->e:I

    .line 722
    iput v1, p0, Ldyz;->m:I

    .line 726
    iput-boolean v1, p0, Ldyz;->f:Z

    .line 162
    if-nez p4, :cond_0

    .line 163
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "mode cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    if-nez p1, :cond_1

    .line 166
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_1
    if-nez p2, :cond_2

    .line 169
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "eventRegistry cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_2
    if-nez p3, :cond_3

    .line 172
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_3
    invoke-virtual {p1}, Ldzs;->isBlocking()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 175
    new-instance v0, Ljava/nio/channels/IllegalBlockingModeException;

    invoke-direct {v0}, Ljava/nio/channels/IllegalBlockingModeException;-><init>()V

    throw v0

    .line 177
    :cond_4
    iget-object v0, p1, Ldzs;->a:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    move-result v0

    if-nez v0, :cond_5

    .line 178
    new-instance v0, Ljava/nio/channels/NotYetConnectedException;

    invoke-direct {v0}, Ljava/nio/channels/NotYetConnectedException;-><init>()V

    throw v0

    .line 180
    :cond_5
    iput-object p1, p0, Ldyz;->a:Ldzs;

    .line 183
    iput-object p2, p0, Ldyz;->g:Ldzr;

    .line 184
    iput-object p3, p0, Ldyz;->h:Ldyp;

    .line 185
    new-instance v0, Ldyj;

    iget-object v1, p0, Ldyz;->i:Ldyi;

    invoke-direct {v0, v1}, Ldyj;-><init>(Ldyi;)V

    iput-object v0, p0, Ldyz;->b:Ljava/io/InputStream;

    .line 186
    new-instance v0, Ldyk;

    iget-object v1, p0, Ldyz;->j:Ldyi;

    invoke-direct {v0, v1}, Ldyk;-><init>(Ldyi;)V

    iput-object v0, p0, Ldyz;->c:Ljava/io/OutputStream;

    .line 191
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Ldyz;->l:Ljava/nio/ByteBuffer;

    .line 193
    iget-object v0, p0, Ldyz;->l:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 196
    :try_start_0
    iget-object v0, p0, Ldyz;->a:Ldzs;

    iget-object v0, v0, Ldzs;->a:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setTcpNoDelay(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :goto_0
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    iget-object v1, p0, Ldyz;->h:Ldyp;

    iget-object v2, p0, Ldyz;->g:Ldzr;

    invoke-static {v1, v0, v2}, Ldyq;->a(Ldyp;Ljava/lang/Exception;Ldzr;)V

    goto :goto_0
.end method

.method private a(Ljava/nio/ByteBuffer;I)I
    .locals 2

    .prologue
    .line 656
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-gt v0, p2, :cond_0

    .line 657
    iget-object v0, p0, Ldyz;->a:Ldzs;

    invoke-virtual {v0, p1}, Ldzs;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 667
    :goto_0
    return v0

    .line 659
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    .line 660
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 662
    :try_start_0
    iget-object v0, p0, Ldyz;->a:Ldzs;

    invoke-virtual {v0, p1}, Ldzs;->a(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 664
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    throw v0
.end method

.method private static a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/ByteBuffer;I)I
    .locals 2

    .prologue
    .line 585
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-gt v0, p2, :cond_0

    .line 586
    invoke-interface {p0, p1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 593
    :goto_0
    return v0

    .line 588
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    .line 590
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 591
    invoke-interface {p0, p1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 593
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Ldyz;->g:Ldzr;

    iget-object v1, p0, Ldyz;->a:Ldzs;

    invoke-interface {v0, v1, p0}, Ldzr;->a(Ljava/nio/channels/SelectableChannel;Ldzo;)V

    .line 377
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 391
    monitor-enter p0

    .line 394
    :try_start_0
    iget-object v0, p0, Ldyz;->j:Ldyi;

    invoke-virtual {v0}, Ldyi;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    :try_start_1
    iget-boolean v0, p0, Ldyz;->k:Z

    if-nez v0, :cond_0

    .line 403
    iget-object v0, p0, Ldyz;->g:Ldzr;

    iget-object v1, p0, Ldyz;->a:Ldzs;

    invoke-interface {v0, v1, p0}, Ldzr;->a(Ljava/nio/channels/SelectableChannel;Ldzw;)V

    .line 406
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldyz;->k:Z

    .line 408
    :cond_0
    monitor-exit p0

    :goto_0
    return-void

    .line 395
    :catch_0
    move-exception v0

    .line 396
    iget-object v1, p0, Ldyz;->h:Ldyp;

    iget-object v2, p0, Ldyz;->g:Ldzr;

    invoke-static {v1, v0, v2}, Ldyq;->a(Ldyp;Ljava/lang/Exception;Ldzr;)V

    .line 397
    monitor-exit p0

    goto :goto_0

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final f()V
    .locals 8

    .prologue
    const/4 v0, -0x1

    .line 462
    :try_start_0
    iget-object v4, p0, Ldyz;->a:Ldzs;

    iget-object v5, p0, Ldyz;->i:Ldyi;

    iget v3, p0, Ldyz;->d:I

    sget-boolean v1, Ldyz;->n:Z

    if-nez v1, :cond_0

    if-gez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    :catch_0
    move-exception v0

    .line 472
    iget-object v1, p0, Ldyz;->h:Ldyp;

    invoke-interface {v1, v0}, Ldyp;->a(Ljava/lang/Exception;)V

    .line 474
    :goto_0
    return-void

    .line 462
    :cond_0
    :try_start_1
    invoke-interface {v4}, Ljava/nio/channels/ReadableByteChannel;->isOpen()Z

    move-result v1

    if-nez v1, :cond_2

    .line 463
    :cond_1
    :goto_1
    if-gez v0, :cond_a

    .line 466
    iget-object v0, p0, Ldyz;->g:Ldzr;

    iget-object v1, p0, Ldyz;->a:Ldzs;

    invoke-interface {v0, v1}, Ldzr;->b(Ljava/nio/channels/SelectableChannel;)V

    .line 467
    iget-object v0, p0, Ldyz;->h:Ldyp;

    invoke-interface {v0}, Ldyp;->a()V

    goto :goto_0

    .line 462
    :cond_2
    const/4 v1, 0x0

    move v2, v3

    :cond_3
    invoke-virtual {v5}, Ldyi;->c()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-static {v4, v6, v2}, Ldyz;->a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/ByteBuffer;I)I

    move-result v6

    sget-boolean v7, Ldyz;->n:Z

    if-nez v7, :cond_4

    if-le v6, v2, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    if-lez v6, :cond_9

    sub-int/2addr v2, v6

    :cond_5
    :goto_2
    if-lez v2, :cond_6

    if-gtz v6, :cond_3

    :cond_6
    if-eqz v1, :cond_7

    if-eq v2, v3, :cond_1

    :cond_7
    if-ge v2, v3, :cond_8

    invoke-virtual {v5}, Ldyi;->a()V

    :cond_8
    sub-int v0, v3, v2

    goto :goto_1

    :cond_9
    if-gez v6, :cond_5

    const/4 v1, 0x1

    goto :goto_2

    .line 470
    :cond_a
    iget-object v1, p0, Ldyz;->h:Ldyp;

    invoke-interface {v1, v0}, Ldyp;->a(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final g()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 483
    :try_start_0
    sget-boolean v1, Ldyz;->n:Z

    if-nez v1, :cond_1

    iget v1, p0, Ldyz;->e:I

    if-gez v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 506
    :catch_0
    move-exception v0

    .line 507
    iget-object v1, p0, Ldyz;->h:Ldyp;

    invoke-interface {v1, v0}, Ldyp;->a(Ljava/lang/Exception;)V

    .line 509
    :cond_0
    :goto_0
    return-void

    .line 483
    :cond_1
    :try_start_1
    iget v3, p0, Ldyz;->e:I

    iget-object v1, p0, Ldyz;->a:Ldzs;

    invoke-virtual {v1}, Ldzs;->isOpen()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v0

    .line 484
    :goto_1
    if-gez v1, :cond_9

    .line 485
    new-instance v0, Ldzf;

    const-string v1, "-1 returned when writing to channel"

    invoke-direct {v0, v1}, Ldzf;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v2, v3

    .line 483
    :cond_3
    iget-object v1, p0, Ldyz;->l:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Ldyz;->l:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v1, v2}, Ldyz;->a(Ljava/nio/ByteBuffer;I)I

    move-result v1

    :goto_2
    sub-int/2addr v2, v1

    if-lez v2, :cond_4

    if-gtz v1, :cond_3

    :cond_4
    sub-int v1, v3, v2

    goto :goto_1

    :cond_5
    iget-object v1, p0, Ldyz;->j:Ldyi;

    invoke-virtual {v1}, Ldyi;->d()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-nez v4, :cond_7

    :cond_6
    move v1, v0

    goto :goto_2

    :cond_7
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    const/16 v5, 0x400

    if-le v4, v5, :cond_8

    invoke-direct {p0, v1, v2}, Ldyz;->a(Ljava/nio/ByteBuffer;I)I

    move-result v1

    goto :goto_2

    :cond_8
    iget-object v1, p0, Ldyz;->l:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v1, p0, Ldyz;->j:Ldyi;

    iget-object v4, p0, Ldyz;->l:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v4}, Ldyi;->a(Ljava/nio/ByteBuffer;)I

    iget-object v1, p0, Ldyz;->l:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v1, p0, Ldyz;->l:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v1, v2}, Ldyz;->a(Ljava/nio/ByteBuffer;I)I

    move-result v1

    goto :goto_2

    .line 487
    :cond_9
    iget v2, p0, Ldyz;->m:I

    add-int/2addr v1, v2

    iput v1, p0, Ldyz;->m:I

    .line 489
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 490
    :try_start_2
    iget-object v1, p0, Ldyz;->l:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Ldyz;->j:Ldyi;

    .line 491
    invoke-virtual {v1}, Ldyi;->b()I

    move-result v1

    if-nez v1, :cond_a

    .line 495
    iget-object v0, p0, Ldyz;->g:Ldzr;

    iget-object v1, p0, Ldyz;->a:Ldzs;

    invoke-interface {v0, v1}, Ldzr;->c(Ljava/nio/channels/SelectableChannel;)V

    .line 496
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldyz;->k:Z

    .line 498
    const/4 v0, 0x1

    .line 500
    :cond_a
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 501
    :try_start_3
    iget-boolean v1, p0, Ldyz;->f:Z

    if-nez v1, :cond_b

    if-eqz v0, :cond_0

    .line 504
    :cond_b
    iget v1, p0, Ldyz;->m:I

    const/4 v2, 0x0

    iput v2, p0, Ldyz;->m:I

    iget-object v2, p0, Ldyz;->h:Ldyp;

    invoke-interface {v2, v1, v0}, Ldyp;->a(IZ)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 500
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 273
    const-string v1, "SocketChannel = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 274
    iget-object v1, p0, Ldyz;->a:Ldzs;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 275
    const-string v1, "; Input buffer = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 276
    iget-object v1, p0, Ldyz;->i:Ldyi;

    invoke-virtual {v1}, Ldyi;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 277
    const-string v1, "; Output buffer = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 278
    iget-object v1, p0, Ldyz;->j:Ldyi;

    invoke-virtual {v1}, Ldyi;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 279
    const-string v1, "; Maximum size per read = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 280
    iget v1, p0, Ldyz;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 281
    const-string v1, "; Maximum size per flush = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 282
    iget v1, p0, Ldyz;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 283
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
