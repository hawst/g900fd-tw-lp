.class final Lecg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lecf;


# instance fields
.field private final a:Landroid/media/AudioTimestamp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 680
    new-instance v0, Landroid/media/AudioTimestamp;

    invoke-direct {v0}, Landroid/media/AudioTimestamp;-><init>()V

    iput-object v0, p0, Lecg;->a:Landroid/media/AudioTimestamp;

    .line 681
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 690
    iget-object v0, p0, Lecg;->a:Landroid/media/AudioTimestamp;

    iget-wide v0, v0, Landroid/media/AudioTimestamp;->nanoTime:J

    return-wide v0
.end method

.method public final a(Landroid/media/AudioTrack;)Z
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lecg;->a:Landroid/media/AudioTimestamp;

    invoke-virtual {p1, v0}, Landroid/media/AudioTrack;->getTimestamp(Landroid/media/AudioTimestamp;)Z

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 695
    iget-object v0, p0, Lecg;->a:Landroid/media/AudioTimestamp;

    iget-wide v0, v0, Landroid/media/AudioTimestamp;->framePosition:J

    return-wide v0
.end method
