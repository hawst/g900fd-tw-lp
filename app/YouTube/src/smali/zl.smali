.class public final Lzl;
.super Lyi;
.source "SourceFile"


# instance fields
.field private a:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "smhd"

    invoke-direct {p0, v0}, Lyi;-><init>(Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lzl;->c(Ljava/nio/ByteBuffer;)J

    .line 44
    invoke-static {p1}, La;->h(Ljava/nio/ByteBuffer;)F

    move-result v0

    iput v0, p0, Lzl;->a:F

    .line 45
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    .line 46
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lzl;->d(Ljava/nio/ByteBuffer;)V

    .line 51
    iget v0, p0, Lzl;->a:F

    float-to-double v0, v0

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;D)V

    .line 52
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 53
    return-void
.end method

.method protected final d_()J
    .locals 2

    .prologue
    .line 38
    const-wide/16 v0, 0x8

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 56
    iget v0, p0, Lzl;->a:F

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "SoundMediaHeaderBox[balance="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
