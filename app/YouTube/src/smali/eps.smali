.class public final Leps;
.super Lekr;


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Lejb;

.field public final e:Ljava/lang/Object;

.field public f:Z

.field private final g:Lepo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lejb;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p2, v0}, Lekr;-><init>(Landroid/content/Context;Lejb;Lejc;[Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leps;->c:Ljava/lang/String;

    invoke-static {p2}, Lb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejb;

    iput-object v0, p0, Leps;->d:Lejb;

    iget-object v0, p0, Leps;->d:Lejb;

    invoke-virtual {v0, p0}, Lejb;->a(Leps;)V

    new-instance v0, Lepo;

    invoke-direct {v0}, Lepo;-><init>()V

    iput-object v0, p0, Leps;->g:Lepo;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Leps;->e:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Leps;->f:Z

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lepm;->a(Landroid/os/IBinder;)Lepl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    .locals 3

    iget-object v1, p0, Leps;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Leps;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Leps;->b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Leps;->j()V

    invoke-virtual {p0}, Leps;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lepl;

    iget-object v2, p0, Leps;->c:Ljava/lang/String;

    invoke-interface {v0, v2, p1, p2}, Lepl;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Couldn\'t send log event.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, p2}, Leps;->b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Service was disconnected.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, p2}, Leps;->b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method protected final a(Lelk;Lekv;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5e3530

    iget-object v2, p0, Lekr;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lelk;->f(Lelh;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v1, p0, Leps;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Leps;->f:Z

    iput-boolean p1, p0, Leps;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Leps;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Leps;->j()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    .locals 3

    iget-object v0, p0, Leps;->g:Lepo;

    iget-object v1, v0, Lepo;->a:Ljava/util/ArrayList;

    new-instance v2, Lepp;

    invoke-direct {v2, p1, p2}, Lepp;-><init>(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, v0, Lepo;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lepo;->b:I

    if-le v1, v2, :cond_0

    iget-object v1, v0, Lepo;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.service.START"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.internal.IPlayLogService"

    return-object v0
.end method

.method public j()V
    .locals 6

    iget-boolean v0, p0, Leps;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Leps;->g:Lepo;

    iget-object v0, v0, Lepo;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Leps;->g:Lepo;

    iget-object v1, v1, Lepo;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepp;

    iget-object v1, v0, Lepp;->a:Lcom/google/android/gms/internal/qu;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/qu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, v0, Lepp;->b:Lcom/google/android/gms/internal/qq;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "PlayLoggerImpl"

    const-string v1, "Couldn\'t send cached log events to AndroidLog service.  Retaining in memory cache."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_2
    return-void

    :cond_3
    :try_start_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Leps;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lepl;

    iget-object v5, p0, Leps;->c:Ljava/lang/String;

    invoke-interface {v1, v5, v2, v3}, Lepl;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;Ljava/util/List;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    :cond_4
    iget-object v1, v0, Lepp;->a:Lcom/google/android/gms/internal/qu;

    iget-object v0, v0, Lepp;->b:Lcom/google/android/gms/internal/qq;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    goto :goto_1

    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Leps;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lepl;

    iget-object v1, p0, Leps;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lepl;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;Ljava/util/List;)V

    :cond_6
    iget-object v0, p0, Leps;->g:Lepo;

    iget-object v0, v0, Lepo;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
