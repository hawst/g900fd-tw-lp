.class public final Lhbw;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhbw;


# instance fields
.field public b:J

.field public c:J

.field public d:J

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lhog;

.field public h:Lhxf;

.field public i:Lhrr;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Lhqy;

.field private m:[B

.field private n:[Lhls;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17703
    const/4 v0, 0x0

    new-array v0, v0, [Lhbw;

    sput-object v0, Lhbw;->a:[Lhbw;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 17704
    invoke-direct {p0}, Lidf;-><init>()V

    .line 17707
    iput-wide v2, p0, Lhbw;->b:J

    .line 17710
    iput-wide v2, p0, Lhbw;->c:J

    .line 17713
    iput-wide v2, p0, Lhbw;->d:J

    .line 17716
    const-string v0, ""

    iput-object v0, p0, Lhbw;->e:Ljava/lang/String;

    .line 17719
    const-string v0, ""

    iput-object v0, p0, Lhbw;->j:Ljava/lang/String;

    .line 17722
    const-string v0, ""

    iput-object v0, p0, Lhbw;->k:Ljava/lang/String;

    .line 17725
    const-string v0, ""

    iput-object v0, p0, Lhbw;->f:Ljava/lang/String;

    .line 17728
    iput-object v1, p0, Lhbw;->g:Lhog;

    .line 17731
    iput-object v1, p0, Lhbw;->h:Lhxf;

    .line 17734
    iput-object v1, p0, Lhbw;->l:Lhqy;

    .line 17737
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhbw;->m:[B

    .line 17740
    iput-object v1, p0, Lhbw;->i:Lhrr;

    .line 17743
    sget-object v0, Lhls;->a:[Lhls;

    iput-object v0, p0, Lhbw;->n:[Lhls;

    .line 17704
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    .line 17866
    .line 17867
    iget-wide v2, p0, Lhbw;->b:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_d

    .line 17868
    const/4 v0, 0x1

    iget-wide v2, p0, Lhbw;->b:J

    .line 17869
    invoke-static {v0, v2, v3}, Lidd;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17871
    :goto_0
    iget-wide v2, p0, Lhbw;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 17872
    const/4 v2, 0x2

    iget-wide v4, p0, Lhbw;->c:J

    .line 17873
    invoke-static {v2, v4, v5}, Lidd;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 17875
    :cond_0
    iget-wide v2, p0, Lhbw;->d:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 17876
    const/4 v2, 0x3

    iget-wide v4, p0, Lhbw;->d:J

    .line 17877
    invoke-static {v2, v4, v5}, Lidd;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 17879
    :cond_1
    iget-object v2, p0, Lhbw;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 17880
    const/4 v2, 0x4

    iget-object v3, p0, Lhbw;->e:Ljava/lang/String;

    .line 17881
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17883
    :cond_2
    iget-object v2, p0, Lhbw;->j:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 17884
    const/4 v2, 0x5

    iget-object v3, p0, Lhbw;->j:Ljava/lang/String;

    .line 17885
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17887
    :cond_3
    iget-object v2, p0, Lhbw;->k:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 17888
    const/4 v2, 0x6

    iget-object v3, p0, Lhbw;->k:Ljava/lang/String;

    .line 17889
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17891
    :cond_4
    iget-object v2, p0, Lhbw;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 17892
    const/4 v2, 0x7

    iget-object v3, p0, Lhbw;->f:Ljava/lang/String;

    .line 17893
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17895
    :cond_5
    iget-object v2, p0, Lhbw;->g:Lhog;

    if-eqz v2, :cond_6

    .line 17896
    const/16 v2, 0x8

    iget-object v3, p0, Lhbw;->g:Lhog;

    .line 17897
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17899
    :cond_6
    iget-object v2, p0, Lhbw;->h:Lhxf;

    if-eqz v2, :cond_7

    .line 17900
    const/16 v2, 0x9

    iget-object v3, p0, Lhbw;->h:Lhxf;

    .line 17901
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17903
    :cond_7
    iget-object v2, p0, Lhbw;->l:Lhqy;

    if-eqz v2, :cond_8

    .line 17904
    const/16 v2, 0xa

    iget-object v3, p0, Lhbw;->l:Lhqy;

    .line 17905
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17907
    :cond_8
    iget-object v2, p0, Lhbw;->m:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_9

    .line 17908
    const/16 v2, 0xc

    iget-object v3, p0, Lhbw;->m:[B

    .line 17909
    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 17911
    :cond_9
    iget-object v2, p0, Lhbw;->i:Lhrr;

    if-eqz v2, :cond_a

    .line 17912
    const/16 v2, 0xd

    iget-object v3, p0, Lhbw;->i:Lhrr;

    .line 17913
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 17915
    :cond_a
    iget-object v2, p0, Lhbw;->n:[Lhls;

    if-eqz v2, :cond_c

    .line 17916
    iget-object v2, p0, Lhbw;->n:[Lhls;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 17917
    if-eqz v4, :cond_b

    .line 17918
    const/16 v5, 0xe

    .line 17919
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 17916
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 17923
    :cond_c
    iget-object v1, p0, Lhbw;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17924
    iput v0, p0, Lhbw;->J:I

    .line 17925
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 17700
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhbw;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhbw;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhbw;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lhbw;->b:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lhbw;->c:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lhbw;->d:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhbw;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhbw;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhbw;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhbw;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lhbw;->g:Lhog;

    if-nez v0, :cond_2

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhbw;->g:Lhog;

    :cond_2
    iget-object v0, p0, Lhbw;->g:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lhbw;->h:Lhxf;

    if-nez v0, :cond_3

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhbw;->h:Lhxf;

    :cond_3
    iget-object v0, p0, Lhbw;->h:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lhbw;->l:Lhqy;

    if-nez v0, :cond_4

    new-instance v0, Lhqy;

    invoke-direct {v0}, Lhqy;-><init>()V

    iput-object v0, p0, Lhbw;->l:Lhqy;

    :cond_4
    iget-object v0, p0, Lhbw;->l:Lhqy;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhbw;->m:[B

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lhbw;->i:Lhrr;

    if-nez v0, :cond_5

    new-instance v0, Lhrr;

    invoke-direct {v0}, Lhrr;-><init>()V

    iput-object v0, p0, Lhbw;->i:Lhrr;

    :cond_5
    iget-object v0, p0, Lhbw;->i:Lhrr;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    const/16 v0, 0x72

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhbw;->n:[Lhls;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhls;

    iget-object v3, p0, Lhbw;->n:[Lhls;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhbw;->n:[Lhls;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhbw;->n:[Lhls;

    :goto_2
    iget-object v2, p0, Lhbw;->n:[Lhls;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhbw;->n:[Lhls;

    new-instance v3, Lhls;

    invoke-direct {v3}, Lhls;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhbw;->n:[Lhls;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lhbw;->n:[Lhls;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Lhbw;->n:[Lhls;

    new-instance v3, Lhls;

    invoke-direct {v3}, Lhls;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhbw;->n:[Lhls;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 17817
    iget-wide v0, p0, Lhbw;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 17818
    const/4 v0, 0x1

    iget-wide v2, p0, Lhbw;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 17820
    :cond_0
    iget-wide v0, p0, Lhbw;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 17821
    const/4 v0, 0x2

    iget-wide v2, p0, Lhbw;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 17823
    :cond_1
    iget-wide v0, p0, Lhbw;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 17824
    const/4 v0, 0x3

    iget-wide v2, p0, Lhbw;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 17826
    :cond_2
    iget-object v0, p0, Lhbw;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 17827
    const/4 v0, 0x4

    iget-object v1, p0, Lhbw;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 17829
    :cond_3
    iget-object v0, p0, Lhbw;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 17830
    const/4 v0, 0x5

    iget-object v1, p0, Lhbw;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 17832
    :cond_4
    iget-object v0, p0, Lhbw;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 17833
    const/4 v0, 0x6

    iget-object v1, p0, Lhbw;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 17835
    :cond_5
    iget-object v0, p0, Lhbw;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 17836
    const/4 v0, 0x7

    iget-object v1, p0, Lhbw;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 17838
    :cond_6
    iget-object v0, p0, Lhbw;->g:Lhog;

    if-eqz v0, :cond_7

    .line 17839
    const/16 v0, 0x8

    iget-object v1, p0, Lhbw;->g:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 17841
    :cond_7
    iget-object v0, p0, Lhbw;->h:Lhxf;

    if-eqz v0, :cond_8

    .line 17842
    const/16 v0, 0x9

    iget-object v1, p0, Lhbw;->h:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 17844
    :cond_8
    iget-object v0, p0, Lhbw;->l:Lhqy;

    if-eqz v0, :cond_9

    .line 17845
    const/16 v0, 0xa

    iget-object v1, p0, Lhbw;->l:Lhqy;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 17847
    :cond_9
    iget-object v0, p0, Lhbw;->m:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_a

    .line 17848
    const/16 v0, 0xc

    iget-object v1, p0, Lhbw;->m:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 17850
    :cond_a
    iget-object v0, p0, Lhbw;->i:Lhrr;

    if-eqz v0, :cond_b

    .line 17851
    const/16 v0, 0xd

    iget-object v1, p0, Lhbw;->i:Lhrr;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 17853
    :cond_b
    iget-object v0, p0, Lhbw;->n:[Lhls;

    if-eqz v0, :cond_d

    .line 17854
    iget-object v1, p0, Lhbw;->n:[Lhls;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_d

    aget-object v3, v1, v0

    .line 17855
    if-eqz v3, :cond_c

    .line 17856
    const/16 v4, 0xe

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 17854
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 17860
    :cond_d
    iget-object v0, p0, Lhbw;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 17862
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 17766
    if-ne p1, p0, :cond_1

    .line 17782
    :cond_0
    :goto_0
    return v0

    .line 17767
    :cond_1
    instance-of v2, p1, Lhbw;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 17768
    :cond_2
    check-cast p1, Lhbw;

    .line 17769
    iget-wide v2, p0, Lhbw;->b:J

    iget-wide v4, p1, Lhbw;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lhbw;->c:J

    iget-wide v4, p1, Lhbw;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lhbw;->d:J

    iget-wide v4, p1, Lhbw;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhbw;->e:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbw;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 17772
    :goto_1
    iget-object v2, p0, Lhbw;->j:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbw;->j:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 17773
    :goto_2
    iget-object v2, p0, Lhbw;->k:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhbw;->k:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 17774
    :goto_3
    iget-object v2, p0, Lhbw;->f:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhbw;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 17775
    :goto_4
    iget-object v2, p0, Lhbw;->g:Lhog;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhbw;->g:Lhog;

    if-nez v2, :cond_3

    .line 17776
    :goto_5
    iget-object v2, p0, Lhbw;->h:Lhxf;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhbw;->h:Lhxf;

    if-nez v2, :cond_3

    .line 17777
    :goto_6
    iget-object v2, p0, Lhbw;->l:Lhqy;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhbw;->l:Lhqy;

    if-nez v2, :cond_3

    .line 17778
    :goto_7
    iget-object v2, p0, Lhbw;->m:[B

    iget-object v3, p1, Lhbw;->m:[B

    .line 17779
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhbw;->i:Lhrr;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhbw;->i:Lhrr;

    if-nez v2, :cond_3

    .line 17780
    :goto_8
    iget-object v2, p0, Lhbw;->n:[Lhls;

    iget-object v3, p1, Lhbw;->n:[Lhls;

    .line 17781
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhbw;->I:Ljava/util/List;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhbw;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 17782
    goto :goto_0

    .line 17769
    :cond_4
    iget-object v2, p0, Lhbw;->e:Ljava/lang/String;

    iget-object v3, p1, Lhbw;->e:Ljava/lang/String;

    .line 17772
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbw;->j:Ljava/lang/String;

    iget-object v3, p1, Lhbw;->j:Ljava/lang/String;

    .line 17773
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhbw;->k:Ljava/lang/String;

    iget-object v3, p1, Lhbw;->k:Ljava/lang/String;

    .line 17774
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhbw;->f:Ljava/lang/String;

    iget-object v3, p1, Lhbw;->f:Ljava/lang/String;

    .line 17775
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhbw;->g:Lhog;

    iget-object v3, p1, Lhbw;->g:Lhog;

    .line 17776
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhbw;->h:Lhxf;

    iget-object v3, p1, Lhbw;->h:Lhxf;

    .line 17777
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhbw;->l:Lhqy;

    iget-object v3, p1, Lhbw;->l:Lhqy;

    .line 17778
    invoke-virtual {v2, v3}, Lhqy;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    .line 17779
    :cond_b
    iget-object v2, p0, Lhbw;->i:Lhrr;

    iget-object v3, p1, Lhbw;->i:Lhrr;

    .line 17780
    invoke-virtual {v2, v3}, Lhrr;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    .line 17781
    :cond_c
    iget-object v2, p0, Lhbw;->I:Ljava/util/List;

    iget-object v3, p1, Lhbw;->I:Ljava/util/List;

    .line 17782
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 17786
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 17788
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhbw;->b:J

    iget-wide v4, p0, Lhbw;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 17789
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhbw;->c:J

    iget-wide v4, p0, Lhbw;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 17790
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhbw;->d:J

    iget-wide v4, p0, Lhbw;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 17791
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbw;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 17792
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbw;->j:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 17793
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbw;->k:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 17794
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbw;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 17795
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbw;->g:Lhog;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 17796
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbw;->h:Lhxf;

    if-nez v0, :cond_7

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 17797
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbw;->l:Lhqy;

    if-nez v0, :cond_8

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 17798
    iget-object v2, p0, Lhbw;->m:[B

    if-nez v2, :cond_9

    mul-int/lit8 v2, v0, 0x1f

    .line 17804
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhbw;->i:Lhrr;

    if-nez v0, :cond_a

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 17805
    iget-object v2, p0, Lhbw;->n:[Lhls;

    if-nez v2, :cond_b

    mul-int/lit8 v2, v0, 0x1f

    .line 17811
    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhbw;->I:Ljava/util/List;

    if-nez v2, :cond_d

    :goto_8
    add-int/2addr v0, v1

    .line 17812
    return v0

    .line 17791
    :cond_2
    iget-object v0, p0, Lhbw;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 17792
    :cond_3
    iget-object v0, p0, Lhbw;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 17793
    :cond_4
    iget-object v0, p0, Lhbw;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 17794
    :cond_5
    iget-object v0, p0, Lhbw;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 17795
    :cond_6
    iget-object v0, p0, Lhbw;->g:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_4

    .line 17796
    :cond_7
    iget-object v0, p0, Lhbw;->h:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_5

    .line 17797
    :cond_8
    iget-object v0, p0, Lhbw;->l:Lhqy;

    invoke-virtual {v0}, Lhqy;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_9
    move v2, v0

    move v0, v1

    .line 17800
    :goto_9
    iget-object v3, p0, Lhbw;->m:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 17801
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhbw;->m:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 17800
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 17804
    :cond_a
    iget-object v0, p0, Lhbw;->i:Lhrr;

    invoke-virtual {v0}, Lhrr;->hashCode()I

    move-result v0

    goto :goto_7

    :cond_b
    move v2, v0

    move v0, v1

    .line 17807
    :goto_a
    iget-object v3, p0, Lhbw;->n:[Lhls;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 17808
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhbw;->n:[Lhls;

    aget-object v2, v2, v0

    if-nez v2, :cond_c

    move v2, v1

    :goto_b
    add-int/2addr v2, v3

    .line 17807
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 17808
    :cond_c
    iget-object v2, p0, Lhbw;->n:[Lhls;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhls;->hashCode()I

    move-result v2

    goto :goto_b

    .line 17811
    :cond_d
    iget-object v1, p0, Lhbw;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_8
.end method
