.class public final Lhgq;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lhgs;

.field public c:[Lgyj;

.field private d:[Lhbj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52382
    invoke-direct {p0}, Lidf;-><init>()V

    .line 52385
    const-string v0, ""

    iput-object v0, p0, Lhgq;->a:Ljava/lang/String;

    .line 52388
    const/4 v0, 0x0

    iput-object v0, p0, Lhgq;->b:Lhgs;

    .line 52391
    sget-object v0, Lhbj;->a:[Lhbj;

    iput-object v0, p0, Lhgq;->d:[Lhbj;

    .line 52394
    sget-object v0, Lgyj;->a:[Lgyj;

    iput-object v0, p0, Lhgq;->c:[Lgyj;

    .line 52382
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 52467
    .line 52468
    iget-object v0, p0, Lhgq;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 52469
    const/4 v0, 0x1

    iget-object v2, p0, Lhgq;->a:Ljava/lang/String;

    .line 52470
    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 52472
    :goto_0
    iget-object v2, p0, Lhgq;->b:Lhgs;

    if-eqz v2, :cond_0

    .line 52473
    const/4 v2, 0x2

    iget-object v3, p0, Lhgq;->b:Lhgs;

    .line 52474
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 52476
    :cond_0
    iget-object v2, p0, Lhgq;->d:[Lhbj;

    if-eqz v2, :cond_2

    .line 52477
    iget-object v3, p0, Lhgq;->d:[Lhbj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 52478
    if-eqz v5, :cond_1

    .line 52479
    const/4 v6, 0x3

    .line 52480
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 52477
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 52484
    :cond_2
    iget-object v2, p0, Lhgq;->c:[Lgyj;

    if-eqz v2, :cond_4

    .line 52485
    iget-object v2, p0, Lhgq;->c:[Lgyj;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 52486
    if-eqz v4, :cond_3

    .line 52487
    const/4 v5, 0x4

    .line 52488
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 52485
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 52492
    :cond_4
    iget-object v1, p0, Lhgq;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52493
    iput v0, p0, Lhgq;->J:I

    .line 52494
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 52378
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhgq;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhgq;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhgq;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgq;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhgq;->b:Lhgs;

    if-nez v0, :cond_2

    new-instance v0, Lhgs;

    invoke-direct {v0}, Lhgs;-><init>()V

    iput-object v0, p0, Lhgq;->b:Lhgs;

    :cond_2
    iget-object v0, p0, Lhgq;->b:Lhgs;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhgq;->d:[Lhbj;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbj;

    iget-object v3, p0, Lhgq;->d:[Lhbj;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhgq;->d:[Lhbj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhgq;->d:[Lhbj;

    :goto_2
    iget-object v2, p0, Lhgq;->d:[Lhbj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhgq;->d:[Lhbj;

    new-instance v3, Lhbj;

    invoke-direct {v3}, Lhbj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgq;->d:[Lhbj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhgq;->d:[Lhbj;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhgq;->d:[Lhbj;

    new-instance v3, Lhbj;

    invoke-direct {v3}, Lhbj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgq;->d:[Lhbj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhgq;->c:[Lgyj;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgyj;

    iget-object v3, p0, Lhgq;->c:[Lgyj;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhgq;->c:[Lgyj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhgq;->c:[Lgyj;

    :goto_4
    iget-object v2, p0, Lhgq;->c:[Lgyj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhgq;->c:[Lgyj;

    new-instance v3, Lgyj;

    invoke-direct {v3}, Lgyj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgq;->c:[Lgyj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lhgq;->c:[Lgyj;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lhgq;->c:[Lgyj;

    new-instance v3, Lgyj;

    invoke-direct {v3}, Lgyj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgq;->c:[Lgyj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 52441
    iget-object v1, p0, Lhgq;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52442
    const/4 v1, 0x1

    iget-object v2, p0, Lhgq;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    .line 52444
    :cond_0
    iget-object v1, p0, Lhgq;->b:Lhgs;

    if-eqz v1, :cond_1

    .line 52445
    const/4 v1, 0x2

    iget-object v2, p0, Lhgq;->b:Lhgs;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 52447
    :cond_1
    iget-object v1, p0, Lhgq;->d:[Lhbj;

    if-eqz v1, :cond_3

    .line 52448
    iget-object v2, p0, Lhgq;->d:[Lhbj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 52449
    if-eqz v4, :cond_2

    .line 52450
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 52448
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 52454
    :cond_3
    iget-object v1, p0, Lhgq;->c:[Lgyj;

    if-eqz v1, :cond_5

    .line 52455
    iget-object v1, p0, Lhgq;->c:[Lgyj;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 52456
    if-eqz v3, :cond_4

    .line 52457
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 52455
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 52461
    :cond_5
    iget-object v0, p0, Lhgq;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 52463
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52408
    if-ne p1, p0, :cond_1

    .line 52415
    :cond_0
    :goto_0
    return v0

    .line 52409
    :cond_1
    instance-of v2, p1, Lhgq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 52410
    :cond_2
    check-cast p1, Lhgq;

    .line 52411
    iget-object v2, p0, Lhgq;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgq;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhgq;->b:Lhgs;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhgq;->b:Lhgs;

    if-nez v2, :cond_3

    .line 52412
    :goto_2
    iget-object v2, p0, Lhgq;->d:[Lhbj;

    iget-object v3, p1, Lhgq;->d:[Lhbj;

    .line 52413
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhgq;->c:[Lgyj;

    iget-object v3, p1, Lhgq;->c:[Lgyj;

    .line 52414
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhgq;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhgq;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 52415
    goto :goto_0

    .line 52411
    :cond_4
    iget-object v2, p0, Lhgq;->a:Ljava/lang/String;

    iget-object v3, p1, Lhgq;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhgq;->b:Lhgs;

    iget-object v3, p1, Lhgq;->b:Lhgs;

    .line 52412
    invoke-virtual {v2, v3}, Lhgs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 52414
    :cond_6
    iget-object v2, p0, Lhgq;->I:Ljava/util/List;

    iget-object v3, p1, Lhgq;->I:Ljava/util/List;

    .line 52415
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 52419
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 52421
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgq;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 52422
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgq;->b:Lhgs;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 52423
    iget-object v2, p0, Lhgq;->d:[Lhbj;

    if-nez v2, :cond_4

    mul-int/lit8 v2, v0, 0x1f

    .line 52429
    :cond_0
    iget-object v0, p0, Lhgq;->c:[Lgyj;

    if-nez v0, :cond_6

    mul-int/lit8 v2, v2, 0x1f

    .line 52435
    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhgq;->I:Ljava/util/List;

    if-nez v2, :cond_8

    :goto_2
    add-int/2addr v0, v1

    .line 52436
    return v0

    .line 52421
    :cond_2
    iget-object v0, p0, Lhgq;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 52422
    :cond_3
    iget-object v0, p0, Lhgq;->b:Lhgs;

    invoke-virtual {v0}, Lhgs;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    move v2, v0

    move v0, v1

    .line 52425
    :goto_3
    iget-object v3, p0, Lhgq;->d:[Lhbj;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 52426
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhgq;->d:[Lhbj;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    .line 52425
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 52426
    :cond_5
    iget-object v2, p0, Lhgq;->d:[Lhbj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbj;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_6
    move v0, v1

    .line 52431
    :goto_5
    iget-object v3, p0, Lhgq;->c:[Lgyj;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 52432
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhgq;->c:[Lgyj;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v1

    :goto_6
    add-int/2addr v2, v3

    .line 52431
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 52432
    :cond_7
    iget-object v2, p0, Lhgq;->c:[Lgyj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgyj;->hashCode()I

    move-result v2

    goto :goto_6

    .line 52435
    :cond_8
    iget-object v1, p0, Lhgq;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
