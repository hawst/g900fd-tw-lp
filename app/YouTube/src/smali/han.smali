.class public final Lhan;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhgz;

.field public c:Lhxf;

.field public d:F

.field public e:Lhgz;

.field public f:Lhog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11626
    invoke-direct {p0}, Lidf;-><init>()V

    .line 11629
    iput-object v1, p0, Lhan;->a:Lhgz;

    .line 11632
    iput-object v1, p0, Lhan;->b:Lhgz;

    .line 11635
    iput-object v1, p0, Lhan;->c:Lhxf;

    .line 11638
    const/4 v0, 0x0

    iput v0, p0, Lhan;->d:F

    .line 11641
    iput-object v1, p0, Lhan;->e:Lhgz;

    .line 11644
    iput-object v1, p0, Lhan;->f:Lhog;

    .line 11626
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 11711
    const/4 v0, 0x0

    .line 11712
    iget-object v1, p0, Lhan;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 11713
    const/4 v0, 0x1

    iget-object v1, p0, Lhan;->a:Lhgz;

    .line 11714
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11716
    :cond_0
    iget-object v1, p0, Lhan;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 11717
    const/4 v1, 0x2

    iget-object v2, p0, Lhan;->b:Lhgz;

    .line 11718
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11720
    :cond_1
    iget-object v1, p0, Lhan;->c:Lhxf;

    if-eqz v1, :cond_2

    .line 11721
    const/4 v1, 0x3

    iget-object v2, p0, Lhan;->c:Lhxf;

    .line 11722
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11724
    :cond_2
    iget v1, p0, Lhan;->d:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_3

    .line 11725
    const/4 v1, 0x4

    iget v2, p0, Lhan;->d:F

    .line 11726
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 11728
    :cond_3
    iget-object v1, p0, Lhan;->e:Lhgz;

    if-eqz v1, :cond_4

    .line 11729
    const/4 v1, 0x5

    iget-object v2, p0, Lhan;->e:Lhgz;

    .line 11730
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11732
    :cond_4
    iget-object v1, p0, Lhan;->f:Lhog;

    if-eqz v1, :cond_5

    .line 11733
    const/4 v1, 0x6

    iget-object v2, p0, Lhan;->f:Lhog;

    .line 11734
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11736
    :cond_5
    iget-object v1, p0, Lhan;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11737
    iput v0, p0, Lhan;->J:I

    .line 11738
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 11622
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhan;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhan;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhan;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhan;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhan;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhan;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhan;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhan;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lhan;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhan;->c:Lhxf;

    if-nez v0, :cond_4

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhan;->c:Lhxf;

    :cond_4
    iget-object v0, p0, Lhan;->c:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhan;->d:F

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhan;->e:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhan;->e:Lhgz;

    :cond_5
    iget-object v0, p0, Lhan;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhan;->f:Lhog;

    if-nez v0, :cond_6

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhan;->f:Lhog;

    :cond_6
    iget-object v0, p0, Lhan;->f:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 11687
    iget-object v0, p0, Lhan;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 11688
    const/4 v0, 0x1

    iget-object v1, p0, Lhan;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11690
    :cond_0
    iget-object v0, p0, Lhan;->b:Lhgz;

    if-eqz v0, :cond_1

    .line 11691
    const/4 v0, 0x2

    iget-object v1, p0, Lhan;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11693
    :cond_1
    iget-object v0, p0, Lhan;->c:Lhxf;

    if-eqz v0, :cond_2

    .line 11694
    const/4 v0, 0x3

    iget-object v1, p0, Lhan;->c:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11696
    :cond_2
    iget v0, p0, Lhan;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    .line 11697
    const/4 v0, 0x4

    iget v1, p0, Lhan;->d:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    .line 11699
    :cond_3
    iget-object v0, p0, Lhan;->e:Lhgz;

    if-eqz v0, :cond_4

    .line 11700
    const/4 v0, 0x5

    iget-object v1, p0, Lhan;->e:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11702
    :cond_4
    iget-object v0, p0, Lhan;->f:Lhog;

    if-eqz v0, :cond_5

    .line 11703
    const/4 v0, 0x6

    iget-object v1, p0, Lhan;->f:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11705
    :cond_5
    iget-object v0, p0, Lhan;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 11707
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11660
    if-ne p1, p0, :cond_1

    .line 11669
    :cond_0
    :goto_0
    return v0

    .line 11661
    :cond_1
    instance-of v2, p1, Lhan;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 11662
    :cond_2
    check-cast p1, Lhan;

    .line 11663
    iget-object v2, p0, Lhan;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhan;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhan;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhan;->b:Lhgz;

    if-nez v2, :cond_3

    .line 11664
    :goto_2
    iget-object v2, p0, Lhan;->c:Lhxf;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhan;->c:Lhxf;

    if-nez v2, :cond_3

    .line 11665
    :goto_3
    iget v2, p0, Lhan;->d:F

    iget v3, p1, Lhan;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-object v2, p0, Lhan;->e:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhan;->e:Lhgz;

    if-nez v2, :cond_3

    .line 11667
    :goto_4
    iget-object v2, p0, Lhan;->f:Lhog;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhan;->f:Lhog;

    if-nez v2, :cond_3

    .line 11668
    :goto_5
    iget-object v2, p0, Lhan;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhan;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 11669
    goto :goto_0

    .line 11663
    :cond_4
    iget-object v2, p0, Lhan;->a:Lhgz;

    iget-object v3, p1, Lhan;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhan;->b:Lhgz;

    iget-object v3, p1, Lhan;->b:Lhgz;

    .line 11664
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhan;->c:Lhxf;

    iget-object v3, p1, Lhan;->c:Lhxf;

    .line 11665
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhan;->e:Lhgz;

    iget-object v3, p1, Lhan;->e:Lhgz;

    .line 11667
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhan;->f:Lhog;

    iget-object v3, p1, Lhan;->f:Lhog;

    .line 11668
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhan;->I:Ljava/util/List;

    iget-object v3, p1, Lhan;->I:Ljava/util/List;

    .line 11669
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 11673
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 11675
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhan;->a:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 11676
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhan;->b:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 11677
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhan;->c:Lhxf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 11678
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhan;->d:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 11679
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhan;->e:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 11680
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhan;->f:Lhog;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 11681
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhan;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 11682
    return v0

    .line 11675
    :cond_0
    iget-object v0, p0, Lhan;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 11676
    :cond_1
    iget-object v0, p0, Lhan;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 11677
    :cond_2
    iget-object v0, p0, Lhan;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_2

    .line 11679
    :cond_3
    iget-object v0, p0, Lhan;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    .line 11680
    :cond_4
    iget-object v0, p0, Lhan;->f:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_4

    .line 11681
    :cond_5
    iget-object v1, p0, Lhan;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
