.class final Leev;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lees;


# instance fields
.field private synthetic a:Leeu;


# direct methods
.method constructor <init>(Leeu;)V
    .locals 0

    .prologue
    .line 537
    iput-object p1, p0, Leev;->a:Leeu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Leev;->a:Leeu;

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x86 -> :sswitch_2
        0x9f -> :sswitch_1
        0xa3 -> :sswitch_3
        0xae -> :sswitch_0
        0xb0 -> :sswitch_1
        0xb3 -> :sswitch_1
        0xb5 -> :sswitch_4
        0xb7 -> :sswitch_0
        0xba -> :sswitch_1
        0xbb -> :sswitch_0
        0xe0 -> :sswitch_0
        0xe1 -> :sswitch_0
        0xe7 -> :sswitch_1
        0xf1 -> :sswitch_1
        0x4282 -> :sswitch_2
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_1
        0x4489 -> :sswitch_4
        0x63a2 -> :sswitch_3
        0x2ad7b1 -> :sswitch_1
        0x1549a966 -> :sswitch_0
        0x1654ae6b -> :sswitch_0
        0x18538067 -> :sswitch_0
        0x1a45dfa3 -> :sswitch_0
        0x1c53bb6b -> :sswitch_0
        0x1f43b675 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(ID)V
    .locals 4

    .prologue
    .line 564
    iget-object v0, p0, Leev;->a:Leeu;

    sparse-switch p1, :sswitch_data_0

    .line 565
    :goto_0
    return-void

    .line 564
    :sswitch_0
    double-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Leeu;->a(J)J

    move-result-wide v2

    iput-wide v2, v0, Leeu;->h:J

    goto :goto_0

    :sswitch_1
    double-to-int v1, p2

    iput v1, v0, Leeu;->l:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xb5 -> :sswitch_1
        0x4489 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(IJ)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 559
    iget-object v0, p0, Leev;->a:Leeu;

    sparse-switch p1, :sswitch_data_0

    .line 560
    :cond_0
    :goto_0
    return-void

    .line 559
    :sswitch_0
    cmp-long v0, p2, v2

    if-eqz v0, :cond_0

    new-instance v0, Lebx;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "EBMLReadVersion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_1
    cmp-long v0, p2, v2

    if-ltz v0, :cond_1

    const-wide/16 v0, 0x2

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    :cond_1
    new-instance v0, Lebx;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x35

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DocTypeReadVersion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_2
    iput-wide p2, v0, Leeu;->g:J

    goto :goto_0

    :sswitch_3
    long-to-int v1, p2

    iput v1, v0, Leeu;->i:I

    goto :goto_0

    :sswitch_4
    long-to-int v1, p2

    iput v1, v0, Leeu;->j:I

    goto :goto_0

    :sswitch_5
    long-to-int v1, p2

    iput v1, v0, Leeu;->k:I

    goto :goto_0

    :sswitch_6
    iget-object v1, v0, Leeu;->t:Legs;

    invoke-virtual {v0, p2, p3}, Leeu;->a(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Legs;->a(J)V

    goto :goto_0

    :sswitch_7
    iget-object v0, v0, Leeu;->u:Legs;

    invoke-virtual {v0, p2, p3}, Legs;->a(J)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {v0, p2, p3}, Leeu;->a(J)J

    move-result-wide v2

    iput-wide v2, v0, Leeu;->p:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x9f -> :sswitch_5
        0xb0 -> :sswitch_3
        0xb3 -> :sswitch_6
        0xba -> :sswitch_4
        0xe7 -> :sswitch_8
        0xf1 -> :sswitch_7
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_0
        0x2ad7b1 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(IJIJ)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 548
    iget-object v0, p0, Leev;->a:Leeu;

    sparse-switch p1, :sswitch_data_0

    .line 550
    :goto_0
    return-void

    .line 548
    :sswitch_0
    iget-wide v2, v0, Leeu;->e:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-wide v2, v0, Leeu;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    :cond_0
    new-instance v0, Lebx;

    const-string v1, "Multiple Segment elements not supported"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    int-to-long v2, p4

    add-long/2addr v2, p2

    iput-wide v2, v0, Leeu;->e:J

    int-to-long v2, p4

    add-long/2addr v2, p2

    add-long/2addr v2, p5

    iput-wide v2, v0, Leeu;->f:J

    goto :goto_0

    :sswitch_1
    int-to-long v2, p4

    add-long/2addr v2, p5

    iput-wide v2, v0, Leeu;->o:J

    new-instance v1, Legs;

    invoke-direct {v1}, Legs;-><init>()V

    iput-object v1, v0, Leeu;->t:Legs;

    new-instance v1, Legs;

    invoke-direct {v1}, Legs;-><init>()V

    iput-object v1, v0, Leeu;->u:Legs;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x18538067 -> :sswitch_0
        0x1c53bb6b -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 569
    iget-object v0, p0, Leev;->a:Leeu;

    sparse-switch p1, :sswitch_data_0

    .line 570
    :cond_0
    return-void

    .line 569
    :sswitch_0
    const-string v0, "webm"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lebx;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DocType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_1
    const-string v0, "V_VP9"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_VORBIS"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lebx;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CodecID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_data_0
    .sparse-switch
        0x86 -> :sswitch_1
        0x4282 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(IJIILefx;)Z
    .locals 12

    .prologue
    .line 576
    iget-object v4, p0, Leev;->a:Leeu;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    const/4 v2, 0x1

    :goto_1
    return v2

    :sswitch_0
    iget-object v2, v4, Leeu;->c:Leby;

    if-nez v2, :cond_0

    iget v2, v4, Leeu;->d:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v4, Leeu;->d:I

    const/4 v2, 0x0

    goto :goto_1

    :cond_0
    iget-object v2, v4, Leeu;->a:Leet;

    move-object/from16 v0, p6

    invoke-interface {v2, v0}, Leet;->b(Lefx;)J

    iget-object v2, v4, Leeu;->a:Leet;

    iget-object v3, v4, Leeu;->b:[B

    const/4 v5, 0x3

    move-object/from16 v0, p6

    invoke-interface {v2, v0, v3, v5}, Leet;->a(Lefx;[BI)V

    iget-object v2, v4, Leeu;->b:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    shl-int/lit8 v2, v2, 0x8

    iget-object v3, v4, Leeu;->b:[B

    const/4 v5, 0x1

    aget-byte v3, v3, v5

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v4, v2, v3}, Leeu;->a(J)J

    move-result-wide v6

    iget-object v2, v4, Leeu;->b:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_2
    iget-object v3, v4, Leeu;->b:[B

    const/4 v5, 0x2

    aget-byte v3, v3, v5

    and-int/lit8 v3, v3, 0x8

    const/16 v5, 0x8

    if-ne v3, v5, :cond_2

    const/4 v3, 0x1

    :goto_3
    iget-object v5, v4, Leeu;->b:[B

    const/4 v8, 0x2

    aget-byte v5, v5, v8

    and-int/lit8 v5, v5, 0x6

    shr-int/lit8 v5, v5, 0x1

    packed-switch v5, :pswitch_data_0

    new-instance v2, Lebx;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Lacing mode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not supported"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lebx;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_3

    :pswitch_0
    move/from16 v0, p4

    int-to-long v8, v0

    add-long/2addr v8, p2

    move/from16 v0, p5

    int-to-long v10, v0

    add-long/2addr v8, v10

    iget-wide v10, v4, Leeu;->p:J

    add-long/2addr v10, v6

    iput-wide v10, v4, Leeu;->q:J

    iget-object v5, v4, Leeu;->c:Leby;

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_4
    iput v2, v5, Leby;->d:I

    iget-object v2, v4, Leeu;->c:Leby;

    iput-boolean v3, v2, Leby;->f:Z

    iget-object v2, v4, Leeu;->c:Leby;

    iget-wide v10, v4, Leeu;->p:J

    add-long/2addr v6, v10

    iput-wide v6, v2, Leby;->e:J

    iget-object v2, v4, Leeu;->c:Leby;

    iget-object v3, v4, Leeu;->a:Leet;

    invoke-interface {v3}, Leet;->a()J

    move-result-wide v6

    sub-long v6, v8, v6

    long-to-int v3, v6

    iput v3, v2, Leby;->c:I

    iget-object v2, v4, Leeu;->c:Leby;

    iget-object v2, v2, Leby;->b:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_3

    iget-object v2, v4, Leeu;->c:Leby;

    iget-object v2, v2, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    iget-object v3, v4, Leeu;->c:Leby;

    iget v3, v3, Leby;->c:I

    if-ge v2, v3, :cond_4

    :cond_3
    iget-object v2, v4, Leeu;->c:Leby;

    iget-object v3, v4, Leeu;->c:Leby;

    iget v3, v3, Leby;->c:I

    invoke-virtual {v2, v3}, Leby;->a(I)Z

    :cond_4
    iget-object v2, v4, Leeu;->c:Leby;

    iget-object v2, v2, Leby;->b:Ljava/nio/ByteBuffer;

    if-nez v2, :cond_6

    iget-object v2, v4, Leeu;->a:Leet;

    iget-object v3, v4, Leeu;->c:Leby;

    iget v3, v3, Leby;->c:I

    move-object/from16 v0, p6

    invoke-interface {v2, v0, v3}, Leet;->a(Lefx;I)V

    iget-object v2, v4, Leeu;->c:Leby;

    const/4 v3, 0x0

    iput v3, v2, Leby;->c:I

    :goto_5
    iget v2, v4, Leeu;->d:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v4, Leeu;->d:I

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    :cond_6
    iget-object v3, v4, Leeu;->a:Leet;

    iget-object v5, v4, Leeu;->c:Leby;

    iget v5, v5, Leby;->c:I

    move-object/from16 v0, p6

    invoke-interface {v3, v0, v2, v5}, Leet;->a(Lefx;Ljava/nio/ByteBuffer;I)V

    goto :goto_5

    :sswitch_1
    move/from16 v0, p5

    new-array v2, v0, [B

    iput-object v2, v4, Leeu;->m:[B

    iget-object v2, v4, Leeu;->a:Leet;

    iget-object v3, v4, Leeu;->m:[B

    move-object/from16 v0, p6

    move/from16 v1, p5

    invoke-interface {v2, v0, v3, v1}, Leet;->a(Lefx;[BI)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xa3 -> :sswitch_0
        0x63a2 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final b(I)V
    .locals 14

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, -0x1

    const/4 v13, 0x0

    const/4 v1, -0x1

    .line 554
    iget-object v12, p0, Leev;->a:Leeu;

    sparse-switch p1, :sswitch_data_0

    .line 555
    :cond_0
    :goto_0
    return-void

    .line 554
    :sswitch_0
    iget-wide v2, v12, Leeu;->e:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    new-instance v0, Lebx;

    const-string v1, "Segment start/end offsets unknown"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-wide v2, v12, Leeu;->h:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    new-instance v0, Lebx;

    const-string v1, "Duration unknown"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-wide v2, v12, Leeu;->o:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    new-instance v0, Lebx;

    const-string v1, "Cues size unknown"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v1, v12, Leeu;->t:Legs;

    if-eqz v1, :cond_4

    iget-object v1, v12, Leeu;->u:Legs;

    if-eqz v1, :cond_4

    iget-object v1, v12, Leeu;->t:Legs;

    iget v1, v1, Legs;->a:I

    if-eqz v1, :cond_4

    iget-object v1, v12, Leeu;->t:Legs;

    iget v1, v1, Legs;->a:I

    iget-object v2, v12, Leeu;->u:Legs;

    iget v2, v2, Legs;->a:I

    if-eq v1, v2, :cond_5

    :cond_4
    new-instance v0, Lebx;

    const-string v1, "Invalid/missing cue points"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v1, v12, Leeu;->t:Legs;

    iget v6, v1, Legs;->a:I

    new-array v2, v6, [I

    new-array v3, v6, [J

    new-array v4, v6, [J

    new-array v5, v6, [J

    move v1, v0

    :goto_1
    if-ge v1, v6, :cond_6

    iget-object v7, v12, Leeu;->t:Legs;

    invoke-virtual {v7, v1}, Legs;->a(I)J

    move-result-wide v8

    aput-wide v8, v5, v1

    iget-wide v8, v12, Leeu;->e:J

    iget-object v7, v12, Leeu;->u:Legs;

    invoke-virtual {v7, v1}, Legs;->a(I)J

    move-result-wide v10

    add-long/2addr v8, v10

    aput-wide v8, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    :goto_2
    add-int/lit8 v1, v6, -0x1

    if-ge v0, v1, :cond_7

    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v3, v1

    aget-wide v10, v3, v0

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v2, v0

    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v5, v1

    aget-wide v10, v5, v0

    sub-long/2addr v8, v10

    aput-wide v8, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    add-int/lit8 v0, v6, -0x1

    iget-wide v8, v12, Leeu;->f:J

    add-int/lit8 v1, v6, -0x1

    aget-wide v10, v3, v1

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v2, v0

    add-int/lit8 v0, v6, -0x1

    iget-wide v8, v12, Leeu;->h:J

    add-int/lit8 v1, v6, -0x1

    aget-wide v6, v5, v1

    sub-long v6, v8, v6

    aput-wide v6, v4, v0

    new-instance v0, Leef;

    iget-wide v6, v12, Leeu;->o:J

    long-to-int v1, v6

    invoke-direct/range {v0 .. v5}, Leef;-><init>(I[I[J[J[J)V

    iput-object v0, v12, Leeu;->s:Leef;

    iput-object v13, v12, Leeu;->t:Legs;

    iput-object v13, v12, Leeu;->u:Legs;

    iget v0, v12, Leeu;->d:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v12, Leeu;->d:I

    goto/16 :goto_0

    :sswitch_1
    iget v0, v12, Leeu;->i:I

    if-eq v0, v1, :cond_9

    iget v0, v12, Leeu;->j:I

    if-eq v0, v1, :cond_9

    iget-object v0, v12, Leeu;->r:Lebv;

    if-eqz v0, :cond_8

    iget-object v0, v12, Leeu;->r:Lebv;

    iget v0, v0, Lebv;->c:I

    iget v2, v12, Leeu;->i:I

    if-ne v0, v2, :cond_8

    iget-object v0, v12, Leeu;->r:Lebv;

    iget v0, v0, Lebv;->d:I

    iget v2, v12, Leeu;->j:I

    if-eq v0, v2, :cond_9

    :cond_8
    const-string v0, "video/x-vnd.on2.vp9"

    iget v2, v12, Leeu;->i:I

    iget v3, v12, Leeu;->j:I

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v5, v13

    invoke-static/range {v0 .. v5}, Lebv;->a(Ljava/lang/String;IIIFLjava/util/List;)Lebv;

    move-result-object v0

    iput-object v0, v12, Leeu;->r:Lebv;

    iget v0, v12, Leeu;->d:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v12, Leeu;->d:I

    goto/16 :goto_0

    :cond_9
    iget-object v0, v12, Leeu;->r:Lebv;

    if-nez v0, :cond_0

    new-instance v0, Lebx;

    const-string v1, "Unable to build format"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_2
    const/4 v0, 0x1

    iput-boolean v0, v12, Leeu;->n:Z

    goto/16 :goto_0

    :sswitch_3
    iget-boolean v0, v12, Leeu;->n:Z

    if-eqz v0, :cond_0

    iget v0, v12, Leeu;->k:I

    if-eq v0, v1, :cond_b

    iget v0, v12, Leeu;->l:I

    if-eq v0, v1, :cond_b

    iget-object v0, v12, Leeu;->r:Lebv;

    if-eqz v0, :cond_a

    iget-object v0, v12, Leeu;->r:Lebv;

    iget v0, v0, Lebv;->f:I

    iget v2, v12, Leeu;->k:I

    if-ne v0, v2, :cond_a

    iget-object v0, v12, Leeu;->r:Lebv;

    iget v0, v0, Lebv;->g:I

    iget v2, v12, Leeu;->l:I

    if-eq v0, v2, :cond_b

    :cond_a
    const-string v3, "audio/vorbis"

    iget v8, v12, Leeu;->l:I

    iget v9, v12, Leeu;->k:I

    invoke-virtual {v12}, Leeu;->e()Ljava/util/ArrayList;

    move-result-object v11

    new-instance v2, Lebv;

    const/16 v4, 0x2000

    const/high16 v7, -0x40800000    # -1.0f

    move v5, v1

    move v6, v1

    move v10, v1

    invoke-direct/range {v2 .. v11}, Lebv;-><init>(Ljava/lang/String;IIIFIIILjava/util/List;)V

    iput-object v2, v12, Leeu;->r:Lebv;

    iget v0, v12, Leeu;->d:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v12, Leeu;->d:I

    goto/16 :goto_0

    :cond_b
    iget-object v0, v12, Leeu;->r:Lebv;

    if-nez v0, :cond_0

    new-instance v0, Lebx;

    const-string v1, "Unable to build format"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_data_0
    .sparse-switch
        0xae -> :sswitch_3
        0xe0 -> :sswitch_1
        0xe1 -> :sswitch_2
        0x1c53bb6b -> :sswitch_0
    .end sparse-switch
.end method
