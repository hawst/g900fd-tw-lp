.class final Lijb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lifx;

.field b:Lifb;

.field c:Lifb;

.field d:Lifb;

.field e:Lifb;

.field f:I

.field g:I

.field h:I

.field i:I


# direct methods
.method constructor <init>(Lifx;Lifb;Lifb;Lifb;Lifb;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    if-nez p2, :cond_0

    if-eqz p4, :cond_3

    :cond_0
    if-nez p3, :cond_1

    if-eqz p5, :cond_3

    :cond_1
    if-eqz p2, :cond_2

    if-eqz p3, :cond_3

    :cond_2
    if-eqz p4, :cond_4

    if-nez p5, :cond_4

    .line 47
    :cond_3
    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    .line 49
    :cond_4
    invoke-direct/range {p0 .. p5}, Lijb;->a(Lifx;Lifb;Lifb;Lifb;Lifb;)V

    .line 50
    return-void
.end method

.method constructor <init>(Lijb;)V
    .locals 6

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iget-object v1, p1, Lijb;->a:Lifx;

    iget-object v2, p1, Lijb;->b:Lifb;

    iget-object v3, p1, Lijb;->c:Lifb;

    iget-object v4, p1, Lijb;->d:Lifb;

    iget-object v5, p1, Lijb;->e:Lifb;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lijb;->a(Lifx;Lifb;Lifb;Lifb;Lifb;)V

    .line 54
    return-void
.end method

.method private a(Lifx;Lifb;Lifb;Lifb;Lifb;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lijb;->a:Lifx;

    .line 62
    iput-object p2, p0, Lijb;->b:Lifb;

    .line 63
    iput-object p3, p0, Lijb;->c:Lifb;

    .line 64
    iput-object p4, p0, Lijb;->d:Lifb;

    .line 65
    iput-object p5, p0, Lijb;->e:Lifb;

    .line 66
    invoke-virtual {p0}, Lijb;->a()V

    .line 67
    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 120
    iget-object v0, p0, Lijb;->b:Lifb;

    if-nez v0, :cond_1

    .line 121
    new-instance v0, Lifb;

    iget-object v1, p0, Lijb;->d:Lifb;

    iget v1, v1, Lifb;->b:F

    invoke-direct {v0, v2, v1}, Lifb;-><init>(FF)V

    iput-object v0, p0, Lijb;->b:Lifb;

    .line 122
    new-instance v0, Lifb;

    iget-object v1, p0, Lijb;->e:Lifb;

    iget v1, v1, Lifb;->b:F

    invoke-direct {v0, v2, v1}, Lifb;-><init>(FF)V

    iput-object v0, p0, Lijb;->c:Lifb;

    .line 128
    :cond_0
    :goto_0
    iget-object v0, p0, Lijb;->b:Lifb;

    iget v0, v0, Lifb;->a:F

    iget-object v1, p0, Lijb;->c:Lifb;

    iget v1, v1, Lifb;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lijb;->f:I

    .line 129
    iget-object v0, p0, Lijb;->d:Lifb;

    iget v0, v0, Lifb;->a:F

    iget-object v1, p0, Lijb;->e:Lifb;

    iget v1, v1, Lifb;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lijb;->g:I

    .line 130
    iget-object v0, p0, Lijb;->b:Lifb;

    iget v0, v0, Lifb;->b:F

    iget-object v1, p0, Lijb;->d:Lifb;

    iget v1, v1, Lifb;->b:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lijb;->h:I

    .line 131
    iget-object v0, p0, Lijb;->c:Lifb;

    iget v0, v0, Lifb;->b:F

    iget-object v1, p0, Lijb;->e:Lifb;

    iget v1, v1, Lifb;->b:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lijb;->i:I

    .line 132
    return-void

    .line 123
    :cond_1
    iget-object v0, p0, Lijb;->d:Lifb;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lifb;

    iget-object v1, p0, Lijb;->a:Lifx;

    iget v1, v1, Lifx;->a:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    iget-object v2, p0, Lijb;->b:Lifb;

    iget v2, v2, Lifb;->b:F

    invoke-direct {v0, v1, v2}, Lifb;-><init>(FF)V

    iput-object v0, p0, Lijb;->d:Lifb;

    .line 125
    new-instance v0, Lifb;

    iget-object v1, p0, Lijb;->a:Lifx;

    iget v1, v1, Lifx;->a:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    iget-object v2, p0, Lijb;->c:Lifb;

    iget v2, v2, Lifb;->b:F

    invoke-direct {v0, v1, v2}, Lifb;-><init>(FF)V

    iput-object v0, p0, Lijb;->e:Lifb;

    goto :goto_0
.end method
