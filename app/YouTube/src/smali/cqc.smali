.class public final Lcqc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lezj;

.field private final b:Lexd;

.field private final c:Levn;

.field private final d:Lggr;

.field private final e:Lezf;

.field private final f:Lgjp;

.field private final g:Lgoc;


# direct methods
.method public constructor <init>(Lgjp;Lezj;Lexd;Levn;Lggr;Lgoc;)V
    .locals 8

    .prologue
    .line 121
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcqc;-><init>(Lgjp;Lezj;Lexd;Levn;Lggr;Lgoc;Lezf;)V

    .line 128
    return-void
.end method

.method public constructor <init>(Lgjp;Lezj;Lexd;Levn;Lggr;Lgoc;Lezf;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput-object p1, p0, Lcqc;->f:Lgjp;

    .line 139
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcqc;->a:Lezj;

    .line 140
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lcqc;->b:Lexd;

    .line 141
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcqc;->c:Levn;

    .line 142
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggr;

    iput-object v0, p0, Lcqc;->d:Lggr;

    .line 143
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoc;

    iput-object v0, p0, Lcqc;->g:Lgoc;

    .line 144
    iput-object p7, p0, Lcqc;->e:Lezf;

    .line 145
    return-void
.end method

.method private a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZLjava/lang/String;Lgog;Lcwx;Ljava/util/List;Lgoc;)Lcqa;
    .locals 27

    .prologue
    .line 246
    new-instance v2, Lcqa;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcqc;->f:Lgjp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcqc;->a:Lezj;

    .line 253
    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcqc;->a:Lezj;

    .line 260
    invoke-virtual {v5}, Lezj;->b()J

    move-result-wide v16

    const-string v19, "-"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcqc;->b:Lexd;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcqc;->c:Levn;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcqc;->d:Lggr;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcqc;->e:Lezf;

    move-object/from16 v25, v0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p5

    move-object/from16 v15, p6

    move-object/from16 v18, p12

    move-object/from16 v20, p13

    move-object/from16 v24, p14

    move-object/from16 v26, p15

    invoke-direct/range {v2 .. v26}, Lcqa;-><init>(Lgjp;Lezj;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLgog;Ljava/lang/String;Lcwx;Lexd;Levn;Lggr;Ljava/util/List;Lezf;Lgoc;)V

    .line 270
    invoke-virtual {v2}, Lcqa;->e()V

    .line 271
    return-object v2
.end method


# virtual methods
.method public final a(Lcwx;Lcqd;)Lcqa;
    .locals 11

    .prologue
    .line 210
    new-instance v0, Lcqa;

    iget-object v1, p0, Lcqc;->f:Lgjp;

    iget-object v2, p0, Lcqc;->a:Lezj;

    .line 213
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcqd;

    .line 214
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcwx;

    iget-object v5, p0, Lcqc;->b:Lexd;

    iget-object v6, p0, Lcqc;->c:Levn;

    iget-object v7, p0, Lcqc;->d:Lggr;

    iget-object v8, p2, Lcqd;->h:Ljava/lang/String;

    .line 218
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    iget-object v9, p0, Lcqc;->e:Lezf;

    iget-object v10, p0, Lcqc;->g:Lgoc;

    invoke-direct/range {v0 .. v10}, Lcqa;-><init>(Lgjp;Lezj;Lcqd;Lcwx;Lexd;Levn;Lggr;Ljava/util/List;Lezf;Lgoc;)V

    .line 221
    invoke-virtual {v0}, Lcqa;->e()V

    .line 222
    return-object v0
.end method

.method public final a(Lfnd;Lfnd;Lfnd;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Lgog;Lcwx;)Lcqa;
    .locals 17

    .prologue
    .line 189
    .line 190
    move-object/from16 v0, p1

    iget-object v1, v0, Lfnd;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 191
    move-object/from16 v0, p2

    iget-object v1, v0, Lfnd;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 192
    move-object/from16 v0, p3

    iget-object v1, v0, Lfnd;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 193
    invoke-static/range {p4 .. p4}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 194
    invoke-static/range {p5 .. p5}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    if-eqz p7, :cond_0

    const/16 v1, 0x1e

    .line 197
    :goto_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lfnd;->a(I)I

    move-result v9

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 201
    invoke-static/range {p8 .. p8}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 202
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lgog;

    .line 203
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcwx;

    .line 204
    new-instance v15, Ljava/util/LinkedList;

    invoke-direct {v15}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcqc;->g:Lgoc;

    move-object/from16 v16, v0

    move-object/from16 v1, p0

    move/from16 v8, p6

    .line 189
    invoke-direct/range {v1 .. v16}, Lcqc;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZLjava/lang/String;Lgog;Lcwx;Ljava/util/List;Lgoc;)Lcqa;

    move-result-object v1

    return-object v1

    .line 194
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Lfnd;Lfnd;Lfnd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLgog;Lcwx;)Lcqa;
    .locals 17

    .prologue
    .line 159
    .line 160
    move-object/from16 v0, p1

    iget-object v1, v0, Lfnd;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 161
    move-object/from16 v0, p2

    iget-object v1, v0, Lfnd;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 162
    move-object/from16 v0, p3

    iget-object v1, v0, Lfnd;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 163
    invoke-static/range {p4 .. p4}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 164
    invoke-static/range {p5 .. p5}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz p8, :cond_0

    const/4 v1, 0x4

    .line 167
    :goto_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lfnd;->a(I)I

    move-result v9

    const/4 v12, 0x0

    .line 172
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lgog;

    .line 173
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcwx;

    .line 174
    new-instance v15, Ljava/util/LinkedList;

    invoke-direct {v15}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcqc;->g:Lgoc;

    move-object/from16 v16, v0

    move-object/from16 v1, p0

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v10, p8

    move/from16 v11, p9

    .line 159
    invoke-direct/range {v1 .. v16}, Lcqc;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZLjava/lang/String;Lgog;Lcwx;Ljava/util/List;Lgoc;)Lcqa;

    move-result-object v1

    return-object v1

    .line 164
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
