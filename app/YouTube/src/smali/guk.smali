.class public final Lguk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgtn;


# instance fields
.field private final a:Lfel;


# direct methods
.method public constructor <init>(Lfel;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfel;

    iput-object v0, p0, Lguk;->a:Lfel;

    .line 33
    return-void
.end method

.method private a(Lgvd;)Lguc;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v2, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v2}, Lgvi;->b()Lgvi;

    move-result-object v2

    .line 60
    iget-object v3, v2, Lgvi;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 63
    :try_start_0
    new-instance v0, Lhno;

    invoke-direct {v0}, Lhno;-><init>()V

    .line 64
    iget-object v1, v2, Lgvi;->j:Ljava/lang/String;

    iput-object v1, v0, Lhno;->b:Ljava/lang/String;

    .line 66
    new-instance v1, Lhmw;

    invoke-direct {v1}, Lhmw;-><init>()V

    iput-object v1, v0, Lhno;->c:Lhmw;

    .line 67
    iget-object v1, v2, Lgvi;->c:Lgvj;

    iget-object v1, v1, Lgvj;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Metadata update with empty title"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catch Lfdv; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    invoke-static {v0}, La;->a(Lfdv;)Lguc;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 60
    goto :goto_0

    .line 70
    :cond_1
    :try_start_1
    iget-object v1, v0, Lhno;->c:Lhmw;

    iget-object v3, v2, Lgvi;->c:Lgvj;

    iget-object v3, v3, Lgvj;->a:Ljava/lang/String;

    iput-object v3, v1, Lhmw;->a:Ljava/lang/String;

    .line 72
    new-instance v1, Lhmi;

    invoke-direct {v1}, Lhmi;-><init>()V

    iput-object v1, v0, Lhno;->d:Lhmi;

    .line 75
    iget-object v1, v0, Lhno;->d:Lhmi;

    iget-object v3, v2, Lgvi;->c:Lgvj;

    iget-object v3, v3, Lgvj;->b:Ljava/lang/String;

    iput-object v3, v1, Lhmi;->a:Ljava/lang/String;

    .line 77
    new-instance v1, Lhms;

    invoke-direct {v1}, Lhms;-><init>()V

    iput-object v1, v0, Lhno;->e:Lhms;

    .line 78
    iget-object v1, v2, Lgvi;->c:Lgvj;

    iget v1, v1, Lgvj;->c:I

    packed-switch v1, :pswitch_data_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-object v1, v2, Lgvi;->c:Lgvj;

    iget v1, v1, Lgvj;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown privacy status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :pswitch_0
    iget-object v1, v0, Lhno;->e:Lhms;

    const/4 v3, 0x0

    iput v3, v1, Lhms;->a:I

    .line 93
    :goto_2
    new-instance v1, Lhmu;

    invoke-direct {v1}, Lhmu;-><init>()V

    iput-object v1, v0, Lhno;->f:Lhmu;

    .line 94
    iget-object v1, v0, Lhno;->f:Lhmu;

    iget-object v3, v2, Lgvi;->c:Lgvj;

    iget-object v3, v3, Lgvj;->d:[Ljava/lang/String;

    iput-object v3, v1, Lhmu;->a:[Ljava/lang/String;

    .line 96
    iget-object v1, p0, Lguk;->a:Lfel;

    iget-object v2, v2, Lgvi;->a:Ljava/lang/String;

    .line 97
    iget-object v3, v1, Lfel;->e:Lfco;

    new-instance v4, Lfte;

    iget-object v5, v1, Lfel;->b:Lfsz;

    iget-object v1, v1, Lfel;->c:Lgix;

    invoke-interface {v1, v2}, Lgix;->a(Ljava/lang/String;)Lgit;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lfte;-><init>(Lfsz;Lgit;)V

    invoke-virtual {v4, v0}, Lfte;->a(Lidh;)V

    invoke-virtual {v3, v4}, Lfco;->a(Lfsp;)Lidh;

    move-result-object v0

    check-cast v0, Lhnp;

    .line 99
    iget-object v0, v0, Lhnp;->a:Lhxy;

    iget v0, v0, Lhxy;->a:I

    if-eqz v0, :cond_2

    .line 100
    const/4 v0, 0x2

    invoke-static {v0}, La;->e(I)Lguc;

    move-result-object v0

    goto/16 :goto_1

    .line 83
    :pswitch_1
    iget-object v1, v0, Lhno;->e:Lhms;

    const/4 v3, 0x1

    iput v3, v1, Lhms;->a:I

    goto :goto_2

    .line 86
    :pswitch_2
    iget-object v1, v0, Lhno;->e:Lhms;

    const/4 v3, 0x2

    iput v3, v1, Lhms;->a:I

    goto :goto_2

    .line 103
    :cond_2
    new-instance v0, Lgul;

    invoke-direct {v0, p0}, Lgul;-><init>(Lguk;)V
    :try_end_1
    .catch Lfdv; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 78
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)J
    .locals 2

    .prologue
    .line 27
    check-cast p1, Lgvd;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v0}, Lgvi;->b()Lgvi;

    move-result-object v0

    iget v1, v0, Lgvi;->d:I

    if-nez v1, :cond_0

    iget-object v1, v0, Lgvi;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lgvi;->c:Lgvj;

    if-eqz v1, :cond_0

    iget-boolean v0, v0, Lgvi;->k:Z

    if-nez v0, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lguc;
    .locals 1

    .prologue
    .line 27
    check-cast p2, Lgvd;

    invoke-direct {p0, p2}, Lguk;->a(Lgvd;)Lguc;

    move-result-object v0

    return-object v0
.end method
