.class public final Lbps;
.super Lbqd;
.source "SourceFile"


# static fields
.field private static final a:Lbps;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1007
    new-instance v0, Lbps;

    invoke-direct {v0}, Lbps;-><init>()V

    sput-object v0, Lbps;->a:Lbps;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1004
    invoke-direct {p0}, Lbqd;-><init>()V

    return-void
.end method

.method public static synthetic a()Lbps;
    .locals 1

    .prologue
    .line 1004
    sget-object v0, Lbps;->a:Lbps;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Lfkq;
    .locals 3

    .prologue
    .line 1004
    check-cast p1, Lfjf;

    iget-object v0, p1, Lfjf;->i:Lfkp;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfjf;->a:Lheb;

    iget-object v0, v0, Lheb;->n:Lhnh;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfjf;->a:Lheb;

    iget-object v0, v0, Lheb;->n:Lhnh;

    iget-object v0, v0, Lhnh;->a:Lhne;

    if-eqz v0, :cond_0

    new-instance v0, Lfkp;

    iget-object v1, p1, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->n:Lhnh;

    iget-object v1, v1, Lhnh;->a:Lhne;

    invoke-direct {v0, v1, p1}, Lfkp;-><init>(Lhne;Lfqh;)V

    iput-object v0, p1, Lfjf;->i:Lfkp;

    :cond_0
    iget-object v0, p1, Lfjf;->i:Lfkp;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lfkp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkq;

    iget-object v2, v0, Lfkq;->a:Lhut;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lhut;->g:Lhgq;

    if-eqz v2, :cond_1

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic b(Ljava/lang/Object;)Lhut;
    .locals 5

    .prologue
    .line 1004
    check-cast p1, Lfjf;

    iget-object v0, p1, Lfjf;->a:Lheb;

    iget-object v2, v0, Lheb;->l:[Lhut;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget-object v4, v0, Lhut;->e:Lhfr;

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final synthetic c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004
    check-cast p1, Lfjf;

    invoke-virtual {p1}, Lfjf;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic d(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004
    check-cast p1, Lfjf;

    iget-object v0, p1, Lfjf;->a:Lheb;

    iget-object v0, v0, Lheb;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected final synthetic e(Ljava/lang/Object;)Lflh;
    .locals 2

    .prologue
    .line 1004
    check-cast p1, Lfjf;

    iget-object v0, p1, Lfjf;->g:Lflh;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfjf;->a:Lheb;

    iget-object v0, v0, Lheb;->i:Lhec;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfjf;->a:Lheb;

    iget-object v0, v0, Lheb;->i:Lhec;

    iget-object v0, v0, Lhec;->a:Lhpn;

    if-eqz v0, :cond_0

    new-instance v0, Lflh;

    iget-object v1, p1, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->i:Lhec;

    iget-object v1, v1, Lhec;->a:Lhpn;

    invoke-direct {v0, v1}, Lflh;-><init>(Lhpn;)V

    iput-object v0, p1, Lfjf;->g:Lflh;

    :cond_0
    iget-object v0, p1, Lfjf;->g:Lflh;

    return-object v0
.end method
