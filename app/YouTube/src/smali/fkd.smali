.class public Lfkd;
.super Lfmr;
.source "SourceFile"


# instance fields
.field private final e:Lhin;

.field private f:Ljava/util/List;

.field private g:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhvp;Lfqh;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lfmr;-><init>(Lhvp;Lfqh;)V

    .line 27
    iget-object v0, p1, Lhvp;->d:Lhvq;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhvp;->d:Lhvq;

    iget-object v0, v0, Lhvq;->a:Lhin;

    :goto_0
    iput-object v0, p0, Lfkd;->e:Lhin;

    .line 29
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 33
    iget-object v0, p0, Lfkd;->f:Ljava/util/List;

    if-nez v0, :cond_5

    .line 34
    iget-object v0, p0, Lfkd;->e:Lhin;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfkd;->e:Lhin;

    iget-object v0, v0, Lhin;->a:[Lhio;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfkd;->e:Lhin;

    iget-object v1, v1, Lhin;->a:[Lhio;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfkd;->f:Ljava/util/List;

    .line 36
    iget-object v0, p0, Lfkd;->e:Lhin;

    iget-object v1, v0, Lhin;->a:[Lhio;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 37
    iget-object v4, v3, Lhio;->e:Lhhn;

    if-eqz v4, :cond_1

    .line 38
    iget-object v4, p0, Lfkd;->f:Ljava/util/List;

    new-instance v5, Lfkc;

    iget-object v3, v3, Lhio;->e:Lhhn;

    invoke-direct {v5, v3, p0}, Lfkc;-><init>(Lhhn;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39
    :cond_1
    iget-object v4, v3, Lhio;->b:Lhhj;

    if-eqz v4, :cond_2

    .line 40
    iget-object v4, p0, Lfkd;->f:Ljava/util/List;

    new-instance v5, Lfjz;

    iget-object v3, v3, Lhio;->b:Lhhj;

    invoke-direct {v5, v3, p0}, Lfjz;-><init>(Lhhj;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 41
    :cond_2
    iget-object v4, v3, Lhio;->c:Lhhk;

    if-eqz v4, :cond_3

    .line 42
    iget-object v4, p0, Lfkd;->f:Ljava/util/List;

    new-instance v5, Lfka;

    iget-object v3, v3, Lhio;->c:Lhhk;

    invoke-direct {v5, v3, p0}, Lfka;-><init>(Lhhk;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 43
    :cond_3
    iget-object v4, v3, Lhio;->d:Lhhm;

    if-eqz v4, :cond_0

    .line 44
    iget-object v4, p0, Lfkd;->f:Ljava/util/List;

    new-instance v5, Lfkb;

    iget-object v3, v3, Lhio;->d:Lhhm;

    invoke-direct {v5, v3, p0}, Lfkb;-><init>(Lhhm;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 48
    :cond_4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfkd;->f:Ljava/util/List;

    .line 52
    :cond_5
    iget-object v0, p0, Lfkd;->f:Ljava/util/List;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lfkd;->e:Lhin;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfkd;->e:Lhin;

    iget v0, v0, Lhin;->d:I

    if-lez v0, :cond_0

    .line 58
    iget-object v0, p0, Lfkd;->e:Lhin;

    iget v0, v0, Lhin;->d:I

    .line 66
    :goto_0
    return v0

    .line 63
    :cond_0
    iget-object v0, p0, Lfkd;->e:Lhin;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfkd;->e:Lhin;

    iget v0, v0, Lhin;->c:I

    if-nez v0, :cond_2

    .line 64
    :cond_1
    const/4 v0, 0x3

    goto :goto_0

    .line 66
    :cond_2
    iget-object v0, p0, Lfkd;->e:Lhin;

    iget v0, v0, Lhin;->c:I

    goto :goto_0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lfkd;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lfkd;->e:Lhin;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfkd;->e:Lhin;

    iget-object v0, v0, Lhin;->b:Lhgz;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lfkd;->e:Lhin;

    iget-object v0, v0, Lhin;->b:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfkd;->g:Ljava/lang/CharSequence;

    .line 76
    :cond_0
    iget-object v0, p0, Lfkd;->g:Ljava/lang/CharSequence;

    return-object v0
.end method
