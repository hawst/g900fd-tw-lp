.class public final Lgox;
.super Lfbd;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Lfbd;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lfah;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 158
    const-class v0, Lgpn;

    invoke-virtual {p1, v0}, Lfah;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgpn;

    .line 159
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "w"

    aput-object v2, v1, v7

    const-string v2, "win"

    aput-object v2, v1, v6

    invoke-static {p2, v1}, La;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v7}, La;->b(Ljava/lang/String;I)I

    move-result v3

    .line 160
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "t"

    aput-object v2, v1, v7

    const-string v2, "start"

    aput-object v2, v1, v6

    invoke-static {p2, v1}, La;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 161
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "d"

    aput-object v2, v1, v7

    const-string v2, "dur"

    aput-object v2, v1, v6

    .line 162
    invoke-static {p2, v1}, La;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v7}, La;->b(Ljava/lang/String;I)I

    move-result v1

    .line 163
    const-string v2, "\n"

    const-string v5, "<br/>"

    invoke-virtual {p3, v2, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 164
    new-array v5, v6, [Ljava/lang/String;

    const-string v6, "append"

    aput-object v6, v5, v7

    invoke-static {p2, v5}, La;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 165
    if-nez v5, :cond_0

    .line 166
    add-int/2addr v1, v4

    invoke-virtual {v0, v3, v2, v4, v1}, Lgpn;->a(ILjava/lang/String;II)Lgpn;

    .line 172
    :goto_0
    return-void

    .line 169
    :cond_0
    add-int/2addr v1, v4

    invoke-virtual {v0, v3}, Lgpn;->a(I)Lgpd;

    move-result-object v0

    iget-object v3, v0, Lgpd;->a:Lgpl;

    iget-object v0, v3, Lgpl;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v3, Lgpl;->b:Ljava/util/List;

    iget-object v5, v3, Lgpl;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v2, v0

    :cond_1
    if-ne v1, v4, :cond_3

    iget-object v0, v3, Lgpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v3, Lgpl;->a:Ljava/util/List;

    iget-object v1, v3, Lgpl;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_2
    invoke-virtual {v3, v2, v4, v0}, Lgpl;->a(Ljava/lang/String;II)Lgpl;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
