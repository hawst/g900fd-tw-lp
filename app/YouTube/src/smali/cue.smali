.class final Lcue;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field private synthetic a:Lgit;

.field private synthetic b:Lcua;

.field private synthetic c:Lcub;


# direct methods
.method constructor <init>(Lcub;Lgit;Lcua;)V
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lcue;->c:Lcub;

    iput-object p2, p0, Lcue;->a:Lgit;

    iput-object p3, p0, Lcue;->b:Lcua;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcue;->c:Lcub;

    invoke-virtual {v0, p2}, Lcub;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 400
    check-cast p2, Lgca;

    iget-object v0, p2, Lgca;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lgca;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcue;->c:Lcub;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Username or channel id is empty."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcub;->a(Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcue;->c:Lcub;

    iget-object v1, p0, Lcue;->a:Lgit;

    iget-object v2, p0, Lcue;->b:Lcua;

    iget-object v3, v0, Lcub;->a:Lcst;

    invoke-virtual {v3}, Lcst;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "Signed in as new account"

    invoke-virtual {v0, v3}, Lcub;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v3, v0, Lcub;->a:Lcst;

    iget-object v3, v3, Lcst;->a:Lctj;

    iget-object v4, v1, Lgit;->b:Lgiv;

    invoke-virtual {v4}, Lgiv;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, Lgit;->b:Lgiv;

    invoke-virtual {v5}, Lgiv;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v1, v1, Lgit;->c:Ljava/lang/String;

    invoke-interface {v3, v4, v5, v1}, Lctj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p2, Lgca;->a:Ljava/lang/String;

    iget-object v4, p2, Lgca;->c:Ljava/lang/String;

    invoke-interface {v3, v1, v4}, Lctj;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Lctj;->a(Lcua;)V

    iget-object v1, v0, Lcub;->i:Levn;

    new-instance v2, Lfcb;

    invoke-direct {v2}, Lfcb;-><init>()V

    invoke-virtual {v1, v2}, Levn;->c(Ljava/lang/Object;)V

    iget-object v1, v0, Lcub;->i:Levn;

    new-instance v2, Lcsw;

    invoke-direct {v2}, Lcsw;-><init>()V

    invoke-virtual {v1, v2}, Levn;->c(Ljava/lang/Object;)V

    iget-object v1, v0, Lcub;->f:Ljava/util/concurrent/Executor;

    new-instance v2, Lcuf;

    invoke-direct {v2, v0}, Lcuf;-><init>(Lcub;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
