.class public Lesv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgjg;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lesh;

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/util/List;

.field public h:Ljava/util/List;

.field public i:Ljava/util/List;

.field public j:Ljava/util/List;

.field public k:Ljava/util/List;

.field l:Lesn;

.field public m:Z

.field n:Ljava/lang/String;

.field private o:[B


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    new-instance v0, Lesh;

    sget-object v1, Lesl;->c:Lesl;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lesh;-><init>(Lesl;J)V

    iput-object v0, p0, Lesv;->b:Lesh;

    .line 256
    iput-boolean v5, p0, Lesv;->c:Z

    .line 257
    iput-boolean v5, p0, Lesv;->d:Z

    .line 258
    iput-boolean v5, p0, Lesv;->e:Z

    .line 259
    iput-object v4, p0, Lesv;->f:Ljava/lang/String;

    .line 260
    iput-object v4, p0, Lesv;->g:Ljava/util/List;

    .line 261
    iput-object v4, p0, Lesv;->h:Ljava/util/List;

    .line 262
    iput-object v4, p0, Lesv;->i:Ljava/util/List;

    .line 263
    iput-object v4, p0, Lesv;->j:Ljava/util/List;

    .line 264
    iput-object v4, p0, Lesv;->k:Ljava/util/List;

    .line 265
    iput-object v4, p0, Lesv;->l:Lesn;

    .line 266
    iput-boolean v5, p0, Lesv;->m:Z

    .line 267
    sget-object v0, Lfhy;->a:[B

    iput-object v0, p0, Lesv;->o:[B

    .line 268
    return-void
.end method

.method static synthetic a(Lesv;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lesv;->h:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lesv;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lesv;->i:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lesv;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lesv;->j:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public final a()Less;
    .locals 19

    .prologue
    .line 379
    move-object/from16 v0, p0

    iget-object v2, v0, Lesv;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v7, v0, Lesv;->a:Ljava/lang/String;

    .line 381
    :goto_0
    new-instance v2, Less;

    move-object/from16 v0, p0

    iget-object v3, v0, Lesv;->b:Lesh;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lesv;->c:Z

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lesv;->d:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lesv;->e:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lesv;->f:Ljava/lang/String;

    if-nez v8, :cond_2

    const-string v8, ""

    :goto_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lesv;->o:[B

    move-object/from16 v0, p0

    iget-object v10, v0, Lesv;->g:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Lesv;->h:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v12, v0, Lesv;->i:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v13, v0, Lesv;->j:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lesv;->k:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, Lesv;->l:Lesn;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lesv;->m:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lesv;->n:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-direct/range {v2 .. v18}, Less;-><init>(Lesh;ZZZLjava/lang/String;Ljava/lang/String;[BLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lesn;ZLjava/lang/String;B)V

    return-object v2

    .line 379
    :cond_0
    const-string v2, "_INTERNAL_"

    .line 380
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_1
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 381
    :cond_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lesv;->f:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a([B)Lesv;
    .locals 1

    .prologue
    .line 301
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lesv;->o:[B

    .line 302
    return-object p0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lesv;->a()Less;

    move-result-object v0

    return-object v0
.end method
