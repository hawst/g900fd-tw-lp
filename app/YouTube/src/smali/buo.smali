.class final enum Lbuo;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbuo;

.field public static final enum b:Lbuo;

.field public static final enum c:Lbuo;

.field private static final synthetic e:[Lbuo;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    new-instance v0, Lbuo;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lbuo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbuo;->a:Lbuo;

    .line 65
    new-instance v0, Lbuo;

    const-string v1, "SET_VIEW"

    invoke-direct {v0, v1, v4, v3}, Lbuo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbuo;->b:Lbuo;

    .line 66
    new-instance v0, Lbuo;

    const-string v1, "VIDEO_INFO_VIEW"

    invoke-direct {v0, v1, v5, v4}, Lbuo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbuo;->c:Lbuo;

    .line 63
    const/4 v0, 0x3

    new-array v0, v0, [Lbuo;

    sget-object v1, Lbuo;->a:Lbuo;

    aput-object v1, v0, v3

    sget-object v1, Lbuo;->b:Lbuo;

    aput-object v1, v0, v4

    sget-object v1, Lbuo;->c:Lbuo;

    aput-object v1, v0, v5

    sput-object v0, Lbuo;->e:[Lbuo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 71
    iput p3, p0, Lbuo;->d:I

    .line 72
    return-void
.end method

.method static synthetic a(Lbuo;)I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lbuo;->d:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbuo;
    .locals 1

    .prologue
    .line 63
    const-class v0, Lbuo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbuo;

    return-object v0
.end method

.method public static values()[Lbuo;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lbuo;->e:[Lbuo;

    invoke-virtual {v0}, [Lbuo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbuo;

    return-object v0
.end method
