.class final Lduu;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldun;


# direct methods
.method constructor <init>(Ldun;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 791
    iput-object p1, p0, Lduu;->a:Ldun;

    .line 792
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 793
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 797
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 916
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 799
    :pswitch_1
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    const-string v1, "Remote going to sleep ..."

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 800
    iget-object v0, p0, Lduu;->a:Ldun;

    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-static {v1}, Ldun;->b(Ldun;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1, v7}, Ldun;->a(Ldun;Landroid/content/Context;Z)V

    goto :goto_0

    .line 803
    :pswitch_2
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->c(Ldun;)Ldst;

    move-result-object v0

    if-nez v0, :cond_1

    .line 804
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    const-string v1, "We should reconnect, but we lost the cloud screen."

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 807
    :cond_1
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    const-string v1, "Remote waking up ..."

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 808
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->d(Ldun;)Ldqg;

    move-result-object v0

    new-instance v1, Ldpu;

    invoke-direct {v1}, Ldpu;-><init>()V

    iget-object v2, p0, Lduu;->a:Ldun;

    .line 809
    invoke-static {v2}, Ldun;->c(Ldun;)Ldst;

    move-result-object v2

    iget-object v2, v2, Ldst;->e:Ldtb;

    .line 808
    iput-object v2, v1, Ldpu;->c:Ldtb;

    .line 809
    invoke-virtual {v1}, Ldpu;->a()Ldpt;

    move-result-object v1

    .line 808
    invoke-virtual {v0, v1}, Ldqg;->a(Ldpt;)Ljava/util/concurrent/CountDownLatch;

    goto :goto_0

    .line 813
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldux;

    .line 814
    iget-object v1, v0, Ldux;->a:Ldus;

    iget-object v1, v1, Ldus;->a:Ldwr;

    invoke-virtual {v1}, Ldwr;->f()Ldwf;

    move-result-object v1

    .line 815
    iget-object v1, v1, Ldwf;->a:Ldtm;

    .line 816
    iget-object v2, p0, Lduu;->a:Ldun;

    iget-object v2, v2, Ldun;->f:Ldwr;

    .line 817
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ldwr;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 818
    invoke-virtual {v2}, Ldwr;->f()Ldwf;

    move-result-object v2

    iget-object v2, v2, Ldwf;->a:Ldtm;

    invoke-virtual {v2, v1}, Ldtm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 819
    :cond_2
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Waking up of DIAL device "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was canceled in the meantime."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 823
    :cond_3
    iget-object v2, v0, Ldux;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_4

    iget v2, v0, Ldux;->c:I

    if-lez v2, :cond_4

    .line 824
    iget-object v2, p0, Lduu;->a:Ldun;

    invoke-static {v2}, Ldun;->e(Ldun;)Ldxn;

    move-result-object v2

    new-instance v3, Lduv;

    invoke-direct {v3, p0, v1, v0}, Lduv;-><init>(Lduu;Ldtm;Ldux;)V

    invoke-virtual {v2, v3}, Ldxn;->a(Ldxw;)V

    .line 847
    iget v1, v0, Ldux;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Ldux;->c:I

    .line 848
    iget-object v1, p0, Lduu;->a:Ldun;

    .line 849
    invoke-static {v1}, Ldun;->f(Ldun;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v1, v6, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0x1388

    .line 848
    invoke-virtual {p0, v0, v2, v3}, Lduu;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 850
    :cond_4
    iget-object v2, v0, Ldux;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    iget v0, v0, Ldux;->c:I

    if-nez v0, :cond_0

    .line 852
    sget-object v0, Ldwh;->d:Ldwh;

    .line 853
    iget-object v2, p0, Lduu;->a:Ldun;

    invoke-static {v2}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x20

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Could not wake up DIAL device  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 854
    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-static {v1, v7}, Ldun;->a(Ldun;Z)Z

    .line 855
    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-static {v1}, Ldun;->g(Ldun;)Lduy;

    move-result-object v1

    invoke-virtual {v1, v0}, Lduy;->a(Ldwh;)V

    goto/16 :goto_0

    .line 859
    :pswitch_4
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->d(Ldun;)Ldqg;

    move-result-object v0

    iget-boolean v0, v0, Ldqg;->b:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->d(Ldun;)Ldqg;

    move-result-object v0

    invoke-virtual {v0}, Ldqg;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 860
    :cond_5
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    const-string v1, "Connecting to a new screen. Will disconnect the previous browser channel connection."

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 862
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->d(Ldun;)Ldqg;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldqg;->a(Z)V

    .line 865
    :cond_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldus;

    .line 866
    iget-object v2, v0, Ldus;->a:Ldwr;

    .line 867
    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-static {v1}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v3

    const-string v4, "Connecting to "

    invoke-virtual {v2}, Ldwr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 868
    invoke-virtual {v2}, Ldwr;->h()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 869
    invoke-virtual {v2}, Ldwr;->f()Ldwf;

    move-result-object v1

    iget-object v1, v1, Ldwf;->a:Ldtm;

    .line 870
    iget-object v2, v1, Ldtm;->b:Landroid/net/Uri;

    if-nez v2, :cond_8

    .line 871
    iget-object v2, v1, Ldtm;->f:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 873
    new-instance v2, Ldxt;

    iget-object v3, p0, Lduu;->a:Ldun;

    invoke-static {v3}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v3

    invoke-direct {v2, v3}, Ldxt;-><init>(Ldyg;)V

    iget-object v3, v1, Ldtm;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ldxt;->a(Ljava/lang/String;)V

    .line 874
    iget-object v2, p0, Lduu;->a:Ldun;

    .line 875
    invoke-static {v2}, Ldun;->f(Ldun;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Ldux;

    .line 876
    iget-object v1, v1, Ldtm;->g:Ljava/lang/Integer;

    invoke-direct {v3, v0, v1}, Ldux;-><init>(Ldus;Ljava/lang/Integer;)V

    .line 874
    invoke-static {v2, v6, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lduu;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 867
    :cond_7
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 878
    :cond_8
    iget-object v2, p0, Lduu;->a:Ldun;

    invoke-static {v2, v1, v0}, Ldun;->b(Ldun;Ldtm;Ldus;)V

    goto/16 :goto_0

    .line 880
    :cond_9
    invoke-virtual {v2}, Ldwr;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 881
    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-virtual {v2}, Ldwr;->e()Ldwd;

    move-result-object v3

    iget-object v3, v3, Ldwd;->a:Ldst;

    invoke-static {v1, v3}, Ldun;->a(Ldun;Ldst;)Ldst;

    move-result-object v1

    .line 882
    if-eqz v1, :cond_a

    .line 883
    iget-object v2, p0, Lduu;->a:Ldun;

    invoke-static {v2, v1, v0}, Ldun;->a(Ldun;Ldst;Ldus;)V

    goto/16 :goto_0

    .line 885
    :cond_a
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v1

    const-string v3, "Couldn\'t obtain cloud screen for "

    invoke-virtual {v2}, Ldwr;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ldyg;->a(Ljava/lang/String;)V

    .line 886
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->g(Ldun;)Lduy;

    move-result-object v0

    sget-object v1, Ldww;->d:Ldww;

    invoke-virtual {v0, v1}, Lduy;->a(Ldww;)V

    .line 887
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->g(Ldun;)Lduy;

    move-result-object v0

    invoke-virtual {v0, v2}, Lduy;->a(Ldwr;)V

    .line 888
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->g(Ldun;)Lduy;

    move-result-object v0

    sget-object v1, Ldww;->e:Ldww;

    invoke-virtual {v0, v1}, Lduy;->a(Ldww;)V

    goto/16 :goto_0

    .line 885
    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 893
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldut;

    .line 894
    iget-object v2, v0, Ldut;->a:Ldwr;

    .line 895
    if-eqz v2, :cond_f

    .line 896
    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-static {v1}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v3

    const-string v4, "Disconnecting from "

    invoke-virtual {v2}, Ldwr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_e

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v3, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 897
    iget-boolean v0, v0, Ldut;->b:Z

    .line 898
    if-eqz v0, :cond_c

    .line 899
    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-static {v1, v2}, Ldun;->b(Ldun;Ldwr;)V

    .line 901
    :cond_c
    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-static {v1}, Ldun;->h(Ldun;)Ldxx;

    move-result-object v1

    invoke-interface {v1}, Ldxx;->a()V

    .line 902
    invoke-virtual {v2}, Ldwr;->h()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 903
    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-static {v1}, Ldun;->i(Ldun;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v2}, Ldwr;->f()Ldwf;

    move-result-object v2

    iget-object v2, v2, Ldwf;->a:Ldtm;

    iget-object v2, v2, Ldtm;->e:Ldtj;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 905
    :cond_d
    iget-object v1, p0, Lduu;->a:Ldun;

    iget-object v2, p0, Lduu;->a:Ldun;

    invoke-static {v2}, Ldun;->b(Ldun;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldun;->a(Ldun;Landroid/content/Context;Z)V

    .line 911
    :goto_4
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0, v7}, Ldun;->b(Ldun;Z)Z

    .line 912
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0, v7}, Ldun;->a(Ldun;Z)Z

    .line 913
    iget-object v0, p0, Lduu;->a:Ldun;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ldun;->b(Ldun;Ldst;)Ldst;

    .line 915
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->g(Ldun;)Lduy;

    move-result-object v0

    sget-object v1, Ldww;->e:Ldww;

    invoke-virtual {v0, v1}, Lduy;->a(Ldww;)V

    goto/16 :goto_0

    .line 896
    :cond_e
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 907
    :cond_f
    iget-object v0, p0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    const-string v1, "Disconnecting from a yet unknown screen"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 908
    iget-object v0, p0, Lduu;->a:Ldun;

    iget-object v1, p0, Lduu;->a:Ldun;

    invoke-static {v1}, Ldun;->b(Ldun;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1, v7}, Ldun;->a(Ldun;Landroid/content/Context;Z)V

    goto :goto_4

    .line 797
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
