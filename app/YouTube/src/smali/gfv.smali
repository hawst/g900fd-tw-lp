.class public final Lgfv;
.super Lefm;
.source "SourceFile"


# instance fields
.field private final d:I

.field private final e:Z

.field private final f:Lezj;

.field private g:J

.field private h:Landroid/net/Uri;

.field private i:Lefg;

.field private j:Landroid/net/Uri;

.field private k:J

.field private l:Z


# direct methods
.method public constructor <init>(Lezj;Ljava/lang/String;Legt;Lega;IIIZ)V
    .locals 6

    .prologue
    .line 94
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lefm;-><init>(Ljava/lang/String;Legt;Lega;II)V

    .line 95
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgfv;->f:Lezj;

    .line 96
    iput p7, p0, Lgfv;->d:I

    .line 97
    iput-boolean p8, p0, Lgfv;->e:Z

    .line 98
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 177
    const/4 v0, 0x0

    iput-object v0, p0, Lgfv;->j:Landroid/net/Uri;

    .line 178
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgfv;->k:J

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgfv;->l:Z

    .line 180
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lgfv;->j:Landroid/net/Uri;

    .line 184
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgfv;->k:J

    .line 185
    iget-boolean v0, p0, Lgfv;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgfv;->l:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lgfv;->l:Z

    .line 186
    return-void

    .line 185
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a([BII)I
    .locals 8

    .prologue
    .line 148
    :try_start_0
    iget v0, p0, Lgfv;->d:I

    if-lez v0, :cond_0

    .line 149
    iget-object v0, p0, Lgfv;->f:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lgfv;->g:J

    sub-long v2, v0, v2

    .line 150
    iget v0, p0, Lgfv;->d:I

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 151
    new-instance v0, Lgfw;

    iget-object v1, p0, Lgfv;->i:Lefg;

    .line 152
    iget-wide v4, p0, Lefm;->c:J

    invoke-virtual {p0}, Lgfv;->b()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lgfw;-><init>(Lefg;JJJ)V

    throw v0
    :try_end_0
    .catch Lefo; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :catch_0
    move-exception v0

    .line 157
    invoke-direct {p0}, Lgfv;->d()V

    .line 158
    throw v0

    .line 155
    :cond_0
    :try_start_1
    invoke-super {p0, p1, p2, p3}, Lefm;->a([BII)I
    :try_end_1
    .catch Lefo; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    return v0
.end method

.method public final a(Lefg;)J
    .locals 9

    .prologue
    .line 102
    iget-object v0, p0, Lgfv;->f:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lgfv;->g:J

    .line 104
    iget-object v0, p0, Lgfv;->j:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lgfv;->g:J

    iget-wide v2, p0, Lgfv;->k:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 106
    invoke-direct {p0}, Lgfv;->c()V

    .line 108
    :cond_0
    iget-object v0, p1, Lefg;->a:Landroid/net/Uri;

    iget-object v1, p0, Lgfv;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 110
    invoke-direct {p0}, Lgfv;->c()V

    .line 111
    iget-object v0, p1, Lefg;->a:Landroid/net/Uri;

    iput-object v0, p0, Lgfv;->h:Landroid/net/Uri;

    .line 113
    :cond_1
    iget-object v0, p0, Lgfv;->j:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 114
    new-instance v0, Lefg;

    iget-object v1, p0, Lgfv;->j:Landroid/net/Uri;

    iget-wide v2, p1, Lefg;->c:J

    iget-wide v4, p1, Lefg;->e:J

    iget-object v6, p1, Lefg;->f:Ljava/lang/String;

    iget-wide v7, p1, Lefg;->d:J

    invoke-direct/range {v0 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;J)V

    .line 131
    :goto_0
    iput-object v0, p0, Lgfv;->i:Lefg;

    .line 133
    :try_start_0
    invoke-super {p0, v0}, Lefm;->a(Lefg;)J

    move-result-wide v0

    .line 134
    iget-object v2, p0, Lgfv;->j:Landroid/net/Uri;

    if-nez v2, :cond_2

    .line 135
    iget-object v2, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lgfv;->j:Landroid/net/Uri;

    .line 136
    iget-object v2, p0, Lgfv;->f:Lezj;

    invoke-virtual {v2}, Lezj;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lgfv;->k:J
    :try_end_0
    .catch Lefo; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_2
    return-wide v0

    .line 120
    :cond_3
    iget-boolean v0, p0, Lgfv;->l:Z

    if-eqz v0, :cond_4

    .line 121
    iget-object v0, p1, Lefg;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cmo"

    const-string v2, "pf=1"

    .line 122
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 123
    new-instance v0, Lefg;

    iget-wide v2, p1, Lefg;->c:J

    iget-wide v4, p1, Lefg;->e:J

    iget-object v6, p1, Lefg;->f:Ljava/lang/String;

    iget-wide v7, p1, Lefg;->d:J

    invoke-direct/range {v0 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;J)V

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    invoke-direct {p0}, Lgfv;->d()V

    .line 141
    throw v0

    :cond_4
    move-object v0, p1

    goto :goto_0
.end method

.method public final a()V
    .locals 6

    .prologue
    .line 165
    :try_start_0
    iget-object v0, p0, Lefm;->b:Ljava/net/HttpURLConnection;

    .line 166
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgfv;->b()J
    :try_end_0
    .catch Lefo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    const-wide/16 v4, 0x800

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 167
    :try_start_1
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.okhttp.internal.http.HttpTransport$FixedLengthInputStream"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "unexpectedEndOfInput"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lefo; {:try_start_1 .. :try_end_1} :catch_0

    .line 169
    :cond_0
    :goto_0
    :try_start_2
    invoke-super {p0}, Lefm;->a()V
    :try_end_2
    .catch Lefo; {:try_start_2 .. :try_end_2} :catch_0

    .line 173
    return-void

    .line 170
    :catch_0
    move-exception v0

    .line 171
    invoke-direct {p0}, Lgfv;->d()V

    .line 172
    throw v0

    :catch_1
    move-exception v0

    goto :goto_0

    .line 167
    :catch_2
    move-exception v0

    goto :goto_0
.end method
