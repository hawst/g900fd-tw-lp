.class public final Lamb;
.super Lann;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;
.implements Lgwb;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lacw;

.field private final c:Lgwd;

.field private final d:Lanc;

.field private final e:Laps;

.field private final f:Lapz;

.field private final g:Lapo;

.field private final h:Laoh;

.field private final i:Laqg;

.field private final j:Laon;

.field private final k:Laou;

.field private final l:Lapl;

.field private final m:Lapr;

.field private final n:Laql;

.field private final o:Laly;

.field private p:Laiw;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lgwd;Laco;Laiw;Lajf;Lajl;Lajo;Lait;Lain;Lajr;Laiq;Laiz;Lajc;Laji;Laju;Z)V
    .locals 16

    .prologue
    .line 89
    invoke-direct/range {p0 .. p0}, Lann;-><init>()V

    .line 90
    const-string v3, "context cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    const-string v3, "uiHandler cannot be null"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->a:Landroid/os/Handler;

    .line 92
    const-string v3, "serviceDestroyedNotifier"

    .line 93
    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lgwd;

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->c:Lgwd;

    .line 94
    const-string v3, "apiEnvironment cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    const-string v3, "apiPlayerClient cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laiw;

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->p:Laiw;

    .line 96
    const-string v3, "playerUiClient cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    if-nez p17, :cond_0

    .line 98
    const-string v3, "surfaceHolderClient cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    :goto_0
    const-string v3, "mediaViewClient cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const-string v3, "adOverlayClient cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    const-string v3, "annotationOverlayClient cannot be null"

    move-object/from16 v0, p12

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string v3, "controlsOverlayClient cannot be null"

    move-object/from16 v0, p13

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v3, "liveOverlayClient cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    const-string v3, "subtitlesOverlayClient cannot be null"

    move-object/from16 v0, p15

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    const-string v3, "thumbnailOverlayClient cannot be null"

    move-object/from16 v0, p16

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    new-instance v3, Lapo;

    move-object/from16 v0, p2

    move-object/from16 v1, p6

    invoke-direct {v3, v0, v1}, Lapo;-><init>(Landroid/os/Handler;Lajf;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->g:Lapo;

    .line 114
    new-instance v3, Laoh;

    move-object/from16 v0, p2

    move-object/from16 v1, p10

    invoke-direct {v3, v0, v1}, Laoh;-><init>(Landroid/os/Handler;Lain;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->h:Laoh;

    .line 115
    new-instance v3, Laqg;

    move-object/from16 v0, p2

    move-object/from16 v1, p11

    invoke-direct {v3, v0, v1}, Laqg;-><init>(Landroid/os/Handler;Lajr;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->i:Laqg;

    .line 116
    new-instance v3, Laon;

    move-object/from16 v0, p2

    move-object/from16 v1, p12

    invoke-direct {v3, v0, v1}, Laon;-><init>(Landroid/os/Handler;Laiq;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->j:Laon;

    .line 117
    new-instance v3, Laou;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p13

    invoke-direct {v3, v0, v1, v2}, Laou;-><init>(Landroid/content/Context;Landroid/os/Handler;Laiz;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->k:Laou;

    .line 118
    new-instance v3, Lapl;

    move-object/from16 v0, p2

    move-object/from16 v1, p14

    invoke-direct {v3, v0, v1}, Lapl;-><init>(Landroid/os/Handler;Lajc;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->l:Lapl;

    .line 119
    new-instance v3, Lapr;

    move-object/from16 v0, p15

    invoke-direct {v3, v0}, Lapr;-><init>(Laji;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->m:Lapr;

    .line 120
    new-instance v3, Laql;

    move-object/from16 v0, p16

    invoke-direct {v3, v0}, Laql;-><init>(Laju;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->n:Laql;

    .line 121
    if-nez p17, :cond_1

    .line 122
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->f:Lapz;

    .line 123
    new-instance v3, Laps;

    move-object/from16 v0, p2

    move-object/from16 v1, p7

    invoke-direct {v3, v0, v1}, Laps;-><init>(Landroid/os/Handler;Lajl;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->e:Laps;

    .line 124
    new-instance v3, Lapy;

    move-object/from16 v0, p0

    iget-object v4, v0, Lamb;->e:Laps;

    move-object/from16 v0, p9

    invoke-direct {v3, v4, v0}, Lapy;-><init>(Landroid/view/SurfaceHolder;Lait;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->o:Laly;

    .line 131
    :goto_1
    new-instance v3, Lanc;

    move-object/from16 v0, p5

    invoke-direct {v3, v0}, Lanc;-><init>(Laiw;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->d:Lanc;

    .line 133
    new-instance v3, Lacw;

    move-object/from16 v0, p0

    iget-object v5, v0, Lamb;->d:Lanc;

    move-object/from16 v0, p0

    iget-object v7, v0, Lamb;->g:Lapo;

    move-object/from16 v0, p0

    iget-object v8, v0, Lamb;->o:Laly;

    move-object/from16 v0, p0

    iget-object v9, v0, Lamb;->h:Laoh;

    move-object/from16 v0, p0

    iget-object v10, v0, Lamb;->i:Laqg;

    move-object/from16 v0, p0

    iget-object v11, v0, Lamb;->j:Laon;

    move-object/from16 v0, p0

    iget-object v12, v0, Lamb;->k:Laou;

    move-object/from16 v0, p0

    iget-object v13, v0, Lamb;->l:Lapl;

    move-object/from16 v0, p0

    iget-object v14, v0, Lamb;->m:Lapr;

    move-object/from16 v0, p0

    iget-object v15, v0, Lamb;->n:Laql;

    move-object/from16 v4, p1

    move-object/from16 v6, p4

    invoke-direct/range {v3 .. v15}, Lacw;-><init>(Landroid/content/Context;Lacy;Laco;Lcwz;Lgec;Ldbm;Ldff;Ldfu;Ldbr;Lddb;Ldeo;Ldfm;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->b:Lacw;

    .line 147
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lgwd;->a(Lgwb;)V

    .line 149
    :try_start_0
    invoke-interface/range {p5 .. p5}, Laiw;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-interface {v3, v0, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_2
    return-void

    .line 100
    :cond_0
    const-string v3, "surfaceTextureClient cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v3}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 126
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->e:Laps;

    .line 127
    new-instance v3, Lapz;

    move-object/from16 v0, p2

    move-object/from16 v1, p8

    invoke-direct {v3, v0, v1}, Lapz;-><init>(Landroid/os/Handler;Lajo;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->f:Lapz;

    .line 128
    new-instance v3, Laqk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lamb;->f:Lapz;

    move-object/from16 v0, p9

    invoke-direct {v3, v4, v0}, Laqk;-><init>(Lapz;Lait;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lamb;->o:Laly;

    goto :goto_1

    .line 152
    :catch_0
    move-exception v3

    invoke-virtual/range {p0 .. p0}, Lamb;->binderDied()V

    goto :goto_2
.end method

.method static synthetic a(Lamb;)Lacw;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lamb;->b:Lacw;

    return-object v0
.end method

.method static synthetic a(Lamb;Z)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lamb;->f(Z)V

    return-void
.end method

.method private f(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 160
    iget-object v0, p0, Lamb;->b:Lacw;

    invoke-virtual {v0}, Lacw;->j()V

    .line 161
    iget-object v0, p0, Lamb;->c:Lgwd;

    invoke-virtual {v0, p0}, Lgwd;->b(Lgwb;)V

    .line 162
    iget-object v0, p0, Lamb;->p:Laiw;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lamb;->p:Laiw;

    invoke-interface {v0}, Laiw;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 164
    iput-object v2, p0, Lamb;->p:Laiw;

    .line 167
    :cond_0
    iget-object v0, p0, Lamb;->d:Lanc;

    iput-object v2, v0, Lanc;->a:Laiw;

    .line 168
    iget-object v0, p0, Lamb;->e:Laps;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lamb;->e:Laps;

    iput-object v2, v0, Laps;->a:Lajl;

    .line 171
    :cond_1
    iget-object v0, p0, Lamb;->f:Lapz;

    if-eqz v0, :cond_2

    .line 172
    iget-object v0, p0, Lamb;->f:Lapz;

    .line 174
    :cond_2
    iget-object v0, p0, Lamb;->g:Lapo;

    iput-object v2, v0, Lapo;->a:Lajf;

    .line 175
    iget-object v0, p0, Lamb;->h:Laoh;

    iput-object v2, v0, Laoh;->a:Lain;

    .line 176
    iget-object v0, p0, Lamb;->k:Laou;

    iput-object v2, v0, Laou;->a:Laiz;

    .line 177
    iget-object v0, p0, Lamb;->l:Lapl;

    iput-object v2, v0, Lapl;->a:Lajc;

    .line 178
    iget-object v0, p0, Lamb;->m:Lapr;

    iput-object v2, v0, Lapr;->a:Laji;

    .line 179
    iget-object v0, p0, Lamb;->n:Laql;

    iput-object v2, v0, Laql;->a:Laju;

    .line 180
    iget-object v0, p0, Lamb;->o:Laly;

    invoke-virtual {v0}, Laly;->g()V

    iput-object v2, v0, Laly;->a:Lait;

    .line 184
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 185
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lamb;->f(Z)V

    .line 191
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamh;

    invoke-direct {v1, p0, p1}, Lamh;-><init>(Lamb;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 341
    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)V
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamq;

    invoke-direct {v1, p0, p1, p2}, Lamq;-><init>(Lamb;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 421
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamc;

    invoke-direct {v1, p0, p1, p2}, Lamc;-><init>(Lamb;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 206
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamv;

    invoke-direct {v1, p0, p1, p2, p3}, Lamv;-><init>(Lamb;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 226
    return-void
.end method

.method public final a(Ljava/util/List;II)V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamx;

    invoke-direct {v1, p0, p1, p2, p3}, Lamx;-><init>(Lamb;Ljava/util/List;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 246
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lame;

    invoke-direct {v1, p0, p1}, Lame;-><init>(Lamb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 311
    return-void
.end method

.method public final a([B)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 457
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 458
    array-length v0, p1

    invoke-virtual {v3, p1, v1, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 459
    invoke-virtual {v3, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 460
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 461
    if-eq v0, v2, :cond_0

    .line 462
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    move v0, v1

    .line 484
    :goto_0
    return v0

    .line 467
    :cond_0
    :try_start_0
    const-class v0, Ldlf;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldlf;
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 474
    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    .line 476
    iget-object v3, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v4, Lamt;

    invoke-direct {v4, p0, v0, v1}, Lamt;-><init>(Lamb;Ldlf;Landroid/os/ConditionVariable;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 483
    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    move v0, v2

    .line 484
    goto :goto_0

    .line 469
    :catch_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamz;

    invoke-direct {v1, p0}, Lamz;-><init>(Lamb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 266
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lami;

    invoke-direct {v1, p0, p1}, Lami;-><init>(Lamb;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 351
    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamr;

    invoke-direct {v1, p0, p1, p2}, Lamr;-><init>(Lamb;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 431
    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamn;

    invoke-direct {v1, p0, p1, p2}, Lamn;-><init>(Lamb;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 216
    return-void
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamw;

    invoke-direct {v1, p0, p1, p2, p3}, Lamw;-><init>(Lamb;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 236
    return-void
.end method

.method public final b(Ljava/util/List;II)V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamy;

    invoke-direct {v1, p0, p1, p2, p3}, Lamy;-><init>(Lamb;Ljava/util/List;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 256
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamj;

    invoke-direct {v1, p0, p1}, Lamj;-><init>(Lamb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 361
    return-void
.end method

.method public final binderDied()V
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lamb;->a(Z)V

    .line 196
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lana;

    invoke-direct {v1, p0}, Lana;-><init>(Lamb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 276
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamk;

    invoke-direct {v1, p0, p1}, Lamk;-><init>(Lamb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 371
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lanb;

    invoke-direct {v1, p0}, Lanb;-><init>(Lamb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 286
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamm;

    invoke-direct {v1, p0, p1}, Lamm;-><init>(Lamb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 391
    return-void
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamp;

    invoke-direct {v1, p0, p1}, Lamp;-><init>(Lamb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 411
    return-void
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 290
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    .line 291
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 292
    iget-object v2, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v3, Lamd;

    invoke-direct {v3, p0, v1, v0}, Lamd;-><init>(Lamb;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 299
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 300
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamf;

    invoke-direct {v1, p0}, Lamf;-><init>(Lamb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 321
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamg;

    invoke-direct {v1, p0}, Lamg;-><init>(Lamb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 331
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Laml;

    invoke-direct {v1, p0}, Laml;-><init>(Lamb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 381
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamo;

    invoke-direct {v1, p0}, Lamo;-><init>(Lamb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 401
    return-void
.end method

.method public final j()[B
    .locals 4

    .prologue
    .line 435
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 437
    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    .line 438
    iget-object v2, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v3, Lams;

    invoke-direct {v3, p0, v0, v1}, Lams;-><init>(Lamb;Ljava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 445
    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    .line 447
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 448
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 449
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 450
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    .line 451
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 452
    return-object v0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 489
    iget-object v0, p0, Lamb;->a:Landroid/os/Handler;

    new-instance v1, Lamu;

    invoke-direct {v1, p0}, Lamu;-><init>(Lamb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 495
    return-void
.end method
