.class public final Leqv;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Intent;

.field private b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Leqv;->a:Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "android.intent.action.SEND_MULTIPLE"

    iget-object v3, p0, Leqv;->a:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v4, "com.google.android.apps.plus.GOOGLE_INTERACTIVE_POST"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v0, "Call-to-action buttons are only available for URLs."

    invoke-static {v2, v0}, Lb;->a(ZLjava/lang/Object;)V

    if-eqz v4, :cond_0

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v5, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    const-string v5, "The content URL is required for interactive posts."

    invoke-static {v0, v5}, Lb;->a(ZLjava/lang/Object;)V

    if-eqz v4, :cond_1

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v4, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v4, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    :goto_1
    const-string v4, "Must set content URL or content deep-link ID to use a call-to-action button."

    invoke-static {v0, v4}, Lb;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v4, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v4, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v0, "GooglePlusPlatform"

    const-string v2, "The provided deep-link ID is empty."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    const-string v0, "The specified deep-link ID was malformed."

    invoke-static {v1, v0}, Lb;->a(ZLjava/lang/Object;)V

    :cond_2
    if-eqz v3, :cond_3

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Leqv;->b:Ljava/util/ArrayList;

    :cond_3
    const-string v0, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    iget-object v1, p0, Leqv;->a:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    :goto_3
    return-object v0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "GooglePlusPlatform"

    const-string v2, "Spaces are not allowed in deep-link IDs."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_2

    :cond_8
    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    goto :goto_3

    :cond_9
    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.apps.plus"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    goto :goto_3
.end method

.method public final a(I)Leqv;
    .locals 2

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.INTERNAL_APP_ICON_RESOURCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Leqv;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :goto_0
    return-object p0

    :cond_1
    iget-object v1, p0, Leqv;->a:Landroid/content/Intent;

    const-string v2, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Leqv;
    .locals 2

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.INTERNAL_APP_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final a(Z)Leqv;
    .locals 3

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.SHARE_ON_PLUS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Leqv;
    .locals 2

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.INTERNAL_SHARE_BUTTON_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Leqv;
    .locals 2

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.SHARE_CONTEXT_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Leqv;
    .locals 2

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Leqv;
    .locals 2

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Leqv;
    .locals 2

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_DISPLAY_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final g(Ljava/lang/String;)Leqv;
    .locals 2

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_IMAGE_URL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final h(Ljava/lang/String;)Leqv;
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "clientApplicationId must not be empty."

    invoke-static {v0, v1}, Lb;->b(ZLjava/lang/Object;)V

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Leqv;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_APPLICATION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "clientApplicationId must be parsable to an int."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
