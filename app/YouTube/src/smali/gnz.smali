.class public Lgnz;
.super Lexx;
.source "SourceFile"


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field private final c:Lgjp;

.field private final d:Lgll;

.field private final e:Lexz;

.field private final f:Lezj;

.field private final g:Lght;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lgnz;

    .line 28
    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgnz;->b:Ljava/lang/String;

    .line 27
    return-void
.end method

.method protected constructor <init>(Lead;Lgjp;Lgll;Lexz;Lezj;Lght;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lexx;-><init>(Lead;)V

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lgnz;->c:Lgjp;

    .line 48
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgll;

    iput-object v0, p0, Lgnz;->d:Lgll;

    .line 49
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexz;

    iput-object v0, p0, Lgnz;->e:Lexz;

    .line 50
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgnz;->f:Lezj;

    .line 51
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lght;

    iput-object v0, p0, Lgnz;->g:Lght;

    .line 52
    return-void
.end method

.method static synthetic a(Lgnz;)Lgll;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lgnz;->d:Lgll;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 56
    iget-object v0, p0, Lgnz;->g:Lght;

    invoke-virtual {v0}, Lght;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lgnz;->c:Lgjp;

    const-string v0, "delayed_request"

    const v1, 0x323467f

    .line 62
    invoke-static {v0, v1}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v1

    .line 65
    iput-boolean v11, v1, Lgjt;->d:Z

    .line 66
    iget-object v2, p0, Lgnz;->d:Lgll;

    const-string v0, "https://www.youtube.com/gen_204"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v3

    iget-object v0, v2, Lgll;->d:Lggr;

    invoke-virtual {v0, v3}, Lggr;->a(Lfao;)Lfao;

    const-string v0, "a"

    const-string v4, "delayed_request"

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "batch_size"

    iget-object v4, v2, Lgll;->c:Lght;

    invoke-virtual {v4}, Lght;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "max_queue_size"

    iget-object v4, v2, Lgll;->c:Lght;

    invoke-virtual {v4}, Lght;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "max_age_hours"

    iget-object v4, v2, Lgll;->c:Lght;

    invoke-virtual {v4}, Lght;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "age_of_oldest_request_hours"

    iget-object v4, v2, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v5, "age_of_oldest_request_hours"

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "current_queue_size"

    iget-object v4, v2, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v5, "current_queue_size"

    invoke-interface {v4, v5, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "peak_queue_size"

    iget-object v4, v2, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v5, "peak_queue_size"

    invoke-interface {v4, v5, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "total_enqueued_requests"

    iget-object v4, v2, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v5, "total_enqueued_requests"

    invoke-interface {v4, v5, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "total_successful_requests"

    iget-object v4, v2, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v5, "total_successful_requests"

    invoke-interface {v4, v5, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "total_server_http_errors"

    iget-object v4, v2, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v5, "total_server_http_errors"

    invoke-interface {v4, v5, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "total_client_http_errors"

    iget-object v4, v2, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v5, "total_client_http_errors"

    invoke-interface {v4, v5, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "report_cap_hours"

    iget-object v4, v2, Lgll;->c:Lght;

    invoke-virtual {v4}, Lght;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    iget-object v0, v2, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v4, "total_sent_requests"

    invoke-interface {v0, v4, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-double v4, v0

    iget-object v0, v2, Lgll;->b:Landroid/content/SharedPreferences;

    const-string v6, "total_dropped_requests"

    invoke-interface {v0, v6, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-double v6, v0

    const-wide/16 v8, 0x0

    cmpl-double v0, v4, v8

    if-lez v0, :cond_1

    div-double v4, v6, v4

    const-string v0, "dropped_vs_sent_ratio"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%1$,.2f"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v8, v10

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    :cond_1
    invoke-virtual {v2}, Lgll;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "sent_requests_%s"

    new-array v7, v11, [Ljava/lang/Object;

    aput-object v0, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v2, Lgll;->b:Landroid/content/SharedPreferences;

    invoke-interface {v6, v5, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    if-lez v6, :cond_3

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    :cond_3
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "dropped_requests_%s"

    new-array v7, v11, [Ljava/lang/Object;

    aput-object v0, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, v2, Lgll;->b:Landroid/content/SharedPreferences;

    invoke-interface {v5, v0, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-lez v5, :cond_2

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    goto :goto_1

    :cond_4
    iget-object v0, v3, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    .line 68
    new-instance v0, Lgoa;

    invoke-direct {v0, p0}, Lgoa;-><init>(Lgnz;)V

    iput-object v0, v1, Lgjt;->h:Lgjr;

    .line 79
    iget-object v0, p0, Lgnz;->c:Lgjp;

    sget-object v2, Lggu;->b:Lwu;

    invoke-virtual {v0, v1, v2}, Lgjp;->a(Lgjt;Lwu;)V

    .line 84
    iget-object v0, p0, Lgnz;->f:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x1e

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lgnz;->g:Lght;

    .line 85
    invoke-virtual {v3}, Lght;->e()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 86
    iget-object v2, p0, Lgnz;->e:Lexz;

    iget-object v3, p0, Lgnz;->g:Lght;

    .line 87
    invoke-static {v0, v1, v3}, Lgob;->a(JLght;)Lead;

    move-result-object v0

    .line 86
    invoke-virtual {v2, v0}, Lexz;->c(Lead;)V

    goto/16 :goto_0
.end method
