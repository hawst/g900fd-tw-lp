.class public final Lawl;
.super Lawp;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private final e:I

.field private final f:I

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lawp;-><init>(Landroid/content/Context;I)V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lawl;->e:I

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lawl;->f:I

    .line 33
    const/4 v0, 0x2

    iput v0, p0, Lawl;->a:I

    .line 34
    const/4 v0, 0x3

    iput v0, p0, Lawl;->b:I

    .line 35
    const/4 v0, 0x4

    iput v0, p0, Lawl;->c:I

    .line 36
    return-void
.end method


# virtual methods
.method public final a(I)Lgbu;
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lawl;->c:I

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lawl;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 50
    :cond_0
    const/4 v0, 0x0

    .line 52
    :goto_0
    return-object v0

    :cond_1
    iget v0, p0, Lawl;->c:I

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lawp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lawl;->g:Z

    .line 57
    invoke-virtual {p0}, Lawl;->notifyDataSetChanged()V

    .line 58
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0}, Lawp;->b()V

    .line 143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lawl;->g:Z

    invoke-virtual {p0}, Lawl;->notifyDataSetChanged()V

    .line 144
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lawl;->b:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 97
    if-ne p1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)Z
    .locals 1

    .prologue
    .line 101
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(I)Z
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lawl;->a:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 40
    iget-boolean v0, p0, Lawl;->g:Z

    if-eqz v0, :cond_0

    .line 41
    invoke-super {p0}, Lawp;->getCount()I

    move-result v0

    iget v1, p0, Lawl;->c:I

    add-int/2addr v0, v1

    .line 43
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lawp;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lawl;->a(I)Lgbu;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 67
    invoke-virtual {p0, p2, p3}, Lawl;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 68
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawq;

    .line 69
    iget-object v3, v0, Lawq;->c:Landroid/view/View;

    if-nez p1, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 71
    iget v1, p0, Lawl;->c:I

    if-lt p1, v1, :cond_1

    .line 72
    iget v0, p0, Lawl;->c:I

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lawp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    invoke-virtual {p0, v0, v2, p3}, Lawl;->a(Lgbu;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 89
    :goto_1
    return-object v0

    .line 69
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {p0, p1}, Lawl;->d(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 76
    iget-object v1, v0, Lawq;->a:Landroid/widget/TextView;

    const v3, 0x7f09026a

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 77
    iget-object v0, v0, Lawq;->b:Landroid/widget/ImageView;

    const v1, 0x7f020133

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    :goto_2
    move-object v0, v2

    .line 89
    goto :goto_1

    .line 78
    :cond_3
    invoke-virtual {p0, p1}, Lawl;->c(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 79
    iget-object v1, v0, Lawq;->a:Landroid/widget/TextView;

    const v3, 0x7f090269

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 80
    iget-object v0, v0, Lawq;->b:Landroid/widget/ImageView;

    const v1, 0x7f020130

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 81
    :cond_4
    invoke-virtual {p0, p1}, Lawl;->e(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 82
    iget-object v1, v0, Lawq;->a:Landroid/widget/TextView;

    const v3, 0x7f0901a6

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 83
    iget-object v0, v0, Lawq;->b:Landroid/widget/ImageView;

    const v1, 0x7f020131

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 84
    :cond_5
    invoke-virtual {p0, p1}, Lawl;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 85
    iget-object v1, v0, Lawq;->a:Landroid/widget/TextView;

    const v3, 0x7f0902d6

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 86
    iget-object v0, v0, Lawq;->b:Landroid/widget/ImageView;

    const v1, 0x7f02012f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method
