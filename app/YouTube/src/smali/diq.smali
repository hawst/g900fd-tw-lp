.class public Ldiq;
.super Lexx;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Ldhy;

.field private d:J

.field private e:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Ldiq;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldiq;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lead;Ldhy;)V
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    .line 46
    invoke-direct {p0, p1}, Lexx;-><init>(Lead;)V

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldhy;

    iput-object v0, p0, Ldiq;->c:Ldhy;

    .line 49
    const/4 v0, 0x0

    .line 51
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 52
    iget-object v1, p1, Lead;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v2, v4

    move-object v6, v0

    move-wide v8, v4

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leac;

    .line 53
    iget-object v1, v0, Leac;->a:Ljava/lang/String;

    const-string v11, "prewarmMillis"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    :try_start_0
    iget-object v0, v0, Leac;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    move-wide v8, v0

    .line 59
    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    const-string v1, "Wrong prewarmMillis format in ScheduledPrewarmTask."

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v8, v4

    .line 59
    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, v0, Leac;->a:Ljava/lang/String;

    const-string v11, "duration"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 62
    :try_start_1
    iget-object v0, v0, Leac;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v0

    move-wide v2, v0

    .line 66
    goto :goto_0

    .line 63
    :catch_1
    move-exception v0

    .line 64
    const-string v1, "Wrong duration format in ScheduledPrewarmTask."

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v2, v4

    .line 66
    goto :goto_0

    .line 67
    :cond_2
    iget-object v1, v0, Leac;->a:Ljava/lang/String;

    const-string v11, "videoId"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 68
    iget-object v0, v0, Leac;->b:Ljava/lang/String;

    move-object v6, v0

    goto :goto_0

    .line 69
    :cond_3
    iget-object v1, v0, Leac;->a:Ljava/lang/String;

    const-string v11, "formatStream"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    :try_start_2
    new-instance v1, Lhgy;

    invoke-direct {v1}, Lhgy;-><init>()V

    .line 73
    iget-object v0, v0, Leac;->b:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v0, v11}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 72
    invoke-static {v1, v0}, Lidh;->a(Lidh;[B)Lidh;

    .line 74
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lidg; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 75
    :catch_2
    move-exception v0

    .line 76
    const-string v1, "Wrong formatStream in ScheduledPrewarmTask."

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 80
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldiq;->e:Ljava/util/List;

    .line 81
    cmp-long v0, v8, v4

    if-lez v0, :cond_6

    .line 82
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    cmp-long v0, v2, v4

    if-lez v0, :cond_6

    .line 84
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 85
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgy;

    .line 86
    iget-object v4, p0, Ldiq;->e:Ljava/util/List;

    new-instance v5, Lfqj;

    invoke-direct {v5, v0, v6, v2, v3}, Lfqj;-><init>(Lhgy;Ljava/lang/String;J)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 88
    :cond_5
    iput-wide v8, p0, Ldiq;->d:J

    .line 93
    :goto_2
    return-void

    .line 91
    :cond_6
    iput-wide v4, p0, Ldiq;->d:J

    goto :goto_2
.end method

.method public static a(JLjava/lang/String;J[Lfqj;J)Lead;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 105
    new-instance v0, Lead;

    invoke-direct {v0}, Lead;-><init>()V

    sget-object v2, Ldiq;->b:Ljava/lang/String;

    .line 106
    invoke-virtual {v0, v2}, Lead;->a(Ljava/lang/String;)Lead;

    move-result-object v0

    .line 107
    invoke-virtual {v0, p0, p1}, Lead;->a(J)Lead;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 108
    invoke-virtual {v0, v2, v3}, Lead;->b(J)Lead;

    move-result-object v0

    new-instance v2, Leac;

    invoke-direct {v2}, Leac;-><init>()V

    const-string v3, "videoId"

    .line 110
    invoke-virtual {v2, v3}, Leac;->a(Ljava/lang/String;)Leac;

    move-result-object v2

    invoke-virtual {v2, p2}, Leac;->b(Ljava/lang/String;)Leac;

    move-result-object v2

    .line 109
    invoke-virtual {v0, v2}, Lead;->a(Leac;)Lead;

    move-result-object v0

    new-instance v2, Leac;

    invoke-direct {v2}, Leac;-><init>()V

    const-string v3, "duration"

    .line 113
    invoke-virtual {v2, v3}, Leac;->a(Ljava/lang/String;)Leac;

    move-result-object v2

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Leac;->b(Ljava/lang/String;)Leac;

    move-result-object v2

    .line 111
    invoke-virtual {v0, v2}, Lead;->a(Leac;)Lead;

    move-result-object v0

    new-instance v2, Leac;

    invoke-direct {v2}, Leac;-><init>()V

    const-string v3, "prewarmMillis"

    .line 116
    invoke-virtual {v2, v3}, Leac;->a(Ljava/lang/String;)Leac;

    move-result-object v2

    invoke-static {p6, p7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Leac;->b(Ljava/lang/String;)Leac;

    move-result-object v2

    .line 114
    invoke-virtual {v0, v2}, Lead;->a(Leac;)Lead;

    move-result-object v2

    .line 117
    array-length v3, p5

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p5, v0

    .line 118
    new-instance v5, Leac;

    invoke-direct {v5}, Leac;-><init>()V

    const-string v6, "formatStream"

    .line 120
    invoke-virtual {v5, v6}, Leac;->a(Ljava/lang/String;)Leac;

    move-result-object v5

    .line 123
    invoke-virtual {v4}, Lfqj;->a()Lhgy;

    move-result-object v4

    invoke-static {v4}, Lidh;->a(Lidh;)[B

    move-result-object v4

    .line 122
    invoke-static {v4, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    .line 121
    invoke-virtual {v5, v4}, Leac;->b(Ljava/lang/String;)Leac;

    move-result-object v4

    .line 118
    invoke-virtual {v2, v4}, Lead;->a(Leac;)Lead;

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    :cond_0
    return-object v2
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Ldiq;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 132
    iget-object v0, p0, Ldiq;->e:Ljava/util/List;

    iget-object v0, p0, Ldiq;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v4, p0, Ldiq;->d:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    :try_start_0
    iget-object v3, p0, Ldiq;->c:Ldhy;

    iget-object v0, p0, Ldiq;->e:Ljava/util/List;

    iget-wide v4, p0, Ldiq;->d:J

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-wide v8, v0, Lfqj;->c:J

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    iget-object v7, v3, Ldhy;->c:Levn;

    new-instance v10, Ldin;

    invoke-direct {v10, v0}, Ldin;-><init>(Lfqj;)V

    invoke-virtual {v7, v10}, Levn;->c(Ljava/lang/Object;)V

    iget-object v7, v3, Ldhy;->a:Ldif;

    invoke-virtual {v7}, Ldif;->a()Ldix;

    move-result-object v7

    new-instance v10, Ldhz;

    iget-object v11, v3, Ldhy;->c:Levn;

    invoke-direct {v10, v7, v11}, Ldhz;-><init>(Ldix;Levn;)V

    iput-object v10, v7, Ldix;->c:Ldiy;

    iget-object v11, v3, Ldhy;->c:Levn;

    invoke-virtual {v11, v10}, Levn;->a(Ljava/lang/Object;)V

    iget-object v11, v0, Lfqj;->b:Ljava/lang/String;

    iget-object v12, v3, Ldhy;->b:Lcws;

    invoke-virtual {v12}, Lcws;->e()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    iget-object v11, v3, Ldhy;->b:Lcws;

    invoke-virtual {v11}, Lcws;->k()Z

    move-result v11

    if-nez v11, :cond_4

    invoke-virtual {v7, v0, v8, v9}, Ldix;->a(Lfqj;J)V

    iget-boolean v0, v7, Ldix;->d:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_1
    iget-object v7, v3, Ldhy;->c:Levn;

    invoke-virtual {v7, v10}, Levn;->b(Ljava/lang/Object;)V

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    if-nez v0, :cond_2

    :cond_3
    iget-object v0, v3, Ldhy;->c:Levn;

    new-instance v1, Ldij;

    invoke-direct {v1}, Ldij;-><init>()V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 138
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 139
    const-string v3, "Fail to prewarm video "

    iget-object v0, p0, Ldiq;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v0, v0, Lfqj;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 137
    :cond_4
    :try_start_1
    iget-object v0, v3, Ldhy;->c:Levn;

    new-instance v7, Ldig;

    invoke-direct {v7}, Ldig;-><init>()V

    invoke-virtual {v0, v7}, Levn;->c(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    .line 139
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 140
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 141
    const-string v3, "Fail to prewarm video "

    iget-object v0, p0, Ldiq;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v0, v0, Lfqj;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-static {v0, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_8
    move v0, v2

    goto :goto_1
.end method
