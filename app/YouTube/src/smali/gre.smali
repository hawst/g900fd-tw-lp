.class final Lgre;
.super Lgrb;
.source "SourceFile"


# instance fields
.field private final c:J

.field private final d:Lgrf;


# direct methods
.method public constructor <init>(JLgrf;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Lgrb;-><init>()V

    .line 104
    iput-wide p1, p0, Lgre;->c:J

    .line 105
    iput-object p3, p0, Lgre;->d:Lgrf;

    .line 106
    return-void
.end method


# virtual methods
.method public final a(Lgqn;Lgqg;)V
    .locals 3

    .prologue
    .line 111
    iget-wide v0, p0, Lgre;->c:J

    iget-object v2, p0, Lgre;->d:Lgrf;

    iget v2, v2, Lgrf;->c:I

    invoke-virtual {p1, v0, v1, v2}, Lgqn;->a(JI)V

    .line 112
    invoke-interface {p2}, Lgqg;->d()V

    .line 113
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 118
    const-string v0, "SeekCommand @ %,d %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lgre;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lgre;->d:Lgrf;

    invoke-virtual {v3}, Lgrf;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
