.class public abstract Lawb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lawm;


# static fields
.field private static b:Landroid/os/Handler;


# instance fields
.field a:Lawd;

.field private final c:Landroid/content/Context;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/view/animation/Animation;

.field private final f:I

.field private g:Lawc;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lawb;->h:Z

    .line 52
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lawb;->c:Landroid/content/Context;

    .line 54
    const/high16 v0, 0x10a0000

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lawb;->e:Landroid/view/animation/Animation;

    .line 55
    iget-object v0, p0, Lawb;->e:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lawb;->f:I

    .line 58
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lawb;->d:Landroid/widget/ImageView;

    .line 59
    sget-object v0, Lawb;->b:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lawb;->b:Landroid/os/Handler;

    .line 62
    :cond_0
    return-void
.end method

.method static synthetic a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lawb;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lawb;)Lawc;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lawb;->g:Lawc;

    return-object v0
.end method

.method static synthetic a(Lawb;Lawc;)Lawc;
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lawb;->g:Lawc;

    return-object v0
.end method

.method static synthetic a(Lawb;Z)Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lawb;->h:Z

    return v0
.end method

.method static synthetic b(Lawb;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lawb;->h:Z

    return v0
.end method

.method static synthetic c(Lawb;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lawb;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lawb;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lawb;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lawb;)Lawd;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lawb;->a:Lawd;

    return-object v0
.end method

.method static synthetic f(Lawb;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lawb;->e:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic g(Lawb;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lawb;->f:I

    return v0
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 70
    new-instance v0, Lawc;

    invoke-direct {v0, p0, p0}, Lawc;-><init>(Lawb;Lawb;)V

    iput-object v0, p0, Lawb;->g:Lawc;

    .line 71
    iget-object v0, p0, Lawb;->d:Landroid/widget/ImageView;

    sget-object v1, Lawb;->b:Landroid/os/Handler;

    iget-object v2, p0, Lawb;->g:Lawc;

    invoke-static {v1, v2}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v1

    invoke-virtual {p0, p2, v0, v1}, Lawb;->a(Ljava/lang/Object;Landroid/view/View;Leuc;)V

    .line 72
    iget-object v0, p0, Lawb;->g:Lawc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lawb;->g:Lawc;

    iget-boolean v0, v0, Lawc;->b:Z

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lawb;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 74
    iget-object v0, p0, Lawb;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 75
    iget-object v0, p0, Lawb;->g:Lawc;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lawc;->a:Z

    .line 77
    :cond_0
    iget-object v0, p0, Lawb;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected a(Landroid/graphics/Matrix;Landroid/widget/ImageView;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method protected abstract a(Ljava/lang/Object;Landroid/view/View;Leuc;)V
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lawb;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 94
    iget-object v0, p0, Lawb;->a:Lawd;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lawb;->a:Lawd;

    invoke-interface {v0}, Lawd;->b()V

    .line 97
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 89
    return-void
.end method
