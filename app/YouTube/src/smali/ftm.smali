.class public abstract Lftm;
.super Lfsp;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/Class;

.field private c:Lidh;


# direct methods
.method public constructor <init>(Lfsz;Lgit;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lfsp;-><init>(Lfsz;Lgit;)V

    .line 31
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lftm;->a:Ljava/lang/String;

    .line 32
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lftm;->b:Ljava/lang/Class;

    .line 33
    return-void
.end method

.method private n()Lidh;
    .locals 2

    .prologue
    .line 84
    :try_start_0
    iget-object v0, p0, Lftm;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidh;
    :try_end_0
    .catch Ljava/lang/ReflectiveOperationException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 86
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MessageNano object can\'t be instantiated (should never happen)"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private o()Lidh;
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0}, Lftm;->n()Lidh;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lftm;->c:Lidh;

    if-eqz v1, :cond_0

    .line 100
    :try_start_0
    iget-object v1, p0, Lftm;->c:Lidh;

    invoke-static {v1}, Lidh;->a(Lidh;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lidh;->a(Lidh;[B)Lidh;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :cond_0
    return-object v0

    .line 102
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MessageNano serialization is broken (should never happen)"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lftm;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lidh;)V
    .locals 1

    .prologue
    .line 61
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidh;

    iput-object v0, p0, Lftm;->c:Lidh;

    .line 62
    return-void
.end method

.method protected abstract a(Lidh;Lhjx;)V
.end method

.method public final c()Lidh;
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Lftm;->o()Lidh;

    move-result-object v0

    .line 51
    invoke-virtual {p0}, Lftm;->i()Lhjx;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lftm;->a(Lidh;Lhjx;)V

    .line 52
    return-object v0
.end method

.method public final m()Lidh;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lftm;->c:Lidh;

    if-nez v0, :cond_0

    .line 72
    invoke-direct {p0}, Lftm;->n()Lidh;

    move-result-object v0

    iput-object v0, p0, Lftm;->c:Lidh;

    .line 74
    :cond_0
    iget-object v0, p0, Lftm;->c:Lidh;

    return-object v0
.end method
