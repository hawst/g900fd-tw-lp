.class public abstract Ledk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:J

.field public final b:Lecw;

.field public final c:J

.field public final d:Ledj;

.field private e:Ljava/lang/String;


# direct methods
.method private constructor <init>(JJLjava/lang/String;JLecw;Ledn;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-wide p3, p0, Ledk;->c:J

    .line 87
    iput-object p5, p0, Ledk;->e:Ljava/lang/String;

    .line 88
    iput-wide p6, p0, Ledk;->a:J

    .line 89
    iput-object p8, p0, Ledk;->b:Lecw;

    .line 90
    invoke-virtual {p9, p0}, Ledn;->a(Ledk;)Ledj;

    move-result-object v0

    iput-object v0, p0, Ledk;->d:Ledj;

    .line 91
    iget-wide v0, p9, Ledn;->c:J

    iget-wide v0, p9, Ledn;->b:J

    .line 92
    return-void
.end method

.method synthetic constructor <init>(JJLjava/lang/String;JLecw;Ledn;B)V
    .locals 1

    .prologue
    .line 15
    invoke-direct/range {p0 .. p9}, Ledk;-><init>(JJLjava/lang/String;JLecw;Ledn;)V

    return-void
.end method


# virtual methods
.method public abstract a()Ledj;
.end method

.method public final b()Ljava/lang/String;
    .locals 7

    .prologue
    .line 127
    iget-object v0, p0, Ledk;->e:Ljava/lang/String;

    iget-object v1, p0, Ledk;->b:Lecw;

    iget-object v1, v1, Lecw;->a:Ljava/lang/String;

    iget-wide v2, p0, Ledk;->a:J

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x16

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
