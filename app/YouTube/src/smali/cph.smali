.class public final Lcph;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlr;


# instance fields
.field final a:Levn;

.field b:J

.field private final c:Lgjp;

.field private final d:Lexd;

.field private final e:Lggr;

.field private final f:Lery;

.field private final g:Ljava/util/PriorityQueue;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/util/concurrent/Executor;


# direct methods
.method protected constructor <init>(Lgjp;Lexd;Lggr;Levn;Lery;Ljava/util/List;Ljava/lang/String;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lcph;->c:Lgjp;

    .line 66
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lcph;->d:Lexd;

    .line 67
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggr;

    iput-object v0, p0, Lcph;->e:Lggr;

    .line 68
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcph;->a:Levn;

    .line 69
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lery;

    iput-object v0, p0, Lcph;->f:Lery;

    .line 70
    new-instance v1, Ljava/util/PriorityQueue;

    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v1, v0}, Ljava/util/PriorityQueue;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcph;->g:Ljava/util/PriorityQueue;

    .line 71
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcph;->h:Ljava/lang/String;

    .line 72
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcph;->i:Ljava/util/concurrent/Executor;

    .line 73
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcpl;
    .locals 3

    .prologue
    .line 157
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcpl;

    iget-object v1, p0, Lcph;->g:Ljava/util/PriorityQueue;

    iget-object v2, p0, Lcph;->h:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcpl;-><init>(Ljava/util/PriorityQueue;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Lfnd;J)V
    .locals 6

    .prologue
    .line 109
    iget-object v0, p1, Lfnd;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v2

    .line 110
    iget-object v0, p1, Lfnd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnf;

    .line 111
    invoke-static {}, Lb;->b()V

    sget-object v1, Lcpj;->a:[I

    invoke-virtual {v0}, Lfnf;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcph;->e:Lggr;

    invoke-virtual {v0, v2}, Lggr;->a(Lfao;)Lfao;

    goto :goto_0

    :pswitch_1
    const-string v0, "cpn"

    iget-object v1, p0, Lcph;->h:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    goto :goto_0

    :pswitch_2
    const-string v0, "conn"

    iget-object v1, p0, Lcph;->d:Lexd;

    invoke-interface {v1}, Lexd;->i()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Lfao;->a(Ljava/lang/String;I)Lfao;

    goto :goto_0

    :pswitch_3
    const-string v0, "cmt"

    const-wide/16 v4, 0x3e8

    div-long v4, p2, v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcph;->f:Lery;

    invoke-virtual {v0}, Lery;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    goto :goto_1

    .line 113
    :cond_1
    iget-object v0, v2, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x8

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Pinging "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->e(Ljava/lang/String;)V

    iget-object v1, p0, Lcph;->c:Lgjp;

    const-string v1, "remarketing"

    const v2, 0x323467f

    invoke-static {v1, v2}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v1

    invoke-virtual {v1, v0}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lgjt;->d:Z

    iget-object v0, p0, Lcph;->c:Lgjp;

    sget-object v2, Lggu;->a:Lwu;

    invoke-virtual {v0, v1, v2}, Lgjp;->a(Lgjt;Lwu;)V

    .line 114
    return-void

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final declared-synchronized handleVideoTimeEvent(Ldad;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p1, Ldad;->d:Z

    if-eqz v0, :cond_2

    .line 84
    iget-wide v0, p1, Ldad;->a:J

    iput-wide v0, p0, Lcph;->b:J

    .line 85
    :goto_0
    iget-object v0, p0, Lcph;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 86
    iget-object v0, p0, Lcph;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnd;

    .line 87
    iget-wide v2, p0, Lcph;->b:J

    iget-object v1, v0, Lfnd;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    iget v1, v0, Lfnd;->d:I

    if-ltz v1, :cond_0

    iget v1, v0, Lfnd;->d:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v4, v1

    cmp-long v1, v4, v2

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 88
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 90
    iget-object v1, p0, Lcph;->i:Ljava/util/concurrent/Executor;

    new-instance v2, Lcpi;

    invoke-direct {v2, p0, v0}, Lcpi;-><init>(Lcph;Lfnd;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 100
    :goto_2
    iget-object v0, p0, Lcph;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 87
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 98
    :cond_1
    :try_start_1
    iget-wide v2, p0, Lcph;->b:J

    invoke-virtual {p0, v0, v2, v3}, Lcph;->a(Lfnd;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 106
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcph;->a:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 262
    return-void
.end method
