.class public final Lccs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field private final a:Lfvi;

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Leyp;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    const v0, 0x7f0800c0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 29
    new-instance v1, Lfvi;

    invoke-direct {v1, p1, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lccs;->a:Lfvi;

    .line 30
    const v0, 0x7f0800ff

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lccs;->b:Landroid/widget/TextView;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Lfju;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 35
    iget-object v0, p0, Lccs;->a:Lfvi;

    iget-object v1, p1, Lfju;->d:Lfnc;

    if-nez v1, :cond_0

    new-instance v1, Lfnc;

    iget-object v2, p1, Lfju;->a:Lhgn;

    iget-object v2, v2, Lhgn;->a:Lhxf;

    invoke-direct {v1, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, p1, Lfju;->d:Lfnc;

    :cond_0
    iget-object v1, p1, Lfju;->d:Lfnc;

    invoke-virtual {v0, v1, v3}, Lfvi;->a(Lfnc;Leyo;)V

    .line 36
    iget-object v0, p0, Lccs;->b:Landroid/widget/TextView;

    iget-object v1, p1, Lfju;->e:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p1, Lfju;->a:Lhgn;

    iget-object v1, v1, Lhgn;->c:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p1, Lfju;->e:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p1, Lfju;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    return-object v3
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 21
    check-cast p2, Lfju;

    invoke-virtual {p0, p2}, Lccs;->a(Lfju;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
