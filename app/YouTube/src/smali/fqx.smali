.class public final Lfqx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lhnq;

.field public final b:Z

.field public final c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lhnq;

    invoke-direct {v0}, Lhnq;-><init>()V

    iput-object v0, p0, Lfqx;->a:Lhnq;

    .line 58
    iput-boolean v1, p0, Lfqx;->b:Z

    .line 59
    iput-boolean v1, p0, Lfqx;->c:Z

    .line 60
    return-void
.end method

.method public constructor <init>(Lhnq;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v4, -0x2710

    const/16 v3, 0x2710

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnq;

    iput-object v0, p0, Lfqx;->a:Lhnq;

    .line 46
    iget-object v0, p0, Lfqx;->a:Lhnq;

    iget v0, v0, Lhnq;->c:I

    if-gt v0, v3, :cond_0

    .line 47
    iget-object v0, p0, Lfqx;->a:Lhnq;

    iget v0, v0, Lhnq;->c:I

    if-lt v0, v4, :cond_0

    .line 48
    iget-object v0, p0, Lfqx;->a:Lhnq;

    iget v0, v0, Lhnq;->d:I

    if-gt v0, v3, :cond_0

    .line 49
    iget-object v0, p0, Lfqx;->a:Lhnq;

    iget v0, v0, Lhnq;->d:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lfqx;->b:Z

    .line 50
    iget-object v0, p0, Lfqx;->a:Lhnq;

    iget v0, v0, Lhnq;->a:I

    if-gt v0, v3, :cond_1

    .line 51
    iget-object v0, p0, Lfqx;->a:Lhnq;

    iget v0, v0, Lhnq;->a:I

    if-lt v0, v4, :cond_1

    .line 52
    iget-object v0, p0, Lfqx;->a:Lhnq;

    iget v0, v0, Lhnq;->b:I

    if-gt v0, v3, :cond_1

    .line 53
    iget-object v0, p0, Lfqx;->a:Lhnq;

    iget v0, v0, Lhnq;->b:I

    if-lez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lfqx;->c:Z

    .line 54
    return-void

    :cond_0
    move v0, v2

    .line 49
    goto :goto_0

    :cond_1
    move v1, v2

    .line 53
    goto :goto_1
.end method
