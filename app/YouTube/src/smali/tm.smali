.class abstract Ltm;
.super Lrm;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 47
    new-instance v0, Lrp;

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "android"

    const-class v3, Ltm;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lrp;-><init>(Landroid/content/ComponentName;)V

    invoke-direct {p0, p1, v0}, Lrm;-><init>(Landroid/content/Context;Lrp;)V

    .line 49
    return-void
.end method

.method public static a(Landroid/content/Context;Ltw;)Ltm;
    .locals 2

    .prologue
    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 53
    new-instance v0, Lts;

    invoke-direct {v0, p0, p1}, Lts;-><init>(Landroid/content/Context;Ltw;)V

    .line 61
    :goto_0
    return-object v0

    .line 55
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 56
    new-instance v0, Ltr;

    invoke-direct {v0, p0, p1}, Ltr;-><init>(Landroid/content/Context;Ltw;)V

    goto :goto_0

    .line 58
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 59
    new-instance v0, Ltn;

    invoke-direct {v0, p0, p1}, Ltn;-><init>(Landroid/content/Context;Ltw;)V

    goto :goto_0

    .line 61
    :cond_2
    new-instance v0, Ltt;

    invoke-direct {v0, p0}, Ltt;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lsk;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public b(Lsk;)V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public c(Lsk;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public d(Lsk;)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method
