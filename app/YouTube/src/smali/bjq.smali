.class public final Lbjq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/app/Activity;

.field final b:Leyt;

.field private final c:Lgix;

.field private final d:Lgng;

.field private final e:Lcub;

.field private final f:Lexd;

.field private final g:Lbjx;

.field private final h:Lbrz;

.field private final i:Lbyg;

.field private final j:Lfrz;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lgix;Lgng;Lcub;Leyt;Lexd;Lbjx;Lbrz;Lbyg;Lfrz;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbjq;->a:Landroid/app/Activity;

    .line 64
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgng;

    iput-object v0, p0, Lbjq;->d:Lgng;

    .line 65
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbjq;->c:Lgix;

    .line 66
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcub;

    iput-object v0, p0, Lbjq;->e:Lcub;

    .line 67
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbjq;->b:Leyt;

    .line 68
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lbjq;->f:Lexd;

    .line 69
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjx;

    iput-object v0, p0, Lbjq;->g:Lbjx;

    .line 70
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrz;

    iput-object v0, p0, Lbjq;->h:Lbrz;

    .line 71
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyg;

    iput-object v0, p0, Lbjq;->i:Lbyg;

    .line 73
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lbjq;->j:Lfrz;

    .line 74
    return-void
.end method


# virtual methods
.method a()Lgnd;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lbjq;->c:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lbjq;->d:Lgng;

    invoke-virtual {v0}, Lgng;->c()Lgnd;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbjq;->d:Lgng;

    iget-object v1, p0, Lbjq;->c:Lgix;

    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    goto :goto_0
.end method

.method a(I)V
    .locals 3

    .prologue
    .line 204
    sget-object v0, Lbjv;->a:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 228
    :goto_0
    return-void

    .line 207
    :pswitch_0
    iget-object v0, p0, Lbjq;->g:Lbjx;

    invoke-virtual {v0}, Lbjx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbjq;->f:Lexd;

    invoke-interface {v0}, Lexd;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    const v0, 0x7f090190

    .line 227
    :goto_1
    iget-object v1, p0, Lbjq;->a:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Leze;->a(Landroid/content/Context;II)V

    goto :goto_0

    .line 212
    :cond_0
    const v0, 0x7f09018c

    .line 214
    goto :goto_1

    .line 217
    :pswitch_1
    const v0, 0x7f09018d

    .line 218
    goto :goto_1

    .line 221
    :pswitch_2
    const v0, 0x7f09018e

    .line 222
    goto :goto_1

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lgbu;)V
    .locals 7

    .prologue
    .line 231
    iget-object v0, p0, Lbjq;->h:Lbrz;

    new-instance v1, Lbjt;

    invoke-direct {v1, p0, p1}, Lbjt;-><init>(Lbjq;Lgbu;)V

    iget-object v2, v0, Lbrz;->m:Landroid/app/AlertDialog;

    if-nez v2, :cond_0

    const/4 v2, 0x2

    new-array v2, v2, [Lbsh;

    const/4 v3, 0x0

    new-instance v4, Lbsh;

    const v5, 0x7f0901ad

    const v6, 0x7f020158

    invoke-direct {v4, v5, v6}, Lbsh;-><init>(II)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lbsh;

    const v5, 0x7f0901ae

    const v6, 0x7f020157

    invoke-direct {v4, v5, v6}, Lbsh;-><init>(II)V

    aput-object v4, v2, v3

    new-instance v3, Lbse;

    invoke-direct {v3, v0}, Lbse;-><init>(Lbrz;)V

    invoke-virtual {v0, v2, v3}, Lbrz;->a([Lbsh;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, v0, Lbrz;->m:Landroid/app/AlertDialog;

    :cond_0
    iput-object v1, v0, Lbrz;->n:Lbsk;

    iget-object v0, v0, Lbrz;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 249
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 255
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 256
    invoke-virtual {p0}, Lbjq;->a()Lgnd;

    move-result-object v0

    invoke-interface {v0, p1}, Lgnd;->c(Ljava/lang/String;)Lglw;

    move-result-object v0

    .line 257
    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lbjq;->h:Lbrz;

    new-instance v1, Lbju;

    invoke-direct {v1, p0, p1}, Lbju;-><init>(Lbjq;Ljava/lang/String;)V

    iget-object v2, v0, Lbrz;->k:Landroid/app/AlertDialog;

    if-nez v2, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Lbsh;

    const/4 v3, 0x0

    new-instance v4, Lbsh;

    const v5, 0x7f0901ae

    const v6, 0x7f020157

    invoke-direct {v4, v5, v6}, Lbsh;-><init>(II)V

    aput-object v4, v2, v3

    new-instance v3, Lbsd;

    invoke-direct {v3, v0}, Lbsd;-><init>(Lbrz;)V

    invoke-virtual {v0, v2, v3}, Lbrz;->a([Lbsh;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, v0, Lbrz;->k:Landroid/app/AlertDialog;

    :cond_0
    iput-object v1, v0, Lbrz;->l:Lbsi;

    iget-object v0, v0, Lbrz;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 269
    :cond_1
    return-void
.end method

.method a(Ljava/lang/String;Lbjy;[BLbjw;)V
    .locals 2

    .prologue
    .line 185
    .line 186
    invoke-virtual {p0}, Lbjq;->a()Lgnd;

    move-result-object v0

    .line 188
    iget-object v1, p2, Lbjy;->c:Lflj;

    .line 186
    invoke-interface {v0, p1, v1, p3}, Lgnd;->b(Ljava/lang/String;Lflj;[B)I

    move-result v0

    .line 190
    if-eqz p4, :cond_0

    .line 191
    invoke-interface {p4, v0}, Lbjw;->a(I)V

    .line 195
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lbjq;->h:Lbrz;

    .line 196
    invoke-virtual {v1}, Lbrz;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 200
    :goto_0
    return-void

    .line 199
    :cond_1
    invoke-virtual {p0, v0}, Lbjq;->a(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lflh;Lbjw;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 93
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 97
    invoke-virtual {p0}, Lbjq;->a()Lgnd;

    move-result-object v0

    invoke-interface {v0, p1}, Lgnd;->c(Ljava/lang/String;)Lglw;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_1

    .line 99
    if-eqz p3, :cond_0

    .line 100
    invoke-interface {p3, v1}, Lbjw;->a(I)V

    .line 102
    :cond_0
    invoke-virtual {p0, v1}, Lbjq;->a(I)V

    .line 150
    :goto_0
    return-void

    .line 107
    :cond_1
    if-nez p2, :cond_3

    .line 108
    if-eqz p3, :cond_2

    .line 109
    invoke-interface {p3, v2}, Lbjw;->a(I)V

    .line 112
    :cond_2
    invoke-virtual {p0, v2}, Lbjq;->a(I)V

    goto :goto_0

    .line 116
    :cond_3
    iget-boolean v0, p2, Lflh;->a:Z

    if-nez v0, :cond_4

    .line 118
    iget-object v0, p0, Lbjq;->i:Lbyg;

    .line 119
    invoke-virtual {p2}, Lflh;->b()Lfki;

    move-result-object v1

    iget-object v2, p0, Lbjq;->j:Lfrz;

    .line 118
    invoke-virtual {v0, v1, v2}, Lbyg;->a(Lfki;Lfrz;)V

    goto :goto_0

    .line 125
    :cond_4
    iget-object v0, p0, Lbjq;->c:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 126
    iget-object v0, p0, Lbjq;->e:Lcub;

    iget-object v1, p0, Lbjq;->a:Landroid/app/Activity;

    new-instance v2, Lbjr;

    invoke-direct {v2, p0, p1, p2, p3}, Lbjr;-><init>(Lbjq;Ljava/lang/String;Lflh;Lbjw;)V

    invoke-virtual {v0, v1, v2}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    goto :goto_0

    .line 148
    :cond_5
    invoke-virtual {p0, p1, p2, p3}, Lbjq;->b(Ljava/lang/String;Lflh;Lbjw;)V

    goto :goto_0
.end method

.method b(Ljava/lang/String;Lflh;Lbjw;)V
    .locals 3

    .prologue
    .line 156
    invoke-virtual {p2}, Lflh;->c()[B

    move-result-object v0

    .line 157
    iget-object v1, p0, Lbjq;->g:Lbjx;

    invoke-virtual {v1, p2}, Lbjx;->a(Lflh;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lbjq;->h:Lbrz;

    new-instance v2, Lbjs;

    invoke-direct {v2, p0, p1, v0, p3}, Lbjs;-><init>(Lbjq;Ljava/lang/String;[BLbjw;)V

    const v0, 0x7f0901a1

    invoke-virtual {v1, p2, v2, v0}, Lbrz;->a(Lflh;Lbsm;I)Z

    .line 178
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v1, p0, Lbjq;->g:Lbjx;

    .line 174
    invoke-virtual {v1}, Lbjx;->b()Lbjy;

    move-result-object v1

    .line 172
    invoke-virtual {p0, p1, v1, v0, p3}, Lbjq;->a(Ljava/lang/String;Lbjy;[BLbjw;)V

    goto :goto_0
.end method
