.class public final Lfzf;
.super Lghv;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lghv;-><init>()V

    return-void
.end method

.method private e(Lorg/apache/http/HttpResponse;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lfzf;->c(Lorg/apache/http/HttpResponse;)V

    .line 25
    const-string v0, "Location"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 26
    if-nez v0, :cond_0

    .line 27
    new-instance v0, Lfax;

    const-string v1, "Location header not present"

    invoke-direct {v0, v1}, Lfax;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a_(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-direct {p0, p1}, Lfzf;->e(Lorg/apache/http/HttpResponse;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lfzf;->e(Lorg/apache/http/HttpResponse;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
