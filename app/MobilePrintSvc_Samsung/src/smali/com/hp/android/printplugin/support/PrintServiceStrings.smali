.class public Lcom/hp/android/printplugin/support/PrintServiceStrings;
.super Ljava/lang/Object;
.source "PrintServiceStrings.java"


# static fields
.field public static final ACTION_CONNECT_DEVICE_USING_WIFIDIRECT:Ljava/lang/String; = "org.androidprinting.intent.ACTION_CONNECT_PRINTER_USING_WIFIDIRECT"

.field public static final ACTION_PRINT_PLUGIN_ABOUT:Ljava/lang/String; = "com.hp.android.printplugin.ABOUT_PLUGIN"

.field public static final ACTION_PRINT_PLUGIN_SETTINGS:Ljava/lang/String; = "com.hp.android.printplugin.PLUGIN_SETTINGS"

.field public static final ACTION_PRINT_SERVICE_CANCEL_ALL_PRINT_JOBS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_CANCEL_ALL_PRINT_JOBS"

.field public static final ACTION_PRINT_SERVICE_CANCEL_PRINT_JOB:Ljava/lang/String; = "org.androidprinting.intent.ACTION_CANCEL_PRINT_JOB"

.field public static final ACTION_PRINT_SERVICE_GET_ALL_JOBS_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_GET_ALL_JOBS_STATUS"

.field public static final ACTION_PRINT_SERVICE_GET_FINAL_PRINT_SETTINGS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_GET_FINAL_PRINT_SETTINGS"

.field public static final ACTION_PRINT_SERVICE_GET_JOB_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_GET_JOB_STATUS"

.field public static final ACTION_PRINT_SERVICE_GET_PRINTER_CAPABILTIES:Ljava/lang/String; = "org.androidprinting.intent.ACTION_GET_PRINT_OPTIONS"

.field public static final ACTION_PRINT_SERVICE_GET_PRINTER_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_GET_PRINTER_STATUS"

.field public static final ACTION_PRINT_SERVICE_GET_PRINT_SERVICE:Ljava/lang/String; = "org.androidprinting.intent.ACTION_GET_PRINT_SERVICE"

.field public static final ACTION_PRINT_SERVICE_GET_SAMSUNG_PRINT_SERVICE:Ljava/lang/String; = "com.sec.print.plugin.intent.ACTION_GET_PRINT_SERVICE"

.field public static final ACTION_PRINT_SERVICE_PRINT:Ljava/lang/String; = "org.androidprinting.intent.ACTION_PRINT"

.field public static final ACTION_PRINT_SERVICE_REGISTER_STATUS_RECEIVER:Ljava/lang/String; = "org.androidprinting.intent.ACTION_REGISTER_STATUS_RECEIVER"

.field public static final ACTION_PRINT_SERVICE_RESUME_PRINT_JOB:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RESUME_PRINT_JOB"

.field public static final ACTION_PRINT_SERVICE_RETURN_CANCEL_ALL_JOBS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_CANCEL_ALL_JOBS"

.field public static final ACTION_PRINT_SERVICE_RETURN_CANCEL_JOB:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_CANCEL_JOB"

.field public static final ACTION_PRINT_SERVICE_RETURN_DEVICE_REMOVED:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_DEVICE_REMOVED"

.field public static final ACTION_PRINT_SERVICE_RETURN_DEVICE_RESOLVED:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_DEVICE_RESOLVED"

.field public static final ACTION_PRINT_SERVICE_RETURN_ERROR:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_PRINT_ERROR"

.field public static final ACTION_PRINT_SERVICE_RETURN_FINAL_PARAMS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_FINAL_PARAMS"

.field public static final ACTION_PRINT_SERVICE_RETURN_GET_ALL_JOBS_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_GET_ALL_JOBS_STATUS"

.field public static final ACTION_PRINT_SERVICE_RETURN_GET_JOB_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_GET_JOB_STATUS"

.field public static final ACTION_PRINT_SERVICE_RETURN_GET_PRINTER_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_GET_PRINTER_STATUS"

.field public static final ACTION_PRINT_SERVICE_RETURN_PRINTER_CAPABILITIES:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_PRINT_CAPABILITIES"

.field public static final ACTION_PRINT_SERVICE_RETURN_PRINTER_STARTED:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_PRINT_STARTED"

.field public static final ACTION_PRINT_SERVICE_RETURN_PRINT_JOB_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_PRINT_JOB_STATUS"

.field public static final ACTION_PRINT_SERVICE_RETURN_RESUME_JOB:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_RESUME_JOB"

.field public static final ACTION_PRINT_SERVICE_START_DISCOVERY:Ljava/lang/String; = "org.androidprinting.intent.ACTION_START_DEVICE_DISCOVERY"

.field public static final ACTION_PRINT_SERVICE_START_MONITORING_PRINTER_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_START_MONITORING_PRINTER_STATUS"

.field public static final ACTION_PRINT_SERVICE_STOP_DISCOVERY:Ljava/lang/String; = "org.androidprinting.intent.ACTION_STOP_DEVICE_DISCOVERY"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_PRINT_SERVICE_STOP_MONITORING_PRINTER_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_STOP_MONITORING_PRINTER_STATUS"

.field public static final ACTION_PRINT_SERVICE_UNREGISTER_STATUS_RECEIVER:Ljava/lang/String; = "org.androidprinting.intent.ACTION_UNREGISTER_STATUS_RECEIVER"

.field public static final ACTION_SCAN_SERVICE_CANCEL_SCAN_JOB:Ljava/lang/String; = "org.androidscanning.intent.ACTION_CANCEL_SCAN_JOB"

.field public static final ACTION_SCAN_SERVICE_GET_SCANNER_CAPABILTIES:Ljava/lang/String; = "org.androidscanning.intent.ACTION_GET_SCAN_OPTIONS"

.field public static final ACTION_SCAN_SERVICE_GET_SCAN_SERVICE:Ljava/lang/String; = "org.androidscanning.intent.ACTION_GET_SCAN_SERVICE"

.field public static final ACTION_SCAN_SERVICE_REGISTER_STATUS_RECEIVER:Ljava/lang/String; = "org.androidscanning.intent.ACTION_REGISTER_STATUS_RECEIVER"

.field public static final ACTION_SCAN_SERVICE_RETURN_ERROR:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_SCAN_ERROR"

.field public static final ACTION_SCAN_SERVICE_RETURN_RESULT:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_SCAN_RESULT"

.field public static final ACTION_SCAN_SERVICE_RETURN_SCANNER_CAPABILITIES:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_SCAN_CAPABILITIES"

.field public static final ACTION_SCAN_SERVICE_RETURN_SCAN_PROGRESS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_RETURN_SCAN_PROGRESS"

.field public static final ACTION_SCAN_SERVICE_SCAN:Ljava/lang/String; = "org.androidscanning.intent.ACTION_SCAN"

.field public static final ACTION_SCAN_SERVICE_START_DISCOVERY:Ljava/lang/String; = "org.androidscanning.intent.ACTION_START_DEVICE_DISCOVERY"

.field public static final ACTION_SCAN_SERVICE_UNREGISTER_STATUS_RECEIVER:Ljava/lang/String; = "org.androidscanning.intent.ACTION_UNREGISTER_STATUS_RECEIVER"

.field public static final ACTION_WIFI_DIRECT_CONNECTION_STATUS:Ljava/lang/String; = "org.androidprinting.intent.ACTION_WIFI_DIRECT_CONNECTION_STATUS"

.field public static final ALIGNMENT:Ljava/lang/String; = "alignment"

.field public static final ALIGN_CENTER:I = 0x11

.field public static final ALIGN_CENTER_HORIZONTAL:I = 0x1

.field public static final ALIGN_CENTER_HORIZONTAL_ON_ORIENTATION:I = 0x7

.field public static final ALIGN_CENTER_VERTICIAL:I = 0x10

.field public static final BLOCKED_REASON__BUSY:Ljava/lang/String; = "device-busy"

.field public static final BLOCKED_REASON__CANCELLED:Ljava/lang/String; = "print-job-cancelled"

.field public static final BLOCKED_REASON__DOOR_OPEN:Ljava/lang/String; = "cover-open"

.field public static final BLOCKED_REASON__JAMMED:Ljava/lang/String; = "jam"

.field public static final BLOCKED_REASON__LOW_ON_INK:Ljava/lang/String; = "marker-ink-almost-empty"

.field public static final BLOCKED_REASON__LOW_ON_TONER:Ljava/lang/String; = "marker-toner-almost-empty"

.field public static final BLOCKED_REASON__OFFLINE:Ljava/lang/String; = "device-offline"

.field public static final BLOCKED_REASON__OUT_OF_INK:Ljava/lang/String; = "marker-ink-empty"

.field public static final BLOCKED_REASON__OUT_OF_PAPER:Ljava/lang/String; = "input-media-supply-empty"

.field public static final BLOCKED_REASON__OUT_OF_TONER:Ljava/lang/String; = "marker-toner-empty"

.field public static final BLOCKED_REASON__REALLY_LOW_ON_INK:Ljava/lang/String; = "marker-ink-almost-empty-probably-empty-but-not-really"

.field public static final BLOCKED_REASON__SERVICE_REQUEST:Ljava/lang/String; = "service-request"

.field public static final BLOCKED_REASON__UNKNOWN:Ljava/lang/String; = "unknown"

.field public static final COLOR_SPACE_COLOR:Ljava/lang/String; = "color"

.field public static final COLOR_SPACE_MONOCHROME:Ljava/lang/String; = "monochrome"

.field public static final COMMUNICATION_ERROR:Ljava/lang/String; = "communication-error"

.field public static final COPIES:Ljava/lang/String; = "copies"

.field public static final DISCOVERY_DEVICE_ADDRESS:Ljava/lang/String; = "device.discovery.device_address"

.field public static final DISCOVERY_DEVICE_BONJOUR_DOMAIN_NAME:Ljava/lang/String; = "device.discovery.device_bonjour_domain_name"

.field public static final DISCOVERY_DEVICE_BONJOUR_NAME:Ljava/lang/String; = "device.discovery.device_bonjour_name"

.field public static final DISCOVERY_DEVICE_HOSTNAME:Ljava/lang/String; = "device.discovery.device_hostname"

.field public static final DISCOVERY_DEVICE_NAME:Ljava/lang/String; = "device.discovery.device_name"

.field public static final DISCOVERY_ERROR_KEY:Ljava/lang/String; = "discovery-error"

.field public static final DISCOVERY_METHOD_NETWORK:Ljava/lang/String; = "network"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DISCOVERY_PROPERTY_PREFIX:Ljava/lang/String; = "device.discovery."

.field public static final DISCOVERY_SERVICES:Ljava/lang/String; = "discovery-services"

.field public static final DISCOVERY_SERVICE_EPRINT:Ljava/lang/String; = "discovery-service-eprint"

.field public static final DISCOVERY_SERVICE_LOCAL:Ljava/lang/String; = "discovery-service-local"

.field public static final DISCOVERY_SERVICE_PPL:Ljava/lang/String; = "discovery-service-ppl"

.field public static final DISCOVERY_SERVICE_QUERY_NAME:Ljava/lang/String; = "device.discovery.service_name"

.field public static final FILL_PAGE:Ljava/lang/String; = "fill-page"

.field public static final FIT_TO_PAGE:Ljava/lang/String; = "fit-to-page"

.field public static final FULL_BLEED:Ljava/lang/String; = "full-bleed"

.field public static final FULL_BLEED_OFF:Ljava/lang/String; = "off"

.field public static final FULL_BLEED_ON:Ljava/lang/String; = "on"

.field public static final FULL_BLEED_SUPPORTED:Ljava/lang/String; = "full-bleed-supported"

.field public static final IS_SUPPORTED:Ljava/lang/String; = "is-supported"

.field public static final JOB_DONE_CANCELLED:Ljava/lang/String; = "job-cancelled"

.field public static final JOB_DONE_CORRUPT:Ljava/lang/String; = "job-corrupt"

.field public static final JOB_DONE_ERROR:Ljava/lang/String; = "job-failed"

.field public static final JOB_DONE_OK:Ljava/lang/String; = "job-success"

.field public static final JOB_DONE_OTHER:Ljava/lang/String; = "job-result-unknown"

.field public static final JOB_STATE_BLOCKED:Ljava/lang/String; = "print-job-blocked"

.field public static final JOB_STATE_DONE:Ljava/lang/String; = "print-job-complete"

.field public static final JOB_STATE_OTHER:Ljava/lang/String; = "print-job-unknown"

.field public static final JOB_STATE_QUEUED:Ljava/lang/String; = "print-job-queued"

.field public static final JOB_STATE_RUNNING:Ljava/lang/String; = "print-job-running"

.field public static final MEDIA_SIZE_A4:Ljava/lang/String; = "iso_a4_210x297mm"

.field public static final MEDIA_SIZE_LEGAL:Ljava/lang/String; = "na_legal_8.5x14in"

.field public static final MEDIA_SIZE_LETTER:Ljava/lang/String; = "na_letter_8.5x11in"

.field public static final MEDIA_SIZE_NAME:Ljava/lang/String; = "media-size-name"

.field public static final MEDIA_SIZE_PHOTO_10x15cm:Ljava/lang/String; = "om_small-photo_100x150mm"

.field public static final MEDIA_SIZE_PHOTO_3_5x5:Ljava/lang/String; = "na_index-3x5_3x5in"

.field public static final MEDIA_SIZE_PHOTO_4x6in:Ljava/lang/String; = "na_index-4x6_4x6in"

.field public static final MEDIA_SIZE_PHOTO_5x7:Ljava/lang/String; = "na_5x7_5x7in"

.field public static final MEDIA_SIZE_PHOTO_L:Ljava/lang/String; = "oe_l_3.5x5in"

.field public static final MEDIA_SOURCE:Ljava/lang/String; = "media-source"

.field public static final MEDIA_TRAY_AUTO:Ljava/lang/String; = "auto"

.field public static final MEDIA_TRAY_PHOTO:Ljava/lang/String; = "photo"

.field public static final MEDIA_TYPE:Ljava/lang/String; = "media-type"

.field public static final MEDIA_TYPE_AUTO:Ljava/lang/String; = "auto"

.field public static final MEDIA_TYPE_PHOTO_GLOSSY:Ljava/lang/String; = "photographic-glossy"

.field public static final MEDIA_TYPE_PHPOTO:Ljava/lang/String; = "Photo"

.field public static final MEDIA_TYPE_PLAIN:Ljava/lang/String; = "Plain"

.field public static final META_DATA__PLUGIN_DISCOVERY_METHODS:Ljava/lang/String; = "com.hp.android.printplugin.discovery_methods"

.field public static final META_DATA__PLUGIN_VENDOR:Ljava/lang/String; = "com.hp.android.printplugin.vendor_name"

.field public static final META_DATA__PLUGIN_VERSION:Ljava/lang/String; = "com.hp.android.printplugin.api_version"

.field public static final META_DATA__PLUGIN_WIRELESS_DIRECT_ADDRESS:Ljava/lang/String; = "com.hp.android.printplugin.wireless_direct_address"

.field public static final META_DATA__PLUGIN_WIRELESS_DIRECT_PREFIX:Ljava/lang/String; = "com.hp.android.printplugin.wireless_direct_prefix"

.field public static final MIME_TYPES:Ljava/lang/String; = "mime-types"

.field public static final MIME_TYPE_FALLBACK:Ljava/lang/String; = "image/hpimage"

.field public static final MISSING_FILE_ERROR:Ljava/lang/String; = "missing-file-error"

.field public static final ORIENTATION_AUTO:Ljava/lang/String; = "auto"

.field public static final ORIENTATION_LANDSCAPE:Ljava/lang/String; = "landscape"

.field public static final ORIENTATION_PORTRAIT:Ljava/lang/String; = "portrait"

.field public static final ORIENTATION_REQUESTED:Ljava/lang/String; = "orientation-requested"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "package-name"

.field public static final PAGE_HEIGHT:Ljava/lang/String; = "page-height"

.field public static final PAGE_MARGIN_BOTTOM:Ljava/lang/String; = "page-margin-bottom"

.field public static final PAGE_MARGIN_LEFT:Ljava/lang/String; = "page-margin-left"

.field public static final PAGE_MARGIN_RIGHT:Ljava/lang/String; = "page-margin-right"

.field public static final PAGE_MARGIN_TOP:Ljava/lang/String; = "page-margin-top"

.field public static final PAGE_RANGE:Ljava/lang/String; = "page-range"

.field public static final PAGE_WIDTH:Ljava/lang/String; = "page-width"

.field public static final PAPER_SIZE_A3:Ljava/lang/String; = "A3"

.field public static final PAPER_SIZE_A4:Ljava/lang/String; = "A4"

.field public static final PAPER_SIZE_A5:Ljava/lang/String; = "A5"

.field public static final PAPER_SIZE_A6:Ljava/lang/String; = "A6"

.field public static final PAPER_SIZE_B5ENV:Ljava/lang/String; = "B5ENV"

.field public static final PAPER_SIZE_EXEC:Ljava/lang/String; = "Executive"

.field public static final PAPER_SIZE_FOLIO:Ljava/lang/String; = "Folio"

.field public static final PAPER_SIZE_JISB4:Ljava/lang/String; = "JISB4"

.field public static final PAPER_SIZE_JISB5:Ljava/lang/String; = "JISB5"

.field public static final PAPER_SIZE_LEGAL:Ljava/lang/String; = "Legal"

.field public static final PAPER_SIZE_LETTER:Ljava/lang/String; = "Letter"

.field public static final PAPER_SIZE_OFICIO:Ljava/lang/String; = "Oficio"

.field public static final PAPER_SIZE_PHOTO_3_5x5:Ljava/lang/String; = "3.5x5in"

.field public static final PAPER_SIZE_PHOTO_4x6in:Ljava/lang/String; = "4x6in"

.field public static final PAPER_SIZE_PHOTO_5x7:Ljava/lang/String; = "5x7in"

.field public static final PAPER_SIZE_STATEMENT:Ljava/lang/String; = "Statement"

.field public static final PAPER_SIZE_TABLOID:Ljava/lang/String; = "Tabloid"

.field public static final PRINTABLE_PIXEL_HEIGHT:Ljava/lang/String; = "printable-pixel-height"

.field public static final PRINTABLE_PIXEL_WIDTH:Ljava/lang/String; = "printable-pixel-width"

.field public static final PRINTER_ADDRESS_KEY:Ljava/lang/String; = "printer-address"

.field public static final PRINTER_MAKE_MODEL:Ljava/lang/String; = "printer-make-model"

.field public static final PRINTER_NAME:Ljava/lang/String; = "printer-name"

.field public static final PRINTER_NDEF_MESSAGE:Ljava/lang/String; = "printer_ndef_message"

.field public static final PRINTER_STATE_BLOCKED:Ljava/lang/String; = "print-state-blocked"

.field public static final PRINTER_STATE_IDLE:Ljava/lang/String; = "print-state-idle"

.field public static final PRINTER_STATE_RUNNING:Ljava/lang/String; = "print-state-running"

.field public static final PRINTER_STATE_UNKNOWN:Ljava/lang/String; = "print-state-unknown"

.field public static final PRINTER_STATUS_KEY:Ljava/lang/String; = "printer-status"

.field public static final PRINT_APP_ID_KEY:Ljava/lang/String; = "print-app-identifier"

.field public static final PRINT_COLOR_MODE:Ljava/lang/String; = "print-color-mode"

.field public static final PRINT_DOCUMENT_CATEGORY:Ljava/lang/String; = "print-document-category"

.field public static final PRINT_DOCUMENT_CATEGORY__DOCUMENT:Ljava/lang/String; = "Doc"

.field public static final PRINT_DOCUMENT_CATEGORY__PHOTO:Ljava/lang/String; = "Photo"

.field public static final PRINT_ERROR_KEY:Ljava/lang/String; = "print-error"

.field public static final PRINT_FILE_LIST:Ljava/lang/String; = "file-list"

.field public static final PRINT_JOBS_STATUS:Ljava/lang/String; = "print-jobs-status"

.field public static final PRINT_JOB_BLOCKED_STATUS_KEY:Ljava/lang/String; = "print-job-blocked-info"

.field public static final PRINT_JOB_DONE_RESULT:Ljava/lang/String; = "print-job-done-result"

.field public static final PRINT_JOB_HANDLE_KEY:Ljava/lang/String; = "print-job-handle"

.field public static final PRINT_JOB_STATUS_KEY:Ljava/lang/String; = "print-job-status"

.field public static final PRINT_QUALITY:Ljava/lang/String; = "print-quality"

.field public static final PRINT_QUALITY_BEST:Ljava/lang/String; = "high"

.field public static final PRINT_QUALITY_DRAFT:Ljava/lang/String; = "draft"

.field public static final PRINT_QUALITY_NORMAL:Ljava/lang/String; = "normal"

.field public static final PRINT_REQUEST_ACTION:Ljava/lang/String; = "request-action"

.field public static final PRINT_REQUEST_RESULT:Ljava/lang/String; = "result"

.field public static final PRINT_RESOLUTION:Ljava/lang/String; = "print-resolution"

.field public static final PRINT_STATUS_COPY_NUMBER:Ljava/lang/String; = "copy-num"

.field public static final PRINT_STATUS_CURRENT_PAGE:Ljava/lang/String; = "current-page"

.field public static final PRINT_STATUS_PAGE_CORRUPTED:Ljava/lang/String; = "page-corrupted"

.field public static final PRINT_STATUS_PAGE_NUMBER:Ljava/lang/String; = "page-num"

.field public static final PRINT_STATUS_PAGE_START_INFO:Ljava/lang/String; = "page-start-info"

.field public static final PRINT_STATUS_TOTAL_PAGE_COUNT:Ljava/lang/String; = "total-pages"

.field public static final PRINT_STATUS_TOTAL_PAGE_UPDATE:Ljava/lang/String; = "page-total-update"

.field public static final PRINT_TO_FILE:Ljava/lang/String; = "print-to-file"

.field public static final SCANNER_ADDRESS_KEY:Ljava/lang/String; = "scanner-address"

.field public static final SCAN_ERROR_AUTH_FAIL:Ljava/lang/String; = "kAuthError"

.field public static final SCAN_ERROR_BUSY:Ljava/lang/String; = "kBusy"

.field public static final SCAN_ERROR_CANCELED:Ljava/lang/String; = "kUserCancel"

.field public static final SCAN_ERROR_CAPABILITIES:Ljava/lang/String; = "kNoGetCapabilities"

.field public static final SCAN_ERROR_CLOSE:Ljava/lang/String; = "kCloseError"

.field public static final SCAN_ERROR_COVER_OPEN:Ljava/lang/String; = "kCoverOpen"

.field public static final SCAN_ERROR_EMPTY_PASSWORD:Ljava/lang/String; = "kEmptyPassword"

.field public static final SCAN_ERROR_EXCEEDED_QUOTA:Ljava/lang/String; = "kExceedQuota"

.field public static final SCAN_ERROR_EXT_STORAGE_LOCKED:Ljava/lang/String; = "kNoWritableExternalStorage"

.field public static final SCAN_ERROR_INVALID_PASSWORD:Ljava/lang/String; = "kInvalidePasswd"

.field public static final SCAN_ERROR_INVALID_USER:Ljava/lang/String; = "kInvalidUser"

.field public static final SCAN_ERROR_JAM:Ljava/lang/String; = "kJam"

.field public static final SCAN_ERROR_KEY:Ljava/lang/String; = "scan-error"

.field public static final SCAN_ERROR_LOCKED:Ljava/lang/String; = "kLocked"

.field public static final SCAN_ERROR_NO_EXT_STORAGE:Ljava/lang/String; = "kNoExternalStorage"

.field public static final SCAN_ERROR_NO_MEMEORY:Ljava/lang/String; = "kNoMemory"

.field public static final SCAN_ERROR_NO_PERMISSION:Ljava/lang/String; = "kNoPermission"

.field public static final SCAN_ERROR_OPEN:Ljava/lang/String; = "kOpenError"

.field public static final SCAN_ERROR_OPENING_SESSION:Ljava/lang/String; = "kSessionOpenError"

.field public static final SCAN_ERROR_SECURITY:Ljava/lang/String; = "kSecurityError"

.field public static final SCAN_ERROR_TIMEOUT:Ljava/lang/String; = "kTimeOut"

.field public static final SCAN_ERROR_UNKNOWN:Ljava/lang/String; = "kUnknownError"

.field public static final SCAN_FILE_PATH_LIST_KEY:Ljava/lang/String; = "file_path_list"

.field public static final SCAN_FILE_TYPE_JPG:Ljava/lang/String; = "JPG"

.field public static final SCAN_FILE_TYPE_KEY:Ljava/lang/String; = "scan_file_type"

.field public static final SCAN_FILE_TYPE_PDF:Ljava/lang/String; = "PDF"

.field public static final SCAN_FILE_TYPE_PNG:Ljava/lang/String; = "PNG"

.field public static final SCAN_IMAGE_FOLDER_KEY:Ljava/lang/String; = "scan_image_folder"

.field public static final SCAN_JOB_ACCOUNTING_PASSWORD:Ljava/lang/String; = "password"

.field public static final SCAN_JOB_ACCOUNTING_USERNAME:Ljava/lang/String; = "username"

.field public static final SCAN_MODE_COLOR:Ljava/lang/String; = "Color"

.field public static final SCAN_MODE_GRAY:Ljava/lang/String; = "Grayscale"

.field public static final SCAN_MODE_KEY:Ljava/lang/String; = "scan_mode"

.field public static final SCAN_PREVIEW_SCALE_HALF:Ljava/lang/String; = "half"

.field public static final SCAN_PREVIEW_SCALE_KEY:Ljava/lang/String; = "scan_scale"

.field public static final SCAN_PREVIEW_SCALE_ONE:Ljava/lang/String; = "one"

.field public static final SCAN_PREVIEW_SCALE_ONE_EIGHTH:Ljava/lang/String; = "eighth"

.field public static final SCAN_PREVIEW_SCALE_ONE_FOURTH:Ljava/lang/String; = "quarter"

.field public static final SCAN_PROGRESS_END_PAGE:Ljava/lang/String; = "progress_end_page"

.field public static final SCAN_PROGRESS_END_PAGE_NO:Ljava/lang/String; = "progress_end_page_no"

.field public static final SCAN_PROGRESS_END_SAVE_FILE:Ljava/lang/String; = "progress_start_save_file"

.field public static final SCAN_PROGRESS_SAVE_KEY:Ljava/lang/String; = "progress_save_file"

.field public static final SCAN_PROGRESS_START_PAGE:Ljava/lang/String; = "progress_start_page"

.field public static final SCAN_PROGRESS_START_PAGE_NO:Ljava/lang/String; = "progress_start_page_no"

.field public static final SCAN_PROGRESS_START_SAVE_FILE:Ljava/lang/String; = "progress_start_save_file"

.field public static final SCAN_PROGRESS_TYPE_KEY:Ljava/lang/String; = "progress_type"

.field public static final SCAN_PROGRESS_TYPE_SAVING:Ljava/lang/String; = "scan_saving"

.field public static final SCAN_REQUEST_ACTION:Ljava/lang/String; = "request-action"

.field public static final SCAN_REQUEST_RESULT:Ljava/lang/String; = "result"

.field public static final SCAN_RESOLUTION_HIGH:Ljava/lang/String; = "Heigh"

.field public static final SCAN_RESOLUTION_KEY:Ljava/lang/String; = "scan_resolution"

.field public static final SCAN_RESOLUTION_LOW:Ljava/lang/String; = "Low"

.field public static final SCAN_RESOLUTION_MEDIUM:Ljava/lang/String; = "Medium"

.field public static final SCAN_SIZE_KEY:Ljava/lang/String; = "scan_size"

.field public static final SCAN_SOURCE_ADF:Ljava/lang/String; = "ADF"

.field public static final SCAN_SOURCE_AUTO:Ljava/lang/String; = "Auto"

.field public static final SCAN_SOURCE_FB:Ljava/lang/String; = "Flatbed"

.field public static final SCAN_SOURCE_KEY:Ljava/lang/String; = "scan_source"

.field public static final SCHEME_DISCOVERY:Ljava/lang/String; = "discovery"

.field public static final SCHEME_JOB_ID:Ljava/lang/String; = "jobid"

.field public static final SIDES:Ljava/lang/String; = "sides"

.field public static final SIDES_DUPLEX_LONG_EDGE:Ljava/lang/String; = "two-sided-long-edge"

.field public static final SIDES_DUPLEX_SHORT_EDGE:Ljava/lang/String; = "two-sided-short-edge"

.field public static final SIDES_SIMPLEX:Ljava/lang/String; = "one-sided"

.field public static final STATUS_BUNDLE_JOB_STATE:Ljava/lang/String; = "status-printer-state"

.field public static final STATUS_BUNDLE_PAGE_INFO:Ljava/lang/String; = "status-page-info"

.field public static final SUPPORTS_CANCEL:Ljava/lang/String; = "supports-cancel"

.field public static final VENDOR_NAME_UNKNOWN:Ljava/lang/String; = "unknown"

.field public static final WIFIDIRECT_CONNECTION_STATUS_CONNECTED:Ljava/lang/String; = "connected_wifi_direct_device"

.field public static final WIFIDIRECT_CONNECTION_STATUS_CONNECTING:Ljava/lang/String; = "connecting_wifi_direct_device"

.field public static final WIFIDIRECT_CONNECTION_STATUS_DEVICE_FOUND:Ljava/lang/String; = "wifi_direct_device_found"

.field public static final WIFIDIRECT_CONNECTION_STATUS_DISCOVERING:Ljava/lang/String; = "discovering_wifi_direct_device"

.field public static final WIFIDIRECT_CONNECTION_STATUS_ERROR_CONNECTING_DEVICE:Ljava/lang/String; = "error_cnnecting_wifidirect_device"

.field public static final WIFIDIRECT_CONNECTION_STATUS_ERROR_DEVICE_NOT_FOUND:Ljava/lang/String; = "wifidirect_device_not_found"

.field public static final WIFIDIRECT_CONNECTION_STATUS_ERROR_WIFIDIRECT_NOT_SUPPORTED:Ljava/lang/String; = "error_wifidirect_not_supported"

.field public static final WIFIDIRECT_CONNECTION_STATUS_ERROR_WIFIDIRECT_TURNED_OFF:Ljava/lang/String; = "error_wifidirect_is_turned_off"

.field public static final WIFIDIRECT_CONNECTION_STATUS_GOT_IPADDRESS:Ljava/lang/String; = "got_ip_address_of_wifidirect_device"

.field public static final WIFIDIRECT_CONNECTION_STATUS_KEY:Ljava/lang/String; = "wifidirect_connection_status_key"

.field public static final WIFIDIRECT_CONNECTION_STATUS_PRESS_WPS_BUTTON:Ljava/lang/String; = "wifidirect_device_press_wps_button"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
