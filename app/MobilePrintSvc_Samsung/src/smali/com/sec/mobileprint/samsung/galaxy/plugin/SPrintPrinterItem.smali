.class public Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
.super Ljava/lang/Object;
.source "SPrintPrinterItem.java"


# instance fields
.field public mColorModel:I

.field public mConnectedType:I

.field public mDuplexModel:I

.field public mIPAddr:Ljava/lang/String;

.field public mLanguages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mMediaSize:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/MediaSizeInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mMediaSizeReference:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mModelName:Ljava/lang/String;

.field public mPaperTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mPid:I

.field public mPort:I

.field public mVid:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "connectType"    # I
    .param p2, "destination"    # Ljava/lang/String;
    .param p3, "modelName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mConnectedType:I

    .line 42
    iput-object p2, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mIPAddr:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mModelName:Ljava/lang/String;

    .line 44
    const/16 v0, 0x238c

    iput v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mPort:I

    .line 45
    iput v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mDuplexModel:I

    .line 46
    iput v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mColorModel:I

    .line 47
    iput v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mPid:I

    .line 48
    iput v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mVid:I

    .line 50
    return-void
.end method
