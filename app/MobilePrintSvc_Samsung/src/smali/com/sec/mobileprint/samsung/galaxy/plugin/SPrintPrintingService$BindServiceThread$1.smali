.class Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread$1;
.super Ljava/lang/Object;
.source "SPrintPrintingService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread$1;->this$1:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;

    .line 588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread$1;->this$1:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;

    # getter for: Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->this$0:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;
    invoke-static {v0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->access$1(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;)Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    move-result-object v0

    invoke-static {p2}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->access$0(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;Lcom/sec/print/mobileprint/dm/IDeviceManagerService;)V

    .line 591
    const-string v0, "SamsungPrintService"

    const-string v1, "SPrintPrintingService : onServiceConnected - mSAPSDeviceManagerService."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 595
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread$1;->this$1:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;

    # getter for: Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->this$0:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;
    invoke-static {v0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->access$1(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;)Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->access$0(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;Lcom/sec/print/mobileprint/dm/IDeviceManagerService;)V

    .line 596
    const-string v0, "SamsungPrintService"

    const-string v1, "SPrintPrintingService : onServiceDisconnected - mSAPSDeviceManagerService."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    return-void
.end method
