.class Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;
.super Ljava/lang/Thread;
.source "SPrintPrintingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BindServiceThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;


# direct methods
.method private constructor <init>(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->this$0:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;)V
    .locals 0

    .prologue
    .line 584
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;-><init>(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;)Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->this$0:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 587
    const-string v0, "SamsungPrintService"

    const-string v1, "SPrintPrintingService : BindServiceThread()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->this$0:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    new-instance v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread$1;

    invoke-direct {v1, p0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread$1;-><init>(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;)V

    invoke-static {v0, v1}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->access$1(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;Landroid/content/ServiceConnection;)V

    .line 600
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->this$0:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.print.mobileprint.plugin.DeviceManagerService"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;->this$0:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    # getter for: Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDevManagerConnection:Landroid/content/ServiceConnection;
    invoke-static {v2}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->access$2(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;)Landroid/content/ServiceConnection;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 601
    return-void
.end method
