.class public Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;
.super Lcom/sec/android/app/mobileprint/service/IPrintService$Stub;
.source "SPrintExtendIPrintServiceAidl.java"


# instance fields
.field mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;)V
    .locals 0
    .param p1, "service"    # Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/mobileprint/service/IPrintService$Stub;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    .line 35
    return-void
.end method


# virtual methods
.method public cancel(II)I
    .locals 1
    .param p1, "handle"    # I
    .param p2, "jobId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->cancel(II)I

    move-result v0

    return v0
.end method

.method public createPrinterInstance(ILjava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "connectType"    # I
    .param p2, "destination"    # Ljava/lang/String;
    .param p3, "modelName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->createPrinterInstance(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getAPILevel()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->getAPILevel()I

    move-result v0

    return v0
.end method

.method public getCapabilities(ILcom/sec/android/app/mobileprint/service/ICapabilitiesCallback;)V
    .locals 1
    .param p1, "handle"    # I
    .param p2, "callback"    # Lcom/sec/android/app/mobileprint/service/ICapabilitiesCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->getCapabilities(ILcom/sec/android/app/mobileprint/service/ICapabilitiesCallback;)V

    .line 67
    return-void
.end method

.method public getPrinterStatus(I)I
    .locals 1
    .param p1, "handle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->getPrinterStatus(I)I

    move-result v0

    return v0
.end method

.method public getReferencePaperSizeList()Lcom/sec/android/app/mobileprint/service/PaperSizeList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->getReferencePaperSizeList()Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    move-result-object v0

    return-object v0
.end method

.method public getReferencePaperTypeList()Lcom/sec/android/app/mobileprint/service/PaperTypeList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->getReferencePaperTypeList()Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    move-result-object v0

    return-object v0
.end method

.method public getVendorName()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->getVendorName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSupportedPrinter(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "connectType"    # I
    .param p2, "destination"    # Ljava/lang/String;
    .param p3, "modelName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->isSupportedPrinter(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isUsbPrintingSupport()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->isUsbPrintingSupport()Z

    move-result v0

    return v0
.end method

.method public print(ILcom/sec/android/app/mobileprint/service/PrintSettings;Lcom/sec/android/app/mobileprint/service/IPrintCallback;)I
    .locals 1
    .param p1, "handle"    # I
    .param p2, "settings"    # Lcom/sec/android/app/mobileprint/service/PrintSettings;
    .param p3, "callback"    # Lcom/sec/android/app/mobileprint/service/IPrintCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;->mService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->print(ILcom/sec/android/app/mobileprint/service/PrintSettings;Lcom/sec/android/app/mobileprint/service/IPrintCallback;)I

    move-result v0

    return v0
.end method
