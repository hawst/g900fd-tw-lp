.class public Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;
.super Landroid/app/Service;
.source "SPrintPrintingService.java"

# interfaces
.implements Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;
    }
.end annotation


# static fields
.field private static final BIND_SAMSUNG_PRINT_SERVICE:Ljava/lang/String; = "com.sec.intent.action.PRINT_SERVICE"

.field private static final SAPS_DEVICE_MANAGER_SERVICE:Ljava/lang/String; = "com.sec.print.mobileprint.plugin.DeviceManagerService"

.field private static final SAPS_PRINT_SERVICE:Ljava/lang/String; = "com.sec.print.mobileprint.plugin.PrintingService"

.field private static final VENDOR_NAME:Ljava/lang/String; = "Samsung"

.field private static mIPrintService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;

.field private static mPrinterList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;",
            ">;"
        }
    .end annotation
.end field

.field private static mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

.field private static mRefPaperTypeList:Lcom/sec/android/app/mobileprint/service/PaperTypeList;


# instance fields
.field private mCurrentJobId:I

.field private mJobMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPrintCallback:Lcom/sec/android/app/mobileprint/service/IPrintCallback;

.field private mSAPSConnection:Landroid/content/ServiceConnection;

.field private mSAPSDevManagerConnection:Landroid/content/ServiceConnection;

.field private mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 92
    sput-object v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    .line 93
    sput-object v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    .line 94
    sput-object v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperTypeList:Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 80
    iput-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDevManagerConnection:Landroid/content/ServiceConnection;

    .line 81
    iput-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    .line 82
    iput-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSConnection:Landroid/content/ServiceConnection;

    .line 87
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mCurrentJobId:I

    .line 89
    iput-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mJobMap:Ljava/util/HashMap;

    .line 96
    iput-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrintCallback:Lcom/sec/android/app/mobileprint/service/IPrintCallback;

    .line 64
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;Lcom/sec/print/mobileprint/dm/IDeviceManagerService;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;Landroid/content/ServiceConnection;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDevManagerConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;)Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDevManagerConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method private getJobHandlefromMap(I)Ljava/lang/Integer;
    .locals 5
    .param p1, "samJobId"    # I

    .prologue
    .line 843
    iget-object v3, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mJobMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 851
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 843
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 844
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 845
    .local v1, "key":Ljava/lang/Integer;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 847
    .local v2, "value":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, p1, :cond_0

    goto :goto_0
.end method

.method private getUSBDevice()Landroid/hardware/usb/UsbDevice;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 797
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_0

    .line 798
    const-string v4, "usb"

    invoke-virtual {p0, v4}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbManager;

    .line 799
    .local v2, "manager":Landroid/hardware/usb/UsbManager;
    if-eqz v2, :cond_0

    .line 800
    invoke-virtual {v2}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v1

    .line 801
    .local v1, "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 802
    .local v0, "deviceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/usb/UsbDevice;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 803
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/usb/UsbDevice;

    .line 808
    .end local v0    # "deviceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/usb/UsbDevice;>;"
    .end local v1    # "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    .end local v2    # "manager":Landroid/hardware/usb/UsbManager;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private declared-synchronized responseCancelCompleted(I)V
    .locals 4
    .param p1, "jobID"    # I

    .prologue
    .line 541
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrintCallback:Lcom/sec/android/app/mobileprint/service/IPrintCallback;

    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v3}, Lcom/sec/android/app/mobileprint/service/IPrintCallback;->responsePrint(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 545
    :goto_0
    monitor-exit p0

    return-void

    .line 542
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 541
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized responsePrintCompleted(II)V
    .locals 3
    .param p1, "jobID"    # I
    .param p2, "totalPage"    # I

    .prologue
    .line 559
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrintCallback:Lcom/sec/android/app/mobileprint/service/IPrintCallback;

    const/4 v2, 0x5

    invoke-interface {v1, p1, v2, p2}, Lcom/sec/android/app/mobileprint/service/IPrintCallback;->responsePrint(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 563
    :goto_0
    monitor-exit p0

    return-void

    .line 560
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 559
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized responsePrintError(II)V
    .locals 3
    .param p1, "jobID"    # I
    .param p2, "value"    # I

    .prologue
    .line 550
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrintCallback:Lcom/sec/android/app/mobileprint/service/IPrintCallback;

    const/4 v2, 0x7

    invoke-interface {v1, p1, v2, p2}, Lcom/sec/android/app/mobileprint/service/IPrintCallback;->responsePrint(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554
    :goto_0
    monitor-exit p0

    return-void

    .line 551
    :catch_0
    move-exception v0

    .line 552
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 550
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized responsePrintStatus(III)V
    .locals 2
    .param p1, "jobID"    # I
    .param p2, "type"    # I
    .param p3, "value"    # I

    .prologue
    .line 532
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrintCallback:Lcom/sec/android/app/mobileprint/service/IPrintCallback;

    invoke-interface {v1, p1, p2, p3}, Lcom/sec/android/app/mobileprint/service/IPrintCallback;->responsePrint(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 536
    :goto_0
    monitor-exit p0

    return-void

    .line 533
    :catch_0
    move-exception v0

    .line 534
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 532
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public cancel(II)I
    .locals 5
    .param p1, "handle"    # I
    .param p2, "jobId"    # I

    .prologue
    .line 495
    const-string v2, "SamsungPrintService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SPrintPrintingService : cancel() handle="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    const/4 v0, 0x0

    .line 498
    .local v0, "result":Z
    iget-object v2, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mJobMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 499
    .local v1, "samJobId":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 500
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->cancelPrint(I)V

    .line 501
    const/4 v0, 0x1

    .line 518
    :cond_0
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public createPrinterInstance(ILjava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "connectType"    # I
    .param p2, "destination"    # Ljava/lang/String;
    .param p3, "modelName"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 240
    const-string v3, "SamsungPrintService"

    const-string v4, "SPrintPrintingService : createPrinterInstance()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->isSupportedPrinter(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 242
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    long-to-int v3, v3

    const v4, 0xffffff

    and-int v1, v3, v4

    .line 243
    .local v1, "handle":I
    new-instance v2, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;

    invoke-direct {v2, p1, p2, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 244
    .local v2, "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    if-nez p1, :cond_0

    .line 245
    invoke-direct {p0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->getUSBDevice()Landroid/hardware/usb/UsbDevice;

    move-result-object v0

    .line 246
    .local v0, "device":Landroid/hardware/usb/UsbDevice;
    if-eqz v0, :cond_1

    .line 248
    const-string v3, "SamsungPrintService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SPrintPrintingService :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v3

    iput v3, v2, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mPid:I

    .line 250
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v3

    iput v3, v2, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mVid:I

    .line 255
    .end local v0    # "device":Landroid/hardware/usb/UsbDevice;
    :cond_0
    :goto_0
    sget-object v3, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    const-string v3, "SamsungPrintService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SPrintPrintingService : handle="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    .end local v1    # "handle":I
    .end local v2    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :goto_1
    return v1

    .line 252
    .restart local v0    # "device":Landroid/hardware/usb/UsbDevice;
    .restart local v1    # "handle":I
    .restart local v2    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :cond_1
    const-string v3, "SamsungPrintService"

    const-string v4, "SPrintPrintingService :usbdevice is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 261
    .end local v0    # "device":Landroid/hardware/usb/UsbDevice;
    .end local v1    # "handle":I
    .end local v2    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getAPILevel()I
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x1

    return v0
.end method

.method public getCapabilities(ILcom/sec/android/app/mobileprint/service/ICapabilitiesCallback;)V
    .locals 19
    .param p1, "handle"    # I
    .param p2, "callback"    # Lcom/sec/android/app/mobileprint/service/ICapabilitiesCallback;

    .prologue
    .line 265
    const-string v16, "SamsungPrintService"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "SPrintPrintingService : getCapabilities() handle="

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    move-object/from16 v8, p2

    .line 268
    .local v8, "mCapCallback":Lcom/sec/android/app/mobileprint/service/ICapabilitiesCallback;
    sget-object v16, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    if-eqz v16, :cond_a

    .line 269
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 270
    .local v6, "hInteger":Ljava/lang/Integer;
    sget-object v16, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;

    .line 271
    .local v11, "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    if-eqz v11, :cond_9

    .line 273
    new-instance v2, Lcom/sec/android/app/mobileprint/service/Capabilities;

    invoke-direct {v2}, Lcom/sec/android/app/mobileprint/service/Capabilities;-><init>()V

    .line 275
    .local v2, "cap":Lcom/sec/android/app/mobileprint/service/Capabilities;
    new-instance v4, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-direct {v4}, Lcom/sec/print/mobileprint/dm/DeviceInfo;-><init>()V

    .line 276
    .local v4, "dInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    iget-object v0, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mModelName:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setName(Ljava/lang/String;)V

    .line 277
    iget-object v0, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mModelName:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNote(Ljava/lang/String;)V

    .line 278
    iget v0, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mConnectedType:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_3

    .line 279
    iget-object v0, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mIPAddr:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 280
    const/16 v16, 0x238c

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setPort(I)V

    .line 281
    invoke-virtual {v4}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNetworkDevice()V

    .line 289
    :goto_0
    const/4 v7, 0x0

    .line 291
    .local v7, "language":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getLanguages(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, v16

    check-cast v0, Ljava/util/ArrayList;

    move-object v7, v0

    .line 292
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v16

    if-lez v16, :cond_0

    .line 293
    iput-object v7, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mLanguages:Ljava/util/ArrayList;

    .line 294
    const/4 v3, 0x0

    .local v3, "count":I
    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v16

    move/from16 v0, v16

    if-lt v3, v0, :cond_4

    .line 303
    .end local v3    # "count":I
    :cond_0
    :goto_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getMediaTypes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v10

    check-cast v10, Ljava/util/ArrayList;

    .line 305
    .local v10, "mediaType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v16

    if-lez v16, :cond_1

    .line 306
    iput-object v10, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mPaperTypes:Ljava/util/ArrayList;

    .line 307
    sget-object v16, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperTypeList:Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/mobileprint/service/PaperTypeList;->getPaperNameList()Ljava/util/ArrayList;

    move-result-object v14

    .line 308
    .local v14, "refPaperType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .restart local v3    # "count":I
    :goto_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v16

    move/from16 v0, v16

    if-lt v3, v0, :cond_5

    .line 320
    .end local v3    # "count":I
    .end local v10    # "mediaType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v14    # "refPaperType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :goto_4
    :try_start_2
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    iput-object v0, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mMediaSizeReference:Ljava/util/ArrayList;

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getMediaSizes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v9

    .line 322
    .local v9, "mediaSize":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    if-eqz v9, :cond_2

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_2

    .line 323
    iput-object v9, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mMediaSize:Ljava/util/List;

    .line 324
    sget-object v16, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->getPaperNameList()Ljava/util/ArrayList;

    move-result-object v13

    .line 325
    .local v13, "refPaperName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .restart local v3    # "count":I
    :goto_5
    invoke-interface {v9}, Ljava/util/List;->size()I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v16

    move/from16 v0, v16

    if-lt v3, v0, :cond_7

    .line 340
    .end local v3    # "count":I
    .end local v9    # "mediaSize":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    .end local v13    # "refPaperName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    :goto_6
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v15

    .line 341
    .local v15, "ret":I
    const-string v16, "SamsungPrintService"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "SPrintPrintingService : isPrinterColorModel : ret="

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iput v15, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mColorModel:I

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mobileprint/service/Capabilities;->setColorType(I)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    .line 348
    .end local v15    # "ret":I
    :goto_7
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isSupportDuplex(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v15

    .line 349
    .restart local v15    # "ret":I
    const-string v16, "SamsungPrintService"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "SPrintPrintingService : isSupportDuplex : ret="

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iput v15, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mDuplexModel:I

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isSupportDuplex(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mobileprint/service/Capabilities;->setDuplexType(I)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    .line 356
    .end local v15    # "ret":I
    :goto_8
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mobileprint/service/Capabilities;->setPdfSupported(Z)V

    .line 359
    sget-object v16, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    :try_start_5
    move/from16 v0, p1

    invoke-interface {v8, v0, v2}, Lcom/sec/android/app/mobileprint/service/ICapabilitiesCallback;->responseCapabilities(ILcom/sec/android/app/mobileprint/service/Capabilities;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_5

    .line 384
    .end local v2    # "cap":Lcom/sec/android/app/mobileprint/service/Capabilities;
    .end local v4    # "dInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v6    # "hInteger":Ljava/lang/Integer;
    .end local v7    # "language":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v11    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :goto_9
    return-void

    .line 283
    .restart local v2    # "cap":Lcom/sec/android/app/mobileprint/service/Capabilities;
    .restart local v4    # "dInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .restart local v6    # "hInteger":Ljava/lang/Integer;
    .restart local v11    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :cond_3
    iget-object v0, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mIPAddr:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 284
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setPort(I)V

    .line 285
    invoke-virtual {v4}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setUSBDevice()V

    goto/16 :goto_0

    .line 295
    .restart local v3    # "count":I
    .restart local v7    # "language":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    :try_start_6
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mobileprint/service/Capabilities;->addAvaliableLanguage(Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0

    .line 294
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 298
    .end local v3    # "count":I
    :catch_0
    move-exception v5

    .line 299
    .local v5, "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2

    .line 309
    .end local v5    # "e":Landroid/os/RemoteException;
    .restart local v3    # "count":I
    .restart local v10    # "mediaType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14    # "refPaperType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    :try_start_7
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v12

    .line 310
    .local v12, "refIndex":I
    if-ltz v12, :cond_6

    .line 311
    invoke-virtual {v2, v12}, Lcom/sec/android/app/mobileprint/service/Capabilities;->addAvaliablePaperType(I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_1

    .line 308
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 315
    .end local v3    # "count":I
    .end local v10    # "mediaType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12    # "refIndex":I
    .end local v14    # "refPaperType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_1
    move-exception v5

    .line 316
    .restart local v5    # "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_4

    .line 327
    .end local v5    # "e":Landroid/os/RemoteException;
    .restart local v3    # "count":I
    .restart local v9    # "mediaSize":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    .restart local v13    # "refPaperName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_7
    :try_start_8
    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;->getMediaName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v12

    .line 328
    .restart local v12    # "refIndex":I
    if-ltz v12, :cond_8

    .line 329
    invoke-virtual {v2, v12}, Lcom/sec/android/app/mobileprint/service/Capabilities;->addAvaliablePaperSize(I)V

    .line 330
    iget-object v0, v11, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mMediaSizeReference:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_2

    .line 325
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    .line 335
    .end local v3    # "count":I
    .end local v9    # "mediaSize":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    .end local v12    # "refIndex":I
    .end local v13    # "refPaperName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_2
    move-exception v5

    .line 336
    .restart local v5    # "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_6

    .line 344
    .end local v5    # "e":Landroid/os/RemoteException;
    :catch_3
    move-exception v5

    .line 345
    .restart local v5    # "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_7

    .line 352
    .end local v5    # "e":Landroid/os/RemoteException;
    :catch_4
    move-exception v5

    .line 353
    .restart local v5    # "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_8

    .line 363
    .end local v5    # "e":Landroid/os/RemoteException;
    :catch_5
    move-exception v5

    .line 364
    .restart local v5    # "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_9

    .line 367
    .end local v2    # "cap":Lcom/sec/android/app/mobileprint/service/Capabilities;
    .end local v4    # "dInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v5    # "e":Landroid/os/RemoteException;
    .end local v7    # "language":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_9
    const-string v16, "SamsungPrintService"

    .line 368
    const-string v17, "SPrintPrintingService : getCapabilities(): printer is not in list, call createPrinterInstance first"

    .line 367
    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    const/16 v16, 0x0

    :try_start_9
    move/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v8, v0, v1}, Lcom/sec/android/app/mobileprint/service/ICapabilitiesCallback;->responseCapabilities(ILcom/sec/android/app/mobileprint/service/Capabilities;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_6

    goto/16 :goto_9

    .line 371
    :catch_6
    move-exception v5

    .line 372
    .restart local v5    # "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_9

    .line 376
    .end local v5    # "e":Landroid/os/RemoteException;
    .end local v6    # "hInteger":Ljava/lang/Integer;
    .end local v11    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :cond_a
    const-string v16, "SamsungPrintService"

    .line 377
    const-string v17, "SPrintPrintingService : getCapabilities(): printer list is empty"

    .line 376
    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    const/16 v16, 0x0

    :try_start_a
    move/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v8, v0, v1}, Lcom/sec/android/app/mobileprint/service/ICapabilitiesCallback;->responseCapabilities(ILcom/sec/android/app/mobileprint/service/Capabilities;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_7

    goto/16 :goto_9

    .line 380
    :catch_7
    move-exception v5

    .line 381
    .restart local v5    # "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_9
.end method

.method public getPrinterStatus(I)I
    .locals 7
    .param p1, "handle"    # I

    .prologue
    const/4 v3, 0x0

    .line 387
    const-string v5, "SamsungPrintService"

    const-string v6, "SPrintPrintingService : getPrinterStatus()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    sget-object v5, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    if-eqz v5, :cond_0

    .line 389
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 390
    .local v2, "hInteger":Ljava/lang/Integer;
    sget-object v5, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;

    .line 391
    .local v4, "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    if-eqz v4, :cond_0

    .line 392
    new-instance v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;-><init>()V

    .line 393
    .local v0, "dInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    iget-object v5, v4, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mModelName:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setName(Ljava/lang/String;)V

    .line 394
    iget-object v5, v4, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mModelName:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNote(Ljava/lang/String;)V

    .line 395
    iget v5, v4, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mConnectedType:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 396
    iget-object v5, v4, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mIPAddr:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 397
    const/16 v5, 0x238c

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setPort(I)V

    .line 398
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNetworkDevice()V

    .line 406
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    invoke-interface {v5, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isPrinterAlive(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 416
    .end local v0    # "dInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v2    # "hInteger":Ljava/lang/Integer;
    .end local v4    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :cond_0
    :goto_1
    return v3

    .line 400
    .restart local v0    # "dInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .restart local v2    # "hInteger":Ljava/lang/Integer;
    .restart local v4    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :cond_1
    iget-object v5, v4, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mIPAddr:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 401
    invoke-virtual {v0, v3}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setPort(I)V

    .line 402
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setUSBDevice()V

    goto :goto_0

    .line 409
    :catch_0
    move-exception v1

    .line 410
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public getReferencePaperSizeList()Lcom/sec/android/app/mobileprint/service/PaperSizeList;
    .locals 1

    .prologue
    .line 522
    sget-object v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    return-object v0
.end method

.method public getReferencePaperTypeList()Lcom/sec/android/app/mobileprint/service/PaperTypeList;
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperTypeList:Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    return-object v0
.end method

.method public getVendorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    const-string v0, "Samsung"

    return-object v0
.end method

.method public isSupportedPrinter(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "connectType"    # I
    .param p2, "destination"    # Ljava/lang/String;
    .param p3, "modelName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 181
    const-string v6, "SamsungPrintService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SPrintPrintingService : isSupportedPrinter() : connectType="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    const-string v6, "SamsungPrintService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SPrintPrintingService : isSupportedPrinter() : destination="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    const-string v6, "SamsungPrintService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SPrintPrintingService : isSupportedPrinter() : modelName="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    new-instance v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;-><init>()V

    .line 186
    .local v0, "dInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    if-ne p1, v5, :cond_0

    .line 187
    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setConnectionType(I)V

    .line 188
    invoke-virtual {v0, p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 189
    const/16 v6, 0x238c

    invoke-virtual {v0, v6}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setPort(I)V

    .line 190
    invoke-virtual {v0, p3}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setName(Ljava/lang/String;)V

    .line 191
    invoke-virtual {v0, p3}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNote(Ljava/lang/String;)V

    .line 192
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNetworkDevice()V

    .line 202
    :goto_0
    const/4 v3, 0x0

    .line 203
    .local v3, "waitCount":I
    :goto_1
    iget-object v6, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    if-eqz v6, :cond_1

    .line 217
    :goto_2
    iget-object v6, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    if-nez v6, :cond_3

    .line 234
    :goto_3
    return v4

    .line 194
    .end local v3    # "waitCount":I
    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setConnectionType(I)V

    .line 195
    invoke-virtual {v0, p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v0, v4}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setPort(I)V

    .line 197
    invoke-virtual {v0, p3}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setName(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v0, p3}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNote(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setUSBDevice()V

    goto :goto_0

    .line 204
    .restart local v3    # "waitCount":I
    :cond_1
    const/16 v6, 0x64

    if-le v3, v6, :cond_2

    .line 205
    const-string v6, "SamsungPrintService"

    const-string v7, "PrintThread : mSAPSPrintService waiting timeout!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 209
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 212
    const-wide/16 v6, 0xc8

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 213
    :catch_0
    move-exception v1

    .line 214
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 220
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_3
    const/4 v2, 0x0

    .line 222
    .local v2, "supported":I
    :try_start_1
    iget-object v6, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDeviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    invoke-interface {v6, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isSupportedPrinter(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 229
    if-nez v2, :cond_4

    .line 230
    const-string v5, "SamsungPrintService"

    const-string v6, "SPrintPrintingService : isSupportedPrinter : Not supported device"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 223
    :catch_1
    move-exception v1

    .line 224
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 225
    const-string v5, "SamsungPrintService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SPrintPrintingService : isSupportedPrinter :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 233
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_4
    const-string v4, "SamsungPrintService"

    const-string v6, "SPrintPrintingService : isSupportedPrinter : supported device"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 234
    goto :goto_3
.end method

.method public isUsbPrintingSupport()Z
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x1

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 135
    const-string v0, "com.sec.intent.action.PRINT_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    sget-object v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mIPrintService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;

    .line 139
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized onCreate()V
    .locals 5

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 101
    const-string v1, "SamsungPrintService"

    const-string v2, "SPrintPrintingService : onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;

    invoke-direct {v1, p0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;-><init>(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;)V

    sput-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mIPrintService:Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintExtendIPrintServiceAidl;

    .line 105
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    .line 107
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mJobMap:Ljava/util/HashMap;

    .line 109
    new-instance v1, Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    invoke-direct {v1}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;-><init>()V

    sput-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    .line 110
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    const-string v2, "A3"

    const/16 v3, 0xdb4

    const/16 v4, 0x1361

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->addPaper(Ljava/lang/String;II)Z

    .line 111
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    const-string v2, "A4"

    const/16 v3, 0x9b0

    const/16 v4, 0xdb3

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->addPaper(Ljava/lang/String;II)Z

    .line 112
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    const-string v2, "A5"

    const/16 v3, 0x6d4

    const/16 v4, 0x9b0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->addPaper(Ljava/lang/String;II)Z

    .line 113
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    const-string v2, "JISB5"

    const/16 v3, 0x865

    const/16 v4, 0xbdb

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->addPaper(Ljava/lang/String;II)Z

    .line 114
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    const-string v2, "Letter"

    const/16 v3, 0x9f6

    const/16 v4, 0xce4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->addPaper(Ljava/lang/String;II)Z

    .line 115
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    const-string v2, "Legal"

    const/16 v3, 0x9f6

    const/16 v4, 0x1068

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->addPaper(Ljava/lang/String;II)Z

    .line 116
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    const-string v2, "Executive"

    const/16 v3, 0x87f

    const/16 v4, 0xc4e

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->addPaper(Ljava/lang/String;II)Z

    .line 117
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    const-string v2, "Folio"

    const/16 v3, 0x9f6

    const/16 v4, 0xf3c

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->addPaper(Ljava/lang/String;II)Z

    .line 119
    new-instance v1, Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    invoke-direct {v1}, Lcom/sec/android/app/mobileprint/service/PaperTypeList;-><init>()V

    sput-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperTypeList:Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    .line 120
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperTypeList:Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    const-string v2, "Normal"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mobileprint/service/PaperTypeList;->addPaper(Ljava/lang/String;)Z

    .line 121
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperTypeList:Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    const-string v2, "Thick"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mobileprint/service/PaperTypeList;->addPaper(Ljava/lang/String;)Z

    .line 122
    sget-object v1, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperTypeList:Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    const-string v2, "Thin"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mobileprint/service/PaperTypeList;->addPaper(Ljava/lang/String;)Z

    .line 124
    new-instance v0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;-><init>(Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService$BindServiceThread;)V

    .line 125
    .local v0, "bindThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    monitor-exit p0

    return-void

    .line 100
    .end local v0    # "bindThread":Ljava/lang/Thread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 156
    const-string v0, "SamsungPrintService"

    const-string v1, "SPrintPrintingService : onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDevManagerConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mSAPSDevManagerConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->unbindService(Landroid/content/ServiceConnection;)V

    .line 165
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 166
    return-void
.end method

.method public onJobStatusUpdated(III)V
    .locals 4
    .param p1, "samJobId"    # I
    .param p2, "type"    # I
    .param p3, "value"    # I

    .prologue
    const/4 v3, 0x5

    .line 815
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->getJobHandlefromMap(I)Ljava/lang/Integer;

    move-result-object v0

    .line 816
    .local v0, "jobId":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 817
    const/4 v1, 0x6

    if-ne p2, v1, :cond_1

    .line 818
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->responsePrintError(II)V

    .line 819
    iget-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mJobMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 820
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->removeUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V

    .line 840
    :cond_0
    :goto_0
    return-void

    .line 821
    :cond_1
    if-ge p2, v3, :cond_2

    .line 822
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, p2, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->responsePrintStatus(III)V

    goto :goto_0

    .line 823
    :cond_2
    if-ne p2, v3, :cond_3

    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v1

    sget-object v2, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_USB:Lcom/sec/mobileprint/core/print/ConnectionType;

    if-ne v1, v2, :cond_3

    .line 824
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, p2, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->responsePrintStatus(III)V

    goto :goto_0

    .line 826
    :cond_3
    if-ne p2, v3, :cond_4

    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v1

    sget-object v2, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

    if-ne v1, v2, :cond_4

    .line 827
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->responsePrintCompleted(II)V

    .line 828
    iget-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mJobMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 829
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->removeUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V

    goto :goto_0

    .line 830
    :cond_4
    const/16 v1, 0xc

    if-ne p2, v1, :cond_5

    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v1

    sget-object v2, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_USB:Lcom/sec/mobileprint/core/print/ConnectionType;

    if-ne v1, v2, :cond_5

    .line 831
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, p2, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->responsePrintStatus(III)V

    goto :goto_0

    .line 832
    :cond_5
    const/16 v1, 0xa

    if-eq p2, v1, :cond_6

    const/16 v1, 0xb

    if-ne p2, v1, :cond_7

    .line 833
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, p2, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->responsePrintStatus(III)V

    goto :goto_0

    .line 834
    :cond_7
    const/16 v1, 0xd

    if-ne p2, v1, :cond_0

    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v1

    sget-object v2, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_USB:Lcom/sec/mobileprint/core/print/ConnectionType;

    if-ne v1, v2, :cond_0

    .line 835
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, p3}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->responsePrintCompleted(II)V

    .line 836
    iget-object v1, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mJobMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 837
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->removeUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V

    goto/16 :goto_0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 150
    const-string v0, "SamsungPrintService"

    const-string v1, "SPrintPrintingService : onRebind()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 152
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 144
    const-string v0, "SamsungPrintService"

    const-string v1, "SPrintPrintingService : onUnbind()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public print(ILcom/sec/android/app/mobileprint/service/PrintSettings;Lcom/sec/android/app/mobileprint/service/IPrintCallback;)I
    .locals 12
    .param p1, "handle"    # I
    .param p2, "settings"    # Lcom/sec/android/app/mobileprint/service/PrintSettings;
    .param p3, "callback"    # Lcom/sec/android/app/mobileprint/service/IPrintCallback;

    .prologue
    .line 420
    const-string v9, "SamsungPrintService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "SPrintPrintingService : print() handle="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    iput-object p3, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrintCallback:Lcom/sec/android/app/mobileprint/service/IPrintCallback;

    .line 424
    const/4 v4, 0x0

    .line 425
    .local v4, "jobId":I
    sget-object v9, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    if-eqz v9, :cond_8

    .line 426
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 427
    .local v2, "hInteger":Ljava/lang/Integer;
    sget-object v9, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mPrinterList:Ljava/util/HashMap;

    invoke-virtual {v9, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;

    .line 428
    .local v7, "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    if-eqz v7, :cond_7

    .line 429
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    long-to-int v9, v9

    const v10, 0xffff

    and-int v4, v9, v10

    .line 430
    iput v4, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mCurrentJobId:I

    .line 431
    const-string v9, "SamsungPrintService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "mCurrentJobId "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mCurrentJobId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    new-instance v6, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {v6}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;-><init>()V

    .line 435
    .local v6, "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    sget-object v9, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperSizeList:Lcom/sec/android/app/mobileprint/service/PaperSizeList;

    invoke-virtual {p2}, Lcom/sec/android/app/mobileprint/service/PrintSettings;->getPaperSize()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->getPaperName(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setMediaSize(Ljava/lang/String;)V

    .line 437
    sget-object v9, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mRefPaperTypeList:Lcom/sec/android/app/mobileprint/service/PaperTypeList;

    invoke-virtual {p2}, Lcom/sec/android/app/mobileprint/service/PrintSettings;->getPaperType()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/mobileprint/service/PaperTypeList;->getPaperName(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setMediaType(Ljava/lang/String;)V

    .line 439
    invoke-virtual {p2}, Lcom/sec/android/app/mobileprint/service/PrintSettings;->getColorType()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->COLOR:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    :goto_0
    invoke-virtual {v6, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setColor(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V

    .line 441
    invoke-virtual {p2}, Lcom/sec/android/app/mobileprint/service/PrintSettings;->getDuplexType()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_2

    .line 442
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_TWO_SIDE_SHORT_EDGE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    invoke-virtual {v6, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    .line 449
    :goto_1
    invoke-virtual {p2}, Lcom/sec/android/app/mobileprint/service/PrintSettings;->getOrientationType()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_4

    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;->ORIENTATION_LANDSCAPE:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    :goto_2
    invoke-virtual {v6, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setOrientation(Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;)V

    .line 451
    invoke-virtual {p2}, Lcom/sec/android/app/mobileprint/service/PrintSettings;->getCopies()I

    move-result v9

    invoke-virtual {v6, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setCopies(I)V

    .line 454
    sget v9, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_PHOTO:I

    invoke-virtual {v6, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setContentType(I)V

    .line 456
    const/4 v3, 0x0

    .line 457
    .local v3, "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    invoke-virtual {p2}, Lcom/sec/android/app/mobileprint/service/PrintSettings;->getContentList()Ljava/util/ArrayList;

    move-result-object v0

    .line 458
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 459
    .local v1, "contents":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 460
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "contents":[Ljava/lang/String;
    check-cast v1, [Ljava/lang/String;

    .line 463
    .restart local v1    # "contents":[Ljava/lang/String;
    :cond_0
    const/4 v9, 0x0

    aget-object v9, v1, v9

    invoke-static {v9}, Lcom/sec/mobileprint/core/utils/Utils;->isSupportedImage(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 465
    const/4 v9, 0x3

    invoke-direct {p0, v4, v9}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->responsePrintError(II)V

    move v5, v4

    .line 491
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "contents":[Ljava/lang/String;
    .end local v2    # "hInteger":Ljava/lang/Integer;
    .end local v3    # "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .end local v4    # "jobId":I
    .end local v6    # "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .end local v7    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    .local v5, "jobId":I
    :goto_3
    return v5

    .line 439
    .end local v5    # "jobId":I
    .restart local v2    # "hInteger":Ljava/lang/Integer;
    .restart local v4    # "jobId":I
    .restart local v6    # "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .restart local v7    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :cond_1
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->MONOCHROME:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    goto :goto_0

    .line 443
    :cond_2
    invoke-virtual {p2}, Lcom/sec/android/app/mobileprint/service/PrintSettings;->getDuplexType()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 444
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_TWO_SIDE_LONG_EDGE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    invoke-virtual {v6, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    goto :goto_1

    .line 446
    :cond_3
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_ONE_SIDE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    invoke-virtual {v6, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    goto :goto_1

    .line 449
    :cond_4
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;->ORIENTATION_PORTRAINT:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    goto :goto_2

    .line 469
    .restart local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v1    # "contents":[Ljava/lang/String;
    .restart local v3    # "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    :cond_5
    iget v9, v7, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mConnectedType:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_6

    .line 471
    iget-object v9, v7, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mModelName:Ljava/lang/String;

    iget-object v10, v7, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mIPAddr:Ljava/lang/String;

    invoke-static {v6, v9, v10, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->createPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v3

    .line 475
    :goto_4
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)I

    move-result v8

    .line 477
    .local v8, "samJobid":I
    iget-object v9, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mJobMap:Ljava/util/HashMap;

    iget v10, p0, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->mCurrentJobId:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v9

    invoke-virtual {v9, p0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V

    .line 482
    const/4 v9, -0x1

    const/4 v10, 0x0

    invoke-direct {p0, v4, v9, v10}, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrintingService;->responsePrintStatus(III)V

    .line 490
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "contents":[Ljava/lang/String;
    .end local v2    # "hInteger":Ljava/lang/Integer;
    .end local v3    # "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .end local v6    # "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .end local v7    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    .end local v8    # "samJobid":I
    :goto_5
    const-string v9, "SamsungPrintService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Returning Jobid  "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 491
    .end local v4    # "jobId":I
    .restart local v5    # "jobId":I
    goto :goto_3

    .line 473
    .end local v5    # "jobId":I
    .restart local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v1    # "contents":[Ljava/lang/String;
    .restart local v2    # "hInteger":Ljava/lang/Integer;
    .restart local v3    # "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .restart local v4    # "jobId":I
    .restart local v6    # "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .restart local v7    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :cond_6
    iget-object v9, v7, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mModelName:Ljava/lang/String;

    iget v10, v7, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mPid:I

    iget v11, v7, Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;->mVid:I

    invoke-static {v6, v9, v10, v11, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->createPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;II[Ljava/lang/String;)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v3

    goto :goto_4

    .line 485
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "contents":[Ljava/lang/String;
    .end local v3    # "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .end local v6    # "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    :cond_7
    const-string v9, "SamsungPrintService"

    const-string v10, "SPrintPrintingService : print(): printer is not in list, call createPrinterInstance first"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 488
    .end local v2    # "hInteger":Ljava/lang/Integer;
    .end local v7    # "printer":Lcom/sec/mobileprint/samsung/galaxy/plugin/SPrintPrinterItem;
    :cond_8
    const-string v9, "SamsungPrintService"

    const-string v10, "SPrintPrintingService : print(): printer list is empty"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method
