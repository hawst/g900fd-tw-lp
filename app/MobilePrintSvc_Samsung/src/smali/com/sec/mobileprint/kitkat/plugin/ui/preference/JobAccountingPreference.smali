.class public Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;
.super Landroid/preference/DialogPreference;
.source "JobAccountingPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference$MyOnItemSelectedListener;
    }
.end annotation


# static fields
.field private static sharedPref:Landroid/content/SharedPreferences;


# instance fields
.field mCancelBtn:Landroid/widget/Button;

.field mCheckBoxJobAccounting:Landroid/widget/CheckBox;

.field mCheckBoxShowPassword:Landroid/widget/CheckBox;

.field mContext:Landroid/content/Context;

.field mEditTextUserID:Landroid/widget/EditText;

.field mEditTextUserPW:Landroid/widget/EditText;

.field mIDOnly:Z

.field mOkBtn:Landroid/widget/Button;

.field mPassword:Ljava/lang/String;

.field mPermission:Ljava/lang/String;

.field mPermissionInt:I

.field mShow:Z

.field mSpinnerPermission:Landroid/widget/Spinner;

.field mUse:Z

.field mUserID:Ljava/lang/String;

.field private tmpPassword:Ljava/lang/String;

.field private tmpUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    iput-boolean v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUse:Z

    .line 44
    iput-boolean v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mShow:Z

    .line 45
    iput-boolean v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mIDOnly:Z

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUserID:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPassword:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPermission:Ljava/lang/String;

    .line 49
    iput v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPermissionInt:I

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->tmpUserID:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->tmpPassword:Ljava/lang/String;

    .line 57
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mContext:Landroid/content/Context;

    .line 58
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setDialogLayoutResource(I)V

    .line 60
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;)V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->checkOkButtonEnable()V

    return-void
.end method

.method private checkOkButtonEnable()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 198
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxJobAccounting:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mOkBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 207
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private getJobAccountingInputItems()V
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->isJobAccountingUse(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUse:Z

    .line 267
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getJobAccountingUserID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUserID:Ljava/lang/String;

    .line 268
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getJobAccountingPassword(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPassword:Ljava/lang/String;

    .line 269
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mShow:Z

    .line 270
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getJobAccountingPermission(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPermissionInt:I

    .line 271
    return-void
.end method

.method public static declared-synchronized getJobAccountingPassword(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 409
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_PASSWORD:Ljava/lang/String;

    sget-object v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_DEFAULT:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getJobAccountingPermission(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 419
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_GROUP_PERMISSION:Ljava/lang/String;

    sget v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_PERMISSION_DEFAULT:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getJobAccountingUserID(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 404
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_USER_NAME:Ljava/lang/String;

    sget-object v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_DEFAULT:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 382
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->sharedPref:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 383
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->sharedPref:Landroid/content/SharedPreferences;

    .line 385
    :cond_0
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->sharedPref:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized isJobAccountingUse(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 414
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_USE:Ljava/lang/String;

    sget-boolean v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_USE_DEFAULT:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized isSupportedJobAccounting(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 423
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "job_accounting_support"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setJobAccountingInputItem(Z)V
    .locals 4
    .param p1, "use"    # Z

    .prologue
    const/4 v3, 0x0

    .line 275
    if-nez p1, :cond_0

    .line 276
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 277
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 278
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 280
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 281
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 282
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 283
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mSpinnerPermission:Landroid/widget/Spinner;

    invoke-virtual {v1, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 286
    return-void
.end method

.method public static declared-synchronized setSupportedJobAccounting(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isSupportJobAccounting"    # Z

    .prologue
    .line 427
    const-class v3, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    monitor-enter v3

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 429
    .local v0, "p":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 431
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "job_accounting_support"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 433
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 434
    monitor-exit v3

    return-void

    .line 427
    .end local v0    # "p":Landroid/content/SharedPreferences;
    .end local v1    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized setUseJobAccounting(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isUse"    # Z

    .prologue
    .line 437
    const-class v3, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    monitor-enter v3

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 439
    .local v0, "p":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 441
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_USE:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 443
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444
    monitor-exit v3

    return-void

    .line 437
    .end local v0    # "p":Landroid/content/SharedPreferences;
    .end local v1    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method


# virtual methods
.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getPermission()I
    .locals 1

    .prologue
    .line 349
    iget v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPermissionInt:I

    return v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method public isShow()Z
    .locals 1

    .prologue
    .line 306
    iget-boolean v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mShow:Z

    return v0
.end method

.method public isUse()Z
    .locals 1

    .prologue
    .line 292
    iget-boolean v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUse:Z

    return v0
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v5, 0x7f080002

    const/4 v4, 0x0

    .line 69
    const v2, 0x7f0c001d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxJobAccounting:Landroid/widget/CheckBox;

    .line 70
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxJobAccounting:Landroid/widget/CheckBox;

    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 71
    const v2, 0x7f0c001f

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    .line 72
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 73
    const v2, 0x7f0c0021

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    .line 74
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 75
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 77
    const v2, 0x7f0c0022

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    .line 78
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 81
    const v2, 0x7f0c0027

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mSpinnerPermission:Landroid/widget/Spinner;

    .line 82
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mContext:Landroid/content/Context;

    const v3, 0x1090008

    invoke-static {v2, v5, v3}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 83
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 84
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mSpinnerPermission:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 85
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mSpinnerPermission:Landroid/widget/Spinner;

    new-instance v3, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference$MyOnItemSelectedListener;

    invoke-direct {v3, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference$MyOnItemSelectedListener;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 87
    const v2, 0x7f0c0024

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mOkBtn:Landroid/widget/Button;

    .line 88
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    const v2, 0x7f0c0023

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCancelBtn:Landroid/widget/Button;

    .line 90
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCancelBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getJobAccountingInputItems()V

    .line 94
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxJobAccounting:Landroid/widget/CheckBox;

    iget-boolean v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUse:Z

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 95
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUserID:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->tmpUserID:Ljava/lang/String;

    .line 96
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPassword:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->tmpPassword:Ljava/lang/String;

    .line 97
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUserID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    new-instance v3, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference$1;

    invoke-direct {v3, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference$1;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 125
    iget-boolean v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mIDOnly:Z

    if-nez v2, :cond_1

    .line 126
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    iget-boolean v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mShow:Z

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 128
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPassword:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 135
    :goto_0
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    new-instance v3, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference$2;

    invoke-direct {v3, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference$2;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 180
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->checkOkButtonEnable()V

    .line 181
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "permissionString":[Ljava/lang/String;
    iget v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPermissionInt:I

    aget-object v2, v1, v2

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPermission:Ljava/lang/String;

    .line 183
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mSpinnerPermission:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPermission:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 185
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    const v3, 0x80001

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 189
    :cond_0
    iget-boolean v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUse:Z

    invoke-direct {p0, v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setJobAccountingInputItem(Z)V

    .line 190
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {p0, v2, v4}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 193
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 194
    return-void

    .line 130
    .end local v1    # "permissionString":[Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 131
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "checked"    # Z

    .prologue
    .line 241
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxJobAccounting:Landroid/widget/CheckBox;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    invoke-direct {p0, p2}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setJobAccountingInputItem(Z)V

    .line 244
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->checkOkButtonEnable()V

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 250
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 251
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    const v2, 0x80001

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 259
    :goto_0
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 260
    .local v0, "textLength":I
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 262
    .end local v0    # "textLength":I
    :cond_1
    return-void

    .line 254
    :cond_2
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    const v2, 0x80081

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mOkBtn:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 368
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->onDialogClosed(Z)V

    .line 369
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCancelBtn:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 371
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    goto :goto_0
.end method

.method protected onDialogClosed(Z)V
    .locals 6
    .param p1, "positiveResult"    # Z

    .prologue
    .line 219
    if-nez p1, :cond_0

    .line 236
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 223
    .local v2, "userid":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 224
    .local v3, "password":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mCheckBoxJobAccounting:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    .line 225
    .local v4, "isUse":Z
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mSpinnerPermission:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v5

    .line 227
    .local v5, "permission":I
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mContext:Landroid/content/Context;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setJobAccountingInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZI)V

    .line 233
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->callChangeListener(Ljava/lang/Object;)Z

    .line 235
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 451
    :try_start_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    if-ne p1, v0, :cond_1

    if-eqz p2, :cond_1

    .line 452
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 453
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 456
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 457
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 460
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 1
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v0, 0x0

    .line 212
    invoke-virtual {p1, v0, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 213
    invoke-virtual {p1, v0, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 214
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    .line 215
    return-void
.end method

.method public setIDOnly(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 345
    iput-boolean p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mIDOnly:Z

    .line 346
    return-void
.end method

.method public declared-synchronized setJobAccountingInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "isUse"    # Z
    .param p5, "permission"    # I

    .prologue
    .line 390
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 392
    .local v0, "p":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 394
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_USER_NAME:Ljava/lang/String;

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 395
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_PASSWORD:Ljava/lang/String;

    invoke-interface {v1, v2, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 396
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_USE:Ljava/lang/String;

    invoke-interface {v1, v2, p4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 397
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_GROUP_PERMISSION:Ljava/lang/String;

    invoke-interface {v1, v2, p5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 399
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    monitor-exit p0

    return-void

    .line 390
    .end local v0    # "p":Landroid/content/SharedPreferences;
    .end local v1    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPassword"    # Ljava/lang/String;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mPassword:Ljava/lang/String;

    .line 342
    return-void
.end method

.method public setShow(Z)V
    .locals 0
    .param p1, "mShow"    # Z

    .prologue
    .line 313
    iput-boolean p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mShow:Z

    .line 314
    return-void
.end method

.method public setUse(Z)V
    .locals 0
    .param p1, "mUse"    # Z

    .prologue
    .line 299
    iput-boolean p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUse:Z

    .line 300
    return-void
.end method

.method public setUserID(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUserID"    # Ljava/lang/String;

    .prologue
    .line 327
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->mUserID:Ljava/lang/String;

    .line 328
    return-void
.end method
