.class public Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;
.super Landroid/printservice/PrintService;
.source "SamsungKitKatPrintService.java"

# interfaces
.implements Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field mJobMap:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Landroid/print/PrintJobId;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/printservice/PrintService;-><init>()V

    .line 43
    sput-object p0, Lcom/sec/mobileprint/core/App;->service:Ljava/lang/Object;

    .line 44
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->mJobMap:Ljava/util/Hashtable;

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;I)Landroid/printservice/PrintJob;
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->getPrintJobfromMap(I)Landroid/printservice/PrintJob;

    move-result-object v0

    return-object v0
.end method

.method private getPrintJobfromMap(I)Landroid/printservice/PrintJob;
    .locals 8
    .param p1, "jobId"    # I

    .prologue
    .line 236
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 249
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 236
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 237
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/print/PrintJobId;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/print/PrintJobId;

    .line 238
    .local v3, "key":Landroid/print/PrintJobId;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 240
    .local v4, "value":Ljava/lang/Integer;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ne v6, p1, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->getActivePrintJobs()Ljava/util/List;

    move-result-object v2

    .line 242
    .local v2, "jobList":Ljava/util/List;, "Ljava/util/List<Landroid/printservice/PrintJob;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/printservice/PrintJob;

    .line 243
    .local v1, "job":Landroid/printservice/PrintJob;
    invoke-virtual {v1}, Landroid/printservice/PrintJob;->getId()Landroid/print/PrintJobId;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/print/PrintJobId;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Landroid/printservice/PrintService;->onCreate()V

    .line 51
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V

    .line 52
    return-void
.end method

.method protected onCreatePrinterDiscoverySession()Landroid/printservice/PrinterDiscoverySession;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;

    invoke-direct {v0}, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;-><init>()V

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Landroid/printservice/PrintService;->onDestroy()V

    .line 58
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->removeUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V

    .line 59
    return-void
.end method

.method public onJobStatusUpdated(III)V
    .locals 3
    .param p1, "jobId"    # I
    .param p2, "type"    # I
    .param p3, "value"    # I

    .prologue
    .line 191
    new-instance v0, Landroid/os/Handler;

    sget-object v2, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 192
    .local v0, "mainHandler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;-><init>(Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;III)V

    .line 232
    .local v1, "myRunnable":Ljava/lang/Runnable;
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 233
    return-void
.end method

.method protected onPrintJobQueued(Landroid/printservice/PrintJob;)V
    .locals 24
    .param p1, "printJob"    # Landroid/printservice/PrintJob;

    .prologue
    .line 71
    invoke-virtual/range {p1 .. p1}, Landroid/printservice/PrintJob;->getInfo()Landroid/print/PrintJobInfo;

    move-result-object v6

    .line 72
    .local v6, "jobInfo":Landroid/print/PrintJobInfo;
    new-instance v12, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {v12}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;-><init>()V

    .line 80
    .local v12, "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    invoke-virtual/range {p1 .. p1}, Landroid/printservice/PrintJob;->getDocument()Landroid/printservice/PrintDocument;

    move-result-object v7

    .line 81
    .local v7, "localDocument":Landroid/printservice/PrintDocument;
    invoke-virtual {v7}, Landroid/printservice/PrintDocument;->getInfo()Landroid/print/PrintDocumentInfo;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/print/PrintDocumentInfo;->getContentType()I

    .line 83
    invoke-virtual {v6}, Landroid/print/PrintJobInfo;->getCopies()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setCopies(I)V

    .line 85
    invoke-virtual {v6}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v19

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    sget-object v19, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->COLOR:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    :goto_0
    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setColor(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V

    .line 87
    invoke-virtual {v6}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;->getMediaSize(Landroid/print/PrintAttributes$MediaSize;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setMediaSize(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v6}, Landroid/print/PrintJobInfo;->getAttributes()Landroid/print/PrintAttributes;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/print/PrintAttributes;->getMediaSize()Landroid/print/PrintAttributes$MediaSize;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;->getMediaSize(Landroid/print/PrintAttributes$MediaSize;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;->getMediatype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setMediaType(Ljava/lang/String;)V

    .line 92
    sget v19, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_DOCUMENT:I

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setContentType(I)V

    .line 93
    invoke-virtual {v7}, Landroid/printservice/PrintDocument;->getInfo()Landroid/print/PrintDocumentInfo;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/print/PrintDocumentInfo;->getContentType()I

    move-result v19

    if-nez v19, :cond_5

    sget v19, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_DOCUMENT:I

    :goto_1
    move/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setAppContentType(I)V

    .line 97
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 99
    .local v13, "sharedPrefs":Landroid/content/SharedPreferences;
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_DUPLEX:Ljava/lang/String;

    sget-object v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_DUPLEX_DEFAULT:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 100
    .local v4, "duplex":Ljava/lang/String;
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_DUPLEX_SIMPLEX:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 101
    sget-object v19, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_ONE_SIDE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    .line 109
    :goto_2
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_USE:Ljava/lang/String;

    sget-boolean v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_USE_DEFAULT:Z

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    .line 110
    .local v16, "useJobAcc":Z
    if-eqz v16, :cond_0

    .line 111
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_USER_NAME:Ljava/lang/String;

    sget-object v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_DEFAULT:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 112
    .local v18, "userName":Ljava/lang/String;
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_PASSWORD:Ljava/lang/String;

    sget-object v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_DEFAULT:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 113
    .local v9, "password":Ljava/lang/String;
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_GROUP_PERMISSION:Ljava/lang/String;

    sget v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_PERMISSION_DEFAULT:I

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 114
    .local v10, "permission":I
    if-nez v10, :cond_8

    const/16 v19, 0x0

    :goto_3
    move/from16 v0, v16

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v9, v2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setJobAccounttingInfo(ZLjava/lang/String;Ljava/lang/String;Z)V

    .line 118
    .end local v9    # "password":Ljava/lang/String;
    .end local v10    # "permission":I
    .end local v18    # "userName":Ljava/lang/String;
    :cond_0
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_USE:Ljava/lang/String;

    sget-boolean v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_CONFIDENTIAL_USE_DEFAULT:Z

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    .line 119
    .local v15, "useConfidential":Z
    if-eqz v15, :cond_1

    .line 120
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_USER_NAME:Ljava/lang/String;

    sget-object v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_CONFIDENTIAL_DEFAULT:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 121
    .restart local v18    # "userName":Ljava/lang/String;
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_PASSWORD:Ljava/lang/String;

    sget-object v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_CONFIDENTIAL_DEFAULT:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 123
    .restart local v9    # "password":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v12, v15, v0, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setConfidentialInfo(ZLjava/lang/String;Ljava/lang/String;)V

    .line 127
    .end local v9    # "password":Ljava/lang/String;
    .end local v18    # "userName":Ljava/lang/String;
    :cond_1
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_SECURE_USE:Ljava/lang/String;

    sget-boolean v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_SECURE_USE_DEFAULT:Z

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    .line 128
    .local v17, "useSecureRelease":Z
    if-eqz v17, :cond_2

    .line 129
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_SECURE_USER_NAME:Ljava/lang/String;

    sget-object v20, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_SECURE_DEFAULT:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 131
    .restart local v18    # "userName":Ljava/lang/String;
    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setSecureReleaseInfo(ZLjava/lang/String;)V

    .line 136
    .end local v18    # "userName":Ljava/lang/String;
    :cond_2
    invoke-virtual {v6}, Landroid/print/PrintJobInfo;->getPages()[Landroid/print/PageRange;

    move-result-object v8

    .line 137
    .local v8, "pageRange":[Landroid/print/PageRange;
    if-eqz v8, :cond_3

    array-length v0, v8

    move/from16 v19, v0

    if-lez v19, :cond_3

    .line 138
    array-length v0, v8

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v14, v0, [Lcom/sec/mobileprint/core/print/SamsungPageRange;

    .line 139
    .local v14, "spr":[Lcom/sec/mobileprint/core/print/SamsungPageRange;
    const/4 v5, 0x0

    .line 140
    .local v5, "index":I
    array-length v0, v8

    move/from16 v20, v0

    const/16 v19, 0x0

    :goto_4
    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_9

    .line 145
    invoke-virtual {v12, v14}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setPageRange([Lcom/sec/mobileprint/core/print/SamsungPageRange;)V

    .line 150
    .end local v5    # "index":I
    .end local v14    # "spr":[Lcom/sec/mobileprint/core/print/SamsungPageRange;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->mJobMap:Ljava/util/Hashtable;

    move-object/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Landroid/printservice/PrintJob;->getId()Landroid/print/PrintJobId;

    move-result-object v20

    const/16 v21, -0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    invoke-virtual/range {p1 .. p1}, Landroid/printservice/PrintJob;->getInfo()Landroid/print/PrintJobInfo;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/print/PrintJobInfo;->getPrinterId()Landroid/print/PrinterId;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/mobileprint/kitkat/plugin/utils/KitKatUtils;->getNameFromPrinterID(Landroid/print/PrinterId;)Ljava/lang/String;

    move-result-object v19

    .line 154
    invoke-virtual/range {p1 .. p1}, Landroid/printservice/PrintJob;->getInfo()Landroid/print/PrintJobInfo;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/print/PrintJobInfo;->getPrinterId()Landroid/print/PrinterId;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/mobileprint/kitkat/plugin/utils/KitKatUtils;->getIPFromPrinterID(Landroid/print/PrinterId;)Ljava/lang/String;

    move-result-object v20

    .line 155
    invoke-virtual {v7}, Landroid/printservice/PrintDocument;->getData()Landroid/os/ParcelFileDescriptor;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v21

    new-instance v22, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$1;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$1;-><init>(Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;Landroid/printservice/PrintJob;)V

    .line 152
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v12, v0, v1, v2, v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->createPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;Ljava/lang/String;Ljava/io/FileDescriptor;Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;)V

    .line 175
    return-void

    .line 85
    .end local v4    # "duplex":Ljava/lang/String;
    .end local v8    # "pageRange":[Landroid/print/PageRange;
    .end local v13    # "sharedPrefs":Landroid/content/SharedPreferences;
    .end local v15    # "useConfidential":Z
    .end local v16    # "useJobAcc":Z
    .end local v17    # "useSecureRelease":Z
    :cond_4
    sget-object v19, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->MONOCHROME:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    goto/16 :goto_0

    .line 93
    :cond_5
    sget v19, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_PHOTO:I

    goto/16 :goto_1

    .line 102
    .restart local v4    # "duplex":Ljava/lang/String;
    .restart local v13    # "sharedPrefs":Landroid/content/SharedPreferences;
    :cond_6
    sget-object v19, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_DUPLEX_LONG_EDGE:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 103
    sget-object v19, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_TWO_SIDE_LONG_EDGE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    goto/16 :goto_2

    .line 105
    :cond_7
    sget-object v19, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_TWO_SIDE_SHORT_EDGE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    goto/16 :goto_2

    .line 114
    .restart local v9    # "password":Ljava/lang/String;
    .restart local v10    # "permission":I
    .restart local v16    # "useJobAcc":Z
    .restart local v18    # "userName":Ljava/lang/String;
    :cond_8
    const/16 v19, 0x1

    goto/16 :goto_3

    .line 140
    .end local v9    # "password":Ljava/lang/String;
    .end local v10    # "permission":I
    .end local v18    # "userName":Ljava/lang/String;
    .restart local v5    # "index":I
    .restart local v8    # "pageRange":[Landroid/print/PageRange;
    .restart local v14    # "spr":[Lcom/sec/mobileprint/core/print/SamsungPageRange;
    .restart local v15    # "useConfidential":Z
    .restart local v17    # "useSecureRelease":Z
    :cond_9
    aget-object v11, v8, v19

    .line 141
    .local v11, "pr":Landroid/print/PageRange;
    const-string v21, "NNN"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Page range: start="

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Landroid/print/PageRange;->getStart()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", end="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v11}, Landroid/print/PageRange;->getEnd()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    new-instance v21, Lcom/sec/mobileprint/core/print/SamsungPageRange;

    invoke-virtual {v11}, Landroid/print/PageRange;->getStart()I

    move-result v22

    invoke-virtual {v11}, Landroid/print/PageRange;->getEnd()I

    move-result v23

    invoke-direct/range {v21 .. v23}, Lcom/sec/mobileprint/core/print/SamsungPageRange;-><init>(II)V

    aput-object v21, v14, v5

    .line 143
    add-int/lit8 v5, v5, 0x1

    .line 140
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_4
.end method

.method protected onRequestCancelPrintJob(Landroid/printservice/PrintJob;)V
    .locals 4
    .param p1, "printJob"    # Landroid/printservice/PrintJob;

    .prologue
    .line 181
    :try_start_0
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {p1}, Landroid/printservice/PrintJob;->getId()Landroid/print/PrintJobId;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 182
    .local v1, "samJobId":I
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->cancelPrint(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    .end local v1    # "samJobId":I
    :goto_0
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p1}, Landroid/printservice/PrintJob;->cancel()Z

    goto :goto_0
.end method
