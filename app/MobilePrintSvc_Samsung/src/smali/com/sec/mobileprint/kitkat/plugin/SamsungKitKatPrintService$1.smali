.class Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$1;
.super Ljava/lang/Object;
.source "SamsungKitKatPrintService.java"

# interfaces
.implements Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->onPrintJobQueued(Landroid/printservice/PrintJob;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

.field private final synthetic val$printJob:Landroid/printservice/PrintJob;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;Landroid/printservice/PrintJob;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

    iput-object p2, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$1;->val$printJob:Landroid/printservice/PrintJob;

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFaliure(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 170
    const-string v0, "SamsungKitKatPrntService"

    const-string v1, "Print Job creation faliled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$1;->val$printJob:Landroid/printservice/PrintJob;

    const-string v1, "Error copying docment"

    invoke-virtual {v0, v1}, Landroid/printservice/PrintJob;->fail(Ljava/lang/String;)Z

    .line 172
    return-void
.end method

.method public onSuccess(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V
    .locals 5
    .param p1, "job"    # Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    .prologue
    .line 160
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)I

    move-result v1

    .line 161
    .local v1, "samJobId":I
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

    const/4 v3, -0x1

    # invokes: Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->getPrintJobfromMap(I)Landroid/printservice/PrintJob;
    invoke-static {v2, v3}, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->access$0(Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;I)Landroid/printservice/PrintJob;

    move-result-object v0

    .line 162
    .local v0, "googleJob":Landroid/printservice/PrintJob;
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

    iget-object v2, v2, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v0}, Landroid/printservice/PrintJob;->getId()Landroid/print/PrintJobId;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

    iget-object v2, v2, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v0}, Landroid/printservice/PrintJob;->getId()Landroid/print/PrintJobId;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    const-string v2, "SamsungKitKatPrntService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Print Job Created: SamJobID = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  GoogleJob id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/printservice/PrintJob;->getId()Landroid/print/PrintJobId;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    return-void
.end method
