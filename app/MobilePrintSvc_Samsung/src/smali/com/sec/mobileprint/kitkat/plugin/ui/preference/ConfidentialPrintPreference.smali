.class public Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;
.super Landroid/preference/DialogPreference;
.source "ConfidentialPrintPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static sharedPref:Landroid/content/SharedPreferences;


# instance fields
.field mCancelBtn:Landroid/widget/Button;

.field mCheckBoxConfidentialPrint:Landroid/widget/CheckBox;

.field mCheckBoxShowPassword:Landroid/widget/CheckBox;

.field mContext:Landroid/content/Context;

.field mEditTextUserID:Landroid/widget/EditText;

.field mEditTextUserPW:Landroid/widget/EditText;

.field mOkBtn:Landroid/widget/Button;

.field mPassword:Ljava/lang/String;

.field mShow:Z

.field mUse:Z

.field mUserID:Ljava/lang/String;

.field private tmpUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    iput-boolean v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUse:Z

    .line 42
    iput-boolean v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mShow:Z

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUserID:Ljava/lang/String;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mPassword:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->tmpUserID:Ljava/lang/String;

    .line 51
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mContext:Landroid/content/Context;

    .line 52
    const v0, 0x7f030007

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setDialogLayoutResource(I)V

    .line 53
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->checkOkButtonEnable()V

    return-void
.end method

.method private checkOkButtonEnable()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 141
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxConfidentialPrint:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mOkBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 152
    :goto_0
    return-void

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private getConfidentialInputItem()V
    .locals 2

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->isUseConfidentialPrint(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUse:Z

    .line 205
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getConfidentialPrintUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUserID:Ljava/lang/String;

    .line 206
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getConfidentialPrintPassword(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mPassword:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUserID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxConfidentialPrint:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUse:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 211
    return-void
.end method

.method public static declared-synchronized getConfidentialPrintPassword(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 326
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_PASSWORD:Ljava/lang/String;

    sget-object v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_CONFIDENTIAL_DEFAULT:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getConfidentialPrintUserId(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 322
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_USER_NAME:Ljava/lang/String;

    sget-object v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_CONFIDENTIAL_DEFAULT:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 311
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->sharedPref:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 312
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->sharedPref:Landroid/content/SharedPreferences;

    .line 314
    :cond_0
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->sharedPref:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized isUseConfidentialPrint(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 318
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_USE:Ljava/lang/String;

    sget-boolean v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_CONFIDENTIAL_USE_DEFAULT:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setConfidentialInputItem(Z)V
    .locals 4
    .param p1, "use"    # Z

    .prologue
    const/4 v3, 0x0

    .line 215
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 216
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 217
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 219
    if-nez p1, :cond_1

    .line 220
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 221
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 222
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 239
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0
.end method

.method public static declared-synchronized setSupportedConfidentialPrint(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isUse"    # Z
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    .line 340
    const-class v3, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    monitor-enter v3

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 342
    .local v0, "p":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 344
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_USE:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 345
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_USER_NAME:Ljava/lang/String;

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 346
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_PASSWORD:Ljava/lang/String;

    invoke-interface {v1, v2, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 348
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    monitor-exit v3

    return-void

    .line 340
    .end local v0    # "p":Landroid/content/SharedPreferences;
    .end local v1    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized setUseConfidentialPrint(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isUse"    # Z

    .prologue
    .line 330
    const-class v3, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    monitor-enter v3

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 332
    .local v0, "p":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 334
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_USE:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 336
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    monitor-exit v3

    return-void

    .line 330
    .end local v0    # "p":Landroid/content/SharedPreferences;
    .end local v1    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method


# virtual methods
.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method public isShow()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mShow:Z

    return v0
.end method

.method public isUse()Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUse:Z

    return v0
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 59
    const v0, 0x7f0c001d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxConfidentialPrint:Landroid/widget/CheckBox;

    .line 60
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxConfidentialPrint:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 61
    const v0, 0x7f0c001f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    .line 62
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 63
    const v0, 0x7f0c0021

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    .line 64
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 65
    const v0, 0x7f0c0022

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    .line 66
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 67
    const v0, 0x7f0c0024

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mOkBtn:Landroid/widget/Button;

    .line 68
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    const v0, 0x7f0c0023

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCancelBtn:Landroid/widget/Button;

    .line 70
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCancelBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getConfidentialInputItem()V

    .line 75
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference$1;

    invoke-direct {v1, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference$1;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 108
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference$2;

    invoke-direct {v1, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference$2;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 126
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxConfidentialPrint:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->requestFocus()Z

    .line 127
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mShow:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 128
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->checkOkButtonEnable()V

    .line 129
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    const v1, 0x80010

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 133
    :cond_0
    iget-boolean v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUse:Z

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setConfidentialInputItem(Z)V

    .line 134
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 136
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 137
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "checked"    # Z

    .prologue
    .line 180
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxConfidentialPrint:Landroid/widget/CheckBox;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    invoke-direct {p0, p2}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setConfidentialInputItem(Z)V

    .line 183
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->checkOkButtonEnable()V

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxShowPassword:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 190
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 198
    :goto_0
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 199
    .local v0, "textLength":I
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 201
    .end local v0    # "textLength":I
    :cond_1
    return-void

    .line 193
    :cond_2
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    const v2, 0x80012

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mOkBtn:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 301
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->onDialogClosed(Z)V

    .line 302
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCancelBtn:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    goto :goto_0
.end method

.method protected onDialogClosed(Z)V
    .locals 4
    .param p1, "positiveResult"    # Z

    .prologue
    .line 164
    if-nez p1, :cond_0

    .line 175
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mCheckBoxConfidentialPrint:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUse:Z

    .line 168
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUserID:Ljava/lang/String;

    .line 169
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mPassword:Ljava/lang/String;

    .line 171
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUse:Z

    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUserID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mPassword:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setSupportedConfidentialPrint(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;)V

    .line 172
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->callChangeListener(Ljava/lang/Object;)Z

    .line 174
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 355
    :try_start_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    if-ne p1, v0, :cond_1

    if-eqz p2, :cond_1

    .line 356
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 357
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserPW:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 360
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 361
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 366
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 1
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v0, 0x0

    .line 157
    invoke-virtual {p1, v0, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 158
    invoke-virtual {p1, v0, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 159
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    .line 160
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPassword"    # Ljava/lang/String;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mPassword:Ljava/lang/String;

    .line 295
    return-void
.end method

.method public setShow(Z)V
    .locals 0
    .param p1, "mShow"    # Z

    .prologue
    .line 266
    iput-boolean p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mShow:Z

    .line 267
    return-void
.end method

.method public setUse(Z)V
    .locals 0
    .param p1, "mUse"    # Z

    .prologue
    .line 252
    iput-boolean p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUse:Z

    .line 253
    return-void
.end method

.method public setUserID(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUserID"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->mUserID:Ljava/lang/String;

    .line 281
    return-void
.end method
