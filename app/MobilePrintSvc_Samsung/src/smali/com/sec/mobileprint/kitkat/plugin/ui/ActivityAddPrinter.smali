.class public Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;
.super Landroid/app/Activity;
.source "ActivityAddPrinter.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddDeviceInterface;,
        Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;
    }
.end annotation


# static fields
.field public static final MENU_ADD_PRINTER:I = 0x1


# instance fields
.field private listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const/high16 v4, 0x7f030000

    invoke-virtual {p0, v4}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->setContentView(I)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 35
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 36
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 37
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 38
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 39
    new-instance v4, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    invoke-direct {v4, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;)V

    iput-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .line 40
    if-nez p1, :cond_1

    .line 41
    const-string v4, "NNN"

    const-string v5, "modifyPrinter"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 43
    .local v3, "ft":Landroid/app/FragmentTransaction;
    const v4, 0x7f0c0012

    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    invoke-static {v5}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->newInstance(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 44
    const/16 v4, 0x1001

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 45
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    .line 56
    .end local v3    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "dialogadd"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    .line 48
    .local v1, "dap":Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;
    if-eqz v1, :cond_2

    .line 49
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    invoke-virtual {v1, v4}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->setListener(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)V

    .line 51
    :cond_2
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "dialogmenu"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;

    .line 52
    .local v2, "dle":Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;
    if-eqz v2, :cond_0

    .line 53
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    invoke-virtual {v2, v4}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->setListener(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x1

    .line 60
    const/4 v0, 0x0

    const v1, 0x7f050023

    invoke-interface {p1, v0, v2, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 61
    const v1, 0x7f020004

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 67
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 82
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    return v3

    .line 69
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 70
    .local v0, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "dialogadd"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 71
    .local v2, "prev":Landroid/app/Fragment;
    if-eqz v2, :cond_0

    .line 72
    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 74
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 75
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    invoke-static {v3}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->newInstance(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    move-result-object v1

    .line 76
    .local v1, "newFragment":Landroid/app/DialogFragment;
    const-string v3, "dialogadd"

    invoke-virtual {v1, v0, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0

    .line 79
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    .end local v1    # "newFragment":Landroid/app/DialogFragment;
    .end local v2    # "prev":Landroid/app/Fragment;
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->finish()V

    goto :goto_0

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x102002c -> :sswitch_1
    .end sparse-switch
.end method

.method public showTextBox(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 114
    const v1, 0x7f0c0013

    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 115
    .local v0, "mTextView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 116
    if-nez p1, :cond_1

    .line 117
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
