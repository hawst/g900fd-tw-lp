.class public interface abstract Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddDeviceInterface;
.super Ljava/lang/Object;
.source "ActivityAddPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DialogAddDeviceInterface"
.end annotation


# virtual methods
.method public abstract addDeviceList(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V
.end method

.method public abstract modifyPrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V
.end method

.method public abstract removePrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V
.end method
