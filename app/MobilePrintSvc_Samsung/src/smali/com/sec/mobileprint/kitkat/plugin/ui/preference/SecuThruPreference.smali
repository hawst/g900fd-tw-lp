.class public Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;
.super Landroid/preference/DialogPreference;
.source "SecuThruPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static sharedPref:Landroid/content/SharedPreferences;


# instance fields
.field mCancelBtn:Landroid/widget/Button;

.field mCheckBoxSecureReleasePrint:Landroid/widget/CheckBox;

.field mContext:Landroid/content/Context;

.field mEditTextUserID:Landroid/widget/EditText;

.field mOkBtn:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mContext:Landroid/content/Context;

    .line 35
    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setDialogLayoutResource(I)V

    .line 36
    return-void
.end method

.method public static declared-synchronized getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->sharedPref:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 109
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->sharedPref:Landroid/content/SharedPreferences;

    .line 111
    :cond_0
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->sharedPref:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getSecureReleaseUserId(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_SECURE_USER_NAME:Ljava/lang/String;

    sget-object v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_SECURE_DEFAULT:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized isUseSecureRelease(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    const-class v1, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_SECURE_USE:Ljava/lang/String;

    sget-boolean v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_SECURE_USE_DEFAULT:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private loadSecureReleaseValues()V
    .locals 3

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->isUseSecureRelease(Landroid/content/Context;)Z

    move-result v0

    .line 67
    .local v0, "isUse":Z
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getSecureReleaseUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "userId":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mCheckBoxSecureReleasePrint:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 70
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 73
    return-void
.end method

.method public static declared-synchronized setSecureRelease(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isSecureRelease"    # Z
    .param p2, "userId"    # Ljava/lang/String;

    .prologue
    .line 123
    const-class v3, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    monitor-enter v3

    :try_start_0
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 125
    .local v0, "p":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 127
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_SECURE_USE:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 128
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_SECURE_USER_NAME:Ljava/lang/String;

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 130
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    monitor-exit v3

    return-void

    .line 123
    .end local v0    # "p":Landroid/content/SharedPreferences;
    .end local v1    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 42
    const v0, 0x7f0c001d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mCheckBoxSecureReleasePrint:Landroid/widget/CheckBox;

    .line 43
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mCheckBoxSecureReleasePrint:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 44
    const v0, 0x7f0c001f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    .line 45
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 46
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 47
    const v0, 0x7f0c0024

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mOkBtn:Landroid/widget/Button;

    .line 48
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    const v0, 0x7f0c0023

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mCancelBtn:Landroid/widget/Button;

    .line 50
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mCancelBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->loadSecureReleaseValues()V

    .line 54
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 55
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "checkbox"    # Landroid/widget/CompoundButton;
    .param p2, "checked"    # Z

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 79
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mOkBtn:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 95
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->onDialogClosed(Z)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mCancelBtn:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    goto :goto_0
.end method

.method protected onDialogClosed(Z)V
    .locals 3
    .param p1, "positiveResult"    # Z

    .prologue
    .line 83
    if-nez p1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mCheckBoxSecureReleasePrint:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setSecureRelease(Landroid/content/Context;ZLjava/lang/String;)V

    .line 87
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->callChangeListener(Ljava/lang/Object;)Z

    .line 88
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 137
    :try_start_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 139
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->mEditTextUserID:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 142
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 1
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-virtual {p1, v0, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 61
    invoke-virtual {p1, v0, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 62
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    .line 63
    return-void
.end method
