.class public Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;
.super Ljava/lang/Object;
.source "ActivityAddPrinter.java"

# interfaces
.implements Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddDeviceInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DialogAddPrinterListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;


# direct methods
.method public constructor <init>(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addDeviceList(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V
    .locals 1
    .param p1, "manualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .prologue
    .line 95
    invoke-static {}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->getInstance()Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->addPrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V

    .line 96
    return-void
.end method

.method public modifyPrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V
    .locals 2
    .param p1, "beforeManualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .param p2, "afterManualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .prologue
    .line 108
    const-string v0, "NNN"

    const-string v1, "modifyPrinter"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-static {}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->getInstance()Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->modifyPrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V

    .line 110
    return-void
.end method

.method public removePrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V
    .locals 1
    .param p1, "manualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .prologue
    .line 101
    invoke-static {}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->getInstance()Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->removePrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V

    .line 102
    return-void
.end method
