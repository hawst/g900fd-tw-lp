.class public Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;
.super Landroid/widget/ArrayAdapter;
.source "AdapterAddPrinterList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p3, "mDeviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 26
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->context:Landroid/content/Context;

    .line 27
    iput-object p3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->items:Ljava/util/ArrayList;

    .line 28
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->items:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->items:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 41
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->getItem(I)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 52
    move-object v4, p2

    .line 54
    .local v4, "v":Landroid/view/View;
    if-nez v4, :cond_0

    .line 55
    iget-object v6, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->context:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 56
    .local v5, "vi":Landroid/view/LayoutInflater;
    const v6, 0x7f030002

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 58
    .end local v5    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v6, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->items:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 59
    .local v0, "device":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    const-string v6, "NNN"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "device: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    if-eqz v0, :cond_2

    .line 62
    const v6, 0x7f0c0015

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 63
    .local v2, "deviceName":Landroid/widget/TextView;
    const v6, 0x7f0c0016

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 64
    .local v1, "deviceIP":Landroid/widget/TextView;
    const v6, 0x7f0c0014

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 66
    .local v3, "icon":Landroid/widget/ImageView;
    if-eqz v2, :cond_1

    .line 67
    invoke-virtual {v0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    :cond_1
    if-eqz v1, :cond_2

    .line 70
    invoke-virtual {v0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    .end local v1    # "deviceIP":Landroid/widget/TextView;
    .end local v2    # "deviceName":Landroid/widget/TextView;
    .end local v3    # "icon":Landroid/widget/ImageView;
    :cond_2
    return-object v4
.end method

.method public setItem(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->items:Ljava/util/ArrayList;

    .line 32
    return-void
.end method
