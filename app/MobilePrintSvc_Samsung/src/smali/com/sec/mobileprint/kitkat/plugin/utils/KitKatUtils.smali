.class public Lcom/sec/mobileprint/kitkat/plugin/utils/KitKatUtils;
.super Ljava/lang/Object;
.source "KitKatUtils.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getIPFromPrinterID(Landroid/print/PrinterId;)Ljava/lang/String;
    .locals 3
    .param p0, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 29
    invoke-virtual {p0}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v1

    .line 30
    .local v1, "str":Ljava/lang/String;
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->IP_ADDRESS_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 31
    .local v0, "index":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 32
    const/4 v2, 0x0

    .line 38
    :goto_0
    return-object v2

    .line 34
    :cond_0
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->IP_ADDRESS_PREFIX:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 35
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PRINTERID_VALUES_DELIMITER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 36
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 38
    goto :goto_0
.end method

.method public static getNameFromPrinterID(Landroid/print/PrinterId;)Ljava/lang/String;
    .locals 3
    .param p0, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 15
    invoke-virtual {p0}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v1

    .line 16
    .local v1, "str":Ljava/lang/String;
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->MODEL_NAME_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 17
    .local v0, "index":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 18
    const/4 v2, 0x0

    .line 24
    :goto_0
    return-object v2

    .line 20
    :cond_0
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->MODEL_NAME_PREFIX:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 21
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PRINTERID_VALUES_DELIMITER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 22
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 24
    goto :goto_0
.end method

.method public static getTypeFromPrinterID(Landroid/print/PrinterId;)Ljava/lang/String;
    .locals 3
    .param p0, "printerId"    # Landroid/print/PrinterId;

    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "str":Ljava/lang/String;
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->TYPE_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 44
    .local v0, "index":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 45
    const/4 v2, 0x0

    .line 51
    :goto_0
    return-object v2

    .line 47
    :cond_0
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->TYPE_PREFIX:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 48
    sget-object v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PRINTERID_VALUES_DELIMITER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 49
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 51
    goto :goto_0
.end method

.method public static makeStringforPrinterID(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p0, "printerName"    # Ljava/lang/String;
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 56
    const/4 v2, 0x0

    .line 57
    .local v2, "printerid":Ljava/lang/String;
    const/4 v1, 0x0

    .line 59
    .local v1, "name":Ljava/lang/String;
    const-string v3, "("

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 60
    .local v0, "index":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 61
    const/4 v3, 0x0

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 65
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->MODEL_NAME_PREFIX:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PRINTERID_VALUES_DELIMITER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 66
    sget-object v4, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->IP_ADDRESS_PREFIX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PRINTERID_VALUES_DELIMITER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 67
    sget-object v4, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->TYPE_PREFIX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 68
    const/4 v3, 0x1

    if-ne p2, v3, :cond_1

    sget-object v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->TYPE_VAL_AUTO_DISCOVERED:Ljava/lang/String;

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 69
    sget-object v4, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PRINTERID_VALUES_DELIMITER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 65
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 71
    return-object v2

    .line 63
    :cond_0
    move-object v1, p0

    goto :goto_0

    .line 69
    :cond_1
    sget-object v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->TYPE_VAL_MANUALLY_ADDED:Ljava/lang/String;

    goto :goto_1
.end method
