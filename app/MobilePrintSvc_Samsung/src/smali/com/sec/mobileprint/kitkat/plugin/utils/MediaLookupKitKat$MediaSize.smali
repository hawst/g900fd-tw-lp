.class public Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;
.super Ljava/lang/Object;
.source "MediaLookupKitKat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaSize"
.end annotation


# instance fields
.field private bottoMargin:I

.field private height:I

.field private leftMargin:I

.field private mediaName:Ljava/lang/String;

.field private rightMargin:I

.field private topMargin:I

.field private width:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 0
    .param p1, "mediaName"    # Ljava/lang/String;
    .param p2, "leftMargin"    # I
    .param p3, "topMargin"    # I
    .param p4, "rightMargin"    # I
    .param p5, "bottoMargin"    # I
    .param p6, "length"    # I
    .param p7, "width"    # I

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->mediaName:Ljava/lang/String;

    .line 98
    iput p7, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->width:I

    .line 99
    iput p6, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->height:I

    .line 100
    iput p3, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->topMargin:I

    .line 101
    iput p5, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->bottoMargin:I

    .line 102
    iput p2, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->leftMargin:I

    .line 103
    iput p4, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->rightMargin:I

    .line 104
    return-void
.end method


# virtual methods
.method public getBottoMargin()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->bottoMargin:I

    return v0
.end method

.method public getLeftMargin()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->leftMargin:I

    return v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->height:I

    return v0
.end method

.method public getMediaName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->mediaName:Ljava/lang/String;

    return-object v0
.end method

.method public getRightMargin()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->rightMargin:I

    return v0
.end method

.method public getTopMargin()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->topMargin:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->width:I

    return v0
.end method

.method public setBottoMargin(I)V
    .locals 0
    .param p1, "bottoMargin"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->bottoMargin:I

    .line 143
    return-void
.end method

.method public setLeftMargin(I)V
    .locals 0
    .param p1, "leftMargin"    # I

    .prologue
    .line 150
    iput p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->leftMargin:I

    .line 151
    return-void
.end method

.method public setLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->height:I

    .line 127
    return-void
.end method

.method public setMediaName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mediaName"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->mediaName:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setRightMargin(I)V
    .locals 0
    .param p1, "rightMargin"    # I

    .prologue
    .line 158
    iput p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->rightMargin:I

    .line 159
    return-void
.end method

.method public setTopMargin(I)V
    .locals 0
    .param p1, "topMargin"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->topMargin:I

    .line 135
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat$MediaSize;->width:I

    .line 119
    return-void
.end method
