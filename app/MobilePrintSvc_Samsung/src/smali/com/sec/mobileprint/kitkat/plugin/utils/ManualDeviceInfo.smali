.class public Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
.super Ljava/lang/Object;
.source "ManualDeviceInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private mDeviceFreindlyName:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;

.field private mIPAdd:Ljava/lang/String;

.field private mPort:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setIPAdd(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 18
    const-string v0, "9100"

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setPort(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 19
    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setDeviceFreindlyName(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 20
    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setDeviceName(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)Z
    .locals 2
    .param p1, "info"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mIPAdd:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mDeviceFreindlyName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mDeviceName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mPort:Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mPort:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const/4 v0, 0x1

    .line 67
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDeviceFreindlyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mDeviceFreindlyName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getIPAdd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mIPAdd:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mPort:Ljava/lang/String;

    return-object v0
.end method

.method public setDeviceFreindlyName(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .locals 0
    .param p1, "mDeviceFreindlyName"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mDeviceFreindlyName:Ljava/lang/String;

    .line 47
    return-object p0
.end method

.method public setDeviceName(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .locals 0
    .param p1, "mDeviceName"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public setIPAdd(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .locals 0
    .param p1, "mIPAdd"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mIPAdd:Ljava/lang/String;

    .line 29
    return-object p0
.end method

.method public setPort(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .locals 0
    .param p1, "mPort"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->mPort:Ljava/lang/String;

    .line 38
    return-object p0
.end method
