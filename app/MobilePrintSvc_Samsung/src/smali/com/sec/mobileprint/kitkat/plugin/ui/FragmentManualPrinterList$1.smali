.class Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList$1;
.super Ljava/lang/Object;
.source "FragmentManualPrinterList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->initContentsView(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemLongClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 121
    .local v0, "ft":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialogmenu"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 122
    .local v1, "prev":Landroid/app/Fragment;
    if-eqz v1, :cond_0

    .line 123
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 125
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 126
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    # getter for: Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->access$0(Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    sget-object v4, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    invoke-static {v2, v4}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->newInstance(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;

    move-result-object v2

    iput-object v2, v3, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->newFragment:Landroid/app/DialogFragment;

    .line 127
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList$1;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    iget-object v2, v2, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->newFragment:Landroid/app/DialogFragment;

    const-string v3, "dialogmenu"

    invoke-virtual {v2, v0, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 129
    const/4 v2, 0x0

    return v2
.end method
