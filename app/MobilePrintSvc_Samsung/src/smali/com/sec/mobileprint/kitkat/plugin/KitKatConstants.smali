.class public Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;
.super Ljava/lang/Object;
.source "KitKatConstants.java"


# static fields
.field public static DEBUG:Z = false

.field public static IP_ADDRESS_PREFIX:Ljava/lang/String; = null

.field public static MODEL_NAME_PREFIX:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_CONFIDENTIAL_PASSWORD:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_CONFIDENTIAL_USE:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_CONFIDENTIAL_USER_NAME:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_DUPLEX:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_JOB_ACCOUNTING_GROUP_PERMISSION:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_JOB_ACCOUNTING_PASSWORD:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_JOB_ACCOUNTING_USE:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_JOB_ACCOUNTING_USER_NAME:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_SECURE_USE:Ljava/lang/String; = null

.field public static PREFERENCE_KEY_SECURE_USER_NAME:Ljava/lang/String; = null

.field public static PREFERENCE_VALUE_CONFIDENTIAL_DEFAULT:Ljava/lang/String; = null

.field public static PREFERENCE_VALUE_CONFIDENTIAL_USE_DEFAULT:Z = false

.field public static PREFERENCE_VALUE_DUPLEX_DEFAULT:Ljava/lang/String; = null

.field public static PREFERENCE_VALUE_DUPLEX_LONG_EDGE:Ljava/lang/String; = null

.field public static PREFERENCE_VALUE_DUPLEX_SIMPLEX:Ljava/lang/String; = null

.field public static PREFERENCE_VALUE_DUPLEX__SHORT_EDGE:Ljava/lang/String; = null

.field public static PREFERENCE_VALUE_JOB_ACCOUNTING_DEFAULT:Ljava/lang/String; = null

.field public static PREFERENCE_VALUE_JOB_ACCOUNTING_PERMISSION_DEFAULT:I = 0x0

.field public static PREFERENCE_VALUE_JOB_ACCOUNTING_USE_DEFAULT:Z = false

.field public static PREFERENCE_VALUE_SECURE_DEFAULT:Ljava/lang/String; = null

.field public static PREFERENCE_VALUE_SECURE_USE_DEFAULT:Z = false

.field public static PRINTERID_VALUES_DELIMITER:Ljava/lang/String; = null

.field public static final TYPE_AUTO_DISCOVERED:I = 0x1

.field public static final TYPE_MANUALLY_ADDED:I = 0x2

.field public static TYPE_PREFIX:Ljava/lang/String;

.field public static TYPE_VAL_AUTO_DISCOVERED:Ljava/lang/String;

.field public static TYPE_VAL_MANUALLY_ADDED:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    .line 8
    const-string v0, "DUPLEX"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_DUPLEX:Ljava/lang/String;

    .line 10
    const-string v0, "SIMPLEX"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_DUPLEX_SIMPLEX:Ljava/lang/String;

    .line 11
    const-string v0, "LONG_EDGE"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_DUPLEX_LONG_EDGE:Ljava/lang/String;

    .line 12
    const-string v0, "SHORT_EDGE"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_DUPLEX__SHORT_EDGE:Ljava/lang/String;

    .line 13
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_DUPLEX_SIMPLEX:Ljava/lang/String;

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_DUPLEX_DEFAULT:Ljava/lang/String;

    .line 15
    const-string v0, "JOB_ACCOUNTING_USE"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_USE:Ljava/lang/String;

    .line 16
    const-string v0, "JOB_ACC_USER_NAME"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_USER_NAME:Ljava/lang/String;

    .line 17
    const-string v0, "JOB_ACC_PASSWORD"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_PASSWORD:Ljava/lang/String;

    .line 18
    const-string v0, "JOB_ACC_PERMISSION"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_JOB_ACCOUNTING_GROUP_PERMISSION:Ljava/lang/String;

    .line 20
    const-string v0, ""

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_DEFAULT:Ljava/lang/String;

    .line 21
    sput-boolean v1, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_USE_DEFAULT:Z

    .line 22
    sput v1, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_JOB_ACCOUNTING_PERMISSION_DEFAULT:I

    .line 24
    const-string v0, "CONF_USE"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_USE:Ljava/lang/String;

    .line 25
    const-string v0, "CONF_USER_NAME"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_USER_NAME:Ljava/lang/String;

    .line 26
    const-string v0, "CONF_PASSWORD"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_CONFIDENTIAL_PASSWORD:Ljava/lang/String;

    .line 28
    const-string v0, ""

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_CONFIDENTIAL_DEFAULT:Ljava/lang/String;

    .line 29
    sput-boolean v1, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_CONFIDENTIAL_USE_DEFAULT:Z

    .line 31
    const-string v0, "SEC_USE"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_SECURE_USE:Ljava/lang/String;

    .line 32
    const-string v0, "SEC_USER_NAME"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_SECURE_USER_NAME:Ljava/lang/String;

    .line 34
    const-string v0, ""

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_SECURE_DEFAULT:Ljava/lang/String;

    .line 35
    sput-boolean v1, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_SECURE_USE_DEFAULT:Z

    .line 37
    const-string v0, "MDL:"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->MODEL_NAME_PREFIX:Ljava/lang/String;

    .line 38
    const-string v0, "IP:"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->IP_ADDRESS_PREFIX:Ljava/lang/String;

    .line 39
    const-string v0, "TYPE:"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->TYPE_PREFIX:Ljava/lang/String;

    .line 40
    const-string v0, ";"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PRINTERID_VALUES_DELIMITER:Ljava/lang/String;

    .line 42
    const-string v0, "Auto"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->TYPE_VAL_AUTO_DISCOVERED:Ljava/lang/String;

    .line 43
    const-string v0, "Manual"

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->TYPE_VAL_MANUALLY_ADDED:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
