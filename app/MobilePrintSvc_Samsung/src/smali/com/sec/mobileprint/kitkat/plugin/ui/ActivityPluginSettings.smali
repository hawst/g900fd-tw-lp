.class public Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;
.super Landroid/preference/PreferenceActivity;
.source "ActivityPluginSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

.field private DuplexPref:Landroid/preference/ListPreference;

.field private JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

.field private secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private createPreferenceHierarchy()Landroid/preference/PreferenceScreen;
    .locals 17

    .prologue
    .line 100
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v9

    .line 102
    .local v9, "root":Landroid/preference/PreferenceScreen;
    new-instance v8, Landroid/preference/PreferenceCategory;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 103
    .local v8, "printSettingsCat":Landroid/preference/PreferenceCategory;
    const v13, 0x7f05002d

    invoke-virtual {v8, v13}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 104
    invoke-virtual {v9, v8}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 106
    new-instance v13, Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    .line 107
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    const/high16 v14, 0x7f080000

    invoke-virtual {v13, v14}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 108
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    const v14, 0x7f080001

    invoke-virtual {v13, v14}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 109
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    sget-object v14, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_DUPLEX:Ljava/lang/String;

    invoke-virtual {v13, v14}, Landroid/preference/ListPreference;->setKey(Ljava/lang/String;)V

    .line 110
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    const v14, 0x7f05001b

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 111
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    const v14, 0x7f05001b

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 113
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 115
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 116
    .local v10, "sharedPref":Landroid/content/SharedPreferences;
    sget-object v13, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_KEY_DUPLEX:Ljava/lang/String;

    sget-object v14, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->PREFERENCE_VALUE_DUPLEX_DEFAULT:Ljava/lang/String;

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 117
    .local v11, "value":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const/high16 v14, 0x7f080000

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "arr":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    invoke-virtual {v13, v11}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v4

    .line 119
    .local v4, "index":I
    aget-object v6, v2, v4

    .line 120
    .local v6, "msg":Ljava/lang/String;
    if-lez v4, :cond_0

    .line 123
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "\n("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050010

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 125
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    invoke-virtual {v13, v6}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 126
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    invoke-virtual {v13, v4}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 129
    new-instance v13, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    .line 130
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    const v14, 0x7f05002e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 131
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    const v14, 0x7f05002e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setKey(Ljava/lang/String;)V

    .line 132
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    const v14, 0x7f05002e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 134
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 136
    invoke-static/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->isUseSecureRelease(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 137
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    const v14, 0x7f050029

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 142
    :goto_0
    new-instance v13, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    .line 143
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    const v14, 0x7f05000f

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 144
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    const v14, 0x7f05000f

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setKey(Ljava/lang/String;)V

    .line 145
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    const v14, 0x7f05000f

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 147
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 149
    invoke-static/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->isUseConfidentialPrint(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->isUseSecureRelease(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 150
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setUse(Z)V

    .line 151
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    const v14, 0x7f050029

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 157
    :goto_1
    new-instance v13, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    .line 158
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    const v14, 0x7f050027

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    const v14, 0x7f050027

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setKey(Ljava/lang/String;)V

    .line 160
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    const v14, 0x7f050027

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 162
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 164
    invoke-static/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->isJobAccountingUse(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-static/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->isUseSecureRelease(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 165
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    const v14, 0x7f050029

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 169
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->setAdvancePreferenceEnable()V

    .line 171
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    invoke-virtual {v8, v13}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 172
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    invoke-virtual {v8, v13}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 173
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    invoke-virtual {v8, v13}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 174
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    invoke-virtual {v8, v13}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 176
    new-instance v1, Landroid/preference/PreferenceCategory;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 177
    .local v1, "applicationSettingCat":Landroid/preference/PreferenceCategory;
    const v13, 0x7f050016

    invoke-virtual {v1, v13}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 178
    invoke-virtual {v9, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 180
    new-instance v12, Landroid/preference/Preference;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 181
    .local v12, "versionPref":Landroid/preference/Preference;
    const v13, 0x7f050033

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 183
    :try_start_0
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getPackageName()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v14

    iget-object v14, v14, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, " \n("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f05001d

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :goto_3
    new-instance v5, Landroid/preference/Preference;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 190
    .local v5, "legalPref":Landroid/preference/Preference;
    const v13, 0x7f050017

    invoke-virtual {v5, v13}, Landroid/preference/Preference;->setTitle(I)V

    .line 191
    new-instance v13, Landroid/content/Intent;

    const-class v14, Lcom/sec/mobileprint/kitkat/plugin/ui/About;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v13}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 193
    new-instance v7, Landroid/preference/Preference;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 194
    .local v7, "openSourcePref":Landroid/preference/Preference;
    const v13, 0x7f050018

    invoke-virtual {v7, v13}, Landroid/preference/Preference;->setTitle(I)V

    .line 195
    new-instance v13, Landroid/content/Intent;

    const-class v14, Lcom/sec/mobileprint/kitkat/plugin/ui/OpenSource;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v13}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 197
    invoke-virtual {v1, v12}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 198
    invoke-virtual {v1, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 205
    return-object v9

    .line 139
    .end local v1    # "applicationSettingCat":Landroid/preference/PreferenceCategory;
    .end local v5    # "legalPref":Landroid/preference/Preference;
    .end local v7    # "openSourcePref":Landroid/preference/Preference;
    .end local v12    # "versionPref":Landroid/preference/Preference;
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    invoke-static/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getSecureReleaseUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 153
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setUse(Z)V

    .line 154
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    invoke-static/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getConfidentialPrintUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 167
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    invoke-static/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getJobAccountingUserID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 184
    .restart local v1    # "applicationSettingCat":Landroid/preference/PreferenceCategory;
    .restart local v12    # "versionPref":Landroid/preference/Preference;
    :catch_0
    move-exception v3

    .line 186
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_3
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->createPreferenceHierarchy()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 37
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 38
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 39
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 40
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 41
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 44
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 231
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 235
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 233
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->finish()V

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x0

    const v6, 0x7f050029

    .line 50
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    if-ne p1, v5, :cond_2

    .line 51
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "value":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v6, 0x7f080000

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "arr":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    invoke-virtual {v5, v4}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 54
    .local v1, "index":I
    aget-object v2, v0, v1

    .line 55
    .local v2, "msg":Ljava/lang/String;
    if-lez v1, :cond_0

    .line 58
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "\n("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050010

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 60
    :cond_0
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    invoke-virtual {v5, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->DuplexPref:Landroid/preference/ListPreference;

    invoke-virtual {v5, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 93
    .end local v0    # "arr":[Ljava/lang/String;
    .end local v1    # "index":I
    .end local v2    # "msg":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->setAdvancePreferenceEnable()V

    .line 94
    return v8

    .line 68
    :cond_2
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    if-ne p1, v5, :cond_5

    .line 69
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->isUseConfidentialPrint(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->isUseSecureRelease(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 70
    :cond_3
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    invoke-virtual {v5, v8}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setUse(Z)V

    .line 71
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    invoke-virtual {p0, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 73
    :cond_4
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setUse(Z)V

    .line 74
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->getConfidentialPrintUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 78
    :cond_5
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    if-ne p1, v5, :cond_8

    .line 79
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getJobAccountingUserID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 80
    .local v3, "userid":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->isJobAccountingUse(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->isUseSecureRelease(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 81
    :cond_6
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    invoke-virtual {p0, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 83
    :cond_7
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->getJobAccountingUserID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 86
    .end local v3    # "userid":Ljava/lang/String;
    :cond_8
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    if-ne p1, v5, :cond_1

    .line 87
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->isUseSecureRelease(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 88
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    invoke-virtual {p0, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 90
    :cond_9
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->secuThruPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;

    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->getSecureReleaseUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setAdvancePreferenceEnable()V
    .locals 4

    .prologue
    const v3, 0x7f050029

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 209
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/SecuThruPreference;->isUseSecureRelease(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setEnabled(Z)V

    .line 211
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setUse(Z)V

    .line 212
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setEnabled(Z)V

    .line 213
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setUse(Z)V

    .line 214
    invoke-static {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setUseConfidentialPrint(Landroid/content/Context;Z)V

    .line 215
    invoke-static {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setUseJobAccounting(Landroid/content/Context;Z)V

    .line 216
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    invoke-virtual {p0, v3}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    invoke-virtual {p0, v3}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 227
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->ConfidentialPrintPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;

    invoke-virtual {v0, v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/ConfidentialPrintPreference;->setEnabled(Z)V

    .line 222
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityPluginSettings;->JobAccountingPref:Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;

    invoke-virtual {v0, v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/preference/JobAccountingPreference;->setEnabled(Z)V

    goto :goto_0
.end method
