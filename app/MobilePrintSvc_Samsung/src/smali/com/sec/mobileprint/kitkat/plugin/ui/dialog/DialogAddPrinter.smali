.class public Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;
.super Landroid/app/DialogFragment;
.source "DialogAddPrinter.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment",
        "NewApi"
    }
.end annotation


# static fields
.field public static final ADD_MODE:I = 0x1

.field public static final MODIFY_MODE:I = 0x2

.field private static _instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;


# instance fields
.field dialog:Landroid/app/AlertDialog;

.field listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

.field mContext:Landroid/content/Context;

.field manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

.field mode:I

.field pd:Landroid/app/ProgressDialog;

.field textIPAddress:Landroid/widget/EditText;

.field textPrinterName:Landroid/widget/EditText;

.field title:Ljava/lang/String;

.field v:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 61
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->mContext:Landroid/content/Context;

    .line 62
    sput-object p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    .line 63
    return-void
.end method

.method public static newInstance(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;
    .locals 2
    .param p0, "listener"    # Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .prologue
    .line 76
    new-instance v0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    invoke-direct {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;-><init>()V

    .line 77
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    iput-object p0, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .line 78
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->mode:I

    .line 79
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;
    .locals 3
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "manualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .param p2, "listener"    # Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .prologue
    .line 83
    new-instance v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    invoke-direct {v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;-><init>()V

    .line 84
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    iput-object p2, v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .line 85
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 86
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "ManualDeviceInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 87
    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    invoke-virtual {v1, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->setArguments(Landroid/os/Bundle;)V

    .line 89
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    iput-object p0, v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->title:Ljava/lang/String;

    .line 90
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    iput-object p1, v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 91
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    iput-object p2, v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .line 92
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    const/4 v2, 0x2

    iput v2, v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->mode:I

    .line 93
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    return-object v1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v3, -0x1

    .line 232
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "strIP":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "strPort":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 235
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 249
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 108
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 110
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 111
    const-string v1, "ManualDeviceInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    iput-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 112
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->title:Ljava/lang/String;

    .line 113
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    const/4 v2, 0x2

    iput v2, v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->mode:I

    .line 118
    :goto_0
    return-void

    .line 115
    :cond_0
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    const/4 v2, 0x1

    iput v2, v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->mode:I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 101
    const/4 v0, 0x0

    .local v0, "style":I
    const/4 v1, 0x0

    .line 102
    .local v1, "theme":I
    invoke-virtual {p0, v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->setStyle(II)V

    .line 103
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 141
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 144
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030003

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->v:Landroid/view/View;

    .line 146
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->v:Landroid/view/View;

    const v5, 0x7f0c0017

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    .line 147
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->v:Landroid/view/View;

    const v5, 0x7f0c0018

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    .line 148
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    invoke-virtual {v4, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 149
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    invoke-virtual {v4, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 150
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    invoke-virtual {v4, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 151
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    invoke-virtual {v4, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 152
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    invoke-virtual {v4, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 154
    const/4 v4, 0x2

    new-array v0, v4, [Landroid/text/InputFilter;

    .line 156
    .local v0, "FilterArray":[Landroid/text/InputFilter;
    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    const/16 v5, 0x40

    invoke-direct {v4, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v0, v7

    .line 158
    new-instance v2, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$1;

    invoke-direct {v2, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$1;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;)V

    .line 172
    .local v2, "filter":Landroid/text/InputFilter;
    const/4 v4, 0x1

    aput-object v2, v0, v4

    .line 174
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 176
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->title:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 177
    const v4, 0x7f050023

    invoke-virtual {p0, v4}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->title:Ljava/lang/String;

    .line 180
    :cond_0
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 181
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 182
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->v:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 183
    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 184
    const v5, 0x7f05002a

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 185
    const v5, 0x7f050024

    new-instance v6, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$2;

    invoke-direct {v6, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$2;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 191
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    .line 192
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    new-instance v5, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;

    invoke-direct {v5, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 211
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 212
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 213
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v4}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 214
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v5}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 215
    :cond_1
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v4}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 216
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v5}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 226
    :cond_2
    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    return-object v4
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 123
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 124
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 129
    invoke-super {p0}, Landroid/app/DialogFragment;->onDetach()V

    .line 130
    return-void
.end method

.method public onDeviceDiscoveredOrUpdated(Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;I)V
    .locals 5
    .param p1, "deviceInfo"    # Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;
    .param p2, "calledFrom"    # I

    .prologue
    const/4 v4, 0x1

    .line 261
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 262
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 264
    :cond_0
    const/4 v1, 0x0

    .line 265
    .local v1, "manualDeviceInfoNew":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 266
    new-instance v2, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-direct {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;-><init>()V

    .line 267
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setDeviceFreindlyName(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    move-result-object v2

    .line 268
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setIPAdd(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    move-result-object v2

    .line 269
    invoke-virtual {p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setDeviceName(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    move-result-object v1

    .line 271
    sget-object v2, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->loadManualDeviceInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 272
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    invoke-static {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->isDuplicate(Ljava/util/ArrayList;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 273
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 275
    iget v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->mode:I

    if-ne v2, v4, :cond_1

    .line 276
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    invoke-virtual {v2, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;->addDeviceList(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V

    .line 293
    .end local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    :goto_0
    return-void

    .line 280
    .restart local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    :cond_1
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->manualDeviceInfoOld:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v2, v3, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;->modifyPrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V

    goto :goto_0

    .line 284
    :cond_2
    sget-object v2, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    const v3, 0x7f050021

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 287
    .end local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    :cond_3
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_4

    .line 288
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 290
    :cond_4
    sget-object v2, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    const v3, 0x7f05001f

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 135
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 136
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, -0x1

    .line 337
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    .line 343
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 311
    :try_start_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    if-ne p1, v0, :cond_1

    if-eqz p2, :cond_1

    .line 312
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 313
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textPrinterName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 317
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 321
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "saveState"    # Landroid/os/Bundle;

    .prologue
    .line 301
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 302
    const-string v1, "waiting"

    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 303
    return-void

    .line 302
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/DialogFragment;->onStop()V

    .line 69
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 72
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    .line 73
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 255
    return-void
.end method

.method public setListener(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .line 297
    return-void
.end method

.method public showProgressDialog()V
    .locals 2

    .prologue
    .line 327
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    .line 328
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    const v1, 0x7f050019

    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 330
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 331
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 332
    return-void
.end method
