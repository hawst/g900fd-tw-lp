.class public Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;
.super Landroid/printservice/PrinterDiscoverySession;
.source "SamsungDiscoverySession.java"

# interfaces
.implements Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SamsungDiscoverySession"


# instance fields
.field mDiscovery:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

.field mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/printservice/PrinterDiscoverySession;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mDiscovery:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    .line 28
    return-void
.end method

.method private removeAllPrinters()V
    .locals 5

    .prologue
    .line 469
    sget-boolean v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "SamsungDiscoverySession"

    const-string v4, "[removeAllPrinters]"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    :cond_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->getPrinters()Ljava/util/List;

    move-result-object v1

    .line 472
    .local v1, "addedPrinterInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 473
    .local v0, "addedPrinterIdList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterId;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 476
    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->removePrinters(Ljava/util/List;)V

    .line 477
    return-void

    .line 473
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/print/PrinterInfo;

    .line 474
    .local v2, "info":Landroid/print/PrinterInfo;
    invoke-virtual {v2}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onDestroy()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public declared-synchronized onDeviceDiscoveredOrUpdated(Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;I)V
    .locals 26
    .param p1, "deviceInfo"    # Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;
    .param p2, "calledAfter"    # I

    .prologue
    .line 238
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "SamsungDiscoverySession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onDeviceDiscoveredOrUpdated] calledAfter="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_0
    new-instance v2, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->permitAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v21

    .line 241
    .local v21, "policy":Landroid/os/StrictMode$ThreadPolicy;
    invoke-static/range {v21 .. v21}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    if-eqz p1, :cond_1

    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 245
    :cond_1
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_2

    const-string v2, "SamsungDiscoverySession"

    const-string v3, "[onDeviceDiscoveredOrUpdated] deviceInfo or ip address is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 466
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    .line 264
    :cond_3
    :try_start_2
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_4

    const-string v2, "SamsungDiscoverySession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onDeviceDiscoveredOrUpdated] device="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_4
    const/16 v22, 0x0

    .line 267
    .local v22, "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    const/4 v12, 0x0

    .line 269
    .local v12, "capabilityBuilder":Landroid/print/PrinterCapabilitiesInfo$Builder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->getPrinters()Ljava/util/List;

    move-result-object v23

    .line 270
    .local v23, "printerList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const/4 v10, 0x0

    .line 271
    .local v10, "bFound":Z
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_a

    .line 343
    :cond_6
    :goto_1
    if-nez v10, :cond_1f

    .line 345
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_7

    const-string v2, "SamsungDiscoverySession"

    const-string v3, "[onDeviceDiscoveredOrUpdated] add new device"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9

    .line 348
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_8

    const-string v2, "SamsungDiscoverySession"

    const-string v3, "[onDeviceDiscoveredOrUpdated] device name is null, try to read device name again"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    if-eqz v2, :cond_9

    .line 350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->getPrinterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 351
    .local v14, "deviceName":Ljava/lang/String;
    if-eqz v14, :cond_9

    .line 352
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceName(Ljava/lang/String;)V

    .line 353
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_9

    const-string v2, "SamsungDiscoverySession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onDeviceDiscoveredOrUpdated] device name="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    .end local v14    # "deviceName":Ljava/lang/String;
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_17

    .line 359
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_2

    const-string v2, "SamsungDiscoverySession"

    const-string v3, "[onDeviceDiscoveredOrUpdated] device name is still null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 463
    .end local v10    # "bFound":Z
    .end local v12    # "capabilityBuilder":Landroid/print/PrinterCapabilitiesInfo$Builder;
    .end local v22    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    .end local v23    # "printerList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    :catch_0
    move-exception v16

    .line 464
    .local v16, "e":Ljava/lang/Exception;
    :try_start_3
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 238
    .end local v16    # "e":Ljava/lang/Exception;
    .end local v21    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 271
    .restart local v10    # "bFound":Z
    .restart local v12    # "capabilityBuilder":Landroid/print/PrinterCapabilitiesInfo$Builder;
    .restart local v21    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    .restart local v22    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    .restart local v23    # "printerList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    :cond_a
    :try_start_4
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/print/PrinterInfo;

    .line 272
    .local v13, "device":Landroid/print/PrinterInfo;
    invoke-virtual {v13}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/mobileprint/kitkat/plugin/utils/KitKatUtils;->getIPFromPrinterID(Landroid/print/PrinterId;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 273
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_b

    const-string v2, "SamsungDiscoverySession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onDeviceDiscoveredOrUpdated] exists: id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v4

    invoke-virtual {v4}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_b
    const/4 v10, 0x1

    .line 276
    new-instance v22, Landroid/print/PrinterInfo$Builder;

    .end local v22    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    move-object/from16 v0, v22

    invoke-direct {v0, v13}, Landroid/print/PrinterInfo$Builder;-><init>(Landroid/print/PrinterInfo;)V

    .line 278
    .restart local v22    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    const/4 v2, 0x3

    move/from16 v0, p2

    if-ne v0, v2, :cond_f

    .line 280
    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceStatus()Ljava/lang/String;

    move-result-object v15

    .line 281
    .local v15, "deviceStatus":Ljava/lang/String;
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_c

    const-string v2, "SamsungDiscoverySession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onDeviceDiscoveredOrUpdated] deviceStatus="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_c
    if-nez v15, :cond_e

    .line 284
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_d

    const-string v2, "SamsungDiscoverySession"

    const-string v3, "[onDeviceDiscoveredOrUpdated] Status is unavailable"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_d
    const/4 v2, 0x3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/print/PrinterInfo$Builder;->setStatus(I)Landroid/print/PrinterInfo$Builder;

    goto/16 :goto_0

    .line 288
    :cond_e
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/print/PrinterInfo$Builder;->setStatus(I)Landroid/print/PrinterInfo$Builder;

    goto/16 :goto_1

    .line 292
    .end local v15    # "deviceStatus":Ljava/lang/String;
    :cond_f
    const/4 v2, 0x2

    move/from16 v0, p2

    if-ne v0, v2, :cond_6

    .line 294
    new-instance v12, Landroid/print/PrinterCapabilitiesInfo$Builder;

    .end local v12    # "capabilityBuilder":Landroid/print/PrinterCapabilitiesInfo$Builder;
    invoke-virtual/range {v22 .. v22}, Landroid/print/PrinterInfo$Builder;->build()Landroid/print/PrinterInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v2

    invoke-direct {v12, v2}, Landroid/print/PrinterCapabilitiesInfo$Builder;-><init>(Landroid/print/PrinterId;)V

    .line 295
    .restart local v12    # "capabilityBuilder":Landroid/print/PrinterCapabilitiesInfo$Builder;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getMediaSize()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_10

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getMediaSize()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 296
    :cond_10
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_11

    const-string v2, "SamsungDiscoverySession"

    const-string v3, "[onDeviceDiscoveredOrUpdated] media empty. use default media."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_11
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 301
    .local v20, "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "A4"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xdb3

    const/16 v9, 0x9b0

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "A5"

    const/16 v4, 0x39

    const/16 v5, 0x39

    const/16 v6, 0x39

    const/16 v7, 0x39

    const/16 v8, 0x9b0

    const/16 v9, 0x6d4

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "A6"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0x6d4

    const/16 v9, 0x4d8

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "JISB5"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xbdb

    const/16 v9, 0x865

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 305
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Letter"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xce4

    const/16 v9, 0x9f6

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Legal"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0x1068

    const/16 v9, 0x9f6

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Executive"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xc4e

    const/16 v9, 0x87f

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Folio"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xf3c

    const/16 v9, 0x9f6

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setMediaSize(Ljava/util/List;)V

    .line 313
    .end local v20    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getMediaSize()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_15

    .line 320
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_14

    const-string v2, "SamsungDiscoverySession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onDeviceDiscoveredOrUpdated] color= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getColor()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_14
    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getColor()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_16

    .line 323
    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v12, v2, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->setColorModes(II)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 328
    :goto_3
    new-instance v2, Landroid/print/PrintAttributes$Margins;

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/print/PrintAttributes$Margins;-><init>(IIII)V

    invoke-virtual {v12, v2}, Landroid/print/PrinterCapabilitiesInfo$Builder;->setMinMargins(Landroid/print/PrintAttributes$Margins;)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 329
    new-instance v2, Landroid/print/PrintAttributes$Resolution;

    const-string v3, "600x600"

    const-string v4, "600 dpi"

    const/16 v5, 0x258

    const/16 v6, 0x258

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/print/PrintAttributes$Resolution;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v3, 0x1

    invoke-virtual {v12, v2, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->addResolution(Landroid/print/PrintAttributes$Resolution;Z)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 331
    invoke-virtual {v12}, Landroid/print/PrinterCapabilitiesInfo$Builder;->build()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v11

    .line 332
    .local v11, "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/print/PrinterInfo$Builder;->setCapabilities(Landroid/print/PrinterCapabilitiesInfo;)Landroid/print/PrinterInfo$Builder;

    .line 334
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_6

    const-string v2, "SamsungDiscoverySession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onDeviceDiscoveredOrUpdated] set capability success, capabilities="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Landroid/print/PrinterCapabilitiesInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 313
    .end local v11    # "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    :cond_15
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    .line 314
    .local v18, "media":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    invoke-static/range {v18 .. v18}, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;->getGoogleMedia(Lcom/sec/print/mobileprint/dm/MediaSizeInfo;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object v19

    .line 315
    .local v19, "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    sget-object v3, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_PORTRAIT:Landroid/print/PrintAttributes$MediaSize;

    move-object/from16 v0, v19

    if-eq v0, v3, :cond_13

    .line 316
    invoke-static/range {v19 .. v19}, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;->isDefaultMedia(Landroid/print/PrintAttributes$MediaSize;)Z

    move-result v3

    move-object/from16 v0, v19

    invoke-virtual {v12, v0, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->addMediaSize(Landroid/print/PrintAttributes$MediaSize;Z)Landroid/print/PrinterCapabilitiesInfo$Builder;

    goto/16 :goto_2

    .line 325
    .end local v18    # "media":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    .end local v19    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    :cond_16
    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v12, v2, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->setColorModes(II)Landroid/print/PrinterCapabilitiesInfo$Builder;

    goto :goto_3

    .line 363
    .end local v13    # "device":Landroid/print/PrinterInfo;
    :cond_17
    new-instance v22, Landroid/print/PrinterInfo$Builder;

    .end local v22    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    sget-object v2, Lcom/sec/mobileprint/core/App;->service:Ljava/lang/Object;

    check-cast v2, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

    .line 364
    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/sec/mobileprint/kitkat/plugin/utils/KitKatUtils;->makeStringforPrinterID(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->generatePrinterId(Ljava/lang/String;)Landroid/print/PrinterId;

    move-result-object v2

    .line 365
    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    .line 366
    const/4 v4, 0x1

    .line 363
    move-object/from16 v0, v22

    invoke-direct {v0, v2, v3, v4}, Landroid/print/PrinterInfo$Builder;-><init>(Landroid/print/PrinterId;Ljava/lang/String;I)V

    .line 368
    .restart local v22    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    new-instance v12, Landroid/print/PrinterCapabilitiesInfo$Builder;

    .end local v12    # "capabilityBuilder":Landroid/print/PrinterCapabilitiesInfo$Builder;
    invoke-virtual/range {v22 .. v22}, Landroid/print/PrinterInfo$Builder;->build()Landroid/print/PrinterInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v2

    invoke-direct {v12, v2}, Landroid/print/PrinterCapabilitiesInfo$Builder;-><init>(Landroid/print/PrinterId;)V

    .line 371
    .restart local v12    # "capabilityBuilder":Landroid/print/PrinterCapabilitiesInfo$Builder;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    if-eqz v2, :cond_23

    .line 372
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->getPrinterStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 373
    .local v24, "printerStatus":Ljava/lang/String;
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_18

    const-string v2, "SamsungDiscoverySession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onDeviceDiscoveredOrUpdated] printerStatus = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_18
    if-nez v24, :cond_19

    .line 376
    const/4 v2, 0x3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/print/PrinterInfo$Builder;->setStatus(I)Landroid/print/PrinterInfo$Builder;

    goto/16 :goto_0

    .line 379
    :cond_19
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/print/PrinterInfo$Builder;->setStatus(I)Landroid/print/PrinterInfo$Builder;

    .line 383
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->getMediaSizes(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v20

    .line 384
    .restart local v20    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    if-eqz v20, :cond_1a

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1c

    .line 385
    :cond_1a
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_1b

    const-string v2, "SamsungDiscoverySession"

    const-string v3, "[onDeviceDiscoveredOrUpdated] mediaSizes is null or empty"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_1b
    new-instance v20, Ljava/util/ArrayList;

    .end local v20    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 390
    .restart local v20    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "A4"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xdb3

    const/16 v9, 0x9b0

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 391
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "A5"

    const/16 v4, 0x39

    const/16 v5, 0x39

    const/16 v6, 0x39

    const/16 v7, 0x39

    const/16 v8, 0x9b0

    const/16 v9, 0x6d4

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "A6"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0x6d4

    const/16 v9, 0x4d8

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "JISB5"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xbdb

    const/16 v9, 0x865

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Letter"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xce4

    const/16 v9, 0x9f6

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 395
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Legal"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0x1068

    const/16 v9, 0x9f6

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Executive"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xc4e

    const/16 v9, 0x87f

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Folio"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xf3c

    const/16 v9, 0x9f6

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    :cond_1c
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1d
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_21

    .line 407
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->isColorPrinter(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v17

    .line 408
    .local v17, "isColorPrinter":Z
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_1e

    const-string v2, "SamsungDiscoverySession"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onDeviceDiscoveredOrUpdated] isColorPrinter = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :cond_1e
    if-eqz v17, :cond_22

    .line 411
    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v12, v2, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->setColorModes(II)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 416
    :goto_5
    new-instance v2, Landroid/print/PrintAttributes$Margins;

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/print/PrintAttributes$Margins;-><init>(IIII)V

    invoke-virtual {v12, v2}, Landroid/print/PrinterCapabilitiesInfo$Builder;->setMinMargins(Landroid/print/PrintAttributes$Margins;)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 417
    new-instance v2, Landroid/print/PrintAttributes$Resolution;

    const-string v3, "600x600"

    const-string v4, "600 dpi"

    const/16 v5, 0x258

    const/16 v6, 0x258

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/print/PrintAttributes$Resolution;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v3, 0x1

    invoke-virtual {v12, v2, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->addResolution(Landroid/print/PrintAttributes$Resolution;Z)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 419
    invoke-virtual {v12}, Landroid/print/PrinterCapabilitiesInfo$Builder;->build()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v11

    .line 420
    .restart local v11    # "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/print/PrinterInfo$Builder;->setCapabilities(Landroid/print/PrinterCapabilitiesInfo;)Landroid/print/PrinterInfo$Builder;

    .line 455
    .end local v11    # "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    .end local v17    # "isColorPrinter":Z
    .end local v20    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    .end local v24    # "printerStatus":Ljava/lang/String;
    :cond_1f
    :goto_6
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 456
    .local v25, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    if-eqz v22, :cond_20

    .line 457
    invoke-virtual/range {v22 .. v22}, Landroid/print/PrinterInfo$Builder;->build()Landroid/print/PrinterInfo;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 458
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->addPrinters(Ljava/util/List;)V

    .line 461
    :cond_20
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_2

    const-string v2, "SamsungDiscoverySession"

    const-string v3, "[onDeviceDiscoveredOrUpdated] end"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 400
    .end local v25    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    .restart local v20    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    .restart local v24    # "printerStatus":Ljava/lang/String;
    :cond_21
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    .line 401
    .restart local v18    # "media":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    invoke-static/range {v18 .. v18}, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;->getGoogleMedia(Lcom/sec/print/mobileprint/dm/MediaSizeInfo;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object v19

    .line 402
    .restart local v19    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    sget-object v3, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_PORTRAIT:Landroid/print/PrintAttributes$MediaSize;

    move-object/from16 v0, v19

    if-eq v0, v3, :cond_1d

    .line 403
    invoke-static/range {v19 .. v19}, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;->isDefaultMedia(Landroid/print/PrintAttributes$MediaSize;)Z

    move-result v3

    move-object/from16 v0, v19

    invoke-virtual {v12, v0, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->addMediaSize(Landroid/print/PrintAttributes$MediaSize;Z)Landroid/print/PrinterCapabilitiesInfo$Builder;

    goto/16 :goto_4

    .line 413
    .end local v18    # "media":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    .end local v19    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    .restart local v17    # "isColorPrinter":Z
    :cond_22
    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v12, v2, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->setColorModes(II)Landroid/print/PrinterCapabilitiesInfo$Builder;

    goto :goto_5

    .line 424
    .end local v17    # "isColorPrinter":Z
    .end local v20    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    .end local v24    # "printerStatus":Ljava/lang/String;
    :cond_23
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 426
    .restart local v20    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "A4"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xdb3

    const/16 v9, 0x9b0

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "A5"

    const/16 v4, 0x39

    const/16 v5, 0x39

    const/16 v6, 0x39

    const/16 v7, 0x39

    const/16 v8, 0x9b0

    const/16 v9, 0x6d4

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 428
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "A6"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0x6d4

    const/16 v9, 0x4d8

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "JISB5"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xbdb

    const/16 v9, 0x865

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Letter"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xce4

    const/16 v9, 0x9f6

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 431
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Legal"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0x1068

    const/16 v9, 0x9f6

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Executive"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xc4e

    const/16 v9, 0x87f

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    new-instance v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v3, "Folio"

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xf3c

    const/16 v9, 0x9f6

    invoke-direct/range {v2 .. v9}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 435
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_24
    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_25

    .line 442
    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v12, v2, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->setColorModes(II)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 444
    new-instance v2, Landroid/print/PrintAttributes$Margins;

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x32

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/print/PrintAttributes$Margins;-><init>(IIII)V

    invoke-virtual {v12, v2}, Landroid/print/PrinterCapabilitiesInfo$Builder;->setMinMargins(Landroid/print/PrintAttributes$Margins;)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 445
    new-instance v2, Landroid/print/PrintAttributes$Resolution;

    const-string v3, "600x600"

    const-string v4, "600 dpi"

    const/16 v5, 0x258

    const/16 v6, 0x258

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/print/PrintAttributes$Resolution;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v3, 0x1

    invoke-virtual {v12, v2, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->addResolution(Landroid/print/PrintAttributes$Resolution;Z)Landroid/print/PrinterCapabilitiesInfo$Builder;

    .line 447
    invoke-virtual {v12}, Landroid/print/PrinterCapabilitiesInfo$Builder;->build()Landroid/print/PrinterCapabilitiesInfo;

    move-result-object v11

    .line 448
    .restart local v11    # "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/print/PrinterInfo$Builder;->setCapabilities(Landroid/print/PrinterCapabilitiesInfo;)Landroid/print/PrinterInfo$Builder;

    .line 451
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/print/PrinterInfo$Builder;->setStatus(I)Landroid/print/PrinterInfo$Builder;

    goto/16 :goto_6

    .line 435
    .end local v11    # "capabilities":Landroid/print/PrinterCapabilitiesInfo;
    :cond_25
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    .line 436
    .restart local v18    # "media":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    invoke-static/range {v18 .. v18}, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;->getGoogleMedia(Lcom/sec/print/mobileprint/dm/MediaSizeInfo;)Landroid/print/PrintAttributes$MediaSize;

    move-result-object v19

    .line 437
    .restart local v19    # "mediaSize":Landroid/print/PrintAttributes$MediaSize;
    sget-object v3, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_PORTRAIT:Landroid/print/PrintAttributes$MediaSize;

    move-object/from16 v0, v19

    if-eq v0, v3, :cond_24

    .line 438
    invoke-static/range {v19 .. v19}, Lcom/sec/mobileprint/kitkat/plugin/utils/MediaLookupKitKat;->isDefaultMedia(Landroid/print/PrintAttributes$MediaSize;)Z

    move-result v3

    move-object/from16 v0, v19

    invoke-virtual {v12, v0, v3}, Landroid/print/PrinterCapabilitiesInfo$Builder;->addMediaSize(Landroid/print/PrintAttributes$MediaSize;Z)Landroid/print/PrinterCapabilitiesInfo$Builder;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_7
.end method

.method public onStartPrinterDiscovery(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "printerList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterId;>;"
    :try_start_0
    sget-boolean v5, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v5, :cond_0

    const-string v5, "SamsungDiscoverySession"

    const-string v6, "[onStartPrinterDiscovery]"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :cond_0
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->removeAllPrinters()V

    .line 54
    sget-object v5, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->loadManualDeviceInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 55
    .local v0, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v4, "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    const/4 v3, 0x0

    .line 58
    .local v3, "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    if-eqz v0, :cond_1

    .line 59
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 66
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 67
    invoke-virtual {p0, v4}, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->addPrinters(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .end local v0    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    .end local v3    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    .end local v4    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    :cond_1
    :goto_1
    new-instance v5, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    sget-object v6, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-direct {v5, v6, p0}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;-><init>(Landroid/content/Context;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V

    iput-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mDiscovery:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    .line 76
    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mDiscovery:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->startDeviceManagerConnector()V

    .line 77
    return-void

    .line 59
    .restart local v0    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    .restart local v3    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    .restart local v4    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    :cond_2
    :try_start_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 60
    .local v2, "manualDevice":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    new-instance v3, Landroid/print/PrinterInfo$Builder;

    .end local v3    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    sget-object v5, Lcom/sec/mobileprint/core/App;->service:Ljava/lang/Object;

    check-cast v5, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

    .line 61
    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-static {v7, v8, v9}, Lcom/sec/mobileprint/kitkat/plugin/utils/KitKatUtils;->makeStringforPrinterID(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->generatePrinterId(Ljava/lang/String;)Landroid/print/PrinterId;

    move-result-object v5

    .line 62
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 63
    const/4 v8, 0x1

    .line 60
    invoke-direct {v3, v5, v7, v8}, Landroid/print/PrinterInfo$Builder;-><init>(Landroid/print/PrinterId;Ljava/lang/String;I)V

    .line 64
    .restart local v3    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    invoke-virtual {v3}, Landroid/print/PrinterInfo$Builder;->build()Landroid/print/PrinterInfo;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 70
    .end local v0    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    .end local v2    # "manualDevice":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .end local v3    # "printerInfoBuilder":Landroid/print/PrinterInfo$Builder;
    .end local v4    # "printers":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onStartPrinterStateTracking(Landroid/print/PrinterId;)V
    .locals 5
    .param p1, "printerId"    # Landroid/print/PrinterId;

    .prologue
    const/4 v4, 0x0

    .line 82
    sget-boolean v1, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "SamsungDiscoverySession"

    const-string v2, "[onStartPrinterStateTracking]"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;

    sget-object v1, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    .line 85
    invoke-static {p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/KitKatUtils;->getNameFromPrinterID(Landroid/print/PrinterId;)Ljava/lang/String;

    move-result-object v2

    .line 86
    invoke-static {p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/KitKatUtils;->getIPFromPrinterID(Landroid/print/PrinterId;)Ljava/lang/String;

    move-result-object v3

    .line 84
    invoke-direct {v0, v1, v2, v3, p0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V

    .line 88
    .local v0, "capability":Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;
    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->startDeviceManagerConnector()V

    .line 89
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    if-eqz v1, :cond_1

    .line 90
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual {v1, v4}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->stopMonitoring(Landroid/os/Message;)V

    .line 91
    iput-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 93
    :cond_1
    new-instance v1, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    sget-object v2, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/KitKatUtils;->getIPFromPrinterID(Landroid/print/PrinterId;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, p0}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V

    iput-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 94
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual {v1}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->startMonitoring()V

    .line 95
    return-void
.end method

.method public onStopPrinterDiscovery()V
    .locals 2

    .prologue
    .line 99
    sget-boolean v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "SamsungDiscoverySession"

    const-string v1, "[onStopPrinterDiscovery]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mDiscovery:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mDiscovery:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->stopDeviceDiscovery()V

    .line 105
    :cond_1
    return-void
.end method

.method public onStopPrinterStateTracking(Landroid/print/PrinterId;)V
    .locals 3
    .param p1, "arg0"    # Landroid/print/PrinterId;

    .prologue
    const/4 v2, 0x0

    .line 110
    sget-boolean v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "SamsungDiscoverySession"

    const-string v1, "[onStopPrinterStateTracking]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual {v0, v2}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->stopMonitoring(Landroid/os/Message;)V

    .line 114
    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungDiscoverySession;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 116
    :cond_1
    return-void
.end method

.method public onValidatePrinters(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/print/PrinterId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "arg0":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterId;>;"
    sget-boolean v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "SamsungDiscoverySession"

    const-string v1, "[onValidatePrinters]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_0
    return-void
.end method
