.class public Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;
.super Ljava/lang/Object;
.source "ManualDeviceIO.java"


# static fields
.field public static final DEVINFO_ADDITION_DUPLICATE:I = 0x2

.field public static final DEVINFO_ADDITION_FAILED:I = 0x0

.field public static final DEVINFO_ADDITION_SUCCESSFUL:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public static addDeviceInfo(Landroid/content/Context;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "devinfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .prologue
    .line 83
    const/4 v1, 0x0

    .line 85
    .local v1, "ret":I
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->loadManualDeviceInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 86
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    if-nez v0, :cond_0

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .restart local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    :cond_0
    invoke-static {v0, p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->isDuplicate(Ljava/util/ArrayList;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 90
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 92
    .local v2, "size":I
    invoke-static {p0, v2, v0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->saveManualDeviceInfo(Landroid/content/Context;ILjava/util/ArrayList;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 93
    const/4 v1, 0x1

    .line 99
    .end local v2    # "size":I
    :cond_1
    :goto_0
    return v1

    .line 97
    :cond_2
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public static deleteDevice(Landroid/content/Context;I)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "index"    # I

    .prologue
    .line 119
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->loadManualDeviceInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 120
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 121
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 122
    .local v1, "size":I
    invoke-static {p0, v1, v0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->saveManualDeviceInfo(Landroid/content/Context;ILjava/util/ArrayList;)Z

    move-result v2

    return v2
.end method

.method public static deleteDevice(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ip"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 126
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->loadManualDeviceInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 128
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    const/4 v0, 0x0

    .line 130
    .local v0, "index":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 139
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 140
    .local v3, "size":I
    invoke-static {p0, v3, v1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->saveManualDeviceInfo(Landroid/content/Context;ILjava/util/ArrayList;)Z

    move-result v4

    return v4

    .line 130
    .end local v3    # "size":I
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 131
    .local v2, "manualDeviceInfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 133
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 136
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getManualDeviceInfoFromFile(Landroid/content/Context;I)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "index"    # I

    .prologue
    .line 144
    const/4 v0, 0x0

    .line 145
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    invoke-static {p0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->loadManualDeviceInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 146
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    return-object v1
.end method

.method public static isDuplicate(Ljava/util/ArrayList;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)Z
    .locals 4
    .param p1, "devInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;",
            ">;",
            "Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    const/4 v1, 0x0

    .line 104
    .local v1, "ret":Z
    if-eqz p0, :cond_0

    .line 105
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 115
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return v1

    .line 106
    .restart local v0    # "i":I
    :cond_1
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 110
    const/4 v1, 0x1

    .line 111
    goto :goto_1

    .line 105
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static loadManualDeviceInfo(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    const/4 v0, 0x0

    .line 27
    .local v0, "deviceCount":I
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .local v1, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    new-instance v2, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-direct {v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;-><init>()V

    .line 29
    .local v2, "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v8

    .line 30
    .local v8, "root":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    const-string v9, "manualdeviceinfo"

    invoke-direct {v4, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 31
    .local v4, "file":Ljava/io/File;
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 33
    .local v6, "ios":Ljava/io/FileInputStream;
    new-instance v7, Ljava/io/ObjectInputStream;

    invoke-direct {v7, v6}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 34
    .local v7, "objectinput":Ljava/io/ObjectInput;
    invoke-interface {v7}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    .line 35
    if-lez v0, :cond_0

    .line 36
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v0, :cond_1

    .line 42
    .end local v5    # "i":I
    :cond_0
    invoke-interface {v7}, Ljava/io/ObjectInput;->close()V

    .line 43
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 49
    .end local v1    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    .end local v2    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .end local v4    # "file":Ljava/io/File;
    .end local v6    # "ios":Ljava/io/FileInputStream;
    .end local v7    # "objectinput":Ljava/io/ObjectInput;
    .end local v8    # "root":Ljava/io/File;
    :goto_1
    return-object v1

    .line 37
    .restart local v1    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    .restart local v2    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "i":I
    .restart local v6    # "ios":Ljava/io/FileInputStream;
    .restart local v7    # "objectinput":Ljava/io/ObjectInput;
    .restart local v8    # "root":Ljava/io/File;
    :cond_1
    invoke-interface {v7}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    check-cast v2, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 38
    .restart local v2    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 46
    .end local v1    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    .end local v2    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "i":I
    .end local v6    # "ios":Ljava/io/FileInputStream;
    .end local v7    # "objectinput":Ljava/io/ObjectInput;
    .end local v8    # "root":Ljava/io/File;
    :catch_0
    move-exception v3

    .line 49
    .local v3, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static saveManualDeviceInfo(Landroid/content/Context;ILjava/util/ArrayList;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 56
    .local p2, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    :try_start_0
    new-instance v0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-direct {v0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;-><init>()V

    .line 57
    .local v0, "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    .line 58
    .local v6, "root":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v7, "manualdeviceinfo"

    invoke-direct {v2, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 59
    .local v2, "file":Ljava/io/File;
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 61
    .local v3, "fos":Ljava/io/FileOutputStream;
    new-instance v5, Ljava/io/ObjectOutputStream;

    invoke-direct {v5, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 62
    .local v5, "objectOutput":Ljava/io/ObjectOutput;
    invoke-interface {v5, p1}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 63
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, p1, :cond_0

    .line 67
    invoke-interface {v5}, Ljava/io/ObjectOutput;->flush()V

    .line 68
    invoke-interface {v5}, Ljava/io/ObjectOutput;->close()V

    .line 69
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 70
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 71
    const/4 v7, 0x1

    .line 76
    .end local v0    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .end local v4    # "i":I
    .end local v5    # "objectOutput":Ljava/io/ObjectOutput;
    .end local v6    # "root":Ljava/io/File;
    :goto_1
    return v7

    .line 64
    .restart local v0    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "i":I
    .restart local v5    # "objectOutput":Ljava/io/ObjectOutput;
    .restart local v6    # "root":Ljava/io/File;
    :cond_0
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    check-cast v0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 65
    .restart local v0    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    invoke-interface {v5, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 73
    .end local v0    # "devinfo":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .end local v4    # "i":I
    .end local v5    # "objectOutput":Ljava/io/ObjectOutput;
    .end local v6    # "root":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 75
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 76
    const/4 v7, 0x0

    goto :goto_1
.end method
