.class public Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;
.super Landroid/app/DialogFragment;
.source "DialogLongPressEventInAddPrinter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment",
        "NewApi"
    }
.end annotation


# static fields
.field static _instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;


# instance fields
.field btnForgetPrinter:Landroid/widget/Button;

.field btnModifyPrinter:Landroid/widget/Button;

.field dialog:Landroid/app/AlertDialog;

.field listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

.field manualDeviceInfo:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

.field v:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 32
    sput-object p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;

    .line 33
    return-void
.end method

.method public static newInstance(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;
    .locals 2
    .param p0, "manualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .param p1, "listener"    # Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .prologue
    .line 37
    new-instance v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;

    invoke-direct {v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;-><init>()V

    .line 39
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;

    iput-object p0, v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->manualDeviceInfo:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 40
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;

    iput-object p1, v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .line 42
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 43
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "ManualDeviceInfo"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 44
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;

    invoke-virtual {v1, v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->setArguments(Landroid/os/Bundle;)V

    .line 45
    sget-object v1, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->_instance:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;

    return-object v1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 104
    :goto_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/Activity;->setResult(I)V

    .line 105
    return-void

    .line 89
    :pswitch_0
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->manualDeviceInfo:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v3, v4}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;->removePrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->dismiss()V

    goto :goto_0

    .line 93
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 94
    .local v0, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "dialogadd"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 95
    .local v2, "prev":Landroid/app/Fragment;
    if-eqz v2, :cond_0

    .line 96
    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 98
    :cond_0
    const v3, 0x7f050015

    invoke-virtual {p0, v3}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->manualDeviceInfo:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    invoke-static {v3, v4, v5}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->newInstance(Ljava/lang/String;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    move-result-object v1

    .line 99
    .local v1, "newFragment":Landroid/app/DialogFragment;
    const-string v3, "dialogadd"

    invoke-virtual {v1, v0, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 100
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->dismiss()V

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x7f0c0019
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const/4 v1, 0x0

    .local v1, "style":I
    const/4 v2, 0x0

    .line 54
    .local v2, "theme":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->setStyle(II)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 57
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 58
    const-string v3, "ManualDeviceInfo"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    iput-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->manualDeviceInfo:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 60
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 66
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030004

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->v:Landroid/view/View;

    .line 67
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->v:Landroid/view/View;

    const v3, 0x7f0c001a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->btnForgetPrinter:Landroid/widget/Button;

    .line 68
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->v:Landroid/view/View;

    const v3, 0x7f0c0019

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->btnModifyPrinter:Landroid/widget/Button;

    .line 69
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->btnForgetPrinter:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->btnModifyPrinter:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 73
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->manualDeviceInfo:Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v3}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 74
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->v:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 75
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 77
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->dialog:Landroid/app/AlertDialog;

    .line 78
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 80
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->dialog:Landroid/app/AlertDialog;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 81
    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->dialog:Landroid/app/AlertDialog;

    return-object v2
.end method

.method public setListener(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogLongPressEventInAddPrinter;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .line 109
    return-void
.end method
