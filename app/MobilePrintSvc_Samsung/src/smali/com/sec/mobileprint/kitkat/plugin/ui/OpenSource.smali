.class public Lcom/sec/mobileprint/kitkat/plugin/ui/OpenSource;
.super Landroid/app/Activity;
.source "OpenSource.java"


# instance fields
.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/OpenSource;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 19
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 20
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 21
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 22
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 24
    const v1, 0x7f030006

    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/OpenSource;->setContentView(I)V

    .line 25
    const v1, 0x7f0c001c

    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/OpenSource;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    iput-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/OpenSource;->webView:Landroid/webkit/WebView;

    .line 26
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/OpenSource;->webView:Landroid/webkit/WebView;

    const-string v2, "file:///android_asset/LICENSE_MobilePrint_Android.html"

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 32
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 36
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 34
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/OpenSource;->finish()V

    goto :goto_0

    .line 32
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
