.class Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;
.super Ljava/lang/Object;
.source "SamsungKitKatPrintService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->onJobStatusUpdated(III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

.field private final synthetic val$jobId:I

.field private final synthetic val$type:I

.field private final synthetic val$value:I


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;III)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->this$0:Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

    iput p2, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->val$jobId:I

    iput p3, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->val$type:I

    iput p4, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->val$value:I

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 195
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->this$0:Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;

    iget v2, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->val$jobId:I

    # invokes: Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->getPrintJobfromMap(I)Landroid/printservice/PrintJob;
    invoke-static {v1, v2}, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;->access$0(Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService;I)Landroid/printservice/PrintJob;

    move-result-object v0

    .line 196
    .local v0, "job":Landroid/printservice/PrintJob;
    if-eqz v0, :cond_0

    .line 197
    iget v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->val$type:I

    if-nez v1, :cond_1

    .line 198
    invoke-virtual {v0}, Landroid/printservice/PrintJob;->start()Z

    .line 230
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 199
    :cond_1
    iget v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->val$type:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    .line 200
    iget v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->val$value:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 202
    :pswitch_1
    const-string v1, "Out of Memory"

    invoke-virtual {v0, v1}, Landroid/printservice/PrintJob;->fail(Ljava/lang/String;)Z

    goto :goto_0

    .line 205
    :pswitch_2
    const-string v1, "Option Error"

    invoke-virtual {v0, v1}, Landroid/printservice/PrintJob;->fail(Ljava/lang/String;)Z

    goto :goto_0

    .line 208
    :pswitch_3
    const-string v1, "Not Supported Contents"

    invoke-virtual {v0, v1}, Landroid/printservice/PrintJob;->fail(Ljava/lang/String;)Z

    goto :goto_0

    .line 211
    :pswitch_4
    const-string v1, "Communication Error"

    invoke-virtual {v0, v1}, Landroid/printservice/PrintJob;->block(Ljava/lang/String;)Z

    goto :goto_0

    .line 216
    :pswitch_5
    const-string v1, "Not Supported Printer"

    invoke-virtual {v0, v1}, Landroid/printservice/PrintJob;->fail(Ljava/lang/String;)Z

    goto :goto_0

    .line 219
    :pswitch_6
    const-string v1, "Invalid Contents"

    invoke-virtual {v0, v1}, Landroid/printservice/PrintJob;->fail(Ljava/lang/String;)Z

    goto :goto_0

    .line 223
    :cond_2
    iget v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->val$type:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    .line 224
    invoke-virtual {v0}, Landroid/printservice/PrintJob;->complete()Z

    goto :goto_0

    .line 225
    :cond_3
    iget v1, p0, Lcom/sec/mobileprint/kitkat/plugin/SamsungKitKatPrintService$2;->val$type:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    .line 226
    invoke-virtual {v0}, Landroid/printservice/PrintJob;->cancel()Z

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
