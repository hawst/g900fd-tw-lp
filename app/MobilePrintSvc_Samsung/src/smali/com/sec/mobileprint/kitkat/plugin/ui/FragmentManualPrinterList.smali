.class public Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;
.super Landroid/app/Fragment;
.source "FragmentManualPrinterList.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# static fields
.field static addPrinterListFragment:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

.field static listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;


# instance fields
.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceListAdapter:Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;

.field private mDeviceListView:Landroid/widget/ListView;

.field private mTextView:Landroid/widget/TextView;

.field newFragment:Landroid/app/DialogFragment;

.field private view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->addPrinterListFragment:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    .line 41
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->addPrinterListFragment:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    return-object v0
.end method

.method private initContentsView(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 110
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initContentsView: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->view:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->view:Landroid/view/View;

    const v1, 0x7f0c001b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListView:Landroid/widget/ListView;

    .line 113
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListAdapter:Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 114
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList$1;

    invoke-direct {v1, p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList$1;-><init>(Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 134
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListAdapter:Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;

    invoke-virtual {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->notifyDataSetChanged()V

    .line 135
    return-void
.end method

.method private loadManualPrinterList()V
    .locals 1

    .prologue
    .line 185
    :try_start_0
    sget-object v0, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->loadManualDeviceInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :goto_0
    return-void

    .line 186
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static newInstance(Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;)Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;
    .locals 1
    .param p0, "dialogListener"    # Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .prologue
    .line 44
    sput-object p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->listener:Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter$DialogAddPrinterListener;

    .line 45
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->addPrinterListFragment:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    invoke-direct {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;-><init>()V

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->addPrinterListFragment:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    .line 47
    :cond_0
    sget-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->addPrinterListFragment:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    return-object v0
.end method

.method public static releaseInstance()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->addPrinterListFragment:Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;

    .line 56
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 57
    return-void
.end method

.method private updateViews()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 190
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 194
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->showTextBox(Z)V

    .line 201
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 199
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/ActivityAddPrinter;->showTextBox(Z)V

    goto :goto_0
.end method


# virtual methods
.method public addPrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V
    .locals 3
    .param p1, "manualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListAdapter:Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;

    invoke-virtual {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->notifyDataSetChanged()V

    .line 147
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 148
    sget-object v0, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->saveManualDeviceInfo(Landroid/content/Context;ILjava/util/ArrayList;)Z

    .line 149
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->updateViews()V

    .line 150
    return-void
.end method

.method public modifyPrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V
    .locals 7
    .param p1, "beforeManualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    .param p2, "afterManualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .prologue
    .line 161
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v2, "newDeviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 173
    iput-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    .line 174
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 177
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListAdapter:Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;

    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->setItem(Ljava/util/ArrayList;)V

    .line 178
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListAdapter:Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;

    invoke-virtual {v3}, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->notifyDataSetChanged()V

    .line 179
    sget-object v3, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget-object v5, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-static {v3, v4, v5}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->saveManualDeviceInfo(Landroid/content/Context;ILjava/util/ArrayList;)Z

    .line 180
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->updateViews()V

    .line 181
    return-void

    .line 163
    :cond_0
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v3, p1}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->equals(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 164
    new-instance v3, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-direct {v3}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;-><init>()V

    .line 165
    invoke-virtual {p2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceFreindlyName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setDeviceFreindlyName(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    move-result-object v3

    .line 166
    invoke-virtual {p2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setDeviceName(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    move-result-object v3

    .line 167
    invoke-virtual {p2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->getIPAdd()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;->setIPAdd(Ljava/lang/String;)Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    move-result-object v3

    .line 164
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 170
    :cond_1
    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 174
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .line 175
    .local v0, "device":Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;
    const-string v4, "NNN"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mDeviceList: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 103
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->initContentsView(Landroid/os/Bundle;)V

    .line 106
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 140
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 141
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->newFragment:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 142
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->setRetainInstance(Z)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    .local v0, "args":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 83
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->loadManualPrinterList()V

    .line 84
    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 85
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    .line 89
    :cond_0
    new-instance v1, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;

    invoke-virtual {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f030002

    iget-object v4, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListAdapter:Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;

    .line 90
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    if-nez p2, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 70
    :goto_0
    return-object v0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->view:Landroid/view/View;

    if-nez v0, :cond_1

    .line 66
    const v0, 0x7f030005

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->view:Landroid/view/View;

    .line 67
    const-string v0, "NNN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreateView: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->view:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->view:Landroid/view/View;

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 96
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->updateViews()V

    .line 97
    return-void
.end method

.method public removePrinter(Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;)V
    .locals 3
    .param p1, "manualDeviceInfo"    # Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceInfo;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListAdapter:Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;

    invoke-virtual {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/AdapterAddPrinterList;->notifyDataSetChanged()V

    .line 155
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 156
    sget-object v0, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->mDeviceList:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/sec/mobileprint/kitkat/plugin/utils/ManualDeviceIO;->saveManualDeviceInfo(Landroid/content/Context;ILjava/util/ArrayList;)Z

    .line 157
    invoke-direct {p0}, Lcom/sec/mobileprint/kitkat/plugin/ui/FragmentManualPrinterList;->updateViews()V

    .line 158
    return-void
.end method
