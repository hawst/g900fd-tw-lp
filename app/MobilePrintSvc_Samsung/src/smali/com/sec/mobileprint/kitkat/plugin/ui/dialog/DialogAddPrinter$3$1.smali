.class Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3$1;
.super Ljava/lang/Object;
.source "DialogAddPrinter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;->onShow(Landroid/content/DialogInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3$1;->this$1:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 203
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 204
    iget-object v0, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3$1;->this$1:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;

    # getter for: Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;
    invoke-static {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;->access$0(Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;)Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->showProgressDialog()V

    .line 205
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;

    sget-object v1, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3$1;->this$1:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;

    # getter for: Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;
    invoke-static {v2}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;->access$0(Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;)Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;->textIPAddress:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3$1;->this$1:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;

    # getter for: Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;->this$0:Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;
    invoke-static {v3}, Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;->access$0(Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter$3;)Lcom/sec/mobileprint/kitkat/plugin/ui/dialog/DialogAddPrinter;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->startDeviceManagerConnector()V

    .line 206
    return-void
.end method
