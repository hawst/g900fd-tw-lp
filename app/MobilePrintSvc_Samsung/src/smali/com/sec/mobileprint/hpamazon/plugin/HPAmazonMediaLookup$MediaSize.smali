.class public Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;
.super Ljava/lang/Object;
.source "HPAmazonMediaLookup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaSize"
.end annotation


# instance fields
.field private bottoMargin:I

.field private height:I

.field private leftMargin:I

.field private mediaName:Ljava/lang/String;

.field private rightMargin:I

.field private topMargin:I

.field private width:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 0
    .param p1, "mediaName"    # Ljava/lang/String;
    .param p2, "leftMargin"    # I
    .param p3, "topMargin"    # I
    .param p4, "rightMargin"    # I
    .param p5, "bottoMargin"    # I
    .param p6, "length"    # I
    .param p7, "width"    # I

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->mediaName:Ljava/lang/String;

    .line 41
    iput p7, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->width:I

    .line 42
    iput p6, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->height:I

    .line 43
    iput p3, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->topMargin:I

    .line 44
    iput p5, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->bottoMargin:I

    .line 45
    iput p2, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->leftMargin:I

    .line 46
    iput p4, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->rightMargin:I

    .line 47
    return-void
.end method


# virtual methods
.method public getBottoMargin()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->bottoMargin:I

    return v0
.end method

.method public getLeftMargin()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->leftMargin:I

    return v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->height:I

    return v0
.end method

.method public getMediaName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->mediaName:Ljava/lang/String;

    return-object v0
.end method

.method public getRightMargin()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->rightMargin:I

    return v0
.end method

.method public getTopMargin()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->topMargin:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->width:I

    return v0
.end method

.method public setBottoMargin(I)V
    .locals 0
    .param p1, "bottoMargin"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->bottoMargin:I

    .line 86
    return-void
.end method

.method public setLeftMargin(I)V
    .locals 0
    .param p1, "leftMargin"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->leftMargin:I

    .line 94
    return-void
.end method

.method public setLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->height:I

    .line 70
    return-void
.end method

.method public setMediaName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mediaName"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->mediaName:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setRightMargin(I)V
    .locals 0
    .param p1, "rightMargin"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->rightMargin:I

    .line 102
    return-void
.end method

.method public setTopMargin(I)V
    .locals 0
    .param p1, "topMargin"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->topMargin:I

    .line 78
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonMediaLookup$MediaSize;->width:I

    .line 62
    return-void
.end method
