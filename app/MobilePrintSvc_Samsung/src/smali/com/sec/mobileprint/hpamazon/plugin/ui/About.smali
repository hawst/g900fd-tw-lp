.class public Lcom/sec/mobileprint/hpamazon/plugin/ui/About;
.super Landroid/app/Activity;
.source "About.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private btnPrintQueue:Landroid/widget/Button;

.field private btnSupplies:Landroid/widget/Button;

.field private textLicense:Landroid/widget/TextView;

.field private textVersion:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->btnSupplies:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    .line 85
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    const-string v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    const-string v1, "http://www.samsung.com"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 89
    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->startActivity(Landroid/content/Intent;)V

    .line 95
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0x8

    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActionBar;->hide()V

    .line 35
    const v7, 0x7f03000a

    invoke-virtual {p0, v7}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->setContentView(I)V

    .line 37
    const v7, 0x7f0c002c

    invoke-virtual {p0, v7}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->textVersion:Landroid/widget/TextView;

    .line 38
    const v7, 0x7f0c002f

    invoke-virtual {p0, v7}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->textLicense:Landroid/widget/TextView;

    .line 39
    const v7, 0x7f0c0030

    invoke-virtual {p0, v7}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->btnSupplies:Landroid/widget/Button;

    .line 40
    iget-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->btnSupplies:Landroid/widget/Button;

    invoke-virtual {v7, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    const v7, 0x7f0c002e

    invoke-virtual {p0, v7}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->btnPrintQueue:Landroid/widget/Button;

    .line 42
    iget-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->btnPrintQueue:Landroid/widget/Button;

    invoke-virtual {v7, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    iget-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->btnSupplies:Landroid/widget/Button;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 45
    iget-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->btnPrintQueue:Landroid/widget/Button;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 48
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 51
    .local v5, "stringBufferEula":Ljava/lang/StringBuffer;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v7

    const-string v8, "LICENSE_EN.txt"

    invoke-virtual {v7, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 52
    .local v2, "inputStream":Ljava/io/InputStream;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    const-string v8, "UTF-8"

    invoke-direct {v7, v2, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 53
    .local v1, "f":Ljava/io/BufferedReader;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 54
    .local v3, "line":Ljava/lang/String;
    :goto_0
    if-nez v3, :cond_1

    .line 67
    .end local v1    # "f":Ljava/io/BufferedReader;
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "line":Ljava/lang/String;
    :goto_1
    iget-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->textLicense:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    const/4 v4, 0x0

    .line 71
    .local v4, "pInfo":Landroid/content/pm/PackageInfo;
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 76
    :goto_2
    if-eqz v4, :cond_0

    .line 77
    iget-object v6, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 78
    .local v6, "version":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->textVersion:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/mobileprint/hpamazon/plugin/ui/About;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050033

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    .end local v6    # "version":Ljava/lang/String;
    :cond_0
    return-void

    .line 56
    .end local v4    # "pInfo":Landroid/content/pm/PackageInfo;
    .restart local v1    # "f":Ljava/io/BufferedReader;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "line":Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 57
    const-string v7, "\n"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 58
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v3

    goto :goto_0

    .line 62
    .end local v1    # "f":Ljava/io/BufferedReader;
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "line":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 72
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v4    # "pInfo":Landroid/content/pm/PackageInfo;
    :catch_1
    move-exception v0

    .line 74
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_2
.end method
