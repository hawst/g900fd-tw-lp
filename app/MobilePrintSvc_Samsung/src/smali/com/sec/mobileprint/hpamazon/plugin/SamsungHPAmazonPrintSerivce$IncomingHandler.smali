.class Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce$IncomingHandler;
.super Landroid/os/Handler;
.source "SamsungHPAmazonPrintSerivce.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IncomingHandler"
.end annotation


# instance fields
.field private final mServiceRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;)V
    .locals 1
    .param p1, "serviceRef"    # Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 75
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce$IncomingHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    .line 76
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 80
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce$IncomingHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;

    .line 81
    .local v12, "serviceRef":Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;
    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Landroid/content/Intent;

    .line 82
    .local v5, "intent":Landroid/content/Intent;
    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 84
    .local v2, "bundle":Landroid/os/Bundle;
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "action":Ljava/lang/String;
    # getter for: Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->access$0()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Action Recvd:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const-string v13, "org.androidprinting.intent.ACTION_START_DEVICE_DISCOVERY"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 88
    new-instance v4, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    invoke-virtual {v12}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    sget-object v15, Lcom/sec/mobileprint/core/print/DeviceType;->DEVICETYPE_PRINTER:Lcom/sec/mobileprint/core/print/DeviceType;

    invoke-direct {v4, v13, v14, v15}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;-><init>(Landroid/content/Context;Landroid/os/Messenger;Lcom/sec/mobileprint/core/print/DeviceType;)V

    .line 89
    .local v4, "deviceDiscovery":Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;
    invoke-virtual {v4}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->startDeviceManagerConnector()V

    .line 154
    .end local v4    # "deviceDiscovery":Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    const-string v13, "org.androidprinting.intent.ACTION_GET_PRINT_OPTIONS"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 92
    const-string v13, "printer-address"

    invoke-virtual {v2, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 93
    .local v6, "ipAddress":Ljava/lang/String;
    new-instance v3, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;

    invoke-virtual {v12}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    const/4 v15, 0x3

    invoke-direct {v3, v13, v6, v14, v15}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Messenger;I)V

    .line 94
    .local v3, "deviceCapability":Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;
    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->startDeviceManagerConnector()V

    goto :goto_0

    .line 96
    .end local v3    # "deviceCapability":Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;
    .end local v6    # "ipAddress":Ljava/lang/String;
    :cond_2
    const-string v13, "org.androidprinting.intent.ACTION_GET_FINAL_PRINT_SETTINGS"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 98
    invoke-static/range {p1 .. p1}, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonPrintJob;->replyFinalSetings(Landroid/os/Message;)V

    goto :goto_0

    .line 100
    :cond_3
    const-string v13, "org.androidprinting.intent.ACTION_PRINT"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 101
    new-instance v11, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonPrintJob;

    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-direct {v11, v2, v13}, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonPrintJob;-><init>(Landroid/os/Bundle;Landroid/os/Messenger;)V

    .line 102
    .local v11, "printJob":Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonPrintJob;
    invoke-virtual {v11}, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonPrintJob;->validatePrintJob()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 103
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v13

    invoke-virtual {v11}, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonPrintJob;->getSamsungPrintJob()Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)I

    move-result v10

    .line 104
    .local v10, "jobid":I
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v13

    invoke-virtual {v13, v12}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V

    .line 105
    iget-object v13, v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v11}, Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonPrintJob;->getJobHandle()Ljava/lang/String;

    move-result-object v14

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 111
    .end local v10    # "jobid":I
    .end local v11    # "printJob":Lcom/sec/mobileprint/hpamazon/plugin/HPAmazonPrintJob;
    :cond_4
    const-string v13, "org.androidprinting.intent.ACTION_CANCEL_PRINT_JOB"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 112
    const-string v13, "print-job-handle"

    invoke-virtual {v2, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 113
    .local v7, "jobHandle":Ljava/lang/String;
    iget-object v13, v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v13, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 114
    .local v9, "jobId":I
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v13

    invoke-virtual {v13, v9}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->cancelPrint(I)V

    goto/16 :goto_0

    .line 115
    .end local v7    # "jobHandle":Ljava/lang/String;
    .end local v9    # "jobId":I
    :cond_5
    const-string v13, "org.androidprinting.intent.ACTION_CANCEL_ALL_PRINT_JOBS"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 116
    iget-object v13, v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v13}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v13

    invoke-static {v13}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object v8

    .line 117
    .local v8, "jobHandleEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 118
    iget-object v13, v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 119
    .restart local v9    # "jobId":I
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v13

    invoke-virtual {v13, v9}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->cancelPrint(I)V

    goto :goto_1

    .line 121
    .end local v8    # "jobHandleEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .end local v9    # "jobId":I
    :cond_6
    const-string v13, "org.androidprinting.intent.ACTION_REGISTER_STATUS_RECEIVER"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 123
    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    if-eqz v13, :cond_0

    .line 124
    # getter for: Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;
    invoke-static {v12}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->access$1(Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;)Ljava/util/LinkedList;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v13, v14}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 125
    # getter for: Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;
    invoke-static {v12}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->access$1(Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;)Ljava/util/LinkedList;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v13, v14}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 129
    :cond_7
    const-string v13, "org.androidprinting.intent.ACTION_UNREGISTER_STATUS_RECEIVER"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 131
    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    if-eqz v13, :cond_0

    .line 132
    # getter for: Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;
    invoke-static {v12}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->access$1(Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;)Ljava/util/LinkedList;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v13, v14}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 134
    :cond_8
    const-string v13, "org.androidprinting.intent.ACTION_GET_JOB_STATUS"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_9

    .line 135
    const-string v13, "org.androidprinting.intent.ACTION_START_MONITORING_PRINTER_STATUS"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 138
    :cond_9
    new-instance v13, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual {v12}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-direct {v13, v14, v0}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;-><init>(Landroid/content/Context;Landroid/os/Message;)V

    iput-object v13, v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 139
    iget-object v13, v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual {v13}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->startMonitoring()V

    goto/16 :goto_0

    .line 141
    :cond_a
    const-string v13, "org.androidprinting.intent.ACTION_GET_ALL_JOBS_STATUS"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 143
    const-string v13, "org.androidprinting.intent.ACTION_GET_PRINTER_STATUS"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 145
    const-string v13, "org.androidprinting.intent.ACTION_STOP_MONITORING_PRINTER_STATUS"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 147
    iget-object v13, v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    if-eqz v13, :cond_0

    .line 148
    iget-object v13, v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->stopMonitoring(Landroid/os/Message;)V

    .line 149
    const/4 v13, 0x0

    iput-object v13, v12, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    goto/16 :goto_0
.end method
