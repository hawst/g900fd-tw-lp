.class public Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;
.super Landroid/app/Service;
.source "SamsungHPAmazonPrintSerivce.java"

# interfaces
.implements Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce$IncomingHandler;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field mJobMap:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final mMessenger:Landroid/os/Messenger;

.field mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

.field private mStatusCallbacks:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "SamsungHPAmazonPrintSerivce"

    sput-object v0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 327
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce$IncomingHandler;-><init>(Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mMessenger:Landroid/os/Messenger;

    .line 29
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;

    return-object v0
.end method

.method private getJobHandlefromMap(I)Ljava/lang/String;
    .locals 5
    .param p1, "jobId"    # I

    .prologue
    .line 312
    iget-object v3, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 320
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 312
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 313
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 314
    .local v1, "key":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 316
    .local v2, "value":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, p1, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public getCopyNumber(III)I
    .locals 4
    .param p1, "currentPage"    # I
    .param p2, "requestedCopies"    # I
    .param p3, "pageCount"    # I

    .prologue
    const/4 v1, 0x1

    .line 298
    const/4 v0, 0x1

    .line 299
    .local v0, "copy":I
    div-int v2, p1, p3

    rem-int v3, p1, p3

    if-ne v3, v1, :cond_0

    const/4 v1, 0x0

    :cond_0
    add-int v0, v2, v1

    .line 300
    return v0
.end method

.method public getPageNumber(III)I
    .locals 1
    .param p1, "currentPage"    # I
    .param p2, "requestedCopies"    # I
    .param p3, "pageCount"    # I

    .prologue
    .line 304
    const/4 v0, 0x1

    .line 305
    .local v0, "pageNumber":I
    rem-int v0, p1, p3

    .line 306
    if-nez v0, :cond_0

    move v0, p3

    .line 307
    :cond_0
    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 56
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    .line 57
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;

    .line 58
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 64
    return-void
.end method

.method public onJobStatusUpdated(III)V
    .locals 12
    .param p1, "jobId"    # I
    .param p2, "type"    # I
    .param p3, "value"    # I

    .prologue
    .line 160
    const/4 v3, 0x0

    .line 161
    .local v3, "mTotalPages":I
    const-string v7, ""

    .line 162
    .local v7, "typeString":Ljava/lang/String;
    const-string v8, ""

    .line 163
    .local v8, "valueString":Ljava/lang/String;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 164
    .local v0, "bundle":Landroid/os/Bundle;
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 166
    .local v5, "returnIntent":Landroid/content/Intent;
    packed-switch p2, :pswitch_data_0

    .line 279
    :cond_0
    :goto_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "type : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " value : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 280
    .local v6, "statusMessage":Ljava/lang/String;
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_2

    .line 281
    :cond_1
    const-string v9, "org.androidprinting.intent.ACTION_RETURN_PRINT_JOB_STATUS"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    invoke-virtual {v5, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 284
    :cond_2
    iget-object v9, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_3

    .line 293
    return-void

    .line 169
    .end local v6    # "statusMessage":Ljava/lang/String;
    :pswitch_0
    const-string v7, "prepare"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 170
    const-string v9, "org.androidprinting.intent.ACTION_RETURN_PRINT_STARTED"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const-string v9, "request-action"

    const-string v10, "org.androidprinting.intent.ACTION_PRINT"

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string v9, "printer-address"

    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getIPAddress()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const-string v9, "print-job-handle"

    invoke-direct {p0, p1}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 176
    :pswitch_1
    const-string v7, "start job"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Total Page="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 177
    move v3, p3

    .line 178
    goto/16 :goto_0

    .line 181
    :pswitch_2
    const-string v7, "start page"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Page No="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 182
    const-string v9, "print-job-handle"

    invoke-direct {p0, p1}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v9, "status-printer-state"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 184
    const-string v9, "print-job-status"

    const-string v10, "print-job-running"

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v9, "status-page-info"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 186
    const-string v9, "page-start-info"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 187
    const-string v9, "copy-num"

    .line 188
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getCopies()I

    move-result v10

    .line 189
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v11

    invoke-virtual {v11, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v11

    array-length v11, v11

    .line 187
    invoke-virtual {p0, p3, v10, v11}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getCopyNumber(III)I

    move-result v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 190
    const-string v9, "page-num"

    .line 191
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getCopies()I

    move-result v10

    .line 192
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v11

    invoke-virtual {v11, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v11

    array-length v11, v11

    .line 190
    invoke-virtual {p0, p3, v10, v11}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getPageNumber(III)I

    move-result v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 193
    const-string v9, "current-page"

    invoke-virtual {v0, v9, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    const-string v9, "total-pages"

    invoke-virtual {v0, v9, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 198
    :pswitch_3
    const-string v7, "printing"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "percentage="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 199
    goto/16 :goto_0

    .line 202
    :pswitch_4
    const-string v7, "completed page"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "completed page No="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 203
    const-string v9, "print-job-handle"

    invoke-direct {p0, p1}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v9, "status-printer-state"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 205
    const-string v9, "print-job-status"

    const-string v10, "print-job-running"

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v9, "status-page-info"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 207
    const-string v9, "page-start-info"

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 208
    const-string v9, "copy-num"

    .line 209
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getCopies()I

    move-result v10

    .line 210
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v11

    invoke-virtual {v11, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v11

    array-length v11, v11

    .line 208
    invoke-virtual {p0, p3, v10, v11}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getCopyNumber(III)I

    move-result v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 211
    const-string v9, "page-num"

    .line 212
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getCopies()I

    move-result v10

    .line 213
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v11

    invoke-virtual {v11, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v11

    array-length v11, v11

    .line 211
    invoke-virtual {p0, p3, v10, v11}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getPageNumber(III)I

    move-result v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    const-string v9, "current-page"

    invoke-virtual {v0, v9, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 215
    const-string v9, "total-pages"

    invoke-virtual {v0, v9, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 220
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v2

    .line 221
    .local v2, "jobHandle":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 224
    const-string v7, "completed job"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "total Page="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 225
    const-string v9, "print-job-handle"

    invoke-virtual {v0, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v9, "status-printer-state"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 227
    const-string v9, "print-job-status"

    const-string v10, "print-job-complete"

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string v9, "print-job-done-result"

    const-string v10, "job-success"

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget-object v9, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 235
    .end local v2    # "jobHandle":Ljava/lang/String;
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v2

    .line 236
    .restart local v2    # "jobHandle":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 238
    const-string v7, "canceled"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 239
    const-string v9, "print-job-handle"

    invoke-virtual {v0, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const-string v9, "status-printer-state"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 241
    const-string v9, "print-job-status"

    const-string v10, "print-job-complete"

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    const-string v9, "print-job-done-result"

    const-string v10, "job-cancelled"

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v9, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 248
    .end local v2    # "jobHandle":Ljava/lang/String;
    :pswitch_7
    const-string v7, "error"

    .line 249
    const-string v9, "org.androidprinting.intent.ACTION_RETURN_PRINT_ERROR"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    const-string v9, "print-error"

    const-string v10, "communication-error"

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    iget-object v9, p0, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-direct {p0, p1}, Lcom/sec/mobileprint/hpamazon/plugin/SamsungHPAmazonPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    packed-switch p3, :pswitch_data_1

    goto/16 :goto_0

    .line 255
    :pswitch_8
    const-string v8, "Out of memory"

    .line 256
    goto/16 :goto_0

    .line 258
    :pswitch_9
    const-string v8, "printing option error"

    .line 259
    goto/16 :goto_0

    .line 261
    :pswitch_a
    const-string v8, "not supported contents"

    .line 262
    goto/16 :goto_0

    .line 264
    :pswitch_b
    const-string v8, "Output stream error"

    .line 265
    goto/16 :goto_0

    .line 267
    :pswitch_c
    const-string v8, "PrintService STILL_RUNNING"

    .line 268
    goto/16 :goto_0

    .line 270
    :pswitch_d
    const-string v8, "NOT_SUPPORTED_PRINTER"

    .line 271
    goto/16 :goto_0

    .line 273
    :pswitch_e
    const-string v8, "Invalid contents"

    goto/16 :goto_0

    .line 284
    .restart local v6    # "statusMessage":Ljava/lang/String;
    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Messenger;

    .line 286
    .local v4, "messenger":Landroid/os/Messenger;
    const/4 v10, 0x0

    const/4 v11, 0x0

    :try_start_0
    invoke-static {v10, v11, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 287
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    .line 166
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 252
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 49
    const/4 v0, 0x2

    return v0
.end method
