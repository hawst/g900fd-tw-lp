.class Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;
.super Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;
.source "SamsungPrintSDKService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    .line 75
    invoke-direct {p0}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelPrintJob(I)V
    .locals 1
    .param p1, "jobId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->cancelPrint(I)V

    .line 81
    return-void
.end method

.method public discoverPrinters(Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;)V
    .locals 3
    .param p1, "arg0"    # Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v1, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iput-object p1, v1, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    .line 89
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    iget-object v1, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    invoke-direct {v0, v1, v2}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;-><init>(Landroid/content/Context;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V

    .line 90
    .local v0, "discovery":Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;
    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->startDeviceManagerConnector()V

    .line 91
    return-void
.end method

.method public getAllJobList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/PrintJob;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 148
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v1, v1, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrintJobs:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 149
    .local v0, "jobs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/PrintJob;>;"
    return-object v0
.end method

.method public getJobStatus(I)Lcom/sec/mobileprint/print/apisdk/PrintJob;
    .locals 3
    .param p1, "jobId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v1, v1, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrintJobs:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/mobileprint/print/apisdk/PrintJob;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_0
    return-object v1

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPrinterCapability(Ljava/lang/String;Ljava/lang/String;Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;)V
    .locals 3
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "arg1"    # Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 101
    iget-object v1, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iput-object p3, v1, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    .line 102
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;

    iget-object v1, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V

    .line 103
    .local v0, "capability":Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;
    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->startDeviceManagerConnector()V

    .line 104
    return-void
.end method

.method public startTrackingPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;)V
    .locals 5
    .param p1, "printerInfo"    # Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    .param p2, "arg1"    # Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 111
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iput-object p1, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterInfo:Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    .line 112
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v0, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v0, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->stopMonitoring(Landroid/os/Message;)V

    .line 114
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iput-object v1, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    new-instance v1, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    iget-object v2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->getIPAddress()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V

    iput-object v1, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 117
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v0, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->startMonitoring()V

    .line 118
    return-void
.end method

.method public stopPrinterDiscovery()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mStopDiscovery:Z

    .line 95
    return-void
.end method

.method public stopTrackingPrinterStatus()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 124
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v0, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v0, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->stopMonitoring(Landroid/os/Message;)V

    .line 126
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iput-object v1, v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 128
    :cond_0
    return-void
.end method

.method public submitPrintJob(Lcom/sec/mobileprint/print/apisdk/PrintJob;Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;)I
    .locals 6
    .param p1, "printJob"    # Lcom/sec/mobileprint/print/apisdk/PrintJob;
    .param p2, "arg1"    # Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 134
    iget-object v3, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    move-result-object v4

    # invokes: Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->getSamsungPrintSettings(Lcom/sec/mobileprint/print/apisdk/PrintSettings;)Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    invoke-static {v3, v4}, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->access$0(Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;Lcom/sec/mobileprint/print/apisdk/PrintSettings;)Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v1

    .line 136
    .local v1, "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    iget-object v3, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iput-object p2, v3, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrintJobStatusListener:Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;

    .line 137
    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->getIPAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->getFilePaths()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->getFilePaths()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-static {v1, v4, v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->createPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v0

    .line 138
    .local v0, "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    invoke-virtual {v3, v4}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V

    .line 139
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)I

    move-result v2

    .line 140
    .local v2, "samJobId":I
    invoke-virtual {p1, v2}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->setJobId(I)V

    .line 141
    iget-object v3, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v3, v3, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrintJobs:Ljava/util/Hashtable;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    return v2
.end method
