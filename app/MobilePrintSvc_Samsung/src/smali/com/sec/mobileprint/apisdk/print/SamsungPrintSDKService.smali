.class public Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;
.super Landroid/app/Service;
.source "SamsungPrintSDKService.java"

# interfaces
.implements Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;
.implements Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;


# instance fields
.field private final mBinder:Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;

.field mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

.field mInitializationListener:Lcom/sec/mobileprint/print/apisdk/OnInitializationListener;

.field mPrintJobStatusListener:Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;

.field mPrintJobs:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/mobileprint/print/apisdk/PrintJob;",
            ">;"
        }
    .end annotation
.end field

.field mPrinterInfo:Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

.field mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

.field mStopDiscovery:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 75
    new-instance v0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$1;-><init>(Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;)V

    iput-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mBinder:Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;

    .line 40
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;Lcom/sec/mobileprint/print/apisdk/PrintSettings;)Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .locals 1

    .prologue
    .line 281
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->getSamsungPrintSettings(Lcom/sec/mobileprint/print/apisdk/PrintSettings;)Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v0

    return-object v0
.end method

.method private getSamsungPrintSettings(Lcom/sec/mobileprint/print/apisdk/PrintSettings;)Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .locals 3
    .param p1, "settings"    # Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    .prologue
    .line 283
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;-><init>()V

    .line 284
    .local v0, "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    invoke-static {}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->values()[Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->getColor()Lcom/sec/mobileprint/print/apisdk/ColorMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/mobileprint/print/apisdk/ColorMode;->getColorTypeValue()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setColor(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V

    .line 285
    invoke-static {}, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->values()[Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->getDuplex()Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->getDuplexTypeValue()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    .line 286
    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->getCopies()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setCopies(I)V

    .line 287
    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->getContentType()Lcom/sec/mobileprint/print/apisdk/ContentType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/mobileprint/print/apisdk/ContentType;->getContentTypeValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setContentType(I)V

    .line 288
    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->isPrintToFile()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setPrintToFile(Z)V

    .line 289
    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->getOutputFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setOutputFilePath(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->getMediaSize()Lcom/sec/mobileprint/print/apisdk/MediaSize;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/mobileprint/print/apisdk/MediaSize;->getMediaName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setMediaSize(Ljava/lang/String;)V

    .line 291
    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mBinder:Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 65
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrintJobs:Ljava/util/Hashtable;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mStopDiscovery:Z

    .line 67
    return-void
.end method

.method public onDeviceDiscoveredOrUpdated(Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;I)V
    .locals 3
    .param p1, "deviceInfo"    # Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;
    .param p2, "calledAfter"    # I

    .prologue
    .line 169
    new-instance v0, Landroid/os/Handler;

    sget-object v2, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 170
    .local v0, "mainHandler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;-><init>(Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;ILcom/sec/mobileprint/core/print/DeviceInfoWithCaps;)V

    .line 277
    .local v1, "myRunnable":Ljava/lang/Runnable;
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 278
    return-void
.end method

.method public onJobStatusUpdated(III)V
    .locals 3
    .param p1, "JobId"    # I
    .param p2, "type"    # I
    .param p3, "value"    # I

    .prologue
    .line 300
    new-instance v0, Landroid/os/Handler;

    sget-object v2, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 301
    .local v0, "mainHandler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;-><init>(Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;III)V

    .line 320
    .local v1, "myRunnable":Ljava/lang/Runnable;
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 321
    return-void
.end method
