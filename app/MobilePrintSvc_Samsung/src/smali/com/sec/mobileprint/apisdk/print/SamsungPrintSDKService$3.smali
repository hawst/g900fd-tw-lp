.class Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;
.super Ljava/lang/Object;
.source "SamsungPrintSDKService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->onJobStatusUpdated(III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

.field private final synthetic val$JobId:I

.field private final synthetic val$type:I

.field private final synthetic val$value:I


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;III)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iput p2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->val$JobId:I

    iput p3, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->val$type:I

    iput p4, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->val$value:I

    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 304
    iget-object v2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v2, v2, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrintJobs:Ljava/util/Hashtable;

    iget v3, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->val$JobId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/mobileprint/print/apisdk/PrintJob;

    .line 305
    .local v1, "job":Lcom/sec/mobileprint/print/apisdk/PrintJob;
    iget v2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->val$type:I

    iget v3, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->val$value:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->setJobStatus(II)V

    .line 306
    iget-object v2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v2, v2, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrintJobs:Ljava/util/Hashtable;

    invoke-virtual {v1}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->getJobId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    iget-object v2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v2, v2, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrintJobStatusListener:Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;

    if-eqz v2, :cond_0

    .line 311
    :try_start_0
    iget-object v2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v2, v2, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrintJobStatusListener:Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;

    iget v3, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->val$JobId:I

    iget v4, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->val$type:I

    iget v5, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$3;->val$value:I

    invoke-interface {v2, v3, v4, v5}, Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;->onPrintJobStatusUpdate(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 312
    :catch_0
    move-exception v0

    .line 314
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
