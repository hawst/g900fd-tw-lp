.class Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;
.super Ljava/lang/Object;
.source "SamsungPrintSDKService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->onDeviceDiscoveredOrUpdated(Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

.field private final synthetic val$calledAfter:I

.field private final synthetic val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;ILcom/sec/mobileprint/core/print/DeviceInfoWithCaps;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iput p2, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$calledAfter:I

    iput-object p3, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    .line 174
    iget v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$calledAfter:I

    if-ne v11, v13, :cond_1

    .line 176
    new-instance v1, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    invoke-direct {v1}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;-><init>()V

    .line 177
    .local v1, "device":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setPrinterName(Ljava/lang/String;)V

    .line 178
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setIPAddress(Ljava/lang/String;)V

    .line 179
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-boolean v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mStopDiscovery:Z

    if-nez v11, :cond_0

    .line 181
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    if-eqz v11, :cond_0

    .line 183
    :try_start_0
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    invoke-interface {v11, v1}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;->onDeviceDiscovered(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    .end local v1    # "device":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :cond_0
    :goto_0
    return-void

    .line 184
    .restart local v1    # "device":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :catch_0
    move-exception v3

    .line 186
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 191
    .end local v1    # "device":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_1
    iget v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$calledAfter:I

    const/4 v12, 0x2

    if-ne v11, v12, :cond_7

    .line 193
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    if-eqz v11, :cond_6

    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getMediaSize()Ljava/util/List;

    move-result-object v11

    if-eqz v11, :cond_6

    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getMediaSize()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_6

    .line 194
    new-instance v1, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    invoke-direct {v1}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;-><init>()V

    .line 195
    .restart local v1    # "device":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setPrinterName(Ljava/lang/String;)V

    .line 196
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceIpAddress()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setIPAddress(Ljava/lang/String;)V

    .line 197
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v7, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/MediaSize;>;"
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getMediaSize()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_4

    .line 206
    invoke-virtual {v1, v7}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setSupportedMediaSizes(Ljava/util/ArrayList;)V

    .line 207
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 208
    .local v10, "mediaTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/MediaType;>;"
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getMediaType()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_5

    .line 211
    invoke-virtual {v1, v10}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setSupportedMediaTypes(Ljava/util/ArrayList;)V

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 214
    .local v0, "colorModes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/ColorMode;>;"
    sget-object v11, Lcom/sec/mobileprint/print/apisdk/ColorMode;->COLOR_MODE_MONO:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getColor()I

    move-result v11

    if-ne v11, v13, :cond_2

    .line 216
    sget-object v11, Lcom/sec/mobileprint/print/apisdk/ColorMode;->COLOR_MODE_COLOR:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_2
    invoke-virtual {v1, v0}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setSupportedColorModes(Ljava/util/ArrayList;)V

    .line 221
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v2, "duplexModes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/DuplexMode;>;"
    sget-object v11, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->DUPLEX_MODE_SIMPLEX:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDuplex()I

    move-result v11

    if-ne v11, v13, :cond_3

    .line 224
    sget-object v11, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->DUPLEX_MODE_LONG_EDGE:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    sget-object v11, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->DUPLEX_MODE_SHORT_EDGE:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    :cond_3
    invoke-virtual {v1, v2}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setSupportedDuplexModes(Ljava/util/ArrayList;)V

    .line 228
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    if-eqz v11, :cond_0

    .line 230
    :try_start_1
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    invoke-interface {v11, v1}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;->onDeviceCapabilityUpdate(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 231
    :catch_1
    move-exception v3

    .line 233
    .restart local v3    # "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 198
    .end local v0    # "colorModes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/ColorMode;>;"
    .end local v2    # "duplexModes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/DuplexMode;>;"
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v10    # "mediaTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/MediaType;>;"
    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    .line 199
    .local v6, "media":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    invoke-virtual {v6}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;->getMediaName()Ljava/lang/String;

    move-result-object v8

    .line 200
    .local v8, "mediaName":Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->getMediaSizeID(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    move-result-object v4

    .line 201
    .local v4, "enumMedia":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    invoke-virtual {v4}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->getValue()I

    move-result v5

    .line 202
    .local v5, "id":I
    invoke-static {v5}, Lcom/sec/mobileprint/print/apisdk/MediaSize;->forValue(I)Lcom/sec/mobileprint/print/apisdk/MediaSize;

    move-result-object v9

    .line 203
    .local v9, "mediaSize":Lcom/sec/mobileprint/print/apisdk/MediaSize;
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 208
    .end local v4    # "enumMedia":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    .end local v5    # "id":I
    .end local v6    # "media":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    .end local v8    # "mediaName":Ljava/lang/String;
    .end local v9    # "mediaSize":Lcom/sec/mobileprint/print/apisdk/MediaSize;
    .restart local v10    # "mediaTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/MediaType;>;"
    :cond_5
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 209
    .local v6, "media":Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/mobileprint/print/apisdk/MediaType;->getMediaTypeValueFromName(Ljava/lang/String;)Lcom/sec/mobileprint/print/apisdk/MediaType;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 240
    .end local v1    # "device":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    .end local v6    # "media":Ljava/lang/String;
    .end local v7    # "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/MediaSize;>;"
    .end local v10    # "mediaTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/MediaType;>;"
    :cond_6
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    if-eqz v11, :cond_0

    .line 242
    :try_start_2
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;->onError(I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 243
    :catch_2
    move-exception v3

    .line 245
    .restart local v3    # "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 252
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_7
    iget v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$calledAfter:I

    const/4 v12, 0x3

    if-ne v11, v12, :cond_0

    .line 254
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceStatus()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceStatus()Ljava/lang/String;

    move-result-object v11

    const-string v12, "idl"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 255
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterInfo:Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    sget-object v12, Lcom/sec/mobileprint/print/apisdk/PrinterStatus;->PRINTER_STATUS_IDLE:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    invoke-virtual {v11, v12}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterStatus;)V

    .line 266
    :goto_3
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    if-eqz v11, :cond_0

    .line 268
    :try_start_3
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mDiscoveryListener:Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    iget-object v12, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v12, v12, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterInfo:Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    invoke-interface {v11, v12}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;->onDeviceStatusUpdate(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 269
    :catch_3
    move-exception v3

    .line 271
    .restart local v3    # "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 256
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_8
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceStatus()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_9

    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceStatus()Ljava/lang/String;

    move-result-object v11

    const-string v12, "printing"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 257
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterInfo:Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    sget-object v12, Lcom/sec/mobileprint/print/apisdk/PrinterStatus;->PRINTER_STATUS_BUSY:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    invoke-virtual {v11, v12}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterStatus;)V

    goto :goto_3

    .line 258
    :cond_9
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceStatus()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_a

    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceStatus()Ljava/lang/String;

    move-result-object v11

    const-string v12, "other"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 259
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterInfo:Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    sget-object v12, Lcom/sec/mobileprint/print/apisdk/PrinterStatus;->PRINTER_STATUS_IDLE:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    invoke-virtual {v11, v12}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterStatus;)V

    goto :goto_3

    .line 260
    :cond_a
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceStatus()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_b

    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->val$deviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceStatus()Ljava/lang/String;

    move-result-object v11

    const-string v12, "unknown"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 261
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterInfo:Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    sget-object v12, Lcom/sec/mobileprint/print/apisdk/PrinterStatus;->PRINTER_STATUS_UNAVAILABLE:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    invoke-virtual {v11, v12}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterStatus;)V

    goto :goto_3

    .line 263
    :cond_b
    iget-object v11, p0, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService$2;->this$0:Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;

    iget-object v11, v11, Lcom/sec/mobileprint/apisdk/print/SamsungPrintSDKService;->mPrinterInfo:Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    sget-object v12, Lcom/sec/mobileprint/print/apisdk/PrinterStatus;->PRINTER_STATUS_IDLE:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    invoke-virtual {v11, v12}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterStatus;)V

    goto/16 :goto_3
.end method
