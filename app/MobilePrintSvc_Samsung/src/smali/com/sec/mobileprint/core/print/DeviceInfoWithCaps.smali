.class public Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;
.super Ljava/lang/Object;
.source "DeviceInfoWithCaps.java"


# instance fields
.field private mColor:I

.field private mDeviceInfo:Lcom/sec/print/mobileprint/dm/DeviceInfo;

.field private mDeviceStatus:Ljava/lang/String;

.field private mDuplex:I

.field private mMediaSize:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/MediaSizeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMinMargin:I

.field private mResolution:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDeviceInfo:Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 0
    .param p1, "devInfo"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDeviceInfo:Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 25
    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mColor:I

    return v0
.end method

.method public getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDeviceInfo:Lcom/sec/print/mobileprint/dm/DeviceInfo;

    return-object v0
.end method

.method public getDeviceIpAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDeviceInfo:Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDeviceInfo:Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDeviceStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getDuplex()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDuplex:I

    return v0
.end method

.method public getMediaSize()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/MediaSizeInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mMediaSize:Ljava/util/List;

    return-object v0
.end method

.method public getMediaType()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mMediaType:Ljava/util/List;

    return-object v0
.end method

.method public getMinMargin()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mMinMargin:I

    return v0
.end method

.method public getResolution()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mResolution:I

    return v0
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "mColor"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mColor:I

    .line 50
    return-void
.end method

.method public setDeviceIpAddress(Ljava/lang/String;)V
    .locals 1
    .param p1, "mIpAddress"    # Ljava/lang/String;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDeviceInfo:Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public setDeviceName(Ljava/lang/String;)V
    .locals 1
    .param p1, "mDeviceName"    # Ljava/lang/String;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDeviceInfo:Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setName(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public setDeviceStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "mDeviceStatus"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDeviceStatus:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setDuplex(I)V
    .locals 0
    .param p1, "mDuplex"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mDuplex:I

    .line 62
    return-void
.end method

.method public setMediaSize(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/MediaSizeInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "mMediaSize":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mMediaSize:Ljava/util/List;

    .line 32
    return-void
.end method

.method public setMediaType(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "mMediaType":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mMediaType:Ljava/util/List;

    .line 38
    return-void
.end method

.method public setMinMargin(I)V
    .locals 0
    .param p1, "mMinMargin"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mMinMargin:I

    .line 56
    return-void
.end method

.method public setResolution(I)V
    .locals 0
    .param p1, "mResolution"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->mResolution:I

    .line 44
    return-void
.end method
