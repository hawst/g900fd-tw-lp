.class public Lcom/sec/mobileprint/core/print/SamsungPageRange;
.super Ljava/lang/Object;
.source "SamsungPageRange.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ALL_PAGES:Lcom/sec/mobileprint/core/print/SamsungPageRange;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/mobileprint/core/print/SamsungPageRange;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mEnd:I

.field private final mStart:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPageRange;

    const/4 v1, 0x0

    const v2, 0x7fffffff

    invoke-direct {v0, v1, v2}, Lcom/sec/mobileprint/core/print/SamsungPageRange;-><init>(II)V

    sput-object v0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->ALL_PAGES:Lcom/sec/mobileprint/core/print/SamsungPageRange;

    .line 266
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPageRange$1;

    invoke-direct {v0}, Lcom/sec/mobileprint/core/print/SamsungPageRange$1;-><init>()V

    .line 264
    sput-object v0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 286
    return-void
.end method

.method public constructor <init>(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    if-gez p1, :cond_0

    .line 86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "start cannot be less than zero."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    if-gez p2, :cond_1

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "end cannot be less than zero."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_1
    if-le p1, p2, :cond_2

    .line 98
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "start must be lesser than end."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_2
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mStart:I

    .line 104
    iput p2, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mEnd:I

    .line 106
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPageRange;-><init>(II)V

    .line 114
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/mobileprint/core/print/SamsungPageRange;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPageRange;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 198
    if-ne p0, p1, :cond_1

    .line 230
    :cond_0
    :goto_0
    return v1

    .line 204
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 206
    goto :goto_0

    .line 210
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 212
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 216
    check-cast v0, Lcom/sec/mobileprint/core/print/SamsungPageRange;

    .line 218
    .local v0, "other":Lcom/sec/mobileprint/core/print/SamsungPageRange;
    iget v3, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mEnd:I

    iget v4, v0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mEnd:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 220
    goto :goto_0

    .line 224
    :cond_4
    iget v3, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mStart:I

    iget v4, v0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mStart:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 226
    goto :goto_0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mEnd:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mStart:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 180
    const/16 v0, 0x1f

    .line 182
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 184
    .local v1, "result":I
    iget v2, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mEnd:I

    add-int/lit8 v1, v2, 0x1f

    .line 186
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mStart:I

    add-int v1, v2, v3

    .line 188
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 240
    iget v1, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mStart:I

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mEnd:I

    const v2, 0x7fffffff

    if-ne v1, v2, :cond_0

    .line 242
    const-string v1, "PageRange[<all pages>]"

    .line 258
    :goto_0
    return-object v1

    .line 246
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "PageRange["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 250
    iget v2, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mStart:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 252
    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 254
    iget v2, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mEnd:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 256
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 168
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mStart:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 170
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPageRange;->mEnd:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 172
    return-void
.end method
