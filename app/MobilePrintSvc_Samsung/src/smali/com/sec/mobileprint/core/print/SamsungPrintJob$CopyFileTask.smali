.class Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;
.super Landroid/os/AsyncTask;
.source "SamsungPrintJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/print/SamsungPrintJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CopyFileTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field mListener:Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;

.field final synthetic this$0:Lcom/sec/mobileprint/core/print/SamsungPrintJob;


# direct methods
.method private constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V
    .locals 1

    .prologue
    .line 265
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->mListener:Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;)V
    .locals 0

    .prologue
    .line 265
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/String;
    .locals 11
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v10, 0x1

    .line 271
    const/4 v3, 0x0

    .line 272
    .local v3, "fIn":Ljava/io/BufferedInputStream;
    const/4 v5, 0x0

    .line 273
    .local v5, "fOut":Ljava/io/BufferedOutputStream;
    aget-object v9, p1, v10

    if-eqz v9, :cond_0

    .line 274
    aget-object v9, p1, v10

    check-cast v9, Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;

    iput-object v9, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->mListener:Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;

    .line 276
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    sget-object v10, Lcom/sec/mobileprint/core/k2/CoreConstants;->TEMP_DOCUMENT_PATH:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".pdf"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 277
    .local v1, "documentFileName":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/mobileprint/core/utils/FileUtil;->createFolder(Ljava/lang/String;)Z

    .line 280
    const/4 v9, 0x0

    :try_start_0
    aget-object v9, p1, v9

    if-eqz v9, :cond_1

    .line 281
    const/4 v9, 0x0

    aget-object v7, p1, v9

    check-cast v7, Ljava/io/FileDescriptor;

    .line 282
    .local v7, "fileDescriptor":Ljava/io/FileDescriptor;
    new-instance v4, Ljava/io/BufferedInputStream;

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-direct {v4, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 283
    .end local v3    # "fIn":Ljava/io/BufferedInputStream;
    .local v4, "fIn":Ljava/io/BufferedInputStream;
    :try_start_1
    new-instance v6, Ljava/io/BufferedOutputStream;

    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 284
    .end local v5    # "fOut":Ljava/io/BufferedOutputStream;
    .local v6, "fOut":Ljava/io/BufferedOutputStream;
    const/16 v9, 0x400

    :try_start_2
    new-array v0, v9, [B

    .line 286
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/BufferedInputStream;->read([B)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v8

    .local v8, "length":I
    if-gtz v8, :cond_4

    move-object v5, v6

    .end local v6    # "fOut":Ljava/io/BufferedOutputStream;
    .restart local v5    # "fOut":Ljava/io/BufferedOutputStream;
    move-object v3, v4

    .line 292
    .end local v0    # "buffer":[B
    .end local v4    # "fIn":Ljava/io/BufferedInputStream;
    .end local v7    # "fileDescriptor":Ljava/io/FileDescriptor;
    .end local v8    # "length":I
    .restart local v3    # "fIn":Ljava/io/BufferedInputStream;
    :cond_1
    if-eqz v3, :cond_2

    .line 293
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 295
    :cond_2
    if-eqz v5, :cond_3

    .line 296
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 302
    .end local v1    # "documentFileName":Ljava/lang/String;
    :cond_3
    :goto_1
    return-object v1

    .line 287
    .end local v3    # "fIn":Ljava/io/BufferedInputStream;
    .end local v5    # "fOut":Ljava/io/BufferedOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "documentFileName":Ljava/lang/String;
    .restart local v4    # "fIn":Ljava/io/BufferedInputStream;
    .restart local v6    # "fOut":Ljava/io/BufferedOutputStream;
    .restart local v7    # "fileDescriptor":Ljava/io/FileDescriptor;
    .restart local v8    # "length":I
    :cond_4
    const/4 v9, 0x0

    :try_start_4
    invoke-virtual {v6, v0, v9, v8}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 291
    .end local v0    # "buffer":[B
    .end local v8    # "length":I
    :catchall_0
    move-exception v9

    move-object v5, v6

    .end local v6    # "fOut":Ljava/io/BufferedOutputStream;
    .restart local v5    # "fOut":Ljava/io/BufferedOutputStream;
    move-object v3, v4

    .line 292
    .end local v4    # "fIn":Ljava/io/BufferedInputStream;
    .end local v7    # "fileDescriptor":Ljava/io/FileDescriptor;
    .restart local v3    # "fIn":Ljava/io/BufferedInputStream;
    :goto_2
    if-eqz v3, :cond_5

    .line 293
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 295
    :cond_5
    if-eqz v5, :cond_6

    .line 296
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V

    .line 298
    :cond_6
    throw v9
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 299
    :catch_0
    move-exception v2

    .line 300
    .local v2, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_1

    .line 291
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v9

    goto :goto_2

    .end local v3    # "fIn":Ljava/io/BufferedInputStream;
    .restart local v4    # "fIn":Ljava/io/BufferedInputStream;
    .restart local v7    # "fileDescriptor":Ljava/io/FileDescriptor;
    :catchall_2
    move-exception v9

    move-object v3, v4

    .end local v4    # "fIn":Ljava/io/BufferedInputStream;
    .restart local v3    # "fIn":Ljava/io/BufferedInputStream;
    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 310
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 311
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->access$0(Lcom/sec/mobileprint/core/print/SamsungPrintJob;Z)V

    .line 312
    if-nez p1, :cond_1

    .line 313
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->mListener:Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->mListener:Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;

    invoke-interface {v0, v2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;->onFaliure(I)V

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->mListener:Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mFilePath:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->access$1(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)[Ljava/lang/String;

    move-result-object v0

    aput-object p1, v0, v2

    .line 319
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->mListener:Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-interface {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;->onSuccess(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V

    goto :goto_0
.end method
