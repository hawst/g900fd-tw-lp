.class Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;
.super Ljava/lang/Object;
.source "PrintingFramework.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/utils/PrintingFramework;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrintingServiceConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/core/utils/PrintingFramework;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/core/utils/PrintingFramework;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "boundService"    # Landroid/os/IBinder;

    .prologue
    .line 69
    iget-object v1, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    invoke-static {p2}, Lcom/sec/print/mobileprint/ISamsungPrintingService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/print/mobileprint/ISamsungPrintingService;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/mobileprint/core/utils/PrintingFramework;->PrintService:Lcom/sec/print/mobileprint/ISamsungPrintingService;

    .line 70
    iget-object v1, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    # getter for: Lcom/sec/mobileprint/core/utils/PrintingFramework;->printServiceConnectedListenerList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/mobileprint/core/utils/PrintingFramework;->access$0(Lcom/sec/mobileprint/core/utils/PrintingFramework;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 74
    return-void

    .line 70
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;

    .line 71
    .local v0, "listener":Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;
    invoke-interface {v0}, Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;->onPrintServiceConnected()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->PrintService:Lcom/sec/print/mobileprint/ISamsungPrintingService;

    .line 78
    return-void
.end method
