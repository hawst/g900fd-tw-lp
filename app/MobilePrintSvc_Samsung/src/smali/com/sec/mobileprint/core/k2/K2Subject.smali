.class public interface abstract Lcom/sec/mobileprint/core/k2/K2Subject;
.super Ljava/lang/Object;
.source "K2Subject.java"


# virtual methods
.method public abstract doNotifyCannotOpenFile()V
.end method

.method public abstract doNotifyFileLoadingFinished()V
.end method

.method public abstract doNotifyImageReload(I)V
.end method

.method public abstract doNotifyLoadedPage(I)V
.end method

.method public abstract doNotifyMemoryOverflow()V
.end method

.method public abstract doNotifyPDFInputPassword()V
.end method

.method public abstract doNotifyStartFileLoading()V
.end method

.method public abstract doNotifyUpdatablePage(I)V
.end method

.method public abstract registerObserver(Lcom/sec/mobileprint/core/k2/K2Observer;)V
.end method

.method public abstract removeObserver(Lcom/sec/mobileprint/core/k2/K2Observer;)V
.end method
