.class public Lcom/sec/mobileprint/core/k2/CoreConstants;
.super Ljava/lang/Object;
.source "CoreConstants.java"


# static fields
.field public static final DEBUG:Z

.field public static final PRINT_TO_FILE_PATH:Ljava/lang/String;

.field public static final SDCARD_ROOT_PATH:Ljava/lang/String;

.field public static final TEMP_DOCUMENT_PATH:Ljava/lang/String;

.field public static final TEMP_K2_PATH:Ljava/lang/String;

.field public static final TEMP_SPOOL_PATH:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/core/k2/CoreConstants;->SDCARD_ROOT_PATH:Ljava/lang/String;

    .line 8
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/mobileprint/core/k2/CoreConstants;->SDCARD_ROOT_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/SamsungPrintPlugIn/PrintToFile/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/core/k2/CoreConstants;->PRINT_TO_FILE_PATH:Ljava/lang/String;

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/mobileprint/core/k2/CoreConstants;->SDCARD_ROOT_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/SamsungPrintPlugIn/TempFile/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/core/k2/CoreConstants;->TEMP_DOCUMENT_PATH:Ljava/lang/String;

    .line 10
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/mobileprint/core/k2/CoreConstants;->SDCARD_ROOT_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/SamsungPrintPlugIn/TempSpoolFile/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/core/k2/CoreConstants;->TEMP_SPOOL_PATH:Ljava/lang/String;

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/mobileprint/core/k2/CoreConstants;->SDCARD_ROOT_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/SamsungPrintPlugIn/TempK2/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/core/k2/CoreConstants;->TEMP_K2_PATH:Ljava/lang/String;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
